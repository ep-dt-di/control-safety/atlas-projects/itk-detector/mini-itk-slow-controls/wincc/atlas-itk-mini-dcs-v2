# ascii dump of database

# Datapoint/DpId
DpName	TypeName	ID
NTC01	Detector	2859651
NTC02	Detector	2859652
NTC03	Detector	2859653
NTC04	Detector	2859654
NTC05	Detector	2859655
NTC06	Detector	2859656
NTC07	Detector	2859657
NTC08	Detector	2859658
NTC09	Detector	2859659
NTC10	Detector	2859660
NTC11	Detector	2859661
NTC12	Detector	2859662
NTC13	Detector	2859663
NTC14	Detector	2859664
NTC15	Detector	2859665
NTC16	Detector	2859666
NTC17	Detector	2859667
NTC18	Detector	2859668

# DistributionInfo
Manager/User	ElementName	TypeName	_distrib.._type	_distrib.._driver
UI (1)/0	NTC01.ReadOut	Detector	56	\1
UI (1)/0	NTC01.TresholdRead	Detector	56	\1
UI (1)/0	NTC01.TresholdWrite	Detector	56	\1
UI (1)/0	NTC01.TresholdStatus	Detector	56	\1
UI (1)/0	NTC02.ReadOut	Detector	56	\1
UI (1)/0	NTC02.TresholdRead	Detector	56	\1
UI (1)/0	NTC02.TresholdWrite	Detector	56	\1
UI (1)/0	NTC02.TresholdStatus	Detector	56	\1
UI (1)/0	NTC03.ReadOut	Detector	56	\1
UI (1)/0	NTC03.TresholdRead	Detector	56	\1
UI (1)/0	NTC03.TresholdWrite	Detector	56	\1
UI (1)/0	NTC03.TresholdStatus	Detector	56	\1
UI (1)/0	NTC04.ReadOut	Detector	56	\1
UI (1)/0	NTC04.TresholdRead	Detector	56	\1
UI (1)/0	NTC04.TresholdWrite	Detector	56	\1
UI (1)/0	NTC04.TresholdStatus	Detector	56	\1
UI (1)/0	NTC05.ReadOut	Detector	56	\1
UI (1)/0	NTC05.TresholdRead	Detector	56	\1
UI (1)/0	NTC05.TresholdWrite	Detector	56	\1
UI (1)/0	NTC05.TresholdStatus	Detector	56	\1
UI (1)/0	NTC06.ReadOut	Detector	56	\1
UI (1)/0	NTC06.TresholdRead	Detector	56	\1
UI (1)/0	NTC06.TresholdWrite	Detector	56	\1
UI (1)/0	NTC06.TresholdStatus	Detector	56	\1
UI (1)/0	NTC07.ReadOut	Detector	56	\1
UI (1)/0	NTC07.TresholdRead	Detector	56	\1
UI (1)/0	NTC07.TresholdWrite	Detector	56	\1
UI (1)/0	NTC07.TresholdStatus	Detector	56	\1
UI (1)/0	NTC08.ReadOut	Detector	56	\1
UI (1)/0	NTC08.TresholdRead	Detector	56	\1
UI (1)/0	NTC08.TresholdWrite	Detector	56	\1
UI (1)/0	NTC08.TresholdStatus	Detector	56	\1
UI (1)/0	NTC09.ReadOut	Detector	56	\1
UI (1)/0	NTC09.TresholdRead	Detector	56	\1
UI (1)/0	NTC09.TresholdWrite	Detector	56	\1
UI (1)/0	NTC09.TresholdStatus	Detector	56	\1
UI (1)/0	NTC10.ReadOut	Detector	56	\1
UI (1)/0	NTC10.TresholdRead	Detector	56	\1
UI (1)/0	NTC10.TresholdWrite	Detector	56	\1
UI (1)/0	NTC10.TresholdStatus	Detector	56	\1
UI (1)/0	NTC11.ReadOut	Detector	56	\1
UI (1)/0	NTC11.TresholdRead	Detector	56	\1
UI (1)/0	NTC11.TresholdWrite	Detector	56	\1
UI (1)/0	NTC11.TresholdStatus	Detector	56	\1
UI (1)/0	NTC12.ReadOut	Detector	56	\1
UI (1)/0	NTC12.TresholdRead	Detector	56	\1
UI (1)/0	NTC12.TresholdWrite	Detector	56	\1
UI (1)/0	NTC12.TresholdStatus	Detector	56	\1
UI (1)/0	NTC13.ReadOut	Detector	56	\1
UI (1)/0	NTC13.TresholdRead	Detector	56	\1
UI (1)/0	NTC13.TresholdWrite	Detector	56	\1
UI (1)/0	NTC13.TresholdStatus	Detector	56	\1
UI (1)/0	NTC14.ReadOut	Detector	56	\1
UI (1)/0	NTC14.TresholdRead	Detector	56	\1
UI (1)/0	NTC14.TresholdWrite	Detector	56	\1
UI (1)/0	NTC14.TresholdStatus	Detector	56	\1
UI (1)/0	NTC15.ReadOut	Detector	56	\1
UI (1)/0	NTC15.TresholdRead	Detector	56	\1
UI (1)/0	NTC15.TresholdWrite	Detector	56	\1
UI (1)/0	NTC15.TresholdStatus	Detector	56	\1
UI (1)/0	NTC16.ReadOut	Detector	56	\1
UI (1)/0	NTC16.TresholdRead	Detector	56	\1
UI (1)/0	NTC16.TresholdWrite	Detector	56	\1
UI (1)/0	NTC16.TresholdStatus	Detector	56	\1
UI (1)/0	NTC17.ReadOut	Detector	56	\1
UI (1)/0	NTC17.TresholdRead	Detector	56	\1
UI (1)/0	NTC17.TresholdWrite	Detector	56	\1
UI (1)/0	NTC17.TresholdStatus	Detector	56	\1
UI (1)/0	NTC18.ReadOut	Detector	56	\1
UI (1)/0	NTC18.TresholdRead	Detector	56	\1
UI (1)/0	NTC18.TresholdWrite	Detector	56	\1
UI (1)/0	NTC18.TresholdStatus	Detector	56	\1

# PeriphAddrMain
Manager/User	ElementName	TypeName	_address.._type	_address.._reference	_address.._poll_group	_address.._connection	_address.._offset	_address.._subindex	_address.._direction	_address.._internal	_address.._lowlevel	_address.._active	_address.._start	_address.._interval	_address.._reply	_address.._datatype	_address.._drv_ident
ASC (1)/0	NTC01.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[0]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC01.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[0]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC01.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[0]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC01.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[0]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC02.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[1]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC02.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[1]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC02.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[1]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC02.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[1]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC03.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[2]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC03.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[2]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC03.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[2]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC03.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[2]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC04.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[3]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC04.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[3]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC04.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[3]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC04.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[3]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC05.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[4]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC05.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[4]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC05.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[4]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC05.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[4]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC06.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[5]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC06.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[5]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC06.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[5]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC06.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[5]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC07.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[6]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC07.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[6]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC07.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[6]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC07.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[6]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC08.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[7]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC08.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[7]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC08.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[7]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC08.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[7]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC09.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[8]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC09.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[8]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC09.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[8]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC09.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[8]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC10.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[9]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC10.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[9]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC10.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[9]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC10.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[9]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC11.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[10]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC11.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[10]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC11.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[10]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC11.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[10]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC12.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[11]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC12.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[11]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC12.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[11]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC12.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[11]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC13.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[12]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC13.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[12]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC13.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[12]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC13.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[12]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC14.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[13]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC14.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[13]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC14.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[13]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC14.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[13]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC15.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[14]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC15.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[14]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC15.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[14]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC15.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[14]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC16.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[15]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC16.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[15]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC16.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[15]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC16.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[15]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC17.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[16]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC17.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[16]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC17.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[16]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC17.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[16]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC18.ReadOut	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Real_Values"."R_NTC"[17]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC18.TresholdRead	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[17]"	 	 	0	0	\2	0	1	0	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
UI (1)/0	NTC18.TresholdWrite	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Treshold_Values"."Treshold_NTC"[17]"	 	 	0	0	\1	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"
ASC (1)/0	NTC18.TresholdStatus	Detector	16	"OPCUA_ITK_PLC1$OPC_Siemens_S7-1500$1$1$ns=3;s="Bool_Values"."WinCC_NTC_Status"[17]"	 	 	0	0	\2	0	1	1	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	01.01.1970 00:00:00.000	750	"OPCUA"