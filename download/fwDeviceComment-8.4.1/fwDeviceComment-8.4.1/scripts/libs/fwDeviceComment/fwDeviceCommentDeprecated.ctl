/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwDeviceCommentDeprecated.ctl

@author
  Dr. Marc Bengulescu (BE-ICS-FD)
*/

#uses "fwDeviceComment/fwDeviceComment"
#uses "fwGeneral/fwGeneral"


/**
* @deprecated 2018-06-21
*/
public void fwDeviceComment_addRow(string dpe, string application, string device, time timestamp, string user, string comment, 
                                   string commentExt, string sys, string host, bool active)
{  
  FWDEPRECATED();
  fwDeviceComment_commentsTable.updatesEnabled = false;
  fwDeviceComment_commentsTable.appendLine(
       "dpe", dpe,
       "application", application,
       "device", device,
       "timestamp", timestamp,
       "user", user, 
       "comment", comment, 
       "extension", commentExt, 
       "system", sys, 
       "host", host,
       "active", active
   );
   fwDeviceComment_commentsTable.updatesEnabled = true;
}

/**
* Clear all the table change callbacks.
*
* @deprecated 2018-06-22
*/
public void fwDeviceComment_clearFilterAppliedCallbacks() {
  FWDEPRECATED();
  gFwDeviceComment_filterAppliedCallbackFunction = makeDynString();
}

/**
* Clear all the table change callbacks.
*
* @deprecated 2018-06-22
*/
public void fwDeviceComment_clearTableChangedCallbacks() {
  FWDEPRECATED();
  gFwDeviceComment_tableChangedCallbackFunction = makeDynString();
}

/**
* Get the total number of comments in the table.
*
* @deprecated 2018-06-22
*/
public unsigned fwDeviceComment_getTotal() {
  FWDEPRECATED();
  return fwDeviceComment_commentsTable.lineCount();
}


/**
* Get the total number of visible comments in the table (i.e. the number
* that have not been filtered).
*
* @deprecated 2018-06-22
*/
public unsigned fwDeviceComment_getTotalShown() {
  FWDEPRECATED();
  return gFwDeviceComment_totalShown;
}


/**
*
* @deprecated 2018-06-22
*/
public void fwDeviceFilter_clearApplyFilterCallbacks() {
  FWDEPRECATED();
  dynClear(gFwDeviceFilter_applyCBFunction);
}


/**
*
* @deprecated 2018-06-22
*/
public dyn_string fwDeviceFilter_getValues(const signed n, bool &areAllSelected, bool &hasSelectionChanged) {
  FWDEPRECATED();
  mapping filterEnableState = _fwDeviceFilter_getEnableState();
  return _fwDeviceFilter_getValues(n, areAllSelected, hasSelectionChanged, filterEnableState, false);
}

