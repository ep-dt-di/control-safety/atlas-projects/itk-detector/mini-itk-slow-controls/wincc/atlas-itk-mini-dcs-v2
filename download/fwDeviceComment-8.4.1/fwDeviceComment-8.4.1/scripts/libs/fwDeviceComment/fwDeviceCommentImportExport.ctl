/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwDeviceFilter.ctl

@author
  Dr. Marc Bengulescu (BE-ICS-FD)
*/



global string gFwDeviceComment_importFunction;
global string gFwDeviceComment_exportFunction;

public bool fwDeviceComment_import(const string filename) {
  bool result = false;
  if (isFunctionDefined(gFwDeviceComment_importFunction)) {      
      evalScript(
         result,
      			"bool main(string filename)" +
      			"{" +
      			" return " + gFwDeviceComment_importFunction + "(filename);" +
      			"}",
      			makeDynString(),
         filename
      );
  }
  return result;  
}

public bool fwDeviceComment_export(const string filename) {
  bool result = false;
  if (isFunctionDefined(gFwDeviceComment_exportFunction)) {      
      evalScript(
         result,
      			"bool main(string filename)" +
      			"{" +
      			" return " + gFwDeviceComment_exportFunction + "(filename);" +
      			"}",
      			makeDynString(),
         filename
      );
  }
  return result;    
}

public void fwDeviceComment_setImportFunction(const string functionName) {
  gFwDeviceComment_importFunction = functionName;
}

public void fwDeviceComment_setExportFunction(const string functionName) {
  gFwDeviceComment_exportFunction = functionName;
}
