#uses "fwDeviceComment/fwDeviceComment"

void fwDeviceComment_populateTable_test(unsigned max, const dyn_string dpList = makeDynString()) {

    fwDeviceComment_deleteAllRows();
    dyn_string dpe, device;
    dyn_time timestamp;
    dyn_string user, comment;
    dyn_string commentExt, sys, host;
    dyn_bool active;

    unsigned total = 0;


      for (unsigned i = 1; i <= 10; i++) {

                    dynAppend(dpe, "device " + i);
                    dynAppend(device, "alias " + i);
                    dynAppend(timestamp, "");
                    dynAppend(user, "user " + i);
                    dynAppend(comment, "comment " + i);
                    dynAppend(commentExt, "commentExt " + i);
                    dynAppend(sys, "sys " + i);
                    dynAppend(host, "host " + i);
                    dynAppend(active, true);

      }   
    

    fwDeviceComment_addRows(dpe, device, timestamp, user, comment, commentExt, sys, host, active);    
}
