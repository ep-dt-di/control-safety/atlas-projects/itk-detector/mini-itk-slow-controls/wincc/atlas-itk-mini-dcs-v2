//===========================================================
//             HISTORY OF MODIFICATIONS
//    
//             ORIGINAL AUTHOR: Bobby
//
//             15-3-2013 G.M. cosmetic changes
//
//             October 2014
//             Modified by Oliver Holme to 
//             - support redundancy
//             - improve queueing of commands to PLC
//
//          2016-07-25 saved as 3.14 G.M.
//
#uses "fwRack/fwRackDeprecated.ctl"
#uses "fwRack/fwRackTypeConversions.ctl"

#uses "fwDevice/fwDevice.ctl"

//============================================================================
//  new function (O.H. 10-2014)
//
void _fwRackCommand_handleCommandQueue(string PlcDp)
{
DebugN("Starting QUEUE handling for " + PlcDp);

  while(true)
  {
    delay(0,500);
    dyn_blob PlcQueue;
    dpGet(PlcDp + ".Output.Queue", PlcQueue);
    
    if(dynlen(PlcQueue) > 0)
    {
      blob bReply, bBusyBlob;
      string sBusyBlob = "FFFF";
      time tReplyTime, tCommandTime;
      
      dpGet(PlcDp + ".Tables.CommandReceive", bReply,
            PlcDp + ".Tables.CommandReceive:_online.._stime", tReplyTime,
            PlcDp + ".Output.CommandReceive:_online.._stime", tCommandTime);
      
      if(bloblen(bReply))
      {
        blobGetValue(bReply, 62, bBusyBlob, 2);
        sBusyBlob = bBusyBlob;
      }
      
      if ((sBusyBlob != "FFFF" && tReplyTime > tCommandTime) //previous command is processed
         || (tCommandTime < (getCurrentTime()-30))) //or if last action was sent more 30 seconds ago
      {
DebugTN(PlcDp, "PROCESSING A COMMAND");
        string PlcName;
        dyn_string dsExceptions;
        fwDevice_getName(PlcDp, PlcName, dsExceptions);
        fwRackCommand_enterCommand(PlcDp, PlcQueue[1]);  //send the command
        _fwRackConnectionsHandleQueue(PlcName, PlcDp, PlcQueue[1], "remove"); //remove from dyn_string, write to dp
      }
    }
  }
}

//============================================
// function modified (o.H. 10-2014)
// 
// void _fwRackCommand_commandReceived(string receiveDp, blob command)
// {
  //A command has been executed. So, we check if the queue for the related PLC is empty
//   string      PlcDp = dpSubStr(receiveDp, DPSUB_DP);
//   string      PlcName, sBusyBlob;
//   dyn_string  dsExceptions;
//   blob        BusyBlob, bCommand;  
//   dyn_blob    PlcQueue;
// 
//   fwDevice_getName(PlcDp, PlcName,dsExceptions);
//   dpGet(PlcDp + ".Output.Queue", PlcQueue);
//  
  //Command received has changed. So the command was written or executed. Check PLC busy to know  
//   if (bloblen(command)) blobGetValue(command, 62, BusyBlob, 2);
//   sBusyBlob = BusyBlob;
//   if (sBusyBlob == "FFFF")  return; //the PLC did not execute yet the previous command
//   
//   if (dynlen(PlcQueue) > 0) {
//        bCommand = PlcQueue[1];  //take first element in the queue
//        fwRackCommand_enterCommand(PlcDp,bCommand);  //send the command
//        _fwRackConnectionsHandleQueue(PlcName ,PlcDp,  bCommand, "remove"); //remove from dyn_string, write to dp
//   } else plcBusyMapping[PlcName+"_BusyFlag"]=0;    
//      
// }

//============================================================================
string fwRackCommand_enterCommand(string plcdp, blob bCommand)
{
  unsigned SessionNumber;
  string Objectplc, Object;
  
  dpGet(plcdp+".Output.LastSessionNumber",  SessionNumber);
  SessionNumber++;  
  if (SessionNumber > 65535) SessionNumber = 1;
  
  blobSetValue(bCommand, 	0, 	SessionNumber, 		2, 	TRUE); //send command to the PLC memory  
  blobGetValue(bCommand, 34, Objectplc, 16, TRUE);
  
  Object = fwRack_formatString(Objectplc);
  
  dpSetWait(plcdp+".Output.CommandReceive", bCommand);  
  dpSetWait(plcdp+".Output.LastSessionNumber", SessionNumber,
            plcdp+".Output.LastActiveObject", Object);
}

//============================================================================
/*
void fwRackCommand_transferCommand(string CommandDPE, string Command)
{

  CommandDPE       = dpSubStr(CommandDPE, DPSUB_DP_EL);
  string      ObjectDP = dpSubStr(CommandDPE, DPSUB_DP),
	      PlcName,
	      User, Plc, Object, ErrorMask,
	      Response;
  unsigned    SessionNumber;
  blob 	      Identification;
  dyn_string  CommandNames, 
	      CommandTranslations,
              dsExceptions;

  fwDevice_getParent(ObjectDP,PlcName,dsExceptions);

 
  dpGet(ObjectDP + ".NameInPlc", Object);
  

  dpGet(	PlcName+".Tables.Identification",        Identification,
	      	PlcName+".Output.LastSessionNumber",     SessionNumber,
	      	PlcName + ".User",            User);////,
	
  Plc = BlobToString(Identification, 0, 7);

  SessionNumber++;
  if (SessionNumber > 65535)
    SessionNumber = 1;
  
  			
  if (isFunctionDefined("fwAtlasRack_WaitSemaphore")) fwAtlasRack_WaitSemaphore(20000); //set atlas Semaphore
  
  Response = EnterCommand(SessionNumber, User, Plc, Object, ObjectDP, Command, ErrorMask, PlcName);	

  if (isFunctionDefined("fwAtlasRack_ReleaseSemaphore")) fwAtlasRack_ReleaseSemaphore(); //Release atlas Semaphore
  dpSetWait(ObjectDP+".Response", Response);
 
}*/

//============================================================================
//string EnterCommand(unsigned SessionNumber, string User, string Plc, string Object, string ObjectDP, 
//                    string Command, string ErrorMask, string PlcName){

/*string EnterCommand(unsigned SessionNumber, string User, string Plc, string Object, string ObjectDP, 
                    string Command, string ErrorMask, string PlcName)
{	
  
// DebugTN( SessionNumber,  User,  Plc,  Object,  ObjectDP,  Command, ErrorMask,  PlcName);
  blob	ConfigurationTable,
	Output, 
	BusyBlob, 
	Mask ,//= PatternToBlob(ErrorMask),
	CommandReceive, 
	CommandReceiveTest;
	
	//blobZero(Mask,2);
	
  bool 	WaitForResponse, 
	CommandWritten;
	
  string  LastActiveObject, 
	  LastCommand,
	  sBusyBlob;
	
  dyn_string	ObjectNames, 
		ObjectDPs;
	
  //DebugTN("There was a command entered: " + Command);
				
  unsigned IndexBreaker ;			 
  	
  if(Command == "W_MASK"){
    dpGet(ObjectDP + ".Mask.wMask", Mask);		   
  }
  
  
  	
  dpGet(PlcName+".Tables.Configuration", ConfigurationTable);
  blobZero(Output, BlobToInt(ConfigurationTable, 5, 1)*2);
  BusyBlob = "0000";
  //Mask = "00000000";
  blobSetValue(Output, 	0, 	SessionNumber, 		2, 	TRUE);
  blobSetValue(Output, 	2, 	User,			16, 	TRUE);
  blobSetValue(Output, 	18,	Plc, 			16, 	TRUE);
  blobSetValue(Output, 	34, 	Object,			16, 	TRUE);
  blobSetValue(Output, 	50, 	Command,		8, 	TRUE);
  blobSetValue(Output, 	58, 	IndexBreaker,		2, 	TRUE);
  blobSetValue(Output, 	60, 	Mask, 			2, 	TRUE);
  blobSetValue(Output, 	62, 	BusyBlob, 		2, 	TRUE);	

  for (int i=0; i<50 ; i++){
    dpGet(PlcName+".Output.WaitForResponse", WaitForResponse);
    if (!WaitForResponse)
      break;
      // Delay Change by Bobby 11-2-2009.
      delay(0,50);
  }  

  for (int i=0 ; i<10 ; i++){
    dpGet(PlcName+".Tables.CommandReceive", CommandReceive);
    //DebugTN(CommandReceive);
    if(bloblen(CommandReceive))
      blobGetValue(CommandReceive, 62, BusyBlob, 2);
    sBusyBlob = BusyBlob;
    //DebugTN("PLC busy? =" + sBusyBlob);
    if (sBusyBlob != "FFFF"){
      //DebugTN(PlcName, Output);
      dpSetWait(PlcName+".Output.CommandReceive", Output);
      //return ("AUS");
      for (int i=0; i<10; i++){
        dpGet(PlcName+".Tables.CommandReceive", CommandReceiveTest);
        	CommandWritten = CompareBlobs(Output, CommandReceiveTest);
        	//DebugTN(Output, CommandReceiveTest);
        	if (CommandWritten){
        	  BusyBlob = "FFFF";
        	  dpSetWait( PlcName+".Output.CommandReceiveBusy", 	BusyBlob);
        	  dpSetWait(	PlcName+".Output.LastSessionNumber", 	SessionNumber,
        			PlcName+".Output.LastActiveObject", 	ObjectDP,
        			PlcName+".Output.LastCommand", 		Command,
        			PlcName+".Output.WaitForResponse", 	TRUE);
        	  return (Command+": WAITING FOR RESPONSE");
        	}
        	delay(0,100);
      }
    }	
    delay(0,100);
  }	
  return (Command+": COMMAND RECEIVE TABLE UNAVAILABLE");
}
*/


//============================================================================
/*
Callback function of CommandReceiveTable. It has no functionality and only displays the table content
in the log viewer for debugging.
*/
/*
void CommandReceiveTable(string CommandInputRaw, blob Content)
{
	
	string Session =			 BlobToUnsigned(Content, 0, 1);
	string User 					= BlobToString(Content, 1, 8);
	string Plc 					= BlobToString(Content, 9, 8);
	string Equipment 			= BlobToString(Content, 17, 8);
	string Command 			= BlobToString(Content, 25, 4);
	string breakerNumber 	= BlobToUnsigned (Content, 29, 1);
	string ErrorMask 			= BlobToPattern(Content, 30);
	string Busy 				= BlobToPattern(Content, 31);
	
	//DebugTN("Content len: " + bloblen(Content) );
	//DebugTN("Command Receive Table:");
	//DebugTN(	"Session: "+Session+"\n"+
	//	"User: "+User+"\n"+
	//	"Plc: "+Plc+"\n"+
	//	"Equipment: "+Equipment+"\n"+
	//	"Command: "+Command+"\n"+
	//	"Breaker Number: " + breakerNumber +"\n"+
	//	"Error Mask: "+ErrorMask +"\n"+
	//	"Busy: " + Busy);
	
}*/

/*   THE COMMENT BELOW DOES NOT MAKE ANY SENSE. THIS IS NOT A CALLBACK FUNCTION. IT IS CALLED DIRECTLY
     BY _fwRackConnections_commandResponseRawChanged. WaitForResponse is set to TRUE only if the command
     is still in execution. Otherwise it is set to FALSE. The Busy flag is not set at all.
     
ORIGINAL CRAPPY COMMENT: Callback function of CommandResponseTable. The response is analyzed and translated 
into a text message. Then it is propagated to the object that has sent the command. 
WaitForResponse is set TRUE, and the BUSY flag in the CommandResponse table is set FALSE.
*/
void CommandResponseTable(string CommandOutputRaw, blob Content)
{     
	blob 		Busy, commandTable;
	string 			sBusy, PlcName, sResponse, Object, ObjectDP, LastCommand;
	unsigned 		SessionNumber, LastSessionNumber;
	bool			WaitForResponse;
	int 			Response;
	dyn_string		ObjectNames, ObjectDPs, dsExceptions;
	string			sMask;
	
 PlcName = CommandOutputRaw;
 if (PlcName == "") return;
 
 SessionNumber = BlobToUnsigned(Content, 0, 1);     
 Response = BlobToInt(Content, 1, 1);
 
	blobGetValue(Content, 4, Busy, 2);
	sBusy = Busy;
        
	if (sBusy != "FFFF") return;
	
	switch (Response){
		case 1:	sResponse = "COMMAND EXECUTED";             break;
		case 2:	sResponse = "USER REJECTED";                break;
		case 3:	sResponse = "PLC IDENTIFICATION ERROR";     break;
		case 4:	sResponse = "EQUIPMENT UNKNOWN";            break;
		case 5:	sResponse = "COMMAND UNKNOWN";              break;
		case 6:	sResponse = "EQUIPMENT LOCKED";             break;
		case 7:	sResponse = "NO CONNECTION TO EQUIPMENT";   break;
		case 8:	sResponse = "UNABLE TO EXECUTE COMMAND";    break;
		case 9:	sResponse = "TIMEOUT FOR EQUIPMENT";        break;
		// response case added by Bobby. Reported by LHCb. 11-2-2009
		case 12: sResponse = "Executing Command";           break;
  case 0: sResponse = "COMMAND EXECUTED";             break;
  
  default: sResponse = "Unknown response";            break;
	}					        
        
	dpGet(PlcName+".Output.LastSessionNumber", LastSessionNumber,
	      PlcName+".Output.WaitForResponse",   WaitForResponse);

  
  //DebugTN("SessionNumber : " +SessionNumber);	
  //DebugTN("LastSessionNumber : " +LastSessionNumber);	  
	
	  if (SessionNumber == LastSessionNumber) {
	    	dpGet(	PlcName  +".Output.LastActiveObject", Object,
		  	        PlcName  +".Output.LastCommand",      LastCommand);
      strreplace(Object," ","");      
        
      if (Object == "") return;        
             
      dpSetWait(PlcName + "/" + Object+ ".Response",sResponse,
	  		           PlcName+".Output.WaitForResponse",(Response == 12)/*  FALSE*/);       
    }	

}

//---------------------

//============================================================================
void _fwRackCommand_configureOutput(string ConfigDPE, blob Config_Raw)
{
int 		PlcNumber, DrvNumber,Address;
string 	PlcName, CommandReceiveDPE, CommandReceiveBusyDPE, CommandResponseBusyDPE;

if (!_fwRackCommand_testContent(Config_Raw)) {
	   DebugTN("Error: Configuration table invalid.");
	   return;
}

blobGetValue(Config_Raw,4*2,Address,2,1);  //Address = BlobToUnsigned(Config_Raw, 4, 1);
 
PlcName = dpSubStr(ConfigDPE, DPSUB_DP);
CommandReceiveDPE = PlcName+".Output.CommandReceive";
CommandReceiveBusyDPE = PlcName+".Output.CommandReceiveBusy";
CommandResponseBusyDPE = PlcName+".Output.CommandResponseBusy";

if(DEBUG)
  DebugTN("Connecting with command receive table "+ConfigDPE+"-- fwRackCommand.ctl / _fwRackCommand_configureOutput");

//------ O.H. 10-2014 adapted to redundancy
dpGet(fwInstallationRedu_getReduDp("_"+PlcName+"_Modbus") + ".PlcNumber", PlcNumber);
dpGet(fwInstallationRedu_getReduDp("_"+PlcName+"_Modbus") + ".DrvNumber", DrvNumber);

dpSetWait( CommandReceiveDPE+":_distrib.._type", 56,
			        CommandReceiveDPE+":_distrib.._driver", DrvNumber,
			        CommandReceiveDPE+":_address.._type", 16,
			        CommandReceiveDPE+":_address.._active", TRUE,
			        CommandReceiveDPE+":_address.._datatype", 570,
			        CommandReceiveDPE+":_address.._direction", 1,
			        CommandReceiveDPE+":_address.._drv_ident", "MODBUS",
			        CommandReceiveDPE+":_address.._internal", FALSE,
			        CommandReceiveDPE+":_address.._mode", 1,
			        CommandReceiveDPE+":_address.._reference", "M."+PlcNumber+".16."+Address,
			        CommandReceiveDPE+":_address.._subindex", 0);

Address += 31;

dpSetWait(	CommandReceiveBusyDPE+":_distrib.._type", 56,
			        CommandReceiveBusyDPE+":_distrib.._driver", DrvNumber,
			        CommandReceiveBusyDPE+":_address.._type", 16,
			        CommandReceiveBusyDPE+":_address.._active", TRUE,
			        CommandReceiveBusyDPE+":_address.._datatype", 570,
			        CommandReceiveBusyDPE+":_address.._direction", 1,
			        CommandReceiveBusyDPE+":_address.._drv_ident", "MODBUS",
			        CommandReceiveBusyDPE+":_address.._internal", FALSE,
			        CommandReceiveBusyDPE+":_address.._mode", 1,
			        CommandReceiveBusyDPE+":_address.._reference", "M."+PlcNumber+".16."+Address,
			        CommandReceiveBusyDPE+":_address.._subindex", 0);



blobGetValue(Config_Raw,6*2,Address,2,1);   //Address = BlobToUnsigned(Config_Raw, 6, 1);
Address += 2;
dpSetWait(CommandResponseBusyDPE+":_distrib.._type", 56,
					     CommandResponseBusyDPE+":_distrib.._driver", DrvNumber,
					     CommandResponseBusyDPE+":_address.._type", 16,
					     CommandResponseBusyDPE+":_address.._active", TRUE,
					     CommandResponseBusyDPE+":_address.._datatype", 570,
					     CommandResponseBusyDPE+":_address.._direction", 1,
					     CommandResponseBusyDPE+":_address.._drv_ident", "MODBUS",
					     CommandResponseBusyDPE+":_address.._internal", FALSE,
					     CommandResponseBusyDPE+":_address.._mode", 1,
					     CommandResponseBusyDPE+":_address.._reference", "M."+PlcNumber+".16."+Address,
					     CommandResponseBusyDPE+":_address.._subindex", 0);
}


//============================================================================
/*
This function makes a simple test of a blob variable. If the length is 0 or the content is zeros,
it returns FALSE, else TRUE.
*/
bool _fwRackCommand_testContent(blob &Content)
{
	string 	sContent, sCompare;
	blob 		Compare;
	int 		Length = bloblen(Content);
	
	if (Length == 0)	{
		  DebugTN("Table Length = 0");
		  return FALSE;
	}
 	
	blobZero(Compare, Length);
	sContent = Content;
	sCompare = Compare;
	
	if (sContent == sCompare)		{
		  DebugTN("Table Content is zero");
		  return FALSE;
	}		
	return TRUE;
}
