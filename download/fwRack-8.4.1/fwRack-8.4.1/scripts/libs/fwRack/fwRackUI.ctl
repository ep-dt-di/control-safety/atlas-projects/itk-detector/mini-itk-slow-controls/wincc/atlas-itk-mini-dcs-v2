//==========================================================
//       HISTORY OF MODIFICATIONS
//
//       ORIGINAL AUTHOR: Bobby
//
//       15-3-2013 G.M. cosmetic changes
//
//
//          2016-07-25 saved as 3.14 G.M.
//

//-----------------------------------------------------------
//        LIST OF FUNCTIONS
//
//   void fwRackUI_addCanvas(string sDpName, int x, int y, float rot, float sizeX, float sizeY,  
//                           bool bTouchScreen = FALSE, string col ="{234,226,216}")
//   void fwRackUI_addRackCrateCanvas(string sDpName, int posX, int posY, float sX=1, float sY=1)
//   void fwRackUI_addRackAlertList(string sDpName, int posX, int posY, float sX=1, float sY=1)
//   void fwRackUI_setTitle(string selTitle)
//   void fwRackUI_setProperty(string selProperty)
//   void fwRackUI_clearSelection(string sDpName)
//   void fwRackUI_getSelectedRacks(string sDpName, dyn_string &dsSelectedRacks)
//   void fwRackUI_selectRack(string sRackName, string sDpName)
//   void fwRackUi_addChangeLabelTSB(string sDpName, int posX, int posY)
//   void fwRackUi_addChangePropertyTSB (string sDpName, int posX, int posY)
//   void fwRackUi_addClearSelectionTSB (string sDpName, int posX, int posY)
//   void fwRackUi_addGroupCommandsTSB  (string sDpName, int posX , int posY)
//   void fwRackUi_addChangeUserTSB(string sDpName, int posX , int posY)
//   void fwRackUi_addHelpTSB      (string sDpName, int posX , int posY)
//   void fwRackUi_addExpertTSB     (string sDpName, int posX , int posY)

//   void fwRackUI_refreshSkin(string      panel,
//                             dyn_string  buttons,
//                             dyn_string  buttonsTexts,
//                             dyn_string  frames,
//                             dyn_string  mainButtons,
//                             dyn_string  mainButtonsTexts,
//                             dyn_string  racks,
//                             dyn_string  canvas,
//                             dyn_string  tables,
//                             dyn_string  texts,
//                             dyn_string  titles,
//                             dyn_string  textFields)
//
//   void fwRackUI_updateSkin(string dpSkin)
//   void fwRackUI_registerSkinVariables()
#uses "fwRack/fwRackDeprecated.ctl"


//==================================================================================
void fwRackUI_addCanvas(string sDpName, int x, int y, float rot, float sizeX, float sizeY,  
                        bool bTouchScreen = FALSE, string col ="{234,226,216}")
{
  float OrigSizeX = 1062;
  float OrigSizeY = 1010;
  float sX = sizeX/OrigSizeX, sY = sizeY/OrigSizeY;
  
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackCanvas.pnl", 
            "Rack Canvas", 
            makeDynString("$sDpName:" + sDpName,
                          "$sBackgroundColor:"  + col,
                          "$bTouchScreen:" +bTouchScreen),
            x,y,rot,sX,sY);       
}


//===================================================================================
void fwRackUI_addRackCrateCanvas(string sDpName, int posX, int posY, float sX=1, float sY=1)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackCrateCanvas.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName,
                          "$sX:" + sX,
                          "$sY:" + sY),
            posX,posY,0,sX,sY);   
}

//===================================================================================
void fwRackUI_addRackAlertList(string sDpName, int posX, int posY, float sX=1, float sY=1)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackActiveAlerts.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName,
                          "$sX:" + sX,
                          "$sY:" + sY),
            posX,posY,0,sX,sY);    
}

//===================================================================================
void fwRackUI_setTitle(string selTitle)
{
  setValue($sDpName + "RackTitlesRequest","text",selTitle);  
}

//===================================================================================
void fwRackUI_setProperty(string selProperty)
{
  setValue($sDpName + "RackPropertyRequest","text",selProperty);  
}
//===================================================================================
void fwRackUI_clearSelection(string sDpName)
{
  shape cmbSelectedRacks = getShape(sDpName + "SelectedRacks");
  dyn_string dsSelectedRacks;
  fwRackUI_getSelectedRacks(sDpName, dsSelectedRacks);
  for(int i=1; i <= dynlen(dsSelectedRacks); i++){
      fwRackUI_selectRack (dsSelectedRacks[i],sDpName);
  }
}

//===================================================================================
void fwRackUI_getSelectedRacks(string sDpName, dyn_string &dsSelectedRacks)
{
  shape cmbSelectedRacks = getShape(sDpName + "SelectedRacks");
  dsSelectedRacks = cmbSelectedRacks.items();  
}

//===================================================================================
void fwRackUI_selectRack(string sRackName, string sDpName)
{
  setValue(sDpName + "UpdateSelectedRacks","text",sRackName);  
}

//////////////////////////PREMAID CONTROLS//////////////////////////

//===================================================================================
void fwRackUi_addChangeLabelTSB(string sDpName, int posX, int posY)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackTouchScreenButtonTitle.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName),
            posX,posY,0,1,1);   
}

//===================================================================================
void fwRackUi_addChangePropertyTSB (string sDpName, int posX, int posY)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackTouchScreenButtonProperty.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName),
            posX,posY,0,1,1); 
  
}
//===================================================================================
void fwRackUi_addClearSelectionTSB (string sDpName, int posX, int posY)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackTouchScreenButtonGroupSelection.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName),
            posX,posY,0,1,1);   
}
//===================================================================================
void fwRackUi_addGroupCommandsTSB  (string sDpName, int posX , int posY)
{
   addSymbol(myModuleName(), myPanelName(),  
             "fwRack/fwRackTouchScreenButtonGroupCommands.pnl", 
             "ChangeLabelButton", 
             makeDynString("$sDpName:" + sDpName),
             posX,posY,0,1,1);
  
}

//===================================================================================
void fwRackUi_addChangeUserTSB(string sDpName, int posX , int posY)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackTouchScreenButtonUserRole.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName),
            posX,posY,0,1,1);
}

//===================================================================================
void fwRackUi_addHelpTSB(string sDpName, int posX , int posY)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackTouchScreenButtonHelp.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName),
            posX,posY,0,1,1); 
}

//===================================================================================
void fwRackUi_addExpertTSB(string sDpName, int posX , int posY)
{
  addSymbol(myModuleName(), myPanelName(),  
            "fwRack/fwRackTouchScreenButtonExpert.pnl", 
            "ChangeLabelButton", 
            makeDynString("$sDpName:" + sDpName),
            posX,posY,0,1,1); 
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//===================================================================================
void fwRackUI_refreshSkin(string      panel,
                          dyn_string  buttons,
                          dyn_string  buttonsTexts,
                          dyn_string  frames,
                          dyn_string  mainButtons,
                          dyn_string  mainButtonsTexts,
                          dyn_string  racks,
                          dyn_string  canvas,
                          dyn_string  tables,
                          dyn_string  texts,
                          dyn_string  titles,
                          dyn_string  textFields)
{
  string Skin;  
  int i;

  dpGet("fwRackSettings.Skin",  Skin);
  if (!globalExists("FWRACK_SKIN")) {
    fwRackUI_registerSkinVariables();
    if (dpExists("fwRackSettings.Skin")) {  
      //DebugTN("Skin: " + Skin);
      if (dpExists("fwRackSkin_" + Skin)) {
        FWRACK_SKIN = Skin;
        fwRackUI_updateSkin("fwRackSkin_" + FWRACK_SKIN);  
      } else {
        FWRACK_SKIN = "";
        DebugTN("Referenced skin not existing...");
        return; 
      }
    } else {
      FWRACK_SKIN = "";
      DebugTN("Missing internal rack settings datapoints...");
      return;
    }
  } else { fwRackUI_updateSkin("fwRackSkin_" + Skin); }
  
  if (shapeExists(panel)) { 
    shape Panel = getShape(panel);   
    Panel.backCol = FWRACK_SKIN_BACKGROUND;
  }
  for (i = 1; i <= dynlen (canvas); i++) {
    shape button = getShape(canvas[i]);      
    button.backCol = FWRACK_SKIN_RACK_CANVAS_BACKGROUND;
    //button.foreCol = FWRACK_SKIN_BUTTON_FOREGROUND;
  } 
  for (i = 1; i <= dynlen (racks); i++) {
    shape button = getShape(racks[i] + "recName");      
    button.backCol = FWRACK_SKIN_RACK_TITLE_BACKGROUND;
    if (shapeExists(racks[i]+ + "TouchScreenTitleText")) {
      button = getShape(racks[i]+ + "TouchScreenTitleText");
      button.foreCol = FWRACK_SKIN_RACK_TITLE_FOREGROUND;
    }
    button = getShape(racks[i]+ + "TitleText");
    button.foreCol = FWRACK_SKIN_RACK_TITLE_FOREGROUND;
    button = getShape(racks[i]+ + "RackCanvas");
    button.backCol = FWRACK_SKIN_RACKBACKGROUND;        
  } 
  for (i = 1; i <= dynlen (tables); i++) {
    shape button = getShape(tables[i]);      
    button.backCol = FWRACK_SKIN_TABLE_BACKGROUND;    
    //button.foreCol = FWRACK_SKIN_BUTTON_FOREGROUND;
  } 
  for (i = 1; i <= dynlen (buttons); i++) {
    shape button = getShape(buttons[i]);      
    button.backCol = FWRACK_SKIN_BUTTON_BACKGROUND;
    //button.foreCol = FWRACK_SKIN_BUTTON_FOREGROUND;
  } 
  for (i = 1; i <= dynlen (buttonsTexts); i++) {
    shape button = getShape(buttonsTexts[i]);      
    //button.backCol = FWRACK_SKIN_BUTTON_BACKGROUND;
    button.foreCol = FWRACK_SKIN_BUTTON_FOREGROUND;
  }  
  for (i = 1; i <= dynlen (mainButtons); i++) {
    shape button = getShape(mainButtons[i]); 
    button.backCol = FWRACK_SKIN_MAIN_BUTTON_BACKGROUND;
    //button.foreCol = FWRACK_SKIN_MAIN_BUTTON_FOREGROUND;   
  }   
  for (i = 1; i <= dynlen (mainButtonsTexts); i++) {
    shape button = getShape(mainButtonsTexts[i]); 
    //button.backCol = FWRACK_SKIN_MAIN_BUTTON_BACKGROUND;
    button.foreCol = FWRACK_SKIN_MAIN_BUTTON_FOREGROUND;   
  } 
  for (i = 1; i <= dynlen (texts); i++) {
    shape button = getShape(texts[i]); 
    button.backCol = FWRACK_SKIN_TEXT_BACKGROUND;
    button.foreCol = FWRACK_SKIN_TEXT_FOREGROUND;   
  }  
  for (i = 1; i <= dynlen (titles); i++) {
    shape button = getShape(titles[i]);     
    button.foreCol = FWRACK_SKIN_TEXT_TITLE_FOREGROUND;   
  }  
  for (i = 1; i <= dynlen (frames); i++) {
    shape button = getShape(frames[i]);     
    button.foreCol = FWRACK_SKIN_FRAME_FOREGROUND;
  } 
  for (i = 1; i <= dynlen (textFields); i++) {
    shape button = getShape(textFields[i]);    
    button.backCol = FWRACK_SKIN_TEXTFIELD_BACKGROUND; 
    button.foreCol = FWRACK_SKIN_TEXTFIELD_FOREGROUND;
  }   
  
  //DebugTN("Colors updated");
}

//===================================================================================
void fwRackUI_updateSkin(string  dpSkin)
{  
  FWRACK_SKIN = substr( dpSubStr (dpSkin,DPSUB_DP),11,strlen(dpSkin)-11) ;
  dpGet(dpSkin + ".Colors.Background",                FWRACK_SKIN_BACKGROUND,
        dpSkin + ".Colors.ButtonForeground",          FWRACK_SKIN_BUTTON_FOREGROUND,
        dpSkin + ".Colors.ButtonBackground",          FWRACK_SKIN_BUTTON_BACKGROUND,
        dpSkin + ".Colors.FrameForeground",           FWRACK_SKIN_FRAME_FOREGROUND,
        dpSkin + ".Colors.MainButtonForeground",      FWRACK_SKIN_MAIN_BUTTON_FOREGROUND,
        dpSkin + ".Colors.MainButtonBackground",      FWRACK_SKIN_MAIN_BUTTON_BACKGROUND,
        dpSkin + ".Colors.RackBackground",            FWRACK_SKIN_RACKBACKGROUND,
        dpSkin + ".Colors.RackCanvasForeground",      FWRACK_SKIN_RACK_CANVAS_FOREGROUND,
        dpSkin + ".Colors.RackCanvasBackground",      FWRACK_SKIN_RACK_CANVAS_BACKGROUND,
        dpSkin + ".Colors.RackTitleForeground",       FWRACK_SKIN_RACK_TITLE_FOREGROUND,
        dpSkin + ".Colors.RackTitleBackground",       FWRACK_SKIN_RACK_TITLE_BACKGROUND,
        dpSkin + ".Colors.TableForeground",           FWRACK_SKIN_TABLE_FOREGROUND,
        dpSkin + ".Colors.TableBackground",           FWRACK_SKIN_TABLE_BACKGROUND,
        dpSkin + ".Colors.TextForeground",            FWRACK_SKIN_TEXT_FOREGROUND,
        dpSkin + ".Colors.TextBackground",            FWRACK_SKIN_TEXT_BACKGROUND,   
        dpSkin + ".Colors.TitleTextForeground",       FWRACK_SKIN_TEXT_TITLE_FOREGROUND,
        dpSkin + ".Colors.TextFieldBackground",       FWRACK_SKIN_TEXTFIELD_BACKGROUND,   
        dpSkin + ".Colors.TextFieldForeground",       FWRACK_SKIN_TEXTFIELD_FOREGROUND);      
}

//===================================================================================
void fwRackUI_registerSkinVariables()
{
  addGlobal("FWRACK_SKIN",                             STRING_VAR); 
  addGlobal("FWRACK_SKIN_BACKGROUND",                  STRING_VAR); 
  addGlobal("FWRACK_SKIN_BUTTON_FOREGROUND",           STRING_VAR); 
  addGlobal("FWRACK_SKIN_BUTTON_BACKGROUND",           STRING_VAR); 
  addGlobal("FWRACK_SKIN_FRAME_FOREGROUND",            STRING_VAR); 
  addGlobal("FWRACK_SKIN_MAIN_BUTTON_FOREGROUND",      STRING_VAR); 
  addGlobal("FWRACK_SKIN_MAIN_BUTTON_BACKGROUND",      STRING_VAR); 
  addGlobal("FWRACK_SKIN_RACKBACKGROUND",              STRING_VAR); 
  addGlobal("FWRACK_SKIN_RACK_CANVAS_BACKGROUND",      STRING_VAR); 
  addGlobal("FWRACK_SKIN_RACK_CANVAS_FOREGROUND",      STRING_VAR); 
  addGlobal("FWRACK_SKIN_RACK_TITLE_BACKGROUND",       STRING_VAR); 
  addGlobal("FWRACK_SKIN_RACK_TITLE_FOREGROUND",       STRING_VAR); 
  addGlobal("FWRACK_SKIN_TABLE_BACKGROUND",            STRING_VAR); 
  addGlobal("FWRACK_SKIN_TABLE_FOREGROUND",            STRING_VAR); 
  addGlobal("FWRACK_SKIN_TEXT_FOREGROUND",             STRING_VAR); 
  addGlobal("FWRACK_SKIN_TEXT_BACKGROUND",             STRING_VAR);   
  addGlobal("FWRACK_SKIN_TEXT_TITLE_FOREGROUND",       STRING_VAR);   
  addGlobal("FWRACK_SKIN_TEXTFIELD_BACKGROUND",        STRING_VAR);   
  addGlobal("FWRACK_SKIN_TEXTFIELD_FOREGROUND",        STRING_VAR);   
}
