/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwSynopticLocatorGedi_ext.ctl

*/   

main() 
{
  int id  = moduleAddMenu("JCOP Framework"); 
  int id_sub = moduleAddSubMenu("Synoptic Locator", id);
  
  
  int id2 = moduleAddAction("Synoptic Locator Management", "", "", id_sub, 0, "openSynopticLocator");
 
 }

 /**
 * @reviewed 2018-06-26 @whitelisted{WinCCOAIntegration}
 */
void openSynopticLocator()
{
  ModuleOnWithPanel("Synoptic Locator Management", 5, 5, 10, 20, 1, 1, "", "fwSynopticLocator/SynopticLocator.pnl", "", makeDynString()); 
}
