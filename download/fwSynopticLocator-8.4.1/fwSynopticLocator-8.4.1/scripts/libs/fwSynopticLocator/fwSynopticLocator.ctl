/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwSynopticLocator.ctl

*/

#uses "fwSynopticLocator/fwSynopticLocatorLogic.ctl"
#uses "fwSynopticLocator/fwSynopticLocatorDeprecated.ctl"
#uses "fwGeneral/fwProgressBar.ctl"
#uses "fwGeneral/fwException.ctl"

const string COMMUNICATION_DPE_PATHS = "_fwSynopticLocator_System_Communication.Configuration.Paths:_original.._value";
const string COMMUNICATION_DPE_DEVICE_INDENTIFIER = "_fwSynopticLocator_System_Communication.Configuration.Identifiers.Device:_original.._value";
const string COMMUNICATION_DPE_PANEL_TITLE_IDENTIFIER = "_fwSynopticLocator_System_Communication.Configuration.Identifiers.Panel_Title:_original.._value";

//Triggers for load the information of the different tabs
//Only when clicked, and onces load, it doesn't load the information again.
const string communicationDPETabVisualizationDevice = "_fwSynopticLocator_System_Communication.TriggersTab.TabVisualizationDevice:_original.._value";
const string communicationDPETabVisualizationPanel = "_fwSynopticLocator_System_Communication.TriggersTab.TabVisualizationPanel:_original.._value";
const string communicationDPETabVerificationDevice = "_fwSynopticLocator_System_Communication.TriggersTab.TabVerificationDevice:_original.._value";
const string communicationDPETabVerificationPanel = "_fwSynopticLocator_System_Communication.TriggersTab.TabVerificationPanel:_original.._value";
const string communicationDPETabVerificationAnimations = "_fwSynopticLocator_System_Communication.TriggersTab.TabVerificationAnimations:_original.._value";


const string COMMUNICATION_TRIGGER_DPE_LOG = "_fwSynopticLocator_System_Communication.Triggers.Log:_original.._value";

const string progressFunction = "main(float progressValue) { fwShowProgressBar(\"Executing the requested action...\", progressValue); }";

  /**
    
   Takes all the synoptic locator building function parameters from the configuration DPs and execute the populate function.
   
   @param dpeThatChanged Is the name of the DPE that changed.
   @param value Is the value which the DPE changed to.
   @pre The system is initialized: The DP Types exists.
   @post The system is populated, taking into account the DP configuration parameters.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */ 

  public int fwSynopticLocator_Build(string dpeThatChanged, bool value) {
  
    dyn_string paths;
    dpGet(COMMUNICATION_DPE_PATHS, paths);
    
    dyn_string deviceIdentifiers;
    dpGet(COMMUNICATION_DPE_DEVICE_INDENTIFIER, deviceIdentifiers);
  
    dyn_string panelTitleIdentifiers;
    dpGet(COMMUNICATION_DPE_PANEL_TITLE_IDENTIFIER, panelTitleIdentifiers);
    
    int replyCode = fwSynopticLocator_populateSynopticLocator(paths, deviceIdentifiers, panelTitleIdentifiers, progressFunction);
        
    if (replyCode==0) { dpSetWait(COMMUNICATION_TRIGGER_DPE_LOG, "Population succesfully finished."); }
    else if (replyCode>0) { dpSetWait(COMMUNICATION_TRIGGER_DPE_LOG, "Already built."); }
    else { dpSetWait(COMMUNICATION_TRIGGER_DPE_LOG, "Could not populate."); }

    //Update the other tabs with the new devices
    //False in communicationDPETabs means when click in the tab and the tab read the state, this tab knows that it is necessary update.
    dyn_string communicationDPETabs = makeDynString(communicationDPETabVisualizationDevice,
                                                  communicationDPETabVisualizationPanel,
                                                  communicationDPETabVerificationDevice,
                                                  communicationDPETabVerificationPanel,
                                                  communicationDPETabVerificationAnimations);
    int rc = dpSetWait(communicationDPETabs, makeDynBool(false, false, false, false, false)); 
    
    return replyCode;
  
  }

  /**
   
   Clears the synoptic locator.
   
   @param dpeThatChanged Is the name of the DPE that changed.
   @param value Is the value which the DPE changed to.
   @pre The system is already initialized.
   @post The system is initialized and empty.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   @reviewed 2018-06-26 @whitelisted{Callback}

   */  
  
  public int fwSynopticLocator_Clean(string dpeThatChanged, bool value) {

    int replyCode = fwSynopticLocator_clearSynopticLocator(progressFunction);
    
    if (replyCode == 0) { dpSetWait(COMMUNICATION_TRIGGER_DPE_LOG, "Synoptic Locator was cleared succesfully."); }

    //Update the other tabs with the new devices
    //False in communicationDPETabs means when click in the tab and the tab read the state, this tab knows that it is necessary update.
    dyn_string communicationDPETabs = makeDynString(communicationDPETabVisualizationDevice,
                                                  communicationDPETabVisualizationPanel,
                                                  communicationDPETabVerificationDevice,
                                                  communicationDPETabVerificationPanel,
                                                  communicationDPETabVerificationAnimations);
    int rc = dpSetWait(communicationDPETabs, makeDynBool(false, false, false, false, false)); 

    return replyCode;
  
  }

  /**
   
   Updates the Synoptic Locator entries. 
   Much more efficient than re-populating it, if there are not a lot of changes.
   
   @param dpeThatChanged Is the name of the DPE that changed.
   @param value Is the value which the DPE changed to.
   @pre The system is already initialized.
   @post The system is initialized and populated, taking into account the DP configuration parameters.
   @return Dyn_int: [1] -> Deleted panels // [2] -> Updated panels // [3] -> Added panels // [4] -> Errors (Error: 0 -> OK, !0 -> Error) 
   
   @reviewed 2018-06-26 @whitelisted{Callback}
   */ 
   
  public dyn_int fwSynopticLocator_Update(string dpeThatChanged, bool value) { 
  
    dyn_string paths;
    dpGet(COMMUNICATION_DPE_PATHS, paths);
  
    dyn_string deviceIdentifiers;
    dpGet(COMMUNICATION_DPE_DEVICE_INDENTIFIER, deviceIdentifiers);
  
    dyn_string panelTitleIdentifiers;
    dpGet(COMMUNICATION_DPE_PANEL_TITLE_IDENTIFIER, panelTitleIdentifiers);
  
    string logS;
    dyn_int replyCode;
          
    replyCode = fwSynopticLocator_updateSynopticLocator(paths, deviceIdentifiers, panelTitleIdentifiers, progressFunction);
        
    if (replyCode[4] == 0) {      
       //Dyn_int: [1] -> Deleted panels // [2] -> Updated panels // [3] -> Added panels // [4] -> Errors (Error: 0 -> OK, !0 -> Error) 
       logS = "Update: " + replyCode[1] + " deleted, " + replyCode[2] + " updated, " + replyCode[3] + " added.";      
    }
    else { logS = "Something went wrong: update could not be performed."; }
    dpSetWait(COMMUNICATION_TRIGGER_DPE_LOG, logS);

    //Update the other tabs with the new devices
    //False in communicationDPETabs means when click in the tab and the tab read the state, this tab knows that it is necessary update.
    if(replyCode[1] != 0 && replyCode[2] != 0 && replyCode[3] != 0)
    {
      //This means some change in the "dictonary". Update the tabs.
      dyn_string communicationDPETabs = makeDynString(communicationDPETabVisualizationDevice,
                                                  communicationDPETabVisualizationPanel,
                                                  communicationDPETabVerificationDevice,
                                                  communicationDPETabVerificationPanel,
                                                  communicationDPETabVerificationAnimations);
      int rc = dpSetWait(communicationDPETabs, makeDynBool(false, false, false, false, false)); 
    }

        
    return replyCode;

  }

/** 
 * Getter function for  COMMUNICATION_DPE_PATHS
 * @param dsException	(dyn_string)	OUT : exception container
 * @return value of type 'dyn_string': list of path currently configured
 */
public dyn_string fwSynopticLocator_getConfiguration_path(dyn_string &dsException)
{
	return _fwSynopticLocator_getConfiguration_generic( __FUNCTION__, COMMUNICATION_DPE_PATHS, dsException);
}

/** 
 * Setter function for COMMUNICATION_DPE_PATHS
 * @param dsPaths	(dyn_string)	IN : list of paths to be set
 * @param dsException	(dyn_string)	IN/OUT : exception handler
 * @return value of type 'void'
 */
public void fwSynopticLocator_setConfiguration_path( dyn_string dsPath, dyn_string &dsException)
{
	
	_fwSynopticLocator_setConfiguration_generic(__FUNCTION__, COMMUNICATION_DPE_PATHS, dsPath, dsException);
}

/** 
 * Getter function for the configuration  COMMUNICATION_DPE_DEVICE_INDENTIFIER
 * @param dsException	(dyn_string)	IN/OUT : exception handler
 * @return value of type 'dyn_string': list of dollar parameters identifying the devices
 */
public dyn_string fwSynopticLocator_getConfiguration_deviceIdentifier( dyn_string &dsException)
{
	return _fwSynopticLocator_getConfiguration_generic( __FUNCTION__, COMMUNICATION_DPE_DEVICE_INDENTIFIER, dsException);
}


/** 
 * Setter function for COMMUNICATION_DPE_DEVICE_INDENTIFIER
 * @param dsDeviceIdentifiers	(dyn_string)	IN : list of device identifier to be set
 * @param dsException	(dyn_string)	IN/OUT : exception handler
 * @return value of type 'void'
 */
public void fwSynopticLocator_setConfiguration_deviceIdentifier( dyn_string dsDeviceIdentifiers, dyn_string &dsException)
{
	_fwSynopticLocator_setConfiguration_generic(__FUNCTION__, COMMUNICATION_DPE_DEVICE_INDENTIFIER, dsDeviceIdentifiers, dsException);
}

/** 
 * Getter function for the configuration COMMUNICATION_DPE_PANEL_TITLE_IDENTIFIER
 * @param dsException	(dyn_string)	IN/OUT : exception handler
 * @return value of type 'dyn_string': list of dollar parameter used to identify the panels title
 */
public dyn_string fwSynopticLocator_getConfiguration_titleIdentifier(dyn_string &dsException)
{
	return _fwSynopticLocator_getConfiguration_generic( __FUNCTION__, COMMUNICATION_DPE_PANEL_TITLE_IDENTIFIER, dsException);
}


/** 
 * Setter function for COMMUNICATION_DPE_PANEL_TITLE_IDENTIFIER
 * @param dsTitleIdentifiers	(dyn_string)	IN : list of title identifier to be set
 * @param dsException	(dyn_string)	IN/OUT : exception handler
 * @return value of type 'void'
 */
public void fwSynopticLocator_setConfiguration_titleIdentifier(dyn_string dsTitleIdentifiers, dyn_string &dsException)
{
	_fwSynopticLocator_setConfiguration_generic(__FUNCTION__, COMMUNICATION_DPE_PANEL_TITLE_IDENTIFIER, dsTitleIdentifiers, dsException);
}

/** 
 * Generic getter function for the fwSynopticLocator settings
 * @param sOriginalFunction (string) IN: name of the function calling this function
 * @param sDpe	(string)	IN : name of the DPE holding the settings to be retrieved
 * @param dsException	(dyn_string)	IN/OUT : exception container
 * @return value of type 'dyn_anytype', value of the settings. Empty if any error occurs
 */
private dyn_anytype _fwSynopticLocator_getConfiguration_generic( string sOriginalFunction, string sDpe, dyn_string &dsException)
{
	dyn_anytype daItems;
	
	if ( dpExists(sDpe) == false)
	{
		fwException_raise(dsException, "ERROR", "["+sOriginalFunction +"] DP "+sDpe+" does not exists", sOriginalFunction);
		return daItems;
	}
	
	int iErr = dpGet( sDpe, daItems);
	
	dyn_errClass decErr = getLastError();
	
	if ( iErr != 0 || dynlen(decErr) > 0)
	{
		fwException_raise(dsException, "ERROR", "["+sOriginalFunction +"] Error while dpGet on DPE "+sDpe, sOriginalFunction);
		return makeDynAnytype();
	}
	
	return daItems;
}

/** 
 * Generic getter function for the fwSynopticLocator settings
 * @param sOriginalFunction (string) IN: name of the function calling this function
 * @param sDpe	(string)	IN : name of the DPE holding the settings to be retrieved
 * @param dsException	(dyn_string)	IN/OUT : exception container
 * @return value of type 'dyn_anytype', value of the settings. Empty if any error occurs
 */
private void _fwSynopticLocator_setConfiguration_generic( string sOriginalFunction, string sDpe, dyn_anytype daItems, dyn_string &dsException)
{
	if ( dpExists(sDpe) == false)
	{
		fwException_raise(dsException, "ERROR", "["+sOriginalFunction +"] DP "+sDpe+" does not exists", sOriginalFunction);
		return ;
	}
	
	int iErr = dpSetWait( sDpe, daItems);
	
	dyn_errClass decErr = getLastError();
	
	if ( iErr != 0 || dynlen(decErr) > 0)
	{
		fwException_raise(dsException, "ERROR", "["+sOriginalFunction +"] Error while dpSetWait on DPE "+sDpe, sOriginalFunction);
		return ;
	}
	
	return ;
} 
