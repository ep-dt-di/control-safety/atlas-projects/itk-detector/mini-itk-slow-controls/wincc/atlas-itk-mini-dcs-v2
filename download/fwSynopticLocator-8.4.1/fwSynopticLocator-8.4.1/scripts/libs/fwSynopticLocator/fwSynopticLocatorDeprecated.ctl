/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */

  /**
   @deprecated 2018-06-26
   
   */
  dyn_string fwSynopticLocator_Data_Device_GetAllExistent() {

    FWDEPRECATED();
    
    dyn_string deviceNames;

    dyn_string devices = fwSynopticLocator_Data_Device_GetAll();  
    int nDevices = dynlen(devices);
    for(int j=1; j<=nDevices;j++)
    {
      //special instruction to search the label _tagalias_ due we need the pipe, |
      string sDeviceAlias = devices[j];
      strreplace( sDeviceAlias, "_tagalias_", "|");
      if(dpExists(dpAliasToName(sDeviceAlias)) == true)
      {
        //But to append again, we want the _tagalias_ structure.
        dynAppend(deviceNames, devices[j]);
      }
    }
    
    return deviceNames;
  
  } 


  /**
    
   Returns the number of devices represented in the system.
   
   @pre --
   @post --
   @return Int: Number of devices represented in the system.
   @deprecated 2018-06-26
   
   */
  
  int fwSynopticLocator_Data_Device_NumberOf() {

    FWDEPRECATED();
    return dynlen(dpNames(DEVICES_DP_PREFIX + "*", DEVICES_DPT_NAME));  
    
  }


/**@file

  @name LIBRARY: fwSynopticLocatorDeprecated.ctl

  @author Dr. Marc Bengulescu (BE-ICS-FD)
 */

/**
 *
 *  Device representation existance checker.
 *
 *  @param deviceName The name of the device.
 *  @pre --
 *  @post --
 *  @return Bool: True (Device <i>deviceName</i> does exist) or False (Device <i>deviceName</i> does not exist).
 *
 *  @deprecated 2018-06-26
 */

public bool fwSynopticLocator_deviceExists(string deviceName) {
    FWDEPRECATED();

    return fwSynopticLocator_Data_Device_Exists  (deviceName);

}


/**
 *
 *  Check whether the device with name <i>deviceName</i> is in the string <i>text</i>.
 *
 *  @param deviceName Is the name of the looked-for device.
 *  @param text Is the text (usually, a texted panel file) where we want to search for the device <i>deviceName</i>.
 *  @pre --
 *  @post --
 *  @return Bool: true (exists) or false (does not).
 *
 *  @deprecated 2018-06-26
 */    

public bool fwSynopticLocator_File_ExistsDevice(string deviceName, string text) {  
    FWDEPRECATED();

    return strpos(text, deviceName, 0) >= 0;

}

/**
 *  @deprecated 2018-06-26
 */
public int fwSynopticLocator_getDevicesExistentMapping(mapping & mainMapping) {
    FWDEPRECATED();

    //dyn_string devices = fwSynopticLocator_Data_Device_GetAll();  
    //dyn_string devices = fwSynopticLocator_Data_Device_GetAllNonExistent();  
    dyn_string devices = fwSynopticLocator_Data_Device_GetAllExistent();  
    //DebugTN("fwSynopticLocator_getDevicesExistentMapping: Number of Existent Devices " + dynlen(devices));

    int nDevices = dynlen(devices);
    int replyCode;

    for (int i = 1; i <= nDevices; i++) {

        replyCode = replyCode || fwSynopticLocator_Data_Device_GetPanels(devices[i], mainMapping[devices[i]]);

    }

    return replyCode;

}

/**
 *
 *  Gets a mapping that represents the exact device->panel representation of every device in the system:
 *
 *  MAPPING: ( 
 *  
 *  DYN_STRING DEVICE_1_NAME:  ( //PANELS// )
 *  DYN_STRING DEVICE_2_NAME:  ( //PANELS// )
 *  
 *  [..]
 *  
 *  DYN_STRING DEVICE_N_NAME:  ( //PANELS// )
 *  
 *  );
 *  
 *  @param mainMapping It will be filled with the mapping of the "device->panel" representation of the system.
 *  @pre --
 *  @post --
 *  @return Int: 0 (OK) or !0 (Something went wrong).
 *  
 *  @deprecated 2018-06-26
 *
 */ 
public int fwSynopticLocator_getDevicesMapping(mapping & mainMapping) {
    FWDEPRECATED();

    dyn_string devices = fwSynopticLocator_Data_Device_GetAll(); 
    //DebugTN("fwSynopticLocator_getDevicesMapping: Number of Devices " + dynlen(devices));

    int nDevices = dynlen(devices);
    int replyCode;

    for (int i = 1; i <= nDevices; i++) {
        replyCode = replyCode || fwSynopticLocator_Data_Device_GetPanels(devices[i], mainMapping[devices[i]]);  
    }

    return replyCode;

}


/**
 *  @deprecated 2018-06-26
 */
public int fwSynopticLocator_getDevicesNonExistentMapping(mapping & mainMapping) {
    FWDEPRECATED();

    //dyn_string devices = fwSynopticLocator_Data_Device_GetAll();  
    dyn_string devices = fwSynopticLocator_Data_Device_GetAllNonExistent();  
    //DebugTN("fwSynopticLocator_getDevicesNonExistentMapping: Number of Nonexistent Devices " + dynlen(devices));

    int nDevices = dynlen(devices);
    int replyCode;

    for (int i = 1; i <= nDevices; i++) {

        replyCode = replyCode || fwSynopticLocator_Data_Device_GetPanels(devices[i], mainMapping[devices[i]]);

    }

    return replyCode;

}


/**
 *
 *  Returns the number of devices represented in the system.
 *
 *  @pre --
 *  @post --
 *  @return Int: The number of devices represented in the system.
 *
 *  @deprecated 2018-06-26
 */    

public int fwSynopticLocator_getNumberOfDevices() {
    FWDEPRECATED();

    return fwSynopticLocator_Data_Device_NumberOf();

}


/**
 *  @deprecated 2018-06-26
 */
public int fwSynopticLocator_getPanelMappingDevicesNonExistent(string panel, mapping & subMapping) {
    FWDEPRECATED();

    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(panel)) {

        DebugTN("fwSynopticLocator_getPanelMapping: Precondition violation (The representation of the panel with name " + panel + " should exist, but it does not.");

    }

    /*  Precondition Checking End  */  

    int replyCode;

    replyCode = replyCode || fwSynopticLocator_Data_Panel_GetTitle(panel, subMapping["Title"]);

    replyCode = replyCode || fwSynopticLocator_Data_Panel_GetPath(panel, subMapping["Path"]);

    //TODO: In testing...
    replyCode = replyCode || fwSynopticLocator_Data_Panel_GetDevicesNonExistent(panel, subMapping["Devices"]);

    return replyCode;

}

/**
 *  
 *  Gets a mapping that represents the exact panel->device representation of every panel in the system:
 *  
 *  MAPPING: ( 
 *  
 *  MAPPING PANEL_1_NAME: (
 *  
 *  DYN_STRING Devices
 *  STRING Title 
 *  STRING Path
 *  )
 *  
 *  MAPPING PANEL_2_NAME: (
 *  
 *  DYN_STRING Devices
 *  STRING Title 
 *  STRING Path
 *  )
 *  
 *  [...]
 *  
 *  MAPPING PANEL_N_NAME: (
 *  
 *  DYN_STRING Devices
 *  STRING Title 
 *  STRING Path
 *  )
 *  
 *  );
 *  
 *  @param mainMapping It will be filled with the mapping of the "panel->device" representation of the system.
 *  @pre --
 *  @post --
 *  @return Int: 0 (OK) or !0 (Something went wrong).
 *  
 *  @deprecated 2018-06-26
 *  
 */  
public int fwSynopticLocator_getPanelsMapping(mapping & mainMapping) {
    FWDEPRECATED();

    dyn_string panels = fwSynopticLocator_Data_Panel_GetAll();  

    int nPanels = dynlen(panels);
    int errorCode = 0;

    for (int i = 1; i <= nPanels; i++) {

        mapping subMapping;
        errorCode = errorCode || fwSynopticLocator_Data_Panel_GetTitle(panels[i], subMapping["Title"]);
        errorCode = errorCode || fwSynopticLocator_Data_Panel_GetPath(panels[i], subMapping["Path"]);
        errorCode = errorCode || fwSynopticLocator_Data_Panel_GetDevices(panels[i], subMapping["Devices"]);

        mainMapping[panels[i]] = subMapping;

    }

    return errorCode;

}


/**
 *  @deprecated 2018-06-26
 */
public int fwSynopticLocator_getPanelsNonExistentMapping(mapping & mainMapping) {
    FWDEPRECATED();
    //dyn_string panels = fwSynopticLocator_Data_Panel_GetAll();  
    dyn_string panels = fwSynopticLocator_Data_Panel_GetAllNonExistent();  

    int nPanels = dynlen(panels);
    int errorCode = 0;

    for (int i = 1; i <= nPanels; i++) {

        mapping subMapping;
        errorCode = errorCode || fwSynopticLocator_Data_Panel_GetTitle(panels[i], subMapping["Title"]);
        errorCode = errorCode || fwSynopticLocator_Data_Panel_GetPath(panels[i], subMapping["Path"]);
        errorCode = errorCode || fwSynopticLocator_Data_Panel_GetDevices(panels[i], subMapping["Devices"]);

        mainMapping[panels[i]] = subMapping;

    }

    return errorCode;
}

