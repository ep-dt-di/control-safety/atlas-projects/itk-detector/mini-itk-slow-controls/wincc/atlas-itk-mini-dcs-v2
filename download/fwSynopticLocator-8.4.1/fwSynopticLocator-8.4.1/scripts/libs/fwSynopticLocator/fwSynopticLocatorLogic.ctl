/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwSynopticLocatorLogic.ctl

*/

#uses "fwSynopticLocator/fwSynopticLocatorDeprecated.ctl"
#uses "fwSynopticLocator/fwSynopticLocatorData.ctl"
#uses "fwSynopticLocator/fwSynopticLocatorFile.ctl"

  /**
    
   Adds to the system the representation of every panel and every device contained in <i>paths</i>.
   The panel "knows" its name, title, date, contained devices (full representation).
   Every device "knows" its name and in which panels it is contained (full representation).
   If a panel contains no devices, the panel is not registered.
   
   @param paths Contains full paths of panel folder structures / pnls files.
   @param deviceIdentifiers It contains the possible identifiers of the devices.
   @param titleIdentifiers It contains the possible identifiers of the panel title.
   @pre --
   @post The system contains a the full representation of the panels and its devices contained in <i>path</i> and its subfolders.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */ 

  public int fwSynopticLocator_populateSynopticLocator(const dyn_string & paths, const dyn_string & deviceIdentifiers, const dyn_string & titleIdentifiers, string progressFunction) {
    
    int replyCode = 0;

    //Get the panels looking for in the paths.
    dyn_string panelsPath = fwSynopticLocator_getPanelsFromPaths(paths);

    fwSynopticLocator_removeRepetitions(panelsPath);
    
    int nPanels = dynlen(panelsPath);
    float progressValue;
    int addReply;
    for (int panelIndex = 1; panelIndex <= nPanels; panelIndex++) {
      
      addReply = fwSynopticLocator_addNewPanelWithDevices(panelsPath[panelIndex], deviceIdentifiers, titleIdentifiers);
      
      if (addReply != 1) {
        
        replyCode = replyCode || addReply;
        
      }
      
      progressValue = panelIndex*100/nPanels;
      execScript(progressFunction, makeDynString(), progressValue);
      
    }
    
    return replyCode;
  
  }
  
  /**
    
   Removes the repetitions in the parameter <i>panels</i>.
   
   @param panels It is an array of panels.
   @pre --
   @post The parameter <i>panels</i> has no string repetitions.
   @return Int: The number of removed items in <i>panels</i>.
   
   */   
  
  public int fwSynopticLocator_removeRepetitions(dyn_string & panels) {
    
    int nPanels = dynlen(panels);
    mapping noRepeatMapping;
    
    // Maybe one panel is added twice (via file and via folder that contains it, f.e), so check it.
    // One of the easiest and fastests ways of removing repeated content is using a mapping / sorting. In this case, 1st solution.
    
    for (int i = 1; i <= nPanels; i++) { 
      
      noRepeatMapping[panels[i]] = "";
      
    }  

    dynClear(panels);
    
    int nPanelsAfter = mappinglen(noRepeatMapping);
    
    for (int i = 1; i <= nPanelsAfter; i++) { 
      
      dynAppend(panels, mappingGetKey(noRepeatMapping, i));
      
    }   
    
    return nPanels - nPanelsAfter;
   
  }    
      
  
  /**
    
   Updates the Synoptic Locator entries. 
   Much more efficient than re-populating it, if there are not a lot of changes.
   
   @param paths Contains full paths of panel folder structures / pnls files
   @param deviceIdentifiers It contains the possible identifiers of the devices (at least one).
   @param titleIdentifiers It contains the possible identifiers of the panel title (at least one).
   @pre --
   @post The system contains a the full representation of the panels and its devices contained in the folder <i>path</i> and its subfolders.
   @return Dyn_int: [1] -> Deleted panels // [2] -> Updated full // [3] -> Added full // [4] -> Errors (Error: 0 -> OK, !0 -> Error) 
   
   */ 
  
  public dyn_int fwSynopticLocator_updateSynopticLocator(const dyn_string & paths, const dyn_string & deviceIdentifiers, const dyn_string & titleIdentifiers, string progressFunction) {
    
    dyn_string oldPanels;
    
    int nPaths = dynlen(paths);
    for (int i = 1; i <= nPaths; i++) {
      
      dyn_string registeredPanels;

      //We obtain the ABSOLUTE path of the registered panels
      fwSynopticLocator_getRegisteredPanels(paths[i], registeredPanels);

      dynAppend(oldPanels, registeredPanels);
      
    } 
   
    //Get the panels looking again in the paths
    dyn_string newPanels = fwSynopticLocator_getPanelsFromPaths(paths);
    
    fwSynopticLocator_removeRepetitions(newPanels);
    
    return fwSynopticLocator_updatePanels(oldPanels,newPanels,deviceIdentifiers, titleIdentifiers, progressFunction);;
    
  }



  private dyn_string fwSynopticLocator_getPanelsFromPaths(const dyn_string paths)
  {
    string path;
    dyn_string panels;

    int nPaths = dynlen(paths);

    for (int pathIndex = 1; pathIndex <= nPaths; pathIndex++) {     
      
      path = paths[pathIndex];  
  
      if (strpos(path, ".pnl") > 0 || strpos(path, ".xml") > 0) {
        
        dynAppend(panels, path);

      } else {
        
        dynAppend(panels, fwSynopticLocator_File_GetPanels(path));
        
      }
    }  

    return panels;
  }


  
  
  /**
    
   Clears the Synoptic Locator entries. Removes every entry. 

   @pre --
   @post The synoptic locator data is empty, so it does not contains any system representation.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */ 
  
  public int fwSynopticLocator_clearSynopticLocator(string progressFunction) {
    
    int replyCode = fwSynopticLocator_Data_Panel_DeleteAll(progressFunction);

    replyCode = replyCode || fwSynopticLocator_Data_Device_DeleteAll(progressFunction);
    
    return replyCode;
    
  }
  
  /**
    
   Adds to the system the representation of the panel with path <i>panelPath</i> and every device contained in it.
   The panel "knows" its name, title, date, contained devices (full representation).
   Every device "knows" its name and in which panels it is contained (full representation).
   
   @param panelPath Is the full path of the panel.
   @param deviceIdentifiers It contains the possible identifiers of the devices).
   @param titleIdentifiers It contains the possible identifiers of the panel title.
   @pre The panel represented with <i>panelPath</i> is not represented in the system. 
   @post The system contains the full representation of the panel and its devices.
   @return Int: 0 (OK), -1 (Something went wrong/already exist), 1 (Not added because it contained no devices).
   
   */ 

  private int fwSynopticLocator_addNewPanelWithDevices(string panelPath, const dyn_string & deviceIdentifiers, const dyn_string & titleIdentifiers) {   

    /* Precondition Checking Start */

    if (fwSynopticLocator_Data_Panel_Exists(fwSynopticLocator_File_GetName(panelPath))) {
      
      DebugTN("fwSynopticLocator_addNewPanelWithDevices: Precondition violation (The representation of the panel with path " + panelPath + " should not exist, but it does.");
      
    }
    
    /*  Precondition Checking End  */
    dyn_string devices = fwSynopticLocator_File_GetDeviceNamesFromPanel(panelPath, deviceIdentifiers);

    //Return devices with standard name
    devices = fwSynopticLocator_File_devicesToStandardName(devices);   //Afects to convert the names to create DPE and compare with real devices.

    //Remove device repetitions 
    fwSynopticLocator_removeRepetitions(devices);

    int replyCode = 0;
    
    int nDevices = dynlen(devices);
    
    
    if (nDevices == 0) {
      
      return 1;
      
    }

    //Before
    // replyCode = fwSynopticLocator_addNewPanel(panelPath, titleIdentifiers);
    // for (int j = 1; j <= nDevices; j++) {
    //    if (!fwSynopticLocator_Data_Device_Exists(devices[j])) {   //check devices     
    //      replyCode = replyCode || fwSynopticLocator_addNewDevice(devices[j]);   //create new devices  
    //    }     
    //    replyCode = replyCode || fwSynopticLocator_addPanelToDevice(devices[j], panelPath);
    // }  
    // replyCode = replyCode || fwSynopticLocator_addDevicesToPanel(panelPath, devices);

    
    replyCode = fwSynopticLocator_addNewPanel(panelPath, titleIdentifiers);

    replyCode = replyCode || fwSynopticLocator_addNewDevices(devices);   //create new devices

    replyCode = replyCode || fwSynopticLocator_addPanelToDevices(devices, panelPath);
      
    replyCode = replyCode || fwSynopticLocator_addDevicesToPanel(panelPath, devices);
      
    return replyCode;
         
  }

  /**
    
   Creates (and adds) the representation of a new Device in the system with an empty panel relationship.
   
   @param deviceName Is the name of the device that will be represented in the system.
   @pre The system does not contain the device represented by <i>deviceName</i>.
   @post The system contains a device representation called <i>deviceName</i> with an empty panel relationship.
   @return Int: 0 (OK) or !0 (Something went wrong/already exist).
   
   */  
  
  private int fwSynopticLocator_addNewDevice(string deviceName) {

    /* Precondition Checking Start */

    if (fwSynopticLocator_Data_Device_Exists(deviceName)) {
      
      DebugTN("fwSynopticLocator_addNewDevice: Precondition violation (The representation of the device named " + deviceName + " should not exist, but it does.");
      
    }
    
    /*  Precondition Checking End  */    
    
    dyn_string panels;
    return fwSynopticLocator_Data_Device_Create(deviceName, panels); 
  
  }

  private int fwSynopticLocator_addNewDevices(dyn_string deviceName) {

    /* Precondition Checking Start */

    dyn_string deviceNonExistent;
    int nDevices = dynlen(deviceName);
    for(int i=1; i<= nDevices; i++)
    {
      //it's normal if some devices already exists (some panels have the same devices)
      if (!fwSynopticLocator_Data_Device_Exists(deviceName[i]))
      {
        dynAppend(deviceNonExistent, deviceName[i]);
      }
    }
    
    /*  Precondition Checking End  */    
    
    return fwSynopticLocator_Data_Devices_Create(deviceNonExistent); 
  
  }
  
  /**
    
   Lets the device <i>deviceName</i> "know" that it is represented in the panel with path <i>panelPath</i>.
   
   @param deviceName Is the name of the device wants to be informed.
   @param panelPath Is the full path of the panel in which the device <i>deviceName</i> is.
   @pre The device <i>deviceName</i> and the panel which path is <i>panelPath</i> are already represented in the system.
   @post The device <i>deviceName</i> knows that it is contained in the panel with path <i>panelPath</i>.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */  
  
  private int fwSynopticLocator_addPanelToDevice(string deviceName, string panelPath) {
   
    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(fwSynopticLocator_File_GetName(panelPath))) {
      
      DebugTN("fwSynopticLocator_addPanelToDevice: Precondition violation (The representation of the panel with path " + panelPath + " should exist, but it does not.");
      
    }
    
    if (!fwSynopticLocator_Data_Device_Exists(deviceName)) {
      
      DebugTN("fwSynopticLocator_addPanelToDevice: Precondition violation (The representation of the device named " + deviceName + " should exist, but it does not.");
      
    }
    
    /*  Precondition Checking End  */
    
    string panelName = fwSynopticLocator_File_GetName(panelPath);    

    dyn_string existingPanels;
    int replyCode = fwSynopticLocator_Data_Device_GetPanels(deviceName, existingPanels);
    
    dynAppend(existingPanels, panelName);  // Add new panel  
    
    return replyCode || fwSynopticLocator_Data_Device_SetPanels(deviceName, existingPanels);    
  
  }



  private int fwSynopticLocator_addPanelToDevices(dyn_string deviceName, string panelPath) {
   
    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(fwSynopticLocator_File_GetName(panelPath))) {
      
      DebugTN("fwSynopticLocator_addPanelToDevice: Precondition violation (The representation of the panel with path " + panelPath + " should exist, but it does not.");
      
    }

    int nDevices = dynlen(deviceName);
    for(int i=1; i<=nDevices; i++)
    {
      if (!fwSynopticLocator_Data_Device_Exists(deviceName[i]))
      {
        DebugTN("fwSynopticLocator_addPanelToDevice: Precondition violation (The representation of the device named " + deviceName[i] + " should exist, but it does not.");
      }
    }

    /*  Precondition Checking End  */
    
    string panelName = fwSynopticLocator_File_GetName(panelPath);    

    dyn_dyn_string existingPanels;
    int replyCode = fwSynopticLocator_Data_Devices_GetPanels(deviceName, existingPanels);

    nDevices = dynlen(deviceName);  //in the case deviceName has changed
    for(int i=1; i<=nDevices; i++)
    {
      dynAppend(existingPanels[i], panelName);  // Add new panel  
    }

    return replyCode || fwSynopticLocator_Data_Devices_SetPanels(deviceName, existingPanels);    
  
  }
  
  /**
    
   Creates (and adds) the representation of a new Panel in the system.
   Every full panel representation is made of:
   The panel identificator (name) will be the name of the .pnl/.xml file.
   The panel full path.
   The panel title will be its title, identified by one of the elements inside <i>titleIdentifiers</i>.
   The panel date will be the .pnl/.xml file last modification date.
   
   @param panelPath Is the path of the panel that will be represented in the system.
   @param titleIdentifiers It is a dyn_string with possible title identifiers.
   @pre The system does not contain the panel represented by <i>panelPath</i>.
   @post The system contains a full panel representation of the panel with path <i>panelPath</i>.
   @return Int: 0 (OK) or !0 (Something went wrong/alredy exist).
   
   */ 
  
  private int fwSynopticLocator_addNewPanel(string panelPath, const dyn_string & titleIdentifiers) {
  
    /* Precondition Checking Start */

    if (fwSynopticLocator_Data_Panel_Exists(fwSynopticLocator_File_GetName(panelPath))) {
      
      DebugTN("fwSynopticLocator_addNewPanel: Precondition violation (The representation of the panel with path " + panelPath + " should not exist, but it does.");

    }
    
    /*  Precondition Checking End  */    
    
    string panelName = fwSynopticLocator_File_GetName(panelPath);
    
    string panelTitle = fwSynopticLocator_File_GetPanelTitle(panelPath, titleIdentifiers);
    
    dyn_string devices;    
    
    return fwSynopticLocator_Data_Panel_Create(panelName, panelTitle, panelPath, fwSynopticLocator_File_GetDate(panelPath), devices);
    
  }
  

  /**
    
   Add the devices names <i>devicesName</i> in the list of contained devices in the panel with path <i>panelPath</i>.
   
   @param deviceNames It contains the names of the devices that will be included in the panel <i>panelName</i>.
   @param panelPath Is the full path of the panel in which the devices <i>deviceNames</i> are.
   @pre The devices <i>deviceNames</i> and the panel with path <i>panelPath</i> are already represented in the system.
   @post The panel representation with path <i>panelPath</i> contains the devices that it previously had, plus the devices <i>deviceNames</i>.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */  
  
  private int fwSynopticLocator_addDevicesToPanel(string panelPath, const dyn_string & deviceNames) {
 
    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(fwSynopticLocator_File_GetName(panelPath))) {
      
      DebugTN("fwSynopticLocator_addDevicesToPanel: Precondition violation (The representation of the panel with path " + panelPath + " not exist, but it does not.");

    }
    
    int nDeviceNames = dynlen(deviceNames);
    
    for (int i = 1; i <= nDeviceNames; i++) {
      
      if (!fwSynopticLocator_Data_Device_Exists(deviceNames[i])) {
        
        DebugTN("fwSynopticLocator_addDevicesToPanel: Precondition violation (The representation of the devices with name " + deviceNames[i] + " should exist, but it does not.");
        
      }
      
    }
    
    /*  Precondition Checking End  */       
    
    string panelName = fwSynopticLocator_File_GetName(panelPath);    
    
    dyn_string existingDevices;
   
    int errorCode = fwSynopticLocator_Data_Panel_GetDevices(panelName, existingDevices);
    
    dynAppend(existingDevices, deviceNames);   
    
    return errorCode || fwSynopticLocator_Data_Panel_SetDevices(panelName, existingDevices);    
  
  }
  
  /**
    
   Returns the number of panels represented in the system.
   
   @pre --
   @post --
   @return Int: The number of panels represented in the system.
   
   */    
  
  public int fwSynopticLocator_getNumberOfPanels() {
    
    return fwSynopticLocator_Data_Panel_NumberOf();
    
  }
  
    /**
    
   Gets a mapping that represents the exact panel->device representation of the <i>panel</i> panel in the system:
   
  	MAPPING PANEL_1_NAME: (

  	  DYN_STRING Devices
  	  STRING Title 
  	  STRING Path
      
  	);
      
   
   @param panel The name of the panel.
   @param subMapping It will contain the mapping.
   @pre The panel <i>panel</i> is represented in the system
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */    
  
  public int fwSynopticLocator_getPanelMapping(string panel, mapping & subMapping) {
    
    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(panel)) {
      
      DebugTN("fwSynopticLocator_getPanelMapping: Precondition violation (The representation of the panel with name " + panel + " should exist, but it does not.");

    }
    
    /*  Precondition Checking End  */  
    
    int replyCode;
    
    replyCode = replyCode || fwSynopticLocator_Data_Panel_GetTitle(panel, subMapping["Title"]);
    
    replyCode = replyCode || fwSynopticLocator_Data_Panel_GetPath(panel, subMapping["Path"]);
    
    replyCode = replyCode || fwSynopticLocator_Data_Panel_GetDevices(panel, subMapping["Devices"]);

    return replyCode;
     
  }



  /**
    
   Returns a mapping that represents the device->panel representation of the <i>device</i> device in the system:
   
	 MAPPING: ( 

		 DYN_STRING DEVICE_1_NAME:  ( //PANELS// )

	 );

   @param device The name of the device.
   @param mainMapping It will contain the mapping.
   @pre The device <i>device</i> is represented in the system
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */    
  
  public int fwSynopticLocator_getDeviceMapping(string device, mapping & mainMapping) {

    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Device_Exists(device)) {
      
      DebugTN("fwSynopticLocator_getDeviceMapping: Precondition violation (The representation of the device with name " + device + " should exist, but it does not.");

    }
    
    /*  Precondition Checking End  */     
    
    return fwSynopticLocator_Data_Device_GetPanels(device, mainMapping[device]);;
     
  }
  
  /**
    
   Gets the registered panel paths of the already represented panels in the Synoptic Locator System which are in the path <i>path</i>. 

   @param path Is the full path of folder where to look for the panel representations path.
   @param panelPaths It will contain the panel paths.
   @pre --
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */ 
  
  private int fwSynopticLocator_getRegisteredPanels(string path, dyn_string & panelPaths) {
    
    dyn_string panelNames = fwSynopticLocator_Data_Panel_GetAll();
    
    int nPanelNames = dynlen(panelNames);
    int replyCode;
    string path;
    for (int i = 1; i <= nPanelNames; i++) {
      
      replyCode = replyCode || fwSynopticLocator_Data_Panel_GetPath(panelNames[i], path);

      //We want the absolute path of the panel
      path = getPath(PANELS_REL_PATH) + path;
      
      dynAppend(panelPaths, path);     
      
    }
    
    return replyCode;
    
  } 
  
  
  /**
    
   Updates the Synoptic Locator entries. 
   
   @param oldPanels Is the list of paths of the already registered panels.
   @param newPanels Is the list of the paths of the new panels (to update to).
   @param deviceIdentifiers It contains the possible identifiers of the devices.
   @param titleIdentifiers It contains the possible identifiers of the panel title.
   @pre --
   @post The system contains the full representation of the panels <i>newPanels</i>.
   @return Dyn_int: [1] -> Deleted panels // [2] -> Updated panels // [3] -> Added panels // [4] -> Errors (Error: 0 -> OK, !0 -> Error) 
   
   */ 
  private dyn_int fwSynopticLocator_updatePanels(const dyn_string & oldPanels, const dyn_string & newPanels, const dyn_string & deviceIdentifiers, const dyn_string & titleIdentifiers, string progressFunction) {
  
    /* Algorithm explanation
       
    // Comparison strategy: -1 Old, -1 +1 = (0) Stay, +1 New.
    
    // Actual System representation    // Actual System (Folder structure) (To update to)
    
    Panel1 -1
    Panel2 -1
    Panel3 -1                          Panel3 1
    Panel4 -1                          Panel4 1
                                       Panel5 1
                                                                      
    // SUM
    
    Panel1 -1
    Panel2 -1
    Panel3 0                          
    Panel4 0                          
    Panel5 1
    
    // SUM
    
    Panel1 Delete
    Panel2 Delete
    Panel3 Stay                     
    Panel4 Stay                         
    Panel5 Add
        
    */

    // DebugTN(oldPanels);    //"vision/EL_Synoptics/PVSS_Panels/ME9/48V_test.pnl"  (now, the absolute path)
    // DebugTN(newPanels);    //"/home/enice/PvssProjects/PSEN/psen/panels/vision/EL_Synoptics/PVSS_Panels/LHC4/LHC4_SE4_48V.pnl"
    // return makeDynInt(0,0,0,0);

    const int old = -1;
    const int stay = 0;
    const int add = 1;
    
    dyn_int returnInfo; //  [1] -> Deleted panels // [2] -> Updated panels // [3] -> Added panels // [4] -> Errors (Error: 0 -> OK, !0 -> Error)    
    
    mapping mainMapping;
    
    int nOldPanels = dynlen(oldPanels);
    for (int i = 1; i <= nOldPanels; i++) {
      
       mainMapping[oldPanels[i]] = old;
      
    }
    
    int nNewPanels = dynlen(newPanels);
    for (int i = 1; i <= nNewPanels; i++)
    {
      //if already exists (the oldpath and the newpath are the same), the panel has the old value (-1) and add (1) to "update" ("stay" state)
      //if doesn't exists, only add (1) to "add" state
      mainMapping[newPanels[i]] = mainMapping[newPanels[i]] + 1;
    }


    int value, deleted, updated, added;
    deleted = updated = added = 0;
    string panel, panelName;
    time oldDate, newDate;
    
    dyn_int returnInfo;    
    returnInfo[4] = 0;
    
    int nMapping = mappinglen(mainMapping);
    float progressValue;
    for (int i = 1; i <= nMapping; i++) {

      progressValue = i*100/nMapping;
      execScript(progressFunction, makeDynString(), progressValue);
      
      panel = mappingGetKey(mainMapping,i);
      value = mappingGetValue(mainMapping, i);
      int addReply;
      
      switch(value) {
        
        case stay:
        
          panelName = fwSynopticLocator_File_GetName(panel);
          returnInfo[4] = returnInfo[4] || fwSynopticLocator_Data_Panel_GetDate(panelName, oldDate);
          newDate = fwSynopticLocator_File_GetDate(panel);
       
          if (oldDate != newDate) {
          
            returnInfo[4] = returnInfo[4] || fwSynopticLocator_updatePanelWithDevices(panel,deviceIdentifiers,titleIdentifiers)[3]; //"Deleted devices","Added devices","Errors"
            ++returnInfo[2]; 
            returnInfo[4] = returnInfo[4] || fwSynopticLocator_Data_Panel_SetDate(panelName, newDate);
        
          }
          
          break;
          
        case old:
          returnInfo[4] = returnInfo[4] || fwSynopticLocator_deletePanelWithDevices(panel); 
          ++returnInfo[1]; 
          
          break;
          
        case add:
          
          addReply = fwSynopticLocator_addNewPanelWithDevices(panel,deviceIdentifiers,titleIdentifiers); 
          
          if (addReply == 0) {
            
            ++returnInfo[3]; 
            
          } else if (addReply == -1) {
            
            returnInfo[4] = returnInfo[4] || addReply;
            
          }
          
          break;
          
        default:
          break;
       
      }
      
    }
    
    return returnInfo;
    
  } 
  
  /**
    
   Deletes the system the representation of the panel with path <i>panelPath</i> and every device relationship with this panel.
   If a device is contained only in this panel, the device is deleted.
   
   @param panelPath Is the full path of the panel.
   @pre The panel represented with <i>panelPath</i> is represented in the system. 
   @post The system does not contain a representation of the panel with path <i>panelPath</i> nor the relationship between devices and this panel.
   @return Int: 0 (OK) or !0 (Something went wrong/already exist).
   
   */ 
  
  private int fwSynopticLocator_deletePanelWithDevices(string panelPath) { 

    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(fwSynopticLocator_File_GetName(panelPath))) {
      
      DebugTN("fwSynopticLocator_deletePanelWithDevices: Precondition violation (The representation of the panel with path " + panelPath + " should exist, but it does not.");

    }
    
    /*  Precondition Checking End  */
    
    string panelName = fwSynopticLocator_File_GetName(panelPath);
    dyn_string devices;
   
    int replyCode = fwSynopticLocator_Data_Panel_GetDevices(panelName, devices);
    
    int nDevices = dynlen(devices);
    for (int i = 1; i <= nDevices; i++) {

      replyCode = replyCode || fwSynopticLocator_removePanelFromDevice(panelName, devices[i]);
     
    }
   
    replyCode = replyCode || fwSynopticLocator_Data_Panel_Delete(panelName); 
    
    return replyCode;
    
  }
  
  /**
    
   Deletes the "panel reference" that a device has (representing that the device is contained in the panel).
   
   @param panelName Is the name of the panel.
   @param deviceName Is the name of the device.
   @pre The panel represented with <i>panelName</i> is represented in the system. The device named <i>deviceName</i> exists and has a reference to the panel <i>panelName</i>.
   @post The panel represented with <i>panelName</i> is represented in the system. The device named <i>deviceName</i> exists and does not have a reference to the panel <i>panelName</i>.
   @return Int: 0 (OK) or !0 (Something went wrong/already exist).
   
   */ 
  
  private int fwSynopticLocator_removePanelFromDevice(string panelName, string deviceName) {

    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(panelName)) {
      
      DebugTN("fwSynopticLocator_removePanelFromDevice: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");

    }
    
    if (!fwSynopticLocator_Data_Device_Exists(deviceName)) {
      
      DebugTN("fwSynopticLocator_removePanelFromDevice: Precondition violation (The representation of the device named " + deviceName + " should exist, but it does not.");

    }
    
    dyn_string panelNames;
    int replyCode = fwSynopticLocator_Data_Device_GetPanels(deviceName, panelNames);
    
    if (dynContains(panelNames, panelName) <= 0) {
      
      DebugTN("fwSynopticLocator_removePanelFromDevice: Precondition violation (The device named " + deviceName + " should have a reference to the panel " + panelName + ", but it does not.");
 
    }
    
    /*  Precondition Checking End  */    
    
    dyn_string devicePanels;
    
    replyCode = replyCode || fwSynopticLocator_Data_Device_GetPanels(deviceName, devicePanels);

    int j = dynContains(devicePanels, panelName);

    replyCode = replyCode || dynRemove(devicePanels,j);

    replyCode = replyCode || fwSynopticLocator_Data_Device_SetPanels(deviceName, devicePanels); 

    // if (dynlen(devicePanels) == 0) {
    //   fwSynopticLocator_Data_Device_Delete(deviceName);
    //   //delete later!
    // }   
    // else { 
    //   replyCode = replyCode || fwSynopticLocator_Data_Device_SetPanels(deviceName, devicePanels); 
    // }
      
    return replyCode;
    
  }  
  
  /**
    
   Deletes the "device reference" that a panel has (representing that the panel contains the device).
   
   @param panelName Is the name of the panel.
   @param deviceName Is the name of the device.
   @pre The panel represented with <i>panelName</i> is represented in the system and has a reference to the device <i>deviceName</i>. The device named <i>deviceName</i> exists.
   @post The panel represented with <i>panelName</i> is represented in the system and does not have a reference to the device <i>deviceName</i>. The device named <i>deviceName</i> exists.
   @return Int: 0 (OK) or !0 (Something went wrong/already exist).
   
   */ 
  
  private int fwSynopticLocator_removeDeviceFromPanel(string panelName, string deviceName) {

    // DebugTN("panelName: " + panelName); //SPS_BA1_18kV_PNL
    // DebugTN("deviceName: " + deviceName); //EMD212_slash_A1

    /* Precondition Checking Start */

    if (!fwSynopticLocator_Data_Panel_Exists(panelName)) {
      
      DebugTN("fwSynopticLocator_removeDeviceFromPanel: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");

    }
    
    if (!fwSynopticLocator_Data_Device_Exists(deviceName)) {
      
      DebugTN("fwSynopticLocator_removeDeviceFromPanel: Precondition violation (The representation of the device named " + deviceName + " should exist, but it does not.");

    }
    
    dyn_string deviceNames;
    
    int replyCode = fwSynopticLocator_Data_Panel_GetDevices(panelName, deviceNames);
    
    if (dynContains(deviceNames, deviceName) <= 0) {
      
      DebugTN("fwSynopticLocator_removeDeviceFromPanel: Precondition violation (The device named " + panelName + " should have a reference to the device " + deviceName + ", but it does not.");
 
    }
    
    /*  Precondition Checking End  */ 

    dyn_string devices;
    int replyCode = fwSynopticLocator_Data_Panel_GetDevices(panelName, devices);
    
    int j = dynContains(devices, deviceName); //for (j = 1; devices[j] != device; j++);

    replyCode = replyCode || dynRemove(devices,j);
    
    return replyCode || fwSynopticLocator_Data_Panel_SetDevices(panelName, devices);
    
  }



private int fwSynopticLocator_removeDeviceIfNoPanels(string deviceName) {

  dyn_string devicePanels;

  int replyCode = fwSynopticLocator_Data_Device_GetPanels(deviceName, devicePanels);

  if (dynlen(devicePanels) == 0) {
    
    fwSynopticLocator_Data_Device_Delete(deviceName);
    
  }
    
  return replyCode;
}

  
  /**
    
   Updates the system representation of the panel with path <i>panelPath</i> and every device contained in it.
   
   @param panelPath Is the full path of the panel.
   @param deviceIdentifiers It contains the posslible identifiers of the devices.
   @param titleIdentifiers It contains the possible identifiers of the panel title.
   @pre --
   @post The system contains a the full representation of the panel and its devices. The devices that are no longer contained in the panel <i>panelPath</i> loose their relationship with the panel, if they were only contained in that panel, they are deleted.
   @return Dyn_int: [1] -> Deleted devices // [2] -> Added devices // [3] -> Errors (Error: 0 -> OK, !0 -> Error)
   
   */ 
  
  private dyn_int fwSynopticLocator_updatePanelWithDevices(const dyn_string & panelPath, const dyn_string & deviceIdentifiers, const dyn_string & titleIdentifiers) {
  
    /*
    // Comparison strategy: -1 Old, -1 +1 = (0) Stay, +1 New.
    
    // Actual System representation    // Actual System (Folder structure) (To update to)
    
    Panel1 -1
    Panel2 -1
    Panel3 -1                          Panel3 1
    Panel4 -1                          Panel4 1
                                       Panel5 1
                                                                      
    // SUM
    
    Panel1 -1
    Panel2 -1
    Panel3 0                          
    Panel4 0                          
    Panel5 1
    
    // SUM
    
    Panel1 Delete
    Panel2 Delete
    Panel3 Stay                     
    Panel4 Stay                         
    Panel5 Add
        
    */

    const int old = -1;
    const int stay = 0;
    const int add = 1;
    
    string panel = fwSynopticLocator_File_GetName(panelPath);
    
    dyn_int returnInfo; //  [1] -> Deleted devices // [2] -> Added devices // [3] -> Errors (Error: 0 -> OK, !0 -> Error)
    
    dyn_string oldDevices;
    returnInfo[3] = fwSynopticLocator_Data_Panel_GetDevices(panel, oldDevices);

/*    string sPanelText = fwSynopticLocator_File_Read(panelPath);
    dyn_string newDevices = fwSynopticLocator_File_GetDevicesFromText(sPanelText, deviceIdentifiers);*/

    dyn_string newDevices = fwSynopticLocator_File_GetDeviceNamesFromPanel(panelPath, deviceIdentifiers);

    //Return devices with standard name
    newDevices = fwSynopticLocator_File_devicesToStandardName(newDevices);

    //Remove device repetitions 
    fwSynopticLocator_removeRepetitions(newDevices);
    
    
        
    mapping m; //Key: DeviceName / Value: -1 (old), 0 (same), 1 (new)
    
    int nOldDevices = dynlen(oldDevices);
    int nNewDevices = dynlen(newDevices);
    
    for (int i = 1; i <= nOldDevices; i++) {
      
      m[oldDevices[i]] = old;
      
    }
      
    for (int i = 1; i <= nNewDevices; i++) {
      
      m[newDevices[i]] = m[newDevices[i]] + 1;
      
    }

    //DebugTN(m);
    //return makeDynInt(0,0,0);

    
    string device;
    int value, added, deleted;
    value = added = deleted = 0;
    for (int i = 1; i <= mappinglen(m); ++i) {
      
      device = mappingGetKey(m,i);
      value = mappingGetValue(m, i);
      
      switch(value) {
        
        case old:
          //DebugTN("OLD. Panel: " + panel + ", device: " + device);
          returnInfo[3] = returnInfo[3] || fwSynopticLocator_removePanelFromDevice(panel, device);
          returnInfo[3] = returnInfo[3] || fwSynopticLocator_removeDeviceFromPanel(panel, device);  
          returnInfo[3] = returnInfo[3] || fwSynopticLocator_removeDeviceIfNoPanels(device);      
          ++returnInfo[1];
          break;
        
        case add:
          //DebugTN("ADD. Panel: " + panelPath + ", device: " + device);
          if (!fwSynopticLocator_Data_Device_Exists(device)) { 
                
            returnInfo[3] = returnInfo[3] || fwSynopticLocator_addNewDevice(device);
          
          } 
          
          returnInfo[3] = returnInfo[3] || fwSynopticLocator_addPanelToDevice(device, panelPath);
          dyn_string devices;
          dynAppend(devices,device);
          returnInfo[3] = returnInfo[3] || fwSynopticLocator_addDevicesToPanel(panelPath, devices);   
        
          ++returnInfo[2];

        
          break;
        
        default:
        
          break;
      
      }
      
    }
    
    return returnInfo;
    
  }


  
