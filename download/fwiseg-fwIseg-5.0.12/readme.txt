-- fwIseg component for Iseg OPC server and WinCC OA --


  fwIseg support Iseg ECH crates family and their modules (1, 8, 16, 32 (2x16) channels)

  authors:  Lionel Wallet, Mateusz Lechman 
  contact person: Mateusz Lechman
  CERN/PH-AID-DC, Alice experiment, Detector Control System

  General warning before using the Iseg Framework component
  Check the followings:
  1 - Can bus speed of the crate is the same with the Can bus speed of the modules
  2 - Connection to one port of the CAN bus PCI, or Usb device is implying you must make a daisy chain for the crate and modules, if you want to control both in the same time.
  3 - Can bus speed of the card itself is set to the same value as the crate and modules speed.


-- Release notes

------------------
| Versions 5.0.x |
------------------

Version 5.0.12 30/10/2019
	
	Migration: 
		If you do the upgrade from version lower than 5.0.10 then please follow the migration instruction described below for version 5.0.10

	Changes:
		- Channels: DelayedTripAction is now of type integer (OPCISEG-117), panel: fwIsegChannelOperation updated



Version 5.0.11 7/6/2019
	
	Migration: 
		If you do the upgrade from version lower than 5.0.10 then please follow the migration instruction described below for version 5.0.10

	Changes:
		- Unification of encoding and improvement of labels in panels, clearing the code of fwIsegTable.pnl


Version 5.0.10 2/6/2019
	
	Migration:
		Before the installation please stop and remove the following WinCC managers that were used so far by fwIseg: 
		- "OPC DA Client -num 8" and "Simulatation Driver -num 8" in case of upgrade from OPC DA
		- "OPC UA Client -num 10" and "Simulatation Driver -num 10" in case of upgrade from OPC UA (fwIseg v. 5.0.9 and its earlier versions for OPC UA)   
  
		At the end of installation please start the fwIseg/fwIsegMigration.pnl panel and follow the instruction to update existing ISEG devices to the latest definitions (addresses/dp_functions)
	
	Changes:
		- OPC UA Client for Iseg runs as manager number 8 as for OPC DA
		- OPC UA Server and subscription were renamed to follow new JCOP convention
	

Version 5.0.9 5/10/2017
	
	Modules: doClearBitField DPE added  (ENS-20567)


Version 5.0.8 2/8/2017

	Please start the fwIseg/fwIsegMigration.pnl panel and follow the instruction to update existing ISEG devices to the latest definitions (addresses/dp_functions)

	Channels: dp_function for killEnable is removed (ENS-20199)

Version 5.0.7 21/7/2017

	Please start the fwIseg/fwIsegMigration.pnl panel and follow the instruction to update existing ISEG devices to the latest definitions (addresses/dp_functions)

	Channels: EmcyStat is now based on EventStatus, dp_function added for killEnable (ENS-20199)

Version 5.0.6 21/4/2017

	Please start the fwIseg/fwIsegMigration.pnl panel and follow the instruction to update existing ISEG devices to the latest definitions (addresses/dp_functions)

	Upgrade of migration procedure to fix ENS-19572 (_distrib configs are also being removed during migration)

	Channels: DelayedTripAction and DelayedTripTime DPEs added (ENS-19255), panel for channel operation updated to support these new features


Version 5.0.5 17/3/2017

	Please start the fwIseg/fwIsegMigration.pnl panel and follow the instruction to update existing ISEG devices to the latest definitions (addresses/dp_functions)

	Upgrade of migration procedure to fix ENS-19253 (Issue with address config for FwIsegChannel.Actual.On)

	Crates: Fix for warning "Can not write to node" concerning ALive item 
	Crates: update of dp fuctions for .AlarmFlag and .StatusACLinePower
	Crates: dpe for FirmwareName, FirmwareRelease and DeviceClass added. Panel for crate operation updated to display these properties.
	Crates: Settings.Status and Settings.StatusACLinePower removed (these items are read-only in OPC UA)
	
	Channels: ReadBackSettings.Iset receives now value via dp function from ReadBackSettings.ITrip (ENS-19254) 

	Modules: Dpe for FirmwareName added. Panel for module operation upgraded to include this property.
	Modules: Address added for GeneralFineAdjust. Panel for module operation upgraded to include this functionality. 


Version 5.0.4 6/2/2017

	New dedicated panel for migration of existing ISEG devices to the latest definitions (addresses/dp_functions) - please start fwIseg/fwIsegMigration.pnl and follow the instructions

	Dedicated panels for browsing and creating ISEG devices in DEN replaced standard fwDevice panels to fix problem with missing dp functions when creating new devices

	Fix for problem with ISEG panels in DEN (�found device in not allowed slot) when using non-default naming

	Module: added mappings for items  .On and .doClear

	Module: update of mapping .setKillEnable -> .KillEnable

	Channel: .Stat register implemented via dpFunction for backward compatiblity with old EHQ modules

	Update of dp functions for:
		Crate: Actual.StatusACLinePower
		Module:  .Actual.AlarmInfoVoltSupplies 
		Module: .Actual.GeneralStable 
		Module: .Actual.GeneralSumError 

	Channel Operation panel: fix for problem with display of nominal voltage/current



Version 5.0.3 6/7/2016

	Update of OPC mappings
	Channels:ISet and ITrip dp are now both asocciated now with CurrentSet OPC item (ENS-17540)


Version 5.0.2 17/6/2016

	Update of opc mappings 
	Error flags added for crates
	Error flags are based now on EventStatus register for modules
	Fix of ISEG_SUBSCRIPTION setting
	Support for setKillEnable added for modules


Version 5.0.1 23/5/2016

	Update of opc mappings  / panels for OPC UA 


Version 5.0.0 - 26/1/2016

	1. support for Iseg OPC UA server added


------------------
| Versions 4.0.x |
------------------

Version 4.0.0 - 22/2/2013

This a Version of fwIseg for WINCC OA 3.11 + fwCore 5.0.0 + IsegOPCs 6.0.0.9
1. upgrade to wincc opc da manager
2. new Prog id of Iseg OPCS  
3. datapoints list updated



------------------
| Versions 3.3.x |
------------------
[Version 3.3.x runs with Iseg OPC 5.00.xx], using the 2.73p DLL version or more recent.



Version 3.3.4 - 23/11/2011

Installation is now in silent mode for compatibility with PVSSBootstrapper/central installation 



Version 3.3.3 - 14/01/2010

Bug fixed :
---------

1. XML file modification : compatible with intallation component 4.0.0

2. implementation of the extention (*.ctl) in fwIseg.config

3. Assignation of the datapoint maxx.Actual.GeneralFineAdjust to the correct OPC item (setAdjust)

Modification :
------------

1. Change Datatypes of canx.cratexx.maxx.chxx.NominalV and NominalI (from uint to float)

2. Change Datatypes of canx.cratexx.maxx.EvenStatus and EventMask (from bool32 to uint)  

3. remove the following datapoints to avoid double connection or to update functionnalities delivered by the OPC server :
- Channel Level : settings.stat, settings.status, readbacksettings.stat, readbacksettings.status, readbacksettings.on
- Module level : Actual.ADCfilterFrequency, readbacksettings.on
- Crate level : all readbacksettings DPs

Improvement :
-----------

1. Add a trend button in the channel operation panel (easy plot VMeas & IMeas)

2. Additionnal information in the text box of the update panel

3. Add the FwComponent version on the devices operation panels

4. Add the text box "OPC connected" (CANbusOperation panel) to display if the client is connected to the server

5. Add the "channels table" button to display all your channels Voltage & Current measurements as a screenshot.  



Version 3.3.2 - 25/09/2009

1. Bug removed from the Emergency button (module operation).

2. Improvement of the Emergency functionality (add the clear Emergency button on Channel & Module panels operation).

3. Remove the error message when clicking on the readback & unit voltage table (Channel operation).

4. Change the format from DPE types channel.Status & channel.Stat (from bit32 to uint).

5. Implementation of the update panel allowing to update all Iseg devices Datapoints (with the default value or addresses).



Version 3.3.1 - 14/07/2009

1. FwComponent support Iseg power supply HPn 20 757 (use "All1Channel" module model).

2. Implementation of the "NO CRATE" crate model (to be used for devices without crate items).

3. Remove the Refresh "Force" access at the CANbus panel level.


Version 3.3.0 - 24/04/2009

1. FwComponent version goes with the Iseg OPC server 5.00.0016 version and is compatible with PVSS 3.8

2. FwComponent compatible with ALL the Iseg EDS, EHS, EHQ modules. 

3. FwComponent include a migration panel to change the old DPT to the new DPT without having to remove any datapoints.

4. Object Library released : - Warning message appears if the setting for VAllChannels is upper than the nominal V.
                          



------------------
| Versions 3.2.x |
------------------
[Version 3.2.x runs with Iseg OPC 5.00], using the 2.68p DLL version !!

Version 3.2.0 - 28/03/2008

1. FwComponent version goes with the Iseg OPC server 5.00.0006 version.

2. FwComponent compatible with ALL the Iseg EDS, EHS, EHQ modules. 

3. 20 OPC client groups created (see document joined with the framework).

4. Simplification of the models : - 1 for the crate
				  - 2 for the modules (8 and 16 channels)
				  - 1 for the channels

5. Object Library released and simplify.




Version BETA 3.2.0 21/02/2008


1. Beta version for LHCb Velo. Compatible with Iseg OPC 5.00




------------------
| Versions 3.1.x |
------------------
[Version 3.1.x runs with Iseg OPC 4.10]


Version 3.1.2 07/06/2007

1. Bug fixed on PHOS request : Modify DP IsetAllChannels from unsigned to float (implemented for ModuleClass7)

			     : Change Ack Alarm code to reset ILimit, VLimit, ITrip, Emergency flag by pushing only one button
                               (implemented for Iseg Objects Library).

			     : Add the GeneralKill item for modules Class7 (create the Datapoint + implemented in Objects Library)


2. Bug fixed on COMPASS request : problem of compatibility with linux

				: Implement an Ack Alarm by channel (Implemented in the Objects Library)


3. Current Development : Modification of some Objects design.

		       : Change objects script for Channel, crate, module to open trends in printable windows.   				



Version 3.1.1 18/04/2007

1. Bugs fixed on Alice TPC request : Remove items from ModelClass6 - statINHIBIT
								   - ChnotOK
								   - AllocChn

				   : Add items for ModelClass6 	   - Supply-15V
				  				   - Supply-5V

				   : Remove items from ModelClass0 - ISetAllChannels

					
	
2. Bug fixed on Alice PHOS request : ITrip setting display enable (Objects Library).

3. Bug fixed on COMPASS request : problem of compatibility with linux.



version 3.1.0 02/04/2007

1. Include the following module :

	- ModelClass3, deviceclass3

2. Separation of the Client groups (GroupIn, GroupOut) in 4 groups (GoupIn, GroupFastIn, GroupOut, GroupFastOut)
GroupIn & GroupOut : OPC Refresh time = 10 sec
GroupFastIn & GroupFastOut : OPC Refresh time = 2 sec

This increase the PVSS Client performance.

3. Implementation of the new groups for crate models, channel models, module models (modelDevice 0,1,2,3,6,7,25 +  EHQ8630nf)
=> Possibity to update some other module models on request.

4. Include Trending Plot DPL Files for Iseg Objects Library (Trending tool)
-> Channel Trends : Voltage, Current, Module Trend : Board Temperature, Crate Trend : Power Supply Temperature.

5. Modification of Iseg Objects Library code (alarm conditions, Ramping display & Setting : %Vnominal, V/sec).

6. Include Iseg Pictures library.

-------

version 3.1.0 beta  25/03/2007 Custom ALICE TRD

-------


------------------
| Versions 3.0.x |
------------------
[Version 3.0.x runs with Iseg OPC 4.10]

version 3.0.0 28/02/2007

1. fix bug of possibility to choose channel class (CL 7)

2. fix bug of possibility to open channel class7 panel from module class7 panel

3. Activate Nominal I and Nominal V (Channel CL7).

4. Fix bug of PVSS3.6 compatibility (Width).

5. Add DPE for Channel and Module (IsetAllChannnels,...).

6. Remove or change error typing items or no OPC connection items.


7.include the following modules.
	
	- EHQF607n-Fp , device class7

	- ModelClass0, device class0
	- ModelClass1, device class1
	- ModelClass2, device class2
	- ModelClass6, device class6
	- ModelClass7, device class7
	- ModelClass25, device class25


8. Include the Iseg Objects Library for easy monitoring (complet monitoring With trends, help,..)
Available only for PVSS3.6.

9. Include Iseg Objects Library Help document.





------------------
| Versions 2.x.x |
------------------

author  Dr S.Popescu 
CERN/PH-AIT, Alice experiment
on leave from IFIN-HH, Bucharest

version 2.1.1 27/09/2006

1.fix bug of set ITrip channels, device class 0

2.increase the number of nodes on the can bus (Phos iseg crate nr 176) increase to 250.

3.first version of documentation included in the panels

4.include the following modules.
	- EHQF006p605 , device class1, request from Atlas LAC
	- EHQ8605pF, 	device class7, request from Alice PHOS
	- EHQ8405p156F, device class1, request from Alice PHOS
	- EMQ8F	, 	device class7, request from Alice SDD
	- EHQ8007nF,	device class7, request from LHCb Velo
	- EHQF007nF, 	device class7, request from LHCb Velo
5.Activate Kill Enable for individual channel operation
6. Change the display of the status of Crate and Module: Alive:true or False

7.upgrade the Iseg.init file
8.upgrade fwIsegTable67: show the inhibitStatus


-------

version 2.1_june2006.zip 09/08/2006
1.fix bug to switch individual channels for device class 7

-------

Version 2.1_11apr2006  11/05/2006
1.remove all items containing Bool aray for the following reasons:
	a. the Bool Arrays items, were not possible to operate for 4.02 Iseg OPC server
	b. with 4.04 Iseg OPC server, one can change the values inside the array but this
	 was not corresponding to the 16 bit status of the module( ex: switch individual channels 
	in the OnBoolArray, the corresponding On 16 bit item was showing all channels are ON)
