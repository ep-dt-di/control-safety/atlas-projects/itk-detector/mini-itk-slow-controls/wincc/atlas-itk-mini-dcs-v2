JCOP FRAMEWORK version 8.4 for WinCC OA 3.16
============================================


INSTALLATION
-------------

This version of the Framework requires WinCC OA 3.16.
If your project is using an earlier version of WinCC OA (e.g. 3.15) please have a look at the "Update" section below.

1. Check that your project works (starts) with WinCC OA 3.16.

2. Stop the project. Perform a backup

3. Download the Component Installation Tool
   from https://jcop.web.cern.ch/jcop-framework-component-installation-tool
   It is recommended to use version 8.4.0 or higher

4. Unzip the installation tool on top of your project folder. 
   If your project already contained a previous version of the Component Installation Tool, 
   the unzipping process should overwrite the previous files

5. Download the JCOP Framework distribution from 
   https://jcop.web.cern.ch/jcop-framework-0
   You may also want to download individual distributions of selected components from 
   https://jcop.web.cern.ch/jcop-framework-components

6. Unzip the JCOP Framework distribution in a convenient temporary space. such as /tmp/jcop-framework-8.4.0
   DO NOT unzip it onto your project or component folder.

7. Start the project

8. Run the Component Installation Tool panel, ie. open the following panel in the "Vision" runtime module
       fwInstallation/fwInstallation.pnl

9. Follow the standard procedure for using the Component Installation tool
   - If it is the first time you open the Installation panel, it will ask you for a directory where to install the Framework.
   - in principle there has to be a different installation directory for each different PVSS project
     where you are using the Framework). It is recommended to use an empty folder in your system. 
   - For the field "Look for new components in:" select the PVSS\  subdirectory below the Framework unzipped distribution

10. Install the components you require for your system, following given instructions and prompts.
    In particular, pay attention to the prompt that offers to set up a default user name and password in your project.

11. It is recommended that a restart of the project is performed during the installation.
    The installation tool offers the option of running postInstallation procedures without a project restart,
    yet this option is only recommended for advanced users who deeply understand the consequences of such action


UPDATE
------
In the case where you want to update framework components, still for the same version of WinCC OA (3.16), simply use the
Component Installation tool pointing it at the place where you downloaded and unzipped the Framework Distribution.

However, if you want to perform an upgrade from the previous version(s) of WinCC OA, we strongly recommend the following sequence of actions

1. Stop the existing WinCC OA 3.15 project, and make a backup of it

2. Start the project again, note the list of installed components, and then un-install all of those that refer to WinCC OA 3.15

3. It is strongly recommended to temporary set the root password to empty, and configure the config file of your project
   to use the root username and password by default.

4. Stop the project

5. Update the project using the tools in WinCC OA Project Administration panel, to version 3.16 of WinCC OA

6. Unzip a recent version of fwInstallation tool on top of the project (see the download link in previous section).

7. Start the project with WinCC OA 3.16. It is recommended to start only the Data Manager, Event Manager, Simulation Manager number 1,
   and the CTRL manager that runs the pvss_scripts.lst . Then start a UI, and open the Component Installation Tool

8. Install newest versions of components that you noted down in point 2; those for the JCOP Framework could be downloaded
   from the Framework downloads pages. The 3.16-version of custom components should be delivered by their maintainers.

9. Restart the project at the end of installation to execute the postInstallation steps

10, Make sure the project works correctly.

11. If you reset the root password to empty in point 3, then you may want to set it again to a strong one  now; 
    Once this is done, remember to put a reasonable default userName/password in the config file of the project

--------------------------------------------------------------------------------------------------------------------
Last update: PG, May 2020
