var group__PermissionFunctions =
[
    [ "fwAccessControl_HasUserAdminPrivilege", "group__PermissionFunctions.html#ga252941306ac100c33f2b55bd0055ff15", null ],
    [ "fwAccessControl_HasGroupAdminPrivilege", "group__PermissionFunctions.html#ga186ffa5c5f7fb2fdc4880079302bb0b2", null ],
    [ "fwAccessControl_HasDomainAdminPrivilege", "group__PermissionFunctions.html#ga2a8fd965ef03a3dcbd365fa2773e4720", null ],
    [ "fwAccessControl_HasSystemAdminPrivilege", "group__PermissionFunctions.html#gab83a70846ec8226518f65ea91177f86d", null ],
    [ "fwAccessControl_getGroupPrivileges", "group__PermissionFunctions.html#ga3e4f6acaf23bd3fda437f26bc0dd46f9", null ],
    [ "fwAccessControl_updateGroupPrivileges", "group__PermissionFunctions.html#ga4d7c7a5f17a7b71a1c80d662a407bb72", null ],
    [ "fwAccessControl_getUserPrivileges", "group__PermissionFunctions.html#ga116a24fc314dd39217203d7d1d0916f5", null ],
    [ "fwAccessControl_checkUserPrivilege", "group__PermissionFunctions.html#ga6fa78917bfc7ff6e851b2a36711db5df", null ],
    [ "fwAccessControl_isGranted", "group__PermissionFunctions.html#ga849cae607bc8b26edce17ad209aa252c", null ],
    [ "fwAccessControl_getGroupsHavingPrivilege", "group__PermissionFunctions.html#gac95e1354799d1e631bf7bb2402b1cd93", null ],
    [ "fwAccessControl_getUsersHavingPrivilege", "group__PermissionFunctions.html#ga09ed5a20aa529024dd6e355700c64a08", null ],
    [ "fwAccessControl_getPrivilegeNames", "group__PermissionFunctions.html#ga66d146b3e996092c2041204935d5e393", null ],
    [ "fwAccessControl_getAllAccessRightNames", "group__PermissionFunctions.html#ga14fb2630696df901e75c6345717263b3", null ],
    [ "fwAccessControl_checkUserPrivilege_AuthFunc", "group__PermissionFunctions.html#ga2d5d0d6544a3f29d7df665f554333d71", null ],
    [ "fwAccessControl_setPrivilegeNames", "group__PermissionFunctions.html#gaf129b8b4003e49c54fe0fa810582677b", null ]
];