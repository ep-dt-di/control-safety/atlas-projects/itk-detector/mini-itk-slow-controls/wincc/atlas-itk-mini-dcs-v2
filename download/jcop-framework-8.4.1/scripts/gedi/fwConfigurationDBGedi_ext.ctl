main() 
{
	int idMenu = moduleAddMenu("JCOP Framework");
	int idToolbar=-1;

	string icon="dbicon_ok.gif", shortcut="";
	moduleAddAction("ConfigurationDB Tool", icon, shortcut, idMenu, idToolbar, "_openConfigurationDB");
}

/**
* @reviewed 2020-05-22 @whitelisted{WinCCOAIntegration}
*/
void _openConfigurationDB()
{
	string resizeMode=""; //default
	ModuleOnWithPanel("Configuration Database Tool", 5, 5, 10, 20, 1, 1, resizeMode,
						"fwConfigurationDB/fwConfigurationDB.pnl", "ConfigurationDB Tool", 
						makeDynString("$sDpName:","$sHierarchyType:LOGICAL")
					);
}
