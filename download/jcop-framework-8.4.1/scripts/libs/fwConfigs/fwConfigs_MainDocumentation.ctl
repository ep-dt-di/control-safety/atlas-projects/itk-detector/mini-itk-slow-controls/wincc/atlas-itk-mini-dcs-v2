/**@file
 *
 * This package contains documentation of the fwConfigs library set.  For documentation and examples, refer to the <a href="pages.html">Related Pages</a> section.

 *
 * @author Marco Boccioli, Piotr Golonka (EN/ICE-SCD)
 */

/**
@page fwDpFunction fwDpFunction
 *fwDpFunction features the following dp function types:
@li @ref fwConfigsDpFunctionsDpeConnection
@li @ref fwConfigsDpFunctionsStat
@li @ref fwConfigsDpFunctions
 *
 */

/**
@page fwAlertConfig fwAlertConfig
*
fwAlertConfig manual: @ref fwConfigsAlert
*
*/

/**
@page fwArchive fwArchiveConfig

@par
@ref fwArchiveConfig provides a set of functions to manipulate the WinCC OA archive configs.
@par
For further details, refer to the description of the library and the examples therein.
*
*/

/**
 * @page fwDPELock fwDPELockManual
 *
 * fwDPELock: This library is moved to fwGeneral as of version 8.4.0 (<a href="https://its.cern.ch/jira/browse/FWCORE-3411">FWCORE-3411</a>)
 *
 */

/**
@page fwPeripheryAddressPage fwPeriphAddress
*
fwPeripheryAddress manual: @ref fwPeripheryAddress
*
*/

/** @mainpage JCOP Framework Configs
 *
 *
\section documentation Documentation:
* For documentation and examples, refer to the <a href="pages.html">Related Pages</a> section.

* Not all the libraries have a manual implemented. If a manual is not yet available on the Related Pages section, please refer directly to the <a href="files.html">Files</a> section.

\section changeLog Change log:
See the full list of <a target=_blank href="https://icecontrols.its.cern.ch/jira/secure/IssueNavigator.jspa?reset=true&jqlQuery=project+%3D+FWCORE+AND+issuetype+in+standardIssueTypes%28%29+AND+status+%3D+resolved+and+fixVersion+is+not+null+ORDER+BY+fixVersion+DESC%2C+updated+DESC%2C+key+DESC">improvements and bug fixes</a>.
*/
