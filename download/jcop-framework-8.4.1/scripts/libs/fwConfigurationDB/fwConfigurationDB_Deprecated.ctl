#uses "fwConfigurationDB/fwConfigurationDB.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Hierarchies.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Utils.ctl"

/**@file
 *
 * This package contains deprecated functions, which are
 * currently kept for compatibility
 *
 * (c) Copyright CERN, All Rights Reserved
 */

global string _fwConfigurationDB_fileVersion_fwConfigurationDB_Deprecated_ctl="8.4.1";

/** Gets meta-information concerning the recipe cache
 *
 * @deprecated 2018-06-22
 */
void fwConfigurationDB_GetRecipeCacheMetaInfo(string recipeCache,
                string &recipeComment,
                int &numDevices,
                int &numValues,
                int &numAlerts,
                dyn_string &exceptionInfo)
{
    FWDEPRECATED();
    fwConfigurationDB_checkInit(exceptionInfo);
    if (dynlen(exceptionInfo)) return;

    string dpName=_fwConfigurationDB_getRecipeCacheDP(recipeCache, exceptionInfo, FALSE);
    if (dynlen(exceptionInfo)) return;
 
    dyn_string devNames,values,alerts;
 
    dpGet(dpName+".RecipeComment",recipeComment,
          dpName+".DataPoints.DPNames",devNames,
          dpName+".Values.DPENames",values,
          dpName+".Alerts.DPENames",alerts);
    numDevices=dynlen(devNames);
    numValues=dynlen(values);
    numAlerts=dynlen(alerts);
 
}
 
/** Finds dpTypes from JCOP Framework device types
 *
 * @deprecated 2018-06-22
 */
void fwConfigurationDB_deviceTypesToDpTypes(dyn_string devTypes, dyn_string &dpTypes, dyn_string &exceptionInfo,
                bool errorOnNonExistingType=true)
{
        FWDEPRECATED();
        dynClear(dpTypes);

        // we will need these for type conversions:
        dyn_dyn_string fwAllDevTypes;
        dyn_string allDpTypes=dpTypes();
        fwDevice_getAllTypes(fwAllDevTypes, exceptionInfo);
        if (dynlen(exceptionInfo)) return;


        for (int i=1; i<=dynlen(devTypes); i++) {
                if (devTypes[i]=="SYSTEM") {
                        dpTypes[i]="";
                        continue;
                };
                int idx=dynContains(fwAllDevTypes[1],devTypes[i]);
                if (idx>=1) {
                        dpTypes[i]=fwAllDevTypes[2][idx];
                } else {
                        int idx0=dynContains(allDpTypes,devTypes[i]);
                        if (idx0>=1) {
                                dpTypes[i]=allDpTypes[idx0];
                        } else {
                                if (errorOnNonExistingType) {
                                        fwException_raise(exceptionInfo,"ERROR","Unknown device/dpType "+devTypes[i],"");
                                } else {
                                        dpTypes[i]="";
                                }
                        }
                }
        };
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeAuthor(dyn_dyn_mixed &recipeObject, string &recipeAuthor, dyn_string &exceptionInfo)
{
        FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_AUTHOR, recipeAuthor,exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeClass(dyn_dyn_mixed &recipeObject, string &recipeClass, dyn_string &exceptionInfo)
{
        FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_CLASSNAME, recipeClass,exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeCreationTime(dyn_dyn_mixed &recipeObject, time &creationTime, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_CREATIONTIME, creationTime,exceptionInfo);
}


/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeLastActivationUser(dyn_dyn_mixed &recipeObject, string &lastActivationUser, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_LASTACTIVATIONUSER, lastActivationUser,exceptionInfo);
}


/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeLastModificationComment(dyn_dyn_mixed &recipeObject, string &lastModificationComment, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_LASTMODIFICATIONCOMMENT, lastModificationComment,exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeLastModificationTime(dyn_dyn_mixed &recipeObject, time &lastModificationTime, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_LASTMODIFICATIONTIME, lastModificationTime,exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeLastModificationUser(dyn_dyn_mixed &recipeObject, string &lastModificationUser, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_LASTMODIFICATIONUSER, lastModificationUser,exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipePredefined(dyn_dyn_mixed &recipeObject, bool &predefined, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_PREDEFINED, predefined, exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_getRecipeVersionString(dyn_dyn_mixed &recipeObject, string &recipeVersion, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_getRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_RECIPEVERSION, recipeVersion, exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_setRecipeClass(dyn_dyn_mixed &recipeObject, string recipeClass, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_setRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_CLASSNAME, recipeClass,exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_setRecipeLastActivationTime(dyn_dyn_mixed &recipeObject, time lastActivationTime, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_setRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_LASTACTIVATIONTIME, lastActivationTime,exceptionInfo);
}


/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_setRecipeLastActivationUser(dyn_dyn_mixed &recipeObject, string lastActivationUser, dyn_string &exceptionInfo)
{
	FWDEPRECATED();
	fwConfigurationDB_setRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_LASTACTIVATIONUSER, lastActivationUser,exceptionInfo);
}

/**
 @deprecated 2018-06-25
 */
void _fwConfigurationDB_setRecipePredefined(dyn_dyn_mixed &recipeObject, bool predefined, dyn_string &exceptionInfo)
{
        FWDEPRECATED();
	fwConfigurationDB_setRecipeMetaInfo(recipeObject, fwConfigurationDB_RO_META_PREDEFINED, predefined, exceptionInfo);
}


/**
 @deprecated 2018-08-03
 */
string _fwConfigurationDB_itemIdListForSQLQuery(dyn_int itemIdList)
{
        FWDEPRECATED();
        return _fwConfigurationDB_listToSQLString("ITEM_ID", itemIdList);
}

/**
 @deprecated 2018-08-03
 */
string _fwConfigurationDB_listToSQLString(string columnName, dyn_string itemIdList)
{
        FWDEPRECATED();
        if (dynlen(itemIdList)==0) return "";

        string sItemIdList=" ( "+columnName+" IN (";
        for (int i=1; i<=dynlen(itemIdList); i++) {
                sItemIdList=sItemIdList+"\'"+itemIdList[i]+"\'";
                if (i!=dynlen(itemIdList)) {
                        if (i%999==0) {
                                sItemIdList=sItemIdList+") OR "+columnName+" IN (";
                        } else {
                                sItemIdList=sItemIdList+",";
                        }
                }
        };
        sItemIdList=sItemIdList+") )";

        return sItemIdList;

}


   
/**
 @deprecated 2018-08-03
 */
void fwConfigurationDB_getDBHierarchies(dyn_int &ids, dyn_string &types, dyn_string &descriptions,
                                                                                dyn_string &validFrom, dyn_string &validTo,
                                                                                dyn_string &exceptionInfo)
{
        FWDEPRECATED();
        dyn_dyn_mixed aRecords;
        string sql="SELECT HVER,HTYPE,DESCR,VALID_FROM,VALID_TO FROM HIERARCHIES ORDER BY HVER";
        _fwConfigurationDB_executeDBQuery(sql,g_fwConfigurationDB_DBConnection, aRecords, exceptionInfo);
        if (dynlen(exceptionInfo)) return;
        for (int i=1; i<=dynlen(aRecords); i++) {
                if (((int)aRecords[i][4])<=0) aRecords[i][4]="";
                if (((int)aRecords[i][5])<=0) aRecords[i][5]="";
                dynAppend(ids,aRecords[i][1]);
                dynAppend(types,aRecords[i][2]);
                dynAppend(descriptions,aRecords[i][3]);
                dynAppend(validFrom,aRecords[i][4]);
                dynAppend(validTo,aRecords[i][5]);
        }

}


 
/** version with improper spelling, for backward compatibility, FWCDB-1102

   @deprecated 2018-08-03
*/
void fwConfigurattionDB_deleteDeviceConfiguration(string confName, dyn_string &exceptionInfo, bool deleteOldOnly=false)
{
    FWDEPRECATED();
    DebugTN("**** WARNING! Using deprecated, mis-spelled function name fwConfigurattionDB_deleteDeviceConfiguration",
        "Please, update your code -> called from", getStackTrace()[2]);
    fwConfigurationDB_deleteDeviceConfiguration(confName, exceptionInfo, deleteOldOnly);
}
      