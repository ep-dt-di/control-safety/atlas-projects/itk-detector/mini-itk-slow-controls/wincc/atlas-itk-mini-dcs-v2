#uses "fwRack/fwRackDeprecated.ctl"


//========================================================================================
//                 HISTORY OF MODIFICATIONS
//
//                 ORIGINAL AUTHOR: Giulio 
//
//========================================================================================
// This library contains general-purpose, not project specific functions, which could be 
// reused in other projects. Ideally they should go into fwGeneral
//
//
// bool fwAuxil_doesDpTypeExist(string dpt)
// int fwAuxil_createDatapointType(dyn_anytype type_desc)
// int fwAuxil_modifyDatapointType(dyn_anytype type_desc)
// void fwAuxil_createListDptype()
// void fwAuxil_createDynIntDptype()
// void fwAuxil_createIntParamDptype()
// void fwAuxil_createBoolDptype()
// void fwAuxil_createParamDptype()
//
// dyn_int fwAuxil_blobGetValue_int(blob myblob,int nelem = 0,int pos = 0)
// dyn_int fwAuxil_dpBlobGetValue_int(string dpe,int nelem = 0,int pos = 0)
// int fwAuxil_blobSetValue_int(blob &myblob,dyn_int iarr, int nelem = 0, int pos = 0)
// int fwAuxil_dpBlobSetValue_int(string dpe,dyn_int iarr, int nelem = 0, int pos = 0)
//
// bool fwAuxil_Confirm(string message)
// void fwAuxil_Error(string message)
//
//========================================================================================
//   DATAPOINT TYPE RELATED FUNCTIONS
//========================================================================================
// function to check if a datapoint type exists. It should go into a more general library
bool fwAuxil_doesDpTypeExist(string dpt)
{
  dyn_string types;
   
  types = dpTypes(dpt);
  if (dynlen(types) > 0) return(TRUE);
  return(FALSE);
}

//============================================================================
/**
   int fwAuxil_createDatapointType(dyn_anytype type_desc)

   fwAuxil_createDatapointType converts the datastructure contain in type_desc and based on triplets
   "dpe_name",  (the name of the dpe)
   "level",     (the level depth of dpe in the tree-like dptype structure. Top level = 1.)
   "dpe_type"   (integer PVSS code for the dpe type. Ex. 1=struct, 21=int, 23=bool, 25=string)
   into the structure used by PVSS dpTypeCreate, and tries to recreate the datatype.
   The first element of type_desc is the datapoint type name.
   Special case : Code 41 = typeref (Reference to an existing Datapoint Type)
   
   It should go to a more general library
*/
//============================================================================
int fwAuxil_createDatapointType(dyn_anytype type_desc)
{
int i,j,k,status;
int level,type;
string name;

dyn_string el_string;
dyn_int    el_type;

dyn_dyn_string xxdepes;
dyn_dyn_int xxdepei;
k=0;
for(i=1;i<=dynlen(type_desc);i=i+3) {
  k=k+1;
  name=type_desc[i];
  level = type_desc[i+1];
  type = type_desc[i+2];
  dynClear(el_string);
  dynClear(el_type);
  for(j=1;j<level;j++) {
    dynAppend(el_string,"");
    dynAppend(el_type,0);
  }
  dynAppend(el_string,name);
  dynAppend(el_type,type);
  if (type==41) {dynAppend(el_string,type_desc[i+3]);i=i+3;if (DLevel > 0) DebugTN(el_string);}
  xxdepes[k]=el_string;
  xxdepei[k]=el_type;
}

status = dpTypeCreate(xxdepes,xxdepei);
return(status);
}
//============================================================================
/**
   int fwAuxil_modifyDatapointType(dyn_anytype type_desc)

   fwAuxil_modifyDatapointType converts the datastructure contain in type_desc and based on triplets
   "dpe_name",  (the name of the dpe)
   "level",     (the level depth of dpe in the tree-like dptype structure. Top level = 1.)
   "dpe_type"   (integer PVSS code for the dpe type. Ex. 1=struct, 21=int, 23=bool, 25=string)
   into the structure used by PVSS dpTypeChange, and tries to modify the (existing) datatype.
   The first element of type_desc is the datapoint type name.
   Special case : Code 41 = typeref (Reference to an existing Datapoint Type)
   
   It should go to a more general library
*/
//============================================================================
int fwAuxil_modifyDatapointType(dyn_anytype type_desc)
{
int i,j,k,status;
int level,type;
string name;

dyn_string el_string;
dyn_int    el_type;

dyn_dyn_string xxdepes;
dyn_dyn_int xxdepei;
k=0;
for(i=1;i<=dynlen(type_desc);i=i+3) {
  k=k+1;
  name=type_desc[i];
  level = type_desc[i+1];
  type = type_desc[i+2];
  dynClear(el_string);
  dynClear(el_type);
  for(j=1;j<level;j++) {
    dynAppend(el_string,"");
    dynAppend(el_type,0);
  }
  dynAppend(el_string,name);
  dynAppend(el_type,type);
  if (type==41) {dynAppend(el_string,type_desc[i+3]);i=i+3;if (DLevel > 0) DebugTN(el_string);}
  xxdepes[k]=el_string;
  xxdepei[k]=el_type;
}

status = dpTypeChange(xxdepes,xxdepei);
return(status);
}
//================================================================================
//
void fwAuxil_createListDptype()
{
dyn_anytype type_desc;

  if (fwAuxil_doesDpTypeExist("FwList")) return;
  dynAppend(type_desc,"FwList");dynAppend(type_desc,1);dynAppend(type_desc,1);
  dynAppend(type_desc,"list");dynAppend(type_desc,2);dynAppend(type_desc,9);
  fwAuxil_createDatapointType(type_desc);
}
//=================================================================================
void fwAuxil_createDynIntDptype()
{
dyn_anytype type_desc;

  if (fwAuxil_doesDpTypeExist("FwDynInt")) return;
  dynAppend(type_desc,"FwDynInt");dynAppend(type_desc,1);dynAppend(type_desc,1);
  dynAppend(type_desc,"iarr");dynAppend(type_desc,2);dynAppend(type_desc,5);
  fwAuxil_createDatapointType(type_desc);
}
//=================================================================================
void fwAuxil_createIntParamDptype()
{
dyn_anytype type_desc;

  if (fwAuxil_doesDpTypeExist("FwIntParam")) return;
  dynAppend(type_desc,"FwIntParam");dynAppend(type_desc,1);dynAppend(type_desc,1);
  dynAppend(type_desc,"count");dynAppend(type_desc,2);dynAppend(type_desc,21);
  fwAuxil_createDatapointType(type_desc);
}
//=================================================================================
void fwAuxil_createBoolDptype()
{
dyn_anytype type_desc;

  if (fwAuxil_doesDpTypeExist("FwBoolValue")) return;
  dynAppend(type_desc,"FwBoolValue");dynAppend(type_desc,1);dynAppend(type_desc,1);
  dynAppend(type_desc,"Value");dynAppend(type_desc,2);dynAppend(type_desc,23);
  fwAuxil_createDatapointType(type_desc);
}
//=================================================================================
void fwAuxil_createParamDptype()
{
dyn_anytype type_desc;

  if (fwAuxil_doesDpTypeExist("FwParam")) return;
  dynAppend(type_desc,"FwParam");dynAppend(type_desc,1);dynAppend(type_desc,1);
  dynAppend(type_desc,"param");dynAppend(type_desc,2);dynAppend(type_desc,25);
  fwAuxil_createDatapointType(type_desc);
}

//========================================================================
// BLOB FUNCTIONS
//========================================================================
dyn_int fwAuxil_blobGetValue_int(blob myblob,int nelem = 0,int pos = 0)
{
int i;
dyn_int iarr;
int b_l;

b_l = bloblen(myblob); 
if (b_l == 0) return(iarr);
if (nelem == 0) nelem = b_l/2;
for(i=1;i<=nelem;i++) blobGetValue(myblob,pos+(i-1)*2,iarr[i],2,TRUE);
return(iarr);
}

//==========================================================================
dyn_int fwAuxil_dpBlobGetValue_int(string dpe,int nelem = 0,int pos = 0)
{
blob myblob;
int i;
dyn_int iarr;
int b_l;

i = dpGet(dpe,myblob);
if (i < 0) return(iarr);
iarr = fwAuxil_blobGetValue_int(myblob,nelem,pos);
return(iarr);
}

//==========================================================================
int fwAuxil_blobSetValue_int(blob &myblob,dyn_int iarr, int nelem = 0, int pos = 0)
{
  int i,error;
  
  if (nelem == 0) nelem = dynlen(iarr);
  if (nelem > dynlen(iarr)) error = -1;
  blobZero(myblob,nelem*2+pos);
  for(i=1;i<=nelem;i++) blobSetValue(myblob,pos+(i-1)*2,iarr[i],2,TRUE);
  //DebugTN("bloblen="+bloblen(myblob));
  return(error);
}



//=============================================================================
//        CONFIRMATION OR INFORMATION DIALOGUE
//=============================================================================
bool fwAuxil_Confirm(string message)
{
dyn_float rf;
dyn_string rt;
dyn_string params;


dynAppend(params,"$1:"+message);
dynAppend(params,"$2:Ok");
dynAppend(params,"$3:Cancel");
ChildPanelOnCentralModalReturn("vision/MessageWarning2","Confirm",params,rf,rt);
if (dynlen(rt) < 1) return(FALSE);
if (rt[1] == "true") return(TRUE);
return(FALSE);
}


