//==============================================
//     HISTORY OF MODIFICATIONS
//
//     ORIGINAL AUTHOR: Bobby
//
//     11-3-2013 G.M. modified fwRackConnections_createPLCConnection to add editing functionality
//
//     15-3-2013 G.M. cosmetic changes
//
//     10-2014   O.H. - adaptation to redundancy
//                    - better queueing mechanism for commands to PLC
//
//     2016-07-25 saved as 3.14 G.M.
//
//     2019-05 G.M., M.S. modifications to catch changes in the Configuration Tables
//       - in _fwRackConnections_updatePLCStatus
//             -added fwRackTool_CompareTablesWithLastUsed, after _fwRackConnections_loadTables (in two places)
//       - in_fwRackConnections_sendCommand
//             -added a check on ConfigurationTablesMismatch, to avoid sending commands when configuration is not consistent
//       - modified function _fwRackConnections_connectConfigurationTable to avoid setting multiple callbacks
//       - added fwRackTool_CompareConfigurationTable in _fwRackConnections_updateConfigTable function
//
//===========================================================

#uses "fwRack/fwRackTool.ctl"
#uses "fwRack/fwRackCommand.ctl"
#uses "fwRack/fwRackDeprecated.ctl"


private bool DEBUG = false;


dyn_string  dsStatusRawPlcs,
            dsCommandResponsePlcs,
            dsCommandPlcs;

mapping g_iThreadIds; // O.H. 10-2014

/** Procedure to insert the modbus plc entries in the config file

@par Constraints

@par Usage
	Private

@par PVSS managers

@param ident destination device object
@param val pasted device object
*/
//=============================================================
void fwRackConnections_connectPLC (string dp)
{
  dpSetWait(dp + ".Command", "CONNECT", dp + ".Status.Busy",  TRUE);                                 
}

//=============================================================
void fwRackConnections_disconnectPLC (string dp)
{
  DebugN("fwRackConnections_disconnectPLC " +dp);
  dpSetWait(dp + ".Command", "DISCONNECT", dp + ".Status.Busy",  TRUE);      
}

//=============================================================
void _fwRackConnections_disconnect(string dp)
{
  _fwRackConnections_loadTables(dp,FALSE);
  string modDP = "_"+dp+"_Modbus";
  //DebugTN(modDP);
  if (dpExists(modDP)) dpSetWait(modDP + ".Active", false);     
  modDP += "_2";  // O.H. 10-2014
  if (dpExists(modDP)) dpSetWait(modDP + ".Active", false);    // O.H. 10-2014 
}

//=============================================================
// modified by G.M. 11-3-2013: now dpSetWait is always called, so that connection parameters can be modified
void fwRackConnections_createPLCConnection (string plcName, string IP, string user, int modNum, 
                                            string port, dyn_string &dsExceptions)
{
  string plcDp = "RCA/" + plcName;
  string driverDp = "_Driver"+modNum;
  string statisticsDp = "_Stat_Configs_driver_"+modNum;
  
  if (!dpExists(plcDp)) dpCreate(plcDp, "FwRackPowerDistributionPLC");

//--------- O.H. 10-2014 adaptation for redundancy
  if (!dpExists(driverDp)) dpCreate(driverDp, "_DriverCommon");
  driverDp+="_2";
  if (!dpExists(driverDp)) dpCreate(driverDp, "_DriverCommon");

  if (!dpExists(statisticsDp)) dpCreate(statisticsDp, "_Statistics_DriverConfigs");
  strreplace(statisticsDp, "_Stat", "_Stat_2");  
  if (!dpExists(statisticsDp)) dpCreate(statisticsDp, "_Statistics_DriverConfigs");

  dpSetWait(plcDp + ".IPAddress", IP,
            plcDp + ".Port", port,
            plcDp + ".ModbusNumber", modNum,
            plcDp + ".User", user);    
}

//=============================================================
void fwRackConnections_ReconfigurePLC (string dp)
{
  dpSetWait(dp + ".Command", "CONFIGURE", dp + ".Status.Busy",  TRUE); 
}

//=============================================================
void _fwRackConnections_connect(string IPAddress,string Port,int ModbusNumber,string dp)
{
  string modDP = "_"+dp+"_Modbus";
  string modDP2 = modDP + "_2";
  int createSimul;  //----new G.M. 2016_07_26 fwRack5.5.0
  string filename;  //----new G.M. 2016_07_26 fwRack5.5.0
  
  if (DEBUG) DebugTN("In local copy of fwRackConnections_connect");
  
  if (!dpExists(modDP)) dpCreate(modDP, "_Mod_Plc");             
  if (!dpExists(modDP2)) dpCreate(modDP2, "_Mod_Plc");             

  if (DEBUG) DebugTN("Setting mod internals for " + modDP +
                    " -- fwRackConnections.ctl/_fwRackConnections_connect");
    
  dpSetWait(modDP + ".DrvNumber", ModbusNumber,
            modDP2 + ".DrvNumber", ModbusNumber);
  string ipAndPort = IPAddress +":"+ Port;
  dpSetWait(modDP + ".PlcNumber",            1,
            modDP + ".TransactionTimeout",   1000,
            modDP + ".Coding",               1,
            modDP + ".Endianity",            2,
            modDP + ".HostsAndPorts",        makeDynString(ipAndPort),
            modDP + ".Active",               true, // O.H. 10-2014 redundancy
            modDP2 + ".PlcNumber",            1,
            modDP2 + ".TransactionTimeout",   1000,
            modDP2 + ".Coding",               1,
            modDP2 + ".Endianity",            2,
            modDP2 + ".HostsAndPorts",        makeDynString(ipAndPort),
            modDP2 + ".Active",               true);       
  
  if (DEBUG)  DebugTN("Inserting plc in config if not there yet "+modDP+":"+ModbusNumber+
                      " -- fwRackConnections.ctl/_fwRackConnections_connect");
  _fwRackConnections_insertPLCInConfig(modDP, ModbusNumber);
  if (DEBUG) DebugTN("Appending modbus driver " + ModbusNumber +
                     " -- fwRackConnections.ctl/_fwRackConnections_connect"); 
  
  //----new G.M. 2016_07_26  : option to start Modbus or a Simulator   fwRack5.5.0
  if (dpExists("FwRack_CreateSimulDriver")==FALSE) dpCreate("FwRack_CreateSimulDriver","FwIntParam");
  dpGet("FwRack_CreateSimulDriver.count",createSimul);
  if (createSimul == 0) {
     filename = getPath(DATA_REL_PATH)+"StartSimulator.txt";
     if (access(filename,F_OK) == 0) createSimul = 1;
  }
  DebugN("createSimul = "+createSimul);
  
  if (createSimul == 0) {
      fwInstallationManager_append(true,"Rack ControlPLC", "WCCOAmod","always", 30, 2, 2, "-num " + ModbusNumber);   
  } else { 
      fwInstallationManager_append(true,"Rack ControlPLC", "WCCILsim","always", 30, 2, 2, "-num " + ModbusNumber);   
      fwInstallationManager_append(true,"Rack ControlPLC", "WCCOAmod","manual", 30, 2, 2, "-num " + ModbusNumber);   
  }
  //_fwRackConnections_restartModbusDriver(ModbusNumber); 
} 

//=============================================================
void _fwRackConnections_setAddress (string plcDP, 
                                    string modDP, 
                                    int iRaw, 
                                    string pollingGroup="RackPowerDistribution", 
                                    bool bActivate=FALSE,
                                    bool invalid = FALSE,
                                    int  iMode = DPATTR_ADDR_MODE_INPUT_POLL)
{
  int DrvNumber;
  int PLCNumber;
  string RawDPE = plcDP + ".RawData.Raw_";
   
  dpGet(modDP + ".PlcNumber", PLCNumber, modDP + ".DrvNumber", DrvNumber);
  
  if (!invalid) {
      dpSetWait(RawDPE+iRaw+":_distrib.._type",56,
                RawDPE+iRaw+":_distrib.._driver", DrvNumber,
	               RawDPE+iRaw+":_address.._type", 16,
	               RawDPE+iRaw+":_address.._active", bActivate,
	               RawDPE+iRaw+":_address.._datatype", 570,
	               RawDPE+iRaw+":_address.._direction", 4,
    	           RawDPE+iRaw+":_address.._drv_ident", "MODBUS",
                RawDPE+iRaw+":_address.._internal", FALSE,
      	         RawDPE+iRaw+":_address.._mode", iMode,
	               RawDPE+iRaw+":_address.._poll_group", "_" + pollingGroup, //"_Mod_Plc_FAST",
	               RawDPE+iRaw+":_address.._reference", "M."+PLCNumber+".3."+iRaw*120,
	               RawDPE+iRaw+":_address.._subindex", 0,
	               RawDPE+iRaw+":_address.._lowlevel", FALSE,
                RawDPE+iRaw+":_smooth.._type", 48,
                RawDPE+iRaw+":_smooth.._std_type", 4);      
      dpSetWait(RawDPE+iRaw+":_address.._lowlevel", TRUE);
  }
  
  dpSetWait(RawDPE+iRaw+":_original.._exp_inv", invalid);
  
}

//=============================================================
// This function is actually a callback. Inside script fwRackControlApplication, 
// function _fwRackControlApplication_PLCConnections does a dpConnect
//
//  PLCdp is the .Status.PLC dpe of a FwRackPowerDistributionPLC datapoint "xyz"
//  modDP is the .ConnState dpe of the corresponding "_xyz_Modbus" _Mod_Plc datapoint
//  drvDP is the _Connections.Driver.ManNums dpe (dptype _Connections)
//
//
// synchronized void _fwRackConnections_updatePLCStatus(string PLCdp,  int rcaState,
//                                                      string modDP,  int plcState,
//                                                      string drvDP,  dyn_int drvNums)
//
//
//  I WANT TO KNOW WHICH DPE WAS CHANGED; therefore I replace the original callback function
//  with three separate functions

//================
synchronized void _fwRackConnections_updatePLCStatus_PLCdp(string PLCdp,  int rcaState)
{
string plc;
string modDP,drvDP;
int plcState;
dyn_int drvNums;

  plc = dpSubStr(PLCdp,DPSUB_DP);
  if (DEBUG) DebugTN("updatePLCStatus_PLCdp   plc = "+plc+"    rcaState="+rcaState);
  modDP = fwInstallationRedu_getReduDp("_"+plc+"_Modbus") + ".ConnState";
  drvDP = fwInstallationRedu_getReduDp("_Connections") + ".Driver.ManNums";
  dpGet(modDP,plcState,drvDP,drvNums);
  _fwRackConnections_updatePLCStatus(PLCdp,rcaState,modDP,plcState,drvDP,drvNums);
}

//================
synchronized void _fwRackConnections_updatePLCStatus_modDP(string modDP,  int plcState)
{
string plc;
string PLCdp, drvDP;
int rcaState;
dyn_int drvNums;

plc = dpSubStr(modDP,DPSUB_DP);
plc = strltrim(plc, "_");
plc = strrtrim(plc, "_2");
strreplace(plc, "_Modbus", "");

if (DEBUG) DebugTN("updatePLCStatus_modDP   plc = "+plc+"    plcState="+plcState);
PLCdp = plc+".Status.PLC";
  drvDP = fwInstallationRedu_getReduDp("_Connections") + ".Driver.ManNums";
  dpGet(PLCdp,rcaState,drvDP,drvNums);
  _fwRackConnections_updatePLCStatus(PLCdp,rcaState,modDP,plcState,drvDP,drvNums);
}

//================
synchronized void _fwRackConnections_updatePLCStatus_drvDP(string drvDP,  dyn_int drvNums)
{
if (DEBUG) DebugTN("updatePLCStatus_drvDP: drvNums="+drvNums);
_fwRackConnections_updatePLCStatus("","","","",drvDP,drvNums);

}

//================
synchronized void _fwRackConnections_updatePLCStatus(string PLCdp,  int rcaState,
                                                     string modDP,  int plcState,
                                                     string drvDP,  dyn_int drvNums)
{
  //DebugTN(PLCdp, modDP);
  //DebugTN(rcaState, plcState);
  
  int syncPLC,syncMOD;
  string plcName;
  dyn_string dsExceptions;
  int DrvNumber;
  string PLC, Pre_fix;
  time setConfigAddressTime, last;
  bool active, waiting;
  int timeout;
  
  //dpGet since being synchronized function the value might have changed!
  //dpGet(PLCdp + ".RawData.Raw_0:_original.._stime",last); 
  if ((PLCdp == "") || (modDP == "")) return;  
  
  dpGet(PLCdp, rcaState, modDP, plcState);  
    
  if (rcaState == plcState) {
       if (DEBUG) DebugTN("PLC "+dpSubStr(PLCdp,DPSUB_DP)+"    rcaState == plcState"); 
       return;
  }
  if (rcaState == 2) return;   // new G.M. 2013-7-3  (waiting for tables)
  if (rcaState == 4) return;   // new G.M. 2013-7-3  (timeout reading tables)
  
  if (DEBUG) {
    DebugTN(PLCdp + " state: " + rcaState+ " -- _fwRackConnections_updatePLCStatus");
    DebugTN(modDP + " state: " + plcState+ " -- _fwRackConnections_updatePLCStatus");
  }
  
  PLCdp = dpSubStr(PLCdp, DPSUB_SYS_DP);
  modDP = dpSubStr(modDP, DPSUB_SYS_DP);

  dpGet(modDP + ".DrvNumber", DrvNumber);
  if (dynContains(drvNums,DrvNumber) == 0) {
    if (DEBUG) DebugTN(PLCdp + ": " + modDP+ " driver is not running so ignoring PLC state -- "+
                       "_fwRackConnections_updatePLCStatus");
    plcState=0;
  }
  //-----------------Check if PLC is active
  active = false;
  dpGet (modDP+".Active", active);
  if (!active) {
    if (DEBUG) DebugTN(PLCdp + ": " + modDP+ " is not active -- _fwRackConnections_updatePLCStatus");
    plcState = 0;
  }
  
  fwDevice_getName(PLCdp,plcName,dsExceptions);
  if (DEBUG) DebugTN("plcName = "+plcName,dsExceptions);
  
  if (DEBUG) DebugTN("---> hardware connected "+hardwareConnected);
  if (plcState) {
      if (dynContains(hardwareConnected,PLCdp) == 0) dynAppend(hardwareConnected,PLCdp);     
  } else {
      if (dynContains(hardwareConnected,PLCdp) > 0) dynRemove(hardwareConnected,dynContains(hardwareConnected,PLCdp));
  }
  
  if (plcState != rcaState) {
    dpSetWait (PLCdp + ".Status.PLC", plcState);
    if ((plcState == 1) || (plcState == 3)) {    
      if (DEBUG) DebugTN("Connecting tables for PLC : " + PLCdp + " -- _fwRackConnections_updatePLCStatus"); 
      _fwRackConnections_setAddress(PLCdp,modDP,0,"RackPowerDistribution", true, false);
      _fwRackConnections_setConfigurationStatusAddress(PLCdp,modDP);
      setConfigAddressTime = getCurrentTime();
      dpSetWait(PLCdp + ".Status.PLC",2); // NEW G.M. 2013-7-2 (waiting for tables)
      waiting = true;
      timeout = 0;
      while (waiting) {        
          dpGet(PLCdp + ".RawData.Raw_0:_original.._stime",last); 
          if (timeout % 4 == 0) DebugTN( "looping waiting for config tables ",timeout,period(setConfigAddressTime)-period(last));
          if (period(last) > period(setConfigAddressTime)) {  
              DebugTN("finally!");          
              if (DEBUG) {
                        DebugTN("Last time configuration table updated was : ");
                        DebugTN(last);
                        DebugTN("and address was set at ");
                        DebugTN(setConfigAddressTime);
              }
              waiting = false;          
          }
          delay(0,500);
          timeout++;
          if (timeout > 60) {
              if(DEBUG) DebugTN("Something really not working " );
              dpSetWait(PLCdp + ".Status.PLC",4); // NEW G.M. 2013-7-2 (timeout reading tables)
              return;
          }
      }      
      dpSetWait (PLCdp + ".Status.PLC", plcState); // NEW G.M. 2013-7-2
      //dpSetWait(PLCdp+".Tables.Configuration:_original.._exp_inv", false);
      _fwRackConnections_connectConfigurationTable(PLCdp);
      _fwRackConnections_loadTables(PLCdp);
      fwRackTool_CompareTablesWithLastUsed(PLCdp); // G.M. 2019-05
      
      if (!dynContains(dsEquipmentStatus, dpSubStr(PLCdp,DPSUB_DP))) {
          dynAppend(dsEquipmentStatus,dpSubStr(PLCdp,DPSUB_DP));
          fwDevice_getName(PLCdp,PLC,dsExceptions);
          Pre_fix = "RCA_"+PLC+"_";
          if (dpExists(plcName+"_ConfigurationDpList") && dpExists(Pre_fix+"FWRack_StatusDpe.list")) {
                //DebugTN("----> dp exists: " +plcName+"_ConfigurationDpList. Connecting to equipment" );
                if (DEBUG) DebugTN("Connecting fwRackConnections_readEquipmentStatus for PLC : " + PLCdp + 
                                   " -- _fwRackConnections_updatePLCStatus"); 
              //if(dynContains(dpConnections,PLCdp+".Tables.EquipmentStatus")){
              //  dynAppend(dpConnections, PLCdp+".Tables.EquipmentStatus");
                DebugTN("---> in _updatePLCStatus: CONNECTING to "+PLCdp+".Tables.EquipmentStatus ");
                dpConnect("fwRackConnections_readEquipmentStatus", PLCdp+".Tables.EquipmentStatus");
              //}
          } else {
                if (DEBUG) DebugTN("PLC connected. Reconfigure to connect to equipment status" );
          }
      } else {
          if (DEBUG) DebugTN("----->"+ PLCdp + " was already in dsEquipmentStatus "+dsEquipmentStatus);
      }      
    } else {      
      _fwRackConnections_setAddress(PLCdp,modDP,0,"RackPowerDistribution", false, true);
      dpSetWait(PLCdp+".Tables.Configuration:_original.._exp_inv", true); 
      _fwRackConnections_loadTables(PLCdp, FALSE);
      fwRackTool_CompareTablesWithLastUsed(PLCdp);  // G.M. 2019-05
      if (rcaState != -1) _fwRackConnections_connectConfigurationTable(PLCdp, false);
    }
  }  
  //delay(1); 
}


//=============================================================
void _fwRackConnections_setConfigurationStatusAddress(string plcDP, 
                                                      string modDP,
                                                      string pollingGroup="RackPowerDistribution", 
                                                      bool bActivate=TRUE,
                                                      bool invalid = FALSE,
                                                      int  iMode = DPATTR_ADDR_MODE_OUTPUT)
{
 int DrvNumber;
 int PLCNumber;
 string RawDPE = plcDP + ".Status.ConfigurationNumber";
   
 dpGet(modDP + ".PlcNumber", PLCNumber, modDP + ".DrvNumber", DrvNumber);
 dpSetWait(RawDPE+":_distrib.._type",56,
           RawDPE+":_distrib.._driver", DrvNumber,
	          RawDPE+":_address.._type", 16,
	          RawDPE+":_address.._active", bActivate,
	          RawDPE+":_address.._datatype", 563,
	          RawDPE+":_address.._direction", 16,
	          RawDPE+":_address.._drv_ident", "MODBUS",
           RawDPE+":_address.._internal", FALSE,
	          RawDPE+":_address.._mode", iMode,
	          RawDPE+":_address.._poll_group", "_" + pollingGroup, //"_Mod_Plc_FAST",
	          RawDPE+":_address.._reference", "M."+PLCNumber+".6."+22,
	          RawDPE+":_address.._subindex", 0,
	          RawDPE+":_address.._lowlevel", FALSE,
           RawDPE+":_smooth.._type", 48,
           RawDPE+":_smooth.._std_type", 4);
  
  dpSetWait(RawDPE+":_address.._lowlevel", TRUE);
}

//=============================================================
void fwRackConnections_readEquipmentStatus(string dp, blob status)
{
  dyn_string exc_info;
  
  dp = dpSubStr(dp,DPSUB_DP);
  fwRackTool_ReadEquipmentStatus(dp, exc_info);  
  _fwRackConnections_refreshElectricalLogic(dp, exc_info);
}
 
//=============================================================
synchronized void _fwRackConnections_refreshElectricalLogic (string dp, dyn_string exception)
{
  dyn_string dsChildren,mains;
  int i,j;
  
  //fwDevice_getName(dp,plcname,exception);
  fwDevice_getChildren("RCA",fwDevice_LOGICAL,dsChildren,exception);
  //DebugTN(dsChildren);
  for(i = 1; i <= dynlen(dsChildren); i++) {
    if (dsChildren[i]==dp) {
      //DebugTN("Electrically do this tree: " + dp);
      fwDevice_getChildren(dp,fwDevice_LOGICAL,mains,exception);
      for (j = 1; j <= dynlen(mains); j++) {
          _fwRackConnections_traverseElectricalTree(mains[j],exception,true );
      }
    }       
  }    
}



//=============================================================
void _fwRackConnections_traverseElectricalTree(string dp, dyn_string exception,bool parenthasPower )
{
  dyn_string equipments;
  bool hasPowerCurrent;
  bool hasPower;
  int i;
   
  //DebugTN(dp);
  fwDevice_getChildren(dp,fwDevice_LOGICAL,equipments,exception);      
  dpGet(dpAliasToName(dp) + "HasPower", hasPowerCurrent);
  if (parenthasPower) {    
       hasPower = _fwRackConnection_hasPowerDevice(dp, exception);
       if (hasPower != hasPowerCurrent) dpSetWait(dpAliasToName(dp) + "HasPower", hasPower);
       //DebugTN("----" + dp);
       //DebugTN("Power: " + hasPower);
       for (i=1; i<=dynlen(equipments); i++) _fwRackConnections_traverseElectricalTree(equipments[i], 
                                                                                         exception, 
                                                                                         hasPower);
  } else {
       if (hasPowerCurrent) dpSetWait(dpAliasToName(dp) + "HasPower", false);
       // DebugTN("----" + dp);
       //DebugTN("Power: " + false);
       for (i=1; i<=dynlen(equipments); i++) _fwRackConnections_traverseElectricalTree(equipments[i], 
                                                                                         exception, 
                                                                                         false);
  }
}

//=============================================================
bool _fwRackConnection_hasPowerDevice (string dp, dyn_string exception)
{
  //DebugTN(dp,dpAliasToName(dp));
  dyn_string eventList, eventPos, powerCondition, dpe;
  bool value;
  string hardDp = dpAliasToName(dp);
  string typeName=dpTypeName(hardDp);
  int i;

  //dpGet(typeName + "_EventPos.list",eventPos);  
  dpGet(typeName + "_EventNames.list",eventList);
  dpGet("RCA_EXCEL_affectchild.list",powerCondition);
  dpGet("RCA_EXCEL_dpe.list",dpe);
  
  //n=dynContains(EXCEL_dpe_list,"junk");
  //if (n > 0) on_condition_flag = EXCEL_affectchild_list[n];
  if (typeName == "FwRackDevicePDType_95") {
    ;//DebugTN();
  }
  
  
  for (i = 1; i <= dynlen(eventList); i++) {
    //DebugTN(dpe,eventList[i]);
      if (dynContains(dpe,eventList[i]) >= 1) {
          if (powerCondition[dynContains(dpe,eventList[i])] == "0") {
             //There is a power condition. Property at pos: eventPos[i] should be false
              dpGet(hardDp + eventList[i],value);
              if (value) return false;
          }
          else if (powerCondition[dynContains(dpe,eventList[i])]=="1") {
             //There is a power condition. Property at pos: eventPos[i] should be false
             dpGet(hardDp + eventList[i],value);
             if (!value) return false;
          }  
      } else return false;
    
  }
  return true;
}

//=============================================================
int _fwRackConnections_waitForRaw(string rawDp)
{
  blob bRaw;
  int i;
  
  for (i = 1; i <= 30; i++) {
    dpGet(rawDp,bRaw);
    if (bloblen(bRaw) == 240) return 0;   //DebugTN("Configuration table ready");      
    //DebugTN("Configuration table not ready, waiting...." + i);
    delay(1);
  } 
  return 1;
}

//=============================================================
int _fwRackConnections_waitForConfigurationTable (string dpConfig)
{
 
  blob configTable;
  int i;
  
  for (i = 1; i <= 30; i++) {
    dpGet(dpConfig,configTable);
    if (bloblen(configTable) == 46) {
      //DebugTN("Configuration table ready");
      return 0;
    }
    //DebugTN("Configuration table not ready, waiting...." + i);
    delay(1);
  } 
  return 1;
}

//=============================================================
void _fwRackConnections_loadTables (string dp, bool connect = true)
{
blob configTable;
int iEquipmentStatusTableAddress,iEquipmentStatusTableLength;
int iError;

  if (connect) {
    
    //wait for configuration table to be loaded
    //the rest bellow depends on the configuration table
    iError = _fwRackConnections_waitForConfigurationTable(dp+".Tables.Configuration");    
    if (iError != 0) {
      DebugTN("CONFIGURATION TABLE NOT LOADED. ABORTING DP CONNECTIONS");
      return; 
    }
     
    dpGet(dp + ".Tables.Configuration", configTable);
    
    blobGetValue(configTable,14*2,iEquipmentStatusTableAddress,2,1); 
    blobGetValue(configTable,15*2,iEquipmentStatusTableLength, 2,1);  
    dpSetWait(dp + ".TableAddresses.EquipmentStatus",        iEquipmentStatusTableAddress,
              dp + ".TableAddresses.EquipmentStatusLength",  iEquipmentStatusTableLength );            
    
    _fwRackConnections_loadIdentificationTable(dp, configTable);
    _fwRackConnections_loadEquipmentDescriptionTable(dp, configTable);
    _fwRackConnections_loadEquipmentConfigurationTable(dp, configTable);
    _fwRackConnections_loadEquipmentStatusTable(dp, configTable);
    _fwRackConnections_loadCommandDescriptionTable(dp, configTable);
    _fwRackConnections_connectEquipmentStatusTable(dp);
    _fwRackConnections_loadCommandReceiveTable(dp, configTable);    
    _fwRackConnections_connectCommandReceiveTable(dp);    
    _fwRackConnections_loadCommandResponseTable(dp, configTable);    
    _fwRackConnections_connectCommandResponseTable(dp);       
    _fwRackCommand_configureOutput (dp, configTable);   
    delay(1);    
  } else {
    _fwRackConnections_connectEquipmentStatusTable(dp,FALSE);        
    _fwRackConnections_connectCommandReceiveTable(dp,FALSE);    
    _fwRackConnections_connectCommandResponseTable(dp,FALSE);       
  }
}

//=============================================================
void _fwRackConnections_connectDeviceCommands(string dp)
{
  if(DEBUG) DebugTN("Connecting to commands to racks commands -- "+
                    "fwRackConnections/_fwRackConnections_connectDeviceCommands");
  
  dpQueryConnectSingle("_fwRackConnections_sendCommand", FALSE, dp,
                       "SELECT '_online.._value' FROM '{*.Command}' WHERE _DPT LIKE \"FwRackDevicePDType*\"" );     
  
}

//=============================================================
void _fwRackConnections_sendCommand (string ident, dyn_dyn_anytype tab)
{
  int i;
  string dpe;
  string Command;
  string CommandDPE;
  string  ObjectDP;
  string  PlcName,PlcNameDp, User, Plc, Object, ErrorMask, Response;
  blob    Identification, bCommand,Mask;
  dyn_string   CommandNames, dsExceptions;
  
  if (DEBUG) DebugTN("_fwRackConnections_sendCommand",ident, dynlen(tab));
  for(i=2;i<=dynlen(tab);i++) {
     dpe = tab[i][1];
     Command = tab[i][2];

     CommandDPE = dpSubStr(dpe, DPSUB_DP_EL);
     ObjectDP   = dpSubStr(CommandDPE, DPSUB_DP);
     if (substr(CommandDPE,0,6) == "dummy_") continue;
  
  
     //I need to PLC name to find the right queue
     if (DEBUG) DebugTN("In _fwRackConnections_sendCommand : dpe = "+dpe+"    Command = "+Command);
     if (DEBUG) DebugTN("CommandDPE = "+CommandDPE+"  ObjectDP = "+ObjectDP);
  
     fwDevice_getParent(ObjectDP,PlcNameDp,dsExceptions);
     fwDevice_getName(PlcNameDp,PlcName,dsExceptions);  
     ////To send the commands we need to use the PLC NAME and not the device name
  
     if (PlcNameDp == "") {
         if(DEBUG) DebugTN("Wrong plc dp name");
         continue;
     }
     
     //----- G.M. 2019-05 look for ConfigurationTablesMismatch. In this case, do not treat command
     bool ConfigurationTablesMismatch;
     dpGet(PlcNameDp+".Status.ConfigurationTablesMismatch",ConfigurationTablesMismatch);
     if (ConfigurationTablesMismatch) {
       DebugTN(PlcNameDp,ObjectDP,"Commands Inhibited due to Configuration Mismatch");
       continue;
     }
     //----- END G.M. 2019
  
     dpGet(ObjectDP + ".NameInPlc", Object);
  
     //We should take the command together with the Session Number 
     dpGet(PlcNameDp +  ".Tables.Identification",      Identification,
	       	 PlcNameDp +  ".User",                       User);

     Plc = BlobToString(Identification, 0, 7);    
  
     if (Command == "W_MASK") dpGet(ObjectDP + ".Mask.wMask", Mask);		    
  
     //62 is the size of the command table. could be retrieved from configuration table but shouldnt change!
     blob busy = "FFFF";
     blobZero(bCommand, 64);
     blobSetValue(bCommand, 	2, 	User,			16,	TRUE);
     blobSetValue(bCommand, 	18,	Plc, 			16,	TRUE);
     blobSetValue(bCommand, 	34, Object,		16,	TRUE);
     blobSetValue(bCommand, 	50, Command,  8, 	TRUE);
     blobSetValue(bCommand, 	58, 0,		    2, 	TRUE);
     blobSetValue(bCommand, 	60, Mask, 		2, 	TRUE);
     blobSetValue(bCommand, 	62, busy,   	2, 	TRUE);	    

//------- O.H. 10-2014  command queue is now stored in a dpe (and handled through a thread)
     //add command to queue if it is not already there
     dyn_blob PlcQueue;
     dpGet(PlcNameDp + ".Output.Queue", PlcQueue);
     if (!dynContains(PlcQueue, bCommand)) {
       //sync queue interactions
       _fwRackConnectionsHandleQueue(PlcName,PlcNameDp, bCommand , "append");                
     }        
  }
}

//=============================================================
synchronized void _fwRackConnectionsHandleQueue (string PlcName,string PlcNameDp, blob bCommand, string todo)
{
  dyn_blob currentQueue;
  dpGet(PlcNameDp + ".Output.Queue", currentQueue); // O.H. 10-2014
  
  if (todo=="append") dynAppend(currentQueue,bCommand);        
  else if (todo =="remove") dynRemove( currentQueue, dynContains(currentQueue,bCommand)); 
  
  dpSetWait(PlcNameDp + ".Output.Queue", currentQueue);
}

//=============================================================
void _fwRackConnections_connectCommandResponseTable(string dp, bool connect = TRUE)
{
int  iTableAddress=40,iTableLength=32;
  ////dpGet(dp + ".TableAddresses.EquipmentStatus",        iEquipmentStatusTableAddress,
  ////      dp + ".TableAddresses.EquipmentStatusLength",  iEquipmentStatusTableLength );
int i, first, last;

   _fwRackConnections_getRawLimits( iTableAddress,  iTableLength, first,last);
  
   for (i = first; i <= last; i++) {
       if (connect) {
           if (dynContains(dsCommandPlcs,dp + ".RawData.Raw_" + i) > 0) return;
           dynAppend(dsCommandPlcs,dp + ".RawData.Raw_" + i) ; 
           if(DEBUG) DebugTN("Connecting _fwRackConnections_commandRawChanged for PLC : " + 
                             dp + ".RawData.Raw_" + i + 
                             " -- _fwRackConnections_connectCommandResponseTable");          
           dpConnect("_fwRackConnections_commandRawChanged", dp + ".RawData.Raw_" + i);
       
       } else {
           if ((dynContains(dsCommandPlcs,dp + ".RawData.Raw_" + i) == 0)) return;
           dynRemove (dsCommandPlcs, dynContains(dsCommandPlcs,dp + ".RawData.Raw_" + i));
           if(DEBUG) DebugTN("Disconnecting _fwRackConnections_commandRawChanged for PLC : " + 
                             dp + ".RawData.Raw_" + i + 
                             " -- _fwRackConnections_connectCommandResponseTable");          
           dpDisconnect("_fwRackConnections_commandRawChanged",  dp + ".RawData.Raw_" + i);
       }
   }
}

//=============================================================
void _fwRackConnections_connectCommandReceiveTable(string dp, bool connect=TRUE)
{
  int  iTableAddress=80,iTableLength=3;
  ////dpGet(dp + ".TableAddresses.EquipmentStatus",        iEquipmentStatusTableAddress,
  ////      dp + ".TableAddresses.EquipmentStatusLength",  iEquipmentStatusTableLength );
   int first, last;
   
   _fwRackConnections_getRawLimits( iTableAddress,  iTableLength, first,last);
  
   for (int i=first; i<=last;i++) {
       if (connect) {
           if (dynContains(dsCommandResponsePlcs,dp + ".RawData.Raw_" + i) > 0) return;
           dynAppend(dsCommandResponsePlcs,dp + ".RawData.Raw_" + i)  ;      
           dpConnect("_fwRackConnections_commandResponseRawChanged", dp + ".RawData.Raw_" + i);
       } else {
           if ((dynContains(dsCommandResponsePlcs,dp + ".RawData.Raw_" + i) == 0)) return;
           dynRemove (dsCommandResponsePlcs, dynContains(dsCommandResponsePlcs,dp + ".RawData.Raw_" + i));       
           dpDisconnect("_fwRackConnections_commandResponseRawChanged", dp + ".RawData.Raw_" + i);
       }     
    }

//------ O.H. 10-2014  start the thread to handle command queue
   if(connect)
      g_iThreadIds[dp] = startThread("_fwRackCommand_handleCommandQueue", dp);
    else
      stopThread(g_iThreadIds[dp]);

//    dpConnect("_fwRackCommand_commandReceived",FALSE, dp + ".Tables.CommandReceive");
//    else dpDisconnect("_fwRackCommand_commandReceived", dp + ".Tables.CommandReceive");    
}
  
  
//=============================================================
void _fwRackConnections_connectEquipmentStatusTable (string dp, bool connect = TRUE)
{
  /*dpQueryConnectSingle("_fwRackConnections_rawsChanged", 
                       TRUE, 
                       "MonitorRAWdata",
                       "SELECT '.RawData' FROM '*' WHERE _DPT = \""+ g_fwRACK_PLC_TYPE +"\"" );*/
int  iEquipmentStatusTableAddress,iEquipmentStatusTableLength;
int  i, first, last;
  
  dpGet(dp + ".TableAddresses.EquipmentStatus",        iEquipmentStatusTableAddress,
        dp + ".TableAddresses.EquipmentStatusLength",  iEquipmentStatusTableLength );
   _fwRackConnections_getRawLimits(iEquipmentStatusTableAddress, iEquipmentStatusTableLength, first, last);
  
   for (i = first; i <= last; i++) {
       if (connect) {
             if (dynContains(dsStatusRawPlcs,dp + ".RawData.Raw_" + i) > 0) return;
             dynAppend(dsStatusRawPlcs,dp + ".RawData.Raw_" + i)  ;
             if(DEBUG) DebugTN("Connecting to raw: " +  dp + ".RawData.Raw_" + i + 
                               " -- _fwRackConnections_connectEquipmentStatusTable");             
             dpConnect("_fwRackConnections_statusRawChanged", dp + ".RawData.Raw_" + i);
       } else {
             if ((dynContains(dsStatusRawPlcs,dp + ".RawData.Raw_" + i) == 0)) return;
             dynRemove (dsStatusRawPlcs, dynContains(dsStatusRawPlcs,dp + ".RawData.Raw_" + i));
             if(DEBUG) DebugTN("Disconnecting to raw: " +  dp + ".RawData.Raw_" + i + 
                               " -- _fwRackConnections_connectEquipmentStatusTable");           
             dpDisconnect("_fwRackConnections_statusRawChanged", dp + ".RawData.Raw_" + i);  
       }    
   }
}


//=============================================================
void _fwRackConnections_commandResponseRawChanged(string dp, blob raw)
{
int first, last, iTableAddress=80, iTableLength=3;
blob blStatusTable, blOldStatusTable ;
dyn_string exc_info;
  
  dp = dpSubStr(dp,DPSUB_DP);
  //dpGet(dp + ".TableAddresses.EquipmentStatus",        iEquipmentStatusTableAddress,
  //      dp + ".TableAddresses.EquipmentStatusLength",  iEquipmentStatusTableLength );
 
  _fwRackConnections_getRawLimits( iTableAddress, iTableLength, first,last);
  _fwRackConnection_getTableFromRaws (iTableAddress, iTableLength, first, last,dp, blStatusTable);
  dpGet(dp + ".Tables.CommandResponse", blOldStatusTable);
  if (blOldStatusTable != blStatusTable) {
    dpSetWait(dp + ".Tables.CommandResponse", blStatusTable);
    if (DEBUG)  DebugTN("Updating command response table: " + dp + 
                        " -- fwRackConnections.ctl / _fwRackConnections_commandResponseRawChanged" );
    CommandResponseTable(dp,blStatusTable);
  }
}       

//=============================================================
//  callback function, called when one of the RawData.Raw_x dpes of a PowerDistributionPLC changes
//  the value is not directly used
void _fwRackConnections_commandRawChanged(string dp, blob raw)
{
int first, last, iTableAddress=40, iTableLength=32;
blob blStatusTable,blOldCommandTable ;
dyn_string exc_info;
  
  dp = dpSubStr(dp,DPSUB_DP);
  //dpGet(dp + ".TableAddresses.EquipmentStatus",        iEquipmentStatusTableAddress,
  //      dp + ".TableAddresses.EquipmentStatusLength",  iEquipmentStatusTableLength );
 
  _fwRackConnections_getRawLimits(iTableAddress,  iTableLength, first, last);
  _fwRackConnection_getTableFromRaws (iTableAddress, iTableLength, first, last,dp, blStatusTable);
  
  dpGet(dp + ".Tables.CommandReceive", blOldCommandTable);
  if (blOldCommandTable != blStatusTable) dpSetWait(dp + ".Tables.CommandReceive", blStatusTable);
  //DebugTN("-------------Updating command table: " + dp);
  //fwRackTool_ReadEquipmentStatus(dp, exc_info);
}       
    
//=============================================================
void _fwRackConnections_statusRawChanged (string dp, blob raw)
{
int first, last,iEquipmentStatusTableAddress,iEquipmentStatusTableLength;
blob blEquipmentStatusTable, blActualEquipmentStatusTable ;
dyn_string exc_info;

  dp = dpSubStr(dp,DPSUB_DP);
  dpGet(dp + ".TableAddresses.EquipmentStatus",        iEquipmentStatusTableAddress,
        dp + ".TableAddresses.EquipmentStatusLength",  iEquipmentStatusTableLength );
 
  _fwRackConnections_getRawLimits( iEquipmentStatusTableAddress,  iEquipmentStatusTableLength, first,last);
  _fwRackConnection_getTableFromRaws (iEquipmentStatusTableAddress, iEquipmentStatusTableLength, 
                                      first, last,dp, blEquipmentStatusTable);
  
  dpGet(dp + ".Tables.EquipmentStatus", blActualEquipmentStatusTable);
  if (blActualEquipmentStatusTable != blEquipmentStatusTable) {
      dpSetWait(dp + ".Tables.EquipmentStatus", blEquipmentStatusTable);
      //DebugTN("-------------Updating status table: " + dp);
  }
  //fwRackTool_ReadEquipmentStatus(dp, exc_info);
}     



//=============================================================
void _fwRackConnections_loadCommandResponseTable (string dp, blob configTable)
{
blob blTable;
int iTableAddress, iTableLength, first, last;

  blobGetValue(configTable,6*2,iTableAddress,2,1); 
  blobGetValue(configTable,7*2,iTableLength, 2,1); 
   _fwRackConnections_getSingleTableReading(iTableAddress, iTableLength, first, last,dp);    
  //if(DEBUG==true){
  //  DebugTN("------------>Command response table");    
  //  DebugTN("Address: " + iTableAddress);
  //  DebugTN("Length: "  + iTableLength);
  //} 
  _fwRackConnection_getTableFromRaws (iTableAddress, iTableLength, first, last,dp, blTable);
  dpSetWait(dp + ".Tables.CommandResponse", blTable);
}

//=============================================================
void _fwRackConnections_loadCommandReceiveTable(string dp, blob configTable)
{
blob blTable;
int iTableAddress, iTableLength, first, last;
  
  blobGetValue(configTable,4*2,iTableAddress,2,1); 
  blobGetValue(configTable,5*2,iTableLength, 2,1); 
   _fwRackConnections_getSingleTableReading(iTableAddress, iTableLength, first, last,dp);    
  //if(DEBUG==true){
  //  DebugTN("------------->Command receive table");    
  //  DebugTN("Address: " + iTableAddress);
  //  DebugTN("Length: "  + iTableLength);
  //} 
  _fwRackConnection_getTableFromRaws (iTableAddress, iTableLength, first, last,dp, blTable);
  dpSetWait(dp + ".Tables.CommandReceive", blTable);
}

//=============================================================
void _fwRackConnections_loadCommandDescriptionTable(string dp, blob configTable)
{
blob blTable;
int iTableAddress, iTableLength, first, last;
  
  blobGetValue(configTable,18*2,iTableAddress,2,1); 
  blobGetValue(configTable,19*2,iTableLength, 2,1); 
   _fwRackConnections_getSingleTableReading(iTableAddress, iTableLength, first, last,dp);    
  //if(DEBUG==true){
  //  DebugTN("------------>Command decription table");    
  //  DebugTN("Address: " + iTableAddress);
  //  DebugTN("Length: "  + iTableLength);
  //} 
  _fwRackConnection_getTableFromRaws (iTableAddress, iTableLength, first, last,dp, blTable);
  dpSetWait(dp + ".Tables.CommandDescription", blTable);
}


//=============================================================
void _fwRackConnections_loadEquipmentStatusTable(string dp, blob configTable)
{
//DebugTN(configTable);
blob blEquipmentStatusTable, actualEquipmentStatusTable;
int iEquipmentStatusTableAddress, iEquipmentStatusTableLength, first, last;
  
  blobGetValue(configTable,14*2,iEquipmentStatusTableAddress,2,1); 
  blobGetValue(configTable,15*2,iEquipmentStatusTableLength, 2,1);     
  _fwRackConnections_getSingleTableReading(iEquipmentStatusTableAddress, 
                                           iEquipmentStatusTableLength, first, last,dp);    
  //if(DEBUG==true){
  //  DebugTN("-------------->Equipment Status table");    
  //  DebugTN("Address: " + iEquipmentStatusTableAddress);
  //  DebugTN("Length: "  + iEquipmentStatusTableLength);
  //}
  _fwRackConnection_getTableFromRaws (iEquipmentStatusTableAddress, iEquipmentStatusTableLength, 
                                      first, last,dp, blEquipmentStatusTable);
  dpGet(dp + ".Tables.EquipmentStatus", actualEquipmentStatusTable);
  
  if (actualEquipmentStatusTable != blEquipmentStatusTable)
       dpSetWait(dp + ".Tables.EquipmentStatus", blEquipmentStatusTable);
}

//=============================================================
void _fwRackConnections_loadEquipmentDescriptionTable (string dp, blob configTable)
{
  //DebugTN(configTable);
blob blEquipmentDescriptionTable;
int iEquipmentDescriptionTableAddress, iEquipmentDescriptionTableLength, first, last;

  blobGetValue(configTable,16*2,iEquipmentDescriptionTableAddress,2,1); 
  blobGetValue(configTable,17*2,iEquipmentDescriptionTableLength, 2,1); 
   
  _fwRackConnections_getSingleTableReading(iEquipmentDescriptionTableAddress, 
                                           iEquipmentDescriptionTableLength, 
                                           first, last,dp);    
  //if(DEBUG==true){
  //  DebugTN("---------------->Equipment Description table");    
  //  DebugTN("Address: " + iEquipmentDescriptionTableAddress);
  //  DebugTN("Length: "  + iEquipmentDescriptionTableLength);
  //} 
  //delay(5);
  _fwRackConnection_getTableFromRaws(iEquipmentDescriptionTableAddress, iEquipmentDescriptionTableLength, 
                                     first, last,dp, blEquipmentDescriptionTable);
  dpSetWait(dp + ".Tables.EquipmentDescription", blEquipmentDescriptionTable);
}

//=============================================================
void _fwRackConnections_loadEquipmentConfigurationTable (string dp, blob configTable)
{  
blob blTable;
int iTableAddress, iTableLength, first, last;
  blobGetValue(configTable,20*2,iTableAddress,2,1); 
  blobGetValue(configTable,21*2,iTableLength, 2,1); 
   _fwRackConnections_getSingleTableReading(iTableAddress, iTableLength, first, last,dp);    
  //if(DEBUG==true){
  //  DebugTN("------------->Equipment Configuration table");    
  //  DebugTN("Address: " + iTableAddress);
  //  DebugTN("Length: "  + iTableLength);
  //} 
  _fwRackConnection_getTableFromRaws (iTableAddress, iTableLength, first, last,dp, blTable);
  dpSetWait(dp + ".Tables.EquipmentConfiguration", blTable);
}

//=============================================================
void _fwRackConnections_loadIdentificationTable(string dp, blob configTable)
{
  //DebugTN(configTable);
blob blIdentificationTable;
int iIdentificationTableAddress, iIdentificationTableLength, first, last;  
    
  blobGetValue(configTable,2*2,iIdentificationTableAddress,2,1); 
  blobGetValue(configTable,3*2,iIdentificationTableLength,2,1);     
  _fwRackConnections_getSingleTableReading(iIdentificationTableAddress, 
                                           iIdentificationTableLength, first, last,dp);    
  //if(DEBUG==true){
  //  DebugTN("---------------------->Identification table");    
  //  DebugTN("Address: " + iIdentificationTableAddress);
  //  DebugTN("Length: "  + iIdentificationTableLength);
  //}
  _fwRackConnection_getTableFromRaws(iIdentificationTableAddress, 
                                     iIdentificationTableLength,
                                     first, last,dp, blIdentificationTable);
  dpSetWait(dp + ".Tables.Identification", blIdentificationTable);
}


//=============================================================
void _fwRackConnection_getTableFromRaws (int iAddress, int Length, int first, int last, string dp, blob &table)
{
int i, iError, subTableAddress;
blob blRaw, blSubTable;
  
  for (i = first; i <= last; i++) {
    iError = _fwRackConnections_waitForRaw(dp + ".RawData.Raw_" +i);
    if (iError != 0) {
       DebugTN("Error, raw datapoint " + i+ " not updating from PLC"); 
       return;
    }
    
    dpGet(dp + ".RawData.Raw_" + i ,blRaw);
    
    if (bloblen(blRaw) > 0) blobAppendValue(blSubTable,blRaw, bloblen(blRaw), TRUE); 
    else {
      DebugTN("Bad configuration table:");
      if (DEBUG) {
        DebugTN(blRaw, bloblen(blRaw));
        DebugTN("Raw = " + i +  " First = " + first + " Last = " + last);      
      }
      exit;
    }
  }
  
  subTableAddress = iAddress - first*120;  
  if (bloblen(blSubTable) >= (subTableAddress *2 + Length*2)  && Length > 0) {
                  blobGetValue(blSubTable, subTableAddress *2, table, Length*2);
  } else DebugTN("ERROR in table lenght: " + first + " " + last);

}


//=============================================================

/*void _fwRackConnections_connectTableToRaws(int iAddress, int Length, int first, int last, 
                                             string dp, blob table)
{    
  for(int i =first; i <= last ; i++){
   //if(DEBUG == TRUE)
   //  DebugTN("++++++++++ DPCONNECT" + "_fwRackConnections_rawStatusChanged", dp + ".RawData.Raw_" +i);
    if(!dynContains(dpConnections,dp + ".RawData.Raw_" +i)) {
     dpConnect("_fwRackConnections_rawStatusChanged", dp + ".RawData.Raw_" +i);
     dynAppend(dpConnections,dp + ".RawData.Raw_" +i);
   }
  }  
}

//=============================================================
void _fwRackConnections_rawStatusChanged (string rawDP, blob value)
{
  
}*/

//=============================================================
void _fwRackConnections_getSingleTableReading(int iAddress, int Length, int &first, int &last, string dp)
{
int i;
  
  _fwRackConnections_getRawLimits(iAddress, Length, first, last);
  for (i = first; i <= last ; i++) {
    //if(DEBUG)  DebugTN("%%% Activating raw %%% " + i);
    _fwRackConnections_getRawReading (dp,i);
  }
}

//=============================================================
void _fwRackConnections_getRawReading (string dpPLC, int iRaw)
{
  
string sSystemName, dpPLCNoSystemName;

  dpPLCNoSystemName = dpSubStr(dpPLC,DPSUB_DP);
  sSystemName = dpSubStr(dpPLC,DPSUB_SYS);
  
  if (iRaw != 0) _fwRackConnections_setAddress (dpPLC, 
                                                fwInstallationRedu_getReduDp(sSystemName+"_"+dpPLCNoSystemName+"_Modbus"), 
                                                iRaw, 
                                                "Rack_"+dpPLCNoSystemName , 
                                                TRUE,
                                                FALSE);
}


//=============================================================
void _fwRackConnections_getRawLimits(int Address, int Length, int &first, int &last)
{
  //Calculate first  
  first = Address/120;
  last = (Address + Length - 1)/120;
}

//================================================================
void _fwRackConnections_connectConfigurationTable (string dp, bool connect = true)
{

  if (connect) {
     if (DEBUG) DebugTN("Connecting to updateConfigTable and Number : " + dp +
                        " -- _fwRackConnections_connectConfigurationTable");
     if (!dynContains(dpConnections,dp + ".RawData.Raw_0")) {
       dynAppend(dpConnections,dp + ".RawData.Raw_0");
       dpConnect("_fwRackConnections_updateConfigTable",dp + ".RawData.Raw_0");
     }
     if (!dynContains(dpConnections,dp + ".Tables.Configuration")) {
       dynAppend(dpConnections,dp + ".Tables.Configuration");
       dpConnect("_fwRackConnections_updateConfigurationNumber",dp + ".Tables.Configuration");
     }   
  } else {
     if(DEBUG)DebugTN("Disconnecting to updateConfigTable and Number : " + dp +
                      " -- _fwRackConnections_connectConfigurationTable");
     if (dynContains(dpConnections,dp + ".RawData.Raw_0")) {
       dynRemove(dpConnections, dynContains(dpConnections,dp + ".RawData.Raw_0"));
       dpDisconnect("_fwRackConnections_updateConfigTable", dp + ".RawData.Raw_0");
     }
     if (dynContains(dpConnections,dp + ".Tables.Configuration")) {
       dynRemove(dpConnections, dynContains(dpConnections,dp + ".Tables.Configuration"));
       dpDisconnect("_fwRackConnections_updateConfigurationNumber", dp + ".Tables.Configuration");
     }
  }
}


//=============================================================
void _fwRackConnections_updateConfigurationNumber (string dpConfigTable, blob ConfigurationTable)
{
int ConfigurationNumber;
string PLCdp = dpSubStr(dpConfigTable, DPSUB_SYS_DP);
  
  if (bloblen(ConfigurationTable) >= 44) blobGetValue(ConfigurationTable,22*2,ConfigurationNumber,2,1);
  dpSetWait(PLCdp + ".Status.ConfigurationNumber", ConfigurationNumber);
}


//=============================================================
void _fwRackConnections_updateConfigTable (string dpRaw0, blob blRaw0)
{
  //Configuration table is always in the same address %MW0-22
  //This address is of course always in Raw_0
  
bool isInvalid;
string PLCdp = dpSubStr(dpRaw0, DPSUB_SYS_DP);
  
  dpGet(PLCdp + ".RawData.Raw_0:_original.._exp_inv", isInvalid);
  if (isInvalid) dpSetWait(PLCdp+".Tables.Configuration:_original.._exp_inv", isInvalid);
  else {
    if (bloblen(blRaw0) >= 22){
      _fwRackConnections_writeTable(0,23,PLCdp+".Tables.Configuration", blRaw0);
    }
    fwRackTool_CompareConfigurationTable(PLCdp);
  }
}

//=============================================================
/*
This function gets the actual table content that has been extracted from Raw_All.
This content is compared with the table content stored in the table DPE. Since blob variables
are not comparable, a transformation into string is required. If the table content has changed,
the DPE is updated.
*/
void _fwRackConnections_writeTable(int TablePosition, int TableLength, string TableDPE, 
                                   blob &Raw_All, bool forceUpdate=false)
{
blob 	TableContent, 	OldTableContent;
string 	sTableContent, 	sOldTableContent;
	
	TableDPE = dpSubStr(TableDPE, DPSUB_DP_EL);
	TablePosition 	= 	TablePosition 	* 2;
	TableLength 	=	TableLength 	* 2;	
	blobGetValue(Raw_All, TablePosition, TableContent, TableLength, TRUE);	
	dpGet(TableDPE, OldTableContent);	
	sTableContent = TableContent;
	sOldTableContent = OldTableContent;	
	if ((sOldTableContent == sTableContent) && !forceUpdate) return;	
 
 if(DEBUG) DebugTN("Writing the table for  : " + TableDPE + 
                   " -- _fwRackConnections_writeTable");    
	dpSetWait(TableDPE, TableContent);
}
 
//=============================================================
void _fwRackConnections_insertPLCInConfig(string modDP,int ModbusNumber)
{
dyn_string  values;
bool needsRestart;
  
  paCfgReadValueList(getPath(CONFIG_REL_PATH, "config"), "mod_" + ModbusNumber,"plc",values); 
  if (dynContains(values, modDP) == 0) {
      paCfgInsertValue(getPath(CONFIG_REL_PATH, "config"),"mod_" + ModbusNumber,"plc",modDP);
      needsRestart = TRUE;
  }    
  paCfgReadValueList( getPath(CONFIG_REL_PATH, "config"), "mod_" + ModbusNumber, "aliveTimeout", values); 
  if (dynlen(values) < 1) {
      paCfgInsertValue(getPath(CONFIG_REL_PATH, "config"), "mod_" + ModbusNumber, "aliveTimeout", 10);
      needsRestart = TRUE;    
  }
  if (needsRestart) {
        if(DEBUG)   DebugTN("Restarting modbus driver: "+ModbusNumber+
                            " -- fwRackConnections.ctl/_fwRackConnections_insertPLCInConfig");   
        _fwRackConnections_restartModbusDriver(ModbusNumber);
  }
}

//=============================================================
void _fwRackConnections_restartModbusDriver (int ModbusNumber)
{
string         host;
int            port;
int            i, opcIdx;
string         command = "##MGRLIST:LIST";
string         command3, command4;
dyn_dyn_string queryResult;
bool querySuccess, querySuccess3, querySuccess4; 

  i = paGetProjHostPort(PROJ,host, port);
  querySuccess = pmon_query(command, host, port, queryResult, 1, 1);
  
  //DebugTN(queryResult,querySuccess, command, host, port);  
  for (i = 1; i <= dynlen(queryResult); i++) {
//    if ((queryResult[i][1] == "PVSS00mod") || (queryResult[i][1] == "WCCOAmod")) {           
    if (queryResult[i][1] == "WCCOAmod") {
      if (queryResult[i][6] == "-num " +  ModbusNumber) {
          opcIdx = i-1;
          command3 = "##SINGLE_MGR:STOP " + opcIdx;
          //DebugTN(command3, host , port, 0, 1);
          querySuccess3 = pmon_command(command3, host , port, 0, 1);
          delay(3);
          command4 = "##SINGLE_MGR:START " + opcIdx;
          querySuccess4 = pmon_command(command4, host , port, 0, 1);
      }
    }
  }  
}



