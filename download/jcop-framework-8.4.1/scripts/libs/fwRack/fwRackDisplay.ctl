//===========================================================
//          HISTORY OF MODIFICATIONS
//
//          ORIGINAL AUTHOR: Bobby
//
//          15-3-2013 G.M. cosmetic changes. Is this library used ?
  //
//          2016-07-25 saved as 3.14 G.M.
//

#uses "fwRack/fwRackDeprecated.ctl"
#uses "fwConfigs/fwAlertConfig.ctl"

//==========================================================================
bool isAlertConfiguredForProperty(string propertyAddress) 
{
	int iConfigType;
	dpGet(dpSubStr(propertyAddress,DPSUB_SYS_DP_EL) + ":_alert_hdl.._type", iConfigType);
	return (iConfigType != 0);
}

//==========================================================================
string textForProperty(string propertyAddress,string alertStatus="", anytype value="")
{
	string text;
 
	if (alertStatus=="") dpGet(dpSubStr(propertyAddress,DPSUB_SYS_DP_EL) + ":_alert_hdl.._act_state_text",alertStatus);
	text=alertStatus;
 
	if (value == "") dpGet(propertyAddress,value);
	int iType = dpElementType (propertyAddress);		
	if (iType == DPEL_INT || iType == DPEL_FLOAT) {	
		     string str = value;		
		     dyn_string dsStrLen = strsplit (str,".");
		     int iLenVal = strlen(dsStrLen[1]);
	     	int iLenDec = 0;
		     if (dynlen(dsStrLen) > 1) iLenDec = strlen(dsStrLen[2]);		
		     int iLen = iLenVal;
		     //DebugTN($sPropertyName +" " +iLenVal +" " + iLenDec +" " + dsStrLen);
		     if (iLenDec) {
			            iLen = iLen + iLenVal +1;
		            	iLenDec =1;
		     }
		     text+= " [" +strformat ("\\left{%"+ iLen +"."+iLenDec +"}",iLen-iLenDec,value)  + "]";
	}
	return text;
}
