/**@file

This library provides functions for opening the fwWebBrowser: a web browser based
on the WinCCOA's (and Qt's) built-in webview widget. This browser has basic
navigation functionality and features tabbed view.
*/


/**
    Opens a new web browser as a child panel, with a specified URL

    @param sLink specifies the URL to open
    @param sLabel (optional) allows to define the name of the tab in which the URL is open
    @param sWindowName (optional) allows to define the window name (it will appear in the task bar)
    @param bNavigationButton (optional) if true, navigation buttons will be shown
    @param bUrlBarVisibility (optional) if true, URL bar will be displayed
    @param bAddressEditable (optional) if true, the address in URL bar may be edited
    @param bNewTabButton (optional) if true, a button to open a new tab will be shown
    @param sDefaultPage (optional) declares the default (home) page
*/
void fwViewer_showStandaloneWindow(string sLink, dyn_string &exceptionInfo,
                                  string sLabel = "",
                                  string sWindowName = "",
                                  bool bNavigationButton = 1,
                                  bool bUrlBarVisibility = 1,
                                  bool bAdressEditable = 1,
                                  bool bNewTabButton = 1,
                                  string sDefaultPage = "http://home.cern",
                                  bool bOpenInNewModule =1)
{
  int x, y;
  bool bExternalBrowser = 1;
  if (sLink == "") {
    fwException_raise(exceptionInfo,"WARNING","Empty link specified for fwWebBrowser.","");
    fwExceptionHandling_display(exceptionInfo);
    return;
  }
  //default window name: WebBrowser_"cryptoHash"
  if (sWindowName == "") sWindowName = cryptoHash(sLink);
  sWindowName = "WebBrowser_" + sWindowName;
  string FileName = "fwViewer/fwViewer_MainWindow.pnl";

  if(bOpenInNewModule) {
    //open browser in new module
    ModuleOnWithPanel("fwWebBrowser", 0, 0, 1000, 700, 0, 1, "Scale", FileName,  sWindowName, makeDynString("$bExternalBrowser:"+ bExternalBrowser));
    shape sPanelShape = getShape("fwWebBrowser"+"."+sWindowName+":");
    sPanelShape.setParentWindowName(sWindowName);
    fwViewer_initFunction(sPanelShape,bNewTabButton,bNavigationButton,bAdressEditable,bUrlBarVisibility, sDefaultPage);
    sPanelShape.openLink(sLink, exceptionInfo, sLabel);
    setPanelSize("fwWebBrowser",sWindowName,0,1000,700); // force a refresh of content; Qt WebView bug?
    moduleRestore("fwWebBrowser");
    moduleRaise("fwWebBrowser");
    return;
  }
   if(isPanelOpen(sWindowName)) {
      shape sPanelShape = getShape(myModuleName()+"."+sWindowName+":");
      fwViewer_initFunction(sPanelShape,bNewTabButton,bNavigationButton,bAdressEditable,bUrlBarVisibility, sDefaultPage);
      sPanelShape.openLink(sLink, exceptionInfo, sLabel);
      panelSize(sWindowName, x, y,1);
      setPanelSize(myModuleName(),sWindowName,0,x,y);
      restorePanel(sWindowName);
    } else {
     ChildPanelOnCentral(FileName,sWindowName, makeDynString("$bExternalBrowser:"+ bExternalBrowser));
      //wait until panel will be open
      int waitTime=0;
      while(isPanelOpen(sWindowName) == 0){
          waitTime+=1;
          delay(0,100);
          if (waitTime>50) {
              fwException_raise(exceptionInfo,"WARNING", "Browser window "+sWindowName+" did not open","");
              return;
          }
      }
      shape sPanelShape = getShape(myModuleName()+"."+sWindowName+":");
      sPanelShape.setParentWindowName(sWindowName);
      fwViewer_initFunction(sPanelShape,bNewTabButton,bNavigationButton,bAdressEditable,bUrlBarVisibility, sDefaultPage);
      sPanelShape.openLink(sLink, exceptionInfo, sLabel);
    }


}

/**
    Initializes the fwWebBrowser shape. Internal.
*/
void fwViewer_initFunction(shape sPanel, bool bNewTabButton, bool bNavigationButton, bool bAdressEditable, bool bUrlBarVisibility, string sDefaultPage)
{
  sPanel.newTabButtonEnable(bNewTabButton);
  sPanel.navigationButtonsVisible(bNavigationButton);
  sPanel.adressEditable(bAdressEditable);
  sPanel.urlBarVisible(bUrlBarVisibility);
  sPanel.setDefaultPage(sDefaultPage);
}



