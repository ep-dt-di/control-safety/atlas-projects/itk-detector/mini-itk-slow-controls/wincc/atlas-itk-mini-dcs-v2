#uses "fwViewer/fwViewer_loginClass.ctl"
#uses "fwViewer/fwViewer_edmsFileManager.ctl"

class fwViewer_documentOpener {
  shared_ptr<fwViewer_loginClass> loginManager;
  shared_ptr<fwViewer_edmsFileManager> fileManager;
  const string PdfViewerPanelPath = "fwViewer/fwViewer_pdfPanel.pnl";

  public fwViewer_documentOpener(shared_ptr<fwViewer_loginClass> obj = new fwViewer_loginClass) {
    loginManager = obj;
    fileManager = fwViewer_edmsFileManager::create(loginManager);
  }

  public bool isPdfFile(string url) {
    bool pdfFile = true;
	string fileName = getFileName(url);
    int res = strpos(fileName, ".pdf");
    if(res < 0) {
      pdfFile = false;
    }
    return pdfFile;
  }

   public string getFileName(string url) {
    string clerPath = getClearPath(url);
    return fileManager.getFileName(clerPath);
  }

  public string getPdfFilePath(string url) {
    string path;
    int page;
    string clearPath = getClearPath(url);
    path = getPdfFile(clearPath);

    return path;
  }


  public string getNamedDestination(string url)
   {
     string namedDest = getUrlParam(url, "nameddest");

     return namedDest;
   }

   public int getPageNumber(string url)
   {
     string pageParam = getUrlParam(url, "page");
     int pageNumber = ( "" != pageParam ) ? (int) pageParam : 1;

     return pageNumber;
   }

   private string getUrlParam(string url, string param)
   // Get the parameter from the URL: <url>?param1=x1&param2=y2#param3=z3...
   {
     dyn_string dsRegexResult;
     string paramValue;

     int iRet = regexpSplit("[?&#]" + param + "=(\\w+)", url, dsRegexResult);

      if ( iRet >= 0 && dynlen(dsRegexResult) == 2 )
      {
        paramValue = dsRegexResult[2];
      }

     return paramValue;
   }

  private string getPdfFile (string url) {
    string path;
    string message ;
    mapping returnValue;
    bool status = true;
    returnValue = fileManager.downloadFile(url);
    status = (bool)returnValue["status"];
    if (!status) {
      message = "Problem with the file: " + url;
      DebugTN(__FUNCTION__ + ":" + __LINE__, message, returnValue);
      message  ="Problem with the pdfViewer, for more information check The LogViewer. "+
                       "Do you want open the pdf file in a external browser?";
      status = fwViewer_showYesNoWindow(message);
      if (status) {
         dyn_string exceptionInfo;
          fwGeneral_openInExternalBrowser(url, exceptionInfo);
          if (dynlen(exceptionInfo)) {
            fwExceptionHandling_display(exceptionInfo);
          }
      }

    }
    path = returnValue["filePath"];
    return path;
  }

	public bool  openPdfViewer(string url) {
		bool status = false;
		string path;
		string clearPath = getClearPath(url);
		path = getPdfFile(clearPath);
		if (path != "") {
			int page = getPageNumber(url);
			string namedDest = getNamedDestination(url);
			status = showPdfFile(path, page, url, namedDest);
		}
		return status;
	}

  private bool showPdfFile(string path, int page = 1, string url = "", string namedDest = "")
  {
    bool status = false;
    mapping returnValue;
    string panelName = "Viewer_";
    string hash = cryptoHash((string)getCurrentTime());

    	panelName += hash;

    if (path !="")
    {
      dyn_string parameters = makeDynString("$path:" + path, "$url:" + url, "$page:" + page, "$namedDest" + namedDest);
      ChildPanelOnCentral(PdfViewerPanelPath, panelName, parameters);
    }

    return status;
  }

  private string getClearPath(string url) {
    string searchedWord = ".pdf?";
    int pos = strpos(url,searchedWord);
    if (pos < 0) {
      return url;
    }
    int lenSearchWordWithoutQuestionMark = strlen(searchedWord) -1;
    int len =lenSearchWordWithoutQuestionMark + pos;
    string clearUrl = substr(url,0,len);
    return clearUrl;

  }

};
