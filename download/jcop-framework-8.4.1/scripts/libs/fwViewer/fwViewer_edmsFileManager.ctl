#uses "fwViewer/fwViewer_loginClass.ctl"
#uses "fwViewer/fwViewer_helpers.ctl"

class fwViewer_edmsFileManager {
  string generalPath;
  shared_ptr<fwViewer_loginClass> loginManager;
  shared_ptr<graphicalNotification> graph;
  const string webSeviceUrl = "https://edms.cern.ch/ws/FileService";

  protected fwViewer_edmsFileManager() {
  }
  
  public static shared_ptr<fwViewer_edmsFileManager> create(shared_ptr<fwViewer_loginClass> loginObject = new fwViewer_loginClass(),
                       shared_ptr<graphicalNotification> graphNotifcation = new graphicalNotification()) {
						   
	shared_ptr<fwViewer_edmsFileManager> myObject = new fwViewer_edmsFileManager();
	myObject.initialise(loginObject,graphNotifcation);
	return myObject;						   
  }
  
  protected initialise(shared_ptr<fwViewer_loginClass> loginObject,
						shared_ptr<graphicalNotification> graphNotifcation) {
							
	generalPath = dirName(tmpnam());

    if (loginObject == nullptr){
      loginObject = new fwViewer_loginClass();
    }
    loginManager = loginObject;

    if (graphNotifcation == nullptr) {
      graphNotifcation = new graphicalNotification();
    }
    graph = graphNotifcation;						
  }

  public mapping downloadFile(string url) {
    graph.showWaitWindow("Please wait, downloading PDF file.",20);
    mapping result;
    if(isItEdmsFile(url)) {
      string user;
      string key;

      if(!loginManager.isUserActive()) {
        bool loginStatus = loginManager.login();
        if(!loginStatus) {
          result = makeMapping("status", "FALSE", "httpStatusCode","", "errorMessage", "Action cancelled", "filePath", "");
          graph.hideWaitWindow();
          return result;
        }
      }
      user = loginManager.getActiveUser();
      key = loginManager.getKey();

      result = downloadEdmsFile(url, user, key);
      if (result["errorMessage"] == "Login problem") {
         // proble with login try again
        result = downloadFile(url);
      }
      graph.hideWaitWindow();
      return result;
    }
	result = downloadNormalFile(url);
	graph.hideWaitWindow();
    return result;
  }

  public string getFileName(string url) {
    dyn_string temp = strsplit(url,"/");
    int elementNumber = dynlen(temp);
    string fileName = temp[elementNumber];
    return fileName;
  }

  private mapping downloadNormalFile(string url) {
    mapping option = makeMapping("target", generalPath);
    mapping result,returnValue;
    bool status = true;
    string httpStatusCode, httpStatusText, errorMessage;
    string fileName = getFileName(url);
    string filePath = generalPath + fileName;
	
    result = getFile(url, option);
	
    if(mappingHasKey(result,"httpStatusCode")) {
      httpStatusCode = result["httpStatusCode"];
    }
    if(mappingHasKey(result,"httpStatusText")) {
      httpStatusText = result["httpStatusText"];
    }
    if (httpStatusText != "OK") {
      filePath = "";
      status = false;
      if(mappingHasKey(result,"sslErrors")) {
        errorMessage = result["sslErrors"];
      }
    }
    returnValue = makeMapping("status", status,"httpStatusCode",httpStatusCode,  "errorMessage", errorMessage, "filePath", filePath);
    return returnValue;

  }
  
  private mapping getFile(string url, mapping option) {
	  mapping result;
	  netGet(url,result,option);
	  return result;  
  }

  private mapping downloadEdmsFile(string url, string userName, string key) {

    mapping returnValue;
    string fileId = geFileId(url);
    if (fileId =="") {
      returnValue = makeMapping("status", "FALSE", "httpStatusCode", "", "errorMessage", "No file id.", "filePath", "");
      return returnValue;
    }
    string fileName = getFileName(url);
     if (fileName =="") {
      returnValue = makeMapping("status", "FALSE", "httpStatusCode", "", "errorMessage", "No file name", "filePath", "");
      return returnValue;
    }
   //fwViewer_ShowWaitWindow("Please wait, downloading PDF file.",20);
    string content = preparContent(userName, fileId, fileName);
    mapping header = prepareHeader(key);
    string filePath = generalPath + fileName;
    mapping result;
    mapping data = makeMapping("target", filePath, "content" ,content,"headers",header);

    returnValue = getEdmsFile(data, filePath);
	
    return returnValue;
  }
  
  private mapping getEdmsFile(mapping data, string filePath) {
	  mapping result;
	  netPost(webSeviceUrl,data,result);
	  result = checkResult(result,filePath);
	  return result;
  }

  private mapping checkResult (mapping result, string filePath) {
    mapping returnValue;
    string httpStatusText;
    string httpStatusCode;
    bool status = true;
    string serverResponseText;
    string errorMessage;

    if(mappingHasKey(result,"httpStatusCode")) {
      httpStatusCode = result["httpStatusCode"];
    }
    if(mappingHasKey(result,"httpStatusText")) {
      httpStatusText = result["httpStatusText"];
    }

    if (httpStatusText != "OK") {
      // proble with login try again
      remove(filePath);
      filePath = "";
      status = false;
      loginManager.logout();
      errorMessage = "Login problem";
      returnValue = makeMapping("status", status,"httpStatusCode",httpStatusCode,  "errorMessage", errorMessage, "filePath", filePath);

      return returnValue;
    }

    serverResponseText = getServerResponseText(filePath);

    if (serverResponseText != "") {
      remove(filePath);
      filePath = "";
      errorMessage = serverResponseText;
      status = false;
    }

    returnValue = makeMapping("status", status,"httpStatusCode",httpStatusCode,  "errorMessage", errorMessage, "filePath", filePath);
    return returnValue;


  }
  private string getServerResponseText(string filePath){
    string response;
    fileToString(filePath,response);

    string serverResponseText;
    string searchStr = "<exittext>";
    int lengthSearchStr = strlen(searchStr);
    int pos = strpos (response,searchStr);
    if (pos < 0) {
      return  serverResponseText;
    }
    pos = pos+lengthSearchStr;
    response = substr(response,pos);
    pos = strpos (response,"</exittext>");
    if (pos < 0) {
       return  serverResponseText;
    }
    serverResponseText = substr(response,0,pos);
    return serverResponseText;
  }

  private bool isItEdmsFile (string url) {
    bool edmsFile = true;
    if (strpos(url,"edms.cern.ch") < 0) {
      edmsFile = false;
    }
    return edmsFile;
  }


  private string preparContent(string user, string fileId, string fileName) {
    string content = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""+
                     " xmlns:web=\"http://webservices.edms.cern.ch\">"+
                       "<soapenv:Header/>"+
                         "<soapenv:Body>"+
                           "<web:getFile>"+
                             "<username>" + user + "</username>"+
                               "<docEdmsId>" + fileId + "</docEdmsId>"+
                               "<docVersion>LAST_RELEASED</docVersion>"+
                               "<fileName>" + fileName + "</fileName>"+
                            "</web:getFile>"+
                          "</soapenv:Body>"+
                       "</soapenv:Envelope>";
    return content;
  }

  private mapping prepareHeader(string key) {
    return makeMapping("Authorization", "Basic " + key);
  }

  private string geFileId(string url) {
    string fileId;
    string searchStr = "/file/";
    int lengthSearchStr = strlen(searchStr);
    int pos = strpos (url,searchStr);
    if (pos < 0) {
      return  fileId;
    }
    pos = pos+lengthSearchStr;
    url = substr(url,pos);
    pos = strpos (url,"/");
    if (pos < 0) {
       return  fileId;
    }
    fileId = substr(url,0,pos);
    return fileId;

  }

};
