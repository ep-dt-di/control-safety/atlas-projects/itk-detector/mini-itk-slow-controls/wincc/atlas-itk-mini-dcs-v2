// temporary solution for testing

class graphicalNotification {
  public int showWaitWindow(string message, int timeoutSeconds) {
    int  returnCurrentSize = 1;
    string sModuleName = "waitWindowModule"+cryptoHash(myPanelName());
    string sSymbolName = "waitWindowSymbol"+cryptoHash(myPanelName());
    moduleOff(sModuleName);
    int errorCode = addSymbol(myModuleName(),myPanelName(),"fwViewer/fwViewer_waitWindowFrame.pnl",sSymbolName,makeDynString("$sModuleName:" + sModuleName,"$sMessage:" + message,"$iTimeoutSeconds:" + timeoutSeconds),0,0,2,1);
    if (errorCode == -1) {
      return errorCode;
    }
    shape embededModuleShape = getShape(sSymbolName +".EMBEDDED_MODULE1");
    int width,height,x,y;
    panelSize(myPanelName(),width,height,returnCurrentSize);
    if (errorCode == -1) return errorCode;
    embededModuleShape.size(width,height);
    delay(0,30);
    return 0;
    }
    int fwViewer_HideWaitWindow() {
      string sModuleName = "waitWindowModule"+cryptoHash(myPanelName());
      if (isModuleOpen(sModuleName)) {
        int errorCode = moduleOff(sModuleName);
        if (errorCode == -1) return errorCode;
      }
      return 0;
  }

  public int hideWaitWindow() {
    string sModuleName = "waitWindowModule"+cryptoHash(myPanelName());
    if (isModuleOpen(sModuleName)) {
      int errorCode = moduleOff(sModuleName);
      if (errorCode == -1) return errorCode;
    }
    return 0;
  }

  public static bool showYesNoWindow(string message, string windowTitle = ""){
    dyn_float floatAnswer;
    dyn_string stringAnswer;
    string panelPath = "fwViewer/fwViewer_questionPanel.pnl";
    floatAnswer[1] = 0;
    ChildPanelOnCentralReturn(panelPath, "questionPanel:"+cryptoHash(message), makeDynString("$message:"+message,"$windowTitle:"+windowTitle,"$button1Text:Yes","$button2Text:No"),  floatAnswer, stringAnswer);
    if(floatAnswer[1] == 1) {
      return true;
    } else {
      return false;
    }
  }
};


int fwViewer_ShowWaitWindow(string message, int timeoutSeconds) {
  int  returnCurrentSize = 1;
  string sModuleName = "waitWindowModule"+cryptoHash(this.name());
  string sSymbolName = "waitWindowSymbol"+cryptoHash(this.name());
  moduleOff(sModuleName);
  int errorCode = addSymbol(myModuleName(),myPanelName(),"fwViewer/fwViewer_waitWindowFrame.pnl",sSymbolName,makeDynString("$sModuleName:" + sModuleName,"$sMessage:" + message,"$iTimeoutSeconds:" + timeoutSeconds),0,0,2,1);
  if (errorCode == -1) {
    return errorCode;
  }
  shape embededModuleShape = getShape(sSymbolName +".EMBEDDED_MODULE1");
  int width,height,x,y;
  panelSize(myPanelName(),width,height,returnCurrentSize);
  if (errorCode == -1) return errorCode;
  embededModuleShape.size(width,height);
  delay(0,30);
  return 0;
}
int fwViewer_HideWaitWindow() {
  string sModuleName = "waitWindowModule"+cryptoHash(this.name());
  if (isModuleOpen(sModuleName)) {
    int errorCode = moduleOff(sModuleName);
    if (errorCode == -1) return errorCode;
  }
  return 0;
}

bool fwViewer_showOkCancelWindow(string message, string windowTitle = "")
{
  dyn_float floatAnswer;
  dyn_string stringAnswer;
  string panelPath = "fwViewer/fwViewer_questionPanel.pnl";
  floatAnswer[1] = 0;
  ChildPanelOnCentralReturn(panelPath, "questionPanel:"+cryptoHash(message), makeDynString("$message:"+message,"$windowTitle:"+windowTitle),  floatAnswer, stringAnswer);
  if(floatAnswer[1] == 1) {
    return true;
  } else {
    return false;
  }

}

bool fwViewer_showYesNoWindow(string message, string windowTitle = "")
{
  dyn_float floatAnswer;
  dyn_string stringAnswer;
  string panelPath = "fwViewer/fwViewer_questionPanel.pnl";
  floatAnswer[1] = 0;
  ChildPanelOnCentralReturn(panelPath, "questionPanel:"+cryptoHash(message), makeDynString("$message:"+message,"$windowTitle:"+windowTitle,"$button1Text:Yes","$button2Text:No"),  floatAnswer, stringAnswer);
  if(floatAnswer[1] == 1) {
    return true;
  } else {
    return false;
  }

}
