#uses "fwUnitTestComponentAsserts.ctl"
#uses "CtrlLDAP"
#uses "fwAccessControl/fwAccessControl.ctc"

// to pull in the constants for error messages, and still have
// the syntax-checking correct, we pull-in this lib (see ETM-1327)
#uses "xmlrpcHandlerCommon.ctl"

global string gLdapHost="";
global string gLdapUser="";
global string gLdapPassword="";
global string gIPAHost="";
global string gIPAUser="";
global string gIPAPassword="";
const string FWAC_DEFAULT_LDAP_HOST="cerndc.cern.ch";
const string FWAC_DEFAULT_FREEIPA_HOST="ipa-dev-1.ipa-dev.cern.ch";

// define some standard error code constants, taken from the main _errors.cat message catalogue
// note that for some others, there are already built-in constants in WinCC OA
const int ARG_MISSING_IN_FUNCTION=75;


// Some of standard LDAP error messages, e.g. following RFC-4511, of from ldap.h
const int LDAP_SUCCESS=0;
const int LDAP_INVALID_CREDENTIALS=49;
const int LDAP_PARAM_ERROR=-9;
const int LDAP_SERVER_DOWN=-1;
const int LDAP_TIMEOUT=-5;


private string getErrorStringFromCatalogue(const dyn_errClass &err)
{
    if (dynlen(err)==0) return "";
    string lErrCat=getErrorCatalog(err);
    int lErrCode=getErrorCode(err);
    if (lErrCat=="") lErrCat="_errors";
    string lErrCodeAsString;
    sprintf (lErrCodeAsString,"%05d",lErrCode);
    string errString=getCatStr(lErrCat,lErrCodeAsString);

    return errString;
}


/** Compares error/exception and reports them

*/
void assertErrClass(dyn_errClass err, int errCode, int errPrio, string errText, bool isException=false)
{
    string what="error";
    if (isException) what="exception";

    if (dynlen(err) <1) {
      assert(true,"expected an "+what+" to appear");
      return;
    }

    string lErrText = getErrorText(err);
    int lErrCode= getErrorCode(err);
    int lErrPrio= getErrorPriority(err);

    // now also retrieve the error string from catalogue,
    // because the actual text is made of errString, functionName and  errText separated by ", "
    string errString=getErrorStringFromCatalogue(err);

    assertEqual(errCode,lErrCode,what+" codes do not match");
    assertEqual(errPrio,lErrPrio,what+" priorities do not match");
    assertEqual(errString+", "+errText, lErrText, what+" texts do not match");
}


void assertLastError(int errCode, int errPrio, string errText)
{
    dyn_errClass err = getLastError();
    assertErrClass(err, errCode, errPrio, errText);
}

void assertNoLastError()
{
    dyn_errClass err = getLastError();
    if (dynlen(err)==0) return;

    DebugTN(err);
    assert(false,"Error encountered unexpectedly, "+getErrorText(err));
}


void assertLastException(int excCode, int excPrio, string excText)
{
    dyn_errClass exc = getLastException();
    assertErrClass(exc, excCode, excPrio, excText, true);
}

void test_CtrlLDAP_setupSuite()
{
  gLdapHost=getenv("FWAC_LDAP_HOST");
  gLdapUser=getenv("FWAC_LDAP_USER");
  gLdapPassword=getenv("FWAC_LDAP_PASSWORD");
  
  if (gLdapHost=="") {
    gLdapHost=FWAC_DEFAULT_LDAP_HOST;
  } 
  gIPAHost=getenv("FWAC_FREEIPA_HOST");
  gIPAUser=getenv("FWAC_FREEIPA_USER");
  gIPAPassword=getenv("FWAC_FREEIPA_PASSWORD");
  
  if (gIPAHost=="") {
    gIPAHost=FWAC_DEFAULT_FREEIPA_HOST;
  }
  if ( (gLdapHost=="") || (gLdapUser=="") || (gLdapPassword=="")) {
    DebugTN("Incomplete setup (wrong authParams in CtrlLDAPAuth.ctc)");
    return;
  }
  if ( (gIPAHost=="") || (gIPAUser=="") || (gIPAPassword=="")) {
    DebugTN("Incomplete FreeIPA setup (wrong authParams in CtrlLDAPAuth.ctc)");
    // return;
  }

  if (isFunctionDefined("ldapCleanup")) ldapCleanup();
}

void test_CtrlLDAP_teardownSuite()
{
    //start by resetting params:
    gLdapHost="";
    gLdapUser="";
    gLdapPassword="";
    gIPAHost="";
    gIPAUser="";
    gIPAPassword="";
}


/**
    Check that the test suite parameters (username and password used for testing) were set up correctly
*/
void test_CtrlLDAP_AuthParamSetupVerify()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly (empty username)");
    assert(gLdapHost!="","LDAP AuthParams not configured correctly (empty hostURI)");

}


void test_CtrlLDAP_FunctionsExist()
{
    dyn_string functionsToCheck=makeDynString(	"ldapGetVersion",
						"ldapCheckAuth",
						"ldapSearch",
						"ldapCleanup",
						"ldapUseExceptions");

    for (int i=1;i<=dynlen(functionsToCheck);i++) {
	assertTrue(isFunctionDefined(functionsToCheck[i]), "Function does not exist:"+functionsToCheck[i]);
	DebugTN(__FUNCTION__,"Function exists",functionsToCheck[i]);
    }

}

void test_CtrlLDAP_ldapGetVersion()
{
    string ver=ldapGetVersion();
    assertNoLastError();
    assertTrue(ver!="","CtrlLDAP Version is empty");

    DebugTN(__FUNCTION__,"ldapGetVersion returns",ver);
}



void test_CtrlLDAP_CheckVersion()
{
    float minCtrlLDAPVersion=7.0;

    assert(isFunctionDefined("ldapUseExceptions"),"ldapUseExceptions() not available");
    assert(isFunctionDefined("ldapGetVersion"),"ldapGetVersion() not available");

    ldapUseExceptions(true);

    string s_ldapVersion;

    try {
	s_ldapVersion=ldapGetVersion();
    } catch {
	dyn_errClass exc=getLastException();
	assert(true,"Exception encountered:"+getErrorText(exc));
    }

    assert (s_ldapVersion!="","Empty ldapVersion");

    float ldapVersion=(float)s_ldapVersion;

    assert(ldapVersion>=minCtrlLDAPVersion,"Invalid CtrlLDAP Version, "+s_ldapVersion+" (required min:"+minCtrlLDAPVersion+")");
}


void test_CtrlLDAP_ldapCleanup()
{
    // we call it twice in a row - potentially to spot problems when cleaning up twice.
    ldapCleanup();
    assertNoLastError();
    ldapCleanup();
    assertNoLastError();

}

void test_CtrlLDAP_ldapCheckAuth_TooFewParameters_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    ldapUseExceptions(false);

    string ldapUser="";
    string ldapAuthMethod="SIMPLE";
    int timeout=5;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost,gLdapUser);
    assertLastError(ARG_MISSING_IN_FUNCTION, PRIO_SEVERE,"ldapCheckAuth, password");
    assertEqual(-1, rc ,"Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_HostURIEmpty_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapHost="";
    string ldapAuthMethod="SIMPLE";
    int timeout=5;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, empty host URI specified");
    assertEqual(-1, rc, "Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_HostURIWrongTransport_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");
    string ldapAuthMethod="SIMPLE";
    int timeout=1;
    string ldapHost="http://"+gLdapHost;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);
    if (_WIN32) {
	assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, invalid scheme in hostURI: "+ldapHost);
	assertEqual(-1, rc, "Wrong return code");
    } else {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, ldap_initialize failed, Bad parameter to an ldap routine (-9)");
	//assertEqual(LDAP_PARAM_ERROR, rc, "Wrong return code");
	assertEqual(-2 , rc, "Wrong return code");  // as of now there is a hardcoded -2... we should improve this somehow... Maybe -9 (LDAP_PARAM_ERROR)???
    }

}


void test_CtrlLDAP_ldapCheckAuth_HostURIPortTooBig_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");
    string ldapAuthMethod="SIMPLE";
    int timeout=1;
    string ldapHost="ldap://"+gLdapHost+":696502";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);
    if (_WIN32) {
	assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, invalid port number in hostURI: "+ldapHost);
	assertEqual(-1, rc, "Wrong return code");
    } else {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, Cannot log in, Can't contact LDAP server (-1)");
	//assertEqual(LDAP_PARAM_ERROR, rc, "Wrong return code");
	assertEqual(-1 , rc, "Wrong return code");  // as of now there is a hardcoded -1... we should improve this somehow... Maybe -9 (LDAP_PARAM_ERROR)???
    }
}

void test_CtrlLDAP_ldapCheckAuth_HostURIPortNegative_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");
    string ldapAuthMethod="SIMPLE";
    int timeout=1;
    string ldapHost="ldap://"+gLdapHost+":-1";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);
    if (_WIN32) {
	assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, invalid port number in hostURI: "+ldapHost);
	assertEqual(-1, rc, "Wrong return code");
    } else {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, Cannot log in, Can't contact LDAP server (-1)");
	//assertEqual(LDAP_PARAM_ERROR, rc, "Wrong return code");
	assertEqual(-1 , rc, "Wrong return code");  // as of now there is a hardcoded -1... we should improve this somehow... Maybe -9 (LDAP_PARAM_ERROR)???
    }
}

void test_CtrlLDAP_ldapCheckAuth_HostURIPortNotNumber_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");
    string ldapAuthMethod="SIMPLE";
    int timeout=1;
    string ldapHost="ldap://"+gLdapHost+":aaa";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);
    if (_WIN32) {
	assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, invalid port number in hostURI: "+ldapHost);
	assertEqual(-1, rc, "Wrong return code");
    } else {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, ldap_initialize failed, Bad parameter to an ldap routine (-9)");
	//assertEqual(LDAP_PARAM_ERROR, rc, "Wrong return code");
	assertEqual(-2 , rc, "Wrong return code");  // as of now there is a hardcoded -2... we should improve this somehow... Maybe -9 (LDAP_PARAM_ERROR)???
    }
}


void test_CtrlLDAP_ldapCheckAuth_UsernameEmpty_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapUser="";
    string ldapAuthMethod="SIMPLE";
    int timeout=5;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, ldapUser, gLdapPassword, timeout);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, empty user name specified");
    assertEqual(-1, rc, "Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_UserNameWrongType_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    dyn_string ldapUser;
    string ldapAuthMethod="SIMPLE";
    int timeout=5;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, ldapUser, gLdapPassword, timeout);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, wrong data type for argument: userName; got DYNTEXT_VAR, expected TEXT_VAR");
    assertEqual(rc,-1,"Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_HostURITooLong_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    // construct ldapHost name exceeding 40 characters
    int len=41;
    string ldapHost="";
    for (int i=1;i<=len;i++) ldapHost+="X";

    string ldapAuthMethod="SIMPLE";
    int timeout=5;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, hostURI too long");
    assertEqual(-1, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_UserNameTooLong_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    // construct ldapUser exceeding 128 characters
    int len=129;
    string ldapUser="";
    for (int i=1;i<=len;i++) ldapUser+="X";

    string ldapAuthMethod="SIMPLE";
    int timeout=5;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, ldapUser, gLdapPassword, timeout);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, userName too long");
    assertEqual(-1, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_PasswordTooLong_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    // construct ldapPassword exceeding 128 characters
    int len=129;
    string ldapPassword="";
    for (int i=1;i<=len;i++) ldapPassword+="X";

    string ldapAuthMethod="SIMPLE";
    int timeout=5;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, gLdapUser, ldapPassword, timeout);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, password too long");
    assertEqual(-1, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_TimeoutNegative_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="SIMPLE";
    int timeout=-1;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, gLdapUser, gLdapPassword, timeout);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, negative timeout specified");
    assertEqual(-1, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_InvalidAuthMethod_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="TEST_METHOD"; // supported are SIMPLE and DIGEST-MD5

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, gLdapUser, gLdapPassword);
    assertLastError(ERR_INVALID_ARGUMENT, PRIO_SEVERE, "ldapCheckAuth, unsupported authMethod: "+ldapAuthMethod);
    assertEqual(-1, rc, "Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_WrongPassword_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="DIGEST-MD5";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, gLdapUser, gLdapPassword+"_BAD_PASSWORD");
    assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, Cannot log in, Invalid credentials (49)");
    assertEqual(LDAP_INVALID_CREDENTIALS, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostName_DIGESTMD5_Auth_OK()
{
	// Skipping test due to reccurent issue with double free on libldap
	skipTest("Known double free issue in libldap. [FWAC-478]");
	return;
	// 	See FWAC-478 for more details
	
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="DIGEST-MD5";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, gLdapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostName_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="SIMPLE";
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gLdapHost, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_FreeIPA_HostName_SIMPLE_Auth_OK()
{
    if ( (gIPAHost=="") || (gIPAUser=="") || (gIPAPassword=="")) {
        skipTest("Incomplete FreeIPA setup (wrong authParams in CtrlLDAPAuth.ctc)");
    }
    assert(gIPAUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="SIMPLE";
    string ipaUser="uid="+gIPAUser+", cn=users, cn=accounts, dc=ipa-dev, dc=cern, dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, gIPAHost, ipaUser, gIPAPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostURISSL_DIGESTMD5_Auth_OK()
{
	// Skipping test due to reccurent issue with double free on libldap
	skipTest("Known double free issue in libldap. [FWAC-478]");
	return;
	// 	See FWAC-478 for more details
	
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="DIGEST-MD5";
    string ldapURI="ldaps://"+gLdapHost;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, gLdapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostURI_DIGESTMD5_Auth_OK()
{
	// Skipping test due to reccurent issue with double free on libldap
	skipTest("Known double free issue in libldap. [FWAC-478]");
	return;
	// 	See FWAC-478 for more details
	
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="DIGEST-MD5";
    string ldapURI="ldap://"+gLdapHost;

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, gLdapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostURISSL_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ldapURI="ldaps://"+gLdapHost;
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostURIDefault_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ldapURI=gLdapHost;
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_FreeIPA_HostURISSL_SIMPLE_Auth_OK()
{

    assert(gIPAUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="SIMPLE";
    string ipaURI="ldaps://"+gIPAHost;
    string ipaUser="uid="+gIPAUser+", cn=users, cn=accounts, dc=ipa-dev, dc=cern, dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ipaURI, ipaUser, gIPAPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostURISSLWithPort_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ldapURI="ldaps://"+gLdapHost+":636";
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostURIDefaultWithPort_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ldapURI=gLdapHost+":636";
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_FreeIPA_HostURISSLWithPort_SIMPLE_Auth_OK()
{
    
    if ( (gIPAHost=="") || (gIPAUser=="") || (gIPAPassword=="")) {
        skipTest("Incomplete FreeIPA setup (wrong authParams in CtrlLDAPAuth.ctc)");
    }
    assert(gIPAUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ipaURI="ldaps://"+gIPAHost+":636";
    string ipaUser="uid="+gIPAUser+", cn=users, cn=accounts, dc=ipa-dev, dc=cern, dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ipaURI, ipaUser, gIPAPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_HostURI_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ldapURI="ldap://"+gLdapHost;
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_FreeIPA_HostURI_SIMPLE_Auth_OK()
{
    skipTest("Unencrypted connections to FreeIPA are no longer allowed");

    if ( (gIPAHost=="") || (gIPAUser=="") || (gIPAPassword=="")) {
        skipTest("Incomplete FreeIPA setup (wrong authParams in CtrlLDAPAuth.ctc)");
    }
    assert(gIPAUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ipaURI="ldap://"+gIPAHost;
    string ipaUser="uid="+gIPAUser+", cn=users, cn=accounts, dc=ipa-dev, dc=cern, dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ipaURI, ipaUser, gIPAPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_HostURIWithPort_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ldapURI="ldap://"+gLdapHost+":389";
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_HostURIDefultWithNonSecurePort_SIMPLE_Auth_OK()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ldapURI=gLdapHost+":389";
    string ldapUser="cn="+gLdapUser+",OU=Users,OU=Organic Units,DC=cern,dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapURI, ldapUser, gLdapPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}


void test_CtrlLDAP_ldapCheckAuth_FreeIPA_HostURIWithPort_SIMPLE_Auth_OK()
{
    skipTest("Unencrypted connections to FreeIPA are no longer allowed");

    if ( (gIPAHost=="") || (gIPAUser=="") || (gIPAPassword=="")) {
        skipTest("Incomplete FreeIPA setup (wrong authParams in CtrlLDAPAuth.ctc)");
    }
    assert(gIPAUser!="","LDAP AuthParams not configured correctly");


    string ldapAuthMethod="SIMPLE";
    string ipaURI="ldap://"+gIPAHost+":389";
    string ipaUser="uid="+gIPAUser+", cn=users, cn=accounts, dc=ipa-dev, dc=cern, dc=ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ipaURI, ipaUser, gIPAPassword);
    assertNoLastError();
    assertEqual(0, rc, "Wrong return code");
}

void test_CtrlLDAP_ldapCheckAuth_HostDoesNotExist_Fail()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="SIMPLE";
    int timeout=1;
    string ldapHost="some-nonexisting-host.cern.ch";

    ldapCleanup();

    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);

    if (_WIN32) {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, Cannot connect to LDAP server, Server Down (81)");
	assertEqual(81, rc, "Wrong return code");
    } else {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, Cannot log in, Can't contact LDAP server (-1)");
	assertEqual(-1, rc, "Wrong return code");
    }
}


void test_CtrlLDAP_ldapCheckAuth_HostOnTN_Timeout()
{
    assert(gLdapUser!="","LDAP AuthParams not configured correctly");

    string ldapAuthMethod="DIGEST-MD5";
    int timeout=3;
    // purposefully check for non accessible host to guarantee timeout
    string ldapHost="tndc.cern.ch";

    ldapCleanup();
    time tStart=getCurrentTime();
    int rc=ldapCheckAuth(ldapAuthMethod, ldapHost, gLdapUser, gLdapPassword, timeout);
    if (_WIN32) {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, Cannot connect to LDAP server, Server Down (81)");
	assertEqual(81, rc, "Wrong return code");
    } else {
	assertLastError(ERR_UNEXPECTED_STATE, PRIO_SEVERE, "ldapCheckAuth, Cannot log in, Can't contact LDAP server (-1)");
	assertEqual(-1, rc, "Wrong return code");
    }
    time tEnd=getCurrentTime();

    int elapsedSeconds=period (tEnd-tStart);
    // assertTrue(elapsedSeconds>2,"Timeout was too early");
    // how can timeout be too fast?
    assertTrue(elapsedSeconds<4,"Timeout was too long");
    DebugTN(__FUNCTION__,"Timeout after", elapsedSeconds);

}


void test_CtrlLDAP_LDAPSearch_AnonymousBind_XLDAP_OK()
{
    // Reminder:
    // ldapSearch(string hostURI, string baseDN, dyn_dyn_string &attrNames, dyn_dyn_string &attrValues, string userName="",
    //            string password="", string filter="(objectClass=*)", int timeout=3,string authMethod="SIMPLE")

    int timeout=3;
    string hostURI="xldap.cern.ch";
    dyn_dyn_string attrNames,attrValues;
    attrNames[1]=makeDynString("cn","department","memberOf");
    string baseDN="OU=Users,OU=Organic Units,DC=cern,dc=ch";
    string filter="(&(objectClass=*)(cn="+gLdapUser+"))";
    string userName="";
    string password="";

    int rc=ldapSearch(hostURI, baseDN, attrNames, attrValues, userName, password="", filter);
    assertEqual(0, rc, "Wrong return code");
    assertNoLastError();
    //DebugTN(attrNames, attrValues);

}
