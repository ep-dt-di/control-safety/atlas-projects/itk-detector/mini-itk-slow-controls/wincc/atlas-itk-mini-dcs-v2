#uses "fwAccessControl/fwAccessControl.ctc"
#uses "fwAccessControl/fwAccessControl_Egroups.ctc" // we may not load this using #uses until testing harness is fixed; see setupSuite()
#uses "fwUnitTestComponentAsserts.ctl"

void assertExceptionInfo(string errSeverity, string errString, dyn_string exceptionInfo)
{
    // until Paul fixes the unit-testing framework's assertError() we need this one:
    if (dynlen(exceptionInfo) <3) {
	assert(false,"No exceptionInfo while expecting to have one");
	return;
    }
    assertError(errSeverity,errString,exceptionInfo);
}



private string getErrorString(int errCode)
{
    // convert a integer error code to a 5-digit string
    // as needed by getCatStr()
    string errCodeAsString;
    sprintf(errCodeAsString,"%05d",errCode);
    string errCat="_errors";
    string errString=getCatStr(errCat,errCodeAsString);
    return errString;
}

// extract the values of a specific key for all entries of a dyn_mapping
dyn_mixed dynMappingExtractByKey(dyn_mapping dm, mixed key)
{
    dyn_mixed retval;
    mixed uninitialized;
    for (int i=1;i<=dynlen(dm);i++) {
	if (mappingHasKey(dm[i],key)) retval[i]=(dm[i])[key]; else retval[i]=uninitialized;
    }
    return retval;
}




global bool g_UNICOSDomainCreated=false;
global bool g_UNICOSDomainModified=false;
global string g_UNICOSDomainOriginalFullName="";

void test_fwAccessControl_Egroups_setupSuite()
{
    // #uses does not work for .ctc files! we need to load it dynamically here!!!
    string loaderScript = "#uses \"fwAccessControl/fwAccessControl_Egroups.ctc\" bool main(){return TRUE};";
    anytype retval;
    int rc=evalScript(retval,loaderScript,makeDynString());
    DebugTN("eval script loading the fwAccessControl/fwAccessControl_Egroups.ctc done...",rc);

    // we need to make sure that there exists the "UNICOS" domain, with proper fullName
    // so that the tests could retrieve egroup name from this field.

    // auxilary, neeeded for setup
    string domainName="UNICOS";
    string necessaryDomainFullName=fwAccessControl_Egroup_FullNamePrefix+"SCADA-fwACDomain-UNICOS";
    dyn_string exceptionInfo;


    g_UNICOSDomainCreated=false;
    g_UNICOSDomainModified=false;
    g_UNICOSDomainOriginalFullName="";

    string fullName="";
    dyn_string domainList, fullDomainNames;
    fwAccessControl_getAllDomains(domainList, fullDomainNames, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");
    int idx=dynContains(domainList,domainName);
    if (!idx) {

	DebugTN("Creating domain UNICOS for testing");
	fwAccessControl_createDomain(domainName,necessaryDomainFullName  , "",
	    makeDynString("monitor","operator","expert","admin"), exceptionInfo);
	if (dynlen(exceptionInfo)) { DebugTN("ERROR in setupSuite()",exceptionInfo);return;}
	g_UNICOSDomainCreated=true;
    } else {
        string domainComment;
        int    domainId;
        dyn_string privilegeNames;
        dyn_int privilegeIds;


	fwAccessControl_getDomain(domainName, fullName, domainComment, domainId, privilegeNames, privilegeIds,exceptionInfo);
	if (dynlen(exceptionInfo)) { DebugTN("ERROR in setupSuite()",exceptionInfo);return;}

	if (fullName!=necessaryDomainFullName) {
	    DebugTN("Temporarily changing the fullName of domain UNICOS from "+fullName + " to " + necessaryDomainFullName);
	    g_UNICOSDomainOriginalFullName=fullName;
	    fwAccessControl_updateDomain(domainName, domainName, necessaryDomainFullName,
	                                  domainComment, privilegeNames, exceptionInfo);
	    if (dynlen(exceptionInfo)) { DebugTN("ERROR in setupSuite()",exceptionInfo);return;}
	    g_UNICOSDomainModified=true;
	}
    }

}

void test_fwAccessControl_Egroups_teardownSuite()
{

    dyn_string exceptionInfo;
    if (g_UNICOSDomainCreated) {
	DebugTN(__FUNCTION__,"Dropping the domain UNICOS");
	fwAccessControl_deleteDomain("UNICOS",exceptionInfo);
	if (dynlen(exceptionInfo)) { DebugTN("ERROR in teardownSuite()",exceptionInfo);}
	return;
    }

    if (g_UNICOSDomainModified) {
	DebugTN(__FUNCTION__,"Restoring original full name of the UNICOS domain",g_UNICOSDomainOriginalFullName);

	string domainName="UNICOS";
        string fullName,domainComment;
        int    domainId;
        dyn_string privilegeNames;
        dyn_int privilegeIds;

	fwAccessControl_getDomain(domainName, fullName, domainComment, domainId, privilegeNames, privilegeIds,exceptionInfo);
	if (dynlen(exceptionInfo)) { DebugTN("ERROR in teardownSuite()",exceptionInfo);return;}
	fwAccessControl_updateDomain(domainName, domainName, g_UNICOSDomainOriginalFullName, domainComment, privilegeNames, exceptionInfo);
	if (dynlen(exceptionInfo)) { DebugTN("ERROR in teardownSuite()",exceptionInfo);return;}

    }
}

void test_fwAccessControl_Egroups_getMatchingPattern()
{
    dyn_string exceptionInfo;

    string egroupNameFilter="SCADA-fwAC*";
    string attrNames="";
    string childType="";
    dyn_string otherFilters;

    dyn_mapping result = fwAccessControl_getEgroupSubgroups(egroupNameFilter, exceptionInfo, attrNames, childType, otherFilters);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    dyn_string allCNs=dynMappingExtractByKey(result, "cn");
    assertTrue(dynContains(allCNs,"SCADA-fwACConfig-ScdAppServiceExperts")>0,"Configuration egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACDomain-UNICOS")>0,"UNICOS Domain egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACDomain-SYSTEM")>0,"SYSTEM Domain egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-SYSTEM-1-Visualize")>0,"SYSTEM:Visualize privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-SYSTEM-2-Normal__user__level")>0,"SYSTEM:Normal user level privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-SYSTEM-3-Extended__user__level")>0,"SYSTEM:Extended user level privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-SYSTEM-4-Administration")>0,"SYSTEM:Administration privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-SYSTEM-5-Acknowledge")>0,"SYSTEM:Acknowledge privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-UNICOS-1-monitor")>0,"UNICOS:monitor user level privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-UNICOS-2-operator")>0,"UNICOS:operator user level privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-UNICOS-3-expert")>0,"UNICOS:expert user level privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACPrivilege-UNICOS-4-admin")>0,"UNICOS:admin user level privilege egroup not found");
    assertTrue(dynContains(allCNs,"SCADA-fwACRole-ScdAppService--Experts")>0,"ScdAppService-Experts Role egroup not found");
}

void test_fwAccessControl_Egroups_getAllTestConfigurations()
{
    dyn_string exceptionInfo;

    string egroupNameFilter="";
    string childType="CONFIGURATIONS";
    string attrNames="";
    dyn_string otherFilters;//=makeDynString("cn=SCADA-fwAC*"); // we put a special filter for this test to help the LDAP server

    dyn_mapping result = fwAccessControl_getEgroupSubgroups(egroupNameFilter, exceptionInfo, attrNames, childType, otherFilters);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

    dyn_string allCNs=dynMappingExtractByKey(result, "cn");
    assertTrue(dynContains(allCNs,"SCADA-fwACConfig-ScdAppServiceExperts")>0,"Configuration egroup not found");
}


void test_fwAccessControl_Egroups_getConfigurationDomains()
{
    dyn_string exceptionInfo;

    string egroupNameFilter="SCADA-fwACConfig-ScdAppServiceExperts";
    string childType="DOMAINS";
    string attrNames="adminDescription,memberOf";
    dyn_string otherFilters;

    dyn_mapping result = fwAccessControl_getEgroupSubgroups(egroupNameFilter, exceptionInfo, attrNames, childType, otherFilters);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

    dyn_string allDomainCNs=dynMappingExtractByKey(result, "cn");
    assertTrue(dynContains(allDomainCNs,"SCADA-fwACDomain-UNICOS")>0,"UNICOS domain egroup not found");
}


void test_fwAccessControl_Egroups_getDomainPrivileges()
{
    dyn_string exceptionInfo;

    string egroupNameFilter="SCADA-fwACDomain-UNICOS";
    string childType="PRIVILEGES";
    string attrNames="adminDescription,memberOf";
    dyn_string otherFilters;

    dyn_mapping result = fwAccessControl_getEgroupSubgroups(egroupNameFilter, exceptionInfo, attrNames, childType, otherFilters);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    //DebugTN(result);


    dyn_string allPrivilegeCNs=dynMappingExtractByKey(result, "cn");
    assertTrue(dynContains(allPrivilegeCNs,"SCADA-fwACPrivilege-UNICOS-1-monitor") >0,"UNICOS:monitor privilege egroup not found");
    assertTrue(dynContains(allPrivilegeCNs,"SCADA-fwACPrivilege-UNICOS-2-operator")>0,"UNICOS:operator privilege egroup not found");
    assertTrue(dynContains(allPrivilegeCNs,"SCADA-fwACPrivilege-UNICOS-3-expert")  >0,"UNICOS:expert privilege egroup not found");
    assertTrue(dynContains(allPrivilegeCNs,"SCADA-fwACPrivilege-UNICOS-4-admin")   >0,"UNICOS:admin privilege egroup not found");

}

void test_fwAccessControl_Egroups_getPrivilegeHolders()
{
    dyn_string exceptionInfo;

    string egroupNameFilter="SCADA-fwAC*";
    string childType="ROLES";
    string attrNames;
    dyn_string otherFilters=makeDynString("member=cn=SCADA-fwACPrivilege-UNICOS-4-admin,"+fwAccessControl_egroupsBaseDN);

    dyn_mapping result = fwAccessControl_getEgroupSubgroups(egroupNameFilter, exceptionInfo, attrNames, childType, otherFilters);
    assertEmpty(exceptionInfo, "Exceptions have been raised");

    dyn_string allRoleCNs=dynMappingExtractByKey(result, "cn");
    assertTrue(dynContains(allRoleCNs,"SCADA-fwACRole-ScdAppService--Experts")>0,"Privilege holder not found");
}


void test_fwAccessControl_Egroups_getConfigurationRoles()
{
    dyn_string exceptionInfo;

    string egroupNameFilter="SCADA-fwACConfig-ScdAppServiceExperts";
    string childType="ROLES";
    string attrNames="adminDescription,memberOf";
    dyn_string otherFilters;

    dyn_mapping result = fwAccessControl_getEgroupSubgroups(egroupNameFilter, exceptionInfo, attrNames, childType, otherFilters);

    assertEmpty(exceptionInfo, "Exceptions have been raised");

//    DebugTN(result);

    dyn_string allRoleCNs=dynMappingExtractByKey(result, "cn");
    assertTrue(dynContains(allRoleCNs,"SCADA-fwACRole-ScdAppService--Experts")>0,"Test role egroup not found");
}


void test_fwAccessControl_Egroups_getDomainFromEgroup_nonExisting()
{
    dyn_string exceptionInfo;

    string domainEgroup="some-non-existing-egroup";
    string domainName, domainComment;
    dyn_string privilegeEgroups, privilegeNames;

    fwAccessControl_getDomainFromEgroup(domainEgroup, domainName, domainComment, privilegeEgroups, privilegeNames, exceptionInfo);
    DebugTN(exceptionInfo);
    assertTrue(dynlen(exceptionInfo),"Got no exception while expected one.");
}


void test_fwAccessControl_Egroups_getDomainFromEgroup_withWildcard()
{
    dyn_string exceptionInfo;

    string domainEgroup="SCADA-fwAC*";
    string domainName, domainComment;
    dyn_string privilegeEgroups, privilegeNames;

    fwAccessControl_getDomainFromEgroup(domainEgroup, domainName, domainComment, privilegeEgroups, privilegeNames, exceptionInfo);
    DebugTN(exceptionInfo);
    assertTrue(dynlen(exceptionInfo),"Got no exception while expected one.");
}


void test_fwAccessControl_Egroups_getDomainFromEgroup_wrongType()
{
    // we try to get a domain, and we pass a role egroup
    dyn_string exceptionInfo;

    string domainEgroup="SCADA-fwACRole-ScdAppService--Experts";
    string domainName, domainComment;
    dyn_string privilegeEgroups, privilegeNames;

    fwAccessControl_getDomainFromEgroup(domainEgroup, domainName, domainComment, privilegeEgroups, privilegeNames, exceptionInfo);
    DebugTN(exceptionInfo);
    assertTrue(dynlen(exceptionInfo),"Got no exception while expected one.");
}

void test_fwAccessControl_Egroups_getDomainFromEgroup()
{
    dyn_string exceptionInfo;

    string domainEgroup="SCADA-fwACDomain-UNICOS";
    string domainName, domainComment;
    dyn_string privilegeEgroups, privilegeNames;

    fwAccessControl_getDomainFromEgroup(domainEgroup, domainName, domainComment, privilegeEgroups, privilegeNames, exceptionInfo);

    //DebugTN("Domain egroup",domainEgroup);
    //DebugTN("Domain name",domainName);
    //DebugTN("Domain comment", domainComment);
    //DebugTN("Privilege egroups",privilegeEgroups);
    //DebugTN("Privilege names",privilegeNames);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("UNICOS",domainName,"Domain name not correct");
    assertEqual(makeDynString("monitor","operator","expert","admin"), privilegeNames,"Privileges wrong");
    assertTrue(strlen(domainComment)>0,"Domain comment empty");
}

void test_fwAccessControl_Egroups_getRoleFromEgroup()
{
    dyn_string exceptionInfo;
    string roleEgroup="SCADA-fwACRole-ScdAppService--Experts";
    string roleName;
    string roleComment;
    dyn_string accessRights;
    dyn_string privilegeEgroups, memberEgroups, childRoleEgroups;
    dyn_string expectedAccessRights=makeDynString("SYSTEM:Visualize","SYSTEM:Normal user level","SYSTEM:Extended user level",
                                                  "SYSTEM:Administration","SYSTEM:Acknowledge","UNICOS:monitor",
                                                  "UNICOS:operator","UNICOS:expert","UNICOS:admin");
    dyn_string expectedMemberEgroups=makeDynString("SCADA-AppService-admin","ACC-UNICOS-ScadaExpert");

    fwAccessControl_getRoleFromEgroup(roleEgroup, roleName, roleComment, accessRights, privilegeEgroups, memberEgroups, childRoleEgroups, exceptionInfo);

    //DebugTN("RoleEgroup", roleEgroup);
    //DebugTN("RoleName", roleName);
    //DebugTN("RoleComment",roleComment);
    //DebugTN("Access Rights", accessRights);
    //DebugTN("Privilege Egroups", privilegeEgroups);
    //DebugTN("Member Egroups", memberEgroups);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("ScdAppService-Experts",roleName);

    dynSortAsc(expectedAccessRights);
    dynSortAsc(accessRights);
    assertEqual(expectedAccessRights,accessRights);

    dynSortAsc(expectedMemberEgroups);
    dynSortAsc(memberEgroups);
    assertEqual(expectedMemberEgroups,memberEgroups);
}


void test_fwAccessControl_Egroups_getRoleFromEgroup_withHierarchy()
{
    dyn_string exceptionInfo;
    string roleEgroup="test-fwACRole-MyDCS--expert";
    string roleName;
    string roleComment;
    dyn_string accessRights;
    dyn_string privilegeEgroups, memberEgroups, childRoleEgroups;
    dyn_string expectedAccessRights=makeDynString("MyDCS:Monitor","MyDCS:Control");
    dyn_string expectedChildRoleEgroups=makeDynString("test-fwACRole-MyDCS--SubDet1--expert","test-fwACRole-MyDCS--SubDet2--expert");

    fwAccessControl_getRoleFromEgroup(roleEgroup, roleName, roleComment, accessRights, privilegeEgroups, memberEgroups, childRoleEgroups, exceptionInfo);

//    DebugTN("RoleEgroup", roleEgroup);
//    DebugTN("RoleName", roleName);
//    DebugTN("RoleComment",roleComment);
//    DebugTN("Access Rights", accessRights);
//    DebugTN("Privilege Egroups", privilegeEgroups);
//    DebugTN("Member Egroups", memberEgroups);
//    DebugTN("ChildRole Egroups", childRoleEgroups);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("MyDCS-expert",roleName);

    dynSortAsc(expectedAccessRights);
    dynSortAsc(accessRights);
    assertEqual(expectedAccessRights,accessRights);

    // we do not expect any member egroup here!

    dynSortAsc(expectedChildRoleEgroups);
    dynSortAsc(childRoleEgroups);
    assertEqual(expectedChildRoleEgroups,childRoleEgroups);

}


void test_fwAccessControl_egroupToAccessRight()
{
    dyn_string exceptionInfo;
    string egroupName="SCADA-fwACPrivilege-UNICOS-2-operator";
    string accessRight;
    int privNumber;
    fwAccessControl_egroupToAccessRight(egroupName, accessRight, privNumber, exceptionInfo);

    //DebugTN("egroupName",egroupName);
    //DebugTN("accessRight",accessRight);
    //DebugTN("privNumber",privNumber);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("UNICOS:operator",accessRight);
    assertEqual(2,privNumber);
}

void test_fwAccessControl_egroupToAccessRight_NameWithSpace()
{
    dyn_string exceptionInfo;
    string egroupName="SCADA-fwACPrivilege-SYSTEM-2-Normal__user__level";
    string accessRight;
    int privNumber;
    fwAccessControl_egroupToAccessRight(egroupName, accessRight, privNumber, exceptionInfo);

    //DebugTN("egroupName",egroupName);
    //DebugTN("accessRight",accessRight);
    //DebugTN("privNumber",privNumber);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("SYSTEM:Normal user level",accessRight);
    assertEqual(2,privNumber);
}

void test_fwAccessControl_accessRightToEgroup()
{
    string accessRight="UNICOS:admin";
    dyn_string exceptionInfo;

    // ----- Note that the UNICOS domain was correctly setup in the setupSuite
    // ----- so that this test may proceed

    string egroup=fwAccessControl_accessRightToEgroup(accessRight, exceptionInfo);

    //DebugTN("AccessRight",accessRight);
    //DebugTN("Egroup",egroup);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("SCADA-fwACPrivilege-UNICOS-4-admin",egroup);
}

void test_fwAccessControl_accessRightToEgroup_NameWithSpace()
{
    dyn_string exceptionInfo;
    string accessRight="SYSTEM:Normal user level";
    string egroup=fwAccessControl_accessRightToEgroup(accessRight, exceptionInfo);

    //DebugTN("AccessRight",accessRight);
    //DebugTN("Egroup",egroup);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("SCADA-fwACPrivilege-SYSTEM-2-Normal__user__level",egroup);
}


void test_fwAccessControl_domainNameToEgroup()
{
    dyn_string exceptionInfo;
    string domainName="UNICOS";
    string egroup=fwAccessControl_domainNameToEgroup(domainName, exceptionInfo);

    //DebugTN("DomainName",domainName);
    //DebugTN("Egroup",egroup);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertEqual("SCADA-fwACDomain-UNICOS",egroup);
}
