#uses "fwUnitTestComponentAsserts.ctl"



/**@file test_fwCaen.ctl

  @brief This file is a library with the required functions to run fwCaen unit test for the Vertical Slice test bench

*/


/**@brief Prepare the appropiate global environment for all of the test cases in this suite.
This function is called only once before any test is launched.

It means:

   - Import dpList: Pilot OPC UA DPs, test DPs,...
   - Create managers
   - Configure OPC UA clients
   - Start managers

*/
void test_fwCaen_setupSuite()
{
  DebugTN("test_fwCaen_setupSuite() -> Launched...");


  const string OPC_UA_SETUP_FILE  = "OpcUaSetup";
  const string sPILOT_OPC_UA_FILE = "PilotCaenOpcUa";


  int iLoop, iLen;
  string sInstallationPath, sFileName, sDpTree, sOpcUaServer, sOpcUaSubscription;
  dyn_int diDriverNumber;
  dyn_string dsInternalDp, dsInternalDpt, exceptionInfo;


  // Initialize fwDevice
  fwDevice_initialize();

  // Create internal DPs
  DebugTN("test_fwCaen_setupSuite() -> Internal DPs -> Start...");
  dsInternalDp  = makeDynString("_Driver6",
                                "_Driver6_2",
                                "_Driver10",
                                "_Driver10_2",
                                "_Simulator6",
                                "_Simulator10",
                                "_Stat_driver_6_to_data_0",
                                "_Stat_driver_10_to_data_0",
                                "_Stat_Configs_driver_6",
                                "_Stat_2_Configs_driver_6",
                                "_Stat_Configs_driver_10",
                                "_Stat_2_Configs_driver_10");

  dsInternalDpt = makeDynString("_DriverCommon",
                                "_DriverCommon",
                                "_DriverCommon",
                                "_DriverCommon",
                                "_Simulator",
                                "_Simulator",
                                "_Statistics_Connections",
                                "_Statistics_Connections",
                                "_Statistics_DriverConfigs",
                                "_Statistics_DriverConfigs",
                                "_Statistics_DriverConfigs",
                                "_Statistics_DriverConfigs");

  if( CreateInternalDPs(dsInternalDp, dsInternalDpt, exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Error creating internal DPs: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Internal DPs -> Created OK");


  //  Disable default CAEN OPC UA server
  DebugTN("test_fwCaen_setupSuite() -> Disable default CAEN server -> Start...");
  DisableDefaultCaenOpcUaServerConnection(exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    breakTest("test_fwCaen_setupSuite() -> Disable default CAEN server -> Error disabling default CAEN OPC UA server connection, due to: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Disable default CAEN server -> Done OK");


  // Import OPC UA configuration
  DebugTN("test_fwCaen_setupSuite() -> Import OPC UA configuration -> Start...");

  sInstallationPath = getPath(DPLIST_REL_PATH, "", "", (SEARCH_PATH_LEN-1));
  sInstallationPath = sInstallationPath + "test/fwCaen/";

  sFileName = OPC_UA_SETUP_FILE;
  if( ImportDplFile(sInstallationPath, sFileName, exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Import OPC UA configuration  -> Error in the process, details: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Import OPC UA configuration  -> Done OK");



  // Stop simulators, create OPC UA managers and start them
  DebugTN("test_fwCaen_setupSuite() -> Create OPC UA managers -> Start...");
  diDriverNumber = makeDynInt(6, 10);
  iLen           = dynlen(diDriverNumber);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( CreateOpcUaManager(diDriverNumber[iLoop], exceptionInfo) == FALSE )
    {
      breakTest("test_fwCaen_setupSuite() -> Error creating OPC UA manager: " + diDriverNumber[iLoop] + ", due to error: " + exceptionInfo[2]);
    }
    else
    {
      DebugTN("test_fwCaen_setupSuite() -> OPC UA manager with driver number: " + diDriverNumber[iLoop] + ", ready");
    }
  }
  DebugTN("test_fwCaen_setupSuite() -> Create OPC UA managers -> OK");


  // Done with ASCII import:
    // Create OPC UA connection to CAEN Venus OPC UA server
    // Create OPC UA connection to Pilot OPC UA server

  DebugTN("test_fwCaen_setupSuite() -> Import Pilot Caen OPC UA setup -> Start...");
  sFileName = sPILOT_OPC_UA_FILE;
  if( ImportDplFile(sInstallationPath, sFileName, exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Import Pilot Caen OPC UA setup -> Error in the process, details: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Import Pilot Caen OPC UA setup -> Done OK");


  // Instantiate CAEN HW using JCOP calls
  DebugTN("test_fwCaen_setupSuite() -> Instantiate lab crate with JCOP calls -> Start...");
  if( InstantiateLabHardware(exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Instantiate lab crate with JCOP calls -> Error in the HW creation, details: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Instantiate lab crate with JCOP calls -> Done OK");


  // Reconfigure CAEN devices to use VenusOPC UA simulator
  DebugTN("test_fwCaen_setupSuite() -> Reconfigure CAEN devices to Venus OPC UA server -> Start...");

  sDpTree            = "CAEN/simSY4527";
  sOpcUaServer       = "CaenVerticalTest";
  sOpcUaSubscription = "CaenVerticalTest_Subscription";

  if( ReconfigureOpcUADpTree(sDpTree, sOpcUaServer, sOpcUaSubscription, exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Reconfigure CAEN devices to Venus OPC UA server -> Error in the process, details: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Reconfigure CAEN devices to Venus OPC UA server -> Done OK");


   // Accept certificates
  DebugTN("test_fwCaen_setupSuite() -> Accept all OPC UA certificates -> Start...");
  if( AcceptAllOpcUaCertificates(exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Accept all OPC UA certificates -> Error in the HW creation, details: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Accept all OPC UA certificates -> Done OK");


  // Check connection with PILOT OPC UA server
  DebugTN("test_fwCaen_setupSuite() -> Check Caen PILOT OPC UA server connection -> Start...");
  if( CheckOpcUaServerConnection("PilotUA", exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Check Caen PILOT OPC UA server connection -> Error in the HW creation, details: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Check Caen PILOT OPC UA server connection -> Done OK");


  // Check connection with Caen Venus OPC UA server
  DebugTN("test_fwCaen_setupSuite() -> Check Caen Venus OPC UA server connection -> Start...");
  if( CheckOpcUaServerConnection("CaenVerticalTest", exceptionInfo) == FALSE )
  {
    breakTest("test_fwCaen_setupSuite() -> Check Caen Venus OPC UA server connection -> Error in the HW creation, details: " + exceptionInfo[2]);
  }
  DebugTN("test_fwCaen_setupSuite() -> Check Caen Venus OPC UA server connection -> Done OK");

}




/**@brief Check OPC UA server connection


*/
bool CheckOpcUaServerConnection(string sOpcUaServerConnection, dyn_string &exceptionInfo)
{
  bool bOpcUaServerConnected;
  int iRes, iDriverNumber;
  string sOpcServerConnectionDpe, sOpcServerStateDpe, sManagerType, sServerState;
  dyn_int diDriverNumbers;
  dyn_errClass deError;


  // Check connection name exists
  if( dpExists("_" + sOpcUaServerConnection ) == FALSE )
  {
    fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Error OPC UA server connection DP does not exist: _" + sOpcUaServerConnection, "");
    return FALSE;
  }


  // Get driver number for OPC UA connection name
  if( GetDriverNumberFromOpcUaConnectionName(sOpcUaServerConnection, iDriverNumber, exceptionInfo) == FALSE )
  {
    fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Error getting driver number for OPC UA server connection: " + sOpcUaServerConnection + " due to: " + exceptionInfo[2], "");
    return FALSE;
  }


  // Get the OPC UA server connection status values
  sOpcServerConnectionDpe = "_" + sOpcUaServerConnection + ".State.ConnState";
  sOpcServerStateDpe      = "_" + sOpcUaServerConnection + ".State.ServerState";


  iRes = dpGet(sOpcServerConnectionDpe,           bOpcUaServerConnected,
               sOpcServerStateDpe,                sServerState,
               "_Connections.Driver.ManNums",     diDriverNumbers,
               "_Driver" + iDriverNumber + ".DT", sManagerType);
  if( iRes != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Getting OPC UA server state: " + sOpcUaServerConnection, "");
    return FALSE;
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Getting OPC UA server state: " + sOpcUaServerConnection + " due to: " + getErrorText(deError) , "");
      return FALSE;
    }
  }

  // Check driver number running
  if( dynContains(diDriverNumbers, iDriverNumber) <= 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Error, there is not any manager with driver number: " + iDriverNumber + " is not running", "");
    return FALSE;
  }


  // Check if the running manager is an OPC UA client
  if( sManagerType != "OPCUAC" )
  {
    fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Error, manager running is not an OPC UA client manager", "");
    return FALSE;
  }


  // Check OPC UA client is connected to the server
  if( bOpcUaServerConnected == FALSE )
  {
    fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Error, OPC UA client number" + iDriverNumber + " is not connected to the server: " + sOpcUaServerConnection, "");
    return FALSE;
  }

  // Check OPC UA client is running
  if( sServerState != "Running" )
  {
    fwException_raise(exceptionInfo, "ERROR", "CheckOpcUaServerConnection() -> Error, OPC UA client number" + iDriverNumber + " is connected to the server: " + sOpcUaServerConnection + " but server is not running", "");
    return FALSE;
  }


  return TRUE;
}




/**@brief Identify OPC UA driver number from connection name


*/
bool GetDriverNumberFromOpcUaConnectionName(string sOpcUaServerConnection, int &iDriverNumber, dyn_string &exceptionInfo)
{
  int iLoop, iLen, iRes;
  string sDriverDp;
  dyn_string dsDriverConnections, dsServerConnections;
  dyn_errClass deError;


  iDriverNumber       = -1;
  dsDriverConnections = dpNames("*.Config.Servers", "_OPCUA");
  iLen                = dynlen(dsDriverConnections);

  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    iRes = dpGet(dsDriverConnections[iLoop], dsServerConnections);
    if( iRes != 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "GetDriverNumberFromOpcUaConnectionName() -> Getting OPC UA connections for driver: " + dsDriverConnections[iLoop], "");
      return FALSE;
    }
    else
    {
      deError = getLastError();
      if( dynlen(deError) > 0 )
      {
        fwException_raise(exceptionInfo, "ERROR", "GetDriverNumberFromOpcUaConnectionName() -> Getting OPC UA connections for driver: " + dsDriverConnections[iLoop] + " due to: " + getErrorText(deError) , "");
        return FALSE;
      }
    }

    if( dynContains(dsServerConnections, sOpcUaServerConnection) > 0 )
    {
      sDriverDp = dpSubStr(dsDriverConnections[iLoop], DPSUB_DP);
      sscanf(sDriverDp, "_OPCUA%d", iDriverNumber);
      break;
    }
  }

  if( iDriverNumber != -1 )
  {
    return TRUE;
  }
  else
  {
    fwException_raise(exceptionInfo, "ERROR", "GetDriverNumberFromOpcUaConnectionName() -> Error was not possible to link the OPC UA server connection: " + sOpcUaServerConnection + " with its driver number.", "");
    return FALSE;
  }
}




/**@brief Accept all OPC UA certificates rejected in the project


*/
bool AcceptAllOpcUaCertificates(dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sRejectedCertificatesPath, sAcceptedCertificatesPath, sFileToMove;
  dyn_string dsRejectedCertiticates;


  // Find certificate path, check first certstore path in config file
  for(  iLoop = 1 ; iLoop <= SEARCH_PATH_LEN ; iLoop++ )
  {
    paCfgReadValue(getPath(CONFIG_REL_PATH, "config", getActiveLang(), iLoop), "opcua", "certificateStore", sRejectedCertificatesPath);
    if( sRejectedCertificatesPath != "" )
    {
      // Certificate store defined in the config file
      break;
    }
    else
    {
      sRejectedCertificatesPath = getPath(DATA_REL_PATH, "", getActiveLang(), iLoop) + "opcua/client";
    }


    if( isdir(sRejectedCertificatesPath) )
    {
      // Certificate folders exists in the folder
      break;
    }
    else
    {
      sRejectedCertificatesPath = "";
    }
  }


  // Get accepted and rejected certificates paths
  sAcceptedCertificatesPath = sRejectedCertificatesPath + "/PKI/CA/certs/";
  sRejectedCertificatesPath = sRejectedCertificatesPath + "/PKI/CA/rejected/";
  if(  _WIN32 )
  {
    if( strreplace(sRejectedCertificatesPath, "/", "\\") == -1)
    {
      fwException_raise(exceptionInfo, "ERROR", "AcceptAllOpcUaCertificates() -> Error in the conversion to Windows of the rejected certificate path: " + sRejectedCertificatesPath, "");
      throw( makeError("", PRIO_SEVERE, ERR_IMPL, 86, exceptionInfo) );
      return FALSE;
    }

    if( strreplace(sAcceptedCertificatesPath, "/", "\\") == -1)
    {
      fwException_raise(exceptionInfo, "ERROR", "AcceptAllOpcUaCertificates() -> Error in the conversion to Windows of the accepted certificate path: " + sAcceptedCertificatesPath, "");
      throw( makeError("", PRIO_SEVERE, ERR_IMPL, 86, exceptionInfo) );
      return FALSE;
    }
  }


  // Get rejected certificates
  dsRejectedCertiticates = getFileNames(sRejectedCertificatesPath, "*.der", FILTER_FILES);


  // Move rejected certificates to approved
  iLen = dynlen(dsRejectedCertiticates);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    sFileToMove = sRejectedCertificatesPath + dsRejectedCertiticates[iLoop];
    if( moveFile(sFileToMove, sAcceptedCertificatesPath) == FALSE )
    {
      fwException_raise(exceptionInfo, "ERROR", "AcceptAllOpcUaCertificates() -> Error moving rejected certificate: " + sFileToMove + " to path " + sAcceptedCertificatesPath, "");
      throw( makeError("", PRIO_SEVERE, ERR_IMPL, 86, exceptionInfo) );
      return FALSE;
    }
    else
    {
      DebugTN("AcceptAllOpcUaCertificates() -> Certificate approved OK: " + sFileToMove);
    }
  }

  return TRUE;
}




/**@brief Disables the default CAEN OPC UA server connection


*/
void DisableDefaultCaenOpcUaServerConnection(dyn_string &exceptionInfo)
{
  const string sCAEN_DEFAULT_CONNECTION_DPE = "_OPCUA_CAEN.Config.Active";


  int iRes;
  dyn_errClass deError;


  iRes = dpSetWait(sCAEN_DEFAULT_CONNECTION_DPE, FALSE);
  if( iRes != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "DisableDefaultCaenOpcUaServerConnection() -> Error setting to FALSE " + sCAEN_DEFAULT_CONNECTION_DPE, "");
    throw( makeError("", PRIO_SEVERE, ERR_IMPL, 86, exceptionInfo) );
    return;
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "DisableDefaultCaenOpcUaServerConnection() -> Error setting to FALSE " + sCAEN_DEFAULT_CONNECTION_DPE + " due to: " + getErrorText(deError) , "");
      throw( makeError("", PRIO_SEVERE, ERR_IMPL, 86, exceptionInfo) );
    }
    else
    {
      DebugTN("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute -> Sent OK");
    }
  }

}




/**@brief Check if desired internal DPs exists, if not it create them


*/
bool CreateInternalDPs(dyn_string dsInternalDp, dyn_string dsInternalDpt, dyn_string &exceptionInfo)
{
  int iLoop, iLen, iRes;
  dyn_errClass deError;


  iLen = dynlen(dsInternalDp);
  if( iLen != dynlen(dsInternalDpt) )
  {
    fwException_raise(exceptionInfo, "ERROR", "CreateInternalDPs() -> Different number of elements of DPs and DPTs", "");
    return FALSE;
  }


  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( dpExists(dsInternalDp[iLoop]) == FALSE )
    {
      DebugTN("CreateInternalDPs() -> " + iLoop + " / " + iLen + " -> Creating internal DP: " + dsInternalDp[iLoop] + ", [" + dsInternalDpt[iLoop] + "]");

      iRes = dpCreate(dsInternalDp[iLoop], dsInternalDpt[iLoop]);
      if( iRes != 0 )
      {
        fwException_raise(exceptionInfo, "ERROR", "CreateInternalDPs() -> Creating DP: " + dsInternalDp[iLoop] + ", [" + dsInternalDpt[iLoop] + "]", "");
        return FALSE;
      }
      else
      {
        deError = getLastError();
        if( dynlen(deError) > 0 )
        {
          fwException_raise(exceptionInfo, "ERROR", "CreateInternalDPs() -> Creating DP: " + dsInternalDp[iLoop] + ", [" + dsInternalDpt[iLoop] + "] -> Error code: " + getErrorCode(deError) + ": " + getErrorText(deError) , "");
          return FALSE;
        }
        else
        {
          DebugTN("CreateInternalDPs() -> " + iLoop + " / " + iLen + " -> Created internal DP: " + dsInternalDp[iLoop] + ", [" + dsInternalDpt[iLoop] + "] OK");
        }
      }

    }// DP exists
    else
    {
      DebugTN("CreateInternalDPs() -> " + iLoop + " / " + iLen + " -> internal DP: " + dsInternalDp[iLoop] + ", [" + dsInternalDpt[iLoop] + "] already exists");
    }

  }// For

  return TRUE;
}





bool CreateOpcUaManager(int iDriverNumber, dyn_string &exceptionInfo)
{
  bool bOpcUaExists, bSimulatorExists, bIsRunning;
  int iRes, iLen, iLoop;
  dyn_dyn_mixed ddmAllManagers;


  iRes = fwInstallationManager_getAllInfoFromPvss(ddmAllManagers);
  if( iRes != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Error getting existant managers information", "");
    return FALSE;
  }


  iLen = dynlen(ddmAllManagers);
  DebugTN("CreateOpcUaManagers() -> Retrieved all project managers (" + iLen + ")");


  // Check if OPCUA -num iDriverNumber and simulator exists
  bOpcUaExists     = FALSE;
  bSimulatorExists = FALSE;


  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    // Check if OPC UA client is already defined
    if( (ddmAllManagers[iLoop][1] == "WCCILsim")             &&
        (ddmAllManagers[iLoop][6] == "-num " + iDriverNumber) )
    {
      bSimulatorExists = TRUE;
      DebugTN("CreateOpcUaManager() -> Simulator manager with driver number: " + iDriverNumber + " already exists");
    }
  }


  if( bSimulatorExists )
  {
    // Change Simulator manager to manual
    if( fwInstallationManager_setMode("WCCILsim", "-num " + iDriverNumber, "manual") != 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Change of startup mode for simulator manager, driver number " + iDriverNumber + " failed in fwInstallationManager_setMode()", "");
      return FALSE;
    }
    else
    {
      DebugTN("CreateOpcUaManager() -> Change of startup mode for Simulator manager, driver number " + iDriverNumber + " to manual OK");
    }


    // Check if Simulator manager is running
    if( fwInstallationManager_isRunning("WCCILsim", "-num " + iDriverNumber, bIsRunning) != 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Check if Simulator manager, driver number " + iDriverNumber + " is running failled in fwInstallationManager_isRunning()", "");
      return FALSE;
    }
    else
    {
      DebugTN("CreateOpcUaManager() -> Simulator manager with driver number " + iDriverNumber + " is: " + (bIsRunning?"Started":"Stopped") );

      // If simulator manager is running, stop it and delay 5 seconds to close it.
      if( bIsRunning )
      {
        if( fwInstallationManager_command("STOP", "WCCILsim", "-num " + iDriverNumber) != 0 )
        {
          fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Error stopping simulation manager with driver number " + iDriverNumber + " in fwInstallationManager_command()", "");
          return FALSE;
        }
        DebugTN("CreateOpcUaManager() -> Simulation manager with with driver number " + iDriverNumber + " ordered to stop, waiting 5 seconds...");
        delay(5);
      }
    }
  }


  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    // Check if OPC UA client is already defined
    if( (ddmAllManagers[iLoop][1] == "WCCOAopcua")             &&
        (ddmAllManagers[iLoop][6] == "-num " + iDriverNumber) )
    {
      bOpcUaExists = TRUE;
      DebugTN("CreateOpcUaManager() -> OPC UA manager with driver number: " + iDriverNumber + " already exists");
    }
  }


  // Create OPC UA manager if doesn't exist
  if( !bOpcUaExists )
  {
    DebugTN("CreateOpcUaManager() -> OPC UA manager with driver number: " + iDriverNumber + " doesn't exist. It will be created");

    iRes = fwInstallationManager_add("WCCOAopcua", "manual", 30, 5, 3, "-num " + iDriverNumber);
    if( iRes == 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Creation of OPC UA manager, driver number " + iDriverNumber + " failed calling fwInstallationManager_add()", "");
      return FALSE;
    }
    else
    {
      DebugTN("CreateOpcUaManager() -> OPC UA manager with driver number: " + iDriverNumber + " created OK");
    }
  }
  else
  {
    // Change OPC UA manager to manual
    if( fwInstallationManager_setMode("WCCOAopcua", "-num " + iDriverNumber, "manual") != 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Change of startup mode for OPC UA manager, driver number " + iDriverNumber + " failed in fwInstallationManager_setMode()", "");
      return FALSE;
    }
    else
    {
      DebugTN("CreateOpcUaManager() -> Change of startup mode for OPC UA manager, driver number " + iDriverNumber + " to manual OK");
    }
  }


  // Check if manager is running
  if( fwInstallationManager_isRunning("WCCOAopcua", "-num " + iDriverNumber, bIsRunning) != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Check if OPC UA manager, driver number " + iDriverNumber + " is running failled in fwInstallationManager_isRunning()", "");
    return FALSE;
  }
  DebugTN("CreateOpcUaManager() -> OPC UA manager, driver number " + iDriverNumber + " status is: " + (bIsRunning?"Started":"Stopped") );

  if( !bIsRunning )
  {
    if( fwInstallationManager_restart("WCCOAopcua", "-num " + iDriverNumber) != 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CreateOpcUaManagers() -> Error restarting OPC UA manager, driver number " + iDriverNumber + " in fwInstallationManager_restart()", "");
      return FALSE;
    }
    DebugTN("CreateOpcUaManager() -> OPC UA manager, driver number " + iDriverNumber + " ordered to restart");
  }

  return TRUE;
}





bool ImportDplFile(string sPath, string sFile, dyn_string &exceptionInfo)
{
  int iSystemCall;
  string sCommand, sAsciiTool, sUser, sPassword, sImportLog, sImportLogError, sFullName;
  dyn_string dsConfigKey, dsConfigAnswer;


  // Generate file paths
  sFullName       = sPath + sFile + ".dpl";
  sImportLog      = getPath(LOG_REL_PATH, "", "", 1) + sFile + "_info.log";
  sImportLogError = getPath(LOG_REL_PATH, "", "", 1) + sFile + "_error.log";
  if(_WIN32)
  {
    strreplace(sFullName,       "/", "\\");
    strreplace(sImportLog,      "/", "\\");
    strreplace(sImportLogError, "/", "\\");
  }


  // Get ASCII manager binary name
  sAsciiTool = getComponentName(ASCII_COMPONENT);
  if(_WIN32)
  {
    sAsciiTool = sAsciiTool + ".exe";
  }


  // Get binary path
  sCommand = getPath(BIN_REL_PATH, sAsciiTool);
  if(_WIN32)
  {
    strreplace(sCommand, "/", "\\");
  }


  // Get user name and password
  dsConfigKey = makeDynString("userName", "password");
  paCfgReadValue(PROJ_PATH + "config/config", "general", dsConfigKey, dsConfigAnswer);
  if( (dsConfigAnswer[1]  == "")                ||
      (getUserName()      != dsConfigAnswer[1]) ||
      (!getUserPermission(4))                     ) // no default user or other user or not enough permission
  {
    fwException_raise(exceptionInfo, "ERROR", "ImportDplFile() -> Error: not default user or other user not enough permissions", "");
    return FALSE;
  }
  else
  {
    sUser     = dsConfigAnswer[1];
    sPassword = dsConfigAnswer[2];
  }
  sCommand = sCommand + " -user " + sUser + ":" + sPassword + ((sPassword != "")? " " : "");


  // Add data port and event port
  sCommand = sCommand + " -data "  + (dataHost()) [1] + ":" + dataPort();
  sCommand = sCommand + " -event " + (eventHost())[1] + ":" + eventPort();


  // Add import file
  sCommand = sCommand + " -in \"" + sFullName + "\"";


  // Add overwrite DPT and load data with alarm handling
  sCommand = sCommand + " -yes";
  sCommand = sCommand + " -inactivateAlert";


  // Add logging
  sCommand = sCommand + " -log +stderr";
  sCommand = sCommand + " -log -file > " + sImportLog + " 2> " + sImportLogError;


  //Launch the system call
  if(_WIN32)
  {
    sCommand = "cmd /c " + sCommand;
  }
  DebugTN("sCommand = ", sCommand);

  iSystemCall = system(sCommand);
  switch( iSystemCall )
  {
    case -1:
      fwException_raise(exceptionInfo, "ERROR", "ImportDplFile() -> Error: system call without arguments", "");
      return FALSE;
      break;

    case 0:
      DebugTN("ImportDplFile() -> DpList file: " + sPath + sFile + ", imported OK");
      break;

    default:
      fwException_raise(exceptionInfo, "ERROR", "ImportDplFile() -> Error: system call returned an error: " + iSystemCall, "");
      return FALSE;
      break;
  }

  return TRUE;
}





/**@brief Cretes the CAEN hardware devices (BE-ICS lab) to connec to it.
*/
bool InstantiateLabHardware(dyn_string &exceptionInfo)
{
  //Crate:
  //  - Model: SY4527
  //  - Name: simSY4527
  string sSystemName, sCrateName, sCrateType, sCrateModel, sCrateDescription, sAddressType, sAlarmType;
  dyn_string dsParentDevice, dsDevice;

  sSystemName       = getSystemName();
  sCrateName        = "simSY4527";
  sCrateType        = "CAEN SY1527";
  sCrateModel       = "SY4527";
  sCrateDescription = "BE ICS lab crate CAEN SY4527";
  sAddressType      = "DEFAULT";      //OPCUA
  sAlarmType        = "DEFAULT";

  dsParentDevice = makeDynString(sSystemName + "CAEN",
                                 "",
                                 "",
                                 "");

  fwDevice_getDpType(sCrateType, dsDevice[fwDevice_DP_TYPE], exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("Error getting type for crate: " + sCrateName + " -> " + exceptionInfo);
    return FALSE;
  }
  dsDevice[fwDevice_MODEL]   = sCrateModel;
  dsDevice[fwDevice_COMMENT] = sCrateDescription;
  dsDevice[fwDevice_DP_NAME] = sCrateName;

  createDevice(dsDevice, dsParentDevice, sAddressType, exceptionInfo);
  if( dynlen(exceptionInfo)> 0 )
  {
    DebugTN("Error creating crate: " + sCrateName + " -> ", exceptionInfo);
    return FALSE;
  }

  configureDevice(dsDevice, sAddressType, sAlarmType, exceptionInfo);
  if( dynlen(exceptionInfo)> 0 )
  {
    DebugTN("Error configuring  crate: " + sCrateName + " -> " + exceptionInfo );
    return FALSE;
  }
  DebugTN("Crate created: " + sCrateName);



  // Create boards
  dsParentDevice = dsDevice;


  //Boards:
  //  - Board:
  //    - Model: A1832
  //    - Slot: 0
  createFullBoard(dsParentDevice, "board00", "CAEN SY1527 Board", "A1832", "My A1832 board", "CAEN Channel", "CAEN SY1527 Channel");

  //  - Board:
  //    - Model: A1519
  //    - Slot: 3
  createFullBoard(dsParentDevice, "board03", "CAEN SY1527 Board", "A1519", "My A1519N board", "CAEN Channel", "CAEN SY1527 Channel");

  //  - Board:
  //    - Model: A1832
  //    - Slot: 5
  createFullBoard(dsParentDevice, "board05", "CAEN SY1527 Board", "A1832", "My A1832 board", "CAEN Channel", "CAEN SY1527 Channel");

  return TRUE;
}






void createDevice(dyn_string &device, dyn_string parentDevice, string addressType, dyn_string &exceptionInfo)
{
  bool isOk;
  string deviceName, deviceNameChecked, message;
  dyn_string addressParameters;


  // Check for valid dp name (only device name is checked because of /'s)
  fwDevice_getName(device[fwDevice_DP_NAME], deviceName, exceptionInfo);
  deviceNameChecked = deviceName;

  if( nameCheck(deviceNameChecked, NAMETYPE_DP) )
  {
    fwException_raise(exceptionInfo, "ERROR", deviceName + " is not a valid device name.", "");
    return;
  }


  // Check if address can be set
  if( addressType != fwDevice_ADDRESS_NONE )
  {
    if( addressType != fwDevice_ADDRESS_DEFAULT )
    {
      fwDevice_getAddressDefaultParams(device[fwDevice_DP_TYPE],
                                       addressParameters,
                                       exceptionInfo,
                                       device[fwDevice_MODEL],
                                       "",
                                       addressType);
      if( dynlen(exceptionInfo) > 0 )
      {
        return;
      }
    }
    else
    {
      addressParameters = addressType;
    }


    fwDevice_checkAddress(device, addressParameters, isOk, exceptionInfo);
    if (dynlen(exceptionInfo))
    {
      return;
    }


    if( !isOk )
    {
      fwDevice_getAddressDefaultParams(device[fwDevice_DP_TYPE], addressParameters, exceptionInfo, device[fwDevice_MODEL], "", addressType);
      fwException_raise(exceptionInfo,
                        "ERROR",
                        "Cannot create device because the driver is not running (number " + addressParameters[fwDevice_ADDRESS_DRIVER_NUMBER] + ")",
                        "");
      return;
    }
  }


  // Create device
  fwDevice_create(device, parentDevice, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }

  device[fwDevice_DP_NAME] = parentDevice[fwDevice_DP_NAME] + fwDevice_HIERARCHY_SEPARATOR + device[fwDevice_DP_NAME];

}






void configureDevice(dyn_string device, string addressType, string alarmType, dyn_string &exceptionInfo)
{
  dyn_string addressParameters;


  // Set the addresses for the device
  if( addressType!=fwDevice_ADDRESS_NONE )
  {
    if( addressType != fwDevice_ADDRESS_DEFAULT )
    {
      fwDevice_getAddressDefaultParams(device[fwDevice_DP_TYPE],
                                       addressParameters,
                                       exceptionInfo,
                                       device[fwDevice_MODEL],
                                       "",
                                       addressType);
      if( dynlen(exceptionInfo) > 0 )
      {
        fwExceptionHandling_display(exceptionInfo);
        return;
      }
    }
    else
    {
      addressParameters = addressType;
    }


    fwShowProgressBar("Setting hardware connection...");
    fwDevice_setAddress(device[fwDevice_DP_NAME],
                        addressParameters,
                        exceptionInfo);
  }


  // Set the alerts for the device
  if( alarmType == fwDevice_ALERT_DEFAULT )
  {
    fwShowProgressBar("Setting alarms for...");
    fwDevice_setAlert(device[fwDevice_DP_NAME], fwDevice_ALERT_SET, exceptionInfo);
  }
}





void createFullBoard(dyn_string dsParentDevice, string sBoardName, string sBoardType, string sBoardModel, string sBoardDescription, string sChannelType, string sChannelModel)
{
  int iSlots, iStartNumber, iChildWidth, iLoop;
  string sAddressType, sAlarmType;
  dyn_string dsDevice, dsChildDevice, dsChildren, exceptionInfo;
  dyn_dyn_string ddsModelProperties, ddsChildModelProperties;


  sAddressType      = "DEFAULT"; //OPCUA
  sAlarmType        = "DEFAULT";


  fwDevice_getDpType(sBoardType, dsDevice[fwDevice_DP_TYPE], exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("Error getting type for board: " + sBoardName + " -> " + exceptionInfo);
    return;
  }
  dsDevice[fwDevice_MODEL]   = sBoardModel;
  dsDevice[fwDevice_COMMENT] = sBoardDescription;

  fwDevice_getModelProperties(dsDevice,	ddsModelProperties, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("Error getting model properties from board: " + sBoardName + " -> " + exceptionInfo);
    return;
  }

  iSlots       = ddsModelProperties[fwDevice_MODEL_SLOTS]          [1];
  iStartNumber = ddsModelProperties[fwDevice_MODEL_STARTING_NUMBER][1];

  fwDevice_getDpType(sChannelType, dsChildDevice[fwDevice_DP_TYPE], exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("Error getting channel type for board: " + sBoardName + " -> " + exceptionInfo);
    return;
  }
  dsChildDevice[fwDevice_MODEL] = sChannelModel;

  dsDevice[fwDevice_DP_NAME] = sBoardName;
  createDevice(dsDevice, dsParentDevice, sAddressType, exceptionInfo);
  if( dynlen(exceptionInfo)> 0 )
  {
    DebugTN("Error creating board: " + sBoardName + " -> ", exceptionInfo);
    return;
  }

  fwDevice_getModelProperties(dsChildDevice, ddsChildModelProperties, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("Error getting model properties from channel: " + dsChildDevice + " -> " + exceptionInfo);
    return;
  }
  iChildWidth = ddsChildModelProperties[fwDevice_MODEL_WIDTH][1];

  for( iLoop = iStartNumber ; iLoop < iStartNumber + iSlots ; iLoop += iChildWidth )
  {
    fwDevice_getDefaultName(dsChildDevice, iLoop, dsChildDevice[fwDevice_DP_NAME], exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      DebugTN("Error getting default name for child: " + dsChildDevice + " -> " + exceptionInfo);
      return;
    }

    createDevice(dsChildDevice, dsDevice, sAddressType, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      DebugTN("Error creating child device: " + dsChildDevice + " -> " + exceptionInfo);
      return;
    }
  }

  configureDevice(dsDevice, sAddressType, sAlarmType, exceptionInfo);
  if( dynlen(exceptionInfo)> 0 )
  {
    DebugTN("Error configuring board: " + sBoardName + " -> " + exceptionInfo );
    return;
  }
  fwDevice_getChildren(dsDevice[fwDevice_DP_NAME], fwDevice_HARDWARE, dsChildren, exceptionInfo);
  if( dynlen(exceptionInfo)> 0 )
  {
    DebugTN("Error getting childrens for board: " + sBoardName + " -> " + exceptionInfo );
    return;
  }

  for( iLoop = 1 ; iLoop <= dynlen(dsChildren) ; iLoop++ )
  {
    dsChildDevice[fwDevice_DP_NAME] = dsChildren[iLoop];
    configureDevice(dsChildDevice, sAddressType, sAlarmType, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      DebugTN("Error configuring channel: " + dsChildDevice + " -> " + exceptionInfo );
      return;
    }
  }

  DebugTN("Board : " + sBoardName + " -> Model: " + sBoardModel + " created OK");

}





/**@brief Prepare the appropiate environment before run each test case in the suite.
This function is called each time before any test case is called.

It means:

   - Import dpList to set all Crates to the default values
   - Wait until the conditions are meet.

*/
void test_fwCaen_setup()
{
  bool bMute, bEnableCounter;
  int iSum, iRes;
  dyn_int diScadaCounter;
  dyn_string exceptionInfo;
  dyn_errClass deError;


  // Mute Pilot OPC UA server and enable counters
  DebugTN("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> Start...");

  iRes = dpSetWait("PilotSimulator_0.Settings.OpcUaMute",          TRUE,
                   "PilotSimulator_0.Settings.EnableScadaCounter", TRUE);
  if( iRes != 0 )
  {
    breakTest("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> setting DP unknown, PilotSimulator_0: Settings.OpcUaMute, Settings.EnableScadaCounter");
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      breakTest("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> error code: " + getErrorCode(deError) + ": " + getErrorText(deError));
    }
    else
    {
      DebugTN("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> Sent OK");
    }
  }

  DebugTN("test_fwCaen_setup() -> Delay 30 seconds to send command and readback the command by the WinCC OA OPC UA client");
  delay(30);

  iRes = dpGet("PilotSimulator_0.ReadBackSettings.OpcUaMute",  bMute,
               "PilotSimulator_0.Settings.EnableScadaCounter", bEnableCounter);
  if( iRes != 0 )
  {
    breakTest("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> readback DP unknown, PilotSimulator_0: Settings.OpcUaMute, Settings.EnableScadaCounter");
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      breakTest("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> readback error, error code: " + getErrorCode(deError) + ": " + getErrorText(deError));
    }
    else
    {
      DebugTN("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> Readback OK, bMute = " + bMute + " bEnableCounter = " + bEnableCounter);
    }
  }

  if( bMute != TRUE )
  {
    breakTest("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> Command sent, but readback failled");
  }

  if( bEnableCounter != TRUE )
  {
    breakTest("test_fwCaen_setup() -> Set Venus Caen OPC UA in mute, enable counter -> Command sent, but readback failled");
  }
  DebugTN("test_fwCaen_setup() -> Venus Caen OPC UA server mute, enable counter -> OK");


  // Reset counters in Pilot OPC UA server
  DebugTN("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> Start...");
  iRes = dpSetWait("PilotSimulator_0.Settings.CountersReset", TRUE);
  if( iRes != 0 )
  {
    breakTest("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> setting DP unknown, PilotSimulator_0.Settings.CountersReset");
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      breakTest("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> setting DP error, error code: " + getErrorCode(deError) + ": " + getErrorText(deError));
    }
    else
    {
      DebugTN("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> Sent OK");
    }
  }


  DebugTN("test_fwCaen_setup() -> Delay 15 seconds to send the counter reset command and readback the command by the WinCC OA OPC UA client");
  delay(15);


  iRes = dpGet("PilotSimulator_0.Actual.ScadaCounters.Counters", diScadaCounter);
  if( iRes != 0 )
  {
    breakTest("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> counter DP unknown, PilotSimulator_0.Actual.ScadaCounters.Counters");
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      breakTest("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> read counter DP error, error code: " + getErrorCode(deError) + ": " + getErrorText(deError));
    }
    else
    {
      DebugTN("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> Counters read OK");
    }
  }

  iSum = dynSum(diScadaCounter);
  if( iSum != 0 )
  {
    breakTest("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> Error: Pilot CAEN OPC UA counters didn't reset");
  }
  DebugTN("test_fwCaen_setup() -> Reset Pilot Caen OPC UA counters -> reset OK");


  // Reset counters in WinCC OA
    // To be done...

}





bool ReconfigureOpcUADpTree(string sDpTree, string sOpcUaServer, string sOpcUaSubscription, dyn_string &exceptionInfo)
{
  int iLoop, iLen, iLenFilter, iRes;
  dyn_bool dbConfigExists, dbIsActive;
  dyn_string dsDpes, dsDpesAddress, exceptionInfo;
  dyn_anytype daAddressType;
  dyn_errClass deError;
  dyn_dyn_anytype ddaConfig;


  dsDpes = dpNames(sDpTree + "**.**:_distrib.._type");
  iLen   = dynlen(dsDpes);

  iRes = dpGet(dsDpes, daAddressType);
  if( iRes != 0 )
  {
    DebugTN("ReconfigureOpcUADpTree() -> Error detecting address type for the CAEN setup");
    return FALSE;
  }
  deError = getLastError();
  if( dynlen(deError) > 0 )
  {
    DebugTN("ReconfigureOpcUADpTree() -> Error getting address type, due to: " + getErrorText(deError));
    return FALSE;
  }

  // Filtering DPEs with addresses
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( daAddressType[iLoop] != DPCONFIG_NONE )
    {
      dynAppend(dsDpesAddress, dpSubStr(dsDpes[iLoop], DPSUB_SYS_DP_EL));
    }
  }
  iLenFilter = dynlen(dsDpesAddress);
  DebugTN("ReconfigureOpcUADpTree() -> Initial: " + iLen + ", filtered: " + iLenFilter);


  // Change addressing
  fwPeriphAddress_getMany(dsDpesAddress,
                          dbConfigExists,
                          ddaConfig,
                          dbIsActive,
                          exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("ReconfigureOpcUADpTree() -> Error getting full addressing details for all the crates");
    return FALSE;
  }


  for( iLoop = 1 ; iLoop <= iLenFilter ; iLoop++ )
  {
    ddaConfig[iLoop][fwPeriphAddress_OPCUA_SERVER_NAME]  = "CaenVerticalTest";
    ddaConfig[iLoop][fwPeriphAddress_OPCUA_SUBSCRIPTION] = "CaenVerticalTest_Subscription";
  }


  fwPeriphAddress_setMany(dsDpesAddress,
                          ddaConfig,
                          exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("ReconfigureOpcUADpTree() -> Error setting full addressing details for all the crates");
    return FALSE;
  }


  return TRUE;

}






/**@brief Called after each case test

*/
void test_fwCaen_teardown()
{
  DebugTN("test_fwCaen_teardown() -> Launched...");

  // Mute Pilot OPC UA server

}





/**@brief Called after all test case rutines have been run

*/
void test_fwCaen_teardownSuite()
{
  int iLen, iLoop, iRes;
  dyn_string dsDpes;
  dyn_errClass deError;


  DebugTN("test_fwCaen_teardownSuite() -> Launched...");
  dsDpes = dpNames("CAEN/**.");
  iLen   = dynlen(dsDpes);


  DebugTN("test_fwCaen_teardownSuite() -> " + iLen + " DPs to remove");
  for( iLoop  = 1 ; iLoop <= iLen ; iLoop++ )
  {

    iRes = dpDelete(dsDpes[iLoop]);
    if( iRes != 0 )
    {
      breakTest("test_fwCaen_teardownSuite() -> Error removing CAEN device DP: " + dsDpes[iLoop]);
    }
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      breakTest("test_fwCaen_teardownSuite() -> Error removing CAEN device DP: " + dsDpes[iLoop] + " due to: " + getErrorText(deError));
    }

  }

  DebugTN("test_fwCaen_teardownSuite() -> all DPs removed");
  DebugTN("test_fwCaen_teardownSuite() -> OK");
}





// Test description: ramp one channel to V0 and send later the Vsel Hardware signal to ramp to V1
//
// Disable the updates
// Put channel 0 of board 0 OFF
// Set the channel setpoints
// Power on until V0
// Wait until V0
// Send hardware signal to jump to V1
// Wait until V1
void test_fwCaen_RampOneChannel()
{
  DebugTN("test_fwCaen_RampOneChannel() -> Launched...");

  int iRes;
  float fVMon, fV0Set, fV1Set, fI0Set, fI1Set, fRampUp, fRampDwn;

  // we ramp up one channel of one board:
  // use the pilot to set the proper load, no noise
  // set V0 and V1, I0 and I1, ramp speeds etc etc
  // use Pw to switch it in by software: it will ramp up to V0Set...
  // then use the pilot to go to V1Set ( VSEL, ISEL)
  //
  // we can ramp down with another method


  // pilot on pcbe13632:8890
  // caen pcbe13632:4841
  // crate=0, slot=3, channel=1
  // p_resistance = 100e8; // 100MOhm
  // p_capacitance =1e-7;  // 0.1uF
  // VSEL, ISEL=false, we are on V0/I0 therefore
  // trip time = 10 seconds hardcoded
  // Vnoise = 0
  // Inoise = 0


  DebugTN("test_fwCaen_RampOneChannel() -> Init Pilot Caen OPC UA values -> Start...");
  iRes = dpSetWait("PilotSimulator_0.Settings.ISel",                                FALSE,
                   "PilotSimulator_0.Settings.VSel",                                FALSE,
                   "PilotSimulator_0.Settings.AllChannels.PowerAllChannels",        FALSE,
                   "PilotSimulator_0.Settings.Selector.Crate",                      0,
                   "PilotSimulator_0.Settings.Selector.Board",                      3,
                   "PilotSimulator_0.Settings.Selector.Channel",                    1,
                   "PilotSimulator_0.Settings.AllChannels.ChannelLoad.Resistance",  100e8,
                   "PilotSimulator_0.Settings.AllChannels.ChannelLoad.Capacitance", 1e-7,
                   "PilotSimulator_0.Settings.SingleChannel.Noise.VoltageNoise",    0,
                   "PilotSimulator_0.Settings.SingleChannel.Noise.CurrentNoise",    0,
                   "CAEN/simSY4527/board03/channel001.settings.onOff",            0,
                   "CAEN/simSY4527/board03/channel001.settings.rDwn",             1000);
  DebugTN("test_fwCaen_RampOneChannel() -> Init Pilot Caen OPC UA values -> delay 60 seconds to assure stable point");
  delay(60);
  DebugTN("test_fwCaen_RampOneChannel() -> Init Pilot Caen OPC UA values -> Done OK");


  // p_V0Set = 15;
  // p_V1Set = 100;
  // p_I0Set = 2e-6; // mA in SI
  // p_+I1Set = 6e-6;
  // p_rup = 5;
  // p_dwn = 5;
  fV0Set   = 15;
  fV1Set   = 100;
  fI0Set   = 2e-6;
  fI1Set   = 6e-6;
  fRampUp  = 5;
  fRampDwn = 5;

  DebugTN("test_fwCaen_RampOneChannel() -> Init Venus Caen OPC UA values -> Start...");
  iRes = dpSetWait("CAEN/simSY4527/board03/channel001.settings.i0",   fI0Set,
                   "CAEN/simSY4527/board03/channel001.settings.i1",   fI1Set,
                   "CAEN/simSY4527/board03/channel001.settings.v0",   fV0Set,
                   "CAEN/simSY4527/board03/channel001.settings.v1",   fV1Set,
                   "CAEN/simSY4527/board03/channel001.settings.rUp",  fRampUp,
                   "CAEN/simSY4527/board03/channel001.settings.rDwn", fRampDwn);
  DebugTN("test_fwCaen_RampOneChannel() -> Init Venus Caen OPC UA values -> delay 5 seconds to assure stable point");
  delay(5);
  DebugTN("test_fwCaen_RampOneChannel() -> Init Venus Caen OPC UA values -> Done OK");






  DebugTN("test_fwCaen_RampOneChannel() -> Power channel from Venus Caen OPC UA -> Start...");
  iRes = dpSetWait("CAEN/simSY4527/board03/channel001.settings.onOff", TRUE);

  DebugTN("test_fwCaen_RampOneChannel() -> Power channel from Venus Caen OPC UA -> Delay of " + ((fV0Set/fRampUp) + 5) + " seconds for the ramping up until V0Set, I0Set");
  delay( (fV0Set/fRampUp) + 50);

  iRes = dpGet("CAEN/simSY4527/board03/channel001.actual.vMon", fVMon);
  DebugTN("test_fwCaen_RampOneChannel() -> V0Set reached? -> VMon = " + fVMon);

  DebugTN("test_fwCaen_RampOneChannel() -> V0Set reached? -> V0Set = " + fV0Set + " and VMon = " + fVMon);
  if( (fVMon < (fV0Set - (fV0Set/100*5))) ||
      (fVMon > (fV0Set + (fV0Set/100*5)))  )
  {
    assertEqual(fV0Set, fVMon, "Vmon: " + fVMon + " did't reach set point: " + fV0Set);
  }

  DebugTN("test_fwCaen_RampOneChannel() -> V1Set reached? -> Sending hardware signal to ramp from V0Set to V1Set");
  iRes = dpSet("PilotSimulator_0.Settings.VSel", TRUE);

  DebugTN("test_fwCaen_RampOneChannel() -> Power channel from Venus Caen OPC UA -> Delay of " + (((fV1Set-fVMon)/fRampUp) + 5) + " seconds for the ramping up until V1Set, I1Set");
  delay( ((fV1Set-fVMon)/fRampUp) + 50 );

  iRes = dpGet("CAEN/simSY4527/board03/channel001.actual.vMon", fVMon);
  DebugTN("test_fwCaen_RampOneChannel() -> V1Set reached? -> VMon = " + fVMon);

  DebugTN("test_fwCaen_RampOneChannel() -> V1Set reached? -> V1Set = " + fV1Set + " and VMon = " + fVMon);
  if( (fVMon < (fV1Set - (fV1Set/100*5))) ||
      (fVMon > (fV1Set + (fV1Set/100*5)))  )
  {
    assertEqual(fV1Set, fVMon, "Vmon: " + fVMon + " did't reach set point: " + fV1Set);
  }


}




/*
void test_fwCaen_RampingUp()
{
  DebugTN("test_fwCaen_RampingUp() -> Launched...");
  delay(2);
  assertEqual(3, 3, "3 and 2 are not equal");
}
*/




/*
void test_fwCaen_RampingDown()
{
  DebugTN("test_fwCaen_RampingDown() -> Launched...");
  delay(2);
}
*/




/*
void test_fwCaen_Stable_Trip()
{
  DebugTN("test_fwCaen_StableTrip() -> Launched...");
  delay(2);
}
*/




/*
void test_fwCaen_RampingUp_Trip()
{
  DebugTN("test_fwCaen_RampingUp_Trip() -> Launched...");
  delay(2);
}
*/




