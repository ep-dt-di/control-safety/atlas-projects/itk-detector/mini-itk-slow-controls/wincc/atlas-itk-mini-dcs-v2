/**@file testFwAccessControl.ctl
  
@brief Test Suite for JCOP Framework Configuration Database Tool
@par Creation Date
		 23/06/2014

*/

#uses "fwConfigurationDB/fwConfigurationDB.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_DeviceConfiguration.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Utils.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_DBAccess.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Setup.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Hierarchies.ctl"
#uses "fwUnitTestComponentAsserts.ctl"


string testTxtDp="TestTxt01";

string testCfgFileName="fwConfigurationDB/UnitTestsCfg.json";

// preserve the DB configuration used in the default setup
string savDbConnName;

/**
@brief This routine is optional.
Prepare the appropriate global environment for all of the test cases in this suite.
setupSuite() is called once only, before any of the test cases are called.
*/ 
void testFwConfigurationDB_setupSuite()
{	

	dyn_string exceptionInfo;

	// get test parameters : at first, from the env variable...
	string dbName=getenv("TEST_FWCONFDB_DB");
	string dbUserName=getenv("TEST_FWCONFDB_DBUSER");
	string dbPass=getenv("TEST_FWCONFDB_DBPASS");	
	
	// then from a local test config file if it exists
	string fPath=getPath(DATA_REL_PATH,testCfgFileName);
	if (fPath!="") {
		
		DebugTN("Reading configuration of DB from",fPath);
		// use the file
		string fTxt;
		bool ok=fileToString(fPath,fTxt);
		if (ok) {
			mapping m=jsonDecode(fTxt);
			if (mappingHasKey(m,"DB"))     dbName=m["DB"];
			if (mappingHasKey(m,"DBUser")) dbUserName=m["DBUser"];
			if (mappingHasKey(m,"DBPass")) dbPass=m["DBPass"];
		}
	}
	
	if (dbName=="")     breakTest("dbName not set: use TEST_FWCONFDB_DB env variable or set in "+testCfgFileName);	
	if (dbUserName=="") breakTest("dbUserName not set: use TEST_FWCONFDB_DBUSER env variable or set in "+testCfgFileName);	
	if (dbPass=="")     breakTest("dbPass not set: use TEST_FWCONFDB_DBPASS env variable or set in "+testCfgFileName);	
	
	string dbType="ORACLE";
	string connectionName="TEST_CONN";
	string schemaName=""; // will use default
	string connectString;
	_fwConfigurationDB_getDefaultConnectString(dbType,connectString,exceptionInfo);
	if (dynlen(exceptionInfo)) breakTest("Cannot get default connect string:"+exceptionInfo[2]);
	DebugTN("Using default connect string",connectString);

	DebugTN("We will create a new connection "+connectionName+" with DBName="+dbName+";User="+dbUserName+";Password=*");
	
	// check if connection already declared, and drop if necessary
	dyn_string connNames, connDPs;
	fwConfigurationDB_getDBConnectionList(connNames, connDPs, exceptionInfo);
	if (dynContains(connNames,connectionName)) {
		DebugTN("Dropping already declared DBConnection",connectionName);
		fwConfigurationDB_dropDBConnection(connectionName,exceptionInfo);
		if (dynlen(exceptionInfo)) breakTest("Cannot drop old DB connection:"+exceptionInfo[2]);
	}
	
	DebugTN("Declaring new DB Connection",connectionName);
	fwConfigurationDB_createDBConnection(connectionName,"Test Connection",dbType,
										dbName, dbUserName, dbPass,
										connectString, schemaName, exceptionInfo);
	if (dynlen(exceptionInfo)) breakTest("Cannot create DB connection:"+exceptionInfo[2]);

	
	// set it into the default connection, yet firstly preserve it:
	dpGet("ConfigurationSetups/default.DBConnection",savDbConnName);
	dpSetWait("ConfigurationSetups/default.DBConnection",connectionName);	

	DebugTN("Reinitializing the ConfDB setup");
	string setupName="default";
	fwConfigurationDB_initialize(setupName, exceptionInfo);
	// we may expect the "NO SCHEMA";
	if (dynlen(exceptionInfo)) {
		if (exceptionInfo[2]!="No database schema") breakTest("Unexpected error at first initialization:"+exceptionInfo[2]);
	}
	dynClear(exceptionInfo);
	
	DebugTN("Recreating DB schema (dropping existing one)");
	bool dropExistingSchema=true;
	fwConfigurationDB_createDBSchema(g_fwConfigurationDB_DBConnection,exceptionInfo,dropExistingSchema);
	if (dynlen(exceptionInfo)) {
		breakTest("Could not create schema:"+exceptionInfo[2]);
	}

	DebugTN("Schema created OK");

	fwConfigurationDB_initialize(setupName, exceptionInfo);
	if (dynlen(exceptionInfo)) breakTest("Error in reinitialization after schema creation:"+exceptionInfo[2]);

	
    if (dpExists(testTxtDp)) dpDelete(testTxtDp); // cleanup previously left ones...
	int rc=dpCreate(testTxtDp,"ExampleDP_Text");
	if (rc) breakTest("Could not create test datapoint");

	DebugTN(	"Test DP created",testTxtDp);
}

//-------------------------------------------------------

/**
@brief testExampleComponent_setup() ensures a consistent environment exists prior to calling each test case.
This routine is optional. If you declare one here, it should prepare any appropriate environment
that will be required before running each of the test cases in this suite.

*/ 
void testFwConfigurationDB_setup()
{
}



//-------------------------------------------------------

/**
@brief Example of a developer's test routine. 
A test*.ctl file can contain as many test routines as the developer wants.
*/ 
void test_fwConfigurationDB_checkInit()
{
    dyn_string exceptionInfo;
    fwConfigurationDB_checkInit(exceptionInfo);
    assertEmpty(exceptionInfo);

}


void test_fwConfigurationDB_stringWithPipes()
{
    dyn_string exceptionInfo;
    fwConfigurationDB_checkInit(exceptionInfo);
    assertEmpty(exceptionInfo);
    
    string testText="Hello | from a string || with pipe chars";
    dpSetWait(testTxtDp+".",testText);
    
	fwConfigurationDB_saveDeviceConfiguration(makeDynString(testTxtDp),"TestConfiguration",fwDevice_HARDWARE,exceptionInfo);
    assertEmpty(exceptionInfo);
	
	fwConfigurationDB_updateDeviceConfigurationFromDB("TestConfiguration",fwDevice_HARDWARE,exceptionInfo);
    assertEmpty(exceptionInfo);
	
	string newText;
	dpGet(testTxtDp+".",newText);
	assertEqual(testText,newText);
	DebugTN("Test OK: strings match",newText);
}

//-------------------------------------------------------

/**
@brief This routine is optional. 
Called after each test case routine.
*/ 
void testFwConfigurationDB_teardown()
{
}

//-------------------------------------------------------

/**
@brief This routine is optional. 
Called after all test case routines in this file have been run.
*/ 
void testFwConfigurationDB_teardownSuite()
{
	dyn_string exceptionInfo;
	if (dpExists(testTxtDp)) dpDelete(testTxtDp);
	// restore the original db connection used by the default setup
	dpSetWait("ConfigurationSetups/default.DBConnection",savDbConnName);
	
	
	string connectionName="TEST_CONN";	
	fwConfigurationDB_dropDBConnection(connectionName,exceptionInfo);
	if (dynlen(exceptionInfo)) {DebugTN("There were problems in dropping the DB Connection",exceptionInfo);}
}
