#uses "fwConfigurationDB/fwConfigurationDB.ctl"
#uses "fwUnitTestComponentAsserts.ctl"


const string testDPName="testFwConfDB01";
const string recipeCacheName="TestRecipe001";

void testFwConfigurationDB_setupSuite()
{	
	if (dpExists(testDPName)) {
		DebugTN(__FUNCTION__, "Removing test datapoint",testDPName);
		dpDelete(testDPName);
	}
	
	DebugTN(__FUNCTION__,"Creating test datapoint", testDPName);
	bool rc=dpCreate(testDPName,"TestFwConfDB");
	if (rc) {
		breakTest("Could not create the test DP");
		return;
	}			

}

/** Called before each test
*/
void testFwConfigurationDB_setup()
{
	dyn_string exceptionInfo;
    fwConfigurationDB_checkInit(exceptionInfo);
    assertEmpty(exceptionInfo);
}

// Until these are merged on the fwUnitTestComponent

/**
	Asserts equality of two float numbers, taking into account
	Floating-point rounding/precision.
	
	The definition of machine epsilon is at
	 https://en.wikipedia.org/wiki/Machine_epsilon 
	- we use the one for the decimal64 encoding.
	
	We take 10x the machine epsilon as we need to calculate a difference and ratio
*/
void fwCDBT_assertEqualFloat(float expected, float actual, string errorMsg="")
{
	const float machine_epsilon=1.0e-15; 
	if (expected==0) {
		assertEqual(expected,actual); // exact matching could be used
	} else {
		float diff=actual-expected;
		if (fabs(diff/expected) > 10.0*machine_epsilon) {
			errClass exc=makeError( "", FW_UNIT_TEST_ERR_PRIO_FAILED, FW_UNIT_TEST_ERR_TYPE_FAILED, FW_UNIT_TEST_ERR_CODE_FAILED, "Expected: " + expected + ", actual: " + actual, errorMsg );
			throw (exc);
		}
	}
}

void fwCDBT_assertEqualDynFloat(dyn_float expected, dyn_float actual, string errorMsg="")
{
	if ( dynlen(expected) != dynlen(actual) ) {
		errClass exc=makeError( "", FW_UNIT_TEST_ERR_PRIO_FAILED, FW_UNIT_TEST_ERR_TYPE_FAILED, FW_UNIT_TEST_ERR_CODE_FAILED, "Expected: " + expected + ", actual: " + actual, errorMsg );
		throw (exc);	
	}

	for (int i=1;i<=dynlen(expected);i++) {
		fwCDBT_assertEqualFloat(expected[i],actual[i]);
	}

}


//-------------------------------------------------------

void recipeCacheTest(string testElement, string recipeType, mixed testValue)
{
	dyn_string exceptionInfo;
	string testDPE=testDPName+testElement;

//DebugTN("Test value",testValue);
	dpSetWait(testDPE, testValue);

	dyn_dyn_mixed recipeObject;
	fwConfigurationDB_extractRecipe(makeDynString(getSystemName()+testDPName),"",recipeObject,exceptionInfo,recipeType);
	assertEmpty(exceptionInfo);

//DebugTN("Extracted values",recipeObject[fwConfigurationDB_RO_VALUE]);


	fwConfigurationDB_storeRecipeInCache(recipeObject,recipeCacheName,"",exceptionInfo);
	assertEmpty(exceptionInfo);
											
	dyn_dyn_mixed recipeObject2;
	fwConfigurationDB_getRecipeFromCache(recipeCacheName,makeDynString(),"",recipeObject2,exceptionInfo);
	assertEmpty(exceptionInfo);

//DebugTN("Loaded values",recipeObject[fwConfigurationDB_RO_VALUE]);

	fwConfigurationDB_applyRecipe(recipeObject2,"",exceptionInfo);
	assertEmpty(exceptionInfo);

	mixed retValue;
	dpGet(testDPE,retValue);
//DebugTN("Retrieved value",retValue);
	if (getType(retValue)==FLOAT_VAR) {
		assertEqualFloat(testValue,retValue);
	} else if (getType(retValue)==DYN_FLOAT_VAR) {
		assertEqualDynFloat(testValue,retValue);
	} else {
		assertEqual(testValue,retValue);
	}
}

void test_fwConfigurationDB_recipeDynString1Pipe()              { recipeCacheTest(".dyn.string", "TestDynString", makeDynString("|"));		}
void test_fwConfigurationDB_recipeDynString2Pipe()              { recipeCacheTest(".dyn.string", "TestDynString", makeDynString("|","|"));	}
void test_fwConfigurationDB_recipeDynStringEmpty()              { recipeCacheTest(".dyn.string", "TestDynString", makeDynString());			}
void test_fwConfigurationDB_recipeDynStringEmptyString1()       { recipeCacheTest(".dyn.string", "TestDynString", makeDynString(""));		}
void test_fwConfigurationDB_recipeDynStringEmptyString2()       { recipeCacheTest(".dyn.string", "TestDynString", makeDynString("",""));	}
void test_fwConfigurationDB_recipeDynStringPipeAndEmptyString() { recipeCacheTest(".dyn.string", "TestDynString", makeDynString("|",""));	}
void test_fwConfigurationDB_recipeDynStringEmptyStringAndPipe() { recipeCacheTest(".dyn.string", "TestDynString", makeDynString("","|"));	}
void test_fwConfigurationDB_recipeDynStringQuotesAndEscapes()   { recipeCacheTest(".dyn.string", "TestDynString", makeDynString("\"","\"\"","\'","\'\'","\\","/")); }
void test_fwConfigurationDB_recipeDynString1000Elem()
{
	dyn_string ds;
	for (int i=1;i<=1000;i++) { dynAppend(ds,"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*() ,."); }
	recipeCacheTest(".dyn.string", "TestDynString", makeDynString("","|"));
}
void test_fwConfigurationDB_recipeDynStringSlash()              { recipeCacheTest(".dyn.string", "TestDynString", makeDynString("\\x40"));	}
void test_fwConfigurationDB_recipeString01()                    { recipeCacheTest(".string", "TestString", "A Test string");	}
void test_fwConfigurationDB_recipeStringEmpty()                 { recipeCacheTest(".string", "TestString", "");		}
void test_fwConfigurationDB_recipeStringPipe()                  { recipeCacheTest(".string", "TestString", "|");	}
void test_fwConfigurationDB_recipeStringSpace()                 { recipeCacheTest(".string", "TestString", " ");	}
void test_fwConfigurationDB_recipeStringSlash()                 { recipeCacheTest(".string", "TestString", "\\");	}
void test_fwConfigurationDB_recipeStringSlashX40()              { recipeCacheTest(".string", "TestString", "\\x40");}
void test_fwConfigurationDB_recipeInt01()                       { recipeCacheTest(".int", "TestInt", 123);}
void test_fwConfigurationDB_recipeIntMax()                      { recipeCacheTest(".int", "TestInt", maxINT());}
void test_fwConfigurationDB_recipeIntMin()                      { recipeCacheTest(".int", "TestInt", minINT());}
void test_fwConfigurationDB_recipeDynInt()                      { recipeCacheTest(".dyn.int", "TestDynInt", makeDynInt(123,456,0,-999, minINT(), maxINT()));}
void test_fwConfigurationDB_recipeBoolTRUE()                    { recipeCacheTest(".bool", "TestBool", true);}
void test_fwConfigurationDB_recipeBoolFALSE()                   { recipeCacheTest(".bool", "TestBool", false);}
void test_fwConfigurationDB_recipeDynBool()               		{ recipeCacheTest(".dyn.bool", "TestDynBool", makeDynBool(true, false));}
void test_fwConfigurationDB_recipeDynBoolEmpty()              	{ recipeCacheTest(".dyn.bool", "TestDynBool", makeDynBool());}
void test_fwConfigurationDB_recipeFloat01()                     { recipeCacheTest(".float", "TestFloat", 3.14);}
void test_fwConfigurationDB_recipeFloat02()                     { recipeCacheTest(".float", "TestFloat", -2.78);}
void test_fwConfigurationDB_recipeFloat03()                     { recipeCacheTest(".float", "TestFloat", 1.234e123);}
void test_fwConfigurationDB_recipeFloat04()                     { recipeCacheTest(".float", "TestFloat", 5.678e-123);}
void test_fwConfigurationDB_recipeFloatMax()                    { recipeCacheTest(".float", "TestFloat", maxFLOAT());}
void test_fwConfigurationDB_recipeFloatMin()                    { recipeCacheTest(".float", "TestFloat", minFLOAT());}
void test_fwConfigurationDB_recipeFloatInfPlus()                { recipeCacheTest(".float", "TestFloat", (float)"inf");}
void test_fwConfigurationDB_recipeFloatInfMinus()               { recipeCacheTest(".float", "TestFloat", (float)"-inf");}
void test_fwConfigurationDB_recipeFloatNaN()                    { recipeCacheTest(".float", "TestFloat", (float)"nan");}
void test_fwConfigurationDB_recipeDynFloat()                    { recipeCacheTest(".dyn.float", "TestDynFloat", makeDynFloat(minFLOAT(),maxFLOAT(),3.14,2.78),(float)"inf", (float)"-inf",(float)"nan");}
void test_fwConfigurationDB_recipeCharA()                       { recipeCacheTest(".char", "TestChar", 'A');}
void test_fwConfigurationDB_recipeCharNewline()                 { recipeCacheTest(".char", "TestChar", '\n');}
void test_fwConfigurationDB_recipeCharPipe()                    { recipeCacheTest(".char", "TestChar", '|');}
void test_fwConfigurationDB_recipeCharSlash()                   { recipeCacheTest(".char", "TestChar", '\\');}
void test_fwConfigurationDB_recipeChar0()                       { recipeCacheTest(".char", "TestChar", '\0');}
void test_fwConfigurationDB_recipeChar255()                     { recipeCacheTest(".char", "TestChar", '\255');}
void test_fwConfigurationDB_recipeDynChar()                     { recipeCacheTest(".dyn.char", "TestDynChar", makeDynChar('\0','\255','A','\n','|','\\'));}
void test_fwConfigurationDB_recipeLong01()                      { recipeCacheTest(".long", "TestLong", 123L);}
void test_fwConfigurationDB_recipeLong02()                      { recipeCacheTest(".long", "TestLong", -456L);}
void test_fwConfigurationDB_recipeLong03()                      { recipeCacheTest(".long", "TestLong", 0x7FFFFFFFFFFFFFFFL);}
void test_fwConfigurationDB_recipeLongMax()                     { recipeCacheTest(".long", "TestLong", maxLONG());}
void test_fwConfigurationDB_recipeLongMin()                     { recipeCacheTest(".long", "TestLong", minLONG());}
void test_fwConfigurationDB_recipeDynLong()                     { recipeCacheTest(".dyn.long", "TestDynLong", makeDynLong(123L,-456L,0L,0x7FFFFFFFFFFFFFFFL, minLONG(), maxLONG()));}
void test_fwConfigurationDB_recipeUInt01()                      { recipeCacheTest(".uint", "TestUInt", 123U);}
void test_fwConfigurationDB_recipeUIntMax()                     { recipeCacheTest(".uint", "TestUInt", maxUINT());}
void test_fwConfigurationDB_recipeUIntMin()                     { recipeCacheTest(".uint", "TestUInt", minUINT());}
void test_fwConfigurationDB_recipeDynUInt()                     { recipeCacheTest(".dyn.uint", "TestDynUInt", makeDynUInt(123U,456U,0U, minUINT(), maxUINT()));}
void test_fwConfigurationDB_recipeULong01()                      { recipeCacheTest(".ulong", "TestULong", 123UL);}
void test_fwConfigurationDB_recipeULongMax()                     { recipeCacheTest(".ulong", "TestULong", maxULONG());}
void test_fwConfigurationDB_recipeULongMin()                     { recipeCacheTest(".ulong", "TestULong", minULONG());}
void test_fwConfigurationDB_recipeDynULong()                     { recipeCacheTest(".dyn.ulong", "TestDynULong", makeDynULong(123UL,456UL,0UL, minULONG(), maxULONG()));}
void test_fwConfigurationDB_recipeBit32Min()                     { recipeCacheTest(".bit32", "TestBit32", 0U);}
void test_fwConfigurationDB_recipeBit32Max()                     { recipeCacheTest(".bit32", "TestBit32", 0xFFFFFFFFU);}
void test_fwConfigurationDB_recipeDynBit32()                     { recipeCacheTest(".dyn.bit32", "TestBit32", makeDynBit32(0U,(bit32)256,0xFFFFFFFFU));}
void test_fwConfigurationDB_recipeBit64Min()                     { recipeCacheTest(".bit64", "TestBit64", 0UL);}
void test_fwConfigurationDB_recipeBit64Max()                     { recipeCacheTest(".bit64", "TestBit64", 0xFFFFFFFFFFFFFFFFUL);}
void test_fwConfigurationDB_recipeDynBit64()                     { recipeCacheTest(".dyn.bit64", "TestBit64", makeDynBit64(0UL,(bit64)256UL,0xFFFFFFFFFFFFFFFFUL));}

void test_fwConfigurationDB_recipeTimeNow()                      { recipeCacheTest(".time", "TestTime", getCurrentTime());}
void test_fwConfigurationDB_recipeTimeZero()                     { time t0=0; recipeCacheTest(".time", "TestTime", t0);}
void test_fwConfigurationDB_recipeTime() 						 {recipeCacheTest(".time", "TestTime", makeTime(2020,06,10,14,32,15,567)); }
void test_fwConfigurationDB_recipeTime2DST()					 {recipeCacheTest(".time", "TestTime", makeTime(2020,06,10,14,32,15,567,true)); }
void test_fwConfigurationDB_recipeTime2NODST()					 {recipeCacheTest(".time", "TestTime", makeTime(2020,06,10,14,32,15,567,false)); }
void test_fwConfigurationDB_recipeDynTime()                      { time t0=0; recipeCacheTest(".dyn.time", "TestDynTime", makeDynTime(t0,getCurrentTime(),makeTime(2020,06,10,14,32,15,567)));}

void test_fwConfigurationDB_recipeDpid(){ recipeCacheTest(".dpid", "TestDpid", getSystemName()+testDPName);}
void test_fwConfigurationDB_recipeDynDpid(){ recipeCacheTest(".dyn.dpid", "TestDynDpid", makeDynString(getSystemName()+testDPName,getSystemName()+testDPName+".dpid"));}

void test_fwConfigurationDB_recipeBlobEmpty()
{ 
	blob b0;
	blobZero(b0,0);
	recipeCacheTest(".blob", "TestBlob", b0);
}

void test_fwConfigurationDB_recipeBlobWithZeros()
{ 
	blob b0;
	blobZero(b0,4096);
	recipeCacheTest(".blob", "TestBlob", b0);
}
void test_fwConfigurationDB_recipeBlob1()
{ 
	blob b0;
	blobZero(b0,0);
	blobAppendValue(b0,"TEST CONTENT",12);
	blobAppendValue(b0,'\255',1);
	blobAppendValue(b0,'\32',1);
	blobAppendValue(b0,'\0',1);
	recipeCacheTest(".blob", "TestBlob", b0);
}

void testFwConfigurationDB_teardown()
{
}

void testFwConfigurationDB_teardownSuite()
{
/*
	if (dpExists(testDPName)) {
		DebugTN(__FUNCTION__, "Removing test datapoint",testDPName);
		dpDelete(testDPName);
	}
*/
}
