#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwDIP/fwDIP.ctl"
#uses "fwDIP/fwDIPImport.ctl"

void test_fwDIP_setupSuite()
{
	// Nothing to do
}

void test_fwDIP_teardownSuite()
{
	// Nothing to do
}

/**
	Start DIP manager
*/
void test_fwDIP_StartManager()
{
	int retCode = fwInstallationManager_add("WCCOAdip", "always", 30, 1, 2, "-num 1");
  assert((retCode==1||retCode==2),"Adding DIP Manager failed" + retCode);
  
  delay(5);
  bool bRet = fwInstallationManager_isDriverRunning(1);
  assert(bRet, "DIP Manager crashed");
}

/**
	Import configuration with publication and subscription
*/
void test_fwDIP_Import()
{
  string sDipConfigFile = getPath(DATA_REL_PATH, "fwDIP_unitTest_config.xml");
  dyn_string exceptionInfo;
  int iDipMgrNumber = 1;
  string sLogFile = fwDIPImport_makeLogFileName();
  dyn_mapping dmDIPtype;
  mapping mDIPdpeList;
  mapping m_dsAllPublication;

  fwDIPImport_loadFile(iDipMgrNumber, sDipConfigFile, dmDIPtype, mDIPdpeList, m_dsAllPublication, exceptionInfo, sLogFile);
  assert(dynContains(exceptionInfo, FWDIPIMPORT_CONST_ERROR_MESSAGE) == 0,"ERROR loading file");

  dynClear(exceptionInfo);
  fwDIPImport_createDIPConfig(dmDIPtype, mDIPdpeList, exceptionInfo, sLogFile);
  assert(dynContains(exceptionInfo, FWDIPIMPORT_CONST_ERROR_MESSAGE) == 0,"ERROR importing file");
  fwInstallationManager_command("RESTART", "WCCOAdip", "-num 1");
  delay(5);
  bool bRet = fwInstallationManager_isDriverRunning(1);
  assert(bRet, "Failed to restart DIP manager after import");
}

/**
	Subscribtion is updating
*/
void test_fwDIP_Diff()
{
  skipTest("Test skipped on CI until ports opened");
  return;

	int n = 0;
	string testDp = "fwDIP_testInt.";
	assert(dpExists(testDp), "Test datapoint does not exist");
	int dpRet = dpGet(testDp, n);
	assert(dynlen(getLastError()) == 0 && dpRet == 0, "Failed to get value from test datapoint");
	delay(5);
	int o = n;
	int dpRet = dpGet(testDp, n);
	assert(dynlen(getLastError()) == 0 && dpRet == 0, "Failed to get value from test datapoint");
  assert(n>o, "Subscribed item not updating old:" + o + " >= new:" + n);
}
