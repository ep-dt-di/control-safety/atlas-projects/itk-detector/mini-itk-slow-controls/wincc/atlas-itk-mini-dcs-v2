#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

/**
 *
 */
void test_fwException_getClass_builtin()
{
	errClass ex = fwException_declare("ctrl-15", BUILTIN_EXCEPTION);

	assertEqual("ctrl-15", fwException_getClass(ex));
}



/**
 *
 */
void test_fwException_getClass_builtin_default()
{
	errClass ex = INDEX_OUT_OF_RANGE_EXCEPTION;

	assertEqual("-79", fwException_getClass(ex));
}

/**
 *
 */
void test_fwException_getClass_custom()
{
	errClass ex = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	assertEqual("EX.FWEXCEPTION.TEST.CUSTOM", fwException_getClass(ex));
}

