#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

/**
 *
 */
void test_fwException_getStackTrace_custom()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	try {
		fwException_throw(CUSTOM_EXCEPTION);
	} catch {
		dyn_string stackTrace = fwException_getStackTrace();
		assertEqual(0, strpos(stackTrace[1], "fwException_throw"), stackTrace);
	}
}

/**
 *
 */
void test_fwException_getStackTrace_custom_rethrown()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	try {
		try {
			fwException_throw(CUSTOM_EXCEPTION);
		} catch {
			fwException_rethrow();
		}
	} catch {
		dyn_string stackTrace = fwException_getStackTrace();
		assertEqual(0, strpos(stackTrace[1], "fwException_throw"), stackTrace);
	}
}

/**
 *
 */
void test_fwException_getStackTrace_builtin()
{
	try {
		dyn_string ds = makeDynString();
		string s = ds[4];
	} catch {
		dyn_string stackTrace = fwException_getStackTrace();
		assertEqual(0, strpos(stackTrace[1], __FUNCTION__), stackTrace);
	}
}

/**
 *
 */
void test_fwException_getStackTrace_builtin_rethrown()
{
	try {
		try {
			dyn_string ds = makeDynString();
			string s = ds[4];
		} catch {
			fwException_rethrow();
		}
	} catch {
		dyn_string stackTrace = fwException_getStackTrace();
		assertEqual(0, strpos(stackTrace[1], __FUNCTION__), stackTrace);
	}
}

