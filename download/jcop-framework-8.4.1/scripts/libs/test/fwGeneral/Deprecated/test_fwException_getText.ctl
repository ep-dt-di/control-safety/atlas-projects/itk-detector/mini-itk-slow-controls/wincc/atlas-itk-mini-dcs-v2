#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

/**
 *
 */
void test_fwException_getText_custom_withNote()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");
	errClass ex = makeError(
		getErrorCatalog(CUSTOM_EXCEPTION),
		getErrorPriority(CUSTOM_EXCEPTION),
		getErrorType(CUSTOM_EXCEPTION),
		getErrorCode(CUSTOM_EXCEPTION),
		"This is a note");

	assertEqual("This is a note", fwException_getText(ex));
}

/**
 *
 */
void test_fwException_getText_custom_withEmptyNote()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");
	errClass ex = makeError(
		getErrorCatalog(CUSTOM_EXCEPTION),
		getErrorPriority(CUSTOM_EXCEPTION),
		getErrorType(CUSTOM_EXCEPTION),
		getErrorCode(CUSTOM_EXCEPTION),
		"");

	assertEqual("", fwException_getText(ex));
}

/**
 *
 */
void test_fwException_getText_custom_withoutNote()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");
	errClass ex = makeError(
		getErrorCatalog(CUSTOM_EXCEPTION),
		getErrorPriority(CUSTOM_EXCEPTION),
		getErrorType(CUSTOM_EXCEPTION),
		getErrorCode(CUSTOM_EXCEPTION));

	assertEqual("", fwException_getText(ex));
}

/**
 *
 */
void test_fwException_getText_builtin_withNote()
{
	errClass ex = makeError(
		getErrorCatalog(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorPriority(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorType(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorCode(INDEX_OUT_OF_RANGE_EXCEPTION),
		"This is a note");

	assertEqual("Index out of range, This is a note", fwException_getText(ex));
}

/**
 *
 */
void test_fwException_getText_builtin_withEmptyNote()
{
	errClass ex = makeError(
		getErrorCatalog(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorPriority(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorType(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorCode(INDEX_OUT_OF_RANGE_EXCEPTION),
		"");

	assertEqual("Index out of range, ", fwException_getText(ex));
}

/**
 *
 */
void test_fwException_getText_builtin_withoutNote()
{
	errClass ex = makeError(
		getErrorCatalog(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorPriority(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorType(INDEX_OUT_OF_RANGE_EXCEPTION),
		getErrorCode(INDEX_OUT_OF_RANGE_EXCEPTION));

	assertEqual("Index out of range", fwException_getText(ex));
}

