#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

void do_some_sql_stuff()
{
	fwException_throw(fwException_declare("EX.FWEXCEPTION.TEST.SQL.ORA-0900"));
}

void display_exception(errClass ex, string context_description = "")
{
	if (strlen(context_description) != 0) {
		DebugN(context_description, fwException_getText(ex));
	} else {
		DebugN("Exception happened:", fwException_getText(ex));
	}
}

int return_old_exceptions(dyn_string &exceptionInfo)
{
	fwException_raise(exceptionInfo, "ERROR", "Old stuff", 54);
	return -1;
}

void test_fwException_howTo_solve()
{
	// Example of how to use exceptions in an algorithm.
	// Script that reorganize the 3 first letters of the alphabet "as the customer asked" for his weird business logic.
	dyn_string ds = makeDynString("c", "a", "b");
	string s = "z";     // Will be the output
	char c = 'm';       // The current character to map

	try {
		s = ds[c - 'a' + 1];
	} catch {
		if (fwException_get() == INDEX_OUT_OF_RANGE_EXCEPTION) {
			s = (string)c;
		} else {
			fwException_rethrow();
		}
	}
	assertEqual(c, s);
}

void test_fwException_howTo_ignore()
{
	// Example of how to use exceptions in an algorithm.
	// Script that reorganize the 3 first letters of the alphabet "as the customer asked" for his weird business logic.
	dyn_string ds = makeDynString("c", "a", "b");
	string s = "-";     // Will be the output
	char c = 'm';       // The current character to map

	try {
		// Bug in wccoa, it replaces the expression throwing an exception by an unidentified expression,
		// and continue the processing of the line...
		// Here it would assign 0 to the string variable, messing up our algorithm.
		string temp_s = ds[c - 'a' + 1];
		s = temp_s;
	} catch {
		if (fwException_get() != INDEX_OUT_OF_RANGE_EXCEPTION)
			fwException_rethrow();
	}
	assertEqual("-", s);
}

void test_fwException_howTo_rollback()
{
	const errClass SQL_ORA_0800_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.SQL.ORA-0800");
	const errClass SQL_ORA_0900_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.SQL.ORA-0900");
	const errClass SQL_ORA_1270_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.SQL.ORA-1270");

	int i = 7;

	try {
		i++;
		do_some_sql_stuff();         // Will throw SQL_ORA_0900_EXCEPTION
	} catch {
		errClass ex = fwException_get();
		switch (ex) {
		  case SQL_ORA_0800_EXCEPTION:
		  case SQL_ORA_1270_EXCEPTION:
			// Harmless, just ignore
			break;
		  case SQL_ORA_0900_EXCEPTION:
			// Oh no! We are doomed! Rollback!
			i -= 2;                     // compensate for the finally
			break;
		  default:
			fwException_rethrow();
		}
	} finally {
		i++;
	}
	assertEqual(7, i);
}

void test_fwException_howTo_translateToMeaningfulException()
{
	const errClass LOW_LEVEL_CRYPTIC_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.SCIENTIST.CRAZY.BAZINGA");
	const errClass HIGH_LEVEL_CLEAR_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.42");     // Could not be clearer ;)

	try {
		try {
			// Throwing some sheldonian stuff at you, watch out!
			fwException_throw(LOW_LEVEL_CRYPTIC_EXCEPTION, "Don't you think if I were wrong, I'd know it?");
		} catch {
			if (fwException_get() == LOW_LEVEL_CRYPTIC_EXCEPTION) {
				fwException_throw(HIGH_LEVEL_CLEAR_EXCEPTION, "Sheldon is not crazy");                 // His mother had him tested ;)
			} else {
				fwException_rethrow();
			}
		}
	} catch {
		assertEqual(HIGH_LEVEL_CLEAR_EXCEPTION, fwException_get());
	}
}

void test_fwException_howTo_translateForHumans()
{
	dyn_string ds = makeDynString();

	try {
		string s = ds[4];
	} catch {
		const errClass ex = fwException_get();
		if (ex == INDEX_OUT_OF_RANGE_EXCEPTION) {
			display_exception(ex, "You should not try to access elements in an empty array... PROGRAMMING-101!");
		} else {
			fwException_rethrow();
		}
	}
}

void test_fwException_howTo_patternMatch()
{
	const errClass EX0 = fwException_declare("EX.FWEXCEPTION.TEST.0");
	const errClass EX1 = fwException_declare("EX.FWEXCEPTION.TEST.1");
	const errClass EX2 = fwException_declare("EX.FWEXCEPTION.TEST.2");
	const errClass EX42 = fwException_declare("EX.FWEXCEPTION.TEST.42");

	try {
		switch (rand() % 3) {
		  case 0:
			fwException_throw(EX0);
			break;
		  case 1:
			fwException_throw(EX1);
			break;
		  case 2:
			fwException_throw(EX2);
			break;
		  default:
			fwException_throw(EX42);
		}
	} catch {
		if (!fwException_match("*FWEXCEPTION*.?"))
			fwException_rethrow();
	}
}

void test_fwException_howTo_fromSystem()
{
	const errClass PROGRAM_NOT_FOUND_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.SYSTEM.PROGRAM_NOT_FOUND");
	const errClass MYPROG_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.SYSTEM.MYPROG_ERR");

	try {
		int err = system("myprog");
		if (err != 0) {
			if (err == 127) {
				fwException_throw(PROGRAM_NOT_FOUND_EXCEPTION);                 // For the shake of example, could mean something else of course
			} else {
				fwException_throw(MYPROG_EXCEPTION, err);
			}
		}
	} catch {
		assertEqual(PROGRAM_NOT_FOUND_EXCEPTION, fwException_get());
	}
}

void test_fwException_howTo_fromBaseFunction()
{
	const errClass FOPEN_EXCEPTION = fwException_declare("EX.WCCOA.FOPEN_ERR");

	try {
		string path = "inexistant.txt";
		file f = fopen(path, "r");
		// getLastError() won't give you anything...
		int err = ferror(f);
		if (err != 0)
			fwException_throw(FOPEN_EXCEPTION, "Could not open file \"" + path + "\": " + ((string)f));
		fclose(f);
	} catch {
		assertEqual(FOPEN_EXCEPTION, fwException_get());
	}
}

void test_fwException_howTo_fromOldExceptions()
{
	const errClass OLD_EXCEPTION = fwException_declare("EX.FWEXCEPTION.TEST.OLD");

	try {
		dyn_string exceptionInfo = makeDynString();
		int err = return_old_exceptions(exceptionInfo);
		if ((err != 0) || (dynlen(exceptionInfo) > 0))
			fwException_throw(OLD_EXCEPTION, exceptionInfo[1][2]);
	} catch {
		assertEqual(OLD_EXCEPTION, fwException_get());
	}
}