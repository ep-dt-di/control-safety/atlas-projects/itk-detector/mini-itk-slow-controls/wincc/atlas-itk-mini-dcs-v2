#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

/**
 *
 */
void test_fwException_match_builtin()
{
	try {
		dyn_string ds = makeDynString();
		string s = ds[4];
	} catch {
		assertTrue(fwException_match("*79"), "*79");                                    // Match any error 79 from any builtin catalog
		assertTrue(fwException_match("-*"), "-*");                                      // Match any error from the default catalog
		assertTrue(fwException_match("Index out of range*"), "Index out of range*");    // Match any error from the default catalog
		assertFalse(fwException_match("EX.*"), "EX.*");                                 // Don't match on custom exceptions
	}
}

/**
 *
 */
void test_fwException_match_custom()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	try {
		fwException_throw(CUSTOM_EXCEPTION, "This is a test");
	} catch {
		assertTrue(fwException_match("*CUSTOM"), "*CUSTOM");
		assertTrue(fwException_match("*TEST*"), "*TEST*");
		assertTrue(fwException_match("EX.*"), "EX.*");
		assertTrue(fwException_match("EX.FWEXCEPTION.*"), "EX.FWEXCEPTION.*");
		assertTrue(fwException_match("EX.*.TEST.*"), "EX.*.TEST.*");
		assertTrue(fwException_match("EX.*.?E[QRS]T.?U*"), "EX.*.?E[QRS]T.?U*");
		assertTrue(fwException_match("This is a test"), "This is a test");
		assertTrue(fwException_match("This*test"), "This is a test");
		assertFalse(fwException_match("*79"), "*79");
		assertFalse(fwException_match("-*"), "-*");
	}
}

