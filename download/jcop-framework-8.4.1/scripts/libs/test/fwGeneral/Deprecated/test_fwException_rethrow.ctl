#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

/**
 *
 */
void test_fwException_rethrow_builtin()
{
	try {
		try {
			dyn_string ds = makeDynString();
			string s = ds[2];
		} catch {
			fwException_rethrow();
		}
	} catch {
		errClass ex = fwException_get();
		assertEqual(INDEX_OUT_OF_RANGE_EXCEPTION, ex);
		assertEqual(0, strpos(fwException_getText(ex), "Index out of range, "), fwException_getText(ex));
		// Check there isn't a duplication of error description
		assertEqual(-1, strpos(fwException_getText(ex), "Index out of range, Index out of range, "), fwException_getText(ex));
	}
}

/**
 *
 */
void test_fwException_rethrow_builtin_noNote()
{
	try {
		try {
			fwException_throw(INDEX_OUT_OF_RANGE_EXCEPTION);
		} catch {
			fwException_rethrow();
		}
	} catch {
		errClass ex = fwException_get();
		assertEqual(INDEX_OUT_OF_RANGE_EXCEPTION, ex);
		assertEqual(0, strpos(fwException_getText(ex), "Index out of range, "), fwException_getText(ex));
		// Check there isn't a duplication of error description
		assertEqual(-1, strpos(fwException_getText(ex), "Index out of range, Index out of range, "), fwException_getText(ex));
	}
}

/**
 *
 */
void test_fwException_rethrow_custom()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	try {
		try {
			fwException_throw(CUSTOM_EXCEPTION, "This is a test");
		} catch {
			fwException_rethrow();
		}
	} catch {
		errClass ex = getLastException()[1];
		assertEqual(CUSTOM_EXCEPTION, ex);
		assertEqual("This is a test", fwException_getText(ex));
	}
}

/**
 *
 */
void test_fwException_rethrow_custom_noNote()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	try {
		try {
			fwException_throw(CUSTOM_EXCEPTION);
		} catch {
			fwException_rethrow();
		}
	} catch {
		errClass ex = getLastException()[1];
		assertEqual(CUSTOM_EXCEPTION, ex);
		assertEqual("", fwException_getText(ex));
	}
}

