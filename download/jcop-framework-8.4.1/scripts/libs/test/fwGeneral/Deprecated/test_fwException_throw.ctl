#uses "fwGeneral/fwException.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

/**
 *
 */
void test_fwException_throw_builtin()
{
	try {
		fwException_throw(INDEX_OUT_OF_RANGE_EXCEPTION, "This is a test");
	} catch {
		errClass ex = fwException_get();
		assertEqual(INDEX_OUT_OF_RANGE_EXCEPTION, ex);
		assertEqual("Index out of range, This is a test", fwException_getText(ex));
	}
}

/**
 *
 */
void test_fwException_throw_builtin_noNote()
{
	try {
		fwException_throw(INDEX_OUT_OF_RANGE_EXCEPTION);
	} catch {
		errClass ex = fwException_get();
		assertEqual(INDEX_OUT_OF_RANGE_EXCEPTION, ex);
		assertEqual("Index out of range", fwException_getText(ex));
	}
}

/**
 *
 */
void test_fwException_throw_custom()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	try {
		fwException_throw(CUSTOM_EXCEPTION, "This is a test");
	} catch {
		errClass ex = fwException_get();
		assertEqual(CUSTOM_EXCEPTION, ex);
		assertEqual("This is a test", fwException_getText(ex));
	}
}

/**
 *
 */
void test_fwException_throw_custom_noNote()
{
	const errClass CUSTOM_EXCEPTION = fwException_declare("FWEXCEPTION.TEST.CUSTOM");

	try {
		fwException_throw(CUSTOM_EXCEPTION);
	} catch {
		errClass ex = fwException_get();
		assertEqual(CUSTOM_EXCEPTION, ex);
		assertEqual("", fwException_getText(ex));
	}
}
