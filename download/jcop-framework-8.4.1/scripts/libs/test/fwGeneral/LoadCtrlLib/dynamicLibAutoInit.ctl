// Example of a library that dynamically loads another lib on runtime

private const bool _initializeMe = fwGeneral_loadCtrlLib("test/fwGeneral/LoadCtrlLib/dynamicLibInternal.ctl", true);

bool dynamicFunctionTest1()
{
	DebugTN("Hello from " + __FUNCTION__, _initializeMe);
	return _initializeMe;
}