// Example of a library that dynamically loads another lib on runtime

private const bool _initializeMe2 = fwGeneral_loadCtrlLib("test/fwGeneral/LoadCtrlLib/dynamicLibInternal2.ctl", true);

bool dynamicFunctionTest2()
{
	DebugTN("Hello from " + __FUNCTION__, _initializeMe2);
	return _initializeMe2;
}