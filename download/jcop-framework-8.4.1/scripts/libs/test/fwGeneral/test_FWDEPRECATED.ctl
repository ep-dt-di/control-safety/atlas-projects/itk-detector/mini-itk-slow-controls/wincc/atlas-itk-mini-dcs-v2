#uses "fwUnitTestComponentAsserts.ctl"
#uses "test/fwGeneral/DeprecatedTestLib.ctl"

dyn_string originalDeprecatedList;

const dyn_string ORIGIN_STRING = " , CALL ORIGIN : main(...) at FwUnitTestComponent/fwUnitTestComponentTestRunnerScript.ctl:95";

// depending on how the test is run the stack trace may or may not contain the "[Script: xxx]" part
// we will normalize the message
const dyn_string ORIGIN_STRING_SQBK = " , CALL ORIGIN : main(...) at [Script: FwUnitTestComponent/fwUnitTestComponentTestRunnerScript.ctl]:95";
string normalizeMsg(string msg)
{
	strreplace(msg,ORIGIN_STRING_SQBK,ORIGIN_STRING);
	return msg;
}

void test_setupSuite()
{
	DebugTN(__FUNCTION__, "Preserving the original list of deprecated function");
	dpGet(_fwDeprecated_DPE, originalDeprecatedList);
	DebugTN(__FUNCTION__, "###INFO: Warnings with 99999/fwGeneral and fwGeneral.ctl in the log below are expected");
	delay(0, 500);
}

void test_teardownSuite()
{
	DebugTN(__FUNCTION__, "Restoring the original list of deprecated function");
	dpSet(_fwDeprecated_DPE, originalDeprecatedList);
}

void test_setup()
{
	DebugTN(__FUNCTION__, "Clearing the list of deprecated functions");
	_fwGeneral_fwDeprecatedClear();
}

void test_fwGeneral_FWDEPRECATED()
{
	DebugTN(__FUNCTION__);
	ParentDeprecatedTestFunction();

	dyn_string deprList;
	dpGet(_fwDeprecated_DPE, deprList);

	assertEqual(1, dynlen(deprList), "Wrong number of recorded deprecated calls");
	assertEqual("DeprecatedTestFunction(...) at DeprecatedTestLib.ctl:"+lineDeprecatedTestFunction+" # ParentDeprecatedTestFunction(...) at DeprecatedTestLib.ctl:"+lineParentDeprecatedTestFunction+ORIGIN_STRING,
				normalizeMsg(deprList[1]), "Wrong deprecation message");
//    DebugTN(deprList);
}

void test_fwGeneral_FWDEPRECATED_Multiple()
{
	DebugTN(__FUNCTION__);
	DeprecatedFunctionMultiCall(100); int lineTest=__LINE__;
	dyn_string deprList;
	dpGet(_fwDeprecated_DPE, deprList);

	assertEqual(1, dynlen(deprList), "Wrong number of recorded deprecated calls");
	assertEqual("DeprecatedTestFunction(...) at DeprecatedTestLib.ctl:"+lineDeprecatedTestFunction+" # DeprecatedFunctionMultiCall(...) at DeprecatedTestLib.ctl:"+lineDeprecatedFunctionMultiCall+ORIGIN_STRING,
				normalizeMsg(deprList[1]), "Wrong deprecation message");

//    DebugTN(deprList);
}

void test_fwGeneral_FWDEPRECATED_Depth_Level_3()
{
	DebugTN(__FUNCTION__);
	for (int i = 1; i <= 5; i++) {
		DeprecatedExtraDummyLevel(i);
	}
	dyn_string deprList;
	dpGet(_fwDeprecated_DPE, deprList);
	DebugTN(deprList);
	assertEqual(1, dynlen(deprList), "Wrong number of recorded deprecated calls");
	assertEqual("DeprecatedTestFunction(...) at DeprecatedTestLib.ctl:"+lineDeprecatedTestFunction+" # ParentDeprecatedTestFunction(...) at DeprecatedTestLib.ctl:"+lineParentDeprecatedTestFunction+ORIGIN_STRING,
				normalizeMsg(deprList[1]), "Wrong deprecation message");
}

void test_fwGeneral_FWDEPRECATED_NoArguments()
{
	DebugTN(__FUNCTION__);
	int lineTest; // remember the line number to be used below
	for (int i = 1; i <= 5; i++) {
		DeprecatedTestFunctionNoArguments();lineTest=__LINE__;
	}
	dyn_string deprList;
	dpGet(_fwDeprecated_DPE, deprList);
	DebugTN(deprList);
	assertEqual(1, dynlen(deprList), "Wrong number of recorded deprecated calls");
	assertEqual("DeprecatedTestFunctionNoArguments(...) at DeprecatedTestLib.ctl:"+lineDeprecatedTestFunctionNoArguments+" # "+__FUNCTION__+"(...) at test_FWDEPRECATED.ctl:"+lineTest+ORIGIN_STRING,
				normalizeMsg(deprList[1]), "Wrong deprecation message");
}

void test_fwGeneral_FWDEPRECATED_DifferentPlaces()
{
	DebugTN(__FUNCTION__);
	DeprecatedTestFunctionNoArguments(); int lineTest1=__LINE__; // remember the line nums to use below
	DeprecatedTestFunctionNoArguments(); int lineTest2=__LINE__;
	dyn_string deprList;
	dpGet(_fwDeprecated_DPE, deprList);

	assertEqual(2, dynlen(deprList), "Wrong number of recorded deprecated calls");
	assertEqual("DeprecatedTestFunctionNoArguments(...) at DeprecatedTestLib.ctl:"+lineDeprecatedTestFunctionNoArguments+" # "+__FUNCTION__+"(...) at test_FWDEPRECATED.ctl:"+lineTest1+ORIGIN_STRING,
				normalizeMsg(deprList[1]), "Wrong deprecation message");
	assertEqual("DeprecatedTestFunctionNoArguments(...) at DeprecatedTestLib.ctl:"+lineDeprecatedTestFunctionNoArguments+" # "+__FUNCTION__+"(...) at test_FWDEPRECATED.ctl:"+lineTest2+ORIGIN_STRING,
				normalizeMsg(deprList[2]), "Wrong deprecation message");
}
