#uses "test/fwGeneral/test_fwDPELock.ctl"

// This is the "Peer" for the main tester of fwDPELock
// that is started in a dedicated CTRL Manager,
// spawned from the test-suite setup scripts

main()
{
    DebugTN("test_fwDPELock_Peer.ctl Starting");
    dpSetWait(peer_comm_dp+".","READY");


    // as we want to cleanly exit on the stop command, 
    // we will make a loop with dpWaitForValue
    // instead of dpConnect of some kind
    // we also put a maximum time limit for running

    int maxRunningTime=10;

    time tStart=getCurrentTime();
    while (true) {
        bool timedOut=false;
        dyn_mixed retValues;
        dpWaitForValue(makeDynString(peer_comm_dp+".:_original.._value"), // we wait on this one
                makeDynAnytype(),          // awaited value
                makeDynString(peer_comm_dp+".:_original.._value"), // returned DPE values
                retValues,    // values will come here
                1,    // time to wait
                timedOut    // will indicate the timeout
        	);

        if ( getCurrentTime() > (tStart+maxRunningTime) ) {
    	    DebugTN("test_fwDPELock_Peer.ctl runs too long already. Stoping");
    	    dpSetWait(peer_comm_dp+".","");
    	    return;
        }

        if (timedOut) continue;
        switch (retValues[1]) {
    	    case "STOP" :
    		DebugTN("test_fwDPELock_Peer.ctl STOP was ordered. Exiting");
    		delay(0,50);
    		dpSetWait(peer_comm_dp+".","");
    		return;
    		break;
    	    case "LOCK" :
    		DebugTN("test_fwDPELock_Peer.ctl LOCK",test_dp+".:_lock._original._locked",true);
    		dpSetWait(test_dp+".:_lock._original._locked",true);
    		break;
    	    case "LOCK_GENERAL" :
    		DebugTN("test_fwDPELock_Peer.ctl LOCK",test_dp+".:_lock._general._locked",true);
    		dpSetWait(test_dp+".:_lock._general._locked",true);
    		break;
    	    case "UNLOCK" :
    		DebugTN("test_fwDPELock_Peer.ctl UNLOCK",test_dp+".:_lock._original._locked",false);
    		dpSetWait(test_dp+".:_lock._original._locked",false);
    		break;
    	    case "UNLOCK_GENERAL" :
    		DebugTN("test_fwDPELock_Peer.ctl UNLOCK",test_dp+".:_lock._general._locked",false);
    		dpSetWait(test_dp+".:_lock._general._locked",false);
    		break;
    	    default:
    		DebugTN("test_fwDPELock_Peer.ctl does not understand command",retValues[1]);
    		break;
        }
    }
}
