#uses "fwGeneral/fwGeneral.ctl"
#uses "fwUnitTestComponentAsserts.ctl"





/**
 *
 */
void test_FwGeneral_dynStringToString_()
{

	string resultString;

	fwGeneral_dynStringToString(makeDynString("oui", "non"),
								resultString,
								fwGeneral_DYN_STRING_DEFAULT_SEPARATOR
								);
	string expected = "oui|non";
	assertEqual( expected, resultString);
}

