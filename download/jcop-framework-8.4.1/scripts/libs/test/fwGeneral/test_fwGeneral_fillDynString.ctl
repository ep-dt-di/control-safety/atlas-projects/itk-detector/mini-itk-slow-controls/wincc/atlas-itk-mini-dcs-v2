#uses "fwGeneral/fwGeneral.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

dyn_string expected1;
dyn_string expected2;

void test_setup()
{
	expected1 = makeDynString("hello", "beautiful", "and", "magic", "world");
	expected2 = makeDynString("hello", "beautiful", "world", "", "", "");
}

/**
 * case number 1 : if the length of dyn_string is already >= requested size
 */
void test_FwGeneral_fillDynString_1()
{
	dyn_string exceptionInfo;
	dyn_string DS = makeDynString("hello", "beautiful", "and", "magic", "world");

	fwGeneral_fillDynString(DS, 4, exceptionInfo);
	assertEqual(expected1, DS);
}

/**
 * case number 2 : if the length of dyn_string is < requested size
 */
void test_FwGeneral_fillDynString_2()
{
	dyn_string exceptionInfo;
	dyn_string DS = makeDynString("hello", "beautiful", "world");

	fwGeneral_fillDynString(DS, 6, exceptionInfo);
	assertEqual(expected2, DS);
}

