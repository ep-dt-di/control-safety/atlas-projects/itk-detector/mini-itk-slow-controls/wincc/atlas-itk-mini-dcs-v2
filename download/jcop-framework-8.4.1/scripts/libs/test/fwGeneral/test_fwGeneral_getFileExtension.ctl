#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwGeneral/fwGeneral.ctl"

void test_fwGeneral_getFileExtension_emptyPath()
{
	string sFilePath = "";
	string sExpectedExtension = "";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}

void test_fwGeneral_getFileExtension_fileNameWithExtension()
{
	string sFilePath = "test.txt";
	string sExpectedExtension = "txt";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}

void test_fwGeneral_getFileExtension_fileNameWithoutExtension()
{
	string sFilePath = "dummy";
	string sExpectedExtension = "";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}

void test_fwGeneral_getFileExtension_filePathWithExtension()
{
	string sFilePath = "dummy/file.a";
	string sExpectedExtension = "a";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}

void test_fwGeneral_getFileExtension_filePathWithoutExtension()
{
	string sFilePath = "dummy/file";
	string sExpectedExtension = "";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}

void test_fwGeneral_getFileExtension_filePathWithExtensionAndDots()
{
	string sFilePath = "dummy.d/file.abc.def";
	string sExpectedExtension = "def";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}

void test_fwGeneral_getFileExtension_filePathWithoutExtensionButDots()
{
	string sFilePath = "dummy.d/file";
	string sExpectedExtension = "";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}

/**void test_fwGeneral_getFileExtension_unixHiddenFilePathWithoutExtension() // This fails currently
   {
   string sFilePath = "dummy/.file";
   string sExpectedExtension = "";
   assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
   }**/

void test_fwGeneral_getFileExtension_windowsFilePathWithoutExtensionButDots()
{
	string sFilePath = "dummy.d\\file";
	string sExpectedExtension = "";

	assertEqual(sExpectedExtension, fwGeneral_getFileExtension(sFilePath));
}
