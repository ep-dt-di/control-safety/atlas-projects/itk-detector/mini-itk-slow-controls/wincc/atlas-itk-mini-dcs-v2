#uses "fwViewer/fwViewer_edmsFileManager.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

class testFwViewer_edmsFileManager: fwViewer_edmsFileManager{
	string httpStatusText;
	public testFwViewer_edmsFileManager(shared_ptr<fwViewer_loginClass> loginObject = new fwViewer_loginClass(),
										shared_ptr<graphicalNotification> graphNotifcation = new graphicalNotification()
										,string statusText = "OK"
										) {

		fwViewer_edmsFileManager::initialise(loginObject, graphNotifcation);
		httpStatusText = statusText;
	}
	protected mapping getEdmsFile(mapping data, string filePath) {
		string status =  "TRUE";
		string httpStatusCode, errorMessage, filePath;

		mapping returnValue = makeMapping("status", status,"httpStatusCode",httpStatusCode,  "errorMessage", errorMessage, "filePath", filePath);
		return returnValue;
	}
	protected mapping getFile(string url, mapping option) {
		mapping returnValue = makeMapping("httpStatusText", httpStatusText);
		return returnValue;
	}

};
class testLoginClass : fwViewer_loginClass {
	string passwordd, userNamee ;
	bool userActive;
	bool loginStatus;

	public testLoginClass(string user ="", string pass = "", bool loginStatus1 = true) {
		passwordd = pass;
		userNamee = user;
		loginStatus = loginStatus1;
	}
	public string getActiveUser() {
		return userNamee;
	}
	public string getKey() {
		return passwordd;
	}
	public bool isUserActive() {
		return userActive;
	}
	public bool login () {
		return loginStatus;
	}
	private mapping getLoginData() {
		return makeMapping("userName", userNamee, "password", passwordd);
	}
};

class testGraphicalNotification : graphicalNotification {
	public int showWaitWindow(string messge, int timeOut) {
		return 0;
	}
	public int hideWaitWindow() {
		return 0;
	}
};


void test_getFileName_returnFileName() {
	string login = "login";
	string password = "password";
	string expectedFileName = "test.pdf";
	string url = "http://www.pdf995.com/samples/test.pdf";

	shared_ptr<testLoginClass> loginManger = new testLoginClass(login, password);
	shared_ptr<fwViewer_edmsFileManager> systemUnderTest = fwViewer_edmsFileManager::create(loginManger);

	string fileName = systemUnderTest.getFileName(url);
	assertEqual(expectedFileName, fileName);

	string expectedFileName = "pdf.pdf";
	string url = "http://www.pdf995.com/samples/pdf.pdf";
	string fileName = systemUnderTest.getFileName(url);
	assertEqual(expectedFileName, fileName);
}
void test_downloadFile_edmsFile_loginCancelled_returnStatusFalse() {
	string login = "login";
	string password = "password";
	string url = "https://edms.cern.ch/file/1302367/last_released/Analyse_Fonctionelle_UW65_new_docx_cpdf.pdf?page=104&pagemode=bookmarks";
	bool loginStatus = false;
	shared_ptr<testGraphicalNotification> graph =  new testGraphicalNotification();
	shared_ptr<testLoginClass> loginManger = new testLoginClass(login, password, loginStatus);
	shared_ptr<fwViewer_edmsFileManager> systemUnderTest = fwViewer_edmsFileManager::create(loginManger, graph);
	mapping returValue;
	returValue = systemUnderTest.downloadFile(url);

	string status;
	string expectedStatus = false;
	status = returValue["status"];
	assertEqual(expectedStatus,status);

	string errorMessage;
	string expectedErrorMessage = "Action cancelled";
	errorMessage = returValue["errorMessage"];
	assertEqual(expectedErrorMessage,errorMessage);
}

void test_downloadFile_edmsFile_returnStatusTrue() {
	string login = "login";
	string password = "password";
	string url = "https://edms.cern.ch/file/1302367/last_released/Analyse_Fonctionelle_UW65_new_docx_cpdf.pdf?page=104&pagemode=bookmarks";
	bool loginStatus = true;
	shared_ptr<testGraphicalNotification> graph =  new testGraphicalNotification();
	shared_ptr<testLoginClass> loginManger = new testLoginClass(login, password, loginStatus);
	testFwViewer_edmsFileManager systemUnderTest = testFwViewer_edmsFileManager(loginManger, graph);
	mapping returValue;
	returValue = systemUnderTest.downloadFile(url);

	string status;
	string expectedStatus = true;
	status = returValue["status"];
	assertEqual(expectedStatus,status);

}

void test_downloadFile_returnStatusTrue() {
	string login = "login";
	string password = "password";
	string url = "http://gdlp01.c-wss.com/gds/5/0300001715/02/PSSX1IS_CUG_EN_02.pdf";
	bool loginStatus = true;
	shared_ptr<testGraphicalNotification> graph =  new testGraphicalNotification();
	shared_ptr<testLoginClass> loginManger = new testLoginClass(login, password, loginStatus);
	testFwViewer_edmsFileManager systemUnderTest = testFwViewer_edmsFileManager(loginManger, graph);
	mapping returValue;
	returValue = systemUnderTest.downloadFile(url);

	string status;
	string expectedStatus = true;
	status = returValue["status"];
	assertEqual(expectedStatus,status);

}

void test_downloadFile_returnStatusFalse() {
	string login = "login";
	string password = "password";
	string url = "http://gdlp01.c-wss.com/gds/5/0300001715/02/PSSX1IS_CUG_EN_02.pdf";
	bool loginStatus = true;
	shared_ptr<testGraphicalNotification> graph =  new testGraphicalNotification();
	shared_ptr<testLoginClass> loginManger = new testLoginClass(login, password, loginStatus);
	testFwViewer_edmsFileManager systemUnderTest = testFwViewer_edmsFileManager(loginManger, graph, "httpStatusTextNotOk");
	mapping returValue;
	returValue = systemUnderTest.downloadFile(url);

	string status;
	string expectedStatus = false;
	status = returValue["status"];
	assertEqual(expectedStatus,status);

}



