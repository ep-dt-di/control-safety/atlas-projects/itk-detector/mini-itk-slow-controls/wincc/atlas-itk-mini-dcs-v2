#include <Manager.hxx>
#include <ManagerIdentifier.hxx>
#include <StartDpInitSysMsg.hxx>
#include <ManagerIdentifier.hxx>
#include <iostream>
using namespace std;

typedef enum REDUMANAGER_STATUS {REDUMANAGER_UNDEFINED = -1, REDUMANAGER_REDUPASSIVE = 0,
									REDUMANAGER_REDUACTIVE = 1, REDUMANAGER_SPLITPASSIVE=2,
									REDUMANAGER_SPLITACTIVE = 3};

class SplitModeListener;

class ReduCallback {

public:
   /*
     	Pure virtual method:
		Callback to be implemented to get notified when status changed
	*/
	virtual void reduStatusChanged(REDUMANAGER_STATUS oldStatus, REDUMANAGER_STATUS newStatus) = 0;

};

class ReduHandler
{
public:

	ReduHandler(ReduCallback* myManager);

	virtual ~ReduHandler();


	/*
		Return true if in status REDU ACTIVE or SPLIT ACTIVE
	*/
	bool isActive() ;

	/*
		Return true if in status REDU_ACTIVE or in SPLIT mode
	*/
	bool isActiveOrInSplitMode();
	


	/*
		Get the current status
	*/
	REDUMANAGER_STATUS getReduStatus() {
			return status;
	}
	
	
	/*
		Initialize the handling of split mode
		This method requires the Resources to be properly initialized so it should be called in the 
		run() method of the manager
	*/
	void initializeRedu();

	/*
		Force Recomputing of the status and call statusChanged if it changes
	*/
	void computeReduStatus();

	
	/*
		Return the human readable name for a given status
	*/
	static const char* reduStatusName(REDUMANAGER_STATUS status) {
		switch (status) {
			case REDUMANAGER_REDUACTIVE: return "REDU ACTIVE"; break;
			case REDUMANAGER_REDUPASSIVE: return "REDU PASSIVE"; break;
			case REDUMANAGER_SPLITACTIVE: return "SPLIT ACTIVE"; break;
			case REDUMANAGER_SPLITPASSIVE: return "SPLIT PASSIVE"; break;
			case REDUMANAGER_UNDEFINED: return "UNDEFINED"; break		;
		}
		return "???";
			
	}
	void doReceive( SysMsg &sysMsg );

protected:


private:
	REDUMANAGER_STATUS status ; // This is the status taking in consideration split mode
	int reduActive; // This is the status of the redu (active or passive). reduActive=1 in split mode but should be ignored
	SplitModeListener* itsSplitModeListener;
	ReduCallback* itsMan;

	void init();
};


class ReduManager : public Manager , public ReduCallback {
public:
		 //Constructor for a manager with the specified id.
	ReduManager (const ManagerIdentifier &manId);
 	ReduManager (const ManagerIdentifier &manId, CommonNameService *customCns);
	
	virtual ~ReduManager();

	/*
		Return true if in status REDU ACTIVE or SPLIT ACTIVE
	*/
	bool isActive() {
		return itsReduHandler->isActive();
	}

	/*
		Return true if in status REDU_ACTIVE or in SPLIT mode
	*/
	bool isActiveOrInSplitMode() {
		return itsReduHandler->isActiveOrInSplitMode();
	}
	


	/*
		Get the current status
	*/
	REDUMANAGER_STATUS getReduStatus() {
		return itsReduHandler->getReduStatus();
	}
	
	
	/*
		Initialize the handling of split mode
		This method requires the Resources to be properly initialized so it should be called in the 
		run() method of the manager
	*/
	void initializeRedu() {
		itsReduHandler->initializeRedu();
	}


	
	/*
		Return the human readable name for a given status
	*/
	static const char* reduStatusName(REDUMANAGER_STATUS status) {
		return ReduHandler::reduStatusName(status);
	
			
	}
	void doReceive( SysMsg &sysMsg ) ;
			
private:
	void init();
	ReduHandler* itsReduHandler;
};
