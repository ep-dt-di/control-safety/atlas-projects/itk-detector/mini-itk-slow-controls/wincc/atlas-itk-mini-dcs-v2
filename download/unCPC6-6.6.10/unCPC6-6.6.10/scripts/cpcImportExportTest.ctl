main()
{
    string filename = getPath(DATA_REL_PATH, "generated.txt");
    int driverNumber = 2;
    
    fwDevice_initialize();
    dyn_string dsExceptions;
    bool bTimerExpired = false;
    unGenericUtilities_startManagerWait("WCCILsim", "-num " + driverNumber, dsExceptions, getSystemName(), 30, bTimerExpired);

    bool ready = cpcImportExportTest_readyToTest(filename, driverNumber);
    if (!ready) {
        DebugN("errors!");
        return;
    }
    
    mapping results;
    dyn_dyn_string testData, testAssert;
    cpcImportExportTest_readTestFile(filename, testData, testAssert);
    
    string plcType = cpcImportExportTest_getPlcType(testData);
    
    cpcImportExportTest_checkFile(testData, testAssert, plcType, driverNumber, results, "1. check");
    cpcImportExportTest_importFile(testData, testAssert, plcType, driverNumber, results, "2. import");
    cpcImportExportTest_exportFile(testData, testAssert, plcType, driverNumber, results, "3. export");
    cpcImportExportTest_importFile(testData, testAssert, plcType, driverNumber, results, "4. reimport");
    cpcImportExportTest_exportFile(testData, testAssert, plcType, driverNumber, results, "5. reexport");

    cpcImportExportTest_createReport(getPath(LOG_REL_PATH) + "cpcImportExportTest.xml", results);
}

// cpcImportExportTest_getPlcType
/**
Purpose: get the PLC type from testData

Parameters:
            testData, dyn_dyn_string, input, representation of importation file
            return value: string, the PLC type if found, empty if not present
            
*/
string cpcImportExportTest_getPlcType(dyn_dyn_string testData){
        dyn_string dsConfig;
        for(int i=1; i<=dynlen(testData); i++){
            dsConfig = testData[i];
            if(dynlen(dsConfig) >= 2){
                if(dsConfig[1] == UN_PLC_COMMAND) {
                    return strsplit(dsConfig[2], ",")[1];
                }
            }
        }
        return "";
    }

bool cpcImportExportTest_readyToTest(string fileName, int driverNumber)
{
    bool check = true;
    dyn_string exceptionInfo;
    
    // unImportDevice_checkDriver(driverNumber, exceptionInfo);
    unImportDevice_checkCtrl(exceptionInfo);
    //unImportDevice_checkDist(exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        DebugN("cpcImportExportTest_readyToTest fails: ", exceptionInfo);
        check = false;
    }
    string fileAccesError = unImportDevice_isFileAccessible(fileName);
    if (fileAccesError != "DEBUGFILEEXISTS") {
        DebugN("cpcImportExportTest_readyToTest: can't access file ", fileName);
        check = false;
    }

    return check;
}

cpcImportExportTest_readTestFile(string filename, dyn_dyn_string& testData, dyn_dyn_string& testAssert) {
    dyn_string dsConfig;
    int lineIndex;
    file ff = fopen(filename, "r");
    while (feof(ff) == 0) {
        unImportDevice_getNextLine(ff, dsConfig, lineIndex);
        if (dynlen(dsConfig) == 0) continue;
        
        if (dsConfig[1] == UN_PLC_COMMAND || dsConfig[1] == UN_DELETE_COMMAND) {
            dynAppend(testData, dsConfig);
            dynAppend(testAssert, makeDynString("ok", ""));
        } else {
            dyn_string assert;
            dynAppend(assert, dsConfig[dynlen(dsConfig)]);
            dynRemove(dsConfig, dynlen(dsConfig));
            if (assert[1] != "ok") {
                dynAppend(assert, dsConfig[dynlen(dsConfig)]);
                dynRemove(dsConfig, dynlen(dsConfig));
            } else {
                dynAppend(assert, "");
            }
            dynAppend(testData, dsConfig);
            dynAppend(testAssert, assert);
        }
    }
    fclose(ff);
}

cpcImportExportTest_checkFile(dyn_dyn_string testData, dyn_dyn_string testAssert, string sFrontEndType, int driverNumber, mapping& results, string suitename)
{
    dyn_string dsConfig, dsAllAliasesDpe, dsAlias;
    string sFEName = "", sFEApplication = "", sPrefix, sFEVersion;
    bool bEvent16;
    dyn_string dsRunningArchive, dsArchive, unicosObjectsNames;
    mapping mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveDpAvail, mArchiveRequiredSpace, objectInfo;
    int iPlcNumber;

    dpGetAllAliases(dsAllAliasesDpe, dsAlias);
    unImportDevice_getRunningArchiveName(dsArchive);
    unImportDevice_getAllArchiveConfig(dsRunningArchive, mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveDpAvail);
    unGenericDpFunctions_getUnicosObjects(unicosObjectsNames);

    unImportDevice_getApplicationCharacteristics(bEvent16, sPrefix);
    mapping checkResults;
    
    for (int i = 1; i <= dynlen(testData); i++) {
        dsConfig = testData[i];
        dyn_string exceptionInfo, dsDeleteDps;
        
        if (dsConfig[1] == UN_PLC_COMMAND) {
            sFEName = dsConfig[UN_CONFIG_PLC_NAME+1];
            sFEApplication = dsConfig[UN_CONFIG_PLC_SUBAPPLICATION+1];
        }

        if (dynContains(unicosObjectsNames, dsConfig[1])) {
            if (dynlen(dsConfig) < 3) {
                unImportDevice_printInfo(logFile, errorLines, "ERROR: Line " + lineIndex + getCatStr("unGeneration", "DEBUGBADFORMAT"));
                continue;
            }
            string sDeviceDpName = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, dsConfig[1], dsConfig[2]);
            unImportDevice_checkDeviceAlias(dsAlias, sDeviceDpName, dsConfig[3], dsDeleteDps, exceptionInfo);
        }
        dsDeleteDps = unImportDevice_check(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, driverNumber, "ValueArchive_0000", "ValueArchive_0000", "ValueArchive_0000", bEvent16, dsRunningArchive, iPlcNumber);
        /*
        if (dynlen(exceptionInfo) == 0) {
            unImportDevice_checkArchive(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, driver, 
                    sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16, dsArchive, iPlcNumber, false, sFEVersion, 
                    mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveRequiredSpace);
        }
        */

        string errorMessage;
        if (dynlen(exceptionInfo) == 0) {
            if (testAssert[i][1] != "ok") {
                errorMessage = "expected error '" + testAssert[i][1] + "' has not received";
            }
        } else {
            strreplace(exceptionInfo[2], "\n", " ");
            if (testAssert[i][2] == "") {
                errorMessage = "unexpexted error: '" + exceptionInfo[2] + "'";
            } else if (!patternMatch(testAssert[i][2], exceptionInfo[2])) {
                errorMessage = "instead of error '" + testAssert[i][2] + "' received '" + exceptionInfo[2] + "'";
            }
        }
        if (errorMessage == "") {
            DebugN("CHECK PASSED");
            checkResults[dsConfig[1]+" | "+dsConfig[3]] = "OK,checked";
        } else {
            DebugN("CHECK FAILED for " + dsConfig[1]+" | "+dsConfig[3] + ", " + errorMessage);
            checkResults[dsConfig[1]+" | "+dsConfig[3]] = "ERROR, CHECK failure: " + errorMessage;
        }
    }
    results[suitename] = checkResults;
}

cpcImportExportTest_importFile(dyn_dyn_string testData, dyn_dyn_string testAssert, string sFrontEndType, int driverNumber, mapping& results, string suitename)
{
    mapping importResults;
    
    dyn_string dsRunningArchive;
    string sFEName, sFEApplication, plcType, sPrefix;
    bool bEvent16;
    int iPlcNumber = 0;
    unImportDevice_getApplicationCharacteristics(bEvent16, sPrefix);

    for (int i = 1; i <= dynlen(testData); i++) {
        dyn_string exceptionInfo;
        dyn_string dsConfig = testData[i];
        if (testAssert[i][1] != "ok") continue;
        if (dsConfig[1] == UN_PLC_COMMAND) {
            sFEName = dsConfig[UN_CONFIG_PLC_NAME+1];
            sFEApplication = dsConfig[UN_CONFIG_PLC_SUBAPPLICATION+1];
            plcType = dsConfig[UN_CONFIG_PLC_TYPE+1];
            iPlcNumber = dsConfig[UN_CONFIG_PLC_NUMBER+1];
        }
 
        unImportDevice_import(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, driverNumber,
                      "ValueArchive_0000", "ValueArchive_0000", "ValueArchive_0000", bEvent16, dsRunningArchive, iPlcNumber);
        
        if (dynlen(exceptionInfo) > 0) {
            DebugN("Error while import dp: " + exceptionInfo);
            importResults[dsConfig[1]+" | "+dsConfig[3]] = "ERROR, Error while import dp: " + exceptionInfo; 
        } else {
            importResults[dsConfig[1]+" | "+dsConfig[3]] = "OK, imported";
        }
    }
    results[suitename] = importResults;
}

cpcImportExportTest_exportFile(dyn_dyn_string testData, dyn_dyn_string testAssert, string sFrontEndType, int driverNumber, mapping& results, string suitename)
{
    mapping exportResults;
    string sPlcName, sSubApplication;
    string currentDeviceType;
    dyn_dyn_string exportedDevices;
    
    for (int i = 1; i <= dynlen(testData); i++) {
        dyn_string exceptionInfo;
        dyn_string dsConfig = testData[i];
        
        if (dsConfig[1] == UN_PLC_COMMAND) {
            sPlcName = dsConfig[UN_CONFIG_PLC_NAME+1];
            sSubApplication = dsConfig[UN_CONFIG_PLC_SUBAPPLICATION+1];
            continue;
        }
        if (dsConfig[1] == UN_DELETE_COMMAND) continue;
        if (testAssert[i][1] != "ok") continue;
    
        if (currentDeviceType != dsConfig[1]) {
            currentDeviceType = dsConfig[1];
            exportedDevices = unExport_loadDPs(sFrontEndType, sPlcName, sSubApplication, currentDeviceType, exceptionInfo);
        }
        
        if (dynlen(exceptionInfo) > 0) {
                exportResults[dsConfig[1]+" | "+dsConfig[3]] = "ERROR, Error importing dp: " + exceptionInfo;
        } else {
            dyn_string exportedData = cpcImportExportTest_findDeviceByAlias(exportedDevices, dsConfig[3]);
            if (dynlen(exportedData) == 0) {
                exportResults[dsConfig[1]+" | "+dsConfig[3]] = "ERROR, Can't find by alias " + dsConfig[3];
            } else if (cpcImportExportTest_compareData(dsConfig, exportedData) > 0) {
                exportResults[dsConfig[1]+" | "+dsConfig[3]] = "ERROR, data doesn't equal\n" + dsConfig + "\n" + exportedData;
            } else {
                exportResults[dsConfig[1]+" | "+dsConfig[3]] = "OK, exported";
            }
        }
    }

    results[suitename] = exportResults;
}

dyn_string cpcImportExportTest_findDeviceByAlias(dyn_dyn_string data, string alias) {
    for (int j = 1; j <= dynlen(data); j++) {
      if (data[j][3] == alias) {
        return data[j];
      }
    }
    return makeDynString(); 
}

int cpcImportExportTest_compareData(dyn_string original_data, dyn_string new_data)
{
    int result = 0;
    if (dynlen(original_data) != dynlen(new_data)) {
        if (dynlen(original_data) + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH == dynlen(new_data)) {
            string dpName = original_data[1];
            int pos = 11 + cpcConfigGenericFunctions_getConstantDPLength(original_data[1]);
            dynRemove(new_data, pos);
            dynRemove(new_data, pos);
            dynRemove(new_data, pos);
        }
    }

    int len = dynlen(original_data);
    int len2 = dynlen(new_data);
    if (len2 < len) {
        len = len2;
    }
    for (int i = 1; i <= len; i++) {
        if (!cpcImportExportTest_itemsAreEqual(original_data[i], new_data[i])) {
            DebugN("'" + original_data[i] + "' != '" + new_data[i] + "' pos " + i);
            result += 1;
        }        
    }
    return result;
}
const string CPC_IMPORT_EXPORT_TEST_COMMA = ",";
bool cpcImportExportTest_itemsAreEqual(string item1, string item2) {
  if (strpos(item1, CPC_IMPORT_EXPORT_TEST_COMMA) >= 0 || strpos (item1, CPC_IMPORT_EXPORT_TEST_COMMA) >= 0) { // compare comma-separated lists
    dyn_string dsItems1 = strsplit(item1, CPC_IMPORT_EXPORT_TEST_COMMA);
    dyn_string dsItems2 = strsplit(item2, CPC_IMPORT_EXPORT_TEST_COMMA);
    if (dynlen(dsItems1) != dynlen(dsItems2)) {
      return false;
    } else {
      dyn_string intersection = dynIntersect(dsItems1, dsItems2);
      return dynlen(intersection) == dynlen(dsItems1);
    }
  } else {
    return cpcImportExportTest_formatData(item1) == cpcImportExportTest_formatData(item2) ||
        (item1 == "-" && item2 == "") ||
        (item1 == "0" && item2 == "N") ||
        (item1 == "0" && item2 == "FALSE") ||
        (item1 == "1" && item2 == "Y") ||
        (item1 == "1" && item2 == "TRUE") ||
        (item1 == "Y" && item2 == "A");
  }
}
string cpcImportExportTest_formatData(string data) {
  string prefix = "";
  if (strlen(data) > 1 && substr(data, 0, 1) == "-") {
    prefix = "-";
    data = substr(data, 1, strlen(data) - 1);
  }
  if (patternMatch("[0123456789]*", data)){
    return prefix + ((string) ((float) data));
  } else if (data == "ValueArchive_0000") {
    return "";
  } else if (data == "True" || data == "TRUE") {
    return "Y";
  } else if (data == "False" || data == "FALSE") {
    return "N";
  } else {
    return data;
  }
}


cpcImportExportTest_createReport(const string filePath, mapping results)
{
    int docNum = xmlNewDocument(); 
    xmlAppendChild(docNum, -1, XML_PROCESSING_INSTRUCTION_NODE, "xml version=\"1.0\"  encoding=\"UTF-8\"");
    
    int rootID = xmlAppendChild(docNum, -1, XML_ELEMENT_NODE, "testsuites");
    
    dyn_string suitenames = mappingKeys(results);
    dynSortAsc(suitenames);

    for (int i = 1; i <= dynlen(suitenames); i++) {
        string testsuiteName = suitenames[i];
        int testsuiteID = xmlAppendChild(docNum, rootID, XML_ELEMENT_NODE, "testsuite");
        xmlSetElementAttribute(docNum, testsuiteID , "name", testsuiteName);
        mapping rr = results[testsuiteName];

        for (int i = 1; i <= mappinglen(rr); i++) {
            string testcaseName = mappingGetKey(rr, i);
            string testcaseMessage = rr[testcaseName];
            int testcaseID = xmlAppendChild(docNum, testsuiteID, XML_ELEMENT_NODE, "testcase");
            xmlSetElementAttribute(docNum, testcaseID , "name", testcaseName);
            
            string tagName = strpos(testcaseMessage, "OK,") == 0 ? "success" : "failure";
            int resultID = xmlAppendChild(docNum, testcaseID, XML_ELEMENT_NODE, tagName);
            xmlSetElementAttribute(docNum, resultID , "message", testcaseMessage);
        }
    }
    DebugN("Creating test report file: " + filePath);
    xmlDocumentToFile(docNum, filePath);
}

