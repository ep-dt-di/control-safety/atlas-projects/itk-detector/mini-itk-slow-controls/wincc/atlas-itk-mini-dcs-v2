/**@file

@brief Common function for faceplate's button animation.

@author Alexey Merezhin (EN-ICE-SIC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

*/

/**Set button to the given state

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param button the button name
@param buttonEnabled button state to set
*/
cpcButton_setButtonState(string button, bool buttonEnabled) {
    if (shapeExists(button)) {
        setValue(button, "enabled", buttonEnabled);
    }
}

/**Generic way to set buttons state

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsButtons buttons list
@param dsUserAccess pass user access list
@param deviceName device name
@param sDpType device type
@param dsData device dpe's values
@param exceptionInfo exceptions
*/
cpcButton_ButtonSetState(dyn_string dsButtons, dyn_string dsUserAccess, string deviceName, string sDpType, dyn_string dsData, dyn_string &exceptionInfo) {
    bool locked = dsData[1];
    string selectedManager = dsData[2];
    string localManager = unSelectDeselectHMI_getSelectedState(locked, selectedManager);
    bool selected = (localManager == "S");
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        dyn_string tmpExceptionInfo;
        string accessMethod;
        string accessMethod = "cpcButton_" + sDpType + "_" + dsButtons[i];
        if (!isFunctionDefined(accessMethod)) {
            accessMethod = "cpcButton_" + dsButtons[i];
        }
        evalScript(tmpExceptionInfo, "dyn_string main(string deviceName, string sDpType, bool buttonEnabled, bool selected, dyn_string dsData) {" +
                   "dyn_string exceptionInfo;" +
                   accessMethod  + "(deviceName, sDpType, buttonEnabled, selected, dsData, exceptionInfo);" +
                   "return exceptionInfo; }", makeDynString(), deviceName, sDpType, dynContains(dsUserAccess, dsButtons[i]) > 0, selected, dsData);
        dynAppend(exceptionInfo, tmpExceptionInfo);
    }
}

/**AllowRestart button animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param sDpType device type
@param buttonEnabled is button enabled
@param selected is device selected
@param dsData device dpe's values
@param exceptionInfo exceptions
*/
cpcButton_AllowRestart(string deviceName, string sDpType, bool buttonEnabled, bool selected, dyn_string dsData, dyn_string &exceptionInfo) {
    string font;
    bool bMRestart = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
    bool bRstartFS = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_RSTARTFS, exceptionInfo);
    if (bMRestart) {
        bit32 stsReg01Value = dsData[3];
        bool bStsReg01Bad = dsData[4];
        bit32 stsReg02Value = dsData[5];
        bool bStsReg02Bad = dsData[6];
        buttonEnabled = buttonEnabled && cpcButton_IsAllowRestartEnabled(deviceName, sDpType, stsReg01Value, bStsReg01Bad, stsReg02Value, bStsReg02Bad, bRstartFS);
        font = "-11,0,0,0,700,0,0,0,0,3,2,1,34,Arial";
    } else {
        buttonEnabled = false;
        font = "-11,0,0,0,700,0,0,1,0,3,2,1,34,Arial";
    }

    setMultiValue(UN_FACEPLATE_BUTTON_PREFIX + CPC_FACEPLATE_BUTTON_ALLOW_RESTART, "enabled", buttonEnabled,
                  UN_FACEPLATE_BUTTON_PREFIX + CPC_FACEPLATE_BUTTON_ALLOW_RESTART, "font", font);
}

/**AllowRestart button animation for PCO

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param sDpType device type
@param buttonEnabled is button enabled
@param selected is device selected
@param dsData device dpe's values
@param exceptionInfo exceptions
*/
cpcButton_ProcessControlObject_AllowRestart(string deviceName, string sDpType, bool buttonEnabled, bool selected, dyn_string dsData, dyn_string &exceptionInfo) {
    string font;
    bool bMRestart = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
    bool bRstartFS = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_RSTARTFS, exceptionInfo);
    if (bMRestart) {
        bit32 stsReg01Value = dsData[3];
        bool bStsReg01Bad = dsData[4];
        bit32 stsReg02Value = dsData[5];
        bool bStsReg02Bad = dsData[6];
        buttonEnabled = buttonEnabled && cpcButton_IsAllowRestartEnabled(deviceName, sDpType, stsReg01Value, bStsReg01Bad, stsReg02Value, bStsReg02Bad, bRstartFS);
        font = "Arial,8,-1,5,75,0,0,0,0,0";
    } else {
        buttonEnabled = false;
        font = "Arial,8,-1,5,75,0,0,1,0,0";
    }

    setMultiValue(UN_FACEPLATE_BUTTON_PREFIX + CPC_FACEPLATE_BUTTON_ALLOW_RESTART, "enabled", buttonEnabled,
                  UN_FACEPLATE_BUTTON_PREFIX + CPC_FACEPLATE_BUTTON_ALLOW_RESTART, "font", font);
}

bool cpcButton_IsAllowRestartEnabled(string deviceName, string dpType, bit32 stsReg01Value, bool bStsReg01Bad, bit32 stsReg02Value, bool bStsReg02Bad, bool bRstartFS) {
    if (dpType == UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME) {
        return (!getBit(stsReg01Value, CPC_StsReg01_FUSTOPAL) || bRstartFS) &&
               !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
               !bStsReg01Bad && !bStsReg02Bad;
    } else {
        return (!getBit(stsReg02Value, CPC_StsReg02_FUSTOPIST) || bRstartFS) &&
               !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
               !bStsReg01Bad && !bStsReg02Bad;
    }
}


cpcButton_AckAlarm(string deviceName, string dpType, bool buttonEnabled, bool selected, dyn_string dsData, dyn_string &exceptionInfo) {
    bit32 stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];
    bit32 stsReg02Value = dsData[5];
    bool bStsReg02Bad = dsData[6];
    setMultiValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_ACK_ALARM, "enabled", buttonEnabled && cpcButton_isAckEnabled(deviceName, dpType, stsReg01Value, bStsReg01Bad, stsReg02Value, bStsReg02Bad));
}

bool cpcButton_isAckEnabled(string deviceName, string dpType, bit32 stsReg01Value, bool bStsReg01Bad, bit32 stsReg02Value, bool bStsReg02Bad) {
    if (dpType == "CPC_MassFlowController") {
        return !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
               !bStsReg01Bad && !bStsReg02Bad;
    } else if (dpType == "CPC_ProcessControlObject") {
        return !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
               !bStsReg01Bad && !bStsReg02Bad;
    } else {
        return !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
               !bStsReg01Bad && !bStsReg02Bad;
    }
}

/**Set up g_dsUserAccess in device's set-value panels

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
cpcButton_SetValue_ButtonUserAccess(string dpName) {
    bool operator, expert, admin, bActionDefined = false;
    dyn_bool dbPermissions;
    dyn_string exceptionInfo;
    string sOperAction, sExpAction, sAdminAction;

    unGenericDpFunctions_getAccessControlPriviledgeRigth(dpName, g_dsDomainAccessControl, dbPermissions, sOperAction, sExpAction, sAdminAction, exceptionInfo);
    bActionDefined = (sOperAction != UNICOS_ACCESSCONTROL_DOMAINNAME) || (sExpAction != UNICOS_ACCESSCONTROL_DOMAINNAME) || (sAdminAction != UNICOS_ACCESSCONTROL_DOMAINNAME);
    operator = dbPermissions[2];
    expert = dbPermissions[3];
    admin = dbPermissions[4];
    g_dsUserAccess = makeDynString();
    if (operator) {
        if (bActionDefined) {
            dynAppend(g_dsUserAccess, strsplit(sOperAction, UN_ACCESS_CONTROL_SEPARATOR));
        }
    }
    if (expert) {
        if (bActionDefined) {
            dynAppend(g_dsUserAccess, strsplit(sExpAction, UN_ACCESS_CONTROL_SEPARATOR));
        } else {
            dynAppend(g_dsUserAccess, makeDynString(CPC_FACEPLATE_BUTTON_SAVE_VALUE));
        }
    }
    if (admin) {
        if (bActionDefined) {
            dynAppend(g_dsUserAccess, strsplit(sAdminAction, UN_ACCESS_CONTROL_SEPARATOR));
        }
    }
}
