/**@file

@brief This library contains functions called from other unCPC scripts.

@author Jonas Arroyo (BE-ICS-FD)
@author Ivan Koblik (IT-CO-FE)
@author Nikolay Kulman (IT-CO-FE)
@author Geraldine Thomas (IT-CO-FE)
@author Francisco Mico (IT-CO-FE)
@author Alexey Merezhin (EN-ICE-PLC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Creation Date
  14.11.2003

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL
*/


const string PARAM_DPE_VALUE = "value:"; // used to specify the dpe's value instead of addressing
//----------------------------------------------------------------------------------------------------------------------------------
/** Check data of the $sDeviceType object before configuration. It calls or reproduces following calls:
        unConfigGenericFunctions_checkParameters(dsConfigs, deviceType, exceptionInfo);
        unConfigGenericFunctions_checkDeviceConfig(dsConfigs, deviceType, exceptionInfo);

Device can specify its own checkConfig function by specifying it in device type configuration.
Also it can overwrite the check for specific front-end as in following example for CMW front end of CPC_WordStatus object:
{code}
CMW_CPC_WordStatus_checkConfig(dyn_string configLine, dyn_string &exceptionInfo) {
    cpcConfigGenericFunctions_checkDeviceConfig(UN_CONFIG_CPC_WORDSTATUS_DPT_NAME, configLine, exceptionInfo);
}
{code}

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_genericCheckDeviceConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  // Initialize CPC constants (performance issue)
  cpcConfigGenericFunctions_initConstants();

    if (dynlen(dsConfigs) >= UN_CONFIG_COMMON_LENGTH) {
        // 1. Common
        unConfigGenericFunctions_checkParameters(dsConfigs, $sDeviceType, exceptionInfo);
        // check if there's special implementation or go with default one
        dyn_string dsDollar, exceptionInfoTemp;
        string sFrontEndType;
        if (unGenericObject_shapeExists("TextFieldPlcType")) {
            sFrontEndType = TextFieldPlcType.text;
        } else {
            sFrontEndType = $sFrontEndType;
            dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
        }
        sFrontEndType = unImportDevice_getFrontEndDeviceType(sFrontEndType);

        string sFunction = sFrontEndType + "_" + $sDeviceType + UN_CONFIG_DEVICE_CHECKFUNCTIONSUFFIX;

        if (isFunctionDefined(sFunction)) {
            evalScript(exceptionInfoTemp, "dyn_string main(dyn_string dsConfigs) {" +
                       "dyn_string exceptionInfo;" +
                       "if (isFunctionDefined(\"" + sFunction + "\"))" +
                       "    {" +
                       "    " + sFunction + "(dsConfigs, exceptionInfo);" +
                       "    }" +
                       "else " +
                       "    {" +
                       "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNCHECK\"),\"\");" +
                       "    }" +
                       "return exceptionInfo; }", dsDollar,
                       dsConfigs);
        } else {
            cpcConfigGenericFunctions_checkDeviceConfig($sDeviceType, dsConfigs, exceptionInfo);
        }
        dynAppend(exceptionInfo, exceptionInfoTemp);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "CPC_WordStatusConfig_checkConfig: " + getCatStr("unGeneration", "BADINPUTS"), "");
        return;
    }
}

/** Creates dp (or checks if it is already presented) and sets configuration for the $sDeviceType object. That calls or reproduces following calls:
        unConfigGenericFunctions_createDp
        unConfigGenericFunctions_setCommon
        unConfigGenericFunctions_setDeviceConfig

Device can specify its own setConfig function by specifying it in device type configuration.
Also it can overwrite the set for specific front-end as in following example for Schneider (_UnPlc) front end of CPC_WordStatus object:
{code}
_UnPlc_CPC_WordStatus_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo) {
    cpcConfigGenericFunctions_setDeviceConfig(dsConfigs, exceptionInfo);
}
{code}

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_genericSetDeviceConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string sDpName;


  // Initialize CPC constants (performance issue)
  cpcConfigGenericFunctions_initConstants();


  // 1. Initialization : checking + dpCreate
  unConfigGenericFunctions_checkAll(dsConfigs, $sDeviceType, exceptionInfo);
  if( dynlen(exceptionInfo) <= 0 )
  {
    // Good inputs
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    unConfigGenericFunctions_createDp(sDpName, $sDeviceType, exceptionInfo);
    if( dynlen(exceptionInfo) <= 0 )
    {
      // Creation ok or dp already exists
      // 2. Generate device

      // 2.1. Parameters
      unConfigGenericFunctions_setCommon(dsConfigs, exceptionInfo);

      // 2.2 call the device check function
      dyn_string dsDollar, exceptionInfoTemp;
      string sFrontEndType;
      if( unGenericObject_shapeExists("TextFieldPlcType") )
      {
        sFrontEndType = TextFieldPlcType.text;
      }
      else
      {
        sFrontEndType = $sFrontEndType;
        dsDollar      = makeDynString("$sFrontEndType:"       + $sFrontEndType,
                                      "$dsArchiveList:"       + $dsArchiveList,
                                      "$sFrontEndVersion:"    + $sFrontEndVersion,
                                      "$bCheckBeforeImport:"  + $bCheckBeforeImport,
                                      "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
      }

      if( isDollarDefined("$bIgnoreDriverRelatedConfigs") )
      {
        dynAppend(dsDollar, "$bIgnoreDriverRelatedConfigs:" + $bIgnoreDriverRelatedConfigs);
      }

      sFrontEndType = unImportDevice_getFrontEndDeviceType(sFrontEndType);
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_setDeviceConfig", sFrontEndType, $sDeviceType, dsDollar, "-");

      string sFunction = sFrontEndType + "_" + $sDeviceType + UN_CONFIG_DEVICE_SETFUNCTIONSUFFIX;
      // DebugTN("cpcConfigGenericFunctions_genericSetDeviceConfig() -> sFunction = " + sFunction + ", isDefined = " + isFunctionDefined(sFunction));

      if( isFunctionDefined(sFunction) )
      {
        evalScript(exceptionInfoTemp, "dyn_string main(dyn_string configLine)"            +
                                      "{"                                                 +
                                      "  dyn_string exceptionInfo;"                       +
                                      "  if( isFunctionDefined(\"" + sFunction + "\") )"  +
                                      "  {"                                               +
                                      "    " + sFunction + "(configLine, exceptionInfo);" +
                                      "  }"                                               +
                                      "  else"                                            +
                                      "  {"                                               +
                                      "    fwException_raise(exceptionInfo,"              +
                                      "                      \"ERROR\","                  +
                                      "                      " + sFunction + "\" \" + getCatStr(\"unGeneration\",\"UNKNOWNSET\")," +
                                      "                      \"\");"                      +
                                      "  }"                                               +
                                      "  return exceptionInfo;"                           +
                                      "}",
                                      dsDollar,
                                      dsConfigs);
      }
      else
      {
        cpcConfigGenericFunctions_setDeviceConfig(dsConfigs, exceptionInfo);
      }

      dynAppend(exceptionInfo, exceptionInfoTemp);

    }
  }
}





/**Generic function to initialize CPC constants for import process

Initialization function to cache CPC constants during the import process

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param None
@return None

*/
void cpcConfigGenericFunctions_initConstants()
{
  dyn_string exceptionInfo;


  if( globalExists("g_mCpcConst") == FALSE )
  {
    // DebugTN("cpcConfigGenericFunctions_initConstants() -> creating global g_mCpcConst");
    if( cpcConfigGenericFunctions_createGlobalConst(exceptionInfo) == FALSE )
    {
      DebugTN("cpcConfigGenericFunctions_getConstantByName() -> Error, creating global variable: " + g_mCpcConst + ", due to: " + exceptionInfo[2]);
      return;
    }

    // Initialize device constants
    CPC_AnaDigConfig_initializeConstants();
    CPC_AnaDOConfig_initializeConstants();
    CPC_AnalogAlarmConfig_initializeConstants();
    CPC_AnalogConfig_initializeConstants();
    CPC_AnalogInputConfig_initializeConstants();
    CPC_AnalogOutputConfig_initializeConstants();
    CPC_AnalogParameterConfig_initializeConstants();
    CPC_AnalogStatusConfig_initializeConstants();
    CPC_ControllerConfig_initializeConstants();
    CPC_DigitalAlarmConfig_initializeConstants();
    CPC_DigitalInputConfig_initializeConstants();
    CPC_DigitalOutputConfig_initializeConstants();
    CPC_DigitalParameterConfig_initializeConstants();
    CPC_LocalConfig_initializeConstants();
    CPC_MassFlowControllerConfig_initializeConstants();
    CPC_OnOffConfig_initializeConstants();
    CPC_ProcessControlObjectConfig();
    CPC_SteppingMotorConfig_initializeConstants();
    CPC_Word2AnalogStatusConfig_initializeConstants();
    CPC_WordParameterConfig_initializeConstants();
    CPC_WordStatusConfig_initializeConstants();
    cpcRcpBuffersConfig_initializeConstants();

  }

}



/**Generic function that check device config.

Check validness of importation line using getConfig(), getParamNames() functions and importation constants defined in the device config script.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType device type
@param dsConfigs importation line, processed in the unCore
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_checkDeviceConfig(string deviceType, dyn_string dsConfigs, dyn_string &exceptionInfo)
{
    if (dynlen(dsConfigs) < UN_CONFIG_COMMON_LENGTH) { // totally broken config
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkDeviceConfig: " + deviceType + " importation line is less that minimum", "");
    }

    bool bHasArchive;

    // check length
    if (!cpcConfigGenericFunctions_checkConfigLength(deviceType, dsConfigs, bHasArchive)) {
        int deviceLength = UN_CONFIG_COMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPLength(deviceType) + cpcConfigGenericFunctions_getConstantDPAdditionalLength(deviceType);
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkDeviceConfig: " + deviceType + " expected length " + (deviceLength + 2) + "(+3) differs from config length " + (dynlen(dsConfigs) + 2), "");
        return;
    }

    // check archives
    if (bHasArchive) {
        cpcConfigGenericFunctions_checkArchiveConfig(deviceType, dsConfigs, exceptionInfo);
        if (dynlen(exceptionInfo) > 0) {
            fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkDeviceConfig: " + deviceType + " " + getCatStr("unGeneration", "BADINPUTS"), "");
            return;
        }
    }

    string plcType = _cpcConfigGenericFunctions_getFrontEndType();


    //// check device specific params
    mapping deviceAddresses = cpcConfigGenericFunctions_getDPEConfig(deviceType);
    dyn_string deviceAdrNames = mappingKeys(deviceAddresses);

    // check addresses
    for (int i = 1; i <= dynlen(deviceAdrNames); i++) {
        string currentAddrName = deviceAdrNames[i];
        mapping currentAddrProps = deviceAddresses[currentAddrName];
        if (plcType != SOFT_FE_DPTYPE) {
            if (currentAddrProps["dataType"] == CPC_BOOL) {
                if (currentAddrProps["bitPosition"] < 0 || currentAddrProps["bitPosition"] > 15) {
                    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + currentAddrName + " has wrong bit position", "");
                }
            } else {
                int addrPosition = cpcConfigGenericFunctions_getConstantDPEAdress(deviceType, currentAddrName);
                if (addrPosition < 1) {
                    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkDeviceAddress: " + deviceType + " has no position constant for " + currentAddrName, "");
                    return;
                }

                string addressValue = dsConfigs[UN_CONFIG_COMMON_LENGTH + addrPosition];
                cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, plcType, deviceType, addressValue, currentAddrProps, bHasArchive, exceptionInfo);
            }
            if (dynlen(exceptionInfo) > 0) { return; }
        }
        if (currentAddrProps["isAlarm"]) { // check alarm
            int normalPosition = cpcConfigGenericFunctions_getConstant(deviceType, "NORMAL_POSITION");
            if (normalPosition > 0) {
                unConfigGenericFunctions_checkNormalPosition(dsConfigs[UN_CONFIG_COMMON_LENGTH + normalPosition], exceptionInfo);
            }
            if (dynlen(exceptionInfo) > 0) { return; }
            if (currentAddrProps["hasAcknowledge"]) {
                int acknowledgeAlarmPos = cpcConfigGenericFunctions_getConstant(deviceType, "ACK_ALARM");
                if (acknowledgeAlarmPos > 0) {
                    bool val;
                    cpcConfigGenericFunctions_checkBool("ACK_ALARM", dsConfigs[UN_CONFIG_COMMON_LENGTH + acknowledgeAlarmPos], val, exceptionInfo);
                } else {
                    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no acknowledge type for " + currentAddrName, "");
                }
            }
            if (dynlen(exceptionInfo) > 0) { return; }
        }

        if (currentAddrProps["hasSmooth"]) {
            int defaultDeadbandPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive,  "DEADBAND");
            int defaultDeadbandTypePos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "DEADBAND_TYPE");
            if (defaultDeadbandPos > 0 && defaultDeadbandTypePos > 0) {
                unConfigGenericFunctions_checkDeadBand(dsConfigs[UN_CONFIG_COMMON_LENGTH + defaultDeadbandPos], dsConfigs[UN_CONFIG_COMMON_LENGTH + defaultDeadbandTypePos], exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no deadband for " + currentAddrName, "");
            }
        }
        if (currentAddrProps["hasPvRange"]) {
            int defaultRangeMaxPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "RANGEMAX");
            int defaultRangeMinPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "RANGEMIN");
            if (defaultRangeMaxPos > 0 && defaultRangeMinPos > 0) {
                unConfigGenericFunctions_checkRange(dsConfigs[UN_CONFIG_COMMON_LENGTH + defaultRangeMinPos], dsConfigs[UN_CONFIG_COMMON_LENGTH + defaultRangeMaxPos], exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no range for " + currentAddrName, "");
            }
        }
        if (currentAddrProps["hasUnit"]) {
            int defaultUnitPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "UNIT");
            if (defaultUnitPos > 0) {
                // @TODO: not checked ever
            } else {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no unit for " + currentAddrName, "");
            }
        }
        if (currentAddrProps["hasFormat"]) {
            int defaultFormatPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "FORMAT");
            if (defaultFormatPos > 0) {
                unConfigGenericFunctions_checkFormat(dsConfigs[UN_CONFIG_COMMON_LENGTH + defaultFormatPos], exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no format for " + currentAddrName, "");
            }
        }
        if (currentAddrProps["hasMapping"]) {
            int descPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "MAPPING");
            if (descPos > 0) {
                cpcConfigGenericFunctions_checkMapping(dsConfigs, dsConfigs[UN_CONFIG_COMMON_LENGTH + descPos], exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no description for " + currentAddrName, "");
            }
        }
        if (currentAddrProps["hasArchive"]) {
            if (strpos(currentAddrName, "evStsReg") == -1 && currentAddrProps["dataType"] != CPC_BOOL) {
                int archivePos    = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "ARCHIVE_ACTIVE");
                int timeFilterPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, currentAddrName, bHasArchive, "ARCHIVE_TIME_FILTER");
                if (archivePos > 0) {
                    string timeFilter = "";
                    if (timeFilterPos > 0) {
                        timeFilter = dsConfigs[UN_CONFIG_COMMON_LENGTH + timeFilterPos];
                        // @TODO: current checks are not full and covers only special case of time filter
                        unConfigGenericFunctions_checkArchiveParameters(timeFilter, dsConfigs[UN_CONFIG_COMMON_LENGTH + archivePos], exceptionInfo);
                    }
                } else {
                    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no archive for " + currentAddrName, "");
                }
            }
        }
        if (currentAddrProps["5RangesAlerts"]) {
            int acknowledgeAlarmPos = cpcConfigGenericFunctions_getConstant(deviceType, "ACK_ALARM");
            if (acknowledgeAlarmPos > 0) {
                bool acknowledgeAlarm;
                cpcConfigGenericFunctions_checkBool("ACK_ALARM", dsConfigs[UN_CONFIG_COMMON_LENGTH + acknowledgeAlarmPos], acknowledgeAlarm, exceptionInfo);
                if (dynlen(exceptionInfo) > 0) { return; }
            } else {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no acknowledge type for " + currentAddrName, "");
            }
            int llLimitPos = cpcConfigGenericFunctions_getConstant(deviceType, "LLLIMIT");
            int lLimitPos = cpcConfigGenericFunctions_getConstant(deviceType, "LLIMIT");
            int hLimitPos = cpcConfigGenericFunctions_getConstant(deviceType, "HLIMIT");
            int hhLimitPos = cpcConfigGenericFunctions_getConstant(deviceType, "HHLIMIT");
            int alarmActivePos = cpcConfigGenericFunctions_getConstant(deviceType, "ALARM_ACTIVE");
            if (alarmActivePos > 0 && llLimitPos > 0 && lLimitPos > 0 && hLimitPos > 0 && hhLimitPos > 0) {
                cpcConfigGenericFunctions_check5RangesAlert(dsConfigs, alarmActivePos,
                        makeDynInt(llLimitPos, lLimitPos, hLimitPos, hhLimitPos),
                        exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkImportConfig: " + deviceType + " has no alarm active or not all limits for " + currentAddrName, "");
            }
        }

        if (dynlen(exceptionInfo) > 0) { return; }
    }

    int parametersPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", bHasArchive, "PARAMETERS");
    if (parametersPos > 0) {
        cpcConfigGenericFunctions_checkParameters(deviceType, dsConfigs[UN_CONFIG_COMMON_LENGTH + parametersPos], exceptionInfo);
        if (dynlen(exceptionInfo) > 0) { return; }
        dyn_string props = strsplit(dsConfigs[UN_CONFIG_COMMON_LENGTH + parametersPos], ",");
        for (int i = 1; i <= dynlen(props); i++) {
            dyn_string pair = strsplit(props[i], "=");
            if (strpos(pair[1], "_addr") > 0) {
                string dpe = pair[1];
                strreplace(dpe, "_addr", "");
                cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, plcType, deviceType, pair[2], deviceAddresses[dpe], bHasArchive, exceptionInfo);
            }
        }
    }

    int smsCategoriesPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", bHasArchive, "SMS_CATEGORIES");
    if (smsCategoriesPos > 0) {
        dyn_string categories = strsplit(dsConfigs[UN_CONFIG_COMMON_LENGTH + smsCategoriesPos], ",");
        unProcessAlarm_checkCategory(categories, exceptionInfo);
    }
    if (dynlen(exceptionInfo) > 0) { return; }

    int maskEventPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", bHasArchive, "MASKEVENT");
    if (maskEventPos > 0) {
        bool maskEvent;
        cpcConfigGenericFunctions_checkBool("MASKEVENT", dsConfigs[UN_CONFIG_COMMON_LENGTH + maskEventPos], maskEvent, exceptionInfo);
    }
    if (dynlen(exceptionInfo) > 0) { return; }

    string customFunction = "CPC_";
    if (plcType == SOFT_FE_DPTYPE) {
        customFunction += "SOFT_FE_";
    }
    customFunction += cpcConfigGenericFunctions_getShortTypeName(deviceType) + "Config_checkCustomConfig";
    if (isFunctionDefined(customFunction)) {
        evalScript(exceptionInfo, "dyn_string main(dyn_string configLine, bool bHasArchive) {" +
                   "dyn_string exceptionInfo;" +
                   customFunction  + "(configLine, bHasArchive, exceptionInfo);" +
                   "return exceptionInfo; }", makeDynString("$sFrontEndDeviceType:" + $sFrontEndDeviceType), dsConfigs, bHasArchive);
        if (dynlen(exceptionInfo) > 0) { return; }
    }

}

/**Check if string represents a boolean value.

@param name name of the field, used in error message
@param source string to check
@param bRes boolean representation of the string
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_checkBool(string name, string source, bool& bRes, dyn_string &exceptionInfo) {
    source = strtolower(strrtrim(strltrim(source)));
    if (source == "true" || source == "1" || source == "y" || source == "yes") {
        bRes = true;
    } else if (source == "false" || source == "0" || source == "n" || source == "no") {
        bRes = false;
    } else {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkBool: the value '" + source + "' is not correct for the " + name, "");
    }
}

/**Check comma separated string for the correctness.

Verifies that each of the values is a pair of an identifier, assignment sign, and an integer as a value.
For example: "value1=23,value2=10"

@param dsConfigs importation line, processed in the unCore
@param sLabels string to test that contains comma separated values
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_checkMapping(dyn_string dsConfigs, string sLabels, dyn_string &exceptionInfo) {
    if (sLabels == "-") sLabels = ""; // backward compatibility after UCPC-1341
    // if the string is not empty, check that each element is: anInt=aString
    dyn_string dsLabels = strsplit(sLabels, ",");

    // Check that each of the sub string is "n=string"
    for (int i = 1; i <= dynlen(dsLabels); i++) {
        int iCorrectValue;
        dyn_string dsValue = strsplit(dsLabels[i], "=");
        if (dynlen(dsValue) != 2) {
            fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkMapping:" + getCatStr("unGeneration", "BADDATA"), "");
            return;
        } else {
            //dsValue first element should be the integer
            dyn_string exceptionInfoTmp;
            unConfigGenericFunctions_checkInt(strrtrim(strltrim(dsValue[1])), iCorrectValue, exceptionInfoTmp);
            if (dynlen(exceptionInfoTmp) > 0) {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkMapping:" + getCatStr("unGeneration", "BADDATA"), "");
                return;
            }
        }
    }
}


/**Check if the configs lenght is correct. Also reports if config has archiving in it.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType DPT name
@param dsConfigs importation line, processed in the unCore
@param bHasArchive set to true if passed config importation line contains bool/analog/event archives, otherwise set to false
@return result of checking, true if config has a proper length, otherwise it's false.
*/
bool cpcConfigGenericFunctions_checkConfigLength(string deviceType, dyn_string dsConfigs, bool &bHasArchive)
{
    int deviceLength = UN_CONFIG_COMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPLength(deviceType) + cpcConfigGenericFunctions_getConstantDPAdditionalLength(deviceType);
    int configLength = dynlen(dsConfigs);

    if (configLength == deviceLength) {
        bHasArchive = false;
        return true;
    } else if (configLength == (deviceLength + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH)) {
        bHasArchive = true;
        return true;
    }
    return false;
}

/**Checks archive names for correctness.

Archives should be defined in passing importation line.
Proxy validation to unConfigGenericFunctions_checkArchive.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType DPT name
@param dsConfigs importation line, processed in the unCore
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_checkArchiveConfig(string deviceType, dyn_string& dsConfigs, dyn_string& exceptionInfo) {
    int commonLength = UN_CONFIG_COMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPLength(deviceType);
    if (dynlen(dsConfigs) == commonLength + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH + cpcConfigGenericFunctions_getConstantDPAdditionalLength(deviceType)) {
        unConfigGenericFunctions_checkArchive(dsConfigs[commonLength + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL],
                                              dsConfigs[commonLength + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG],
                                              dsConfigs[commonLength + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT],
                                              exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkArchiveConfig:" + getCatStr("unGeneration", "BADLENGTH"), "");
    }
}

/**Check device address

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param plcType current plc type
@param deviceType device type
@param addressValue address reference
@param addrProps checked address properties
@param bHasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info

@return nothing
*/
void cpcConfigGenericFunctions_checkDeviceAddress(dyn_string dsConfigs, string sFrontEndType, string sDeviceType, string sAddressValue, mapping mAddressProperties, bool bHasArchive, dyn_string &exceptionInfo)
{
  float fParsedValue;
  string sOriginalAddressValue;
  dyn_string dsSplitAddress, exceptionInfoTmp;


  if( sAddressValue == "" )
  {
    return;
  }

  sOriginalAddressValue = sAddressValue;

  if( strpos(sAddressValue, PARAM_DPE_VALUE) == 0 )
  {
    strreplace(sAddressValue, PARAM_DPE_VALUE, "");
    unConfigGenericFunctions_checkFloat(sAddressValue, fParsedValue, exceptionInfoTmp);
    if( dynlen(exceptionInfoTmp) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkDeviceAddress: bad format for set-value-instead-of-address: " + sOriginalAddressValue, "");
    }
  }
  else
  {
    switch( sFrontEndType )
    {
      case UN_PLC_DPTYPE:
        // TODO: provide unConfigGenericFunctions_checkAddressMODBUS ?
        dsSplitAddress = strsplit(sAddressValue, ".");
        if( dynlen(dsSplitAddress) > 2 )
        {
          fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkDeviceAddress: bad format for UN_PLC_DPTYPE (MODBUS): " + sAddressValue, "");
        }
        unConfigGenericFunctions_checkAddresses(dsSplitAddress, exceptionInfo);
        break;


      case S7_PLC_DPTYPE:
        unConfigGenericFunctions_checkAddressS7(makeDynString(sAddressValue),
                                                cpcConfigGenericFunctions_translateDataTypeForS7Check(mAddressProperties["dataType"], sAddressValue),
                                                exceptionInfo);
        break;


      case CPC_CMW_DPTYPE:
        cpcConfigGenericFunctions_checkAddressCMW(makeDynString(sAddressValue), exceptionInfo);
        break;


      case CPC_OPCUA_DPTYPE:
        cpcConfigGenericFunctions_checkAddressOPCUA(makeDynString(sAddressValue), exceptionInfo);
        break;


      case CPC_BACNET_DPTYPE:
        // TO DO
        break;


      case CPC_DIP_DPTYPE:
        // TO DO
        break;


      case CPC_SNMP_DPTYPE:
        cpcConfigGenericFunctions_checkAddressSNMP(sAddressValue, exceptionInfo);
        break;


      case CPC_IEC104_DPTYPE:
        cpcConfigGenericFunctions_checkAddressIEC(sAddressValue, exceptionInfo);
        break;


      default:
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkDeviceAddress: unknown front end " + sFrontEndType, "");
        break;
    }
  }

}//  cpcConfigGenericFunctions_checkDeviceAddress()





/**Check if CMW address is correctly formered.
It's allows to have an emtpy string for address.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param address address string
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_checkAddressCMW(string address, dyn_string &exceptionInfo) {
    dyn_string dsAddressCMW;
    fwGeneral_stringToDynString(address, dsAddressCMW, CPC_CMW_ADDRESS_CONFIG_SEPARATOR);
    bool addressIsCorrect = dynlen(dsAddressCMW) == 7;
    if (!addressIsCorrect) {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkAddressCMW: address '" + address + "' does not follow format TYPE|TRANSFORMATION_TYPE|POLL_GROUP|ADDRESS_CONFIG|FILTER_NAME|FILTER_VALUE|LLC", "");
        return;
    }
    string sType = dsAddressCMW[1];
    string sPollGroup = dsAddressCMW[3];
    unConfigCMW_checkType(sType, sPollGroup, exceptionInfo);

    string sAddressConfig = dsAddressCMW[4];
    dyn_string addressParts = strsplit(sAddressConfig, "$");
    if (dynlen(addressParts) != 3) {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkAddressCMW: wrong CMW address  '" + sAddressConfig + "'", "");
    }
}

/**Check 5 ranges _alert_hdl data.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param alarmActivePos position of alarm active field in the importation line; device's importation constant should be used.
@param diLimits positions of 4 limits - LL,L,H,HH - in the importation line; device's importation constant should be used.
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_check5RangesAlert(dyn_string dsConfigs, int alarmActivePos, dyn_int diLimits, dyn_string &exceptionInfo) {
    float fLL, fL, fH, fHH;
    int iLevel;
    bool alarmActive, bOkRange, bError, bSMS;

    cpcConfigGenericFunctions_checkBool("ALARM_ACTIVE", dsConfigs[UN_CONFIG_COMMON_LENGTH + alarmActivePos], alarmActive, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        return;
    }
    if (dynlen(diLimits) != 4) {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_check5RangesAlert: wrong amount of alert limits: " + dynlen(diLimits) + " instead of 4.", "");
        return;
    }
    float prev;
    bool prevInit = false;
    for (int i = 1; i <= 4; i++) {
        if (dsConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[i]] != "") {
            float current;
            dyn_string exceptionInfoTemp;
            string limit = dsConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[i]];
            unConfigGenericFunctions_checkFloat(limit, current, exceptionInfoTemp);
            if (dynlen(exceptionInfoTemp) > 0) {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_check5RangesAlert: wrong limit #" + (5 - i) + ": " + limit, "");
                return;
            }
            if (prevInit && prev >= current) {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_check5RangesAlert: limits are not consistent ([limit#" + i + "] " + prev + " >= " + current + " [limit#" + (i - 1) + "])", "");
                return;
            }
            prev = current;
            prevInit = true;
        }
    }
}

/**Check device parameters field.

Parameters is the special field of importation line that should be presented as a KEY=VALUE comma separated list.
Parameters should contains all and the only keys that defined in device' getParamNames function.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType importation line, processed in the unCore
@param parameters parameters value
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_checkParameters(string deviceType, string parameters, dyn_string &exceptionInfo) {
    dyn_string possibleKeys;
    dyn_string props = strsplit(parameters, ",");
    for (int i = 1; i <= dynlen(props); i++) {
        dyn_string pair = strsplit(props[i], "=");
        if (dynlen(pair) == 1) dynAppend(pair, "");
        if (dynlen(pair) != 2) {
            fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkParameters:" + getCatStr("unGeneration", "BADDATA"), "");
            return;
        } else {
            dynAppend(possibleKeys, pair[1]);
        }
    }

    int amount = dynlen(possibleKeys);
    if (dynUnique(possibleKeys) != amount) {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkParameters: duplicate in parameters", "");
        return;
    }
    dyn_string devicesKeys = cpcConfigGenericFunctions_getDeviceParamNames(deviceType);
    for (int i = 1; i <= dynlen(devicesKeys); i++) {
        string key = devicesKeys[i];
        if (dynContains(possibleKeys, key) < 1) {
            fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkParameters: device should contain '" + key + "' parameter.", "");
            return;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------
/**Generic function that set device config.
Set up device's dpes using getConfig(), getParamNames() functions and importation constants defined in the device config script.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setDeviceConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string deviceType = dsConfigs[UN_CONFIG_OBJECTGENERAL_OBJECT];
  string sDpName    = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];

  int deviceLength = UN_CONFIG_ALLCOMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPLength(deviceType) + cpcConfigGenericFunctions_getConstantDPAdditionalLength(deviceType);
  bool bHasArchive = dynlen(dsConfigs) == deviceLength + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH;
  string plcType   = cpcConfigGenericFunctions_getFrontEndType(dsConfigs);

  cpcConfigGenericFunctions_processArchives(UN_CONFIG_ALLCOMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPLength(deviceType), bHasArchive, dsConfigs);

  mapping deviceAddresses   = cpcConfigGenericFunctions_getDPEConfig(deviceType);
  dyn_string deviceAdrNames = mappingKeys(deviceAddresses);

  int parametersPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", bHasArchive, "PARAMETERS");
  if( parametersPos > 0 )
  {
    cpcConfigGenericFunctions_setParameters(deviceType, dsConfigs, dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + parametersPos], exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      return;
    }
  }


  for( int i = 1 ; i <= dynlen(deviceAdrNames) ; i++ )
  {
    string currentAddrName   = deviceAdrNames[i];
    mapping currentAddrProps = deviceAddresses[currentAddrName];

    if( plcType != SOFT_FE_DPTYPE )
    {
      string addr = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], currentAddrName+"_addr", exceptionInfo);
      if( addr != "" )
      {
        cpcConfigGenericFunctions_setAddress(dsConfigs, currentAddrName, currentAddrProps, addr, plcType, exceptionInfo);
        //cpcGenericDpFunctions_setDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], currentAddrName+"_addr", "", exceptionInfo);
      }
      else
      {
        if( currentAddrProps["dataType"] == CPC_BOOL )
        {
          string addrPosition = cpcConfigGenericFunctions_getConstantDPEAdress(deviceType, currentAddrProps["dpe"]);
          cpcConfigGenericFunctions_setAlert(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, currentAddrProps["dpe"], exceptionInfo);
        }
        else
        {
          int addrPosition = cpcConfigGenericFunctions_getConstantDPEAdress(deviceType, currentAddrName);
          string addr = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + addrPosition];
          cpcConfigGenericFunctions_setAddress(dsConfigs, currentAddrName, currentAddrProps, dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + addrPosition], plcType, exceptionInfo);
        }
      }
    }


    if( currentAddrProps["isAlarm"] && !currentAddrProps["stubAlarm"] )
    {
      cpcConfigGenericFunctions_setAlertDetails(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, currentAddrProps["dpe"], exceptionInfo);
    }


    if( currentAddrProps["hasUnit"] )
    {
      cpcConfigGenericFunctions_setUnit(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, bHasArchive, exceptionInfo);
    }


    if( currentAddrProps["hasFormat"] )
    {
      cpcConfigGenericFunctions_setFormat(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, bHasArchive, exceptionInfo);
    }


    if( currentAddrProps["hasSmooth"] )
    {
      cpcConfigGenericFunctions_setSmoothing(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, bHasArchive, exceptionInfo);
    }


    if( currentAddrProps["hasPvRange"] )
    {
      cpcConfigGenericFunctions_setRange(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, bHasArchive, exceptionInfo);
    }


    if( currentAddrProps["hasMapping"] )
    {
      cpcConfigGenericFunctions_setMapping(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, bHasArchive, exceptionInfo);
    }


    if( currentAddrProps["hasArchive"] )
    {
      if( strpos(currentAddrName, "evStsReg") == 0 )
      {
        cpcConfigGenericFunctions_setEvArchive(dsConfigs, currentAddrProps["address"], exceptionInfo);
      }
      else
      {
        cpcConfigGenericFunctions_setArchive(dsConfigs, deviceType, currentAddrName, currentAddrProps, plcType, bHasArchive, exceptionInfo);
      }
    }


    if( currentAddrProps["5RangesAlerts"] )
    {
      bool acknowledgeAlarm, alarmActive;

      int acknowledgeAlarmPos = cpcConfigGenericFunctions_getConstant(deviceType, "ACK_ALARM");
      cpcConfigGenericFunctions_checkBool("ACK_ALARM", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + acknowledgeAlarmPos], acknowledgeAlarm, exceptionInfo);

      int alarmActivePos = cpcConfigGenericFunctions_getConstant(deviceType, "ALARM_ACTIVE");
      cpcConfigGenericFunctions_checkBool("ALARM_ACTIVE", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + alarmActivePos], alarmActive, exceptionInfo);

      int smsCategoriesPos = cpcConfigGenericFunctions_getConstant(deviceType, "SMS_CATEGORIES");
      bool bSMS = true;

      int normalPosition = cpcConfigGenericFunctions_getConstant(deviceType, "NORMAL_POSITION");
      int llLimitPos     = cpcConfigGenericFunctions_getConstant(deviceType, "LLLIMIT");
      int lLimitPos      = cpcConfigGenericFunctions_getConstant(deviceType, "LLIMIT");
      int hLimitPos      = cpcConfigGenericFunctions_getConstant(deviceType, "HLIMIT");
      int hhLimitPos     = cpcConfigGenericFunctions_getConstant(deviceType, "HHLIMIT");

      cpcConfigGenericFunctions_set5RangesAlert(dsConfigs,
                                                dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + currentAddrProps["address"],
                                                "Position Status",
                                                acknowledgeAlarm,
                                                alarmActive,
                                                bSMS,
                                                makeDynInt(llLimitPos, lLimitPos, hLimitPos, hhLimitPos),
                                                exceptionInfo);
    }


    string sDeviceAlias, sDeviceLinks;
    unConfigGenericFunctions_getAliasLink(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS], sDeviceAlias, sDeviceLinks);

    cpcConfigGenericFunctions_setDPEAlias(sDeviceAlias, dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + currentAddrProps["address"], exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      DebugTN("cpcConfigGenericFunctions_setDeviceConfig() -> Error due to: " + exceptionInfo[2]);
      return;
    }
  }


  cpcConfigGenericFunctions_setMetainfo(deviceType, dsConfigs, bHasArchive, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }


  int smsCategoriesPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", bHasArchive, "SMS_CATEGORIES");
  if( smsCategoriesPos > 0 )
  {
    dyn_string categories = strsplit(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + smsCategoriesPos], ",");
    for( int i = 1 ; i <= dynlen(deviceAdrNames) ; i++ )
    {
      string currentAddrName   = deviceAdrNames[i];
      mapping currentAddrProps = deviceAddresses[currentAddrName];

      if( (currentAddrProps["isAlarm"] && !currentAddrProps["stubAlarm"]) || currentAddrProps["5RangesAlerts"] )
      {
        unProcessAlarm_removeDpFromList(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + currentAddrProps["address"], exceptionInfo);
        if( dynlen(categories) > 0 )
        {
          unProcessAlarm_createCategory(categories, "", makeDynString(), exceptionInfo);
          for( int i = 1 ; i <= dynlen(categories) ; i++ )
          {
            bool hasSMS = cpcConfigGenericFunctions_alertWithMail(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], currentAddrName, categories[i]);
            if( hasSMS )
            {
              unProcessAlarm_addDpToList(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + currentAddrProps["address"], categories[i], exceptionInfo);
            }
          }
        }
      }
    }
  }


  int smsMessagesPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", bHasArchive, "SMS_MESSAGE");
  if( smsMessagesPos > 0 )
  {
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_SMS_MESSAGE, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + smsMessagesPos]), exceptionInfo);
  }


  int maskEventPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", bHasArchive, "MASKEVENT");
  if( maskEventPos > 0 )
  {
    bool maskEvent;
    cpcConfigGenericFunctions_checkBool("MASKEVENT", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + maskEventPos], maskEvent, exceptionInfo);
    cpcConfigGenericFunctions_setMaskEvent(deviceType, dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], maskEvent, exceptionInfo);
  }


  string customFunction = "CPC_";
  if( plcType == SOFT_FE_DPTYPE )
  {
    customFunction += "SOFT_FE_";
  }

  customFunction += cpcConfigGenericFunctions_getShortTypeName(deviceType) + "Config_setCustomConfig";
  if( isFunctionDefined(customFunction) )
  {
    // DebugTN("cpcConfigGenericFunctions_setDeviceConfig() -> customFunction = " + customFunction);
    evalScript(exceptionInfo, "dyn_string main(dyn_string configLine, bool bHasArchive)"           +
                              "{"                                                                  +
                              "  dyn_string exceptionInfo;"                                        +
                              "  " + customFunction  + "(configLine, bHasArchive, exceptionInfo);" +
                              "  return exceptionInfo;"                                            +
                              "}",
                              makeDynString(),
                              dsConfigs,
                              bHasArchive);
  }

}




/**Set DPE alias to %alias%.%dpe_name% (e.g. "object1.PosSt")

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name (alias)
@param dpe full dpe name (e.g. dist_1:un-PLCATLTRT-ATLTRT-CPC_AnalogParameter-00001.ProcessInput.PosSt)
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setDPEAlias(string deviceName, string dpe, dyn_string &exceptionInfo)
{
  dyn_string parts = strsplit(dpe, ".");
  string dpeName   = parts[dynlen(parts)];

  int iRes = dpSetAlias(dpe, deviceName + "." + dpeName);
  if( iRes != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setDPEAlias: can't set alias for " + dpe, "");
  }
}

/**Set description for the given element (dpe)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param regName dpe name
@param regProperties dpe properties
@param plcType current plc type
@param bHasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setMapping(dyn_string dsConfigs, string deviceType, string regName, mapping regProperties, string plcType, bool bHasArchive, dyn_string &exceptionInfo) {
    int descPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "MAPPING");

    string desc = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + descPos];
    if (desc == "-") desc = ""; // backward compatibility after UCPC-1341

    string dpeName = regProperties["address"];
    strreplace(dpeName, ".ProcessInput.", "");
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_MAPPING + dpeName, makeDynString(desc), exceptionInfo);
}

/**Set unit for the given element (dpe)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param regName dpe name
@param regProperties dpe properties
@param plcType current plc type
@param bHasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setUnit(dyn_string dsConfigs, string deviceType, string regName, mapping regProperties, string plcType, bool bHasArchive, dyn_string &exceptionInfo) {
    string unit;

    int unitPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "UNIT");
    unit = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + unitPos];

    unConfigGenericFunctions_setUnit(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"], unit, exceptionInfo);
}

/**Set format for the given element (dpe)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param regName dpe name
@param regProperties dpe properties
@param plcType current plc type
@param bHasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setFormat(dyn_string dsConfigs, string deviceType, string regName, mapping regProperties, string plcType, bool bHasArchive, dyn_string &exceptionInfo) {
    string format;

    int formatPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "FORMAT");
    format = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + formatPos];

    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"], format, exceptionInfo);
}

/**Init range from importation line

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param regName dpe name
@param rangeMax max range to set
@param rangeMin min range to set
@param bHasArchive does importation line contain bool/analog/event archives
*/
void cpcConfigGenericFunctions_getRange(dyn_string dsConfigs, string deviceType, string regName, string &rangeMax, string &rangeMin, bool bHasArchive) {
    int rangeMaxPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "RANGEMAX");
    int rangeMinPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "RANGEMIN");
    if (rangeMaxPos > 0 && rangeMinPos > 0) {
        rangeMin = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + rangeMinPos];
        rangeMax = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + rangeMaxPos];
    } else {
        rangeMin = "";
        rangeMax = "";
    }
}

/**Set range for the given element (dpe)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param regName dpe name
@param regProperties dpe properties
@param plcType current plc type
@param bHasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setRange(dyn_string dsConfigs, string deviceType, string regName, mapping regProperties, string plcType, bool bHasArchive, dyn_string &exceptionInfo) {
    float fMin, fMax;
    string sRangeMin, sRangeMax;
    cpcConfigGenericFunctions_getRange(dsConfigs, deviceType, regName, sRangeMax, sRangeMin, bHasArchive);
    if (sRangeMin != "" && sRangeMax != "") {
        //Format values
        fMin = sRangeMin;
        fMax = sRangeMax;

        if (fMin != fMax) { // if min!= max --> create pv_range config
            unConfigGenericFunctions_ScientificFormat(sRangeMin);
            unConfigGenericFunctions_ScientificFormat(sRangeMax);

            fwPvRange_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"],
                          sRangeMin,
                          sRangeMax,
                          UN_CONFIG_DEFAULT_RANGE_NEGATE_RANGE,
                          UN_CONFIG_DEFAULT_RANGE_IGNORE_OUTSIDE,
                          UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MIN,
                          UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MAX,
                          exceptionInfo);
        } else { // if min == max --> delete pv_range
            fwPvRange_delete(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"], exceptionInfo);
        }
    } else {
        fwPvRange_delete(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"], exceptionInfo);
    }
}

/**Set parameters for the given element (dpe)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType device type
@param dsConfigs importation line, processed in the unCore
@param parameters parameters value
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setParameters(string deviceType, dyn_string dsConfigs, string parameters, dyn_string &exceptionInfo)
{
  dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + UN_DEVICE_CONFIGURATION, makeDynString());

  dyn_string props = strsplit(parameters, ",");
  for( int i = 1 ; i <= dynlen(props) ; i++ )
  {
    dyn_string prop = strsplit(props[i], "=");
    if( dynlen(prop) == 1 )
    {
      dynAppend(prop, "");
    }

    cpcGenericDpFunctions_setDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], prop[1], prop[2], exceptionInfo);

  }

}

/**Set meta info for the given element (dpe)

Meta info is device's heirarchy (i.e. master/parents/children), type and second alias

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType device type
@param dsConfigs importation line, processed in the unCore
@param hasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setMetainfo(string deviceType, dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo)
{
  int masterNamePos  = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", hasArchive, "MASTER_NAME");
  int parentsPos     = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", hasArchive, "PARENTS");
  int childrenPos    = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", hasArchive, "CHILDREN");
  int typePos        = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", hasArchive, "TYPE");
  int secondAliasPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, "", hasArchive, "SECOND_ALIAS");

  if( masterNamePos > 0 )
  {
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_MASTER_NAME_KEY, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + masterNamePos]), exceptionInfo);
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_PARENTS_KEY, splitCommaSeparatedString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + parentsPos]), exceptionInfo);
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_CHILDREN_KEY, splitCommaSeparatedString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + childrenPos]), exceptionInfo);
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_TYPE_KEY, splitCommaSeparatedString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + typePos]), exceptionInfo);
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_SECOND_ALIAS_KEY, splitCommaSeparatedString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + secondAliasPos]), exceptionInfo);
  }
}

/**Set deadband for the given element (dpe)

Deadband could be absolute, relative, old/new comparison and none.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param regName dpe name
@param regProperties dpe properties
@param plcType current plc type
@param bHasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setSmoothing(dyn_string dsConfigs, string deviceType, string regName, mapping regProperties, string plcType, bool bHasArchive, dyn_string &exceptionInfo) {
    float deadband;
    int deadbandType;

    int deadbandPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "DEADBAND");
    int deadbandTypePos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "DEADBAND_TYPE");

    if (deadbandPos > 0 && deadbandTypePos > 0) {
        deadband = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + deadbandPos];
        deadbandType = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + deadbandTypePos];

        switch (deadbandType) {
            case UN_CONFIG_DEADBAND_VALUE:
                deadbandType = deadband > 0 ? DPATTR_VALUE_SMOOTH : -1;
                break;
            case UN_CONFIG_DEADBAND_RELATIF:
                deadbandType = deadband > 0 ? DPATTR_VALUE_REL_SMOOTH : -1;
                break;
            case UN_CONFIG_DEADBAND_OLD_NEW:
                deadbandType = DPATTR_COMPARE_OLD_NEW;
                deadband = - 1;
                break;
            default:
                deadbandType = -1;
        }

        if (deadbandType == DPATTR_VALUE_SMOOTH || deadbandType == DPATTR_VALUE_REL_SMOOTH || deadbandType == DPATTR_COMPARE_OLD_NEW) {
            fwSmoothing_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"], deadbandType, deadband, UN_CONFIG_DEFAULT_SMOOTHING_TIME_FILTER, exceptionInfo);
        } else {
            fwSmoothing_delete(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"], exceptionInfo);
        }
    }
}

/**Set archiving for the given element (dpe)

For analog dpes archive parameters (including smoothing) are coded in ARCHIVE_ACTIVE and ARCHIVE_TIME_FILTER (optional) fields.
For boolean dpes it's always old/new smooth, ARCHIVE_ACTIVE is treated only as "yes" or "no".

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param regName dpe name
@param regProperties dpe properties
@param plcType current plc type
@param bHasArchive does importation line contain bool/analog/event archives
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setArchive(dyn_string dsConfigs, string deviceType, string regName, mapping regProperties, string plcType, bool bHasArchive, dyn_string &exceptionInfo) {
    bool bArchiveActive;
    float archiveTimeFilter = 0;
    int iArchiveType, iSmoothProcedure;
    float fReturnDeadBand;

    int archivePos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "ARCHIVE_ACTIVE");
    int timeFilterPos = cpcConfigGenericFunctions_getConstantDPEProperty(deviceType, regName, bHasArchive, "ARCHIVE_TIME_FILTER");
    if (archivePos > 0) {
        if (timeFilterPos > 0) {
            archiveTimeFilter = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + timeFilterPos];
        }
        unConfigGenericFunctions_getArchiving(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + archivePos], bArchiveActive, archiveTimeFilter, iArchiveType, iSmoothProcedure);
        unConfigGenericFunctions_getArchivingDeadband(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + archivePos], fReturnDeadBand);
        if (regProperties["dataType"] == CPC_BOOL) {
            iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
            iSmoothProcedure = DPATTR_COMPARE_OLD_NEW;
            fReturnDeadBand = -1;
            archiveTimeFilter = 0;
        }
    } else {
        bArchiveActive = false;
    }

    string archive = regProperties["dataType"] == CPC_BOOL ? dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL] : dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG];

    if (bArchiveActive) {
        fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"],
                      archive,
                      iArchiveType,
                      iSmoothProcedure,
                      fReturnDeadBand,
                      archiveTimeFilter,
                      exceptionInfo);
    } else {
        fwArchive_delete(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + regProperties["address"], exceptionInfo);
    }
}

/**Set archiving for the device event dpes

The event dpe's archiving is always configured with old/new smoothing.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param address dpe name
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setEvArchive(dyn_string dsConfigs, string address, dyn_string &exceptionInfo) {
    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + address,
                  dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  0,
                  0,
                  exceptionInfo);
}

/**Set mask state for the given device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType device type
@param deviceName device name
@param isMasked mask state to set
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setMaskEvent(string deviceType, string deviceName, bool isMasked, dyn_string &exceptionInfo)
{
  dyn_string dpes;


  mapping deviceAddresses = cpcConfigGenericFunctions_getDPEConfig(deviceType);
  dyn_string deviceAdrNames = mappingKeys(deviceAddresses);

  for( int i = 1 ; i <= dynlen(deviceAdrNames) ; i++ )
  {
    string currentAddrName = deviceAdrNames[i];

    if( deviceAddresses[currentAddrName]["isInput"] )
    {
      if( deviceAddresses[currentAddrName]["dataType"] == CPC_BOOL )
      {
        dynAppend(dpes, deviceAddresses[currentAddrName]["address"]);
      }
      else
      {
        if( strpos(currentAddrName, "evStsReg") == 0 )
        {
          dynAppend(dpes, deviceAddresses[currentAddrName]["address"]);
        }
      }
    }
  }

  unConfigGenericFunctions_setMaskEvent(deviceName, isMasked, dpes, exceptionInfo);

}



/**Set address for the given element (dpe)

Set up periphery address for S7 or UNICOS (Schneider)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param regName dpe name
@param regProperties dpe properties
@param regPosition adress position in the importation line
@param plcType current plc type
@param exceptionInfo exception info

@return nothing
*/
void cpcConfigGenericFunctions_setAddress(dyn_string dsConfigs, string sRegName, mapping mRegProperties, string sAddressValue, string sFrontEndType, dyn_string &exceptionInfo)
{
  bool bPollingFrontEnd;
  int iDataType;
  string sDpe, sBitNumber, sAddressReference, sAddressMode, sPollingDp, sAddressType;


  sDpe       = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + mRegProperties["address"];
  sBitNumber = "0";


  if( sAddressValue == "" )
  {
    fwPeriphAddress_delete(sDpe, exceptionInfo);
    return;
  }


  iDataType        = cpcConfigGenericFunctions_translateDataType(sFrontEndType, mRegProperties["dataType"]);
  bPollingFrontEnd = unConfigGenericFunctions_isPollingOnly(dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE]);


  if( strpos(sAddressValue, PARAM_DPE_VALUE) == 0 )
  {
    fwPeriphAddress_delete(sDpe, exceptionInfo);
    strreplace(sAddressValue, PARAM_DPE_VALUE, "");
    dpSetWait(sDpe, sAddressValue);
  }
  else
  {
    switch( sFrontEndType )
    {
      case UN_PLC_DPTYPE:
      case S7_PLC_DPTYPE:
        if( mRegProperties["isInput"] )
        {
          if( bPollingFrontEnd == TRUE )
          {
            sAddressMode = DPATTR_ADDR_MODE_INPUT_POLL;
          }
          else
          {
            sAddressMode = DPATTR_ADDR_MODE_INPUT_SPONT;
          }
        }
        else
        {
          sAddressMode = DPATTR_ADDR_MODE_OUTPUT;
        }


        if( sFrontEndType == UN_PLC_DPTYPE )
        {
          // For MODBUS detect bit number
          if( dynlen(strsplit(sAddressValue, ".")) > 1 )
          {
            sBitNumber = strsplit(sAddressValue, ".")[2];
          }
          else
          {
            sBitNumber = "0";
          }


          unConfigGenericFunctions_getUnicosAddressReference(makeDynString(sAddressMode,
                                                                           dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE],
                                                                           dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER],
                                                                           strpos(sRegName, "evStsReg") == 0,  // @TODO: is event. how to check more correct?
                                                                           sAddressValue,
                                                                           dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]),
                                                             sAddressReference);
        }
        else
          if( sFrontEndType == S7_PLC_DPTYPE )
          {
            unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(sAddressMode,
                                                                               dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                                                                               sAddressValue),
                                                                 sAddressReference);
          }


        if( sAddressReference == "" )
        {
          fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setAddress: can't create addressReference", "");
          return;
        }


        sPollingDp = "";
        if( bPollingFrontEnd )
        {
          if( sFrontEndType == UN_PLC_DPTYPE )
          {
            sPollingDp = UN_FE_POLL_DP_PREFIX + dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME];
          }
          else
            if( sFrontEndType == S7_PLC_DPTYPE )
            {
              sPollingDp = "_poll_" + c_unSystemIntegrity_S7 + dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME];
            }
        }


        if( sFrontEndType == UN_PLC_DPTYPE )
        {
          sAddressType = fwPeriphAddress_TYPE_MODBUS;
        }
        else
        {
          sAddressType = fwPeriphAddress_TYPE_S7;
        }

        fwPeriphAddress_set(sDpe,
                            makeDynString(sAddressType,
                                          dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER],
                                          sAddressReference,
                                          sAddressMode,
                                          iDataType,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          mRegProperties["isInput"] ? UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_INPUT : UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_OUTPUT,
                                          sBitNumber, // before was hardcoded 0 - for S7 OK, but not MODBUS
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          sPollingDp),
                            exceptionInfo,
                            FALSE,
                            FALSE);
        break;


      case CPC_CMW_DPTYPE:
        cpcConfigGenericFunctions_setCMWAddress(sDpe, sAddressValue, dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER], exceptionInfo);
        break;


      case CPC_OPCUA_DPTYPE:
        cpcConfigGenericFunctions_setAddressOPCUA(sDpe, sAddressValue, iDataType, dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER], exceptionInfo);
        break;


      case CPC_BACNET_DPTYPE:
        cpcConfigGenericFunctions_setBACnetAddress(sDpe, dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME], sAddressValue, iDataType, dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER], exceptionInfo);
        break;


      case CPC_DIP_DPTYPE:
        cpcConfigGenericFunctions_setDIPAddress(sDpe, dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME], sAddressValue, exceptionInfo);
        break;


      case CPC_SNMP_DPTYPE:
        cpcConfigGenericFunctions_setSNMPAddress(sDpe, dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME], sAddressValue, dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER], exceptionInfo);
        break;


      case CPC_IEC104_DPTYPE:
        cpcConfigGenericFunctions_setIEC104Address(sDpe, dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME], sAddressValue, dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER], exceptionInfo);
        break;


      default:
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setAddress: unknown front end " + sFrontEndType, "");
        break;
    }
  }

}//  cpcConfigGenericFunctions_setAddress()





/**Set up CMW address config

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpeName dpe name
@param addressReference address configuration in the importation line
@param driverReference driver reference
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setCMWAddress(string dpeName, string addressReference, string driverReference, dyn_string &exceptionInfo) {
    dyn_string dsFullAddressCMW;
    fwGeneral_stringToDynString(addressReference, dsFullAddressCMW, CPC_CMW_ADDRESS_CONFIG_SEPARATOR);

    dyn_string dsAddressCMW = strsplit(dsFullAddressCMW[4], "$");
    unConfigCMW_setAddress(dpeName,
                           driverReference,
                           dsFullAddressCMW[1], // type
                           dsFullAddressCMW[2], // transformation type
                           dsFullAddressCMW[3], // poll group
                           dsFullAddressCMW[5], // filter name
                           dsFullAddressCMW[6], // filter value
                           dsAddressCMW[1],
                           dsAddressCMW[2],
                           dsAddressCMW[3],
                           dsFullAddressCMW[7], // LLC
                           exceptionInfo);
}





/**Check IEC104 address config

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sAddress, string, IEC104 address reference
@param exceptionInfo exception info

@return Nothing
*/
void cpcConfigGenericFunctions_checkAddressIEC(string sAddressValue, dyn_string &exceptionInfo)
{
  unConfigIEC104_checkAddress(sAddressValue, exceptionInfo);

}//  cpcConfigGenericFunctions_checkAddressIEC()





cpcConfigGenericFunctions_checkAddressOPCUA(string address, dyn_string &exceptionInfo) {
    unConfigOPCUA_checkAddress(address, exceptionInfo);
}

/**Check if SNMP address is correctly formered.
It's allows to have an emtpy string for address.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param addressReference address string
@param exceptionInfo exception info
*/
cpcConfigGenericFunctions_checkAddressSNMP(string addressReference, dyn_string &exceptionInfo) {

    dyn_string dsSplitAddress = strsplit(addressReference, "$");
    if(dynlen(dsSplitAddress) >= 5) {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkAddressSNMP: wrong address format: " + addressReference, "");
    } else {
        string sTransformation, sSubindex, sPollInterval;
        unConfigSNMP_splitAddress(addressReference, sTransformation, sSubindex, sPollInterval);
        unConfigSNMP_checkAddress(addressReference, exceptionInfo);

        if(sSubindex != ""){
            int iSubindex;
            if(sscanf(sSubindex, "%d", iSubindex) <= 0)
            {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkAddressSNMP: wrong subindex:" + sSubindex, "");
            } else if (iSubindex < 0){
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkAddressSNMP: wrong subindex value: " + iSubindex, "");
            }
        }

        if(unConfigSNMP_dataTypeStringToInt(sTransformation) == -1) {
            fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkAddressSNMP: wrong transformation type: " + sTransformation, "");
        }
    }
}

void cpcConfigGenericFunctions_setAddressOPCUA(string dpeName, string addressReference, int dataType, string driverReference, dyn_string &exceptionInfo) {
    int mode = strpos(dpeName, "ProcessInput") > 0 ? DPATTR_ADDR_MODE_INPUT_SPONT : DPATTR_ADDR_MODE_OUTPUT_SINGLE;
    unConfigOPCUA_setAddress(dpeName, addressReference, mode, driverReference, dataType, exceptionInfo);
}

void cpcConfigGenericFunctions_setBACnetAddress(string dpeName, string frontEndName, string addressReference, int dataType, string driverReference, dyn_string &exceptionInfo) {
    int mode = strpos(dpeName, "ProcessInput") > 0 ? DPATTR_ADDR_MODE_INPUT_POLL : DPATTR_ADDR_MODE_OUTPUT;
    unConfigBACnet_setAddress(dpeName, frontEndName, addressReference, mode, driverReference, dataType, exceptionInfo);
}

void cpcConfigGenericFunctions_setDIPAddress(string dpeName, string frontEndName, string addressReference, dyn_string &exceptionInfo) {
    if(strpos(dpeName, "ProcessInput") > 0) {
        unConfigDIP_addSubscription(dpeName, frontEndName, addressReference, exceptionInfo);
    }
    else  {
        unConfigDIP_addPublication(dpeName, frontEndName, addressReference, exceptionInfo, 0, true);
    }
}

void cpcConfigGenericFunctions_setSNMPAddress(string sDpeName, string sFrontEndName, string sAddressReference, string sDriverReference, dyn_string &exceptionInfo) {
    int mode = strpos(sDpeName, "ProcessInput") > 0 ? DPATTR_ADDR_MODE_INPUT_POLL : DPATTR_ADDR_MODE_OUTPUT_SINGLE;

    int iAgentNumber;
    string sAgentVersion;
    string sFEdpe = getSystemName() + c_unSystemIntegrity_UNSNMP + sFrontEndName;
    int iRes = dpGet(sFEdpe + ".configuration.agent_number", iAgentNumber,
          sFEdpe + ".configuration.agent_version", sAgentVersion);
    if(iRes !=0) {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkAddressSNMP: cannot retrieve values from " + sFEdpe + ".configuration.agent_number and/or .configuration.agent_version", "");
        return;
    }

    string sAddress = sAddressReference, sSubindex="", sTransformation="", sPollInterval="";
    unConfigSNMP_splitAddress(sAddress, sTransformation, sSubindex, sPollInterval);
    if(sSubindex=="") sSubindex = "0";
    int iDataType = unConfigSNMP_dataTypeStringToInt(sTransformation);
    string sPollGroup = unConfigSNMP_getPollGroupName(sFrontEndName, sPollInterval);
    unConfigSNMP_setAddress(sDpeName, sFrontEndName, sAddress, mode, sDriverReference, iDataType, sSubindex, iAgentNumber, sAgentVersion, sPollGroup, exceptionInfo);
}







void cpcConfigGenericFunctions_setIEC104Address(string sDpeName,
                                                string sFrontEndName,
                                                string sAddressReference,
                                                string sDriver,
                                                dyn_string &exceptionInfo)
{
  int iMode, iDataType, iLen;
  string sReference, sCot;
  dyn_string dsReference;


  // Parsing the address reference
  dsReference = strsplit(sAddressReference, "|");
  iLen        = dynlen(dsReference);
  if( dynlen(dsReference) != iIEC104_ADDRESS_LENGTH )
  {
    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setIEC104Address() -> Error extracting IEC fields from address reference, it should have " + iIEC104_ADDRESS_LENGTH + " fields and currently has: " + iLen + ", " + sAddressReference, "");
    return;
  }


  // Checking the DPE and the type of input / output
  if( strpos(sDpeName, "ProcessInput") > 0 )
  {
    iMode = DPATTR_ADDR_MODE_INPUT_SPONT;
  }
  else
  {
    // Get mode from config file
    if( (dsReference[iIEC104_ADDRESS_DIRECTION] != DPATTR_ADDR_MODE_OUTPUT)       ||
        (dsReference[iIEC104_ADDRESS_DIRECTION] != DPATTR_ADDR_MODE_OUTPUT_SINGLE)  )
    {
      iMode = DPATTR_ADDR_MODE_OUTPUT_SINGLE;
    }
  }


  // Checking the data type
  iDataType = _unConfigIEC104_convertDataToType(dsReference[iIEC104_ADDRESS_DATATYPE], dsReference[iIEC104_ADDRESS_COT], exceptionInfo);
  if( (iDataType == -1) ||
      (iDataType == 520)  )
  {
    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setIEC104Address() -> Error translating the IEC data type: " + dsReference[iIEC104_ADDRESS_DATATYPE] +" . Selected data type is invalid: " + exceptionInfo[2], "");
    return;
  }


  // Creating address reference
  if( dsReference[iIEC104_ADDRESS_COT] == 1 )
  {
    sCot = "C";
  }
  sReference = "IEC_" + sFrontEndName + "-" + sCot + dsReference[iIEC104_ADDRESS_DATATYPE] + "." + dsReference[iIEC104_ADDRESS_REFERENCE];


  unConfigIEC104_setAddress(sDpeName, sReference, iMode, sDriver , iDataType, dsReference[iIEC104_ADDRESS_SUBINDEX], exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setIEC104Address() -> Error setting the IEC address to DPE: " + sDpe + ", due to: " + exceptionInfo[2], "");
    return;
  }

}//  cpcConfigGenericFunctions_setIEC104Address()





/**Set alarm address for the given element (dpe)

Set up periphery address of bool dpe which is linked to StsReg for S7 or UNICOS (Schneider)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param alertName dpe name
@param alertProps dpe properties
@param plcType current plc type
@param regName name of StsReg dpe which contains the bit to link to
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setAlert(dyn_string dsConfigs, string sDeviceType, string sAlertName, mapping mAlertProperties, string sFrontEndType, string sRegisterName, dyn_string &exceptionInfo)
{
  bool bPollingFrontEnd;
  int iRegisterPosition;
  string sAddressReference, sAddressMode, sAddress;


  if( sFrontEndType == UN_PLC_DPTYPE ||
      sFrontEndType == S7_PLC_DPTYPE   )
  {
    bPollingFrontEnd = unConfigGenericFunctions_isPollingOnly(dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE]);
    if( bPollingFrontEnd )
    {
      sAddressMode = DPATTR_ADDR_MODE_INPUT_POLL;
    }
    else
    {
      sAddressMode = DPATTR_ADDR_MODE_INPUT_SPONT;
    }
  }


  switch( sFrontEndType )
  {
    case UN_PLC_DPTYPE:
      // Alerts for inputs only
      unConfigGenericFunctions_getUnicosAddressReference(makeDynString(sAddressMode,
                                                                       dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE],
                                                                       dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER],
                                                                       FALSE,  // @TODO: is it correct?
                                                                       dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPEAdress(sDeviceType, sRegisterName)],
                                                                       dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]),
                                                         sAddressReference);
      if( sAddressReference == "" )
      {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setAlert: can't create address reference for " + sAlertName, "");
        return;
      }

      unConfigGenericFunctions_setStsRegAlarmBitAddress(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + mAlertProperties["address"],
                                                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER],
                                                        sAddressReference,
                                                        mAlertProperties["bitPosition"],
                                                        exceptionInfo);
      break;


    case S7_PLC_DPTYPE:
      unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(sAddressMode,
                                                                         dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                                                                         dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPEAdress(sDeviceType, sRegisterName)]),
                                                           sAddressReference);
      if( sAddressReference == "" )
      {
        fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setAlert: can't create address reference for " + sAlertName, "");
        return;
      }

      unConfigGenericFunctions_setStsRegAlarmBitAddressS7(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + mAlertProperties["address"],
                                                          dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER],
                                                          sAddressReference,
                                                          mAlertProperties["bitPosition"],
                                                          exceptionInfo);
      break;


    case CPC_CMW_DPTYPE:
      iRegisterPosition = cpcConfigGenericFunctions_getConstantDPEAdress(sDeviceType, sAlertName);
      cpcConfigGenericFunctions_setCMWAddress(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + mAlertProperties["address"],
                                              dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + iRegisterPosition],
                                              dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER],
                                              exceptionInfo);
      break;


    case CPC_OPCUA_DPTYPE:
      iRegisterPosition = cpcConfigGenericFunctions_getConstantDPEAdress(sDeviceType, sAlertName);
      sAddress          = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPEAdress(sDeviceType, sRegisterName)];
      cpcConfigGenericFunctions_setAddressOPCUA(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + mAlertProperties["address"],
                                                sAddress,
                                                OPCUA_BOOL,
                                                dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER],
                                                exceptionInfo);
      break;


    case CPC_IEC104_DPTYPE:
      iRegisterPosition = cpcConfigGenericFunctions_getConstantDPEAdress(sDeviceType, sAlertName);
      sAddress          = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + cpcConfigGenericFunctions_getConstantDPEAdress(sDeviceType, sRegisterName)];
      cpcConfigGenericFunctions_setIEC104Address(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + mAlertProperties["address"],
                                                 dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                                                 sAddress,
                                                 dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER],
                                                 exceptionInfo);
      break;


    case CPC_BACNET_DPTYPE:
      // TODO
      break;


    case CPC_DIP_DPTYPE:
      // TODO
      break;


    case CPC_SNMP_DPTYPE:
      // TODO
      break;


    default:
      fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_setAlert: unknown front end " + sFrontEndType, "");
      break;
  }

}//  cpcConfigGenericFunctions_setAlert()





/**Set alarm config for the given element (dpe)

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param deviceType device type
@param alertName dpe name
@param alertProps dpe properties
@param plcType current plc type
@param regName name of StsReg dpe which contains the bit to link to
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setAlertDetails(dyn_string dsConfigs, string deviceType, string alertName, mapping alertProps, string plcType, string regName, dyn_string &exceptionInfo)
{
  string bitDescription, sDomain, sNature, sDeviceAlias, sDeviceLinks;
  dyn_string dsbitDescription, dsAlertTexts, dsAlertClasses;


  unGenericDpFunctions_getBitDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_OBJECT],
                                         regName,
                                         alertProps["bitPosition"],
                                         bitDescription);
  if( bitDescription == "" )
  {
    // Bit description was not found
    bitDescription = alertName;
  }

  cpcConfigGenericFunctions_createAlarmDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN],
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE],
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS],
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION],
                                                   bitDescription,
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL],
                                                   dsbitDescription);


  bool bOkRange, alarmExists, alarmActive, bSMS;
  int iLevel;

  // Set alarm
  int normalPosition = cpcConfigGenericFunctions_getConstant(deviceType, "NORMAL_POSITION");
  unConfigGenericFunctions_getNormalPosition(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + normalPosition], alarmExists, alarmActive, bOkRange, iLevel, bSMS);

  int descriptionPos = cpcConfigGenericFunctions_getConstant(deviceType, "ALARM_DESCRIPTION");

  string alertMessageOK, alertMessageBAD;
  if( descriptionPos < 1 )
  {
    string descriptionText;
    descriptionText = cpcConfigGenericFunctions_getConstantByName("UN_CONFIG_" + strtoupper(deviceType), strtoupper(alertName) + "_ALERT_TEXT", TRUE);

    if( descriptionText != CPC_UNDEFINED_CONSTANT_VALUE )
    {
      alertMessageOK  = descriptionText + CPC_CONFIG_ALARM_CUSTOM_OK_POSTFIX;
      alertMessageBAD = descriptionText;
    }
    else
    {
      alertMessageOK  = UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_OK;
      alertMessageBAD = UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_BAD;
    }
  }
  else
  {
    alertMessageOK  = "";
    alertMessageBAD = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + descriptionPos];
  }


  dsAlertTexts = bOkRange ? makeDynString(alertMessageBAD, alertMessageOK) : makeDynString(alertMessageOK, alertMessageBAD);


  string alertClassPostfix;
  alertClassPostfix = cpcConfigGenericFunctions_getConstantByName("UN_CONFIG_" + strtoupper(deviceType), strtoupper(alertName) + "_ALERT_CLASS_POSTFIX", TRUE);

  if( alertClassPostfix == CPC_UNDEFINED_CONSTANT_VALUE )
  {
    alertClassPostfix = "";
  }


  bool acknowledgeAlarm = false;
  if( alertName == "TStopISt"  ||
      alertName == "FuStopISt" ||
      alertName == "StartISt"  ||
      alertName == "AlSt"        )
  {
    acknowledgeAlarm = true;
    bSMS             = false; // UCPC-2095 avoid send message for interlocks
  }
  else
  {
    bSMS = true;
  }

  if( alertProps["hasAcknowledge"] )
  {
    int acknowledgeAlarmPos = cpcConfigGenericFunctions_getConstant(deviceType, "ACK_ALARM");
    cpcConfigGenericFunctions_checkBool("ACK_ALARM", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + acknowledgeAlarmPos], acknowledgeAlarm, exceptionInfo);
  }
  string alertClass = cpcConfigGenericFunctions_getAlertClass(acknowledgeAlarm, bSMS, alertClassPostfix);

  dsAlertClasses = bOkRange ? makeDynString(alertClass, "") : makeDynString("", alertClass);
  if( alarmExists )
  {
    cpcConfigGenericFunctions_setDigitalAlert(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + alertProps["address"],
                                              dsbitDescription,
                                              dsAlertTexts,
                                              dsAlertClasses,
                                              bOkRange,
                                              alarmActive,
                                              bSMS,
                                              exceptionInfo);
  }
  else
  {
    fwAlertConfig_deleteDigital(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + alertProps["address"], exceptionInfo);

    unConfigGenericFunctions_setAlarmDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + alertProps["address"],
                                                 dsbitDescription,
                                                 exceptionInfo);  // Set description for alert screen
  }

}



/** return true if alertName should be configured with mail handler
* returns true except there's device property %DPE%_NO_MAIL = true and %CATEGORY%_%DPE%_NO_MAIL = true
* alertName name of dpe with alert handler, for example "PosSt"
*/
bool cpcConfigGenericFunctions_alertWithMail(string deviceName, string alertName, string category="") {
    bool noSMSValue;
    dyn_string exceptionInfo;
    string noSMS_property, noSMS;

    if("" != category) { // check if for given category this alert should exist
        noSMS_property = category + "_" + alertName + NO_MAIL_POSTFIX;
        noSMS = cpcGenericDpFunctions_getDeviceProperty(deviceName, noSMS_property, exceptionInfo, "false");
        cpcConfigGenericFunctions_checkBool(noSMS_property, noSMS, noSMSValue, exceptionInfo);
        if(noSMSValue == true){
            return false;
        }
    }

    noSMS_property = alertName + NO_MAIL_POSTFIX;
    noSMS = cpcGenericDpFunctions_getDeviceProperty(deviceName, noSMS_property, exceptionInfo, "false");
    cpcConfigGenericFunctions_checkBool(noSMS_property, noSMS, noSMSValue, exceptionInfo);

    return !noSMSValue;
}

/**Save alerm info

Port of unConfigGenericFunctions_setDigitalAlert

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDpName datapoint name
@param dsdescription datapoint description
@param dsAlertTexts alerm messages (On and Off)
@param dsAlertClasses alert class, should exist in db
@param bOkRange deprecated
@param active is alarm active
@param bSMS deprecated
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_setDigitalAlert(string sDpName, dyn_string dsdescription, dyn_string dsAlertTexts, dyn_string dsAlertClasses, bool bOkRange, bool active, bool bSMS, dyn_string &exceptionInfo) {
    int i, length, alertType;
    dyn_float dfLimitsGood;
    int isAlarmActive = -1;

    unConfigGenericFunctions_setAlarmDescription(sDpName, dsdescription, exceptionInfo);	// Set description for alert screen
    dpGet(sDpName + ":_alert_hdl.._type", alertType);
    switch (alertType) {
        case DPCONFIG_NONE:
            fwAlertConfig_createDigital(sDpName,
                                        dsAlertTexts,
                                        dsAlertClasses,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS,
                                        UN_CONFIG_DEFAULT_ALERT_HELP,
                                        exceptionInfo);
            if (active) {
                dpSetWait(sDpName + ":_alert_hdl.._active", active);
            }
            break;

        case DPCONFIG_ALERT_BINARYSIGNAL:
            fwAlertConfig_modifyDigital(sDpName,
                                        dsAlertTexts,
                                        dsAlertClasses,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS,
                                        UN_CONFIG_DEFAULT_ALERT_HELP,
                                        exceptionInfo);
            dpGet(sDpName + ":_alert_hdl.._active", isAlarmActive);
            if ((isAlarmActive != active) || (isAlarmActive == -1)) {
                dpSetWait(sDpName + ":_alert_hdl.._active", active);
            }
            break;

        case DPCONFIG_ALERT_NONBINARYSIGNAL:
        default:
            fwException_raise(exceptionInfo, "ERROR:", "cpcConfigGenericFunctions_setDigitalAlert :" + getCatStr("unGeneration", "BADALERT"));
            break;
    }
}

/**Create

get alarm description

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param description description
@param dsDescription description in the format needed by unConfigGenericFunctions_setAlarmDescription
*/
cpcConfigGenericFunctions_createAlarmDescription(string sDomainConfig, string sNatureConfig, string sAlias, string sDescription, string sAlarmName, string sDefaultPanel, dyn_string &dsDescription)
{
  string sDomain;

  dyn_string ds = strsplit(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  if( dynlen(ds) == UN_CONFIG_UNICOS_DOMAIN_AND_ACCESS_CONTROL_LEN )
  {
    // access control + domain present
    sDomain = ds[2];
  }
  else
    if( strpos(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR) >= 0 )
    {
      // if UN_ACCESS_CONTROL_DOMAIN_SEPARATOR in sDomain config --> domain = ""
      sDomain = "";
    }
    else
    {
      sDomain = sDomainConfig;
    }

    string sNature;

    ds = strsplit(sNatureConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);

    // if no ACC priv assume list of nature
    if( dynlen(ds) >= (UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN - 1) )
    {
      if( dynlen(ds) == UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN )
      {
        sNature = ds[4];
      }
      else
      {
        // dynlen = UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN -1 --> consider nature = ""
        sNature = "";
      }
    }
    else
    {
      sNature = sNatureConfig;
    }


    string sDeviceAlias, sDeviceLinks;
    // alias is the deviceName only
    unConfigGenericFunctions_getAliasLink(sAlias, sDeviceAlias, sDeviceLinks);

    dsDescription [UN_CONFIG_ALARM_DOMAIN]       = sDomain;
    dsDescription [UN_CONFIG_ALARM_NATURE]       = sNature;
    dsDescription [UN_CONFIG_ALARM_ALIAS]        = sDeviceAlias;
    dsDescription [UN_CONFIG_ALARM_DESCRIPTION]  = sDescription;
    dsDescription [UN_CONFIG_ALARM_NAME]         = sAlarmName;
    dsDescription [UN_CONFIG_ALARM_DEFAULTPANEL] = sDefaultPanel;
}

/**Set 5 ranges _alert_hdl on given dpe

Port of unConfigGenericFunctions_set5RangesAlert

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param sDp dpe name
@param description description
@param acknowledgeAlarm is alarm acknowledgeable
@param alarmActive is alarm active
@param bSMS does alarm trigger mail/sms notifier
@param diLimits ubdex of 4 limits - LL,L,H,HH - in the importation line; device's importation constant should be used
@param exceptionInfo exception info
*/
void cpcConfigGenericFunctions_set5RangesAlert(dyn_string dsConfigs, string sDp, string description, bool acknowledgeAlarm, bool alarmActive, bool bSMS, dyn_int diLimits, dyn_string &exceptionInfo)
{
  bool alarmExists, bOkRange;
  dyn_string dsDescription;
  dyn_string dsAlarms, dsAlertClass;
  int iAlertType, iLevel;

  cpcConfigGenericFunctions_createAlarmDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN],
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE],
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS],
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION],
                                                   description,
                                                   dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL],
                                                   dsDescription);

  alarmExists = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[1]] != "" ||
                dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[2]] != "" ||
                dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[3]] != "" ||
                dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[4]] != "";

  if( alarmExists )
  {
    dyn_float dfLevelLimits = makeDynFloat();

    dpGet(sDp + ":_alert_hdl.._type", iAlertType);
    if( iAlertType != DPCONFIG_NONE )
    {
      fwAlertConfig_deleteAnalog(sDp, exceptionInfo);
    }

    cpcConfigGenericFunctions_get5AlertConfig(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[1]],
                                              dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[2]],
                                              dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[3]],
                                              dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[4]],
                                              bSMS,
                                              acknowledgeAlarm,
                                              dfLevelLimits,
                                              dsAlarms,
                                              dsAlertClass);

    unConfigGenericFunctions_setAnalogAlert(sDp, dsDescription, dfLevelLimits, dsAlarms, dsAlertClass, alarmActive, exceptionInfo);
  }
  else
  {
    fwAlertConfig_deleteAnalog(sDp, exceptionInfo);

    // even if the alarm is deleted set the description
    unConfigGenericFunctions_setAlarmDescription(sDp, dsDescription, exceptionInfo);
  }
}




/**Build config of 5-range alert

Port of unConfigGenericFunctions_set4LevelsAlert

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param LLparam LL threshold value
@param Lparam L threshold value
@param Hparam H threshold value
@param HHparam HH threshold value
@param bSMS does alarm trigger mail/sms notifier
@param acknowledgeAlarm is alarm acknowledgeable
@param dfLevelLimits threshold values array
@param dsLevelsAlarm threshold levels array
@param dsLevelsAlertsClass threshold classes array
*/
void cpcConfigGenericFunctions_get5AlertConfig(string LLparam, string Lparam, string Hparam, string HHparam, bool bSMS, bool acknowledgeAlarm, dyn_float &dfLevelLimits, dyn_string &dsLevelsAlarm, dyn_string &dsLevelsAlertsClass) {
    string sLLClass, sLClass, sHClass, sHHClass;

    sLLClass = "_cpcAnalogLL";
    sLClass  = "_cpcAnalogL";
    sHClass  = "_cpcAnalogH";
    sHHClass = "_cpcAnalogHH";
    if (acknowledgeAlarm) {
        sLLClass += "_Ack";
        sHHClass += "_Ack";
    }
    if (bSMS) {
        sLLClass += "_Mail";
        sLClass  += "_Mail";
        sHClass  += "_Mail";
        sHHClass += "_Mail";
    }
    sLLClass += ".";
    sLClass  += ".";
    sHClass  += ".";
    sHHClass += ".";

    dynClear(dsLevelsAlarm);
    dynClear(dsLevelsAlertsClass);

    if (LLparam != "") {
        dynAppend(dsLevelsAlarm, "LL");
        dynAppend(dsLevelsAlertsClass, sLLClass);
        dynAppend(dfLevelLimits, LLparam);
    }

    if (Lparam != "") {
        dynAppend(dsLevelsAlarm, "L");
        dynAppend(dsLevelsAlertsClass, sLClass);
        dynAppend(dfLevelLimits, Lparam);
    }

    dynAppend (dsLevelsAlarm, "Ok");
    dynAppend (dsLevelsAlertsClass, "");

    if (Hparam != "") {
        dynAppend(dsLevelsAlarm, "H");
        dynAppend(dsLevelsAlertsClass, sHClass);
        dynAppend(dfLevelLimits, Hparam);
    }

    if (HHparam != "") {
        dynAppend(dsLevelsAlarm, "HH");
        dynAppend(dsLevelsAlertsClass, sHHClass);
        dynAppend(dfLevelLimits, HHparam);
    }
}

/** Returns alert class according to passed params

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param acknowledgeAlarm is alarm acknowledgeable
@param bSMS does alarm trigger mail/sms notifier
@param postfix class postfix
*/
string cpcConfigGenericFunctions_getAlertClass(bool acknowledgeAlarm, bool bSMS, string postfix = "", string baseClass = CPC_CONFIG_ALARM_BASE_CLASS)
{
  string alertClass = baseClass + postfix;

  if( acknowledgeAlarm )
  {
    alertClass += CPC_CONFIG_ALARM_ACK_POSTFIX;
  }

  if( bSMS )
  {
    alertClass += CPC_CONFIG_ALARM_MAIL_POSTFIX;
  }

  return alertClass;

}




//----------------------------------------------------------------------------------------------------------------------------------
/**Return the list of parameter names of device

This list is defined in DT_Config.ctl script by function called DT_getParamNames()

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDeviceType device type

@return device's parameter names list
*/
dyn_string cpcConfigGenericFunctions_getDeviceParamNames(string sDeviceType)
{
  string sFunction;
  dyn_string dsParameterNames;


  sFunction = "CPC_" + cpcConfigGenericFunctions_getShortTypeName(sDeviceType) + "Config_getParamNames";
  if( isFunctionDefined(sFunction) )
  {
//     DebugTN("cpcConfigGenericFunctions_getDeviceParamNames() -> sFunction = " + sFunction);
    evalScript(dsParameterNames, "dyn_string main()"                                     +
                                 "{"                                                     +
                                 "  dyn_string dsParameterNames = " + sFunction  + "();" +
                                 "  return (dsParameterNames);"                          +
                                 "}",
                                 makeDynString());
  }

  return dsParameterNames;
}



/**Returns dpes config

This mapping is defined in DT_Config.ctl script by function called DT_getConfig()

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType device type
@return dpes configs
*/
mapping cpcConfigGenericFunctions_getDPEConfig(string deviceType)
{
  mapping addresses;
  dyn_string dollars;


  if( isDollarDefined("$sFrontEndDeviceType") )
  {
    dynAppend(dollars, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
  }

  evalScript(addresses, "mapping main()" +
                        "{"              +
                        "  return CPC_" + cpcConfigGenericFunctions_getShortTypeName(deviceType) + "Config_getConfig();" +
                        "}",
                        dollars);


  dyn_string keys     = mappingKeys(addresses);
  dyn_string allProps = makeDynString("hasUnit", "hasFormat", "hasSmooth", "hasArchive", "hasPvRange", "hasMapping", "isAlarm", "stubAlarm", "hasAcknowledge", "5RangesAlerts");
  string plcType      = _cpcConfigGenericFunctions_getFrontEndType();

  for( int i = 1 ; i <= dynlen(keys) ; i++ )
  {
    string dpe = keys[i];
    for( int j = 1 ; j <= dynlen(allProps) ; j++ )
    {
      if( !mappingHasKey(addresses[dpe], allProps[j]) )
      {
        addresses[dpe][allProps[j]] = false;
      }
    }

    if( !mappingHasKey(addresses[dpe], "dataType") )
    {
      addresses[dpe]["dataType"] = CPC_BOOL;
    }

    addresses[dpe]["isInput"] = strpos(addresses[dpe]["address"], ".ProcessInput") == 0;
    if( plcType == SOFT_FE_DPTYPE )
    {
      if( addresses[dpe]["hasSmooth"] )
      {
        DebugN("disable smooth options of " + deviceType + "-" + dpe);
      }

      addresses[dpe]["hasSmooth"] = false;
    }
  }

  return addresses;

}




synchronized bool cpcConfigGenericFunctions_createGlobalConst(dyn_string &exceptionInfo)
{
  if( addGlobal("g_mCpcConst", MAPPING_VAR) != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "Error creating global variable: g_mCpcConst", "");
    return FALSE;
  }

  return TRUE;
}





synchronized bool cpcConfigGenericFunctions_AddConst(string sPrefix, string sConstant, anytype aValue)
{
  mapping mTemp;


  if( mappingHasKey(g_mCpcConst, sPrefix) == FALSE )
  {
    mTemp[sConstant]     = aValue;
    g_mCpcConst[sPrefix] = mTemp;
  }
  else
  {
    if( mappingHasKey(g_mCpcConst[sPrefix], sConstant) == FALSE )
    {
      g_mCpcConst[sPrefix][sConstant] = aValue;
    }
  }

  return TRUE;
}





/**Return the value of a device's property constant

Property's constant should be defined in DP_Config.ctl script and have following notation %FRONT_END_PREFIX%_%SHORT_DEVICE_NAME%_%PROPERTY_NAME%

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDeviceType device type
@param sConstantName constant name
@param bVerbose, [optional] displays if constant doesn't exist

@return device's property value
*/
int cpcConfigGenericFunctions_getConstant(string sDeviceType, string sConstantName, bool bVerbose = FALSE)
{
  bool bConstInCache;
  int iResult;
  string sName, sNamePrefix, sFrontEndType, sPrefix;
  mapping mTemp;
  dyn_string exceptionInfo;

  // DebugTN("cpcConfigGenericFunctions_getConstant() -> sDeviceType = " + sDeviceType + ", sConstantName = " + sConstantName);

  bConstInCache = FALSE;

  if( globalExists("g_mCpcConst") == FALSE )
  {
    // Create global
    if( cpcConfigGenericFunctions_createGlobalConst(exceptionInfo) == FALSE )
    {
      DebugTN("cpcConfigGenericFunctions_getConstant() -> Error, gathering constant for device type: " + sDeviceType + ", constant name: " + sConstantName + ", due to problems checking global variable g_mCpcConst: " + exceptionInfo[2]);
      return -1;
    }
  }

  // DebugTN("cpcConfigGenericFunctions_getConstant() -> check g_mCpcConst = ", g_mCpcConst);

  sFrontEndType = _cpcConfigGenericFunctions_getFrontEndType();
  sPrefix       = cpcConfigGenericFunctions_getConstantPrefix(sFrontEndType);
  sNamePrefix   = sPrefix + strtoupper(sDeviceType);

  // Check if constant exist in the cache
  if( mappingHasKey(g_mCpcConst, sNamePrefix) == TRUE )
  {
    if( mappingHasKey(g_mCpcConst[sNamePrefix], sConstantName) == TRUE )
    {
      // Return the constant in the cache
      // DebugTN("cpcConfigGenericFunctions_getConstant() -> constant available in cache, value = ", g_mCpcConst[sNamePrefix][sConstantName] );

      bConstInCache = TRUE;
      iResult       = g_mCpcConst[sNamePrefix][sConstantName];
    }
  }


  if( bConstInCache == FALSE )
  {
    // We need to query the constant and cache it
    sName = sNamePrefix + "_" + sConstantName;
    evalScript(iResult, "int main()"                    +
                        "{"                             +
                        "  int iResult = -1;"           +
                        "  try"                         +
                        "  {"                           +
                        "    iResult = " + sName + ";"  +
                        "  }"                           +
                        "  catch"                       +
                        "  {"                           +
                        "    iResult = -1;"             +
                        "  }"                           +
                        "  return iResult;"             +
                        "}",
                        makeDynString());


    // Update the constant in the cache
    if( mappingHasKey(g_mCpcConst, sNamePrefix) == FALSE )
    {
      // NamePrefix item doesn't exist
      mTemp[sConstantName]     = iResult;
      g_mCpcConst[sNamePrefix] = mTemp;
    }
    else
    {
      if( mappingHasKey(g_mCpcConst[sNamePrefix], sConstantName) )
      {
        if( g_mCpcConst[sNamePrefix][sConstantName] != iResult )
        {
          DebugTN("cpcConfigGenericFunctions_getConstant() -> Error, constant already exists and new value found, please contact icecontrols.support@cern.ch -> Previous = " + g_mCpcConst[sNamePrefix][sConstantName] + " -> New = " + iResult);
          g_mCpcConst[sNamePrefix][sConstantName] = iResult;
        }
        // else  // Constant is the same than in the cache and doesn't need to be updated
      }
      else
      {
        // NamePrefix and next mapping already exists
        g_mCpcConst[sNamePrefix][sConstantName] = iResult;
      }
    }
  }


  // DebugTN("cpcConfigGenericFunctions_getConstant() -> iResult = ", iResult);


  if( bVerbose && iResult == -1 )
  {
    DebugTN("cpcConfigGenericFunctions_getConstant() -> Error, Can't find required constant " + sName);
  }

  return iResult;
}




/**Returns address position in the importation line for passed DPE name

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDeviceType device type
@param sAddress DPE name

@return address position in the import line
*/
int cpcConfigGenericFunctions_getConstantDPEAdress(string sDeviceType, string sAddress)
{
  int iPosition;
  string sConstantPrefix, sFrontEndType, sConstantName;

  // DebugTN("cpcConfigGenericFunctions_getConstantDPEAdress() -> sDeviceType = " + sDeviceType + ", sAddress = " + sAddress);

  sFrontEndType        = _cpcConfigGenericFunctions_getFrontEndType();
  sConstantPrefix      = cpcConfigGenericFunctions_getConstantPrefix(sFrontEndType);
  iPosition            = cpcConfigGenericFunctions_getConstantByName(sConstantPrefix + strtoupper(sDeviceType), "ADDRESS_" + strtoupper(sAddress));

  //   DebugTN("cpcConfigGenericFunctions_getConstantDPEAdress() -> iPosition = " + iPosition);

  return (iPosition);

}

/**Returns config position for given device type, periphery address name and property name.

Function make a lookup for constant UN_CONFIG_[DEVICE]_[ADDRESS_NAME]_[PROP]. if not found then try constant UN_CONFIG_[DEVICE]_[PROP]

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDeviceType, device type
@param sAddressName, DPE name
@param bHasArchive, does importation line contain bool/analog/event archives
@param sPropertyName, DPE's property name
*/
int cpcConfigGenericFunctions_getConstantDPEProperty(string sDeviceType, string sAddressName, bool bHasArchive, string sPropertyName)
{
  int iPosition;
  string sName, sPrefix, sFrontEndType;


  // DebugTN("cpcConfigGenericFunctions_getConstantDPEProperty() -> sDeviceType = " + sDeviceType + ", sAddressName = " + sAddressName + ", bHasArchive = " + bHasArchive + ", sPropertypName = " + sPropertyName);

  iPosition     = 0;
  sFrontEndType = _cpcConfigGenericFunctions_getFrontEndType();
  sPrefix       = cpcConfigGenericFunctions_getConstantPrefix(sFrontEndType);
  sName         = sPrefix + strtoupper(sDeviceType);
  // DebugTN("cpcConfigGenericFunctions_getConstantDPEProperty() -> sName = " + sName);


  if( strlen(sAddressName) > 0 )
  {
    iPosition = cpcConfigGenericFunctions_getConstantByName(sName, "ADDITIONAL_" + strtoupper(sAddressName) + "_" + strtoupper(sPropertyName));
  }

  if( iPosition < 1 )
  {
    iPosition = cpcConfigGenericFunctions_getConstantByName(sName, "ADDITIONAL_" + strtoupper(sPropertyName));
  }


  if( iPosition > 0 )
  {
    // Calculate full position
    iPosition = iPosition + cpcConfigGenericFunctions_getConstantDPLength(sDeviceType);
    if( bHasArchive )
    {
      iPosition = iPosition + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH;
    }

  }

  if( iPosition < 1 && strlen(sAddressName) > 0 )
  {
    iPosition = cpcConfigGenericFunctions_getConstantByName(sName, strtoupper(sAddressName) + "_" + strtoupper(sPropertyName));
  }

  if( iPosition < 1 )
  {
    iPosition = cpcConfigGenericFunctions_getConstantByName(sName, strtoupper(sPropertyName));
  }

  return iPosition;
}




/**Returns a length of importation line

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDeviceType device type

@return length of the importation line
*/
int cpcConfigGenericFunctions_getConstantDPLength(string sDeviceType)
{
  int iLength;


  iLength = (int) cpcConfigGenericFunctions_getConstant(strtoupper(sDeviceType) , "LENGTH", TRUE);

  return iLength;

}




/**Returns an additional length of importation line

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDeviceType device type

@return additional lenght of importation line
*/
int cpcConfigGenericFunctions_getConstantDPAdditionalLength(string sDeviceType)
{
  int iLength;


  iLength = (int) cpcConfigGenericFunctions_getConstant(sDeviceType, "ADDITIONAL_LENGTH");
  if( iLength < 0 )
  {
    iLength = 0;
  }

  return iLength;

}


/**Returns a value of constant by name

Returns -1 if value is not defined.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sName, constant name

@return constant's value
*/
anytype cpcConfigGenericFunctions_getConstantByName(string sPrefix, string sConstant, bool bSpecialConstant = FALSE)
{
  bool bConstInCache;
  string sName;
  anytype aResult, aReturn;
  mapping mTemp;
  dyn_string exceptionInfo;

  // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> sPrefix = " + sPrefix + ", sConstant = " + sConstant);


  bConstInCache = FALSE;


  if( globalExists("g_mCpcConst") == FALSE )
  {
    // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> creating global g_mCpcConst");
    if( cpcConfigGenericFunctions_createGlobalConst(exceptionInfo) == FALSE )
    {
      DebugTN("cpcConfigGenericFunctions_getConstantByName() -> Error, gathering constant by name for device type: " + sDeviceType + ", constant name: " + sConstant + ", due to problems checking global variable g_mCpcConst: " + exceptionInfo[2]);
      return -1;
    }
  }


  // Check if constant exist in the cache
  if( mappingHasKey(g_mCpcConst, sPrefix) == TRUE )
  {
    if( mappingHasKey(g_mCpcConst[sPrefix], sConstant) == TRUE )
    {
      // Return the constant in the cache
      // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> constant available in cache, value = ", g_mCpcConst[sPrefix][sConstant]);
      // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> constant available in cache, g_mCpcConst[sPrefix] = ", g_mCpcConst[sPrefix]);

      bConstInCache = TRUE;
      aReturn       = g_mCpcConst[sPrefix][sConstant];
    }
  }


  if( bConstInCache == FALSE )
  {
    // We need to query the constant and cache it
    if( sPrefix == "UNICOS" )
    {
      sName = sConstant;
    }
    else
    {
      sName = sPrefix + "_" + sConstant;
    }
    // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> sName query = ", sName);

    evalScript(aResult, "anytype main()"               +
                        "{"                            +
                        "  anytype aResult;"           +
                        "  try"                        +
                        "  {"                          +
                        "    aResult = " + sName + ";" +
                        "  }"                          +
                        "  catch"                      +
                        "  {"                          +
                        "    aResult = -1;"            +
                        "  }"                          +
                        "  return aResult;"
                        "}",
                        makeDynString());

    // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> aResult = ", aResult);

    if( (bSpecialConstant == TRUE) && (aResult == -1) )
    {
      // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> Change to CPC_UNDEFINED_CONSTANT_VALUE: " + CPC_UNDEFINED_CONSTANT_VALUE);
      aReturn = (string) CPC_UNDEFINED_CONSTANT_VALUE;
    }
    else
    {
      aReturn = aResult;
    }
    // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> aReturn = ", aReturn);


    // Update the constant in the cache
    if( mappingHasKey(g_mCpcConst, sPrefix) == FALSE )
    {
      // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> Update constant in cache: prefix doesn't exist in cache = " + sPrefix );

      // NamePrefix item doesn't exist
      mTemp[sConstant]     = aReturn;
      g_mCpcConst[sPrefix] = mTemp;
    }
    else
    {
      // NamePrefix and next mapping already exists
      // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> Update constant in cache: prefix already exists in cache = " + sPrefix + " -> ", g_mCpcConst[sPrefix]);

      if( mappingHasKey(g_mCpcConst[sPrefix], sConstant) )
      {
        if( g_mCpcConst[sPrefix][sConstant] != aReturn )
        {
          DebugTN("cpcConfigGenericFunctions_getConstantByName() -> Error, constant already exists and new value found, please contact icecontrols.support@cern.ch -> Previous = " + g_mCpcConst[sPrefix][sConstant] + " -> New = " + aReturn);
          g_mCpcConst[sPrefix][sConstant] = aReturn;
        }
        // else  // Constant is the same than in the cache and doesn't need to be updated

      }
      else
      {
        // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> Update constant in cache[" + sPrefix + "]: Constant didn't exist: " + sConstant);
        g_mCpcConst[sPrefix][sConstant] = aReturn;
      }
    }
  }
  // DebugTN("cpcConfigGenericFunctions_getConstantByName() -> exit aReturn = ", aReturn);


  return aReturn;
}




/**Returns a string value of constant by name

Returns CPC_UNDEFINED_CONSTANT_VALUE if value is not defined.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sName constant name

@return constant's value
*/
string cpcConfigGenericFunctions_getStringConstantByName(string sName)
{
  string sResult;


  //DebugTN("cpcConfigGenericFunctions_getStringConstantByName() -> sName = " + sName);
  evalScript(sResult, "anytype main()"                              +
                      "{"                                           +
                      "  string sResult = \"\";"                    +
                      "  try"                                       +
                      "  {"                                         +
                      "    sResult = " + sName + ";"                +
                      "  }"                                         +
                      "  catch"                                     +
                      "  {"                                         +
                      "    sResult = CPC_UNDEFINED_CONSTANT_VALUE;" +
                      "  }"                                         +
                      "  return sResult;"                           +
                      "}",
                      makeDynString());


  //DebugTN("cpcConfigGenericFunctions_getStringConstantByName() -> sResult = " + sResult);

  return sResult;

}




/**Returns a device type name without CPC_ prefix

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType device type
@return DT name without CPC_ prefix
*/
string cpcConfigGenericFunctions_getShortTypeName(string deviceType) {
    string dt = deviceType;
    strreplace(dt, "CPC_", "");
    return dt;
}

/**Returns a position constants prefix

Can be SOFT_FE for SOFT Front-End or UN_CONFIG for others.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sFrontEndType, Front end type
@return front-end related prefix
*/
string cpcConfigGenericFunctions_getConstantPrefix(string sFrontEndType)
{
  string sPrefix;


  sPrefix = "UN_CONFIG_";
  if( _cpcConfigGenericFunctions_getFrontEndType() == SOFT_FE_DPTYPE )
  {
    sPrefix = sPrefix + "SOFT_FE_";
  }


  return sPrefix;
}

//----------------------------------------------------------------------------------------------------------------------------------
/**Returns selected front-end type

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@return deviceType device type
*/
string _cpcConfigGenericFunctions_getFrontEndType() {
    // TODO: refactor me. animation can call this...
    // TODO: we should remove this function. It creates a wrong abstraction that front end type is always available, like a global value. it doesn't correspond the reality
    // the problem is that in device check it's the only way to get front end type.
    string sFrontEndType;
    if (isFunctionDefined("shapeExists") && shapeExists("TextFieldPlcType")) {
        sFrontEndType = unConfigGenericFunctions_extractFeProtocol(TextFieldPlcType.text);
        if (sFrontEndType == UN_CONFIG_PROTOCOL_MODBUS || sFrontEndType == UN_CONFIG_PREMIUM || sFrontEndType == UN_CONFIG_QUANTUM || sFrontEndType == UN_CONFIG_UNITY) {
            sFrontEndType = UN_PLC_DPTYPE;
        } else if (sFrontEndType == UN_CONFIG_PROTOCOL_S7 || sFrontEndType == UN_CONFIG_S7_400 || sFrontEndType == UN_CONFIG_S7_300) {
            sFrontEndType = S7_PLC_DPTYPE;
        }
    } else if (isDollarDefined("$sFrontEndDeviceType")) {
        sFrontEndType = $sFrontEndDeviceType;
    } else {
        DebugN("_cpcConfigGenericFunctions_getFrontEndType: can't detect front-end type");
    }
    return sFrontEndType;
}

string cpcConfigGenericFunctions_getFrontEndType(dyn_string dsConfig)
{
    return unImportDevice_getFrontEndDeviceType(dsConfig[UN_CONFIG_OBJECTGENERAL_PLC_TYPE]);
}

/**Convert CPC data-type representation to the front-end specific

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param plcType plc to translate to
@param commonDataType CPC data type
@return int, fromt end data type
*/
int cpcConfigGenericFunctions_translateDataType(string sFrontEndType, int iCommonDataType)
{
  switch( sFrontEndType )
  {
    case UN_PLC_DPTYPE:
      switch( iCommonDataType )
      {
        case CPC_INT16:
          return PVSS_MODBUS_INT16;
          break;

        case CPC_INT32:
          return PVSS_MODBUS_INT32;
          break;

        case CPC_UINT16:
          return PVSS_MODBUS_UINT16;
          break;

        case CPC_BOOL:
          return PVSS_MODBUS_BOOL;
          break;

        case CPC_FLOAT:
          return PVSS_MODBUS_FLOAT;
          break;

        default:
          break;
      }
      break;


    case CPC_OPCUA_DPTYPE:
      switch( iCommonDataType )
      {
        case CPC_INT16:
          return OPCUA_INT16;
          break;

        case CPC_INT32:
          return OPCUA_INT32;
          break;

        case CPC_UINT16:
          return OPCUA_UINT16;
          break;

        case CPC_BOOL:
          return OPCUA_BOOL;
          break;

        case CPC_FLOAT:
          return OPCUA_FLOAT;
          break;

        default:
          break;
      }
      break;


    case CPC_BACNET_DPTYPE:
      // TODO
      switch( iCommonDataType )
      {
        case CPC_INT16:
          return 800;
          break;

        case CPC_INT32:
          return 800;
          break;

        case CPC_UINT16:
          return 800;
          break;

        case CPC_BOOL:
          return 800;
          break;

        case CPC_FLOAT:
          return 800;
          break;

        default:
          break;
      }
      break;


    case CPC_SNMP_DPTYPE:
      switch( iCommonDataType )
      {
        case CPC_INT16:
          return 661;
          break;

        case CPC_INT32:
          return 661;
          break;

        case CPC_UINT16:
          return 661;
          break;

        case CPC_BOOL:
          return 661;
          break;

        case CPC_FLOAT:
          return 661;
          break;

        default:
          break;
      }
      break;


    case CPC_IEC104_DPTYPE:
      // To be done
      switch( iCommonDataType )
      {
        case CPC_INT16:
          return PVSS_S7_INT16;
          break;

        case CPC_INT32:
          return PVSS_S7_INT32;
          break;

        case CPC_UINT16:
          return PVSS_S7_UINT16;
          break;

        case CPC_BOOL:
          return PVSS_S7_BOOL;
          break;

        case CPC_FLOAT:
          return PVSS_S7_FLOAT;
          break;

        default:
          break;
      }
      break;


    default:
      switch( iCommonDataType )
      {
        case CPC_INT16:
          return PVSS_S7_INT16;
          break;

        case CPC_INT32:
          return PVSS_S7_INT32;
          break;

        case CPC_UINT16:
          return PVSS_S7_UINT16;
          break;

        case CPC_BOOL:
          return PVSS_S7_BOOL;
          break;

        case CPC_FLOAT:
          return PVSS_S7_FLOAT;
          break;

        default:
          break;

      }
      break;
  }

}//  cpcConfigGenericFunctions_translateDataType()





/**Convert CPC data-type representation to the S7 check data type

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param commonDataType CPC data type
@param sAddressValue S7 Address to be checked
@return S7 data type check
*/
int cpcConfigGenericFunctions_translateDataTypeForS7Check(int commonDataType, string sAddressValue) {
    // First try to get the type from the address
    string s1, s2, s3;
    int i1, i2, i3;
    sscanf(sAddressValue, "%[^0-9]%d%[^0-9]%d%[^0-9]%d", s1, i1, s2, i2, s3, i3);
    if (s2 == "." + UN_S7_FORMAT_WORD) {
        return UN_CONFIG_S7_PLC_DATATYPE_WORD;
    } else if (s2 == "." + UN_S7_FORMAT_BIT) {
        return UN_CONFIG_S7_PLC_DATATYPE_BOOL;
    } else if (s2 == "." + UN_S7_FORMAT_BYTE) {
        return UN_CONFIG_S7_PLC_DATATYPE_BYTE;
    }
    // If the type is DOUBLE or not found, then get it from the commonDataType
    switch (commonDataType) {
        case CPC_INT16:
        case CPC_UINT16:
            return UN_CONFIG_S7_PLC_DATATYPE_WORD;
        case CPC_INT32:
            return UN_CONFIG_S7_PLC_DATATYPE_DWORD;
        case CPC_BOOL:
            return UN_CONFIG_S7_PLC_DATATYPE_BOOL; //@TODO: verify with reality
        case CPC_FLOAT:
            return UN_CONFIG_S7_PLC_DATATYPE_FLOAT;
    }
}

/**Split comma-separated string and trim each element

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@todo move to generic place. reuse in other places. merge with cpcGenericObject_StringToMapping

@param data comma-separated string
@return array of trimmed elements
*/
dyn_string splitCommaSeparatedString(string data) {
    dyn_string result = strsplit(data, ",");
    for (int i = 1; i <= dynlen(result); i++) {
        result[i] = strrtrim(strltrim(result[i]));
    }
    return result;
}

/**Gets archiving configs from object and puts them to the common archiving configs.

@todo move to the unicos core

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iToSkip Num elements in dsConfigs to skip before common part
@param hasArchive is archived defined in the importation line
@param dsConfigs importation line, processed in the unCore
*/
void cpcConfigGenericFunctions_processArchives(int iToSkip, bool hasArchive, dyn_string& dsConfigs) {
    if (hasArchive) {
        if (dsConfigs[iToSkip + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL] != "") {
            dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL] = dsConfigs[iToSkip + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL];
        }

        if (dsConfigs[iToSkip + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG] != "") {
            dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG] = dsConfigs[iToSkip + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG];
        }

        if (dsConfigs[iToSkip + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT] != "") {
            dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT] = dsConfigs[iToSkip + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT];
        }
    }
}

/**Gets additional parameters configs from object and puts them to a mapping.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsConfigs importation line, processed in the unCore
@param sDeviceType Device type name
@param bHasArchive is archived defined in the importation line
*/
mapping cpcConfigGenericFunctions_getParametersMapping(dyn_string dsConfigs, string sDeviceType, bool bHasArchive){
    int sParametersPos = cpcConfigGenericFunctions_getConstantDPEProperty(sDeviceType, "", bHasArchive, "PARAMETERS");
    dyn_string dsParameters = strsplit(dsConfigs[UN_CONFIG_COMMON_LENGTH + sParametersPos], ",");
    mapping mParameters;
    for(int i=1; i<=dynlen(dsParameters); i++){
        dyn_string dsEntry = strsplit(dsParameters[i], "=");
        string sKey = dsEntry[1];
        string sValue = "";
        if(dynlen(dsEntry) > 1) {
            sValue = dsEntry[2];
        }
        mParameters[sKey] = sValue;
    }
    return mParameters;
}

/**Checks message convertion parameter

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sConversion string with message convertion configuration
@param exceptionInfo for errors
*/
void cpcConfigGenericFunctions_checkMsgConv(string sConversion, dyn_string &exceptionInfo) {
    if (sConversion != ""){
        dyn_string dsConversion = strsplit(sConversion, "|");
        string sType = strrtrim(strltrim(strtolower(dsConversion[1])));
        if(sType == "linear") {
            if(dynlen(dsConversion) != 3) {
                fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkMsgConv: wrong set of parameters for linear transformation: " + getCatStr("unGeneration", "BADINPUTS"), "");
                return;
            } else {
                float fValue;
                unConfigGenericFunctions_checkFloat(dsConversion[2], fValue, exceptionInfo);
                if (dynlen(exceptionInfo) > 0) {
                    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkMsgConv: wrong float value: " + dsConversion[2] + ", " + getCatStr("unGeneration", "BADINPUTS"), "");
                    return;
                }
                unConfigGenericFunctions_checkFloat(dsConversion[3], fValue, exceptionInfo);
                if (dynlen(exceptionInfo) > 0) {
                    fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkMsgConv: wrong float value: " + dsConversion[3] + ", " + getCatStr("unGeneration", "BADINPUTS"), "");
                    return;
                }
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "cpcConfigGenericFunctions_checkMsgConv: wrong type of transformation: " + sType + ", " + getCatStr("unGeneration", "BADINPUTS"), "");
            return;
        }
    }
}

/** Sets _msg_conv dpe config based on given parameter string

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sConversion string with message convertion configuration
@param dpes list of dpes to e configured
@param exceptionInfo for errors
*/
void cpcConfigGenericFunctions_setMsgConv(string sConversion, dyn_string dpes, dyn_string &exceptionInfo) {
    if(sConversion!="") {
        dyn_string dsConversion = strsplit(sConversion, "|");
        string sType = strrtrim(strltrim(strtolower(dsConversion[1])));

        for(int i=1; i<=dynlen(dpes); i++) {
            if(sType == "linear") {
                fwConfigConversion_set(dpes[i], DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN, DPDETAIL_CONV_POLY, 1, makeDynFloat(dsConversion[2], dsConversion[3]), exceptionInfo);
            }
        }
    }
}


/** Adds system name to the dp if not present

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param textBox contains the dp to be modified in its text attribute
*/
void cpcConfigGenericFunctions_eventCommand(string textBox)
{
    string sDpName;
	dyn_string ds;
	int pos;


    //get the datapoint
    getValue(textBox, "text", sDpName);

    // if no sys name is defined then should take the local
	if (strrtrim(strltrim(sDpName)) != "")
	{
		if( strpos(sDpName,":") <0)
		{
			//no check on the system is done
			//because this is to allow the user to develop in his own lab without having to be connected to the hardware

            // if there is a $ given assume that this is a dollar parameter
			if(strpos(sDpName,"$") <0)
				sDpName = getSystemName() + sDpName;

		}
		setValue(textBox,"text",sDpName);
    }
}
