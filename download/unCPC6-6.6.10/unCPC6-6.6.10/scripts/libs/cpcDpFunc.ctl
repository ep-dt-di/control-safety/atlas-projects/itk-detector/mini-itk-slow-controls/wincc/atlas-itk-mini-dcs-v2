/**@file

// cpcDpFunc.ctl
This library contains dpFunctions and functions to set them up.

@par Creation Date
  04/02/2014

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL, DP_FUNC

@author
  Alexey Merezhin (EN-ICE)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
#uses "unicos_declarations.ctl"
#uses "CPC_declarations.ctl"

/** Function to retrieve a status register value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL

@reviewed 2018-06-22 @whitelisted{DPFunction}

@param name name of datapoint
@param manreg01 manreg value
*/
bit32 cpcDigitalParameterDPFunc_getStsReg01Value(string name, bit32 manreg01) {
    bit32 result = 0;

    if (getBit(manreg01, CPC_ManReg01_MONR)) {
        setBit(result, CPC_StsReg01_POSST, 1);
    } else if (getBit(manreg01, CPC_ManReg01_MOFFR)) {
        setBit(result, CPC_StsReg01_POSST, 0);
    } else {
        dpGet(name, result);
    }

    return result;
}

/** Function set up the workaround for a dpe (deviceName + elemName) to keep it consistent with AlUnAck signal.

See cpcDpFunc_syncAcknowledge for more details.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL

@param deviceName name of datapoint (e.g. "un-PLCATLTRT-ATLTRT-CPC_AnalogAlarm-00761")
@param elemName name of element (e.g. ".ProcessInput.HHAlSt")
*/
void cpcDpFunc_fixAckPropogation(string deviceName, string elemName) {
    string dpeName = deviceName + elemName;
    unGenericDpFunctions_createDpeDpFunction(dpeName);
    dyn_string params = makeDynString(deviceName + ".ProcessInput.AlUnAck:_original.._value");
    dpSetWait(dpeName + ":_dp_fct.._param", params, dpeName + ":_dp_fct.._fct", "cpcDpFunc_syncAcknowledge(\"" + dpeName + "\", p1)");
}

/** Function acknowledges alert handler of dpe called dpeName if isUnAck is false but alert is not acknowledged.

Function is needed to repair the consistency between device acknowledged in PLC bypassing WinCC OA.
For more details see UCPC-862.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL, DP_FUNC

@reviewed 2018-06-22 @whitelisted{DPFunction}

@param dpeName name of dpe configured with alert handler ("un-PLCATLTRT-ATLTRT-CPC_AnalogAlarm-00761.ProcessInput.HHAlSt")
@param isUnAck if device is not acknowledged
*/
int cpcDpFunc_syncAcknowledge(string dpeName, bool isUnAck) {
    int value, state;
    dpGet(dpeName, value, dpeName + ":_alert_hdl.._act_state", state);
    if (!isUnAck && (state == 1 || state == 3)) {
        //DebugN("repair alert handler " + dpeName);
        dpSetWait(dpeName + ":_alert_hdl.._ack", DPATTR_ACKTYPE_MULTIPLE);
    }
    return value;
}
