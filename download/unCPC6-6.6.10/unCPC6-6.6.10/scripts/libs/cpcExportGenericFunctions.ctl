#uses "cpcExportGenericFunctionsDeprecated.ctl"




/**@file

@brief This library contains generic exportation functions.

Normally a function in this library is designed to accept currentDP and dsDpParameters (array representation of pvss importation line) with two first parameters.
Other parameters may be accepted in order to specify a particular element in device to process.
Function adds certain parameters to the end of dsDpParameters as a result of its work.

@author Nikolay KULMAN (IT/CO)
@author Alexey Merezhin (EN-ICE-PLC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Creation Date
  06/04/2006

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL

*/

/**Add DPE address to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param deviceType device type
@param dpeName dpe name (without path)
@param isInput should be true if it's in ProcessInput and false if it's in ProcessOutput
@param dsDpParameters object's output
@return nothing
*/
void cpcExportGenericFunctions_processAddress(string sDp, string sDeviceType, string sDpeName, bool bIsInput, dyn_string &dsDpParameters)
{
  int iUpdateTime;
  string sTemp, sPrefix, sDpe, sFunction;
  mixed mValue;


  // Get the Dpe name
  if( bIsInput == TRUE )
  {
    sPrefix = ".ProcessInput.";
  }
  else
  {
    sPrefix = ".ProcessOutput.";
  }
  sDpe = sDp + sPrefix + sDpeName;

  dpGet(sDpe + ":_address.._drv_ident" , sTemp);

  switch( sTemp )
  {
    case "IEC":
      sTemp = "IEC104";
      break;

    case "":
      break;

    default:
      break;
  }

  sFunction = "DRV_" + sTemp + "_convertToUnicosAddress";
  if( isFunctionDefined(sFunction) )
  {
    evalScript(sTemp, "string main(string sDeviceDpeName, string sDeviceType)"    +
                      "{"                                                         +
                      "  string sTemp;"                                           +
                      "  sTemp = " + sFunction + "(sDeviceDpeName, sDeviceType);" +
                      "  return sTemp;"                                           +
                      "}",
               makeDynString(),
               sDpe,
               sDeviceType);
  }
  else
  {
    // Address not set
    dpGet(sDpe,                        mValue,
          sDpe + ":_original.._stime", iUpdateTime);
    if( iUpdateTime == 0 )
    {
      sTemp = UN_CONFIG_EXPORT_NONE; // UN_CONFIG_EXPORT_UNDEF;
    }
    else
    {
      sTemp = PARAM_DPE_VALUE + mValue;
    }
  }

  dynAppend(dsDpParameters, sTemp);

}//  cpcExportGenericFunctions_processAddress()





// DRV_CMW_convertToUnicosAddress
/**
Purpose: function to retrieve CMW address for given Data Point

Parameters:
		sDeviceDpeName: string, input, dpe name for which address should be formatted
		sDeviceType: string, input, device type name

Usage: External function

PVSS manager usage: Ctrl, NG, NV

@reviewed 2018-07-24 @whitelisted{Callback}
*/
string DRV_CMW_convertToUnicosAddress(string sDeviceDpeName, string sDeviceType)
{
  dyn_string addressConfig;
  unConfigCMW_getAddressConfig(sDeviceDpeName, addressConfig);
  if (addressConfig[4] == "") {             // no address config
    return "";
  } else {
    return addressConfig[1] + "|" +         // type
           addressConfig[2] + "|" +         // transformation
           addressConfig[3] + "|" +         // poll group, interval
           addressConfig[4] + "$" + addressConfig[5] + "$" + addressConfig[6] + "|" + // device$property$tag
           addressConfig[7] + "|" +         // filter name
           addressConfig[8] + "|" +         // filter value
           addressConfig[9];                // LLC
  }
}




/**Add parameters field to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param exceptionInfo exception info
*/
void cpcExportGenericFunctions_getParameters(string sDp, dyn_string &dsDpParameters, dyn_string &exceptionInfo) {
    dyn_string params;

    dyn_string dsKey;
    dyn_dyn_string ddsConfig;
    _unGenericDpFunctions_getConfiguration(sDp, UN_DEVICE_CONFIGURATION, dsKey, ddsConfig, exceptionInfo);
    for (int i = 1; i <= dynlen(dsKey); i++) {
        string key = dsKey[i];
        if (strpos(key, UN_CPC_DEVICE_PROPERTY_PREFIX) == 0) {
            strreplace(key, UN_CPC_DEVICE_PROPERTY_PREFIX, "");
            string value = ddsConfig[i];
            dynAppend(params, key + "=" + value);
        }
    }

    dynAppend(dsDpParameters, dynJoin(params, ","));
}

/**Add meta-info fields to the device's output

Meta info is device's heirarchy (i.e. master/parents/children), type and second alias

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param exceptionInfo exception info
*/
void cpcExportGenericFunctions_getMetainfo(string sDp, dyn_string &dsDpParameters, dyn_string &exceptionInfo) {
    dyn_string master, parents, children, type, second_alias;
    unGenericDpFunctions_getKeyDeviceConfiguration(sDp, CPC_CONFIG_MASTER_NAME_KEY, master, exceptionInfo);
    unGenericDpFunctions_getKeyDeviceConfiguration(sDp, CPC_CONFIG_PARENTS_KEY, parents, exceptionInfo);
    unGenericDpFunctions_getKeyDeviceConfiguration(sDp, CPC_CONFIG_CHILDREN_KEY, children, exceptionInfo);
    unGenericDpFunctions_getKeyDeviceConfiguration(sDp, CPC_CONFIG_TYPE_KEY, type, exceptionInfo);
    unGenericDpFunctions_getKeyDeviceConfiguration(sDp, CPC_CONFIG_SECOND_ALIAS_KEY, second_alias, exceptionInfo);

    dynAppend(dsDpParameters, dynJoin(master, ","));
    dynAppend(dsDpParameters, dynJoin(parents, ","));
    dynAppend(dsDpParameters, dynJoin(children, ","));
    dynAppend(dsDpParameters, dynJoin(type, ","));
    dynAppend(dsDpParameters, dynJoin(second_alias, ","));
}

/**Add unit to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param dpe name of unit's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getUnit(string sDp, dyn_string &dsDpParameters, string dpe = ".ProcessInput.PosSt") {
    string unit;
    unit = dpGetUnit(sDp + dpe);
    dynAppend(dsDpParameters, unit);
}

/**Add format to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param dpe name of format's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getFormat(string sDp, dyn_string &dsDpParameters, string dpe = ".ProcessInput.PosSt") {
    string format;
    dyn_string exceptionInfo;
    format = unExportDevice_getFormat(sDp, dpe, exceptionInfo);
    dynAppend(dsDpParameters, format);
}

/**Add format without log_scale flag to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param dpe name of format's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getSimpleFormat(string sDp, dyn_string &dsDpParameters, string dpe = ".ProcessInput.PosSt") {
    string format;
    format = unExportDevice_convertFormat(dpGetFormat(sDp + dpe), "#");
    dynAppend(dsDpParameters, format);
}

/**Add range fields (max, min) to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param exceptionInfo exception info
@param dpe name of range's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getRange(string sDp, dyn_string &dsDpParameters, dyn_string &exceptionInfo, string dpe = ".ProcessInput.PosSt") {
    bool bDoesExist, bNegateRange, bIgnoreOutside, bInclusiveMin, bInclusiveMax;
    float fMinValue, fMaxValue;

    fwPvRange_get(sDp + dpe, bDoesExist, fMinValue, fMaxValue, bNegateRange, bIgnoreOutside, bInclusiveMin, bInclusiveMax, exceptionInfo);
    dynAppend(dsDpParameters, unExportDevice_floatFormat(fMaxValue));
    dynAppend(dsDpParameters, unExportDevice_floatFormat(fMinValue));
}

/**Add out range fields (max, min) to the device's output

Out range stands for the scaled range.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param exceptionInfo exception info
@param dpe name of out range's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getOutRange(string sDp, dyn_string &dsDpParameters, dyn_string &exceptionInfo, string dpe = ".ProcessInput.PosSt") {
    float rangeMin, rangeMax, outRangeMax, outRangeMin;
    bool bDoesExist, bNegateRange, bIgnoreOutside, bInclusiveMin, bInclusiveMax, configExists;
    int conversionType, order;
    dyn_float arguments;

    fwPvRange_get(sDp + dpe, bDoesExist, rangeMin, rangeMax, bNegateRange, bIgnoreOutside, bInclusiveMin, bInclusiveMax, exceptionInfo);
    fwConfigConversion_get(sDp + dpe, configExists, DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN, conversionType, order, arguments, exceptionInfo);
    outRangeMax = arguments[2] * rangeMax + arguments[1];
    outRangeMin = arguments[2] * rangeMin + arguments[1];

    dynAppend(dsDpParameters, unExportDevice_floatFormat(outRangeMax));
    dynAppend(dsDpParameters, unExportDevice_floatFormat(outRangeMin));
}

/**Add deadband fields (deadband and deadband type) to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param exceptionInfo exception info
@param dpe name of deadband's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getDeadband(string sDp, dyn_string &dsDpParameters, dyn_string &exceptionInfo, string dpe = ".ProcessInput.PosSt") {
    float deadband, timeInterval;
    int smoothProcedure, deadbandType;
    bool isSmoothDefined;

    unExportDevice_fwSmoothing_getTemp(sDp + dpe, isSmoothDefined, smoothProcedure, deadband, timeInterval);
    if (isSmoothDefined) {
        switch (smoothProcedure) {
            case DPATTR_VALUE_REL_SMOOTH:
                deadbandType = UN_CONFIG_DEADBAND_RELATIF;
                break;
            case DPATTR_VALUE_SMOOTH:
                deadbandType = UN_CONFIG_DEADBAND_VALUE;
                break;
            case DPATTR_COMPARE_OLD_NEW:
                deadbandType = UN_CONFIG_DEADBAND_OLD_NEW;
                deadband = 0;
                break;
            default:
                deadbandType = DPCONFIG_NONE;
                deadband = 0;
                break;
        }
    } else {
        deadbandType = DPCONFIG_NONE;
        deadband = 0;
    }

    dynAppend(dsDpParameters, deadband);
    dynAppend(dsDpParameters, deadbandType);
}

/**Add archive fields (archive active and archive time filter) to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param dpe name of archive's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getArchive(string sDp, dyn_string &dsDpParameters, string dpe = ".ProcessInput.PosSt") {
    string sArchiveActive;
    float fArchiveTimeFilter;
    unExportDevice_getArchiveParametersDpe(sDp + dpe, sArchiveActive, fArchiveTimeFilter);

    dynAppend(dsDpParameters, sArchiveActive);
    if (fArchiveTimeFilter < 0) {
        dynAppend(dsDpParameters, "");
    } else {
        dynAppend(dsDpParameters, fArchiveTimeFilter);
    }
}

/**Add isDeviceMasked value to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
*/
void cpcExportGenericFunctions_getMaskEvent(string sDp, dyn_string &dsDpParameters) {
    int iMask;
    string sMask;
    unGenericDpFunctions_getMaskEvent(sDp, iMask);
    sprintf(sMask, "%d", iMask);
    dynAppend(dsDpParameters, sMask);
}

/**Add default value to the device's output

Function stands for fetch online value of ProcessOutput DPE

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param dfltElementName dpe name, "DfltVal" by default
*/
void cpcExportGenericFunctions_getDefaultValue(string sDp, dyn_string &dsDpParameters, string dfltElementName = "DfltVal") {
    float defaultValue;
    dpGet(sDp + ".ProcessOutput." + dfltElementName + ":_online.._value", defaultValue);
    dynAppend(dsDpParameters, defaultValue);
}

/**Add default value casted to bool to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@see cpcExportGenericFunctions_getDefaultValue

@param sDp current dp name
@param dsDpParameters object's output
@param dfltElementName dpe name, "DfltVal" by default
*/
void cpcExportGenericFunctions_getBoolDefaultValue(string sDp, dyn_string &dsDpParameters, string dfltElementName = "DfltVal") {
    bool defaultValue;
    dpGet(sDp + ".ProcessOutput." + dfltElementName + ":_online.._value", defaultValue);
    dynAppend(dsDpParameters, defaultValue);
}


/**Add DPE values to the device's output

Function fetches the online value of DPE

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp Current DP name
@param sElementName The DPE name
@param dsDpParameters A dyn_string to which the value of the DPE is appended; the output of the function
@param sPrintFormat Optional sprintf() format used to print the DPE value; default is "", i.e. no custom formatting
@param sConfigDetailAttrib Optional config, detail and attribute of the DPE; default is ":_online.._value", replicating the default dpGet() behaviour in WccOA 3.15
*/
void cpcExportGenericFunctions_getDPEValue(string sDp, string sElementName, dyn_string &dsDpParameters, string sPrintFormat="", string sConfigDetailAttrib=":_online.._value") {
    anytype aReturnValue; // holds value to be returned
    string sReturnType; // the type of the returned valued, osed for debugging only
    string sFullNameDPE; // the complete name of the DPE whose values is to be returned    

    sFullNameDPE = sDp + sElementName + sConfigDetailAttrib;

    sReturnType = dpAttributeType(sFullNameDPE);
    dpGet(sFullNameDPE, aReturnValue);
    

    if ( sPrintFormat == "" )
    {
      dynAppend(dsDpParameters, aReturnValue);
      DebugFN(2, "Exporting online value of type " + sReturnType + ": " + sFullNameDPE + " = " + aReturnValue);
    }
    else
    {
      string sFormatted;
      sprintf(sFormatted, sPrintFormat, aReturnValue);
      DebugFN(2, "Exporting online value of type " + sReturnType + ": " + sFullNameDPE + " = " + sFormatted);
      dynAppend(dsDpParameters, sFormatted);
    }        
}

/**Add PID name to the device's output

Proxy to unExportDevice_getPIDName

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
*/
void cpcExportGenericFunctions_getPIDName(string sDp, dyn_string &dsDpParameters) {
    string sPIDName;
    unExportDevice_getPIDName(sDp, sPIDName);
    dynAppend(dsDpParameters, sPIDName);
}

/**Add device config value for given name to the device's output

Proxy to unExportDevice_getPCOName

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param dsDpParameters object's output
@param key key in device configuration
@param exceptionInfo exception info
*/
void cpcExportGenericFunctions_getKeyDeviceConfiguration(string sDp, dyn_string &dsDpParameters, string key, dyn_string &exceptionInfo) {
    string value;
    unGenericDpFunctions_getKeyDeviceConfiguration(sDp, key, value, exceptionInfo);
    dynAppend(dsDpParameters, value);
}

/**Add digital normal position to the device's output

Port of unExportDevice_getDigitalNormalPositionDPE.

Use full path in alarm list (dsDpElements), for example, there's a list of interlocks for a field object:

    makeDynString(".ProcessInput.StartISt", ".ProcessInput.TStopISt", ".ProcessInput.FuStopISt")

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param sObject device type; don't used
@param dsDpElements an array of alarm dpes
@param dsDpParameters object's output
*/
void cpcExportGenericFunctions_getDigitalNormalPosition(string sDP, string sObject, dyn_string dsDpElements, dyn_string &dsDpParameters) {
    // sObject is not used at the moment, but keept as unicos functions need it. you can pass "".
    bool bAlarmMask, bOkRange, bBoolAlert;
    dyn_int diAlertClass;
    int iAlertType, iRes;
    string sDataPoint, sAlertClass1, sAlertClass2;
    int iNormalPosition = -1;

    for (int i = 1; i <= dynlen(dsDpElements); i++) {
        iRes = dpGet(sDP + dsDpElements[i] + ":_alert_hdl.._type", iAlertType);
        dynAppend(diAlertClass, iAlertType);
    }

    if (dynUnique(diAlertClass) != 1) {
        iAlertType = DPCONFIG_NONE;
    } else {
        iAlertType = diAlertClass[1];
    }

    if (iAlertType == DPCONFIG_NONE)	{
        iNormalPosition = 2;
    } else {
        sDataPoint = sDP + dsDpElements[1];
        bBoolAlert = ((iRes >= 0) && (iAlertType == DPCONFIG_ALERT_BINARYSIGNAL));
        if (bBoolAlert) {
            dpGet(sDataPoint + ":_alert_hdl.._ok_range", bOkRange);
        }

        dpGet(sDataPoint + ":_alert_hdl.._active",  bAlarmMask);
        dpGet(sDataPoint + ":_alert_hdl.1._class",  sAlertClass1);
        dpGet(sDataPoint + ":_alert_hdl.2._class",  sAlertClass2);

        if (bAlarmMask) { //	Alarm unmasked
            iNormalPosition = bOkRange ? 1 : 0;
        } else { //	Alarm masked
            iNormalPosition = bOkRange ? 4 : 3;
        }
    }

    dynAppend(dsDpParameters, iNormalPosition);
}

/**Add PCO parameters to the device's output

Proxy to unExportDevice_getPCOParameters.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param dsDpParameters object's output
*/
void cpcExportGenericFunctions_getPCOParameters(string sDP, dyn_string &dsDpParameters) {
    dyn_string dsPCOParameters;

    unExportDevice_getPCOParameters(sDP, dsPCOParameters);
    for (int i = 1; i <= dynlen(dsPCOParameters); i++) {
        dynAppend(dsDpParameters, dsPCOParameters[i]);
    }
}

/**Add autonomous flag to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param dsDpParameters object's output
@param dpe name of autonomous's dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getAutonomous(string sDP, dyn_string &dsDpParameters, string dpe = ".ProcessInput.PosSt") {
    int iType, iResult;
    iResult = dpGet(sDP + dpe + ":_dp_fct.._type", iType);
    dynAppend(dsDpParameters, iType == DPCONFIG_DP_FUNCTION ? "TRUE" : "FALSE");
}

/**Add archive name of given dpes to the device's output

Funtion collect archive name for each of dpes.
If archive is the same for everyone, it use this archive.
Else it use empty string.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param defaultValue value that used if archive is not configured for dpe
@param dsDpes list of dpes that supposed to have same archive
@param dsDpParameters object's output
*/
void cpcExportGenericFunctions_getArchiveNameForDpes(string sDp, string defaultValue, dyn_string dsDpes, dyn_string &dsDpParameters) {
    dyn_string dsCheck;
    string archiveName;

    if (dynlen(dsDpes) == 0) {
        archiveName = defaultValue;
    } else {
        for (int i = 1; i <= dynlen(dsDpes); i++) {
            string sArchiveClass, sName;

            dpGet(sDp + "." + dsDpes[i] + ":_archive.1._class", sArchiveClass);

            if (dpExists(sArchiveClass + ".general.arName")) {
                dpGet(sArchiveClass + ".general.arName:_online.._value", sName);
            } else if (dpSubStr(sArchiveClass, DPSUB_DP_EL_CONF_DET_ATT) == "_EVENT") {
                sName = "RDB-99) EVENT";
            } else {
                sName = defaultValue;
            }
            dynAppend(dsCheck, sName);
        }
        dynUnique(dsCheck);
        if (dynContains(dsCheck, "") > 0) {
            dynRemove(dsCheck, dynContains(dsCheck, ""));
        }
        if (dynlen(dsCheck) == 1) {     //check if same archive for same type
            archiveName = dsCheck[1];
        } else {
            archiveName = "";
        }
    }

    dynAppend(dsDpParameters, archiveName);
}

/**Return list of dpes of given deviceType that have hasArchive property

Funtion collect dpes' names that are archived

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceType device type name
@param archiveType type of archive: 1 (UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL) for BOOLEAN, 2 (UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG) for ANALOG and 3 (UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT) for EVENT

@return list of dpes
*/
dyn_string cpcExportGenericFunctions_getArchivedDpes(string deviceType, int archiveType) {
    mapping addresses = callFunction("CPC_" + cpcConfigGenericFunctions_getShortTypeName(deviceType) + "Config_getConfig");
    dyn_string dsArchivedDpes;
    
    dyn_string keys = mappingKeys(addresses);
    for (int i = 1; i <= dynlen(keys); i++) {
        string dpe = keys[i];
        if(mappingHasKey(addresses[dpe], "hasArchive")) {
            switch (archiveType) {
                case UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL:
                    if (mappingHasKey(addresses[dpe], "bitPosition") || (mappingHasKey(addresses[dpe], "dataType") && addresses[dpe]["dataType"] == CPC_BOOL)) {
                        dynAppend(dsArchivedDpes, strltrim(addresses[dpe]["address"], "."));
                    }
                    break;
                case UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG:
                    if (mappingHasKey(addresses[dpe], "dataType") && (dynContains(makeDynInt(CPC_FLOAT, CPC_INT16, CPC_UINT16), addresses[dpe]["dataType"]) > 0)) {
                        dynAppend(dsArchivedDpes, strltrim(addresses[dpe]["address"], "."));
                    }
                    break;
                case UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT:
                    if (mappingHasKey(addresses[dpe], "dataType") && addresses[dpe]["dataType"] == CPC_INT32) {
                        dynAppend(dsArchivedDpes, strltrim(addresses[dpe]["address"], "."));
                    }
                    break;
            }
        }
    }

    return dsArchivedDpes;
}

/**Add alarm categories assigned to device to the device's output

Proxy for unProcessAlarm_getCategory.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param dsDpParameters object's output
@param exceptionInfo exception info
*/
void cpcExportGenericFunctions_getCategories(string sDP, dyn_string dpes, dyn_string &dsDpParameters, dyn_string &exceptionInfo) {
    dyn_dyn_string ddsCategory;
    for (int i = 1; i <= dynlen(dpes); i++) {
      dpes[i] = sDP + dpes[i];
    }

    unProcessAlarm_getCategory(dpes, ddsCategory, exceptionInfo);

    if (dynlen(ddsCategory) > 0) {
        string categories = "";
        dyn_string dpCategories = ddsCategory[1];
        for (int i = 1; i <= dynlen(dpCategories); i++) {
            if (categories != "") {
                categories += ",";
            }
            categories += dpCategories[i];
        }
        dynAppend(dsDpParameters, categories);
    } else {
        dynAppend(dsDpParameters, "");
    }
}

/**Returns if dpe's alarm acknowledgeable

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param dpe name of acknowledgeable's dpe, ProcessInput.PosSt by default
*/
string cpcExportGenericFunctions_getAcknowledgeAlarmValue(string sDP, string dpe = ".ProcessInput.PosSt") {
    int iRes, iAlertType;
    string className;

    iRes = dpGet(sDP + dpe + ":_alert_hdl.._type", iAlertType);
    if ((iRes < 0) || (iAlertType == DPCONFIG_NONE)) {
        return "N";
    } else {
        dpGet(sDP + dpe + ":_alert_hdl.._class", className);
        if (strpos(className, CPC_CONFIG_ALARM_ACK_POSTFIX) >= 0) {
            return "Y";
        } else {
            return "N";
        }
    }
}

/**Add acknowledgeable value to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@todo unify format: rename alarmDP to dpe and move to the end of parameters

@param sDP current dp name
@param alarmDP name of acknowledgeable's dpe
@param dsDpParameters object's output
@param exceptionInfo exception info
*/
void cpcExportGenericFunctions_getAcknowledgeAlarm(string sDP, string alarmDP, dyn_string &dsDpParameters, dyn_string &exceptionInfo) {
    string isAck = cpcExportGenericFunctions_getAcknowledgeAlarmValue(sDP, alarmDP);
    dynAppend(dsDpParameters, isAck);
}

/**Add alarm's threshold values to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param dsDpParameters object's output
*/
void cpcExportGenericFunctions_get5AlertLimits(string sDP, dyn_string &dsDpParameters) {
    int rangesNumber;
    dyn_string dsLimits = makeDynString("", "", "", "");

    dpGet(sDP + ".ProcessInput.PosSt:_alert_hdl.._num_ranges", rangesNumber);
    for (int i = 1; i <= rangesNumber; i++) {
        float value;
        string rangeClass = "#";
        dpGet(sDP + ".ProcessInput.PosSt:_alert_hdl." + i + "._class", rangeClass);
        if (strpos(rangeClass, "_cpcAnalogHH") > 0) {
            dpGet(sDP + ".ProcessInput.PosSt:_alert_hdl." + i + "._l_limit", value);
            dsLimits[1] = value;
        } else if (strpos(rangeClass, "_cpcAnalogH") > 0) {
            dpGet(sDP + ".ProcessInput.PosSt:_alert_hdl." + i + "._l_limit", value);
            dsLimits[2] = value;
        } else if (strpos(rangeClass, "_cpcAnalogLL") > 0) {
            dpGet(sDP + ".ProcessInput.PosSt:_alert_hdl." + i + "._u_limit", value);
            dsLimits[4] = value;
        } else if (strpos(rangeClass, "_cpcAnalogL") > 0) {
            dpGet(sDP + ".ProcessInput.PosSt:_alert_hdl." + i + "._u_limit", value);
            dsLimits[3] = value;
        }
    }

    dynAppend(dsDpParameters, dsLimits[1]);
    dynAppend(dsDpParameters, dsLimits[2]);
    dynAppend(dsDpParameters, dsLimits[3]);
    dynAppend(dsDpParameters, dsLimits[4]);
}

/**Fetch classes of 5-range alert

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@todo return dsClasses instead

@param sDP current dp name
@param dsClasses output
@param exceptionInfo exception info
@param dpe name of dpe with 5-range alert, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_get5AlertClasses(string sDP, dyn_string &dsClasses, dyn_string &exceptionInfo, string dpe = ".ProcessInput.PosSt") {
    int rangesNumber;

    dynClear(dsClasses);

    dpGet(sDP + dpe + ":_alert_hdl.._num_ranges", rangesNumber);
    for (int i = 1; i <= rangesNumber; i++) {
        string rangeClass;
        dpGet(sDP + dpe + ":_alert_hdl." + i + "._class", rangeClass);
        if (rangeClass != "") {
            dynAppend(dsClasses, rangeClass);
        }
    }
}

/**Return is 5-range alert acknowledgeable

If at least one of thresholds contains CPC_CONFIG_ALARM_ACK_POSTFIX in the alert class, alert is acknowledgeable
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param exceptionInfo exception info
*/
string cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(string sDP, dyn_string &exceptionInfo) {
    dyn_string dsClasses;

    cpcExportGenericFunctions_get5AlertClasses(sDP, dsClasses, exceptionInfo);
    if (dynlen(dsClasses) > 0) {
        for (int i = 1; i <= dynlen(dsClasses); i++) {
            if (strpos(dsClasses[i], CPC_CONFIG_ALARM_ACK_POSTFIX) > 0) {
                return "TRUE";
            }
        }
    } 
    return "FALSE";
}

/**Add is 5-range alert acknowledgeable to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@todo unify format: rename alarmDP to dpe and move to the end of parameters

@param sDP current dp name
@param alarmDP name of acknowledgeable's dpe
@param dsDpParameters object's output
@param exceptionInfo exception info
*/
void cpcExportGenericFunctions_get5AlertAcknowledgeAlarm(string sDP, string alarmDP, dyn_string &dsDpParameters, dyn_string &exceptionInfo) {
    string isAck = cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(sDP, alarmDP, exceptionInfo);
    dynAppend(dsDpParameters, isAck);
}

/**Add is alarm active to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDP current dp name
@param dsDpParameters object's output
@param dpe name of alarm dpe, ProcessInput.PosSt by default
*/
void cpcExportGenericFunctions_getAlarmActive(string sDP, dyn_string &dsDpParameters, string dpe = ".ProcessInput.PosSt") {
    bool isAlarmActive;
    dpGet(sDP + dpe + ":_alert_hdl.._active", isAlarmActive);
    dynAppend(dsDpParameters, isAlarmActive);
}

/**Convertion dyn_string into string; elements is glued via separator

Function returns an empty string if input array is empty or string with all elements of array merged with separator.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@todo move to generic place. reuse in other places

@param array data to process
@param separator item's separator
@return result string
*/
string dynJoin(dyn_string array, string separator) {
    if (dynlen(array) == 0) {
        return "";
    } else {
        string result = array[1];
        for (int i = 2; i <= dynlen(array); i++) {
            result = result + separator + array[i];
        }
        return result;
    }
}

/**updates CPC_CONFIG_CONVERSION parameter based on _msg_conv dpe config 

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dp datapoint name
@param dpe datapoint element
@param exceptionInfo for errors
*/
void cpcExportGenericFunctions_updateConversionParameters(string dp, string dpe, dyn_string exceptionInfo){
    bool configExists;
    int conversionType, order;
    dyn_float arguments;
    fwConfigConversion_get(dp + dpe, configExists, DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN, conversionType, order, arguments, exceptionInfo);
    if(configExists){
        if(order==1){
            string sValue = "linear|" + arguments[1] + "|" + arguments[2];
            cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_CONVERSION, sValue, exceptionInfo);
        }
    }
}

void cpcExportGenericFunctions_updateAlarmParameters(string dp, string dpe, string deviceTypeName, dyn_string exceptionInfo){
    int alertType;
    dyn_string alertTexts, alertClass, summaryDpeList, alertPanelParameters, alertLimits;
    string alertPanel, alertHelp, sAlarmAck = "TRUE";
    bool alertActive;
    
    // retrieve alert details
    _fwAlertConfig_get(dp + dpe, alertType, alertTexts, alertLimits, alertClass, summaryDpeList, alertPanel, alertPanelParameters, alertHelp, alertActive, exceptionInfo);
    if (alertType == DPCONFIG_ALERT_NONBINARYSIGNAL) {
		if (deviceTypeName == UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME) {    // for AnalogStatus
            dyn_string dsDpParameters;
            cpcExportGenericFunctions_get5AlertLimits(dp, dsDpParameters);
            
            string sLimits = strjoin(dsDpParameters, "|");
            if(sLimits != "") sLimits = sLimits + "|";
            
            if (dsDpParameters[1] != "" || dsDpParameters[4] != ""){ // alarm Ack should be updated ONLY if there are LL or HH alarms defined, otherwise - keep the original value
                sAlarmAck = cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(dp, exceptionInfo);
            }
            
            if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_VALUES, exceptionInfo) || sLimits != ""){
                cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_VALUES, sLimits, exceptionInfo);
            }
            
		} else if (deviceTypeName == UN_CONFIG_CPC_WORDSTATUS_DPT_NAME){    // for WordStatus
            dyn_string dsAlarmConfigLow, dsAlarmConfigMedium, dsAlarmConfigHigh, dsAlarmConfigSafety;

			string alertLimit;
			for(int i=1; i<=dynlen(alertClass); i++){
                
                dpGet(dp + dpe + ":_alert_hdl." + i + "._match", alertLimit);   // retrieve limits separately, as _fwAlertConfig_get does not handle discrete alarms correctly
				if(strpos(alertClass[i], "cpcP1") >= 0)         dynAppend(dsAlarmConfigSafety, alertLimit);
				else if (strpos(alertClass[i], "cpcP2") >= 0)   dynAppend(dsAlarmConfigHigh, alertLimit);
				else if (strpos(alertClass[i], "cpcP3") >= 0)   dynAppend(dsAlarmConfigMedium, alertLimit);
				else if (strpos(alertClass[i], "cpcP4") >= 0)   dynAppend(dsAlarmConfigLow, alertLimit);
                
                // alarm Ack should be updated ONLY if there are Safety (P1) or High (P2) alarms defined, otherwise - keep the original value
                if (((strpos(alertClass[i], "cpcP1") >= 0) || strpos(alertClass[i], "cpcP2") >= 0 ) && (strpos(alertClass[i], "_Ack") < 0)) {
                    sAlarmAck = "FALSE";
                }
			}
			
            if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_VALUES_LOW, exceptionInfo) || dynlen(dsAlarmConfigLow) > 0){
                cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_VALUES_LOW, strjoin(dsAlarmConfigLow, "|"), exceptionInfo);
            }
            
            if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_VALUES_MEDIUM, exceptionInfo) || dynlen(dsAlarmConfigMedium) > 0){
                cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_VALUES_MEDIUM, strjoin(dsAlarmConfigMedium, "|"), exceptionInfo);
            }
            
            if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_VALUES_HIGH, exceptionInfo) || dynlen(dsAlarmConfigHigh) > 0){
                cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_VALUES_HIGH, strjoin(dsAlarmConfigHigh, "|"), exceptionInfo);
            }
            
            if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_VALUES_SAFETY, exceptionInfo) || dynlen(dsAlarmConfigSafety) > 0){
                cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_VALUES_SAFETY, strjoin(dsAlarmConfigSafety, "|"), exceptionInfo);
            }
		}
		
		dyn_string dsSMSConfig;
        unGenericDpFunctions_getKeyDeviceConfiguration(dp, CPC_CONFIG_SMS_MESSAGE, dsSMSConfig, exceptionInfo);
        string sAlarmMessage = dynlen(dsSMSConfig) > 0 ? dsSMSConfig[1] : "";
        
        if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_MESSAGE, exceptionInfo) || sAlarmMessage != ""){
            cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_MESSAGE, sAlarmMessage, exceptionInfo);
        }
		
        if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_ACK, exceptionInfo) || sAlarmAck != "TRUE") {
            cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_ACK, sAlarmAck, exceptionInfo);
        }
		
        if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_ACTIVE, exceptionInfo) || !alertActive) {
            cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_ACTIVE, alertActive?"true":"false", exceptionInfo, exceptionInfo);
        }
		
        dyn_dyn_string ddsCategories;
        unProcessAlarm_getCategory(makeDynString(dp + dpe), ddsCategories, exceptionInfo);
        
        if(cpcGenericDpFunctions_doesDevicePropertyExist(dp, CPC_CONFIG_ALARM_SMS, exceptionInfo) || dynlen(ddsCategories[1]) > 0) {
            cpcGenericDpFunctions_setDeviceProperty(dp, CPC_CONFIG_ALARM_SMS, strjoin(ddsCategories[1],"|"), exceptionInfo);
        }
    }
   
}
