/**@file

@brief Generic functions to interact with WinCC-OA DB.

@author Alexey Merezhin (EN-ICE-SIC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Creation Date
  20/06/2011

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL

*/


/**Prefix for property fields.

Device may contain special field called PARAMETERS, having a set of key-value.
In WinCC-OA those params will be saved with this prefix (UN_CPC_DEVICE_PROPERTY_PREFIX+key -> value) in the device configuration (available by 'Device configuration' right-click menu's item).

@see cpcConfigGenericFunctions_checkParameters, cpcConfigGenericFunctions_setParameters
**/
const string UN_CPC_DEVICE_PROPERTY_PREFIX = "PROP_";

/**Save the device property for a given property name

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName device name DP name
@param propertyName property name
@param propertyValue property value
@param exceptionInfo error returned here
*/
void cpcGenericDpFunctions_setDeviceProperty(string sDeviceDpName, string propertyName, string propertyValue, dyn_string &exceptionInfo) {
    unGenericDpFunctions_setKeyDeviceConfiguration(sDeviceDpName, UN_CPC_DEVICE_PROPERTY_PREFIX + propertyName, makeDynString(propertyValue), exceptionInfo);
}

/**Get the device property for a given key

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName device name DP name
@param propertyName property name
@param exceptionInfo error returned here
@return propertyValue property value
*/
string cpcGenericDpFunctions_getDeviceProperty(string sDeviceDpName, string propertyName, dyn_string &exceptionInfo, string defaultValue = "") {
    dyn_string dsConfig;
    unGenericDpFunctions_getKeyDeviceConfiguration(sDeviceDpName, UN_CPC_DEVICE_PROPERTY_PREFIX + propertyName, dsConfig, exceptionInfo);
    if (dynlen(dsConfig) == 0) {
        return defaultValue;
    } else {
        return dsConfig[1];
    }
}

/**Check if device property for a given key exist

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName device name DP name
@param propertyName property name
@param exceptionInfo error returned here
@return property existence
*/
bool cpcGenericDpFunctions_doesDevicePropertyExist(string sDeviceDpName, string propertyName, dyn_string &exceptionInfo) {
    dyn_string dsConfig;
    unGenericDpFunctions_getKeyDeviceConfiguration(sDeviceDpName, UN_CPC_DEVICE_PROPERTY_PREFIX + propertyName, dsConfig, exceptionInfo);
    return dynlen(dsConfig) != 0;
}

/**Return the list of associate devices (dp names), formed from MASTER/PARENT/CHILDREN hierarchy's properties

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceDpName device name DP name
@return dp names of hierarchically linked devices
*/
dyn_string cpcConfigGenericFunctions_getAssociatedDevices(string deviceDpName) {
    dyn_string associatedDevices, tmp, exceptionInfo;
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceDpName, CPC_CONFIG_CHILDREN_KEY, tmp, exceptionInfo);
    dynAppend(associatedDevices, tmp);
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceDpName, CPC_CONFIG_MASTER_NAME_KEY, tmp, exceptionInfo);
    dynAppend(associatedDevices, tmp);
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceDpName, CPC_CONFIG_PARENTS_KEY, tmp, exceptionInfo);
    dynAppend(associatedDevices, tmp);

    string deviceSystemName = unGenericDpFunctions_getSystemName(deviceDpName);

    for (int i = 1; i <= dynlen(associatedDevices); i++) {
        associatedDevices[i] = unGenericDpFunctions_dpAliasToName(deviceSystemName + associatedDevices[i]);
    }

    return associatedDevices;
}

/** Return device's key-value mapping for a dpe.
Also see cpcConfigGenericFunctions_setMapping.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceDpName device name DP name
@param dpeName name of dpe
@return comma-separated string of dpe's key=value mapping
*/
string cpcGenericDpFunctions_getMapping(string deviceDpName, string dpeName) {
    dyn_string tmp, exceptionInfo;
    strreplace(dpeName, ".ProcessInput.", "");
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceDpName, CPC_CONFIG_MAPPING + dpeName, tmp, exceptionInfo);
    if (dynlen(tmp) == 0) { dynAppend(tmp, ""); }
    return tmp[1];
}