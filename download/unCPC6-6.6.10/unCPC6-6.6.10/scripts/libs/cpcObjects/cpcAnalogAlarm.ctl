/**@file

// cpcAnalogAlarm.ctl
This library contains the widget, faceplate, etc. functions of AnalogAlarm.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of AnalogAlarm DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_AnalogAlarm_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.HHAlSt", "ProcessInput.LLAlSt", UN_ACKNOWLEDGE_PLC);
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_AnalogAlarm_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.PosSt", value,
          deviceName + ".ProcessInput.PosSt:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.PosSt"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_AnalogAlarm_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_ACK_TEXT, CPC_POPUPMENU_DIAG_INFO_TEXT,
                                            CPC_POPUPMENU_MASK_ALST_TEXT, UN_POPUPMENU_TREND_TEXT, UN_POPUPMENU_BLOCK_PLC_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_AnalogAlarm_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName input, the device name

*/
CPC_AnalogAlarm_WidgetInitStatics(string sDeviceName)
{
  int iRes, iAlertType;
  string sDescription;


  iRes         = dpGet(sDeviceName + ".ProcessInput.HHAlSt:_alert_hdl.._type", iAlertType);
  g_bBoolAlert = ((iRes >= 0) && (iAlertType == DPCONFIG_ALERT_BINARYSIGNAL));

  if( g_sWidgetType == "AA_Analog" )
  {
    g_sPosStFormat = dpGetFormat(sDeviceName + ".ProcessInput.PosSt");
    g_sPosStUnit   = dpGetUnit(sDeviceName + ".ProcessInput.PosSt");
  }

  if( shapeExists("Description") )
  {
    sDescription = dpGetDescription(sDeviceName + ".");
    if( sDescription == sDeviceName + "." )
    {
      sDescription = "N/A";
    }

    setValue("Description", "text", sDescription);
  }

}




/** Returns the list of AnalogAlarm DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogAlarm_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.HHAlSt:_alert_hdl.._active");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogAlarm_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bit32 bit32StsReg02 = values[9];
    bool bStsReg02Invalid = values[10];
    float fPosSt = values[11];
    bool bPosStInvalid =  values[12];
    bool bIsAlarmActive = g_bBoolAlert ? values[13] : false;

    string sBody1Color, sBody1BColor, sWarningColor, sWarningLetter, sColorSelect;
    string sAlarmTypeLetter, sAlarmTypeColor, sAlarmTextLetter, sAlarmTextColor;
    bool bLockVisible;

    // animate body
    if (bStsReg01Invalid || bStsReg02Invalid) {
        sBody1Color = "unDataNotValid";
        sBody1BColor = "";
    } else {
        sBody1Color = "unAlarm_Ok";
        sBody1BColor = "";

        if (getBit(bit32StsReg01, CPC_StsReg01_IST) == 1) {		//	Alarm is active
            sBody1Color  = "cpcColor_Alarm_Bad";
            sBody1BColor = "cpcColor_Alarm_Bad";
        } else if (getBit(bit32StsReg01, CPC_StsReg01_WST) == 1)	{	//	Warning is active
            sBody1Color  = "cpcColor_Alarm_Warning";
        }
        if (getBit(bit32StsReg01, CPC_StsReg01_ALUNACK) == 1) {
            sBody1Color = "cpcColor_Alarm_AlarmNotAck";
            if (getBit(bit32StsReg01, CPC_StsReg01_IST) == 1) {
                sBody1BColor = "cpcColor_Alarm_AlarmNotAck";
            } else if (getBit(bit32StsReg01, CPC_StsReg01_WST) == 1) {
                sBody1Color  = "cpcColor_Alarm_WarningNotAck";
            }
        }
        if (getBit(bit32StsReg01, CPC_StsReg01_MALBRST) == 1) {
            sBody1Color = "unAlarmMasked";
            sBody1BColor = "";
        }
    }

    if (!bIsAlarmActive) {
        sAlarmTextLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
        sAlarmTextColor = "unAlarmMasked";
    }
    if (!bStsReg01Invalid && !bStsReg02Invalid) {
        if (getBit(bit32StsReg01, CPC_StsReg01_MALBRST) == 1) {
            sAlarmTextLetter = CPC_WIDGET_TEXT_ALARM_BLOCKED;
            sAlarmTextColor = "unAlarmMasked";
        }
    }

    cpcGenericObject_WidgetStsRegBitAnimation(bit32StsReg01, bStsReg01Invalid, bit32StsReg02, bStsReg02Invalid,
            makeDynInt(CPC_StsReg02_HWST, CPC_StsReg02_LWST, CPC_StsReg02_HHALST, CPC_StsReg02_LLALST),
            makeDynInt(2,                 2,                 2,                   2),
            makeDynString(CPC_WIDGET_ALARM_HIGH, CPC_WIDGET_ALARM_LOW, CPC_WIDGET_ALARM_HIGHHIGH, CPC_WIDGET_ALARM_LOWLOW),
            makeDynString("cpcColor_Alarm_Warning", "cpcColor_Alarm_Warning", "cpcColor_Alarm_Bad", "cpcColor_Alarm_Bad"),
            sAlarmTypeLetter, sAlarmTypeColor);

    cpcGenericObject_WidgetStsRegBitAnimation(bit32StsReg01, bStsReg01Invalid, bit32StsReg02, bStsReg02Invalid,
            makeDynInt(CPC_StsReg01_CONFIGW, CPC_StsReg01_POSHHW, CPC_StsReg01_POSHW, CPC_StsReg01_POSLW, CPC_StsReg01_POSLLW, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW),
            makeDynInt(1,                    1,                   1,                  1,                  1,                   1,                   1),
            makeDynString(CPC_WIDGET_TEXT_CONFIGW, CPC_WIDGET_TEXT_POSHHW, CPC_WIDGET_TEXT_POSHW, CPC_WIDGET_TEXT_POSLW, CPC_WIDGET_TEXT_POSLLW, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR),
            makeDynString("cpcColor_Widget_Warning", "cpcColor_Widget_Warning", "cpcColor_Widget_Warning", "cpcColor_Widget_Warning", "cpcColor_Widget_Warning", "cpcColor_Widget_Warning", "cpcColor_Widget_Warning"),
            sWarningLetter, sWarningColor);

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue,
            bStsReg01Invalid || bStsReg02Invalid || bPosStInvalid,
            sWarningLetter, sWarningColor);
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sColorSelect, bLockVisible);

    if (g_bSystemConnected) {
        setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "AlarmType", "text", sAlarmTypeLetter, "AlarmType", "foreCol", sAlarmTypeColor,
                      "AlarmText", "text", sAlarmTextLetter, "AlarmText", "foreCol", sAlarmTextColor,
                      "Body1", "fill", "[solid]", "Body1", "foreCol", sBody1Color, "Body1", "backCol", sBody1BColor,
                      "SelectArea", "foreCol", sColorSelect, "LockBmp", "visible", bLockVisible,
                      "WidgetArea", "visible", true, "eventState", "visible", iMask == 0,
                      "commentsButton", "visible", isCommentsActive);
    }
    if (g_bSystemConnected) {
        if (g_sWidgetType == "AA_Analog") {
            cpcGenericObject_WidgetDisplayValueAnimation("Display1", g_sPosStUnit, g_sPosStFormat, fPosSt, sBody1Color, bPosStInvalid);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_AnalogAlarm_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background", "Body1", "fill", "[solid]",
                  "WarningText", "text", "", "WidgetArea", "visible", false, "AlarmType", "text", "", "AlarmText", "text", "");
    if (sWidgetType == "AA_Analog") {
        unGenericObject_WidgetDisplayValueDisconnect(makeDynString("Display1"));
    }
    eventState.visible = false;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_AnalogAlarm_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_SET_AA_LIMITS] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MASK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
    if (cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.HHAlSt") != "N") {
        buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    } else {
        buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_NOONE;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_AnalogAlarm_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_AnalogAlarm_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case CPC_FACEPLATE_BUTTON_SET_AA_LIMITS:
                buttonEnabled = buttonEnabled && bSelected;
                break;
            case UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION:
                buttonEnabled = buttonEnabled && !getBit(stsReg01Value, CPC_StsReg01_AUIHMBST);
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
    setValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION, "text", getBit(stsReg01Value, CPC_StsReg01_MALBRST) ? "Deblock Alarm" : "Block Alarm");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName input, string, The device name

*/
CPC_AnalogAlarm_FaceplateInitStatics(string sDeviceName)
{
  string sSystemName;
  dyn_string dsAlarmTypes, dsParents, exceptionInfo;


  sSystemName = unGenericDpFunctions_getSystemName(sDeviceName);

  g_params["AlAck"]               = cpcExportGenericFunctions_getAcknowledgeAlarmValue(sDeviceName, ".ProcessInput.HHAlSt") != "N";
  g_params["AlarmDelay"]          = cpcGenericDpFunctions_getDeviceProperty(sDeviceName, CPC_PARAMS_ALDT, exceptionInfo);
  g_params["alarmDelayDPe"]       = dpAliasToName(sSystemName + g_params["AlarmDelay"]) + "ProcessInput.PosSt";
  g_params["alarmDelayInhibited"] = dpExists(dpAliasToName(sSystemName + g_params["AlarmDelay"]));
  g_params["alarmDelayFormat"]    = g_params["alarmDelayInhibited"] ? dpGetFormat(g_params["alarmDelayDPe"]) : "%2.1f";
  g_params["alarmDelayUnit"]      = g_params["alarmDelayInhibited"] ? dpGetUnit(g_params["alarmDelayDPe"]) : "s";
  g_params["bAlarmDelayIsValue"]  = g_params["alarmDelayInhibited"] || g_params["AlarmDelay"] == (string)((int) g_params["AlarmDelay"]);
  g_params["PosStFormat"]         = dpGetFormat(sDeviceName + ".ProcessInput.PosSt");
  g_params["PosStUnit"]           = dpGetUnit(sDeviceName + ".ProcessInput.PosSt");
  g_params["MailSMS"] = cpcGenericObject_isInMailSMS(sDeviceName + ".ProcessInput.HHAlSt") ||
                        cpcGenericObject_isInMailSMS(sDeviceName + ".ProcessInput.HWSt")   ||
                        cpcGenericObject_isInMailSMS(sDeviceName + ".ProcessInput.LWSt")   ||
                        cpcGenericObject_isInMailSMS(sDeviceName + ".ProcessInput.LLAlSt");

  unGenericDpFunctions_getKeyDeviceConfiguration(sDeviceName, CPC_CONFIG_TYPE_KEY, dsAlarmTypes, exceptionInfo);

  setMultiValue("AlarmDelayButton",  "enabled", g_params["alarmDelayInhibited"],
                "AlarmDelayButton",  "text",    g_params["alarmDelayInhibited"] ? g_params["AlarmDelay"] : "none",
                "AlarmType.display", "visible", FALSE,
                "AlarmTypeNew",      "visible", FALSE);
  if( dynlen(dsAlarmTypes) > 1 )
  {
    unGenericDpFunctions_getKeyDeviceConfiguration(sDeviceName, CPC_CONFIG_PARENTS_KEY, dsParents, exceptionInfo);
    setMultiValue("AlarmTypeNew", "visible", TRUE,
                  "AlarmTypeNew", "items",   cpcGenericObject_translateMultiAlarmAcronym(dsAlarmTypes, dsParents));
  }
  else
  {
    setMultiValue("AlarmType.display", "visible", TRUE,
                  "AlarmType.display", "text",    cpcGenericObject_translateAlarmAcronym(dsAlarmTypes));
  }

  if( !g_params["alarmDelayInhibited"] )
  {
    cpcGenericObject_DisplayText(g_params["AlarmDelay"], "AlarmDelayText", "unDisplayValue_Parameter", FALSE);
  }

  g_params["hhSource"] = cpcGenericDpFunctions_getDeviceProperty(sDeviceName, "HH_SOURCE", exceptionInfo);
  g_params["hSource"]  = cpcGenericDpFunctions_getDeviceProperty(sDeviceName, "H_SOURCE",  exceptionInfo);
  g_params["lSource"]  = cpcGenericDpFunctions_getDeviceProperty(sDeviceName, "L_SOURCE",  exceptionInfo);
  g_params["llSource"] = cpcGenericDpFunctions_getDeviceProperty(sDeviceName, "LL_SOURCE", exceptionInfo);

}




/** Returns the list of AnalogAlarm DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogAlarm_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.HHSt");
    dynAppend(dpes, deviceName + ".ProcessInput.HHSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.HSt");
    dynAppend(dpes, deviceName + ".ProcessInput.HSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.LSt");
    dynAppend(dpes, deviceName + ".ProcessInput.LSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.LLSt");
    dynAppend(dpes, deviceName + ".ProcessInput.LLSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.HHAlSt:_alert_hdl.._act_state");
    dynAppend(dpes, deviceName + ".ProcessInput.LLAlSt:_alert_hdl.._act_state");
    if (g_params["alarmDelayInhibited"]) {
        dynAppend(dpes, g_params["alarmDelayDPe"]);
        dynAppend(dpes, g_params["alarmDelayDPe"] + ":_online.._invalid");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogAlarm_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    int stsReg01, stsReg02;
    bool stsReg01Invalid, stsReg02Invalid;
    unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "StsReg01", stsReg01, stsReg01Invalid);
    unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "StsReg02", stsReg02, stsReg02Invalid);

    unFaceplate_animateAnalogValue(dpes, values, "PosSt", "PosSt", "cpcColor_Faceplate_Status");
    if (mappingHasKey(g_params, "hhSource") && g_params["hhSource"] == "disabled") {
        unFaceplate_animateText("HHSt", "", "unDisplayValue_Parameter");
    } else {
        unFaceplate_animateAnalogValue(dpes, values, "HHSt", "PosSt", "unDisplayValue_Parameter");
    }
    if (mappingHasKey(g_params, "hSource") && g_params["hSource"] == "disabled") {
        unFaceplate_animateText("HSt", "", "unDisplayValue_Parameter");
    } else {
        unFaceplate_animateAnalogValue(dpes, values, "HSt", "PosSt", "unDisplayValue_Parameter");
    }
    if (mappingHasKey(g_params, "lSource") && g_params["lSource"] == "disabled") {
        unFaceplate_animateText("LSt", "", "unDisplayValue_Parameter");
    } else {
        unFaceplate_animateAnalogValue(dpes, values, "LSt", "PosSt", "unDisplayValue_Parameter");
    }
    if (mappingHasKey(g_params, "llSource") && g_params["llSource"] == "disabled") {
        unFaceplate_animateText("LLSt", "", "unDisplayValue_Parameter");
    } else {
        unFaceplate_animateAnalogValue(dpes, values, "LLSt", "PosSt", "unDisplayValue_Parameter");
    }

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUIHMBST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ARMRCPST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_EHHST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_EHST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_ELST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_ELLST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_IHMHHST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_IHMHST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_IHMLST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_IHMLLST", true, "cpcColor_Faceplate_Status");

    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg02_HHALST",    "HHAlSt",  "CPC_StsReg01_ALUNACK");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_HWST", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_LWST", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg02_LLALST",    "LLAlSt",  "CPC_StsReg01_ALUNACK");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ALUNACK", true, "cpcColor_Alarm_Bad");
    if (mappingHasKey(g_params, "MailSMS")) {
        cpcGenericObject_CheckboxAnimate("MailSMS", g_params["MailSMS"]);
    }

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_POSHHW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_POSHW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_POSLW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_POSLLW", true, "cpcColor_Faceplate_Warning");

    cpcGenericObject_animateMaskEvent(dpes, values);
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MALBRST", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_CONFIGW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");

    // animate alarm delay
    anytype alarmDelay;
    if (dynlen(dpes) > 0) {
        if (mappingHasKey(g_params, "alarmDelayInhibited") && g_params["alarmDelayInhibited"] && mappingHasKey(g_params, "alarmDelayDPe")) {
            alarmDelay = unFaceplate_fetchAnimationCBValue(dpes, values, g_params["alarmDelayDPe"]);
            if (g_bSystemConnected && mappingHasKey(g_params, "alarmDelayFormat") && mappingHasKey(g_params, "alarmDelayUnit")) {
                unGenericObject_DisplayValue(g_params["alarmDelayFormat"], g_params["alarmDelayUnit"], alarmDelay, "AlarmDelayText", "unDisplayValue_Parameter", unFaceplate_connectionValid(values));
            }
        }
    } else {
        unGenericObject_DisplayValueDisconnect(makeDynString("AlarmDelayText"));
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}
