/**@file

// cpcAnalogParameter.ctl
This library contains the widget, faceplate, etc. functions of AnalogParameter.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_AnalogParameter_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.PosSt", value,
          deviceName + ".ProcessInput.PosSt:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.PosSt"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_AnalogParameter_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_MASK_POSST_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_AnalogParameter_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_AnalogParameter_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    g_sMPosRStFormat = dpGetFormat(deviceName + ".ProcessInput.MPosRSt");
    g_sMPosRStUnit = dpGetUnit(deviceName + ".ProcessInput.MPosRSt");
    g_sPosStFormat = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_sPosStUnit = dpGetUnit(deviceName + ".ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of AnalogParameter DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogParameter_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogParameter_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    float fMPosRSt = values[7];
    bool bMPosRStInvalid = values[8];
    float fPosSt = values[9];
    bool bPosStInvalid = values[10];

    string sBody1Text, sBody1Color, sDisplay1Text, ssDisplay1Color, sColorSelect;
    string sFormattedValue, sFormattedValueBody, sWarningLetter, sWarningColor;
    bool bLockVisible;

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue, bMPosRStInvalid || bPosStInvalid, sWarningLetter, sWarningColor);

    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sColorSelect, bLockVisible);
    if (g_bSystemConnected) {
        setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "SelectArea", "foreCol", sColorSelect, "LockBmp", "visible", bLockVisible,
                      "WidgetArea", "visible", true, "eventState", "visible", iMask == 0,
                      "commentsButton", "visible", isCommentsActive);
    }
    cpcGenericObject_WidgetDisplayValueAnimation("Body1", g_sMPosRStUnit, g_sMPosRStFormat, fMPosRSt, "cpcColor_Widget_Request", bMPosRStInvalid);
    cpcGenericObject_WidgetDisplayValueAnimation("Display1", g_sPosStUnit, g_sPosStFormat, fPosSt, "cpcColor_Widget_Status", bPosStInvalid);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_AnalogParameter_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    setMultiValue("Body1", "foreCol", "unDataNoAccess",
                  "Display1", "foreCol", "unDataNoAccess",
                  "WarningText", "text", "",
                  "SelectArea", "foreCol", "", "LockBmp", "visible", false, "WidgetArea", "visible", false);
    eventState.visible	= false;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_AnalogParameter_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_PARAMETER_SET_VALUE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_AnalogParameter_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_AnalogParameter_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    g_params["PosStFormat"] = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_params["PosStUnit"] = dpGetUnit(deviceName + ".ProcessInput.PosSt");
    g_params["MPosRStFormat"] = dpGetFormat(deviceName + ".ProcessInput.MPosRSt");
    g_params["MPosRStUnit"] = dpGetUnit(deviceName + ".ProcessInput.MPosRSt");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of AnalogParameter DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogParameter_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_pv_range.._min");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_pv_range.._max");
    dynAppend(dpes, deviceName + ".ProcessOutput.DfltVal");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogParameter_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    if (mappingHasKey(g_params, "PosStUnit") && mappingHasKey(g_params, "PosStFormat")) {
        unFaceplate_animateOnlineValue(dpes, values, "PosSt", g_params["PosStUnit"], g_params["PosStFormat"], "cpcColor_Faceplate_Status");
        unFaceplate_animateOnlineValue(dpes, values, "DfltVal", g_params["PosStUnit"], g_params["PosStFormat"], "unDisplayValue_Parameter");
        unFaceplate_animateRange(dpes, values, "PosSt", DPCONFIG_MINMAX_PVSS_RANGECHECK, g_params["PosStUnit"], g_params["PosStFormat"]);
    }
    if (mappingHasKey(g_params, "MPosRStUnit") && mappingHasKey(g_params, "MPosRStFormat")) {
        unFaceplate_animateOnlineValue(dpes, values, "MPosRSt", g_params["MPosRStUnit"], g_params["MPosRStFormat"], "cpcColor_Faceplate_Request");
    }

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ARMRCPST", true, "cpcColor_Faceplate_Status");
    cpcGenericObject_animateMaskEvent(dpes, values);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}