/**@file

// cpcAnalogStatusConfig.ctl
This library contains the import and export function of the CPC_AnalogStatus.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME = "CPC_AnalogStatus";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_LENGTH                        = 9;

const unsigned UN_CONFIG_CPC_ANALOGSTATUS_UNIT                          = 1;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_FORMAT                        = 2;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_DEADBAND                      = 3;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_DEADBAND_TYPE                 = 4;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_RANGEMAX                      = 5;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_RANGEMIN                      = 6;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ARCHIVE_ACTIVE                = 7;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ARCHIVE_TIME_FILTER           = 8;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDRESS_POSST                 = 9;

const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_LENGTH          = 6;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_PARAMETERS      = 1;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_MASTER_NAME     = 2;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_PARENTS         = 3;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_CHILDREN        = 4;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_TYPE            = 5;
const unsigned UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_SECOND_ALIAS    = 6;

// SOFT_FE constants
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_UNIT                 = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_FORMAT               = 2;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_RANGEMAX             = 3;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_RANGEMIN             = 4;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_ARCHIVE_ACTIVE       = 5;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_ARCHIVE_TIME_FILTER  = 6;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_LENGTH               = 6;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_AnalogStatusConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dataType"] 		= CPC_FLOAT;
    config["PosSt"] 		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_AnalogStatusConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_ANALOGSTATUS_LENGTH;
  mTemp["UNIT"]                    = UN_CONFIG_CPC_ANALOGSTATUS_UNIT;
  mTemp["FORMAT"]                  = UN_CONFIG_CPC_ANALOGSTATUS_FORMAT;
  mTemp["DEADBAND"]                = UN_CONFIG_CPC_ANALOGSTATUS_DEADBAND;
  mTemp["DEADBAND_TYPE"]           = UN_CONFIG_CPC_ANALOGSTATUS_DEADBAND_TYPE;
  mTemp["RANGEMAX"]                = UN_CONFIG_CPC_ANALOGSTATUS_RANGEMAX;
  mTemp["RANGEMIN"]                = UN_CONFIG_CPC_ANALOGSTATUS_RANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_ANALOGSTATUS_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_ANALOGSTATUS_ARCHIVE_TIME_FILTER;
  mTemp["ADDRESS_POSST"]           = UN_CONFIG_CPC_ANALOGSTATUS_ADDRESS_POSST;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_ANALOGSTATUS_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_ANALOGSTATUS"] = mTemp;


  mappingClear(mTemp);
  mTemp["UNIT"]                = UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_UNIT;
  mTemp["FORMAT"]              = UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_FORMAT;
  mTemp["RANGEMAX"]            = UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_RANGEMAX;
  mTemp["RANGEMIN"]            = UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_RANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]      = UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"] = UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_ARCHIVE_TIME_FILTER;
  mTemp["LENGTH"]              = UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS_LENGTH;

  g_mCpcConst["UN_CONFIG_SOFT_FE_CPC_ANALOGSTATUS"] = mTemp;
}





/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
CPC_AnalogStatusConfig_setCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {

    string sDp = configLine[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt";

    // message conversion AND alarm processing should be included in the importation line, but adding them would break importation backwards comaptibility
    // code below is a hack

    // message conversion
    string sConversion = cpcGenericDpFunctions_getDeviceProperty(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_CONVERSION, exceptionInfo, "");
    cpcConfigGenericFunctions_setMsgConv(sConversion, makeDynString(sDp), exceptionInfo);

    // alarms

    // configure alarm
    string sAlarmConfig = cpcGenericDpFunctions_getDeviceProperty(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_VALUES, exceptionInfo, "||||");

    dyn_string dsAlarmConfig = strsplit(sAlarmConfig, "|");
    dyn_int diLimits = makeDynInt(1, 2, 3, 4);
    dyn_string dsFakeConfigs;   // create "fake" importation line with fields needed by cpcConfigGenericFunctions_set5RangesAlert
    dsFakeConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[1]] = dsAlarmConfig[4]; // LL
    dsFakeConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[2]] = dsAlarmConfig[3]; // L
    dsFakeConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[3]] = dsAlarmConfig[2]; // H
    dsFakeConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[4]] = dsAlarmConfig[1]; // HH

    dsFakeConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN] = configLine[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN];
    dsFakeConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE] = configLine[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE];
    dsFakeConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS] = configLine[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS];
    dsFakeConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION] = configLine[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION];
    dsFakeConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL] = configLine[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL];

	// alarm active
    string sAlarmActive = cpcGenericDpFunctions_getDeviceProperty(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_ACTIVE, exceptionInfo, "true");
    bool bAlarmActive = strtolower(sAlarmActive) == "true";

    // alarm ack
    string sAlarmAck = cpcGenericDpFunctions_getDeviceProperty(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_ACK, exceptionInfo, "true");
    bool bAlarmAck = strtolower(sAlarmAck) == "true";

    cpcConfigGenericFunctions_set5RangesAlert(dsFakeConfigs, sDp, ""/* description */, bAlarmAck, bAlarmActive, true/* bSMS */ , diLimits, exceptionInfo);

    // sms categories
    string sAlarmSMS = cpcGenericDpFunctions_getDeviceProperty(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_SMS, exceptionInfo, "");
    dyn_string categories = strsplit(sAlarmSMS, "|");
    bool hasSMS = cpcConfigGenericFunctions_alertWithMail(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], "PosSt");
    if(hasSMS) {
        unProcessAlarm_removeDpFromList(sDp, exceptionInfo);
        if(dynlen(categories) > 0){
            unProcessAlarm_createCategory(categories, "", makeDynString(), exceptionInfo);
            for (int i = 1; i <= dynlen(categories); i++) {
                unProcessAlarm_addDpToList(sDp, categories[i], exceptionInfo);
            }
        }
    }

    // sms message
    string sAlarmSMSMessage = cpcGenericDpFunctions_getDeviceProperty(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_MESSAGE, exceptionInfo, "");
    unGenericDpFunctions_setKeyDeviceConfiguration(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_SMS_MESSAGE, makeDynString(sAlarmSMSMessage), exceptionInfo);

//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/** Check custom configuration
*/
CPC_AnalogStatusConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    mapping mParameters = cpcConfigGenericFunctions_getParametersMapping(configLine, UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME, hasArchive);
    string sConversion = mappingHasKey(mParameters, CPC_CONFIG_CONVERSION) ? mParameters[CPC_CONFIG_CONVERSION] : "";

    // check message conversion
    cpcConfigGenericFunctions_checkMsgConv(sConversion, exceptionInfo);

    // check 5rangesAlert
	string sAlarmConfig = mappingHasKey(mParameters, CPC_CONFIG_ALARM_VALUES) ? mParameters[CPC_CONFIG_ALARM_VALUES] : "||||";

	dyn_string dsAlarmConfig = strsplit(sAlarmConfig, "|");
	if(dynlen(dsAlarmConfig) != 4){
		fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_checkCustomConfig: wrong number of alert limits: " + dynlen(dsAlarmConfig) + " instead of 4.", "");
		return;
	} else {
		dyn_int diLimits = makeDynInt(1, 2, 3, 4);
		dyn_string dsFakeConfigs;   // create "fake" importation line with fields needed by cpcConfigGenericFunctions_check5RangesAlert
		dsFakeConfigs[UN_CONFIG_COMMON_LENGTH + 0]           = mappingHasKey(mParameters, CPC_CONFIG_ALARM_ACTIVE) ? mParameters[CPC_CONFIG_ALARM_ACTIVE] : "true";
		dsFakeConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[1]] = dsAlarmConfig[4];    // LL
		dsFakeConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[2]] = dsAlarmConfig[3];    // L
		dsFakeConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[3]] = dsAlarmConfig[2];    // H
		dsFakeConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[4]] = dsAlarmConfig[1];    // HH
		cpcConfigGenericFunctions_check5RangesAlert(dsFakeConfigs, 0, diLimits, exceptionInfo);
	}

    bool bCorrectValue;
    string sAlarmAck = mappingHasKey(mParameters, CPC_CONFIG_ALARM_ACK) ? mParameters[CPC_CONFIG_ALARM_ACK] : "true";
    cpcConfigGenericFunctions_checkBool("CPC_CONFIG_ALARM_ACK", sAlarmAck, bCorrectValue, exceptionInfo);

    string sAlarmSMS = mappingHasKey(mParameters, CPC_CONFIG_ALARM_SMS) ? mParameters[CPC_CONFIG_ALARM_SMS] : "";
    if(sAlarmSMS != "") {
        dyn_string categories = strsplit(sAlarmSMS, "|");
        unProcessAlarm_checkCategory(categories, exceptionInfo);
    }

    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_checkCustomConfig: " + exceptionInfo + " " + getCatStr("unGeneration", "BADDATA"), "");
        return;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/**
Purpose: Export CPC_AnalogStatus Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_AnalogStatusConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");
    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");
    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_ExportConfig() -> Error exporting deadbands, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_ExportConfig() -> Error exporting ranges, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt", TRUE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    // Get all the alarm parameters updated
    cpcExportGenericFunctions_updateAlarmParameters(sCurrentDp, ".ProcessInput.PosSt", UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_ExportConfig() -> Error exporting alarm parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    // Update msg_conv parameter before export
    cpcExportGenericFunctions_updateConversionParameters(sCurrentDp, ".ProcessInput.PosSt", exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_ExportConfig() -> Error exporting conversion parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogStatusConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_AnalogStatusConfig_ExportConfig()





/**
Purpose: Export CPC_AnalogStatus Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_AnalogStatus Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

CMW_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

SNMP_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

DIP_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}


void IEC104_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_AnalogStatus_ExportConfig() -> Error exporting CPC_AnalogStatus device, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_AnalogStatus_ExportConfig()


/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
void SOFT_FE_CPC_AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    string sObject = UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME;
    int amount = dynlen(dsDpList);
    for (int i = 1; i <= amount; i++) {
        dyn_string dsDpParameters;
        string currentDP = dsDpList[i];

        if (!dpExists(currentDP)) { continue; }

        unExportDevice_getAllCommonParameters(currentDP, sObject, dsDpParameters);
        cpcExportGenericFunctions_getUnit(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getFormat(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getRange(currentDP, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getArchive(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_1, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_2, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_3, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT), dsDpParameters);

        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions


//@}
