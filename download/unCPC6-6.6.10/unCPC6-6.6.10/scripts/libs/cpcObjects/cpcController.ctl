/**@file

// cpcController.ctl
This library contains the widget, faceplate, etc. functions of Controller.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_Controller_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.OutOVSt", value,
          deviceName + ".ProcessInput.OutOVSt:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.OutOVSt"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.OutOVSt"); // formated value with unit
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_Controller_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_Controller_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}


/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_Controller_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    dyn_string exceptionInfo;
    unGenericDpFunctions_getParameters(deviceName, g_dsParameters);
    g_sMVUnit = dpGetUnit(deviceName + ".ProcessInput.MV");
    g_sDescription = unGenericDpFunctions_getDescription(deviceName);
    g_sOutOVStUnit = dpGetUnit(deviceName + ".ProcessInput.OutOVSt");

    if (g_sWidgetType == "ControllerSetPoint") {
        g_sActSPFormat = dpGetFormat(deviceName + ".ProcessInput.ActSP");
        g_sActSPUnit = dpGetUnit(deviceName + ".ProcessInput.ActSP");
    }

    if (g_sWidgetType == "ControllerActiveX") {
        g_sActSPFormat = dpGetFormat(deviceName + ".ProcessInput.ActSP");
        g_sOutOVStFormat = dpGetFormat(deviceName + ".ProcessInput.OutOVSt");
        if (!g_bBargraphInitialised) {
            g_bBargraphInitialised = true;
            _CPC_Controller_initBargraph("TRUE");
        }
        string scalingMethod;
        unGenericDpFunctions_getKeyDeviceConfiguration(deviceName, UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD_KEY, scalingMethod, exceptionInfo);
        if (scalingMethod == "1") {
            ScalingMethod.text() = "I Scaling";
        } else if (scalingMethod == "2") {
            ScalingMethod.text() = "I/O Scaling";
        } else if (scalingMethod == "3") {
            ScalingMethod.text() = "No Scaling";
        } else {
            ScalingMethod.text() = "";
        }
    }
    int iRangeType1, iRangeType2;
    int iRes = dpGet(deviceName + ".ProcessInput.ActSP:_pv_range.._type", iRangeType1,
                     deviceName + ".ProcessInput.OutOVSt:_pv_range.._type", iRangeType2);
    g_iRangeTypeSp = (iRes >= 0) && (iRangeType1 == DPCONFIG_MINMAX_PVSS_RANGECHECK) ? DPCONFIG_MINMAX_PVSS_RANGECHECK : DPCONFIG_NONE;
    g_iRangeTypeOut = (iRes >= 0) && (iRangeType2 == DPCONFIG_MINMAX_PVSS_RANGECHECK) ? DPCONFIG_MINMAX_PVSS_RANGECHECK : DPCONFIG_NONE;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of Controller DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_Controller_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuSPSt"); // Setpoint
    dynAppend(dpes, deviceName + ".ProcessInput.AuSPSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MSPSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MSPSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSP");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSP:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPH");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPH:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPL");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPL:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuPosRSt"); // Output
    dynAppend(dpes, deviceName + ".ProcessInput.AuPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt");
    dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutH");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutH:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutL");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutL:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActKc"); // PID parameters
    dynAppend(dpes, deviceName + ".ProcessInput.ActKc:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTi");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTi:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTd");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTd:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTds");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTds:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MV");
    dynAppend(dpes, deviceName + ".ProcessInput.MV:_online.._invalid");
    dynAppend(dpes, deviceName + ".AutoTunning.Relay.Active");
    dynAppend(dpes, deviceName + ".AutoTunning.IFT.Active");
    dynAppend(dpes, deviceName + ".AutoTunning.SIMC.Active");

    if (g_iRangeTypeSp == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        dynAppend(dpes, deviceName + ".ProcessInput.ActSP:_pv_range.._max");
        dynAppend(dpes, deviceName + ".ProcessInput.ActSP:_pv_range.._min");
    }
    if (g_iRangeTypeOut == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt:_pv_range.._max");
        dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt:_pv_range.._min");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_Controller_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bit32 bit32StsReg02 = values[9];
    bool bStsReg02Invalid = values[10];
    float fAuSPSt = values[11];
    bool bAuSPStInvalid = values[12];
    float fMSPSt = values[13];
    bool bMSPStInvalid = values[14];
    float fActSP = values[15];
    bool bActSPInvalid = values[16];
    float fActSPH = values[17];
    bool bActSPHInvalid = values[18];
    float fActSPL = values[19];
    bool bActSPLInvalid = values[20];
    float fAuPosRSt = values[21];
    bool bAuPosRStInvalid = values[22];
    float fMPosRSt = values[23];
    bool bMPosRStInvalid = values[24];
    float fOutOVSt = values[25];
    bool bOutOVStInvalid = values[26];
    float fAtOutH = values[27];
    bool bAtOuOHInvalid = values[28];
    float fAtOutL = values[29];
    bool bAtOuOLInvalid = values[30];
    float fActKc = values[31];
    bool bActKcInvalid = values[32];
    float fActTi = values[33];
    bool bActTiInvalid = values[34];
    float fActTd = values[35];
    bool bActTdInvalid = values[36];
    float fActTds = values[37];
    bool bActTdsInvalid = values[38];
    float fMV = values[39];
    bool bMVInvalid = values[40];
    bool ActiveIFT = values[41];
    bool ActiveSIMC = values[42];
    bool ActiveRelay = values[43];
    float maxActSP, minActSP, maxOut, minOut;
    int idx = 44;
    if (g_iRangeTypeSp == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        maxActSP = values[idx++];
        minActSP = values[idx++];
    }
    if (g_iRangeTypeOut == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        maxOut = values[idx++];
        minOut = values[idx++];
    }

// 1. Generic animation
    cpcGenericObject_WidgetAnimation(g_sWidgetType, UN_CONFIG_CPC_CONTROLLER_DPT_NAME, bLocked, sSelectedManager, bit32StsReg01,
                                     makeDynInt(CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW),
                                     makeDynString(CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR), "", "",
                                     makeDynString("CPC_StsReg01_FOMOST", "CPC_StsReg01_MMOST", "CPC_StsReg01_SOFTLDST", "CPC_StsReg01_AUINHFMO", "CPC_StsReg01_AUINHMMO"),
                                     "", "", -1,
                                     bStsReg01Invalid || bStsReg02Invalid || bAuSPStInvalid || bMSPStInvalid || bActSPInvalid || bActSPHInvalid || bActSPLInvalid || bAuPosRStInvalid || bMPosRStInvalid || bOutOVStInvalid || bAtOuOHInvalid || bAtOuOLInvalid || bActKcInvalid || bActTiInvalid || bActTdInvalid || bActTdsInvalid,
                                     bStsReg01Invalid || bStsReg02Invalid,
                                     iSystemIntegrityAlarmValue, bSystemIntegrityAlarmEnabled);

    string letter, color;
    if (!bStsReg01Invalid && !bStsReg02Invalid) {
        unGenericObject_WidgetControlStateAnimation(bit32StsReg01, makeDynInt(CPC_StsReg01_REGST, CPC_StsReg01_TRST, CPC_StsReg01_OUTPST), makeDynString("R", "T", "P"), letter, color);
    }
    if (g_bSystemConnected) {
        setMultiValue("ControlWorkingModeText", "text", letter, "ControlWorkingModeText", "foreCol", color);
    }

    if (g_sWidgetType == "ControllerSetPoint") {
        setMultiValue("Body3", "text", cpcGenericObject_FormatValueWithUnit(fActSP, g_sActSPFormat, g_sActSPUnit));
    }

// 2. ActiveX animation
    if (g_sWidgetType == "ControllerActiveX") {
        string pidName;
        bool bInvalid;

        bInvalid = bStsReg01Invalid || bStsReg02Invalid || bAuSPStInvalid || bMSPStInvalid || bActSPInvalid || bActSPHInvalid || bActSPLInvalid || bAuPosRStInvalid || bMPosRStInvalid || bOutOVStInvalid || bAtOuOHInvalid || bAtOuOLInvalid || bActKcInvalid || bActTiInvalid || bActTdInvalid || bActTdsInvalid;

        // Color
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Color_MV", "cpcColor_Widget_Status");
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Color_OutO", "cpcColor_Widget_Request");
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Color_SP", "cpcColor_Widget_Controller_SP");
        }

        if (bInvalid) {
            if (g_bSystemConnected) {
                CPC_Controller_setProperty("BackColor", "unDataNotValid");
            }
        } else {
            if (g_bSystemConnected) {
                CPC_Controller_setProperty("BackColor", "unSynopticBackground");
            }
        }

        pidName = g_dsParameters[UN_PARAMETER_NAME];
        if (pidName == "") {
            pidName = "PID";
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("PIDName", pidName);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Description", g_sDescription);
        }

        // MV
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("MV_Unit", g_sMVUnit);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("MV", fMV);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("MVLabel", unGenericObject_FormatValue(g_sMVFormat, fMV));
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("MV_MaxRange", maxActSP);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("MV_MinRange", minActSP);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Out_MaxRange", maxOut);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Out_MinRange", minOut);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("LabelMV_MaxRange", unGenericObject_FormatValue(g_sActSPFormat, maxActSP));
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("LabelMV_MinRange", unGenericObject_FormatValue(g_sActSPFormat, minActSP));
        }
        // Setpoint active
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("SP", fActSP);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("SPLabel", unGenericObject_FormatValue(g_sActSPFormat, fActSP));
        }

        // Setpoint high limit
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("SP_LimitHigh", fActSPH);
        }

        // Setpoint low limit
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("SP_LimitLow", fActSPL);
        }

        // Output active
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("OutO", fOutOVSt);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("OutOLabel", unGenericObject_FormatValue(g_sOutOVStFormat, fOutOVSt));
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("OutO_Unit", g_sOutOVStUnit);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("LabelMaxO", unGenericObject_FormatValue(g_sOutOVStFormat, 100) + " " + g_sOutOVStUnit);
        }
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("LabelMinO", unGenericObject_FormatValue(g_sOutOVStFormat, 0) + " " + g_sOutOVStUnit);
        }

        // Output high limit
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("OutO_LimitHigh", fAtOutH);
        }

        // Output low limit
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("OutO_LimitLow", fAtOutL);
        }

        // Operation mode
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Operation_Mode", bit32StsReg01);
        }

        // Update
        if (g_bSystemConnected) {
            CPC_Controller_setProperty("Update", "");
        }

        // set O/N letter
        if (bInvalid) {
            if (g_bSystemConnected) {
                WarningText.text = CPC_WIDGET_TEXT_INVALID;
            }
        } else if (cpcGenericObject_SystemInvalid(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue)) {
            if (g_bSystemConnected) {
                WarningText.text = CPC_WIDGET_TEXT_OLD_DATA;
            }
        }

        WarningText.visible = cpcGenericObject_SystemInvalid(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue) || bInvalid;
    }

    if (g_bSystemConnected) {
        eventState.visible = iMask == 0;
        commentsButton.visible = isCommentsActive;
    }
    
    if (ActiveIFT || ActiveSIMC || ActiveRelay) 
    {
       setValue("Body2", "foreCol", "cpcColor_Alarm_OK_NotAck");
       setValue("Body1", "foreCol", "cpcColor_Alarm_OK_NotAck");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_Controller_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    cpcGenericObject_WidgetDisconnection(sWidgetType, UN_CONFIG_CPC_CONTROLLER_DPT_NAME);
    if (sWidgetType == "ControllerActiveX") {
        // Color
        CPC_Controller_setProperty("Color_MV", "unDataNoAccess");
        CPC_Controller_setProperty("Color_OutO", "unDataNoAccess");
        CPC_Controller_setProperty("Color_SP", "unDataNoAccess");
        CPC_Controller_setProperty("BackColor", "unDataNoAccess");
        WarningText.visible(false);
    }

    // set event state
    if (shapeExists("eventState")) {
        eventState.visible = false;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_Controller_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_AUTO_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MANUAL_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_SETPOINT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_PID_PARAMETERS] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_SET_PID_LIMITS] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_SETOUTPUT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_REGULATION_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_OUTPUTPOSITIONING_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_FORCED_MODE] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[CPC_FACEPLACE_BUTTON_AUTO_TUNE] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_Controller_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_Controller_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);
    
    bool ActiveIFT, ActiveSIMC, ActiveRelay;
    dpGet(unGenericDpFunctions_getDpName(deviceName)+".AutoTunning.Relay.Active",ActiveRelay);
    dpGet(unGenericDpFunctions_getDpName(deviceName)+".AutoTunning.IFT.Active",ActiveIFT);
    dpGet(unGenericDpFunctions_getDpName(deviceName)+".AutoTunning.SIMC.Active",ActiveSIMC);
    
    

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];

    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_AUTO_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !ActiveRelay &&
                                !ActiveIFT &&
                                !ActiveSIMC &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_SOFTLDST) &&
                                !bStsReg01Bad;
                break;
            case UN_FACEPLATE_BUTTON_MANUAL_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !ActiveRelay &&
                                !ActiveIFT &&
                                !ActiveSIMC &&
                                !getBit(stsReg01Value, CPC_StsReg01_MMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHMMO) &&
                                !getBit(stsReg01Value, CPC_StsReg01_SOFTLDST) &&
                                !bStsReg01Bad;
                break;
            case UN_FACEPLATE_BUTTON_FORCED_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !ActiveRelay &&
                                !ActiveIFT &&
                                !ActiveSIMC &&
                                !getBit(stsReg01Value, CPC_StsReg01_FOMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHFMO) &&
                                !bStsReg01Bad;
                break;
            case UN_FACEPLATE_BUTTON_SETPOINT:
                buttonEnabled = buttonEnabled && bSelected &&
                                !ActiveRelay &&
                                !ActiveIFT &&
                                !ActiveSIMC &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUESPO) &&
                                !getBit(stsReg01Value, CPC_StsReg01_SOFTLDST) &&
                                !bStsReg01Bad;
                break;
            case UN_FACEPLATE_BUTTON_PID_PARAMETERS:
            case UN_FACEPLATE_BUTTON_SET_PID_LIMITS:
                buttonEnabled = buttonEnabled && bSelected &&
                                !ActiveRelay &&
                                !ActiveIFT &&
                                !ActiveSIMC &&
                                !getBit(stsReg01Value, CPC_StsReg01_SOFTLDST) &&
                                !bStsReg01Bad;
                break;
            case CPC_FACEPLATE_BUTTON_OUTPUTPOSITIONING_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !ActiveRelay &&
                                !ActiveIFT &&
                                !ActiveSIMC &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_OUTPST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_TRST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_SOFTLDST) &&
                                !bStsReg01Bad;
                break;
            case UN_FACEPLATE_BUTTON_REGULATION_MODE:
            case UN_FACEPLATE_BUTTON_SETOUTPUT:
                buttonEnabled = buttonEnabled && bSelected &&
                                !ActiveRelay &&
                                !ActiveIFT &&
                                !ActiveSIMC &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_REGST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_TRST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_SOFTLDST) &&
                                !bStsReg01Bad;
                break;
            case CPC_FACEPLACE_BUTTON_AUTO_TUNE:
                buttonEnabled = buttonEnabled && bSelected;
                dpSet(deviceName + ".AutoTunning.Access", buttonEnabled);
                break;
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_Controller_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    if (!g_bBargraphInitialised) {
        g_bBargraphInitialised = true;
        _CPC_Controller_initBargraph("FALSE");
    }

    int iRes, iRangeType1, iRangeType2;
    dyn_string exceptionInfo;
    string scalingMethod;
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceName, UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD_KEY, scalingMethod, exceptionInfo);
    if (scalingMethod == "1") {
        ScalingMethod.text() = "I Scaling";
    } else if (scalingMethod == "2") {
        ScalingMethod.text() = "I/O Scaling";
    } else if (scalingMethod == "3") {
        ScalingMethod.text() = "No Scaling";
    } else {
        ScalingMethod.text() = "";
    }

    g_sAuSPStFormat = dpGetFormat(deviceName + ".ProcessInput.AuSPSt");
    g_sAuSPStUnit = dpGetUnit(deviceName + ".ProcessInput.AuSPSt");
    g_sMSPStFormat = dpGetFormat(deviceName + ".ProcessInput.MSPSt");
    g_sMSPStUnit = dpGetUnit(deviceName + ".ProcessInput.MSPSt");
    g_sActSPFormat = dpGetFormat(deviceName + ".ProcessInput.ActSP");
    g_sActSPUnit = dpGetUnit(deviceName + ".ProcessInput.ActSP");
    g_sActSPHFormat = dpGetFormat(deviceName + ".ProcessInput.ActSPH");
    g_sActSPHUnit = dpGetUnit(deviceName + ".ProcessInput.ActSPH");
    g_sActSPLFormat = dpGetFormat(deviceName + ".ProcessInput.ActSPL");
    g_sActSPLUnit = dpGetUnit(deviceName + ".ProcessInput.ActSPL");
    g_sAuPosRStFormat = dpGetFormat(deviceName + ".ProcessInput.AuPosRSt");
    g_sAuPosRStUnit = dpGetUnit(deviceName + ".ProcessInput.AuPosRSt");
    g_sMPosRStFormat = dpGetFormat(deviceName + ".ProcessInput.MPosRSt");
    g_sMPosRStUnit = dpGetUnit(deviceName + ".ProcessInput.MPosRSt");
    g_sOutOVStFormat = dpGetFormat(deviceName + ".ProcessInput.OutOVSt");
    g_sOutOVStUnit = dpGetUnit(deviceName + ".ProcessInput.OutOVSt");
    g_sAtOutHFormat = dpGetFormat(deviceName + ".ProcessInput.ActOutH");
    g_sAtOutHUnit = dpGetUnit(deviceName + ".ProcessInput.ActOutH");
    g_sAtOutLFormat = dpGetFormat(deviceName + ".ProcessInput.ActOutL");
    g_sAtOutLUnit = dpGetUnit(deviceName + ".ProcessInput.ActOutL");
    g_sActKcFormat = dpGetFormat(deviceName + ".ProcessInput.ActKc");
    g_sActKcUnit = dpGetUnit(deviceName + ".ProcessInput.ActKc");
    g_sActTiFormat = dpGetFormat(deviceName + ".ProcessInput.ActTi");
    g_sActTiUnit = dpGetUnit(deviceName + ".ProcessInput.ActTi");
    g_sActTdFormat = dpGetFormat(deviceName + ".ProcessInput.ActTd");
    g_sActTdUnit = dpGetUnit(deviceName + ".ProcessInput.ActTd");
    g_sActTdsFormat = dpGetFormat(deviceName + ".ProcessInput.ActTds");
    g_sActTdsUnit = dpGetUnit(deviceName + ".ProcessInput.ActTds");
    unGenericDpFunctions_getParameters(deviceName, g_dsParameters);
    g_sDescription = unGenericDpFunctions_getDescription(deviceName);
    g_sMVFormat = dpGetFormat(deviceName + ".ProcessInput.MV");
    g_sMVUnit = dpGetUnit(deviceName + ".ProcessInput.MV");

    iRes = dpGet(deviceName + ".ProcessInput.ActSP:_pv_range.._type", iRangeType1,
                 deviceName + ".ProcessInput.OutOVSt:_pv_range.._type", iRangeType2);
    g_iRangeTypeSp = (iRes >= 0) && (iRangeType1 == DPCONFIG_MINMAX_PVSS_RANGECHECK) ? DPCONFIG_MINMAX_PVSS_RANGECHECK : DPCONFIG_NONE;
    g_iRangeTypeOut = (iRes >= 0) && (iRangeType2 == DPCONFIG_MINMAX_PVSS_RANGECHECK) ? DPCONFIG_MINMAX_PVSS_RANGECHECK : DPCONFIG_NONE;
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of Controller DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_Controller_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuSPSt"); // Setpoint
    dynAppend(dpes, deviceName + ".ProcessInput.AuSPSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MSPSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MSPSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSP");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSP:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPH");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPH:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPL");
    dynAppend(dpes, deviceName + ".ProcessInput.ActSPL:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuPosRSt"); // Output
    dynAppend(dpes, deviceName + ".ProcessInput.AuPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt");
    dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutH");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutH:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutL");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutL:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActKc"); // PID parameters
    dynAppend(dpes, deviceName + ".ProcessInput.ActKc:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTi");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTi:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTd");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTd:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTds");
    dynAppend(dpes, deviceName + ".ProcessInput.ActTds:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MV");
    dynAppend(dpes, deviceName + ".ProcessInput.MV:_online.._invalid");

    if (g_iRangeTypeSp == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        dynAppend(dpes, deviceName + ".ProcessInput.ActSP:_pv_range.._max");
        dynAppend(dpes, deviceName + ".ProcessInput.ActSP:_pv_range.._min");
    }
    if (g_iRangeTypeOut == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt:_pv_range.._max");
        dynAppend(dpes, deviceName + ".ProcessInput.OutOVSt:_pv_range.._min");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_Controller_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    unFaceplate_animateOnlineValue(dpes, values, "AuSPSt", g_sAuSPStUnit, g_sAuSPStFormat, "cpcColor_Faceplate_Request");
    unFaceplate_animateOnlineValue(dpes, values, "MSPSt", g_sMSPStUnit, g_sMSPStFormat, "cpcColor_Faceplate_Request");
    unFaceplate_animateOnlineValue(dpes, values, "ActSP", g_sActSPUnit, g_sActSPFormat, "cpcColor_Faceplate_Request");
    unFaceplate_animateOnlineValue(dpes, values, "AuPosRSt", g_sAuPosRStUnit, g_sAuPosRStFormat, "cpcColor_Faceplate_Request");
    unFaceplate_animateOnlineValue(dpes, values, "MPosRSt", g_sMPosRStUnit, g_sMPosRStFormat, "cpcColor_Faceplate_Request");
    unFaceplate_animateOnlineValue(dpes, values, "OutOVSt", g_sOutOVStUnit, g_sOutOVStFormat, "cpcColor_Faceplate_Request");

    unFaceplate_animateOnlineValue(dpes, values, "ActKc", g_sActKcUnit, g_sActKcFormat, "unDisplayValue_Parameter");
    unFaceplate_animateOnlineValue(dpes, values, "ActTi", g_sActTiUnit, g_sActTiFormat, "unDisplayValue_Parameter");
    unFaceplate_animateOnlineValue(dpes, values, "ActTd", g_sActTdUnit, g_sActTdsFormat, "unDisplayValue_Parameter");
    unFaceplate_animateOnlineValue(dpes, values, "ActTds", g_sActTdsUnit, g_sActTdFormat, "unDisplayValue_Parameter");
    unFaceplate_animateOnlineValue(dpes, values, "ActSPH", g_sActSPHUnit, g_sActSPHFormat, "unDisplayValue_Parameter");
    unFaceplate_animateOnlineValue(dpes, values, "ActSPL", g_sActSPLUnit, g_sActSPLFormat, "unDisplayValue_Parameter");
    unFaceplate_animateOnlineValue(dpes, values, "ActOutH", g_sAtOutHUnit, g_sAtOutHFormat, "unDisplayValue_Parameter");
    unFaceplate_animateOnlineValue(dpes, values, "ActOutL", g_sAtOutLUnit, g_sAtOutLFormat, "unDisplayValue_Parameter");

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ACTIVE", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_SOFTLDST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_TRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_OUTPST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_REGST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPKP", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPTI", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPTD", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPKD", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPSPOH", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPSPOL", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPOUTH", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_BOPOUTL", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUINHFMO", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUREGR", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUINHMMO", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_PIDARMRCPST", true, "cpcColor_Faceplate_Status");

    cpcGenericObject_animateMaskEvent(dpes, values);
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");

    CPC_Controller_setProperty("Color_MV", "cpcColor_Widget_Status");
    CPC_Controller_setProperty("Color_OutO", "cpcColor_Widget_Request");
    CPC_Controller_setProperty("Color_SP", "cpcColor_Widget_Controller_SP");
    CPC_Controller_setProperty("BackColor", "unSynopticBackground");

    // Name & Description
    string pidName = dynlen(g_dsParameters)>=UN_PARAMETER_NAME ? g_dsParameters[UN_PARAMETER_NAME] : "";
    if (pidName == "") { pidName = "PID"; }
    CPC_Controller_setProperty("PIDName", pidName);
    CPC_Controller_setProperty("Description", g_sDescription);

    // MV
    float fMV = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.MV");
    float maxActSP = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.ActSP:_pv_range.._max");
    float minActSP = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.ActSP:_pv_range.._min");
    float maxOut = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.OutOVSt:_pv_range.._max");
    float minOut = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.OutOVSt:_pv_range.._min");
    CPC_Controller_setProperty("MV_Unit", g_sMVUnit);
    CPC_Controller_setProperty("MV", fMV);
    CPC_Controller_setProperty("MVLabel", unGenericObject_FormatValue(g_sMVFormat, fMV));
    CPC_Controller_setProperty("MV_MaxRange", maxActSP);
    CPC_Controller_setProperty("MV_MinRange", minActSP);
    CPC_Controller_setProperty("Out_MaxRange", maxOut);
    CPC_Controller_setProperty("Out_MinRange", minOut);
    CPC_Controller_setProperty("LabelMV_MaxRange", unGenericObject_FormatValue(g_sActSPFormat, maxActSP));
    CPC_Controller_setProperty("LabelMV_MinRange", unGenericObject_FormatValue(g_sActSPFormat, minActSP));

    // Setpoint active
    float fActSP = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.ActSP");
    float fActSPH = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.ActSPH");
    float fActSPL = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.ActSPL");
    CPC_Controller_setProperty("SP", fActSP);
    CPC_Controller_setProperty("SPLabel", unGenericObject_FormatValue(g_sActSPFormat, fActSP));
    CPC_Controller_setProperty("SP_LimitHigh", fActSPH);
    CPC_Controller_setProperty("SP_LimitLow", fActSPL);

    // Output active
    float fOutOVSt = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.OutOVSt");
    float fAtOutH = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.ActOutH");
    float fAtOutL = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.ActOutL");
    bit32 bit32StsReg01 = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.StsReg01");
    CPC_Controller_setProperty("OutO", fOutOVSt);
    CPC_Controller_setProperty("OutOLabel", unGenericObject_FormatValue(g_sOutOVStFormat, fOutOVSt));
    CPC_Controller_setProperty("OutO_Unit", g_sOutOVStUnit);
    CPC_Controller_setProperty("LabelMaxO", unGenericObject_FormatValue(g_sOutOVStFormat, maxOut) + " " + g_sOutOVStUnit);
    CPC_Controller_setProperty("LabelMinO", unGenericObject_FormatValue(g_sOutOVStFormat, minOut) + " " + g_sOutOVStUnit);
    CPC_Controller_setProperty("OutO_LimitHigh", fAtOutH); // Output high limit
    CPC_Controller_setProperty("OutO_LimitLow", fAtOutL); // Output low limit
    CPC_Controller_setProperty("Operation_Mode", bit32StsReg01); // Operation mode
    _CPC_Controller_updateBargraph();
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}


//begin_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions
// CPC_Controller_setProperty
/**
Purpose: Set the property value of PID panel

Parameters: None

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_Controller_setProperty(string sProperty, anytype aPropertyValue) {
    if (!g_bSystemConnected) { return; }

    float fVal;
    string sString;
    bit32 b32;

    switch (getType(aPropertyValue)) {
        case BIT32_VAR:
            b32 = aPropertyValue;
            switch (sProperty) {
                case "Operation_Mode":
                    Operation_Mode.text() = b32;
                    break;
                default:
                    DebugN("********* UNKNOWN BIT32", sProperty, b32);
                    break;
            }
//      setValue("PidActiveX", sProperty, b32);
//DebugN(sProperty, aPropertyValue, b32, PidActiveX.Operation_Mode());
//DebugN(sProperty, b32);
            break;
        case FLOAT_VAR:
            fVal = aPropertyValue;
            switch (sProperty) {
                case "MV":
                    MV.text() = fVal;
                    break;
                case "MV_MaxRange":
                    MV_MaxRange.text() = fVal;
                    break;
                case "MV_MinRange":
                    MV_MinRange.text() = fVal;
                    break;
                case "SP":
                    SP.text() = fVal;
                    break;
                case "SP_LimitHigh":
                    SP_LimitHigh.text() = fVal;
                    break;
                case "SP_LimitLow":
                    SP_LimitLow.text() = fVal;
                    break;
                case "OutO":
                    OutO.text() = fVal;
                    break;
                case "OutO_LimitHigh":
                    OutO_LimitHigh.text() = fVal;
                    break;
                case "OutO_LimitLow":
                    OutO_LimitLow.text() = fVal;
                    break;
                case "Out_MaxRange":
                    Out_MaxRange.text() = fVal;
                    break;
                case "Out_MinRange":
                    Out_MinRange.text() = fVal;
                    break;
                default:
                    DebugN("********* UNKNOWN FLOAT", sProperty, fVal);
                    break;
            }
//      setValue("PidActiveX", sProperty, fVal);
//DebugN(sProperty, fVal);
            break;
        case STRING_VAR:
            sString = aPropertyValue;
            switch (sProperty) {
                case "Color_MV":
                    ShapeMV.backCol() = sString;
                    ShapeMV.foreCol() = sString;
                    TxtMV.foreCol() = sString;
                    LabelMV.foreCol() = sString;
                    LabelMV_Unit.foreCol() = sString;
                    break;
                case "Color_OutO":
                    ShapeOutO.backCol() = sString;
                    ShapeOutO.foreCol() = sString;
                    TxtOutO.foreCol() = sString;
                    LabelOutO.foreCol() = sString;
                    LabelOutO_Unit.foreCol() = sString;
                    LabelOutOLow.foreCol() = sString;
                    LabelOutOHigh.foreCol() = sString;
                    break;
                case "Color_SP":
                    ShapeSP.backCol() = sString;
                    ShapeSP.foreCol() = sString;
                    TxtSP.foreCol() = sString;
                    LabelSP.foreCol() = sString;
                    LabelSP_Unit.foreCol() = sString;
                    LabelSPHigh.foreCol() = sString;
                    LabelSPLow.foreCol() = sString;
                    break;
                case "BackColor":
                    Pidcon.backCol() = sString;
                    Pidcon.foreCol() = sString;
                    break;
                case "PIDName":
                    LabelName.text() = sString;
                    break;
                case "Description":
                    LabelDescription.text() = sString;
                    break;
                case "MV_Unit":
                    LabelMV_Unit.text() = sString;
                    LabelSP_Unit.text() = sString;
                    break;
                case "OutO_Unit":
                    LabelOutO_Unit.text() = sString;
                    break;
                case "OutOLabel":
                    TxtOutO.text() = sString;
                    break;
                case "SPLabel":
                    TxtSP.text() = sString;
                    break;
                case "MVLabel":
                    TxtMV.text() = sString;
                    break;
                case "LabelMV_MaxRange":
                    LabelMV_MaxRange.text() = sString;
                    break;
                case "LabelMV_MinRange":
                    LabelMV_MinRange.text() = sString;
                    break;
                case "LabelMaxO":
                    LabelMaxO.text() = sString;
                    break;
                case "LabelMinO":
                    LabelMinO.text() = sString;
                    break;
                case "Update":
                    _CPC_Controller_updateBargraph();
                    break;
                default:
                    DebugN("********* UNKNOWN STRING", sProperty, sString);
                    break;
            }
//DebugN(sProperty, sString);
            break;
        default:
            break;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _CPC_Controller_updateBargraph
/**
Purpose: Update the bargraph of PID panel

Parameters: None

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
_CPC_Controller_updateBargraph() {
    bit32 b32OperationMode = Operation_Mode.text();
    float fX, fY, fH, fL, fLH, fLL, f, fScale, fPos, fShY, fSizeY;

//DebugN("start _CPC_Controller_updateBargraph");
    if (StandAlone.text() == "TRUE") {
        LabelMV_Unit.visible = true;
        LabelSP_Unit.visible = true;
        LabelOutO_Unit.visible = true;
// Update LabelReMoSt based on Operation_Mode
        LabelReMoSt.visible = getBit(b32OperationMode, CPC_StsReg01_REMOST);
// Update LabelAuMoSt based on Operation_Mode
        LabelAuMoSt.visible = getBit(b32OperationMode, CPC_StsReg01_AUMOST);
// Update LabelMMoSt based on Operation_Mode
        LabelMMoSt.visible = getBit(b32OperationMode, CPC_StsReg01_MMOST);
// Update LabelFoMoSt based on Operation_Mode
        LabelFoMoSt.visible = getBit(b32OperationMode, CPC_StsReg01_FOMOST);
// Update LabelTracking based on Operation_Mode
        LabelTracking.visible = getBit(b32OperationMode, CPC_StsReg01_TST);
    } else {
        LabelMV_Unit.visible = false;
        LabelSP_Unit.visible = false;
        LabelOutO_Unit.visible = false;
        LabelReMoSt.visible = false;
        LabelAuMoSt.visible = false;
        LabelMMoSt.visible = false;
        LabelFoMoSt.visible = false;
        LabelTracking.visible = false;
    }
    LabelSPHigh.toolTipText() = "SP High Limit = " + SP_LimitHigh.text();
    LabelSPLow.toolTipText() = "SP Low Limit = " + SP_LimitLow.text();
    LabelOutOHigh.toolTipText() = "Out High Limit = " + OutO_LimitHigh.text();
    LabelOutOLow.toolTipText() = "Out Low Limit = " + OutO_LimitLow.text();

// Update ShapeSPLimitH based on SP_LimitHigh --> SP High limit
    fH = MV_MaxRange.text();
    fL = MV_MinRange.text();
    f = SP_LimitHigh.text();
    if ((fH - fL) != 0) {
        fScale = (fH - f) / (fH - fL);
    } else {
        fScale = 0;
    }
    if (fScale < 0) {
        fScale = 0;
    }
    if (fScale > 1) {
        fScale = 1;
    }
    setValue("ShapeSPLimitH", "scale", 1, fScale);
//DebugN("ShapeSPLimitH=" + fScale);
// Update ShapeSPLimitL based on SP_LimitLow --> SP Low limit
    f = SP_LimitLow.text();
    if ((fH - fL) != 0) {
        fScale = (fL - f) / (fL - fH);
    } else {
        fScale = 0;
    }
    if (fScale < 0) {
        fScale = 0;
    }
    if (fScale > 1) {
        fScale = 1;
    }
    setValue("ShapeSPLimitL", "scale", 1, fScale);
//DebugN("ShapeSPLimitL=" + fScale);
// Update ShapeMV based on MV
    f = MV.text();
    if ((fH - fL) != 0) {
        fScale = (fL - f) / (fL - fH);
    } else {
        fScale = 0;
    }
    if (fScale < 0) {
        fScale = 0;
    }
    if (fScale > 1) {
        fScale = 1;
    }
    setValue("ShapeMV", "scale", 1, fScale);
//DebugN("ShapeMV=" + fScale);
// Update ShapeSP based on SP
    f = SP.text();
    if ((fH - fL) != 0) {
        fScale = (fL - f) / (fL - fH);
    } else {
        fScale = 0;
    }
    if (fScale < 0) {
        fScale = 0;
    }
    if (fScale > 1) {
        fScale = 1;
    }
    setValue("ShapeSP", "scale", 1, fScale);
//DebugN("ShapeSP=" + fScale);
    fSizeY = YAxisSize.text();
// Update ShapeSPLimit based on SP & SP Limit High & Low
    fX = ShapeX.text();
    fY = ShapeSPY.text();
    f = SP.text();
    fLH = SP_LimitHigh.text();
    fLL = SP_LimitLow.text();
    if (f <= fLL) {
        LabelSPLow.visible = true;
        LabelSPHigh.visible = false;
        if ((fH - fLL) < 0) {
            fPos = fY;
        } else {
            fPos = fY + fSizeY - ((fLL - fL) / (fH - fL)) * fSizeY - UN_CONTROLLER_SHAPE_LIMIT_YSIZE;
        }
        setMultiValue("ShapeSPLimit", "position", fX, fPos, "ShapeSPLimit", "visible", true);
//DebugN("ShapeSPLimit f<=fLL", fX, fY, f, fH, fLL, fLH, fPos);
    } else if (f >= fLH) {
        LabelSPLow.visible = false;
        LabelSPHigh.visible = true;
        if ((fH - fLH) < 0) {
            fPos = fY;
        } else {
            fPos = fY + fSizeY - ((fLH - fL) / (fH - fL)) * fSizeY - UN_CONTROLLER_SHAPE_LIMIT_YSIZE;
        }
        setMultiValue("ShapeSPLimit", "position", fX, fPos, "ShapeSPLimit", "visible", true);
//DebugN("ShapeSPLimit f>=fLH", fX, fY, f, fH, fLL, fLH, fPos);
    } else {
        LabelSPLow.visible = false;
        LabelSPHigh.visible = false;
        ShapeSPLimit.visible = false;
    }
// Update ShapeOutOLimitH based on OutO_LimitHigh --> OutO High limit
    fH = Out_MaxRange.text();
    fL = Out_MinRange.text();
    f = OutO_LimitHigh.text();
    fScale = (100 - f) / (100);
    if (fScale < 0) {
        fScale = 0;
    }
    if (fScale > 1) {
        fScale = 1;
    }
    setValue("ShapeOutOLimitH", "scale", 1, fScale);
//DebugN("ShapeOutOLimitH=" + fScale);
// Update ShapeOutOLimitL based on OutO_LimitLow --> OutO Low limit
    f = OutO_LimitLow.text();
    fScale = (f) / (100);
    if (fScale < 0) {
        fScale = 0;
    }
    if (fScale > 1) {
        fScale = 1;
    }
    setValue("ShapeOutOLimitL", "scale", 1, fScale);
//DebugN("ShapeOutOLimitL=" + fScale);
// Update ShapeOutO based on OutO
    f = OutO.text();
    if ((fH - fL) != 0) {
        fScale = (f - fL) / (fH - fL);
    } else {
        fScale = 0;
    }
    if (fScale < 0) {
        fScale = 0;
    }
    if (fScale > 1) {
        fScale = 1;
    }
    setValue("ShapeOutO", "scale", 1, fScale);
//DebugN("ShapeOutO=" + fScale);
// Update ShapeOutOLimit based on OutO & OutO Limit High & Low
    fX = ShapeOX.text();
    fY = ShapeOutY.text();
    f = OutO.text();
    fLH = OutO_LimitHigh.text();
    fLL = OutO_LimitLow.text();
    if (f <= fLL) {
        LabelOutOLow.visible = true;
        LabelOutOHigh.visible = false;
        if ((fH - fLL) < 0) {
            fPos = fY;
        } else {
            fPos = fY + fSizeY - ((fLL - fL) / (fH - fL)) * fSizeY - UN_CONTROLLER_SHAPE_LIMIT_YSIZE;
        }
        setMultiValue("ShapeOutOLimit", "position", fX, fPos, "ShapeOutOLimit", "visible", true);
//DebugN("ShapeOutOLimit f<=fLL", fX, fY, f, fH, fLL, fLH, fPos);
    } else if (f >= fLH) {
        LabelOutOLow.visible = false;
        LabelOutOHigh.visible = true;
        if ((fH - fLH) < 0) {
            fPos = fY;
        } else {
            fPos = fY + fSizeY - ((fLH - fL) / (fH - fL)) * fSizeY - UN_CONTROLLER_SHAPE_LIMIT_YSIZE;
        }
        setMultiValue("ShapeOutOLimit", "position", fX, fPos, "ShapeOutOLimit", "visible", true);
//DebugN("ShapeOutOLimit f>=fLH", fX, fY, f, fH, fLL, fLH, fPos);
    } else {
        LabelOutOLow.visible = false;
        LabelOutOHigh.visible = false;
        ShapeOutOLimit.visible = false;
    }
//DebugN("end _CPC_Controller_updateBargraph");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _CPC_Controller_initBargraph
/**
Purpose: Init the bargraph of PID panel

Parameters:
  sStandAlone: string, input, TRUE for standalone mode, anything else for non standalone mode

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
_CPC_Controller_initBargraph(string sStandAlone) {
    float fX, fY;

    StandAlone.text() = sStandAlone;
    MV_MaxRange.text() = 100;
    MV_MinRange.text() = 0;
    MV.text() = 75;
    SP_LimitHigh.text() = 100;
    SP_LimitLow.text() = 0;
    SP.text() = 50;
    OutO_LimitHigh.text() = 100;
    OutO_LimitLow.text() = 0;
    OutO.text() = 50;
    getValue("ShapeSPLimit", "position", fX, fY);
    ShapeX.text() = fX;
    ShapeSPY.text() = fY;
    getValue("ShapeOutOLimit", "position", fX, fY);
    ShapeOX.text() = fX;
    ShapeOutY.text() = fY;
    getValue("YAxis", "size", fX, fY);
    YAxisSize.text() = fY;
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions


//@}
