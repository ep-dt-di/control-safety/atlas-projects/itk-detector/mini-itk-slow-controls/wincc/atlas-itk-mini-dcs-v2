/**@file

// cpcControllerConfig.ctl
This library contains the import and export function of the CPC_Controller.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_CONTROLLER_DPT_NAME = "CPC_Controller";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_CONTROLLER_LENGTH = 49;

const unsigned UN_CONFIG_CPC_CONTROLLER_PIDNAME 				= 1;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSP_UNIT				= 2;
const unsigned UN_CONFIG_CPC_CONTROLLER_MSPST_UNIT				= 2;
const unsigned UN_CONFIG_CPC_CONTROLLER_AUSPST_UNIT				= 2;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPL_UNIT				= 2;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPH_UNIT				= 2;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_UNIT					= 2;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSP_FORMAT			= 3;
const unsigned UN_CONFIG_CPC_CONTROLLER_MSPST_FORMAT			= 3;
const unsigned UN_CONFIG_CPC_CONTROLLER_AUSPST_FORMAT			= 3;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPL_FORMAT			= 3;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPH_FORMAT			= 3;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_FORMAT				= 3;
const unsigned UN_CONFIG_CPC_CONTROLLER_RANGEMAX 				= 4;
const unsigned UN_CONFIG_CPC_CONTROLLER_RANGEMIN	 			= 5;
const unsigned UN_CONFIG_CPC_CONTROLLER_UNIT 					= 6;
const unsigned UN_CONFIG_CPC_CONTROLLER_FORMAT	 				= 7;
const unsigned UN_CONFIG_CPC_CONTROLLER_OUTOVST_RANGEMAX		= 8;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_RANGEMAX				= 8;
const unsigned UN_CONFIG_CPC_CONTROLLER_OUTOVST_RANGEMIN		= 9;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_RANGEMIN				= 9;
const unsigned UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD			= 10;
const unsigned UN_CONFIG_CPC_CONTROLLER_DEADBAND 				= 11;
const unsigned UN_CONFIG_CPC_CONTROLLER_DEADBAND_TYPE 			= 12;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_DEADBAND 			= 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSP_DEADBAND 			= 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_MSPST_DEADBAND			= 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_ATAUSP_DEADBAND			= 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPH_DEADBAND			= 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPL_DEADBAND			= 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_AUSPST_DEADBAND			= 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_DEADBAND_TYPE 		= 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSP_DEADBAND_TYPE 	= 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_MSPST_DEADBAND_TYPE		= 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_ATAUSP_DEADBAND_TYPE	= 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPH_DEADBAND_TYPE	= 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSPL_DEADBAND_TYPE	= 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_AUSPST_DEADBAND_TYPE	= 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_OUTOVST_ARCHIVE_ACTIVE  = 15;
const unsigned UN_CONFIG_CPC_CONTROLLER_OUTOVST_ARCHIVE_TIME_FILTER = 16;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSP_ARCHIVE_ACTIVE	= 17;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_ARCHIVE_ACTIVE		= 17;
const unsigned UN_CONFIG_CPC_CONTROLLER_ACTSP_ARCHIVE_TIME_FILTER	= 18;
const unsigned UN_CONFIG_CPC_CONTROLLER_MV_ARCHIVE_TIME_FILTER	= 18;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_STSREG01 		= 19;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_STSREG02 		= 20;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_EVSTSREG01 		= 21;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_EVSTSREG02 		= 22;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTSP 			= 23;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTSPL 			= 24;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTSPH 			= 25;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_OUTOVST 		= 26;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTOUTL			= 27;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTOUTH 		= 28;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MV 				= 29;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSPST 			= 30;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_AUSPST 			= 31;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MPOSRST 		= 32;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_AUPOSRST 		= 33;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTKC 			= 34;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTTI 			= 35;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTTD 			= 36;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTTDS 			= 37;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MANREG01 		= 38;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MANREG02 		= 39;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MPOSR 			= 40;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSP 			= 41;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSPL 			= 42;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSPH 			= 43;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MOUTL 			= 44;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MOUTH 			= 45;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MKC 			= 46;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MTI 			= 47;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MTD 			= 48;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDRESS_MTDS 			= 49;

// New implementation with all default values (PID, Spoint, Limits Sp, Limits OUT
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_LENGTH		    = 16;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_MASKEVENT	  = 1;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_KC		  = 2;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TI		  = 3;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TD		  = 4;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TDS		  = 5;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPO		  = 6;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPH		  = 7;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPL		  = 8;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTH	  = 9;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTL	  = 10;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_PARAMETERS   = 11;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_MASTER_NAME  = 12;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_PARENTS      = 13;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_CHILDREN     = 14;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_TYPE         = 15;
const unsigned UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_SECOND_ALIAS = 16;

// Format for parameters
const string UN_CONFIG_CPC_CONTROLLER_FORMAT_KP = "####.###";
const string UN_CONFIG_CPC_CONTROLLER_FORMAT_TI = "####.###";
const string UN_CONFIG_CPC_CONTROLLER_FORMAT_KD = "####.###";
const string UN_CONFIG_CPC_CONTROLLER_FORMAT_TD = "####.###";
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_ControllerConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg01";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg01"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg02";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg02"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.MV";
    props["dataType"] 		= CPC_FLOAT;
    config["MV"] 			= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["address"] 		= ".ProcessInput.MPosRSt";
    props["dataType"] 		= CPC_FLOAT;
    config["MPosRSt"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["address"] 		= ".ProcessInput.AuPosRSt";
    props["dataType"] 		= CPC_FLOAT;
    config["AuPosRSt"] 		= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.OutOVSt";
    props["dataType"] 		= CPC_FLOAT;
    config["OutOVSt"] 		= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.ActSP";
    props["dataType"] 		= CPC_FLOAT;
    config["ActSP"] 		= props;

    mappingClear(props);
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.MSPSt";
    props["dataType"] 		= CPC_FLOAT;
    config["MSPSt"] 		= props;

    mappingClear(props);
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.AuSPSt";
    props["dataType"] 		= CPC_FLOAT;
    config["AuSPSt"] 		= props;

    mappingClear(props);
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.ActSPH";
    props["dataType"] 		= CPC_FLOAT;
    config["ActSPH"] 		= props;

    mappingClear(props);
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.ActSPL";
    props["dataType"] 		= CPC_FLOAT;
    config["ActSPL"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["address"] 		= ".ProcessInput.ActOutH";
    props["dataType"] 		= CPC_FLOAT;
    config["ActOutH"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["address"] 		= ".ProcessInput.ActOutL";
    props["dataType"] 		= CPC_FLOAT;
    config["ActOutL"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.ActKc";
    props["dataType"] 		= CPC_FLOAT;
    config["ActKc"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.ActTi";
    props["dataType"] 		= CPC_FLOAT;
    config["ActTi"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.ActTd";
    props["dataType"] 		= CPC_FLOAT;
    config["ActTd"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.ActTds";
    props["dataType"] 		= CPC_FLOAT;
    config["ActTds"] 		= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg01";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg01"] 	= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg02";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg02"] 	= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg01";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg01"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg02";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg02"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MPosR";
    props["dataType"] 		= CPC_FLOAT;
    config["MPosR"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MSP";
    props["dataType"] 		= CPC_FLOAT;
    config["MSP"] 			= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MSPH";
    props["dataType"] 		= CPC_FLOAT;
    config["MSPH"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MSPL";
    props["dataType"] 		= CPC_FLOAT;
    config["MSPL"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MOutH";
    props["dataType"] 		= CPC_FLOAT;
    config["MOutH"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MOutL";
    props["dataType"] 		= CPC_FLOAT;
    config["MOutL"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MKc";
    props["dataType"] 		= CPC_FLOAT;
    config["MKc"]	 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MTi";
    props["dataType"] 		= CPC_FLOAT;
    config["MTi"] 			= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MTd";
    props["dataType"] 		= CPC_FLOAT;
    config["MTd"] 			= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MTds";
    props["dataType"] 		= CPC_FLOAT;
    config["MTds"] 			= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_ControllerConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                    = UN_CONFIG_CPC_CONTROLLER_DPT_NAME;
  mTemp["LENGTH"]                      = UN_CONFIG_CPC_CONTROLLER_LENGTH;
  mTemp["PIDNAME"]                     = UN_CONFIG_CPC_CONTROLLER_PIDNAME;
  mTemp["ACTSP_UNIT"]                  = UN_CONFIG_CPC_CONTROLLER_ACTSP_UNIT;
  mTemp["MSPST_UNIT"]                  = UN_CONFIG_CPC_CONTROLLER_MSPST_UNIT;
  mTemp["AUSPST_UNIT"]                 = UN_CONFIG_CPC_CONTROLLER_AUSPST_UNIT;
  mTemp["ACTSPL_UNIT"]                 = UN_CONFIG_CPC_CONTROLLER_ACTSPL_UNIT;
  mTemp["ACTSPH_UNIT"]                 = UN_CONFIG_CPC_CONTROLLER_ACTSPH_UNIT;
  mTemp["MV_UNIT"]                     = UN_CONFIG_CPC_CONTROLLER_MV_UNIT;
  mTemp["ACTSP_FORMAT"]                = UN_CONFIG_CPC_CONTROLLER_ACTSP_FORMAT;
  mTemp["MSPST_FORMAT"]                = UN_CONFIG_CPC_CONTROLLER_MSPST_FORMAT;
  mTemp["AUSPST_FORMAT"]               = UN_CONFIG_CPC_CONTROLLER_AUSPST_FORMAT;
  mTemp["ACTSPL_FORMAT"]               = UN_CONFIG_CPC_CONTROLLER_ACTSPL_FORMAT;
  mTemp["ACTSPH_FORMAT"]               = UN_CONFIG_CPC_CONTROLLER_ACTSPH_FORMAT;
  mTemp["MV_FORMAT"]                   = UN_CONFIG_CPC_CONTROLLER_MV_FORMAT;
  mTemp["RANGEMAX"]                    = UN_CONFIG_CPC_CONTROLLER_RANGEMAX;
  mTemp["RANGEMIN"]                    = UN_CONFIG_CPC_CONTROLLER_RANGEMIN;
  mTemp["UNIT"]                        = UN_CONFIG_CPC_CONTROLLER_UNIT;
  mTemp["FORMAT"]                      = UN_CONFIG_CPC_CONTROLLER_FORMAT;
  mTemp["OUTOVST_RANGEMAX"]            = UN_CONFIG_CPC_CONTROLLER_OUTOVST_RANGEMAX;
  mTemp["MV_RANGEMAX"]                 = UN_CONFIG_CPC_CONTROLLER_MV_RANGEMAX;
  mTemp["OUTOVST_RANGEMIN"]            = UN_CONFIG_CPC_CONTROLLER_OUTOVST_RANGEMIN;
  mTemp["MV_RANGEMIN"]                 = UN_CONFIG_CPC_CONTROLLER_MV_RANGEMIN;
  mTemp["SCALING_METHOD"]              = UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD;
  mTemp["DEADBAND"]                    = UN_CONFIG_CPC_CONTROLLER_DEADBAND;
  mTemp["DEADBAND_TYPE"]               = UN_CONFIG_CPC_CONTROLLER_DEADBAND_TYPE;
  mTemp["MV_DEADBAND"]                 = UN_CONFIG_CPC_CONTROLLER_MV_DEADBAND;
  mTemp["ACTSP_DEADBAND"]              = UN_CONFIG_CPC_CONTROLLER_ACTSP_DEADBAND;
  mTemp["MSPST_DEADBAND"]              = UN_CONFIG_CPC_CONTROLLER_MSPST_DEADBAND;
  mTemp["ATAUSP_DEADBAND"]             = UN_CONFIG_CPC_CONTROLLER_ATAUSP_DEADBAND;
  mTemp["ACTSPH_DEADBAND"]             = UN_CONFIG_CPC_CONTROLLER_ACTSPH_DEADBAND;
  mTemp["ACTSPL_DEADBAND"]             = UN_CONFIG_CPC_CONTROLLER_ACTSPL_DEADBAND;
  mTemp["AUSPST_DEADBAND"]             = UN_CONFIG_CPC_CONTROLLER_AUSPST_DEADBAND;
  mTemp["MV_DEADBAND_TYPE"]            = UN_CONFIG_CPC_CONTROLLER_MV_DEADBAND_TYPE;
  mTemp["ACTSP_DEADBAND_TYPE"]         = UN_CONFIG_CPC_CONTROLLER_ACTSP_DEADBAND_TYPE;
  mTemp["MSPST_DEADBAND_TYPE"]         = UN_CONFIG_CPC_CONTROLLER_MSPST_DEADBAND_TYPE;
  mTemp["ATAUSP_DEADBAND_TYPE"]        = UN_CONFIG_CPC_CONTROLLER_ATAUSP_DEADBAND_TYPE;
  mTemp["ACTSPH_DEADBAND_TYPE"]        = UN_CONFIG_CPC_CONTROLLER_ACTSPH_DEADBAND_TYPE;
  mTemp["ACTSPL_DEADBAND_TYPE"]        = UN_CONFIG_CPC_CONTROLLER_ACTSPL_DEADBAND_TYPE;
  mTemp["AUSPST_DEADBAND_TYPE"]        = UN_CONFIG_CPC_CONTROLLER_AUSPST_DEADBAND_TYPE;
  mTemp["OUTOVST_ARCHIVE_ACTIVE"]      = UN_CONFIG_CPC_CONTROLLER_OUTOVST_ARCHIVE_ACTIVE;
  mTemp["OUTOVST_ARCHIVE_TIME_FILTER"] = UN_CONFIG_CPC_CONTROLLER_OUTOVST_ARCHIVE_TIME_FILTER;
  mTemp["ACTSP_ARCHIVE_ACTIVE"]        = UN_CONFIG_CPC_CONTROLLER_ACTSP_ARCHIVE_ACTIVE;
  mTemp["MV_ARCHIVE_ACTIVE"]           = UN_CONFIG_CPC_CONTROLLER_MV_ARCHIVE_ACTIVE;
  mTemp["ACTSP_ARCHIVE_TIME_FILTER"]   = UN_CONFIG_CPC_CONTROLLER_ACTSP_ARCHIVE_TIME_FILTER;
  mTemp["MV_ARCHIVE_TIME_FILTER"]      = UN_CONFIG_CPC_CONTROLLER_MV_ARCHIVE_TIME_FILTER;
  mTemp["ADDRESS_STSREG01"]            = UN_CONFIG_CPC_CONTROLLER_ADDRESS_STSREG01;
  mTemp["ADDRESS_STSREG02"]            = UN_CONFIG_CPC_CONTROLLER_ADDRESS_STSREG02;
  mTemp["ADDRESS_EVSTSREG01"]          = UN_CONFIG_CPC_CONTROLLER_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_EVSTSREG02"]          = UN_CONFIG_CPC_CONTROLLER_ADDRESS_EVSTSREG02;
  mTemp["ADDRESS_ACTSP"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTSP;
  mTemp["ADDRESS_ACTSPL"]              = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTSPL;
  mTemp["ADDRESS_ACTSPH"]              = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTSPH;
  mTemp["ADDRESS_OUTOVST"]             = UN_CONFIG_CPC_CONTROLLER_ADDRESS_OUTOVST;
  mTemp["ADDRESS_ACTOUTL"]             = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTOUTL;
  mTemp["ADDRESS_ACTOUTH"]             = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTOUTH;
  mTemp["ADDRESS_MV"]                  = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MV;
  mTemp["ADDRESS_MSPST"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSPST;
  mTemp["ADDRESS_AUSPST"]              = UN_CONFIG_CPC_CONTROLLER_ADDRESS_AUSPST;
  mTemp["ADDRESS_MPOSRST"]             = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MPOSRST;
  mTemp["ADDRESS_AUPOSRST"]            = UN_CONFIG_CPC_CONTROLLER_ADDRESS_AUPOSRST;
  mTemp["ADDRESS_ACTKC"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTKC;
  mTemp["ADDRESS_ACTTI"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTTI;
  mTemp["ADDRESS_ACTTD"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTTD;
  mTemp["ADDRESS_ACTTDS"]              = UN_CONFIG_CPC_CONTROLLER_ADDRESS_ACTTDS;
  mTemp["ADDRESS_MANREG01"]            = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MANREG01;
  mTemp["ADDRESS_MANREG02"]            = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MANREG02;
  mTemp["ADDRESS_MPOSR"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MPOSR;
  mTemp["ADDRESS_MSP"]                 = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSP;
  mTemp["ADDRESS_MSPL"]                = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSPL;
  mTemp["ADDRESS_MSPH"]                = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MSPH;
  mTemp["ADDRESS_MOUTL"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MOUTL;
  mTemp["ADDRESS_MOUTH"]               = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MOUTH;
  mTemp["ADDRESS_MKC"]                 = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MKC;
  mTemp["ADDRESS_MTI"]                 = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MTI;
  mTemp["ADDRESS_MTD"]                 = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MTD;
  mTemp["ADDRESS_MTDS"]                = UN_CONFIG_CPC_CONTROLLER_ADDRESS_MTDS;
  mTemp["ADDITIONAL_LENGTH"]           = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]        = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_DEFV_KC"]          = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_KC;
  mTemp["ADDITIONAL_DEFV_TI"]          = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TI;
  mTemp["ADDITIONAL_DEFV_TD"]          = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TD;
  mTemp["ADDITIONAL_DEFV_TDS"]         = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TDS;
  mTemp["ADDITIONAL_DEFV_SPO"]         = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPO;
  mTemp["ADDITIONAL_DEFV_SPH"]         = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPH;
  mTemp["ADDITIONAL_DEFV_SPL"]         = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPL;
  mTemp["ADDITIONAL_DEFV_OUTH"]        = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTH;
  mTemp["ADDITIONAL_DEFV_OUTL"]        = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTL;
  mTemp["ADDITIONAL_PARAMETERS"]       = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]      = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]          = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]         = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]             = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"]     = UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_SECOND_ALIAS;
  mTemp["FORMAT_KP"]                   = UN_CONFIG_CPC_CONTROLLER_FORMAT_KP;
  mTemp["FORMAT_TI"]                   = UN_CONFIG_CPC_CONTROLLER_FORMAT_TI;
  mTemp["FORMAT_KD"]                   = UN_CONFIG_CPC_CONTROLLER_FORMAT_KD;
  mTemp["FORMAT_TD"]                   = UN_CONFIG_CPC_CONTROLLER_FORMAT_TD;

  g_mCpcConst["UN_CONFIG_CPC_CONTROLLER"] = mTemp;
}




/** Check custom configuration
*/
CPC_ControllerConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    float fFloat;
    int shift = UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_CONTROLLER_LENGTH;
    if (hasArchive) {
        shift += UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH;
    }
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_KC], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TI], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TD], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TDS], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPO], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPH], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPL], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTH], fFloat, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTL], fFloat, exceptionInfo);

    int scalingMethod = configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD];
    if (scalingMethod < 1 || scalingMethod > 3) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_checkCustomConfig: Controller has wrong scaling method", "");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_ControllerConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    float fDefKc, fDefTi, fDefTd, fDefTds,  fDefSP, fDefSPH, fDefSPL,  fDefOutH, fDefOutL;
    int shift = UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_CONTROLLER_LENGTH;
    if (hasArchive) {
        shift += UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH;
    }

    unGenericDpFunctions_changeDpParameter(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], UN_PARAMETER_NAME, dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_CONTROLLER_PIDNAME], exceptionInfo);
    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActKc", UN_CONFIG_CPC_CONTROLLER_FORMAT_KP, exceptionInfo);
    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActTi", UN_CONFIG_CPC_CONTROLLER_FORMAT_TI, exceptionInfo);
    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActTds", UN_CONFIG_CPC_CONTROLLER_FORMAT_KD, exceptionInfo);
    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActTd", UN_CONFIG_CPC_CONTROLLER_FORMAT_TD, exceptionInfo);

    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActKc", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);
    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActTi", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);
    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActTds", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);
    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.ActTd", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);

    // TODO: write setting method for default value
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_KC], "%f", fDefKc);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TI], "%f", fDefTi);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TD], "%f", fDefTd);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_TDS], "%f", fDefTds);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPO], "%f", fDefSP);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPH], "%f", fDefSPH);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_SPL], "%f", fDefSPL);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTH], "%f", fDefOutH);
    sscanf(dsConfigs[shift + UN_CONFIG_CPC_CONTROLLER_ADDITIONAL_DEFV_OUTL], "%f", fDefOutL);

    int iResult = dpSet(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefKc", fDefKc,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefTi", fDefTi,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefTd", fDefTd,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefTds", fDefTds,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefSP", fDefSP,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefSPH", fDefSPH,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefSPL", fDefSPL,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefOutH", fDefOutH,
                        dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DefOutL", fDefOutL);
    if (iResult == -1) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_setCustomConfig: Controller Dflt DPEs did not set properly", "");
        return;
    }
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD_KEY, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD]), exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_setCustomConfig: Controller scaling method did not set properly", "");
        return;
    }

    string outDevices = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_OUT_DEVICES, exceptionInfo);
    if (outDevices != "") {
        unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], UN_CPC_DEVICE_PROPERTY_PREFIX + CPC_CONFIG_OUT_DEVICES, strsplit(outDevices, "|"), exceptionInfo);
    }
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_Controller Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_ControllerConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo, bool bExportOnlineValuesAsDefault = FALSE)
{
  int iLoop, iLen;
  float fDefKc, fDefTi, fDefTd, fDefTds, fDefSP, fDefSPH,  fDefSPL, fDefOutH, fDefOutL;
  string sObject, sCurrentDp, sMask;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_CONTROLLER_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getPIDName(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters, ".ProcessInput.MV");

    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters, ".ProcessInput.MV");

    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.ActSP");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_ExportConfig() -> Error exporting ranges for ActSP, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters, ".ProcessInput.OutOVSt");
    cpcExportGenericFunctions_getSimpleFormat(sCurrentDp, dsDpParameters, ".ProcessInput.OutOVSt");
    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.OutOVSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_ExportConfig() -> Error exporting ranges for OutOVSt, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD_KEY, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_ExportConfig() -> Error exporting SCALING METHOD key / value, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.OutOVSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_ExportConfig() -> Error exporting OutOVSt deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.ActSP");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_ExportConfig() -> Error exporting ActSP deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp,
                                         dsDpParameters,
                                         ".ProcessInput.OutOVSt");
    cpcExportGenericFunctions_getArchive(sCurrentDp,
                                         dsDpParameters,
                                         ".ProcessInput.MV");

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg02",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg02", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActSP",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActSPL",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActSPH",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "OutOVSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActOutL",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActOutH",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MV",         TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MSPSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuSPSt",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuPosRSt",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActKc",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActTi",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActTd",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActTds",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg02",   FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosR",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MSP",        FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MSPL",       FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MSPH",       FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MOutL",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MOutH",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MKc",        FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MTi",        FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MTd",        FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MTds",       FALSE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    if( bExportOnlineValuesAsDefault )
    {
      // Replace default values with current online values
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActKc",   dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActTi",   dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActTd",   dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActTds",  dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActSP",   dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActSPH",  dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActSPL",  dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActOutH", dsDpParameters, "%f");
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.ActOutL", dsDpParameters, "%f");
    }
    else
    {
      // Use the regular default values
      CPC_ControllerConfig_getDefaultValuesAll(dsDpList[iLoop], fDefKc, fDefTi, fDefTd, fDefTds, fDefSP, fDefSPH, fDefSPL, fDefOutH, fDefOutL);

      // PID params
      sprintf(sMask, "%f", fDefKc);
      dynAppend(dsDpParameters, sMask);

      sprintf(sMask, "%f", fDefTi);
      dynAppend(dsDpParameters, sMask);

      sprintf(sMask, "%f", fDefTd);
      dynAppend(dsDpParameters, sMask);

      sprintf(sMask, "%f", fDefTds);
      dynAppend(dsDpParameters, sMask);

      // Sp, SP & OUT limits
      sprintf(sMask, "%f", fDefSP);
      dynAppend(dsDpParameters, sMask);

      sprintf(sMask, "%f", fDefSPH);
      dynAppend(dsDpParameters, sMask);

      sprintf(sMask, "%f", fDefSPL);
      dynAppend(dsDpParameters, sMask);

      sprintf(sMask, "%f", fDefOutH);
      dynAppend(dsDpParameters, sMask);

      sprintf(sMask, "%f", fDefOutL);
      dynAppend(dsDpParameters, sMask);
    }

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ControllerConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_ControllerConfig_ExportConfig()





/**
Purpose: Export CPC_Controller Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_Controller_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_Controller Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_Controller_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_Controller_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo);
}


void IEC104_CPC_Controller_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_Controller_ExportConfig() -> Error exporting Controller devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_Controller_ExportConfig()





/**
Purpose: Export CPC_Controller Devices and override the default parameters with the online values
         These are just simple wrappers to indicate that the device supports the feature

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_Controller_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_ControllerConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo, true);
}
_UnPlc_CPC_Controller_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_ControllerConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

OPCUA_CPC_Controller_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_ControllerConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo, true);
}




void IEC104_CPC_Controller_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  // Call CPC_ControllerConfig_ExportConfig with bExportOnlineValuesAsDefault = TRUE
  CPC_ControllerConfig_ExportConfig(dsDpList, exceptionInfo, TRUE);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_Controller_ExportConfigOnlineValues() -> Error exporting Controller devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_Controller_ExportConfigOnlineValues()





/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
//------------------------------------------------------------------------------------------------------------------------
// CPC_ControllerConfig_getDefaultValuesAll
/**
Purpose:  set the all default values (PID, SP, SP & OUT limits) for the Controller

Parameters:
		sDeviceDpName: string, input, the device DP
		fDefKc: float, output, the default P value
		fDefTi: float, output, the default I value
		fDefTd: float, output, the default D value
		fDefTds: float, output, the default TD value
        fDefSP: float, input, the default SP value
        fDefSPH: float, input, the default SP High limit value
        fDefSPL: float, input, the default SP Low limit value
        fDefOutH: float, input, the default OUT High limit value
        fDefOutL: float, input, the default OUT Low limit value
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.6
	. operating system: XP, linux..
	. distributed system: yes.
*/
CPC_ControllerConfig_getDefaultValuesAll(string sDeviceDpName,  float &fDefKc, float &fDefTi, float &fDefTd, float &fDefTds, float &fDefSP, float &fDefSPH, float &fDefSPL, float &fDefOutH, float &fDefOutL) {
    dpGet(sDeviceDpName + ".ProcessOutput.DefKc", fDefKc,
          sDeviceDpName + ".ProcessOutput.DefTi", fDefTi,
          sDeviceDpName + ".ProcessOutput.DefTd", fDefTd,
          sDeviceDpName + ".ProcessOutput.DefTds", fDefTds,
          sDeviceDpName + ".ProcessOutput.DefSP", fDefSP,
          sDeviceDpName + ".ProcessOutput.DefSPH", fDefSPH,
          sDeviceDpName + ".ProcessOutput.DefSPL", fDefSPL,
          sDeviceDpName + ".ProcessOutput.DefOutH", fDefOutH,
          sDeviceDpName + ".ProcessOutput.DefOutL", fDefOutL);
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions


//@}
