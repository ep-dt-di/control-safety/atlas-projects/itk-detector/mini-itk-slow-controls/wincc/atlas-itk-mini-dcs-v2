/**@file

// cpcDigitalAlarm.ctl
This library contains the widget, faceplate, etc. functions of DigitalAlarm.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of DigitalAlarm DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_DigitalAlarm_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.ISt", UN_ACKNOWLEDGE_PLC);
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_DigitalAlarm_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.ISt", value,
          deviceName + ".ProcessInput.ISt:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = value;
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_DigitalAlarm_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_ACK_TEXT, CPC_POPUPMENU_DIAG_INFO_TEXT, CPC_POPUPMENU_MASK_IST_TEXT,
                                            UN_POPUPMENU_TREND_TEXT, UN_POPUPMENU_BLOCK_PLC_TEXT, UN_POPUPMENU_FACEPLATE_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_DigitalAlarm_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName input, the device name

*/
CPC_DigitalAlarm_WidgetInitStatics(string sDeviceName)
{
  int iAlertType, iRes;
  string sDescription;


  iRes         = dpGet(sDeviceName + ".ProcessInput.ISt:_alert_hdl.._type", iAlertType);
  g_bBoolAlert = ((iRes >= 0) && (iAlertType == DPCONFIG_ALERT_BINARYSIGNAL));

  if( shapeExists("Description") )
  {
    sDescription = dpGetDescription(sDeviceName + ".");
    if( sDescription == sDeviceName + "." )
    {
      sDescription = "N/A";
    }
    setValue("Description", "text", sDescription);
  }

}





/** Returns the list of DigitalAlarm DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalAlarm_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.ISt:_alert_hdl.._active");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_DigitalAlarm_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bool bIsAlarmActive = false;
    if (g_bBoolAlert) {
        bIsAlarmActive = values[9];
    }

    string sBody1Color, sBody1BColor = "", sColorSelect = "", sWarningColor = "", sWarningLetter = "";
    string sAlarmTypeLetter = "", sAlarmTypeColor = "", sAlarmTextLetter = "", sAlarmTextColor = "";
    sBody1Color = "unDataNotValid";
    bool bLockVisible;
    bool bAlarmSt = getBit(bit32StsReg01, CPC_StsReg01_IST) == 1;

    if (!bIsAlarmActive) {
        sAlarmTextLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
        sAlarmTextColor = "unAlarmMasked";
    }

    if (!bStsReg01Invalid && unFaceplate_connectionValid(dpes)) {
        sBody1Color = bAlarmSt ? "cpcColor_Alarm_Bad" : "unAlarm_Ok";

        if (getBit(bit32StsReg01, CPC_StsReg01_ALUNACK) == 1) {
            sBody1Color = "cpcColor_Alarm_AlarmNotAck";
        }
        if (bAlarmSt) {
            sAlarmTypeLetter = CPC_WIDGET_ALARM;
            sAlarmTypeColor  = "cpcColor_Alarm_Bad";
            sBody1BColor = sBody1Color;
        }
        if (getBit(bit32StsReg01, CPC_StsReg01_MALBRST) == 1) { // Alarm blocked
            sAlarmTextLetter = CPC_WIDGET_TEXT_ALARM_BLOCKED;
            sAlarmTextColor = "unAlarmMasked";
            sBody1Color = "unAlarmMasked";
            sBody1BColor = "";
        }

        // Warning animation
        unGenericObject_WidgetWarningAnimation(bit32StsReg01,
                                               makeDynInt(CPC_StsReg01_CONFIGW, CPC_StsReg01_POSW, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW),
                                               makeDynString(CPC_WIDGET_TEXT_CONFIGW, CPC_WIDGET_TEXT_POSW, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR),
                                               sWarningLetter, sWarningColor);
    }

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue,
            bStsReg01Invalid,
            sWarningLetter, sWarningColor);
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sColorSelect, bLockVisible);
    if (g_bSystemConnected) {
        setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "AlarmType", "text", sAlarmTypeLetter, "AlarmType", "foreCol", sAlarmTypeColor,	//	"A"
                      "AlarmText", "text", sAlarmTextLetter, "AlarmText", "foreCol", sAlarmTextColor,	//	"B"
                      "ControlStateText", "text", "", "SelectArea", "foreCol", sColorSelect,
                      "LockBmp", "visible", bLockVisible, "Body1", "fill", "[solid]",
                      "Body1", "foreCol", sBody1Color, "Body1", "backCol", sBody1BColor,
                      "WidgetArea", "visible", true, "eventState", "visible", iMask == 0,
                      "commentsButton", "visible", isCommentsActive);
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_DigitalAlarm_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background", "Body1", "fill", "[solid]",
                  "WarningText", "text", "", "WidgetArea", "visible", false, "AlarmType", "text", "", "AlarmText", "text", "",
                  "eventState", "visible", false);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_DigitalAlarm_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_SET_DA_MAIL_SMS] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MASK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    if (cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.ISt") != "N") {
        buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    } else {
        buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_NOONE;
    }
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_DigitalAlarm_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_DigitalAlarm_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case CPC_FACEPLATE_BUTTON_SET_DA_MAIL_SMS:
                buttonEnabled = buttonEnabled && bSelected;
                break;
            case UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION:
                buttonEnabled = buttonEnabled && !getBit(stsReg01Value, CPC_StsReg01_AUIHMBST);
                break;
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
    setValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION, "text", getBit(stsReg01Value, CPC_StsReg01_MALBRST) ? "Deblock Alarm" : "Block Alarm");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_DigitalAlarm_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    dyn_string exceptionInfo;
    string systemName = unGenericDpFunctions_getSystemName(deviceName);
    g_params["AlAck"] = cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.ISt") != "N";
    g_params["alarmDelay"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_ALDT, exceptionInfo);
    g_params["alarmDelayDPe"] = dpAliasToName(systemName + g_params["alarmDelay"]) + "ProcessInput.PosSt";
    g_params["alarmDelayInhibited"] = dpExists(dpAliasToName(systemName + g_params["alarmDelay"]));
    g_params["alarmDelayFormat"] = g_params["alarmDelayInhibited"] ? dpGetFormat(g_params["alarmDelayDPe"]) : "%2.1f";
    g_params["alarmDelayUnit"] = g_params["alarmDelayInhibited"] ? dpGetUnit(g_params["alarmDelayDPe"]) : "s";
    g_params["alarmDelayIsValue"] = g_params["alarmDelayInhibited"] || g_params["alarmDelay"] == (string)((int) g_params["alarmDelay"]);
    g_params["MailSMS"] = cpcGenericObject_isInMailSMS(deviceName + ".ProcessInput.ISt");

    dyn_string alarmTypes;
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceName, CPC_CONFIG_TYPE_KEY, alarmTypes, exceptionInfo);
    setMultiValue("AlarmType.display", "visible", false,
                  "AlarmTypeNew", "visible", false);
    if (dynlen(alarmTypes) > 1) {
        dyn_string parents;
        unGenericDpFunctions_getKeyDeviceConfiguration(deviceName, CPC_CONFIG_PARENTS_KEY, parents, exceptionInfo);
        setMultiValue("AlarmTypeNew", "visible", true,
                      "AlarmTypeNew", "items", cpcGenericObject_translateMultiAlarmAcronym(alarmTypes, parents));
    } else {
        setMultiValue("AlarmType.display", "visible", true,
                      "AlarmType.display", "text", cpcGenericObject_translateAlarmAcronym(alarmTypes));
    }
    if (!g_params["alarmDelayInhibited"]) {
        cpcGenericObject_DisplayText(g_params["alarmDelay"], "PosSt", "unDisplayValue_Parameter", false);
    }
    setMultiValue("AlarmDelayButton", "enabled", g_params["alarmDelayInhibited"], "AlarmDelayButton", "text", g_params["alarmDelayInhibited"] ? g_params["alarmDelay"] : "none");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of DigitalAlarm DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalAlarm_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ISt:_alert_hdl.._act_state");
    if (g_params["alarmDelayInhibited"]) {
        dynAppend(dpes, g_params["alarmDelayDPe"]);
        dynAppend(dpes, g_params["alarmDelayDPe"] + ":_online.._invalid");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDpes    input, dyn_string,  The dpe names
@param daValues  input, dyn_anytype, The dpe values

*/
CPC_DigitalAlarm_FaceplateStatusAnimationCB(dyn_string dsDpes, dyn_anytype daValues)
{
  //bit32 b32StsReg01;
  //b32StsReg01 = unFaceplate_fetchAnimationCBValue(dsDpes, daValues, "ProcessInput.StsReg01");

  unFaceplate_animateInterlock(dsDpes, daValues, "CPC_StsReg01_IST",      "ISt", "CPC_StsReg01_ALUNACK");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_AUEAIST",  TRUE,  "cpcColor_Faceplate_Status");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_AUIHMBST", TRUE,  "cpcColor_Faceplate_Status");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_CONFIGW",  TRUE,  "cpcColor_Faceplate_Warning");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_MALBRST",  TRUE,  "cpcColor_Faceplate_Warning");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_POSW",     TRUE,  "cpcColor_Faceplate_Warning");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_IOERRORW", TRUE,  "cpcColor_Faceplate_Warning");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_IOSIMUW",  TRUE,  "cpcColor_Faceplate_Warning");
  unFaceplate_animateStatusBit(dsDpes, daValues, "CPC_StsReg01_ALUNACK",  TRUE,  "cpcColor_Alarm_Bad");

  cpcGenericObject_CheckboxAnimate("MailSMS", mappingHasKey(g_params, "MailSMS") && g_params["MailSMS"]);
  cpcGenericObject_animateMaskEvent(dsDpes, daValues);

  if( mappingHasKey(g_params, "alarmDelayInhibited") &&
      g_params["alarmDelayInhibited"]                &&
      mappingHasKey(g_params, "alarmDelayUnit")      &&
      mappingHasKey(g_params, "alarmDelayFormat")      )
  {
    unFaceplate_animateOnlineValue(dsDpes, daValues, "PosSt", g_params["alarmDelayUnit"], g_params["alarmDelayFormat"], "unDisplayValue_Parameter");
  }

}





//@}
