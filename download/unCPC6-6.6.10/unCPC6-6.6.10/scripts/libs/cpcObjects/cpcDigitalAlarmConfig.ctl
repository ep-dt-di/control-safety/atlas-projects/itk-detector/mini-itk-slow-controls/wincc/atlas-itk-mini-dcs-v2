/**@file

// cpcDigitalAlarmConfig.ctl
This library contains the import and export function of the CPC_DigitalAlarm.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_DIGITALALARM_DPT_NAME = "CPC_DigitalAlarm";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_DIGITALALARM_LENGTH = 9;

const unsigned UN_CONFIG_CPC_DIGITALALARM_ARCHIVE_ACTIVE             = 1;
const unsigned UN_CONFIG_CPC_DIGITALALARM_SMS_CATEGORIES             = 2;
const unsigned UN_CONFIG_CPC_DIGITALALARM_SMS_MESSAGE                = 3;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ACK_ALARM                  = 4;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ALARM_LEVEL                = 5;
const unsigned UN_CONFIG_CPC_DIGITALALARM_NORMAL_POSITION            = 6;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDRESS_STSREG01           = 7;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDRESS_EVSTSREG01         = 8;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDRESS_MANREG01           = 9;

const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_SECOND_ALIAS    = 7;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_DigitalAlarmConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"]		= ".ProcessInput.StsReg01";
    props["dataType"]		= CPC_UINT16;
    config["StsReg01"]		= props;

    mappingClear(props);
    props["hasArchive"]		= true;
    props["address"]		= ".ProcessInput.evStsReg01";
    props["dataType"]		= CPC_INT32;
    config["evStsReg01"]	= props;

    mappingClear(props);
    props["hasAcknowledge"]	= true;
    props["hasArchive"]		= true;
    props["address"]		= ".ProcessInput.ISt";
    props["dpe"]			= "StsReg01";
    props["bitPosition"]	= CPC_StsReg01_IST;
    props["isAlarm"]		= true;
    config["ISt"]			= props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlUnAck";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALUNACK;
    config["AlUnAck"]     = props;

    mappingClear(props);
    props["address"]		= ".ProcessOutput.ManReg01";
    props["dataType"]		= CPC_UINT16;
    config["ManReg01"]		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}





/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_DigitalAlarmConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_DIGITALALARM_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_DIGITALALARM_LENGTH;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_DIGITALALARM_ARCHIVE_ACTIVE;
  mTemp["SMS_CATEGORIES"]          = UN_CONFIG_CPC_DIGITALALARM_SMS_CATEGORIES;
  mTemp["SMS_MESSAGE"]             = UN_CONFIG_CPC_DIGITALALARM_SMS_MESSAGE;
  mTemp["ACK_ALARM"]               = UN_CONFIG_CPC_DIGITALALARM_ACK_ALARM;
  mTemp["ALARM_LEVEL"]             = UN_CONFIG_CPC_DIGITALALARM_ALARM_LEVEL;
  mTemp["NORMAL_POSITION"]         = UN_CONFIG_CPC_DIGITALALARM_NORMAL_POSITION;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_DIGITALALARM_ADDRESS_STSREG01;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_DIGITALALARM_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_DIGITALALARM_ADDRESS_MANREG01;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_DIGITALALARM_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_DIGITALALARM"] = mTemp;
}





/**
Returns the list of parameter names that passed via config line.
*/
dyn_string CPC_DigitalAlarmConfig_getParamNames() {
//begin_TAG_SCRIPT_DEVICE_TYPE_getParamNames
    return makeDynString(CPC_PARAMS_ALDT);
//end_TAG_SCRIPT_DEVICE_TYPE_getParamNames
}

/** Check custom configuration
*/
CPC_DigitalAlarmConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    int alarmLevel;
    unConfigGenericFunctions_checkInt(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_DIGITALALARM_ALARM_LEVEL], alarmLevel, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) { return; }
    if (alarmLevel < 0 || alarmLevel > 3) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalAlarmConfig_checkCustomConfig: wrong alarm level: '" + configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_DIGITALALARM_ALARM_LEVEL] + "'", "");
        return;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_DigitalAlarmConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_LEVEL, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_DIGITALALARM_ALARM_LEVEL]), exceptionInfo);

    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.ISt");
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_DigitalAlarm Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_DigitalAlarmConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_DIGITALALARM_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.ISt");
    dynRemove(dsDpParameters, dynlen(dsDpParameters));

    cpcExportGenericFunctions_getCategories(sCurrentDp, makeDynString(".ProcessInput.ISt"), dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalAlarmConfig_ExportConfig() -> Error exporting ISt categories, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_CONFIG_SMS_MESSAGE, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalAlarmConfig_ExportConfig() -> Error exporting SMS key / value, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getAcknowledgeAlarm(sCurrentDp, ".ProcessInput.ISt", dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalAlarmConfig_ExportConfig() -> Error exporting ISt acknowledge alarm, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_CONFIG_ALARM_LEVEL, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalAlarmConfig_ExportConfig() -> Error exporting ALARM LEVEL key / value, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getDigitalNormalPosition(sCurrentDp, sObject, makeDynString(".ProcessInput.ISt"), dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalAlarmConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalAlarmConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_DigitalAlarmConfig_ExportConfig()





/**
Purpose: Export CPC_DigitalAlarm Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_DigitalAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_DigitalAlarm Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_DigitalAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_DigitalAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_DigitalAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}


void IEC104_CPC_DigitalAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_DigitalAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_DigitalAlarm_ExportConfig() -> Error exporting CPC_DigitalAlarm devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_DigitalAlarm_ExportConfig()




//@}
