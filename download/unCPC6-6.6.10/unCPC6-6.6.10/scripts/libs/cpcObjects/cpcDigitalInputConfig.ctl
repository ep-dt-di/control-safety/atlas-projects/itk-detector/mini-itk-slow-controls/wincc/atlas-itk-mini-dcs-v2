/**@file

// cpcDigitalInputConfig.ctl
This library contains the import and export function of the CPC_DigitalInput.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_DIGITALINPUT_DPT_NAME = "CPC_DigitalInput";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_DIGITALINPUT_LENGTH  = 8;

const unsigned UN_CONFIG_CPC_DIGITALINPUT_ARCHIVE_ACTIVE     = 1;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_SMS_CATEGORIES    = 2;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_SMS_MESSAGE      = 3;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ACK_ALARM        = 4;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_NORMAL_POSITION     = 5;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDRESS_STSREG01     = 6;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDRESS_EVSTSREG01   = 7;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDRESS_MANREG01     = 8;

const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_SECOND_ALIAS    = 7;

// SOFT_FE constants
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ARCHIVE_ACTIVE        = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_SMS_CATEGORIES        = 2;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_SMS_MESSAGE           = 3;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ACK_ALARM             = 4;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_NORMAL_POSITION       = 5;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_LENGTH                = 5;

const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ADDITIONAL_LENGTH     = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ADDITIONAL_MASKEVENT  = 1;

//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_DigitalInputConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"]     = ".ProcessInput.StsReg01";
    props["dataType"]     = CPC_UINT16;
    config["StsReg01"]     = props;

    mappingClear(props);
    props["hasArchive"]   = true;
    props["address"]     = ".ProcessInput.evStsReg01";
    props["dataType"]     = CPC_INT32;
    config["evStsReg01"]   = props;

    mappingClear(props);
    props["hasAcknowledge"] = true;
    props["hasArchive"]   = true;
    props["isAlarm"]    = true;
    props["address"]     = ".ProcessInput.PosSt";
    props["dpe"]      = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_POSST;
    config["PosSt"]     = props;

    mappingClear(props);
    props["address"]     = ".ProcessOutput.ManReg01";
    props["dataType"]     = CPC_UINT16;
    config["ManReg01"]     = props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_DigitalInputConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_DIGITALINPUT_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_DIGITALINPUT_LENGTH;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_DIGITALINPUT_ARCHIVE_ACTIVE;
  mTemp["SMS_CATEGORIES"]          = UN_CONFIG_CPC_DIGITALINPUT_SMS_CATEGORIES;
  mTemp["SMS_MESSAGE"]             = UN_CONFIG_CPC_DIGITALINPUT_SMS_MESSAGE;
  mTemp["ACK_ALARM"]               = UN_CONFIG_CPC_DIGITALINPUT_ACK_ALARM;
  mTemp["NORMAL_POSITION"]         = UN_CONFIG_CPC_DIGITALINPUT_NORMAL_POSITION;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_DIGITALINPUT_ADDRESS_STSREG01;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_DIGITALINPUT_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_DIGITALINPUT_ADDRESS_MANREG01;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_DIGITALINPUT_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_DIGITALINPUT"] = mTemp;


  mappingClear(mTemp);
  mTemp["ARCHIVE_ACTIVE"]       = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ARCHIVE_ACTIVE;
  mTemp["SMS_CATEGORIES"]       = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_SMS_CATEGORIES;
  mTemp["SMS_MESSAGE"]          = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_SMS_MESSAGE;
  mTemp["ACK_ALARM"]            = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ACK_ALARM;
  mTemp["NORMAL_POSITION"]      = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_NORMAL_POSITION;
  mTemp["LENGTH"]               = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_LENGTH;
  mTemp["ADDITIONAL_LENGTH"]    = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"] = UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT_ADDITIONAL_MASKEVENT;

  g_mCpcConst["UN_CONFIG_SOFT_FE_CPC_DIGITALINPUT"] = mTemp;
}





/** Set custom configuration
*/
CPC_DigitalInputConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    unGenericDpFunctions_setFaceplateRange(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], makeDynString("PosSt"), makeDynString(UN_FACEPLATE_TREND_DIGITAL_DEFAULT_MAX), makeDynString(UN_FACEPLATE_TREND_DIGITAL_DEFAULT_MIN), exceptionInfo);

    if (dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_DIGITALINPUT_ADDRESS_STSREG01] == "") {
        dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.StsReg01", pow(2, CPC_StsReg01_AUMOST) + pow(2, CPC_StsReg01_AUINHFMO));
    }
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_DigitalInput Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_DigitalInputConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_DIGITALINPUT_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);
    dynRemove(dsDpParameters, dynlen(dsDpParameters));

    cpcExportGenericFunctions_getCategories(sCurrentDp, makeDynString(".ProcessInput.PosSt"), dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalInputConfig_ExportConfig() -> Error exporting PosSt categories, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_CONFIG_SMS_MESSAGE, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalInputConfig_ExportConfig() -> Error exporting SMS key / value, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getAcknowledgeAlarm(sCurrentDp, ".ProcessInput.PosSt", dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalInputConfig_ExportConfig() -> Error exporting PosSt alarm acknowledge, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getDigitalNormalPosition(sCurrentDp, sObject, makeDynString(".ProcessInput.PosSt"), dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalInputConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalInputConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_DigitalInputConfig_ExportConfig()








/**
Purpose: Export CPC_DigitalInput Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_DigitalInput Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

CMW_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

SNMP_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

DIP_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
}




void IEC104_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_DigitalInputConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_DigitalInput_ExportConfig() -> Error exporting CPC_DigitalInput devices, due to: " + exceptionInfo[2], "");
    return;
  }


}//  IEC104_CPC_DigitalInput_ExportConfig()






/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
void SOFT_FE_CPC_DigitalInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    string sObject = UN_CONFIG_CPC_DIGITALINPUT_DPT_NAME;
    int amount = dynlen(dsDpList);
    for (int i = 1; i <= amount; i++) {
        dyn_string dsDpParameters;
        string currentDP = dsDpList[i];

        if (!dpExists(currentDP)) { continue; }

        unExportDevice_getAllCommonParameters(currentDP, sObject, dsDpParameters);
        cpcExportGenericFunctions_getArchive(currentDP, dsDpParameters);
        dynRemove(dsDpParameters, dynlen(dsDpParameters));
        cpcExportGenericFunctions_getCategories(currentDP, makeDynString(".ProcessInput.PosSt"), dsDpParameters, exceptionInfo);
        cpcExportGenericFunctions_getKeyDeviceConfiguration(currentDP, dsDpParameters, CPC_CONFIG_SMS_MESSAGE, exceptionInfo);
        cpcExportGenericFunctions_getAcknowledgeAlarm(currentDP, ".ProcessInput.PosSt", dsDpParameters, exceptionInfo);
        cpcExportGenericFunctions_getDigitalNormalPosition(currentDP, sObject, makeDynString(".ProcessInput.PosSt"), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_1, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_2, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_3, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT), dsDpParameters);
        cpcExportGenericFunctions_getMaskEvent(currentDP, dsDpParameters);

        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
}

//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions


//@}
