/**@file

// cpcDigitalOutput.ctl
This library contains the widget, faceplate, etc. functions of DigitalOutput.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of DigitalOutput DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_DigitalOutput_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    CPC_DigitalInput_AcknowledgeAlarm(deviceName, dpType, dsNeedAck);
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_DigitalOutput_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    CPC_DigitalInput_ObjectListGetValueTime(deviceName, deviceType, dsReturnData);
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_DigitalOutput_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    CPC_DigitalInput_MenuConfiguration(deviceName, dpType, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_DigitalOutput_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    CPC_DigitalInput_HandleMenu(deviceName, dpType, menuList, menuAnswer);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_DigitalOutput_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    CPC_DigitalInput_WidgetInitStatics(deviceName);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of DigitalOutput alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalOutput_WidgetLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
    CPC_DigitalInput_WidgetLockDPEs(deviceName, dpes);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
}

/** Returns the list of DigitalOutput DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalOutput_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    CPC_DigitalInput_WidgetDPEs(deviceName, dpes);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_DigitalOutput_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    CPC_DigitalInput_WidgetAnimation(dpes, values, widgetType);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_DigitalOutput_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    CPC_DigitalInput_WidgetDisconnection(sWidgetType);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_DigitalOutput_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons = CPC_DigitalInput_ButtonConfig(deviceName);
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_DigitalOutput_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    CPC_DigitalInput_ButtonDPEs(deviceName, dpes);
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_DigitalOutput_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];

    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_FORCED_MODE:
                bit1 = !(getBit(stsReg01Value, CPC_StsReg01_FOMOST) == 0) && !bStsReg01Bad;
                bit2 = !(getBit(stsReg01Value, CPC_StsReg01_AUINHFMO) == 0) && !bStsReg01Bad;
                buttonEnabled = buttonEnabled && bSelected && !(bit1 || bit2);
                break;
            case CPC_FACEPLACE_BUTTON_BLOCK_IOERROR:
                buttonEnabled = (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_FORCED_MODE) > 0) && bSelected && !bStsReg01Bad; // same access rights as "Forced mode" button
                break;
            case UN_FACEPLATE_BUTTON_ONOPEN_REQUEST:
            case UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST:
            case UN_FACEPLATE_BUTTON_AUTO_MODE:
                bit1 = !(getBit(stsReg01Value, CPC_StsReg01_AUMOST) == 0) && !bStsReg01Bad;
                buttonEnabled = buttonEnabled && bSelected && !bit1;
                break;
            case UN_FACEPLATE_BUTTON_ACK_ALARM:
                buttonEnabled = buttonEnabled && bSelected && cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.PosSt") == "Y";
                break;
            case UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS:
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_DigitalOutput_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    CPC_DigitalInput_FaceplateInitStatics(deviceName);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of DigitalOutput alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalOutput_FaceplateLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
    CPC_DigitalInput_FaceplateLockDPEs(deviceName, dpes);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
}

/** Returns the list of DigitalOutput DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalOutput_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    CPC_DigitalInput_FaceplateDPEs(deviceName, dpes);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_DigitalOutput_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    CPC_DigitalInput_FaceplateStatusAnimationCB(dpes, values);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}