/**@file

// cpcDigitalParameter.ctl
This library contains the widget, faceplate, etc. functions of DigitalParameter.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_DigitalParameter_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.StsReg01", value,
          deviceName + ".ProcessInput.StsReg01:_online.._stime", sTime);

    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.StsReg01"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.StsReg01"); // formated value with unit
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_DigitalParameter_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_MASK_POSST_TEXT,
                                            UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_DigitalParameter_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Returns the list of DigitalParameter DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalParameter_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_DigitalParameter_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bool posSt = values[9];
    bool posStInvalid = values[10];

    string sbody1Color, sbody2Color, body1fill = "[outline]", body2fill = "[outline]";
    string selectColor, sWarningLetter, sWarningColor;
    bool bLockVisible;
    
    dyn_string exceptionInfo;

    if (bStsReg01Invalid || posStInvalid || cpcGenericObject_SystemInvalid(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue)) {
        sbody1Color = "unDataNotValid";
        sbody2Color = "unDataNotValid";
    } else {
        sbody1Color = "cpcColor_Widget_Status";
        sbody2Color = "cpcColor_Widget_Request";
        if (posSt) {
            body1fill = "[solid]";
        }
        if (getBit(bit32StsReg01, CPC_StsReg01_MONRST) == 1) {
            body2fill = "[solid]";
        }
    }

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue,
            bStsReg01Invalid,
            sWarningLetter, sWarningColor);
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, bLockVisible);
    if (g_bSystemConnected) {
        setMultiValue("WarningText", "text", sWarningLetter,
                      "WarningText", "foreCol", sWarningColor,
                      "SelectArea", "foreCol", selectColor,
                      "LockBmp", "visible", bLockVisible,
                      "WidgetArea", "visible", true,
                      "eventState", "visible", iMask == 0,
                      "commentsButton", "visible", isCommentsActive);
        if(widgetType == "DPKnob"){
            
            float fRotation = 0.0;
            if(getBit(bit32StsReg01, CPC_StsReg01_MONRST) == 1 && getBit(bit32StsReg01, CPC_StsReg01_MOFFRST) == 0) {
                fRotation = 315.0;
            } 
            else if(getBit(bit32StsReg01, CPC_StsReg01_MONRST) == 0 && getBit(bit32StsReg01, CPC_StsReg01_MOFFRST) == 1) {
                fRotation = 45.0;
            }
            string sLabelOn = cpcGenericDpFunctions_getDeviceProperty(g_sDpName, CPC_CONFIG_LABEL_ON, exceptionInfo, "On");
            if(strrtrim(strltrim(sLabelOn))==""){
                sLabelOn = "On";
            }
            string sLabelOff = cpcGenericDpFunctions_getDeviceProperty(g_sDpName, CPC_CONFIG_LABEL_OFF, exceptionInfo, "Off");
            if(strrtrim(strltrim(sLabelOff))==""){
                sLabelOff = "Off";
            }
            
            setMultiValue("Body1", "foreCol", sbody2Color, 
                          "Body2", "foreCol", sbody2Color, 
                          "Body2", "backCol", sbody2Color,
                          "Body2", "rotation", fRotation,
                          "OnLabel", "foreCol", posSt?"unWidget_Background":sbody1Color,
                          "OnLabel", "backCol", posSt?sbody1Color:"_Transparent",
                          "OnLabel", "text", sLabelOn,
                          "OffLabel", "foreCol", posSt?sbody1Color:"unWidget_Background",
                          "OffLabel", "backCol", posSt?"_Transparent":sbody1Color,
                          "OffLabel", "text", sLabelOff
                          );
        } 
        else {
            setMultiValue("Body1", "foreCol", sbody1Color, "Body1", "backCol", sbody1Color, "Body1", "fill", body1fill,
                          "Body2", "foreCol", sbody2Color, "Body2", "backCol", sbody2Color, "Body2", "fill", body2fill);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_DigitalParameter_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    setMultiValue("Body1", "foreCol", "unDataNoAccess",
                  "Body2", "foreCol", "unDataNoAccess",
                  "WarningText", "text", "",
                  "SelectArea", "foreCol", "",
                  "LockBmp", "visible", false,
                  "WidgetArea", "visible", false,
                  "eventState", "visible", false);
    if(sWidgetType == "DPKnob"){
        setMultiValue(
            "OffLabel", "foreCol", "unDataNoAccess",
            "OnLabel", "foreCol", "unDataNoAccess",
            "Body2", "backCol", "unDataNoAccess"
        );
    } else {
        setMultiValue(
            "Body1", "fill", "[solid]",
            "Body2", "fill", "[solid]",
            "Body1", "backCol", "unWidget_Background",
            "Body2", "backCol", "unWidget_Background"
        );
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_DigitalParameter_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_DIGITALPARAMETER_SET_VALUE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_DigitalParameter_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_DigitalParameter_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access

        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled && bSelected);
        }

    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Returns the list of DigitalParameter DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalParameter_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessOutput.DfltVal");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_DigitalParameter_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    if(g_bSystemConnected) {
        bool posSt, posStInvalid;
        unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "PosSt", posSt, posStInvalid);
        unGenericObject_ColorBoxAnimate("POSST", posSt, true, "cpcColor_Faceplate_Status",
                    posStInvalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
    } else { // disconnection animation
        unGenericObject_ColorBoxDisconnect(makeDynString("POSST"));
    }

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MONRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MOFFRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ARMRCPST", true, "cpcColor_Faceplate_Status");
    cpcGenericObject_CheckboxAnimate("DefaultValue", unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessOutput.DfltVal"));
    cpcGenericObject_animateMaskEvent(dpes, values);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}