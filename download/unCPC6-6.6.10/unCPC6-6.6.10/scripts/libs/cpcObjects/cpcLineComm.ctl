/**@name LIBRARY: unLineComm.ctl

@author: Arnaud BLAIN (AB-CO)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

Creation Date: 25/01/2007

Modification History:
    19/10/2011: Alexey
        ported to CPC6


Internal Functions :
    . unDigitalAlarm_LineCommWidgetRegisterCB : register callback Function
    . unDigitalAlarm_LineCommDisconnection : animate widget disconnection
    . unDigitalAlarm_LineCommAnimationCB : animate widget


Purpose: This library contains functions for the LineComm widget.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
    . Alarm object (DPType etc.)
    . UNICOS (DPType etc.)
    . PVSS version: 3.0
    . operating system: WXP.
    . distributed system: yes.
*/

//---------------------------------------------------------------------------------------------------------------------------------------
// unDigitalAlarm_LineCommWidgetRegisterCB
/**
Purpose: register callback function

Parameters:
	- sDp, string, input, dp
	- bSystemConnected, bool, input, state of the remote system

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Line Comm
	. PVSS version: 3.0
	. operating system: WXP.
	. distributed system: yes.
  @reviewed 2018-06-25 @whitelisted{Callback}
*/
unDigitalAlarm_LineCommWidgetRegisterCB(string sDp, bool bSystemConnected) {
    dyn_string exceptionInfo;
    string deviceName;
    int iAction, iRes;
    string sPlcName, sSystemName;
    bool bRemote;

    deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
    if (deviceName == "") { // In case of disconnection
        deviceName = g_sDpName;
    }
    sSystemName = unGenericDpFunctions_getSystemName(deviceName);
    sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);

    unDistributedControl_isRemote(bRemote, sSystemName);
    g_bSystemConnected = bRemote ? bSystemConnected : true;
    g_bUnSystemAlarmPlc = (sPlcName != "") && dpExists(sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName);

    unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
    switch (iAction) {
        case UN_ACTION_DISCONNECT:
            unDigitalAlarm_LineCommDisconnection(g_sWidgetType);
            break;
        case UN_ACTION_DPCONNECT:
            g_sDpName = deviceName;
            if (dpTypeName(deviceName) != UN_CONFIG_CPC_DIGITALALARM_DPT_NAME) {
                g_bUnSystemAlarmPlc = false;
            }

            if (g_bUnSystemAlarmPlc) {
                iRes = dpConnect("unDigitalAlarm_LineCommAnimationCB",
                                 deviceName + ".ProcessInput.StsReg01",
                                 deviceName + ".ProcessInput.StsReg01:_online.._invalid",
                                 deviceName + ".ProcessInput.ISt",
                                 sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".alarm",
                                 sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".enabled");
                g_bCallbackConnected = (iRes >= 0);
            } else {
                unDigitalAlarm_LineCommDisconnection(g_sWidgetType);
            }
            break;
        case UN_ACTION_DPDISCONNECT_DISCONNECT:
            dpDisconnect("unDigitalAlarm_LineCommAnimationCB",
                         deviceName + ".ProcessInput.StsReg01",
                         deviceName + ".ProcessInput.StsReg01:_online.._invalid",
                         deviceName + ".ProcessInput.ISt",
                         sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".alarm",
                         sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".enabled");

            g_bCallbackConnected = !(iRes >= 0);
            unDigitalAlarm_LineCommDisconnection(g_sWidgetType);
            break;
        case UN_ACTION_NOTHING:
        default:
            break;
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------

//unDigitalAlarm_LineCommDisconnection
/**
Purpose: Animate LineComm widget disconnection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: WXP.
	. distributed system: yes.
*/

unDigitalAlarm_LineCommDisconnection(string sWidgetType) {
    setValue("LineComm", "foreCol", "_dpdoesnotexist");
}

//---------------------------------------------------------------------------------------------------------------------------------------

//unDigitalAlarm_LineCommAnimationCB
/**
Purpose: Animate widget through Alarm status

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-25 @whitelisted{Callback}
*/
unDigitalAlarm_LineCommAnimationCB(string sDpStsReg01, bit32 bit32StsReg01, string sDp4, bool bStsReg01Invalid, string sDeviceName, bool bISt, string sDpPLCAlarm, int iAlarm, string sDpPLCEnabled, bool bEnabled) {
    if (bStsReg01Invalid || cpcGenericObject_SystemInvalid(bEnabled, iAlarm)) {
        setValue( "LineComm", "foreCol", "unDataNotValid");
    } else {
        if (bISt) {
            setValue( "LineComm", "foreCol", "{255,0,0}"); //if Communication BAD : RED
        } else {
            setValue( "LineComm", "foreCol", "{0,255,0}"); //if Communication OK : GREEN
        }
    }
}
