/**@file

// cpcMassFlowController.ctl
This library contains the widget, faceplate, etc. functions of MassFlowController.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of MassFlowController DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_MassFlowController_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.StartISt", "ProcessInput.TStopISt", "ProcessInput.FuStopISt", "ProcessInput.AlSt", UN_ACKNOWLEDGE_PLC);
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_MassFlowController_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.PosSt", value,
          deviceName + ".ProcessInput.PosSt:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.PosSt"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_MassFlowController_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, CPC_POPUPMENU_ALARMS_TEXT, UN_POPUPMENU_TREND_TEXT, CPC_POPUPMENU_ALLOW_TO_RESTART_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_MassFlowController_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_MassFlowController_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    int iRes, iCtrlRes, iAlertType1, iAlertType2, iAlertType3, iAlertType4;
    dyn_string exceptionInfo;

    iRes = dpGet(deviceName + ".ProcessInput.StartISt:_alert_hdl.._type", iAlertType1,
                 deviceName + ".ProcessInput.TStopISt:_alert_hdl.._type", iAlertType2,
                 deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._type", iAlertType3,
                 deviceName + ".ProcessInput.AlSt:_alert_hdl.._type", iAlertType4);

    g_bBoolAlert = ((iRes >= 0) && (iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType2 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType3 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType4 == DPCONFIG_ALERT_BINARYSIGNAL));
    g_bMRestart = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
    g_sPosStFormat = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_sPosStUnit = dpGetUnit(deviceName + ".ProcessInput.PosSt");

    string sDeviceLink;
    unGenericDpFunctions_getDeviceLink(deviceName, sDeviceLink);
    g_dsCtrl = cpcGenericObject_getCtrlLink(unGenericDpFunctions_getSystemName(deviceName), strsplit(sDeviceLink, UN_CONFIG_GROUP_SEPARATOR));
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of MassFlowController DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_MassFlowController_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.StartISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.TStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.AlSt:_alert_hdl.._act_state");
    }
    for (int i = 1; i <= dynlen(g_dsCtrl); i++) {
        dynAppend(dpes, g_dsCtrl[i]);
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_MassFlowController_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iAlValue = values[1];
    bool bPlcEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    // unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "StsReg01", bit32StsReg01, bStsReg01Invalid);
    // unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "StsReg02", bit32StsReg02, bStsReg02Invalid);
    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bit32 bit32StsReg02 = values[9];
    bool bStsReg02Invalid = values[10];
    float fPosRSt = values[11];
    bool bPosRStInvalid = values[12];
    float fPosSt = values[13];
    bool bPosStInvalid = values[14];

    int iStartIStState = 0, iTStopIStState = 0, iFuStopIStState = 0, iAlState = 0;
    if (g_bBoolAlert) {
        iStartIStState = values[15];
        iTStopIStState = values[16];
        iFuStopIStState = values[17];
        iAlState = values[18];
    }

    cpcGenericObject_WidgetAnimationDoubleStsReg(g_sWidgetType, UN_CONFIG_CPC_MASSFLOWCONTROLLER_DPT_NAME,
            bLocked, sSelectedManager, bit32StsReg01, bit32StsReg02,
            makeDynInt(CPC_StsReg01_CONFIGW, CPC_StsReg01_AUMRW, CPC_StsReg01_POSALW, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW),
            makeDynInt(CPC_StsReg02_ALBW),
            makeDynString(CPC_WIDGET_TEXT_CONFIGW, CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR),
            makeDynString(CPC_WIDGET_TEXT_WARNING_BLOCKED),
            makeDynInt(CPC_StsReg01_ALST, CPC_StsReg01_STARTIST, CPC_StsReg01_TSTOPIST),
            makeDynString(CPC_WIDGET_TEXT_ALARM_ALST, CPC_WIDGET_TEXT_ALARM_STARTIST, CPC_WIDGET_TEXT_ALARM_TSTOPIST),
            makeDynInt(CPC_StsReg02_FUSTOPIST),
            makeDynString(CPC_WIDGET_TEXT_ALARM_FUSTOPIST),
            iStartIStState, iTStopIStState, iFuStopIStState, iAlState,
            makeDynString("CPC_StsReg01_FOMOST", "CPC_StsReg01_MMOST", "CPC_StsReg02_SOFTLDST", "CPC_StsReg01_AUIHFOMOST", "CPC_StsReg01_AUINHMMO"),
            "CPC_StsReg01_ONST", "CPC_StsReg01_OFFST", CPC_StsReg01_ALUNACK,
            bStsReg01Invalid || bStsReg02Invalid || bPosStInvalid || bPosRStInvalid,
            bStsReg01Invalid || bStsReg02Invalid,
            iAlValue, bPlcEnabled);

    cpcGenericObject_WidgetDisplayValueAnimation("Display1", g_sPosStUnit, g_sPosStFormat, fPosRSt, "cpcColor_Widget_Request", bPosRStInvalid);
    cpcGenericObject_WidgetDisplayValueAnimation("Display2", g_sPosStUnit, g_sPosStFormat, fPosSt, "cpcColor_Widget_Status", bPosStInvalid);
    if (g_bSystemConnected) {
        eventState.visible = (iMask == 0);
        commentsButton.visible = isCommentsActive;
    }

    cpcWidget_WidgetCtrlAnimationCB(dpes, values, g_dsCtrl);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_MassFlowController_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    cpcGenericObject_WidgetDisconnection(sWidgetType, UN_CONFIG_CPC_MASSFLOWCONTROLLER_DPT_NAME);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_MassFlowController_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_AUTO_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MANUAL_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_ALLOW_RESTART] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_MFC_PARAMS] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_FORCED_MODE] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_MassFlowController_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_MassFlowController_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];
    mixed stsReg02Value = dsData[5];
    bool bStsReg02Bad = dsData[6];
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_FORCED_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_FOMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHFMO) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_MANUAL_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_MMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHMMO) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_AUTO_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case CPC_FACEPLATE_BUTTON_MFC_PARAMS:
                buttonEnabled = buttonEnabled && bSelected;
                break;
            default:
                break;
        }

        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else if (dsButtons[i] == CPC_FACEPLATE_BUTTON_ALLOW_RESTART || dsButtons[i] == UN_FACEPLATE_BUTTON_ACK_ALARM) {
            cpcButton_ButtonSetState(makeDynString(CPC_FACEPLATE_BUTTON_ALLOW_RESTART, UN_FACEPLATE_BUTTON_ACK_ALARM), dsUserAccess, deviceName, dpType, dsData, exceptionInfo);
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_MassFlowController_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    dyn_string exceptionInfo;
    int iAlertRes, iAlertType1, iAlertType2, iAlertType3, iAlertType4;
    iAlertRes = dpGet(deviceName + ".ProcessInput.StartISt:_alert_hdl.._type", iAlertType1,
                      deviceName + ".ProcessInput.TStopISt:_alert_hdl.._type", iAlertType2,
                      deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._type", iAlertType3,
                      deviceName + ".ProcessInput.AlSt:_alert_hdl.._type", iAlertType4);
    if (iAlertRes >= 0) {
        g_params["BoolAlert"] = ((iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType2 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType3 == DPCONFIG_ALERT_BINARYSIGNAL));
    } else {
        dynAppend(exceptionInfo, "CPC_MassFlowController_FaceplateConnect: problems to get alert handling data, object:" + deviceName);
    }

    g_params["MRestart"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
    g_params["FSPosOn"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_FSPOSON, exceptionInfo);
    g_params["NoiseFilter"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_NOISE_FILTER, exceptionInfo);
    g_params["FeedbackSimulator"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_FEEDBACK_SIM, exceptionInfo);
    g_params["InhibitManualTotalizer"] = (bool) cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_INHIBITMANUALTOTALIZER, exceptionInfo);
    g_params["PosStFormat"] = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_params["PosStUnit"] = dpGetUnit(deviceName + ".ProcessInput.PosSt");
    g_params["VoTStFormat"] = dpGetFormat(deviceName + ".ProcessInput.VoTSt");
    g_params["VoTStUnit"] = dpGetUnit(deviceName + ".ProcessInput.VoTSt");

    string flowUnit, gasName;
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceName, CPC_MFC_CALIBRATION_UNIT, flowUnit, exceptionInfo);
    unGenericDpFunctions_getKeyDeviceConfiguration(deviceName, CPC_MFC_CALIBRATION_GAS_NAME, gasName, exceptionInfo);
    g_params["Calibration"] = gasName + " - " + flowUnit;

    mapping CCLabels, VoTLabels, DMLabels;
    cpcGenericObject_StringToMapping(cpcGenericDpFunctions_getMapping(deviceName, ".ProcessInput.ActCC"), CCLabels);
    cpcGenericObject_StringToMapping(cpcGenericDpFunctions_getMapping(deviceName, ".ProcessInput.ActVoTMo"), VoTLabels);
    cpcGenericObject_StringToMapping(cpcGenericDpFunctions_getMapping(deviceName, ".ProcessInput.ActDMo"), DMLabels);
    g_params["CCLabels"] = CCLabels;
    g_params["VoTLabels"] = VoTLabels;
    g_params["DMLabels"] = DMLabels;
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of MassFlowController DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_MassFlowController_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.VoTSt");
    dynAppend(dpes, deviceName + ".ProcessInput.VoTSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuVoTOfSt");
    dynAppend(dpes, deviceName + ".ProcessInput.AuVoTOfSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MaxPosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MaxPosSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MDMoRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MDMoRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuDMoRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.AuDMoRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActDMo");
    dynAppend(dpes, deviceName + ".ProcessInput.ActDMo:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MVoTMRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MVoTMRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuVoTMRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.AuVoTMRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActVoTMo");
    dynAppend(dpes, deviceName + ".ProcessInput.ActVoTMo:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MCCRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MCCRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuCCRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.AuCCRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActCC");
    dynAppend(dpes, deviceName + ".ProcessInput.ActCC:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.MPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuPosRSt");
    dynAppend(dpes, deviceName + ".ProcessInput.AuPosRSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutOV");
    dynAppend(dpes, deviceName + ".ProcessInput.ActOutOV:_online.._invalid");

    if (g_params["BoolAlert"]) {
        dynAppend(dpes, deviceName + ".ProcessInput.StartISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.TStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.AlSt:_alert_hdl.._act_state");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_MassFlowController_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    if (mappingHasKey(g_params, "PosStUnit") && mappingHasKey(g_params, "PosStFormat")) {
        unFaceplate_animateOnlineValue(dpes, values, "PosSt", g_params["PosStUnit"], g_params["PosStFormat"], "cpcColor_Faceplate_Status");
        unFaceplate_animateOnlineValue(dpes, values, "MaxPosSt", g_params["PosStUnit"], g_params["PosStFormat"], "cpcColor_Faceplate_Status");
        
        unFaceplate_animateOnlineValue(dpes, values, "AuPosRSt", g_params["PosStUnit"], g_params["PosStFormat"], "cpcColor_Faceplate_Status");
        unFaceplate_animateOnlineValue(dpes, values, "MPosRSt",  g_params["PosStUnit"], g_params["PosStFormat"], "cpcColor_Faceplate_Status");
        unFaceplate_animateOnlineValue(dpes, values, "ActOutOV", g_params["PosStUnit"], g_params["PosStFormat"], "cpcColor_Faceplate_Status");
    }
    if (mappingHasKey(g_params, "VoTStUnit") && mappingHasKey(g_params, "VoTStFormat")) {
        unFaceplate_animateOnlineValue(dpes, values, "VoTSt", g_params["VoTStUnit"], g_params["VoTStFormat"], "cpcColor_Faceplate_Status");
		unFaceplate_animateOnlineValue(dpes, values, "AuVoTOfSt", g_params["VoTStUnit"], g_params["VoTStFormat"], "cpcColor_Faceplate_Status");
    }
    if (mappingHasKey(g_params, "FSPosOn")) {
        cpcGenericObject_CheckboxAnimate("PFSPosOn", g_params["FSPosOn"]);
    }
    if (mappingHasKey(g_params, "NoiseFilter")) {
        cpcGenericObject_CheckboxAnimate("PNoiseFilter", g_params["NoiseFilter"]);
    }
    if (mappingHasKey(g_params, "FeedbackSimulator")) {
        cpcGenericObject_CheckboxAnimate("PFeedbackSim", g_params["FeedbackSimulator"]);
    }
    if (mappingHasKey(g_params, "Calibration")) {
        unFaceplate_animateTextParameter(dpes, values, "Calibration", g_params["Calibration"]);
    }
    
    if (mappingHasKey(g_params, "DMLabels")) {
        unFaceplate_animateTranslatedValue(dpes, values, "AuDMoRSt", "", "", g_params["DMLabels"], "cpcColor_Faceplate_Status");
        unFaceplate_animateTranslatedValue(dpes, values, "MDMoRSt",  "", "", g_params["DMLabels"], "cpcColor_Faceplate_Status");
        unFaceplate_animateTranslatedValue(dpes, values, "ActDMo",   "", "", g_params["DMLabels"], "cpcColor_Faceplate_Status");
    }
    
    if (mappingHasKey(g_params, "VoTLabels")) {
        unFaceplate_animateTranslatedValue(dpes, values, "AuVoTMRSt", "", "", g_params["VoTLabels"], "cpcColor_Faceplate_Status");
        unFaceplate_animateTranslatedValue(dpes, values, "MVoTMRSt",  "", "", g_params["VoTLabels"], "cpcColor_Faceplate_Status");
        unFaceplate_animateTranslatedValue(dpes, values, "ActVoTMo",  "", "", g_params["VoTLabels"], "cpcColor_Faceplate_Status");
    }
    
    if (mappingHasKey(g_params, "CCLabels")) {
        unFaceplate_animateTranslatedValue(dpes, values, "AuCCRSt", "", "", g_params["CCLabels"], "cpcColor_Faceplate_Status");
        unFaceplate_animateTranslatedValue(dpes, values, "MCCRSt",  "", "", g_params["CCLabels"], "cpcColor_Faceplate_Status");
        unFaceplate_animateTranslatedValue(dpes, values, "ActCC",   "", "", g_params["CCLabels"], "cpcColor_Faceplate_Status");
    }
    

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUINHMMO", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUIHFOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_SOFTLDST", true, "cpcColor_Faceplate_Status");

    if (mappingHasKey(g_params, "InhibitManualTotalizer")) {
        cpcGenericObject_ColorBoxAnimate("AUIHTOTST", g_params["InhibitManualTotalizer"], 1, "cpcColor_Faceplate_Status", unFaceplate_connectionValid(values));
    }

    if (mappingHasKey(g_params, "BoolAlert") && g_params["BoolAlert"]) {
        unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_STARTAL", "StartISt");
        unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_TSTOPAL", "TStopISt");
        unFaceplate_animateInterlock(dpes, values, "CPC_StsReg02_FUSTOPIST", "FuStopISt");
        unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_ALST", "AlSt");
        unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ALUNACK", true, "cpcColor_Alarm_Bad");
    }

    cpcGenericObject_animateMaskEvent(dpes, values);
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMRW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_ALBW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_POSALW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_CONFIGW", true, "cpcColor_Faceplate_Warning");
    bool needRestartInvalid, needRestart;
    unFaceplate_fetchStatusBitAndValidness(dpes, values, "CPC_StsReg02_NEEDRESTART", needRestart, needRestartInvalid);
    if (mappingHasKey(g_params, "MRestart")) {
        cpcGenericObject_ColorBoxAnimate("NeedRestart", g_params["MRestart"] && needRestart == 0, 1, "cpcColor_Faceplate_Warning",
                                     needRestartInvalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}