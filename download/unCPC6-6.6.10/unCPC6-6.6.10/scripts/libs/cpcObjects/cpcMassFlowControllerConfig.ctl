
/**@file

// cpcMassFlowControllerConfig.ctl
This library contains the import and export function of the CPC_MassFlowController.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_MASSFLOWCONTROLLER_DPT_NAME = "CPC_MassFlowController";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_LENGTH = 44;

const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_UNIT                           = 1;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_FORMAT                         = 2;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_DEADBAND                       = 3;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_DEADBAND_TYPE                  = 4;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSST_ARCHIVE_ACTIVE           = 5;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSST_ARCHIVE_TIME_FILTER      = 6;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSRST_ARCHIVE_ACTIVE          = 5;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSRST_ARCHIVE_TIME_FILTER     = 6;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTOUTOV_ARCHIVE_ACTIVE        = 5;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTOUTOV_ARCHIVE_TIME_FILTER   = 6;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_UNIT                     = 7;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_FORMAT                   = 8;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_DEADBAND                 = 9;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_DEADBAND_TYPE            = 10;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_ARCHIVE_ACTIVE           = 11;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_ARCHIVE_TIME_FILTER      = 12;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_DEADBAND             = 9;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_DEADBAND_TYPE        = 10;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_ARCHIVE_ACTIVE       = 11;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_ARCHIVE_TIME_FILTER  = 12;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUCCRST_ARCHIVE_ACTIVE         = 13;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUCCRST_ARCHIVE_TIME_FILTER    = 14;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUDMORST_ARCHIVE_ACTIVE        = 13;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUDMORST_ARCHIVE_TIME_FILTER   = 14;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTDMO_ARCHIVE_ACTIVE          = 13;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTDMO_ARCHIVE_TIME_FILTER     = 14;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTCC_ARCHIVE_ACTIVE           = 13;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTCC_ARCHIVE_TIME_FILTER      = 14;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_NORMAL_POSITION                = 15;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_STSREG01               = 16;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_STSREG02               = 17;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_EVSTSREG01             = 18;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_EVSTSREG02             = 19;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_POSST                  = 20;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_POSRST                 = 21;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MAXPOSST               = 22;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MPOSRST                = 23;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUPOSRST               = 24;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUVOTOFST              = 25;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUCCRST                = 26;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUDMORST               = 27;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUVOTMRST              = 28;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MVOTMRST               = 29;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MDMORST                = 30;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MCCRST                 = 31;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_VOTST                  = 32;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTOUTOV               = 33;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTDMO                 = 34;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTCC                  = 35;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTVOTMO               = 36;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MANREG01               = 37;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MPOSR                  = 38;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MDMOR                  = 39;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MCCR                   = 40;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MVOTMOR                = 41;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_MAPPING                        = 42;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_CALIB_UNIT                     = 43;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_CALIB_GAS_NAME                 = 44;

const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_SECOND_ALIAS    = 7;
//end_TAG_SCRIPT_DEVICE_TYPE_constants




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_MassFlowControllerConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                      = UN_CONFIG_CPC_MASSFLOWCONTROLLER_DPT_NAME;
  mTemp["LENGTH"]                        = UN_CONFIG_CPC_MASSFLOWCONTROLLER_LENGTH;
  mTemp["UNIT"]                          = UN_CONFIG_CPC_MASSFLOWCONTROLLER_UNIT;
  mTemp["FORMAT"]                        = UN_CONFIG_CPC_MASSFLOWCONTROLLER_FORMAT;
  mTemp["DEADBAND"]                      = UN_CONFIG_CPC_MASSFLOWCONTROLLER_DEADBAND;
  mTemp["DEADBAND_TYPE"]                 = UN_CONFIG_CPC_MASSFLOWCONTROLLER_DEADBAND_TYPE;
  mTemp["POSST_ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSST_ARCHIVE_ACTIVE;
  mTemp["POSST_ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSST_ARCHIVE_TIME_FILTER;
  mTemp["POSRST_ARCHIVE_ACTIVE"]         = UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSRST_ARCHIVE_ACTIVE;
  mTemp["POSRST_ARCHIVE_TIME_FILTER"]    = UN_CONFIG_CPC_MASSFLOWCONTROLLER_POSRST_ARCHIVE_TIME_FILTER;
  mTemp["ACTOUTOV_ARCHIVE_ACTIVE"]       = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTOUTOV_ARCHIVE_ACTIVE;
  mTemp["ACTOUTOV_ARCHIVE_TIME_FILTER"]  = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTOUTOV_ARCHIVE_TIME_FILTER;
  mTemp["VOTST_UNIT"]                    = UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_UNIT;
  mTemp["VOTST_FORMAT"]                  = UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_FORMAT;
  mTemp["VOTST_DEADBAND"]                = UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_DEADBAND;
  mTemp["VOTST_DEADBAND_TYPE"]           = UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_DEADBAND_TYPE;
  mTemp["VOTST_ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_ARCHIVE_ACTIVE;
  mTemp["VOTST_ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_MASSFLOWCONTROLLER_VOTST_ARCHIVE_TIME_FILTER;
  mTemp["AUVOTOFST_DEADBAND"]            = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_DEADBAND;
  mTemp["AUVOTOFST_DEADBAND_TYPE"]       = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_DEADBAND_TYPE;
  mTemp["AUVOTOFST_ARCHIVE_ACTIVE"]      = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_ARCHIVE_ACTIVE;
  mTemp["AUVOTOFST_ARCHIVE_TIME_FILTER"] = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUVOTOFST_ARCHIVE_TIME_FILTER;
  mTemp["AUCCRST_ARCHIVE_ACTIVE"]        = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUCCRST_ARCHIVE_ACTIVE;
  mTemp["AUCCRST_ARCHIVE_TIME_FILTER"]   = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUCCRST_ARCHIVE_TIME_FILTER;
  mTemp["AUDMORST_ARCHIVE_ACTIVE"]       = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUDMORST_ARCHIVE_ACTIVE;
  mTemp["AUDMORST_ARCHIVE_TIME_FILTER"]  = UN_CONFIG_CPC_MASSFLOWCONTROLLER_AUDMORST_ARCHIVE_TIME_FILTER;
  mTemp["ACTDMO_ARCHIVE_ACTIVE"]         = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTDMO_ARCHIVE_ACTIVE;
  mTemp["ACTDMO_ARCHIVE_TIME_FILTER"]    = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTDMO_ARCHIVE_TIME_FILTER;
  mTemp["ACTCC_ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTCC_ARCHIVE_ACTIVE;
  mTemp["ACTCC_ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ACTCC_ARCHIVE_TIME_FILTER;
  mTemp["NORMAL_POSITION"]               = UN_CONFIG_CPC_MASSFLOWCONTROLLER_NORMAL_POSITION;
  mTemp["ADDRESS_STSREG01"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_STSREG01;
  mTemp["ADDRESS_STSREG02"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_STSREG02;
  mTemp["ADDRESS_EVSTSREG01"]            = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_EVSTSREG02"]            = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_EVSTSREG02;
  mTemp["ADDRESS_POSST"]                 = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_POSST;
  mTemp["ADDRESS_POSRST"]                = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_POSRST;
  mTemp["ADDRESS_MAXPOSST"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MAXPOSST;
  mTemp["ADDRESS_MPOSRST"]               = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MPOSRST;
  mTemp["ADDRESS_AUPOSRST"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUPOSRST;
  mTemp["ADDRESS_AUVOTOFST"]             = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUVOTOFST;
  mTemp["ADDRESS_AUCCRST"]               = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUCCRST;
  mTemp["ADDRESS_AUDMORST"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUDMORST;
  mTemp["ADDRESS_AUVOTMRST"]             = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_AUVOTMRST;
  mTemp["ADDRESS_MVOTMRST"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MVOTMRST;
  mTemp["ADDRESS_MDMORST"]               = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MDMORST;
  mTemp["ADDRESS_MCCRST"]                = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MCCRST;
  mTemp["ADDRESS_VOTST"]                 = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_VOTST;
  mTemp["ADDRESS_ACTOUTOV"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTOUTOV;
  mTemp["ADDRESS_ACTDMO"]                = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTDMO;
  mTemp["ADDRESS_ACTCC"]                 = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTCC;
  mTemp["ADDRESS_ACTVOTMO"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_ACTVOTMO;
  mTemp["ADDRESS_MANREG01"]              = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MANREG01;
  mTemp["ADDRESS_MPOSR"]                 = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MPOSR;
  mTemp["ADDRESS_MDMOR"]                 = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MDMOR;
  mTemp["ADDRESS_MCCR"]                  = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MCCR;
  mTemp["ADDRESS_MVOTMOR"]               = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDRESS_MVOTMOR;
  mTemp["MAPPING"]                       = UN_CONFIG_CPC_MASSFLOWCONTROLLER_MAPPING;
  mTemp["CALIB_UNIT"]                    = UN_CONFIG_CPC_MASSFLOWCONTROLLER_CALIB_UNIT;
  mTemp["CALIB_GAS_NAME"]                = UN_CONFIG_CPC_MASSFLOWCONTROLLER_CALIB_GAS_NAME;
  mTemp["ADDITIONAL_LENGTH"]             = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]          = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]         = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]        = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]            = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]           = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]               = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"]       = UN_CONFIG_CPC_MASSFLOWCONTROLLER_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_MASSFLOWCONTROLLER"] = mTemp;
}




/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_MassFlowControllerConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"]        = ".ProcessInput.StsReg01";
    props["dataType"]       = CPC_UINT16;
    config["StsReg01"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.StsReg02";
    props["dataType"]       = CPC_UINT16;
    config["StsReg02"]      = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.evStsReg01";
    props["dataType"]       = CPC_INT32;
    config["evStsReg01"]    = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.evStsReg02";
    props["dataType"]       = CPC_INT32;
    config["evStsReg02"]    = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["hasSmooth"]      = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.PosSt";
    props["dataType"]       = CPC_FLOAT;
    config["PosSt"]         = props;

    mappingClear(props);
    props["hasSmooth"]      = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.PosRSt";
    props["dataType"]       = CPC_FLOAT;
    config["PosRSt"]         = props;

    mappingClear(props);
    props["hasSmooth"]      = true;
    props["address"]        = ".ProcessInput.MaxPosSt";
    props["dataType"]       = CPC_FLOAT;
    config["MaxPosSt"]      = props;

    mappingClear(props);
    props["hasSmooth"]      = true;
    props["address"]        = ".ProcessInput.MPosRSt";
    props["dataType"]       = CPC_FLOAT;
    config["MPosRSt"]       = props;

    mappingClear(props);
    props["hasSmooth"]      = true;
    props["address"]        = ".ProcessInput.AuPosRSt";
    props["dataType"]       = CPC_FLOAT;
    config["AuPosRSt"]      = props;

    mappingClear(props);
    props["hasSmooth"]      = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.AuVoTOfSt";
    props["dataType"]       = CPC_FLOAT;
    config["AuVoTOfSt"]     = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.AuCCRSt";
    props["dataType"]       = CPC_INT16;
    config["AuCCRSt"]       = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.AuDMoRSt";
    props["dataType"]       = CPC_INT16;
    config["AuDMoRSt"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.AuVoTMRSt";
    props["dataType"]       = CPC_INT16;
    config["AuVoTMRSt"]     = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.MVoTMRSt";
    props["dataType"]       = CPC_INT16;
    config["MVoTMRSt"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.MDMoRSt";
    props["dataType"]       = CPC_INT16;
    config["MDMoRSt"]       = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.MCCRSt";
    props["dataType"]       = CPC_INT16;
    config["MCCRSt"]        = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["hasSmooth"]      = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.VoTSt";
    props["dataType"]       = CPC_FLOAT;
    config["VoTSt"]         = props;

    mappingClear(props);
    props["hasSmooth"]      = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.ActOutOV";
    props["dataType"]       = CPC_FLOAT;
    config["ActOutOV"]      = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.ActDMo";
    props["dataType"]       = CPC_INT16;
    config["ActDMo"]        = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["hasMapping"] = true;
    props["address"]        = ".ProcessInput.ActCC";
    props["dataType"]       = CPC_INT16;
    config["ActCC"]         = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.ActVoTMo";
    props["dataType"]       = CPC_INT16;
    config["ActVoTMo"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.StartISt";
    props["dpe"]            = "StsReg01";
    props["bitPosition"]    = CPC_StsReg01_STARTIST;
    props["isAlarm"]        = true;
    config["StartISt"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.TStopISt";
    props["dpe"]            = "StsReg01";
    props["bitPosition"]    = CPC_StsReg01_TSTOPIST;
    props["isAlarm"]        = true;
    config["TStopISt"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.FuStopISt";
    props["dpe"]            = "StsReg02";
    props["bitPosition"]    = CPC_StsReg02_FUSTOPIST;
    props["isAlarm"]        = true;
    config["FuStopISt"]     = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.AlSt";
    props["dpe"]            = "StsReg01";
    props["bitPosition"]    = CPC_StsReg01_ALST;
    props["isAlarm"]        = true;
    config["AlSt"]          = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlUnAck";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALUNACK;
    config["AlUnAck"]     = props;

    mappingClear(props);
    props["address"]        = ".ProcessOutput.ManReg01";
    props["dataType"]       = CPC_UINT16;
    config["ManReg01"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessOutput.MPosR";
    props["dataType"]       = CPC_FLOAT;
    config["MPosR"]         = props;

    mappingClear(props);
    props["address"]        = ".ProcessOutput.MDMoR";
    props["dataType"]       = CPC_UINT16;
    config["MDMoR"]         = props;

    mappingClear(props);
    props["address"]        = ".ProcessOutput.MCCR";
    props["dataType"]       = CPC_UINT16;
    config["MCCR"]          = props;

    mappingClear(props);
    props["address"]        = ".ProcessOutput.MVoTMoR";
    props["dataType"]       = CPC_UINT16;
    config["MVoTMoR"]       = props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}

/**
Returns the list of parameter names that passed via config line.
*/
dyn_string CPC_MassFlowControllerConfig_getParamNames() {
//begin_TAG_SCRIPT_DEVICE_TYPE_getParamNames
    return makeDynString(CPC_PARAMS_MRESTART, CPC_PARAMS_RSTARTFS, CPC_PARAMS_FSPOSON, CPC_PARAMS_NOISE_FILTER, CPC_PARAMS_FEEDBACK_SIM, CPC_PARAMS_INHIBITMANUALTOTALIZER);
//end_TAG_SCRIPT_DEVICE_TYPE_getParamNames
}

/** Set custom configuration
*/
CPC_MassFlowControllerConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_MAPPING + "ActDMo", makeDynString("1=Close,2=Min,3=Regulated,4=Max,5=Open"), exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_setCustomConfig: Set new description (ActDMo)- ", "");
        return;
    }

    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_MAPPING + "ActVoTMo", makeDynString("1=Stop,2=Start,3=Reset"), exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_setCustomConfig: Set new description (ActVoTMo)- ", "");
        return;
    }

    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_MFC_CALIBRATION_UNIT, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_MASSFLOWCONTROLLER_CALIB_UNIT]), exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_setCustomConfig: can't set calibration gas unit ", "");
        return;
    }
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_MFC_CALIBRATION_GAS_NAME, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_MASSFLOWCONTROLLER_CALIB_GAS_NAME]), exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_setCustomConfig: can't set calibration gas name ", "");
        return;
    }

    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.MaxPosSt", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_setCustomConfig: can't set archive for MaxPosSt.", "");
        return;
    }

    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.TStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.FuStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.StartISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.AlSt");
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_MassFlowController Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_MassFlowControllerConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_MASSFLOWCONTROLLER_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");

    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");

    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_ExportConfig() -> Error exporting PosSt deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters, ".ProcessInput.VoTSt");

    cpcExportGenericFunctions_getSimpleFormat(sCurrentDp, dsDpParameters, ".ProcessInput.VoTSt");

    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.VoTSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_ExportConfig() -> Error exporting VoTSt deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.VoTSt");

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.AuCCRSt");

    cpcExportGenericFunctions_getDigitalNormalPosition(sCurrentDp,
                                                       sObject,
                                                       makeDynString(".ProcessInput.StartISt",
                                                                     ".ProcessInput.TStopISt",
                                                                     ".ProcessInput.FuStopISt"),
                                                       dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg02",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg02", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosRSt",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MaxPosSt",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuPosRSt",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuVoTOfSt",  TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuCCRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuDMoRSt",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuVoTMRSt",  TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MVoTMRSt",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MDMoRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MCCRSt",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "VoTSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActOutOV",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActDMo",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActCC",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ActVoTMo",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosR",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MDMoR",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MCCR",       FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MVoTMoR",    FALSE, dsDpParameters);
    dynAppend(dsDpParameters, cpcGenericDpFunctions_getMapping(sCurrentDp, ".ProcessInput.ActCC"));

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_MFC_CALIBRATION_UNIT, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_ExportConfig() -> Error exporting MFC_CALIBRATION_UNIT key / value, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_MFC_CALIBRATION_GAS_NAME, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_ExportConfig() -> Error exporting MFC_CALIBRATION_GAS key / value, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_MassFlowControllerConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

  }

}//  CPC_MassFlowControllerConfig_ExportConfig()





/**
Purpose: Export CPC_MassFlowController Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_MassFlowController_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_MassFlowControllerConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_MassFlowController Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_MassFlowController_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_MassFlowControllerConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_MassFlowController_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_MassFlowControllerConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_MassFlowController_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_MassFlowControllerConfig_ExportConfig(dsDpList, exceptionInfo);
}




void IEC104_CPC_MassFlowController_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_MassFlowControllerConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_MassFlowController_ExportConfig() -> Error exporting CPC_MassFlowController, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_MassFlowController_ExportConfig()





//@}
