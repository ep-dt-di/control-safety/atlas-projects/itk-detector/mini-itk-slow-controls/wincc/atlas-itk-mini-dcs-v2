/**@file

// cpcOnOff.ctl
This library contains the widget, faceplate, etc. functions of OnOff.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of OnOff DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_OnOff_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.StartISt", "ProcessInput.TStopISt", "ProcessInput.FuStopISt", "ProcessInput.AlSt", UN_ACKNOWLEDGE_PLC);
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_OnOff_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    bit32 b32StsReg01;
    string sTime, sRes;

    dpGet(deviceName + ".ProcessInput.StsReg01", b32StsReg01, deviceName + ".ProcessInput.StsReg01:_online.._stime", sTime);
    // Treat the first two bits
    if (getBit(b32StsReg01, CPC_StsReg01_ONST)) {
        if (!getBit(b32StsReg01, CPC_StsReg01_OFFST)) {
            sRes = "On";
        }
    } else if (getBit(b32StsReg01, CPC_StsReg01_OFFST)) {
        sRes = "0ff";
    } else {
        sRes = "";
    }

    dsReturnData[1] = sTime;
    dsReturnData[2] = sRes;
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_OnOff_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, CPC_POPUPMENU_ALARMS_TEXT, UN_POPUPMENU_TREND_TEXT, CPC_POPUPMENU_ALLOW_TO_RESTART_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_OnOff_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_OnOff_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    dyn_string exceptionInfo;
    int iRes, iAlertType1, iAlertType2, iAlertType3, iAlertType4;

    g_bMRestart = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
    iRes = dpGet(deviceName + ".ProcessInput.StartISt:_alert_hdl.._type", iAlertType1,
                 deviceName + ".ProcessInput.TStopISt:_alert_hdl.._type", iAlertType2,
                 deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._type", iAlertType3,
                 deviceName + ".ProcessInput.AlSt:_alert_hdl.._type", iAlertType4);
    g_bBoolAlert = ((iRes >= 0) && (iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType2 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType3 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType4 == DPCONFIG_ALERT_BINARYSIGNAL));
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of OnOff DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_OnOff_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.StartISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.TStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.AlSt:_alert_hdl.._act_state");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_OnOff_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iAlValue = values[1];
    bool bPlcEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    // unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "StsReg01", bit32StsReg01, bStsReg01Invalid);
    // unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "StsReg02", bit32StsReg02, bStsReg02Invalid);
    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bit32 bit32StsReg02 = values[9];
    bool bStsReg02Invalid = values[10];

    int iStartIStState = 0, iTStopIStState = 0, iFuStopIStState = 0, iAlState = 0;
    if (g_bBoolAlert) {
        iStartIStState = values[11];
        iTStopIStState = values[12];
        iFuStopIStState = values[13];
        iAlState = values[14];
    }

    cpcGenericObject_WidgetAnimationDoubleStsReg(widgetType, UN_CONFIG_CPC_ONOFF_DPT_NAME,
            bLocked, sSelectedManager, bit32StsReg01, bit32StsReg02,
            makeDynInt(CPC_StsReg01_AUMRW, CPC_StsReg01_POSALW, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW),
            makeDynInt(CPC_StsReg02_ALBW),
            makeDynString(CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR),
            makeDynString(CPC_WIDGET_TEXT_WARNING_BLOCKED),
            makeDynInt(CPC_StsReg01_ALST, CPC_StsReg01_STARTIST, CPC_StsReg01_TSTOPIST),
            makeDynString(CPC_WIDGET_TEXT_ALARM_ALST, CPC_WIDGET_TEXT_ALARM_STARTIST, CPC_WIDGET_TEXT_ALARM_TSTOPIST),
            makeDynInt(CPC_StsReg02_FUSTOPIST),
            makeDynString(CPC_WIDGET_TEXT_ALARM_FUSTOPIST),
            iStartIStState, iTStopIStState, iFuStopIStState, iAlState,
            makeDynString("CPC_StsReg01_FOMOST", "CPC_StsReg01_MMOST", "CPC_StsReg01_LDST", "CPC_StsReg02_SOFTLDST", "CPC_StsReg01_AUIHFOMOST", "CPC_StsReg01_AUINHMMO"),
            "CPC_StsReg01_ONST", "CPC_StsReg01_OFFST", CPC_StsReg01_ALUNACK,
            bStsReg01Invalid || bStsReg02Invalid,
            bStsReg01Invalid || bStsReg02Invalid,
            iAlValue, bPlcEnabled);

    if (g_bSystemConnected) {
        eventState.visible = (iMask == 0);
        commentsButton.visible = isCommentsActive;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_OnOff_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    cpcGenericObject_WidgetDisconnection(sWidgetType, UN_CONFIG_CPC_ONOFF_DPT_NAME);
    // set event state
    eventState.visible	= false;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_OnOff_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_AUTO_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MANUAL_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ONOPEN_REQUEST] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_ALLOW_RESTART] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_FORCED_MODE] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT; // just in the popup menu
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_OnOff_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_OnOff_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];
    mixed stsReg02Value = dsData[5];
    bool bStsReg02Bad = dsData[6];

    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_FORCED_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_FOMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHFMO) &&
                                !getBit(stsReg01Value, CPC_StsReg01_LDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_MANUAL_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_MMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHMMO) &&
                                !getBit(stsReg01Value, CPC_StsReg01_LDST) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_ONOPEN_REQUEST:
            case UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST:
            case UN_FACEPLATE_BUTTON_AUTO_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_LDST) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            default:
                break;
        }

        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else if (dsButtons[i] == CPC_FACEPLATE_BUTTON_ALLOW_RESTART || dsButtons[i] == UN_FACEPLATE_BUTTON_ACK_ALARM) {
            cpcButton_ButtonSetState(makeDynString(CPC_FACEPLATE_BUTTON_ALLOW_RESTART, UN_FACEPLATE_BUTTON_ACK_ALARM), dsUserAccess, deviceName, dpType, dsData, exceptionInfo);
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_OnOff_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    int iRes, iAlertType1, iAlertType2, iAlertType3, iAlertType4;
    dyn_string exceptionInfo;
    iRes = dpGet(deviceName + ".ProcessInput.StartISt:_alert_hdl.._type", iAlertType1,
                 deviceName + ".ProcessInput.TStopISt:_alert_hdl.._type", iAlertType2,
                 deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._type", iAlertType3,
                 deviceName + ".ProcessInput.AlSt:_alert_hdl.._type", iAlertType4);
    g_params["BoolAlert"] = ((iRes >= 0) && (iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType2 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType3 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType4 == DPCONFIG_ALERT_BINARYSIGNAL));
    g_params["PFSPosOn"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_FSPOSON, exceptionInfo);
    g_params["PHFOn"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_HFONST, exceptionInfo);
    g_params["PHFOff"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_HFOFFST, exceptionInfo);
    g_params["POutOff"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_OUTOFF, exceptionInfo);
    g_params["MRestart"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
    g_params["HLDrive"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_HLDRIVE, exceptionInfo);
    setMultiValue("Request_Output_Off_Text", "visible", g_params["POutOff"],
                  "OUTOFFOVST.colorbox", "visible", g_params["POutOff"],
                  "NOT_OUTOFFOVST.colorbox", "visible", g_params["POutOff"]);

    string onLabel, offLabel;
    CPC_OnOff_getLabels(deviceName, onLabel, offLabel, "On / Opened", "Off / Closed");
    setMultiValue("Status_OnOpenedText", "text", onLabel,
                  "Status_OffClosedText", "text", offLabel);
    string FSPosOn = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_FSPOSON, exceptionInfo);
    if (FSPosOn == "2DOON") {
        FSPosOn = "2 DO On";
    } else if (FSPosOn == "2DOOFF") {
        FSPosOn = "2 DO Off";
    } else {
        FSPosOn = ((bool) FSPosOn) ? onLabel : offLabel;
    }
    if (g_bSystemConnected) {
        setMultiValue("FSPosOn" + ".display", "foreCol", "unDisplayValue_Parameter", "FSPosOn" + ".display", "text", FSPosOn);
    }

    bool PPulseSt = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_PULSEST, exceptionInfo);
    cpcGenericObject_CheckboxAnimate("Status_Pulse", PPulseSt);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of OnOff DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_OnOff_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    if (g_params["BoolAlert"]) {
        dynAppend(dpes, deviceName + ".ProcessInput.StartISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.TStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.AlSt:_alert_hdl.._act_state");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_OnOff_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ONST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_OFFST", true, "cpcColor_Faceplate_Status");

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUINHMMO", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUINHFMO", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_SOFTLDST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_LDST", true, "cpcColor_Faceplate_Status");

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_AUONRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_AUOFFRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_MONRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_MOFFRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_OUTOVST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStsRegNotBit(dpes, values, "CPC_StsReg02_OUTOVST", true, "cpcColor_Faceplate_Status");
    if (mappingHasKey(g_params, "POutOff") && g_params["POutOff"]) {
        unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_OUTOFFOVST", true, "cpcColor_Faceplate_Status");
        unFaceplate_animateStsRegNotBit(dpes, values, "CPC_StsReg02_OUTOFFOVST", true, "cpcColor_Faceplate_Status");
    } else {
        unGenericObject_ColorBoxDisconnect(makeDynString("OUTOFFOVST", "NOT_OUTOFFOVST"));
    }

    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_STARTAL",    "StartISt",  "CPC_StsReg01_ALUNACK");
    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_TSTOPAL",    "TStopISt",  "CPC_StsReg01_ALUNACK");
    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg02_FUSTOPIST", "FuStopISt", "CPC_StsReg01_ALUNACK");
    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_ALST",      "AlSt",      "CPC_StsReg01_ALUNACK");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ALUNACK", true, "cpcColor_Alarm_Bad");

    cpcGenericObject_animateMaskEvent(dpes, values);
    cpcGenericObject_animateNeedRestart(dpes, values);
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMRW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_ALBW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_POSALW", true, "cpcColor_Faceplate_Warning");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}


//begin_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions
CPC_OnOff_getLabels(string deviceName, string &onLabel, string &offLabel, string defaultOn, string defaultOff) {
    onLabel = unGenericDpFunctions_getDescription(deviceName + ".ProcessInput.OnSt");
    if (onLabel == "") {
        onLabel = defaultOn;
    }
    offLabel = unGenericDpFunctions_getDescription(deviceName + ".ProcessInput.OffSt");
    if (offLabel == "") {
        offLabel = defaultOff;
    }
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions


//@}