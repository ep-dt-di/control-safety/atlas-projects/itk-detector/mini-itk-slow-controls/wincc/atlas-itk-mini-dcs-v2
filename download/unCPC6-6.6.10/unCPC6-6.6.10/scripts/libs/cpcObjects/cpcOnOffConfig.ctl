/**@file

// cpcOnOffConfig.ctl
This library contains the import and export function of the CPC_OnOff.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_ONOFF_DPT_NAME = "CPC_OnOff";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_ONOFF_LENGTH = 6;

const unsigned UN_CONFIG_CPC_ONOFF_NORMAL_POSITION    = 1;
const unsigned UN_CONFIG_CPC_ONOFF_ADDRESS_STSREG01   = 2;
const unsigned UN_CONFIG_CPC_ONOFF_ADDRESS_STSREG02   = 3;
const unsigned UN_CONFIG_CPC_ONOFF_ADDRESS_EVSTSREG01 = 4;
const unsigned UN_CONFIG_CPC_ONOFF_ADDRESS_EVSTSREG02 = 5;
const unsigned UN_CONFIG_CPC_ONOFF_ADDRESS_MANREG01   = 6;

const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_LENGTH          = 9;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_ON_LABEL        = 2;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_OFF_LABEL       = 3;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_PARAMETERS      = 4;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_MASTER_NAME     = 5;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_PARENTS         = 6;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_CHILDREN        = 7;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_TYPE            = 8;
const unsigned UN_CONFIG_CPC_ONOFF_ADDITIONAL_SECOND_ALIAS    = 9;
//end_TAG_SCRIPT_DEVICE_TYPE_constants





/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_OnOffConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_ONOFF_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_ONOFF_LENGTH;
  mTemp["NORMAL_POSITION"]         = UN_CONFIG_CPC_ONOFF_NORMAL_POSITION;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_ONOFF_ADDRESS_STSREG01;
  mTemp["ADDRESS_STSREG02"]        = UN_CONFIG_CPC_ONOFF_ADDRESS_STSREG02;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_ONOFF_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_EVSTSREG02"]      = UN_CONFIG_CPC_ONOFF_ADDRESS_EVSTSREG02;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_ONOFF_ADDRESS_MANREG01;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_ONOFF_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_ONOFF_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_ON_LABEL"]     = UN_CONFIG_CPC_ONOFF_ADDITIONAL_ON_LABEL;
  mTemp["ADDITIONAL_OFF_LABEL"]    = UN_CONFIG_CPC_ONOFF_ADDITIONAL_OFF_LABEL;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_ONOFF_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_ONOFF_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_ONOFF_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_ONOFF_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_ONOFF_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_ONOFF_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_ONOFF"] = mTemp;
}




/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_OnOffConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"]      = ".ProcessInput.StsReg01";
    props["dataType"]     = CPC_UINT16;
    config["StsReg01"]    = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.StsReg02";
    props["dataType"]     = CPC_UINT16;
    config["StsReg02"]    = props;

    mappingClear(props);
    props["hasArchive"]   = true;
    props["address"]      = ".ProcessInput.evStsReg01";
    props["dataType"]     = CPC_INT32;
    config["evStsReg01"]  = props;

    mappingClear(props);
    props["hasArchive"]   = true;
    props["address"]      = ".ProcessInput.evStsReg02";
    props["dataType"]     = CPC_INT32;
    config["evStsReg02"]  = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlUnAck";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALUNACK;
    config["AlUnAck"]     = props;

    mappingClear(props);
    props["address"]      = ".ProcessOutput.ManReg01";
    props["dataType"]     = CPC_UINT16;
    config["ManReg01"]    = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.StartISt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_STARTIST;
    props["isAlarm"]      = true;
    config["StartISt"]    = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.TStopISt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_TSTOPIST;
    props["isAlarm"]      = true;
    config["TStopISt"]    = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.FuStopISt";
    props["dpe"]          = "StsReg02";
    props["bitPosition"]  = CPC_StsReg02_FUSTOPIST;
    props["isAlarm"]      = true;
    config["FuStopISt"]   = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlSt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALST;
    props["isAlarm"]      = true;
    config["AlSt"]        = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.OnSt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ONST;
    config["OnSt"]        = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.OffSt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_OFFST;
    config["OffSt"]       = props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}

/**
Returns the list of parameter names that passed via config line.
*/
dyn_string CPC_OnOffConfig_getParamNames() {
//begin_TAG_SCRIPT_DEVICE_TYPE_getParamNames
    return makeDynString(CPC_PARAMS_FSPOSON, CPC_PARAMS_HFONST, CPC_PARAMS_HFOFFST, CPC_PARAMS_PULSEST, CPC_PARAMS_MRESTART, CPC_PARAMS_RSTARTFS, CPC_PARAMS_OUTOFF, CPC_PARAMS_HLDRIVE);
//end_TAG_SCRIPT_DEVICE_TYPE_getParamNames
}

/** Set custom configuration
*/
CPC_OnOffConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    dyn_string tmpExceptionInfo;
    int shift = UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_ONOFF_LENGTH;
    if (hasArchive) {
        shift += UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH;
    }

    unConfigGenericFunctions_setDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.OnSt", dsConfigs[shift + UN_CONFIG_CPC_ONOFF_ADDITIONAL_ON_LABEL], tmpExceptionInfo);
    if (dynlen(tmpExceptionInfo) > 0) {
        dynAppend(exceptionInfo, tmpExceptionInfo);
        tmpExceptionInfo = makeDynString();
    }
    unConfigGenericFunctions_setDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.OffSt", dsConfigs[shift + UN_CONFIG_CPC_ONOFF_ADDITIONAL_OFF_LABEL], tmpExceptionInfo);
    if (dynlen(tmpExceptionInfo) > 0) {
        dynAppend(exceptionInfo, tmpExceptionInfo);
    }

    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.OnSt",
                  dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  -1,
                  0,
                  exceptionInfo);
    if (dynlen(tmpExceptionInfo) > 0) {
        dynAppend(exceptionInfo, tmpExceptionInfo);
    }

    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.OffSt",
                  dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  -1,
                  0,
                  exceptionInfo);
    if (dynlen(tmpExceptionInfo) > 0) {
        dynAppend(exceptionInfo, tmpExceptionInfo);
    }

    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.TStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.FuStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.StartISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.AlSt");
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_OnOff Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_OnOffConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_ONOFF_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getDigitalNormalPosition(sCurrentDp,
                                                       sObject,
                                                       makeDynString(".ProcessInput.FuStopISt",
                                                                     ".ProcessInput.TStopISt",
                                                                     ".ProcessInput.StartISt"),
                                                       dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg02",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg02", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    makeDynString("ProcessInput.OnSt", "ProcessInput.OffSt"),
                                                    dsDpParameters); // handling special archiving config
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    dynAppend(dsDpParameters, unGenericDpFunctions_getDescription(sCurrentDp + ".ProcessInput.OnSt"));
    dynAppend(dsDpParameters, unGenericDpFunctions_getDescription(sCurrentDp + ".ProcessInput.OffSt"));

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_OnOffConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_OnOffConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_OnOffConfig_ExportConfig()






/**
Purpose: Export CPC_OnOff Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_OnOff_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_OnOffConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_OnOff Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_OnOff_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_OnOffConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_OnOff_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_OnOffConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_OnOff_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_OnOffConfig_ExportConfig(dsDpList, exceptionInfo);
}


void IEC104_CPC_OnOff_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_OnOffConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_OnOff_ExportConfig() -> Error exporting CPC_OnOff devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_OnOff_ExportConfig()

//@}
