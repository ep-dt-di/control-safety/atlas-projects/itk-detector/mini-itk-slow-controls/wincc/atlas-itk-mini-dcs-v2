/**@name LIBRARY: cpcStepPLC.ctl

@author: Alexey Merezhin (EN-ICE-PLC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

Creation Date: 03.11.2011

Modification History:

version 1.0

External Function :
    * CPC_StepPLC_registerCB

Internal Functions :
    * CPC_StepPLC_animationCB
    * CPC_StepPLC_WidgetDisconnect
    * CPC_StepPLC_WidgetDisconnection

Purpose: This library contains StepPLC animation functions.

Usage: Public

PVSS manager usage: NG, NV

@reviewed 2018-06-22 @whitelisted{Callback}

Constraints:
	. StepPLC widget
	. Global variables and $parameters defined in StepPLC widget
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

CPC_StepPLC_registerCB(string sDp, bool bSystemConnected) {
    int iRes, iAction;
    dyn_string exceptionInfo;
    string deviceName, deviceType;
    bool bRemote;

    deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
    if (deviceName == "") {// In case of disconnection
        deviceName = g_sDpName;
    }

    string systemName = unGenericDpFunctions_getSystemName(deviceName);
    string plcName = unGenericObject_GetPLCNameFromDpName(deviceName);

    unDistributedControl_isRemote(bRemote, systemName);
    if (bRemote) { g_bSystemConnected = bSystemConnected; }
    else { g_bSystemConnected = true; }
    g_bUnSystemAlarmPlc = plcName != "" && dpExists(systemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + plcName);

    if (deviceName != "") {
        deviceType = dpTypeName(deviceName);
        if (deviceType != UN_CONFIG_CPC_ANALOGINPUT_DPT_NAME && deviceType != UN_CONFIG_CPC_ANALOGSTATUS_DPT_NAME && deviceType != UN_CONFIG_CPC_WORDSTATUS_DPT_NAME) {
            g_bUnSystemAlarmPlc = false;
        }
    }

    unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
    switch (iAction) {
        case UN_ACTION_DISCONNECT:
            CPC_StepPLC_WidgetDisconnection();
            break;
        case UN_ACTION_DPCONNECT:
            g_sDpName = deviceName;
            g_bDisconnectCB = false;
            if (g_bUnSystemAlarmPlc) {
                iRes = dpConnect("CPC_StepPLC_animationCB", deviceName + ".ProcessInput.PosSt",
                                 systemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + plcName + ".alarm",
                                 systemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + plcName + ".enabled");

                g_bCallbackConnected = (iRes >= 0);
            } else {
                CPC_StepPLC_WidgetDisconnection();
            }
            break;
        case UN_ACTION_DPDISCONNECT_DISCONNECT:
            CPC_StepPLC_WidgetDisconnect(g_sDpName, systemName, plcName, iRes);

            g_bCallbackConnected = !(iRes >= 0);
            g_bDisconnectCB = false;

            CPC_StepPLC_WidgetDisconnection();
            break;
        case UN_ACTION_NOTHING:
        default:
            break;
    }
}

/**
Purpose: Animate Step widget disconnection

Parameters:

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_StepPLC_WidgetDisconnection() {
    step_Plc.backCol = "unDataNoAccess";
}

/**
Purpose: Animate Step widget disconnect

Parameters:
	- deviceName, string, Device Name to disconnect
	- iRes, int, output, Disconnection result

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_StepPLC_WidgetDisconnect(string deviceName, string systemName, string plcName, int iRes) {
    if (g_bUnSystemAlarmPlc) {
        iRes = dpDisconnect("CPC_StepPLC_animationCB", deviceName + ".ProcessInput.PosSt",
                            systemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + plcName + ".alarm",
                            systemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + plcName + ".enabled");
    }
}

/**
Purpose: callback function for step PLC

Parameters:
	- sDp1, string, input, datapoint nane
	- fPosSt, float, input, value of datapoint
Usage: External

PVSS manager usage: NG, NV

@reviewed 2018-06-22 @whitelisted{Callback}

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_StepPLC_animationCB(string sDp1, int iPosSt, string sDpAl, int iAlValue, string sDpEnabled, bool bPlcEnabled) {
    string sColor;

    if ((!bPlcEnabled) || (iAlValue > c_unSystemIntegrity_no_alarm_value)) {
        sColor = "unDataNotValid";
    } else {
        sColor = iPosSt == g_iValue ? g_sColor_Active : g_sColor_Passive;
    }
    if (g_bSystemConnected) {
        setValue("step_Plc", "backCol", sColor);
    }
}
