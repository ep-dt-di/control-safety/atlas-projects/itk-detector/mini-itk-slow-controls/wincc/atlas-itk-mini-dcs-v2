/**@file

// cpcSummaryAlarmConfig.ctl
This library contains the import and export function of the CPC_SumAlarm device

@par Creation Date
  01/10/2020

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)
  Jonas Arroyo (BE-ICS-FD)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


// Constants
const string UN_CONFIG_CPC_SUMALARM_DPT_NAME = "CPC_SumAlarm";

const unsigned UN_CONFIG_CPC_SUMALARM_LENGTH            = 22;
const unsigned UN_CONFIG_CPC_SUMALARM_ADDITIONAL_LENGTH = 0;

const unsigned UN_CONFIG_CPC_SUMALARM_ARCHIVE               = 1;
const unsigned UN_CONFIG_CPC_SUMALARM_ON_TEXT               = 2;
const unsigned UN_CONFIG_CPC_SUMALARM_OFF_TEXT              = 3;
const unsigned UN_CONFIG_CPC_SUMALARM_ALARM_CLASS           = 4;
const unsigned UN_CONFIG_CPC_SUMALARM_ACTIVE                = 5;
const unsigned UN_CONFIG_CPC_SUMALARM_DP_LIST_BASED         = 6;
const unsigned UN_CONFIG_CPC_SUMALARM_DP_PATTERN            = 7;
const unsigned UN_CONFIG_CPC_SUMALARM_PANEL                 = 8;
const unsigned UN_CONFIG_CPC_SUMALARM_DPE_FRONTEND          = 9;
const unsigned UN_CONFIG_CPC_SUMALARM_DPE_OUTSIDE           = 10;
const unsigned UN_CONFIG_CPC_SUMALARM_DPE_THRESHOLD         = 11;
const unsigned UN_CONFIG_CPC_SUMALARM_DPE_THRESHOLD_VALUE   = 12;
const unsigned UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_ORDER     = 13;
const unsigned UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY  = 14;
const unsigned UN_CONFIG_CPC_SUMALARM_ALERT_PRIORITY_NUMBER = 15;
const unsigned UN_CONFIG_CPC_SUMALARM_ALERT_SHORT_SIGN      = 16;
const unsigned UN_CONFIG_CPC_SUMALARM_ACK_DELETES           = 17;
const unsigned UN_CONFIG_CPC_SUMALARM_NOT_ACK               = 18;
const unsigned UN_CONFIG_CPC_SUMALARM_CAME_ACK              = 19;
const unsigned UN_CONFIG_CPC_SUMALARM_COUPLE_REQ_ACK        = 20;
const unsigned UN_CONFIG_CPC_SUMALARM_CAME_WENT_REQ_ACK     = 21;
const unsigned UN_CONFIG_CPC_SUMALARM_ENABLE_FILTER         = 22;







mapping CPC_SumAlarmConfig_getConfig()
{
  mapping mConfig;


  return mConfig;

}





/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_SumAlarmConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_LENGTH"]           = UN_CONFIG_CPC_SUMALARM_LENGTH;
  mTemp["ADDITIONAL_LENGTH"]    = UN_CONFIG_CPC_SUMALARM_ADDITIONAL_LENGTH;

  mTemp["ARCHIVE"]              = UN_CONFIG_CPC_SUMALARM_ARCHIVE;
  mTemp["ON_TEXT"]              = UN_CONFIG_CPC_SUMALARM_ON_TEXT;
  mTemp["OFF_TEXT"]             = UN_CONFIG_CPC_SUMALARM_OFF_TEXT;
  mTemp["ALARM_CLASS"]          = UN_CONFIG_CPC_SUMALARM_ALARM_CLASS;
  mTemp["ACTIVE"]               = UN_CONFIG_CPC_SUMALARM_ACTIVE;
  mTemp["DP_LIST_BASED"]        = UN_CONFIG_CPC_SUMALARM_DP_LIST_BASED;
  mTemp["DP_PATTERN"]           = UN_CONFIG_CPC_SUMALARM_DP_PATTERN;
  mTemp["PANEL"]                = UN_CONFIG_CPC_SUMALARM_PANEL;
  mTemp["DPE_FRONTEND"]         = UN_CONFIG_CPC_SUMALARM_DPE_FRONTEND;
  mTemp["DPE_OUTSIDE"]          = UN_CONFIG_CPC_SUMALARM_DPE_OUTSIDE;
  mTemp["DPE_FILTER"]           = UN_CONFIG_CPC_SUMALARM_DPE_THRESHOLD;
  mTemp["DPE_FILTER_VALUE"]     = UN_CONFIG_CPC_SUMALARM_DPE_THRESHOLD_VALUE;
  mTemp["ALERT_ORDER_PRIORITY"] = UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY;
  mTemp["ALERT_ORDER_ORDER"]    = UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_ORDER;
  mTemp["ALERT_PRIORITY"]       = UN_CONFIG_CPC_SUMALARM_ALERT_PRIORITY_NUMBER;
  mTemp["ALERT_SHORT_SIGN"]     = UN_CONFIG_CPC_SUMALARM_ALERT_SHORT_SIGN;
  mTemp["ACK_DELETES"]          = UN_CONFIG_CPC_SUMALARM_ACK_DELETES;
  mTemp["NOT_ACK"]              = UN_CONFIG_CPC_SUMALARM_NOT_ACK;
  mTemp["CAME_ACK"]             = UN_CONFIG_CPC_SUMALARM_CAME_ACK;
  mTemp["COUPLE_REQ_ACK"]       = UN_CONFIG_CPC_SUMALARM_COUPLE_REQ_ACK;
  mTemp["CAME_WENT_REQ_ACK"]    = UN_CONFIG_CPC_SUMALARM_CAME_WENT_REQ_ACK;
  mTemp["ENABLE_FILTER"]        = UN_CONFIG_CPC_SUMALARM_ENABLE_FILTER;

  g_mCpcConst["UN_CONFIG_CPC_SUMALARM"] = mTemp;
}




/**
Returns the list of parameter names that passed via config line.
*/
dyn_string CPC_SumAlarmConfig_getParamNames()
{
  return makeDynString();
}





/** Check custom configuration
*/
void CPC_SumAlarmConfig_checkCustomConfig(dyn_string dsConfigLine, bool bHasArchive, dyn_string &exceptionInfo)
{
  bool bValue;
  int iLen, iValue;
  string sAlarmClass;
  dyn_string dsDpes, dsDpeNotExist, dsDpeExist;


  // DebugTN("CPC_SumAlarmConfig_checkCustomConfig() -> dsConfigLine = ", dsConfigLine);

  // Check text alarm ON (string)
  if( dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ON_TEXT] == "" )
  {
    fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarmConfig_checkCustomConfig() -> Alarm text ON is empty", "");
    return;
  }


  // Check text alarm OFF (string)
  if( dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_OFF_TEXT] == "" )
  {
    fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarmConfig_checkCustomConfig() -> Alarm text OFF is empty", "");
    return;
  }


  // Check alarm class (string, exist)
  sAlarmClass = dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALARM_CLASS];
  if( sAlarmClass != "" )
  {
    if( dpExists(sAlarmClass) == FALSE )
    {
      fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarmConfig_checkCustomConfig() -> Summary alarm class: " + sAlarmClass + ", does not exist", "");
      return;
    }
  }


  // Check alarm active (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_ACTIVE", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ACTIVE], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }

  // Check alarm DP list based (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_DP_LIST_BASED", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_DP_LIST_BASED], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }


  // Check alarm DP list frontend (dyn_string DP exists)
  dsDpes = strsplit(dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_DPE_FRONTEND], ",");
  if( dynlen(dsDpes) <= 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CPC_SumAlarmConfig_checkCustomConfig() -> Error none DPE in the front end to configure the summary alarm", "");
    return;
  }


  if( CPC_SumAlarmConfig_checkDpesExist(dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_DPE_FRONTEND], dsDpeNotExist, dsDpeExist, exceptionInfo) == FALSE )
  {
    fwException_raise(exceptionInfo, "ERROR", "CPC_SumAlarmConfig_checkCustomConfig() -> Error checking DPEs in the summary alarm: " + dsConfigLine[UN_CONFIG_OBJECTGENERAL_DPNAME] + ", due to: " + exceptionInfo[2], "");
    return;
  }

  if( dynlen(dsDpeNotExist) > 0 )
  {
    DebugTN("CPC_SumAlarmConfig_checkCustomConfig() -> Summary alarm: " + dsConfigLine[UN_CONFIG_OBJECTGENERAL_DPNAME] +  " DPEs does not exist: " + dsDpeNotExist);
  }


  // Check alarm order (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_ORDER", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_ORDER], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }


  // Check alarm priority (int)
  unConfigGenericFunctions_checkInt(dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY], iValue, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_checkCustomConfig() -> checking the UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY parameter: " + dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY] + ", due to: " + exceptionInfo[2];
    return;
  }
  else
  {
    switch( iValue )
    {
      case 0: // ORDER_PRIO_STATE
      case 1: // ORDER_SIGN_STATE
      case 2: // ORDER_STATE_PRIO
      case 3: // ORDER_STATE_SIGN
        break;

      default:
        fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarmConfig_checkCustomConfig() -> unknown value for parameter UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY: " + dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY], "");
        DebugTN("exceptionInfo = ", exceptionInfo);
        return;
        break;
    }
  }


  // Check option Acknowledge delete (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_ACK_DELETES", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ACK_DELETES], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }


  // Check option Not acknknowledge (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_NOT_ACK", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_NOT_ACK], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }


  // Check option Came acknowledge (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_CAME_ACK", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_CAME_ACK], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }


  // Check option Couple requieres acknowledge (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_COUPLE_REQ_ACK", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_COUPLE_REQ_ACK], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }


  // Check alarm Came Went requieres acknowledge (bool)
  cpcConfigGenericFunctions_checkBool("the field UN_CONFIG_CPC_SUMALARM_CAME_WENT_REQ_ACK", dsConfigLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_CAME_WENT_REQ_ACK], bValue, exceptionInfo );
  if( dynlen(exceptionInfo) > 0 )
  {
    return;
  }

}




/**
Checks the existant of connected DPEs or Aliases in the Summary alarm
*/
bool CPC_SumAlarmConfig_checkDpesExist(string sDpes, dyn_string &dsDpeNotExist, dyn_string &dsSumDpes, dyn_string &exceptionInfo)
{
  bool bDpeExists, bAliasExists;
  int iLen, iLoop;
  string sDpeFromAlias;
  dyn_errClass deError;
  dyn_string dsDpes;


  fwGeneral_stringToDynString(sDpes, dsDpes, ",");
  iLen = dynlen(dsDpes);
  if( iLen > 0 )
  {
    for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
    {
      bAliasExists  = FALSE;
      bDpeExists    = FALSE;
      sDpeFromAlias = "";

      // DPE name exists
      if( dpExists(dsDpes[iLoop]) == TRUE )
      {
        bDpeExists = TRUE;
        dynAppend(dsSumDpes, dsDpes[iLoop]);
      }

      // DPE alias exists
      sDpeFromAlias = dpAliasToName(dsDpes[iLoop]);
      if( sDpeFromAlias != "" )
      {
        bAliasExists = TRUE;
        dynAppend(dsSumDpes, sDpeFromAlias);
      }
      deError = getLastError();
      if( dynlen(deError) > 0 )
      {
        fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarmConfig_checkDpesExist() -> Getting dp name from alias, due to: " + getErrorText(deError), "");
        return FALSE;
      }

      if( bDpeExists == FALSE && bAliasExists == FALSE )
      {
        dynAppend(dsDpeNotExist, dsDpes[iLoop]);
      }

    }
  }
  return TRUE;
}






/** Set custom configuration
*/
void CPC_SumAlarmConfig_setCustomConfig(dyn_string dsConfigs, bool bHasArchive, dyn_string &exceptionInfo)
{
  bool bValue, bUnAckHasPriority, bOptAckDeletes, bOptNotAcknowledge, bOptCameAck, bCoupleAck, bCameWentAck;
  int iLoop, iLen, iAnswer, iOrderPriority;
  string sDpeFromAlias;
  dyn_string dsDpes, dsDpeNotExist;
  dyn_errClass deError;
  dyn_mixed dmAlertConfig;


  if( CPC_SumAlarmConfig_checkDpesExist(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_DPE_FRONTEND], dsDpeNotExist, dsDpes, exceptionInfo) == FALSE )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() configuring the DPE list in device: " + dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ", due to: " + exceptionInfo[2];
    return;
  }


  // Create summary object
  fwAlertConfig_objectCreateSummary(dmAlertConfig,
                                    makeDynString(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_OFF_TEXT],
                                                  dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ON_TEXT]),
                                    0,
                                    makeDynString("", dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALARM_CLASS]),
                                    dsDpes,
                                    "",
                                    makeDynString(),
                                    "",
                                    exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error creating summary alarm object for DPE: " + dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ", due to: " + exceptionInfo[2];
    return;
  }


  // Set summary alarm
  fwAlertConfig_objectSet(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm",
                          dmAlertConfig,
                          exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error setting the summary alarm in: " + dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ", due to: " + exceptionInfo[2];
    return;
  }


  // Configure Alarm Order
  cpcConfigGenericFunctions_checkBool("Alarm order",
                                      dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_ORDER],
                                      bUnAckHasPriority,
                                      exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting bool value from UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_ORDER, due to: " + exceptionInfo[2];
    return;
  }


  // Configure Alarm priority
  unConfigGenericFunctions_checkInt(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY],
                                    iOrderPriority,
                                    exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting integer value from UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY parameter: " + dsConfigs[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ALERT_ORDER_PRIORITY] + ", due to: " + exceptionInfo[2];
    return;
  }



  // Configure option Acknowledge delete (bool)

  cpcConfigGenericFunctions_checkBool("Option: Acknowledge deletes",
                                      dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ACK_DELETES],
                                      bOptAckDeletes,
                                      exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting bool value from UN_CONFIG_CPC_SUMALARM_ACK_DELETES, due to: " + exceptionInfo[2];
    return;
  }


  // Configure option Not acknknowledge (bool)
  cpcConfigGenericFunctions_checkBool("Option: Not acknowledge",
                                      dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_NOT_ACK],
                                      bOptNotAcknowledge,
                                      exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting bool value from UN_CONFIG_CPC_SUMALARM_NOT_ACK, due to: " + exceptionInfo[2];
    return;
  }


  // Configure option Came acknowledge (bool)
  cpcConfigGenericFunctions_checkBool("Option: Came acknowledge",
                                      dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_CAME_ACK],
                                      bOptCameAck,
                                      exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting bool value from UN_CONFIG_CPC_SUMALARM_ACK_DELETES, due to: " + exceptionInfo[2];
    return;
  }


  // Check option Couple requieres acknowledge (bool)
  cpcConfigGenericFunctions_checkBool("Option: Couple required ack",
                                      dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_COUPLE_REQ_ACK],
                                      bCoupleAck,
                                      exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting bool value from UN_CONFIG_CPC_SUMALARM_ACK_DELETES, due to: " + exceptionInfo[2];
    return;
  }


  // Check alarm Came Went requieres acknowledge (bool)
  cpcConfigGenericFunctions_checkBool("Option: Came Went ack",
                                      dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_CAME_WENT_REQ_ACK],
                                      bCameWentAck,
                                      exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting bool value from UN_CONFIG_CPC_SUMALARM_ACK_DELETES, due to: " + exceptionInfo[2];
    return;
  }


  iAnswer = dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm:_alert_hdl.._ack_has_prio", bUnAckHasPriority,
                      dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm:_alert_hdl.._order",        iOrderPriority,
                      dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm:_alert_hdl.._ack_deletes",  bOptAckDeletes,
                      dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm:_alert_hdl.._non_ack",      bOptNotAcknowledge,
                      dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm:_alert_hdl.._came_ack",     bOptCameAck,
                      dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm:_alert_hdl.._pair_ack",     bCoupleAck,
                      dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm:_alert_hdl.._both_ack",     bCameWentAck);
  if( iAnswer != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "Error setting summary alarm priority for: " + dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], "");
    return;
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "Error setting summary alarm priority for: " + dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ", due to: " + getErrorText(deError), "");
      return;
    }
  }


  // Activate summary alarm
  cpcConfigGenericFunctions_checkBool("Active alarm", dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_SUMALARM_ACTIVE], bValue, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    exceptionInfo[2] = "Error in CPC_SumAlarmConfig_setCustomConfig() -> Getting bool value from UN_CONFIG_CPC_SUMALARM_ACTIVE, due to: " + exceptionInfo[2];
    return;
  }
  else
  {
    if( bValue == TRUE )
    {
      fwAlertConfig_activate(makeDynString(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.SumAlarm"), exceptionInfo);
      if( dynlen(exceptionInfo) > 0 )
      {
        exceptionInfo[2] = "Error activating alarm: " + dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ", due to: " + exceptionInfo[2];
        return;
      }
    }
  }

}





/**
Purpose: Export CPC_SumlAlarm Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_SumAlarmConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLen, iLoop;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_SUMALARM_DPT_NAME;
  iLen    = dynlen(dsDpList);


  dynAppend(dsDpParameters, "#CPC_SumAlarm");
  dynAppend(dsDpParameters, "DeviceNumber");
  dynAppend(dsDpParameters, "Alias[,DeviceLinkList]");
  dynAppend(dsDpParameters, "Description - ElectricalDiagram");
  dynAppend(dsDpParameters, "Diagnostics");
  dynAppend(dsDpParameters, "WWWLink");
  dynAppend(dsDpParameters, "Synoptic");
  dynAppend(dsDpParameters, "Domain");
  dynAppend(dsDpParameters, "Nature");
  dynAppend(dsDpParameters, "WidgetType");
  dynAppend(dsDpParameters, "ArchiveMode");
  dynAppend(dsDpParameters, "AlarmTextOn (string)");
  dynAppend(dsDpParameters, "AlarmTextOff (string)");
  dynAppend(dsDpParameters, "AlarmClass (string)");
  dynAppend(dsDpParameters, "Active (Y/N)");
  dynAppend(dsDpParameters, "DP_list_based (Y/N)");
  dynAppend(dsDpParameters, "DP_pattern (WiP)");
  dynAppend(dsDpParameters, "DPE_panel (WiP)");
  dynAppend(dsDpParameters, "DPE_FrontEnd_list (string1,string2, …)");
  dynAppend(dsDpParameters, "DPE_Outside_list (WiP)");
  dynAppend(dsDpParameters, "DPE_threshold (WiP)");
  dynAppend(dsDpParameters, "DPE_threshold_value (WiP)");
  dynAppend(dsDpParameters, "Alarm_order (Y/N)");
  dynAppend(dsDpParameters, "Alarm_priority (int from 0-3)");
  dynAppend(dsDpParameters, "Alarm_priority_number (WiP)");
  dynAppend(dsDpParameters, "Alarm_filter_short_sign (WiP)");
  dynAppend(dsDpParameters, "Alarm_Ack_deletes (WiP)");
  dynAppend(dsDpParameters, "Alarm_not_Ack (WiP)");
  dynAppend(dsDpParameters, "Alarm_came_ack (WiP)");
  dynAppend(dsDpParameters, "Alarm_couple_req_Ack (WiP)");
  dynAppend(dsDpParameters, "Alarm_came_went_req_Ack (WiP)");
  dynAppend(dsDpParameters, "Alarm_Enable_filter (WiP)");
  unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);


  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);

    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.SumAlarm");
    dynRemove(dsDpParameters, dynlen(dsDpParameters));


    /*
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject,
                                                                                              UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject,
                                                                                              UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject,
                                                                                              UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);
    */

    if( CPC_SumAlarmConfig_ExportParameters(sCurrentDp, dsDpParameters, exceptionInfo) == FALSE)
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_SumAlarmConfig_ExportConfig() -> Error exporting summary alarm parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_SumAlarmConfig_ExportConfig()





bool CPC_SumAlarmConfig_ExportParameters(string sDp, dyn_string &dsDpParameters, dyn_string &exceptionInfo)
{
  bool bActive, bAlarmOrder, bOptAckDeletes, bOptNotAcknowledge, bOptCameAck, bCoupleAck, bCameWentAck;
  int iAlarmOrder, iLoop, iLen;
  string sMessageOn, sMessageOff, sAlarmClass, sDpeList, sAlias;
  dyn_string dsDpeList;
  dyn_errClass deError;


  if( dpGet(sDp + ".ProcessInput.SumAlarm:_alert_hdl.._text1",        sMessageOn,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._text0",        sMessageOff,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._class",        sAlarmClass,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._dp_list",      dsDpeList,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._active",       bActive,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._ack_has_prio", bAlarmOrder,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._order",        iAlarmOrder,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._ack_deletes",  bOptAckDeletes,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._non_ack",      bOptNotAcknowledge,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._came_ack",     bOptCameAck,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._pair_ack",     bCoupleAck,
            sDp + ".ProcessInput.SumAlarm:_alert_hdl.._both_ack",     bCameWentAck) != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "Error getting summary parameters for: " + sDp + " in CPC_SumAlarmConfig_ExportParameters()", "");
    return FALSE;
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) >  0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "Error getting summary parameters for: " + sDp + " in CPC_SumAlarmConfig_ExportParameters(), due to: " + getErrorText(deError), "");
      return FALSE;
    }
  }


  // Add Alarm text ON
  dynAppend(dsDpParameters, sMessageOn);

  // Add Alarm text OFF
  dynAppend(dsDpParameters, sMessageOff);

  // Add Alarm class
  dynAppend(dsDpParameters, sAlarmClass);

  // Add Alarm active
  if( bActive )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add if it is a dpList based summary alarm
  if( bAlarmOrder )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add DP_pattern (future functionality)
  dynAppend(dsDpParameters, "");

  // Add DPE_panel (future functionality)
  dynAppend(dsDpParameters, "");

  // Add DPE_FrontEnd_list items
  iLen = dynlen(dsDpeList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    sAlias = dpGetAlias(dsDpeList[iLoop]);
    if( sAlias != "" )
    {
      dsDpeList[iLoop] = sAlias;
    }
  }
  fwGeneral_dynStringToString(dsDpeList, sDpeList, ",");
  dynAppend(dsDpParameters, sDpeList);

  // Add DPE_Outside_list (future functionality to add DPEs outside this frontend)
  dynAppend(dsDpParameters, "");

  // Add DPE threshold
  dynAppend(dsDpParameters, "");

  // Add DPE threshold value
  dynAppend(dsDpParameters, "");

  // Add Alarm order
  if( bAlarmOrder )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add Alarm priority
  dynAppend(dsDpParameters, iAlarmOrder);

  // Add priority number (future functionality)
  dynAppend(dsDpParameters, "");

  // Add Alarm_filter_short_sign
  dynAppend(dsDpParameters, "");

  // Add Alarm_Ack_deletes
  if( bOptAckDeletes )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add Alarm_not_Ack
  if( bOptNotAcknowledge )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add Alarm_came_ack
  if( bOptCameAck )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add Alarm_couple_req_Ack
  if( bCoupleAck )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add Alarm_came_went_req_Ack (future feature)
  if( bCameWentAck )
  {
    dynAppend(dsDpParameters, "Y");
  }
  else
  {
    dynAppend(dsDpParameters, "N");
  }

  // Add Alarm_Enable_filter
  dynAppend(dsDpParameters, "");

  return TRUE;
}




/**
Purpose: Export CPC_SumAlarm Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
void S7_PLC_CPC_SumAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  CPC_SumAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_SumAlarm_ExportConfig() -> Error exporting CPC_SumAlarm devices, due to: " + exceptionInfo[2], "");
    return;
  }
}





/**
Purpose: Export CPC_SumAlarm Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
void _UnPlc_CPC_SumAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  CPC_SumAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_SumAlarm_ExportConfig() -> Error exporting CPC_SumAlarm devices, due to: " + exceptionInfo[2], "");
    return;
  }
}





/**
Purpose: Export CPC_SumAlarm Devices for OPCUA front-end

Usage: External function

PVSS manager usage: NG, NV
*/
void OPCUA_CPC_SumAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  CPC_SumAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "OPCUA_CPC_SumAlarm_ExportConfig() -> Error exporting CPC_SumAlarm devices, due to: " + exceptionInfo[2], "");
    return;
  }
}





/**
Purpose: Export CPC_SumAlarm Devices for BACnet front-end

Usage: External function

PVSS manager usage: NG, NV
*/
void BACnet_CPC_SumAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  CPC_SumAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "BACnet_CPC_SumAlarm_ExportConfig() -> Error exporting CPC_SumAlarm devices, due to: " + exceptionInfo[2], "");
    return;
  }
}





/**
Purpose: Export CPC_SumAlarm Devices for IEC104 front-end

Usage: External function

PVSS manager usage: NG, NV
*/
void IEC104_CPC_SumAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  CPC_SumAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_SumlAlarm_ExportConfig() -> Error exporting CPC_SumAlarm devices, due to: " + exceptionInfo[2], "");
    return;
  }
}

//@}
