/**@file

// cpcWord2AnalogStatusConfig.ctl
This library contains the import and export function of the CPC_Word2AnalogStatus.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_WORD2ANALOGSTATUS_DPT_NAME = "CPC_Word2AnalogStatus";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_LENGTH				= 11;

const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_UNIT					= 1;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_FORMAT				= 2;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_DEADBAND				= 3;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_DEADBAND_TYPE		= 4;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_RANGEMAX				= 5;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_RANGEMIN				= 6;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMAX			= 7;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMIN			= 8;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ARCHIVE_ACTIVE		= 9;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ARCHIVE_TIME_FILTER	= 10;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDRESS_POSST		= 11;

const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_LENGTH          = 6;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_PARAMETERS      = 1;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_MASTER_NAME     = 2;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_PARENTS         = 3;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_CHILDREN        = 4;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_TYPE            = 5;
const unsigned UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_SECOND_ALIAS    = 6;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_Word2AnalogStatusConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dataType"] 		= CPC_INT16;
    config["PosSt"] 		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_Word2AnalogStatusConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_WORD2ANALOGSTATUS_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_WORD2ANALOGSTATUS_LENGTH;
  mTemp["UNIT"]                    = UN_CONFIG_CPC_WORD2ANALOGSTATUS_UNIT;
  mTemp["FORMAT"]                  = UN_CONFIG_CPC_WORD2ANALOGSTATUS_FORMAT;
  mTemp["DEADBAND"]                = UN_CONFIG_CPC_WORD2ANALOGSTATUS_DEADBAND;
  mTemp["DEADBAND_TYPE"]           = UN_CONFIG_CPC_WORD2ANALOGSTATUS_DEADBAND_TYPE;
  mTemp["RANGEMAX"]                = UN_CONFIG_CPC_WORD2ANALOGSTATUS_RANGEMAX;
  mTemp["RANGEMIN"]                = UN_CONFIG_CPC_WORD2ANALOGSTATUS_RANGEMIN;
  mTemp["OUTRANGEMAX"]             = UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMAX;
  mTemp["OUTRANGEMIN"]             = UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ARCHIVE_TIME_FILTER;
  mTemp["ADDRESS_POSST"]           = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDRESS_POSST;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_WORD2ANALOGSTATUS_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_WORD2ANALOGSTATUS"] = mTemp;
}




/** Check custom configuration
*/
CPC_Word2AnalogStatusConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    float sMin, sMax;
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMIN], sMin, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMAX], sMax, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_Word2AnalogStatusConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    float outRangeMax, outRangeMin, rangeMax, rangeMin;
    int iRes;
    outRangeMax = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMAX];
    outRangeMin = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_WORD2ANALOGSTATUS_OUTRANGEMIN];
    rangeMax = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_WORD2ANALOGSTATUS_RANGEMAX];
    rangeMin = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_WORD2ANALOGSTATUS_RANGEMIN];

    // (b*x^2 + a) conversion
    float b = (outRangeMax - outRangeMin) / (rangeMax - rangeMin);
    float a = b * rangeMax - outRangeMax;

    string dpe = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt";
    iRes = dpSetWait(dpe + ":_msg_conv.._type", DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN,
                     dpe + ":_msg_conv.1._type", DPDETAIL_CONV_POLY,
                     dpe + ":_msg_conv.1._poly_grade", 1,
                     dpe + ":_msg_conv.1._poly_a", a,
                     dpe + ":_msg_conv.1._poly_b", b);
    if (iRes != 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_Word2AnalogStatusConfig_setConfig: error in convertion" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_Word2AnalogStatus Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_Word2AnalogStatusConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_WORD2ANALOGSTATUS_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters);
    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters);
    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_Word2AnalogStatusConfig_ExportConfig() -> Error exporting deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_Word2AnalogStatusConfig_ExportConfig() -> Error exporting ranges, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getOutRange(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_Word2AnalogStatusConfig_ExportConfig() -> Error exporting out of range, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt", TRUE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_Word2AnalogStatusConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_Word2AnalogStatusConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_Word2AnalogStatusConfig_ExportConfig()





/**
Purpose: Export CPC_Word2AnalogStatus Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_Word2AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_Word2AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_Word2AnalogStatus Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_Word2AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_Word2AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_Word2AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_Word2AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_Word2AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_Word2AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}



void IEC104_CPC_Word2AnalogStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_Word2AnalogStatusConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_Word2AnalogStatus_ExportConfig() -> Error exporting Word2AnalogStatus devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_Word2AnalogStatus_ExportConfig()





//@}
