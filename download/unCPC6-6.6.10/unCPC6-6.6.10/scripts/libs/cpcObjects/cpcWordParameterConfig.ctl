#uses "cpcObjects/cpcWordParameterConfigDeprecated.ctl"




/**@file

// cpcWordParameterConfig.ctl
This library contains the import and export function of the CPC_WordParameter.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_WORDPARAMETER_DPT_NAME = "CPC_WordParameter";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_WORDPARAMETER_LENGTH				= 15;

const unsigned UN_CONFIG_CPC_WORDPARAMETER_UNIT					= 1;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_FORMAT				= 2;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_DEADBAND				= 3;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_DEADBAND_TYPE		= 4;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_RANGEMAX				= 5;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_RANGEMIN				= 6;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ARCHIVE_ACTIVE		= 7;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ARCHIVE_TIME_FILTER	= 8;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_MPOSRST		= 9;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_POSST		= 10;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_MPOSR		= 11;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_STSREG01		= 12;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_EVSTSREG01	= 13;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_MANREG01		= 14;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_DEFAULT_VALUE		= 15;

const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_MAPPING         = 2;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_SECOND_ALIAS    = 7;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_WordParameterConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg01";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg01"] 		= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg01";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg01"] 	= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["address"] 		= ".ProcessInput.MPosRSt";
    props["dataType"] 		= CPC_UINT16;
    config["MPosRSt"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	        = true;
    props["hasPvRange"] 	        = true;
    props["hasMapping"]         = true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dataType"] 		= CPC_UINT16;
    config["PosSt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg01";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg01"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessOutput.MPosR";
    props["dataType"] 		= CPC_UINT16;
    config["MPosR"] 		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_WordParameterConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_WORDPARAMETER_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_WORDPARAMETER_LENGTH;
  mTemp["UNIT"]                    = UN_CONFIG_CPC_WORDPARAMETER_UNIT;
  mTemp["FORMAT"]                  = UN_CONFIG_CPC_WORDPARAMETER_FORMAT;
  mTemp["DEADBAND"]                = UN_CONFIG_CPC_WORDPARAMETER_DEADBAND;
  mTemp["DEADBAND_TYPE"]           = UN_CONFIG_CPC_WORDPARAMETER_DEADBAND_TYPE;
  mTemp["RANGEMAX"]                = UN_CONFIG_CPC_WORDPARAMETER_RANGEMAX;
  mTemp["RANGEMIN"]                = UN_CONFIG_CPC_WORDPARAMETER_RANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_WORDPARAMETER_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_WORDPARAMETER_ARCHIVE_TIME_FILTER;
  mTemp["ADDRESS_MPOSRST"]         = UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_MPOSRST;
  mTemp["ADDRESS_POSST"]           = UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_POSST;
  mTemp["ADDRESS_MPOSR"]           = UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_MPOSR;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_STSREG01;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_WORDPARAMETER_ADDRESS_MANREG01;
  mTemp["DEFAULT_VALUE"]           = UN_CONFIG_CPC_WORDPARAMETER_DEFAULT_VALUE;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_MAPPING"]      = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_MAPPING;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_WORDPARAMETER_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_WORDPARAMETER_DPT_NAME"] = mTemp;
}




/** Check custom configuration
*/
CPC_WordParameterConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    int iValue, iMax, iMin;
    unConfigGenericFunctions_checkInt(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_WORDPARAMETER_DEFAULT_VALUE], iValue, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_WORDPARAMETER_RANGEMIN], iMin, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_WORDPARAMETER_RANGEMAX], iMax, exceptionInfo);
    if ((iValue < iMin) || (iValue > iMax)) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_WordParameterConfig_checkCommonConfig: " + getCatStr("unCPCGeneration", "BADRANGE"), "");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_WordParameterConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    unConfigGenericFunctions_setUnit(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_WORDPARAMETER_UNIT], exceptionInfo);
    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_WORDPARAMETER_FORMAT], exceptionInfo);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_WORDPARAMETER_DEFAULT_VALUE]);
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_WordParameter Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_WordParameterConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo, bool bExportOnlineValuesAsDefault = FALSE)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_WORDPARAMETER_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordParameterConfig_ExportConfig() -> Error exporting deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordParameterConfig_ExportConfig() -> Error exporting ranges, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosR",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);

    if( bExportOnlineValuesAsDefault )
    {
      // Replace default values with current online values
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.PosSt", dsDpParameters);
    }
    else
    {
      // Use the regular default values
      cpcExportGenericFunctions_getDefaultValue(sCurrentDp, dsDpParameters);
    }

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    dynAppend(dsDpParameters, cpcGenericDpFunctions_getMapping(sCurrentDp, ".ProcessInput.PosSt"));

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordParameterConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_WordParameterConfig_ExportConfig()

/**
Purpose: Export CPC_WordParameter Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_WordParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_WordParameter Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_WordParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_WordParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_WordParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

DIP_CPC_WordParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

SNMP_CPC_WordParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}




void IEC104_CPC_WordParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CPC_WordParameterConfig_ExportConfig() -> Error exporting CPC_WordParameter devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_WordParameter_ExportConfig()





/**
Purpose: Export CPC_AnalogParameter Devices and override the default parameters with the online values
         These are just simple wrappers to indicate that the device supports the feature

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_WordParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_WordParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

_UnPlc_CPC_WordParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_WordParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

OPCUA_CPC_WordParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_WordParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

BACnet_CPC_WordParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_WordParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

DIP_CPC_WordParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_WordParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

SNMP_CPC_WordParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_WordParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}





void IEC104_CPC_WordParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  // Call CPC_WordParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = TRUE
  CPC_WordParameterConfig_ExportConfig(dsDpList, exceptionInfo, TRUE);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_WordParameter_ExportConfigOnlineValues() -> Error exporting CPC_WordParameter devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_WordParameter_ExportConfigOnlineValues()






//@}
