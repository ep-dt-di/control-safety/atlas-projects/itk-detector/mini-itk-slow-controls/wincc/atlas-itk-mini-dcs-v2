/**@file

// cpcWordStatusConfig.ctl
This library contains the import and export function of the CPC_WordStatus.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_WORDSTATUS_DPT_NAME = "CPC_WordStatus";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_WORDSTATUS_LENGTH                     = 10;

const unsigned UN_CONFIG_CPC_WORDSTATUS_UNIT                       = 1;
const unsigned UN_CONFIG_CPC_WORDSTATUS_FORMAT                     = 2;
const unsigned UN_CONFIG_CPC_WORDSTATUS_DEADBAND                   = 3;
const unsigned UN_CONFIG_CPC_WORDSTATUS_DEADBAND_TYPE              = 4;
const unsigned UN_CONFIG_CPC_WORDSTATUS_RANGEMAX                   = 5;
const unsigned UN_CONFIG_CPC_WORDSTATUS_RANGEMIN                   = 6;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ARCHIVE_ACTIVE             = 7;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ARCHIVE_TIME_FILTER        = 8;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDRESS_POSST              = 9;
const unsigned UN_CONFIG_CPC_WORDSTATUS_MAPPING                    = 10;

const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_LENGTH          = 6;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_PARAMETERS      = 1;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_MASTER_NAME     = 2;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_PARENTS         = 3;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_CHILDREN        = 4;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_TYPE            = 5;
const unsigned UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_SECOND_ALIAS    = 6;

const string CPC_PARAMS_NO_BIT    = "PNoStatus";
const string CPC_PARAMS_MULTI_BIT = "PMultipleStatus";

// SOFT_FE constants
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_UNIT                   = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_FORMAT                 = 2;
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_RANGEMAX               = 3;
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_RANGEMIN               = 4;
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_ARCHIVE_ACTIVE         = 5;
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_ARCHIVE_TIME_FILTER    = 6;
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_MAPPING                = 7;
const unsigned UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_LENGTH                 = 7;

//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_WordStatusConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["hasMapping"]	= true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dataType"] 		= CPC_UINT16;
    config["PosSt"] 		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_WordStatusConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_WORDSTATUS_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_WORDSTATUS_LENGTH;
  mTemp["UNIT"]                    = UN_CONFIG_CPC_WORDSTATUS_UNIT;
  mTemp["FORMAT"]                  = UN_CONFIG_CPC_WORDSTATUS_FORMAT;
  mTemp["DEADBAND"]                = UN_CONFIG_CPC_WORDSTATUS_DEADBAND;
  mTemp["DEADBAND_TYPE"]           = UN_CONFIG_CPC_WORDSTATUS_DEADBAND_TYPE ;
  mTemp["RANGEMAX"]                = UN_CONFIG_CPC_WORDSTATUS_RANGEMAX;
  mTemp["RANGEMIN"]                = UN_CONFIG_CPC_WORDSTATUS_RANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_WORDSTATUS_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_WORDSTATUS_ARCHIVE_TIME_FILTER;
  mTemp["ADDRESS_POSST"]           = UN_CONFIG_CPC_WORDSTATUS_ADDRESS_POSST;
  mTemp["MAPPING"]                 = UN_CONFIG_CPC_WORDSTATUS_MAPPING;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_PARENTS ;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_CHILDREN ;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_WORDSTATUS_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_WORDSTATUS"] = mTemp;


  mappingClear(mTemp);
  mTemp["UNIT"]                = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_UNIT;
  mTemp["FORMAT"]              = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_FORMAT;
  mTemp["RANGEMAX"]            = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_RANGEMAX;
  mTemp["RANGEMIN"]            = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_RANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]      = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"] = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_ARCHIVE_TIME_FILTER;
  mTemp["MAPPING"]             = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_MAPPING;
  mTemp["LENGTH"]              = UN_CONFIG_SOFT_FE_CPC_WORDSTATUS_LENGTH;

  g_mCpcConst["UN_CONFIG_SOFT_FE_CPC_WORDSTATUS"] = mTemp;
}




/** Check custom configuration
*/
CPC_WordStatusConfig_checkCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig

    mapping mParameters = cpcConfigGenericFunctions_getParametersMapping(dsConfigs, UN_CONFIG_CPC_WORDSTATUS_DPT_NAME, hasArchive);

    string sAlarmConfigLow      = mappingHasKey(mParameters, CPC_CONFIG_ALARM_VALUES_LOW)       ? mParameters[CPC_CONFIG_ALARM_VALUES_LOW]      : "";
    string sAlarmConfigMedium   = mappingHasKey(mParameters, CPC_CONFIG_ALARM_VALUES_MEDIUM)    ? mParameters[CPC_CONFIG_ALARM_VALUES_MEDIUM]   : "";
    string sAlarmConfigHigh     = mappingHasKey(mParameters, CPC_CONFIG_ALARM_VALUES_HIGH)      ? mParameters[CPC_CONFIG_ALARM_VALUES_HIGH]     : "";
    string sAlarmConfigSafety   = mappingHasKey(mParameters, CPC_CONFIG_ALARM_VALUES_SAFETY)    ? mParameters[CPC_CONFIG_ALARM_VALUES_SAFETY]   : "";

    dyn_string dsAlarmValues;
    dynAppend(dsAlarmValues, strsplit(sAlarmConfigLow, "|"));
    dynAppend(dsAlarmValues, strsplit(sAlarmConfigMedium, "|"));
    dynAppend(dsAlarmValues, strsplit(sAlarmConfigHigh, "|"));
    dynAppend(dsAlarmValues, strsplit(sAlarmConfigSafety, "|"));

    int iCorrectValue;
    for(int i=1; i<=dynlen(dsAlarmValues); i++){
        unConfigGenericFunctions_checkInt(strrtrim(strltrim(dsAlarmValues[i])), iCorrectValue, exceptionInfo);
    }

    bool bCorrectValue;
    string sAlarmActive = mappingHasKey(mParameters, CPC_CONFIG_ALARM_ACTIVE) ? mParameters[CPC_CONFIG_ALARM_ACTIVE] : "true";
    cpcConfigGenericFunctions_checkBool("CPC_CONFIG_ALARM_ACTIVE", sAlarmActive, bCorrectValue, exceptionInfo);

    string sAlarmAck = mappingHasKey(mParameters, CPC_CONFIG_ALARM_ACK) ? mParameters[CPC_CONFIG_ALARM_ACK] : "true";
    cpcConfigGenericFunctions_checkBool("CPC_CONFIG_ALARM_ACK", sAlarmAck, bCorrectValue, exceptionInfo);

    string sAlarmSMS = mappingHasKey(mParameters, CPC_CONFIG_ALARM_SMS) ? mParameters[CPC_CONFIG_ALARM_SMS] : "";
    if(sAlarmSMS != "") {
        dyn_string categories = strsplit(sAlarmSMS, "|");
        unProcessAlarm_checkCategory(categories, exceptionInfo);
    }

    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_WordStatusConfig_checkCustomConfig:" + getCatStr("unGeneration", "BADDATA"), "");
        return;
    }

//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_WordStatusConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
  string sConversion;

    string sDp = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt";

    // set the alarms

    // alarm values
    string sAlarmConfigLow      = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_VALUES_LOW, exceptionInfo, "");
    string sAlarmConfigMedium   = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_VALUES_MEDIUM, exceptionInfo, "");
    string sAlarmConfigHigh     = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_VALUES_HIGH, exceptionInfo, "");
    string sAlarmConfigSafety   = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_VALUES_SAFETY, exceptionInfo, "");

    // alarm ack
    string sAlarmAck = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_ACK, exceptionInfo, "true"); // by default expect alarm to require ack from user, unless ALARM_ACK=false
    bool bAlarmAck = sAlarmAck == "true";

    dyn_dyn_string dsAlarmValues;
    dsAlarmValues[1] = strsplit(sAlarmConfigLow, "|");
    dsAlarmValues[2] = strsplit(sAlarmConfigMedium, "|");
    dsAlarmValues[3] = strsplit(sAlarmConfigHigh, "|");
    dsAlarmValues[4] = strsplit(sAlarmConfigSafety, "|");

    dyn_string dsAlertClassesNames = makeDynString(CPC_CONFIG_ALARM_LOW_BASE_CLASS, CPC_CONFIG_ALARM_MEDIUM_BASE_CLASS, CPC_CONFIG_ALARM_HIGH_BASE_CLASS, CPC_CONFIG_ALARM_SAFETY_BASE_CLASS);

    if(dynlen(dsAlarmValues[1]) > 0 || dynlen(dsAlarmValues[2]) > 0 || dynlen(dsAlarmValues[3]) > 0 || dynlen(dsAlarmValues[4]) > 0) {
        // setting alarms
        string sAlertClass = "";

        mapping mLabels;
        cpcGenericObject_StringToMapping(cpcGenericDpFunctions_getMapping(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.PosSt"), mLabels);

        dyn_string dsAlarmDesc, dsAlertTexts, dsAlertClasses, dsLimits;
        dyn_string dsAlarmClassNames = makeDynString("Low Warning", "Warning", "Alarm", "Safety Alarm");

        // prepare OK-class
        // TODO: check
        dynAppend(dsAlertTexts, "");
        dynAppend(dsLimits, "");
        dynAppend(dsAlertClasses, "");

        for(int i= 1; i<=4; i++){ // iterate all the classess
            for(int j=1; j<=dynlen(dsAlarmValues[i]); j++){ // iterate alarms in the classess
                string sAlarmText = dsAlarmClassNames[i];
                if(mappingHasKey(mLabels, (string)dsAlarmValues[i][j])){
                    sAlarmText = sAlarmText + ": " + mLabels[(string)dsAlarmValues[i][j]];
                }
                dynAppend(dsAlertTexts, sAlarmText);
                dynAppend(dsLimits, dsAlarmValues[i][j]);
                bool bTmpAlarmAck = bAlarmAck && dsAlertClassesNames[i] != CPC_CONFIG_ALARM_LOW_BASE_CLASS && dsAlertClassesNames[i] != CPC_CONFIG_ALARM_MEDIUM_BASE_CLASS;
                dynAppend(dsAlertClasses, cpcConfigGenericFunctions_getAlertClass(bTmpAlarmAck , true /* bSMS */, "", dsAlertClassesNames[i]));
            }
        }

        // no unicos support for discrete alarm values, so using JCOP directly
        fwAlertConfig_set(
            sDp,                                        // string       dpe
            DPCONFIG_ALERT_NONBINARYSIGNAL,             // int          alertConfigType
            dsAlertTexts,                               // dyn_string   alertTexts
            dsLimits,                                   // dyn_float    alertLimits
            dsAlertClasses,                             // dyn_string   alertClasses
            makeDynString(),                            // dyn_string   summaryDpeList
            UN_CONFIG_DEFAULT_ALERT_PANEL,              // string       alertPanel
            UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS,   // dyn_string   alertPanelParameters
            UN_CONFIG_DEFAULT_ALERT_HELP,               // string       alertHelp
            exceptionInfo,                              // dyn_string   &exceptionInfo
            FALSE,                                      // bool         modifyOnly
            FALSE,                                      // bool         fallBackToSet
            "",                                         // string       addDpeInSummary
            TRUE,                                       // bool         storeInParamHistory
            TRUE);                                      // bool         discrete

        string sAlarmActive = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_ACTIVE, exceptionInfo, "true");   // by default alarm acuitve, unless CPC_CONFIG_ALARM_ACTIVE=false
        bool bAlarmActive = sAlarmActive == "true";
        if (bAlarmActive) {
            dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt:_alert_hdl.._active", true);
        }
    } else {
        fwAlertConfig_delete(sDp, exceptionInfo);
    }

    // sms categories
    string sAlarmSMS = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_SMS, exceptionInfo, "");
    dyn_string categories = strsplit(sAlarmSMS, "|");
    bool hasSMS = cpcConfigGenericFunctions_alertWithMail(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], "PosSt");
    if(hasSMS) {
        unProcessAlarm_removeDpFromList(sDp, exceptionInfo);
        if(dynlen(categories) > 0){
            unProcessAlarm_createCategory(categories, "", makeDynString(), exceptionInfo);
            for (int i = 1; i <= dynlen(categories); i++) {
                unProcessAlarm_addDpToList(sDp, categories[i], exceptionInfo);
            }
        }
    }

    // sms message
    string sAlarmSMSMessage = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_MESSAGE, exceptionInfo, "");
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_SMS_MESSAGE, makeDynString(sAlarmSMSMessage), exceptionInfo);

    dyn_string dsDescription;
    cpcConfigGenericFunctions_createAlarmDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN],
                                                     dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE],
                                                     dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS],
                                                     dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION],
                                                     "",
                                                     dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL],
                                                     dsDescription);
    unConfigGenericFunctions_setAlarmDescription(sDp, dsDescription, exceptionInfo);


  // Message conversion
  sConversion = cpcGenericDpFunctions_getDeviceProperty(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME],
                                                        CPC_CONFIG_CONVERSION,
                                                        exceptionInfo,
                                                        "");
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("Error in function CPC_WordStatusConfig_setCustomConfig() -> getting the device properties, due to: " + exceptionInfo[2]);
    return;
  }

  cpcConfigGenericFunctions_setMsgConv(sConversion,
                                       makeDynString(sDp),
                                       exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    DebugTN("Error in function CPC_WordStatusConfig_setCustomConfig() -> setting the message conversion: " + sConversion + ", due to: " + exceptionInfo[2]);
    return;
  }

}





/**
Purpose: Export CPC_WordStatus Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_WordStatusConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_WORDSTATUS_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");

    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");

    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordStatusConfig_ExportConfig() -> Error exporting PosSt deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordStatusConfig_ExportConfig() -> Error exporting PosSt range, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt", TRUE, dsDpParameters);

    dynAppend(dsDpParameters, cpcGenericDpFunctions_getMapping(sCurrentDp, ".ProcessInput.PosSt"));

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    // Get all the alarm parameters updated
    cpcExportGenericFunctions_updateAlarmParameters(sCurrentDp, ".ProcessInput.PosSt", sObject, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordStatusConfig_ExportConfig() -> Error exporting PosSt alarm parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordStatusConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_WordStatusConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_WordStatusConfig_ExportConfig()





/**
Purpose: Export CPC_WordStatus Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_WordStatus Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

CMW_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

SNMP_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}

DIP_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
}



void IEC104_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_WordStatusConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_WordStatus_ExportConfig() ->  Error exporting WordStatus devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_WordStatus_ExportConfig()





/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
void SOFT_FE_CPC_WordStatus_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    string sObject = UN_CONFIG_CPC_WORDSTATUS_DPT_NAME;
    int amount = dynlen(dsDpList);
    for (int i = 1; i <= amount; i++) {
        dyn_string dsDpParameters;
        string currentDP = dsDpList[i];

        if (!dpExists(currentDP)) { continue; }

        unExportDevice_getAllCommonParameters(currentDP, sObject, dsDpParameters);
        cpcExportGenericFunctions_getUnit(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getFormat(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getRange(currentDP, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getArchive(currentDP, dsDpParameters);
        dynAppend(dsDpParameters, cpcGenericDpFunctions_getMapping(currentDP, ".ProcessInput.PosSt"));
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_1, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_2, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_3, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT), dsDpParameters);
        // todo: get actual values of the parameters (from alarm config)
        cpcExportGenericFunctions_getParameters(currentDP, dsDpParameters, exceptionInfo);
        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions


//@}
