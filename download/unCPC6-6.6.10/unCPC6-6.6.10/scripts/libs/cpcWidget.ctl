/**@file

@brief This library contains implementations of widget animation fpr different widget types.

@author Alexey Merezhin (EN-ICE-PLC)

@par Creation Date
  31.10.2011

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Modification History

    23/01/2012: Alexey
        [UCPC-644] fixed cpcWidget_WidgetDoubleDamperDisconnection

    20/01/2012: Alexey
        [UCPC-599] added MFC renders

    13/12/2011: Alexey
        [UCPC-625] added DoubleDamper renders

    31/10/2011: Alexey
        [UCPC-593] created lib with widget animation and disconnection functions ported from the core

*/

/**Animate heater

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetHeaterAnimation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor, body4Color, body5Color;

// 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    body4Color = body1Color;
    body5Color = body1Color;
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            body4Color = "unWidget_AlternativeColor";
            if (iOn == 1) {
                body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
                body5Color = "unWidget_AlternativeColor";
            }
        } else if (iOn == 1) {
            // Onst = 1 and OffSt = 1
            body2BackColor = body1Color;
            body4Color = "unWidget_AlternativeColor";
        }
    }
// 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
                  "Body4", "foreCol", body4Color, "Body5", "foreCol", body5Color);
}

/**Animate heater disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetHeaterDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "foreCol", "unDataNoAccess", "Body5", "foreCol", "unDataNoAccess");
}

/**Animate valve

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetValveAnimation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor;

// 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            if (iOn == 1) {
                body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
            }
        } else if (iOn == 1) {                                                    // Onst = 1 and OffSt = 1
            body2BackColor = body1Color;
        }
    }
// 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor);
}

/**Animate valve disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetValveDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]");
}

/**Animate Analog 3 way valve

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_Widget3WayValveAnimation(int iOn, int iOff, string sBodyColor) {
    string sBody1Color = sBodyColor, sToBackColor, sOnBackColor, sOffBackColor;

    // 1. Set colors
    sOnBackColor = "unWidget_Background";
    sOffBackColor = "unWidget_Background";
    sToBackColor = "unWidget_Background";
    if (sBody1Color != "unDataNotValid") {
        sToBackColor = sBody1Color;
        if ((iOff == 1) && (iOn == 0)) {
            sOffBackColor = sBody1Color;              // OnSt = 0 and OffSt = 1
        } else if ((iOff == 0) && (iOn == 1)) {
            sOnBackColor = sBody1Color;                   // Onst = 1 and OffSt = 0
        } else if ((iOff == 0) && (iOn == 0)) {
            sOnBackColor = sBody1Color;                   // Onst = 0 and OffSt = 0
            sOffBackColor = sBody1Color;              // OnSt = 0 and OffSt = 1
        }
    }
    // 2. Animate body
    setMultiValue("Body1", "foreCol", sBody1Color,
                  g_sElemOn,   "fill", "[solid]", g_sElemOn,   "backCol", sOnBackColor,
                  g_sElemOff,  "fill", "[solid]", g_sElemOff,  "backCol", sOffBackColor,
                  g_sElemTo, "fill", "[solid]", g_sElemTo, "backCol", sToBackColor);
}

/**Animate Analog 3 way valve disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_Widget3WayValveDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", /*"Body1", "backCol", "unWidget_Background",*/
                  g_sElemOn, "backCol", "unWidget_Background", g_sElemOn, "fill", "[solid]",
                  g_sElemOff, "backCol", "unWidget_Background", g_sElemOff, "fill", "[solid]",
                  g_sElemTo, "backCol", "unWidget_Background", g_sElemTo, "fill", "[solid]");
}

/**Animate Analog 3 way valve

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetAnalog3WayValveAnimation(int iOn, int iOff, string sBodyColor) {
    string sBody1Color = sBodyColor, sToBackColor, sOnBackColor, sOffBackColor;

    // 1. Set colors
    sOnBackColor = "unWidget_Background";
    sOffBackColor = "unWidget_Background";
    sToBackColor = "unWidget_Background";
    if (sBody1Color != "unDataNotValid") {
        sToBackColor = sBody1Color;
        if ((iOff == 1) && (iOn == 0)) {
            sOffBackColor = sBody1Color;				// OnSt = 0 and OffSt = 1
        } else if ((iOff == 0) && (iOn == 1)) {
            sOnBackColor = sBody1Color;					// Onst = 1 and OffSt = 0
        } else if ((iOff == 0) && (iOn == 0)) {
            sOnBackColor = sBody1Color;					// Onst = 0 and OffSt = 0
            sOffBackColor = sBody1Color;				// OnSt = 0 and OffSt = 1
        }
    }
    // 2. Animate body
    setMultiValue("Body1", "foreCol", sBody1Color,
                  "pos_on",   "fill", "[solid]", "pos_on",   "backCol", sOnBackColor,
                  "pos_off",  "fill", "[solid]", "pos_off",  "backCol", sOffBackColor,
                  "pos_to", "fill", "[solid]", "pos_to", "backCol", sToBackColor);
}

/**Animate Analog 3 way valve disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetAnalog3WayValveDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", /*"Body1", "backCol", "unWidget_Background",*/
                  "pos_on", "backCol", "unWidget_Background", "pos_on", "fill", "[solid]",
                  "pos_off", "backCol", "unWidget_Background", "pos_off", "fill", "[solid]",
                  "pos_to", "backCol", "unWidget_Background", "pos_to", "fill", "[solid]");
}

/**Animate OnOff 3 way valve

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetOnOff3WayValveAnimation(int iOn, int iOff, string sBodyColor) {
    string sBody1Color = sBodyColor, sFromBackColor, sOnBackColor, sOffBackColor;

    // 1. Set colors
    sOnBackColor = "unWidget_Background";
    sOffBackColor = "unWidget_Background";
    sFromBackColor = "unWidget_Background";
    if (sBody1Color != "unDataNotValid") {
        sFromBackColor = sBody1Color;					//	filled in all cases.
        if ((iOff == 1) && (iOn == 0)) {
            sOffBackColor = sBody1Color;    // OnSt = 0 and OffSt = 1
        }

        if ((iOff == 0) && (iOn == 1)) {
            sOnBackColor = sBody1Color;    // Onst = 1 and OffSt = 0
        }
    }
    // 2. Animate body
    setMultiValue("Body1", "foreCol", sBody1Color,
                  "pos_on",   "fill", "[solid]", "pos_on",   "backCol", sOnBackColor,
                  "pos_off",  "fill", "[solid]", "pos_off",  "backCol", sOffBackColor,
                  "pos_from", "fill", "[solid]", "pos_from", "backCol", sFromBackColor);
}

/**Animate Local 3 way valve

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetLocal3WayValveAnimation(int iOn, int iOff, string sBodyColor) {
    cpcWidget_WidgetOnOff3WayValveAnimation(iOn, iOff, sBodyColor);
}

/**Animate OnOff 3 way valve disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetOnOff3WayValveDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", /*"Body1", "backCol", "unWidget_Background",*/
                  "pos_on", "backCol", "unWidget_Background", "pos_on", "fill", "[solid]",
                  "pos_off", "backCol", "unWidget_Background", "pos_off", "fill", "[solid]",
                  "pos_from", "backCol", "unWidget_Background", "pos_from", "fill", "[solid]");
}

/**Animate Local 3 way valve disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetLocal3WayValveDisconnection() {
    cpcWidget_WidgetOnOff3WayValveDisconnection();
}

/**Animate OnOff Knob

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetOnOffKnobAnimation(int iOn, int iOff, string sBodyColor) {
    string sBody1Color = sBodyColor, sLabelOnColor = sBodyColor, sLabelOffColor = sBodyColor;
    string sLabelOnBackground = "_Transparent", sLabelOffBackground = "_Transparent";
    
    string onLabel, offLabel;
    CPC_OnOff_getLabels(g_sDpName, onLabel, offLabel, "On / Opened", "Off / Closed");
    
    float fRotation  = 0.0;
    if (sBody1Color != "unDataNotValid") {
        if(iOn == 1) {
            if (iOff == 0) {
                fRotation = 315.0;
            }
            sLabelOnColor = "unWidget_Background";
            sLabelOnBackground = sBodyColor;
        }
        
        if (iOff == 1) {
            if (iOn == 0){
                fRotation = 45.0;
            }
            sLabelOffColor = "unWidget_Background";
            sLabelOffBackground = sBodyColor;
        }
    }
    
    // 2. Animate body
    setMultiValue("Body1", "foreCol",   sBody1Color,
                  "Body2", "foreCol",   sBody1Color,
                  "Body2", "backCol",   sBody1Color,
                  "Body2", "rotation",  fRotation,
                  "OffLabel", "foreCol", sLabelOffColor,
                  "OffLabel", "backCol", sLabelOffBackground,
                  "OffLabel", "text", offLabel,
                  "OnLabel", "foreCol", sLabelOnColor,
                  "OnLabel", "backCol", sLabelOnBackground,
                  "OnLabel", "text", onLabel
                  );
}

/**Animate Knob disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetOnOffKnobDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess",
                  "Body2", "foreCol", "unDataNoAccess",
                  "Body2", "backCol", "unDataNoAccess",
                  "OnLabel", "foreCol", "unDataNoAccess",
                  "OffLabel", "foreCol", "unDataNoAccess");
}

/**Animate inverted valve

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetInvertedValveAnimation(int iOn, int iOff, string sBodyColor) {
    cpcWidget_WidgetValveAnimation(iOff, iOn, sBodyColor);
}

/**Animate inverted valve disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetInvertedValveDisconnection() {
    cpcWidget_WidgetValveDisconnection();
}

/**Animate motor

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetMotorAnimation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor, body4BackColor, body5BackColor, body6Color, body7Color;

// 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    body4BackColor = "unWidget_Background";
    body5BackColor = "unWidget_Background";
    body6Color = body1Color;
    body7Color = body1Color;
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            body3BackColor = body1Color;
            body6Color = "unWidget_AlternativeColor";
            if (iOn == 1) {
                body4BackColor = body1Color;					// Onst = 1 and OffSt = 0
                body5BackColor = body1Color;
                body7Color = "unWidget_AlternativeColor";
            }
        } else if (iOn == 1) {
            body2BackColor = body1Color;					// OnSt = 1 and OffSt = 1
            body3BackColor = body1Color;
            body6Color = "unWidget_AlternativeColor";
        }
    }
// 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
                  "Body4", "fill", "[solid]", "Body4", "backCol", body4BackColor,
                  "Body5", "fill", "[solid]", "Body5", "backCol", body5BackColor,
                  "Body6", "foreCol", body6Color, "Body7", "foreCol", body7Color);
}

/**Animate motor disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetMotorDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "backCol", "unWidget_Background", "Body4", "fill", "[solid]",
                  "Body5", "backCol", "unWidget_Background", "Body5", "fill", "[solid]",
                  "Body6", "foreCol", "unDataNoAccess", "Body7", "foreCol", "unDataNoAccess");
}


/**Animate Stepping Motor
*/

void cpcWidget_WidgetSteppingMotorAnimation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor, body4Color = sBodyColor, body5Color = sBodyColor;

    // 1. Set colors
    body2BackColor = "unWidget_Background";
    body3BackColor = "unWidget_Background";

    if (body1Color != "unDataNotValid") {
        if (iOff == 1) {
            setMultiValue("Body2", "backCol", sBodyColor);
        } else {
            setMultiValue("Body2", "backCol", "");
        }
        if (iOn == 1) {
            setMultiValue("Body3", "backCol", sBodyColor);
        } else {
            setMultiValue("Body3", "backCol", "");
        }
    }

    // 2. Animate body
    setMultiValue("Body1", "foreCol", sBodyColor,
                  "Body2", "foreCol", sBodyColor,
                  "Body3", "foreCol", sBodyColor,
                  "Body4", "foreCol", sBodyColor,
                  "Body5", "foreCol", sBodyColor,
                  "Body6", "foreCol", sBodyColor,
                  "Body7", "foreCol", sBodyColor);
}

/**Animate motor disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetSteppingMotorDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "foreCol", "unDataNoAccess", "Body5", "foreCol", "unDataNoAccess",
                  "Body6", "foreCol", "unDataNoAccess", "Body7", "foreCol", "unDataNoAccess");
}

/**Animate 3-body square

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetSquare3Animation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor;

// 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            if (iOn == 1) {
                body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
            }
        }
    }
// 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor);
}

/**Animate 3-body square disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetSquare3Disconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]");
}

/**Animate 4-body square

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetSquare4Animation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor, body4Color;

// 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    body4Color = body1Color;
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            if (iOn == 1) {
                body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
            }
        } else if (iOn == 1) {
            body2BackColor = body1Color;    // OnSt = 1 and OffSt = 1
        }
    }
// 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
                  "Body4", "foreCol", body4Color);
}

/**Animate 4-body square disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetSquare4Disconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "foreCol", "unDataNoAccess");
}

/**Animate turbine

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetTurbineAnimation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor, body4BackColor, body5BackColor, body6Color;

// 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    body4BackColor = "unWidget_Background";
    body5BackColor = "unWidget_Background";
    body6Color = body1Color;
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            body3BackColor = body1Color;
            if (iOn == 1) {
                body4BackColor = body1Color;					// Onst = 1 and OffSt = 0
                body5BackColor = body1Color;
            }
        }
    }
// 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
                  "Body4", "fill", "[solid]", "Body4", "backCol", body4BackColor,
                  "Body5", "fill", "[solid]", "Body5", "backCol", body5BackColor,
                  "Body6", "foreCol", body6Color);
}

/**Animate turbine disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetTurbineDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "backCol", "unWidget_Background", "Body4", "fill", "[solid]",
                  "Body5", "backCol", "unWidget_Background", "Body5", "fill", "[solid]",
                  "Body6", "foreCol", "unDataNoAccess");
}

/**Animate compressor

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetCompressorAnimation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor, body4BackColor, body5BackColor;
    string body6Color, body7Color, body8Color, body9Color;

// 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    body4BackColor = "unWidget_Background";
    body5BackColor = "unWidget_Background";
    body6Color = body1Color;
    body7Color = body1Color;
    body8Color = body1Color;
    body9Color = body1Color;
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            body3BackColor = body1Color;
            body6Color = "unWidget_AlternativeColor";
            body8Color = "unWidget_AlternativeColor";
            if (iOn == 1) {
                body4BackColor = body1Color;					// Onst = 1 and OffSt = 0
                body5BackColor = body1Color;
                body7Color = "unWidget_AlternativeColor";
                body9Color = "unWidget_AlternativeColor";
            }
        }
    }
// 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
                  "Body4", "fill", "[solid]", "Body4", "backCol", body4BackColor,
                  "Body5", "fill", "[solid]", "Body5", "backCol", body5BackColor,
                  "Body6", "foreCol", body6Color, "Body7", "foreCol", body7Color,
                  "Body8", "foreCol", body8Color, "Body9", "foreCol", body9Color);
}

/**Animate compressor disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetCompressorDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "backCol", "unWidget_Background", "Body4", "fill", "[solid]",
                  "Body5", "backCol", "unWidget_Background", "Body5", "fill", "[solid]",
                  "Body6", "foreCol", "unDataNoAccess", "Body7", "foreCol", "unDataNoAccess",
                  "Body8", "foreCol", "unDataNoAccess", "Body9", "foreCol", "unDataNoAccess");
}

/**Animate Controller

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetControllerAnimation(int iOn, int iOff, string sBodyColor) {
    setMultiValue("Body1", "foreCol", sBodyColor, "Body1", "fill", "[solid]", "Body1", "backCol", "unWidget_Background",
                  "Body2", "foreCol", sBodyColor, "Body3", "foreCol", sBodyColor);
}

/**Animate controller disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetControllerDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "fill", "[solid]", "Body1", "backCol", "unWidget_Background",
                  "Body2", "foreCol", "unDataNoAccess", "Body3", "foreCol", "unDataNoAccess");
}

/**Animate Controller

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetControllerSetPointAnimation(int iOn, int iOff, string sBodyColor) {
    setMultiValue("Body1", "foreCol", sBodyColor, "Body1", "fill", "[solid]", "Body1", "backCol", "unWidget_Background",
                  "Body2", "foreCol", sBodyColor, "Body3", "foreCol", sBodyColor);
}

/**Animate controller disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetControllerSetPointDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "fill", "[solid]", "Body1", "backCol", "unWidget_Background",
                  "Body2", "foreCol", "unDataNoAccess", "Body3", "foreCol", "unDataNoAccess");
}

/**Animate pump

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetPumpAnimation(int iOn, int iOff, string sBodyColor) {
    cpcWidget_WidgetMotorAnimation(iOn, iOff, sBodyColor);
}

/**Animate pump disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetPumpDisconnection() {
    cpcWidget_WidgetMotorDisconnection();
}

/**Animate damper

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetDamperAnimation(int iOn, int iOff, string sBodyColor) {
    string backColor = "unWidget_Background";
    bool body2Enabled = false, body3Enabled = false;
    if (sBodyColor != "unDataNotValid") {
        if (iOn == 0 && iOff == 1) {
            body2Enabled = true;
        } else if (iOn == 1 && iOff == 0) {
            backColor = sBodyColor;
            body3Enabled = true;
        }
    }
    setMultiValue("Body1", "foreCol", sBodyColor,
                  "Body1", "fill", "[solid]", "Body1", "backCol", backColor,
                  "Body2", "foreCol", sBodyColor, "Body2", "visible", body2Enabled,
                  "Body2", "fill", "[solid]", "Body2", "backCol", backColor,
                  "Body3", "foreCol", sBodyColor, "Body3", "visible", body3Enabled,
                  "Body3", "fill", "[solid]", "Body3", "backCol", backColor,
                  "Body4", "foreCol", sBodyColor, "Body4", "visible", !body2Enabled && !body3Enabled,
                  "Body4", "fill", "[solid]", "Body4", "backCol", sBodyColor
                 );

}

/**Animate damper disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetDamperDisconnection() {
    cpcWidget_WidgetDamperAnimation(0, 0, "unDataNoAccess");
}

/**Animate double damper

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetDoubleDamperAnimation(int iOn, int iOff, string sBodyColor) {
    string backColor = "unWidget_Background";
    bool body2Enabled = false, body3Enabled = false;
    if (sBodyColor != "unDataNotValid") {
        if (iOn == 0 && iOff == 1) {
            body2Enabled = true;
        } else if (iOn == 1 && iOff == 0) {
            backColor = sBodyColor;
            body3Enabled = true;
        }
    }
    setMultiValue("Body1", "foreCol", sBodyColor,
                  "Body1", "fill", "[solid]", "Body1", "backCol", backColor,
                  "Body2", "foreCol", sBodyColor, "Body2", "visible", body2Enabled,
                  "Body2", "fill", "[solid]", "Body2", "backCol", backColor,
                  "Body3", "foreCol", sBodyColor, "Body3", "visible", body3Enabled,
                  "Body3", "fill", "[solid]", "Body3", "backCol", backColor,
                  "Body4", "foreCol", sBodyColor, "Body4", "visible", !body2Enabled && !body3Enabled,
                  "Body4", "fill", "[solid]", "Body4", "backCol", sBodyColor,
                  "Body5", "foreCol", sBodyColor,
                  "Body5", "fill", "[solid]", "Body5", "backCol", backColor,
                  "Body6", "foreCol", sBodyColor, "Body6", "visible", body2Enabled,
                  "Body6", "fill", "[solid]", "Body6", "backCol", backColor,
                  "Body7", "foreCol", sBodyColor, "Body7", "visible", body3Enabled,
                  "Body7", "fill", "[solid]", "Body7", "backCol", backColor,
                  "Body8", "foreCol", sBodyColor, "Body8", "visible", !body2Enabled && !body3Enabled,
                  "Body8", "fill", "[solid]", "Body8", "backCol", sBodyColor
                 );

}

/**Animate double damper disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetDoubleDamperDisconnection() {
    cpcWidget_WidgetDoubleDamperAnimation(0, 0, "unDataNoAccess");
}

/**Animate disconnection for body with only one shape

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetBody1Disconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background", "Body1", "fill", "[solid]");
}

/**Animate disconnection for DIDO

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetDIDODisconnection() {
    cpcWidget_WidgetBody1Disconnection();
}

void cpcWidget_WidgetDIPumpDisconnection() {
    cpcWidget_WidgetBody1Disconnection();
    setMultiValue("Body2", "foreCol", "unDataNoAccess");
}

/**Animate disconnection for AIAO

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetAIAODisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess"/*, "Body1", "backCol", "unWidget_Background", "Body1", "fill", "[solid]"*/);
}

/**Animate PCO air

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetAirAnimation(int iOn, int iOff, string sBodyColor) {
    string body1Color = sBodyColor, body2BackColor, body3BackColor, body4Color;

    // 1. Set colors
    body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
    body3BackColor = "unWidget_Background";
    body4Color = body1Color;
    if (body1Color != "unDataNotValid") {
        if (iOff == 0) {
            body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
            if (iOn == 1) {
                body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
            }
        } else if (iOn == 1) {
            body2BackColor = body1Color;					// OnSt = 1 and OffSt = 1
        }
    }
    // 2. Animate body
    setMultiValue("Body1", "foreCol", body1Color,
                  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
                  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
                  "Body4", "foreCol", body4Color,
                  "Body5", "foreCol", body4Color,
                  "Body6", "foreCol", body4Color);
}

/**Animate pco air disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetAirDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "foreCol", "unDataNoAccess",
                  "Body5", "foreCol", "unDataNoAccess",
                  "Body6", "foreCol", "unDataNoAccess");
}

/**Animate Mass Flow Controller

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param iOn on state
@param iOff off state
@param sBodyColor body color
*/
void cpcWidget_WidgetMFCAnimation(int iOn, int iOff, string sBodyColor) {
    string body2Color = sBodyColor, body3Color = "unWidget_Background";

    if (sBodyColor != "unDataNotValid") {
        if (iOff == 0 && iOn == 1) { // explicitly on
            body3Color = sBodyColor;
        } else if (iOff == 1 && iOn == 0) { // explicitly off
            body2Color = "unWidget_Background";
        }
    }
    setMultiValue("Body1", "foreCol", sBodyColor, "Body1", "backCol", "",
                  "Body2", "foreCol", sBodyColor,  "Body2", "backCol", body2Color, "Body2", "fill", "[solid]",
                  "Body4", "foreCol", sBodyColor,  "Body4", "backCol", body2Color, "Body4", "fill", "[solid]",
                  "Body3", "foreCol", sBodyColor, "Body3", "backCol", body3Color, "Body3", "fill", "[solid]",
                  "Body5", "foreCol", sBodyColor, "Body5", "backCol", body3Color, "Body5", "fill", "[solid]",
                  "Line4", "foreCol", sBodyColor, "Line5", "foreCol", sBodyColor, "Line6", "foreCol", sBodyColor);
}

/**Animate Mass Flow Controller disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void cpcWidget_WidgetMFCDisconnection() {
    setMultiValue("Body1", "foreCol", "unDataNoAccess",
                  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
                  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
                  "Body4", "foreCol", "unDataNoAccess", "Body4", "backCol", "unWidget_Background",  "Body4", "fill", "[solid]",
                  "Body5", "foreCol", "unDataNoAccess", "Body5", "backCol", "unWidget_Background",  "Body5", "fill", "[solid]",
                  "Line4", "foreCol", "unDataNoAccess", "Line5", "foreCol", "unDataNoAccess", "Line6", "foreCol", "unDataNoAccess",
                  "Display1", "foreCol", "unDataNoAccess", "Display2", "foreCol", "unDataNoAccess");
}

/**Animate regulation letters

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dsCtrl control device list
*/
void cpcWidget_WidgetCtrlAnimationCB(dyn_string dpes, dyn_anytype values, dyn_string dsCtrl) {
    int activeCtrlIdx = 0;
    for (int i = dynlen(dpes) - dynlen(dsCtrl) + 1; i <= dynlen(dpes); i++) {
        if (getBit(values[i], CPC_StsReg01_ACTIVE)) {
            activeCtrlIdx = i;
            break;
        }
    }
    if (activeCtrlIdx == 0) {
        setMultiValue("RegulationText1", "visible", false,
                      "RegulationText2", "visible", false);
    } else {
        bit32 activeStsReg = values[activeCtrlIdx];
        string operationModeLetter, operationModeColor, workingStateLetter, workingStateColor;
        unGenericObject_WidgetControlStateAnimation(activeStsReg, makeDynInt(CPC_StsReg01_AUMOST, CPC_StsReg01_MMOST, CPC_StsReg01_FOMOST), makeDynString("A", CPC_WIDGET_TEXT_CONTROL_MANUAL, CPC_WIDGET_TEXT_CONTROL_FORCED), operationModeLetter, operationModeColor);
        unGenericObject_WidgetControlStateAnimation(activeStsReg, makeDynInt(CPC_StsReg01_REGST, CPC_StsReg01_TRST, CPC_StsReg01_OUTPST), makeDynString("R", "T", "P"), workingStateLetter, workingStateColor);

        if (g_bSystemConnected) {
            setMultiValue("RegulationText1", "text", operationModeLetter, "RegulationText1", "visible", true,
                          "RegulationText2", "text", workingStateLetter, "RegulationText2", "visible", true);
        }
    }
}


