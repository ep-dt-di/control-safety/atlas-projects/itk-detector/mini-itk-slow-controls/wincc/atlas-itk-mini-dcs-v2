
const string UN_CONFIG_CPC_TESTOBJECT_DPT_NAME = "CPC_TestObject";

const unsigned UN_CONFIG_CPC_TESTOBJECT_LENGTH = 14;

const unsigned UN_CONFIG_CPC_TESTOBJECT_UNIT					= 1;
const unsigned UN_CONFIG_CPC_TESTOBJECT_FORMAT					= 2;
const unsigned UN_CONFIG_CPC_TESTOBJECT_RANGEMAX				= 3;
const unsigned UN_CONFIG_CPC_TESTOBJECT_RANGEMIN				= 4;
const unsigned UN_CONFIG_CPC_TESTOBJECT_DEADBAND				= 5;
const unsigned UN_CONFIG_CPC_TESTOBJECT_DEADBAND_TYPE			= 6;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ARCHIVE_ACTIVE			= 7;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ARCHIVE_TIME_FILTER		= 8;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDRESS_STSREG01		= 9;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDRESS_EVSTSREG01		= 10;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDRESS_POSST			= 11;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDRESS_AUPOSRST		= 12;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDRESS_MANREG01		= 13;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDRESS_MPOSR			= 14;


const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_LENGTH		= 7;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_MASKEVENT	= 1;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_PARAMETERS	= 2;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_MASTER_NAME	= 3;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_PARENTS		= 4;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_CHILDREN		= 5;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_TYPE			= 6;
const unsigned UN_CONFIG_CPC_TESTOBJECT_ADDITIONAL_SECOND_ALIAS	= 7;

mapping CPC_TestObjectConfig_getConfig() {
	mapping config;
	mapping props;
	
    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg01";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg01"] 		= props;
	
    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg01";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg01"] 	= props;
	
	mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dataType"] 		= CPC_FLOAT;
    config["PosSt"] 		= props;
	
	mappingClear(props);
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.AuPosRSt";
    props["dataType"] 		= CPC_FLOAT;
    config["AuPosRSt"] 		= props;
	
	mappingClear(props);
    props["address"]      = ".ProcessInput.AlSt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALST;
    props["isAlarm"]      = true;
    config["AlSt"]        = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlUnAck";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALUNACK;
    config["AlUnAck"]     = props;
	
	mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg01";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg01"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MPosR";
    props["dataType"] 		= CPC_FLOAT;
    config["MPosR"] 		= props;
	
	return config;
}

dyn_string CPC_TestObjectConfig_getParamNames() {
    return makeDynString(CPC_PARAMS_FSPOSON, CPC_PARAMS_MRESTART, CPC_PARAMS_RSTARTFS, CPC_PARAMS_HLDRIVE);
}

CPC_TestObjectConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    string sObject = UN_CONFIG_CPC_TESTOBJECT_DPT_NAME;
    int amount = dynlen(dsDpList);
    for (int i = 1; i <= amount; i++) {
        dyn_string dsDpParameters;
        string currentDP = dsDpList[i];

        if (!dpExists(currentDP)) { continue; }

        unExportDevice_getAllCommonParameters(currentDP, sObject, dsDpParameters);
        cpcExportGenericFunctions_getUnit(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getFormat(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getRange(currentDP, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getDeadband(currentDP, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getArchive(currentDP, dsDpParameters);
        cpcExportGenericFunctions_processAddress(currentDP, sObject, "StsReg01", 	true, dsDpParameters);
        cpcExportGenericFunctions_processAddress(currentDP, sObject, "evStsReg01", 	true, dsDpParameters);
        cpcExportGenericFunctions_processAddress(currentDP, sObject, "PosSt", 		true, dsDpParameters);
        cpcExportGenericFunctions_processAddress(currentDP, sObject, "AuPosRSt",    true, dsDpParameters);
        cpcExportGenericFunctions_processAddress(currentDP, sObject, "ManReg01", 	false, dsDpParameters);
        cpcExportGenericFunctions_processAddress(currentDP, sObject, "MPosR", 		false, dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_1, makeDynString(), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_3, makeDynString("ProcessInput.evStsReg01"), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_2, makeDynString("ProcessInput.PosSt"), dsDpParameters);
        cpcExportGenericFunctions_getMaskEvent(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getParameters(currentDP, dsDpParameters, exceptionInfo);
        cpcExportGenericFunctions_getMetainfo(currentDP, dsDpParameters, exceptionInfo);
        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
}

cpcButton_TestButton(string deviceName, string sDpType, bool buttonEnabled, bool selected, dyn_string dsData, dyn_string &exceptionInfo) {
    exceptionInfo = makeDynString("generic");
}

cpcButton_DT_TestButton(string deviceName, string sDpType, bool buttonEnabled, bool selected, dyn_string dsData, dyn_string &exceptionInfo) {
    exceptionInfo = makeDynString("specific");
}
