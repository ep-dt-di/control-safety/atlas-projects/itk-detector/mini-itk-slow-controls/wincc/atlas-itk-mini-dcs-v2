#uses "fwUnitTestComponentAsserts.ctl"
#uses "CPC_declarations.ctl"
#uses "S7Constant_declarations.ctl"
#uses "fwConfigs/fwPeriphAddress.ctl"
#uses "cpcConfigGenericFunctions.ctl"
#uses "cpcDpFunc.ctl"
#uses "test/unCPC6/cpcTestObjectConfig.ctl"

void test_cpcButton_isAckEnabled_CPC_ProcessControlObject(){
    //cpcButton_isAckEnabled(string deviceName, string dpType, bit32 stsReg01Value, bool bStsReg01Bad, bit32 stsReg02Value, bool bStsReg02Bad)
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_ProcessControlObject", 0, true, 0, false));
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_ProcessControlObject", 0, false, 0, true));
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_ProcessControlObject", 0, false, 5000, false));
    assertTrue(cpcButton_isAckEnabled("device_name", "CPC_ProcessControlObject", 32, false, 0, false));
    assertTrue(cpcButton_isAckEnabled("device_name", "CPC_ProcessControlObject", 67, false, 1000, false));
    }
    
void test_cpcButton_isAckEnabled_CPC_MassFlowController(){
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_MassFlowController", 0, true, 0, false));
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_MassFlowController", 0, false, 0, true));
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_MassFlowController", 0, false, 5000, false));
    assertTrue(cpcButton_isAckEnabled("device_name", "CPC_MassFlowController", 32, false, 0, false));
    assertTrue( cpcButton_isAckEnabled("device_name", "CPC_MassFlowController", 67, false, 1000, false));
}

void test_cpcButton_isAckEnabled_CPC_OtherDevices(){
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_DigitalInput", 0, true, 0, false));
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_DigitalInput", 0, false, 0, true));
    assertFalse(cpcButton_isAckEnabled("device_name", "CPC_DigitalInput", 0, false, 5000, false));
    assertTrue(cpcButton_isAckEnabled("device_name", "CPC_DigitalInput", 32, false, 0, false));
    assertTrue( cpcButton_isAckEnabled("device_name", "CPC_DigitalInput", 67, false, 1000, false));
}

void test_cpcButton_IsAllowRestartEnabled_CPC_ProcessControlObject() {
    //cpcButton_IsAllowRestartEnabled(string deviceName, string dpType, bit32 stsReg01Value, bool bStsReg01Bad, bit32 stsReg02Value, bool bStsReg02Bad, bool bRstartFS) {
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 163, true, 1134, false, false));
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 167, false, 1234, true, false));
    assertTrue(cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 40, false, 0, false, false));
    assertTrue( cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 0, false, 0, false, true));
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 512, false, 0, false, false));
    assertTrue(cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 0, false, 1024, false, false));
    assertTrue(cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 1024, false, 0, false, false));
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_ProcessControlObject", 0, false, 5000, false, false));
}

void test_cpcButton_IsAllowRestartEnabled_CPC_OtherDevices() {
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_MassFlowController", 167, true, 1234, false, false));
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_MassFlowController", 167, false, 1234, true, false));
    assertTrue(cpcButton_IsAllowRestartEnabled("device_name", "CPC_MassFlowController", 0, false, 0, false, true));
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_MassFlowController", 0, false, 1024, false, false));
    assertTrue(cpcButton_IsAllowRestartEnabled("device_name", "CPC_MassFlowController", 0, false, 1000, false, false));
    assertFalse(cpcButton_IsAllowRestartEnabled("device_name", "CPC_MassFlowController", 0, false, 5000, false, false));
}

void test_cpcButton_ButtonSetState() {
    dyn_string dsUserAccess, dsData, exceptionInfo;
    dyn_string dsButtons = makeDynString("TestButton");
    string deviceName;
    
    dsUserAccess = makeDynString("");
    dsData = makeDynString("", "");
    deviceName = "devname";
    cpcButton_ButtonSetState(dsButtons, dsUserAccess, deviceName, "", dsData, exceptionInfo);
    assertEqual(makeDynString("generic"), exceptionInfo);
    dynClear(exceptionInfo);

    cpcButton_ButtonSetState(dsButtons, dsUserAccess, deviceName, "DT1", dsData, exceptionInfo);
    assertEqual(makeDynString("generic"), exceptionInfo);
    dynClear(exceptionInfo);

    cpcButton_ButtonSetState(dsButtons, dsUserAccess, deviceName, "DT", dsData, exceptionInfo);
    assertEqual(makeDynString("specific"), exceptionInfo);
    dynClear(exceptionInfo);
}
