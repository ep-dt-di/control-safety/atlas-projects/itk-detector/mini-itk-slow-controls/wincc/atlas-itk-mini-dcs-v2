#uses "fwUnitTestComponentAsserts.ctl"
#uses "CPC_declarations.ctl"
#uses "S7Constant_declarations.ctl"
#uses "fwConfigs/fwPeriphAddress.ctl"
#uses "cpcConfigGenericFunctions.ctl"
#uses "test/unCPC6/cpcTestObjectConfig.ctl"
string TEST_DP_NAME = "my_test_dp";

void assertErrorAndClean(string sSeverity, string sMessage, dyn_string& exceptionInfo){
    assertError(sSeverity, sMessage, exceptionInfo);
    dynClear(exceptionInfo);
}

dyn_string makeEmptyDynString(int amount) {
    dyn_string result;
    for (int i = 1; i <= amount; i++) {
        dynAppend(result, "");
    }
    return result;
}

void test_setup() {
    dpCreate(TEST_DP_NAME, "CPC_TestObject");
}

void test_teardown() {
   dpDelete(TEST_DP_NAME, "CPC_TestObject");
}

void test_splitCommaSeparatedString() {
    assertEqual(makeDynString(), splitCommaSeparatedString(""));
    assertEqual(makeDynString("a"), splitCommaSeparatedString("a"));
    assertEqual(makeDynString("a", "b"), splitCommaSeparatedString("a,b"));
    assertEqual(makeDynString("a", "b"), splitCommaSeparatedString("a,b,"));
    assertEqual(makeDynString("a", "b", ""), splitCommaSeparatedString("a,b,,"));
    assertEqual(makeDynString("", "", ""), splitCommaSeparatedString(",,,"));
}

void test_cpcConfigGenericFunctions_translateDataTypeForS7Check() {
    assertEqual(UN_CONFIG_S7_PLC_DATATYPE_WORD, cpcConfigGenericFunctions_translateDataTypeForS7Check(CPC_INT16));
    assertEqual(UN_CONFIG_S7_PLC_DATATYPE_DWORD, cpcConfigGenericFunctions_translateDataTypeForS7Check(CPC_INT32));
    assertEqual(UN_CONFIG_S7_PLC_DATATYPE_WORD, cpcConfigGenericFunctions_translateDataTypeForS7Check(CPC_UINT16));
    assertEqual(UN_CONFIG_S7_PLC_DATATYPE_BOOL, cpcConfigGenericFunctions_translateDataTypeForS7Check(CPC_BOOL));
    assertEqual(UN_CONFIG_S7_PLC_DATATYPE_FLOAT, cpcConfigGenericFunctions_translateDataTypeForS7Check(CPC_FLOAT));
}

void test_cpcConfigGenericFunctions_translateDataType() {
    assertEqual(PVSS_MODBUS_INT16, cpcConfigGenericFunctions_translateDataType(UN_PLC_DPTYPE, CPC_INT16));
    assertEqual(PVSS_MODBUS_INT32, cpcConfigGenericFunctions_translateDataType(UN_PLC_DPTYPE, CPC_INT32));
    assertEqual(PVSS_MODBUS_UINT16, cpcConfigGenericFunctions_translateDataType(UN_PLC_DPTYPE, CPC_UINT16));
    assertEqual(PVSS_MODBUS_BOOL, cpcConfigGenericFunctions_translateDataType(UN_PLC_DPTYPE, CPC_BOOL));
    assertEqual(PVSS_MODBUS_FLOAT, cpcConfigGenericFunctions_translateDataType(UN_PLC_DPTYPE, CPC_FLOAT));

    assertEqual(PVSS_S7_INT16, cpcConfigGenericFunctions_translateDataType(S7_PLC_DPTYPE, CPC_INT16));
    assertEqual(PVSS_S7_INT32, cpcConfigGenericFunctions_translateDataType(S7_PLC_DPTYPE, CPC_INT32));
    assertEqual(PVSS_S7_UINT16, cpcConfigGenericFunctions_translateDataType(S7_PLC_DPTYPE, CPC_UINT16));
    assertEqual(PVSS_S7_BOOL, cpcConfigGenericFunctions_translateDataType(S7_PLC_DPTYPE, CPC_BOOL));
    assertEqual(PVSS_S7_FLOAT, cpcConfigGenericFunctions_translateDataType(S7_PLC_DPTYPE, CPC_FLOAT));

    // TODO
    assertEqual(OPCUA_INT16, cpcConfigGenericFunctions_translateDataType(CPC_OPCUA_DPTYPE, CPC_INT16));
    assertEqual(OPCUA_INT32, cpcConfigGenericFunctions_translateDataType(CPC_OPCUA_DPTYPE, CPC_INT32));
    assertEqual(OPCUA_UINT16, cpcConfigGenericFunctions_translateDataType(CPC_OPCUA_DPTYPE, CPC_UINT16));
    assertEqual(OPCUA_BOOL, cpcConfigGenericFunctions_translateDataType(CPC_OPCUA_DPTYPE, CPC_BOOL));
    assertEqual(OPCUA_FLOAT, cpcConfigGenericFunctions_translateDataType(CPC_OPCUA_DPTYPE, CPC_FLOAT));

}

void test_cpcConfigGenericFunctions_processArchives() {
    dyn_string dsConfigs;
    cpcConfigGenericFunctions_processArchives(1, false, dsConfigs);
    assertEmpty(dsConfigs);
    dsConfigs = makeEmptyDynString(6);
    cpcConfigGenericFunctions_processArchives(1, true, dsConfigs);
    assertEqual("", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL]);
    assertEqual("", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG]);
    assertEqual("", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT]);
    dsConfigs = makeEmptyDynString(9);
    dsConfigs[7] = "bool";;
    dsConfigs[8] = "analog";
    dsConfigs[9] = "event";
    cpcConfigGenericFunctions_processArchives(6, true, dsConfigs);
    assertEqual("bool", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL]);
    assertEqual("analog", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG]);
    assertEqual("event", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT]);
}

void test_cpcConfigGenericFunctions_get5AlertConfig_allThresholds() {
    dyn_float dfLevelLimits;
    dyn_string dsLevelsAlarms, dsLevelsAlertsClass;
    cpcConfigGenericFunctions_get5AlertConfig("0.25","1","1","1",true, true, dfLevelLimits,dsLevelsAlarms,dsLevelsAlertsClass);
    assertEqual("0.25 | 1 | 1 | 1",dfLevelLimits);
    assertEqual("LL | L | Ok | H | HH",dsLevelsAlarms);
    assertEqual("_cpcAnalogLL_Ack_Mail. | _cpcAnalogL_Mail. |  | _cpcAnalogH_Mail. | _cpcAnalogHH_Ack_Mail.",dsLevelsAlertsClass);
}

void test_cpcConfigGenericFunctions_get5AlertConfig_alarmThresholdsOnly() {
    dyn_float dfLevelLimits;
    dyn_string dsLevelsAlarms, dsLevelsAlertsClass;
    cpcConfigGenericFunctions_get5AlertConfig("0.25","","","1",false, false, dfLevelLimits,dsLevelsAlarms,dsLevelsAlertsClass);
    assertEqual("0.25 | 1",dfLevelLimits);
    assertEqual("LL | Ok | HH",dsLevelsAlarms);
    assertEqual("_cpcAnalogLL. |  | _cpcAnalogHH.",dsLevelsAlertsClass);
}

void test_cpcConfigGenericFunctions_get5AlertConfig_noThresholds() {
    dyn_float dfLevelLimits;
    dyn_string dsLevelsAlarms, dsLevelsAlertsClass;
    cpcConfigGenericFunctions_get5AlertConfig("","","","",false, false, dfLevelLimits,dsLevelsAlarms,dsLevelsAlertsClass);
    assertEmpty(dfLevelLimits);
    assertEqual("Ok", dsLevelsAlarms);
    assertEqual("",dsLevelsAlertsClass);
}

void test_cpcConfigGenericFunctions_get5AlertConfig_wrongValue() {
    dyn_float dfLevelLimits;
    dyn_string dsLevelsAlarms, dsLevelsAlertsClass;
    cpcConfigGenericFunctions_get5AlertConfig("a","2.a","3EXP5","",false, false, dfLevelLimits,dsLevelsAlarms,dsLevelsAlertsClass);
    assertEqual("0 | 2 | 3",dfLevelLimits);
    assertEqual("LL | L | Ok | H", dsLevelsAlarms);
    assertEqual("_cpcAnalogLL. | _cpcAnalogL. |  | _cpcAnalogH.", dsLevelsAlertsClass);
}

void test_cpcConfigGenericFunctions_getAlertClass() {
    assertEqual("_cpcAnalog",cpcConfigGenericFunctions_getAlertClass(false, false));
    assertEqual("_cpcAnalogLL_Ack",cpcConfigGenericFunctions_getAlertClass(true, false,"LL"));
    assertEqual("_cpcAnalogH_Ack_Mail",cpcConfigGenericFunctions_getAlertClass(true,true,"H"));
    assertEqual("_cpcAnalogL_Mail",cpcConfigGenericFunctions_getAlertClass(false,true,"L"));
}

void test_cpcConfigGenericFunctions_getConstant() {
    assertEqual(14, cpcConfigGenericFunctions_getConstant("CPC_TestObject", "LENGTH"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstant("CPC_TestObject", "MissedProperty"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstant("CPC_MissedObject", "TESTPROPERTY"));
}

void test_cpcConfigGenericFunctions_getConstantByName() {
    assertEqual(1, cpcConfigGenericFunctions_getConstantByName("UN_ACCESS_RIGHTS_OPERATOR"));
    assertEqual(3, cpcConfigGenericFunctions_getConstantByName("UN_ACCESS_RIGHTS_ADMIN"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstantByName("UN_ACCESS_RIGHTS_wrong"));
}

void test_cpcConfigGenericFunctions_getConstantPrefix() {
    assertEqual("UN_CONFIG_",cpcConfigGenericFunctions_getConstantPrefix(""));
    assertEqual("UN_CONFIG_",cpcConfigGenericFunctions_getConstantPrefix(UN_PLC_DPTYPE));
    assertEqual("UN_CONFIG_",cpcConfigGenericFunctions_getConstantPrefix(S7_PLC_DPTYPE));
    assertEqual("UN_CONFIG_",cpcConfigGenericFunctions_getConstantPrefix("SOFT"));
    assertEqual("UN_CONFIG_",cpcConfigGenericFunctions_getConstantPrefix("CMW"));
}

void test_cpcConfigGenericFunctions_getStringConstantByName() {
    assertEqual(1, cpcConfigGenericFunctions_getStringConstantByName("UN_ACCESS_RIGHTS_OPERATOR"));
    assertEqual(3, cpcConfigGenericFunctions_getStringConstantByName("UN_ACCESS_RIGHTS_ADMIN"));
    assertEqual(CPC_UNDEFINED_CONSTANT_VALUE, cpcConfigGenericFunctions_getStringConstantByName("CPC_ACCESS_RIGHTS_wrong"));
}

void test_cpcConfigGenericFunctions_getConstantDPLength() {
    assertEqual(14, cpcConfigGenericFunctions_getConstantDPLength("CPC_TestObject"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstantDPLength("CPC_MissedObject"));
}

void test_cpcConfigGenericFunctions_getConstantDPAdditionalLength() {
    assertEqual(7, cpcConfigGenericFunctions_getConstantDPAdditionalLength("CPC_TestObject"));
    assertEqual(0, cpcConfigGenericFunctions_getConstantDPAdditionalLength("CPC_MissedObject"));
}

void test_cpcConfigGenericFunctions_getConstantDPEAdress() {
    assertEqual(11, cpcConfigGenericFunctions_getConstantDPEAdress("CPC_TestObject", "POSST"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstantDPEAdress("CPC_MissedObject", "TESTADDRESS"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstantDPEAdress("CPC_TestObject", "MissAddress"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstantDPEAdress("CPC_TestObject", ""));
}

void test_cpcConfigGenericFunctions_getConstantDPEProperty() {
    assertEqual(21, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "" , false , "LENGTH"));
    assertEqual(24, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "" , true , "LENGTH"));
    assertEqual(11, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "ADDRESS" , true , "POSST"));
    assertEqual(15, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "" , false , "MASKEVENT"));
    assertEqual(18, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "" , true , "MASKEVENT"));
    assertEqual(1, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "" , false , "UNIT"));
    assertEqual(1, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "" , true , "UNIT"));
    assertEqual(-1, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "" , true , ""));
    assertEqual(-1, cpcConfigGenericFunctions_getConstantDPEProperty("CPC_TestObject", "Missing" , true , "Missing"));
}

void test_cpcConfigGenericFunctions_getDeviceParamNames() {
    dyn_string dsResult, dsParams = makeDynString ("PFSPosOn", "PEnRstart", "PRstartFS", "PHLDrive");
    dsResult = cpcConfigGenericFunctions_getDeviceParamNames("CPC_TestObject");
    assert(dynlen(dsResult) == dynlen(dsParams, "cpcConfigGenericFunctions_getDeviceParamNames: Wrong number of parameters returned."));
    if (dynlen(dsResult) == dynlen(dsParams)) {
        for (int i = 1; i <= dynlen(dsResult); i++) {
            assertTrue(dynContains(dsParams, dsResult[i]));
        }
    }
    assertEqual("",cpcConfigGenericFunctions_getDeviceParamNames("CPC_MissedObject"));
}

void test_cpcConfigGenericFunctions_getDPEConfig() {
    mapping addresses;
    addresses = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    dyn_string keys = mappingKeys(addresses);
    for (int i =1; i<= dynlen(keys);i++) {
        dyn_string keys2 = mappingKeys(addresses[keys[i]]);
        assertTrue(mappingHasKey(addresses[keys[i]], "dataType"));
        assertTrue(dynlen(keys2)>=10);
    }
}

void test_cpcConfigGenericFunctions_getRange() {
    string rangeMax, rangeMin;
    dyn_string dsConfigs = makeEmptyDynString(23);
    dsConfigs[19] = "16";
    dsConfigs[20] = "17";

    cpcConfigGenericFunctions_getRange(dsConfigs, "CPC_TestObject", "PosSt", rangeMax, rangeMin, false);
    assertEqual("", rangeMax);
    assertEqual("", rangeMin);

    dsConfigs[21] = "16";
    dsConfigs[22] = "17";

    cpcConfigGenericFunctions_getRange(dsConfigs, "CPC_TestObject", "PosSt", rangeMax, rangeMin, false);
    assertEqual("16", rangeMax);
    assertEqual("17", rangeMin);
    
    dsConfigs[21] = "56.9";
    dsConfigs[22] = "-10";
    
    cpcConfigGenericFunctions_getRange(dsConfigs, "CPC_TestObject", "PosSt", rangeMax, rangeMin, false);
    assertEqual("56.9", rangeMax);
    assertEqual("-10", rangeMin);

}

void test_cpcConfigGenericFunctions_getShortTypeName() {
    assertEqual(cpcConfigGenericFunctions_getShortTypeName("CPC_Test"), "Test");
    assertEqual(cpcConfigGenericFunctions_getShortTypeName("Test"), "Test");
}

void test_cpcConfigGenericFunctions_check5RangesAlert() {
    dyn_string exceptionInfo;
    dyn_string dsConfigs;
    dyn_int diLimits;
    int alarmActivePos = 1;

    dsConfigs = makeEmptyDynString(9);
    dsConfigs[9] = "missbool";
    diLimits = makeDynInt(1, 2, 3);
    cpcConfigGenericFunctions_check5RangesAlert(dsConfigs, alarmActivePos, diLimits, exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkBool: the value '" + dsConfigs[9] +"' is not correct for the ALARM_ACTIVE", exceptionInfo);

    dsConfigs = makeEmptyDynString(9);
    dsConfigs[9] = "false";
    cpcConfigGenericFunctions_check5RangesAlert(dsConfigs, alarmActivePos, diLimits, exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_check5RangesAlert: wrong amount of alert limits: " + dynlen(diLimits) + " instead of 4.", exceptionInfo);

    dsConfigs = makeEmptyDynString(9);
    dsConfigs[9] = "Missed_Real";
    diLimits = makeDynInt(2, 3, 4, 5);
    cpcConfigGenericFunctions_check5RangesAlert(dsConfigs, alarmActivePos, diLimits, exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkBool: the value 'missed_real' is not correct for the ALARM_ACTIVE", exceptionInfo);

    dsConfigs = makeEmptyDynString(11);
    dsConfigs[9] = "false";
    dsConfigs[10] = "4.5";
    dsConfigs[11] = "4.5";
    cpcConfigGenericFunctions_check5RangesAlert(dsConfigs, alarmActivePos, diLimits, exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_check5RangesAlert: limits are not consistent ([limit#2] 4.5 >= 4.5 [limit#1])", exceptionInfo);

    dsConfigs = makeEmptyDynString(13);
    dsConfigs[9] = "false";
    dsConfigs[10] = "4.5";
    dsConfigs[11] = "missedFloat";
    dsConfigs[12] = "6.5";
    dsConfigs[13] = "7.5";
    cpcConfigGenericFunctions_check5RangesAlert(dsConfigs, alarmActivePos, diLimits, exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_check5RangesAlert: wrong limit #3: missedFloat", exceptionInfo);

    dsConfigs = makeEmptyDynString(13);
    dsConfigs[9] = "false";
    dsConfigs[10] = "4.5";
    dsConfigs[11] = "5.5";
    dsConfigs[12] = "6.5";
    dsConfigs[13] = "7.5";
    cpcConfigGenericFunctions_check5RangesAlert(dsConfigs, alarmActivePos, diLimits, exceptionInfo);
    assertEmpty(exceptionInfo);
}

//NOT OK
void test_cpcConfigGenericFunctions_checkAddressCMW() {
    dyn_string exceptionInfo;
    string address;

    address = "";
    cpcConfigGenericFunctions_checkAddressCMW(address, exceptionInfo);
    //assertEmpty(exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkAddressCMW: address '' does not follow format TYPE|TRANSFORMATION_TYPE|POLL_GROUP|ADDRESS_CONFIG|FILTER_NAME|FILTER_VALUE|LLC", exceptionInfo);

    address = "MissedAddress";
    cpcConfigGenericFunctions_checkAddressCMW(address,exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkAddressCMW: address 'MissedAddress' does not follow format TYPE|TRANSFORMATION_TYPE|POLL_GROUP|ADDRESS_CONFIG|FILTER_NAME|FILTER_VALUE|LLC", exceptionInfo);

    address = "8|2|2|1|1|1|1";
    cpcConfigGenericFunctions_checkAddressCMW(address,exceptionInfo);
    assertErrorAndClean("ERROR", "unConfigCMW_checkType: type (spont, poll, SQ) 8", exceptionInfo);

    address = "2|2|2|1|1|1|1";
    cpcConfigGenericFunctions_checkAddressCMW(address,exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkAddressCMW: wrong CMW address  '1'", exceptionInfo);

    address = "2|2|2|1$2|1|1|1";
    cpcConfigGenericFunctions_checkAddressCMW(address,exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkAddressCMW: wrong CMW address  '1$2'", exceptionInfo);

    address = "2|2|2|1$2$3|1|1|1";
    cpcConfigGenericFunctions_checkAddressCMW(address,exceptionInfo);
    assertEmpty(exceptionInfo);

    address = "2|2|2|1$2$3$4|1|1|1";
    cpcConfigGenericFunctions_checkAddressCMW(address,exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkAddressCMW: wrong CMW address  '1$2$3$4'", exceptionInfo);
}

void test_cpcConfigGenericFunctions_checkBool() {
    bool bRes;
    dyn_string exceptionInfo;
    dyn_string trueCases = makeDynString("true", "1", "yes", "y", "True", "TRUE", "YES", "Y");
    for (int i = 1; i <= dynlen(trueCases); i++) {
        cpcConfigGenericFunctions_checkBool("test", trueCases[i], bRes, exceptionInfo);
        assertTrue(bRes);
        assertEmpty(exceptionInfo);
    }
    dyn_string falseCases = makeDynString("false", "0", "no", "n", "False", "FALSE", "NO", "N");
    for (int i = 1; i <= dynlen(falseCases); i++) {
        cpcConfigGenericFunctions_checkBool("test", falseCases[i], bRes, exceptionInfo);
        assertFalse(bRes);
        assertEmpty(exceptionInfo);
    }
    cpcConfigGenericFunctions_checkBool("test", "ok", bRes, exceptionInfo);
    assertFalse(bRes);
    assertError("ERROR", "cpcConfigGenericFunctions_checkBool: the value 'ok' is not correct for the test", exceptionInfo);
}

void test_cpcConfigGenericFunctions_checkConfigLength() {
    dyn_string dsConfigs;
    bool bHasArchive;
    assertFalse(cpcConfigGenericFunctions_checkConfigLength("CPC_TestObject", dsConfigs, bHasArchive));
    dsConfigs = makeEmptyDynString(32); 
    assertTrue(cpcConfigGenericFunctions_checkConfigLength("CPC_TestObject", dsConfigs, bHasArchive));
    assertTrue(bHasArchive);
    dsConfigs = makeEmptyDynString(29);
    assertTrue(cpcConfigGenericFunctions_checkConfigLength("CPC_TestObject", dsConfigs, bHasArchive));
    assertFalse(bHasArchive);
}

void test_cpcConfigGenericFunctions_checkDeviceAddress_UN() {
    dyn_string exceptionInfo;
    dyn_string dsConfigs;
    mapping addrProps;
    string deviceType = "CPC_TestObject";
    string addressName = "POSST";
    dsConfigs = makeEmptyDynString(19);
    dsConfigs[19] = "MissedAddress";
    cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, "", deviceType, addressName, addrProps, false, exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkDeviceAddress: unknown front end ", exceptionInfo);
    cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, "_UnPlc", deviceType, addressName, addrProps, false, exceptionInfo);
    assertErrorAndClean("ERROR", "unConfigGenericFunctions_checkUnsigned: bad data.", exceptionInfo);
    dsConfigs = makeEmptyDynString(19);
    dsConfigs[19] = "1";
    cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, "_UnPlc", "CPC_TestObject", "1097", addrProps, false, exceptionInfo);
    assertEmpty(exceptionInfo);
    
}

void test_cpcConfigGenericFunctions_checkDeviceAddress_S7() {
    dyn_string exceptionInfo;
    dyn_string dsConfigs;
    mapping addrProps;
    string deviceType = "CPC_TestObject";
    string addressName = "POSST";
    addrProps["dataType"] = 1;
    dsConfigs = makeEmptyDynString (19);
    cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, "S7_PLC", deviceType, addressName, addrProps, false, exceptionInfo);
    assertErrorAndClean("ERROR", "unConfigGenericFunctions_checkAddressS7:POSST bad data.", exceptionInfo);
    dsConfigs = makeEmptyDynString(19);
    dsConfigs[19] = "DB10.DBW10";
    cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, "S7_PLC", deviceType, "DB10.DBW10", addrProps, false, exceptionInfo);
    assertEmpty(exceptionInfo);
}

void test_cpcConfigGenericFunctions_checkDeviceAddress_CMW() {
    dyn_string exceptionInfo;
    dyn_string dsConfigs;
    mapping addrProps;
    string deviceType = "CPC_TestObject";
    string addressValue = "2|2|2|1$2$3|1|1|1";
    dsConfigs = makeEmptyDynString(19);
    cpcConfigGenericFunctions_checkDeviceAddress(dsConfigs, "CMW", deviceType, addressValue, addrProps, false, exceptionInfo);
    assertEmpty(exceptionInfo);
}

void test_cpcConfigGenericFunctions_checkFormat() {
    dyn_string exceptionInfo;
    dyn_string okSamples = makeDynString("###", "#.###", "EXP", "4EXP", "2d", "");
    for (int i = 1; i <= dynlen(okSamples); i++) {
        unConfigGenericFunctions_checkFormat(okSamples[i], exceptionInfo);
        assertEmpty(exceptionInfo, "check fails on " + okSamples[i]);
    }
    dyn_string badSamples = makeDynString("test", "##2", "##d");
    for (int i = 1; i <= dynlen(badSamples); i++) {
        unConfigGenericFunctions_checkFormat(badSamples[i], exceptionInfo);
        assertError("ERROR", "unConfigGenericFunctions_checkFormat: " + badSamples[i] + " bad format", exceptionInfo);
    }
    // TODO: improve unConfigGenericFunctions_checkFormat function
    badSamples = makeDynString("#EXP");
    for (int i = 1; i <= dynlen(badSamples); i++) {
        unConfigGenericFunctions_checkFormat(badSamples[i], exceptionInfo);
        assertError("ERROR", "unConfigGenericFunctions_checkInt: bad data.", exceptionInfo);
    }
}

void test_cpcConfigGenericFunctions_checkMapping() {
    dyn_string exceptionInfo;

    cpcConfigGenericFunctions_checkMapping(makeDynString(), "-", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkMapping(makeDynString(), "", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkMapping(makeDynString(), "1=a", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkMapping(makeDynString(), "1=a,2=b", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkMapping(makeDynString(), "1=", exceptionInfo);
    assertError("ERROR", "cpcConfigGenericFunctions_checkMapping: bad data.", exceptionInfo);
    cpcConfigGenericFunctions_checkMapping(makeDynString(), "1=a,2", exceptionInfo);
    assertError("ERROR", "cpcConfigGenericFunctions_checkMapping: bad data.", exceptionInfo);
    cpcConfigGenericFunctions_checkMapping(makeDynString(), "z=a", exceptionInfo);
    assertError("ERROR", "cpcConfigGenericFunctions_checkMapping: bad data.", exceptionInfo);
}

void test_cpcConfigGenericFunctions_checkParameters() {
    dyn_string exceptionInfo;
    cpcConfigGenericFunctions_checkParameters("", "", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkParameters("", "a=b,c=d", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkParameters("", "a=", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkParameters("", "a=b=c", exceptionInfo);
    assertError("ERROR", "cpcConfigGenericFunctions_checkParameters: bad data.", exceptionInfo);
    cpcConfigGenericFunctions_checkParameters("", "a=b,a=c", exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_checkParameters: duplicate in parameters", exceptionInfo);
    cpcConfigGenericFunctions_checkParameters("CPC_TestObject", "PFSPosOn=FALSE,PEnRstart=FALSE,PRstartFS=FALSE,PHLDrive=FALSE", exceptionInfo);
    assertEmpty(exceptionInfo);
    cpcConfigGenericFunctions_checkParameters("CPC_TestObject", "PEnRstart=FALSE,PRstartFS=FALSE,PHLDrive=FALSE", exceptionInfo);
    assertError("ERROR", "cpcConfigGenericFunctions_checkParameters: device should contain 'PFSPosOn' parameter.", exceptionInfo);
}

void test_cpcConfigGenericFunctions_setRange() {
    dyn_string dsConfigs, exceptionInfo,dsDpParameters;
    mapping keys, regProperties;
    float rangeMin, rangeMax, rangeMin2, rangeMax2;
    keys = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProperties =  keys["PosSt"];
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";

    dsConfigs = makeEmptyDynString(22);
    dsConfigs[8] = TEST_DP_NAME;
    cpcConfigGenericFunctions_setRange(dsConfigs, "CPC_TestObject", "", regProperties, "", false, exceptionInfo);
    assertEmpty(exceptionInfo); 

    dsConfigs = makeEmptyDynString(22);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[21] = "18";
    dsConfigs[22] = "17";
    cpcConfigGenericFunctions_setRange(dsConfigs, "CPC_TestObject", "", regProperties, "", false, exceptionInfo);
    dpGet(dpe + ":_pv_range.._min", rangeMin);
    dpGet(dpe + ":_pv_range.._max", rangeMax);
    assertEqual("17", rangeMin);
    assertEqual("18", rangeMax);
    
    dsConfigs = makeEmptyDynString(22);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[21] = "14";
    dsConfigs[22] = "14";
    cpcConfigGenericFunctions_setRange(dsConfigs, "CPC_TestObject", "", regProperties, "", false, exceptionInfo);
    dpGet(dpe + ":_pv_range.._min", rangeMin2);
    dpGet(dpe + ":_pv_range.._max", rangeMax2);
    assertEqual("17", rangeMin);
    assertEqual("18", rangeMax);

    dsConfigs = makeEmptyDynString(22);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[21] = "13";
    dsConfigs[22] = "14";
    cpcConfigGenericFunctions_setRange(dsConfigs, "CPC_TestObject", "", regProperties, "", false, exceptionInfo);
    assertErrorAndClean("ERROR", "_fwPvRange_checkInputConfiguration(): Minimum value must be less than maximum value for my_test_dp.ProcessInput.PosSt", exceptionInfo);
}

void test_cpcConfigGenericFunctions_setUnit() {
    dyn_string dsConfigs, exceptionInfo,dsDpParameters;
    mapping keys, regProperties;
    keys = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProperties =  keys["PosSt"];
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";

    dsConfigs = makeEmptyDynString(19);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[19] = "Test_Unit";
    cpcConfigGenericFunctions_setUnit(dsConfigs, "CPC_TestObject", "", regProperties, "", false, exceptionInfo);
    assertEqual("Test_Unit", dpGetUnit(dpe));
}

void test_cpcConfigGenericFunctions_setFormat() {
    dyn_string dsConfigs, exceptionInfo,dsDpParameters;
    mapping keys, regProperties;
    keys = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProperties =  keys["PosSt"];
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";

    dsConfigs = makeEmptyDynString(20);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[20] = "##.###";
    cpcConfigGenericFunctions_setFormat(dsConfigs, "CPC_TestObject", "", regProperties, "", false, exceptionInfo); 
    assertEqual( "%6.3f", dpGetFormat(dpe));
}

void test_cpcConfigGenericFunctions_setMaskEvent() {
    dyn_string exceptionInfo;
    bool iMask;
    dpGet(TEST_DP_NAME +".eventMask", iMask);
    assertFalse(iMask);
    cpcConfigGenericFunctions_setMaskEvent("CPC_TestObject" , TEST_DP_NAME, true, exceptionInfo);
    dpGet(TEST_DP_NAME +".eventMask", iMask);
    assertTrue(iMask);
}

void test_cpcConfigGenericFunctions_setAddress_UN() {
    dyn_string exceptionInfo, dsConfigs, dsDpParameters;
    mapping Config, regProperties;
    string drv_ident, datatype, address, value;
    Config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProperties = Config["PosSt"];

    dsConfigs = makeEmptyDynString(22);
    cpcConfigGenericFunctions_setAddress(dsConfigs, "", regProperties, 1, "", exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_setAddress: unknown front end ", exceptionInfo);

    dsConfigs = makeEmptyDynString(22);
    dsConfigs[1] = "1";
    dsConfigs[2] = "MODBUS";
    dsConfigs[3] = "1";
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[9] = "1000";

    //dsConfigs[19] = "value:50";
    cpcConfigGenericFunctions_setAddress(dsConfigs, "PosSt", regProperties, "value:50", UN_PLC_DPTYPE, exceptionInfo);
    dpGet(TEST_DP_NAME + ".ProcessInput.PosSt" , value);
    assertEqual("50", value);

    dsConfigs[19] = "3";
    cpcConfigGenericFunctions_setAddress(dsConfigs, "PosSt", regProperties, "3", UN_PLC_DPTYPE, exceptionInfo);
    dpGet(TEST_DP_NAME + ".ProcessInput.PosSt:_address.._drv_ident" , drv_ident);
    assertEqual("MODBUS", drv_ident);
    dpGet(TEST_DP_NAME + ".ProcessInput.PosSt:_address.._datatype" , datatype);
    assertEqual(PVSS_MODBUS_FLOAT, datatype);
    dpGet(TEST_DP_NAME + ".ProcessInput.PosSt:_address.._reference" , address);
    assertEqual("U.1.1.3", address);
}

void test_cpcConfigGenericFunctions_setAddress_S7() {
    dyn_string exceptionInfo, dsConfigs, dsDpParameters;
    mapping Config, regProperties;
    string drv_ident, datatype, address;
    Config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProperties = Config["PosSt"];
    dsConfigs = makeEmptyDynString(24);
    dsConfigs[1] = "1";
    dsConfigs[2] = "S7_PLC";
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[10] = "PLC_NAME";
    cpcConfigGenericFunctions_setAddress(dsConfigs, "PosSt", regProperties, "DB10.DBW78", S7_PLC_DPTYPE, exceptionInfo);
    dpGet(TEST_DP_NAME + ".ProcessInput.PosSt:_address.._drv_ident" , drv_ident);
    assertEqual("S7", drv_ident);
    dpGet(TEST_DP_NAME + ".ProcessInput.PosSt:_address.._datatype" , datatype);
    assertEqual(PVSS_S7_FLOAT, datatype);
    dpGet(TEST_DP_NAME + ".ProcessInput.PosSt:_address.._reference" , address);
    assertEqual( address, "PLC_NAME.DB10.DBW78");
}

void test_cpcConfigGenericFunctions_setSmoothing() {
    dyn_string dsConfigs, exceptionInfo;
    mapping Config, regProperties;
    int configExists;
    float smoothValue;

    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    Config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProperties = Config["PosSt"];

    dsConfigs = makeEmptyDynString(26);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[23] = "0.5";
    dsConfigs[24] = "1";

    cpcConfigGenericFunctions_setSmoothing(dsConfigs, "CPC_TestObject", "PosSt", regProperties, "_UnPlc", false, exceptionInfo);
    dpGet(dpe+":_smooth.._type", configExists);
    dpGet(dpe+":_smooth.._std_tol", smoothValue);
    assertEqual("48",configExists);
    assertEqual("0.5", smoothValue);

    dsConfigs[24] = "2";
    cpcConfigGenericFunctions_setSmoothing(dsConfigs, "CPC_TestObject", "PosSt", regProperties, "_UnPlc", false, exceptionInfo);
    dpGet(dpe+":_smooth.._type", configExists);
    dpGet(dpe+":_smooth.._std_tol", smoothValue);
    assertEqual("48",configExists);
    assertEqual("0.5", smoothValue);

    dsConfigs[24] = "3";
    cpcConfigGenericFunctions_setSmoothing(dsConfigs, "CPC_TestObject", "PosSt", regProperties, "_UnPlc", false, exceptionInfo);
    dpGet(dpe+":_smooth.._type", configExists);
    dpGet(dpe+":_smooth.._std_tol", smoothValue);
    assertEqual("48",configExists);
    assertEqual("0", smoothValue);

    dsConfigs = makeEmptyDynString(26);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[25] = "-2";
    dsConfigs[26] = "1";
    cpcConfigGenericFunctions_setSmoothing(dsConfigs, "CPC_TestObject", "PosSt", regProperties, "_UnPlc", false, exceptionInfo);
    dpGet(dpe+":_smooth.._type", configExists);
    assertEqual("0", configExists);
}

void test_cpcConfigGenericFunctions_setDPEAlias() {
    dyn_string exceptionInfo;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    cpcConfigGenericFunctions_setDPEAlias("My_Alias", dpe, exceptionInfo);
    assertEqual("My_Alias.PosSt", dpGetAlias(TEST_DP_NAME + ".ProcessInput.PosSt"));

    dpe = TEST_DP_NAME;
    cpcConfigGenericFunctions_setDPEAlias("My_Alias", dpe, exceptionInfo);
    assertErrorAndClean( "ERROR", "cpcConfigGenericFunctions_setDPEAlias: can't set alias for my_test_dp", exceptionInfo);
}

void test_cpcConfigGenericFunctions_setArchive() {
    dyn_string dsConfigs, exceptionInfo;
    mapping deviceAddresses, regProperties;
    bool bArchiveActive;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt", sArchiveActive;
    float fArchiveTimeFilter;
    string string1, string2, string3, string4;

    deviceAddresses = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProperties = deviceAddresses["PosSt"];

    dsConfigs = makeEmptyDynString(29);
    dsConfigs[5] = "ValueArchive_0000";
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[25] = "VR,0.5,N";
    dsConfigs[26] = "2";
    cpcConfigGenericFunctions_setArchive(dsConfigs, "CPC_TestObject", "PosSt", regProperties, "_UnPlc", true, exceptionInfo);
    unExportDevice_getArchiveParameters(TEST_DP_NAME, "CPC_TestObject", sArchiveActive, fArchiveTimeFilter);
    assertEqual("VR,0.5,N", sArchiveActive);

    cpcConfigGenericFunctions_setArchive(dsConfigs, "CPC_TestObject", "AlSt", regProperties, "_UnPlc", true, exceptionInfo);
    unExportDevice_getArchiveParameters(TEST_DP_NAME, "CPC_TestObject", sArchiveActive, fArchiveTimeFilter);
    assertEqual("VR,0.5,N", sArchiveActive);


}

void test_cpcConfigGenericFunctions_setCMWAddress() {
    dyn_string dsResult, exceptionInfo;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";

    cpcConfigGenericFunctions_setCMWAddress(dpe, "1|2|3|4$5$6|7|8|9", "1", exceptionInfo);
    unConfigCMW_getAddressConfig(dpe, dsResult);    
    assertEqual("0 | 2 |  | 4 | 5: | 6 | 7 | 8 | TRUE", dsResult);
    //cpcConfigGenericFunctions_setCMWAddress(dpe, "", "1", exceptionInfo);
    //dynClear(dsResult);
    //unConfigCMW_getAddressConfig(dpe, dsResult);    
    //assertEqual("0 | 0 |  |  |  |  |  |  | ", dsResult);
}

void test_cpcConfigGenericFunctions_setEvArchive() {
    dyn_string dsConfigs, exceptionInfo;
    bool configExists, archiveClass, isActive;
    int smoothProcedure, archiveType;
    float deadband, timeInterval;
    dsConfigs = makeEmptyDynString(15);
    dsConfigs[6] = "ValueArchive_0000";
    dsConfigs[8] = TEST_DP_NAME;
    cpcConfigGenericFunctions_setEvArchive(dsConfigs, ".ProcessInput.AlSt", exceptionInfo);
    fwArchive_get(TEST_DP_NAME + ".ProcessInput.AlSt", configExists, archiveClass, archiveType, smoothProcedure, deadband, timeInterval, isActive, exceptionInfo);
    assertTrue(configExists);
    assertTrue(isActive);
    assertEmpty(exceptionInfo);
}

void test_cpcConfigGenericFunctions_setDigitalAlert() {
    dyn_string dsdescription, dsAlertTexts, dsAlertClasses, exceptionInfo;

    string dpe = TEST_DP_NAME + ".ProcessInput.AlSt";
    bool bActive;
    dsdescription = makeDynString("AlarmDescription");
    dsAlertTexts = makeDynString("Ok", "Bad");
    dsAlertClasses = makeDynString("", "_cpcAnalog_Ack");

    cpcConfigGenericFunctions_setDigitalAlert(dpe, dsdescription, dsAlertTexts, dsAlertClasses, true, true, true, exceptionInfo);
    dpGet(dpe + ":_alert_hdl.._active", bActive);
    assertTrue(bActive);
    cpcConfigGenericFunctions_setDigitalAlert(dpe, dsdescription, dsAlertTexts, dsAlertClasses, true, false, true, exceptionInfo);
    dpGet(dpe + ":_alert_hdl.._active", bActive);
    assertFalse(bActive);
}

void test_cpcConfigGenericFunctions_setAlert_CMW() {
    dyn_string dsConfigs, dsResult, exceptionInfo;
    mapping config, alertProps;
    config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    alertProps = config["AlSt"];
    dsConfigs = makeEmptyDynString(19);
    dsConfigs[1] = "1";
    dsConfigs[8] = TEST_DP_NAME; 
    dsConfigs[17] = "1|2|3|4$5$6|7|8|9";
    
    cpcConfigGenericFunctions_setAlert(dsConfigs, "CPC_TestObject", "Test_Alert", alertProps, "missed_protocol", "AlSt", exceptionInfo);
    assertErrorAndClean("ERROR", "cpcConfigGenericFunctions_setAlert: unknown front end missed_protocol", exceptionInfo);
    
    cpcConfigGenericFunctions_setAlert(dsConfigs, "CPC_TestObject", "Test_Alert", alertProps, "CMW", "AlSt", exceptionInfo);
    unConfigCMW_getAddressConfig("my_test_dp.ProcessInput.AlSt", dsResult);
    assertEqual("0 | 2 |  | 4 | 5: | 6 | 7 | 8 | TRUE", dsResult);
}

void test_cpcConfigGenericFunctions_setAlert_S7() {
    dyn_string dsConfigs, dsResult, exceptionInfo;
    mapping config, alertProps;
    string drv_ident, datatype, address;
    config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    alertProps = config["AlSt"];
    dsConfigs = makeEmptyDynString(30);
    dsConfigs[1] = "1";
    dsConfigs[2] = "S7_PLC";
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[10] = "PLC_NAME";
    dsConfigs[17] = "DB10.DBW78";
    cpcConfigGenericFunctions_setAlert(dsConfigs, "CPC_TestObject", "Test_Alert", alertProps, "S7_PLC", "AlSt", exceptionInfo);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._drv_ident" , drv_ident);
    assertEqual("S7", drv_ident);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._datatype" , datatype);
    assertEqual(PVSS_S7_BOOL, datatype);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._reference" , address);
    assertEqual( address, "PLC_NAME.DB10.DBX78.6");
}

void test_cpcConfigGenericFunctions_setAlert_UN() {
    dyn_string dsConfigs, dsResult, exceptionInfo;
    mapping config, alertProps;
    string drv_ident, datatype, address;
    config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    alertProps = config["AlSt"];
    dsConfigs = makeEmptyDynString(22);
    dsConfigs[1] = "1";
    dsConfigs[2] = "MODBUS";
    dsConfigs[3] = "1";
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[9] = "1000";
    dsConfigs[17] = "3";
    cpcConfigGenericFunctions_setAlert(dsConfigs, "CPC_TestObject", "Test_Alert", alertProps, UN_PLC_DPTYPE, "AlSt", exceptionInfo);
    DebugN(exceptionInfo);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._drv_ident" , drv_ident);
    assertEqual("MODBUS", drv_ident);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._datatype" , datatype);
    assertEqual(PVSS_MODBUS_BOOL, datatype);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._reference" , address);
    assertEqual("U.1.1.3", address);
}

void test_cpcConfigGenericFunctions_setAlert_OPCUA() {
    dpCreate("_OPCUA_1", "_OPCUA");
    dpCreate("_OPCUA_Sub_1", "_OPCUASubscription");
    dyn_string dsConfigs, dsResult, exceptionInfo;
    mapping config, alertProps;
    string address;
    config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    alertProps = config["AlSt"];
    dsConfigs = makeEmptyDynString(27);
    dsConfigs[1] = "1";
    dsConfigs[8] = TEST_DP_NAME; 
    dsConfigs[27] = "1|2|3|4";
    cpcConfigGenericFunctions_setAlert(dsConfigs, "CPC_TestObject", "Test_Alert", alertProps, "OPCUA", "StsReg01", exceptionInfo);
    
    string reference, mode, drv_ident, datatype;
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._reference", reference);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._drv_ident", drv_ident);
    dpGet(TEST_DP_NAME + ".ProcessInput.AlSt:_address.._datatype", datatype);
    
    assertEqual("OPCUA_1$OPCUA_Sub_1$1$1$ns=1;2=3", reference);
    assertEqual("OPCUA", drv_ident);
    assertEqual(OPCUA_DEFAULT, datatype);
    dpDelete("_OPCUA_Sub_1", "_OPCUASubscription");
    dpDelete("_OPCUA_1", "_OPCUA");
}

void test_cpcConfigGenericFunctions_setMetainfo() {
    dyn_string dsConfigs, exceptionInfo, dsMetaInfo, dsConfig;

    dsConfigs = makeEmptyDynString(50);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[35] = "MasterName";
    dsConfigs[36] = "ParentsName";
    dsConfigs[37] = "ChildrenName";
    dsConfigs[38] = "TypeName";
    dsConfigs[39] = "SecondAliasName";
    cpcConfigGenericFunctions_setMetainfo("CPC_TestObject", dsConfigs, false, exceptionInfo);
    cpcExportGenericFunctions_getMetainfo(TEST_DP_NAME, dsMetaInfo, exceptionInfo);
    assertEqual( "MasterName | ParentsName | ChildrenName | TypeName | SecondAliasName", dsMetaInfo);
}

void test_cpcConfigGenericFunctions_setMapping() {
    dyn_string dsConfigs, exceptionInfo, dsConfig;
    mapping config, regProps;
    string sDpe = TEST_DP_NAME + ".statusInformation.configuration.deviceConfiguration";
    config = cpcConfigGenericFunctions_getDPEConfig("CPC_TestObject");
    regProps = config["PosSt"];

    dsConfigs = makeEmptyDynString(50);
    dsConfigs[8] = TEST_DP_NAME;
    dsConfigs[17] = "Test_Element_Description";
    cpcConfigGenericFunctions_setMapping(dsConfigs, "CPC_TestObject", "PosSt", regProps, "_UnPlc", false, exceptionInfo);
    dpGet(sDpe, dsConfig);
    assertEqual(dsConfig, "DESC_PosSt:Test_Element_Description");
    dynClear(dsConfig);

    dsConfigs[17] = "-";
    cpcConfigGenericFunctions_setMapping(dsConfigs, "CPC_TestObject", "PosSt", regProps, "_UnPlc", false, exceptionInfo);
    dpGet(sDpe, dsConfig);
    assertEqual(dsConfig, "DESC_PosSt:");
}

void test_cpcConfigGenericFunctions_setParameters() {
    dyn_string dsConfigs, exceptionInfo, dsParameters;
    string sDpe = TEST_DP_NAME + ".statusInformation.configuration.deviceConfiguration";

    dsConfigs = makeEmptyDynString(50);
    dsConfigs[8] = TEST_DP_NAME;
    cpcConfigGenericFunctions_setParameters("CPC_TestObject", dsConfigs, "Test_Parameter1=1,Test_Parameter2,Test_Parameter2=2", exceptionInfo);
    dpGet(sDpe, dsParameters);
    assertEqual(dsParameters,"PROP_Test_Parameter2:2 | PROP_Test_Parameter1:1");
}
