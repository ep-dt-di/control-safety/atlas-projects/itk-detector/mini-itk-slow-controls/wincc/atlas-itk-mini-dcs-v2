#uses "fwUnitTestComponentAsserts.ctl"
#uses "CPC_declarations.ctl"
#uses "S7Constant_declarations.ctl"
#uses "fwConfigs/fwPeriphAddress.ctl"
#uses "cpcConfigGenericFunctions.ctl"
#uses "cpcDpFunc.ctl"
#uses "test/unCPC6/cpcTestObjectConfig.ctl"
string TEST_DP_NAME = "my_test_dp";

dyn_string makeEmptyDynString(int amount) {
    dyn_string result;
    for (int i = 1; i <= amount; i++) {
        dynAppend(result, "");
    }
    return result;
}

void test_setup() {
    dpCreate(TEST_DP_NAME, "CPC_TestObject");
}

void test_teardown() {
    dpDelete(TEST_DP_NAME, "CPC_TestObject");
}


void test_cpcDigitalParameterDPFunc_getStsReg01Value() {
    string dpe = TEST_DP_NAME + ".ProcessInput.StsReg01";

	dpSetWait(dpe,10);
	bit32 test = 16; //Manual On Request. Ignoring first parameter
	assertEqual(1, cpcDigitalParameterDPFunc_getStsReg01Value(dpe,test));
	test = 32; //Manual Off Request. Ignoring first parameter. 
	assertEqual(0, cpcDigitalParameterDPFunc_getStsReg01Value(dpe,test));
	test = 0; //No request, returning dpe (stsReg01)
	assertEqual(10, cpcDigitalParameterDPFunc_getStsReg01Value(dpe ,test));
}

void test_cpcDpFunc_syncAcknowledge() {
	int state;
	dyn_string dsdescription, dsAlertTexts, dsAlertClasses, exceptionInfo;
    string dpe_Al = TEST_DP_NAME + ".ProcessInput.AlSt";
    string dpe_Ack = TEST_DP_NAME + ".ProcessInput.AlUnAck";
    dsdescription = makeDynString("AlarmDescription");
    dsAlertTexts = makeDynString("Ok", "Bad");
    dsAlertClasses = makeDynString("", "_cpcAnalog_Ack");
    //Create Alarm
    cpcConfigGenericFunctions_setDigitalAlert(dpe_Al, dsdescription, dsAlertTexts, dsAlertClasses, true, true, true, exceptionInfo);

    //Check the inintial state of the alarm
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
	assertEqual(0,state);

	//Activate the alarm
    dpSetWait(dpe_Al, 1, dpe_Ack, 1);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(1,state);

    //Missed alarm
    dpSetWait(dpe_Al,0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(3 ,state);

    //not fixed the ack propagation. After ack, still in state 3.
    dpSetWait(dpe_Ack, 0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(3, state);

    //call the dp function manually.
    cpcDpFunc_syncAcknowledge(dpe_Al, false);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(0, state);

    //Activate the alarm
    dpSetWait(dpe_Al, 1, dpe_Ack, 1);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(1,state);

    //ack the alarm doesn't work
    dpSetWait(dpe_Ack, 0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(1, state);

    //call the function manually.
    cpcDpFunc_syncAcknowledge(dpe_Al, false);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(2, state);

    //alarm gone
    dpSetWait(dpe_Al, 0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(0, state);
}

void test_cpcDpFunc_fixAckPropogation() {
    int state;
    dyn_string dsdescription, dsAlertTexts, dsAlertClasses, exceptionInfo;
    string dpe_Al = TEST_DP_NAME + ".ProcessInput.AlSt";
    string dpe_Ack = TEST_DP_NAME + ".ProcessInput.AlUnAck";
    dsdescription = makeDynString("AlarmDescription");
    dsAlertTexts = makeDynString("Ok", "Bad");
    dsAlertClasses = makeDynString("", "_cpcAnalog_Ack");
    //Create Alarm
    cpcConfigGenericFunctions_setDigitalAlert(dpe_Al, dsdescription, dsAlertTexts, dsAlertClasses, true, true, true, exceptionInfo);
    
    //Fix Propagation
    cpcDpFunc_fixAckPropogation (TEST_DP_NAME, ".ProcessInput.AlSt");

    //Activate the alarm
    dpSetWait(dpe_Al, 1, dpe_Ack, 1);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(1,state);

    //Missed alarm
    dpSetWait(dpe_Al,0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(3 ,state);

    //Ack the alarm and automatically go to state 0
    dpSetWait(dpe_Ack, 0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(0, state);

    //Activate the alarm
    dpSetWait(dpe_Al, 1, dpe_Ack, 1);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(1,state);

    //Ack the alarm
    dpSetWait(dpe_Ack, 0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(2, state);

    //remove the alarm and goes to state 0
    dpSetWait(dpe_Al, 0);
    dpGet(dpe_Al + ":_alert_hdl.._act_state",state);
    assertEqual(0, state);
}