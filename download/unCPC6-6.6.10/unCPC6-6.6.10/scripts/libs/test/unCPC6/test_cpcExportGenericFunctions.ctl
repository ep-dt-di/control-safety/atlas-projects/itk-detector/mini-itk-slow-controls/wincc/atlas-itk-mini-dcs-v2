#uses "fwUnitTestComponentAsserts.ctl"
#uses "CPC_declarations.ctl"
#uses "S7Constant_declarations.ctl"
#uses "fwConfigs/fwPeriphAddress.ctl"
#uses "cpcConfigGenericFunctions.ctl"
#uses "test/unCPC6/cpcTestObjectConfig.ctl"
string TEST_DP_NAME = "my_test_dp";

void assertErrorAndClean(string sSeverity, string sMessage, dyn_string& exceptionInfo){
    assertError(sSeverity, sMessage, exceptionInfo);
    dynClear(exceptionInfo);
}

dyn_string makeEmptyDynString(int amount) {
    dyn_string result;
    for (int i = 1; i <= amount; i++) {
        dynAppend(result, "");
    }
    return result;
}

void test_setup() {
    dpCreate(TEST_DP_NAME, "CPC_TestObject");
}

void test_teardown() {
    dpDelete(TEST_DP_NAME, "CPC_TestObject");
}

void test_cpcExportGenericFunctions_getParameters() {

    dyn_string dsParameters, dsDpParameters, exceptionInfo;
    int result;
    dynAppend(dsParameters, "PROP_Param1:Value1");
    dynAppend(dsParameters, "Param2:Value2");
    dynAppend(dsParameters, "PROP_Param3:Value3");
    dynAppend(dsParameters, "PROP_Param4:");
    dpSetWait(TEST_DP_NAME + ".statusInformation.configuration.deviceConfiguration", dsParameters);

    cpcExportGenericFunctions_getParameters(TEST_DP_NAME, dsDpParameters, exceptionInfo );
    assertEqual("Param4=,Param1=Value1,Param3=Value3", dsDpParameters);
}

void test_cpcExportGenericFunctions_getMetainfo() {
    dyn_string dsParameters, dsDpParameters, exceptionInfo;
    int result;
    dynAppend(dsParameters, "MASTER_NAME:MasterName");
    dynAppend(dsParameters, "PARENTS:Parent1");
    dynAppend(dsParameters, "PARENTS:Parent2");
    dynAppend(dsParameters, "CHILDREN:Children");
    dynAppend(dsParameters, "TYPE:Type");
    dynAppend(dsParameters, "SECOND_ALIAS:SecondAlias");
    dpSetWait(TEST_DP_NAME + ".statusInformation.configuration.deviceConfiguration", dsParameters);

    cpcExportGenericFunctions_getMetainfo (TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("MasterName | Parent1,Parent2 | Children | Type | SecondAlias",dsDpParameters);
}

void test_cpcExportGenericFunctions_getUnit() {
    dyn_string unit;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    dpSetUnit(dpe, "EUR");
    cpcExportGenericFunctions_getUnit(TEST_DP_NAME, unit);
    assertEqual("EUR", unit);
}

void test_cpcExportGenericFunctions_getFormat() {
    dyn_string dsDpParameters;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    dpSetFormat(dpe, "%8.2f" );
    cpcExportGenericFunctions_getFormat(TEST_DP_NAME, dsDpParameters);
    assertEqual("#####.##",dsDpParameters);
}


void test_cpcExportGenericFunctions_getSimpleFormat() {
    dyn_string dsDpParameters;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    dpSetFormat(dpe + "", "%8.2f" );
    cpcExportGenericFunctions_getSimpleFormat(TEST_DP_NAME, dsDpParameters);
    assertEqual("#####.##",dsDpParameters);
}

void test_cpcExportGenericFunctions_getRange() {
    dyn_string dsDpParameters, exceptionInfo;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    int result = dpSet(dpe + ":_pv_range.._type", 7, 
    dpe + ":_pv_range.._min", 10, 
    dpe + ":_pv_range.._max", 30, 
    dpe + ":_pv_range.._neg", FALSE, 
    dpe + ":_pv_range.._incl_min", TRUE, 
    dpe + ":_pv_range.._incl_max", TRUE, 
    dpe + ":_pv_range.._ignor_inv", FALSE); 
    cpcExportGenericFunctions_getRange(TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("30.0 | 10.0", dsDpParameters);
}

void test_cpcExportGenericFunctions_getOutRange() {
    dyn_string dsDpParameters, exceptionInfo;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    dpSetWait(  dpe + ":_pv_range.._type", 7, 
                dpe + ":_pv_range.._min", 10, 
                dpe + ":_pv_range.._max", 20, 
                dpe + ":_pv_range.._neg", FALSE, 
                dpe + ":_pv_range.._incl_min", TRUE, 
                dpe + ":_pv_range.._incl_max", TRUE, 
                dpe + ":_pv_range.._ignor_inv", FALSE);
    dpSetWait(  dpe + ":_msg_conv.._type", DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN,
                dpe + ":_msg_conv.1._type", DPDETAIL_CONV_POLY,
                dpe + ":_msg_conv.1._poly_grade", 1,
                dpe + ":_msg_conv.1._poly_a", 0.1,
                dpe + ":_msg_conv.1._poly_b", 0.1);
    cpcExportGenericFunctions_getOutRange(TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("2.1 | 1.1", dsDpParameters);
}

void test_cpcExportGenericFunctions_getDeadband() {
    dyn_string dsDpParameters, exceptionInfo;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    cpcExportGenericFunctions_getDeadband(TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("0 | 0", dsDpParameters);
    dynClear(dsDpParameters);
    dpSetWait(  dpe + ":_smooth.._type", 48 ,
                dpe + ":_smooth.._std_type", DPATTR_VALUE_REL_SMOOTH,
                dpe+":_smooth.._std_tol", 0.5);
    cpcExportGenericFunctions_getDeadband(TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("0.5 | 2", dsDpParameters);
    dynClear(dsDpParameters);
    dpSetWait(  dpe + ":_smooth.._std_type", DPATTR_VALUE_SMOOTH,
                dpe+":_smooth.._std_tol", 0.4);
    cpcExportGenericFunctions_getDeadband(TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("0.4 | 1", dsDpParameters);
    dynClear(dsDpParameters);
    dpSetWait(  dpe + ":_smooth.._std_type", DPATTR_COMPARE_OLD_NEW,
                dpe+":_smooth.._std_tol", 0.3);
    cpcExportGenericFunctions_getDeadband(TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("0 | 3", dsDpParameters);
    dynClear(dsDpParameters);
    dpSetWait(  dpe + ":_smooth.._std_type", 9,
                dpe+":_smooth.._std_tol", 0.2);
    cpcExportGenericFunctions_getDeadband(TEST_DP_NAME, dsDpParameters, exceptionInfo);
    assertEqual("0 | 0", dsDpParameters);
}

void test_cpcExportGenericFunctions_getArchive() {
    dyn_string dsDpParameters, exceptionInfo;
    string dpe = TEST_DP_NAME + ".ProcessInput.PosSt";
    cpcExportGenericFunctions_getArchive(TEST_DP_NAME, dsDpParameters);
    assertEqual("N | ", dsDpParameters);
    dynClear(dsDpParameters);
    dpSetWait(  dpe + ":_archive.._type", 45,
                dpe + ":_archive.._archive", TRUE,
                dpe + ":_archive.1._type", 15,
                dpe + ":_archive.1._class", "dist_1:_ValueArchive_6");
    cpcExportGenericFunctions_getArchive(TEST_DP_NAME, dsDpParameters);
    assertEqual("Y | 0", dsDpParameters);
}

void test_cpcExportGenericFunctions_getMaskEvent() {
    dyn_string dsDpParameters;
    dpSetWait(TEST_DP_NAME + ".eventMask", 32);
    cpcExportGenericFunctions_getMaskEvent(TEST_DP_NAME, dsDpParameters);
    assertEqual("32", dsDpParameters);
}

void test_cpcExportGenericFunctions_getDefaultValue() {
    dyn_string dsDpParameters;
    dpSetWait(TEST_DP_NAME + ".ProcessOutput.MPosR", 50.0);
    DebugN(getLastError());
    cpcExportGenericFunctions_getDefaultValue(TEST_DP_NAME, dsDpParameters, "MPosR");
    assertEqual("50", dsDpParameters);
}

void test_cpcExportGenericFunctions_getPIDName() {
    string description;
    dyn_string dsDpParameters;
    dpSetDescription(TEST_DP_NAME + ".ProcessInput", "Name1;Description1;Name2;Description2;Name3;Description3");
    description = dpGetDescription(TEST_DP_NAME + ".ProcessInput");
    cpcExportGenericFunctions_getPIDName(TEST_DP_NAME, dsDpParameters);
    assertEqual("Description3", dsDpParameters);
}
