/**@file test_unRcpFunctions.ctl  
   
@author: Ivan Prieto Barreiro (EN-ICE-SIC)

@par Creation Date
		 08/05/2017
 */
 
// You must pick up this library:
#uses "fwUnitTestComponentAsserts.ctl"

// and, of course, the library of routines you are wanting to call (ie wanting to test):
#uses "unRcpFunctions.ctl"


//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_setupSuite() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_setup() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_teardown() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_teardownSuite() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getRowStylesForValueComparison() {
  dyn_anytype daValues1, daValues2;
  dyn_int diRowStyleIndex, diRowStyles, diDiffRowIndex;
  
  daValues1 = makeDynAnytype(1, 2);
  daValues2 = makeDynAnytype(1, 1);
  
  _unRecipeFunctions_getRowStylesForValueComparison(daValues1, daValues2, diRowStyleIndex, diRowStyles, diDiffRowIndex);
  assertEqual(dynlen(daValues1), dynlen(diRowStyleIndex), "The number of elements in diRowStyleIndex is wrong");
  assertEqual(0, diRowStyleIndex[1], "The element with index 1 is wrong in diRowStyleIndex");
  assertEqual(1, diRowStyleIndex[2], "The element with index 2 is wrong in diRowStyleIndex");
  
  assertEqual(dynlen(daValues1), dynlen(diRowStyles), "The number of elements in diRowStyles is wrong");
  assertEqual(0, diRowStyles[1], "The element with index 1 is wrong in diRowStyles");
  assertEqual(1, diRowStyles[2], "The element with index 2 is wrong in diRowStyles");
  
  assertEqual(1, dynlen(diDiffRowIndex), "The number of elements in diDiffRowIndex is wrong");
  assertEqual(2, diDiffRowIndex[1], "The element with index 1 is wrong in diDiffRowIndex");
  
}
//-------------------------------------------------------
