/**@file testDeviceLock.ctl  
   
@author: Ivan Prieto Barreiro (EN-ICE-SIC)

@par Creation Date
		 26/09/2014   
 */
 
// You must pick up this library:
#uses "fwUnitTestComponentAsserts.ctl"

// and, of course, the library of routines you are wanting to call (ie wanting to test):
#uses "unRcpFunctions.ctl"
#uses "unRcpDbFunctions.ctl"

string sDpName1 = "test_unRecipeFunctions_lockDevices1";
string sDpName2 = "test_unRecipeFunctions_lockDevices2";

//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_lockDevices_setupSuite() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_lockDevices_setup() {
  int iRetVal;
  
  // Create a dummy datapoint to lock it
  iRetVal = dpCreate(sDpName1, "ExampleDP_Int");
  assertEqual(0, iRetVal);
  iRetVal = dpCreate(sDpName2, "ExampleDP_Int");
  assertEqual(0, iRetVal);
}
//-------------------------------------------------------
/** 
 * Try to lock and unlock a device.
 */
void testUnRecipe_unRecipeFunctions_lockUnlockDevice() {    
  dyn_string dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo;
    
  // TODO: implement mechanism in fwUnitTestHarness to allow skipping tests
  if ( UI_MAN != myManType() )
  {
     DebugTN("Skipping test '" + __FUNCTION__ + "', since we are *NOT* running within a UI");
     return;
  }

  // Try to lock the device sDpName1
  bool bResult = _unRecipeFunctions_lockDevices(makeDynString(sDpName1+"."), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo);
  assertEqual(TRUE, bResult);
  assertEqual(makeDynString(), dsLockedDeviceDp);
  assertEqual(makeDynString(), dsLockedDeviceAlias);
  assertEqual(makeDynString(), exceptionInfo);
  
  // Try to unlock the device sDpName1
  bResult = _unRecipeFunctions_unlockDevices(makeDynString(sDpName1+"."), exceptionInfo);
  assertEqual(TRUE, bResult);
  assertEqual(makeDynString(), exceptionInfo);
}
//-------------------------------------------------------
/** 
 * Try to lock a device that is already locked.
 */ 
void testUnRecipe_unRecipeFunctions_lockDeviceTwice() {
  dyn_string dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo;
    
  // TODO: implement mechanism in fwUnitTestHarness to allow skipping tests
  if ( UI_MAN != myManType() )
  {
     DebugTN("Skipping test '" + __FUNCTION__ + "', since we are *NOT* running within a UI");
     return;
  }

  // Try to lock the device sDpName1
  bool bResult = _unRecipeFunctions_lockDevices(makeDynString(sDpName1+"."), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo);
  assertEqual(TRUE, bResult);
  assertEqual(makeDynString(), dsLockedDeviceDp);
  assertEqual(makeDynString(), dsLockedDeviceAlias);
  assertEqual(makeDynString(), exceptionInfo);

  bResult = _unRecipeFunctions_lockDevices(makeDynString(sDpName1+"."), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo);
  assertEqual(FALSE, bResult);
  assertEqual(makeDynString(sDpName1+"."), dsLockedDeviceDp);
  assertEqual(makeDynString(""), dsLockedDeviceAlias);
  assertEqual(makeDynString(), exceptionInfo);
}
//-------------------------------------------------------
/** 
 * Try to lock an empty list of devices.
 */ 
void testUnRecipe_unRecipeFunctions_lockEmptyListOfDevices() {
  dyn_string dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo;
    
  // Try to lock the device sDpName1
  bool bResult = _unRecipeFunctions_lockDevices(makeDynString(), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo);
  assertEqual(FALSE, bResult);
  assertEqual(makeDynString(), dsLockedDeviceDp);
  assertEqual(makeDynString(), dsLockedDeviceAlias);
  assertEqual(3, dynlen(exceptionInfo));
}
//-------------------------------------------------------
/** 
 * Try to unlock an empty list of devices.
 */ 
void testUnRecipe_unRecipeFunctions_unlockEmptyListOfDevices() {
  dyn_string exceptionInfo;
    
  bool bResult = _unRecipeFunctions_unlockDevices(makeDynString(), exceptionInfo);
  assertEqual(FALSE, bResult);
  assertEqual(3, dynlen(exceptionInfo));
}
//-------------------------------------------------------
/** 
 * Try to lock a list of devices where one of them is already locked.
 */ 
void testUnRecipe_unRecipeFunctions_lockDeviceList() {
  dyn_string dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo;
    
  // TODO: implement mechanism in fwUnitTestHarness to allow skipping tests
  if ( UI_MAN != myManType() )
  {
     DebugTN("Skipping test '" + __FUNCTION__ + "', since we are *NOT* running within a UI");
     return;
  }

  // Try to lock the device sDpName1
  bool bResult = _unRecipeFunctions_lockDevices(makeDynString(sDpName2+"."), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo);
  assertEqual(TRUE, bResult);
  assertEqual(makeDynString(), dsLockedDeviceDp);
  assertEqual(makeDynString(), dsLockedDeviceAlias);
  assertEqual(makeDynString(), exceptionInfo);
  
  bResult = _unRecipeFunctions_lockDevices(makeDynString(sDpName1+".", sDpName2+"."), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo);
  assertEqual(FALSE, bResult);
  assertEqual(makeDynString(sDpName2+"."), dsLockedDeviceDp);
  assertEqual(makeDynString(""), dsLockedDeviceAlias);
  assertEqual(makeDynString(), exceptionInfo);
}
//-------------------------------------------------------
/** 
 * Try to unlock a device that is not locked.
 */ 
void testUnRecipe_unRecipeFunctions_unlockUnlockedDevice() {
  dyn_string exceptionInfo;
    
  bool bResult = _unRecipeFunctions_unlockDevices(makeDynString(sDpName1+"."), exceptionInfo);
  assertEqual(TRUE, bResult);
  assertEqual(makeDynString(), exceptionInfo);
}

//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_lockDevices_teardown() {
  int iRetVal;
  
  // Delete the dummy datapoint
  if (dpExists(sDpName1)) {
    iRetVal = dpDelete(sDpName1);
    assertEqual(0, iRetVal);
  }
  
  if (dpExists(sDpName2)) {
    iRetVal = dpDelete(sDpName2);
    assertEqual(0, iRetVal);
  }
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_lockDevices_teardownSuite() {

}
//-------------------------------------------------------
