/**@file test_unRcpFunctions_dpe.ctl  
   
@author: Ivan Prieto Barreiro (EN-ICE-SIC)

@par Creation Date
		 23/03/2017
 */
 
// You must pick up this library:
#uses "fwUnitTestComponentAsserts.ctl"

// and, of course, the library of routines you are wanting to call (ie wanting to test):
#uses "unRcpFunctions_dpe.ctl"


const string APAR_DPE = "un-SiemensUnitTestPLC-RecipesUnitTest-CPC_AnalogParameter-99999";
const string APAR_ALIAS = "UnitTestApar";

const string CONTROLLER_DPE = "un-SiemensUnitTestPLC-RecipesUnitTest-CPC_Controller-99999";
const string CONTROLLER_ALIAS = "UnitTestController";


//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_dpe_setupSuite() {
  dpCreate(APAR_DPE, "CPC_AnalogParameter");
  dpSetWait(APAR_DPE + ".ProcessInput.PosSt:_pv_range.._type", 7);
  dpSetWait(APAR_DPE + ".ProcessInput.PosSt:_pv_range.._min", 20.0,
            APAR_DPE + ".ProcessInput.PosSt:_pv_range.._max", 50.0);
  dpSetAlias(APAR_DPE + ".", APAR_ALIAS);
  dpSetUnit(APAR_DPE + ".ProcessInput.PosSt", "C");
  
  dpCreate(CONTROLLER_DPE, "CPC_Controller");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_dpe_setup() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getRangeElementFromPVSSDPE() {
  string sElement, sDpe = ".ProcessOutput.MPosR";
  dyn_string exceptionInfo;
  
  unRecipeFunctions_getRangeElementFromPVSSDPE(APAR_DPE + sDpe, sElement, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_getRangeElementFromPVSSDPE should not return any errors");
  assertTrue("" != sElement, "The element returned by unRecipeFunctions_getRangeElementFromPVSSDPE should not be empty" );
  assertTrue(sDpe != sElement, "The element returned by unRecipeFunctions_getRangeElementFromPVSSDPE is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getRangeElementFromPVSSDPE_with_fake_dp() {
  string sElement, sDpe = ".ProcessOutput.MPosR";
  dyn_string exceptionInfo;
  
  unRecipeFunctions_getRangeElementFromPVSSDPE("Fake" + sDpe, sElement, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_getRangeElementFromPVSSDPE should return an error");
}
//-------------------------------------------------------
void interactiveTestUnRecipe_unRecipeFunctions_getRangeElementFromPVSSDPE_with_fake_dp() {
  string sElement, sDpe = ".ProcessOutput.MPosR";
  dyn_string exceptionInfo;
  
  unRecipeFunctions_getRangeElementFromPVSSDPE("Fake" + sDpe, sElement, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_getRangeElementFromPVSSDPE should return an error");
}
//-------------------------------------------------------
void backgroundTestUnRecipe_unRecipeFunctions_getRangeElementFromPVSSDPE_with_fake_dp() {
  string sElement, sDpe = ".ProcessOutput.MPosR";
  dyn_string exceptionInfo;
  
  unRecipeFunctions_getRangeElementFromPVSSDPE("Fake" + sDpe, sElement, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_getRangeElementFromPVSSDPE should return an error");
}

void testUnRecipe_unRecipeFunctions_getInstanceRange() {
  string sDpe = ".ProcessInput.PosSt";
  string sMin, sMax;
  dyn_string exceptionInfo;
  
  unRecipeFunctions_getInstanceRange(APAR_DPE, sDpe, sMin, sMax, exceptionInfo);

  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_getInstanceRange should not return any errors");
  assertEqual("20.00", sMin, "The min value returned by unRecipeFunctions_getInstanceRange is wrong" );
  assertEqual("50.00", sMax, "The min value returned by unRecipeFunctions_getInstanceRange is wrong" );
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getInstanceRange_with_fake_dp() {
  string sDpe = ".ProcessInput.PosSt";
  string sMin, sMax;
  dyn_string exceptionInfo;
  
  unRecipeFunctions_getInstanceRange("Fake", sDpe, sMin, sMax, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_getInstanceRange should return an error");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getDeviceRange() {
  string sDpe = ".ProcessInput.PosSt";
  dyn_string exceptionInfo;
  
  string sRange = unRecipeFunctions_getDeviceRange(APAR_DPE + sDpe,exceptionInfo);

  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_getDeviceRange should not return any errors");
  assertEqual("[20.00, 50.00]", sRange, "The range value returned by unRecipeFunctions_getDeviceRange is wrong" );
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getDeviceRange_with_fake_dp() {
  string sDpe = ".ProcessInput.PosSt";
  dyn_string exceptionInfo;
  
  string sRange = unRecipeFunctions_getDeviceRange("Fake" + sDpe,exceptionInfo);

  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_getDeviceRange should not return any errors");
  assertEqual("", sRange, "The range value returned by unRecipeFunctions_getDeviceRange is wrong" );
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getDpeValueString() {
  dyn_string dsDpes = makeDynString(APAR_DPE + ".ProcessInput.PosSt");
  dyn_string dsValues = makeDynString("10");
  
  string sRet = unRecipeFunctions_getDpeValueString(dsDpes, dsValues);

  assertEqual(APAR_DPE + ".PosSt" + "|" + dsValues[1], sRet, "The range value returned by unRecipeFunctions_getDpeValueString is wrong" );
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getDpeValueString_get_online_value() {
  dyn_string dsDpes = makeDynString(APAR_ALIAS + ".ProcessInput.PosSt");
  dyn_string dsValues;
  dyn_string exceptionInfo;
  
  dpSet(APAR_DPE + ".ProcessInput.PosSt", 99);
  string sRet = unRecipeFunctions_getDpeValueString(dsDpes, dsValues);
  assertEqual(APAR_ALIAS + ".PosSt" + "|99", sRet, "The range value returned by unRecipeFunctions_getDpeValueString is wrong" );
  dpSet(dsDpes[1], "0");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getDpeUnit() {
  dyn_string exceptionInfo;

  string sRet = unRecipeFunctions_getDpeUnit(APAR_DPE + ".ProcessInput.PosSt", exceptionInfo);
  assertEqual("C", sRet, "The range value returned by unRecipeFunctions_getDpeUnit is wrong" );
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getOnlineValueDpes() {
  
  helper_unRecipeFunctions_getOnlineValueDpes(APAR_DPE + ".ProcessOutput.MPosR", APAR_DPE + ".ProcessInput.PosSt");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MSPH", CONTROLLER_DPE + ".ProcessInput.ActSPH");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MSPL", CONTROLLER_DPE + ".ProcessInput.ActSPL");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MSP", CONTROLLER_DPE + ".ProcessInput.ActSP"); // IS-1943
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MOutH", CONTROLLER_DPE + ".ProcessInput.ActOutH");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MOutL", CONTROLLER_DPE + ".ProcessInput.ActOutL");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MKc", CONTROLLER_DPE + ".ProcessInput.ActKc");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MTi", CONTROLLER_DPE + ".ProcessInput.ActTi");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MTd", CONTROLLER_DPE + ".ProcessInput.ActTd");
  helper_unRecipeFunctions_getOnlineValueDpes(CONTROLLER_DPE + ".ProcessOutput.MTds", CONTROLLER_DPE + ".ProcessInput.ActTds");
      
}

void helper_unRecipeFunctions_getOnlineValueDpes(string sInputDpe, string sExpectedResult) {
  dyn_string dsOutputDpes, exceptionInfo;
  dyn_string dsDpNames = makeDynString(sInputDpe);
  
  unRecipeFunctions_getOnlineValueDpes(dsDpNames, dsOutputDpes, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_getOnlineValueDpes(..) should not return any errors");
  assertEqual(dynlen(dsDpNames), dynlen(dsOutputDpes), "The number of elements returned by unRecipeFunctions_getOnlineValueDpes(..) is wrong");
  assertEqual(sExpectedResult, dsOutputDpes[1], "The value returned by unRecipeFunctions_getOnlineValueDpes(..) is wrong");
}

//-------------------------------------------------------

void testUnRecipe_unRecipeFunctions_getDefaultValueDpes() {
  
  helper_unRecipeFunctions_getDefaultValueDpes(APAR_DPE + ".ProcessOutput.MPosR", APAR_DPE + ".ProcessOutput.DfltVal");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MSPH",  CONTROLLER_DPE + ".ProcessOutput.DefSPH");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MSPL",  CONTROLLER_DPE + ".ProcessOutput.DefSPL");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MSP",   CONTROLLER_DPE + ".ProcessOutput.DefSP");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MOutH", CONTROLLER_DPE + ".ProcessOutput.DefOutH");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MOutL", CONTROLLER_DPE + ".ProcessOutput.DefOutL");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MKc",   CONTROLLER_DPE + ".ProcessOutput.DefKc");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MTi",   CONTROLLER_DPE + ".ProcessOutput.DefTi");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MTd",   CONTROLLER_DPE + ".ProcessOutput.DefTd");
  helper_unRecipeFunctions_getDefaultValueDpes(CONTROLLER_DPE + ".ProcessOutput.MTds",  CONTROLLER_DPE + ".ProcessOutput.DefTds");
      
}


void helper_unRecipeFunctions_getDefaultValueDpes(string sInputDpe, string sExpectedResult) {
  dyn_string dsOutputDpes, exceptionInfo;
  dyn_string dsDpNames = makeDynString(sInputDpe);
  
  unRecipeFunctions_getDefaultValueDpes(dsDpNames, dsOutputDpes, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) == 0, "The call to helper_unRecipeFunctions_getDefaultValueDpes(..) should not return any errors");
  assertEqual(dynlen(dsDpNames), dynlen(dsOutputDpes), "The number of elements returned by helper_unRecipeFunctions_getDefaultValueDpes(..) is wrong");
  assertEqual(sExpectedResult, dsOutputDpes[1], "The value returned by helper_unRecipeFunctions_getDefaultValueDpes(..) is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_dpe_teardown() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_dpe_teardownSuite() {
  dpDelete(APAR_DPE);
  dpDelete(CONTROLLER_DPE);
}
//-------------------------------------------------------
