#uses "unImportDevice"
#uses "unConfigGenericFunctions"
#uses "unRcpConfigUnRcpInstance.ctl"
#uses "unRcpConfigUnRcpClass.ctl"
#uses "../unCPC/CPC.postInstall"

const string DEVICES_DRIVER_NUMBER = "2";
const string RECIPE_DRIVER_NUMBER = "3";

//-------------------------------------------------------
bool unRecipeTestFunctions_importDevices() {
  bool importOk = true;
  const dyn_string dsImportFiles = makeDynString("wincc_oa_db_file_RecipesTest.txt", "Recipes_RecipesTest.txt");
    
  for ( int i = 1; i <= dynlen(dsImportFiles); i++ )
  {
      importOk &= importDevices(dsImportFiles[i], DEVICES_DRIVER_NUMBER);
      DebugN(__FUNCTION__ + ":" + __LINE__ + ": Imported '" + dsImportFiles[i] + "' --> " + importOk);
  }


  return importOk;
}
//-------------------------------------------------------
private bool importDevices(string sFileName, int iDriver) {
  dyn_string exceptionInfo;
  string logfile = "";

  sFileName = getPath(DATA_REL_PATH, sFileName);
 
  cpcGenericDpFunctions_runSimDriver(iDriver, exceptionInfo);

  if (dynlen(exceptionInfo) > 0) {
      DebugTN(exceptionInfo);
      exit(1);
  }

 //string logfile = getPath(LOG_REL_PATH) + "cli_check.txt";
 //remove(logfile);
 //DebugTN("log file: " + logfile);


  bool importOk = unImportDevice_importFile(sFileName, 
                                "",  // log file
                                iDriver, 
                                "RDB-99) EVENT",
                                "RDB-99) EVENT",
                                "RDB-99) EVENT",
                                false);
  return importOk;
}
//-------------------------------------------------------
bool unRecipeTestFunctions_deleteDevices() {  
  bool deleteOk = true;
  deleteOk &= deleteDevices("DELETE_Recipes_RecipesTest.txt", "SOFT_FE", RECIPE_DRIVER_NUMBER);
  deleteOk &= deleteDevices("DELETE_wincc_oa_db_file_RecipesTest.txt", "S7", DEVICES_DRIVER_NUMBER);
  return deleteOk;
}

//-------------------------------------------------------
private bool deleteDevices(string sFileName, string sFrontEndType, int iDriver) {
  dyn_string exceptionInfo;

  sFileName = getPath(DATA_REL_PATH, sFileName);
  
  cpcGenericDpFunctions_runSimDriver(iDriver, exceptionInfo);
  if (dynlen(exceptionInfo) > 0) {
      DebugTN(exceptionInfo);
      exit(1);
  }

  bool deleteOk = unImportDevice_deleteFromFile(sFileName, "",
                                                  unConfigGenericFunctions_convertPlcTypeToLegacyType(sFrontEndType),
                                                  iDriver);
  return deleteOk;
}
//-------------------------------------------------------

int startSimulationDriver(int iManNum) {
  string simMan,cmd;

  if (_UNIX) {
    simMan = getPath(BIN_REL_PATH,"WCCILsim");
    cmd = simMan + " -proj "+PROJ+" -num " + iManNum + " &";
  } else {
    simMan = getPath(BIN_REL_PATH,"WCCILsim.exe");
    cmd = "START " + simMan + " -proj " + PROJ + " -num " + iManNum;
  }
  
  int iRetVal = system(cmd); 
  // give it time to start up, just with sleep now, find something to wait for instead....
  delay(0,500);

  return iRetVal;
}
//-------------------------------------------------------
