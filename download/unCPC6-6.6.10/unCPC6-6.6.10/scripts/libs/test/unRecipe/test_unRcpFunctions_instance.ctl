#uses "unRcpFunctions_privileges"
#uses "test/unRecipe/test_unRcpFunctions_importDevices.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

#uses "unRcpFunctions_instance.ctl"

//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_instance_setupSuite() {
  unRecipeTestFunctions_importDevices();
  unGenericDpFunctions_InitializeAliasDp("");
}

//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_instance_teardownSuite() {
  unRecipeTestFunctions_deleteDevices();
}

//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_updatePredefinedRecipeInstancePrivileges() {
  dyn_string exceptionInfo;
  string sDeviceOperator, sDeviceExpert, sDeviceAdmin;
  string sDefaultOperator, sDefaultExpert, sDefaultAdmin;
  
  string sClassName = "SieRcpClass2";
  string sDpName = _unGenericDpFunctions_dpAliasToName(sClassName + "/" + UN_RCP_PREDEFINED_NAME);
  unRecipeFunctions_normalizeDp(sDpName);  
  
  // First remove all the privileges of the predefined recipe instance
  dpSetWait(sDpName + ".accessControl.operator", "",
	       sDpName + ".accessControl.expert", "",
        sDpName + ".accessControl.admin", "");
  
  // Update the privileges
  unRecipeFunctions_updatePredefinedRecipeInstancePrivileges(sClassName, exceptionInfo);
  
  // Get the privileges assigned to the predefined recipe instance
  dpGet(sDpName + ".accessControl.operator", sDeviceOperator,
	       sDpName + ".accessControl.expert", sDeviceExpert,
        sDpName + ".accessControl.admin", sDeviceAdmin);
  
  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_updatePredefinedRecipeInstancePrivileges returned some errors");
  
  // Get the default privileges
  dyn_string dsDefaultOperator, dsDefaultExpert, dsDefaultAdmin;
  unRecipeFunctions_getDefaultInstancePrivileges("operator", dsDefaultOperator);
  unRecipeFunctions_getDefaultInstancePrivileges("expert", dsDefaultExpert);
  unRecipeFunctions_getDefaultInstancePrivileges("admin", dsDefaultAdmin);
  
  dyn_string dsDeviceOperator, dsDeviceExpert, dsDeviceAdmin;
  dsDeviceOperator = strsplit(sDeviceOperator, ",");
  dsDeviceExpert   = strsplit(sDeviceExpert, ",");
  dsDeviceAdmin    = strsplit(sDeviceAdmin, ",");
  
  bool bOperatorOk = unRecipeFunctions_areListsEqual(dsDefaultOperator, dsDeviceOperator);
  bool bExpertOk   = unRecipeFunctions_areListsEqual(dsDefaultExpert, dsDeviceExpert);
  bool bAdminOk    = unRecipeFunctions_areListsEqual(dsDefaultAdmin, dsDeviceAdmin);

  assertTrue(bOperatorOk, "The predefined recipe instance privileges are wrong for the operator. Expected: " + dsDefaultOperator + " , Value: " + dsDeviceOperator );  
  assertTrue(bExpertOk, "The predefined recipe instance privileges are wrong for the expert. Expected: " + dsDefaultExpert + " , Value: " + dsDeviceExpert);
  assertTrue(bAdminOk, "The predefined recipe instance privileges are wrong for the admin. Expected: " + dsDefaultAdmin + " , Value: " + dsDeviceAdmin);
  
}
//-------------------------------------------------------
