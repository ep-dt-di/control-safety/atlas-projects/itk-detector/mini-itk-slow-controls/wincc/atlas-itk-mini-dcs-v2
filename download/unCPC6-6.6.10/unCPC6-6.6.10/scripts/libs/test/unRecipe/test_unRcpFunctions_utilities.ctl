/**@file test_unRcpFunctions_utilities.ctl  
   
@author: Ivan Prieto Barreiro (EN-ICE-SIC)

@par Creation Date
		 23/03/2017
 */
 
// You must pick up this library:
#uses "fwUnitTestComponentAsserts.ctl"

// and, of course, the library of routines you are wanting to call (ie wanting to test):
#uses "unRcpFunctions_utilities.ctl"


//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_utilities_setupSuite() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_utilities_setup() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_setBit() {    
  int iReg01, iReg02;
  
  unRecipeFunctions_setBit(iReg01, 0, true);
  assertEqual(1, iReg01, "The bit has not been set correctly");
  
  unRecipeFunctions_setBit(iReg02, 1, true);
  assertEqual(2, iReg02, "The bit has not been set correctly");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getBit() {    
  int iReg01, iValue;
  
  iReg01 = 1;
  unRecipeFunctions_getBit(iReg01, 0, iValue);
  assertEqual(1, iValue, "The bit has not been get correctly");
  unRecipeFunctions_getBit(iReg01, 1, iValue);
  assertEqual(0, iValue, "The bit has not been get correctly");
  
  iReg01 = 2;
  unRecipeFunctions_getBit(iReg01, 0, iValue);
  assertEqual(0, iValue, "The bit has not been get correctly");
  unRecipeFunctions_getBit(iReg01, 1, iValue);
  assertEqual(1, iValue, "The bit has not been get correctly");
}

//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_sortAscDynLists_different_elements() {  
  dyn_string dsFirst = makeDynString("Test");
  dyn_int diSecond = makeDynInt();
  dyn_string exceptionInfo;
  
  unRecipeFunctions_sortAscDynLists(dsFirst, diSecond, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_sortAscDynLists should return an exception");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_sortAscDynLists() {  
  dyn_string dsFirst = makeDynString("B", "A");
  dyn_int diSecond = makeDynInt(2, 1);
  dyn_string exceptionInfo;
  
  unRecipeFunctions_sortAscDynLists(dsFirst, diSecond, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_sortAscDynLists should not return any exception");
  assertEqual("A", dsFirst[1], "The first element of the string list is wrong");
  assertEqual("B", dsFirst[2], "The second element of the string list is wrong");
  assertEqual(1, diSecond[1], "The first element of the int list is wrong");
  assertEqual(2, diSecond[2], "The second element of the int list is wrong");
  
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getDynPos_empty_search() {  
  dyn_string dsItems = makeDynString("Test");
  dyn_string dsSearch = makeDynString();
  dyn_int diResult;
  
  unRecipeFunctions_getDynPos(dsSearch, dsItems, diResult);
  assertTrue(dynlen(diResult) == 0, "The call to unRecipeFunctions_getDynPos should return an empty result");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getDynPos() {  
  dyn_string dsItems = makeDynString("Test1", "Test2", "Test3");
  dyn_string dsSearch = makeDynString("Test2");
  dyn_int diResult;
  
  unRecipeFunctions_getDynPos(dsSearch, dsItems, diResult);
  assertTrue(dynlen(diResult) == 1, "The call to unRecipeFunctions_getDynPos should return one result");
  assertEqual(2, diResult[1], "The value returned by unRecipeFunctions_getDynPos is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_dynDiff_empty_result() {  
  dyn_string dsFirst = makeDynString("Test1", "Test2", "Test3");
  dyn_string dsSecond = makeDynString("Test3", "Test2", "Test1");
  dyn_string dsResult;
  
  unRecipeFunctions_dynDiff(dsFirst, dsSecond, dsResult);
  assertTrue(dynlen(dsResult) == 0, "The call to unRecipeFunctions_dynDiff should return an empty list");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_dynDiff() {  
  dyn_string dsFirst = makeDynString("Test1", "Test2", "Test3");
  dyn_string dsSecond = makeDynString("Test3", "Test2");
  dyn_string dsResult;
  
  unRecipeFunctions_dynDiff(dsFirst, dsSecond, dsResult);
  assertTrue(dynlen(dsResult) == 1, "The call to unRecipeFunctions_dynDiff should return one result value");
  assertEqual("Test1", dsResult[1], "The value returned by the call to unRecipeFunctions_dynDiff is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_areListEqual_with_different_list_size() {  
  dyn_string dsFirst = makeDynString("Test1", "Test2", "Test3");
  dyn_string dsSecond = makeDynString("Test1", "Test2");
  bool bResult;
  
  bResult = unRecipeFunctions_areListsEqual(dsFirst, dsSecond);
  assertFalse(bResult, "The call to unRecipeFunctions_areListsEqual should return false");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_areListEqual_with_different_ordered_elements() {  
  dyn_string dsFirst = makeDynString("Test1", "Test2");
  dyn_string dsSecond = makeDynString("Test2", "Test1");
  bool bResult;
  
  bResult = unRecipeFunctions_areListsEqual(dsFirst, dsSecond);
  assertTrue(bResult, "The call to unRecipeFunctions_areListsEqual should return true");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_mappingInsertValue_with_unique_value() {  
  mapping m;
  bool bUniqueValue = true;
  
  unRecipeFunctions_mappingInsertValue(m, "key", "value1", bUniqueValue);
  unRecipeFunctions_mappingInsertValue(m, "key", "value2", bUniqueValue);
  unRecipeFunctions_mappingInsertValue(m, "key", "value1", bUniqueValue);
  
  assertEqual(1, dynlen(mappingKeys(m)), "The number of elements in the map is wrong");
  assertEqual(2, dynlen(m["key"]), "The number of elements for the key is wrong");
  assertTrue(dynContains(m["key"], "value1") > 0, "The value 'value1' is not in the map");
  assertTrue(dynContains(m["key"], "value2") > 0, "The value 'value2' is not in the map");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_mappingInsertValue_with_non_unique_value() {  
  mapping m;
  bool bUniqueValue = false;
  
  unRecipeFunctions_mappingInsertValue(m, "key", "value1", bUniqueValue);
  unRecipeFunctions_mappingInsertValue(m, "key", "value2", bUniqueValue);
  unRecipeFunctions_mappingInsertValue(m, "key", "value1", bUniqueValue);
  
  assertEqual(1, dynlen(mappingKeys(m)), "The number of elements in the map is wrong");
  assertEqual(3, dynlen(m["key"]), "The number of elements for the key is wrong");
  assertTrue(dynContains(m["key"], "value1") > 0, "The value 'value1' is not in the map");
  assertTrue(dynContains(m["key"], "value2") > 0, "The value 'value2' is not in the map");
}

//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_isLegalRecipeName() {
  assertFalse(unRecipeFunctions_isLegalRecipeName("test/dpName"), "The recipe name 'test/dpName' should not be legal ");
  assertFalse(unRecipeFunctions_isLegalRecipeName("test\\dpName"), "The recipe name 'test\\dpName' should not be legal ");
  assertFalse(unRecipeFunctions_isLegalRecipeName("test.dpName"), "The recipe name 'test.dpName' should not be legal ");
  assertTrue(unRecipeFunctions_isLegalRecipeName("testDpName"), "The recipe name 'test/dpName' should not be legal ");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_joinDynString_with_different_elements_in_lists() {
  dyn_string dsFirst = makeDynString("one");
  dyn_string dsSecond = makeDynString("one", "two");
  dyn_string exceptionInfo;
  
  dyn_string dsResult = unRecipeFunctions_joinDynString(dsFirst, dsSecond, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_joinDynString should return an exception");
  assertEqual(0, dynlen(dsResult), "The numberf of elements in the result list is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_joinDynString() {
  dyn_string dsFirst = makeDynString("1-", "2-");
  dyn_string dsSecond = makeDynString("one", "two");
  dyn_string exceptionInfo;
  
  dyn_string dsResult = unRecipeFunctions_joinDynString(dsFirst, dsSecond, exceptionInfo);
  assertFalse(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_joinDynString should not return an exception");
  assertEqual(2, dynlen(dsResult), "The numberf of elements in the result list is wrong");
  assertEqual("1-one", dsResult[1], "The first element of the result list is wrong");
  assertEqual("2-two", dsResult[2], "The second element of the result list is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_compareListElements_with_missing_dps() {
  dyn_string dsFirst = makeDynString("one", "two", "three");
  dyn_string dsSecond = makeDynString("one", "two");
  dyn_string dsMissingDpNames, dsExtraDpNames;
  
  bool bResult = unRecipeFunctions_compareListElements(dsFirst, dsSecond, dsMissingDpNames, dsExtraDpNames);
  assertFalse(bResult, "The call to unRecipeFunctions_compareListElements should return false");
  assertEqual(1, dynlen(dsMissingDpNames), "The number of missing dp names is wrong");
  assertEqual(0, dynlen(dsExtraDpNames), "The number of extra dp names is wrong");
  assertEqual("three", dsMissingDpNames[1], "The element returned in dsMissingDpNames is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_compareListElements_with_extra_dps() {
  dyn_string dsFirst = makeDynString("one", "two");
  dyn_string dsSecond = makeDynString("one", "two", "three");
  dyn_string dsMissingDpNames, dsExtraDpNames;
  
  bool bResult = unRecipeFunctions_compareListElements(dsFirst, dsSecond, dsMissingDpNames, dsExtraDpNames);
  assertFalse(bResult, "The call to unRecipeFunctions_compareListElements should return false");
  assertEqual(0, dynlen(dsMissingDpNames), "The number of missing dp names is wrong");
  assertEqual(1, dynlen(dsExtraDpNames), "The number of extra dp names is wrong");
  assertEqual("three", dsExtraDpNames[1], "The element returned in dsExtraDpNames is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_compareListElements() {
  dyn_string dsFirst = makeDynString("one", "two");
  dyn_string dsSecond = makeDynString("two", "one");
  dyn_string dsMissingDpNames, dsExtraDpNames;
  
  bool bResult = unRecipeFunctions_compareListElements(dsFirst, dsSecond, dsMissingDpNames, dsExtraDpNames);
  assertTrue(bResult, "The call to unRecipeFunctions_compareListElements should return false");
  assertEqual(0, dynlen(dsMissingDpNames), "The number of missing dp names is wrong");
  assertEqual(0, dynlen(dsExtraDpNames), "The number of extra dp names is wrong");  
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_compareListElements_with_ignore_system_names() {
  dyn_string dsFirst = makeDynString("dist_1:one", "dist_1:two");
  dyn_string dsSecond = makeDynString("dist_2:two", "dist_2:one");
  dyn_string dsMissingDpNames, dsExtraDpNames;
  
  bool bResult = unRecipeFunctions_compareListElements(dsFirst, dsSecond, dsMissingDpNames, dsExtraDpNames);
  assertTrue(bResult, "The call to unRecipeFunctions_compareListElements should return false");
  assertEqual(0, dynlen(dsMissingDpNames), "The number of missing dp names is wrong");
  assertEqual(0, dynlen(dsExtraDpNames), "The number of extra dp names is wrong");  
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_compareListElements_without_ignore_system_names() {
  dyn_string dsFirst = makeDynString("dist_1:one", "dist_1:two");
  dyn_string dsSecond = makeDynString("dist_2:two", "dist_2:one");
  dyn_string dsMissingDpNames, dsExtraDpNames;
  
  bool bResult = unRecipeFunctions_compareListElements(dsFirst, dsSecond, dsMissingDpNames, dsExtraDpNames, false);
  assertFalse(bResult, "The call to unRecipeFunctions_compareListElements should return false");
  assertEqual(2, dynlen(dsMissingDpNames), "The number of missing dp names is wrong");
  assertEqual(2, dynlen(dsExtraDpNames), "The number of extra dp names is wrong");  
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_removeSystemNameFromList() {
  dyn_string dsList = makeDynString("dist_1:one", "dist_1:two");
  dyn_string dsRetList = unRecipeFunctions_removeSystemNameFromList(dsList);
  
  assertEqual(2, dynlen(dsRetList), "The number of elements in the returned list is wrong");
  assertEqual("one", dsRetList[1], "The first element in the returned list is wrong");
  assertEqual("two", dsRetList[2], "The second element in the returned list is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_normalizeDp_without_trailing_dot() {
  string sDpName = "testDp";
  
  unRecipeFunctions_normalizeDp(sDpName);
  assertEqual("testDp", sDpName, "The normalized dp is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_normalizeDp_with_trailing_dot() {
  string sDpName = "testDp.";
  
  unRecipeFunctions_normalizeDp(sDpName);
  assertEqual("testDp", sDpName, "The normalized dp is wrong");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getRangeValues_with_bad_refined_range() {
  string sRange = "[0-100]";
  string sMin, sMax;
  dyn_string exceptionInfo;
    
  unRecipeFunctions_getRangeValues(sRange, sMin, sMax, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) > 0, "The call to unRecipeFunctions_getRangeValues should return an exception");
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_getRangeValues() {
  string sRange = "[ 0, 100 ]";
  string sMin, sMax;
  dyn_string exceptionInfo;
    
  unRecipeFunctions_getRangeValues(sRange, sMin, sMax, exceptionInfo);
  assertTrue(dynlen(exceptionInfo) == 0, "The call to unRecipeFunctions_getRangeValues should not return an exception");
  assertEqual("0", sMin, "The min value for the range is wrong");
  assertEqual("100", sMax, "The max value for the range is wrong");  
}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_utilities_teardown() {

}
//-------------------------------------------------------
void testUnRecipe_unRecipeFunctions_utilities_teardownSuite() {

}
//-------------------------------------------------------
