//------------------------------------------------------------------------------------------------------------------------
//unHVACAlarmDiagnostic_getAlarmDiagnosticInfo
/** function retrieves alarm diagnostic data from files in panels/vision/diagnostic/appname/*

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sAlarmName, input, name of the alarm
@param sApplicationName, input, name of the application = name of the directory containing file with alarm information

@return output, for description
*/

dyn_string unHVACAlarmDiagnostic_getAlarmDiagnosticInfo(string sAlarmName, string sApplicationName){

  string sDelimiter = "@";
  dyn_string dsExceptionInfo;
  dyn_string dsLineItems;
  
  // sPath to diagnostic file directory (panels/vision/diagnostic/sApplicationName/):
  string sPath = getPath(PANELS_REL_PATH, "vision/diagnostic/" + sApplicationName + "/");
  dyn_string dsFileNames = getFileNames(sPath);
  for(int i=1; i <= dynlen(dsFileNames); i++){  
    string sFileString, sLine;
    fileToString(sPath + dsFileNames[i], sFileString);
    dyn_string dsLines = strsplit(sFileString, "\n");
      
    for (int j=1; j <= dynlen(dsLines); j++){
      sLine = strrtrim(strrtrim(strltrim(dsLines[j])), "\r");
      if(sLine != ""){
        dsLineItems = strsplit(sLine,sDelimiter);
        if(strtolower(dsLineItems[1]) == strtolower(sAlarmName)){
          return dsLineItems;
        }
      }
    }
  }
  
  fwException_raise(dsExceptionInfo, "ERROR", "Alarm " + sAlarmName + " info not found in " + sPath,"");
  fwExceptionHandling_display(dsExceptionInfo);
  return dsLineItems;
}
