/*
@author: Alexey Merezhin

Creation Date: 04/02/2013

version 1.0

External Function :
    . unHVACUserlib_UnicosInfosCV

Purpose:
This library contains the user functions to create Configuration file for the LHCLogging API in CV projects

Usage: Public

PVSS manager usage: NV

Constraints:
    . PVSS version: 3.1
    . operating system: wds XP
    . distributed system: No

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@reviewed 2018-07-24 @whitelisted{BackgroundProcessing}

*/

//-----------------------------------------------------------------------------------------------------------

unHVACUserlib_UnicosInfosCV(string sDpe, string &sName,  string &sDescription, string &sHierarchy, string &sAlias, string &sE) {
    dyn_string dsSplit;
    string sLocation, sApplication;
    bool bState, bUseDpe, bOK;
    int iNum, iLen;

    string sSystemName = getSystemName();
    sHierarchy = "ERROR";
    sE = sDpe;

    //remove system name
    //unCoreLHCServices_set_SystemName(sDpe, bState, false);
    
    if (strpos(sDpe, ":") >= 0) {   // if name contains system name
        string sSystem_defined = unGenericDpFunctions_getSystemName(sDpe);
        sDpe = substr(sDpe, strlen(sSystem_defined), strlen(sDpe));
    }

    //Domain / nature
    string deviceName = unGenericDpFunctions_getDpName(sDpe);

    //Location
    dsSplit = strsplit(sSystemName, "_");
    if (dynlen(dsSplit) > 1) {
        sLocation = dsSplit[1];
    } else {
        sLocation = substr(sSystemName, 0, strlen(sSystemName) - 1);
    }

    dsSplit = strsplit(sDpe, ".");
    if (dynlen(dsSplit) == 3) { //UNICOS Device
        sAlias = dpGetAlias(dsSplit[1] + "."); // TODO: check me
        sName = sAlias + "." + dsSplit[3];
        sE = substr(sDpe, strpos(sDpe, ".") + 1, strlen(sDpe));
        sDescription = unGenericDpFunctions_getDescription(deviceName);
        strreplace(sDescription, "$", "_");
        strreplace(sDescription, "?", "_");

        dynClear(dsSplit);
        dsSplit = strsplit(sSystemName, "_");
        if (dynlen(dsSplit) > 1) {
            sLocation = dsSplit[1];
        } else {
            sLocation = substr(sSystemName, 0, strlen(sSystemName) - 1);
        }

        //Application
        dynClear(dsSplit);
        dsSplit = strsplit(sDpe, "-");
        if (dynlen(dsSplit) > 3) {
            sApplication = dsSplit[3];
        } else {
            sApplication = "UNKNOWN";
        }

        sHierarchy = unHVACUserlib_Hierarchy(sApplication, dpTypeName(sDpe), sDpe);
    } else { // process front-end
        int s7pos = strpos(dsSplit[1], S7_PLC_DPTYPE);
        int un_pos = strpos(dsSplit[1], c_unSystemIntegrity_UnPlc);
        dyn_string subApps;
        if (s7pos >= 0) {
            unLHCLoggingDBUserLib_getSystemAlarmInfo(dsSplit, sLocation, sDpe, sDescription, sAlias, sName, sHierarchy);
            string plcName = substr(dsSplit[1], s7pos);
            dpGet(sSystemName + plcName + ".configuration.subApplications", subApps);
            sApplication=subApps[1]; // just take 1st subApplication for simplicity
            sHierarchy = unHVACUserlib_Hierarchy(sApplication, "WCCOA_INTEGRITY", sDpe);
        } else if (un_pos >= 0) {
            unLHCLoggingDBUserLib_getSystemAlarmInfo(dsSplit, sLocation, sDpe, sDescription, sAlias, sName, sHierarchy);
            string plcName = substr(dsSplit[1], un_pos);
            dpGet(sSystemName + plcName + ".configuration.subApplications", subApps);
            sApplication=subApps[1]; // just take 1st subApplication for simplicity
            sHierarchy = unHVACUserlib_Hierarchy(sApplication, "WCCOA_INTEGRITY", sDpe);
        } else {
            DebugN("Can't process dpe: " + sDpe);
        }
    }

    // ignore all evStsReg0*
    if (strpos(sDpe, "evStsReg0") >= 0) {
        sHierarchy = "IGNORE";
    } else if (strpos(sDpe, "_unSystemAlarm_DS_Comm_SOFT_FE_") >= 0) {
        sHierarchy = "IGNORE";
    } else {
        // ignore all SPARE, and print name to log, for info
        if (strpos(strtoupper(sDescription), "SPARE") >= 0) {
            DebugN("unHVACUserlib_UnicosInfosCV: IGNORE SPARE: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
            sHierarchy = "IGNORE";
        }
        // ignore all RESERVE, and print name to log, for info
        if (strpos(strtoupper(sDescription), "RESERVE") >= 0) {
            DebugN("unHVACUserlib_UnicosInfosCV: IGNORE RESERVE: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
            sHierarchy = "IGNORE";
        }
    }
}

/** Function builds a hierarchy for CV project.
 * The hierarchy format: Location/SubLocation/Application/DeviceType
*/
string unHVACUserlib_Hierarchy(string application, string deviceType, string sDpe) {
    string hierarchy = "CV/";
    if (patternMatch("*L4_*", application) || patternMatch("*LINAC4*", application)) {
        hierarchy += "MEYRIN/LINAC4/";
    } else if (patternMatch("*RFQ4*", application)) {
        hierarchy += "MEYRIN/LINAC4/";
    } else if (patternMatch("*LINAC3*", application)) {
        hierarchy += "MEYRIN/LINAC3/";
    } else if (patternMatch("*MEDICIS*", application)) {
        hierarchy += "MEYRIN/MEDICIS/";
    } else if (patternMatch("*_AD_*", application)) {
        hierarchy += "MEYRIN/AD/";
    } else if (patternMatch("*B176*", application)) {
        hierarchy += "MEYRIN/176/";
    } else if (patternMatch("*EA_ED_SKID*", application)) {
        hierarchy += "MEYRIN/157/";
    } else if (patternMatch("*_ELENA*", application)) {
        hierarchy += "MEYRIN/AD/";
    } else if (patternMatch("*180*", application)) {
        hierarchy += "MEYRIN/";
    } else if (patternMatch("*192*", application)) {
        hierarchy += "MEYRIN/B192/";
    } else if (patternMatch("*B201", application)) {
        hierarchy += "MEYRIN/201/";
    } else if (patternMatch("*ME9*", application)) {
        hierarchy += "MEYRIN/212/";
    } else if (patternMatch("*EAST*", application)) {
        hierarchy += "MEYRIN/EAST_AREA/";
    } else if (patternMatch("[CHM]*513*", application) || patternMatch("513*", application)) {
        hierarchy += "MEYRIN/513/";
    } else if (patternMatch("*378*", application) || patternMatch("COOL_MEQ59", application)) {
        hierarchy += "MEYRIN/378/";
    } else if (patternMatch("*LEIR*", application)) {
        hierarchy += "MEYRIN/LEIR/";
    } else if (patternMatch("*NTOF*", application)) {
        hierarchy += "MEYRIN/NTOF/";
    } else if (patternMatch("*ISOLDE*", application)) {
        hierarchy += "MEYRIN/ISOLDE/";
    } else if (patternMatch("*FAIR*", application)) {
        hierarchy += "MEYRIN/FAIR/";
    } else if (patternMatch("HVAC_PS*", application) || patternMatch("COOL_PS*", application)) {
        hierarchy += "MEYRIN/PS/";
    } else if (patternMatch("*107*", application)) {
        hierarchy += "MEYRIN/107/";
    } else if (patternMatch("LASER_HVAC_311_OFFICES", application)) {
        // trick to ignore all signals from LASER_HVAC_311_OFFICES application (CVMON)
        return "IGNORE";
    } else if (patternMatch("*311*", application)) {
        hierarchy += "MEYRIN/311/";
    } else if (patternMatch("*B355*", application)) {
        hierarchy += "MEYRIN/355/";
    } else if (patternMatch("*COOL_POPS_DL*", application)) {
        hierarchy += "MEYRIN/367/";
    } else if (patternMatch("*245*", application)) {
        hierarchy += "MEYRIN/245/";
    } else if (patternMatch("*B269*", application)) {
        hierarchy += "MEYRIN/269/";
    } else if (patternMatch("*HVAC_SPS*", application) || patternMatch("*HVAC_SUI*", application)) {
        hierarchy += "SPS/TUNNEL/";
    } else if (patternMatch("*COOL_SPS*", application)) {
        hierarchy += "SPS/TUNNEL/";
    } else if (patternMatch("COOL_BA6_SEC", application) || patternMatch("*COOL_BA6_TOURS", application) || patternMatch("MON_862_SIG", application)) {
        hierarchy += "SPS/TOURS_SPS/";
    } else if (patternMatch("COOL_BA[1234567]_*", application)) {
        hierarchy += "SPS/COOL_BAx/";
    } else if (patternMatch("BA[1234567]TRANE", application)) {
        hierarchy += "SPS/COOL_BAx/";
    } else if (patternMatch("COOL_BB3_*", application)) {
        hierarchy += "SPS/";
    } else if (patternMatch("HVAC_BA[1234567]*", application) || patternMatch("HVAC_BG5*", application) || patternMatch("HVAC_BM[1246]", application) || patternMatch("HVAC_BB[3456]*", application)) {
        hierarchy += "SPS/HVAC_BAx/";
    } else if (patternMatch("*NA62*", application)) {
        hierarchy += "PREVESSIN/NA62/";
    } else if (patternMatch("*TDC2*", application)) {
        hierarchy += "PREVESSIN/TDC2/";		
    } else if (patternMatch("*NA58*", application)) {
        hierarchy += "PREVESSIN/NA58/";
    } else if (patternMatch("*AMS*", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("*867*", application)) {
        hierarchy += "PREVESSIN/COOLING/";
    } else if (patternMatch("*BE91*", application) || patternMatch("HVAC_BE[12]", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("*CCR*", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("*CCC*", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("*EHN1*", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("*924*", application) || patternMatch("*773*", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("*BAF3*", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("*AC_PREVESSIN*", application)) {
        hierarchy += "PREVESSIN/HVAC/";
    } else if (patternMatch("MON_CEDAR*", application)) {
        hierarchy += "PREVESSIN/MONITORING/";
    } else if (patternMatch("*SPS_PUISARD*", application)) {
        hierarchy += "SPS/MONITORING/";
    } else if (patternMatch("*SPS_VIBRATION*", application)) {
        hierarchy += "SPS/MONITORING/";
    } else if (patternMatch("*BA8*", application) || patternMatch("*ba8*", application) || patternMatch("*CT2*", application)) {
        hierarchy += "SPS/North_Area/";
    } else if (patternMatch("*BDF_SKID*", application) ) {
        hierarchy += "SPS/North_Area/";
    } else if (patternMatch("*HIRADMAT*", application) || patternMatch("*HiRadMat*", application)) {
        hierarchy += "SPS/HIRADMAT/";
    } else if (patternMatch("*HRM*", application)) {
        hierarchy += "SPS/HIRADMAT/";
    } else if (patternMatch("*AWAKE*", application)) {
        hierarchy += "SPS/AWAKE/";
    } else if (patternMatch("*SMI2*", application)) {
        hierarchy += "SPS/HVAC_SMI2/";
    } else if (patternMatch("*ALICE*", application)) {
        hierarchy += "LHC/P2/";
    } else if (patternMatch("*CMS*", application)) {
        hierarchy += "LHC/P5/";
    } else if (patternMatch("*UIA[NO]_5*", application)) {
        hierarchy += "LHC/P5/HVAC_CMS/";
    } else if (patternMatch("*LHCb*", application) || patternMatch("*LHCB*", application)) {
        hierarchy += "LHC/P8/";
    } else if (patternMatch("*ATLAS*", application) || patternMatch("*Atlas*", application)) {
        hierarchy += "LHC/P1/";
    } else if (patternMatch("*UIA[NO]_1*", application)) {
        hierarchy += "LHC/P1/HVAC_ATLAS/";
    } else if (patternMatch("*S[GHFWURADLXM]18*", application) || patternMatch("*UW18*", application) || patternMatch("*SHM18*", application) || patternMatch("*SMA18*", application) || patternMatch("*LHC18*", application) || patternMatch("*PM18*", application) || patternMatch("*SFA181*", application)) {
        hierarchy += "LHC/P18/";
    } else if (patternMatch("*S[GHFWURADLX]1*", application) || patternMatch("*UW1*", application) || patternMatch("*SHM1*", application)  || patternMatch("*SUX1*", application) || patternMatch("*LHC1*", application) || patternMatch("FSED_3184", application)  || patternMatch("*SGX1*", application)  || patternMatch("*SCX1*", application)	|| patternMatch("*US15*", application)	|| patternMatch("COOL_USA15", application)	|| patternMatch("*SDX1*", application)) {
        hierarchy += "LHC/P1/";
    } else if (patternMatch("*S[GHFWURADLX]2*", application) || patternMatch("*UW2*", application) || patternMatch("*SHM2*", application) || patternMatch("*LHC2*", application) || patternMatch("*US2*", application)) {
        hierarchy += "LHC/P2/";
    } else if (patternMatch("*S[GHFWURADLXZ]3*", application) || patternMatch("*UW3*", application) || patternMatch("*SHM3*", application) || patternMatch("*LHC3*", application) || patternMatch("*PM3*", application)) {
        hierarchy += "LHC/P3/";
    } else if (patternMatch("*S[GHFWURADLX]4*", application) || patternMatch("*UW4*", application) || patternMatch("*SHM4*", application) || patternMatch("*LHC4*", application) || patternMatch("*US4*", application)) {
        hierarchy += "LHC/P4/";
    } else if (patternMatch("*S[GHFWURADLX]5*", application) || patternMatch("*UW5*", application) || patternMatch("*SHM5*", application) || patternMatch("*SUX5*", application) || patternMatch("*SXA5*", application) || patternMatch("*LHC5*", application) || patternMatch("*PM5*", application) || patternMatch("*UJ5*", application)  || patternMatch("*SGX5*", application)  || patternMatch("*SCX5*", application)) {
        hierarchy += "LHC/P5/";
    } else if (patternMatch("*S[GHFWURADLX]6*", application) || patternMatch("*UW6*", application) || patternMatch("*SHM6*", application) || patternMatch("*LHC6*", application) || patternMatch("*US6*", application)) {
        hierarchy += "LHC/P6/";
    } else if (patternMatch("*S[GHFWURADLX]7*", application) || patternMatch("*UW7*", application) || patternMatch("*SHM7*", application) || patternMatch("*LHC7*", application)) {
        hierarchy += "LHC/P7/";
    } else if (patternMatch("*S[GHFWURADLX]8*", application) || patternMatch("*UW8*", application) || patternMatch("*SHM8*", application) || patternMatch("*LHC8*", application) || patternMatch("*US8*", application)) {
        hierarchy += "LHC/P8/";
    } else if (patternMatch("*LHC_AirMon*", application)) {
        hierarchy += "LHC/MONITORING/";
    } else {
        DebugTN("ERROR: can't detect application for " + sDpe + " app " + application);
        return "ERROR";
    }

    hierarchy += application + "/" + deviceType;

    return hierarchy;
}
