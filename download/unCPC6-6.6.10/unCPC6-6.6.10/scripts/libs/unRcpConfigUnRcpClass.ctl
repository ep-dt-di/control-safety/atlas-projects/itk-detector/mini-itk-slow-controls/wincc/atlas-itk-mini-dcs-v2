/**
 * UNICOS
 * Copyright (C) CERN 2013 All rights reserved
 */
/**@file

// unConfigUnRcpClass.ctl
This library contains the function to configure the UnRcpClass.

@par Creation Date
  27/05/2011

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (EN-ICE)
*/

#uses "unRcpFunctions_instance.ctl"
#uses "fwGeneral/fwException.ctl"
#uses "fwConfigurationDB/fwConfigurationDB.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "unicos_declarations.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unGenericDpFunctions.ctl"
#uses "unConfigGenericFunctions.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unRcpFunctions_privileges.ctl"

//@{
//------------------------------------------------------------------------------------------------------------------------
// unConfigUnRcpClass_checkConfig
/** check the device configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
unConfigUnRcpClass_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  if (dynlen(dsConfigs) >= UN_CONFIG_COMMON_LENGTH)
  {
    // 1. Common
    unConfigGenericFunctions_checkParameters(dsConfigs, UN_CONFIG_UNRCPCLASS_DPT_NAME, exceptionInfo);
    // 2. call the device check function
    unConfigGenericFunctions_checkDeviceConfig(dsConfigs, UN_CONFIG_UNRCPCLASS_DPT_NAME, exceptionInfo);
  }
  else
  {
    fwException_raise(exceptionInfo,"ERROR","unConfigUnRcpClass_checkConfig: " + getCatStr("unGeneration","BADINPUTS"),"");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigUnRcpClass_setConfig
/** set the device configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs  input, config line as dyn_string
@param exceptionInfo output, for errors
*/
unConfigUnRcpClass_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string sDpName;
  
  // 1. Initialization : checking + dpCreate
  unConfigGenericFunctions_checkAll(dsConfigs, UN_CONFIG_UNRCPCLASS_DPT_NAME, exceptionInfo);
  if (dynlen(exceptionInfo) <= 0)    // Good inputs
  {
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];	
	unConfigGenericFunctions_createDp(sDpName, UN_CONFIG_UNRCPCLASS_DPT_NAME, exceptionInfo);
		
    if (dynlen(exceptionInfo) <= 0)  // Creation ok or dp already exists
    {
      // 2. Generate device
      // 2.1. Parameters
      unConfigGenericFunctions_setCommon(dsConfigs, exceptionInfo);
      // 2.2 call the device check function
      unConfigGenericFunctions_setDeviceConfig(dsConfigs, UN_CONFIG_UNRCPCLASS_DPT_NAME, exceptionInfo);
    }    
  }
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_UnRcpClass_checkConfig
/** check the UnRcpClass device configuration for the SOFT_FE front-end
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
SOFT_FE_UnRcpClass_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string pvssDpe;
  string recipeClass = strsplit(dsConfigs[UN_CONFIG_COMMON_ALIAS], ",")[1];
  string recipeType  = dsConfigs[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPCLASS_RECIPE_TYPE];
  string deviceList  = dsConfigs[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPCLASS_DEVICE_LIST];

  dyn_string deviceDpes = strsplit(deviceList, ",");

  // Check the length of the config line
  if (dynlen(dsConfigs) != (UN_CONFIG_UNRCPCLASS_MIN_LENGHT + UN_CONFIG_COMMON_LENGTH) &&
    dynlen(dsConfigs) != (UN_CONFIG_UNRCPCLASS_MAX_LENGHT + UN_CONFIG_COMMON_LENGTH)) {
    fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpClass_checkConfig: " + getCatStr("unGeneration","BADINPUTS"),"");
  } 
  
  // Check the recipe type
  if (recipeType != UN_CONFIG_UNRCPTYPE_DPT_NAME) {
    fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpClass_checkConfig: " + UN_CONFIG_UNRCPCLASS_DPT_NAME + " has bad recipe type","");
  }
  
  // Check the recipe class name
  if (!dpIsLegalName(recipeClass)) {
    fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpClass_checkConfig: Bad recipe class name: " + recipeClass,"");
  }
  
  // Check that all the devices exist
  int len = dynlen(deviceDpes);
  dyn_string dsWrongDpeFormats, dsWrongDpeNames;
  for (int i=1; i<=len; i++) {
    // Check that the deviceDpe is in the form: deviceAlias.Dpe
    if (dynlen(strsplit(deviceDpes[i], "."))!=2) {
      dynAppend(dsWrongDpeFormats, deviceDpes[i]);
    }
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(deviceDpes[i], pvssDpe);	
    if (!dpExists(pvssDpe) || pvssDpe[strlen(pvssDpe)-1] == "."){
      dynAppend(dsWrongDpeNames, deviceDpes[i]);
    }
  }
  
  if (dynlen(dsWrongDpeFormats)) {
    fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpClass_checkConfig: The following device dpes have a wrong format: " + dsWrongDpeFormats + ".","");	
  }
  
  if (dynlen(dsWrongDpeNames)) {
    fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpClass_checkConfig: The following device dpes don't exist: " + dsWrongDpeNames + ".","");		
  }
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_UnRcpClass_setConfig
/** set the UnRcpClass device configuration for the SOFT_FE front-end
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
SOFT_FE_UnRcpClass_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
	int iPos;
	string sDpName 	   = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
	string sClassName  = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS];	
	string sDesc	   = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION];
	string sTypeName   = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPCLASS_RECIPE_TYPE];
	string sDeviceList = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPCLASS_DEVICE_LIST];
    string sDeviceLink = "";
	string sAppName;
	bool bEditable 	  = false;
	
	if ((iPos=strpos(sClassName, ","))>=0) {
		sDeviceLink = substr(sClassName, iPos+1);
		sClassName = substr(sClassName, 0, iPos);
	}
	
	dpSet(sDpName+".ProcessInput.ClassName", sClassName);
	dpSet(sDpName+".ProcessInput.RecipeType", sTypeName);
	
    if (dynlen(dsConfigs) == (UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPCLASS_MAX_LENGHT)) {
        // The device order is defined
        string sDeviceOrder = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPCLASS_MAX_LENGHT];
        dyn_string dsDeviceOrder = strsplit(sDeviceOrder, "|");
        for (int i=1; i<=dynlen(dsDeviceOrder); i++) {
            dsDeviceOrder[i] = strltrim(strrtrim(dsDeviceOrder[i]));
        }
        dpSet(sDpName+".ProcessInput.DeviceOrder", dsDeviceOrder);
    }
    
	dyn_string deviceDpes = strsplit(sDeviceList, ",");
	
	// Adds the 'ProcessOutput' to the device dpes
	int len = dynlen(deviceDpes);
	for (int i=1; i<=len; i++) {
		dyn_string deviceSplit = strsplit(deviceDpes[i], ".");
		string alias = deviceSplit[1];
		string dpe   = deviceSplit[2];
		deviceDpes[i] = alias + ".ProcessOutput." + dpe;
	}
	
	if (!dpExists(fwConfigurationDB_RecipeClassDpPrefix+sClassName)){
		// Create the JCOP Recipe class DP
		fwConfigurationDB_createRecipeClass(sClassName, sTypeName, sDesc, bEditable, deviceDpes, exceptionInfo);
	} else {
		// Updates the JCOP Recipe class DP
		fwConfigurationDB_modifyRecipeClass(sClassName, deviceDpes, sTypeName, sDesc, exceptionInfo);
	}
	
	if (dynlen(exceptionInfo)) {
		if (dpExists(sDpName)) {
			dpDelete(sDpName);
		}
		return;
	}
	
	unRecipeFunctions_updateRecipeClassPrivileges(sDpName);
	
	// Create a predefined recipe instance of the class to store the configuration data (AC. Domain and privileges)
	sAppName = unGenericDpFunctions_getApplication(sDpName);
	_unRecipeFunctions_createPredefinedRecipeInstance(sClassName, sDeviceLink, sAppName, exceptionInfo);
	
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Performs additional processing for removing a recipe class
 * @param sDevice - [IN] Datapoint name of a recipe class
 * @return
 *
 * @reviewed 2018-06-25 @whitelisted{UNICOSGenericDPFunctions}
 */
dyn_string UnRcpClass_processDeviceToDelete(string sDevice) {
    dyn_string dsRecipes, exceptionInfo;
    string sClassName;
    
    if (dpExists(sDevice + ".")) {
        dpGet(sDevice + ".ProcessInput.ClassName", sClassName);
        fwConfigurationDB_deleteRecipeClass(sClassName, TRUE, exceptionInfo);
        
        if (dpExists(fwConfigurationDB_RecipeClassDpPrefix + sClassName)) {
            // The recipe class could not be removed, force its deletion
            dpGet(fwConfigurationDB_RecipeClassDpPrefix + sClassName + ".Recipes", dsRecipes);
            for (int i=1; i<=dynlen(dsRecipes); i++){
                if (dpExists(fwConfigurationDB_RecipeCacheDpPrefix + dsRecipes[i])) {
                    dpDelete(fwConfigurationDB_RecipeCacheDpPrefix + dsRecipes[i]);
                }
            }
            
            dpDelete(fwConfigurationDB_RecipeClassDpPrefix + sClassName);
        }
    }
    
    return makeDynString();
}
//------------------------------------------------------------------------------------------------------------------------

//@}
