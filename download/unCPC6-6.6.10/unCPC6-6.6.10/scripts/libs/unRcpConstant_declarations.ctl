/**
 * UNICOS
 * Copyright (C) CERN 2013 All rights reserved
 */
/**@file

// unRcpConstant_declarations.ctl
This file contains the constant definitions for the UNICOS Recipes.

@par Creation Date
  27/05/2011

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (EN-ICE)
*/

//@{

// Recipe Constant Definitions
const string UN_CONFIG_UNRCPCLASS_DPT_NAME 			   = "UnRcpClass";
const string UN_CONFIG_UNRCPTYPE_DPT_NAME  			   = "UnRcpType";
const string UN_CONFIG_UNRCPINSTANCE_DPT_NAME 		   = "UnRcpInstance";
const string UN_CONFIG_INTERNAL_UNRCPCLASS_DPT_NAME    = "_UnRcpClass";
const string UN_CONFIG_INTERNAL_UNRCPINSTANCE_DPT_NAME = "_UnRcpInstance";
const string UN_RECIPE_FE_ALIAS                        = "RCP_FE";

const unsigned UN_CONFIG_UNRCPCLASS_MIN_LENGHT   = 2;  // Min length of the recipe class definition; before the device order was introduced 
const unsigned UN_CONFIG_UNRCPCLASS_MAX_LENGHT   = 3;  // Max length of the recipe class definition; after the device order was introduced
const unsigned UN_CONFIG_UNRCPCLASS_RECIPE_TYPE  = 1;  // Recipe class type
const unsigned UN_CONFIG_UNRCPCLASS_DEVICE_LIST  = 2;  // Recipe class device list
const unsigned UN_CONFIG_UNRCPCLASS_DEVICE_ORDER = 3;  // Device order

const unsigned UN_CONFIG_UNRCPINSTANCE_LENGHT = 3;
const unsigned UN_CONFIG_UNRCPINSTANCE_RECIPE_CLASS = 1;	// Recipe class of the recipe instance
const unsigned UN_CONFIG_UNRCPINSTANCE_INITIAL_RECIPE = 2;	// Is an initial recipe?
const unsigned UN_CONFIG_UNRCPINSTANCE_VALUES_LIST = 3;		// Values list

// constant for export

// constant for widget

// constant for faceplate

// constant for device action interface

// Recipe instance states
const string UN_RCP_INSTANCE_STATE_ACTIVATE  	  = "Activate";
const string UN_RCP_INSTANCE_STATE_EDIT		 	  = "Edit";
const string UN_RCP_INSTANCE_STATE_DELETE	 	  = "Delete";
const string UN_RCP_INSTANCE_STATE_INVALID	 	  = "Invalid";
const string UN_RCP_INSTANCE_STATE_DUPLICATE 	  = "Duplicate";
const string UN_RCP_INSTANCE_STATE_CANCELLED 	  = "Cancelled";
const string UN_RCP_INSTANCE_STATE_UNLOCK_BUFFERS = "Unlocking Buffers";
const string UN_RCP_INSTANCE_STATE_LOADING_VALUES = "Loading Values";
const string UN_RCP_INSTANCE_STATE_LOCKED = "Locked";

// Predefined recipe instance to store the configuration data (AC Domain and privileges)
const string UN_RCP_PREDEFINED_NAME 		  = "__Predefined__";
const string UN_RCP_PREDEFINED_RCPCLASS_ALIAS = "_UnRcpClass__Predefined__";
const string UN_RCP_PREDEFINED_RCPCLASS_DPE   = "un-/-/-_UnRcpClass-00000";


// Recipe class states
const string UN_RCP_CLASS_STATE_EDIT		                = "Edit";
const string UN_RCP_CLASS_STATE_INVALID	 	                = "Invalid";
const string UN_RCP_CLASS_STATE_INVALID_EDIT                = "Invalid Edit";
const string UN_RCP_CLASS_STATE_APPLY_CHANGES               = "Apply Changes";
const string UN_RCP_ACTION_ACTIVATE_INITIAL_RECIPES 		= "ActivateInitialRecipes";
const string UN_RCP_ACTION_ACTIVATE_LAST_ACTIVATED_RECIPES 	= "ActivateLastActivatedRecipes";
const string UN_RCP_SAVE_TO_DB_STATE                        = "Save to DB";
const string UN_RCP_WARNING_MESSAGE_CLASS_MODIFIED          = "Warning! The recipe class has been modified. Please reload the recipe to avoid inconsistencies.";
const string UN_RCP_WARNING_MESSAGE_VALUES_MODIFIED         = "Warning! The recipe values have been modified. Please reload the recipe to avoid inconsistencies.";


const int unRecipeFunctions_ERROR_NoRecipeClass=-1;
const int unRecipeFunctions_ERROR_InvalidElements=-2;
const int unRecipeFunctions_ERROR_NoRecipeInstance=-3;


/**************************************************************************
 *      Constants used as return value while getting the initial recipes  *
 **************************************************************************/
const int RCP_INITIAL_OK		 =  0;	// The initial recipe of the class has been found
const int RCP_INITIAL_NO_DEFINED = -1;	// There is no initial recipe for the recipe class
const int RCP_INITIAL_TOO_MANY	 = -2;	// There are several initial recipes for the class
const int RCP_INITIAL_UNDEFINED	 = -3;	// Other error while getting the initial recipe			


/************************************
 *      Constants to define colors  *
 ************************************/
const string CELL_EDIT_COLOR 			   = "{204,255,153}";
const string CELL_VALUE_OUT_OF_RANGE_COLOR = "{255,64,64}";
const string CELL_ERROR_COLOR              = "{255,0,0}";
const string CELL_BG_COLOR                 = "{255,255,255}";
const string INACTIVE_RCP_COLOR            = "{251,255,72}";
const string ACTIVE_RCP_COLOR              = "{0,245,0}";



/********************************************************* 
 *  Indexes for the elements in the recipe header buffer *
 *********************************************************/
const int RCP_HEADER_MAX_SIZE	= 15;
const int RCP_HEADER_ID_LOW		= 1;
const int RCP_HEADER_ID_HIGH	= 2;
const int RCP_HEADER_NUM_MANREG	= 3;
const int RCP_HEADER_NUM_DPAR	= 4;
const int RCP_HEADER_DB_DPAR	= 5;
const int RCP_HEADER_NUM_WPAR	= 6;
const int RCP_HEADER_DB_WPAR	= 7;
const int RCP_HEADER_NUM_APAR	= 8;
const int RCP_HEADER_DB_APAR	= 9;
const int RCP_HEADER_NUM_AALARM	= 10;
const int RCP_HEADER_DB_AALARM	= 11;
const int RCP_HEADER_NUM_CONTR	= 12;
const int RCP_HEADER_DB_CONTR	= 13;
const int RCP_HEADER_SENT_VALUE	= 14;
const int RCP_HEADER_CRC		= 15;


/********************************************************* 
 *  Indexes for the elements in the recipe status buffer *
 *********************************************************/
const int RCP_STATUS_MAX_SIZE			= 16;
const int RCP_STATUS_MIRROR_DATA_SIZE	= 13;	// Indicates the last position where the data in the status buffer is a copy
												// of the data in the header buffer
const int RCP_STATUS_ID_LOW				= 1;
const int RCP_STATUS_ID_HIGH			= 2;
const int RCP_STATUS_NUM_MANREG			= 3;
const int RCP_STATUS_NUM_DPAR			= 4;
const int RCP_STATUS_DB_DPAR			= 5;
const int RCP_STATUS_NUM_WPAR			= 6;
const int RCP_STATUS_DB_WPAR			= 7;
const int RCP_STATUS_NUM_APAR			= 8;
const int RCP_STATUS_DB_APAR			= 9;
const int RCP_STATUS_NUM_AALARM			= 10;
const int RCP_STATUS_DB_AALARM			= 11;
const int RCP_STATUS_NUM_CONTR			= 12;
const int RCP_STATUS_DB_CONTR			= 13;
const int RCP_STATUS_STATUS				= 14;
const int RCP_STATUS_CRC				= 15;
const int RCP_STATUS_ERR				= 16;


/********************************************************* 
 *                 Recipe Status                         *
 *********************************************************/
const int RCP_ACTIVATION_TRIGGERED  = 0; // The recipe activation has been triggered
const int RCP_ACTIVATED             = 1; // The recipe is activated in the PLCs
const int RCP_NO_PLC_CONNECTION     = 2; // There is no connection with one or more PLCs
const int RCP_INACTIVE              = 3; // The recipe is inactive
const int RCP_BAD_TYPES             = 4; // The value type is different than the DPE type
const int RCP_BAD_STRUCTURE         = 5; // The number of values is different than the number of DPEs
const int RCP_BAD_DPES              = 6; // The recipe has some invalid DPEs
const int RCP_ACTIVATION_FAILED     = 7; // The recipe activation failed


/********************************************************* 
 *                 Recipe Activation Status              *
 *    (values received in the recipe status buffer)      *
 *********************************************************/
const int RCP_ACT_STATUS_ERROR      	   = -1;  // There was an error during the recipe activation
const int RCP_ACT_STATUS_STANDBY           = 0;   // Recipe activation in standby
const int RCP_ACT_STATUS_NEW_ID_RECEIVED   = 1;   // New recipe identified received
const int RCP_ACT_STATUS_CRC_OK			   = 2;
const int RCP_ACT_STATUS_DONE              = 3;   // Recipe activation done
                                  

/********************************************************* 
 *      Constants used in the recipe activation          *
 *********************************************************/
// Recipe header 'Sent Value'
const int RCP_ACT_HEADER_CANCEL			= -1;	// Cancel the recipe activation
const int RCP_ACT_HEADER_SENDING_DATA	= 1;	// Sending recipe data to the PLC
const int RCP_ACT_HEADER_DATA_SENT		= 2;	// All data sent to the PLC
const int RCP_ACT_HEADER_ACTIVATE		= 3;	// Recipe activation order 
const int RCP_ACT_HEADER_ACTIVATION_OK	= 4;  	// Recipe activation Ok
			

/**************************************************************************
 *      Constants used as return value for the recipe activation          *
 **************************************************************************/
const int RCP_ACT_RETVAL_ACTIVATION_OK		 =  0;	// The recipe activation has been completed successfully
const int RCP_ACT_RETVAL_BAD_RECIPE_DATA	 = -1;	// The recipe data is wrong (recipe doesn't exist, DPs and values don't match, ...)
const int RCP_ACT_RETVAL_BAD_PLC_STATUS		 = -2;	// The PLC status is wrong
const int RCP_ACT_RETVAL_BAD_PLC_TYPE		 = -3;	// The PLC Type is unknown
const int RCP_ACT_RETVAL_PLC_BUSY			 = -4;	// The PLC is busy activating another recipe
const int RCP_ACT_RETVAL_ACTIVATION_TIMEOUT  = -5;	// The recipe activation timeout has expired
const int RCP_ACT_RETVAL_PLC_CANCELLATION	 = -6;	// The PLC has cancelled the recipe activation
const int RCP_ACT_RETVAL_ACTIVATION_ERROR	 = -7;	// The PLC has cancelled the recipe activation
const int RCP_ACT_RETVAL_BAD_RECIPE_STATUS   = -8;  // The recipe status is wrong
const int RCP_ACT_RETVAL_WRONG_ONLINE_VALUES = -9;  // The recipe online values are wrong


/*******************************************************************************************
 * Constants used as return value when getting the recipe elements for a PLC               *
 *******************************************************************************************/
const int WRONG_NUMBER_OF_DPE_AND_VALUE = -100;
const int RECIPE_BUFFERS_DONT_EXIST     = -101;
const int BUFFER_OVERFLOW               = -102;
const int INVALID_RECIPE_DPE            = -103;
const int ERROR_LOADING_RECIPE          = -104;
const int VALUE_OUT_OF_RANGE            = -105;




/****************************** 
 *    ManReg Bit positions    *
 *****************************/
// DPAR bit definitions
const int DPAR_ARM_BIT		= 2;
const int DPAR_MONR_BIT 	= 4;
const int DPAR_MOFFR_BIT	= 5;
// WPAR bit definitions
const int WPAR_ARM_BIT		= 2;
const int WPAR_NEWMR_BIT	= 6;
// APAR bit definitions
const int APAR_ARM_BIT		= 2;
const int APAR_NEWMR_BIT	= 6;
// AALARM bit definitions
const int AALARM_ARM_BIT	= 2;
// PID bit definitions
const int PID_ARM_BIT		= 4;


// Index constants for the recipe activation info matrix.
const int PLCS_REQUIRED_FOR_ACTIVATION_INDEX	= 1;    // The index contains the list of PLCs required for the recipe activation.
const int PLCS_READY_TO_ACTIVATE_INDEX 			= 2;	// The index contains the list of PLCs that are ready to activate the recipe.
const int PLCS_ACTIVATED_RECIPE_INDEX			= 3;	// The index contains the list of PLCs where the recipe has been activated.
const int LOCAL_MANAGER_INDEX                   = 4;    // The index contains the local manager that initiated the recipe activation.


//@}

//@{

// put here the global variables

//@}

