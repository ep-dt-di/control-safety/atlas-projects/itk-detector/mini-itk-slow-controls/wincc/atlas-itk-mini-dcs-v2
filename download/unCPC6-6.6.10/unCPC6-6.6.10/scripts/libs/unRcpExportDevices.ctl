/**
 * UNICOS
 * Copyright (C) CERN 2013 All rights reserved
 */
/**@file

// unRcpExportDevices.ctl
This library contains the export functions of the package.

@par Creation Date
  27/05/2011

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (EN-ICE)
*/

#uses "fwGeneral/fwException.ctl"
#uses "fwGeneral/fwGeneral.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "unExportDevice.ctl"
#uses "unicos_declarations.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unRcpFunctions.ctl"

//@{

//------------------------------------------------------------------------------------------------------------------------
// unRcpExportDevice_writeDeviceTypeLineFormat
/** returns the format of the config line for a device type
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceType input, device type
@param sCommand input, keyword (for front-end only)
*/
unRcpExportDevice_writeDeviceTypeLineFormat(string sDeviceType, string sCommand)
{
  string sFormat, sMandatory, sDeviceConfig;
  
  sMandatory = sDeviceType+";Device number (unique);Device name (Device Identifier);description;Diagnostic panel;HTML page;Default panel;subsystem1 (Domain);subsystem2 (Nature);Widget Name";
  switch(sDeviceType) 
  {
    case UN_CONFIG_UNRCPCLASS_DPT_NAME:
      sDeviceConfig = "RecipeType;DeviceList;DeviceOrder;" ;
      sFormat = sMandatory+";"+sDeviceConfig;
      fputs("#format: " +sFormat + "\n" , g_fileToExport);
      break;
	case UN_CONFIG_UNRCPINSTANCE_DPT_NAME:
      sDeviceConfig = "RecipeClass;InitialRecipe;RecipeValues;";
      sFormat = sMandatory+";"+sDeviceConfig;
      fputs("#format: " +sFormat + "\n" , g_fileToExport);
      break;	
    default:
      fputs("#no format: \n" , g_fileToExport);
      break;
  }
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_UnRcpClass_ExportConfig
/** returns the format of the config line for a device type
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDpList input, list of device dp
@param exceptionInfo output, the errors are returned here
*/
void SOFT_FE_UnRcpClass_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
 	int rc, i, iLeni, iLenj;
	string sClassName, sDeviceType, sRecipeType, sDeviceList, sDeviceOrder;
	dyn_mixed recipeClassInfo;
	dyn_string dsAllCommonParameters, dsDpParameters;
	
	sDeviceType=UN_CONFIG_UNRCPCLASS_DPT_NAME;
  
	iLeni=dynlen(dsDpList);
	if(iLeni > 0) {
		unRcpExportDevice_writeDeviceTypeLineFormat(sDeviceType, "");
	}

	for(i=1; i<=iLeni; i++)	{                  
		if (dpExists(dsDpList[i])) {
		    dynClear(dsAllCommonParameters);
			dynClear(dsDpParameters);

			//Get all common Parameters
			unExportDevice_getAllCommonParameters(dsDpList[i], sDeviceType, dsAllCommonParameters);
			dsDpParameters = dsAllCommonParameters;

			// Get the recipe class data
			rc = dpGet(dsDpList[i] + ".ProcessInput.ClassName", sClassName,
					   dsDpList[i] + ".ProcessInput.RecipeType", sRecipeType,
                       dsDpList[i] + ".ProcessInput.DeviceOrder", sDeviceOrder);
			
			if (rc) {
				fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpClass_ExportConfig: Error getting the recipe class data from " + dsDpList[i], "");
				continue;	  
			}
	  
			// Get the recipe class 
   unRecipeFunctions_getRecipeClassObject(dsDpList[i], recipeClassInfo, exceptionInfo);
			if (dynlen(exceptionInfo)) {
				fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpClass_ExportConfig: Error getting the recipe class data of: " + sClassName, "");
				continue;	  			
			}
			
            dyn_string dsDeviceList;
			iLenj = dynlen(recipeClassInfo[fwConfigurationDB_RCL_ELEMENTS]);
			for (int j=1; j<=iLenj; j++) {
				string sTmp = recipeClassInfo[fwConfigurationDB_RCL_ELEMENTS][j];
				strreplace(sTmp, ".ProcessOutput.", ".");
                dynAppend(dsDeviceList, sTmp);
			}
			fwGeneral_dynStringToString(dsDeviceList, sDeviceList, ",");
			// Get the recipe type and devices list
			dynAppend(dsDpParameters, sRecipeType); 
			dynAppend(dsDpParameters, sDeviceList); 
            dynAppend(dsDpParameters, sDeviceOrder);
			
			unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_UnRcpInstance_ExportConfig
/** returns the format of the config line for a device type
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDpList input, list of device dp
@param exceptionInfo output, the errors are returned here
*/
void SOFT_FE_UnRcpInstance_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
	int rc, i, iLeni, iLenj, pos;
	string sClassName, sInstanceName, sDeviceType, sDeviceValueList;
	dyn_dyn_mixed recipeObject;
	dyn_string dsAllCommonParameters, dsDpParameters;
	
	sDeviceType=UN_CONFIG_UNRCPINSTANCE_DPT_NAME;
  
	iLeni=dynlen(dsDpList);
	if(iLeni > 0) {
		unRcpExportDevice_writeDeviceTypeLineFormat(sDeviceType, "");
	}

	for(i=1; i<=iLeni; i++)	{                  
		if (dpExists(dsDpList[i])) {
		    dynClear(dsAllCommonParameters);
			dynClear(dsDpParameters);
			dynClear(recipeObject);

			//Get all common Parameters
			unExportDevice_getAllCommonParameters(dsDpList[i], sDeviceType, dsAllCommonParameters);
			dsDpParameters = dsAllCommonParameters;

			// Get the recipe instance data
			rc = dpGet(dsDpList[i] + ".ProcessInput.ClassName", sClassName,
					   dsDpList[i] + ".ProcessInput.InstanceName", sInstanceName);
			
			if (rc) {
				fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpInstance_ExportConfig: Error getting the recipe instance data from " + dsDpList[i], "");
				continue;	  
			}
	  
			// Get the recipe instance from JCOP
			fwConfigurationDB_loadRecipeFromCache(sClassName+"/"+sInstanceName, makeDynString(), "", recipeObject, exceptionInfo);
			if (dynlen(exceptionInfo)) {
				fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpClass_ExportConfig: Error getting the recipe instance data of: " + sClassName, "");
				continue;	  			
			}
			
			iLenj = dynlen(recipeObject[fwConfigurationDB_RO_DPE_NAME]);
            dyn_string dsDeviceValue;
			for (int j=1; j<=iLenj; j++) {
                string sDevice = recipeObject[fwConfigurationDB_RO_DP_NAME][j] + recipeObject[fwConfigurationDB_RO_ELEMENT_NAME][j];
                strreplace(sDevice, ".ProcessOutput.", ".");
				// TODO: removes the distributed system name (temporary solution!!! find a solution for the recipes 
				// that use devices of several systems
				if ((pos=strpos(sDevice, ":"))>=0) {
					sDevice = substr(sDevice, pos+1);
				}
				
                dynAppend(dsDeviceValue, sDevice + "|" + recipeObject[fwConfigurationDB_RO_VALUE][j]);
			}
			
            fwGeneral_dynStringToString(dsDeviceValue, sDeviceValueList, ",");
			// Get the recipe type and devices list
			dynAppend(dsDpParameters, sClassName); 
			dynAppend(dsDpParameters, recipeObject[fwConfigurationDB_RO_META_PREDEFINED][1]); 
			dynAppend(dsDpParameters, sDeviceValueList); 
			
			unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------

//@}
