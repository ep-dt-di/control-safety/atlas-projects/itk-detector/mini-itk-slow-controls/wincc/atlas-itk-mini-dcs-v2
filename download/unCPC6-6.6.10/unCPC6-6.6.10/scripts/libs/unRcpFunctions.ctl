/**
 * UNICOS
 * Copyright (C) CERN 2014 All rights reserved
 */
/**@file

@name LIBRARY: unRecipeFunctions.ctl

@author: Ivan Prieto Barreiro (EN-ICE-SIC)

Creation Date: April 2011 

version 1.4.0

Modification History:
  01/11/2011: Ivan
    - v1.1.0: The device type names have been modified to use the "CPC_" prefix.
	
  01/08/2012: Ivan
	- v1.2.0 : Adding new functionalities to the component:
		- Dynamic creation of recipe classes.
		- Recipe activation in multiple PLCs.

  09/12/2013: Ivan
	- v1.3.4 : Bug fixing and improvements in existing functionalities.
    
  09/09/2014: Ivan
    - v1.4.0 : Bug fixing and improvements in existing functionalities.
		
External Functions: 
  .  
  
Internal Functions:
  .
*/
#uses "cpcGenericObject.ctl"
#uses "CtrlTreeDeviceOverview"
#uses "fwAccessControl/fwAccessControl.ctc"
#uses "fwAccessControl/fwAccessControl_Egroups.ctc"
#uses "fwGeneral/fwException.ctl"
#uses "fwGeneral/fwGeneral.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Setup.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unGenericObject.ctl"
#uses "unExportDevice.ctl"
#uses "unSystemIntegrity_unicosPLC.ctl"
#uses "unSystemIntegrity.ctl"
#uses "unicos_declarations.ctl"
#uses "libunCore/unApplicationFilter.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unRcpFunctions_utilities.ctl"
#uses "unRcpFunctions_gui.ctl"
#uses "unRcpFunctions_dpe.ctl"
#uses "cpcObjects/cpcControllerConfig.ctl"
#uses "unRcpDeprecated.ctl"

//@{

/****************************** 
 *      Global   Variables    *
 *****************************/
// Map containing the selected recipe in each opened faceplace
// Key: $sDpName of the faceplate
// Value: Dp name of the selected recipe in the faceplate
global mapping unRecipeFaceplate_selectedRecipe;
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Writes a report on the log window with the different values from the recipe and the PLC online values.
 * @param diDiffRowIndex - [IN] Indexes of rows where there are different values
 * @param dsDpNames      - [IN] Datapoint names of the recipe elements
 * @param dsAlias        - [IN] Alias of the recipe elements
 * @param daRcpValues    - [IN] Recipe values
 * @param daOnlineValues - [IN] PLC online values
 */
void unRecipeFunctions_writeDifferentOnlineValuesReport(dyn_int diDiffRowIndex, dyn_string dsDpNames, 
            dyn_string dsAlias, dyn_anytype daRcpValues, dyn_anytype daOnlineValues) {

  int iLen = dynlen(diDiffRowIndex);
  if (iLen>0) {
    string sLogMessages = "Recipe values and loaded values are different:";
    for (int i=1; i<=iLen; i++) {
      string sDpe = dsDpNames[(diDiffRowIndex[i])];
      dyn_string sDpeSplit = strsplit(sDpe, ".");
      string sAlias = dsAlias[(diDiffRowIndex[i])] + "." + sDpeSplit[(dynlen(sDpeSplit))];
      sLogMessages += "\nDifferent values on row index " + diDiffRowIndex[i] 
                   + ". Device: " + sAlias + " => Recipe Value: " + daRcpValues[(diDiffRowIndex[i])] 
                   + ", Loaded Value: " + daOnlineValues[(diDiffRowIndex[i])];
    }
    unRecipeFunctions_writeInRecipeLog(sLogMessages, false, true);
  } else {
    unRecipeFunctions_writeInRecipeLog("Recipe values and online values are equal.", false, true);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Compares the value lists daValues1 and daValues2 and set:
 *  - Row indexes (diRowStyleIndex).
 *  - Row styles (diRowStyles): normal (0) if the values are the same, bold (1) if the values are different.
 *  - Index of rows with different values (diDiffRowIndex).
 * @param daValues1       - [IN] First list of values to compare.
 * @param daValues2       - [IN] Second list of values to compare.
 * @param diRowStyleIndex - [OUT] Row index for the styles.
 * @param diRowStyles     - [OUT] Styles for the table rows.
 * @param diDiffRowIndex  - [OUT] Index of the rows with different values.
 * @return FALSE if the length of the lists is different, otherwise TRUE.
 */
bool _unRecipeFunctions_getRowStylesForValueComparison(dyn_anytype daValues1, dyn_anytype daValues2, dyn_int &diRowStyleIndex, dyn_int &diRowStyles, dyn_int &diDiffRowIndex) 
{
  if (dynlen(daValues1) != dynlen(daValues2) ) {
    return false;
  }
  
  int iLen = dynlen(daValues1);
  for (int i=1; i<=iLen; i++) {
    dynAppend(diRowStyleIndex, (i-1));
    if (daValues1[i] != daValues2[i]) {
      dynAppend(diRowStyles, 1);
      dynAppend(diDiffRowIndex, i);
    } else {
      dynAppend(diRowStyles, 0);
    }
  }

  return true;
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the online values of a recipe instance.
 * @param dsDpNames 	- [IN] DP Names of the elements which online values are required.
 * @param values 		- [OUT] Online formatted values corresponding to the recipe class DPEs.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */ 
void unRecipeFunctions_getOnlineValues(dyn_string dsDpNames, dyn_anytype &values, dyn_string &exceptionInfo)
{
  dyn_string outputDpNames;
  dyn_anytype formattedValues;

  dynClear(values);
  
   // Get recipe dpes
  unRecipeFunctions_getOnlineValueDpes(dsDpNames, outputDpNames, exceptionInfo);
  dpGet(outputDpNames, values);
  unRecipeFunctions_formatValues(dsDpNames, values, formattedValues, exceptionInfo);  
  
  values = formattedValues;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the DPEs associated to each device type in a recipe type.
 * @param sRecipeType - [IN] Recipe type name.
 * @param map - [OUT] Mapping variable where Key=Device type name, Value=List containing the DPEs 
 *              included in the recipe type for the specified key (device type).
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void _unRecipeFunctions_getTypeMapping(string sRecipeType, mapping &map, dyn_string &exceptionInfo)
{
  dyn_dyn_string rtData;
  _fwConfigurationDB_getRecipeTypeData(sRecipeType,rtData,exceptionInfo);
    
  if (dynlen(exceptionInfo)) {
    return;
  }
   
  int len = dynlen(rtData);
  for (int i=1; i<=len; i++){
    if (strtoupper(rtData[i][5])=="TRUE") {
      string deviceType = rtData[i][2];
      dyn_string items;
      if (mappingHasKey(map,deviceType)){
        items = map[deviceType];
      }
      dyn_string splitElement = strsplit(rtData[i][3], ".");
      dynAppend(items, splitElement[dynlen(splitElement)]);
      map[deviceType] = items;
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------
/** 
 * Try to lock the devices specified.
 * @param dsDpNames           - [IN] DP names of the devices to lock.
 * @param dsLockedDeviceDp    - [OUT] DP names of the devices that are already locked.
 * @param dsLockedDeviceAlias - [OUT] Alias of the devices that are already locked.
 * @param exceptionInfo       - [OUT] Standard exception handling variable.
 * @return TRUE if the devices were locked, otherwise FALSE.
 */
synchronized bool _unRecipeFunctions_lockDevices(dyn_string dsDpNames, dyn_string &dsLockedDeviceDp, 
    dyn_string &dsLockedDeviceAlias, dyn_string &exceptionInfo) 
{
  int iLen, iErr;
  dyn_string dsIsLockedDp, dsIsLockedValue, dsLockedByDp, dsLockRequestValue;
  dyn_int diLockedByValue;
  
  iLen = dynlen(dsDpNames);
  if (iLen == 0) {
    fwException_raise(exceptionInfo, "ERROR", "The list of devices to lock is empty.", "");
    return false;
  }
  
  for (int i=1; i<=iLen; i++) {
    dynAppend(dsIsLockedDp, dsDpNames[i] + ":_lock._original._locked");
    dynAppend(dsLockedByDp, dsDpNames[i] + ":_lock._original._man_nr");
    dynAppend(dsLockRequestValue, "true");
  }
  
  // Get the 'locked' value of the devices
  iErr = dpGet(dsIsLockedDp, dsIsLockedValue);
  if (unRecipeFunctions_handleDpGetSetError(iErr, exceptionInfo)) {
	return false;
  }
  
  // Check if all the devices are unlocked
  for (int i=1; i<=iLen; i++) {
	if (dsIsLockedValue[i] == true) {
      dynAppend(dsLockedDeviceDp, dsDpNames[i]);
	  dynAppend(dsLockedDeviceAlias, dpGetAlias(dsDpNames[i]));
	}
  }
  
  // If some devices are locked return
  if (dynlen(dsLockedDeviceDp)) {
	return false;
  }
  
  // Try to lock all the required devices
  iErr = dpSetWait(dsIsLockedDp, dsLockRequestValue);
  if (unRecipeFunctions_handleDpGetSetError(iErr, exceptionInfo)) {
	return false;
  }
  
  // Get the 'locked' value of the devices
  iErr = dpGet(dsIsLockedDp, dsIsLockedValue,
			   dsLockedByDp, diLockedByValue);
  if (unRecipeFunctions_handleDpGetSetError(iErr, exceptionInfo)) {
	return false;
  }
  
  // Check if all the devices are locked by the current manager
  for (int i=1; i<=iLen; i++) {
	if (dsIsLockedValue[i] == false || diLockedByValue[i] != myManNum()) {
	  dynAppend(dsLockedDeviceDp, dsDpNames[i]);
      dynAppend(dsLockedDeviceAlias, dpGetAlias(dsDpNames[i]));
	} else {
      unRecipeFunctions_writeInRecipeLog("Device locked: " + dpGetAlias(dsDpNames[i]), true);
    }
  }
  
  // If the lock failed in any device, unlock all the devices
  if (dynlen(dsLockedDeviceDp)) {
    string sAliasLockError;
    fwGeneral_dynStringToString(dsLockedDeviceAlias, sAliasLockError, ", ");
    unRecipeFunctions_writeInRecipeLog("The following devices could not be locked: " + sAliasLockError, true);
    unRecipeFunctions_writeInRecipeLog("Unlocking devices ..", true);
    _unRecipeFunctions_unlockDevices(dsDpNames, exceptionInfo);
    return false;
  }
    
  return true;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Try to unlock the devices specified.
 * @param dsDpNames           - [IN] DP names of the devices to unlock.
 * @param exceptionInfo       - [OUT] Standard exception handling variable.
 * @return TRUE if the devices were unlocked, otherwise FALSE.
 */
synchronized bool _unRecipeFunctions_unlockDevices(dyn_string dsDpNames, dyn_string &exceptionInfo) {
  int iErr;
  int iManNum = myManNum();
  dyn_string dsLockedDpName, dsLockedByDpName, dsLockedValue, dsAlias;
  dyn_int diLockedByValue;

  dynClear(exceptionInfo);
  
  int iLen = dynlen(dsDpNames);
  if (iLen == 0) {
    fwException_raise(exceptionInfo, "ERROR", "The list of devices to lock is empty.", "");
    return false;
  }
  
  for (int i=1; i<=iLen; i++) {
    dynAppend(dsAlias, dpGetAlias(dsDpNames[i]));
	dynAppend(dsLockedDpName,   dsDpNames[i] + ":_lock._original._locked");
	dynAppend(dsLockedByDpName, dsDpNames[i] + ":_lock._original._man_nr");
  }
  
  // Get the 'locked' state of the devices and the manager number that locked them
  iErr = dpGet(dsLockedDpName, dsLockedValue,
			   dsLockedByDpName, diLockedByValue);			   
  if (unRecipeFunctions_handleDpGetSetError(iErr, exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Failure while getting the lock state of the devices: '" + dsAlias + "'", true);
    unRecipeFunctions_writeInRecipeLog("The devices cannot be unlocked", true);    
    return false;
  }
  
  for (int i=1; i<=iLen; i++) {
    // Check if the device is locked by the current manager
    if (dsLockedValue[i] == true) {
      if (diLockedByValue[i] == iManNum) {
        // Release the device
	    iErr = dpSetWait(dsLockedDpName[i], 0);
        if (unRecipeFunctions_handleDpGetSetError(iErr, exceptionInfo)) {
          unRecipeFunctions_writeInRecipeLog("Failure while unlocking the device: " + dsAlias[i], true);
        } else {
          unRecipeFunctions_writeInRecipeLog("Device unlocked: " + dsAlias[i] , true);
        }
      } else {
        unRecipeFunctions_writeInRecipeLog("The device '" + dsAlias[i]
          + "' is locked by a different manager (" + diLockedByValue[i] + ") and it cannot be unlocked", true);
      }        
    }
  }
  
  return (dynlen(exceptionInfo) == 0);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Gets the formatted values of a set of DPEs.
 * @param dsDpNames       - [IN] List of dp names in the UNICOS format.
 * @param values          - [IN] List of values for the specified dpNames.
 * @param formattedValues - [OUT] Formatted values.
 * @param exceptionInfo   - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_formatValues(dyn_string dsDpNames, dyn_string values, 
                                     dyn_anytype &formattedValues, dyn_string &exceptionInfo)
{
  string rangeElement, deviceType, rangeMin, rangeMax, pvssDpName;
  dyn_string dsOnlineDpes;
  float fValue;
  bool bOk;
  
  if (dynlen(dsDpNames)!=dynlen(values)) {
    fwException_raise(exceptionInfo, "ERROR", "The number of DPEs is different than the number of values!", "");
    return;
  }  
  
  for (int i=1; i<=dynlen(dsDpNames); i++) {  
    dynClear(exceptionInfo);
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(dsDpNames[i], pvssDpName);  
    // Get the DPE name (e.g. .ProcessOutput.MPosR")
    string dp=unGenericDpFunctions_getDpName(pvssDpName);
    int pos = strpos(pvssDpName, dp);
    string element = substr(pvssDpName, pos+strlen(dp), strlen(pvssDpName)-strlen(dp));  

    if (!dpExists(pvssDpName)) {
      formattedValues[i] = values[i];
      continue;
    }
    
    deviceType = dpTypeName(pvssDpName);

	// Removes the white spaces to format properly the exponential values (1.5 e1 => 1.5e1)
	strreplace(values[i], " ", "");
	if (deviceType=="CPC_DigitalParameter") {	
		if (values[i]==0) {
			formattedValues[i] = "FALSE";
			continue;
		}
		else if (values[i]==1) {
			formattedValues[i] = "TRUE";
			continue;
		}
			
		string value = strtoupper(values[i]);
		if (value=="TRUE" || value=="FALSE") {
			formattedValues[i] = value;
		} 
		continue;
	}
		
    // Get the dpe where the range is defined for the current recipe element
    if (!mappingHasKey(unRecipe_rangeElements, deviceType+":"+element)){
      unRecipeFunctions_getRangeElementFromPVSSDPE(pvssDpName, rangeElement, exceptionInfo);  
      unRecipe_rangeElements[deviceType+":"+element] = rangeElement;
    } else {
      rangeElement = unRecipe_rangeElements[deviceType+":"+element];    
    }   
    
    string format="";
    unRecipeFunctions_getInstanceRange(pvssDpName, rangeElement, rangeMin, rangeMax, exceptionInfo);
    if (rangeMin!="" && rangeMax!=""){
      format = (string) dpGetFormat(unGenericDpFunctions_getDpName(pvssDpName)+rangeElement);
    }
    
    if (format!="") {
      formattedValues[i]=unGenericObject_FormatValue(format, (float) values[i]);
	  continue;
    } 
    
	// if the value is a float, format the value using the online DPE format
	if (dpElementType(dsDpNames[i]) == DPEL_FLOAT) {
        string sDpName = unGenericDpFunctions_getDpName(pvssDpName)+rangeElement;
        unRecipeFunctions_getOnlineValueDpes(makeDynString(sDpName), dsOnlineDpes, exceptionInfo);
        if (!dynlen(exceptionInfo)) {
		  format = (string) dpGetFormat(dsOnlineDpes[1]);
		  unGenericObject_StringToFloat(values[i], fValue, bOk);
		  if (!bOk) {
			fwException_raise(exceptionInfo, "ERROR", "Float conversion error :" + values[i], "");
		  }
		  formattedValues[i] = unGenericObject_FormatValue(format, fValue);
        } else {
          formattedValues[i] = values[i];
        }
	} else {
		// Applies the default PVSS format
		string s = values[i];  
		formattedValues[i] = s;
	}
  }  
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Internal function to check if the recipe values are valid according to the defined ranges.
 * @param dsDps           - [IN]  Datapoint elements of the recipe values.
 * @param values          - [IN]  Recipe values.
 * @param bShowErrorLines - [IN]  TRUE if the values out of range must be highlighted in the table.
 * @param exceptionInfo   - [OUT] Standard exception handling variable.
 * @return TRUE if the recipe values are Ok according to the ranges, otherwise FALSE.
 */
public bool _unRecipeFunctions_checkRecipeValues(dyn_string dsDps, dyn_anytype values, bool bShowErrorLines, dyn_string &exceptionInfo) {  
  int iRetVal;
  dyn_int diErrorLines;

  dynClear(exceptionInfo);  
  int iLen = dynlen(dsDps);
  
  // Check if the number DPs matches the number of values.
  if (iLen != dynlen(values)) {
    fwException_raise(exceptionInfo, "ERROR", "The number of elements in the recipe " + 
                      "is different than the number of values.", "");
    return FALSE;
  }
  
  for (int i=1; i<=iLen; i++){
    dyn_string exception;
	// Check if the recipe value is valid
    iRetVal = unRecipeFunctions_checkRecipeValue(dsDps[i], values[i], exception);

	if (dynlen(exception)) {
      dynAppend(exceptionInfo, exception);
    }	  
	  
	if (iRetVal == -1) {
      dynAppend(diErrorLines, i);
    } 
  }
  
  if (dynlen(diErrorLines)) {
    fwException_raise(exceptionInfo, "ERROR", "The value of some recipe elements is out of range.", "");
  }
  
  if (bShowErrorLines) {
    _unRecipeFunctions_showValuesOutOfRange(diErrorLines);
  }
  
  return dynlen(diErrorLines) == 0;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Check if the recipe value is valid according to the defined range.
 * @param sDpName - [IN] Data point name of the recipe element.
 * @param value   - [IN] Value of the recipe element.
 * @param exceptionInfo - [OUT] Standard exception handling routine.
 * @return 0 if the value is in the defined range or if no range is defined, otherwise -1.
 */
int unRecipeFunctions_checkRecipeValue (string sDpName, anytype value, dyn_string &exceptionInfo)
{
	string sVal, range, min, max;
	
	range = unRecipeFunctions_getDeviceRange(sDpName, exceptionInfo);	
	if (dynlen(exceptionInfo)) {
		return -1;
	}
	
	// No range defined => Value is valid
	if (range=="") {
		return 0;	
    }
		
	// Check if the range is FALSE, TRUE
	if (range=="[FALSE, TRUE]") {
		if (value=="0" || value=="1" || strtoupper(value)=="TRUE" || strtoupper(value)=="FALSE") {
			return 0;
        }
		return -1;
    }	
		
    unRecipeFunctions_getRangeValues(range, min, max, exceptionInfo);
	
    if (dynlen(exceptionInfo)) {
      return -1;
    } 
	
    float fmin, fmax, fval;
    fmin = (float)min;
    fmax = (float)max;
	
	// Removes the white space in the exponential value (1.5 e1 => 1.5e1) before converting to float
	sVal = value;
	strreplace(sVal, " ", "");	
    fval = (float)sVal;

    if (fmin>fval || fval>fmax){
      return -1;
    } 
	
	return 0;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe instance object.
 * @param sRecipeDp     - [IN] Datapoint name of the recipe instance.
 * @param recipeObject  - [OUT] Recipe instance object from fwConfigDB.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void _unRecipeFunctions_getRecipeObjectFromDp (string sRecipeDp, dyn_dyn_mixed &recipeObject, dyn_string &exceptionInfo)
{
  int pos;
  string recipeClass, recipeInstance, sRcpClassDp, sSystemName;
  dyn_string dsDeviceOrder;
  
  if ((pos=strpos(sRecipeDp, "."))>=0) {
	sRecipeDp = substr(sRecipeDp, 0, pos);
  }
  
  if (!dpExists(sRecipeDp+".ProcessInput.ClassName") || 
      !dpExists(sRecipeDp+".ProcessInput.InstanceName"))
  {
	fwException_raise(exceptionInfo, "ERROR", "The recipe DP '" + sRecipeDp + "' doesn't exist.","");
	return;
  }  
  
  dpGet(sRecipeDp+".ProcessInput.ClassName", recipeClass,
		sRecipeDp+".ProcessInput.InstanceName", recipeInstance);
  
  sSystemName = unGenericDpFunctions_getSystemName(sRecipeDp);
  fwConfigurationDB_loadRecipeFromCache(sSystemName + recipeClass+"/"+recipeInstance, makeDynString(), "", recipeObject, exceptionInfo);
  
  // Order the devices
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + recipeClass, sRcpClassDp);
  dpGet(sRcpClassDp + "ProcessInput.DeviceOrder", dsDeviceOrder);
  _unRecipeFunctions_orderRecipeInstanceElements(dsDeviceOrder, recipeObject);
}

//------------------------------------------------------------------------------------------------------------------------
private void _unRecipeFunctions_orderRecipeInstanceElements(dyn_string dsDeviceOrder, dyn_dyn_mixed &recipeObject) {
  dyn_mixed dmDpeName, dmDpName, dmDpType, dmElementName, dmElementType, dmValues;

  if (!dynlen(dsDeviceOrder)) {
    return;
  }

  // Check if the DeviceOrder and dsDeviceOrder lists contain the same elements
  dyn_mixed dmOriginalDevices = recipeObject[fwConfigurationDB_RO_DP_NAME];
  dynUnique(dmOriginalDevices);  
  if (!unRecipeFunctions_areListsEqual(dmOriginalDevices, dsDeviceOrder)) {
    DebugTN("Error: The device lists are not synchronized. The recipe elements can not be ordered.");
    DebugTN("OriginalDevices: ", dmOriginalDevices, "Device Order: ", dsDeviceOrder);
    DebugTN(getStackTrace());
    return;
  }
  
  for (int i=1; i<=dynlen(dsDeviceOrder); i++) {
    string sDevice = dsDeviceOrder[i];
    int j = dynContains(recipeObject[fwConfigurationDB_RO_DP_NAME], sDevice);
    if (j<=0) {
      // Error, the device lists are not synchronized
      DebugTN("Error: The device lists are not synchronized. Device " + sDevice + " does not exist in the recipeObject.");      
      return;
    }
    
    do {      
      dynAppend(dmDpeName, recipeObject[fwConfigurationDB_RO_DPE_NAME][j]);
      dynAppend(dmDpName,  recipeObject[fwConfigurationDB_RO_DP_NAME][j]);
      dynAppend(dmDpType,  recipeObject[fwConfigurationDB_RO_DP_TYPE][j]);
      dynAppend(dmElementName, recipeObject[fwConfigurationDB_RO_ELEMENT_NAME][j]);
      dynAppend(dmElementType, recipeObject[fwConfigurationDB_RO_ELEMENT_TYPE][j]);
      dynAppend(dmValues,  recipeObject[fwConfigurationDB_RO_VALUE][j]);
      j++;
    } while (j<=dynlen(recipeObject[fwConfigurationDB_RO_DP_NAME]) && recipeObject[fwConfigurationDB_RO_DP_NAME][j] == sDevice);
  }

  recipeObject[fwConfigurationDB_RO_DPE_NAME]     = dmDpeName;
  recipeObject[fwConfigurationDB_RO_DP_NAME]      = dmDpName;
  recipeObject[fwConfigurationDB_RO_DP_TYPE]      = dmDpType;
  recipeObject[fwConfigurationDB_RO_ELEMENT_NAME] = dmElementName;
  recipeObject[fwConfigurationDB_RO_ELEMENT_TYPE] = dmElementType;
  recipeObject[fwConfigurationDB_RO_VALUE]        = dmValues;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the recipe class object from the JCOP library.
 * @param sRcpClassDp	  - [IN] Recipe class name.
 * @param recipeClassInfo - [OUT] Recipe class object returned from the JCOP library.
 * @param exceptionInfo   - [OUT] Standard exception handling variable.
 */
void unRecipeFunctions_getRecipeClassObject(string sRcpClassDp, dyn_mixed &recipeClassInfo, dyn_string &exceptionInfo) {
  string sRcpClassName, sSystemName;
  unRecipeFunctions_normalizeDp(sRcpClassDp);
  sSystemName = unGenericDpFunctions_getSystemName(sRcpClassDp);
  dpGet(sRcpClassDp + ".ProcessInput.ClassName", sRcpClassName);
  fwConfigurationDB_getRecipeClassInfo(sSystemName + sRcpClassName, recipeClassInfo, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Gets the DB number of a S7 address for a DPE.
 * @param sDpe - [IN] DPE which DB number is required.
 * @param sDeviceType - [IN] Device type name of the dpe.
 * @param sDb - [OUT] DB number of the DPE S7 address.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void _unRecipeFunctions_getS7AddressDBNumber(string sDpe, string sDeviceType, string &sDb, dyn_string &exceptionInfo)
{
  string fullAddress = DRV_S7_convertToUnicosAddress(sDpe, sDeviceType);

  int pos=strpos(fullAddress, ".");
  if(pos<0) {
    fwException_raise(exceptionInfo, "ERROR", "The S7 address format is invalid: " + fullAddress, "");
    return;
  }
  
  sDb = substr(fullAddress, 0, pos);
  strreplace(sDb, "DB", "");
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Gets the PLC status of a recipe.
 * @param recipeElements - [IN] List of DPEs.
 * @param bActive        - [OUT] True if the PLCs are active, otherwise false.
 * @param statusMessage  - [OUT] PLC status message.
 * @param exceptionInfo  - [OUT] Standard exception handling variable.
 */
void unRecipeFunctions_getPlcStatus(dyn_string recipeElements, bool &bActive, 
                                    dyn_string &statusMessage, dyn_string &exceptionInfo)
{
  int iRes;
  bActive = true;
	
  // Get the list of PLCs used in the recipe
  dyn_string plcList;
  for (int i=1; i<=dynlen(recipeElements); i++){
    if (!dpExists(recipeElements[i])) {
        fwException_raise(exceptionInfo, "ERROR", "The recipe element DPE does not exist: " + recipeElements[i], "");
        return;
    }
    
    string sSystemName = unGenericDpFunctions_getSystemName(recipeElements[i]);
    string sPlc = sSystemName + unGenericObject_GetPLCNameFromDpName(recipeElements[i]);
    if (!dynContains(plcList, sPlc)){
        dynAppend(plcList, sPlc);
    }
  }
	
  //check PLC status
  for (int i=1; i<=dynlen(plcList); i++){
    //sPlcName - corresponding dp name in "_Mod_Plc" internal DPT
    int iSystemIntegrityAlarmValue = 0;
    bool bSystemIntegrityAlarmEnabled = false;
    string sSystemName = unGenericDpFunctions_getSystemName(plcList[i]);
    string sPlcName = substr(plcList[i], strlen(sSystemName));	
    iRes = dpGet(sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".alarm", iSystemIntegrityAlarmValue);
    if (iRes < 0)	{
      dynAppend(exceptionInfo, "unRecipeFunctions_getPlcStatus: failed to get current state of plc - " + sPlcName +"\n");
      return;
    }
	
    iRes = dpGet(sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".enabled", bSystemIntegrityAlarmEnabled);
    if (iRes < 0)	{
     	dynAppend(exceptionInfo, "unRecipeFunctions_getPlcStatus: failed to get current state of plc - " + sPlcName+"\n");
        return;
    }	
    
    if(!bSystemIntegrityAlarmEnabled || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)) {
        bActive = false;
        dynAppend(statusMessage, sPlcName);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Gets a mapping where the key is the application name and the value is a dyn_string containing the PCOs in that application.
 * @param bLocalOnly - [IN] TRUE if and only if the local applications must be included, otherwise FALSE.
 * @param bOnlyPcosWithRecipes - [IN] TRUE if and only if the PCOs with recipes must be included, otherwise FALSE.
 * @return map where key=application name, value = List of PCOs for the application
 */
// TODO: Rename function to end with suffix 'Mapping'
public mapping unRecipeFunctions_getApplicationPcos(bool bLocalOnly=TRUE, bool bOnlyPcosWithRecipes=FALSE) {
  int i, iLen;
  mapping mApplicationPcos;
  dyn_string dsSelectedDeviceTypes, dsRetDpNames, dsRetAppl, dsPcoNames, dsPcoNamesWithRecipes, dsPcoDpsWithRecipes;
  
  dsSelectedDeviceTypes = makeDynString(UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME);
  _unRecipeFunctions_getFilteredDevices(dsSelectedDeviceTypes, dsRetDpNames, dsPcoNames, dsRetAppl, bLocalOnly);
  
  iLen = dynlen(dsPcoNames);
  if (iLen == 0) {
    return mApplicationPcos;    
  }
  
  if (bOnlyPcosWithRecipes) {
    unRecipeFunctions_getPcosWithRecipes(dsPcoNamesWithRecipes, dsPcoDpsWithRecipes, bLocalOnly, "*", dsPcoNames, dsRetDpNames);
  }
  
  for (i=1; i<=iLen; i++) {
     string key = dsRetAppl[i];
     string sPcoName = dsPcoNames[i];
     if (bOnlyPcosWithRecipes && !dynContains(dsPcoNamesWithRecipes, sPcoName)) {
       continue;
     }
    unRecipeFunctions_mappingInsertValue(mApplicationPcos, key, sPcoName); 
  }
  
  return mApplicationPcos;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Gets the list of PCOs for the specified application.
 * @param mApplicationPcos - [IN] Mapping where the key is the application name and the value is the list of PCOs for the application
 * @param sApplication - [IN] Application name or '*' to get the PCOs of all the applications
 * @return dyn_string containing the list of PCOs for the selected application
 */
public dyn_string unRecipeFunctions_getApplicationPcoList(mapping mApplicationPcos, string sApplication) {
  dyn_string result;
  
  dynAppend(result, "*");
  if (sApplication =="*") {
    int i, iLen;
    dyn_string keys = mappingKeys(mApplicationPcos);
    iLen = dynlen(keys);
    for (i=1; i<=iLen; i++) {
      string key = keys[i];
      dyn_string value = mApplicationPcos[key];
      dynAppend(result, value);
    }
  } else if (mappingHasKey(mApplicationPcos, sApplication)) {
    dyn_string dsApplicationPcos = mApplicationPcos[sApplication];
    dynAppend(result, dsApplicationPcos);    
  }
  
  return result;
} 
//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the list of PCOs with recipes
 * @param dsPcoNamesWithRecipes - [OUT] Alias of the PCOs with recipes found.
 * @param dsPcoDpsWithRecipes   - [OUT] Dp names of the PCOs with recipes found.
 * @param bLocalOnly - [IN] TRUE if and only if the PCOs on the local application must be queried, otherwise FALSE.
 * @param sApplication - [IN] Applications where to look for the PCOs with recipes.
 * @param dsAllPcos - [IN] List of PCO aliases, if not provided it will search for the PCOs in the specified applications and systems.
 * @param dsAllPcoDps - [IN] List of PCO dp names, if not provided it will search for the PCO dpnames in the specified applications and systems.
 */
public void unRecipeFunctions_getPcosWithRecipes(dyn_string &dsPcoNamesWithRecipes, dyn_string &dsPcoDpsWithRecipes,
        bool bLocalOnly=FALSE, string sApplication ="*", dyn_string dsAllPcos=makeDynString(), dyn_string dsAllPcoDps=makeDynString()) {
  int i, iLen, j;  
  dyn_string dsRetAppl;
  dyn_dyn_anytype resultTable;
  
  if (dynlen(dsAllPcos) == 0 || dynlen(dsAllPcoDps) ==0) {
    _unRecipeFunctions_getFilteredDevices(makeDynString(UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME), 
        dsAllPcoDps, dsAllPcos, dsRetAppl, bLocalOnly, makeDynString(sApplication));
  }
  
  // Get the list of PCOs with recipe classes   
  iLen = dynlen(dsAllPcoDps);
  dyn_string dsQueriedSystems;
  for (i = 1; i<=iLen; i++) {    
    string sSystemName = unGenericDpFunctions_getSystemName(dsAllPcoDps[i]); 
    if (dynContains(dsQueriedSystems, sSystemName)) {
      continue;
    }
    dynAppend(dsQueriedSystems, sSystemName);
    string sQuery = "SELECT '.statusInformation.deviceLink:_online.._value' FROM '*' REMOTE '" + sSystemName + "' WHERE _DPT=\"UnRcpClass\" "; 
    dpQuery(sQuery, resultTable);   	  
    
    int jLen = dynlen(resultTable);
    for (j = 2; j<=jLen; j++) {
      if (!dynContains(dsPcoNamesWithRecipes, resultTable[j][2])) {
        int iPos = dynContains(dsAllPcos, resultTable[j][2]);
        if (iPos > 0 ) {
          string sPcoDp = dsAllPcoDps[iPos];
          dynAppend(dsPcoNamesWithRecipes, resultTable[j][2]);
          dynAppend(dsPcoDpsWithRecipes, sPcoDp);      
        }
      }
    }
  } 
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Load the PCO List in the PcoList combo box.
 * @param sApplication - [IN] Application name where to load the PCOs from.
 */
public void _unRecipeFunctions_loadPcosWithRecipes(string sApplication) {
  dyn_string dsPcoNamesWithRecipes, dsPcoDpsWithRecipes;
	
  // Check that all the necessary shapes exist
  if (!shapeExists("PcoList")) {
    dyn_string exceptionInfo;
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return;
  }
                                        
  // Get the list of PCOs with recipe classes  
  bool bLocalOnly=FALSE;
  unRecipeFunctions_getPcosWithRecipes(dsPcoNamesWithRecipes, dsPcoDpsWithRecipes, bLocalOnly, sApplication);   
  	
  dynInsertAt(dsPcoNamesWithRecipes, "*", 1);
  PcoList.deleteAllItems();
  PcoList.items = dsPcoNamesWithRecipes;
} 

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the list of devices filtering the applications and the device types specified.
 * @param dsSelectedDevType     - [IN]  List of selected device types.
 * @param dsRetDpName 	        - [OUT] List of DP Names found.
 * @param dsRetAlias            - [OUT] List of devices aliases found.
 * @param dsRetAppl             - [OUT] List of applications of the found devices.
 * @param bLocalOnly            - [IN]  TRUE if only the devices from the local system must be returned, otherwise FALSE.
 * @param dsSelectedApplication - [IN]  List of applications where to look for the devices.
 */
public void _unRecipeFunctions_getFilteredDevices(dyn_string dsSelectedDevType, dyn_string &dsRetDpName, dyn_string &dsRetAlias, 
            dyn_string &dsRetAppl, bool bLocalOnly=FALSE, dyn_string dsSelectedApplication=makeDynString("*")) 
{
  dyn_string dsSystemRetDpName, dsSystemRetAlias, dsSystemRetAppl, dsSystemNames;
  if (bLocalOnly) {
    dsSystemNames = makeDynString(getSystemName());
  } else {
    dsSystemNames = mappingKeys(g_m_ddsTreeDeviceOverviewDeviceList);
  }
  
  for (int i=1; i<=dynlen(dsSystemNames); i++) {
    _unRecipeFunctions_getFilteredDevicesBySystemName(dsSystemNames[i], dsSelectedDevType, dsSystemRetDpName, dsSystemRetAlias, dsSystemRetAppl, dsSelectedApplication);
    dynAppend(dsRetDpName, dsSystemRetDpName);
    dynAppend(dsRetAlias, dsSystemRetAlias);
    dynAppend(dsRetAppl, dsSystemRetAppl);
  }
}
//------------------------------------------------------------------------------------------------------------------------
public void _unRecipeFunctions_getFilteredDevicesBySystemName(string sSystemName, dyn_string dsSelectedDevType, dyn_string &dsRetDpName, 
         dyn_string &dsRetAlias, dyn_string &dsRetAppl, dyn_string dsSelectedApplication=makeDynString("*")) {
  bit32 b32SelectState;
  dyn_string dsSelectedSystem, dsSelectedFE, dsSelectedAppl, dsSelectedSubsystem1, dsSelectedSubsystem2, dsAliasSelected ;
  dyn_string dsExcludedAliasSelected, dsDpName, dsAlias, dsFE, dsFEDpName, dsFEDPType, dsAppl, dsDevType, dsSubsystem1, dsSubsystem2;
  dyn_string dsRetFE, dsRetFEDpName, dsRetFEDPType, dsRetDevType, dsRetSubsystem1, dsRetSubsystem2;
  dyn_dyn_string dsSystemData;
  
  // Set the bits of the SelectState variable
  setBit(b32SelectState, FILTER_APPLICATION, TRUE);
  setBit(b32SelectState, FILTER_DEVICETYPE, TRUE);

  dsSelectedSystem = makeDynString("*");
  dsSelectedFE = makeDynString("*");
  dsSelectedSubsystem1 = makeDynString("*");
  dsSelectedSubsystem2 = makeDynString("*");
  dsAliasSelected = makeDynString("*");
  dsExcludedAliasSelected = makeDynString();
  
  // Get the system data  
  dsSystemData = g_m_ddsTreeDeviceOverviewDeviceList[sSystemName];
  if (!dynlen(dsSystemData)) {
    return;
  }
  dsDpName = dsSystemData[TREE_DEVICE_DPNAME];
  dsAlias = dsSystemData[TREE_DEVICE_ALIAS];
  dsFE = dsSystemData[TREE_DEVICE_FE];
  dsFEDpName = dsSystemData[TREE_DEVICE_FEDPNAME];
  dsFEDPType = dsSystemData[TREE_DEVICE_FEDPTYPE];
  dsDevType = dsSystemData[TREE_DEVICE_TYPE];
  dsSubsystem1 = dsSystemData[TREE_DEVICE_SUBSYSTEM1];
  dsSubsystem2 = dsSystemData[TREE_DEVICE_SUBSYSTEM2];
  dsAppl = dsSystemData[TREE_DEVICE_FEAPPLICATION];
  
  // Get the visible applications
  _unRecipeFunctions_getVisibleApplicationsList(dsSelectedAppl);
  
  CtrlTreeDeviceOverview_getSelectedDeviceList(
    b32SelectState, sSystemName, dsSelectedSystem, dsSelectedFE, dsSelectedAppl,
	dsSelectedDevType, dsSelectedSubsystem1, dsSelectedSubsystem2, dsAliasSelected, dsExcludedAliasSelected,
	dsDpName, dsAlias, dsFE, dsFEDpName, dsFEDPType, dsAppl, dsDevType, dsSubsystem1, dsSubsystem2,
	dsRetDpName, dsRetAlias, dsRetFE, dsRetFEDpName, dsRetFEDPType, dsRetAppl, dsRetDevType, dsRetSubsystem1, dsRetSubsystem2);
	  
  // Remove the DPNames and Alias that don't exist (the list may not be synchronized)
  int iLen = dynlen(dsRetDpName);
  for (int i=1; i<=iLen; i++) {
	  if (!dpExists(dsRetDpName[i]) || (!dynContains(dsSelectedApplication, dsRetAppl[i]) && !dynContains(dsSelectedApplication, "*"))) {
	    dynRemove(dsRetDpName, i);
	    dynRemove(dsRetAlias, i);
      dynRemove(dsRetAppl, i);
	    iLen--;
	    i--;
	  }
  }  
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the list of visible applications.
 * @param dsApplications - [OUT] List of visible applications.
 */
void _unRecipeFunctions_getVisibleApplicationsList(dyn_string &dsApplications) synchronized(g_unicosHMI_dsApplicationList) { 
  dynClear(dsApplications);
  
  // Check if the application filter is being used
  if (false == g_unicosHMI_bApplicationFilterInUse) {
	dynAppend(dsApplications, "*");
	return;
  }
  
  fwGeneral_stringToDynString(g_unicosHMI_dsApplicationList, dsApplications);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get all the PCOs that are children of the sPco device (recursive method, get all the PCOs in the hierarchy).
 * @param sPcoDpName - [IN] Name of the PCO which children are requested.
 * @param children - [OUT] Contains the alias of all the PCO children of the sPcoDp device.
 * @param exceptionInfo - [OUT] Standard exception handling variable. 
 */
void _unRecipeFunctions_getPcoChildren(string sPcoDpName, dyn_string &children, dyn_string & exceptionInfo) {
  string sCurrentPcoAlias;
  dyn_string dsAllChildren;

  unRecipeFunctions_normalizeDp(sPcoDpName);
  
  dsAllChildren = cpcGenericObject_getLinkedObjects(sPcoDpName, UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME, exceptionInfo);
  
  for (int i = 1; i <= dynlen(dsAllChildren); i++) {
    sCurrentPcoAlias = dsAllChildren[i];
    dynAppend(children, sCurrentPcoAlias);
	_unRecipeFunctions_getPcoChildren(sCurrentPcoAlias, children, exceptionInfo);    
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Create the SOFT_FE for the recipes.
 * @param sAppName - [IN] Application name.
 * @param sPrefix  - [IN] Application prefix.
 * @param bEvent   - [IN] TRUE=16 bit event type, FALSE=32 bit event type
 * @param sAnalog  - [IN] Name of the analog archive file.
 * @param sBool    - [IN] Name of the boolean archive file.
 * @param sEvent   - [IN] Name of the event archive file.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return TRUE if and only if the recipe front-end was created successfully.
 */ 
bool unRecipeFunctions_createRecipeFrontEnd(string sAppName, string sPrefix, bool bEvent, string sAnalog, string sBool, string sEvent, dyn_string & exceptionInfo)
{
  string sPvssDpe;
  
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(UN_RECIPE_FE_ALIAS, sPvssDpe);
  if (sPvssDpe==UN_RECIPE_FE_ALIAS) {
	dyn_float df;
	dyn_string ds;
	ChildPanelOnCentralModalReturn(
		"vision/unRecipe/UnRcpFrontEnd/unUnRcpFrontEnd_Create.pnl",
		"Recipe Front End Creation",
		makeDynString(
			"$sAppName:" + sAppName,
			"$sPrefix:" + sPrefix,
			"$bEvent:" + bEvent,
			"$sAnalog:" + sAnalog,
			"$sBool:" + sBool,
			"$sEvent:" + sEvent),
		df, ds);

	if (dynlen(df)==1 && df[1]==1) {
		return true;
    }
    
    dynAppend(exceptionInfo, "Create recipe FrontEnd cancelled by the user.");
	return false;
  } 
  
  return true;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Open the faceplate of a device provided the device alias.
 * @param sAlias        - [IN] Device alias
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void unRecipeFunctions_openFaceplate(string sAlias, dyn_string &exceptionInfo) {
  string sDpName;
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sAlias, sDpName);
  
  if (!dpExists(sDpName)) {
    return;
  }
  
  unGenericObject_OpenFaceplate(sDpName, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
//@}
