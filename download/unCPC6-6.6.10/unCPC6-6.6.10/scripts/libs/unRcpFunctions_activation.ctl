/**
 * UNICOS
 * Copyright (C) CERN 2014 All rights reserved
 */
/**@file

@name LIBRARY: unRecipeFunctions_activation.ctl

@author: Ivan Prieto Barreiro (EN-ICE-SIC)

Creation Date: April 2011 

version 1.4.0

Modification History:

  20/10/2014: Ivan
    - v1.4.0 : The file contains the functions used for recipe activation.
		
External Functions: 
  .  
  
Internal Functions:
  .
*/

#uses "unRcpFunctions_instance.ctl"
#uses "unRcpFunctions_gui.ctl"
#uses "unRcpFunctions_utilities.ctl"
#uses "unRcpFunctions_dpe.ctl"
#uses "fwGeneral/fwException.ctl"
#uses "fwGeneral/fwGeneral.ctl"
#uses "fwConfigurationDB/fwConfigurationDB.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unGenericObject.ctl"
#uses "unExportTree.ctl"
#uses "unProgressBar.ctl"
#uses "unSystemIntegrity_unicosPLC.ctl"
#uses "unicos_declarations.ctl"
#uses "unConfigSOFT_FE.ctl"
#uses "unRcpFunctions.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "cpcRcpBuffer/cpcRcpBuffersConfig.ctl"

/****************************** 
 *       Recipe Buffers       *
 *****************************/
global mapping gHeaderMap;			// Header buffer map
global mapping gManRegAddrMap;  	// ManReg address buffer map
global mapping gManRegValMap;		// ManReg values buffer map: this are the values used in the ArmRcp
global mapping gReqAddrMap;			// Req address buffer map
global mapping gReqValMap;			// Req values buffer map


/****************************** 
 *      Global   Variables    *
 *****************************/
global mapping unRecipe_unRecipe_pidManRegBits; // Map where key=controller dpe name, value=bit position of the dpe inside the ManReg01 
global mapping unRecipe_aalarmManRegBits;		// Map where key=analog alarm dpe name, value=bit position of the dpe inside the ManReg01 
global mapping unRecipe_recipeActivationInfo; 	// Map where key=recipe instance Dp, value=Matrix containing the necessary information to activate recipes in multi-PLCs 								


//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Initialization of the ManReg bit positions used in the recipes by the different device types.
 */
private void _unRecipeFunctions_initializeManRegBits()
{
  // Initializes the bit positions of the ManReg01 for the CPC_Controller Objects
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.ArmRcp"]= 4;
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MSP"]   = 7;
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MSPH"]  = 8;
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MSPL"]  = 9;  
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MOutH"] = 10;  
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MOutL"] = 11;  
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MKc"]   = 12;  
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MTi"]   = 14;
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MTd"]   = 13;
  unRecipe_unRecipe_pidManRegBits[".ProcessOutput.MTds"]  = 15;  
  
  // Initializes the bit positions of the ManReg01 for the Analog Alarm Objects
  unRecipe_aalarmManRegBits[".ProcessOutput.HH"] = 8;
  unRecipe_aalarmManRegBits[".ProcessOutput.H"]  = 9;
  unRecipe_aalarmManRegBits[".ProcessOutput.L"]  = 12;
  unRecipe_aalarmManRegBits[".ProcessOutput.LL"] = 13;  
}

//--------------------------------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		Methods to get recipe buffers by PLC name
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** 
 * Get the recipe header buffer for the specified PLC.
 * @param sPlcName - [IN] The PLC name which recipe header buffer is required.
 * @return dyn_int containing the values of the recipe header buffer for the specified PLC.
 */
private dyn_int _unRecipeFunctions_getHeaderBuffer(string sPlcName) {
  if (mappingHasKey(gHeaderMap, sPlcName)) 
    return gHeaderMap[sPlcName];
  
  dyn_int buffer;
  dynAppend(buffer, 0);
  gHeaderMap[sPlcName] = buffer;
  return buffer;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe ManRegAddress buffer for the specified PLC.
 * @param sPlcName - [IN] The PLC name which recipe ManRegAddress buffer is required.
 * @return dyn_int containing the values of the recipe ManRegAddress buffer for the specified PLC.
 */
private dyn_int _unRecipeFunctions_getManRegAddrBuffer(string sPlcName) {
  if (mappingHasKey(gManRegAddrMap, sPlcName)) 
    return gManRegAddrMap[sPlcName];
  
  dyn_int buffer;
  dynAppend(buffer, 0);
  gManRegAddrMap[sPlcName] = buffer;
  return buffer;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe ManRegValues buffer for the specified PLC.
 * @param sPlcName - [IN] The PLC name which recipe ManRegValues buffer is required.
 * @return dyn_int containing the values of the recipe ManRegValues buffer for the specified PLC.
 */
private dyn_int _unRecipeFunctions_getManRegValBuffer(string sPlcName) {
  if (mappingHasKey(gManRegValMap, sPlcName)) 
    return gManRegValMap[sPlcName];
  
  dyn_int buffer;
  dynAppend(buffer, 0);
  gManRegValMap[sPlcName] = buffer;
  return buffer;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe ReqAddress buffer for the specified PLC.
 * @param sPlcName - [IN] The PLC name which recipe ReqAddress buffer is required.
 * @return dyn_int containing the values of the recipe ReqAddress buffer for the specified PLC.
 */
private dyn_int _unRecipeFunctions_getReqAddrBuffer(string sPlcName) {
  if (mappingHasKey(gReqAddrMap, sPlcName)) 
    return gReqAddrMap[sPlcName];
  
  dyn_int buffer;
  dynAppend(buffer, 0);
  gReqAddrMap[sPlcName] = buffer;
  return buffer;
}


//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe ReqValues buffer for the specified PLC.
 * @param sPlcName - [IN] The PLC name which recipe ReqValues buffer is required.
 * @return dyn_float containing the values of the recipe ReqValues buffer for the specified PLC.
 */
private dyn_float _unRecipeFunctions_getReqValBuffer(string sPlcName) {
  if (mappingHasKey(gReqValMap, sPlcName)) 
    return gReqValMap[sPlcName];
  
  dyn_float buffer;
  dynAppend(buffer, 0.0);
  gReqValMap[sPlcName] = buffer;
  return buffer;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Remove all the recipe buffers associated to the specified PLC.
 * @param sPlcName - [IN] The PLC name which recipe buffers must be removed.
 */
private void _unRecipeFunctions_removeRecipeBuffers(string sPlcName) {
  mappingRemove(gHeaderMap, sPlcName);
  mappingRemove(gManRegAddrMap, sPlcName);
  mappingRemove(gManRegValMap, sPlcName);
  mappingRemove(gReqAddrMap, sPlcName);
  mappingRemove(gReqValMap, sPlcName);
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Activate a recipe.
 * @param sRcpInstanceDp - [IN] Data point name of the recipe which activation is triggered.
 * @param exceptionInfo  - [OUT] Standard exception handling variable.
 * @return Integer value to indicate the activation result (see the RCP_ACT_RETVAL_ constants).
 */
public int unRecipeFunctions_activateRecipe(string sRcpInstanceDp, dyn_string &exceptionInfo) {
  mapping plcDpes;
  dyn_anytype plcNames;
  dyn_dyn_mixed rcpInstance;
  dyn_string dsStatusMessage, dsBusyPlcs;
  string sRcpClassName, sRcpInstanceName, sRcpState, sSystemName;
  bool bActive, bOk;
  int iLen, iActivationResult, iRetVal;
  
  // Check if the recipe DP exists
  if (!dpExists(sRcpInstanceDp)) {
    fwException_raise(exceptionInfo, "ERROR", "There recipe DP doesn't exist: " + sRcpInstanceDp, "");
	return RCP_ACT_RETVAL_BAD_RECIPE_DATA;
  }
  
  // If the sRcpInstanceDp ends with '.', remove the dot
  unRecipeFunctions_normalizeDp(sRcpInstanceDp);
  sSystemName = unGenericDpFunctions_getSystemName(sRcpInstanceDp);
  dpGet(sRcpInstanceDp+".ProcessInput.State", sRcpState, 
        sRcpInstanceDp+".ProcessInput.ClassName", sRcpClassName,
		sRcpInstanceDp+".ProcessInput.InstanceName", sRcpInstanceName);

  string sRcpName = sRcpClassName + "/" + sRcpInstanceName;        
  if (sRcpState != "") {
    unRecipeFunctions_writeInRecipeLog("Error: The recipe " + sRcpName + " is in state " + sRcpState + ".");
    unRecipeFunctions_writeInRecipeLog("The recipe activation is cancelled.");
    return RCP_ACT_RETVAL_BAD_RECIPE_STATUS;
  }
  
  // Disables the graphical elements from the recipe panel
  _unRecipeFunctions_setRecipeInstancePanelComponentsEnabled(false);
  dpSet(sRcpInstanceDp+".ProcessInput.State", UN_RCP_INSTANCE_STATE_ACTIVATE);
  dynClear(exceptionInfo);  
   
  if (mappinglen(unRecipe_unRecipe_pidManRegBits)==0 || mappinglen(unRecipe_aalarmManRegBits)==0) {
    _unRecipeFunctions_initializeManRegBits();
  }
    
  fwConfigurationDB_loadRecipeFromCache(sSystemName + sRcpName, makeDynString(), "", rcpInstance, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    _unRecipeFunctions_resetRecipeActivation(ERROR_LOADING_RECIPE, sRcpInstanceDp, sRcpName, "");
	return RCP_ACT_RETVAL_BAD_RECIPE_DATA; 
  }

  // Check if the recipe instance has elements
  if (dynlen(rcpInstance[fwConfigurationDB_RO_DPE_NAME]) == 0) {
	unRecipeFunctions_writeInRecipeLog("The recipe instance has no elements and it can not be activated.");
    _unRecipeFunctions_resetRecipeActivation(RCP_ACT_RETVAL_ACTIVATION_OK, sRcpInstanceDp, sRcpName, "");
	return RCP_ACT_RETVAL_ACTIVATION_OK;
  }
  
  // Check if the recipe values are out of range
  bOk = _unRecipeFunctions_checkRecipeValues (
            rcpInstance[fwConfigurationDB_RO_DPE_NAME], 
            rcpInstance[fwConfigurationDB_RO_VALUE], 
            FALSE, exceptionInfo);
            
  if (!bOk || dynlen(exceptionInfo)>0) {
    _unRecipeFunctions_resetRecipeActivation(VALUE_OUT_OF_RANGE, sRcpInstanceDp, sRcpName, "");
	return RCP_ACT_RETVAL_BAD_RECIPE_DATA;  
  }
  
  // Check if all the PLCs are connected
  unRecipeFunctions_getPlcStatus(rcpInstance[fwConfigurationDB_RO_DPE_NAME], bActive, dsStatusMessage, exceptionInfo);
  if (!bActive) {
    string sMessage;
    fwGeneral_dynStringToString(dsStatusMessage, sMessage, ", ");
    fwException_raise(exceptionInfo, "ERROR", "There isn't connection with the PLC(s): " + sMessage, "");
    _unRecipeFunctions_resetRecipeActivation(RCP_ACT_RETVAL_BAD_PLC_STATUS, sRcpInstanceDp, sRcpName, sMessage);
	return RCP_ACT_RETVAL_BAD_PLC_STATUS;
  }
  
  // Gets the recipe elements by PLC
  iRetVal = _unRecipeFunctions_getElementsByPLC(
                rcpInstance[fwConfigurationDB_RO_DPE_NAME], 
  				rcpInstance[fwConfigurationDB_RO_VALUE], 
  				plcDpes, 
  				exceptionInfo);
  
  if (iRetVal<0)  { 
    _unRecipeFunctions_resetRecipeActivation(iRetVal, sRcpInstanceDp, sRcpName, "");
	return RCP_ACT_RETVAL_BAD_RECIPE_DATA;
  }
  
  // Check if the recipe buffers are unlocked in all the PLCs
  plcNames = mappingKeys(plcDpes);
  iRetVal = _unRecipeFunctions_canActivateRecipeInPlcs(sRcpInstanceDp, plcNames, dsBusyPlcs, exceptionInfo);
  if (iRetVal<0) {
    string sMessage;
    fwGeneral_dynStringToString(dsBusyPlcs, sMessage, ", ");
    _unRecipeFunctions_resetRecipeActivation(iRetVal, sRcpInstanceDp, sRcpName, sMessage);
	return RCP_ACT_RETVAL_PLC_BUSY;
  }
  
  // Create the recipe header buffer for the PLCs 
  iLen = dynlen(plcNames);
  for (int i=1; i<=iLen; i++) {
	_unRecipeFunctions_getHeaderBuffer(plcNames[i]);
  }
  
  unRecipeFunctions_setRecipeStatusInfo(RCP_ACTIVATION_TRIGGERED);
  unRecipeFunctions_writeInRecipeLog("Activating recipe: " + sRcpName, TRUE);
  
  // Format all the recipe values and saves the formatted values 
  // This is necessary to avoid problems if the formats have been modified and the recipe values don't have the correct formats
  unRecipeFunctions_formatValues(rcpInstance[fwConfigurationDB_RO_DPE_NAME], rcpInstance[fwConfigurationDB_RO_VALUE], 
                                     rcpInstance[fwConfigurationDB_RO_VALUE], exceptionInfo);
  
  fwConfigurationDB_saveRecipeToCache(rcpInstance, "", sSystemName + sRcpName, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    _unRecipeFunctions_resetRecipeActivation(RCP_ACT_RETVAL_BAD_RECIPE_DATA, sRcpInstanceDp, sRcpName, "");
    unRecipeFunctions_unlockRecipeBuffers(plcNames, exceptionInfo);
    _unRecipeFunctions_removeRecipeHeaderBuffers(plcNames);
	return RCP_ACT_RETVAL_BAD_RECIPE_DATA;
  }
    
  // Recipe activation
  iActivationResult = _unRecipeFunctions_activateRecipeByPLC(plcDpes, sRcpInstanceDp, exceptionInfo);  
  
  // Enables the recipe class combo box, the recipe instances table and the recipe elements table
  _unRecipeFunctions_setRecipeInstancePanelComponentsEnabled(true);
  
  _unRecipeFunctions_removeRecipeHeaderBuffers(plcNames);
  bool releaseResult = unRecipeFunctions_unlockRecipeBuffers(plcNames, exceptionInfo);
  if (releaseResult == false) {
	unRecipeFunctions_writeInRecipeLog("Error releasing the recipe buffers: " + exceptionInfo[2]);
	DebugTN(exceptionInfo);
  }
  
  // Reset the recipe state
  dpSet(sRcpInstanceDp+".ProcessInput.State", "");
  if (iActivationResult<0)
  {
    if ( dynlen(exceptionInfo) )
    // sometimes recipe activation fails but the exceptionInfo is cleared in the call to
    // unRecipeFunctions_unlockRecipeBuffers; this prevents printing an empty dyn_string in the log
    {
    	DebugTN(exceptionInfo); 
    }
  	return iActivationResult;
  }
  
  return RCP_ACT_RETVAL_ACTIVATION_OK;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Reset the recipe activation after an error or a successful activation.
 * @param iCode          - [IN] Error code during the recipe activation.
 * @param sRcpInstanceDp - [IN] Datapoint name of the recipe instance being activated.
 * @param sRcpName       - [IN] Name of the recipe being activated.
 * @param sMessage       - [IN] Message to display in the log.
 */
private void _unRecipeFunctions_resetRecipeActivation(int iCode, string sRcpInstanceDp, string sRcpName, string sMessage) {

  _unRecipeFunctions_setRecipeInstancePanelComponentsEnabled(true);
  dpSet(sRcpInstanceDp+".ProcessInput.State", "");
  mappingRemove(unRecipe_recipeActivationInfo, sRcpInstanceDp);
  
  switch(iCode) {
    case WRONG_NUMBER_OF_DPE_AND_VALUE:
      unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' can't be activated: The number of recipe elements and values are not the same.");
      break;
        
    case RECIPE_BUFFERS_DONT_EXIST:
      unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' can't be activated: The recipe buffers don't exist.");
      break;
    
    case BUFFER_OVERFLOW:
      unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' can't be activated: The number of recipe elements is bigger than the recipe buffer size.");
      break;
      
    case INVALID_RECIPE_DPE:
      unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' can't be activated: Wrong recipe elements.");
      break;
      
    case RCP_ACT_RETVAL_BAD_PLC_STATUS:
      unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' can't be activated: There isn't connection with the PLC(s): " + sMessage + ".");
      break;
      
    case RCP_ACT_RETVAL_PLC_BUSY: 
      unRecipeFunctions_writeInRecipeLog("Another recipe is being activated in the PLC(s): " + sMessage);
      unRecipeFunctions_writeInRecipeLog("Please wait until the recipe activation is completed.");
      unRecipeFunctions_writeInRecipeLog("If this message persists, you may be stuck. Please try restarting your HMI to see if that fixes the problem.");
      break;
      
    case RCP_ACT_RETVAL_BAD_RECIPE_DATA:
      unRecipeFunctions_writeInRecipeLog("Error formatting the recipe values: the recipe activation is cancelled.");
	  unRecipeFunctions_setRecipeStatusInfo(RCP_ACTIVATION_FAILED);
      break;
    
    case RCP_ACT_RETVAL_ACTIVATION_OK:
    case ERROR_LOADING_RECIPE:
      break;
    case VALUE_OUT_OF_RANGE:
      unRecipeFunctions_writeInRecipeLog("Error: The value of some recipe elements is out of range.");
      break;
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Remove the recipe header buffers associated to the specified PLC names.
 * @param daPlcNames - [IN] The PLC names which recipe header buffers must be removed.
 */
private void _unRecipeFunctions_removeRecipeHeaderBuffers(dyn_anytype daPlcNames) {
  int iLen;
  
  iLen = dynlen(daPlcNames);
  for (int i=1; i<=iLen; i++) {
	mappingRemove(gHeaderMap, daPlcNames[i]);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Unlock the recipe buffers used during the recipe activation.
 * @param daPlcNames - [IN] The PLC names which recipe header buffers must be unlocked.
 * @param exceptionInfo - [OUT] Standard exception handling variable. 
 * @return TRUE if the recipe buffers were successfully unlocked, otherwise false.
 */
public bool unRecipeFunctions_unlockRecipeBuffers(dyn_anytype daPlcNames, dyn_string &exceptionInfo) {
  dyn_string dsDpNames;

  dynClear(exceptionInfo);
  
  // Get the recipe buffer DP names for the plcs
  int iLen = dynlen(daPlcNames);
  for (int i=1; i<=iLen; i++) {
    string sDpName;
    _unRecipeFunctions_getRecipeBuffersName(daPlcNames[i], sDpName, exceptionInfo);
    dynAppend(dsDpNames, sDpName);
  }
  
  return _unRecipeFunctions_unlockDevices(dsDpNames, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the recipe elements according to the PLC where the elements are located.
 * @param dsElements    - [IN]  Recipe data point elements.
 * @param dsValues      - [IN]  Recipe values.
 * @param plcDpes       - [OUT] Map containing the recipe elements and values for each PLC.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */  
private int _unRecipeFunctions_getElementsByPLC(dyn_string dsElements, dyn_anytype dsValues, 
                                         mapping &plcDpes, dyn_string &exceptionInfo) {
  int rc, iElements, iBufferLength;
  string sBufferDpname, sPlcName, sSystemName;
  dyn_dyn_string data;
  mappingClear(plcDpes);
  
  int len = dynlen(dsElements);
  if (len!=dynlen(dsValues)) {
    fwException_raise(exceptionInfo, "ERROR", "The number of DPEs and values of the recipe must be equal.", "");
    return WRONG_NUMBER_OF_DPE_AND_VALUE;
  }  
  
  // Inserts the DPEs in the mapping according to the PLC name
  for (int i=1; i<=len; i++) {
    if (!dpExists(dsElements[i])) {
      return INVALID_RECIPE_DPE;
    }
    sSystemName = unGenericDpFunctions_getSystemName(dsElements[i]);
    sPlcName = unGenericObject_GetPLCNameFromDpName(dsElements[i]);
    
    dynClear(data);
    if (mappingHasKey(plcDpes, sSystemName+sPlcName)) {
      data = plcDpes[sSystemName+sPlcName];
    }

    dynAppend(data[1], dsElements[i]);
    dynAppend(data[2], dsValues[i]);
    plcDpes[sSystemName+sPlcName] = data;
  }    
  
  // Check that the number of elements in the recipe (for each PLC) isn't bigger than the recipe buffers size
  len = mappinglen(plcDpes);
  for (int i=1; i<=len; i++) {
    data = mappingGetValue(plcDpes, i);
	iElements = dynlen(data[1]);
	sPlcName = mappingGetKey(plcDpes, i);
	
	bool bResult = _unRecipeFunctions_getRecipeBuffersName(sPlcName, sBufferDpname, exceptionInfo);
	if (!bResult) { 
        DebugTN(exceptionInfo); 
        return RECIPE_BUFFERS_DONT_EXIST; 
    }

	rc = dpGet(sBufferDpname + "ProcessInput.BuffersLength", iBufferLength);
	if (iElements > iBufferLength) {
		fwException_raise(exceptionInfo, "ERROR", "The number of recipe elements (" + iElements + ") is bigger than the PLC buffers length (" + iBufferLength + ") for the PLC: " + sPlcName, "");
		DebugTN(exceptionInfo);
		return BUFFER_OVERFLOW;
	}
  }
  
  return 0;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe buffers DPE name for an specific PLC.
 * @param plcName - [IN] PLC name.
 * @param dpName  - [OUT] Name of the DPE where the recipe buffers of the PLC are located.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private bool _unRecipeFunctions_getRecipeBuffersName(string plcName, string &dpName, dyn_string &exceptionInfo) {
  dpName = plcName;
  
  // Removes the system name of the plcName
  plcName = unExportTree_removeSystemName(plcName);
  
  string buffersName = dpName + "_" + CPC_CONFIG_RECIPEBUFFERS_DPT_NAME;
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(buffersName, dpName);  
 
  if (!dpExists(dpName)) {
    fwException_raise(exceptionInfo, "ERROR", "The PLC recipe buffers don't exist: " + dpName, "");
	DebugTN(exceptionInfo);
    return FALSE;
  }

  return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Check if a recipe can be activated in the specified PLCs.
 * The recipe can't be activated in a PLC if there is other recipe being activated in that PLC (i.e. the recipe buffer is locked).
 * @param sRcpInstanceDp - [IN] DPE name of the recipe instance.
 * @param dsPlcNames     - [IN] List of PLC names.
 * @param dsBusyPlcs     - [OUT] List of busy PLCs.  
 * @param exceptionInfo  - [OUT] Standard exception handling variable.  
 * @return Integer < 0 if the recipe can not be activated in the PLCs, otherwise 0
 */
synchronized private int _unRecipeFunctions_canActivateRecipeInPlcs(string sRcpInstanceDp, dyn_anytype dsPlcNames, dyn_string &dsBusyPlcs, dyn_string &exceptionInfo) {
  dynClear(dsBusyPlcs);
  dyn_string dsPlcBufferDpNames, dsBusyDeviceDps, dsBusyDeviceAlias;
  
  int iLen = dynlen(dsPlcNames);
  // Get the recipe buffer dp names
  for (int i=1; i<=iLen; i++) {
	string sDpName;
	if (!_unRecipeFunctions_getRecipeBuffersName(dsPlcNames[i], sDpName, exceptionInfo)) {
      return RCP_ACT_RETVAL_PLC_BUSY;
    }
	dynAppend(dsPlcBufferDpNames, sDpName);
  }
  
  // Try to lock the recipe buffers
  _unRecipeFunctions_createRecipeActivationInfo(dsPlcNames, sRcpInstanceDp);
  if (!_unRecipeFunctions_lockDevices(dsPlcBufferDpNames, dsBusyDeviceDps, dsBusyDeviceAlias, exceptionInfo)) {
    if (dynlen(dsBusyDeviceDps)) {
      // Get the list of busy PLCs
      for (int i=1; i<=dynlen(dsBusyDeviceDps); i++) {
        int iPos = dynContains(dsPlcBufferDpNames, dsBusyDeviceDps[i]);
        if (iPos>0) {
          dynAppend(dsBusyPlcs, dsPlcNames[iPos]);
        }
      }
    }
    return RCP_ACT_RETVAL_PLC_BUSY;
  }

  return 0;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the time when the recipe activation must be cancelled (if the activation wasn't completed).
 * @param plcNames        - [IN] The list of PLC names where the recipe will be activated.
 * @param tExpirationTime - [OUT] The time when the recipe activation must be cancelled.
 * @param iTimeoutSec     - [OUT] Timeout value in seconds. 
 * @param exceptionInfo   - [OUT] Standard exception handling variable.  
 */
public void unRecipeFunctions_getRecipeActivationTimeout(dyn_anytype plcNames, time &tExpirationTime, int &iTimeoutSec, dyn_string & exceptionInfo) {
  int rc, iLen;
  dyn_int timeouts;
  time tStart;
  string sBuffersAlias, sBuffersName;
  
  iLen = dynlen(plcNames);
  for (int i=1; i<=iLen; i++) {  
	  // Get the recipe buffers name for the current PLC
	  _unRecipeFunctions_getRecipeBuffersName(plcNames[i], sBuffersAlias, exceptionInfo);  
	  if (dynlen(exceptionInfo))  { 
        DebugTN(exceptionInfo); 
        return; 
      }
	  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sBuffersAlias, sBuffersName);

	  // Get the activation timeout from the recipe buffers
	  rc = dpGet(sBuffersName+"ProcessInput.ActivationTimeout", iTimeoutSec);
	  dynAppend(timeouts, iTimeoutSec);
  }	  

  // The activation timeout is the minimum timeout of all the PLCs
  iTimeoutSec = dynMax(timeouts);
  
  // Activation timeout
  tStart = getCurrentTime();
  tExpirationTime   = makeTime((unsigned)year(tStart), 
                    (unsigned)month(tStart), 
                    (unsigned)day(tStart), 
                    (unsigned)hour(tStart), 
                    (unsigned)minute(tStart), 
                    (unsigned)second(tStart)+(unsigned)iTimeoutSec);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Triggers the recipe activation in all the PLCs containing recipe elements.
 * @param plcDpes - [IN] DPE name of the PLCs where the recipe must be activated.
 * @param sRcpInstanceDp - [IN] DPE name of the recipe instance.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */ 
private int _unRecipeFunctions_activateRecipeByPLC(mapping plcDpes, string sRcpInstanceDp, dyn_string &exceptionInfo)
{  
  int iLen, iTimeoutSec, iActivationResult;
  time tExpirationTime;
  
  unRecipeFunctions_getRecipeActivationTimeout(mappingKeys(plcDpes), tExpirationTime, iTimeoutSec, exceptionInfo);
  
  // Initializes the recipe activation progress bar
  if (shapeExists("ActivationProgressBar")) { 
    unProgressBar_setMax("ActivationProgressBar", iTimeoutSec);
	unProgressBar_setPosition("ActivationProgressBar", iTimeoutSec);
  }  
  
  // Start the recipe activation in all the PLCs (sending the recipe header)
  iLen = mappinglen(plcDpes);
  for (int i=1; i<=iLen; i++) {
    string sPlcName = mappingGetKey(plcDpes, i);
    string sPlcType = dpTypeName(sPlcName); 
    dyn_dyn_string recipeData = mappingGetValue(plcDpes, i);  
    
    if (sPlcType==UN_PLC_DPTYPE || sPlcType==S7_PLC_DPTYPE){
      bool bActivationInitOk = _unRecipeFunctions_activateRecipe(sPlcName, sPlcType, sRcpInstanceDp, recipeData[1], recipeData[2], exceptionInfo);
      if (!bActivationInitOk) {
        unRecipeFunctions_cancelRecipeActivation(sRcpInstanceDp, exceptionInfo, exceptionInfo[1]);
        return RCP_ACT_RETVAL_ACTIVATION_ERROR;
      }
    } else {
      // Unknown PLC
      fwException_raise(exceptionInfo, "ERROR", "The PLC type is unknown for the PLC:" + sPlcName, "");
	  unRecipeFunctions_cancelRecipeActivation(sRcpInstanceDp, exceptionInfo);
      return RCP_ACT_RETVAL_BAD_PLC_TYPE;      
    }
  }
  
  iActivationResult = _unRecipeFunctions_waitUntilActivationIsCompleted(sRcpInstanceDp, tExpirationTime, exceptionInfo);
  if (iActivationResult==RCP_ACT_RETVAL_ACTIVATION_TIMEOUT) {
	// Recipe activation timeout: Cancel the recipe activation in all the PLCs
	unRecipeFunctions_cancelRecipeActivation(sRcpInstanceDp, exceptionInfo);
	return RCP_ACT_RETVAL_ACTIVATION_TIMEOUT;
  }
  return iActivationResult;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Creates a matrix containing the necessary information to activate a recipe and stores it in the map unRecipeActivationInfo.
 *	 Row 1: List of PLCS containing recipe elements.
 *	 Row 2: List containing the names of the PLCs that are ready to activate the recipe.
 *	 Row 3: List of PLC names where the recipe has been activated.
 */
synchronized private void _unRecipeFunctions_createRecipeActivationInfo(dyn_anytype plcNames, string sRcpInstanceDp) {
	dyn_dyn_anytype recipeActivationInfo;
	dyn_string plcsReadyToActivate;
	dyn_string recipeAcivatedInPLCs;
	
	dynAppend(recipeActivationInfo, plcNames);
	dynAppend(recipeActivationInfo, plcsReadyToActivate);
	dynAppend(recipeActivationInfo, recipeAcivatedInPLCs);
    dynAppend(recipeActivationInfo, myManNum() + ";" + getSystemName());
	unRecipe_recipeActivationInfo[sRcpInstanceDp] = recipeActivationInfo;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Increment the number of PLCs ready to activate the recipe. When all the PLCs are ready the recipe activation is triggered in all of them.
 * @param sPlcName - [IN] The name of the PLC ready to activate the recipe.
 * @param sRcpInstanceDp - [IN] The data point of the recipe instance to be activated.
 * @return TRUE if all the PLCs containing recipe elements are ready to activate, otherwise FALSE.
 */
synchronized private bool _unRecipeFunctions_incrementNumPlcsReadyToActivate(string sPlcName, string sRcpInstanceDp) {
	return _unRecipeFunctions_addPlcToRecipeActivationInfo(PLCS_READY_TO_ACTIVATE_INDEX, sPlcName, sRcpInstanceDp);
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Increment the number of PLCs ready to activate the recipe. When all the PLCs are ready the recipe activation is triggered in all of them.
 * @param sPlcName - [IN] The name of the PLC where the recipe has been activated.
 * @param sRcpInstanceDp - [IN] The data point of the recipe instance activated in the PLC.
 * @return TRUE if the recipe has been activated in all the PLCs containing recipe elements, otherwise FALSE.
 */
synchronized private bool _unRecipeFunctions_incrementNumPlcsActivatedRecipe(string sPlcName, string sRcpInstanceDp) {
	return _unRecipeFunctions_addPlcToRecipeActivationInfo(PLCS_ACTIVATED_RECIPE_INDEX, sPlcName, sRcpInstanceDp);
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Adds the PLC name to the unRecipe_recipeActivationInfo in the index specified as a parameter.
 * @param index          - [IN] Index where to add the PLC name in the recipe activation info matrix.
 * @param sPlcName       - [IN] The PLC name to add in the recipe activation info matrix.
 * @param sRcpInstanceDp - [IN] DPE name of the recipe instance.
 * @return TRUE if the number of PLCs added in the matrix row is equal to the number of PLCs containing recipe elements, otherwise FALSE.
 */
synchronized private bool _unRecipeFunctions_addPlcToRecipeActivationInfo(int index, string sPlcName, string sRcpInstanceDp) {
	dyn_dyn_anytype recipeActivationInfo;
	dyn_string plcs;
	int iPlcsReady, iPlcsNumber;
	
	recipeActivationInfo = unRecipe_recipeActivationInfo[sRcpInstanceDp];
	plcs = recipeActivationInfo[index];
	if (dynContains(plcs, sPlcName)) {
		return false;
    }
		
	dynAppend(plcs, sPlcName);
	
	iPlcsReady = dynlen(plcs);
	recipeActivationInfo[index] = plcs;
	unRecipe_recipeActivationInfo[sRcpInstanceDp] = recipeActivationInfo;
	iPlcsNumber = dynlen(recipeActivationInfo[PLCS_REQUIRED_FOR_ACTIVATION_INDEX]);
	
	if (iPlcsNumber == iPlcsReady) {
		return true;
    }
    
	return false;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Wait until the recipe activation is completed or the timeout expires.
 * @param sRcpInstanceDp - [IN] DPE name of the recipe instance.
 * @param tEnd - [IN] Expiration time for the recipe activation.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private int _unRecipeFunctions_waitUntilActivationIsCompleted(string sRcpInstanceDp, time tEnd, dyn_string & exceptionInfo) {

  bool bActivationProgressBarExists;
  int iSleepTime=1, iProgressBarValue=0;
  string sRecipeState, sRemainingTime;
  time tCurrent;
  
  bActivationProgressBarExists = shapeExists("ActivationProgressBar");
  if (bActivationProgressBarExists) {
    getValue("ActivationProgressBar", "progress", iProgressBarValue);
  }
  
  // Loop until the recipe activation is finished or the timeout has expired  
  while ((tCurrent = getCurrentTime()) < tEnd) {
    dpGet(sRcpInstanceDp+".ProcessInput.State", sRecipeState);
    if (sRecipeState!=UN_RCP_INSTANCE_STATE_ACTIVATE) {
      // Recipe activation finished
      break;
    }

    delay((unsigned) iSleepTime);
    // Updates the Activation Timeout progress bar
    if (bActivationProgressBarExists) {	
      iProgressBarValue = iProgressBarValue - iSleepTime;
      unProgressBar_setPosition("ActivationProgressBar", iProgressBarValue);
    }	
	
    // if the selection timeout is less than the activation timeout, selects the recipe instance
    dpGet(getSystemName() + "_unSelectDeselect.remainingTime", sRemainingTime);
    if (iProgressBarValue > (int)(strsplit(sRemainingTime, ";")[3])) {
      unGenericObject_ButtonSelectAction($sDpName, true, exceptionInfo);
    }
  } 
  
  // Resets the progress bar
  if (bActivationProgressBarExists) {
	unProgressBar_setPosition("ActivationProgressBar", 0);
  }
  
  // Check if the activation timeout has expired
  if (tCurrent >= tEnd && sRecipeState==UN_RCP_INSTANCE_STATE_ACTIVATE) {
    return RCP_ACT_RETVAL_ACTIVATION_TIMEOUT;
  } 
  
  if (sRecipeState == UN_RCP_INSTANCE_STATE_CANCELLED) {
    return RCP_ACT_RETVAL_PLC_CANCELLATION;
  }
 
  return _unRecipeFunctions_checRecipeActivation(sRcpInstanceDp);
}
//------------------------------------------------------------------------------------------------------------------------
private int _unRecipeFunctions_checRecipeActivation(string sRcpInstanceDp) {
  string sErrDpe;
  dyn_string exceptionInfo;
  dyn_dyn_mixed recipeObject;
  
  _unRecipeFunctions_getRecipeObjectFromDp(sRcpInstanceDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo); 
    return RCP_ACT_RETVAL_BAD_RECIPE_DATA;
  }

  dyn_string rcpElements = recipeObject[fwConfigurationDB_RO_DPE_NAME];
  dyn_string rcpValues   = recipeObject[fwConfigurationDB_RO_VALUE];
	  
  bool bOnlineValuesOk = _unRecipeFunctions_checkRecipeOnlineValues(rcpElements, rcpValues, sErrDpe, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    // The DPEs of some recipe elements don't exist.
    unRecipeFunctions_setRecipeStatusInfo(RCP_ACTIVATION_FAILED);
    unRecipeFunctions_writeInRecipeLog("Recipe activation failed: The recipe has wrong elements.");	
    return RCP_ACT_RETVAL_BAD_RECIPE_DATA;
  } else if(!bOnlineValuesOk) {
    _unRecipeFunctions_recipeActivationWrongOnlineValues(sErrDpe); 
    return RCP_ACT_RETVAL_WRONG_ONLINE_VALUES;
  } else {
	// Updates the recipe meta info
    _unRecipeFunctions_recipeActivationUpdateMetaInfo(recipeObject, sRcpInstanceDp);
    return RCP_ACT_RETVAL_ACTIVATION_OK;
  } 
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Activates a recipe in a PLC.
 * @param plcName - [IN] PLC name.
 * @param plcType - [IN] PLC type.
 * @param sRcpInstanceDp - [IN] DPE name of the recipe instance.
 * @param dpes - [IN] List of recipe DPEs for the PLC.
 * @param values - [IN] List of values for the recipe DPEs.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return TRUE if the recipe activation was initiated correctly, otherwise FALSE.
 */
private bool _unRecipeFunctions_activateRecipe(string plcName, string plcType, string sRcpInstanceDp, dyn_string dpes, 
                                              dyn_string values, dyn_string &exceptionInfo)
{
  string sSystemName, dpBuffersAlias, dpBuffersName, sRemainingTime;
  int iActivationTimeout, iTimeOut;
  
  // Check if the current PLC is in a distributed system and if it's connected
  sSystemName = unGenericDpFunctions_getSystemName(plcName);
  if (!unRecipeFunctions_isRemoteSystemConnected(sSystemName, exceptionInfo)) {
    return false;
  }
  
  // Get the recipe buffers name for the current PLC
  _unRecipeFunctions_getRecipeBuffersName(plcName, dpBuffersAlias, exceptionInfo);  
  if (dynlen(exceptionInfo)) { 
    DebugTN(exceptionInfo); 
    return false; 
  }
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(dpBuffersAlias, dpBuffersName);			
  
  // if the selection timeout is less than the activation timeout, selects the recipe instance
  dpGet(getSystemName() + "_unSelectDeselect.remainingTime", sRemainingTime,
        getSystemName() + "_unSelectDeselect.timeOut", iTimeOut);
  if (iActivationTimeout > (int)(strsplit(sRemainingTime, ";")[3])) {
	unGenericObject_ButtonSelectAction($sDpName, true, exceptionInfo);
	if (shapeExists("SelectionProgressBar")) {
		unProgressBar_setPosition("SelectionProgressBar", iTimeOut);
	}
  }
 
  _unRecipeFunctions_createBuffersData(plcName, dpes, values, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo); 
    return false;
  }
  
  if (plcType==S7_PLC_DPTYPE) {
	// If the PLC is SIEMENS creates the recipe header DB numbers
	_unRecipeFunctions_createRecipeHeaderDBNumber(plcName, dpes, exceptionInfo); 
	if (dynlen(exceptionInfo)) {
      DebugTN(exceptionInfo); 
      return false;
    }  
  }
  
  unRecipeFunctions_writeInRecipeLog("Sending recipe data to the PLC " + plcName);
  _unRecipeFunctions_sendRecipeHeader(plcName, exceptionInfo);

  dpConnect("_unRecipeFunctions_checkRecipeStatusBufferCB", false,
				dpBuffersName+"ProcessInput.Status.RecipeStatus", 
				sRcpInstanceDp+".ProcessInput.State");
  
  return true;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Send the recipe header buffer to the PLC.
 * @param sPlcName      - [IN]  PLC Name.
 * @param exceptionInfo - [OUT] Standard error handling variable.
 * @param bWaitForDpSet - [IN]  TRUE if the dp set operation on the recipe buffers must be synchronous, otherwise FALSE.
 */
private void _unRecipeFunctions_sendRecipeHeader(string sPlcName, dyn_string &exceptionInfo, bool bWaitForDpSet=TRUE)
{
  int iLen;
  string sDpName, sSystemName;
  dyn_string recipeHeaderDpes;
  dyn_int recipeHeaderValues;
  
  _unRecipeFunctions_getRecipeBuffersName(sPlcName, sDpName, exceptionInfo);
  if (strpos(sPlcName, ":") < 0) {
	// Adds the system name to the PLC name
	sSystemName = unGenericDpFunctions_getSystemName(sPlcName);
	sPlcName = sSystemName + sPlcName;
  }
  
  // Get the recipe header values
  recipeHeaderValues = _unRecipeFunctions_getHeaderBuffer(sPlcName);
  
  // Creates the strings to represent the recipe header dpes
  iLen = dynlen(recipeHeaderValues);
  for (int i=1; i<=iLen; i++) {
	recipeHeaderDpes[i] = sDpName + "ProcessOutput.Header[" + i + "]";
  }
          
  if (bWaitForDpSet) {
    dpSetWait(recipeHeaderDpes, recipeHeaderValues);
  } else {
    dpSet(recipeHeaderDpes, recipeHeaderValues);
  }
 
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Callback function executed during the recipe activation when the recipe status has changed.
 * @param sRecipeStatusDp - [IN] DP Name of the recipe buffer status.
 * @param recipeStatus    - [IN] Value of the recipe buffer status.
 * @param sRecipeState    - [IN] DP Name of the recipe state.
 * @param sState          - [IN] Value of the recipe state.
 *
 * @reviewed 2018-09-12 @whitelisted{Callback}
 */
void _unRecipeFunctions_checkRecipeStatusBufferCB(string sRecipeStatusDp, int recipeStatus, string sRecipeState, string sState) {
  string sBuffersDpName, plcName, sRcpInstanceDp;
    	
  // Get the PLC name
  plcName = unGenericObject_GetPLCNameFromDpName(sRecipeStatusDp);
  if (strpos(plcName, ":")<0) {
	plcName = unGenericDpFunctions_getSystemName(sRecipeStatusDp) + plcName;
  }
  
  // Get the PLC buffers dp name
  int pos = strpos(sRecipeStatusDp, "ProcessInput");
  if (pos>=0) {
	sBuffersDpName = substr(sRecipeStatusDp, 0, pos);
  }
  
  // Get the recipe timestamp from the header and status buffers
  int headerTimeLow, headerTimeHigh, statusTimeLow, statusTimeHigh;
  dpGet(sBuffersDpName + "ProcessOutput.Header[1]:_online.._value", headerTimeLow,
		sBuffersDpName + "ProcessOutput.Header[2]:_online.._value", headerTimeHigh,
		sBuffersDpName + "ProcessInput.Status[1]:_online.._value", statusTimeLow,
		sBuffersDpName + "ProcessInput.Status[2]:_online.._value", statusTimeHigh);
  
  if (headerTimeLow != statusTimeLow || headerTimeHigh != statusTimeHigh){
    // If the timestamp are not equal means that the callback function is called 
	// after a TSPP update and the status buffer doesn't correspond to the current
	// recipe activation. Ignore.
	return;
  }
  
  // Get the recipe DP name
  pos = strpos(sRecipeState, ".");
  if (pos>=0) {
	sRcpInstanceDp = substr(sRecipeState, 0, pos);
  }
	    
  switch (recipeStatus){
    case RCP_ACT_STATUS_ERROR:
      _unRecipeFunctions_recipeActivationStatusError(plcName, sRecipeStatusDp, sRecipeState);
      break;
  
    case RCP_ACT_STATUS_STANDBY:
      break;
      
    case RCP_ACT_STATUS_NEW_ID_RECEIVED:
      _unRecipeFunctions_recipeActivationStatusNewIdReceived(plcName, sBuffersDpName, sRcpInstanceDp);
      break;

    case RCP_ACT_STATUS_CRC_OK:
      _unRecipeFunctions_recipeActivationStatusCrcOk(plcName, sBuffersDpName, sRcpInstanceDp);
      break;      

    case RCP_ACT_STATUS_DONE:
      _unRecipeFunctions_recipeActivationStatusDone(plcName, sRecipeStatusDp, sRecipeState, sRcpInstanceDp);
      break;
	  
	default:
	  unRecipeFunctions_writeInRecipeLog("Recipe activation error: Unknown recipe status value (" + recipeStatus + ")", TRUE);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function executed during the recipe activation when the recipe status is in error.
 * @param sPlcName        - [IN] Name of the PLC where the recipe is being activated.
 * @param sRecipeStatusDp - [IN] Dp name of the recipe status buffer.
 * @param sRecipeStateDp  - [IN] Dp name of the recipe state.
 */
private void _unRecipeFunctions_recipeActivationStatusError(string sPlcName, string sRecipeStatusDp, string sRecipeStateDp) {
  unRecipeFunctions_writeInRecipeLog("Recipe activation error in PLC " + sPlcName, TRUE);
  dpDisconnect("_unRecipeFunctions_checkRecipeStatusBufferCB", sRecipeStatusDp, sRecipeStateDp);
  strreplace(sRecipeStateDp, ":_online.._value", "");
  dpSet(sRecipeStateDp, UN_RCP_INSTANCE_STATE_CANCELLED);
  unRecipeFunctions_setRecipeStatusInfo(RCP_ACTIVATION_FAILED);
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function executed during the recipe activation when the PLC confirms that a new recipe activation has been triggered.
 * @param sPlcName       - [IN] Name of the PLC where the recipe is being activated.
 * @param sBuffersDpName - [IN] Dp name of the recipe buffers.
 * @param sRcpInstanceDp - [IN] Dp name of the recipe instance.
 */
private void _unRecipeFunctions_recipeActivationStatusNewIdReceived(string sPlcName, string sBuffersDpName, string sRcpInstanceDp) {
  dyn_string exceptionInfo;
  dyn_int recipeHeaderBuffer;
  
  bool checkResult = _unRecipeFunctions_checkRecipeStatusBufferData(sBuffersDpName, exceptionInfo);  
  if (false==checkResult) {
	// Wrong data in the recipe status buffer
	unRecipeFunctions_writeInRecipeLog("Activation error in PLC " + sPlcName + ": " + exceptionInfo[2]);
	unRecipeFunctions_cancelRecipeActivation(sRcpInstanceDp, exceptionInfo, "Bad status data");
  } else {
	// The data in the recipe status buffer is Ok
	// 1. Send the recipe data (ManRegAddr, ManRegVal, ReqAddr and ReqVal buffers)
	_unRecipeFunctions_sendData(sPlcName, exceptionInfo);
	// 2. Send the recipe header buffer
	recipeHeaderBuffer = _unRecipeFunctions_getHeaderBuffer(sPlcName);
	recipeHeaderBuffer[RCP_HEADER_SENT_VALUE] = RCP_ACT_HEADER_DATA_SENT;
	gHeaderMap[sPlcName] = recipeHeaderBuffer;
	_unRecipeFunctions_sendRecipeHeader(sPlcName, exceptionInfo);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function executed during the recipe activation when the PLC confirms that the CRC value is Ok.
 * @param sPlcName       - [IN] Name of the PLC where the recipe is being activated.
 * @param sBuffersDpName - [IN] Dp name of the recipe buffers.
 * @param sRcpInstanceDp - [IN] Dp name of the recipe instance.
 */
private void _unRecipeFunctions_recipeActivationStatusCrcOk(string sPlcName, string sBuffersDpName, string sRcpInstanceDp) {
  dyn_string exceptionInfo;
  dyn_int recipeHeaderBuffer;
  
  bool checkResult = _unRecipeFunctions_checkRecipeStatusBufferData(sBuffersDpName, exceptionInfo);
  if (false==checkResult) {
    // Wrong data in the recipe status buffer
	unRecipeFunctions_writeInRecipeLog("Activation error in PLC " + sPlcName + ": " + exceptionInfo[2]);
	unRecipeFunctions_cancelRecipeActivation(sRcpInstanceDp, exceptionInfo, "Bad status data");
  } else {
	// Check if all the PLCs are ready to activate the recipe
	bool bPlcsReadyToActivate = _unRecipeFunctions_incrementNumPlcsReadyToActivate(sPlcName, sRcpInstanceDp);
	if (bPlcsReadyToActivate) {
      // If all the PLCs are ready to activate the recipe, send the activation order
      dyn_anytype allPlcNames = unRecipe_recipeActivationInfo[sRcpInstanceDp][1];
      for (int i=1; i<=dynlen(allPlcNames); i++) {
        // Send the activation order 
        string sCurrPlcName = allPlcNames[i];
        recipeHeaderBuffer = _unRecipeFunctions_getHeaderBuffer(sCurrPlcName);
        recipeHeaderBuffer[RCP_HEADER_SENT_VALUE] = RCP_ACT_HEADER_ACTIVATE;
        gHeaderMap[sCurrPlcName] = recipeHeaderBuffer;
        _unRecipeFunctions_sendRecipeHeader(sCurrPlcName, exceptionInfo);  
      }
	}
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function executed during the recipe activation when the PLC confirms that the recipe activation is completed.
 * @param sPlcName        - [IN] Name of the PLC where the recipe is being activated.
 * @param sRecipeStatusDp - [IN] Dp name of the recipe status buffer.
 * @param sRecipeStateDp  - [IN] Dp name of the recipe state.
 * @param sRcpInstanceDp  - [IN] Dp name of the recipe instance.
 */
private void _unRecipeFunctions_recipeActivationStatusDone(string sPlcName, string sRecipeStatusDp, string sRecipeStateDp, string sRcpInstanceDp) {
  
  dpDisconnect("_unRecipeFunctions_checkRecipeStatusBufferCB", sRecipeStatusDp, sRecipeStateDp);
	  
  // Is enough this delay?
  delay(1);  
  unRecipeFunctions_writeInRecipeLog("Recipe activation completed in PLC: " + sPlcName, TRUE);

  // Check if the activation is completed in all the PLCs
  if (_unRecipeFunctions_incrementNumPlcsActivatedRecipe(sPlcName, sRcpInstanceDp)) {
	// Resets the recipe instance state
	strreplace(sRecipeStateDp, ":_online.._value", "");
	dpSet(sRecipeStateDp, UN_RCP_INSTANCE_STATE_UNLOCK_BUFFERS);
	unRecipeFunctions_writeInRecipeLog("Unlocking recipe buffers...", true);
    mappingRemove(unRecipe_recipeActivationInfo, sRcpInstanceDp);
  }

  // Remove the recipe buffers for the PLC
  _unRecipeFunctions_removeRecipeBuffers(sPlcName);
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function executed after the recipe activation when the online values are different than the recipe values.
 * @param sErrDpe  - [IN] Dp name of the first recipe element which value doesn't match the PLC online value.
 */
private void _unRecipeFunctions_recipeActivationWrongOnlineValues(string sErrDpe) {
  string sPlcName;
  
  // Some online values are different than the recipe values.
  unGenericDpFunctions_convert_PVSSDPE_to_UNICOSDPE(sErrDpe, sErrDpe);
  unRecipeFunctions_setRecipeStatusInfo(RCP_ACTIVATION_FAILED);
  
  sPlcName = unGenericObject_GetPLCNameFromDpName(sErrDpe);
  strreplace(sPlcName, SOFT_FE_DPTYPE+"_", "");
  
  unRecipeFunctions_writeInRecipeLog("Recipe activation failed in PLC " + sPlcName + ": Recipe read back values are different than sent values");		
  unRecipeFunctions_writeInRecipeLog("The first different value corresponds to the data point: " + sErrDpe);
  if (patternMatch("*.MOutH", sErrDpe) || patternMatch("*.MOutL", sErrDpe)) {
    unRecipeFunctions_writeInRecipeLog("Please, verify that the recipe value is defined in the controlled object range.");
  } else if (patternMatch("*.MSPH", sErrDpe) || patternMatch("*.MSPL", sErrDpe)) {
    unRecipeFunctions_writeInRecipeLog("Please, verify that the recipe value is defined in the measured value range.");
  }        
}    
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function to update the recipe activation meta-info when the recipe activation was completed successfully.
 * @param recipeObject - [IN] Recipe object.
 * @param sRcpDp       - [IN] Datapoint name of the recipe instance.
 */
private void _unRecipeFunctions_recipeActivationUpdateMetaInfo(dyn_dyn_mixed recipeObject, string sRcpDp) {
  string sSystemName, sClassName, sInstName, sJcopRcpClassName, sJcopRcpInstName;
  time tActivationTime = getCurrentTime();
  
  sSystemName = unGenericDpFunctions_getSystemName(sRcpDp);
  // Updates the recipe meta info  
  recipeObject[fwConfigurationDB_RO_META_LASTACTIVATIONTIME] = tActivationTime;
  recipeObject[fwConfigurationDB_RO_META_LASTACTIVATIONUSER] = getUserName();
  recipeObject[fwConfigurationDB_RO_META_ORIGNAME] = unExportTree_removeSystemName(recipeObject[fwConfigurationDB_RO_META_ORIGNAME]);
  
  // TODO: Fails in distributed systems because of the recipe class name without dist_name
  // dyn_string exceptionInfo;
  //_fwConfigurationDB_activateRecipeUpdateMetaInfo(recipeObject, exceptionInfo);
  
  dpGet(sRcpDp + ".ProcessInput.ClassName", sClassName,
        sRcpDp + ".ProcessInput.InstanceName", sInstName);
  
  sJcopRcpClassName = sSystemName + fwConfigurationDB_RecipeClassDpPrefix + sClassName;
  if (dpExists(sJcopRcpClassName)) {
    dpSet(sJcopRcpClassName + ".LastActivatedOfThisType", unExportTree_removeSystemName(recipeObject[fwConfigurationDB_RO_META_ORIGNAME]),
          sJcopRcpClassName + ".LastActivationUser", getUserName(),
          sJcopRcpClassName + ".LastActivationTime", tActivationTime);
  } 
  
  sJcopRcpInstName = sSystemName + fwConfigurationDB_RecipeCacheDpPrefix + sClassName + "/" + sInstName;
  if (dpExists(sJcopRcpInstName)) {
    dpSet(sJcopRcpInstName + ".MetaInfo.LastActivationTime", tActivationTime,
          sJcopRcpInstName + ".MetaInfo.LastActivationUser", getUserName());
  } 
   
  _unRecipeFunctions_loadPropertiesTableInfo(sRcpDp, recipeObject);
  _unRecipeFunctions_loadHistoryTableInfo(sRcpDp, recipeObject);  
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Checks the data in the recipe status buffer to verify:
 * <ul>
 * 	<li>The received data corresponds to the data in the recipe header buffer.</li>
 *	<li>The timestamp received is synchronized with the PLC: PVSS Time +/- PLC Delay = received timestamp
 * </ul>
 * @param sBuffersDpName - [IN] DpName of the recipe buffers.
 * @param exceptionInfo  - [OUT] Standard exception handling variable.
 */
private bool _unRecipeFunctions_checkRecipeStatusBufferData(string sBuffersDpName, dyn_string &exceptionInfo)
{
	string sPlcName, sPlcType, sDelayDpName, sSystemName;
	time timeMin, timeMax, currentTime = getCurrentTime();
	dyn_string timestampDpes, statusValueDpes, headerValueDpes;
	dyn_string statusValues, headerValues, sDelay;
	dyn_time timestamps;
	
	// Creates strings containing the DPEs for: 
	//	 - Timestamp of the status buffer data
	//   - Status buffer data
	//   - Header buffer data
	int i=1;
	while (i<=RCP_STATUS_MAX_SIZE) {
		timestampDpes[i]   = sBuffersDpName + "ProcessInput.Status[" + i + "]:_online.._stime";
		statusValueDpes[i] = sBuffersDpName + "ProcessInput.Status[" + i + "]:_online.._value";
		headerValueDpes[i] = sBuffersDpName + "ProcessOutput.Header[" + i + "]:_online.._value";
		i++;
	}
	
	// Get the PLC Delay DPName
    sSystemName = unGenericDpFunctions_getSystemName(sBuffersDpName);
	sPlcName    = unGenericObject_GetPLCNameFromDpName(sBuffersDpName);
	sPlcType = dpTypeName(sSystemName + sPlcName);
	if (sPlcType==S7_PLC_DPTYPE) {
		sDelayDpName = sSystemName + "unicosS7PLC_systemIntegrityInfo.config.data";
	} else if (sPlcType==UN_PLC_DPTYPE) {
		sDelayDpName = sSystemName + "unicosPLC_systemIntegrityInfo.config.data";
	} else {
		// Unknown PLC type
		fwException_raise(exceptionInfo, "ERROR", "The PLC type is unknown: " + sPlcType +".", "");
		return false;
	}

	// Get all the necessary values
	delay(2);
	int rc = dpGet(timestampDpes, timestamps, statusValueDpes, statusValues, headerValueDpes, headerValues, sDelayDpName, sDelay);
	if (rc<0) {
		fwException_raise(exceptionInfo, "ERROR", "DpGet failed in _unRecipeFunctions_checkRecipeStatusBufferData() function.", "");
		return false;	
	}
	
	// Check that the received timestamp is in a valid range
	timeMin = makeTime((unsigned)year(currentTime), 
                       (unsigned)month(currentTime), 
                       (unsigned)day(currentTime), 
                       (unsigned)hour(currentTime), 
                       (unsigned)minute(currentTime), 
                       (unsigned)second(currentTime)-(unsigned)sDelay[1]);
	timeMax = makeTime((unsigned)year(currentTime), 
                       (unsigned)month(currentTime), 
                       (unsigned)day(currentTime), 
                       (unsigned)hour(currentTime), 
                       (unsigned)minute(currentTime), 
                       (unsigned)second(currentTime)+(unsigned)sDelay[1]);
	
	if (timestamps[RCP_STATUS_STATUS]<timeMin || timestamps[RCP_STATUS_STATUS]>timeMax){
		fwException_raise(exceptionInfo, "ERROR", "The timestamp of the received data is out of range.", "");
		return false;
	}
		
	// Check that all the 'mirrored' values corresponds to the values in the recipe header buffer
	for (i=1; i<=RCP_STATUS_MAX_SIZE && i<=RCP_STATUS_MIRROR_DATA_SIZE; i++) {
		if (headerValues[i]!=statusValues[i]) {
			fwException_raise(exceptionInfo, "ERROR", "The status buffer values are different than the header buffer values.", "");
			return false;
		}
	}

	return true;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Cancel the activation of a recipe.
 * @param sPlcName       - [IN] Name of the PLC where the recipe is being activated.
 * @param sBuffersDpName - [IN] DP Name of the recipe buffers for the specified PLC.
 * @param sRecipeState   - [IN] Recipe state.
 * @param sReason        - [IN] Reason for the cancellation.
 * @param bDisconnect    - [IN] Flag to specify if the dpDisconnect of the recipe status must be done.
 * @param bTerminate     - [IN] TRUE if the function has been called from a panel termination.
 */
private void _unRecipeFunctions_cancelRecipeActivation (string sPlcName, string sBuffersDpName, string sRecipeState, string sReason, 
                                                bool bDisconnect=true, bool bTerminate=false) 
{
  dyn_string exceptionInfo;
	
  if (strpos(sPlcName, ":") < 0) {
   	// Adds the system name to the PLC name
   	string sSystemName = unGenericDpFunctions_getSystemName(sPlcName);
   	sPlcName = sSystemName + sPlcName;
  }
  
  dyn_int headerBuffer = _unRecipeFunctions_getHeaderBuffer(sPlcName);
	
  if (bDisconnect) {
    dpDisconnect("_unRecipeFunctions_checkRecipeStatusBufferCB", 
						sBuffersDpName+"ProcessInput.Status.RecipeStatus", 
						sRecipeState);  
  }

  headerBuffer[RCP_HEADER_SENT_VALUE] = RCP_ACT_HEADER_CANCEL;
  gHeaderMap[sPlcName] = headerBuffer;
  _unRecipeFunctions_sendRecipeHeader(sPlcName, exceptionInfo, false);
  dpSet(sRecipeState, "");
  if (!bTerminate) {
    unRecipeFunctions_setRecipeStatusInfo(RCP_ACTIVATION_FAILED, makeDynString());	
    unRecipeFunctions_writeInRecipeLog("Recipe activation cancelled in PLC " + sPlcName + ": " + sReason);
  }
  
  // Remove the recipe buffers for the PLC (To unlock the activation of more recipes in the PLC)
  _unRecipeFunctions_removeRecipeBuffers(sPlcName);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Cancel the activation of a recipe.
 * @param sRecipeDpName - [IN]  Datapoint name of the recipe which cancellation is requested.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @param sReason 		- [IN]  String containing the reason of the cancellation.
 * @param bTerminate    - [IN]  TRUE if the function has been called from a panel termination.
 */
synchronized void unRecipeFunctions_cancelRecipeActivation(string sRecipeDpName, dyn_string &exceptionInfo, string sReason="", bool bTerminate=FALSE) 
{
    int iLen;
    string sRcpBuffersDp, sPlcName, sRecipeState;

    // If the sRecipeDpName ends with '.', remove the dot character
    if (sRecipeDpName[strlen(sRecipeDpName)-1] == ".") {
        sRecipeDpName = substr(sRecipeDpName, 0, strlen(sRecipeDpName)-1);
    }

    // Check if the recipe is being activated, if not return.
    if (!mappingHasKey(unRecipe_recipeActivationInfo, sRecipeDpName)) {
        return;
    }
    
    // Verifies if the local manager that triggered the recipe activation is the same calling this function.
    dyn_dyn_anytype activationMatrix = unRecipe_recipeActivationInfo[sRecipeDpName];
    string sActivationManager = activationMatrix[LOCAL_MANAGER_INDEX][1];
    string sLocalManager = myManNum() + ";" + getSystemName();
    if (sActivationManager != sLocalManager) {
      return;
    }
	
    dpSet(sRecipeDpName+".ProcessInput.State", UN_RCP_INSTANCE_STATE_CANCELLED);
    dyn_dyn_anytype recipeActivationInfo = unRecipe_recipeActivationInfo[sRecipeDpName];
    dyn_anytype plcNames = recipeActivationInfo[1];

    iLen = dynlen(plcNames);
    for (int i=1; i<=iLen; i++) {
      sPlcName = plcNames[i];
      _unRecipeFunctions_getRecipeBuffersName(sPlcName, sRcpBuffersDp, exceptionInfo);
      sRecipeState = sRecipeDpName + ".ProcessInput.State";
      dpDisconnect("_unRecipeFunctions_checkRecipeStatusBufferCB",
        sRcpBuffersDp+"ProcessInput.Status.RecipeStatus", 
        sRecipeDpName+".ProcessInput.State");
      _unRecipeFunctions_cancelRecipeActivation(sPlcName, sRcpBuffersDp, sRecipeState, sReason, false, bTerminate);
    }
    
    if (bTerminate) {
      // The recipe activation panel has been terminated, unlock the recipe buffers
      dyn_string dsPlcBufferLock;
      dyn_int diLockValue;
      for (int i=1; i<=dynlen(plcNames); i++) {
        _unRecipeFunctions_getRecipeBuffersName(plcNames[i], sRcpBuffersDp, exceptionInfo);
        dynAppend(dsPlcBufferLock, sRcpBuffersDp + ":_lock._original._locked");
        dynAppend(diLockValue, 0);
      }
      dpSet(dsPlcBufferLock, diLockValue);  
    } else {
      // Unlock the recipe buffers normally
      unRecipeFunctions_unlockRecipeBuffers(plcNames, exceptionInfo);
    }
	
    _unRecipeFunctions_removeRecipeHeaderBuffers(plcNames);
    mappingRemove(unRecipe_recipeActivationInfo, sRecipeDpName);
    dpSet(sRecipeDpName+".ProcessInput.State", "");
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Create the recipe buffer data for a recipe activation.
 * @param sPlcName      - [IN]  Name of the PLC where the recipe is about to be activated.
 * @param dpes          - [IN]  Datapoint names of the recipe elements.
 * @param values        - [IN]  Values of the recipe elements.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private void _unRecipeFunctions_createBuffersData(string sPlcName, dyn_string dpes, dyn_string values, dyn_string &exceptionInfo)
{
  dyn_int dparManRegAddr, wparManRegAddr, aparManRegAddr, aalarmManRegAddr, pidManRegAddr;
  dyn_int dparManRegVal,  wparManRegVal,  aparManRegVal,  aalarmManRegVal,  pidManRegVal;
  dyn_int dparReqAddr, wparReqAddr, aparReqAddr, aalarmReqAddr, pidReqAddr;
  dyn_float wparReqVal,  aparReqVal,  aalarmReqVal,  pidReqVal;
  mapping controllerMap, aalarmMap;
  int numDPar=0, numWPar=0, numAPar=0, numAAlarm=0, numPid=0;  
  
  if (strpos(sPlcName, ":") < 0) {
	// Adds the system name to the PLC name
	string sSystemName = unGenericDpFunctions_getSystemName(sPlcName);
	sPlcName = sSystemName + sPlcName;
  }
  	
  int len = dynlen(dpes);
  for (int i=1; i<=len; i++){    
    // Get the DPE device type name
    string dpe = dpes[i];
    string deviceType = dpTypeName(dpe);
    string deviceName = unGenericDpFunctions_getDpName(dpe);
    string dpeName = substr(dpe, strlen(deviceName), strlen(dpe)-strlen(deviceName));
			
    switch (deviceType){
      case "CPC_DigitalParameter":    
        _unRecipeFunctions_addDigitalParameterToBuffers(numDPar, deviceName, values[i], dparManRegAddr, dparManRegVal, exceptionInfo);
        break;
        
      case "CPC_WordParameter": 
        _unRecipeFunctions_addXParameterToBuffers(numWPar, deviceName, "CPC_WordParameter", values[i], 
                WPAR_ARM_BIT, WPAR_NEWMR_BIT, wparManRegAddr, wparManRegVal, wparReqAddr, wparReqVal, exceptionInfo);
        break; 
        
      case "CPC_AnalogParameter":
        _unRecipeFunctions_addXParameterToBuffers(numAPar, deviceName, "CPC_AnalogParameter", values[i], 
                APAR_ARM_BIT, APAR_NEWMR_BIT, aparManRegAddr, aparManRegVal, aparReqAddr, aparReqVal, exceptionInfo);
        break;        
        
      case "CPC_AnalogAlarm":
        _unRecipeFunctions_addMultiDpeDeviceToBuffers(numAAlarm, deviceName, dpeName, "CPC_AnalogAlarm", values[i], 
            aalarmMap, unRecipe_aalarmManRegBits, AALARM_ARM_BIT, exceptionInfo);
        break;
        
      case "CPC_Controller":
        _unRecipeFunctions_addMultiDpeDeviceToBuffers(numPid, deviceName, dpeName, "CPC_Controller", values[i], 
            controllerMap, unRecipe_unRecipe_pidManRegBits, PID_ARM_BIT, exceptionInfo);
        break;
    }
    
    if (dynlen(exceptionInfo)) {
      return;
    }
  }
  
  // Gets all the recipe buffers
  dyn_int recipeHeaderBuffer = _unRecipeFunctions_getHeaderBuffer(sPlcName);
  dyn_int manRegAddrBuffer   = _unRecipeFunctions_getManRegAddrBuffer(sPlcName);
  dyn_int manRegValBuffer    = _unRecipeFunctions_getManRegValBuffer(sPlcName);
  dyn_int reqAddrBuffer      = _unRecipeFunctions_getReqAddrBuffer(sPlcName);
  dyn_float reqValBuffer   	 = _unRecipeFunctions_getReqValBuffer(sPlcName);
   
  _unRecipeFunctions_processMultiDpeDevices(controllerMap, pidManRegAddr, pidManRegVal, pidReqAddr, pidReqVal);
  _unRecipeFunctions_processMultiDpeDevices(aalarmMap, aalarmManRegAddr, aalarmManRegVal, aalarmReqAddr, aalarmReqVal);
  
  // Inserts the data in the recipe buffers
  _unRecipeFunctions_fillRecipeHeaderBuffer(recipeHeaderBuffer, numDPar, numWPar, numAPar, numAAlarm, numPid);
  _unRecipeFunctions_fillRecipeBuffer(manRegAddrBuffer, dparManRegAddr, wparManRegAddr, aparManRegAddr, aalarmManRegAddr, pidManRegAddr);
  _unRecipeFunctions_fillRecipeBuffer(manRegValBuffer, dparManRegVal, wparManRegVal, aparManRegVal, aalarmManRegVal, pidManRegVal);
  _unRecipeFunctions_fillRecipeBuffer(reqAddrBuffer, dparReqAddr, wparReqAddr, aparReqAddr, aalarmReqAddr, pidReqAddr);
  _unRecipeFunctions_fillRecipeBuffer(reqValBuffer, makeDynString(), wparReqVal, aparReqVal, aalarmReqVal, pidReqVal);
  
  gHeaderMap[sPlcName] = recipeHeaderBuffer;
  gManRegAddrMap[sPlcName] = manRegAddrBuffer; 
  gManRegValMap[sPlcName] = manRegValBuffer;
  gReqAddrMap[sPlcName] = reqAddrBuffer;
  gReqValMap[sPlcName] = reqValBuffer;
  
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Add the address and value of a digital parameter device in the recipe buffers.
 * @param numDevices    - [IN/OUT] Number of digital parameter devices included in the recipe.
 * @param sDeviceName   - [IN]  Device name.
 * @param sValue        - [IN]  Value for the device in the recipe.
 * @param diManRegAddr  - [OUT] Temporary buffer for the manual register address.
 * @param diManRegVal   - [OUT] Temporary buffer for the manual register value.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private void _unRecipeFunctions_addDigitalParameterToBuffers(int &numDevices, string sDeviceName, string sValue, dyn_int &diManRegAddr, dyn_int &diManRegVal, dyn_string &exceptionInfo) {
  int iManRegAddr, iManRegVal;
  bool bVal;
  string sDeviceType = "CPC_DigitalParameter";
  
  numDevices++;    
  // Get the ManReg01 address
  _unRecipeFunctions_getDpeAddress(sDeviceName+".ProcessOutput.ManReg01", sDeviceType, iManRegAddr, exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    return;
  }

  // Set the new ManReg01 values 
  bVal = strtoupper(sValue)=="TRUE";    

  // Set the ManReg values for the ArmRcp
  unRecipeFunctions_setBit(iManRegVal, DPAR_ARM_BIT, true);
  unRecipeFunctions_setBit(iManRegVal, DPAR_MONR_BIT, bVal);
  unRecipeFunctions_setBit(iManRegVal, DPAR_MOFFR_BIT, !bVal);  	
  
  // Insert the address and values in the lists
  dynAppend(diManRegAddr, iManRegAddr);       
  dynAppend(diManRegVal, iManRegVal);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Add the addresses and values of a word parameter or analog parameter device in the recipe buffers.
 * @param numDevices    - [IN/OUT] Number of digital parameter devices included in the recipe.
 * @param sDeviceName   - [IN]  Device name.
 * @param sDeviceType   - [IN]  Device type.
 * @param sValue        - [IN]  Value for the device in the recipe.
 * @param iArmBit       - [IN]  Position of the 'Armed' bit of the device in the Manual Register.
 * @param iNewMrBit     - [IN]  Position of the 'New Manual Request' bit of the device in the Manual Register.
 * @param diManRegAddr  - [OUT] Temporary buffer for the manual register address.
 * @param diManRegVal   - [OUT] Temporary buffer for the manual register value.
 * @param diReqAddr     - [OUT] Temporary buffer for the requested value address.
 * @param dfReqVal      - [OUT] Temporary buffer for the requested value.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private void _unRecipeFunctions_addXParameterToBuffers(int &numDevices, string sDeviceName, string sDeviceType, string sValue, 
        int iArmBit, int iNewMrBit, dyn_int &diManRegAddr, dyn_int &diManRegVal, dyn_int &diReqAddr, dyn_float &dfReqVal, 
        dyn_string &exceptionInfo) 
{
  bool bResult;
  int iManRegAddr, iManRegVal, iReqAddr;
  float fVal;
  
  numDevices++;
  
  // Get the ManReg01 address
  _unRecipeFunctions_getDpeAddress(sDeviceName+".ProcessOutput.ManReg01", sDeviceType, iManRegAddr, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }

  // Set the new ManReg01 values for the ArmRcp
  unRecipeFunctions_setBit(iManRegVal, iArmBit, true);
  unRecipeFunctions_setBit(iManRegVal, iNewMrBit, true);
  
  // Insert the address and values in the lists
  dynAppend(diManRegAddr, iManRegAddr);       
  dynAppend(diManRegVal, iManRegVal);
  
  // Get the MPosR address
  _unRecipeFunctions_getDpeAddress(sDeviceName+".ProcessOutput.MPosR", sDeviceType, iReqAddr, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }
  dynAppend(diReqAddr, iReqAddr);
  unGenericObject_StringToFloat(sValue, fVal, bResult);
  dynAppend(dfReqVal, fVal);
}        

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Add the addresses and values of an analog alarm or controller device in the recipe buffers.
 * @param numDevices    - [IN/OUT] Number of digital parameter devices included in the recipe.
 * @param sDeviceName   - [IN]  Device name.
 * @param sDpeName      - [IN]  Datapoint element name.
 * @param sDeviceType   - [IN]  Device type.
 * @param sValue        - [IN]  Value for the device in the recipe.
 * @param deviceMap     - [IN/OUT] Map where key=device name, 
 *                                 Value=Matrix: (1,1) -> ManReg Address
 *                                               (1,2) -> ManReg value 
 *                                               (2,1) -> Requested address
 *                                               (2,2) -> Requested value
 * @param manRegBitsMap - [IN]
 * @param iArmBit       - [IN]  Position of the 'Armed' bit of the device in the Manual Register.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private void _unRecipeFunctions_addMultiDpeDeviceToBuffers(int &numDevices, string sDeviceName, string sDpeName, string sDeviceType, string sValue, 
        mapping &deviceMap, mapping manRegBitsMap, int iArmBit, dyn_string &exceptionInfo) 
{
  bool bResult;
  int iManRegAddr, iManRegVal, iReqAddr, iPos;
  float fVal;
  dyn_dyn_string list;

  numDevices++;
  
  // Get the ManReg01 address and ManReg01 value
  if (!mappingHasKey(deviceMap, sDeviceName)){     
    _unRecipeFunctions_getDpeAddress(sDeviceName+".ProcessOutput.ManReg01", sDeviceType, iManRegAddr, exceptionInfo);
	if (dynlen(exceptionInfo)) {
      return;		  
    }
    list[1][1] = iManRegAddr;
    list[1][2] = 0;
    deviceMap[sDeviceName]=list;
    iManRegVal    = 0;          
  } else {
    list=deviceMap[sDeviceName];
    iManRegAddr   = (int) list[1][1];
    iManRegVal    = (int) list[1][2];
  }
  //Debug("ManRegAddr of : " + sDeviceName +".ProcessOutput.ManReg01 : " + iManRegAddr +"\n");
		
  // Modifies the ManReg Value
  if (!mappingHasKey(manRegBitsMap, sDpeName)){
    fwException_raise(exceptionInfo, "ERROR", "The CPC_Controller dpe:" + sDpeName + 
                      " hasn't a bit possition defined in the ManReg01.", "");
    return;  
  }
  
  iPos = manRegBitsMap[sDpeName];
  unRecipeFunctions_setBit(iManRegVal, iArmBit, true); 
  unRecipeFunctions_setBit(iManRegVal, iPos, true); 
  list[1][2]=iManRegVal;   
		
  // Set the Req. Address and Req. Value
  _unRecipeFunctions_getDpeAddress(sDeviceName + sDpeName, sDeviceType, iReqAddr, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }

  dynAppend(list, iReqAddr);
  unGenericObject_StringToFloat(sValue, fVal, bResult);
  list[dynlen(list)][2] = fVal; 
  deviceMap[sDeviceName] = list; 
} 
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Fill the contents of the recipe header buffer.
 * @param recipeHeaderBuffer - [OUT] Recipe header buffer.
 * @param iNumDPar           - [IN]  Number of digital parameter devices included in the recipe.
 * @param iNumWPar           - [IN]  Number of word parameter devices included in the recipe.
 * @param iNumAPar           - [IN]  Number of analog parameter devices included in the recipe.
 * @param iNumAAlarm         - [IN]  Number of analog alarm devices included in the recipe.
 * @param iNumPid            - [IN]  Number of controller devices included in the recipe.
 */ 
private void _unRecipeFunctions_fillRecipeHeaderBuffer(dyn_int &recipeHeaderBuffer, int iNumDPar, int iNumWPar, int iNumAPar, int iNumAAlarm, int iNumPid) {
  // Inserts the data in the recipe header buffer 
  // Split the current time in high part and low part
  dynClear(recipeHeaderBuffer);
  bit64 t = (bit64) getCurrentTime();
  bit32 bLow  = (bit32) (0x0000FFFFu & t);
  bit32 bHigh = (bit32) (0x0000FFFFu & (t >> 16));
  recipeHeaderBuffer[RCP_HEADER_ID_LOW] 	= (int)bLow % 32000;
  recipeHeaderBuffer[RCP_HEADER_ID_HIGH] 	= (int)bHigh % 32000;
  recipeHeaderBuffer[RCP_HEADER_NUM_MANREG] = (iNumDPar + iNumWPar + iNumAPar + iNumAAlarm + iNumPid);
  recipeHeaderBuffer[RCP_HEADER_NUM_DPAR] 	= iNumDPar;
  recipeHeaderBuffer[RCP_HEADER_DB_DPAR] 	= 0;
  recipeHeaderBuffer[RCP_HEADER_NUM_WPAR] 	= iNumWPar;
  recipeHeaderBuffer[RCP_HEADER_DB_WPAR] 	= 0;
  recipeHeaderBuffer[RCP_HEADER_NUM_APAR] 	= iNumAPar;
  recipeHeaderBuffer[RCP_HEADER_DB_APAR] 	= 0;
  recipeHeaderBuffer[RCP_HEADER_NUM_AALARM] = iNumAAlarm;
  recipeHeaderBuffer[RCP_HEADER_DB_AALARM]  = 0;
  recipeHeaderBuffer[RCP_HEADER_NUM_CONTR] 	= iNumPid;
  recipeHeaderBuffer[RCP_HEADER_DB_CONTR] 	= 0;
  recipeHeaderBuffer[RCP_HEADER_SENT_VALUE] = RCP_ACT_HEADER_SENDING_DATA;
  recipeHeaderBuffer[RCP_HEADER_CRC] 		= 0;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Fill the contents of a recipe buffer.
 * @param buffer       - [OUT] Buffer where to add the recipe values.
 * @param dparValues   - [IN]  Values of the digital parameter device.
 * @param wparValues   - [IN]  Values of the word parameter device.
 * @param aparValues   - [IN]  Values of the analog parameter device.
 * @param aalarmValues - [IN]  Values of the analog alarm device.
 * @param pidValues    - [IN]  Values of the controller device.
 */
private void _unRecipeFunctions_fillRecipeBuffer(dyn_anytype &buffer, dyn_anytype dparValues, dyn_anytype wparValues, 
        dyn_anytype aparValues, dyn_anytype aalarmValues, dyn_anytype pidValues)
{
  dynClear(buffer);
  dynAppend(buffer, dparValues);
  dynAppend(buffer, wparValues);
  dynAppend(buffer, aparValues);
  dynAppend(buffer, aalarmValues);
  dynAppend(buffer, pidValues);
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Send the contents of the recipe buffers to a PLC.
 * @param plcName       - [IN]  The PLC name.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private void _unRecipeFunctions_sendData(string plcName, dyn_string &exceptionInfo)
{  
  bool done = false;
  dyn_string dpeManRegAddr, dpeManRegVal, dpeReqAddr, dpeReqVal;
  dyn_int manRegAddr, manRegVal, reqAddr;
  dyn_float reqVal;
  string sBufferDpName;
  
  _unRecipeFunctions_getRecipeBuffersName(plcName, sBufferDpName, exceptionInfo);
  manRegAddr = _unRecipeFunctions_getManRegAddrBuffer(plcName);
  manRegVal  = _unRecipeFunctions_getManRegValBuffer(plcName);
  reqAddr    = _unRecipeFunctions_getReqAddrBuffer(plcName);
  reqVal     = _unRecipeFunctions_getReqValBuffer(plcName);
    
  int count=0;
  int numManReg  = dynlen(manRegAddr);
  int numReqAddr = dynlen(reqAddr);  
  int numStructures = ((numManReg-1)/CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE)+1;
  
//  Debug("1 - Creating " + numStructures + " WORD structure DPES _unRecipeFunctions_sendData\n");
  for (int i=1; i<=numStructures && !done; i++){
    for (int j=1; j<=CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE; j++){
      string dpe = ".Structure"+i+".Value"+(count+1);
      dynAppend(dpeManRegAddr, sBufferDpName+"ProcessOutput.ManRegAddr"+dpe);
      dynAppend(dpeManRegVal, sBufferDpName+"ProcessOutput.ManRegVal"+dpe);
            
      if (count<numReqAddr)
        dynAppend(dpeReqAddr, sBufferDpName+"ProcessOutput.ReqAddr"+dpe);  
      count++;   
      if (count==numManReg){
        done=true;
        break;     
      }
    }
  }
  
  count=0; 
  done=false;
  
  if (numReqAddr>0) {
//    Debug("NumReqAddr: " + numReqAddr + "\n");
	numStructures = ((numReqAddr-1)/CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE)+1;
	//Debug("2 - Creating " + numStructures + " FLOAT structure DPES _unRecipeFunctions_sendData\n");
	for (int i=1; i<=numStructures && !done; i++){
	  for (int j=1; j<=CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE; j++){
		string dpe = ".Structure"+i+".Value"+(count+1);
		dynAppend(dpeReqVal, sBufferDpName+"ProcessOutput.ReqVal"+dpe);
		count++;
		if (count==numReqAddr){
		  done=true;
		  break;
		}
	  }
	}
  }
  //Debug("3 - _unRecipeFunctions_sendData\n");
			
  dpSetWait(dpeManRegAddr, manRegAddr);
  dpSetWait(dpeManRegVal, manRegVal);
  if (numReqAddr > 0) {
	dpSetWait(dpeReqAddr, reqAddr);
	dpSetWait(dpeReqVal, reqVal);
  }
  
//  Debug("4 - All recipe data sent to the PLC " + plcName + " _unRecipeFunctions_sendData\n");
  unRecipeFunctions_writeInRecipeLog("All recipe data sent to the PLC " + plcName);
}

//------------------------------------------------------------------------------------------------------------------------

private void _unRecipeFunctions_processMultiDpeDevices(mapping map, dyn_int &manRegAddresses, dyn_int &manRegValues, 
                                               dyn_int &reqAddresses, dyn_float &reqValues)
{
  dynClear(manRegAddresses);
  dynClear(manRegValues);
  dynClear(reqAddresses);
  dynClear(reqValues);  
  
  int len = mappinglen(map);
  dyn_string keys = mappingKeys(map);
  
  // Iterates through all the devices in the map
  for (int i=1; i<=len; i++){
    string deviceName = keys[i];
    dyn_dyn_string data = map[deviceName];
    int manRegAddr   = (int) data[1][1];
    int manRegVal    = (int) data[1][2];
    int datalen = dynlen(data);

    // Iterates through all the device DPES     
    for (int j=2; j<=datalen; j++){
      int reqAddr = (int) data[j][1];
      float reqVal  = (float) data[j][2];

      // Inserts the data in the buffers
      dynAppend(manRegAddresses, manRegAddr);
      dynAppend(manRegValues, manRegVal);
      dynAppend(reqAddresses, reqAddr);
      dynAppend(reqValues, reqVal);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Insert the DB number for each recipe device type in the recipe header buffer (Siemens only).
 * @param sPlcName      - [IN] PLC Name.
 * @param dsDpes        - [IN] List of DPEs of a recipe.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private void _unRecipeFunctions_createRecipeHeaderDBNumber(string sPlcName, dyn_string dsDpes, dyn_string &exceptionInfo)
{
  string dbNum;
  mapping dbMapping;  
  dyn_int recipeHeaderBuffer = _unRecipeFunctions_getHeaderBuffer(sPlcName);   
  
  int iLen=dynlen(dsDpes);
  for (int i=1; i<=iLen; i++) {
    string deviceType = dpTypeName(dsDpes[i]);
    if (mappingHasKey(dbMapping, deviceType)) {
      continue;
    }
    
    _unRecipeFunctions_getS7AddressDBNumber(dsDpes[i], deviceType, dbNum, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      return;
    }
       
    dbMapping[deviceType] = dbNum;
    //Debug("DB of " + deviceType + ": " + dbMapping[deviceType] +"\n");
  }
  
  // Insert the data in the recipe header
  if (mappingHasKey(dbMapping, "CPC_DigitalParameter")) {
    recipeHeaderBuffer[RCP_HEADER_DB_DPAR] = dbMapping["CPC_DigitalParameter"];
  } else {
    recipeHeaderBuffer[RCP_HEADER_DB_DPAR] = 0;
  }
  
  if (mappingHasKey(dbMapping, "CPC_WordParameter")) {
    recipeHeaderBuffer[RCP_HEADER_DB_WPAR] = dbMapping["CPC_WordParameter"];
  } else {
    recipeHeaderBuffer[RCP_HEADER_DB_WPAR] = 0;
  }
  
  if (mappingHasKey(dbMapping, "CPC_AnalogParameter")) {
    recipeHeaderBuffer[RCP_HEADER_DB_APAR] = dbMapping["CPC_AnalogParameter"];  
  } else {
    recipeHeaderBuffer[RCP_HEADER_DB_APAR] = 0;
  }
  
  if (mappingHasKey(dbMapping, "CPC_AnalogAlarm")) {
    recipeHeaderBuffer[RCP_HEADER_DB_AALARM] = dbMapping["CPC_AnalogAlarm"];
  } else {
    recipeHeaderBuffer[RCP_HEADER_DB_AALARM] = 0;
  }
  
  if (mappingHasKey(dbMapping, "CPC_Controller")) {
    recipeHeaderBuffer[RCP_HEADER_DB_CONTR] = dbMapping["CPC_Controller"];  
  } else {
    recipeHeaderBuffer[RCP_HEADER_DB_CONTR] = 0;
  }
  
  gHeaderMap[sPlcName] = recipeHeaderBuffer;
  //Debug("Header Buffer: " + recipeHeaderBuffer + "\n");
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Activate a list of recipes. This method must be called from a panel containing a 'RcpTable' with the list of recipes.
 * @param dsRcpDps - [IN] List of datapoints of the recipes to activate.
 */
synchronized void unRecipeFunctions_activateRecipes(dyn_string dsRcpDps) {
  int iDpCol, iNameCol;
  string sSystemName;
  dyn_string dsAllRcpDps, dsRcpNames, exceptionInfo;
  mapping mConnectionDps;
  
  if (!shapeExists("RcpTable")) {
    unRecipeFunctions_writeInRecipeLog("The shape 'RcpTable' is missing.", TRUE, TRUE);
    unRecipeFunctions_writeInRecipeLog("The recipes will not be activated.", TRUE, TRUE);
    return;
  }

  dynClear(dsConnectionDps);
  getMultiValue("RcpTable", "nameToColumn", "RcpInstanceDp", iDpCol,
                "RcpTable", "nameToColumn", "RcpInstanceName", iNameCol,
                "RcpTable", "getColumnN", iDpCol, dsAllRcpDps,
                "RcpTable", "getColumnN", iNameCol, dsRcpNames);
  
  if (isDollarDefined("$sDpName") && dpExists($sDpName)) {
    dynAppend(dsConnectionDps, $sDpName + ".statusInformation.selectedManager:_lock._original._locked"); 
  }
  
  int iLen = dynlen(dsRcpDps);
  for (int i=1; i<=iLen; i++) {
    string sRcpStatusDp = dsRcpDps[i] + ".ProcessInput.State";
    // Reset the recipe status color
    int iPos = dynContains(dsAllRcpDps, dsRcpDps[i]);
    if (iPos > 0) {
      _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "{192,200,200}");
    }
    
    sSystemName = unGenericDpFunctions_getSystemName(sRcpStatusDp);
    unRecipeFunctions_mappingInsertValue(mConnectionDps, sSystemName, sRcpStatusDp); 
  }
  
  // Connect to the recipe instance states
  iLen = dynlen(mappingKeys(mConnectionDps));
  for (int i=1; i<=iLen; i++) {
    dyn_string dsConnectTo = mappingGetValue(mConnectionDps, i);
    dpConnect("_unRecipeFunctions_recipeStateCB", TRUE, dsConnectTo);    
  }
  
  // Activate all the selected recipes
  iLen = dynlen(dsRcpDps);
  for (int i=1; i<=iLen && bCancelActivation==false; i++) {
    sActivatingRcpDp = dsRcpDps[i];
    int iRow = dynContains(dsAllRcpDps, sActivatingRcpDp)-1;
    _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "yellow");
    if (!dpExists(sActivatingRcpDp)) {
      unRecipeFunctions_writeInRecipeLog("The recipe '"  + dsRcpNames[(iRow+1)] + "' doesn't exist.");
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "red");
      continue;
    }
    
    int iActivationResult = unRecipeFunctions_activateRecipe(sActivatingRcpDp, exceptionInfo);
    if (dynlen(exceptionInfo) || iActivationResult<0) {
      // The recipe activation failed
      dynClear(exceptionInfo);
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "red");
    } else {
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "green");
    }
  }

  sActivatingRcpDp = "";
  delay(1);
  iLen = dynlen(mappingKeys(mConnectionDps));
  for (int i=1; i<=iLen; i++) {
    dyn_string dsConnectTo = mappingGetValue(mConnectionDps, i);
    dpDisconnect("_unRecipeFunctions_recipeStateCB", dsConnectTo);    
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Callback function executed when the recipe state has been modified.
 * @param dsDpList - [IN] List of recipe state datapoints.
 * @param dsValues - [IN] List of recipe state values.
 *
 * @reviewed 2018-09-12 @whitelisted{Callback}
 */
void _unRecipeFunctions_recipeStateCB(dyn_string dsDpList, dyn_string dsValues) { 
  int i, iStateCol, iDpCol, iLen;
  dyn_string dsRcpDp;
  
  if (!shapeExists("RcpTable")) {
    return;
  }
  
  getMultiValue(
      "RcpTable", "nameToColumn", "RcpState", iStateCol,
      "RcpTable", "nameToColumn", "RcpInstanceDp", iDpCol,
      "RcpTable", "getColumnN", iDpCol, dsRcpDp);
  
  iLen = dynlen(dsDpList);  
  for (i=1; i<=iLen; i++) {
    string sDpName = substr(dsDpList[i], 0, strpos(dsDpList[i], "."));
    int iPos = dynContains(dsRcpDp, sDpName);    
    if (iPos>0) {      
      RcpTable.cellValueRC((iPos-1), "RcpState", dsValues[i]);
    }
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Check the status of a list of recipes. This method must be called from a panel containing a 'RcpTable' with the list of recipes.
 * @param dsRcpDps - [IN] List of datapoints of the recipes to verify its activation.
 */
public void unRecipeFunctions_checkRecipeStatus(dyn_string dsRcpDps) {
  int iDpCol;
  bool bActive;
  string sAlias, sErrDpe, sClassName, sSystemName;
  dyn_string dsAllRcpDps, dsStatusMessage, exceptionInfo;
  dyn_mixed recipeClassObject;
  dyn_dyn_mixed recipeObject;

  if (!shapeExists("RcpTable")) {
    unRecipeFunctions_writeInRecipeLog("The shape 'RcpTable' is missing.", TRUE, TRUE);
    unRecipeFunctions_writeInRecipeLog("The recipe status will not be checked.", TRUE, TRUE);
    return;
  }
  
  unRecipeFunctions_writeInRecipeLog("Checking the recipe activation status ... ");
  
  getMultiValue("RcpTable", "nameToColumn", "RcpInstanceDp", iDpCol,
                "RcpTable", "getColumnN", iDpCol, dsAllRcpDps);
  
  int iLen = dynlen(dsRcpDps);
  for (int i=1; i<=iLen; i++) {
    int iRow = dynContains(dsAllRcpDps, dsRcpDps[i]) - 1;
    sAlias = dpGetAlias(dsRcpDps[i] + ".");
    sSystemName = unGenericDpFunctions_getSystemName(dsRcpDps[i]);
    _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "grey");
    // Check if the recipe DP exists
    if (!dpExists(dsRcpDps[i])) {
      unRecipeFunctions_writeInRecipeLog("The recipe '"  + sAlias + "' doesn't exist.");
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "red");
      continue;
    }
    
    unRecipeFunctions_getRecipeClassObject(dsRcpDps[i], recipeClassObject, exceptionInfo);
    if (dynlen(exceptionInfo))  { 
      unRecipeFunctions_writeInRecipeLog("Exception loading the recipe class '"  + sClassName + "': " + sClassName);
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "red");
      continue;
    }
    
    // Check if the last activated recipe of this type is the current one
    if (recipeClassObject[fwConfigurationDB_RCL_LAST_ACTIVATED_RECIPE]!=sAlias && 
        recipeClassObject[fwConfigurationDB_RCL_LAST_ACTIVATED_RECIPE] != sSystemName + sAlias){
      unRecipeFunctions_writeInRecipeLog("The recipe " + sAlias + " is inactive.");
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "yellow");
      continue;
    }   
    
    // Get the recipe object
    dynClear(recipeObject);
    dynClear(exceptionInfo);
    _unRecipeFunctions_getRecipeObjectFromDp (dsRcpDps[i], recipeObject, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Error loading the recipe '"  + sAlias + "' : " + exceptionInfo[2]);
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "red");
      continue;    
    }

    // Check if the PLCs are active
    dynClear(dsStatusMessage);
    unRecipeFunctions_getPlcStatus(recipeObject[fwConfigurationDB_RO_DPE_NAME], bActive, 
                                   dsStatusMessage, exceptionInfo);
    
    if (!bActive) {
      string sPlcs;
      fwGeneral_dynStringToString(dsStatusMessage, sPlcs, ", ");
      unRecipeFunctions_writeInRecipeLog("Checking recipe " + sAlias + " ...");
      unRecipeFunctions_writeInRecipeLog("Error: The following PLC(s) are disconnected: " + sPlcs);
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "red");
      continue;
    }    
    
    dynClear(exceptionInfo);
    bActive = _unRecipeFunctions_checkRecipeOnlineValues(
                  recipeObject[fwConfigurationDB_RO_DPE_NAME], 
                  recipeObject[fwConfigurationDB_RO_VALUE], 
                  sErrDpe, exceptionInfo);
    
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Wrong recipe elements in the recipe " + sAlias + ".");
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "red");
      continue;
    }    
    
    if (bActive) {
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "green");
      unRecipeFunctions_writeInRecipeLog("The recipe " + sAlias + " is active.");
    } else {
      _unRecipeFunctions_setTableCellBGColor("RcpTable", iRow, "RcpActivationStatus", "yellow");
      unRecipeFunctions_writeInRecipeLog("The recipe " + sAlias + " is inactive.");
    }
  }
  unRecipeFunctions_writeInRecipeLog("Checking the recipe activation status done! ");
}

//------------------------------------------------------------------------------------------------------------------------
