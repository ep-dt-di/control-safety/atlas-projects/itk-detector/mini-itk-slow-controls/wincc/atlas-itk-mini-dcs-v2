/**
 * UNICOS
 * Copyright (C) CERN 2017 All rights reserved
 */
/**@file

// unRcpFunctions_dpe.ctl
This file contains functions related to datapoints.

@par Creation Date
  15/03/2017

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (BE-ICS)
*/
#uses "unRcpFunctions_utilities.ctl"
#uses "fwGeneral/fwGeneral.ctl"
#uses "unGenericObject.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "fwGeneral/fwException.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "cpcObjects/cpcControllerConfig.ctl"


/****************************** 
 *      Global   Variables    *
 *****************************/
global mapping unRecipe_rangeElements;  // Map where key=device type, value=dpe containing the range definition for the device type

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the element where the range is defined for a PVSS DPE
 * @param sDpName - DPE name in the PVSS format (e.g. dist_1:gcs-CFP_STP_CVSF18-SF18-CPC_AnalogParameter-00001.ProcessOutput.ValRq)
 * @param sElement - Variable to store the element name that contains the range (e.g. .ProcessInput.CurValSt)
 * @param exceptionInfo - Standard exception handling variable.
 */
public void unRecipeFunctions_getRangeElementFromPVSSDPE(string sDpName, string &sElement, dyn_string &exceptionInfo)
{
    string deviceType;
    dyn_string dpeInfo;
    
    sElement = "";
    dynClear(exceptionInfo);
    
    if (!dpExists(sDpName)) {
      fwException_raise(exceptionInfo,"ERROR","The data point element: "+sDpName+
                        " doesn't exist.",unRecipeFunctions_ERROR_InvalidElements);
      return;
    }
    
    deviceType = dpTypeName(sDpName);
    int pos = strpos(sDpName, ".");
    if (pos<0) {
      return;
    }

    if (!dpExists(deviceType+"_RcpInfo.DpeInfo")) {
      // If the information about what is the DPE containing the range definition doesn't exist,
      // returns the DPE name 
      string dp=unGenericDpFunctions_getDpName(sDpName);
      sElement = substr(sDpName, strlen(dp), strlen(sDpName)-strlen(dp));
      return;
    }
        
    sElement = substr(sDpName, pos, strlen(sDpName)-pos);
    dpGet(deviceType+"_RcpInfo.DpeInfo", dpeInfo);

    for (int i=1; i<=dynlen(dpeInfo); i++){
      string currDpeInfo = dpeInfo[i];
      dyn_string splitInfo = strsplit(currDpeInfo, ":");
      if (dynlen(splitInfo)!=2) {
        fwException_raise(exceptionInfo,"ERROR","The recipe info for the device type: "+deviceType+
                          " is wrong.",unRecipeFunctions_ERROR_InvalidElements);
        return;        
      }
      
      splitInfo[1] = strltrim(splitInfo[1]);
      splitInfo[1] = strrtrim(splitInfo[1]);
      if (splitInfo[1] == sElement) {
        sElement = strltrim(splitInfo[2]);
        sElement = strrtrim(sElement);
        return;
      }
    }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the min. and max. values defined in a range for a DPE.
 * @param sDpe - [IN] Data point element for whom the range values are required.
 * @param sRangeElement - [IN] DPE where the range is defined for the 'sDpe' parameter (e.g.: ".ProcessInput.CurValSt")
 * @param sRangeMin - [OUT] Minimum value of the range definition.
 * @param sRangeMax - [OUT] Maximum value of the range definition.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getInstanceRange(string sDpe, string sRangeElement, string &sRangeMin, 
                                         string &sRangeMax, dyn_string &exceptionInfo)
{
  string dpName = unGenericDpFunctions_getDpName(sDpe)+sRangeElement;
  if (!dpExists(dpName)){
    fwException_raise(exceptionInfo,"ERROR","The specified data point doesn't exist: "+dpName,"");
    return;        
  }
  
  string min="",max="";
  dpGet(dpName+":_pv_range.._min", min,
        dpName+":_pv_range.._max", max);
    
  if (min=="" && max=="") {
    sRangeMin="";
    sRangeMax="";
  } else {
    string format = (string) dpGetFormat(dpName);
    sRangeMin = unGenericObject_FormatValue(format, min);
    sRangeMax = unGenericObject_FormatValue(format, max);     
  }
}  

//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the range of the specified DP.
 * @param sDpName - [IN] The DP which range is required.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return String containing the valid range for the specified DP (format: [min, max]).
 */
string unRecipeFunctions_getDeviceRange(string sDpName, dyn_string &exceptionInfo)
{
  string range, deviceType, dp, element, rangeMin, rangeMax, rangeElement;
  
  if (!dpExists(sDpName)) {
    return "";
  }
  
  deviceType = dpTypeName(sDpName);   
  dp=unGenericDpFunctions_getDpName(sDpName);
  int pos = strpos(sDpName, dp);
  element = substr(sDpName, pos+strlen(dp), strlen(sDpName)-strlen(dp));

  // If the device type is a DigitalParameter the range is [FALSE, TRUE]
  if (deviceType=="CPC_DigitalParameter") {
    range = "[FALSE, TRUE]";
    return range;
  }
       
  if (!mappingHasKey(unRecipe_rangeElements, deviceType+":"+element)){
    unRecipeFunctions_getRangeElementFromPVSSDPE(sDpName, rangeElement, exceptionInfo);
    if (dynlen(exceptionInfo)) {
        return "";
    }
    unRecipe_rangeElements[deviceType+":"+element] = rangeElement;
  } else {
    rangeElement = unRecipe_rangeElements[deviceType+":"+element];
  }

  // Get the range values
  unRecipeFunctions_getInstanceRange(sDpName, rangeElement, rangeMin, rangeMax, exceptionInfo);       
  if (rangeMin!="" && rangeMax!="") {
    range = "[" + rangeMin +", " + rangeMax + "]";  
  }
	
  return range;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the PLC address of a DPE.
 * @param dpe 			- [IN] The Datapoint element which address will be calculated.
 * @param deviceType 	- [IN] The device type name.
 * @param address 		- [OUT] The PLC address.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void _unRecipeFunctions_getDpeAddress(string dpe, string deviceType, 
                                     string &address, dyn_string &exceptionInfo)
{
  string addressType, sFunction;
  dyn_errClass err; 

  dpGet(dpe + ":_address.._drv_ident", addressType);
  err = getLastError(); //test whether an error occurred 
  if(dynlen(err) > 0) 
  { 
    string sErrorMessage = "Could not read the address config of " +
                           unGenericDpFunctions_getSystemName(dpe) +
                           unGenericDpFunctions_getAlias(dpe)[1]; // end of error message string
    
    unRecipeFunctions_writeInRecipeLog(sErrorMessage, false, true);
    throwError(makeError( "", PRIO_SEVERE, ERR_CONTROL, 0, sErrorMessage));
    fwException_raise(exceptionInfo, "ERROR", "Could not read the address config of DPE. " + getErrorText(err), "");  
    DebugTN(exceptionInfo);
    return;
  }
  
  sFunction = "DRV_" + addressType + "_convertToUnicosAddress";
  if(!isFunctionDefined(sFunction)) {
    fwException_raise(exceptionInfo, "ERROR", "The function " +sFunction+ " isn't defined.", "");  
    DebugTN(exceptionInfo);
    return;
  }	

  evalScript(address,	"string main(string sDeviceDpeName, string sDeviceType)"+
  										"{	string temp;"+
  										"	temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
  										"	return temp;"+
  										"}", makeDynString(), dpe, deviceType);     
  
  // If it's an S7 address, gets the offset
  if (addressType=="S7"){
    string fullAddress = address;
    string offset;
    
    int pos=strpos(fullAddress, ".");
    if(pos<0) {
      fwException_raise(exceptionInfo, "ERROR", "The S7 address format is invalid: " + fullAddress, "");
      return;
    }
  
    string temp = substr(fullAddress, pos+1, strlen(fullAddress)-pos);
    int len = strlen(temp);
    int j=0;
    for (int i=0; i<=len; i++){
      if ((temp[i]>='0' && temp[i]<='9') || temp[i]=='\0'){
        offset = offset + temp[i];
        j++;
      }
    }
    address = offset;  
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get a string containing the list of DPEs and values with the format DPName|Value[,DPName|Value]*
 * @param dsRcpDpes   - [IN] List of recipe elements (e.g. Device1.ProcessOutput.ManReg01);
 * @param dsRcpValues - [IN] List of recipe values for the dsRecipeDpes.
 * @return String containing the list of DPEs and values with the format DPName|Value[,DPName|Value]*
 */
public string unRecipeFunctions_getDpeValueString(dyn_string dsRcpDpes, dyn_string dsRcpValues) {
  int iLen;
  string sPvssDpe, sDpeValue;
  dyn_string dsSplitDp, dsOnlineDpes, dsTmpDpes, dsDpNameValue, exceptionInfo;

  iLen = dynlen(dsRcpDpes);
  if (iLen<=0) {
    return "";
  }
  
  if (dynlen(dsRcpValues)==0 || dynlen(dsRcpValues)!=iLen) { 
    // Get the online values
    dynClear(dsRcpValues);
    for (int i=1; i<=iLen; i++) {
      dsSplitDp = strsplit(dsRcpDpes[i], ".");
      unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(dsSplitDp[1] + "." + dsSplitDp[dynlen(dsSplitDp)], sPvssDpe);
      unRecipeFunctions_getOnlineValueDpes(makeDynString(sPvssDpe), dsTmpDpes, exceptionInfo);
      dynAppend(dsOnlineDpes, dsTmpDpes);
    }
    dpGet(dsOnlineDpes, dsRcpValues);
  } 
  
  // Create the string containing the DPName|Value[,DPName|Value]*
  for (int i=1; i<=iLen; i++) {
    dsSplitDp = strsplit(dsRcpDpes[i], ".");
    dynAppend(dsDpNameValue, dsSplitDp[1] + "." + dsSplitDp[dynlen(dsSplitDp)] + "|" + dsRcpValues[i]);
  }

  fwGeneral_dynStringToString(dsDpNameValue, sDpeValue, ",");
  
  return sDpeValue;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Creates a dp name that is not existing already
 * @param sDeviceType - [IN] Recipe device type name.
 * @param sFrontEndName - [IN] Front end name where the recipe will be linked.
 * @param sAppName - [IN] Application name.
 * @param sDpName - [OUT] Data point name for the recipe instance.
 * @param iDeviceNumber - [OUT] Device number for the recipe.
 */
public void unRecipeFunctions_createNewRecipeDp(string sDeviceType, string sFrontEndName, string sAppName, string &sDpName, int &iDeviceNumber)
{
  string sRandomNumber, sTmp, sPrefix;
  
  dpGet("_unApplication.devicePrefix", sPrefix);
  sDpName = sPrefix + "-" + sFrontEndName + "-" + sAppName + "-" + sDeviceType + "-";
  
  do {
	  sRandomNumber="";
	  for (int i=1; i<=5; i++) {
		sTmp = rand();
		sRandomNumber += sTmp[strlen(sTmp)-1];    
	  }
  } while (dpExists(sDpName+sRandomNumber));
    
  sDpName += sRandomNumber;
  iDeviceNumber = (int) sRandomNumber;
}

//--------------------------------------------------------------------------------------------------------------------------
/**
 * Function used to set a value in a list of DPEs.
 * @param dsDpNames     - [IN]  List of DP names where to set the values.
 * @param sAppendStr    - [IN]  String to append to the DP names.
 * @param value         - [IN]  Value to set the the DPEs.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return TRUE if the values were set successfully, otherwise FALSE.
 */
public bool unRecipeFunctions_dpMultiSet(dyn_string dsDpNames, string sAppendStr, anytype value, dyn_string &exceptionInfo) {
  int iLen;
  dyn_anytype daValues;
  
  iLen = dynlen(dsDpNames);
  for (int i=1; i<=iLen; i++) {
    dsDpNames[i] += sAppendStr;
    dynAppend(daValues, value);
  }
  
  int iResult = dpSet(dsDpNames, daValues);
  return unRecipeFunctions_handleDpGetSetError(iResult, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the DPEs that contains the online values (ProcessInput) for the specified DPEs (ProcessOutput)
 * @param dsDpNames     - [IN] List of DPEs which online value DPEs are required.
 * @param outputDpes    - [OUT] List of online value DPEs.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getOnlineValueDpes (dyn_string dsDpNames, dyn_string &outputDpes, dyn_string &exceptionInfo) {
	int len = dynlen(dsDpNames);
	for (int i=1; i<=len; i++) {    
        if (!dpExists(dsDpNames[i])) {
          fwException_raise(exceptionInfo, "ERROR", "The datapoint " + dsDpNames[i] + " doesn't exist.", "");
          continue;
        }
		string dpType = dpTypeName(dsDpNames[i]);
		
		if (dpType=="CPC_DigitalParameter") {
			strreplace(dsDpNames[i], "ProcessOutput.ManReg01", "ProcessInput.PosSt");
		} else if (dpType=="CPC_AnalogAlarm") {
			strreplace(dsDpNames[i], "ProcessOutput", "ProcessInput");
			dsDpNames[i] += "St";
		} else if (dpType=="CPC_Controller") {
			if (strpos(dsDpNames[i], "ProcessOutput.MSPH") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MSPH", "ProcessInput.ActSPH");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MSPL") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MSPL", "ProcessInput.ActSPL");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MSP") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MSP", "ProcessInput.ActSP");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MOutH") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MOutH", "ProcessInput.ActOutH");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MOutL") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MOutL", "ProcessInput.ActOutL");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MKc") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MKc", "ProcessInput.ActKc");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MTi") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MTi", "ProcessInput.ActTi");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MTd") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MTd", "ProcessInput.ActTd");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MTds") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MTds", "ProcessInput.ActTds");
			} 
		} else if (dpType=="CPC_WordParameter" || dpType=="CPC_AnalogParameter") {
			strreplace(dsDpNames[i], "ProcessOutput.MPosR", "ProcessInput.PosSt");
		} 
	}
	
	outputDpes = dsDpNames;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the DPEs that contains the default values for the specified DPEs 
 * @param dsDpNames     - [IN] List of DPEs which online value DPEs are required.
 * @param outputDpes    - [OUT] List of online value DPEs.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getDefaultValueDpes (dyn_string dsDpNames, dyn_string &outputDpes, dyn_string &exceptionInfo) {
	int len = dynlen(dsDpNames);
	for (int i=1; i<=len; i++) {    
        if (!dpExists(dsDpNames[i])) {
          fwException_raise(exceptionInfo, "ERROR", "The datapoint " + dsDpNames[i] + " doesn't exist.", "");
          continue;
        }
		string dpType = dpTypeName(dsDpNames[i]);
		
		if (dpType=="CPC_DigitalParameter") {
			strreplace(dsDpNames[i], "ProcessOutput.ManReg01", "ProcessOutput.DfltVal");
		} else if (dpType=="CPC_AnalogAlarm") {
      // Nothing to do
		} else if (dpType=="CPC_Controller") {
			if (strpos(dsDpNames[i], "ProcessOutput.MSPH") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MSPH", "ProcessOutput.DefSPH");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MSPL") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MSPL", "ProcessOutput.DefSPL");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MSP") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MSP", "ProcessOutput.DefSP");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MOutH") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MOutH", "ProcessOutput.DefOutH");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MOutL") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MOutL", "ProcessOutput.DefOutL");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MKc") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MKc", "ProcessOutput.DefKc");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MTi") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MTi", "ProcessOutput.DefTi");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MTd") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MTd", "ProcessOutput.DefTd");
			} else if (strpos(dsDpNames[i], "ProcessOutput.MTds") >= 0) {
				strreplace(dsDpNames[i], "ProcessOutput.MTds", "ProcessOutput.DefTds");
			} 
		} else if (dpType=="CPC_WordParameter" || dpType=="CPC_AnalogParameter") {
			strreplace(dsDpNames[i], "ProcessOutput.MPosR", "ProcessOutput.DfltVal");
		} 
	}
	
	outputDpes = dsDpNames;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the measurement unit of a DP.
 * @param sDpName - [IN] The datapoint name.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return String containing the measurement unit of the specified DP.
 */
public string unRecipeFunctions_getDpeUnit(string sDpName, dyn_string &exceptionInfo) {
  string deviceType, dp, element, rangeElement;
  
  if (!dpExists(sDpName)) {
    return "";
  }
  
  deviceType = dpTypeName(sDpName);   
  dp=unGenericDpFunctions_getDpName(sDpName);
  int pos = strpos(sDpName, dp);
  element = substr(sDpName, pos+strlen(dp), strlen(sDpName)-strlen(dp));
  
  // Hard-coding values for Controller: MTi, MTd, MTds (IS-1456)
  if (deviceType == UN_CONFIG_CPC_CONTROLLER_DPT_NAME 
        && (element==".ProcessOutput.MTi" || element==".ProcessOutput.MTd" || element==".ProcessOutput.MTds") ) 
  {
    return "s.";
  }      
  
  if (!mappingHasKey(unRecipe_rangeElements, deviceType+":"+element)){
    unRecipeFunctions_getRangeElementFromPVSSDPE(sDpName, rangeElement, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      return "";
    }
    unRecipe_rangeElements[deviceType+":"+element] = rangeElement;
  } else {
    rangeElement = unRecipe_rangeElements[deviceType+":"+element];
  }
  
  return dpGetUnit(dp + rangeElement);
}

//------------------------------------------------------------------------------------------------------------------------
