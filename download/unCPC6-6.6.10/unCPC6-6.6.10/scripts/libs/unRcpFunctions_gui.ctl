
/**
 * UNICOS
 * Copyright (C) CERN 2017 All rights reserved
 */
/**@file

// unRcpFunctions_gui.ctl
This file contains functions related to the graphical user interface.

@par Creation Date
  15/03/2017

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (BE-ICS)
*/
#uses "fwGeneral/fwException"
#uses "fwGeneral/fwException.ctl"
#uses "unProgressBar.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unGenericObject.ctl"
#uses "unicos_declarations.ctl"
#uses "unGraphicalFrame.ctl"


//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the data point name of the recipe instance selected in the panel.
 * @param sSelected     - [OUT] DP name of the recipe instance selected in the panel.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void unRecipeFunctions_getSelectedRecipeDp(string &sSelected, dyn_string &exceptionInfo)
{
  sSelected = "";
  string sRecipeClass;
  int iLineCount;
  dyn_int diLines;

  if (!shapeExists("RecipeClasses") || !shapeExists("RecipeInstances")) {
  	fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
   return;
  }
  
  getValue("RecipeClasses", "text", sRecipeClass);
  if (sRecipeClass=="")	{
    return; 
  }
  
  getMultiValue("RecipeInstances", "lineCount", iLineCount,
				"RecipeInstances", "getSelectedLines", diLines);
				
  if (!dynlen(diLines) || diLines[1] >= iLineCount) {
    return;
  }

  getValue("RecipeInstances", "cellValueRC", diLines[1], "RecipeInstancesDP", sSelected);
  
}

//------------------------------------------------------------------------------------------------------------------------

/**
 * Method used to animate the selection progress bar in the recipe instance panel.
 * @param sDpName - [IN] Data point name of the selected recipe instance.
 *
 * @reviewed 2018-09-12 @whitelisted{Thread}
 */
void unRecipeFunctions_animateSelectionProgressBar (string sDpName) 
{
  int iTimeOut;
  dyn_string exceptionInfo;
  string sRemainingTime, sSelectedManager, sSelectedRecipeDp;
  bool bSelect;

  // Get the selected recipe instance in the panel
  unRecipeFunctions_getSelectedRecipeDp(sSelectedRecipeDp, exceptionInfo); 
  if (sDpName != sSelectedRecipeDp) {
    unProgressBar_setPosition("SelectionProgressBar", 0);  
    return;
  }
	
  /** Selection Progress Bar animation **/
  if (shapeExists("SelectionProgressBar") && shapeExists("buttonSelect")) {
    delay (0, 50);
    dpGet(sDpName + ".statusInformation.selectedManager", sSelectedManager);
    bSelect = (sSelectedManager != "");
    if (bSelect) {
      dpGet(getSystemName() + "_unSelectDeselect.remainingTime", sRemainingTime,
		         getSystemName() + "_unSelectDeselect.timeOut", iTimeOut);      
      
      unProgressBar_setMax("SelectionProgressBar", iTimeOut);
      unProgressBar_setPosition("SelectionProgressBar", iTimeOut);

      while (strsplit(sRemainingTime, ";")[3] != "0" && bSelect) {
        unRecipeFunctions_getSelectedRecipeDp(sSelectedRecipeDp, exceptionInfo);
        if (sDpName != sSelectedRecipeDp) {
          return;
      		}

        dpGet(getSystemName() + "_unSelectDeselect.remainingTime", sRemainingTime);
        unProgressBar_setPosition("SelectionProgressBar", strsplit(sRemainingTime, ";")[3]);
    		  delay(5);
      		// If the DP was removed, return
        if (!dpExists(sDpName + ".statusInformation.selectedManager")) {
          return;
        }
  		    dpGet(sDpName + ".statusInformation.selectedManager", sSelectedManager);
  		    bSelect = (sSelectedManager != "");
  	   }
    }
    
    unRecipeFunctions_getSelectedRecipeDp(sSelectedRecipeDp, exceptionInfo);
    if (sDpName != sSelectedRecipeDp) {
	     return;
   	}
	   unProgressBar_setPosition("SelectionProgressBar", 0);     
  } 
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Selects an item in a combo box.
 * @param sShape - [IN] Combo box name.
 * @param sSelect - [IN] Item to select.
 * @param indexOtherwise - [IN] Index to select in case the selected item does not exist.
 * @return TRUE if the item to select exists in the combobox, otherwise false.
 */
public bool unRecipeFunctions_selectItemInComboBox(string sShape, string sSelect, int indexOtherwise=1) {  
  dyn_string dsItems;
  
  if (!shapeExists(sShape)) {
    dyn_string exceptionInfo;
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return false;
  }    
  
  getValue(sShape, "items", dsItems);
  int iPos = dynContains(dsItems, sSelect);
  if (iPos>0) {
    setValue(sShape, "selectedPos", iPos);
  } else if (dynlen(dsItems)>1 && indexOtherwise>0) {
    setValue(sShape, "selectedPos", indexOtherwise);
  } else {
    return false;
  }
  
  return true;
}
//------------------------------------------------------------------------------------------------------------------------
public bool _unRecipeFunctions_showValuesOutOfRange(dyn_int diErrorLines) {
  // check if the range of any value is wrong
  if (dynlen(diErrorLines)) {
    int iLen=dynlen(diErrorLines);
	
    dyn_string dsElementsIndex;
    getValue("RecipeElements", "getColumnN", 0, dsElementsIndex);
    for (int i=1; i<=iLen; i++) {
      int index = (int)diErrorLines[i];
      int iPos = dynContains(dsElementsIndex, index);
      if (iPos>0) {
        setValue("RecipeElements", "cellBackColRC", (iPos-1), "Value", CELL_VALUE_OUT_OF_RANGE_COLOR);
      }
    }
    return TRUE;
  }
  
  return FALSE;
}
//------------------------------------------------------------------------------------------------------------------------

/** 
 * Set the background color of a column in a table.
 * @param tableName  - [IN] The name of the table.
 * @param columnName - [IN] The name of the column.
 * @param color		 - [IN] The new color for the column.
 */
public void _unRecipeFunctions_setTableColumnBGColor(string tableName, string columnName, string color)
{
  int lines;
  int rc=getValue(tableName, "lineCount", lines);
  if(rc)  return;

  for (int i=0; i<lines; i++) {
    setValue(tableName, "cellBackColRC", i, columnName, color);
  }  
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Set the background color of a row in a table.
 * @param tableName - [IN] The name of the table.
 * @param rowIndex  - [IN] The row index.
 * @param color		- [IN] The new color for the row.
 */
public void _unRecipeFunctions_setTableRowBGColor(string tableName, int rowIndex, string color)
{
  int cols, rows; 
  int rc=getMultiValue(tableName, "columnCount", cols,
                       tableName, "lineCount", rows);
  if (rc) {
    return;
  }
  
  if (rowIndex >= rows) {
    return;
  }
  
  for (int i=0; i<cols; i++) {
    string colName;
    getValue(tableName, "columnName", i, colName);
    setValue(tableName, "cellBackColRC", rowIndex, colName, color);
  }  
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Set the background color of a cell in a table.
 * @param tableName  - [IN] The name of the table.
 * @param rowIndex   - [IN] The row index.
 * @param columnName - [IN] The name of the column.
 * @param color		 - [IN] The new color for the row.
 */
public void _unRecipeFunctions_setTableCellBGColor(string tableName, int rowIndex, string columnName, string color)
{
  int lines;
  int rc=getValue(tableName, "lineCount", lines);
  if(rc) {
    return;
  }

  setValue(tableName, "cellBackColRC", rowIndex, columnName, color);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Restore the original colors in the recipe elements table (recipe instance panel).
 */
public void _unRecipeFunctions_resetRecipeElementsBgColor() {
  int lines;

  getValue("RecipeElements", "lineCount", lines);
  for (int i=1; i<=lines; i++){
    _unRecipeFunctions_setTableRowBGColor("RecipeElements", (i-1), CELL_BG_COLOR);
    // Resets the font style
    RecipeElements.rowFontType (i-1, makeDynInt(0));
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Set the background color of the rows in the RecipeElements table that contains invalid values.
 * @param rows       - [IN] Indices of the invalid rows.
 * @param sTableName - [IN] Name of the table where the invalid rows must be set.
 */
public void _unRecipeFunctions_setInvalidRows(dyn_string rows, string sTableName="RecipeElements") {
  int len=dynlen(rows);
  for (int i=1; i<=len; i++) {
    int row = (int) rows[i];
    _unRecipeFunctions_setTableRowBGColor(sTableName, (row-1), CELL_ERROR_COLOR); 
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Method used to trim a label to the appropriate size.
 * @param hiddenShape - [IN] Hidden shape (label) to calculate the width of the string.
 * @param aString 	  - [IN] String to be trimmed if its width is bigger than the size specified.
 * @param width		  - [IN] Width of the shape where the label will be placed.
 * @return A new string that fits in the specified width.
 */
public string _unRecipeFunctions_adjustStringToSize(shape hiddenShape, string aString, int width) {
	int x, y;
	string sTmpStr = aString;
	bool bCalculateNewSize, bResized = false;

	do {
		setValue(hiddenShape, "text", sTmpStr);
		getValue(hiddenShape, "size", x, y);
		bCalculateNewSize = (x>width-8);
		if (bCalculateNewSize) {
			sTmpStr = substr(sTmpStr, 0, strlen(sTmpStr)-1);
			bResized = true;
		}
	} while (bCalculateNewSize);
	
	if (bResized) {
	  sTmpStr = sTmpStr + "..";
	}
	
	return sTmpStr;
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Enable/disable some graphical components of the recipe instance panel (required for the recipe activation).
 * @param value - [IN] The value for the enabled property of the graphical components.
 */
public void _unRecipeFunctions_setRecipeInstancePanelComponentsEnabled(bool value) {
  if (shapeExists("RecipeClasses")) {
	setValue("RecipeClasses", "enabled", value);
  }
	
  if (shapeExists("RecipeInstances")) {
	  setValue("RecipeInstances", "enabled", value);
  }
	
  if (shapeExists("RecipeElements")) {
	  setValue("RecipeElements", "enabled", value);
  }
	
  if (shapeExists("PcoList")) {
	  setValue("PcoList", "enabled", value);
  }
  
  if (shapeExists("ApplicationList")) {
    setValue("ApplicationList", "enabled", value);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function to show/hide the recipe instance warning message.
 * @param bEnable  - [IN] Flag to specify the visibility of the warning message.
 * @param sMessage - [IN] Message to display in the WarningText label.
 */
public void _unRecipeFunctions_enableRecipeInstanceWarning(bool bEnable, string sMessage="") {
  if (shapeExists("WarningIcon")) {
	setValue("WarningIcon", "visible", bEnable);
  }
	
  if (shapeExists("WarningText")) {
    setValue("WarningText", "text", sMessage);
	setValue("WarningText", "visible", bEnable);	
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Registers the callback to the select button
 * @param sDp - [IN] 
 * @param bSystemConnected - [IN] 
 * @reviewed 2018-08-06 @whitelisted{Callback}
 */
public void unRecipeFunctions_registerSelectButtonCB(string sDp, bool bSystemConnected) {
	string deviceName;
	int iAction, iRes;
	dyn_string exceptionInfo;
	
	deviceName = unGenericDpFunctions_getDpName($sDpName);
	unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
	switch (iAction)
		{
		case UN_ACTION_DISCONNECT:
			unRecipeFunctions_disconnectSelectButton();
			break;
		case UN_ACTION_DPCONNECT:
			iRes = dpConnect("unRecipeFunctions_animateSelectButtonCB",
							 deviceName + ".statusInformation.selectedManager:_lock._original._locked",
							 deviceName + ".statusInformation.selectedManager");
			g_bCallbackConnected = (iRes >= 0);
			break;
		case UN_ACTION_DPDISCONNECT_DISCONNECT:
			iRes = dpDisconnect("unRecipeFunctions_animateSelectButtonCB",
							 deviceName + ".statusInformation.selectedManager:_lock._original._locked",
							 deviceName + ".statusInformation.selectedManager");
			g_bCallbackConnected = !(iRes >= 0);
			unRecipeFunctions_disconnectSelectButton();
			break;
		case UN_ACTION_NOTHING:
		default:
			break;
		}
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Animates the select button
 * @param sDp1
 * @param bLocked
 * @param sDp2
 * @param sSelectedManager
 * @reviewed 2018-08-06 @whitelisted{Callback}
 */
public void unRecipeFunctions_animateSelectButtonCB(string sDp1, bool bLocked, string sDp2, string sSelectedManager) {
  if(!bLocked)	{
	  	if((g_bFirst == true) && (strpos(myModuleName(), "unicosGFContextPanel") > 0)) {
   			unGraphicalFrame_restoreLastPanelDpContextualPanel(unGenericDpFunctions_getDpName($sDpName));
			 }
		} else {
  		g_bFirst = true;
		}
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Disconnection of the select button
 */
public void unRecipeFunctions_disconnectSelectButton() {
  unGraphicalFrame_restoreLastPanelDpContextualPanel(unGenericDpFunctions_getDpName($sDpName));
}

//------------------------------------------------------------------------------------------------------------------------
