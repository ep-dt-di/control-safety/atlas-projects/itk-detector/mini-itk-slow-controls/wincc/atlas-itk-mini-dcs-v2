/**
 * UNICOS
 * Copyright (C) CERN 2017 All rights reserved
 */
/**@file

// unRcpFunctions_privileges.ctl
This file contains functions related to the recipes privileges.

@par Creation Date
  15/03/2017

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (BE-ICS)
*/
#uses "fwAccessControl/fwAccessControl.ctc"
#uses "fwGeneral/fwException.ctl"
#uses "fwGeneral/fwGeneral.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unRcpFunctions_utilities.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unicos_declarations.ctl"
#uses "unRcpFunctions_class.ctl"
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the default privileges of a recipe device (recipe class or recipe instance).
 * @param sDpType 			  - [IN] Datapoint type which default privileges are required.
 * @param sOperatorPrivileges - [OUT] Default privileges for the operator.
 * @param sExpertPrivileges   - [OUT] Default privileges for the expert.
 * @param sAdminPrivileges    - [OUT] Default privileges for the administrator.
 * @param exceptionInfo       - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getDeviceDefaultPrivileges(string sDpType, string & sOperatorPrivileges, string & sExpertPrivileges, 
		string & sAdminPrivileges, dyn_string exceptionInfo) 
{
  string sFunction;
  dyn_string dsOperatorPrivileges, dsExpertPrivileges, dsAdminPrivileges;
  
  if (sDpType == UN_CONFIG_UNRCPCLASS_DPT_NAME) {
	sFunction = "unRecipeFunctions_getDefaultClassPrivileges";
  } else if (sDpType == UN_CONFIG_UNRCPINSTANCE_DPT_NAME) {
    sFunction = "unRecipeFunctions_getDefaultInstancePrivileges";
  } else {
    fwException_raise(exceptionInfo, "ERROR", "_unRecipeFunctions_getDeviceDefaultPrivileges - The DPType " +sDpType+ " is unknown." ,"");
    DebugTN(exceptionInfo);
    return;
  }
  
  string sFunctionDefinition = "dyn_string main(string sRoleType)"+
  										"{	dyn_string temp;"+
  										"	"+sFunction+"(sRoleType, temp);"+
  										"	return temp;"+
  										"}";
										
  evalScript(dsOperatorPrivileges, sFunctionDefinition, makeDynString(), "Operator");     
  evalScript(dsExpertPrivileges, sFunctionDefinition, makeDynString(), "Expert");  
  evalScript(dsAdminPrivileges, sFunctionDefinition, makeDynString(), "Admin");  
  fwGeneral_dynStringToString(dsOperatorPrivileges, sOperatorPrivileges, ",");
  fwGeneral_dynStringToString(dsExpertPrivileges, sExpertPrivileges, ",");
  fwGeneral_dynStringToString(dsAdminPrivileges, sAdminPrivileges, ",");
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Update the recipe class privileges.
 * @param sDpName - [IN] Recipe class DP name.
 */
public void unRecipeFunctions_updateRecipeClassPrivileges(string sDpName) {
  string sDeviceOperatorPrivileges, sDeviceExpertPrivileges, sDeviceAdminPrivileges;
  string sDefaultOperatorPrivileges, sDefaultExpertPrivileges, sDefaultAdminPrivileges;
  dyn_string dsDeviceOperatorPrivileges, dsDeviceExpertPrivileges, dsDeviceAdminPrivileges;
  dyn_string dsDefaultOperatorPrivileges, dsDefaultExpertPrivileges, dsDefaultAdminPrivileges;
  
  // Get the default privileges for the recipe class
  unRecipeFunctions_getDefaultClassPrivileges("operator", dsDefaultOperatorPrivileges);
  unRecipeFunctions_getDefaultClassPrivileges("expert", dsDefaultExpertPrivileges);
  unRecipeFunctions_getDefaultClassPrivileges("admin", dsDefaultAdminPrivileges);
  fwGeneral_dynStringToString(dsDefaultOperatorPrivileges, sDefaultOperatorPrivileges, ",");
  fwGeneral_dynStringToString(dsDefaultExpertPrivileges, sDefaultExpertPrivileges, ",");
  fwGeneral_dynStringToString(dsDefaultAdminPrivileges, sDefaultAdminPrivileges, ",");
  
  // Get the device privileges
  unRecipeFunctions_normalizeDp(sDpName);
  dpGet(sDpName + ".statusInformation.accessControl.operator", sDeviceOperatorPrivileges,
        sDpName + ".statusInformation.accessControl.expert", sDeviceExpertPrivileges,
        sDpName + ".statusInformation.accessControl.admin", sDeviceAdminPrivileges);
		  		  
  if (sDeviceOperatorPrivileges=="" && sDeviceExpertPrivileges=="" && sDeviceAdminPrivileges=="") {
    // Apply the default privileges
    dpSet(sDpName + ".statusInformation.accessControl.operator", sDefaultOperatorPrivileges,
          sDpName + ".statusInformation.accessControl.expert", sDefaultExpertPrivileges,
          sDpName + ".statusInformation.accessControl.admin", sDefaultAdminPrivileges);
  } else {
    // There are some privileges defined in the device, add the new privileges
    dsDeviceOperatorPrivileges = strsplit(sDeviceOperatorPrivileges, ",");
    dsDeviceExpertPrivileges   = strsplit(sDeviceExpertPrivileges, ",");
    dsDeviceAdminPrivileges    = strsplit(sDeviceAdminPrivileges, ",");
		
    // Replace the 'Apply' privilege by 'ApplyChanges' (from version 1.1.x to 1.2.0)
    unRecipeFunctions_replaceDynItem(dsDeviceOperatorPrivileges, "Apply", "ApplyChanges");
    unRecipeFunctions_replaceDynItem(dsDeviceExpertPrivileges, "Apply", "ApplyChanges");
    unRecipeFunctions_replaceDynItem(dsDeviceAdminPrivileges, "Apply", "ApplyChanges");		
    
    fwGeneral_dynStringToString(dsDeviceOperatorPrivileges, sDeviceOperatorPrivileges, ",");
    fwGeneral_dynStringToString(dsDeviceExpertPrivileges, sDeviceExpertPrivileges, ",");
    fwGeneral_dynStringToString(dsDeviceAdminPrivileges, sDeviceAdminPrivileges, ",");
    
    unRecipeFunctions_arrangeRecipeInstancePrivileges(sDefaultOperatorPrivileges, sDefaultExpertPrivileges, sDefaultAdminPrivileges, 
                                                      sDeviceOperatorPrivileges, sDeviceExpertPrivileges, sDeviceAdminPrivileges);
		
    // Apply the device privileges
    dpSet(sDpName + ".statusInformation.accessControl.operator", sDeviceOperatorPrivileges,
          sDpName + ".statusInformation.accessControl.expert", sDeviceExpertPrivileges,
          sDpName + ".statusInformation.accessControl.admin", sDeviceAdminPrivileges);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Update the recipe instances privileges.
 * @param sDpName - [IN] DP name of the recipe instance.
 * @param sClassName - [IN] Recipe class name.
 */
public void unRecipeFunctions_updateRecipeInstancePrivileges(string sDpName, string sClassName) {
  string sPredefinedRcp;
  string sDeviceAcDomain,sDeviceOperatorPrivileges, sDeviceExpertPrivileges, sDeviceAdminPrivileges;
  string sDefaultAcDomain, sDefaultOperatorPrivileges, sDefaultExpertPrivileges, sDefaultAdminPrivileges;

  // Get the default privileges for the recipe instance from the predefined recipe instance
  unRecipeFunctions_getRecipeInstancePredefinedPrivileges(sClassName, sPredefinedRcp, sDefaultAcDomain, sDefaultOperatorPrivileges, sDefaultExpertPrivileges, sDefaultAdminPrivileges);
  unRecipeFunctions_normalizeDp(sDpName);
  
  // Get the device privileges
  dpGet(sDpName + ".statusInformation.accessControl.accessControlDomain", sDeviceAcDomain,
        sDpName + ".statusInformation.accessControl.operator", sDeviceOperatorPrivileges,
        sDpName + ".statusInformation.accessControl.expert", sDeviceExpertPrivileges,
        sDpName + ".statusInformation.accessControl.admin", sDeviceAdminPrivileges);
		  		  
  // If the AcDomain is empty on the device, it applies the default AcDomain 
  if (sDeviceAcDomain == "" && sDefaultAcDomain!="") {
      dpSet(sDpName + ".statusInformation.accessControl.accessControlDomain", sDefaultAcDomain);
  }  
  
  if (sDeviceOperatorPrivileges=="" && sDeviceExpertPrivileges=="" && sDeviceAdminPrivileges=="") {
    // Apply the default privileges
    int iRes = dpSet(
      sDpName + ".statusInformation.accessControl.operator", sDefaultOperatorPrivileges,
      sDpName + ".statusInformation.accessControl.expert", sDefaultExpertPrivileges,
      sDpName + ".statusInformation.accessControl.admin", sDefaultAdminPrivileges);
    if (iRes<0) {
      dyn_errClass err = getLastError();
      DebugTN(err);
    }
  } else {
    // There are some privileges defined in the device, add the new privileges
    unRecipeFunctions_arrangeRecipeInstancePrivileges(sDefaultOperatorPrivileges, sDefaultExpertPrivileges, sDefaultAdminPrivileges, 
                                                      sDeviceOperatorPrivileges, sDeviceExpertPrivileges, sDeviceAdminPrivileges);
   
    // Apply the device privileges
    dpSet(
      sDpName + ".statusInformation.accessControl.operator", sDeviceOperatorPrivileges,
      sDpName + ".statusInformation.accessControl.expert", sDeviceExpertPrivileges,
      sDpName + ".statusInformation.accessControl.admin", sDeviceAdminPrivileges);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Adds the unassigned privileges and removes the obsolete ones.
 * @param sDefaultOperatorPrivileges - [IN] Default operator privileges.
 * @param sDefaultExpertPrivileges   - [IN] Default expert privileges.
 * @param sDefaultAdminPrivileges   - [IN] Default admin privileges.
 * @param sDeviceOperatorPrivileges - [IN/OUT] Device operator privileges.
 * @param sDeviceExpertPrivileges   - [IN/OUT] Device expert privileges.
 * @param sDeviceAdminPrivileges    - [IN/OUT] Device admin privileges.
 */
public void unRecipeFunctions_arrangeRecipeInstancePrivileges(string sDefaultOperatorPrivileges, string sDefaultExpertPrivileges, 
                                                              string sDefaultAdminPrivileges, string &sDeviceOperatorPrivileges, 
                                                              string &sDeviceExpertPrivileges, string &sDeviceAdminPrivileges) {
  
  dyn_string dsDefaultOperatorPrivileges, dsDefaultExpertPrivileges, dsDefaultAdminPrivileges;
  dyn_string dsDeviceOperatorPrivileges, dsDeviceExpertPrivileges, dsDeviceAdminPrivileges;
  
  dsDeviceOperatorPrivileges  = strsplit(sDeviceOperatorPrivileges, ",");
  dsDeviceExpertPrivileges    = strsplit(sDeviceExpertPrivileges, ",");
  dsDeviceAdminPrivileges     = strsplit(sDeviceAdminPrivileges, ",");
  dsDefaultOperatorPrivileges = strsplit(sDefaultOperatorPrivileges, ",");
  dsDefaultExpertPrivileges   = strsplit(sDefaultExpertPrivileges, ",");
  dsDefaultAdminPrivileges    = strsplit(sDefaultAdminPrivileges, ",");
  
  // Add the operator default privileges
  _unRecipeFunctions_addPrivileges(dsDefaultOperatorPrivileges, dsDeviceOperatorPrivileges, dsDeviceExpertPrivileges, dsDeviceAdminPrivileges);
				
  // Add the expert default privileges
  _unRecipeFunctions_addPrivileges(dsDefaultExpertPrivileges, dsDeviceExpertPrivileges, dsDeviceOperatorPrivileges, dsDeviceAdminPrivileges);
				
  // Add the admin default privileges
  _unRecipeFunctions_addPrivileges(dsDefaultAdminPrivileges, dsDeviceAdminPrivileges, dsDeviceOperatorPrivileges, dsDeviceExpertPrivileges);
		
  // Remove the operator obsolete privileges 
  _unRecipeFunctions_removeObsoletePrivileges(dsDeviceOperatorPrivileges, dsDefaultOperatorPrivileges, dsDefaultExpertPrivileges, dsDefaultAdminPrivileges);

  // Remove the expert obsolete privileges 
  _unRecipeFunctions_removeObsoletePrivileges(dsDeviceExpertPrivileges, dsDefaultOperatorPrivileges, dsDefaultExpertPrivileges, dsDefaultAdminPrivileges);
				
  // Remove the admin obsolete privileges 
  _unRecipeFunctions_removeObsoletePrivileges(dsDeviceAdminPrivileges, dsDefaultOperatorPrivileges, dsDefaultExpertPrivileges, dsDefaultAdminPrivileges);				
		
  fwGeneral_dynStringToString(dsDeviceOperatorPrivileges, sDeviceOperatorPrivileges, ",");
  fwGeneral_dynStringToString(dsDeviceExpertPrivileges, sDeviceExpertPrivileges, ",");
  fwGeneral_dynStringToString(dsDeviceAdminPrivileges, sDeviceAdminPrivileges, ",");
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Auxiliary function to add the default privileges to the privileges list.
 * If none of the dyn_strings (dsDevicePrivileges1, dsDevicePrivileges2, dsDevicePrivileges3) contains the privileges
 * included in dsDefaultPrivileges, the privilege will be added to dsDevicePrivileges1. Otherwise (the privilege is included
 * in any dyn_string) no actions will be done.
 * @param dsDefaultPrivileges - [IN] The list of privileges to add.
 * @param dsDevicePrivileges1 - [OUT] The list where add the privileges if they don't exist in any dyn_string.
 * @param dsDevicePrivileges2 - [IN] Device privileges list.
 * @param dsDevicePrivileges3 - [IN] Device privileges list.
 */
private void _unRecipeFunctions_addPrivileges(dyn_string dsDefaultPrivileges, dyn_string &dsDevicePrivileges1, 
					dyn_string dsDevicePrivileges2, dyn_string dsDevicePrivileges3)
{
  int i, iLen;
  string sPrivilege;
  
  iLen = dynlen(dsDefaultPrivileges);
  for (i=1; i<=iLen; i++) {
    sPrivilege = dsDefaultPrivileges[i];
	
    if (dynContains(dsDevicePrivileges1, sPrivilege) || 
        dynContains(dsDevicePrivileges2, sPrivilege) ||
        dynContains(dsDevicePrivileges3, sPrivilege) ) {
            continue;
    }
		
    // The current privilege is not assigned to any role,
    // add it to the default role.
    dynAppend(dsDevicePrivileges1, sPrivilege);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Auxiliary function to remove the obsolete privileges from the privileges list.
 * @param dsDevicePrivileges - [OUT] List where the obsolete parameters will be removed.
 * @param dsDefaultPrivileges1 - [IN] Default privileges list 1 (e.g. operator).
 * @param dsDefaultPrivileges2 - [IN] Default privileges list 2 (e.g. expert). 
 * @param dsDefaultPrivileges3 - [IN] Default privileges list 3 (e.g. admin).
 */
private void _unRecipeFunctions_removeObsoletePrivileges(dyn_string & dsDevicePrivileges, dyn_string dsDefaultPrivileges1, 
				dyn_string dsDefaultPrivileges2, dyn_string dsDefaultPrivileges3) 
{
	int i, iLen;
	
	iLen = dynlen(dsDevicePrivileges);
	for (i=1; i<=iLen; i++) {
	  string sPrivilege = dsDevicePrivileges[i];
	  
	  if (dynContains(dsDefaultPrivileges1, sPrivilege) ||
          dynContains(dsDefaultPrivileges2, sPrivilege) || 
          dynContains(dsDefaultPrivileges3, sPrivilege)) 
      {
        continue;
      }

	  // Remove the obsolete privilege
	  dynRemove(dsDevicePrivileges, i);
	  iLen--;
	  i--;
	}
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Gets the predefined privileges for a recipe instance based on the recipe class privileges.
 * @param sClassName      - [IN] Recipe class name
 * @param sRcpInstanceDp  - [OUT] Datapoint name of the predefined recipe instance
 * @param sAcDomain       - [OUT] Access control domain
 * @param sOperatorAction - [OUT] Operator privileges
 * @param sExpertAction   - [OUT] Expert privileges
 * @param sAdminAction    - [OUT] Admin privileges
 */
public void unRecipeFunctions_getRecipeInstancePredefinedPrivileges
    (string sClassName, string &sRcpInstanceDp, string &sAcDomain, string &sOperatorAction, string &sExpertAction, string &sAdminAction) {
  
  // Get the recipe instance privileges and ACDomain.
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sClassName + "/" + UN_RCP_PREDEFINED_NAME, sRcpInstanceDp);

  dpGet(sRcpInstanceDp + "accessControl.accessControlDomain", sAcDomain,
        sRcpInstanceDp + "accessControl.operator", sOperatorAction,
        sRcpInstanceDp + "accessControl.expert", sExpertAction,
        sRcpInstanceDp + "accessControl.admin", sAdminAction);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Open a new window to edit the recipe class privileges and parameters.
 * @param sRcpClassDp - [IN] The recipe class DP.
 */
public void unRecipeFunctions_editRecipeClassPrivileges(string sRcpClassDp) {
  int i, iLen;
  string sClassName, sDefinedACDomains, sClassACDomain, sClassOperatorAction, sClassExpertAction, sClassAdminAction;
  string sRcpInstanceDp, sInstanceACDomain, sInstanceOperatorAction, sInstanceExpertAction, sInstanceAdminAction;
  dyn_string ds, dsAccessControlDomains, dsFullAccessControlDomain, exceptionInfo, dsRcpInstanceNames, dsRcpInstanceDps;
  dyn_float df;
  
  // Get the recipe class privileges and ACDomain.
  sClassName = dpGetAlias(sRcpClassDp + ".");
  _unRecipeFunctions_getDevicePrivileges(sRcpClassDp, sClassACDomain, sClassOperatorAction, 
			sClassExpertAction, sClassAdminAction);
			
  // Get the recipe instance privileges and ACDomain.
  unRecipeFunctions_getRecipeInstancePredefinedPrivileges(sClassName, sRcpInstanceDp, sInstanceACDomain, sInstanceOperatorAction, sInstanceExpertAction, sInstanceAdminAction);
		
  // Get the list of access control domains
  fwAccessControl_getAllDomains(dsAccessControlDomains, dsFullAccessControlDomain, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    string errMsg = "Error getting the list of access control domains.";
    unRecipeFunctions_writeInRecipeLog(errMsg);
    DebugTN(errMsg, exceptionInfo);
    return;
  }
  
  // Remove the SYSTEM DOMAIN if it exists
  if ( (i=dynContains(dsAccessControlDomains, "SYSTEM")) > 0) {
    dynRemove(dsAccessControlDomains, i);
  }
  fwGeneral_dynStringToString(dsAccessControlDomains, sDefinedACDomains, ",");

  // Display the panel with the recipe class and recipe instance privileges  
  ChildPanelOnCentralModalReturn(
		"vision/unRecipe/UnRcpClass/unUnRcpClass_Privileges.pnl",
		sClassName + " - Edit recipe privileges",
		makeDynString(
			"$sDpName:" + sRcpClassDp,
			"$sDefinedACDomains:" + sDefinedACDomains,
			"$sClassACDomain:" + sClassACDomain,
			"$sClassOperatorAction:" + sClassOperatorAction,
			"$sClassExpertAction:" + sClassExpertAction,
			"$sClassAdminAction:" + sClassAdminAction,
			"$sInstanceACDomain:" + sInstanceACDomain,
			"$sInstanceOperatorAction:" + sInstanceOperatorAction,
			"$sInstanceExpertAction:" + sInstanceExpertAction,
			"$sInstanceAdminAction:" + sInstanceAdminAction
			),
		df, ds);
		
  // Check if the Ok button has been pressed
  if (dynlen(df) && df[1]>0) {
    // Set the recipe class privileges
	sClassACDomain = ds[1];
	sClassOperatorAction = ds[2];
	sClassExpertAction = ds[3];
	sClassAdminAction = ds[4];	
	_unRecipeFunctions_setDevicePrivileges(sRcpClassDp, sClassACDomain, sClassOperatorAction, 
			sClassExpertAction, sClassAdminAction);
	
	// Get the recipe instance privileges
	sInstanceACDomain = ds[5];
	sInstanceOperatorAction = ds[6];
	sInstanceExpertAction = ds[7];
	sInstanceAdminAction = ds[8];

	// Apply the privileges to the default recipe instance
	dpSet(sRcpInstanceDp + "accessControl.accessControlDomain", sInstanceACDomain,
        sRcpInstanceDp + "accessControl.operator", sInstanceOperatorAction,
		sRcpInstanceDp + "accessControl.expert", sInstanceExpertAction,
		sRcpInstanceDp + "accessControl.admin", sInstanceAdminAction);
	
	// Get all the recipe instances of the class
	_unRecipeFunctions_getRecipeInstancesOfClass(sClassName, dsRcpInstanceNames, dsRcpInstanceDps);
	// Set the recipe instances privileges
	iLen = dynlen(dsRcpInstanceDps);
	for (i=1; i<=iLen; i++) {
		_unRecipeFunctions_setDevicePrivileges(dsRcpInstanceDps[i], sInstanceACDomain, sInstanceOperatorAction, 
			sInstanceExpertAction, sInstanceAdminAction);
	}
	
	unRecipeFunctions_writeInRecipeLog("The privileges of the recipe class " + sClassName + 
			" and all its instances have been modified.", TRUE);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Auxiliary function to get the device privileges.
 * @param sDpName 	   - [IN] Datapoint name of the device.
 * @param sAcDomain	   - [OUT] Access control domain for the device.
 * @param sOperatorAction - [OUT] Privileged actions for the operator.
 * @param sExpertAction   - [OUT] Privileged actions for the expert.
 * @param sAdminAction	  - [OUT] Privileged actions for the admin.
 */
private void _unRecipeFunctions_getDevicePrivileges(string sDpName, string & sAcDomain, string & sOperatorAction, 
			string & sExpertAction, string & sAdminAction) 
{
	unRecipeFunctions_normalizeDp(sDpName);

	dpGet(sDpName + ".statusInformation.accessControl.accessControlDomain", sAcDomain,
 		  sDpName + ".statusInformation.accessControl.operator", sOperatorAction,
		  sDpName + ".statusInformation.accessControl.expert", sExpertAction,
		  sDpName + ".statusInformation.accessControl.admin", sAdminAction);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Auxiliary function to set the device privileges.
 * @param sDpName 	      - [IN] Datapoint name of the device.
 * @param sAcDomain	      - [IN] Access control domain for the device.
 * @param sOperatorAction - [IN] Privileged actions for the operator.
 * @param sExpertAction   - [IN] Privileged actions for the expert.
 * @param sAdminAction	  - [IN] Privileged actions for the admin.
 */
private void _unRecipeFunctions_setDevicePrivileges(string sDpName, string sAcDomain, string sOperatorAction, 
			string sExpertAction, string sAdminAction) 
{
    unRecipeFunctions_normalizeDp(sDpName);
	
	dpSet(sDpName + ".statusInformation.accessControl.accessControlDomain", sAcDomain,
 		  sDpName + ".statusInformation.accessControl.operator", sOperatorAction,
		  sDpName + ".statusInformation.accessControl.expert", sExpertAction,
		  sDpName + ".statusInformation.accessControl.admin", sAdminAction);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function used to get the default privileges for the recipe class actions.
 * @param sRoleType    - [IN] Role type (operator, expert or admin).
 * @param dsPrivileges - [OUT] List of default privileged actions for the specified role.
 */
public void unRecipeFunctions_getDefaultClassPrivileges(string sRoleType, dyn_string & dsPrivileges) 
{
  string sRole = strtolower(sRoleType);
  dynClear(dsPrivileges);
  
  if (sRole=="operator") {
    return;
  } else if (sRole=="expert") {
    return;
  } else if (sRole=="admin") {
    dynAppend(dsPrivileges, makeDynString("AddDevices", "ApplyChanges", "Cancel", "Duplicate", "EditElements", "NewClass", 
			"NewInstance", "Privileges", "RemoveClass", "RemoveSelected", "SaveToDb", "LoadFromDb", UN_FACEPLATE_BUTTON_SELECT));
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function used to get the default privileges for the recipe instance actions.
 * @param sRoleType    - [IN] Role type (operator, expert or admin).
 * @param dsPrivileges - [OUT] List of default privileged actions for the specified role.
 */
public void unRecipeFunctions_getDefaultInstancePrivileges(string sRoleType, dyn_string & dsPrivileges) 
{
  string sRole = strtolower(sRoleType);
  dynClear(dsPrivileges);
  
  if (sRole=="operator") {
    dynAppend(dsPrivileges, makeDynString("Activate", UN_FACEPLATE_BUTTON_SELECT));
  } else if (sRole=="expert"){
    dynAppend(dsPrivileges, makeDynString("Cancel", "Delete", "Duplicate", "Edit", "InitialRecipes", "NewInstance", 
				"LastActivatedRecipes", "OnlineValues", "DbValues", "Save", "SaveToDb",  "SaveAsInitial"));
  } else if (sRole=="admin") {
  }
}

//------------------------------------------------------------------------------------------------------------------------
