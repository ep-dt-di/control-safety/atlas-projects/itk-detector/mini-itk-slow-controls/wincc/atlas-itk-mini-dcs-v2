/**
  This script used to import importation configurations for ASCII Manager which allows to export data.

  Installation instruction.
  1. Copy the script to the script folder of your pvss project.
  2. If it is necessary configure constant PATH_PREFIX.
  3. Run the script via PVSS Console or OS terminal:
	a. in the PVSS Console add 'Control Manager' with script name as an options and put 'start mode' to 'manual'; then start this new manager once.
	b. in the OS console (windows cmd or linux terminal) execure follow command:
		 PVSS00ctrl -proj [PROJECT_NAME] unCPC_AsciiExportConfigurations.ctl
	   where PROJECT_NAME is name of your pvss project
*/

string PATH_PREFIX = "/afs/cern.ch/user/a/amerezhi/wccoa/cpc/installed_components/cpc/dplist/"; // path prefix for generated dpl files; may be setted to the dplist of your working project or to the unCPC/dplist folder of svn

main()
{
  import_dp_configuration("CPC_generated", PATH_PREFIX + "CPC.dpl",
                          makeDynBool(1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                          makeDynString("alle"),
                          makeDynString("CPC_AnaDig", "CPC_Analog", "CPC_AnaDO", "CPC_AnalogAlarm", "CPC_AnalogInput", "CPC_AnalogOutput", "CPC_AnalogParameter", "CPC_AnalogStatus", "CPC_Controller", "CPC_DigitalAlarm", "CPC_DigitalInput", "CPC_DigitalOutput", "CPC_DigitalParameter", "CPC_Local", "CPC_OnOff", "CPC_ProcessControlObject", "CPC_Word2AnalogStatus", "CPC_WordParameter", "CPC_WordStatus", "CPC_MassFlowController", "CPC_SteppingMotor"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle")
                          );
  import_dp_configuration("CPCinfo_generated", PATH_PREFIX + "CPCinfo.dpl",
                          makeDynBool(0, 1, 0, 1, 0, 0, 0, 0, 0, 0),
                          makeDynString("dist_1:unicosDefault_TextLabel", "dist_1:_template_unCPC_AnaDig_Trends", "dist_1:_template_unCPC_AnaDO_Trends", "dist_1:_template_unCPC_Analog_Trends", "dist_1:_template_unCPC_AnalogAlarm_Thresholds", "dist_1:_template_unCPC_AnalogAlarm_Trends", "dist_1:_template_unCPC_AnalogInput_Trends", "dist_1:_template_unCPC_AnalogOutput_Trends", "dist_1:_template_unCPC_AnalogParameter_Trends", "dist_1:_template_unCPC_AnalogStatus_Trends", "dist_1:_template_unCPC_Controller_Trends", "dist_1:_template_unCPC_DigitalAlarm_Trends", "dist_1:_template_unCPC_DigitalInput_Trends", "dist_1:_template_unCPC_DigitalOutput_Trends", "dist_1:_template_unCPC_DigitalParameter_Trends", "dist_1:_template_unCPC_OnOff_Trends", "dist_1:_template_unCPC_Word2AnalogStatus_Trends", "dist_1:_template_unCPC_WordParameter_Trends", "dist_1:_template_unCPC_WordStatus_Trends",
                                        "dist_1:CPC_AnaDig_trendConfiguration", "dist_1:CPC_AnaDig_unicosInfo", "dist_1:CPC_Analog_trendConfiguration", "dist_1:CPC_Analog_unicosInfo", "dist_1:CPC_AnaDO_trendConfiguration", "dist_1:CPC_AnaDO_unicosInfo", "dist_1:CPC_AnalogAlarm_trendConfiguration", "dist_1:CPC_AnalogAlarm_unicosInfo", "dist_1:CPC_AnalogInput_trendConfiguration", "dist_1:CPC_AnalogInput_unicosInfo", "dist_1:CPC_AnalogOutput_trendConfiguration", "dist_1:CPC_AnalogOutput_unicosInfo", "dist_1:CPC_AnalogParameter_trendConfiguration", "dist_1:CPC_AnalogParameter_unicosInfo", "dist_1:CPC_AnalogStatus_trendConfiguration", "dist_1:CPC_AnalogStatus_unicosInfo", "dist_1:CPC_Controller_trendConfiguration", "dist_1:CPC_Controller_unicosInfo", "dist_1:CPC_DigitalAlarm_trendConfiguration", "dist_1:CPC_DigitalAlarm_unicosInfo", "dist_1:CPC_DigitalInput_trendConfiguration", "dist_1:CPC_DigitalInput_unicosInfo", "dist_1:CPC_DigitalOutput_trendConfiguration", "dist_1:CPC_DigitalOutput_unicosInfo", "dist_1:CPC_DigitalParameter_trendConfiguration", "dist_1:CPC_DigitalParameter_unicosInfo", "dist_1:CPC_Local_unicosInfo", "dist_1:CPC_OnOff_trendConfiguration", "dist_1:CPC_OnOff_unicosInfo", "dist_1:CPC_ProcessControlObject_unicosInfo", "dist_1:CPC_Word2AnalogStatus_trendConfiguration", "dist_1:CPC_Word2AnalogStatus_unicosInfo", "dist_1:CPC_WordParameter_trendConfiguration", "dist_1:CPC_WordParameter_unicosInfo", "dist_1:CPC_WordStatus_trendConfiguration", "dist_1:CPC_WordStatus_unicosInfo", "dist_1:CPC_MassFlowController_trendConfiguration", "dist_1:CPC_MassFlowController_unicosInfo", "dist_1:CPC_SteppingMotor_trendConfiguration", "dist_1:CPC_SteppingMotor_unicosInfo"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle")
                          );
  import_dp_configuration("CPCfw_generated", PATH_PREFIX + "CPCfwDeviceDefinition.dpl",
                          makeDynBool(0, 1, 0, 1, 0, 0, 0, 0, 0, 0),
                          makeDynString("dist_1:CPC_AnaDigInfo", "dist_1:CPC_AnalogInfo", "dist_1:CPC_AnaDOInfo", "dist_1:CPC_AnalogAlarmInfo", "dist_1:CPC_AnalogInputInfo", "dist_1:CPC_AnalogOutputInfo", "dist_1:CPC_AnalogParameterInfo", "dist_1:CPC_AnalogStatusInfo", "dist_1:CPC_ControllerInfo", "dist_1:CPC_DigitalAlarmInfo", "dist_1:CPC_DigitalInputInfo", "dist_1:CPC_DigitalOutputInfo", "dist_1:CPC_DigitalParameterInfo", "dist_1:CPC_LocalInfo", "dist_1:CPC_OnOffInfo", "dist_1:CPC_ProcessControlObjectInfo", "dist_1:CPC_Word2AnalogStatusInfo", "dist_1:CPC_WordParameterInfo", "dist_1:CPC_WordStatusInfo", "dist_1:CPC_MassFlowControllerInfo", "dist_1:CPC_SteppingMotorInfo"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle")
                          );
  import_dp_configuration("CPCAlertClasses_generated", PATH_PREFIX + "CPCAlertClasses.dpl",
                          makeDynBool(0, 1, 1, 1, 1, 0, 0, 0, 0, 0),
                          makeDynString("dist_1:_cpcAnalogLL", "dist_1:_cpcAnalogLL_Ack", "dist_1:_cpcAnalogLL_Mail", "dist_1:_cpcAnalogLL_Ack_Mail",
                                        "dist_1:_cpcAnalogL", "dist_1:_cpcAnalogL_Ack", "dist_1:_cpcAnalogL_Mail", "dist_1:_cpcAnalogL_Ack_Mail",
                                        "dist_1:_cpcAnalogH", "dist_1:_cpcAnalogH_Ack", "dist_1:_cpcAnalogH_Mail", "dist_1:_cpcAnalogH_Ack_Mail",
                                        "dist_1:_cpcAnalogHH", "dist_1:_cpcAnalogHH_Ack", "dist_1:_cpcAnalogHH_Mail", "dist_1:_cpcAnalogHH_Ack_Mail",
                                        "dist_1:_cpcAnalog", "dist_1:_cpcAnalog_Ack", "dist_1:_cpcAnalog_Mail", "dist_1:_cpcAnalog_Ack_Mail"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle"),
                          makeDynString("alle")
                          );
}

/**
  save import DP configuration for ASCII manager into the related dp

  parameters:
  cname - configuration name
  path - exported file path
  filters - arrays of 6 booleans for output filters:
    1 - datapoint types (T) - DPTypen
    2 - data points (D) - DP
    3 - aliases and comments (A) - Alias
    4 - original values (O) - Originalwerte
    5 - parametrization (P) - Parametrierung
    6 - parametrization including history (PH) - Historisch
  dp - datapoints that will be imported. array with 1 'alle' string means all elements will be imported
  dpt - datapoint types that will be imported. array with 1 'alle' string means all elements will be imported
  sprachen
  konfig
*/
import_dp_configuration(string cname, string path, dyn_bool filters, dyn_string dp, dyn_string dpt, dyn_string sprachen, dyn_string konfig, dyn_string CNSPaths, dyn_string CNSViews) {
  string configDP = "";
  dyn_dyn_anytype allConfigs;

  // init config dpe
  // search for existing dpe
  dpQuery("SELECT '_original.._value' FROM 'o*.Name' WHERE _DPT = \"_AsciiConfig\"", allConfigs);
  for (int i = 2; i <= dynlen(allConfigs); i++) {
    if (allConfigs[i][2] == cname) {
      configDP = dpSubStr(allConfigs[i][1],DPSUB_DP);
      break;
    }
  }
  if (configDP == "") { // create new dpe
    int idx = 1;
    while (true) {
        configDP = "o_" + idx;
        if (dpExists(configDP)==0) {
          dpCreate(configDP, "_AsciiConfig");
          break;
        }
        idx += 1;
    }
  }

  // update element
  dpSet(configDP + ".Name:_original.._value",cname);

  dpSet(configDP + ".ASCII_Datei:_original.._value", path,
        configDP + ".OutFilter.DPTypen:_original.._value", filters[1],
        configDP + ".OutFilter.DP:_original.._value", filters[2],
        configDP + ".OutFilter.Alias:_original.._value", filters[3],
        configDP + ".OutFilter.Originalwerte:_original.._value", filters[4],
        configDP + ".OutFilter.Parametrierung:_original.._value", filters[5],
        configDP + ".OutFilter.Historisch:_original.._value", filters[6],
        configDP + ".OutFilter.Typreferenzen:_original.._value", filters[7],
        configDP + ".OutFilter.PwrConfEinst:_original.._value", filters[8],
        configDP + ".OutFilter.CNS:_original.._value", filters[9],
        configDP + ".OutFilter.LinkedDps:_original.._value", filters[10],
        configDP + ".Lokalzeit:_original.._value", 0,
        configDP + ".Sprachen.Mehrsprachig:_original.._value", 0,
        configDP + ".Sprachen.Sprachen:_original.._value",sprachen,
        configDP + ".DP:_original.._value",dp,
        configDP + ".DPTypen:_original.._value",dpt,
        configDP + ".Younger.Younger:_original.._value", 0,
        configDP + ".Younger.Zeit:_original.._value",getCurrentTime(),
        configDP + ".Konfig:_original.._value",konfig,
        configDP + ".CNSPaths:_original.._value",CNSPaths,
        configDP + ".CNSViews:_original.._value",CNSViews
        );

  dpSet(configDP + ".outputVersion:_original.._value", 0);
}
