This folder should contain the control extensions generated from the source codes in the Source/ subfolder above.
For the current release (WinCC OA 3.15) it should contain the following files:
    ctrlEventList.dll_3.15
    ctrlEventList.dll_3.15.pdb
    ctrlEventList.so_3.15
    ctrlEventList.so_3.15.debug
At the moment of initial checkin, these were available in SVN.

The files have been temporarily added to enable component builds, before full build system integration is achieved.
