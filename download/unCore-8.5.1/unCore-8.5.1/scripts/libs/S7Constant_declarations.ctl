/**@name LIBRARY: S7Constant_declarations.ctl

@author: Enrique Blanco (AB/CO)

Creation Date: 19-05-2005

Modification History:
  30/03/2012: Herve
  - IS-755  unCore - unFrontEnd  Front-end (SIEMENS and SCHNEIDER PLCS UCPC v6) Information updated: baseline and
  application versions added; spec version dropped. Front-end diagnostic panel updated accordingly.

  20/03/2012: Herve
  - IS-735  unCore - unFrontEnd  SIEMENS PLC: new field "resources package employed" used for the CPC 6

	- 14/04/2006: Herve
		implementation of front-end version

	- 9/9/05 Enrique:
		Include new fields for UN_CONFIG_S7 (total length now is 18)
				- version of PLC UNICOS application (PLC address where the PLC version is placed)
				- version of PVSS UNICOS application (float number read from the import file)

version 1#
*/


// constant declaration
const string S7_DPNAME_PREFIX = "S7";
const string S7_POLLING_COMMUNICATION = "_POLL";

// PLC diagnostic info is read every x seconds
const unsigned S7_POLL_PLCINFO_TIME = 10000;		// 10 seconds
const bool UN_CONFIG_LOWLEVEL_POLLING_VALUES = true;

// Data type S7
const int PVSS_S7_INT16 =701;
const int PVSS_S7_INT32 =702;
const int PVSS_S7_UINT16 =703;
const int PVSS_S7_BOOL =706;
const int PVSS_S7_FLOAT =705;

// Defined in the lib: unicos_declarations.ctl, unicos_declarations_core.ctl
// maybe should be defined here...
// const string S7_PLC_DPTYPE = "S7_PLC";

// PLC S7 Communication internal data points
// "_S7_Conn" is already defined in the lib: _fwPeriphAddressS7.ctl
// ONCE IT/CO INTEGRATES THE _fwPeriphAddressS7.ctl ON ITS FRAMEWORK THIS CONST MUST BE DELETED: _S7_Conn
// const string S7_PLC_INT_DPTYPE_CONN = "_S7_Conn";
const string S7_PLC_INT_DPTYPE_CONF = "_S7_Config";

// S7 Connection types
const unsigned S7_CONN_TYPE_PG =0;
const unsigned S7_CONN_TYPE_OP =1;
const unsigned S7_CONN_TYPE_OTHER =2;
const unsigned S7_CONN_TYPE_TSPP =3;

// S7 data types for UNICOS Devices configuration
const unsigned UN_CONFIG_S7_PLC_DATATYPE_WORD=1;
const unsigned UN_CONFIG_S7_PLC_DATATYPE_DWORD=2;
const unsigned UN_CONFIG_S7_PLC_DATATYPE_FLOAT=3;
const unsigned UN_CONFIG_S7_PLC_DATATYPE_BOOL=4;
const unsigned UN_CONFIG_S7_PLC_DATATYPE_BYTE=5;

// constant for import of S7 PLC  (index to explose the array!)
// const unsigned UN_CONFIG_S7_LENGTH = 15;
// If new info from the PLC is needed then
// const unsigned UN_CONFIG_S7_LENGTH = 16;
// including 2 fields for the address of the PLC version and the PVSS version
const unsigned UN_CONFIG_S7_LENGTH = 18;

const unsigned UN_CONFIG_S7_PLC_TYPE=1;					// S7-400, S7-300
const unsigned UN_CONFIG_S7_PLC_NAME=2;					// Name of PLC
const unsigned UN_CONFIG_S7_APPLICATION=3;				// Name of the application
const unsigned UN_CONFIG_S7_LOCAL_ID=4;					// Local_ID(hex)
const unsigned UN_CONFIG_S7_LOCAL_RACK=5;				// Local_Rack
const unsigned UN_CONFIG_S7_LOCAL_SLOT=6;				// Local_Slot
const unsigned UN_CONFIG_S7_LOCAL_CONNRESOURCE=7;		// Local_ConnectionResource(hex)
const unsigned UN_CONFIG_S7_PARTNER_RACK=8;				// Partner_Rack
const unsigned UN_CONFIG_S7_PARTNER_SLOT=9;				// Partner_Slot
const unsigned UN_CONFIG_S7_PARTNER_CONNRESOURCE=10; 	// Partner_ConnectionResource(hex)
const unsigned UN_CONFIG_S7_TIMEOUT=11;					// Conexion
const unsigned UN_CONFIG_S7_PLC_IP=12;					// PLC IP number
const unsigned UN_CONFIG_S7_ADD_IP=13;					// IP send address
const unsigned UN_CONFIG_S7_ADD_COUNTER=14;				// Counter alive address
const unsigned UN_CONFIG_S7_ADD_COMMANDINTERFACE=15;	// Syncro, RequestAll address
const unsigned UN_CONFIG_S7_ADD_PLCINFO=16;	// Additional PLC info
const unsigned UN_CONFIG_S7_ADD_PLC_VERSION=17; // PLC UNICOS application version address
const unsigned UN_CONFIG_S7_PVSS_VERSION=18;		// PVSS UNICOS application version number (float)

const unsigned UN_CONFIG_S7_FE_VERSION_ADDITIONAL_LENGTH = 1;
const unsigned UN_CONFIG_S7_FE_VERSION = 19;

const unsigned UN_CONFIG_S7_LENGTH_CPC6 = 22; // CPC6
const unsigned UN_CONFIG_S7_CPC6_PLC_BASELINE_ADDRESS = 17; // PLC baseline version
const unsigned UN_CONFIG_S7_CPC6_PLC_APPLICATION_ADDRESS = 18; // PLC application version
const unsigned UN_CONFIG_S7_CPC6_RESPACK_MAJOR_VERSION_ADDRESS = 19; // (S7 address word polling) WORD, Addressing to read the major version of the Resource Package
const unsigned UN_CONFIG_S7_CPC6_RESPACK_MINOR_VERSION_ADDRESS = 20; // (S7 address word polling) WORD, Addressing to read the minor version of the Resource Package
const unsigned UN_CONFIG_S7_CPC6_RESPACK_SMALL_VERSION_ADDRESS= 21; // (S7 address word polling) WORD, Addressing to read the small version of the Resource Package
const unsigned UN_CONFIG_S7_CPC6_RESPACK_VERSION = 22; // (String) : version of the Resource Package employed

//---------------------------------------------------------------------------------------------------------------------------------------
