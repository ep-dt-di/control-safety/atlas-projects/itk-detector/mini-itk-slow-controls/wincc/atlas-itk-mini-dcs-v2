// $License: NOLICENSE

/**
@file unApplicationConfiguration.ctl
Functions used to configure automatically a UNICOS application.
The functions here assume the user is connected as root, as some administration settings can be modified.

@author Cyril Caillaba (EN-ICE-SCD)
@version 1.0.0
@date March 2012

*/

#uses "CtrlXml"
#uses "fwAccessControl/fwAccessControl.ctc"
#uses "fwAccessControl/fwAccessControl_Egroups.ctc"


// Constant indexes that will be used to read in dynamic arrays without any mistake

// ------ Index of the project global parameters ------
const int UN_APPLICATION_CONFIGURATION_PARAMETERS                       = 1;  // dyn_anytype

// ------ Index of the panel settings ------
const int UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS             = 2;  // dyn_anytype

// ------ Index of the alarm archive config ------
const int UN_APPLICATION_CONFIGURATION_INDEX_ALARM_CONFIG               = 3;  // dyn_anytype

// ------ Index of the front-end application list ------
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST = 4;  // dyn_dyn_anytype

// ------ Index of the archive list ------
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST               = 5;  // dyn_dyn_anytype

// ------ Index of the user access control ------
const int UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL        = 6;  // Mapping

// ------ Index of the email groups configuration
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS       = 7;  // dyn_dyn_anytype

// ------ Index of the autologout configuration
const int UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT                 = 8;  // dyn_int



// -----------------------------------------------
// 1) Elements of the global parameters ----------
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_APPLICATION_NAME = 1;
const int UN_APPLICATION_CONFIGURATION_INDEX_DISTRIBUTED_PORT = 2;
const int UN_APPLICATION_CONFIGURATION_INDEX_BACKUP_PATH      = 3;
const int UN_APPLICATION_CONFIGURATION_INDEX_DEFAULT_PANEL    = 4;

// -----------------------------------------------
// 2) Elements of the panel settings -------------
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT          = 1;

// -----------------------------------------------
// 3) Elements of the alarm config ---------------
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_ALARM_CONFIG_DELETION_CYCLE = 1;

// -----------------------------------------------
// 4) Elements of a front-end application --------
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_NAME = 1;
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE = 2;

// -----------------------------------------------
// Elements of a front-end application type
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_NAME = 1;
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_S7_DRIVER_NUM = 2;
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_DRIVER_NUM = 2;
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_DATA_SERVER_PORT = 3;
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_FRONT_END_IP_ADDRESS = 4;

// -----------------------------------------------
// 5) Elements of an archive set in the archive list
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NAME = 1;
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_ANA = 2;
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_BOOL = 3;
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_EVENT = 4;

// -----------------------------------------------
// Elements of an archive in the set
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM = 1;
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_VALUES = 2;
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_DPES = 3;
const int UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_FILES = 4;

// -----------------------------------------------
// 6) Elements of the user access control node node
// -----------------------------------------------
const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_LDAP = "LDAP";
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_LDAP_USE = 1;

const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_CENTRAL_SERVER = "CENTRAL_SERVER"; // dyn_anytype
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_CENTRAL_SERVER_NAME = 1;

const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_LIST = "DOMAIN_LIST";       // dyn_dyn_anytype
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_NAME        = 1;            // string
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_FULL_NAME   = 2;            // string
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_COMMENT     = 3;            // string
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_PRIVILEGES  = 4;            // dyn_string

const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_LIST = "GROUP_LIST";         // dyn_dyn_anytype
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_NAME         = 1;            // string
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_FULL_NAME    = 2;            // string
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_DESCRIPTION  = 3;            // string
const int     UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_PRIVILEGES   = 4;            // dyn_string
const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_PRIVILEGE_SEPARATOR = ":";

const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING = "EGROUP_MAPPING"; // dyn_dyn_string
const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING_GROUP = 1;          // string
const string  UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING_EGROUP = 2;         // string


// -----------------------------------------------
// 7) Elements of the alert email groups configuration
// -----------------------------------------------
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SYSTEM         = 1;  // string
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_NAME           = 2;  // string
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_MAX_PER_DAY    = 3;  // int
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_HTML_LINK      = 4;  // string
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SENDER_ADDRESS = 5;  // string
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_DOMAIN         = 6;  // string
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SMTP_SERVER    = 7;  // string
const int UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_RECEIVER_LIST  = 8;  // dyn_string


// -----------------------------------------------
// 8) Elements of the autologout config
// -----------------------------------------------

const int UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_WARNING_HOURS    = 1;
const int UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_WARNING_MINUTES  = 2;
const int UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_COMMIT_HOURS     = 3;
const int UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_COMMIT_MINUTES   = 4;


// Elements of a device type
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_DEVICE_TYPE_NAME = 1;
const int UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_DEVICE_TYPE_QUANTITY = 2;



// String to use for an archive name depending on the type analog/boolean/event
dyn_string UN_APPLICATION_CONFIGURATION_APPL_ARCHIVES_LABEL;
const int  UN_APPLICATION_CONFIGURATION_APPL_ARCHIVE_ANALOG = 1;
const int  UN_APPLICATION_CONFIGURATION_APPL_ARCHIVE_BOOLEAN = 2;
const int  UN_APPLICATION_CONFIGURATION_APPL_ARCHIVE_EVENT = 3;

// File where to log the output to
file UN_APPLICATION_CONFIGURATION_LOG_FILE;


//unApplicationConfiguration_configureApplication
/**
  Purpose: 
  Configures a project with the given parameters
  
  @param sConfigurationFilePath: string, input: path to the XML file containing the parameters
  @param dsExceptions: dyn_string, output: list of errors occurring in the function
  @param bDoBackupBefore: bool, input: indicate whether or not do a backup of the project before changing any parameter (default: false)
  
  @return path to the file containing the log of the execution
  
  Usage: Public.  
*/
string unApplicationConfiguration_configureApplication(string sConfigurationFilePath, dyn_string &dsExceptions, bool bDoBackupBefore = false, bool doBackupAfter = false)
{
  // Open log file
  string sLogFilePath = getPath(LOG_REL_PATH) + "/unApplicationConfiguration" + formatTime("_%Y_%m_%d_%H_%M_%S", getCurrentTime()) + ".txt";
  UN_APPLICATION_CONFIGURATION_LOG_FILE = fopen(sLogFilePath, "w");
  
  if (0 == UN_APPLICATION_CONFIGURATION_LOG_FILE)
  {
    fwException_raise(
        dsExceptions,
        "ERROR",
        "Could not open log file. File may be already in use or you don't have write authorization.",
        ""
    );
    
    return "";
  } 
  
  
  // Init global parameter
  UN_APPLICATION_CONFIGURATION_APPL_ARCHIVES_LABEL = makeDynString("ANA", "BOOL", "EVENT");
    
  // Variable containing the entire configuration of the project
  dyn_anytype daProjectConfig;
    
  // Extract data from the XML file
  _unApplicationConfiguration_logMessage("Reading configuration file...");
  _unApplicationConfiguration_readProjectConfig(sConfigurationFilePath, daProjectConfig, dsExceptions);
  
  // Error? Leave configuration.
  if (dynlen(dsExceptions))
  {
    if (UN_APPLICATION_CONFIGURATION_LOG_FILE)
    {
      fclose(UN_APPLICATION_CONFIGURATION_LOG_FILE);
    } 
    
    return "";
  }
  _unApplicationConfiguration_logMessage("Configuration read.");
  
  // Do a backup if needed  
  if (bDoBackupBefore)
  {        
    string sBackupPath;
    dpGet("_DataManager.Backup.InputFile.Device", sBackupPath);
    // Warning, the backup can be not configured yet, thus no path is set. 
    // This is the case when the application is new, so this time we just ignore the backup since there is nothing to lose.
    if ("" != sBackupPath)
    {
      _unApplicationConfiguration_logMessage("Running initial backup.");    
      unBackup_runBackupWait(getSystemName(), dsExceptions);
    
      // If the backup failed, stop
      if (dynlen(dsExceptions) > 0)
      {
        _unApplicationConfiguration_logError("Initial backup failed.\n" + dsExceptions[2]);
        return "";
      }
      
      // Note : this backup will be moved to folder PVSS_backup_old when the second backup is done at the end
    }   
  }  
  
  // Proceed to configuration with extracted data  
   _unApplicationConfiguration_proceed(dsExceptions, daProjectConfig, doBackupAfter);
  
  if (UN_APPLICATION_CONFIGURATION_LOG_FILE)
  {
    fclose(UN_APPLICATION_CONFIGURATION_LOG_FILE);
  }
        
  return sLogFilePath;
}



//_unApplicationConfiguration_proceed
/**
  Purpose:
  Does the configuration.
  
  @param dsExceptions: dyn_string, output: list of errors that occurred during the execution of the function
  @param daProjectConfig: dyn_anytype, input: contains all the parameters to set to the application.
  
  
  Usage: Internal 
*/
void _unApplicationConfiguration_proceed(dyn_string &dsExceptions, dyn_anytype daProjectConfig, bool bDoBackup)
{
  int  iExceptionsLength;
  
  // Check if the project has not already been configured
  // i.e. its name is set
  bool bProjectIsAlreadyConfigured = false;
  
  if(dpExists(UN_APPLICATION_DPNAME)) 
  {
    string sApplicationName;
    dpGet(UN_APPLICATION_DPNAME + ".applicationName", sApplicationName);
    if ((strlen(sApplicationName) > 0) && ("<< Application name not set >>" != sApplicationName))
    {
      _unApplicationConfiguration_logWarning("Warning: UNICOS application " + sApplicationName + " seems to have already been configured.");
      bProjectIsAlreadyConfigured = true;
    }
  }  
  
  // Proceed to configuration
  // Notes : 
  //   - "read data" always refers to the appropriate data that can be read from the configuration file
  
  _unApplicationConfiguration_logMessage("Configurating project...");
  
   // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 0) Access control setup (/!\ Only if project not already configured)
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  if (!bProjectIsAlreadyConfigured)
  {
    _unApplicationConfiguration_logMessage("  0) Configuring access control...");
  
    dyn_mixed dmAcConfiguration;
    dyn_string dsTempExceptions;
    /*
  // --------------------------------------------- 0.1) Change access rights    
  //                           |--------|---------------------|
  //                           | Domain |     Privilege       |
  // |-------------------------|--------|---------------------|
  // | Domain administration   | SYSTEM |    FwAccessCtrl     |
  // | Group administration    | SYSTEM |    FwAccessCtrl     |
  // | User administration     | SYSTEM |    FwAccessCtrl     |    
  // |-------------------------|--------|---------------------|
    
    iExceptionsLength = dynlen(dsExceptions);
    fwAccessControl_getConfiguration(dmAcConfiguration, dsExceptions);  
    if (dynlen(dsExceptions) > iExceptionsLength)
    {
      _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
      _unApplicationConfiguration_logError("Administration setup failed.");
    }
    
    dmAcConfiguration[fwAccessControl_CONFIG_AccessRight_DomainAdmin] = "SYSTEM:FwAccessCtrl";
    dmAcConfiguration[fwAccessControl_CONFIG_AccessRight_GroupAdmin]  = "SYSTEM:FwAccessCtrl";
    dmAcConfiguration[fwAccessControl_CONFIG_AccessRight_UserAdmin]   = "SYSTEM:FwAccessCtrl";

    
    // It appears that if the exception list is not empty, the function fwAccessControl_setConfiguration will fail.
    // So we use a temporary variable (empty) to store the exceptions only for this function call
    // Note: this function seems to be bugged. If an error occured previously, it will fail. 
    // If it's call in a new execution with the same parameters, without errors before, it will succeed.
    // So we ignore the errors. The parameters are hard-coded and can't be wrong.
    fwAccessControl_setConfiguration(dmAcConfiguration, dsTempExceptions); 
    if (dynlen(dsTempExceptions) > 0)
    {
      _unApplicationConfiguration_logError("Access right setup failed... " + dsTempExceptions[2]);
      dynAppend(dsExceptions, dsTempExceptions);
    }
    */
  // --------------------------------------------- 0.2) If LPAP included in config file check "LDAP" authentication, else "Local"
    dyn_string dsAuthFuncParam;
    fwAccessControl_getConfiguration(dmAcConfiguration, dsExceptions);  
    if (mappingHasKey(daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL], UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_LDAP))
    {
      if (daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL][UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_LDAP][UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_LDAP_USE])
      {
        dsAuthFuncParam = 
            makeDynString(
              "_fwAccessControl_LDAPAuthRoutine",
              "DIGEST-MD5",
              "cerndc.cern.ch",
              "",
              false
        );
      }
      else
      {
        dsAuthFuncParam = makeDynString("");
      }
    }
    else
    {
      dsAuthFuncParam = makeDynString("");
    }
  
    dynClear(dsTempExceptions);
    dmAcConfiguration[fwAccessControl_CONFIG_Authentication_Configuration] = dsAuthFuncParam;
    fwAccessControl_setConfiguration(dmAcConfiguration, dsTempExceptions); 
    if (dynlen(dsTempExceptions) > 0)
    {
      _unApplicationConfiguration_logError("LDAP setup failed... " + dsTempExceptions[2]);
      dynAppend(dsExceptions, dsTempExceptions);
    }
    
    
    
    
  
    // --------------------------------------------- 0.3) Access control server : if read data contains central server address set host to the read value
    // Read available servers
    string sServer;
    if (mappingHasKey(daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL], UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_CENTRAL_SERVER))
    {
      sServer = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL][UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_CENTRAL_SERVER][UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_CENTRAL_SERVER_NAME];      
    }
    
    dyn_string dsSystemNames;
    dyn_uint dsSystemIds;
    getSystemNames(dsSystemNames,dsSystemIds);
    
    // Server should be one of the known distributed systems
    if (dynContains(dsSystemNames, sServer) || (sServer == ""))
    {   
      dpSetWait(g_fwAccessControl_ConfigurationDP + ".Integrated.Reply",    "");
      dpSetWait(g_fwAccessControl_ConfigurationDP + ".Integrated.ACServer", (sServer == "")? "" : sServer + ":");
      dpSetWait(g_fwAccessControl_ConfigurationDP + ".Integrated.Request",  "");      
    }
    else
    {
      string sError = getCatStr("unApplicationConfiguration","017-INVALID_CENTRAL_SERVER");
      string sExpectedServers;
      for (int i = 1 ; i <= dynlen(dsSystemNames) ; i++)
      {
        sExpectedServers += dsSystemNames[i];
        if (i < dynlen(dsSystemNames)) 
        {
          sExpectedServers += ", ";
        }
      }
      sExpectedServers += " or none";
      
      strreplace(sError, "SERVER", sServer);
      strreplace(sError, "EXPECTED", sExpectedServers);
      _unApplicationConfiguration_logError(sError);
      fwException_raise(
          dsExceptions,
          "ERROR",
          sError,
          "017-INVALID_CENTRAL_SERVER"
      );      
    }
    
    
    // --------------------------------------------- 0.4) Create new domains
    _unApplicationConfiguration_logMessage("     Creating domains...");   
    dyn_dyn_anytype ddaDomainList = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL][UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_LIST];
    for (int i = 1 ; i <= dynlen(ddaDomainList) ; i++)
    {
      dyn_string dsExceptionsLocal;
      dyn_anytype daDomain = ddaDomainList[i];
      
      fwAccessControl_createDomain(	
          daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_NAME],
          daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_FULL_NAME],
          daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_COMMENT],
          daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_PRIVILEGES],
          dsExceptionsLocal
        );	
        
      if (dynlen(dsExceptionsLocal) > 0)
      {
        dynAppend(dsExceptions, dsExceptionsLocal);
      }
    }
    
    // --------------------------------------------- 0.5) Creating new groups.
    _unApplicationConfiguration_logMessage("     Creating groups...");   
    dyn_dyn_anytype ddaGroups = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL][UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_LIST];
    for (int i = 1 ; i <= dynlen(ddaGroups) ; i++)
    {
      dyn_string dsExceptionsLocal;
      dyn_anytype daGroup = ddaGroups[i];
      
      fwAccessControl_createGroup(	
          daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_NAME],
          daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_FULL_NAME],
          daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_DESCRIPTION],
          daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_PRIVILEGES],
          dsExceptionsLocal
        );	
        
      if (dynlen(dsExceptionsLocal) > 0)
      {
        dynAppend(dsExceptions, dsExceptionsLocal);
      }
    }
    
    
    // --------------------------------------------- 0.6) Map groups and e-groups.
    _unApplicationConfiguration_logMessage("     Mapping to egroups...");   
    if (mappingHasKey(daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL], UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING))
    {
      dyn_dyn_string ddsMappingList = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL][UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING];
      bool bOneGroupSynced = false;
      for (int i = 1 ; i <= dynlen(ddsMappingList) ; i++)
      {
        dyn_string dsExceptionsLocal;
        dyn_string dsMapping = ddsMappingList[i];
            
        fwAccessControl_setEgroupSyncForGroup(
            dsMapping[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING_GROUP],
            dsMapping[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING_EGROUP],
            dsExceptionsLocal
          );
          
        if (dynlen(dsExceptionsLocal) > 0)
        {
          dynAppend(dsExceptions, dsExceptionsLocal);
        }
        else
        {
          bOneGroupSynced = true;
        }
      }
      
      if (bOneGroupSynced)
      {
        int iManagerAddState = unSystemIntegrity_addManager(
            "WCCOActrl",
            "always",
            30,
            2,
            2,
            "fwAccessControl/fwAccessControl_EgroupSync.ctc",
            dsExceptions
          );
        
        if (0 == iManagerAddState)
        {
          string sError = "Failed to add fwAccessControl_EgroupSync.ctc to console";
          _unApplicationConfiguration_logError(sError);
          fwException_raise(
              dsExceptions,
              "ERROR",
              sError,
              "ADD_MANAGER_FAILED"
          );
        }
      }
    }
    
    // --------------------------------------------- 0.7) Change passwords
    _unApplicationConfiguration_logMessage("     Changing passwords...");   
    
    string sUserFullName;
    string sUserDescription;
    string sUserNewPassword;
    int iUserId;
    bool bEnabled;
    dyn_string dsGroupNames;
    
    
    
    
  // --------------------------------------------- 0.7.1) Change admin password to admin_projAppName
    iExceptionsLength = dynlen(dsExceptions);
    sUserNewPassword = "admin_" + daProjectConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_APPLICATION_NAME];
    strreplace(sUserNewPassword, " ", ""); // Remove blank spaces
    fwAccessControl_getUser("admin", sUserFullName, sUserDescription, iUserId, bEnabled, dsGroupNames, dsExceptions);   
    DebugN("CHANGING ADMIN PASSWORD TO " + sUserNewPassword);
    fwAccessControl_updateUser("admin", "admin", sUserFullName, sUserDescription, bEnabled, sUserNewPassword, dsGroupNames, dsExceptions, false, true);
    
    if (dynlen(dsExceptions) > iExceptionsLength)
    {
      _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
      _unApplicationConfiguration_logError("Error changing admin password.");
    }
    
  // --------------------------------------------- 0.7.2) Change expert password to expert_projAppName
    iExceptionsLength = dynlen(dsExceptions);
    sUserNewPassword = "expert_" + daProjectConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_APPLICATION_NAME];
    strreplace(sUserNewPassword, " ", ""); // Remove blank spaces
    fwAccessControl_getUser("expert", sUserFullName, sUserDescription, iUserId, bEnabled, dsGroupNames, dsExceptions);    
    fwAccessControl_updateUser("expert", "expert", sUserFullName, sUserDescription, bEnabled, sUserNewPassword, dsGroupNames, dsExceptions, false, true);
    
    if (dynlen(dsExceptions) > iExceptionsLength)
    {
      _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
      _unApplicationConfiguration_logError("Error changing expert password.");
    }
    
  // --------------------------------------------- 0.7.3) Change operator password to operator_projAppName
    iExceptionsLength = dynlen(dsExceptions);
    sUserNewPassword = "operator_" + daProjectConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_APPLICATION_NAME];
    strreplace(sUserNewPassword, " ", ""); // Remove blank spaces
    fwAccessControl_getUser("operator", sUserFullName, sUserDescription, iUserId, bEnabled, dsGroupNames, dsExceptions);    
    fwAccessControl_updateUser("operator", "operator", sUserFullName, sUserDescription, bEnabled, sUserNewPassword, dsGroupNames, dsExceptions, false, true);
    
    if (dynlen(dsExceptions) > iExceptionsLength)
    {
      _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
      _unApplicationConfiguration_logError("Error changing operator password.");
    }
    
  // --------------------------------------------- 0.7.4) Change root password to hard-coded constant    
    iExceptionsLength = dynlen(dsExceptions);
    sUserNewPassword = "Enice09";
    fwAccessControl_getUser("root", sUserFullName, sUserDescription, iUserId, bEnabled, dsGroupNames, dsExceptions);    
    fwAccessControl_updateUser("root", "root", sUserFullName, sUserDescription, bEnabled, sUserNewPassword, dsGroupNames, dsExceptions, false, true);
    
    if (dynlen(dsExceptions) > iExceptionsLength)
    {
      _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
      _unApplicationConfiguration_logError("Error changing root password.");
    }
    
    
  // --------------------------------------------- 0.7.5) Delete useless users
    // Use temporary exception variable
    // If the users don't exist, ignore the error
    dynClear(dsTempExceptions);
    fwAccessControl_deleteUser("operatorAll", dsTempExceptions);
    fwAccessControl_deleteUser("guest", dsTempExceptions);
    fwAccessControl_deleteUser("gast", dsTempExceptions);
    fwAccessControl_deleteUser("demo", dsTempExceptions);
    
  // --------------------------------------------- 0.7.6) Change config file to userName="monitor" (in [general])
    dyn_string dsGeneralSectionConfig;
    fwInstallation_getSection("general", dsGeneralSectionConfig);
    for (int i = 1 ; i <= dynlen(dsGeneralSectionConfig) ; i++)
    {
      if (strpos(dsGeneralSectionConfig[i], "userName") == 0)
      {
        dsGeneralSectionConfig[i] = "userName = \"monitor\"";
      }
      else if (strpos(dsGeneralSectionConfig[i], "password") == 0)
      {
        dsGeneralSectionConfig[i] = "password = \"\"";
      }
    }    
    fwInstallation_setSection("general", dsGeneralSectionConfig);
    
    
    
    _unApplicationConfiguration_logMessage("    ... done.");
  }
  else
  {    
    _unApplicationConfiguration_logMessage("  0) Access control already configured. Ignoring step.");
  }
   
 
  
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 1) Alarm archive 
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  
  _unApplicationConfiguration_logMessage("  1) Configurating alarm archive...");
 
  // The code here is extracted from the defaut alarm archive configuration panel, which is used as-is by UNICOS
  
  // --------------------------------------------- 1.1) Deactivate
  time t;
  dpSet( 
    "_AlertArchivControl.StatusSystem:_original.._value", 0,
    "_AlertFileChange.validFrom:_original.._value",       setPeriod( t, 0 ),
    "_AlertFileChange.validUntil:_original.._value",      setPeriod( t, 1 ) 
  );
  
  // Copy-past from original panel
  dyn_string ds;
  ds[1] = "daily";
  sprintf( ds[2], "%2d", 4 );
  sprintf( ds[3], "%2d", 0 );
  sprintf( ds[4], "%2d", 1 );
  sprintf( ds[5], "%5d", 1 );
  
  dpSet( "_AlertArchivControl.Media.CycleRefresh12:_original.._value", ds );
  
  if( isLightRedundant() ) 
  {
    infoLine( 136, -1, "" );
    infoLine( 136, -1, "_2" );
  }
  else
  {
    infoLine( 136, -1, "" );
  }
 
  // --------------------------------------------- 1.2) Uncheck "Delete only archived files automatically"
  dpSet("_AlertArchivControl.DeleteOnlyArchiv:_original.._value", false);
  
  // --------------------------------------------- 1.3) Set frequency type do daily and value to 1
  dpSet(
      "_AlertArchivControl.CreationCycleTime.Resolution:_original.._value", 4, // Daily (4th element in the combobox)
      "_AlertArchivControl.CreationCycleTime.Count:_original.._value",      1  // Once a day
  );
  
  // --------------------------------------------- 1.4) Set deletion cycle frequency to read data
  dpSet("_AlertArchivControl.DeleteCycleTime:_original.._value", daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ALARM_CONFIG][UN_APPLICATION_CONFIGURATION_INDEX_ALARM_CONFIG_DELETION_CYCLE]);
  

  int iMinute = 0;
  int iHour = 0;
  
  dpSet( "_AlertFileChange.validFrom:_original.._value",   setPeriod(t,0),
         "_AlertFileChange.validUntil:_original.._value",  setPeriod(t,1));
  dpSet( "_AlertFileChange.validFrom:_original.._value",   setPeriod(t,0),
         "_AlertFileChange.validUntil:_original.._value",  setPeriod(t,0),
         "_AlertFileChange.time:_original.._value",        makeDynInt(),
         "_AlertFileChange.weekDay:_original.._value",     makeDynInt(),
         "_AlertFileChange.monthDay:_original.._value",    makeDynInt(),
         "_AlertFileChange.month:_original.._value",       makeDynInt(),
         "_AlertFileChange.interval:_original.._value",    86400,
         "_AlertFileChange.syncTime:_original.._value",    iMinute * 60 + iHour * 60 * 60,
         "_AlertFileChange.syncWeekDay:_original.._value", 1,
         "_AlertFileChange.syncDay:_original.._value",     1,
         "_AlertFileChange.syncMonth:_original.._value",   1,
         "_AlertFileChange.delay:_original.._value",       0 );
  dpSet( "_AlertArchivControl.StatusSystem:_original.._value", 1 );
      
  // --------------------------------------------- 1.5) Set other DPEs to predefined values
  dpSet(
      "_AlertArchivControl.SyncTime.Hour:_original.._value", iHour,
      "_AlertArchivControl.SyncTime.Minute:_original.._value", iMinute,
      "_AlertArchivControl.SyncTime.Month:_original.._value", 1,
      "_AlertArchivControl.SyncTime.MonthDay:_original.._value", 1 ,
      "_AlertArchivControl.SyncTime.WeekDay:_original.._value", 1 
      
      );
  
  // --------------------------------------------- 1.6) Activate
  dpSet("_AlertArchivControl.StatusSystem:_original.._value", 1);
  
  if(isLightRedundant()) 
  {
    infoLine( 0, -1, ""   );
    infoLine( 0, -1, "_2" );
  }
  else
  {
    infoLine( 0, -1, "" );
  }
  
  // --------------------------------------------- 1.7) File-switch, check new file was created
  
  // If a switch is already in progress, ignore
  bool bSwitchInProgress;
  dpGet("_DataManager.AlertDataSetChange.InProgress", bSwitchInProgress);
  if (!bSwitchInProgress)
  {
    t = getCurrentTime();
    dpSet("_DataManager.AlertDataSetChange.Start:_original.._value", 1);
    dyn_anytype dtDataSetStartTimes;
    bool bTimeOut;
  
    // File list update might take some time, we need to wait. 
    dyn_anytype daConditions = makeDynInt(); 
    dpWaitForValue(
        makeDynString("_AlertDataSet.List.StartTimes:_original.._value"), 
        daConditions,
        makeDynString(), 
        dtDataSetStartTimes, 
        daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS][UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT], 
        bTimeOut);
  
    dpGet("_AlertDataSet.List.StartTimes", dtDataSetStartTimes);
  
    if (bTimeOut) 
    {
      string sError = getCatStr("unApplicationConfiguration","005-FILE_SWITCH_ERROR_ALARM");
      fwException_raise(
          dsExceptions,
          "ERROR",
          sError,
          "005-FILE_SWITCH_ERROR_ALARM"
      );
      _unApplicationConfiguration_logError(sError);
      // Note : we do not stop the execution if the file switch failed. The user can do it quickly manually later
    }
    else
    {
      _unApplicationConfiguration_logMessage("    ... done.");
    }
  }
  else
  {
    string sError = getCatStr("unApplicationConfiguration","005-FILE_SWITCH_ERROR_ALARM");
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "005-FILE_SWITCH_ERROR_ALARM"
    );
    _unApplicationConfiguration_logError(sError);
  }

  
  unImportDevice_initialize();
      
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 2) Value archives
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  
  _unApplicationConfiguration_logMessage("  2) Configurating value archives...");
  
  // --------------------------------------------- 2.1) Rename ValueArchive_0000 to UNICOS  
  // Get available archives list  
  dyn_string dsAllArchiveDps = unGenericUtilities_getArchiveList(getSystemName());
  
  for (int i = 1 ; i <= dynlen(dsAllArchiveDps) ; i++)
  {
    string sArchiveName;
    dpGet(dsAllArchiveDps[i] + ".general.arName",sArchiveName);  
    if (("ValueArchive_0000" == sArchiveName) && !patternMatch("*_2", dsAllArchiveDps[i]))
    {
      dpSet(dsAllArchiveDps[i] + ".general.arName","UNICOS"); 
    }
  }
  
  // --------------------------------------------- 2.2) Edit UNICOS archive  
  bool bUnicosArchiveFound = false;
  for (int i = 1 ; i <= dynlen(dsAllArchiveDps) ; i++)
  {
    string sArchiveName;
    dpGet(dsAllArchiveDps[i] + ".general.arName",sArchiveName);  
    if (sArchiveName == "UNICOS")
    {
      bUnicosArchiveFound = true;
      
  // ------------------------------------------------ 2.2.1) Activate compression    
    
      int iOldExceptionLen = dynlen(dsExceptions);
      unGenericUtilities_activateArchiveCompression(dsAllArchiveDps[i], true, dsExceptions);
            
  // ------------------------------------------------ 2.2.2) Start manager
      
      int iUnicosArchiveManagerNumber;
      dpGet(dsAllArchiveDps[i] + ".arNr", iUnicosArchiveManagerNumber);
      
     
      int iIndex;
      unSystemIntergity_manageManager("WCCOAvalarch", "-num " + iUnicosArchiveManagerNumber, "", iIndex, dsExceptions, getSystemName());
    
      // Read state
      int iState;
      dyn_string dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, getSystemName());
    
      // If stopped : start.
      if (0 == dsResults[1])
      {
        // Start it
        bool bTimerExpired = false;
        unGenericUtilities_startManagerWait("WCCOAvalarch", "-num " + iUnicosArchiveManagerNumber, dsExceptions, getSystemName(), 30, bTimerExpired);
           
        if (bTimerExpired)
        {
          string sError = getCatStr("unApplicationConfiguration","010-ARCHIVE_MANAGER_START_FAILED");
          strreplace(sError, "MANAGER", iUnicosArchiveManagerNumber);
          _unApplicationConfiguration_logError(sError);
          fwException_raise(
              dsExceptions,
              "ERROR",
              sError,
              "010-ARCHIVE_MANAGER_START_FAILED"
          );
        
          // Do not stop configuration
        }
        else
        {
          unGenericUtilities_setManagerAlways("WCCOAvalarch",  "-num " + iUnicosArchiveManagerNumber, dsExceptions, getSystemName());
          _unApplicationConfiguration_logMessage("        UNICOS archive manager started...");
        }   
      }
      else // Else set it to always start (doesn't matter if it's already the case
      {
        unGenericUtilities_setManagerAlways("WCCOAvalarch",  "-num " + iUnicosArchiveManagerNumber, dsExceptions, getSystemName());
        _unApplicationConfiguration_logMessage("        UNICOS archive manager started...");
      }  

      
  // ------------------------------------------------ 2.2.3) File-switch, check new file was created 
      
      
      iOldExceptionLen = dynlen(dsExceptions);
      unImportDevice_valarchFileSwitch(dsAllArchiveDps[i], dsExceptions, daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS][UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT]);
      if (dynlen(dsExceptions) > iOldExceptionLen)
      {
        _unApplicationConfiguration_logError(dsExceptions[iOldExceptionLen + 2]);
      }    
    }
  }
  // UNICOS archive was not found in the archive list
  if (!bUnicosArchiveFound)
  {
    string sError = getCatStr("unApplicationConfiguration","006-UNICOS_ARCHIVE_NOT_FOUND");
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "006-UNICOS_ARCHIVE_NOT_FOUND"
    );
    _unApplicationConfiguration_logError(sError);
    return;
  }
  
  
  
  
  
  
  // Go through all the archive DP, if one has state 3 we remove the dp and its redundancy
  for (int i = 1 ; i <= dynlen(dsAllArchiveDps) ; i++)
  {
    int iState;
    // Warning : some DP will be deleted during the loop so always check it still exists first
    if (dpExists(dsAllArchiveDps[i]))
    {
      dpGet(dsAllArchiveDps[i] + ".state",iState); 
      if (iState == 3)
      {
        _unApplicationConfiguration_logMessage("Deleting unused archive " + dsAllArchiveDps[i]);
        dpDelete(dsAllArchiveDps[i]);
        if (dpExists(dsAllArchiveDps[i] + "_2"))
          dpDelete(dsAllArchiveDps[i] + "_2"); // Redundancy
      }
    }
  }
  
  // Update values (some DP may have been removed)
  dsAllArchiveDps = unGenericUtilities_getArchiveList(getSystemName());
    
  
  // --------------------------------------------- 2.3) For each front-end application, start driver   
  
  for (int i = 1 ; i <= dynlen(daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST]) ; i++)
  {
    string sFrontEndAppName = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_NAME];
    _unApplicationConfiguration_logMessage("        Configuring front-end application '" + sFrontEndAppName + "'");
 
    
    string sDriverType = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_NAME];
    string sDriverManager;   
    int iManagerNumber;
    if (sDriverType == "MODBUS")
    {
      sDriverManager = "WCCOAmod";
      iManagerNumber = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_DRIVER_NUM];
      int iModbusPort = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_DATA_SERVER_PORT];
      string iModbusServerAddress = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_FRONT_END_IP_ADDRESS];
      _unApplicationConfiguration_logMessage("        Adding MODBUS driver #" + iManagerNumber + " ("+ iModbusServerAddress + ") on port " + iModbusPort );      
      _unApplicationConfiguration_logWarning("Modbus redirection required. If you need assistance, please contact icecontrols.support@cern.ch and refer to this message");
      
      // Add port info to config file if it is not the default port (502)
      // [mod_X] (X = Manager number)
      // tcpServerPort=Y (Y = port number)
      if (502 != iModbusPort)
      {
        dyn_string dsModSectionConfig;
        fwInstallation_getSection("mod_" + iManagerNumber, dsModSectionConfig);
        bool bSectionFound = false;
        for (i = 1 ; i <= dynlen(dsModSectionConfig) ; i++)
        {
          if (strpos(dsModSectionConfig[i], "tcpServerPort") == 0)
          {
            dsModSectionConfig[i] = "tcpServerPort = " + iModbusPort;
          }          
        }
        if (!bSectionFound)
        {
          dsModSectionConfig = makeDynString("tcpServerPort = " + iModbusPort);
        }
    
        fwInstallation_setSection("mod_" + iManagerNumber, dsModSectionConfig);
        
      }  
      else // Else remove this section from the configuration file
      {
        fwInstallation_clearSection("mod_" + iManagerNumber);
      }      
      
      
      // Add sim driver
      int iManagerAddState = unSystemIntegrity_addManager(
          "WCCILsim",
          "manual",
          30,
          2,
          2,
          "-num " + iManagerNumber,
          dsExceptions
      );
        
      if (0 == iManagerAddState)
      {
        string sError = getCatStr("unApplicationConfiguration","011-DRIVER_MANAGER_ADD_FAILED");
        strreplace(sError, "MANAGER", iManagerNumber);
        strreplace(sError, "TYPE", "SIM");
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "011-DRIVER_MANAGER_ADD_FAILED"
        );
      }
    }
    else
    {
      sDriverManager = "WCCOAs7";
      iManagerNumber = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE][UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_S7_DRIVER_NUM];
      _unApplicationConfiguration_logMessage("        Adding S7 driver #" + iManagerNumber);
    } 
  
      
    int iPort;
    string sHost;
    string sIP;
    string sSystem = getSystemName();
    unGenericDpFunctions_getHostName(sSystem, sHost, sIP);
    unGenericDpFunctions_getpmonPort(sSystem, iPort);
    
    int iManagerAddState = unSystemIntegrity_addManager(
        sDriverManager,
        "manual",
        30,
        2,
        2,
        "-num " + iManagerNumber,
        dsExceptions
    );
      
    if (0 == iManagerAddState)
    {
      string sError = getCatStr("unApplicationConfiguration","011-DRIVER_MANAGER_ADD_FAILED");
      strreplace(sError, "MANAGER", iManagerNumber);
      strreplace(sError, "TYPE", sDriverType);
      _unApplicationConfiguration_logError(sError);
      fwException_raise(
          dsExceptions,
          "ERROR",
          sError,
          "011-DRIVER_MANAGER_ADD_FAILED"
      );
    }
      
  // --------------------------------------------- end for
  }
  
  
  // --------------------------------------------- 2.4) For each archive set, Create or update archive ARxx_AppName_ANA/BOOL/EVENT
  for (int i = 1 ; i <= dynlen(daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST]) ; i++)
  {
    string sArchiveName = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NAME];
    _unApplicationConfiguration_logMessage("        Configuring archive set '" + sArchiveName + "'");
    
    
  
  // ------------------------------------------------ 2.4.1) Create or update archive ARxx_AppName_ANA/BOOL/EVENT
    int iFormerLen = dynlen(dsExceptions);
    _unApplicationConfiguration_createFrontEndApplArchive(
        UN_APPLICATION_CONFIGURATION_APPL_ARCHIVE_ANALOG, 
        sArchiveName, 
        daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_ANA], 
        dsExceptions,
        daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS][UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT]
    );
    _unApplicationConfiguration_createFrontEndApplArchive(
        UN_APPLICATION_CONFIGURATION_APPL_ARCHIVE_BOOLEAN, 
        sArchiveName, 
        daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_BOOL], 
        dsExceptions,
        daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS][UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT]
    );
    _unApplicationConfiguration_createFrontEndApplArchive(
        UN_APPLICATION_CONFIGURATION_APPL_ARCHIVE_EVENT, 
        sArchiveName, 
        daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_EVENT], 
        dsExceptions,
        daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS][UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT]
    );
  }
  
  
  _unApplicationConfiguration_logMessage("    ... done.");

  
  
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 3) Unicos scripts
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  _unApplicationConfiguration_logMessage("  3) Starting UNICOS script...");
  
  
  // --------------------------------------------- 3.1) Set unicos_scripts.lst to always start, check it is started
  iExceptionsLength = dynlen(dsExceptions);
  _unApplicationConfiguration_logMessage("        Starting \"WCCOActrl -f unicos_scripts.lst\"...");
  // Check it exists
    
  // Get manager index
  int iIndex;
  unSystemIntergity_manageManager("WCCOActrl", "-f unicos_scripts.lst", "", iIndex, dsExceptions, getSystemName());
    
  // Read state
  int iState;
  dyn_string dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, getSystemName());
  
  if(dynlen(dsResults) != 5)
  {    
    // Error
    string sError = getCatStr("unApplicationConfiguration","013-MANAGER_NOT_FOUND");
    strreplace(sError, "MANAGER", "WCCOActrl -f unicos_scripts.lst");
    _unApplicationConfiguration_logError(sError);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "013-MANAGER_NOT_FOUND"
    );
    
    return;   
  }         
  else
  {
    if (dsResults[1] != 2) // Not started
    {
      bool bTimedOut = false;
      unGenericUtilities_startManagerWait("WCCOActrl", "-f unicos_scripts.lst", dsExceptions, getSystemName(), 30, bTimedOut);
      if (bTimedOut)
      { 
        // Error
        string sError = getCatStr("unApplicationConfiguration","014-MANAGER_NOT_STARTED");
        strreplace(sError, "MANAGER", "WCCOActrl -f unicos_scripts.lst");
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "014-MANAGER_NOT_STARTED"
        );
      }  
    }
  }   
    
  
  
  // Why set to always after starting ? The first function checks the manager is started (waits after starting command), which is what we need
  // So once we are sure it's started we set it to always
  unGenericUtilities_setManagerAlways("WCCOActrl", "-f unicos_scripts.lst", dsExceptions, getSystemName());
  
  if (dynlen(dsExceptions) > iExceptionsLength)
  {
    _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
    _unApplicationConfiguration_logError("        Manager did not start.");
  }
  else
  {
    _unApplicationConfiguration_logMessage("        Started.");
  }
  
  
  // --------------------------------------------- 3.2) Set unDeviceListUpdate to always start, check it is started  
  iExceptionsLength = dynlen(dsExceptions);
  _unApplicationConfiguration_logMessage("        Starting \"WCCOActrl unDeviceListUpdate.ctl\"...");
  // Check it exists    
  // Get manager index
  unSystemIntergity_manageManager("WCCOActrl", "unDeviceListUpdate.ctl", "", iIndex, dsExceptions, getSystemName());
    
  // Read state
  dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, getSystemName());
  
  if(dynlen(dsResults) != 5)
  {    
    // Error
    string sError = getCatStr("unApplicationConfiguration","013-MANAGER_NOT_FOUND");
    strreplace(sError, "MANAGER", "WCCOActrl unDeviceListUpdate.ctl");
    _unApplicationConfiguration_logError(sError);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "013-MANAGER_NOT_FOUND"
    );
    
    return;   
  }        
  else
  {
    if (dsResults[1] != 2) // Not started
    {
      bool bTimedOut = false;
      unGenericUtilities_startManagerWait("WCCOActrl", "unDeviceListUpdate.ctl", dsExceptions, getSystemName(), 30, bTimedOut);
      if (bTimedOut)
      { 
        // Error
        string sError = getCatStr("unApplicationConfiguration","014-MANAGER_NOT_STARTED");
        strreplace(sError, "MANAGER", "WCCOActrl unDeviceListUpdate.ctl");
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "014-MANAGER_NOT_STARTED"
        );
      }  
    }
  }
    
  
  
  unGenericUtilities_setManagerAlways("WCCOActrl", "unDeviceListUpdate.ctl", dsExceptions, getSystemName());
  if (dynlen(dsExceptions) > iExceptionsLength)
  {
    _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
    _unApplicationConfiguration_logError("        Manager did not start.");
  }
  else
  {
    _unApplicationConfiguration_logMessage("        Started.");
  }
  
  
  // --------------------------------------------- 3.3) Set unBackup.ctl to always start, check it is started
  iExceptionsLength = dynlen(dsExceptions);
  _unApplicationConfiguration_logMessage("        Starting \"WCCOActrl unBackup.ctl\"...");


  // Check it exists    
  // Get manager index
  unSystemIntergity_manageManager("WCCOActrl", "unBackup.ctl", "", iIndex, dsExceptions, getSystemName());
    
  // Read state
  dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, getSystemName());
  
  if(dynlen(dsResults) != 5)
  {    
    // Error
    string sError = getCatStr("unApplicationConfiguration","013-MANAGER_NOT_FOUND");
    strreplace(sError, "MANAGER", "WCCOActrl unBackup.ctl");
    _unApplicationConfiguration_logError(sError);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "013-MANAGER_NOT_FOUND"
    );
    
    return;   
  }    
  else
  {
    if (dsResults[1] != 2) // Not started
    {
      bool bTimedOut = false;
      unGenericUtilities_startManagerWait("WCCOActrl", "unBackup.ctl", dsExceptions, getSystemName(), 30, bTimedOut);
      if (bTimedOut)
      { 
        // Error
        string sError = getCatStr("unApplicationConfiguration","014-MANAGER_NOT_STARTED");
        strreplace(sError, "MANAGER", "WCCOActrl unBackup.ctl");
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "014-MANAGER_NOT_STARTED"
        );
      } 
    }
  }
    
   


  if (dynlen(dsExceptions) > iExceptionsLength)
  {
    _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
    _unApplicationConfiguration_logError("        Manager did not start.");
  }
  else
  {
    _unApplicationConfiguration_logMessage("        Started.");
  }

  _unApplicationConfiguration_logMessage("    ... done.");
  
  
  
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 4) System integrity
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  _unApplicationConfiguration_logMessage("  4) Configuring system integrity...");
  
  // --------------------------------------------- 4.1) Add PVSSDB, check it is added
  _unApplicationConfiguration_logMessage("        Adding PVSSDB...");
	dpSet(
      "PVSSDB" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.command", 1, // 1 = Add
      "PVSSDB" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.parameters", makeDynString(UN_SYSTEM_INTEGRITY_PVSSDB));
  
  // Send update to UI in case it is open
  dpSet("PVSSDB" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.command", UN_SYSTEMINTEGRITY_DIAGNOSTIC);
  
  // Leave enough time for the command to spread
  delay(1); 
  // Check
  dyn_string dsPvssDbSystemIntegrity;
  dpGet("PVSSDB" + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.result", dsPvssDbSystemIntegrity);

  if ((dynlen(dsPvssDbSystemIntegrity) == 1) && dpExists(dsPvssDbSystemIntegrity[1]))
  {
    _unApplicationConfiguration_logMessage("        Added.");
  }
  else
  {
    // Error
    string sError = getCatStr("unApplicationConfiguration","015-SYSTEM_INTEGRITY_ADD_FAILED");
    strreplace(sError, "COMPONENT", "PVSSDB");
    _unApplicationConfiguration_logError(sError);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "015-SYSTEM_INTEGRITY_ADD_FAILED"
    );
  }
  
  
  // --------------------------------------------- 4.2) Edit "archive"
  _unApplicationConfiguration_logMessage("        Setting archive parameters...");
  // ------------------------------------------------ 4.2.1) Set fileswitch to 600
  int iFileSwitchDelay = 600;
  // ------------------------------------------------ 4.2.2) Set alarm file size to 300
  int iAlarmFileSizeDelay = 300;
  // ------------------------------------------------ 4.2.3) Set max missing update to 2
  int iMaxMissingUpdate = 2;
  // ------------------------------------------------ 4.2.4) Set update stats max retry to 2
  int iUpdateStatsMaxRetry = 2;
  // ------------------------------------------------ 4.2.5) Set update stats max delay to 5
  int iUpdateStatsMaxDelay = 5;
    
	dpSet("archive" + UN_SYSTEMINTEGRITY_EXTENSION+".config.data", makeDynString(iFileSwitchDelay, iAlarmFileSizeDelay, iMaxMissingUpdate, iUpdateStatsMaxRetry, iUpdateStatsMaxDelay)); 
  
  _unApplicationConfiguration_logMessage("        Paremeters set.");
  
  // --------------------------------------------- 4.3) Add "Alarm" 
  _unApplicationConfiguration_logMessage("        Adding Alarm...");
  //  UN_SYSTEMINTEGRITY_ALARM = "Alarm"
  string sSelectedArchive = UN_SYSTEMINTEGRITY_ALARM;
  string sDpToAdd = UN_SYSTEMINTEGRITY_ALARM;
  
  dpSet(
      "archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.command", 1, // 1 = add
				"archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.parameters", makeDynString(sDpToAdd)
  );
		
  // Send update to UI in case it is open
  dpSet("archive" + UN_SYSTEMINTEGRITY_EXTENSION+".interface.command", UN_SYSTEMINTEGRITY_DIAGNOSTIC);
  // Leave enough time for the command to spread
  delay(1); 
  // Check
  dyn_string dsAlarmSystemIntegrity;
  dpGet("archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.result", dsAlarmSystemIntegrity);
  
  if (dynContains(dsAlarmSystemIntegrity, c_unSystemAlarm_dpPattern + "archive_" + UN_SYSTEMINTEGRITY_ALARM) && dpExists(c_unSystemAlarm_dpPattern + "archive_" + UN_SYSTEMINTEGRITY_ALARM))
  {
    _unApplicationConfiguration_logMessage("        Alarm added.");
  }
  else
  {
    // Error
    string sError = getCatStr("unApplicationConfiguration","015-SYSTEM_INTEGRITY_ADD_FAILED");
    strreplace(sError, "COMPONENT", UN_SYSTEMINTEGRITY_ALARM);
    _unApplicationConfiguration_logError(sError);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "015-SYSTEM_INTEGRITY_ADD_FAILED"
    );
  }
  
  
  
  // --------------------------------------------- 4.4) Add every archive created in steps 2.3/2.4 (in "archive"), check they are added
  _unApplicationConfiguration_logMessage("        Adding archives...");
  
  for (int i = 1 ; i <= dynlen(daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST]) ; i++)
  {
    string sArchiveAnalogDp = "_ValueArchive_" + daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_ANA][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM];
    string sArchiveBoolDp =   "_ValueArchive_" + daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_BOOL][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM];
    string sArchiveEventDp =  "_ValueArchive_" + daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST][i][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_EVENT][UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM];
         
    dyn_string dsArchiveDps = makeDynString(sArchiveAnalogDp, sArchiveBoolDp, sArchiveEventDp);
    
    for (int j = 1 ; j <= dynlen(dsArchiveDps) ; j++)
    {
      sDpToAdd = dsArchiveDps[j];
      _unApplicationConfiguration_logMessage("          Adding archive " + sDpToAdd + "...");
  
      dpSet(
          "archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.command", 1, // 1 = add
    				"archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.parameters", makeDynString(sDpToAdd)
      );
		
      // Send update to UI in case it is open
      dpSet("archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.command", UN_SYSTEMINTEGRITY_DIAGNOSTIC);
      // Leave enough time for the command to spread
      delay(1); 
      // Check
      dpGet("archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.result", dsAlarmSystemIntegrity);
  
      if (dynContains(dsAlarmSystemIntegrity, c_unSystemAlarm_dpPattern + "archive_" + sDpToAdd) && dpExists(c_unSystemAlarm_dpPattern + "archive_" + sDpToAdd))
      {
        _unApplicationConfiguration_logMessage("          Archive added.");
      }
      else
      {
        // Error
        string sError = getCatStr("unApplicationConfiguration","015-SYSTEM_INTEGRITY_ADD_FAILED");
        strreplace(sError, "COMPONENT", sDpToAdd);
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "015-SYSTEM_INTEGRITY_ADD_FAILED"
        );
      }
    } 
  }
  
  // Add archive UNICOS   
  _unApplicationConfiguration_logMessage("          Adding archive UNICOS...");
  dpSet(
      "archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.command", 1, // 1 = add
				"archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.parameters", makeDynString("_ValueArchive_0")
  ); 
  
  // Send update to UI in case it is open
  dpSet("archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".interface.command", UN_SYSTEMINTEGRITY_DIAGNOSTIC);
  // Leave enough time for the command to spread
  delay(1); 
  // Check
  dpGet("archive" + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.result", dsAlarmSystemIntegrity);
  
  if (dynContains(dsAlarmSystemIntegrity, c_unSystemAlarm_dpPattern + "archive_" + "_ValueArchive_0") && dpExists(c_unSystemAlarm_dpPattern + "archive_" + "_ValueArchive_0"))
  {
    _unApplicationConfiguration_logMessage("          Archive added.");
  }
  else
  {
    // Error
    string sError = getCatStr("unApplicationConfiguration","015-SYSTEM_INTEGRITY_ADD_FAILED");
    strreplace(sError, "COMPONENT", "_ValueArchive_0");
    _unApplicationConfiguration_logError(sError);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "015-SYSTEM_INTEGRITY_ADD_FAILED"
    );
  }
      
      
  _unApplicationConfiguration_logMessage("        All archives added.");
  _unApplicationConfiguration_logMessage("    ... done.");
  
  
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 5) Application configuration
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  
  _unApplicationConfiguration_logMessage("  5) Configuring application parameters...");
  // --------------------------------------------- 5.1) Set application name to read data (/!\ Only if it is not already set)
  if (!bProjectIsAlreadyConfigured)
  {
    dpSet(UN_APPLICATION_DPNAME+".applicationName", daProjectConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_APPLICATION_NAME]);
  }
  
  // --------------------------------------------- 5.2) Set default panel
  string sDefaultPanel = daProjectConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_DEFAULT_PANEL];
  if (sDefaultPanel != "")
  {
    // If full path, narrow it down to relative path
    if (isfile(sDefaultPanel))
    {
      strreplace(sDefaultPanel, "\\", "/"); // Convert from windows to linux format
      
      sDefaultPanel = substr(sDefaultPanel, strpos(sDefaultPanel, "/panels/") + strlen("/panels"));
    }
    
    if(getPath(PANELS_REL_PATH, sDefaultPanel) != "") 
    {
      // Get the panelDp of the panel, take the text before the .pnl as panelDpName without all the / 
      string sPanelDp = unPanel_convertPanelFileNameToDp(sDefaultPanel);
      
      // get the panelDp, creates it if it does not exist and show it.
      if(!dpExists(sPanelDp)) 
      {
        unPanel_create(sDefaultPanel, sPanelDp, dsExceptions);
      }
      
      dpSet(UN_APPLICATION_DPNAME + ".defaultPanel", sDefaultPanel);
    }
    else 
    {
      // if the file is not existing, send on info message
      fwException_raise(
          dsExceptions, 
          "INFO", 
          "Default panel " + sDefaultPanel + " doesn't exist",
          ""
        );
    }
  }
  
  // --------------------------------------------- 5.3) Add "archive" (all) and "PVSSDB" to alarm summary 
  dyn_string dsDpAlarmSummaryList = dpNames(c_unSystemAlarm_dpPattern + archive_pattern + "*" , c_unSystemAlarm_dpType);
  
  dynAppend(dsDpAlarmSummaryList, dpNames(c_unSystemAlarm_dpPattern + PVSSDB_pattern + "*", c_unSystemAlarm_dpType));
  
  for (int i = 1 ; i <= dynlen(dsDpAlarmSummaryList) ; i++)
  {
    dsDpAlarmSummaryList[i] = dsDpAlarmSummaryList[i] + ".alarm";
  }

  // Warning : do not remove what has previously been added
  dyn_string dsOldAlarmList;
  dpGet(UN_APPLICATION_DPNAME+".applicationName:_alert_hdl.._dp_list", dsOldAlarmList);
  dynAppend(dsDpAlarmSummaryList, dsOldAlarmList);
  dynUnique(dsDpAlarmSummaryList);
      
  int iAlertType;
  dpGet(UN_APPLICATION_DPNAME+".applicationName:_alert_hdl.._type", iAlertType);
  string sAlertPanel;
  string sAlertPanelParameter;
  string sHelpPanel;
  
  if(iAlertType == DPCONFIG_SUM_ALERT)
  {
    bool bActive;
    dpGet(UN_APPLICATION_DPNAME+".applicationName:_alert_hdl.._active", bActive);
    if(bActive) // Deactivate for configuration
    {
      dpSetWait(UN_APPLICATION_DPNAME+".applicationName:_alert_hdl.._active", false);
    }
    dyn_string dsAlertText;
  
    dpGet(
        UN_APPLICATION_DPNAME + ".applicationName:_alert_hdl.._text0",       dsAlertText[1],
        UN_APPLICATION_DPNAME + ".applicationName:_alert_hdl.._text1",       dsAlertText[2],
        UN_APPLICATION_DPNAME + ".applicationName:_alert_hdl.._panel",       sAlertPanel,
        UN_APPLICATION_DPNAME + ".applicationName:_alert_hdl.._panel_param", sAlertPanelParameter,
        UN_APPLICATION_DPNAME + ".applicationName:_alert_hdl.._help",        sHelpPanel
    );
    
    // WARNING : this function clears the exceptionInfo parameter 
    dyn_string dsTempExceptions;
    fwAlertConfig_modifySummary(
        UN_APPLICATION_DPNAME + ".applicationName", 
        dsAlertText, 
        dsDpAlarmSummaryList, 
        sAlertPanel, 
        sAlertPanelParameter, 
        sHelpPanel, 
        dsTempExceptions
    );
    dynAppend(dsExceptions, dsTempExceptions);
    
    if(bActive) // Reactivate
    {
      dpSetWait(UN_APPLICATION_DPNAME + ".applicationName:_alert_hdl.._active", true);
    }
  }
  else
  {
    // WARNING : this function clears the exceptionInfo parameter 
    dyn_string dsTempExceptions;
    fwAlertConfig_createSummary(
        UN_APPLICATION_DPNAME + ".applicationName", 
        makeDynString("", ""), 
        dsDpAlarmSummaryList, 
        sAlertPanel, 
        makeDynString(), 
        sHelpPanel, 
        dsExceptions
    );
    dynAppend(dsExceptions, dsTempExceptions);
  }
 
  
  // --------------------------------------------- 5.4) Set distributed port number
  
  
  iExceptionsLength = dynlen(dsExceptions);
  int iPortNumber = daProjectConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_DISTRIBUTED_PORT];
  string sSystemName = getSystemName();
  // Remove the ":" at the end of the system
  sSystemName = substr(sSystemName, 0, strpos(sSystemName, ":")); 
  int iSystemId = getSystemId();
  
  dyn_string dsSystemNames;
  dyn_int diSystemIds;
  dyn_string dsHostNames;
  dyn_int diPortNumbers;
  // Get distributed systems already configured  
  unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);

  // Local system should always be in the list  
  string sLocalHostName = dsHostNames[dynContains(dsSystemNames,sSystemName)];  
  unDistributedControl_setDeviceConfig(sSystemName, iSystemId, sLocalHostName, iPortNumber, dsExceptions);
  
  if (dynlen(dsExceptions) > iExceptionsLength)
  {
    _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
  }
  
  // --------------------------------------------- 5.5) Auto logout
  if (dynlen(daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT]) > 0)
  {
    bool bOk;
    int iWarningHours = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_WARNING_HOURS];
    int iWarningMinutes = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_WARNING_MINUTES];
    int iCommitHours = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_COMMIT_HOURS];
    int iCommitMinutes = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_COMMIT_MINUTES];
    int iAction = 1;
    int iWarningType;
    int iCommitType;
    
    
    dyn_string dsUi = dpNames("*", "_Ui");
    
    
    
    for (int i = 1; i <= dynlen(dsUi); i++ )
    {
      dpGet(
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._type",  iWarningType,
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._type",   iCommitType
        );


      // Generating alert classes
      if (!dpExists("_inactWarningAlertClass") )
      {
        dpCreate("_inactWarningAlertClass", "_AlertClass");
        dpSetWait("_inactWarningAlertClass.:_alert_class.._type", DPCONFIG_ALERT_CLASS);
        dpSetWait(
            "_inactWarningAlertClass.:_alert_class.._prior",          "40",  
            "_inactWarningAlertClass.:_alert_class.._abbr",           "W",  
            "_inactWarningAlertClass.:_alert_class.._archive_time",   0,  
            "_inactWarningAlertClass.:_alert_class.._perm",           0,  
            "_inactWarningAlertClass.:_alert_class.._delete",         true,  
            "_inactWarningAlertClass.:_alert_class.._archive",        false,  
            "_inactWarningAlertClass.:_alert_class.._ack_type",       DPATTR_ACK_APP,  
            "_inactWarningAlertClass.:_alert_class.._single_ack",     false,  
            "_inactWarningAlertClass.:_alert_class.._inact_ack",      false,  
            "_inactWarningAlertClass.:_alert_class.._color_none",     "",  
            "_inactWarningAlertClass.:_alert_class.._color_c_nack",   "warningCamUna",  
            "_inactWarningAlertClass.:_alert_class.._color_c_ack",    "",  
            "_inactWarningAlertClass.:_alert_class.._color_g_nack",   "",  
            "_inactWarningAlertClass.:_alert_class.._color_c_g_nack", "",  
            "_inactWarningAlertClass.:_alert_class.._ctrl_none",      "",  
            "_inactWarningAlertClass.:_alert_class.._ctrl_c_nack",    "",  
            "_inactWarningAlertClass.:_alert_class.._ctrl_c_ack",     "",  
            "_inactWarningAlertClass.:_alert_class.._ctrl_g_nack",    "",  
            "_inactWarningAlertClass.:_alert_class.._ctrl_c_g_nack",  "",  
            "_inactWarningAlertClass.:_alert_class.._arg_list",       makeDynString()
          );
      }
      if ( !dpExists("_inactCommitAlertClass") )
      {
        dpCreate("_inactCommitAlertClass", "_AlertClass");
        dpSetWait("_inactCommitAlertClass.:_alert_class.._type",DPCONFIG_ALERT_CLASS);
        dpSetWait(
            "_inactCommitAlertClass.:_alert_class.._prior",           "60",  
            "_inactCommitAlertClass.:_alert_class.._abbr",            "W",  
            "_inactCommitAlertClass.:_alert_class.._archive_time",    0,  
            "_inactCommitAlertClass.:_alert_class.._perm",            0,  
            "_inactCommitAlertClass.:_alert_class.._delete",          true,  
            "_inactCommitAlertClass.:_alert_class.._archive",         false,  
            "_inactCommitAlertClass.:_alert_class.._ack_type",        DPATTR_ACK_APP,  
            "_inactCommitAlertClass.:_alert_class.._single_ack",      false,  
            "_inactCommitAlertClass.:_alert_class.._inact_ack",       false,  
            "_inactCommitAlertClass.:_alert_class.._color_none",      "",  
            "_inactCommitAlertClass.:_alert_class.._color_c_nack",    "alertCamUna",  
            "_inactCommitAlertClass.:_alert_class.._color_c_ack",     "",  
            "_inactCommitAlertClass.:_alert_class.._color_g_nack",    "",  
            "_inactCommitAlertClass.:_alert_class.._color_c_g_nack",  "",  
            "_inactCommitAlertClass.:_alert_class.._ctrl_none",       "",  
            "_inactCommitAlertClass.:_alert_class.._ctrl_c_nack",     "",  
            "_inactCommitAlertClass.:_alert_class.._ctrl_c_ack",      "",  
            "_inactCommitAlertClass.:_alert_class.._ctrl_g_nack",     "",  
            "_inactCommitAlertClass.:_alert_class.._ctrl_c_g_nack",   "",  
            "_inactCommitAlertClass.:_alert_class.._arg_list",        makeDynString()
          );
      }

      // generating/deactivating alert handlings
      if (iWarningType == DPCONFIG_NONE)
      {
        dpSetWait(dsUi[i] + ".Inactivity.Warning:_alert_hdl.._type",  DPCONFIG_ALERT_BINARYSIGNAL);
      }
      else
      {
        dpDeactivateAlert(dsUi[i] + ".Inactivity.Warning", bOk);
      }

      if (iCommitType == DPCONFIG_NONE)
      {
        dpSetWait(dsUi[i] + ".Inactivity.Commit:_alert_hdl.._type", DPCONFIG_ALERT_BINARYSIGNAL);
      }
      else
      {
        dpDeactivateAlert(dsUi[i] + ".Inactivity.Commit", bOk);
      }

      // setting alert handlings
      dpSet(
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._text1",       "!!!",
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._text0",       "",
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._class",       "_inactWarningAlertClass.",
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._panel",       "",
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._help",        "",
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._panel_param", makeDynString(),
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._ok_range",    0,
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._orig_hdl",    1,
          dsUi[i] + ".Inactivity.Warning:_alert_hdl.._min_prio",    0,
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._text1",        "!!!",
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._text0",        "",
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._class",        "_inactCommitAlertClass.",
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._panel",        "",
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._help",         "",
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._panel_param",  makeDynString(),
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._ok_range",     0,
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._orig_hdl",     1,
          dsUi[i] + ".Inactivity.Commit:_alert_hdl.._min_prio",     0
        );  

      // activating alert handlings
      dpSet(dsUi[i] + ".Inactivity.Warning:_alert_hdl.._active",  true);
      dpSet(dsUi[i] + ".Inactivity.Commit:_alert_hdl.._active",   true);

      // setting inactivity parameters
      dpSet(
          dsUi[i] + ".Inactivity.WarningTimeout:_original.._value", iWarningHours * 3600 + iWarningMinutes * 60,
          dsUi[i] + ".Inactivity.CommitTimeout:_original.._value",  iCommitHours * 3600 + iCommitMinutes * 60,
          dsUi[i] + ".Inactivity.Action:_original.._value",         iAction
        );
    }
  }
  
  
  
  
  
  _unApplicationConfiguration_logMessage("    ... done.");
  
  
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 6) Online backup (/!\ Only if project not already configured)
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  
  if (!bProjectIsAlreadyConfigured)
  {
    _unApplicationConfiguration_logMessage("  6) Configuring online backup...");
  
  // --------------------------------------------- 6.1) Set path
    dpSet("_DataManager.Backup.InputFile.Device", daProjectConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_BACKUP_PATH]);
    
  // --------------------------------------------- 6.2) Set "DB-backup range back to" to 1 hour    
    dpSet("_DataManager.Backup.InputFile.StartTime", 3600);
    
  // --------------------------------------------- 6.3) Uncheck "Backup all not archived HDB datasets"
    dpSet("_DataManager.Backup.InputFile.UnsavedValArchs", false);
    
  // --------------------------------------------- 6.4) Set "automatic DB-backup" to never 
    dpSet("_OnlineBackup.interval", 0);
    dpSet("_OnlineBackup.validFrom", 0);
    dpSet("_OnlineBackup.validUntil", 1);
    
  // --------------------------------------------- 6.5) Set "Cancel DB-backup if"
  // ------------------------------------------------ 6.5.1) Maximum time of 0 minutes have been exceeded
    dpSet("_DataManager.Backup.Timeout", 0);
    
  // ------------------------------------------------ 6.5.2) Free virtual memory < than 58MB
    dpSet("_DataManager.Backup.MemoryKBLimit", 59392);
    
  // ------------------------------------------------ 6.5.3) More than 0 messages in the buffer
    dpSet("_DataManager.Backup.MaxBufferedMessages", 0);
    
  // --------------------------------------------- 6.6) Set other parameters
    dpSet("_DataManager.Backup.InputFile.Command", "backup");
    dpSet("_DataManager.Backup.InputFile.Type", "FILE");
    
    _unApplicationConfiguration_logMessage("    ... done.");
  }
  else
  {
    _unApplicationConfiguration_logMessage("  6) Online backup already configured. Ignoring step.");
  }
  
 

  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 8) Email alert groups configuration
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  _unApplicationConfiguration_logMessage("  8) Configuring email alert groups...");
  dyn_dyn_anytype ddaEmailConfigList = daProjectConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS];
  for (int i = 1 ; i <= dynlen(ddaEmailConfigList) ; i++)
  {
    dyn_anytype daEmailConfig = ddaEmailConfigList[i];
    
    sSystemName = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SYSTEM];
    if (strpos(sSystemName, ":") < 0)
    {
      sSystemName += ":";
    }
    
    string sConfigName = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_NAME];
    int iMaxPerDay = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_MAX_PER_DAY];
    string sHTMLLink = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_HTML_LINK];
    string sSenderAddress = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SENDER_ADDRESS];
    string sDomain = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_DOMAIN];
    string sSMTPServer = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SMTP_SERVER];
    dyn_string dsReceivers = daEmailConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_RECEIVER_LIST];
     
    dyn_uint dsSystemIds;
    
    getSystemNames(dsSystemNames,dsSystemIds);
    
    // System should be one of the known distributed systems
    if (dynContains(dsSystemNames, sSystemName))
    {
      string sError = "Alert email configuration error: system \"" + sSystemName + "\" doesn't exist.";
      
      _unApplicationConfiguration_logError(sError);
      fwException_raise(
          dsExceptions,
          "ERROR",
          sError,
          "INVALID_EMAIL_CONFIG_SYSTEM"
      );
    }
    else
    {
      const string SEPARATOR = ";";
      const string DELIMITER = "~";
      string sReceiver;
      for (i = 1 ; i <= dynlen(dsReceivers) ; i++)
      {
        if(sReceiver != "")
        {
          sReceiver = sReceiver + SEPARATOR + dsReceivers[i];
        }
        else
        {
          sReceiver = dsReceivers[i];
        }
      }
      
      string sConfigParams = sConfigName + DELIMITER + sDomain + DELIMITER + sSMTPServer + DELIMITER + sSenderAddress + DELIMITER + sReceiver + DELIMITER + sHTMLLink + DELIMITER + iMaxPerDay;	
    
      
      if (dpExists(sSystemName + "_unSendMail"))
      {      
        dyn_string dsConfigSettings;
        dyn_bool dbConfigDisabled;
        dyn_string dsReportSettings;
        
        dpGet(
            sSystemName + "_unSendMail.config.mail_settings",   dsConfigSettings,
            sSystemName + "_unSendMail.config.mail_disable",    dbConfigDisabled,
            sSystemName + "_unSendMail.config.report_settings", dsReportSettings
          );
          
        int iConfigExistsIndex = 0;
        for (i = 1 ; i <= dynlen(dsConfigSettings) ; i++)
        {
          dyn_string dsConfigSetting = strsplit(dsConfigSettings[i], DELIMITER);
          if (dsConfigSetting[1] == sConfigName)
          {
            // Already exists, overwrite with new settings
            iConfigExistsIndex = i;
          }
        }
        
        if (iConfigExistsIndex > 0)
        {
          dsConfigSettings[iConfigExistsIndex] = sConfigParams;
          dbConfigDisabled[iConfigExistsIndex] = false;
          dsReportSettings[iConfigExistsIndex] = "";
        }
        else
        {
          dynAppend(dsConfigSettings, sConfigParams);
          dynAppend(dbConfigDisabled, false);
          dynAppend(dsReportSettings, "");
        }
        
        string sDpCategory = sSystemName + sConfigName;
        
        // ------------------- Create scheduler-----------------
        if(!dpExists(sDpCategory))
        {
          if(nameCheck(sConfigName) == 0)
          {
            int iRes;
            _unSystemIntegrity_createScheduler(sConfigName, iRes, sSystemName);
          }
          else
          {
            string sError = "Alert email configuration error: invalid name \"" + sConfigName + "\".";
      
            _unApplicationConfiguration_logError(sError);
            fwException_raise(
                dsExceptions,
                "ERROR",
                sError,
                "EMAILCONFIGINVALIDNAME"
            );
          }
        }
        
        if(dpExists(sDpCategory))
        {
          dpSetWait(sSystemName + "_ScCom.transfer:_original.._value", sConfigName + "|" + dpSubStr(sDpCategory, DPSUB_SYS_DP));
        }
        else
        {
          string sError = "Alert email configuration error: creating config \"" + sConfigName + "\" scheduler creation failed.";
      
          _unApplicationConfiguration_logError(sError);
          fwException_raise(
              dsExceptions,
              "ERROR",
              sError,
              "EMAILCONFIGSCHEDULERFAILED"
          );
        }
        // ----------------------------------------
        
        dpSetWait(
            sSystemName + "_unSendMail.config.mail_settings",  dsConfigSettings,
            sSystemName + "_unSendMail.config.mail_disable",   dbConfigDisabled,
            sSystemName + "_unSendMail.config.report_settings", dsReportSettings
          );
        
      }
      else
      {
        string sError = "Alert email configuration error: could not read or write datapoint \"" + sSystemName + "_unSendMail" + "\"\nCheck system is connected";
        
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "COULDNOTREADDPECONFIG"
        );
      }
    }
  }
  _unApplicationConfiguration_logMessage("    ... done.");


  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  // ------------------------------------------------------- 9) Proceed to online backup
  // -------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------
  
  if (bDoBackup)
  {
    // --------------------------------------------- 9.1) Start online backup  
    _unApplicationConfiguration_logMessage("  9) Launching online backup. This operation can last several minutes...");
  
    iExceptionsLength = dynlen(dsExceptions);
    unBackup_runBackupWait(getSystemName(), dsExceptions);
  
    // --------------------------------------------- 9.2) Check backcup has been sucessfully executed
    if (dynlen(dsExceptions) > iExceptionsLength)
    {
      _unApplicationConfiguration_logError(dsExceptions[iExceptionsLength + 2]);
    }
    else
    {
      _unApplicationConfiguration_logMessage("    ... done.");
    }  
  }
}

void _unApplicationConfiguration_logMessage(string sMessage)
{
  _unApplicationConfiguration_log(sMessage);
}

void _unApplicationConfiguration_logError(string sError)
{
  _unApplicationConfiguration_log(sError, true);
}

void _unApplicationConfiguration_logWarning(string sWarning)
{
  _unApplicationConfiguration_log(sWarning, false, true);
}

void _unApplicationConfiguration_log(string sLogMessage, bool bError = false, bool bWarning = false)
{
  // If called from the panel, log the message in the text edit
  if (shapeExists("textEditLog"))
  {
    if (bError)
    {
      textEditLog.append("<font color=#FF0000>" + formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " - " + sLogMessage + "</font>");
    }
    else if (bWarning)
    {
      textEditLog.append("<font color=#FFA500>" + formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " - " + sLogMessage + "</font>");
    }
    else
    {
      textEditLog.append(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " - " + sLogMessage);
    }
  }
  
  // Log the message in log viewer
  // Produces lots of spam, uncomment if necessary
  // DebugTN(sLogMessage);
  
  // Log the message in a file.
  if (UN_APPLICATION_CONFIGURATION_LOG_FILE)
  {
    string sFileMessage = formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d");
    if (bError)
    {
      sFileMessage += "-ERROR-";
    }
    else if (bWarning)
    {
      sFileMessage += "-WARNING-";
    }
    else
    {
      sFileMessage += "-INFO-";
    }
    
    sFileMessage += sLogMessage + "\n";
    fputs(sFileMessage, UN_APPLICATION_CONFIGURATION_LOG_FILE);    
  }  
}

//_unApplicationConfiguration_createFrontEndApplArchive
/**
  Purpose: 
  Create an archive (DP + archive manager). This function will create an archive with a precise name on a precise manager number X.
  All archive DP must be _ValueArchive_X, if this DP already exist for an archive with another name then it is an error. 
  Similarly, if this archive name is already used in another DP it is also an error.
  
  
  @param iArchiveType: int, input: 1 for analog, 2 for boolean, 3 for event
  @param sFrontEndAppName: string, input: name of the front end application, used to build the name of the archive
  @param daArchiveConfig: dyn_anytype, input: configuration of the archive
  @param dsExceptions: dyn_string, output: list of errors occurring in the function
  
  Usage: Internal.
*/
void _unApplicationConfiguration_createFrontEndApplArchive(int iArchiveType, string sFrontEndAppName, dyn_anytype daArchiveConfig, dyn_string &dsExceptions, int iFileSwitchTimeout)
{
  dyn_string dsAllArchiveDps = unGenericUtilities_getArchiveList(getSystemName());
  int iManager = daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM];
  string sArchiveName = "AR" + iManager + "_" + sFrontEndAppName + "_" + UN_APPLICATION_CONFIGURATION_APPL_ARCHIVES_LABEL[iArchiveType];
  
   // --------------------------------------------- 1) Check validity of data regarding current config
  
  // Check if an archive _ValueArchive_XXXX already exists (XXXX = manager number given to the function)
  // If it exists 
  //   If its name is ARxx_AppName_TYPE we update it
  //   Else we stop configuration : error
  // If it does not exist    
  //   Check if ARxx_AppName_TYPE exists with another DP name
  //     If ARxx_AppName_TYPE exists : error
  //     Else we create it
  bool bValueArchiveDpExists = false;
  bool bArchiveExists = false;
  
  for (int i = 1 ; i <= dynlen(dsAllArchiveDps) ; i++)
  {
    if (dpSubStr(dsAllArchiveDps[i],DPSUB_DP) == "_ValueArchive_" + iManager)
    {
      string sTempArchiveName;
      dpGet(dsAllArchiveDps[i] + ".general.arName", sTempArchiveName);
      if (sTempArchiveName == sArchiveName)
      {
        // Ok, update
        bArchiveExists = true;
        bValueArchiveDpExists = true;
        break;        
      }
      else
      {
        // Error
        string sError = getCatStr("unApplicationConfiguration","007-MANAGER_ALREADY_IN_USE");
        strreplace(sError, "MANAGER", iManager);
        strreplace(sError, "VALUE_ARCHIVE", dsAllArchiveDps[i]);
        strreplace(sError, "OLD_ARCHIVE", sTempArchiveName);
        strreplace(sError, "ARCHIVE", sArchiveName);
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "007-MANAGER_ALREADY_IN_USE"
        );
        
        return;        
      }
    } 
  }
    
  if (!bValueArchiveDpExists)
  {
    for (int i2 = 1 ; i2 <= dynlen(dsAllArchiveDps) ; i2++)
    {        
      string sTempArchiveName;
      dpGet(dsAllArchiveDps[i2] + ".general.arName", sTempArchiveName);
      int iMgr;
      dpGet(dsAllArchiveDps[i2] + ".arNr", iMgr);
      if (sTempArchiveName == sArchiveName)
      {
        // Error
        string sError = getCatStr("unApplicationConfiguration","008-ARCHIVE_ALREADY_EXISTS");
        strreplace(sError, "ARCHIVE_NAME", sArchiveName);
        strreplace(sError, "ARCHIVE_DP", dsAllArchiveDps[i2]);
        strreplace(sError, "MANAGER", iMgr);
        strreplace(sError, "ARCHIVE", sArchiveName);
        
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "008-ARCHIVE_ALREADY_EXISTS"
        );
        
        return;  
      }
    }
  }
  
  // --------------------------------------------- 1) If archive ARxx_AppName_Type does not exist (xx is the manager number, read in the config file) 

  string sArchiveDp = "_ValueArchive_" + iManager;
  if (!bArchiveExists)
  {
    _unApplicationConfiguration_logMessage("        Creating archive '" + sArchiveName + "'...");
      
  // ------------------------------------------------ 1.1) Create it
           
    int iOldExceptionLen = dynlen(dsExceptions);
    unGenericUtilities_createArchive(sArchiveName, dsExceptions, iManager);
    if (dynlen(dsExceptions) > iOldExceptionLen)
    {
      _unApplicationConfiguration_logError(dsExceptions[iOldExceptionLen + 2]);
      return; 
    }
    
    
  // ------------------------------------------------ 1.2) Set max number of values
    dpSet(sArchiveDp + ".size.maxValuesSet", daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_VALUES]);
      
  // ------------------------------------------------ 1.3) Set max number of DPE to read data
    dpSet(sArchiveDp + ".size.maxDpElSet", daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_DPES]);
    
  // ------------------------------------------------ 1.4) Activate compression
    unGenericUtilities_activateArchiveCompression(sArchiveDp, true, dsExceptions);
      
      
  // ------------------------------------------------ 1.5) Set data records to remain on the disk to read data
    dpSet(sArchiveDp + ".setMgmt.fileDeletion.keepCountSet", daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_FILES]);
      
  // ------------------------------------------------ 1.6) Add archive manager, set to always start and check if started
    // Add the manager
    int iPort;
    string sHost;
    string sIP;
    string sSystem = getSystemName();
    unGenericDpFunctions_getHostName(sSystem, sHost, sIP);
    unGenericDpFunctions_getpmonPort(sSystem, iPort);
    
    _unApplicationConfiguration_logMessage("        Creating archive manager...");
    if (
        unSystemIntegrity_addManager(
          "WCCOAvalarch",
          "manual",
          30,
          2,
          2,
          "-num " + iManager,
          dsExceptions
        ) == 0
      )
    {
      string sError = getCatStr("unApplicationConfiguration","009-ARCHIVE_MANAGER_ADD_FAILED");
      strreplace(sError, "MANAGER", iManager);
      _unApplicationConfiguration_logError(sError);
      fwException_raise(
          dsExceptions,
          "ERROR",
          sError,
          "009-ARCHIVE_MANAGER_ADD_FAILED"
      );
        
      return;  
    }
      
    // Start it
    bool bTimerExpired = false;
    unGenericUtilities_startManagerWait("WCCOAvalarch", "-num " + iManager, dsExceptions, sSystem, 30, bTimerExpired);
     
    if (bTimerExpired)
    {
      string sError = getCatStr("unApplicationConfiguration","010-ARCHIVE_MANAGER_START_FAILED");
      strreplace(sError, "MANAGER", iManager);
      _unApplicationConfiguration_logError(sError);
      fwException_raise(
          dsExceptions,
          "ERROR",
          sError,
          "010-ARCHIVE_MANAGER_START_FAILED"
      );
        
      // Do not stop configuration
    }
    else
    {
      unGenericUtilities_setManagerAlways("WCCOAvalarch", "-num " + iManager, dsExceptions, getSystemName());
      _unApplicationConfiguration_logMessage("        Archive manager started...");
    }
         
      
  // ------------------------------------------------ 1.7) File-switch, check file was created    
    unImportDevice_valarchFileSwitch(sArchiveDp, dsExceptions, iFileSwitchTimeout);
    
    if (dynlen(dsExceptions) > iOldExceptionLen) // Error creating the archive.
    {
      _unApplicationConfiguration_logError(dsExceptions[iOldExceptionLen + 2]);
      return;
    }
    else
    {
      _unApplicationConfiguration_logMessage("        Archive created.");  
    }
  }
  else
  {
  // --------------------------------------------- 2) Else
    _unApplicationConfiguration_logMessage("        Updating archive '" + sArchiveName + "'");   
      
      
  // ------------------------------------------------ 2.1) Add former number of values to the newly read data
    int iMaxValuesValue;
    dpGet(sArchiveDp + ".size.maxValuesSet", iMaxValuesValue);
    dpSet(sArchiveDp + ".size.maxValuesSet", iMaxValuesValue + daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_VALUES]);
    
  // ------------------------------------------------ 2.2) Add former number of DPE to the newly read data
    int iMaxDpElValue;
    dpGet(sArchiveDp + ".size.maxDpElSet", iMaxDpElValue);
    dpSet(sArchiveDp + ".size.maxDpElSet", iMaxDpElValue + daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_DPES]);
    
  // ------------------------------------------------ 2.3) Keep larger value for number of records between the former and newly read data
    int iKeepCountValue;
    dpGet(sArchiveDp + ".setMgmt.fileDeletion.keepCountGet", iKeepCountValue); 
    if (daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_FILES] > iKeepCountValue)
    {
      iKeepCountValue = daArchiveConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_FILES];
    }
    dpSet(sArchiveDp + ".setMgmt.fileDeletion.keepCountSet", iKeepCountValue);
    
  // ------------------------------------------------ 2.4) Add archive manager, set to alwasy start and check if started
    // If an error occur during a previous configuration, managar may not exist, then we add it
    unSystemIntegrity_addManager(
          "WCCOAvalarch",
          "manual",
          30,
          2,
          2,
          "-num " + iManager,
          dsExceptions
    );
        
    // Manager already exists, start it (always) if necessary
    
    // Get manager index
    int iIndex;
    unSystemIntergity_manageManager("WCCOAvalarch", "-num " + iManager, "", iIndex, dsExceptions, getSystemName());
    
    // Read state
    int iState;
    dyn_string dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, getSystemName());
    
    if(dynlen(dsResults)==5)
    {
      iState = (int)dsResults[1];
    }
    else 
    {
      iState = -1;
    }
    
    // Manager is not started, do it
    if (2 != iState)
    {
      bool bTimedOut = false;
      unGenericUtilities_startManagerWait("WCCOAvalarch", "-num " + iManager, dsExceptions, getSystemName(), 30, bTimedOut);
      if (bTimedOut)
      {
        string sError = getCatStr("unApplicationConfiguration","010-ARCHIVE_MANAGER_START_FAILED");
        strreplace(sError, "MANAGER", iManager);
        _unApplicationConfiguration_logError(sError);
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            "010-ARCHIVE_MANAGER_START_FAILED"
        );
        // Do not stop configuration
      }      
    }
    
    
    // In case it wasn't, set it to always start.
    unGenericUtilities_setManagerAlways("WCCOAvalarch", "-num " + iManager, dsExceptions, getSystemName());
    
    
    
        
    
      
  // ------------------------------------------------ 2.5) File-switch, check file was created  
    int iOldExceptionLen = dynlen(dsExceptions);
    unImportDevice_valarchFileSwitch(sArchiveDp, dsExceptions, iFileSwitchTimeout);
    if (dynlen(dsExceptions) > iOldExceptionLen)
    {
      _unApplicationConfiguration_logError(dsExceptions[iOldExceptionLen + 2]);
      return;
    } 
  }
}



//_unApplicationConfiguration_readProjectConfig
/**
  Purpose: 
  Reads the given XML file and stores the data in a variable to be used later.
  
  @param sConfigFilePath: string, input: path to the XML file containing the parameters
  @param daConfig: dyn_anytype, output: content of the confiuration file
  @param dsExceptions: dyn_string, output: list of errors occurring in the function
  
  Usage: Internal.  
*/
_unApplicationConfiguration_readProjectConfig(string sConfigFilePath, dyn_anytype &daConfig, dyn_string &dsExceptions)
{
  
  // This functions reads a configuration file which has been validated before, so we suppose everything will be as expected.
  // Note  : dyn_anytype are used to represent a XML element node even when there is only one value to store, 
  //         this is to avoid to many changes in case we want to add extra data in the future.
  //         Indexes to this dynamic arrays are defined in the top of the lib.

  
  // XSD file to validate the xml structure.
  const string XSD_FILE = getPath(DATA_REL_PATH, "unApplicationConfiguration.xsd");
 
  // ----------------------------------------------------------------------------------------
  // -------------------------------------------- XML elements and attributes
  // ----------------------------------------------------------------------------------------
  // This is a list of all the XML elements and attribues likely to be encountered in the file.

  //------------------------------------------------------------------------------------------------------ --|
  const string XML_ELEMENT_PROJECT_CONFIG                                   = "projectConfig";
  //------------------------------------------------------------------------------------------------------ -- --@
  const string XML_PROJECT_CONFIG_APPLICATION_NAME                          = "appName";
  //------------------------------------------------------------------------------------------------------ -- --@
  const string XML_PROJECT_CONFIG_DISTRIBUTED_PORT                          = "distributedPortNumber";
  //------------------------------------------------------------------------------------------------------ -- --@
  const string XML_PROJECT_CONFIG_BACKUP_PATH                               = "backupPath";
  //------------------------------------------------------------------------------------------------------ -- --@
  const string XML_PROJECT_CONFIG_DEFAULT_PANEL                             = "defaultPanel";
  
  //------------------------------------------------------------------------------------------------------ -- --|
  const string XML_ELEMENT_PANEL_SETTINGS                                   = "settings";
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_PANEL_SETTINGS_FILE_SWITCH_TIMEOUT               = "fileSwitchTimeOut";
  
  //------------------------------------------------------------------------------------------------------ -- --|
  const string XML_ELEMENT_ALARM_CONFIG                                     = "alarm";  
  //------------------------------------------------------------------------------------------------------ -- -- --@
  const string XML_ALARM_CONFIG_DELETION_FREQUENCY                          = "deletionCycleFrequency";
  
  //------------------------------------------------------------------------------------------------------ -- --|
  const string XML_ELEMENT_FRONT_END_APPLICATION_LIST                       = "frontEndList";
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_FRONT_END_APPLICATION                            = "frontEnd";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@
  const string XML_FRONT_END_APPLICATION_NAME                               = "appName";
  //------------------------------------------------------------------------------------------------------ -- -- -- --|
  const string XML_ELEMENT_FRONT_END_APPLICATION_TYPE                       = "type";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --|
  const string XML_ELEMENT_FRONT_END_APPLICATION_TYPE_MODBUS                = "MODBUS";  
  //------------------------------------------------------------------------------------------------------ -- -- -- -- -- --@
  const string XML_FRONT_END_APPLICATION_TYPE_MODBUS_DATA_SERVER_PORT       = "dataServerPort";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- -- --@
  const string XML_FRONT_END_APPLICATION_TYPE_MODBUS_FRONT_END_IP           = "frontEndIPAdress";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- -- --@
  const string XML_FRONT_END_APPLICATION_TYPE_MODBUS_DRIVER_NUM             = "driverNum";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --|
  const string XML_ELEMENT_FRONT_END_APPLICATION_TYPE_S7                    = "S7";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- -- --@
  const string XML_FRONT_END_APPLICATION_TYPE_S7_DRIVER_NUM                 = "driverNum";
  
  
  //------------------------------------------------------------------------------------------------------ -- --|
  const string XML_ELEMENT_ARCHIVES_LIST                                    = "archivesList";
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_ARCHIVES                                         = "archives";
  //------------------------------------------------------------------------------------------------------ -- -- --@
  const string XML_ARCHIVES_NAME                                            = "appName";
  //------------------------------------------------------------------------------------------------------ -- -- -- --|
  const string XML_ELEMENT_ARCHIVES_ANA                                     = "ANA";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ARCHIVE_MANAGER_NUM                                      = "managerNum";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ARCHIVE_NUMBER_OF_VALUES                                 = "numberOfValues";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ARCHIVE_NUMBER_OF_DPES                                   = "numberOfDPEs";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ARCHIVE_NUMBER_OF_FILES                                  = "numberOfFiles"; 
  //------------------------------------------------------------------------------------------------------ -- -- -- --|     
  const string XML_ELEMENT_ARCHIVES_BOOL                                    = "BOOL";
  //------------------------------------------------------------------------------------------------------ -- -- -- --|
  const string XML_ELEMENT_ARCHIVES_EVENT                                   = "EVENT";
  
    
  
  
  
  //------------------------------------------------------------------------------------------------------ -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL                                        = "userAccessControl";        
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_LDAP                                   = "LDAP";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@
  const string XML_USER_ACCESS_CONTROL_LDAP_USE                                       = "use";
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_CENTRAL_SERVER                         = "centralServer";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_CENTRAL_SERVER_NAME                    = "serverName";
  
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST                            = "domainList";
  //------------------------------------------------------------------------------------------------------ -- -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN                     = "domain";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_NAME                = "name";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_FULL_NAME           = "fullName";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_COMMENT             = "comment";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_PRIVILEGE           = "privilege";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_PRIVILEGE_NAME      = "name";
  
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST                             = "groupList";
  //------------------------------------------------------------------------------------------------------ -- -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP                       = "group";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_NAME                  = "name";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_FULL_NAME             = "fullName";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_DESCRIPTION           = "description";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_PRIVILEGE             = "privilege";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_PRIVILEGE_DOMAIN      = "domain";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_PRIVILEGE_PRIVILEGE   = "privilege";
  
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_EGROUP_MAPPINGS                        = "eGroupsMapping";
  //------------------------------------------------------------------------------------------------------ -- -- -- --|
  const string XML_ELEMENT_USER_ACCESS_CONTROL_EGROUP_MAPPINGS_MAPPING                = "mapping";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_EGROUP_MAPPINGS_MAPPING_GROUP_NAME     = "groupName";
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@
  const string XML_ELEMENT_USER_ACCESS_CONTROL_EGROUP_MAPPINGS_MAPPING_EGROUP_NAME    = "egroupName";

  
  
  
  //------------------------------------------------------------------------------------------------------ -- --|
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS                                       = "emailConfigurations";     
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION                   = "emailConfiguration";       
  //------------------------------------------------------------------------------------------------------ -- -- -- --@   
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_SYSTEM            = "systemName";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@   
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_NAME              = "configName";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@   
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_MAX_PER_DAY       = "maxPerDay";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@   
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_HTML_LINK         = "htmlLink";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@   
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_SENDER_ADDRESS    = "senderAddress";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@   
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_DOMAIN            = "domain";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@    
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_SMTP_SERVER       = "SMTPserver";
  //------------------------------------------------------------------------------------------------------ -- -- -- --|
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_RECEIVER          = "receiver";       
  //------------------------------------------------------------------------------------------------------ -- -- -- -- --@  
  const string XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_RECEIVER_ADDRESS  = "address";       

  
  //------------------------------------------------------------------------------------------------------ -- --|
  const string XML_ELEMENT_AUTOLOGOUT                           = "autoLogout";
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_AUTOLOGOUT_WARNING_TIMEOUT           = "warningTimeout";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@
  const string XML_ELEMENT_AUTOLOGOUT_WARNING_TIMEOUT_HOURS     = "hours";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@
  const string XML_ELEMENT_AUTOLOGOUT_WARNING_TIMEOUT_MINUTES   = "minutes";
  //------------------------------------------------------------------------------------------------------ -- -- --|
  const string XML_ELEMENT_AUTOLOGOUT_COMMIT_TIMEOUT            = "commitTimeout";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@
  const string XML_ELEMENT_AUTOLOGOUT_COMMIT_TIMEOUT_HOURS      = "hours";
  //------------------------------------------------------------------------------------------------------ -- -- -- --@
  const string XML_ELEMENT_AUTOLOGOUT_COMMIT_TIMEOUT_MINUTES    = "minutes";
  
  

  // First, validate the XML file
  unGenericUtilities_validateXmlFile(sConfigFilePath, XSD_FILE, dsExceptions);
  if (dynlen(dsExceptions))
  {
    return;
  }
  
  // Init variable
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST] = makeDynAnytype();
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS] = makeDynAnytype();
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_ALARM_CONFIG] = makeDynAnytype();
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST] = makeDynAnytype();
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST] = makeDynAnytype();
  mapping mTemp;
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL] = mTemp;
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS] = makeDynAnytype();
  daConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT] = makeDynInt();
  
  
  
  string sErrorMessage;
  int iErrorLine;
  int iErrorColumn;
  
  // We know the xml exists and is valid so no need to check anything
  int xmlConfigFile = xmlDocumentFromFile(sConfigFilePath, sErrorMessage, iErrorLine, iErrorColumn);
  
  // Root node of the file (projectConfig)
  int xmlConfigRootElement = xmlFirstChild(xmlConfigFile);
  
  // Warning : the first node can be "<?xml version="1.0" encoding="UTF-8"?>", in which case the file is still considered valid, thus we switch to the next node
  if (xmlNodeName(sConfigFilePath, xmlConfigRootElement) != XML_ELEMENT_PROJECT_CONFIG)
  {
    xmlConfigRootElement = xmlNextSibling(xmlConfigFile, xmlConfigRootElement);
  } 
  
  // -------------------------------------------------- 1) Read the project settings  
  
  daConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS] = makeDynAnytype();
  
  mapping mProjectAttributes = xmlElementAttributes(xmlConfigFile, xmlConfigRootElement);

  // The name is always there  
  _unApplicationConfiguration_logMessage("   Reading project application " + mProjectAttributes[XML_PROJECT_CONFIG_APPLICATION_NAME]);
  daConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_APPLICATION_NAME] = mProjectAttributes[XML_PROJECT_CONFIG_APPLICATION_NAME];
  
  // The port can be missing from the file, in this case it is set do a default value
  int iDistributedPortNumber;
  if (mappingHasKey(mProjectAttributes, XML_PROJECT_CONFIG_DISTRIBUTED_PORT))
  {
    iDistributedPortNumber = mProjectAttributes[XML_PROJECT_CONFIG_DISTRIBUTED_PORT];
  }
  else
  {
    iDistributedPortNumber = 4777;
  }
  
  // Distributed port number
  _unApplicationConfiguration_logMessage("     Distributed port: " + iDistributedPortNumber);
  daConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_DISTRIBUTED_PORT] = iDistributedPortNumber;
  
  // The backup path is always there
  _unApplicationConfiguration_logMessage("     Backup path: " + mProjectAttributes[XML_PROJECT_CONFIG_BACKUP_PATH]);
  daConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_BACKUP_PATH] = mProjectAttributes[XML_PROJECT_CONFIG_BACKUP_PATH];
  
  // Default panel
  string sDefaultPanel;  
  if (mappingHasKey(mProjectAttributes, XML_PROJECT_CONFIG_DEFAULT_PANEL))
  {
    sDefaultPanel = mProjectAttributes[XML_PROJECT_CONFIG_DEFAULT_PANEL];
  }
  daConfig[UN_APPLICATION_CONFIGURATION_PARAMETERS][UN_APPLICATION_CONFIGURATION_INDEX_DEFAULT_PANEL] = sDefaultPanel;
  
  _unApplicationConfiguration_logMessage("     Backup path: " + mProjectAttributes[XML_PROJECT_CONFIG_BACKUP_PATH]);
  
  int xmlCurrentNode = xmlFirstChild(xmlConfigFile, xmlConfigRootElement);
  
  while (xmlCurrentNode != -1)
  {
    switch(xmlNodeName(xmlConfigFile, xmlCurrentNode))
    {
      case XML_ELEMENT_PANEL_SETTINGS:
      {
      
  // -------------------------------------------------- 2) Read the panel settings
  
        // First child of the projectConfig node (settings)
        int xmlSettingsNode = xmlCurrentNode;
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS] = makeDynAnytype();
        _unApplicationConfiguration_logMessage("   Reading panel settings...");
        
        // List of possible parameters expected (currently only one)
        bool bSettingSwitchTimeoutFound = false;
        
        int xmlSetting =  xmlFirstChild(xmlConfigFile, xmlSettingsNode);
        while (xmlSetting != -1)
        {
          switch (xmlNodeName(xmlConfigFile, xmlSetting))
          {
            case XML_ELEMENT_PANEL_SETTINGS_FILE_SWITCH_TIMEOUT:
            {
              int iSettingValue = (int)xmlNodeValue(xmlConfigFile, xmlFirstChild(xmlConfigFile, xmlSetting));
              _unApplicationConfiguration_logMessage("     File switch timeout value: " + iSettingValue);
              daConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS][UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT] = iSettingValue;  
              bSettingSwitchTimeoutFound = true;
              break;
            }      
            default: // Impossible 
              break;            
          }    
          
          xmlSetting = xmlNextSibling(xmlConfigFile,xmlSetting);
        }  
        
        // Apply default parameters for those that were not found
        if (!bSettingSwitchTimeoutFound)
        {
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_PANEL_SETTINGS][UN_APPLICATION_CONFIGURATION_INDEX_FILE_SWITCH_TIMEOUT] = 60;
          _unApplicationConfiguration_logMessage("     File switch timeout value: 60");
        }
        
        break;
      }
      case XML_ELEMENT_ALARM_CONFIG:
      {
        // -------------------------------------------------- 3) Read the alarm config
        
        _unApplicationConfiguration_logMessage("   Reading alarm config...");
        int xmlAlarmNode = xmlCurrentNode;
        dyn_anytype daAlarmConfig;
        int iAlarmArchiveDeletionCycleFrequency;
        xmlGetElementAttribute(xmlConfigFile, xmlAlarmNode, XML_ALARM_CONFIG_DELETION_FREQUENCY, iAlarmArchiveDeletionCycleFrequency);
        daAlarmConfig[UN_APPLICATION_CONFIGURATION_INDEX_ALARM_CONFIG_DELETION_CYCLE] = iAlarmArchiveDeletionCycleFrequency;
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_ALARM_CONFIG] = daAlarmConfig;
        _unApplicationConfiguration_logMessage("     Alarm deletion cycle " + iAlarmArchiveDeletionCycleFrequency);
        
        // First front-end application
        int xmlFrontEndApplicationNode = xmlNextSibling(xmlConfigFile, xmlAlarmNode);
        
        break;
      }
      case XML_ELEMENT_FRONT_END_APPLICATION_LIST:
      {
        
        // -------------------------------------------------- 4) Read the front-end application list
        
        _unApplicationConfiguration_logMessage("   Reading front-end application list...");
        dyn_dyn_anytype ddaFrontEndApplicationList;
        // We do not know how many front-end application there are, so we iterate until we find a node of another kind or until the end
        int indexFrontEndApplication = 0;
        
        int xmlFrontEndApplicationNode = xmlFirstChild(xmlConfigFile, xmlCurrentNode);
        
        while (xmlFrontEndApplicationNode != -1)
        {
          indexFrontEndApplication++;
          _unApplicationConfiguration_logMessage("     Front-end application #" + indexFrontEndApplication);
          
          // ---------- 4.1) Front-end application name
          dyn_anytype daFrontEndApplication;
          string sFrontEndApplName;
          xmlGetElementAttribute(xmlConfigFile, xmlFrontEndApplicationNode, XML_FRONT_END_APPLICATION_NAME, sFrontEndApplName);
          daFrontEndApplication[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_NAME] = sFrontEndApplName;
          _unApplicationConfiguration_logMessage("       Name: " + sFrontEndApplName);
          
          
          
          // ---------- 4.2) Front-end application type
          dyn_anytype daFrontEndApplicationType;
          int xmlFrontEndApplicationTypeNode = xmlFirstChild(xmlConfigFile, xmlFrontEndApplicationNode);
          string sFrontEndApplicationTypeName = xmlNodeName(xmlConfigFile,xmlFirstChild(xmlConfigFile, xmlFrontEndApplicationTypeNode));    
          // Application type name (MODBUS/S7)
          daFrontEndApplicationType[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_NAME] = sFrontEndApplicationTypeName;
          _unApplicationConfiguration_logMessage("       Type: " + sFrontEndApplicationTypeName);
          switch (sFrontEndApplicationTypeName)
          {
            case XML_ELEMENT_FRONT_END_APPLICATION_TYPE_MODBUS:
            {
              int iDataServerPort;
              int iDriverNum;
              string sFrontEndIpAddress;
              xmlGetElementAttribute(xmlConfigFile, xmlFirstChild(xmlConfigFile, xmlFrontEndApplicationTypeNode), XML_FRONT_END_APPLICATION_TYPE_MODBUS_DATA_SERVER_PORT, iDataServerPort);
              xmlGetElementAttribute(xmlConfigFile, xmlFirstChild(xmlConfigFile, xmlFrontEndApplicationTypeNode), XML_FRONT_END_APPLICATION_TYPE_MODBUS_DRIVER_NUM, iDriverNum);
              xmlGetElementAttribute(xmlConfigFile, xmlFirstChild(xmlConfigFile, xmlFrontEndApplicationTypeNode), XML_FRONT_END_APPLICATION_TYPE_MODBUS_FRONT_END_IP, sFrontEndIpAddress);
              daFrontEndApplicationType[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_DATA_SERVER_PORT] = iDataServerPort;
              daFrontEndApplicationType[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_DRIVER_NUM] = iDriverNum;
              daFrontEndApplicationType[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_MODBUS_FRONT_END_IP_ADDRESS] = sFrontEndIpAddress;
              _unApplicationConfiguration_logMessage("         Data server port: " + iDataServerPort);
              _unApplicationConfiguration_logMessage("         Driver number: " + iDriverNum);
              _unApplicationConfiguration_logMessage("         Front-end IP address: " + sFrontEndIpAddress);
              
              break;
            }
            case XML_ELEMENT_FRONT_END_APPLICATION_TYPE_S7:
            {
              int iDriverNum;
              xmlGetElementAttribute(xmlConfigFile, xmlFirstChild(xmlConfigFile, xmlFrontEndApplicationTypeNode), XML_FRONT_END_APPLICATION_TYPE_S7_DRIVER_NUM, iDriverNum);
              daFrontEndApplicationType[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE_S7_DRIVER_NUM] = iDriverNum;
              _unApplicationConfiguration_logMessage("         Driver number : " + iDriverNum);
            
              break;
            }      
            // Cannot be any other case
          }
          
          daFrontEndApplication[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_TYPE] = daFrontEndApplicationType;
          
          dynAppend(ddaFrontEndApplicationList, daFrontEndApplication);
          
          xmlFrontEndApplicationNode = xmlNextSibling(xmlConfigFile, xmlFrontEndApplicationNode);  
        }
        
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_FRONT_END_APPLICATION_LIST] = ddaFrontEndApplicationList;
        
        break;
      }
      case XML_ELEMENT_ARCHIVES_LIST:
      {
        
        // -------------------------------------------------- 5) Read the archive list
        
        int xmlArchiveSetNode = xmlFirstChild(xmlConfigFile, xmlCurrentNode);
        
        _unApplicationConfiguration_logMessage("   Reading archives list...");
        
        
        dyn_dyn_anytype ddaArchiveSetList;
        // We do not know how many archives node there are, so we iterate until we find a node of another kind or until the end
        int iIndexArchiveSetElement = 0;
        
        while (xmlArchiveSetNode != -1)
        {
          iIndexArchiveSetElement++;    
          
          dyn_anytype daArchiveSetElements;
              
          int iManagerNum;
          int iNumberOfValues;
          int iNumberOfDpes;
          int iNumberOfFiles;
          
          // ---------- 5.1) Archive name
          string sArchiveName;
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetNode, XML_ARCHIVES_NAME, sArchiveName);
          daArchiveSetElements[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NAME] = sArchiveName;
          
          _unApplicationConfiguration_logMessage("     Archive set #" + iIndexArchiveSetElement + " (" + sArchiveName + "):");
          
          // ---------- 5.2) Analog archive    
          _unApplicationConfiguration_logMessage("       Analog:");
          int xmlArchiveSetAnalog = xmlFirstChild(xmlConfigFile, xmlArchiveSetNode);
          dyn_anytype daAnalogArchive;
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetAnalog, XML_ARCHIVE_MANAGER_NUM, iManagerNum);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetAnalog, XML_ARCHIVE_NUMBER_OF_VALUES, iNumberOfValues);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetAnalog, XML_ARCHIVE_NUMBER_OF_DPES, iNumberOfDpes);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetAnalog, XML_ARCHIVE_NUMBER_OF_FILES, iNumberOfFiles);
          daAnalogArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM] = iManagerNum;
          daAnalogArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_VALUES] = iNumberOfValues;
          daAnalogArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_DPES] = iNumberOfDpes;
          daAnalogArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_FILES] = iNumberOfFiles;
          _unApplicationConfiguration_logMessage("         Manager: " + iManagerNum);
          _unApplicationConfiguration_logMessage("         Values: " + iNumberOfValues);
          _unApplicationConfiguration_logMessage("         DPEs: " + iNumberOfDpes);
          _unApplicationConfiguration_logMessage("         Files: " + iNumberOfFiles);
          
          daArchiveSetElements[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_ANA] = daAnalogArchive;
          
          // ---------- 5.3) Boolean archive
          _unApplicationConfiguration_logMessage("       Boolean:");
          int xmlArchiveSetBool = xmlNextSibling(xmlConfigFile, xmlArchiveSetAnalog);
          dyn_anytype daBoolArchive;
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetBool, XML_ARCHIVE_MANAGER_NUM, iManagerNum);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetBool, XML_ARCHIVE_NUMBER_OF_VALUES, iNumberOfValues);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetBool, XML_ARCHIVE_NUMBER_OF_DPES, iNumberOfDpes);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetBool, XML_ARCHIVE_NUMBER_OF_FILES, iNumberOfFiles);
          daBoolArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM] = iManagerNum;
          daBoolArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_VALUES] = iNumberOfValues;
          daBoolArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_DPES] = iNumberOfDpes;
          daBoolArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_FILES] = iNumberOfFiles;
          _unApplicationConfiguration_logMessage("         Manager: " + iManagerNum);
          _unApplicationConfiguration_logMessage("         Values: " + iNumberOfValues);
          _unApplicationConfiguration_logMessage("         DPEs: " + iNumberOfDpes);
          _unApplicationConfiguration_logMessage("         Files: " + iNumberOfFiles);
          
          daArchiveSetElements[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_BOOL] = daBoolArchive;
          
          
          // ---------- 5.4) Event archive
          _unApplicationConfiguration_logMessage("       Event:");
          int xmlArchiveSetEvent = xmlNextSibling(xmlConfigFile, xmlArchiveSetBool);
          dyn_anytype daEventArchive;
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetEvent, XML_ARCHIVE_MANAGER_NUM, iManagerNum);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetEvent, XML_ARCHIVE_NUMBER_OF_VALUES, iNumberOfValues);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetEvent, XML_ARCHIVE_NUMBER_OF_DPES, iNumberOfDpes);
          xmlGetElementAttribute(xmlConfigFile, xmlArchiveSetEvent, XML_ARCHIVE_NUMBER_OF_FILES, iNumberOfFiles);
          daEventArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_MANAGER_NUM] = iManagerNum;
          daEventArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_VALUES] = iNumberOfValues;
          daEventArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_DPES] = iNumberOfDpes;
          daEventArchive[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_NUMBER_OF_FILES] = iNumberOfFiles;
          _unApplicationConfiguration_logMessage("         Manager: " + iManagerNum);
          _unApplicationConfiguration_logMessage("         Values: " + iNumberOfValues);
          _unApplicationConfiguration_logMessage("         DPEs: " + iNumberOfDpes);
          _unApplicationConfiguration_logMessage("         Files: " + iNumberOfFiles);
          
          daArchiveSetElements[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_EVENT] = daEventArchive;
          
          
          // Append the new set
          dynAppend(ddaArchiveSetList, daArchiveSetElements);
          
          xmlArchiveSetNode = xmlNextSibling(xmlConfigFile, xmlArchiveSetNode);  
        }  
        
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_ARCHIVE_LIST] = ddaArchiveSetList;
        break;
      }
      case XML_ELEMENT_USER_ACCESS_CONTROL:
      {
            
        // -------------------------------------------------- 6) Read the user access control configuration
        
        _unApplicationConfiguration_logMessage("   Reading user access control configuration...");  
        // The current node after the previous loop should be either the user access control node or null (since user access is not mandatory)
        // Besides, there can be one, two, or no element inside this node so care is needed
        mapping daUserAccessControl;
        
        int xmlUserAccessControlNode = xmlCurrentNode;
        
        int xmlUserAccessControlNextChild = xmlFirstChild(xmlConfigFile, xmlUserAccessControlNode);
        
        while (xmlUserAccessControlNextChild != -1)
        {
          switch (xmlNodeName(xmlConfigFile, xmlUserAccessControlNextChild))
          {
            case XML_ELEMENT_USER_ACCESS_CONTROL_LDAP:
            {        
              _unApplicationConfiguration_logMessage("     LDAP:");
              
              dyn_anytype daUserAccessControlLdap;        
              bool bUseLdap;
              
              xmlGetElementAttribute(xmlConfigFile, xmlUserAccessControlNextChild, XML_USER_ACCESS_CONTROL_LDAP_USE, bUseLdap);
              
              _unApplicationConfiguration_logMessage("       Use: " + bUseLdap);
              
              daUserAccessControlLdap[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_LDAP_USE] = bUseLdap;
              daUserAccessControl[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_LDAP] = daUserAccessControlLdap;
              
              break;
            }
            case XML_ELEMENT_USER_ACCESS_CONTROL_CENTRAL_SERVER:
            {
              _unApplicationConfiguration_logMessage("     Central server:");
              
              dyn_anytype daUserAccessControlCentralServer;
              string sCentralServerName;
              
              xmlGetElementAttribute(xmlConfigFile, xmlUserAccessControlNextChild, XML_ELEMENT_USER_ACCESS_CONTROL_CENTRAL_SERVER_NAME, sCentralServerName);
              
              _unApplicationConfiguration_logMessage("       Name: " + sCentralServerName);
              
              daUserAccessControlCentralServer[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_CENTRAL_SERVER_NAME] = sCentralServerName; 
              daUserAccessControl[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_CENTRAL_SERVER] = daUserAccessControlCentralServer;   
              
              break;
            }
            case XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST:
            {
              _unApplicationConfiguration_logMessage("     Domains:");

              dyn_dyn_anytype ddaDomainList;
              int xmlDomain = xmlFirstChild(xmlConfigFile, xmlUserAccessControlNextChild);
              while (xmlDomain != -1)
              {
                dyn_anytype daDomain;
                
                string sDomainName;
                string sDomainFullName;
                string sDomainComment;
                xmlGetElementAttribute(xmlConfigFile, xmlDomain, XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_NAME, sDomainName);
                xmlGetElementAttribute(xmlConfigFile, xmlDomain, XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_FULL_NAME, sDomainFullName);
                xmlGetElementAttribute(xmlConfigFile, xmlDomain, XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_COMMENT, sDomainComment);
                            
                _unApplicationConfiguration_logMessage("       Domain: " + sDomainName);
                
                dyn_string dsPrivileges;
                int xmlDomainPrivilege = xmlFirstChild(xmlConfigFile, xmlDomain);
                while (xmlDomainPrivilege != -1 && (xmlNodeName(xmlConfigFile, xmlDomainPrivilege) == XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_PRIVILEGE))
                {
                  string sPrivilege;
                  xmlGetElementAttribute(xmlConfigFile, xmlDomainPrivilege, XML_ELEMENT_USER_ACCESS_CONTROL_DOMAIN_LIST_DOMAIN_PRIVILEGE_NAME, sPrivilege);
                  dynAppend(dsPrivileges, sPrivilege);
                  
                  xmlDomainPrivilege = xmlNextSibling(xmlConfigFile, xmlDomainPrivilege);  
                }
                
                daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_NAME] = sDomainName;
                daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_FULL_NAME] = sDomainFullName;
                daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_COMMENT] = sDomainComment;
                daDomain[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_PRIVILEGES] = dsPrivileges;
                
                dynAppend(ddaDomainList, daDomain);
                
                xmlDomain = xmlNextSibling(xmlConfigFile, xmlDomain);             
              }
              
              daUserAccessControl[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_DOMAIN_LIST] = ddaDomainList;
              break;
            }
            case XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST:
            {
              _unApplicationConfiguration_logMessage("     Groups:");

              dyn_dyn_anytype ddaGroupList;
              int xmlGroup = xmlFirstChild(xmlConfigFile, xmlUserAccessControlNextChild);
              while (xmlGroup != -1)
              {
                dyn_anytype daGroup;
                
                string sGroupName;
                string sGroupFullName;
                string sGroupDescription;
                xmlGetElementAttribute(xmlConfigFile, xmlGroup, XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_NAME, sGroupName);
                xmlGetElementAttribute(xmlConfigFile, xmlGroup, XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_FULL_NAME, sGroupFullName);
                xmlGetElementAttribute(xmlConfigFile, xmlGroup, XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_DESCRIPTION, sGroupDescription);
                            
                _unApplicationConfiguration_logMessage("       Group: " + sGroupName);
                
                dyn_string dsPrivileges;
                int xmlGroupPrivilege = xmlFirstChild(xmlConfigFile, xmlGroup);
                while (xmlGroupPrivilege != -1 && (xmlNodeName(xmlConfigFile, xmlGroupPrivilege) == XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_PRIVILEGE))
                {
                  string sDomain;
                  string sPrivilege;
                  xmlGetElementAttribute(xmlConfigFile, xmlGroupPrivilege, XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_PRIVILEGE_DOMAIN, sDomain);
                  xmlGetElementAttribute(xmlConfigFile, xmlGroupPrivilege, XML_ELEMENT_USER_ACCESS_CONTROL_GROUP_LIST_GROUP_PRIVILEGE_PRIVILEGE, sPrivilege);
                  
                  dynAppend(dsPrivileges, sDomain + UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_PRIVILEGE_SEPARATOR + sPrivilege);
                  
                  _unApplicationConfiguration_logMessage("       Privilege: " + sDomain + UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_PRIVILEGE_SEPARATOR + sPrivilege);
                  xmlGroupPrivilege = xmlNextSibling(xmlConfigFile, xmlGroupPrivilege);  
                }
                
                daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_NAME] = sGroupName;
                daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_FULL_NAME] = sGroupFullName;
                daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_DESCRIPTION] = sGroupDescription;
                daGroup[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_PRIVILEGES] = dsPrivileges;
                
                dynAppend(ddaGroupList, daGroup);
                
                xmlGroup = xmlNextSibling(xmlConfigFile, xmlGroup);             
              }
              
              daUserAccessControl[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_GROUP_LIST] = ddaGroupList;
              break;
            }
            case XML_ELEMENT_USER_ACCESS_CONTROL_EGROUP_MAPPINGS:
            {
              _unApplicationConfiguration_logMessage("     E-groups mapping:");
              dyn_dyn_string ddsMappingList;          
              
              int xmlMapping = xmlFirstChild(xmlConfigFile, xmlUserAccessControlNextChild);          
              while (xmlMapping != -1)
              {
                string sEgroupName;
                string sGroupName;
                dyn_string dsMapping;
                xmlGetElementAttribute(xmlConfigFile, xmlMapping, XML_ELEMENT_USER_ACCESS_CONTROL_EGROUP_MAPPINGS_MAPPING_GROUP_NAME, sGroupName);
                xmlGetElementAttribute(xmlConfigFile, xmlMapping, XML_ELEMENT_USER_ACCESS_CONTROL_EGROUP_MAPPINGS_MAPPING_EGROUP_NAME, sEgroupName);
                
                _unApplicationConfiguration_logMessage("       Group: " + sGroupName + " - e-group: " + sEgroupName);
                  
                dsMapping[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING_GROUP] = sGroupName;
                dsMapping[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING_EGROUP] = sEgroupName;
                
                
                dynAppend(ddsMappingList, dsMapping);
                
                xmlMapping = xmlNextSibling(xmlConfigFile, xmlMapping);   
              }         
              
              daUserAccessControl[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL_EGROUP_MAPPING] = ddsMappingList;
              break;
            }
          }      
          
          xmlUserAccessControlNextChild = xmlNextSibling(xmlConfigFile, xmlUserAccessControlNextChild);  
        }
        
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_USER_ACCESS_CONTROL] = daUserAccessControl;   
      
        break;
      }
      case XML_ELEMENT_EMAIL_CONFIGURATIONS:
      {
      
        // -------------------------------------------------- 7) Email configurations
      
        _unApplicationConfiguration_logMessage("   Reading alert email configuration...");  
        dyn_dyn_anytype ddaEmailConfigs;
        int xmlEmailConfig = xmlFirstChild(xmlConfigFile, xmlCurrentNode); 
        
        while (xmlEmailConfig != -1)
        {
          dynClear(daConfig);
          
          string sSystemName;
          string sConfigName;
          int iMaxPerDay;
          string sHTMLLink;
          string sSenderAddress;
          string sDomain;
          string sSMTPServer;         
          dyn_string dsReceivers;
          
          xmlGetElementAttribute(xmlConfigFile, xmlEmailConfig, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_SYSTEM, sSystemName);
          xmlGetElementAttribute(xmlConfigFile, xmlEmailConfig, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_NAME, sConfigName);
          xmlGetElementAttribute(xmlConfigFile, xmlEmailConfig, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_MAX_PER_DAY, iMaxPerDay);
          xmlGetElementAttribute(xmlConfigFile, xmlEmailConfig, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_HTML_LINK, sHTMLLink);
          xmlGetElementAttribute(xmlConfigFile, xmlEmailConfig, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_SENDER_ADDRESS, sSenderAddress);
          xmlGetElementAttribute(xmlConfigFile, xmlEmailConfig, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_DOMAIN, sDomain);
          xmlGetElementAttribute(xmlConfigFile, xmlEmailConfig, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_SMTP_SERVER, sSMTPServer);
          
          _unApplicationConfiguration_logMessage("     Config: " + sConfigName);
          
          if ("" == sSystemName)
          {
            sSystemName = getSystemName();
          }
          
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SYSTEM] = sSystemName;
          
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_NAME] = sConfigName;
          
          if (0 == iMaxPerDay)
          {
            iMaxPerDay = 250;
          }        
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_MAX_PER_DAY] = iMaxPerDay;
          
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_HTML_LINK] = sHTMLLink;
          
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SENDER_ADDRESS] = sSenderAddress;
          
          if ("" == sDomain)
          {
            sDomain = "cern.ch";
          }
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_DOMAIN] = sDomain;
          
          if ("" == sSMTPServer)
          {
            sSMTPServer = "cernmx.cern.ch";
          }
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_SMTP_SERVER] = sSMTPServer;
          
          int xmlEmailConfigReceiver = xmlFirstChild(xmlConfigFile, xmlEmailConfig);
          while (xmlEmailConfigReceiver != -1)
          {
            string sReceiverAddress;
            xmlGetElementAttribute(xmlConfigFile, xmlEmailConfigReceiver, XML_ELEMENT_EMAIL_CONFIGURATIONS_EMAIL_CONFIGURATION_RECEIVER_ADDRESS, sReceiverAddress);
            
            dynAppend(dsReceivers, sReceiverAddress);
            
            xmlEmailConfigReceiver = xmlNextSibling(xmlConfigFile, xmlEmailConfigReceiver); 
          }
        
          daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS_CONFIG_RECEIVER_LIST] = dsReceivers;
          
          dynAppend(ddaEmailConfigs, daConfig);
          
          xmlEmailConfig = xmlNextSibling(xmlConfigFile, xmlEmailConfig); 
        }
        
        
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_EMAIL_CONFIGURATIONS] = ddaEmailConfigs;
        break;
      }
      case XML_ELEMENT_AUTOLOGOUT:
      {
      
        // -------------------------------------------------- 8) Autologout   
        _unApplicationConfiguration_logMessage("   Reading autologout configuration...");       
        int xmlAutologoutNode = xmlCurrentNode;        
        int xmlWarningTimeoutNode = xmlFirstChild(xmlConfigFile, xmlAutologoutNode);
        int xmlCommitTimeoutNode = xmlNextSibling(xmlConfigFile, xmlWarningTimeoutNode);
        
        int iWarningHours;
        int iWarningMinutes;
        int iCommitHours;
        int iCommitMinutes;
        
        xmlGetElementAttribute(xmlConfigFile, xmlWarningTimeoutNode, XML_ELEMENT_AUTOLOGOUT_WARNING_TIMEOUT_HOURS, iWarningHours);
        xmlGetElementAttribute(xmlConfigFile, xmlWarningTimeoutNode, XML_ELEMENT_AUTOLOGOUT_WARNING_TIMEOUT_MINUTES, iWarningMinutes);
        xmlGetElementAttribute(xmlConfigFile, xmlCommitTimeoutNode, XML_ELEMENT_AUTOLOGOUT_COMMIT_TIMEOUT_HOURS, iCommitHours);
        xmlGetElementAttribute(xmlConfigFile, xmlCommitTimeoutNode, XML_ELEMENT_AUTOLOGOUT_COMMIT_TIMEOUT_MINUTES, iCommitMinutes);
          
        _unApplicationConfiguration_logMessage("     Warning: " + iWarningHours + " hours, " + iWarningMinutes  + " minutes.");
        _unApplicationConfiguration_logMessage("     Commit: "  + iCommitHours  + " hours, " + iCommitMinutes   + " minutes.");
        
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_WARNING_HOURS] = iWarningHours;
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_WARNING_MINUTES] = iWarningMinutes;
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_COMMIT_HOURS] = iCommitHours;
        daConfig[UN_APPLICATION_CONFIGURATION_INDEX_AUTOLOGOUT][UN_APPLICATION_CONFIGURATION_INDEX_AUTO_LOGOUT_COMMIT_MINUTES] = iCommitMinutes;
        break;
      }
    }
    
    xmlCurrentNode = xmlNextSibling(xmlConfigFile, xmlCurrentNode);
  }
}
