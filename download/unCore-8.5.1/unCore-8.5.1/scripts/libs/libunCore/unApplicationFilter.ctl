/**
@file unApplicationFilter.ctl
Functions used to manage an application filter. 
An application filter is a list of front-end applications the user is interested in. This means any other front-end application is ignored and not displayed anywhere.

Two variables from this library will mainly be used by the developer : 
 - g_unicosHMI_bApplicationFilterInUse: indicates wether a filter has been set at startup. If not, the application behaves as it originally used to.
 - g_unicosHMI_dsApplicationList: list of the applications matching the filter set at startup. 
   Note that it can contain not-connected applications (only if the remote system has been connected at least once, else there is no way to see them).
   You can then intersect with g_m_dsTreeApplication for instance, which contains only connected applications, to ignore the not-connected ones.
   
Other variables are internal, but you can use some like g_unicosHMI_sApplicationDpName which is the pattern for the name of the DP to create.

It is possible to register a callback to be triggered when the filter is likely to change (e.g. a new system is connected or application DPs are added) with the function unApplicationFilter_registerAppListChangedCB.
The callback function must have a bool as parameter. 
This bool's values mean:
 - True: the DPs have changed on one of the system. For this value, the system internally updates the filter. You will most likely have to ignore this value.
 - False: the list has been updated. If you need to, you can now update the displayed elements.



@author Cyril Caillaba (EN-ICE-SCD)
@version 1.0.0
@date April 2012
*/



//@{



// ---------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------- Variables and functions for filter management -----------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------


// List of the applications to be supervised
global dyn_string g_unicosHMI_dsApplicationList;

// Keep a raw version of the filter, to update the real time filter when needed
// This variable must be initialized alone in the unicosHMI panel 
global dyn_string g_unicosHMI_dsRawApplicationFilter;

// To initialize the previous variable, we split a string according to a delimiter
global const string UN_APPLICATION_LIST_DELIMITER = ";";

// Keep the information of there is a filter in use or not
global bool g_unicosHMI_bApplicationFilterInUse;

// Memorize a default application for when loading parameters such as default panel
global string g_unicosHMI_sDefaultApplication;

// For a particuler alarm filtering, we need to store the filters if they contain the character '*' and at least one other character
global dyn_string g_unicosHMI_dsMultiAlarmFilter;

// Name to give to a local application data point (add the application name)
global const string g_unicosHMI_sApplicationDpName = "_unApplication_";

// Type of this dp
global const string g_unicosHMI_sApplicationDpType = "_UnApplication";

// Dpe flag to signal a change in the application list
global const string g_unicosHMI_sApplicationListChangedDpe = "applicationListChanged";

// State of the system connections
// Key: system
// Value: true => dp connected, false => dp not connected
// Note: local system is always connected so its value is irrelevant
global mapping g_unicosHMI_mSystemConnected;

// This variable will be used to filter window/trend tree: anything contained in this variable is hidden from the tree display
global dyn_string g_unicosHMI_dsTreeHiddenNames;

// Keep in memory the name of this filter (i.e. the name of the file)
global string g_unicosHMI_sApplicationFilterName;


//unApplicationFilter_initFilterData
/**
  @par Description:
  Initialize the filter according to a list of application passed as a string separated by ";".
  To be called once at the very beginning of the application.
  Note that this function will NOT set the needed callback. It should be call at the very start of the UNICOS application to be sure variables are ready.
  Then unApplicationFilter_initFilterCallback() must be called.
  The panel initializing the entire application is executed as a separated module and any callback set at this moment would be lost.
  Therefore it is not possible to set the callback at the same time the application is initialized.
  
  @par Usage: 
  Public
  
  @param sApplicationList input, list of application as a string separated by ";"
*/
unApplicationFilter_initFilterData(string sApplicationList, string sDefaultApplication, string sFilterName) synchronized(g_unicosHMI_dsApplicationList)
{
  g_unicosHMI_dsRawApplicationFilter = strsplit(sApplicationList, UN_APPLICATION_LIST_DELIMITER);
  g_unicosHMI_sDefaultApplication = sDefaultApplication;
  
  // Empty list ?
  if (dynlen(g_unicosHMI_dsRawApplicationFilter) < 1)
  {
    // Ignore, leave global variable empty  
    g_unicosHMI_bApplicationFilterInUse = false;
    dynClear(g_unicosHMI_dsApplicationList);
    dynClear(g_unicosHMI_dsMultiAlarmFilter);    
    
    return;
  }
  else
  {
    // Check every element of the filter exists
    for (int i = 1 ; i <= dynlen(g_unicosHMI_dsRawApplicationFilter) ; i++)
    {
      // If one of the filters is "*", it cancels all the others.
      // This step is necessary instead of simply adding all the applications to the filter
      // For instance, without it it would not be possible to select all applications when filtering in the alarms panel
      if ("*" == g_unicosHMI_dsRawApplicationFilter[i])
      {
        // Clear filters
        dynClear(g_unicosHMI_dsApplicationList);
        dynClear(g_unicosHMI_dsMultiAlarmFilter);
        g_unicosHMI_bApplicationFilterInUse = false;
          
        // Leave the function
        return;
      }        
      
      // Only for alarm panel, we need to save filter with a wildcard "*" and at least one other character      
      if (strpos(g_unicosHMI_dsRawApplicationFilter[i], "*") >= 0)
      {
        // Not possible with regular expressions, so we need to check every character to find one different from '*'
        bool bCharFound = false;
        for (i = 0 ; i < strlen(g_unicosHMI_dsRawApplicationFilter[1]) ; i++)
        {
          if ("*" != g_unicosHMI_dsRawApplicationFilter[1][i])
          {
            bCharFound = true;
          }
        }
        
        if (bCharFound)
        {
          dynAppend(g_unicosHMI_dsMultiAlarmFilter, g_unicosHMI_dsRawApplicationFilter[i]);
        }
      }  
                        
      
      // Get all the DPs matching this pattern
      
      // On every system
      dyn_string dsSystemNames;
      dyn_int diSystemIds;
      dyn_string dsHostNames;
      dyn_int diPortNumbers;

      unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);
      
      dyn_string dsDpNamesResult;
      for (int j = 1 ; j <= dynlen(dsSystemNames) ; j++)
      {
        dyn_string dsNamesTemp = dpNames(dsSystemNames[j] + ":" + g_unicosHMI_sApplicationDpName + g_unicosHMI_dsRawApplicationFilter[i], g_unicosHMI_sApplicationDpType);
        dynAppend(dsDpNamesResult, dsNamesTemp);        
      }

      
      dynUnique(dsDpNamesResult);

      // Extract the application name from the DP name and save them
      for (int j = 1 ; j <= dynlen(dsDpNamesResult) ; j++)
      {
        dynAppend(g_unicosHMI_dsApplicationList, substr(dsDpNamesResult[j], strpos(dsDpNamesResult[j], g_unicosHMI_sApplicationDpName) + strlen(g_unicosHMI_sApplicationDpName)));
      }     
    }
    
    // Remove duplicated apps
    dynUnique(g_unicosHMI_dsApplicationList);
    
    _unApplicationFilter_updateHiddenTreeElements();
        
    g_unicosHMI_sApplicationFilterName = sFilterName;
    
    // Filter is ready
    g_unicosHMI_bApplicationFilterInUse = true;
  }    
}


//unApplicationFilter_initFilterCallback
/**
  @par Description:
  Initialize the filter according to a list of application passed as a string separated by ";".
  To be called once at the very beginning of the application.
  
  @par Usage: 
  Public
  
*/
unApplicationFilter_initFilterCallback()
{
  if (g_unicosHMI_bApplicationFilterInUse)
  {
    dyn_string dsSystemNames;
    dyn_int diSystemIds;
    dyn_string dsHostNames;
    dyn_int diPortNumbers;
    unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);
    
    dpConnect("_unApplicationFilter_dpListChangedCB", false, getSystemName() + UN_APPLICATION_DPNAME + "." + g_unicosHMI_sApplicationListChangedDpe);
      
    for (int j = 1 ; j <= dynlen(dsSystemNames) ; j++)
    {   
      g_unicosHMI_mSystemConnected[dsSystemNames[j] + ":"] = false;
      bool bRes;
      bool bConnected;
      dyn_string dsExceptions;
      unDistributedControl_register("_unApplicationFilter_systemStateChangedCB", bRes, bConnected, dsSystemNames[j] + ":", dsExceptions);      
    }   
  
    // Note: this must be called after everything is initialized, else the widgets don't exist yet
    _unApplicationFilter_initUserTag();
    
    
    int iRes;
    dyn_string dsExceptions;
    unGenericDpFunctionsHMI_setCallBack_user("_unApplicationFilter_userCB", iRes, dsExceptions);
  }
}

//_unApplicationFilter_initAESProperties
/**
  @par Description:
  Configure AES so that alarm row displays only alarms from wanted applications
  
  @par Usage: Internal
*/
_unApplicationFilter_initAESProperties()
{
  const string sAESPropertiesRowDp = "_AESPropertiesRTRow_" + myManNum() + "_" + myModuleName() + "_Top.Alerts.Filter.DpList";
  dyn_string dsAESFilterDpList = unApplicationFilter_getAESFilterDpList();
  
  dpSet(sAESPropertiesRowDp, dsAESFilterDpList);  
}

//_unApplicationFilter_systemStateChangedCB
/**
  @par Description:
  Function to be called when a system is connected or disconnected
  
  @param sDpe input, the dpe that triggered the callback.
  @param bConnected input, state of the system that triggered the callback.
  
  @par Usage: 
  Internal
  
  @reviewed 2018-06-22 @whitelisted{Callback}
*/
_unApplicationFilter_systemStateChangedCB(string sDpe, bool bConnected)
{    
  string sDp = dpSubStr(sDpe, DPSUB_DP);
  
  // Extract system from dp name
  string sSystemName;
  sSystemName = substr(sDp, strlen(c_unDistributedControl_dpName), strlen(sDp)) + ":";
    
  // Ignore local system
  if (getSystemName() == sSystemName)
  {
    return;
  }
  
  // If the system is not connected we don't need to update the list, since we want to keep in memory all the applications
  // With this, the application list here also contains not connected applications. An intersection with other global variables allows ignoring them.
  if (bConnected)
  {    
    if (!g_unicosHMI_mSystemConnected[sSystemName])
    {
      g_unicosHMI_mSystemConnected[sSystemName] = true;
      dpConnect("_unApplicationFilter_dpListChangedCB", false, sSystemName + UN_APPLICATION_DPNAME + "." + g_unicosHMI_sApplicationListChangedDpe);
    }
    
    // If the system is the local, ignore (it will never be disconnected)
    if (getSystemName() != sSystemName)
    {      
      _unApplicationFilter_updateFilter();     
    
      // The list is up to date with newly connected applications, send information to the concerned UIs
      _unApplicationFilter_signalListChanged();
    }
  }
  else
  {
    if (g_unicosHMI_mSystemConnected[sSystemName])
    {
      g_unicosHMI_mSystemConnected[sSystemName] = false;
      dpDisconnect("_unApplicationFilter_dpListChangedCB", sSystemName + UN_APPLICATION_DPNAME + "." + g_unicosHMI_sApplicationListChangedDpe);
    }
  }
}



//_unApplicationFilter_updateFilter
/**
  @par Description:
  Updates the filter regarding to the original stored value. 
  This function needs to be called when there is any change, such as new system connected or new application dp added.
    
  @par Usage: 
  Internal
  
*/
_unApplicationFilter_updateFilter() synchronized(g_unicosHMI_dsApplicationList)
{
  dynClear(g_unicosHMI_dsApplicationList);
  for (int i = 1 ; i <= dynlen(g_unicosHMI_dsRawApplicationFilter) ; i++)
  {
    // We need to check on every system
    dyn_string dsSystemNames;
    dyn_int diSystemIds;
    dyn_string dsHostNames;
    dyn_int diPortNumbers;

    unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);
      
    dyn_string dsDpNamesResult;
    for (int j = 1 ; j <= dynlen(dsSystemNames) ; j++)
    {
      dyn_string dsNamesTemp = dpNames(dsSystemNames[j] + ":" + g_unicosHMI_sApplicationDpName + g_unicosHMI_dsRawApplicationFilter[i], g_unicosHMI_sApplicationDpType);
      dynAppend(dsDpNamesResult, dsNamesTemp); 
    }

      
    dynUnique(dsDpNamesResult);   
    
    // Extract the application name from the DP name
    for (int j = 1 ; j <= dynlen(dsDpNamesResult) ; j++)
    {
      dynAppend(g_unicosHMI_dsApplicationList, substr(dsDpNamesResult[j], strpos(dsDpNamesResult[j], g_unicosHMI_sApplicationDpName) + strlen(g_unicosHMI_sApplicationDpName)));
    }
    
    dynUnique(g_unicosHMI_dsApplicationList);
    
    _unApplicationFilter_updateHiddenTreeElements();
  }
}


//_unApplicationFilter_dpListChangedCB
/**
  @par Description:
  Function to be called when the list of applications on any system changes. 
  If the value that triggered the callback is "true", we update the filter.
  Once the filter has been properly updated, the DPE that triggered the callback is set to false. 
  This tells to the visible panels they have to update their display.
  
  If the value that triggered is false we ignore the callback, it concerns only the UIs.
  
  @param sDpe input, the dpe that triggered the callback.
  @param bListChanged input, will always be true, value irrelevant

  @par Usage: 
  Internal
  
  @reviewed 2018-06-22 @whitelisted{Callback}
*/
_unApplicationFilter_dpListChangedCB(string sDpe, bool bListChanged)
{
  if (bListChanged)
  {
    dyn_string dsListCopy = g_unicosHMI_dsApplicationList;
    _unApplicationFilter_updateFilter();

    // Check if the list has changed to avoid updating UIs for nothing    
    bool bListUnchanged = true;
    if (dynlen(dsListCopy) == dynlen(g_unicosHMI_dsApplicationList))
    {
      for (int i = 1 ; i <= dynlen(g_unicosHMI_dsApplicationList) ; i++)
      {
        // Check if all element of the new list are in the old one
        // Since they are the same size there is nothing more to check
        if (dynContains(dsListCopy, g_unicosHMI_dsApplicationList[i]) < 1)
        {
          // This element was not in the list
          bListUnchanged = false;
          
          // Leave the loop
          break;
        }        
      }
    }
    else
    {
      bListUnchanged = false;
    }
    if (!bListUnchanged)
    {
      _unApplicationFilter_signalListChanged();
      unApplicationFilter_updateUserTagWidget();
    }
  }    
}

//_unApplicationFilter_signalListChanged
/**
  @par Description:
  Set the DPE to trigger the callbacks waiting for a list change.
  
  @par Usage: 
  Internal
  
*/
_unApplicationFilter_signalListChanged()
{
  dpSet(UN_APPLICATION_DPNAME + "." + g_unicosHMI_sApplicationListChangedDpe, false);
}

//unApplicationFilter_registerAppListChangedCB
/**
  @par Description:
  Registers a callback function to be called when the application list changes on a system
  
  @param sWorkFunction input, the callback function to be executed when the application list changes.
  
  @par Usage: 
  Internal
  
*/
unApplicationFilter_registerAppListChangedCB(string sWorkFunction)
{
  dpConnect(sWorkFunction, false, getSystemName() + UN_APPLICATION_DPNAME + "." + g_unicosHMI_sApplicationListChangedDpe);     
}

//unApplicationFilter_isFrontEndVisible
/**
  @par Description:
  Helper function to test if a front-end is used by any of the filter applications
  
  @param sFrontEnd input, the front-end to check
  @return true if the front-end is used by a filtered applications, false otherwise (or if the front-end is not found)
  
  @par Usage:
  Public
*/
bool unApplicationFilter_isFrontEndVisible(string sFrontEnd)
{
  // Warning
  // If a front-end is on two systems, it might be not visible in the first system but visible on the second.
  // This is why it is needed to check on all systems every time, else the check is done only on one system
  dyn_string dsSystemNames;
  dyn_int diSystemIds;
  dyn_string dsHostNames;
  dyn_int diPortNumbers;

  unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);
      
  dyn_string dsDpNamesResult;
  for (int j = 1 ; j <= dynlen(dsSystemNames) ; j++)
  {
    // The DP might not exist
    if (dpExists(dsSystemNames[j] + ":" + sFrontEnd + ".configuration.subApplications"))
    {
      dyn_string dsFrontEndSubApplications;
      dpGet(dsSystemNames[j] + ":" + sFrontEnd + ".configuration.subApplications", dsFrontEndSubApplications);
      
      if (dynlen(dynIntersect(dsFrontEndSubApplications, g_unicosHMI_dsApplicationList)) > 0)
      {
        return true;
      }  
    }
    else
    {
      // If it doesn't, maybe an alias was given    
      string sFrontEndDp = _unGenericDpFunctions_dpAliasToName(dsSystemNames[j] + ":" + sFrontEnd);
      
      if (dpExists(sFrontEndDp + "configuration.subApplications"))
      {
        dyn_string dsFrontEndSubApplications;
        dpGet(sFrontEndDp + "configuration.subApplications", dsFrontEndSubApplications);
        if (dynlen(dynIntersect(dsFrontEndSubApplications, g_unicosHMI_dsApplicationList)) > 0)
        {
          return true;
        }     
      }   
    }
  }
  
  return false;
}

//unApplicationFilter_getAESFilterDpList
/**
  @par Description:
  Helper function to test if a front-end is used by any of the filter applications
  
  @return a list of DP to filter by AES  
  
  @par Usage:
  Public
*/
dyn_string unApplicationFilter_getAESFilterDpList()
{
  dyn_string dsAESFilterDpList; 
  
  // On every system
  dyn_string dsSystemNames;
  dyn_int diSystemIds;
  dyn_string dsHostNames;
  dyn_int diPortNumbers;
  
  for (int i = 1 ; i <= dynlen(g_unicosHMI_dsApplicationList) ; i++)
  {
    dynAppend(dsAESFilterDpList, "*-*-" + g_unicosHMI_dsApplicationList[i] + "-*-*");
  }  
  
  // If no applications are currently visible, mask everything by using a non-valid DP filter (i.e. which will never return any result)
  if (dynlen(dsAESFilterDpList) < 1)
  {
    dynAppend(dsAESFilterDpList, "NOT-VALID-DP");
  }
  
  return dsAESFilterDpList;
}


//unApplicationFilter_getVisibleApplicationDpList
/**
  @par Description:
  Get the DPs of all visible applications.
  
  @return the list of DPs or an empty list if no filter is used
  
  @par Usage:
  Public
  
*/
dyn_string unApplicationFilter_getVisibleApplicationDpList()
{
  dyn_string dsDpList = makeDynString();

  if (g_unicosHMI_bApplicationFilterInUse)
  {
    dyn_string dsSystemNames;
    dyn_int diSystemIds;
    dyn_string dsHostNames;
    dyn_int diPortNumbers;

    unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);
      
    for (int j = 1 ; j <= dynlen(dsSystemNames) ; j++)
    {
      bool bSystemIsConnected;
      unDistributedControl_isConnected(bSystemIsConnected, dsSystemNames[j] + ":");
      
      if (bSystemIsConnected)
      {
        for (int i = 1 ; i <= dynlen(g_unicosHMI_dsApplicationList) ; i++)
        {
          if (dpExists(dsSystemNames[j] + ":" + g_unicosHMI_sApplicationDpName + g_unicosHMI_dsApplicationList[i]))
          {   
            dynAppend(dsDpList, dsSystemNames[j] + ":" + g_unicosHMI_sApplicationDpName + g_unicosHMI_dsApplicationList[i]);
          }
        }
      }
    } 
  }
  
  dynUnique(dsDpList);
  
  return dsDpList;
}

//unApplicationFilter_getDefaultApplicationDp
/**
  @par Description:
  Get the DP of the application set as default. If several application are marked as default, the first encountered will be 
  
  @return the default application. If there is none, the dp "_unApplication" on the local system is returned.
  
  @par Usage:
  Public
*/
string unApplicationFilter_getDefaultApplicationDp() synchronized(g_unicosHMI_dsApplicationList)
{
  dyn_string dsApplicationList = unApplicationFilter_getVisibleApplicationDpList();
  
  for (int i = 1 ; i <= dynlen(dsApplicationList) ; i++)
  {
    const string sApplicationName = substr(dsApplicationList[i], strpos(dsApplicationList[i], g_unicosHMI_sApplicationDpName) + strlen(g_unicosHMI_sApplicationDpName));
    
    if ((sApplicationName == g_unicosHMI_sDefaultApplication) && (dynContains(g_unicosHMI_dsApplicationList, sApplicationName) > 0))
    {
      return dsApplicationList[i];
    }
  }
  
  return getSystemName() + UN_APPLICATION_DPNAME;
}

//_unApplicationFilter_updateHiddenTreeElements
/**
  @par Description:
  Keeps an internal list of extra-elements to hide in the window and trend tree
  
  @par Usage:
  Internal
  
*/
_unApplicationFilter_updateHiddenTreeElements() synchronized(g_unicosHMI_dsTreeHiddenNames)
{
  dynClear(g_unicosHMI_dsTreeHiddenNames);
  
  
  dyn_string dsSystemNames;
  dyn_int diSystemIds;
  dyn_string dsHostNames;
  dyn_int diPortNumbers;
  unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);
      
  dyn_string dsDpNamesResult;
  for (int j = 1 ; j <= dynlen(dsSystemNames) ; j++)
  {
    dyn_string dsNamesTemp = dpNames(dsSystemNames[j] + ":" + g_unicosHMI_sApplicationDpName + "*", g_unicosHMI_sApplicationDpType);
    dynAppend(dsDpNamesResult, dsNamesTemp);        
  }
  
  for (int i = 1 ; i <= dynlen(dsDpNamesResult) ; i++)
  {
    string sAppName = substr(
        dsDpNamesResult[i],
        strpos(dsDpNamesResult[i], g_unicosHMI_sApplicationDpName) + strlen(g_unicosHMI_sApplicationDpName)
      );
    
    if (dynContains(g_unicosHMI_dsApplicationList, sAppName) < 1)
    {
      dynAppend(g_unicosHMI_dsTreeHiddenNames, sAppName);
    }
  }
    
  dynUnique(g_unicosHMI_dsTreeHiddenNames); 
}












// -----------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------- Variables and functions for user tag management -----------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------

// The character separating the tag content from the validity limit
global const string g_unicosHMI_sApplicationFilterUserTagDelimiter = "~";

// The DPE containing messages
global const string g_unicosHMI_sMessageListDpe = "userTags";

global const int g_unicosHMI_ApplicationFilterTagIndex_Date        = 1;
global const int g_unicosHMI_ApplicationFilterTagIndex_Expiration  = 2;
global const int g_unicosHMI_ApplicationFilterTagIndex_Username    = 3;
global const int g_unicosHMI_ApplicationFilterTagIndex_Group       = 4;
global const int g_unicosHMI_ApplicationFilterTagIndex_Tag         = 5;
global const int g_unicosHMI_ApplicationFilterTagIndex_Level       = 6;
global const int g_unicosHMI_ApplicationFilterTagIndex_Application = 7;

// Widget displaying the tag
const string sApplicationFilterUserTagInfoWidget     = "unApplicationFilter_userTagInfoWidget";
const string sApplicationFilterUserTagWarningWidget  = "unApplicationFilter_userTagWarningWidget";
const string sApplicationFilterUserTagCriticalWidget = "unApplicationFilter_userTagCriticalWidget";


// Possible message levels
const string g_unicosHMI_ApplicationFilterMessageInformation = "information";
const string g_unicosHMI_ApplicationFilterMessageWarning     = "warning";
const string g_unicosHMI_ApplicationFilterMessageCritical    = "critical";
const string g_unicosHMI_ApplicationFilterMessageAll         = "all";


/** _unApplicationFilter_userCB
  *
  * @reviewed 2018-06-22 @whitelisted{Callback}
  */
_unApplicationFilter_userCB(string sUi, string sUser)
{
  unApplicationFilter_updateUserTagWidget();
}

//_unApplicationFilter_initUserTag
/**
  @par Description: 
  If there is one, init the user tag for the current filter and display it.
  
  
  @par Usage:
  Internal
*/  
_unApplicationFilter_initUserTag()
{  
  // Start infinite loop in thread to check tag expiration
  startThread("_unApplicationFilter_tagCheckLoop");
}

//unApplicationFilter_setFilterTagInformation
/**
  @par Description:
  Saves the user tag information to the given DP. Can also erase a tag if parameter bDelete is set to true.
  
  @param tValidUntil time input, the timestamp (seconds since 1.1.1970) until which this tag is valid
  @param dsGroups dyn_string input, the groups for which this message is visible
  @param sLevel string input, information warning or critical
  @param sApplicationDp string input, the application datapoint to which add this tag  
  @param sTag string input, the message to display
  @param sSystem string input, the system on which the message must be written
  @param bDelete boolean input, if set to true the message with the matching previous parameters is removed
  
  @par Usage:
  Public  
*/
// unApplicationFilter_setFilterTagInformation(string sFilterName, time tValidUntil, string sUserTag)
unApplicationFilter_setFilterTagInformation(time tValidUntil, dyn_string dsGroups, string sLevel, string sApplicationDp, string sTag, bool bDelete = false)
{
  dyn_string dsMessage;
  
  // 1: modification time
  dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Date] = period(getCurrentTime());
  
  // 2: expiration time
  dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Expiration] = period(tValidUntil);
  
  // 3: user name  
  string sUserName;
  fwAccessControl_getUserName(sUserName);
  
  string sUserFullName;
  string sDescription;
  int iUserId;
  bool bEnabled;
  dyn_string dsGroupNames;
  dyn_string dsExceptionsInfo;
  
  fwAccessControl_getUser(sUserName, sUserFullName, sDescription, iUserId, bEnabled, dsGroupNames, dsExceptionsInfo);
  strreplace(sUserFullName, g_unicosHMI_sApplicationFilterUserTagDelimiter, "");
    
  // dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Username] = sUserFullName + " (" + sUserName + ")";
  dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Username] = sUserName;
  
  // 4: group
  // dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Group] = sGroup;
  
  // 5: message
  string sMessage = sTag;
  strreplace(sMessage, g_unicosHMI_sApplicationFilterUserTagDelimiter, "");
  dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Tag] = sMessage;
  
  // 6: level
  dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Level] = sLevel;
  
  // 7: application
  string sApplication = substr(sApplicationDp, strpos(sApplicationDp, g_unicosHMI_sApplicationDpName) + strlen(g_unicosHMI_sApplicationDpName));
  dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Application] = sApplication;
    
  if (dpExists(sApplicationDp + "." + g_unicosHMI_sMessageListDpe))
  {
    dyn_string dsUserTagInfo;
    dpGet(sApplicationDp + "." + g_unicosHMI_sMessageListDpe, dsUserTagInfo);
    
    for (int j = 1 ; j <= dynlen(dsGroups) ; j++)
    {      
      dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Group] = dsGroups[j];
      if (bDelete)
      {
        for (int i = 1 ; i <= dynlen(dsUserTagInfo) ; i++)
        {
          dyn_string dsUserTagSplit = strsplit(dsUserTagInfo[i], g_unicosHMI_sApplicationFilterUserTagDelimiter);
          if (dynlen(dsUserTagSplit) < g_unicosHMI_ApplicationFilterTagIndex_Application)
          {
            continue;
          }
          
          if (
            (dsUserTagSplit[g_unicosHMI_ApplicationFilterTagIndex_Expiration]  == dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Expiration]) &&
            (dsUserTagSplit[g_unicosHMI_ApplicationFilterTagIndex_Group]       == dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Group]) &&
            (dsUserTagSplit[g_unicosHMI_ApplicationFilterTagIndex_Tag]         == dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Tag]) &&
            (dsUserTagSplit[g_unicosHMI_ApplicationFilterTagIndex_Level]       == dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Level]) &&
            (dsUserTagSplit[g_unicosHMI_ApplicationFilterTagIndex_Application] == dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Application])
          )
          {
            dynRemove(dsUserTagInfo, i);
          }    
        }
      
        // Save the modified list
        dpSet(sApplicationDp + "." + g_unicosHMI_sMessageListDpe, dsUserTagInfo);
      }
      else
      {
        sMessage="";
        for (int i = 1 ; i <= dynlen(dsMessage) ; i++)
        {
          sMessage += dsMessage[i] + g_unicosHMI_sApplicationFilterUserTagDelimiter;
        }
      
        dynAppend(dsUserTagInfo, sMessage);
        dpSet(sApplicationDp + "." + g_unicosHMI_sMessageListDpe, dsUserTagInfo);
      }
    }
  }
  else
  {
    return;
  }  
}

//unApplicationFilter_updateUserTag
/**
  @par Description:
  Updates the current tag widget according to the given information (user, validity, date, content). Is the information are not for the current filter, nothing happens.
  
    
  @param dsUserTagInfo dyn_string input, the tag parameters
  
  Usage:
  Public.
*/
unApplicationFilter_updateUserTagWidget() synchronized (g_unicosHMI_dsApplicationList)
{  
  if (!shapeExists("unApplicationFilter_userTagText"))
  {
    return;
  }
  
  mapping mMessages = unApplicationFilter_getAllMessages();
  
  int bThereIsOneInfo = 0;
  int bThereIsOneWarning = 0;
  int bThereIsOneCritical = 0;
  dyn_string dsMessageToShow;
	  
  for (int i = 1 ; i <= mappinglen(mMessages) ; i++)
  {
    dyn_dyn_string ddsMessages = mappingGetValue(mMessages, i);
    for (int j = 1 ; j <= dynlen(ddsMessages) ; j++)
    {
      dyn_string dsMessage = ddsMessages[j];
      // Check if the message has the required number of field, otherwise ignore it
      if (dynlen(dsMessage) < g_unicosHMI_ApplicationFilterTagIndex_Application)
      {
        continue;
      }
      
      // 1: level
      const string sLevel = dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Level];
        
      // 2: application
      // If this application is not meant to be seen, ignore the message
      const string sApplication = dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Application];
   
      if (dynContains(g_unicosHMI_dsApplicationList, sApplication) < 1)
      {
        continue;
      }    
              
      // 3: expiration timestamp
      time tExpiration;
      setPeriod(tExpiration, (int) dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Expiration]);
      if ((int) dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Expiration] < period(getCurrentTime()))
      {
        // Expired, ignore the message
        continue;
      }
      
      // 4: destination group
      string sMessageGroup = dsMessage[g_unicosHMI_ApplicationFilterTagIndex_Group];
      
      string sCurrentUser;
      fwAccessControl_getUserName(sCurrentUser);
      string sUserFullName;
      string sUserDescription;
      int iUserId;
      bool bUserEnabled;
      dyn_string dsUserGroups;
      dyn_string dsExceptions;

      fwAccessControl_getUser(sCurrentUser, sUserFullName, sUserDescription, iUserId,bUserEnabled, dsUserGroups, dsExceptions);
        
      if (dynContains(dsUserGroups, sMessageGroup) < 1)
      {
        // User not in the group concerned by the message, ignore it
        continue;
      }
        
      // All conditions are filled, this message is of interest.
      // Display the icon
      switch(sLevel)
      {
        case g_unicosHMI_ApplicationFilterMessageInformation:
        {
          // If a same message is processed several times (can happen it is on several applications), count it as one
          if (!unApplicationFilter_messagesAreEqual(dsMessageToShow, dsMessage))
          {
            bThereIsOneInfo ++;
  		      dsMessageToShow = dsMessage;
            break;
          }
        }
        case g_unicosHMI_ApplicationFilterMessageWarning:
        {
          if (!unApplicationFilter_messagesAreEqual(dsMessageToShow, dsMessage))
          {
            bThereIsOneWarning ++;
  		      dsMessageToShow = dsMessage;
            break;
          }
        }
        case g_unicosHMI_ApplicationFilterMessageCritical:
        {
          if (!unApplicationFilter_messagesAreEqual(dsMessageToShow, dsMessage))
          {
            bThereIsOneCritical ++;
  		      dsMessageToShow = dsMessage;
            break;
          }
        }
        default:
        {          
          break;
        }
      }
    }
  }
  
	if((bThereIsOneInfo + bThereIsOneWarning + bThereIsOneCritical) == 1)
	{
		 setValue("unApplicationFilter_userTagText", "text", dsMessageToShow[g_unicosHMI_ApplicationFilterTagIndex_Tag]);
		 setValue("unApplicationFilter_userTagText", "visible", true);
	}
	else
	{
		 setValue("unApplicationFilter_userTagText", "visible", false);
	}
  
	
  if (bThereIsOneInfo)
  {
    if (shapeExists(sApplicationFilterUserTagInfoWidget))
    {
      setValue(sApplicationFilterUserTagInfoWidget, "visible", true);
    }

  }
  else
  {
    if (shapeExists(sApplicationFilterUserTagInfoWidget))
    {
      setValue(sApplicationFilterUserTagInfoWidget, "visible", false);
    }
  }
  
  if (bThereIsOneWarning)
  {
    if (shapeExists(sApplicationFilterUserTagWarningWidget))
    {
      setValue(sApplicationFilterUserTagWarningWidget, "visible", true);
    }
  }
  else
  {
    if (shapeExists(sApplicationFilterUserTagWarningWidget))
    {
      setValue(sApplicationFilterUserTagWarningWidget, "visible", false);
    }
  }
  
  if (bThereIsOneCritical)
  {
    if (shapeExists(sApplicationFilterUserTagCriticalWidget))
    {
      setValue(sApplicationFilterUserTagCriticalWidget, "visible", true);
    }
  }
  else
  {
    if (shapeExists(sApplicationFilterUserTagCriticalWidget))
    {
      setValue(sApplicationFilterUserTagCriticalWidget, "visible", false);
    }
  }
}

//unApplicationFilter_updateUserTagCB
/**
  @par Description:
  Callback called when the DP _unApplication.userTags changes. It checks if the DP contains information for the current filter. 
  If it does, the tag is updated according to the data from the DP (user, validity, date, content).
  
  @param sDpe string input, the DPE that triggers the CB
  @param dsUserTags dyn_string input, the value of this DPE
  
  @par Usage:
  Internal.
*/
unApplicationFilter_updateUserTagCB(string sDpe, dyn_string dsUserTags) synchronized(g_unicosHMI_dsApplicationList)
{
  dpSet(getSystemName() + UN_APPLICATION_DPNAME + "." + g_unicosHMI_sMessageListDpe, makeDynString());
  DebugN("unApplicationFilter_updateUserTagCB", sDpe + dsUserTags);
  unApplicationFilter_updateUserTagWidget();  
}

//_unApplicationFilter_tagCheckLoop
/**
  @par Description:
  Infinie loop on a separated thread. Is sleeps for 30 seconds then check if the tag is still valid.
  
  
  @par Usage:
  Internal.
  
  @reviewed 2018-06-22 @whitelisted{Thread}
*/
_unApplicationFilter_tagCheckLoop()
{
  while(1)
  {
    unApplicationFilter_updateUserTagWidget();
    
    // Sleep for 30 seconds before checking again
    delay(30);
  }
}

//unApplicationFilter_getAllMessages
/**
  @par Description:
  Get the user tags of all applications on all system. The filtering must be done later on by the function using this list.
  
  @par Usage:
  Public.
  
  @return mapping. Key: the DPE containing this message, value: all the messages for this DPE (dyn_dyn_string)  
*/
mapping unApplicationFilter_getAllMessages()
{
  mapping mMessages;

  dyn_string dsSystemNames;
  dyn_int diSystemIds;
  dyn_string dsHostNames;
  dyn_int diPortNumbers;
  unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemIds, dsHostNames, diPortNumbers);
      
  for (int k = 1 ; k <= dynlen(dsSystemNames) ; k++)
  {
    const string sSystem = dsSystemNames[k] + ":";    
    dyn_string dsDps = dpNames(sSystem + g_unicosHMI_sApplicationDpName + "*", g_unicosHMI_sApplicationDpType);
  
    for (int i = 1 ; i <= dynlen(dsDps) ; i++)
    {
      dyn_dyn_string ddsAppMessages;
    
      dyn_string dsMessages;
      dpGet(dsDps[i] + "." + g_unicosHMI_sMessageListDpe, dsMessages);
    
      for (int j = 1 ; j <= dynlen(dsMessages) ; j++)
      {
        dyn_string dsMessage = strsplit(dsMessages[j], g_unicosHMI_sApplicationFilterUserTagDelimiter);
        dynAppend(ddsAppMessages, dsMessage);
      }
    
      mMessages[dsDps[i] + "." + g_unicosHMI_sMessageListDpe] = ddsAppMessages;
    }
  }  
  
  return mMessages;
}

//unApplicationFilter_messagesAreEqual
/**
  @par Purpose:
  Compares to message to check if they are identical.
  Identical mean same level, same expiration date, same user and same message content.
  
  @param dsMessage1 dyn_string input, the first message to compare
  @param dsMessage2 dyn_string input, the second message to compare
  
  @par Usage:
  Public.
*/
bool unApplicationFilter_messagesAreEqual(dyn_string dsMessage1, dyn_string dsMessage2)
{
  if ((dynlen(dsMessage1) < g_unicosHMI_ApplicationFilterTagIndex_Application) || (dynlen(dsMessage2) < g_unicosHMI_ApplicationFilterTagIndex_Application))
  {
    return false;
  }
  
  
  if (
    (dsMessage1[g_unicosHMI_ApplicationFilterTagIndex_Level]      == dsMessage2[g_unicosHMI_ApplicationFilterTagIndex_Level])      &&
    (dsMessage1[g_unicosHMI_ApplicationFilterTagIndex_Expiration] == dsMessage2[g_unicosHMI_ApplicationFilterTagIndex_Expiration]) &&
    (dsMessage1[g_unicosHMI_ApplicationFilterTagIndex_Username]   == dsMessage2[g_unicosHMI_ApplicationFilterTagIndex_Username])   &&
    (dsMessage1[g_unicosHMI_ApplicationFilterTagIndex_Tag]        == dsMessage2[g_unicosHMI_ApplicationFilterTagIndex_Tag])
  )
  {
    return true;
  }
  
  return false;
}


//@}
