/** @name LIBRARY: unCoreDeprecated.ctl

Creation date: 2018-06-22

Purpose:
This library contains deprecated functions from unCore libraries, 
that may be eventually removed from component distribution. 

*/

//------------------------------------------------------------------------------------------------------------------------


// _unImportTree_charsOfStringData
/**
Purpose:
Return the individual characters of a data string

	@param data: string, input: the data string

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @deprecated 2018-06-22
*/
string _unImportTree_charsOfStringData(string data)
{
  FWDEPRECATED();
  
char c;
int len;
string buf;
  
    buf = "";
    len = strlen(data);
    
    for ( int pos = 0 ; pos < len ; ++pos )
    {
      c = (char) substr ( data , pos , 1 );
      if ( ( c >= ' ' ) && ( c <= '~' ) )
         buf += " "+c;
      else
        sprintf(buf,"%s 0x%02x",buf,c);
    }
    
    return(buf);
}


//----------------------------
//unBackup_runBackupWait
/**
  Purpose:
  Runs backup on given system and wait until it is over or encounters an error.
  
  @param sSystemName: string, input: name of the system on which to run the backup
  @param dsExceptions: dyn_string, output: list of encountered errors 
  
  @return true if the backup ended successfully, false if it failed
  
  @deprecated 2018-06-22
*/
bool unBackup_runBackupWait(string sSystemName, dyn_string &dsExceptions)
{
  FWDEPRECATED();
  
  // ------------------------------------------------------ 1) Start backup
  time tLastActionTime;
  if (dpExists(sSystemName + "_unApplication.backupConfiguration.command"))
  {
    DebugTN("Backup starting on system \"" + sSystemName + "\"...");
    tLastActionTime = getCurrentTime();
    dpSet(sSystemName + "_unApplication.backupConfiguration.command", UNONLINE_BACKUP_COMMAND_START);
  } 
  else
  {  
    string sError = getCatStr("unBackup","1");
    strreplace(sError,"SYSTEM", sSystemName);
    strreplace(sError,"DP_NAME", "_unApplication.backupConfiguration.command");
        
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "1"
    );
   
    return false;
  }  
  
  
  // ------------------------------------------------------ 2) Wait for first reaction
  if (dpExists(sSystemName + UN_APPLICATION_DPNAME + ".unBackupStatus"))
  {
    dyn_anytype daReturnValue;  
    time tTimeOut = 10;
    bool bTimerExpired;
    dpWaitForValue(
        makeDynString(sSystemName + UN_APPLICATION_DPNAME + ".unBackupStatus:_online.._value"), 
        makeDynAnytype(), 
        makeDynString(sSystemName + UN_APPLICATION_DPNAME + ".unBackupStatus:_online.._value"), 
        daReturnValue,
        tTimeOut,
        bTimerExpired
    );
  
    if (bTimerExpired) // Can happen if somehow the unBackup.ctl script was stopped for instance
    {
      string sError = "Internal error happened on system \"" + sSystemName + "\". Online backup request timed out. Check if script unBackup.ctl is running.";        
      fwException_raise(
          dsExceptions,
          "ERROR",
          sError,
          ""
      );
   
      return false;  
    }
    else
    {
      string sStatus = daReturnValue[1];
      
      // Error ?
      // Stop execution
      if (strpos(sStatus,"BAD-") == 0)
      {
        string sError = substr(sStatus,4);        
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            ""
        );
   
        return false;  
      }
      else
      {
          // Display it
        DebugTN(sStatus);       
      }     
    }
  }
  else
  {  
    string sError = getCatStr("unBackup","1");
    strreplace(sError,"SYSTEM", sSystemName);
    strreplace(sError,"DP_NAME", UN_APPLICATION_DPNAME + ".unBackupStatus");
        
    fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "1"
    );
    
    return false;
  }
  
  
  
  
  
  
  
  bool bBackupOver = false;  
  while (!bBackupOver)
  {    
    // ------------------------------------------------------ 3) Read every change until the end.
    dyn_anytype daReturnValue;  
    bool bTimerExpired;
    dpWaitForValue(
        makeDynString(sSystemName + UN_APPLICATION_DPNAME + ".unBackupStatus:_online.._value"), 
        makeDynAnytype(), 
        makeDynString(sSystemName + UN_APPLICATION_DPNAME + ".unBackupStatus:_online.._value"), 
        daReturnValue,
        0,
        bTimerExpired
    );
    
    // A new value is available.
    // Check it
    string sStatus = daReturnValue[1];
      
    // Error ?
    // Stop execution
    if (strpos(sStatus,"BAD-") == 0)
    {
      
      string sError = substr(sStatus,4);        
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            ""
        );
   
      return false;   
    }
    else
    {
      // Information message
      if (sStatus == "OK")
      {
        // Backup is over
        bBackupOver = true;
      }
      else if (sStatus == "") // script was restarted
      {
        string sError = "Unexpected unBackup.ctl state on system \"" + sSystemName + "\". The manager must have been restarted.";        
        fwException_raise(
            dsExceptions,
            "ERROR",
            sError,
            ""
        );
   
        return false;  
      }
      else          
      {
        // Display it
        DebugTN(sStatus, sSystemName);          
      }         
    }       
    // Keep looping for the next value
  }
  
  
  return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_waitForSingleValue
/**
Purpose: wait for new dpe value (with timeout) (anytype dpe)

Parameter: sDp, string, input, dpe name
		   timeout, unsigned, input, request timeout
		   dpValue, anytype, output, returned value
		   timeExpired, bool, output, request time expired
		   exceptionInfo, dyn_string, output, for errors
			
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/

unGenericDpFunctions_waitForSingleValue(string sDp, unsigned timeout, anytype &dpValue, bool &timeExpired, dyn_string &exceptionInfo)
{
  FWDEPRECATED();
  
	int iRes;
	time begin, now;
	float timeDiff;
	
	timeExpired = false;
	g_unGenericDpFunctions_waitForSingleValue = "##%%";
	begin = getCurrentTime();
	if (dpExists(sDp))
		{
		iRes = dpConnect("unGenericDpFunctions_waitForSingleValueCB", false, sDp);
		if (iRes >= 0)
			{
			while(g_unGenericDpFunctions_waitForSingleValue == "##%%")
				{
				now = getCurrentTime();
				timeDiff = (float)(now - begin);
				if (timeDiff > timeout)
					{
					timeExpired = true;
					g_unGenericDpFunctions_waitForSingleValue = "";
					}
				delay(0, 100);
				}
			dpDisconnect("unGenericDpFunctions_waitForSingleValueCB", sDp);
			dpValue = g_unGenericDpFunctions_waitForSingleValue;
			}
		else
			{
			fwException_raise(exceptionInfo, "unGenericDpFunctions_waitForSingleValue", getCatStr("unPVSS", "DPCONNECTFAILED"),"");
			}
		}
	else
		{
		fwException_raise(exceptionInfo, "unGenericDpFunctions_waitForSingleValue", getCatStr("unFunctions", "BADINPUTS"),"");
		}
}


//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_waitForSingleValueCB
/**
Purpose: callback function for unGenericDpFunctions_waitForSingleValue function

Usage: Internal function

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/

unGenericDpFunctions_waitForSingleValueCB(string sDp, anytype value)
{
  FWDEPRECATED(); // rather not needed here
  
	g_unGenericDpFunctions_waitForSingleValue = value;
}


// -------------------------------------------------------------------------------------

// unGenericDpFunctions_createUser
/**
Purpose: Create a user if not existing and set password, the password is not reseted if the user already exists

Parameters:
		usr: string, input, the user name
		group: string, input, the group name
		password: string, input, the default password
		sFullName: string, input, the full user name
		sComment: string, input, the description
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/
unGenericDpFunctions_createUser(string usr, string group, string password, string sFullName, string sComment)
{
  FWDEPRECATED(); 
  
DebugN("unGenericDpFunctions_createUser deprecated function: Use JCOP function instead");
}


// -------------------------------------------------------------------------------------

// unGenericDpFunctions_setPrivDomain
/**
Purpose: Set the priviledge/domain for a given group name

Parameters:
		iDomainNumber: int, input, the domain number (0 to 7)
		sGroupName: string, input, the group name
		priviledge1: bool, input, priviledge1
		priviledge2: bool, input, priviledge2
		priviledge3: bool, input, priviledge3
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/
unGenericDpFunctions_setPrivDomain(string sGroupName, int iDomainNumber, bool priviledge1, bool priviledge2, bool priviledge3)
{
  FWDEPRECATED(); 
  
DebugN("unGenericDpFunctions_setPrivDomain deprecated function: Use JCOP function instead");
}

// unGenericDpFunctions_disconnectCallBack_lock
/**
Purpose:
disconnect the call back function when a device is selected or deselected. This function does a dpDisConnect of the  
sCallBackFunction function to all the select configuration of a device. 

In case of error, iResult is set to -1 and an exception is raised in exceptionInfo.

	@param sCallBackFunction: string, input, the name of the call back function
	@param sDeviceName: string, input, the name of the device
	@param iResult: int, output, the result of the dpConnect
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/
unGenericDpFunctions_disconnectCallBack_lock(string sCallBackFunction, string sDeviceName, int &iResult, dyn_string &exceptionInfo)
{
  FWDEPRECATED();
  
	iResult = -1;
	if(sCallBackFunction == "") {
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneral", "UNLOCKWRONGCB"),"");
		return;
	}

	iResult = dpDisconnect(sCallBackFunction, 
			sDeviceName+".statusInformation.selectedManager:_lock._original._locked",
			sDeviceName+".statusInformation.selectedManager");
	if(iResult == -1) 
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneral", "UNLOCKDPNOTEXIST")+ sDeviceName,"");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getAvailableRanges
/**
Purpose: get ranges defined for current device

Parameters : 
	sDevice, string, input, device
	dsDpe, dyn_string, output, list of DPE with pv_range
	dfMin, dyn_float, output, list of minimum values
	dfMax, dyn_float, output, list of maximum values

In case of error, dyn are empty.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/

unGenericDpFunctions_getAvailableRanges(string sDevice, dyn_string &dsDpe, dyn_float &dfMin, dyn_float &dfMax)
{
  FWDEPRECATED();
  
	string sDp, sPattern;
	dyn_string dsType, dsDPEs, dsPattern;
	dyn_dyn_string ddsElements;
	dyn_int diType;
	int i, j, length, iRangeType, iRes;
	float fMin, fMax;
	
	dynClear(dsDpe);
	dynClear(dfMin);
	dynClear(dfMax);
	sDp = unGenericDpFunctions_getDpName(sDevice);
	if (dpExists(sDp))
		{
		_acGetDptStructureFromDp(dpTypeName(sDp), ddsElements, diType, dsType);		// dpTypeGet could not be use under Linux
		length = dynlen(ddsElements);
		for(i=2;i<=length;i++)
			{
			if (diType[i] == DPEL_STRUCT)
				{
				j = 1;
				while (ddsElements[i][j] == "")
					{
					j++;
					}
				while (dynlen(dsPattern) >= j)
					{
					dynRemove(dsPattern, j);
					}
				dsPattern[j] = ddsElements[i][j];
				sPattern = "";
				for(j=1;j<=dynlen(dsPattern);j++)
					{
					sPattern = sPattern + dsPattern[j] + ".";
					}
				sPattern = substr(sPattern, 1);
				}
			else
				{
				j = 1;
				while (ddsElements[i][j] == "")
					{
					j++;
					}
				dynAppend(dsDPEs, sPattern + ddsElements[i][j]);
				}
			}
		length = dynlen(dsDPEs);
		for(i=1;i<=length;i++)
			{
			iRangeType = DPCONFIG_NONE;
			dpGet(sDp + "." + dsDPEs[i] + ":_pv_range.._type", iRangeType);
			if (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)
				{
				iRes = dpGet(sDp + "." + dsDPEs[i] + ":_pv_range.._min", fMin,
					  		 sDp + "." + dsDPEs[i] + ":_pv_range.._max", fMax);
				if (iRes >= 0)
					{
					dynAppend(dsDpe, dsDPEs[i]);
					dynAppend(dfMin, fMin);
					dynAppend(dfMax, fMax);
					}
				}
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_resetHostName
/**
Purpose: this function reset the Data Server HostName

Parameter:
	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0  
	. operating system: XP, Linux
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/

void unGenericDpFunctions_resetHostName()
{
  FWDEPRECATED();
  
	if (dpExists("_unApplication.hostname") && dpExists("_unApplication.pmonPort")) // Local system
		dpSetWait("_unApplication.hostname", "", "_unApplication.pmonPort", 0);
}

// unGenericDpFunctions_setCallBack_lock
/**
Purpose:
to set a call back function a device is selected or deselected. This function does a dpConnect of the  
sCallBackFunction function to all the select configuration of a device. 

In case of error, iResult is set to -1 and an exception is raised in exceptionInfo.

The sCallBackFunction must be like:
sCallBackFunction(string sDp1, bool bLocked, string sDp2, string sManager)
{
}
bLock: true (device selected)/false (device not selected)
sManager: "ManNumber;SystemName" (device selected)/"" (device not selected) 
	ManNumber is the PVSS manager number of the PVSS manager that selected the device
	SystemName is the PVSS system name to which is connected the PVSS manager that selected the device

	@param sCallBackFunction: string, input, the name of the call back function
	@param sDeviceName: string, input, the name of the device
	@param iResult: int, output, the result of the dpConnect
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/
unGenericDpFunctions_setCallBack_lock(string sCallBackFunction, string sDeviceName, int &iResult, dyn_string &exceptionInfo)
{
  FWDEPRECATED();
  
	iResult = -1;
	if(sCallBackFunction == "") {
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneral", "LOCKWRONGCB"),"");
		return;
	}

	iResult = dpConnect(sCallBackFunction, 
			sDeviceName+".statusInformation.selectedManager:_lock._original._locked",
			sDeviceName+".statusInformation.selectedManager");
	if(iResult == -1) 
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneral", "LOCKDPNOTEXIST")+ sDeviceName,"");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_setDeviceWidget
/**
Purpose: set device widget

Parameters :
	sDp, string, input, device name
	sWidget, string, input, device widget
	exceptionInfo, dyn_string, output, for errors

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-06-26
*/

unGenericDpFunctions_setDeviceWidget(string sDp, string sWidget, dyn_string &exceptionInfo)
{
  FWDEPRECATED();
  
	string deviceName, sWidgetFormatted;
	dyn_string dsParameters, dsTemp;
	
	sWidgetFormatted = unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName(sWidget);

	deviceName = unGenericDpFunctions_getDpName(sDp);
	unGenericDpFunctions_changeDpParameter(deviceName, UN_PARAMETER_WIDGET, sWidgetFormatted, exceptionInfo);
}



/** 
@deprecated 2018-06-29 ; not sure what the function is used for...
 */
dyn_dyn_string unExport_loadDPs(string sFrontEndType, string sPlcName, string sSubApplication, string sDeviceType, dyn_string& exceptionInfo) {
    FWDEPRECATED();
    string sFileName = tmpnam();
    unExportDevice_exportDPs(sFrontEndType, sPlcName, sSubApplication, sDeviceType, sFileName, exceptionInfo);
    // read file into the memory
    g_fileToExport = fopen(sFileName, "r");
    int lineIdx = 0;
    dyn_string lineSplit;
    dyn_dyn_string result;
    while (feof(g_fileToExport) == 0) {
        unImportDevice_getNextLine(g_fileToExport, lineSplit, lineIdx);
        if (dynlen(lineSplit) > 3) {
            dynAppend(result, lineSplit);
        }
    }
    fclose(g_fileToExport);
    
    remove(sFileName);
    return result;
}

// unExportDevice_getAlarmMasterType
/**
Purpose:  Get the Alarm Type and its Master (defined in UN_PARAMETER_NAME)
          The format is: Master,Type
Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/
void unExportDevice_getAlarmMasterType(string sDp, string &sMasterType)
{
    FWDEPRECATED();
    int iLen;
    string tempName;
    dyn_string splitAlarmName;
	
	tempName = unGenericDpFunctions_getDescription( sDp + ".ProcessInput");
	splitAlarmName=strsplit(tempName , UN_PARAMETER_DELIMITER);
	
	iLen=dynlen(splitAlarmName);
	if (iLen>=UN_PARAMETER_NAME)
		sMasterType = splitAlarmName[UN_PARAMETER_NAME];
}

// unExportDevice_getAnalogDPEAlarmConfig
/**
Purpose:  Get the archive config of non-boolean DPE

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/
unExportDevice_getAnalogDPEAlarmConfig(dyn_string dsDpe, dyn_dyn_string &ddsLimits, dyn_int &diNormalPosition)
{
    FWDEPRECATED();
	int iRes, iAlertType;
	string sAlertType, sLimit1="0.0", sLimit2="0.0", sLimit3="0.0", sLimit4="0.0";
	bool bBoolAlert;
	dyn_string exceptionInfo;
	int iLen, len=dynlen(dsDpe), iNormalPosition;
	string sHH, sLL, sH, sL;
	dyn_string dsLimits;
	
	dynClear(ddsLimits);
	dynClear(diNormalPosition);
	for(iLen=1; iLen<=len;iLen++)
	{
		sLimit1 = sLimit2 = sLimit3 = sLimit4 = "0.0";
		sHH=sLimit1;
		sLL=sLimit1;
		sH=sLimit1;
		sL=sLimit1;
		//Get Limits
		unGenericObject_CheckAnalogAlarmType(dsDpe[iLen], iAlertType, exceptionInfo);
		bBoolAlert = (iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL);
		dynClear(dsLimits);
		if(bBoolAlert) {
			switch(g_iAlertRange) //g_iAlertRange set in unGenericObject_CheckAnalogAlarmType(...) 
			{
				case UN_AIAO_ALARM_5_HH_H_L_LL:
					dpGet(	dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit1,
									dsDpe[iLen]+":_alert_hdl.2._u_limit" , sLimit2, 
									dsDpe[iLen]+":_alert_hdl.4._l_limit" , sLimit3,
									dsDpe[iLen]+":_alert_hdl.5._l_limit" , sLimit4);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
					sLimit3=unExportDevice_floatFormat(sLimit3);
					sLimit4=unExportDevice_floatFormat(sLimit4);
													
					dynAppend(dsLimits, sLimit4 );//HH
					dynAppend(dsLimits, sLimit3 );//H
					dynAppend(dsLimits, sLimit2 );//L
					dynAppend(dsLimits, sLimit1 );//LL
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_5_HH_H_L_LL, iNormalPosition);
					break;
					
				case UN_AIAO_ALARM_4_HH_H_L:	
					dpGet(	dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit1,
									dsDpe[iLen]+":_alert_hdl.2._u_limit" , sLimit2,
									dsDpe[iLen]+":_alert_hdl.4._l_limit" , sLimit3);
	
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
					sLimit3=unExportDevice_floatFormat(sLimit3);
										
					dynAppend(dsLimits, sLimit3);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_4_HH_H_L, iNormalPosition);
					break;
						
				case UN_AIAO_ALARM_4_H_L_LL:
					dpGet(	dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit1,
									dsDpe[iLen]+":_alert_hdl.2._u_limit" , sLimit2,
									dsDpe[iLen]+":_alert_hdl.4._l_limit" , sLimit3);
	
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
					sLimit3=unExportDevice_floatFormat(sLimit3);
										
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sLimit3);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sLimit1);
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_4_H_L_LL, iNormalPosition);
					break;
					
				case UN_AIAO_ALARM_3_H_L:
					dpGet(	dsDpe[iLen]+":_alert_hdl.3._l_limit" , sLimit1,
									dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit2);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
									
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_3_H_L, iNormalPosition);
					break;
				
				case UN_AIAO_ALARM_3_HH_H:
					dpGet(	dsDpe[iLen]+":_alert_hdl.3._l_limit" , sLimit1,
									dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit2);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
									
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sL);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_3_HH_H, iNormalPosition);
					break;
				
				case UN_AIAO_ALARM_3_LL_L:
					dpGet(	dsDpe[iLen]+":_alert_hdl.3._l_limit" , sLimit1,
									dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit2);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
									
					dynAppend(dsLimits, sHH );
					dynAppend(dsLimits, sH );				
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLimit2);
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_3_LL_L, iNormalPosition);
					break;
					
				case UN_AIAO_ALARM_2_H:
					dpGet(dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit1 );
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
				
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sL);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_2_H, iNormalPosition);
					break;
				
				case UN_AIAO_ALARM_2_L:
					dpGet(dsDpe[iLen]+":_alert_hdl.1._u_limit" , sLimit1 );
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sH);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition1DPE(dsDpe[iLen], UN_AIAO_ALARM_2_L, iNormalPosition);
					break;
			}
		}
		else {
			iNormalPosition=2;
			dynAppend(dsLimits, sHH);
			dynAppend(dsLimits, sH);
			dynAppend(dsLimits, sL);
			dynAppend(dsLimits, sLL);
		}
		dynAppend(ddsLimits, dsLimits);
		dynAppend(diNormalPosition, iNormalPosition);
	}
}

// unExportDevice_getArchiveNamesDpe
/**
Purpose: Get Archives names (Bool, Analog and Event values)

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/
void unExportDevice_getArchiveNamesDpe(string sDp, dyn_string dsInDpe, dyn_string &dsNames)
{
    FWDEPRECATED();
    int i, j, iLen, iLen_j;
    string sArchiveClass, sName;
    dyn_string dsTemp, dsCheck, dsDpe = dsInDpe;       

		while(dynlen(dsDpe) < 3)
			dynAppend(dsDpe, UN_CONFIG_EXPORT_UNDEF);
		iLen=dynlen(dsDpe);
		for (i=1; i<=iLen; i++)
			{
			if (dsDpe[i]!=UN_CONFIG_EXPORT_UNDEF)
				{
				dynClear(dsTemp);
				dynClear(dsCheck);
				
				dsTemp=strsplit(dsDpe[i], DPE_ARCHIVE_DELIMITER);
				iLen_j=dynlen(dsTemp);
				for (j=1; j<=iLen_j; j++)
					{
					sName="";
					sArchiveClass="";
					
					dpGet(sDp + dsTemp[j] + ":_archive.1._class", sArchiveClass);
		
					if(dpExists(sArchiveClass + ".general.arName"))
				 		dpGet(sArchiveClass + ".general.arName:_online.._value", sName);
				 	else
				 		{
				 		if (i==1)
							sName=UN_CONFIG_EXPORT_ARCHIVE_1;
						else if (i==2)
							sName=UN_CONFIG_EXPORT_ARCHIVE_2;
						else if (i==3)
							sName=UN_CONFIG_EXPORT_ARCHIVE_3;
						else
							sName=UN_CONFIG_EXPORT_UNDEF;	
				 		}
				 	
				 	dynAppend(dsCheck, sName);	
					}
				
				dynUnique(dsCheck);
				
				if (dynlen(dsCheck)==1)		//check if same archive for same type
					sName=dsCheck[1];	
				else
					sName=UN_CONFIG_EXPORT_UNDEF;	
					
				}
			else		//default
				{
				if (i==1)
					sName=UN_CONFIG_EXPORT_ARCHIVE_1;
				else if (i==2)
					sName=UN_CONFIG_EXPORT_ARCHIVE_2;
				else if (i==3)
					sName=UN_CONFIG_EXPORT_ARCHIVE_3;
				else
					sName=UN_CONFIG_EXPORT_UNDEF;	
				}
							
			dynAppend(dsNames, sName);	
			}
		unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unExportDevice_getArchiveNamesDpe", "unExportDevice_getArchiveNamesDpe", sDp, dsInDpe, dsNames, "-");
}

// unExportDevice_getDigitalNormalPositionDPE
/**
Purpose:  Get the normal position of boolean DPE

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/
unExportDevice_getDigitalNormalPositionDPE(dyn_string dsDpe, dyn_int &diNormalPosition)
{
    FWDEPRECATED();
	int iNormalPosition;
	int i, len=dynlen(dsDpe);
	bool bAlarmMask, bOkRange, bBoolAlert,bSMS;
	string sAlertClass1,sAlertClass2;
	int iAlertType, iRes;
	
	dynClear(diNormalPosition);
	for(i=1;i<=len;i++)
	{
		iNormalPosition = -1;
		iAlertType = DPCONFIG_NONE;
		iRes = dpGet(dsDpe[i] + ":_alert_hdl.._type", iAlertType);
		bSMS = false;

		bBoolAlert = ((iRes >= 0) && (iAlertType == DPCONFIG_ALERT_BINARYSIGNAL)); 
		if(!bBoolAlert)
			iAlertType = DPCONFIG_NONE;
			
		if(iAlertType == DPCONFIG_NONE)
		{
			iNormalPosition = 2;
		}
		else
		{	
			iRes = dpGet(dsDpe[i] + ":_alert_hdl.._ok_range", bOkRange, dsDpe[i] + ":_alert_hdl.._active",  bAlarmMask,
										dsDpe[i] + ":_alert_hdl.1._class",  sAlertClass1, dsDpe[i] + ":_alert_hdl.2._class",  sAlertClass2);
		
			if((patternMatch(UN_PROCESSALARM_PATTERN,sAlertClass1))||(patternMatch(UN_PROCESSALARM_PATTERN,sAlertClass2)))
				bSMS = true;
			else 
				bSMS = false;		
//	DebugN(bSMS, sAlertClass1,sAlertClass2);
	
			if(bAlarmMask)
			{ //Alarm unmasked
				if(bOkRange) {//ON
					if(bSMS) iNormalPosition =101;
						else iNormalPosition = 1;
				}
				else //OFF
				{
					if(bSMS) iNormalPosition = 100;
					else iNormalPosition = 0;
				}
			}
			else
			{ //Alarm masked
				if(bOkRange)//ON
				{
					if (bSMS) iNormalPosition =104;
						else iNormalPosition = 4;
				}
				else {//OFF
					if (bSMS) iNormalPosition = 103;
						else iNormalPosition = 3;
				}
			}	
		}
		dynAppend(diNormalPosition, iNormalPosition);
	}
}

// unExportDevice_getLimits
/**
Purpose:  Get limits

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/

void unExportDevice_getLimits(string sDp, dyn_string &dsLimits, int &iNormalPosition, float fH, float fL)
{
    FWDEPRECATED();
	int iRes, iRangeType, iAlertType, i;
	string sAlertType, sLimit1="0.0", sLimit2="0.0", sLimit3="0.0", sLimit4="0.0";
	string sHH, sLL, sH, sL;
	bool bBoolAlert;
	
	dyn_string exceptionInfo;
	
	//***************************************
	//13/07/2006 FB - Now default limits are 0.0 amd not based on PvRange
	//***************************************
	/*sHH = fH + (fH*UN_PERCENT_RANGE_DEFAULT);
	sHH=unExportDevice_floatFormat(sHH);
		
	sLL = fL - (fH*UN_PERCENT_RANGE_DEFAULT);
	sLL=unExportDevice_floatFormat(sLL);
	
	sH=unExportDevice_floatFormat(fH);
	sL=unExportDevice_floatFormat(fL);*/

	sHH=sLimit1;
	sLL=sLimit1;
	sH=sLimit1;
	sL=sLimit1;
	//***************************************
	
	//Get Limits
	unGenericObject_CheckAnalogAlarmType(sDp + ".ProcessInput.PosSt", iAlertType, exceptionInfo);
	bBoolAlert = (iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL);
	
	iRes = dpGet(sDp + ".ProcessInput.PosSt:_pv_range.._type", iRangeType);
	if ((iRes < 0) || (iRangeType != DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
		iRangeType = DPCONFIG_NONE;
		}
	
	if ((bBoolAlert) && (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
		dynClear(dsLimits);
			switch(g_iAlertRange) //g_iAlertRange set in unGenericObject_CheckAnalogAlarmType(...) 
				{
				case UN_AIAO_ALARM_5_HH_H_L_LL:
					dpGet(	sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit1,
									sDp + ".ProcessInput.PosSt:_alert_hdl.2._u_limit" , sLimit2, 
									sDp + ".ProcessInput.PosSt:_alert_hdl.4._l_limit" , sLimit3,
									sDp + ".ProcessInput.PosSt:_alert_hdl.5._l_limit" , sLimit4);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
					sLimit3=unExportDevice_floatFormat(sLimit3);
					sLimit4=unExportDevice_floatFormat(sLimit4);
													
					dynAppend(dsLimits, sLimit4 );//HH
					dynAppend(dsLimits, sLimit3 );//H
					dynAppend(dsLimits, sLimit2 );//L
					dynAppend(dsLimits, sLimit1 );//LL
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_5_HH_H_L_LL, iNormalPosition);
					break;
					
				case UN_AIAO_ALARM_4_HH_H_L:	
					dpGet(	sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit1,
									sDp + ".ProcessInput.PosSt:_alert_hdl.2._u_limit" , sLimit2,
									sDp + ".ProcessInput.PosSt:_alert_hdl.4._l_limit" , sLimit3);
	
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
					sLimit3=unExportDevice_floatFormat(sLimit3);
										
					dynAppend(dsLimits, sLimit3);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_4_HH_H_L, iNormalPosition);
					break;
						
				case UN_AIAO_ALARM_4_H_L_LL:
					dpGet(	sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit1,
									sDp + ".ProcessInput.PosSt:_alert_hdl.2._u_limit" , sLimit2,
									sDp + ".ProcessInput.PosSt:_alert_hdl.4._l_limit" , sLimit3);
	
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
					sLimit3=unExportDevice_floatFormat(sLimit3);
										
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sLimit3);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sLimit1);
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_4_H_L_LL, iNormalPosition);
					break;
					
				case UN_AIAO_ALARM_3_H_L:
					dpGet(	sDp + ".ProcessInput.PosSt:_alert_hdl.3._l_limit" , sLimit1,
									sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit2);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
									
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_3_H_L, iNormalPosition);
					break;
				
				case UN_AIAO_ALARM_3_HH_H:
					dpGet(	sDp + ".ProcessInput.PosSt:_alert_hdl.3._l_limit" , sLimit1,
									sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit2);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
									
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLimit2);
					dynAppend(dsLimits, sL);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_3_HH_H, iNormalPosition);
					break;
				
				case UN_AIAO_ALARM_3_LL_L:
					dpGet(	sDp + ".ProcessInput.PosSt:_alert_hdl.3._l_limit" , sLimit1,
									sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit2);
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					sLimit2=unExportDevice_floatFormat(sLimit2);
									
					dynAppend(dsLimits, sHH );
					dynAppend(dsLimits, sH );				
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLimit2);
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_3_LL_L, iNormalPosition);
					break;
					
				case UN_AIAO_ALARM_2_H:
					dpGet(sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit1 );
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
				
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sL);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_2_H, iNormalPosition);
					break;
				
				case UN_AIAO_ALARM_2_L:
					dpGet(sDp + ".ProcessInput.PosSt:_alert_hdl.1._u_limit" , sLimit1 );
					
					sLimit1=unExportDevice_floatFormat(sLimit1);
					
					dynAppend(dsLimits, sHH);
					dynAppend(dsLimits, sH);
					dynAppend(dsLimits, sLimit1);
					dynAppend(dsLimits, sLL);
					
					unExportDevice_getAnalogNormalPosition(sDp, UN_AIAO_ALARM_2_L, iNormalPosition);
					break;
			}
		}
	else
		{
		iNormalPosition=2;
		dynAppend(dsLimits, sHH);
		dynAppend(dsLimits, sH);
		dynAppend(dsLimits, sL);
		dynAppend(dsLimits, sLL);
		}
}

// unExportDevice_getPCOName
/**
Purpose:  Get the PCO Name

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/
void unExportDevice_getPCOName(string sDp, string &sPCOName)
{
    FWDEPRECATED();
    int iLen;
    string tempPCOName;
    dyn_string splitPCOName;
	
	tempPCOName = unGenericDpFunctions_getDescription( sDp + ".ProcessInput");
	splitPCOName=strsplit(tempPCOName , UN_PARAMETER_DELIMITER);
	
	iLen=dynlen(splitPCOName);
	if (iLen>=UN_PARAMETER_NAME)
		sPCOName = splitPCOName[UN_PARAMETER_NAME];

}

// unExportDevice_getStsReg
/**
Purpose:  Get the StsReg of a dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/
void unExportDevice_getStsReg(string sDp, string &sStrReg)
{
    FWDEPRECATED();
	dpGet(sDp + ".ProcessInput.StsReg01" , sStrReg );
}

// unExportDevice_writeStringToFile
/**
Purpose:  Write a string to a selected file

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/

void unExportDevice_writeStringToFile(string sTextToWrite, string delimiter)
{
    FWDEPRECATED();
	fputs(sTextToWrite + delimiter , g_fileToExport );
}

// unExportDevices_getArchiveDPE
/**
Purpose:  Get archive parameters of a dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/
void unExportDevices_getArchiveDPE(anytype sDPE, string &sReturnArchiveActive,  float &fArchiveTimeFilter)
{
    FWDEPRECATED();
	bool bArchiveActive;
	int iType = getType(sDPE), i, length;
	dyn_string dsDpe;
	string sArchiveActive, sTempArchive;
	
	switch(iType) {
		case STRING_VAR:
			dsDpe = makeDynString(sDPE);
			break;
		case DYN_STRING_VAR:
			dsDpe = sDPE;
			break;
		default:
			dsDpe = makeDynString();
			break;
	}
	length = dynlen(dsDpe);
	for(i=1;i<=length;i++) // For each element
	{
		dpGet(dsDpe[i] + ":_archive.._archive" , bArchiveActive);
			
		if (bArchiveActive)
		{
			unExportDevice_getArchiveTemp(dsDpe[i], bArchiveActive, sTempArchive, fArchiveTimeFilter);
		}
		else
		{	
			sTempArchive = "N";
			fArchiveTimeFilter = -1;
		}
		if(sArchiveActive == "") {
			if(iType == STRING_VAR)
				sArchiveActive = sTempArchive;
			else
				sArchiveActive = sTempArchive+",";
		}
		else
			sArchiveActive += "|"+sTempArchive;
	}
	sReturnArchiveActive = sArchiveActive;
}

// unExportDevices_getSmoothingDPE
/**
Purpose:  Get the deadband type and deadband value

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV

@deprecated 2018-06-25
*/

unExportDevices_getSmoothingDPE(string sDpName, dyn_string dsDpe, dyn_int &diSmoothingType, dyn_float &dfDeadBand)
{
    FWDEPRECATED();
	int i, len, iSmoothProcedure, bIsSmoothDefined, iDeadBandType;
	bool bNegateRange , bIgnoreOutside , bInclusiveMin , bInclusiveMax, bDoesExist;
	float fMinValue , fMaxValue, fDeadbandTemp, fTimeInterval, fDeadband;
	dyn_string exceptionInfo;
	
	len = dynlen(dsDpe);
	for(i=1;i<=len;i++) {
		//Get smoothProcedure, fDeadband & timeInterval
		unExportDevice_fwSmoothing_getTemp(sDpName + dsDpe[i], bIsSmoothDefined, iSmoothProcedure, fDeadbandTemp, fTimeInterval);
		
		//Get unicos DeadBand
		unExportDevice_getDeadBand(fDeadbandTemp, bIsSmoothDefined, 100, 0, iSmoothProcedure, fDeadband, iDeadBandType);	
		dfDeadBand[i] = fDeadband;
		diSmoothingType[i] = iDeadBandType;
	}
}



// unGenericUtilities_activateArchiveCompression
/**
  @par Description
  Activate the compression of the given archive.

  @par Usage
        public
  
  @param sArchiveDp: string, input: DP of the archive
  @param bActivate: string, input: true to activate, false to deactivate
  @param dsExceptions, dyn_string, output: list of errors encountered during the execution

  @deprecated 2018-06-26
*/
unGenericUtilities_activateArchiveCompression(string sArchiveDp, bool bActivate, dyn_string &dsExceptions)
{
  FWDEPRECATED();
  if (!dpExists(sArchiveDp))
  {
    string sError = "Error in function unGenericUtilities_activateArchiveCompression : " + getCatStr("unGenericUtilities","1");
    strreplace(sError, "ARCHIVE_DP", sArchiveDp);
    fwException_raise(
      dsExceptions,
      "ERROR",
      sError,
      "1"
    );
    return;
  }

  dpSet(sArchiveDp + ".setMgmt.fileCompression.autoStartAfterSwitchSet", bActivate);
}




//unGenericUtilities_getArchiveList
/**
 @par Description
  Get the list of available active archive for the given system.

  @par Usage
        public


  @param sSystemName: string, input: system in which to look for

  @return the list of all the active archives in the system


  @deprecated 2018-06-26
*/
dyn_string unGenericUtilities_getActiveArchiveList(string sSystemName)
{
  FWDEPRECATED();
  dyn_string dsArchives;
  dyn_string dsActiveArchives;
  string sPattern = sSystemName + "*";;
  dsArchives = dpNames(sPattern,"_ValueArchive");

  for (int i = 1 ; i <= dynlen(dsArchives) ; i++)
  {
    int iActive = 0;
    dpGet(dsArchives[i] + ".state", iActive);
    if (iActive == 1)
    {
      dynAppend(dsActiveArchives, dsArchives[i]);
    }
  }

  return dsActiveArchives;
}




//unGenericUtilities_getArchiveList
/**
 @par Description
  Get the list of all available archives for the given system.

  @par Usage
        public


  @param sSystemName: string, input: system in which to look for

  @return the list of all the archives in the system

  @deprecated 2018-06-26

*/
dyn_string unGenericUtilities_getArchiveList(string sSystemName)
{
  FWDEPRECATED();
  dyn_string dsArchives;
  dyn_string dsActiveArchives;
  string sPattern = sSystemName + "*";;
  dsArchives = dpNames(sPattern,"_ValueArchive");

  return dsArchives;
}




//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_getDeviceArchiveName
/**
Get the archive name of a given device config.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFrontEndDeviceType  input, the front-end device type
@param sDeviceType  input, the device type
@param dsConfig  input, device or front-end config
@param dsDollar  input, dollar parameter given to the evalScript
@param sArchiveBoolName  output, the boolean archive name
@param sArchiveAnalogName  output, the analog archive name
@param sArchiveEventName  output, the event archive name

@deprecated 2018-06-26 moved to deprecated as suggested long time ago; use unImportDevice_getArchiveName instead

*/
unImportDevice_getDeviceArchiveName(string sFrontEndDeviceType, string sDeviceType, dyn_string dsConfig,
                  dyn_string dsDollar, string &sArchiveBoolName, string &sArchiveAnalogName, string &sArchiveEventName)
{
  FWDEPRECATED();

  string sTemp;
  dyn_string dsResult;

  sTemp = sFrontEndDeviceType;
  if((sTemp == UN_PLC_DPTYPE) || (sTemp == S7_PLC_DPTYPE))
    sTemp = strtoupper(sDeviceType);
  else
    sTemp = sTemp+"_" + strtoupper(sDeviceType);
  
// check if a constant UN_CONFIG_FEType_DeviceType_LENGTH exists:
// if yes check if the length of the dsConfig == (UN_CONFIG_DeviceType_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH)
// if yes so take the archive from the import file, if "" use default one
// if not then use default one.

  evalScript(dsResult, "dyn_string main(dyn_string configLine)"+
                        "{" +
                         "  dyn_string dsResult = makeDynString(\"\", \"\", \"\");" +
                          "  if(globalExists(\"UN_CONFIG_" + sTemp + "_LENGTH\"))" +
                          "  { " +
                          "    if(unConfigGenericFunctions_additionalDeviceInfo())"+
                          "    {" +
                          "     if(globalExists(\"UN_CONFIG_" + sTemp + "_ADDITIONAL_LENGTH\"))"+
                          "     {" +
                          "      if(dynlen(configLine) == (UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH + UN_CONFIG_"+ sTemp +"_ADDITIONAL_LENGTH)) {" +
                          "        dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL] = configLine[UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL];"+
                          "        dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG] = configLine[UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG];"+
                          "        dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT] = configLine[UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT];"+
                          "      }"+
                          "     }"+
                          "    } "+
                          "    else"+
                          "    {" +
                          "      if(dynlen(configLine) == (UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH)) {" +
                          "        dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL] = configLine[UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL];"+
                          "        dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG] = configLine[UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG];"+
                          "        dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT] = configLine[UN_CONFIG_" + sTemp + "_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT];"+
                          "      }"+
                          "    }" +
                          "  }" +
                          "  return dsResult;" +
                          "}",
                 dsDollar, dsConfig);

//DebugN(dsResult, dynlen(dsConfig));
  sArchiveBoolName = dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL];
  sArchiveAnalogName = dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG];
  sArchiveEventName = dsResult[UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT];
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_getDeviceArchiveName", "unImportDevice_getDeviceArchiveName", sDeviceType, sArchiveBoolName, sArchiveAnalogName, sArchiveEventName, "-");
}

// -------------------------------------------------------------------------------------

//unGenericUtilities_stopManager
/**
 @par Description
  Helper function so easily stop a manager. This function exists right after sending the order to stop, the manager will not necessarily stopped immediatly.
 
  @par Usage
	public
    
  @param sManager: string, input: manager to start (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystem: string, input: system on which this manager is to be stopped
  
  
  @return When timeout value is set, returns true if the manager is properly stopped, or false if it failed to stop. When no timeout is set, useless value.
 
  @deprecated 2018-08-14
*/
unGenericUtilities_stopManager(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystem)
{
  FWDEPRECATED();
  
  int iIndex;  
  unSystemIntergity_manageManager(sManager, sManagerCommand, SYSTEMINTEGRITY_STOP_COMMAND, iIndex, dsExceptions, sSystem);  
  
}
