/** @name LIBRARY: unGenericObjectDeprecated.ctl

Creation date: 2018-08-02

Purpose:
This library contains deprecated functions from unGenericObject.ctl library (unCore), 
that may be eventually removed from component distribution.
This library contains obsolete functions to amimate CPC5 widgets.

*/

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetTurbineAnimation
/**
Purpose: animate turbine

Parameters:
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetTurbineAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	string body1Color = sBodyColor, body2BackColor, body3BackColor, body4BackColor, body5BackColor, body6Color;

// 1. Set colors
	body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
	body3BackColor = "unWidget_Background";
	body4BackColor = "unWidget_Background";
	body5BackColor = "unWidget_Background";
	body6Color = body1Color;
	if (body1Color != "unDataNotValid")
		{
		if (iOff == 0)
			{
			body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
			body3BackColor = body1Color;
			if (iOn == 1)
				{
				body4BackColor = body1Color;					// Onst = 1 and OffSt = 0
				body5BackColor = body1Color;
				}
			}
		}
// 2. Animate body
	setMultiValue("Body1", "foreCol", body1Color,
				  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
				  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
				  "Body4", "fill", "[solid]", "Body4", "backCol", body4BackColor,
				  "Body5", "fill", "[solid]", "Body5", "backCol", body5BackColor,
				  "Body6", "foreCol", body6Color);
}

// -------------------------------------------------------------------------------------

// unGenericObject_WidgetTurbineDisconnection
/**
Purpose: animate turbine disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetTurbineDisconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
				  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
				  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
				  "Body4", "backCol", "unWidget_Background", "Body4", "fill", "[solid]",
				  "Body5", "backCol", "unWidget_Background", "Body5", "fill", "[solid]",
				  "Body6", "foreCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetSquare4Animation
/**
Purpose: animate 4-body square

Parameters: 
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetSquare4Animation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	string body1Color = sBodyColor, body2BackColor, body3BackColor, body4Color;

// 1. Set colors
	body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
	body3BackColor = "unWidget_Background";
	body4Color = body1Color;
	if (body1Color != "unDataNotValid")
		{
		if (iOff == 0)
			{
			body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
			if (iOn == 1)
				{
				body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
				}
			}
                 else if (iOn == 1)
                    body2BackColor = body1Color;					// OnSt = 1 and OffSt = 1
		}
// 2. Animate body
	setMultiValue("Body1", "foreCol", body1Color,
				  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
				  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
				  "Body4", "foreCol", body4Color);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetSquare4Disconnection
/**
Purpose: animate 4-body square disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
 
  @deprecated 2018-08-02
*/

unGenericObject_WidgetSquare4Disconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
				  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
				  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
				  "Body4", "foreCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetSquare3Animation
/**
Purpose: animate 3-body square

Parameters: 
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetSquare3Animation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	string body1Color = sBodyColor, body2BackColor, body3BackColor;

// 1. Set colors
	body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
	body3BackColor = "unWidget_Background";
	if (body1Color != "unDataNotValid")
		{
		if (iOff == 0)
			{
			body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
			if (iOn == 1)
				{
				body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
				}
			}
		}
// 2. Animate body
	setMultiValue("Body1", "foreCol", body1Color,
				  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
				  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor);
}
//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetSquare3Disconnection
/**
Purpose: animate 3-body square disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetSquare3Disconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
				  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
				  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetPumpAnimation
/**
Purpose: animate pump

Parameters: 
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetPumpAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	unGenericObject_WidgetMotorAnimation(iOn, iOff, sBodyColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetPumpDisconnection
/**
Purpose: animate pump disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetPumpDisconnection()
{
  FWDEPRECATED();
  
	unGenericObject_WidgetMotorDisconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetOnOff3WayValveAnimation
/**
Purpose: animate OnOff 3 way valve

Parameters:
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/
unGenericObject_WidgetOnOff3WayValveAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
  string sBody1Color = sBodyColor, sFromBackColor, sOnBackColor, sOffBackColor;

  // 1. Set colors
  sOnBackColor = "unWidget_Background";
  sOffBackColor = "unWidget_Background";
  sFromBackColor = "unWidget_Background";
  if (sBody1Color != "unDataNotValid")
  {
    sFromBackColor = sBody1Color;					//	filled in all cases.
    if ((iOff == 1) && (iOn == 0))
      sOffBackColor = sBody1Color;				// OnSt = 0 and OffSt = 1

    if ((iOff == 0) && (iOn == 1))
      sOnBackColor = sBody1Color;					// Onst = 1 and OffSt = 0
  }
  // 2. Animate body
  setMultiValue("Body1", "foreCol", sBody1Color,
                "pos_on",   "fill", "[solid]", "pos_on",   "backCol", sOnBackColor,
                "pos_off",  "fill", "[solid]", "pos_off",  "backCol", sOffBackColor,
                "pos_from", "fill", "[solid]", "pos_from", "backCol", sFromBackColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetOnOff3WayValveDisconnection
/**
Purpose: animate OnOff 3 way valve disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/
unGenericObject_WidgetOnOff3WayValveDisconnection()
{
  FWDEPRECATED();
  
  setMultiValue("Body1", "foreCol", "unDataNoAccess", /*"Body1", "backCol", "unWidget_Background",*/
                "pos_on", "backCol", "unWidget_Background", "pos_on", "fill", "[solid]",
                "pos_off", "backCol", "unWidget_Background", "pos_off", "fill", "[solid]",
                "pos_from", "backCol", "unWidget_Background", "pos_from", "fill", "[solid]");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetInvertedValveAnimation
/**
Purpose: animate inverted valve

Parameters:
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/
unGenericObject_WidgetInvertedValveAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
  unGenericObject_WidgetValveAnimation(iOff, iOn, sBodyColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetInvertedValveDisconnection
/**
Purpose: animate inverted valve disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/
unGenericObject_WidgetInvertedValveDisconnection()
{
  FWDEPRECATED();
  
  unGenericObject_WidgetValveDisconnection();
}
//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetHeaterAnimation
/**
Purpose: animate heater

Parameters:
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetHeaterAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	string body1Color = sBodyColor, body2BackColor, body3BackColor, body4Color, body5Color;

// 1. Set colors
	body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
	body3BackColor = "unWidget_Background";
	body4Color = body1Color;
	body5Color = body1Color;
	if (body1Color != "unDataNotValid")
		{
		if (iOff == 0)
			{
			body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0	
			body4Color = "unWidget_AlternativeColor";
			if (iOn == 1)
				{
				body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
				body5Color = "unWidget_AlternativeColor";
				}
			}
                  else if (iOn == 1)          
                    {                                              // Onst = 1 and OffSt = 1
                      body2BackColor = body1Color;   
                      body4Color = "unWidget_AlternativeColor";
                    }
		}
// 2. Animate body
	setMultiValue("Body1", "foreCol", body1Color,
				  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
				  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
				  "Body4", "foreCol", body4Color, "Body5", "foreCol", body5Color);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetHeaterDisconnection
/**
Purpose: animate heater disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetHeaterDisconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
				  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
				  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
				  "Body4", "foreCol", "unDataNoAccess", "Body5", "foreCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetDIDOAnimation
/**
Purpose: animate DI DO widget body

Parameters:
	- bLocked, bool, input, for select animation
	- sSelectedManager, string, input, for select animation
	- bit32StsReg01, bit32, input, status register value
	- bStsReg01Invalid, bool, input, data status
	- bBoolAlert, bool, input, alert hdl exists ?
	- bIsAlarmActive, bool, input, alert hdl active ?
	- iActState, int, input, alert hdl act state
	- bAckPossible, bool, input, has alert to be acknowleged ?
	- iSystemIntegrityAlarmValue, int, input, the value of the systemIntegrity alarm for the PLC: 
							c_unSystemIntegrity_no_alarm_value (0) no alarm PLC is running, 
							c_unSystemIntegrity_alarm_value_level1 (10): alarm PLC is not correctly running
	- bSystemIntegrityAlarmEnabled, bool, input, the systemIntegrity alarm is enabled
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetDIDOAnimation(bool bLocked, string sSelectedManager, bit32 bit32StsReg01, bool bStsReg01Invalid, 
									bool bBoolAlert, bool bIsAlarmActive, int iActState, bool bAckPossible, int iSystemIntegrityAlarmValue,
									bool bSystemIntegrityAlarmEnabled)
{
  FWDEPRECATED();
  
	string warningLetter, warningColor, alarmLetter, alarmColor, controlLetter, controlColor;
	string body1Color, body1BackColor, selectColor;
	bool lockVisible;
	
	if (bStsReg01Invalid)
		{
		warningLetter = UN_WIDGET_TEXT_INVALID;
		warningColor = "unDataNotValid";
		body1Color = "unDataNotValid";
		body1BackColor = "unWidget_Background";
		}
	else
		{
		body1Color = "unWidget_ControlStateAuto";				// Auto mode
		if (getBit(bit32StsReg01, UN_STSREG01_FOMOST) == 1)		// Forced mode
			{
			body1Color = "unWidget_ControlStateForced";				
			controlLetter = UN_WIDGET_TEXT_CONTROL_FORCED;
			controlColor = "unWidget_ControlStateForced";
			}
		if (bBoolAlert) 										// Alarm config exists
			{
			if (bIsAlarmActive)									// Alarm active
				{
				if ((iActState == DPATTR_ALERTSTATE_APP_NOT_ACK) || (iActState == DPATTR_ALERTSTATE_APP_ACK)) // Alarm ?
					{
					body1Color = "unFaceplate_AlarmActive";
					}
				if (bAckPossible)								// Alarm not ack ?
					{
					body1Color = "unWidget_AlarmNotAck";
					}
				}
			else												// Alarm masked
				{
				alarmLetter = UN_WIDGET_TEXT_ALARM_MASKED;
				alarmColor = "unAlarmMasked";
				}
			}
		body1BackColor = "unWidget_Background";
		if (getBit(bit32StsReg01, UN_STSREG01_POSST) == 1)
			{
			body1BackColor = body1Color;
			}
		unGenericObject_WidgetWarningAnimation(bit32StsReg01, 
											   makeDynInt(UN_STSREG01_FODIPRO, UN_STSREG01_IOSIMUW, UN_STSREG01_IOERRORW),
											   makeDynString(UN_WIDGET_TEXT_WARNING_DEFAULT, UN_WIDGET_TEXT_WARNING_SIMU, UN_WIDGET_TEXT_WARNING_ERROR),
											   warningLetter, warningColor);

// 6.1 if the device is not correclty running (the iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value) 
// or the checking is not enabled then set the letter to O and the color to not valid data

		if ((!bSystemIntegrityAlarmEnabled) || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value))
			{
			warningLetter = UN_WIDGET_TEXT_OLD_DATA;
			warningColor = "unDataNotValid";
			}

		}
	unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, lockVisible);
	if(g_bSystemConnected)
		setMultiValue("Body1", "foreCol", body1Color, "Body1", "fill", "[solid]", "Body1", "backCol", body1BackColor,
					  "WarningText", "text", warningLetter, "WarningText", "foreCol", warningColor,
					  "AlarmText", "text", alarmLetter, "AlarmText", "foreCol", alarmColor,
					  "ControlStateText", "text", controlLetter, "ControlStateText", "foreCol", controlColor,
					  "SelectArea", "foreCol", selectColor, "LockBmp", "visible", lockVisible,
					  "WidgetArea", "visible", true);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetDIDODisconnection
/**
Purpose: animate disconnection for DIDO

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-02
*/

unGenericObject_WidgetDIDODisconnection()
{
  FWDEPRECATED();
  
	unGenericObject_WidgetBody1Disconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetControllerAnimation
/**
Purpose: animate Controller

Parameters: 
	- iOn, int, input, on state
	- iOff, int, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetControllerAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", sBodyColor, "Body1", "fill", "[solid]", "Body1", "backCol", "unWidget_Background",
				  "Body2", "foreCol", sBodyColor, "Body3", "foreCol", sBodyColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetControllerDisconnection
/**
Purpose: animate controller disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetControllerDisconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "fill", "[solid]", "Body1", "backCol", "unWidget_Background",
				  "Body2", "foreCol", "unDataNoAccess", "Body3", "foreCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetCompressorAnimation
/**
Purpose: animate compressor

Parameters: 
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
 
  @deprecated 2018-08-03
*/

unGenericObject_WidgetCompressorAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	string body1Color = sBodyColor, body2BackColor, body3BackColor, body4BackColor, body5BackColor;
	string body6Color, body7Color, body8Color, body9Color;

// 1. Set colors
	body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
	body3BackColor = "unWidget_Background";
	body4BackColor = "unWidget_Background";
	body5BackColor = "unWidget_Background";
	body6Color = body1Color;
	body7Color = body1Color;
	body8Color = body1Color;
	body9Color = body1Color;
	if (body1Color != "unDataNotValid")
		{
		if (iOff == 0)
			{
			body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
			body3BackColor = body1Color;
			body6Color = "unWidget_AlternativeColor";
			body8Color = "unWidget_AlternativeColor";
			if (iOn == 1)
				{
				body4BackColor = body1Color;					// Onst = 1 and OffSt = 0
				body5BackColor = body1Color;
				body7Color = "unWidget_AlternativeColor";
				body9Color = "unWidget_AlternativeColor";
				}
			}
		}
// 2. Animate body
	setMultiValue("Body1", "foreCol", body1Color,
				  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
				  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
				  "Body4", "fill", "[solid]", "Body4", "backCol", body4BackColor,
				  "Body5", "fill", "[solid]", "Body5", "backCol", body5BackColor,
				  "Body6", "foreCol", body6Color, "Body7", "foreCol", body7Color,
				  "Body8", "foreCol", body8Color, "Body9", "foreCol", body9Color);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetCompressorDisconnection
/**
Purpose: animate compressor disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetCompressorDisconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
				  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
				  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
				  "Body4", "backCol", "unWidget_Background", "Body4", "fill", "[solid]",
				  "Body5", "backCol", "unWidget_Background", "Body5", "fill", "[solid]",
				  "Body6", "foreCol", "unDataNoAccess", "Body7", "foreCol", "unDataNoAccess",
				  "Body8", "foreCol", "unDataNoAccess", "Body9", "foreCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAnalog3WayValveAnimation
/**
Purpose: animate Analog 3 way valve

Parameters:
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/
unGenericObject_WidgetAnalog3WayValveAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
  string sBody1Color = sBodyColor, sToBackColor, sOnBackColor, sOffBackColor;

  // 1. Set colors
  sOnBackColor = "unWidget_Background";
  sOffBackColor = "unWidget_Background";
  sToBackColor = "unWidget_Background";
  if (sBody1Color != "unDataNotValid")
  {
    sToBackColor = sBody1Color;
    if ((iOff == 1) && (iOn == 0)) {
      sOffBackColor = sBody1Color;				// OnSt = 0 and OffSt = 1
    }
    else if ((iOff == 0) && (iOn == 1)) {
      sOnBackColor = sBody1Color;					// Onst = 1 and OffSt = 0
    }
    else if ((iOff == 0) && (iOn == 0)) {
      sOnBackColor = sBody1Color;					// Onst = 0 and OffSt = 0
      sOffBackColor = sBody1Color;				// OnSt = 0 and OffSt = 1
    }
  }
  // 2. Animate body
  setMultiValue("Body1", "foreCol", sBody1Color,
                "pos_on",   "fill", "[solid]", "pos_on",   "backCol", sOnBackColor,
                "pos_off",  "fill", "[solid]", "pos_off",  "backCol", sOffBackColor,
                "pos_to", "fill", "[solid]", "pos_to", "backCol", sToBackColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAnalog3WayValveDisconnection
/**
Purpose: animate Analog 3 way valve disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/
unGenericObject_WidgetAnalog3WayValveDisconnection()
{
  FWDEPRECATED();
  
  setMultiValue("Body1", "foreCol", "unDataNoAccess", /*"Body1", "backCol", "unWidget_Background",*/
                "pos_on", "backCol", "unWidget_Background", "pos_on", "fill", "[solid]",
                "pos_off", "backCol", "unWidget_Background", "pos_off", "fill", "[solid]",
                "pos_to", "backCol", "unWidget_Background", "pos_to", "fill", "[solid]");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetValveAnimation
/**
Purpose: animate valve

Parameters:
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetValveAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	string body1Color = sBodyColor, body2BackColor, body3BackColor;

// 1. Set colors
	body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
	body3BackColor = "unWidget_Background";
	if (body1Color != "unDataNotValid")
		{
		if (iOff == 0)
			{
			body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0	
			if (iOn == 1)
				{
				body3BackColor = body1Color;					// Onst = 1 and OffSt = 0
				}
			}
                else if (iOn == 1)                                                        // Onst = 1 and OffSt = 1
                  body2BackColor = body1Color;                                            
		}
// 2. Animate body
	setMultiValue("Body1", "foreCol", body1Color,
				  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
				  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetValveDisconnection
/**
Purpose: animate valve disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetValveDisconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
				  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
				  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAIAOAnimation
/**
Purpose: animate AIAO widget

Parameters:
	- bLocked, bool, input, for select animation
	- sSelectedManager, string, input, for select animation
	- bit32StsReg01, bit32, input, status register value
	- bStsReg01Invalid, bool, input, data status
	- fPosSt, float, input, value to display
	- sFormat, string, input, the format to use to display the data
	- sUnit, string, input, the unit of the data
	- iAlertType, int, input, alert hdl type
	- bIsAlarmActive, bool, input, alert hdl active ?
	- iActState, int, input, alert hdl act state
	- bAckPossible, bool, input, has alert to be acknowleged ?
	- bInvalidBody, bool, input, for body invalid animation
	- bInvalidLetter, bool, input, for widget invalid animation
	- iAlarmRange, int, input, level of alarm
	- iSystemIntegrityAlarmValue, int, input, the value of the systemIntegrity alarm for the PLC: 
							c_unSystemIntegrity_no_alarm_value (0) no alarm PLC is running, 
							c_unSystemIntegrity_alarm_value_level1 (10): alarm PLC is not correctly running
	- bSystemIntegrityAlarmEnabled, bool, input, the systemIntegrity alarm is enabled
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetAIAOAnimation(bool bLocked, string sSelectedManager, bit32 bit32StsReg01, bool bStsReg01Invalid,
									string sFormat, string sUnit, float fPosSt,
									/*int iRangeType, float fMin, float fMax,*/
									int iAlertType, bool bIsAlarmActive, int iActState, bool bAckPossible,
									bool bInvalidBody, bool bInvalidLetter, int iAlarmRange, int iSystemIntegrityAlarmValue,
									bool bSystemIntegrityAlarmEnabled)
{
  FWDEPRECATED();
  
	string formattedValue;
	string warningLetter, warningColor, alarmLetter, alarmColor, controlLetter, controlColor, body1Text, body1Color, selectColor;
	bool lockVisible;
	
//DebugN(/*iRangeType, fMin, fMax, */iAlertType, bIsAlarmActive, iActState, bAckPossible, bInvalidBody, bInvalidLetter, iAlarmRange);
	// 1. Control animation
	body1Color = "unWidget_ControlStateAuto";					// Auto mode (default)
	controlLetter = "";
	if ((!bStsReg01Invalid) && (getBit(bit32StsReg01, UN_STSREG01_FOMOST) == 1))
		{
		body1Color = "unWidget_ControlStateForced";				// Forced mode
		controlLetter = UN_WIDGET_TEXT_CONTROL_FORCED;
		controlColor = "unWidget_ControlStateForced";
		}
	// 2. Range
/*	if (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)		// Range config exists
		{
		if ((fPosSt > fMax) || (fPosSt < fMin))
			{
			body1Color = "unFaceplate_AlarmActive";				// Outside range -> alarm
			}
		}
*/
	// 3. Alarm
	alarmLetter = "";
	if (iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL)
		{
		if (bIsAlarmActive)										// Alarm is active
		{
			if (bAckPossible)
			{
				body1Color = "unWidget_AlarmNotAck";
				switch(iAlarmRange){
					case UN_AIAO_ALARM_5_HH_H_L_LL:
						switch(iActState)
						{
							case UN_AIAO_ALARM_5_RANGE_HH:
								alarmLetter = UN_AIAO_ALARM_HH;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_5_RANGE_H:
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_5_RANGE_L:
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_5_RANGE_LL:
								alarmLetter = UN_AIAO_ALARM_LL;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_4_HH_H_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_4_HH_H_L_RANGE_HH:
								alarmLetter = UN_AIAO_ALARM_HH;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_4_HH_H_L_RANGE_H:
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_4_HH_H_L_RANGE_L:
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_4_H_L_LL:
						switch(iActState)
						{
							case UN_AIAO_ALARM_4_H_L_LL_RANGE_H:
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_4_H_L_LL_RANGE_L:
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_4_H_L_LL_RANGE_LL:
								alarmLetter = UN_AIAO_ALARM_LL;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_3_H_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_3_H_L_RANGE_H:
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_3_H_L_RANGE_L:
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_3_HH_H:
						switch(iActState)
						{
							case UN_AIAO_ALARM_3_HH_H_RANGE_HH:
								alarmLetter = UN_AIAO_ALARM_HH;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_3_HH_H_RANGE_H:
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_3_LL_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_3_LL_L_RANGE_LL:
								alarmLetter = UN_AIAO_ALARM_LL;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							case UN_AIAO_ALARM_3_LL_L_RANGE_L:
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_2_H:
						switch(iActState)
						{
							case UN_AIAO_ALARM_2_H_RANGE_H:
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_2_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_2_L_RANGE_L:
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unWidget_AlarmNotAck";
								break;
							default:
								break;
						}
						break;
				}
			}
			else
			{
				switch(iAlarmRange){
					case UN_AIAO_ALARM_5_HH_H_L_LL:
						switch(iActState)
						{
							case UN_AIAO_ALARM_5_RANGE_HH:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_HH;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_5_RANGE_H:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_5_RANGE_L:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_5_RANGE_LL:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_LL;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_4_HH_H_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_4_HH_H_L_RANGE_HH:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_HH;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_4_HH_H_L_RANGE_H:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_4_HH_H_L_RANGE_L:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_4_H_L_LL:
						switch(iActState)
						{
							case UN_AIAO_ALARM_4_H_L_LL_RANGE_H:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_4_H_L_LL_RANGE_L:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_4_H_L_LL_RANGE_LL:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_LL;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_3_H_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_3_H_L_RANGE_H:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_3_H_L_RANGE_L:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_3_HH_H:
						switch(iActState)
						{
							case UN_AIAO_ALARM_3_HH_H_RANGE_HH:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_HH;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_3_HH_H_RANGE_H:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_3_LL_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_3_LL_L_RANGE_LL:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_LL;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							case UN_AIAO_ALARM_3_LL_L_RANGE_L:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_2_H:
						switch(iActState)
						{
							case UN_AIAO_ALARM_2_H_RANGE_H:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_H;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
					case UN_AIAO_ALARM_2_L:
						switch(iActState)
						{
							case UN_AIAO_ALARM_2_L_RANGE_L:
								body1Color = "unFaceplate_AlarmActive";
								alarmLetter = UN_AIAO_ALARM_L;
								alarmColor = "unFaceplate_AlarmActive";
								break;
							default:
								break;
						}
						break;
				}
			}
		}
		else													// Alarm isn't active
			{
			alarmLetter = UN_WIDGET_TEXT_ALARM_MASKED;
			alarmColor = "unAlarmMasked";
			}
		}
	else														// Alarm config doesn't exist
		{
		alarmLetter = UN_WIDGET_TEXT_ALARM_DOES_NOT_EXIST;
		alarmColor = "unAlarmDoesNotExist";
		}
	// 4. Warning animation
	if (!bStsReg01Invalid)
		{
		unGenericObject_WidgetWarningAnimation(bit32StsReg01, 
					   makeDynInt(UN_STSREG01_FODIPRO, UN_STSREG01_IOSIMUW, UN_STSREG01_IOERRORW),
					   makeDynString(UN_WIDGET_TEXT_WARNING_DEFAULT, UN_WIDGET_TEXT_WARNING_SIMU, UN_WIDGET_TEXT_WARNING_ERROR),
					   warningLetter, warningColor);
		}
	// 5. Select animation
	unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, lockVisible);
	// 6. PosSt value
	formattedValue = unGenericObject_FormatValue(sFormat, fPosSt);
	body1Text = formattedValue + " " + sUnit;
	if (bInvalidBody)
		{
		body1Color = "unDataNotValid";
		}
// 6.1 if the device is not correclty running (the iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value) 
// or the checking is not enabled then set the letter to O and the color to not valid data

	if ((!bSystemIntegrityAlarmEnabled) || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value))
		{
		warningLetter = UN_WIDGET_TEXT_OLD_DATA;
		warningColor = "unDataNotValid";
		}
	// 7. Invalid data

	if (bInvalidLetter)
		{
		warningLetter = UN_WIDGET_TEXT_INVALID;
		warningColor = "unDataNotValid";
		}
	// 8. Animate widget
	if(g_bSystemConnected)
	setMultiValue("Body1", "text", body1Text, /*"Body1", "fill", "[solid]", */
				  "Body1", "foreCol", body1Color, /*"Body1", "backCol", "unWidget_Background",*/
				  "WarningText", "text", warningLetter, "WarningText", "foreCol", warningColor,
				  "AlarmText", "text", alarmLetter, "AlarmText", "foreCol", alarmColor,
				  "ControlStateText", "text", controlLetter, "ControlStateText", "foreCol", controlColor,
				  "SelectArea", "foreCol", selectColor, "LockBmp", "visible", lockVisible, 
				  "WidgetArea", "visible", true);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAIAODisconnection
/**
Purpose: animate disconnection for AIAO

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetAIAODisconnection()
{
  FWDEPRECATED();
  
//	unGenericObject_WidgetBody1Disconnection();
	setMultiValue("Body1", "foreCol", "unDataNoAccess"/*, "Body1", "backCol", "unWidget_Background", "Body1", "fill", "[solid]"*/);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAlarmBodyDisconnection
/**
Purpose: animate alarm body disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetAlarmBodyDisconnection()
{
  FWDEPRECATED();
  
	unGenericObject_WidgetBody1Disconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetMotorAnimation
/**
Purpose: animate motor

Parameters: 
	- bOn, bool, input, on state
	- bOff, bool, input, off state
	- sBodyColor, string, input, body color

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetMotorAnimation(int iOn, int iOff, string sBodyColor)
{
  FWDEPRECATED();
  
	string body1Color = sBodyColor, body2BackColor, body3BackColor, body4BackColor, body5BackColor, body6Color, body7Color;

// 1. Set colors
	body2BackColor = "unWidget_Background";				// OnSt = 0/1 and OffSt = 1
	body3BackColor = "unWidget_Background";
	body4BackColor = "unWidget_Background";
	body5BackColor = "unWidget_Background";
	body6Color = body1Color;
	body7Color = body1Color;
	if (body1Color != "unDataNotValid")
		{
		if (iOff == 0)
			{
			body2BackColor = body1Color;					// OnSt = 0 and OffSt = 0
			body3BackColor = body1Color;
			body6Color = "unWidget_AlternativeColor";
			if (iOn == 1)
				{
				body4BackColor = body1Color;					// Onst = 1 and OffSt = 0
				body5BackColor = body1Color;
				body7Color = "unWidget_AlternativeColor";
				}
			}
                 else if (iOn == 1)
                   {  
                        body2BackColor = body1Color;					// OnSt = 1 and OffSt = 1
			body3BackColor = body1Color;
			body6Color = "unWidget_AlternativeColor";
                   }
		}
// 2. Animate body
	setMultiValue("Body1", "foreCol", body1Color,
				  "Body2", "fill", "[solid]", "Body2", "backCol", body2BackColor,
				  "Body3", "fill", "[solid]", "Body3", "backCol", body3BackColor,
				  "Body4", "fill", "[solid]", "Body4", "backCol", body4BackColor,
				  "Body5", "fill", "[solid]", "Body5", "backCol", body5BackColor,
				  "Body6", "foreCol", body6Color, "Body7", "foreCol", body7Color);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetMotorDisconnection
/**
Purpose: animate motor disconnection

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-08-03
*/

unGenericObject_WidgetMotorDisconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
				  "Body2", "backCol", "unWidget_Background", "Body2", "fill", "[solid]",
				  "Body3", "backCol", "unWidget_Background", "Body3", "fill", "[solid]",
				  "Body4", "backCol", "unWidget_Background", "Body4", "fill", "[solid]",
				  "Body5", "backCol", "unWidget_Background", "Body5", "fill", "[solid]",
				  "Body6", "foreCol", "unDataNoAccess", "Body7", "foreCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetBody1Disconnection
/**
Purpose: animate disconnection for body with only one shape

Parameters: None

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @deprecated 2018-09-17
*/

unGenericObject_WidgetBody1Disconnection()
{
  FWDEPRECATED();
  
	setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background", "Body1", "fill", "[solid]");
}

//---------------------------------------------------------------------------------------------------------------------------------------
