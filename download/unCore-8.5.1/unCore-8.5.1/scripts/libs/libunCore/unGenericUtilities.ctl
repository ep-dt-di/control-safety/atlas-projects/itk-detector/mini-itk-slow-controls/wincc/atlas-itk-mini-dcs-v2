/**
@file unGenericUtilities.ctl
Various functions to simplify some commonly used actions.

@author Cyril Caillaba (EN-ICE-SCD)
@version 1.0.0
@par Creation Date
	January 2012
	
@par Modification History
  
  26/07/2012 Marco Boccioli
  - @jira{IS-792}: improved documentation.
  
*/

#uses "libunCore/unCoreDeprecated.ctl"

//unGenericUtilities_validateXmlFile
/**
 @par Description
  Validates an XML file according to a given Xml Schema Definition (XSD).
 @par Usage
	public 
  
  @param sXmlFilePath  input, path of the XML file to test.
  @param sXsdFilePath  input, path of the XSD file used for validation.
  @param dsExceptions output, the list of exceptions that occur in the function.
  
  @return true if the XML file is correct according to the XML schema, false else or if one of the file is not found.


  
*/
bool unGenericUtilities_validateXmlFile(string sXmlFilePath, string sXsdFilePath, dyn_string &dsExceptions)
{
  
  // Java executable to validate the file
  const string XML_VALIDATOR_JAR = "unGenericUtilities_xmlValidator.jar";
  
  // File where the output of the validation is written
  const string XML_VALIDATION_OUTPUT = "unGenericUtilities_validationResult.txt";
  
  // Those 2 files are supposed to be in the data folder.

  
  
  // Check the presence of the required files.  
  // If one does not exist we cannot proceed and raise an error.
  
  // Java validator
  string sValidator = getPath(DATA_REL_PATH,XML_VALIDATOR_JAR);
  if (!isfile(sValidator))
  {
    string sTemp = getCatStr("unGenericUtilities","7");
    strreplace(sTemp,"VALIDATOR",XML_VALIDATOR_JAR);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sTemp,
        "7"
      );
    return false;
  }
  
  // Xml schema
  string sXsd = sXsdFilePath;
  if (!isfile(sXsd))
  {
    string sTemp = getCatStr("unGenericUtilities","6");
    strreplace(sTemp,"FILE",sXsdFilePath);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sTemp,
        "6"
      );
    return false;
  }
  
  
  if (!isfile(sXmlFilePath))
  {
    string sTemp = getCatStr("unGenericUtilities","8");
    strreplace(sTemp,"FILE",sXmlFilePath);
    fwException_raise(
        dsExceptions,
        "ERROR",
        sTemp,
        "8"
      );
    
    return false;
  }
  
  
  string sOutput = getPath(DATA_REL_PATH) + XML_VALIDATION_OUTPUT;
  if (!isfile(sOutput))
  {
    // If the output file does not exist, we need to create it. For unknown reasons, the command line below seems to not create it by itself.
    file f = fopen(sOutput,"w");
    fclose(f);
  }
  
  // Get a time stamp
  string sCurrentTime = formatTime("%Y%m%d%H%M%S",getCurrentTime());
  
  string sCommand = "java -Xms128m -Xmx256m -jar " + sValidator + " " + sXmlFilePath + " " + sXsd + " " + sCurrentTime + " > " + sOutput;
  int iCommandReturn = system(sCommand);

  string sError;
  bool bRead = fileToString(sOutput, sError);
  if (!bRead)
  {
    string sTemp = getCatStr("unGenericUtilities","10");
    sTemp += sOutput;
    fwException_raise(
        dsExceptions,
        "ERROR",
        sTemp,
        "10"
      );
    return false;
  }
	
  // When the external program returns 0, it means the xml file has been correctly validated
  if (iCommandReturn == 0)
  {
    return true;
  }
  else
  {
    string sValidationError;
    // Check the created file is the right one (i.e. the one with the desired timestamp)
    string sReadTimeStamp = substr(sError,0,strpos(sError,"\n"));
    
    // If it is not, then we cannot know what the error is.
    if (sReadTimeStamp != sCurrentTime)
    {
      sValidationError = "Unknown error.";      
    }
    // Else we return the error (without the timestamp)
    else
    {
      sValidationError = substr(sError,strpos(sError,"\n"));      
    }
    
    sValidationError = getCatStr("unGenericUtilities","9") + sValidationError;
    
    fwException_raise(
      dsExceptions,
      "ERROR",
      sValidationError,
      "XMLVALIDATION-OUTPUTNOTFOUND"
    );
    
    return false;
  }
  
}


//unGenericUtilities_createArchive
/**
 @par Description
  Create a new archive
  
  @par Usage
	public  
	
  @param sArchiveName: string, input: name of the archive to create
  @param dsExceptions: dyn_string, output: list of errors encountered during the execution
  @param iManager: int, input: index of the dp archive to create (_ValueArchive_iManager). Default (-1) : first unused _VALUE_ARCHIVE_XXXX
  @param sSystemName: string, input: system in which create the archive (default: local)
  @param bActivateRedundancy: bool, input: whether the redundancy should be active or not (default: false)
  @param bDoDpConnect: bool, input: to copy the behavior of the default archive creation panel set to true. Will generate warnings if used out the default panel.

*/
unGenericUtilities_createArchive(string sArchiveName, dyn_string &dsExceptions, int iManager = -1, string sSystemName = "", bool bActivateRedundancy = false, bool bDoDpConnect = false)
{  
  
  string sDpName;
  
  if (sSystemName == "")
  {
    sSystemName = getSystemName();
  }
  
  // The DP name will be "_ValueArchive_XXX", we need to find the first possible value of XXX.
  // To that end, we try every number from 0 to see when such a DP does not exist
  int index = 0;
  if (iManager < 0)
  {
    dyn_string dsArchiveDpList = dpNames(sSystemName + "*","_ValueArchive");
    for (int i = 1 ; i <= dynlen(dsArchiveDpList) ; i++)
    {
      if (dynContains(dsArchiveDpList, sSystemName + "_ValueArchive_" + index) == 0)
      {
        break;
      }
      index++;
    }
    
    sDpName = "_ValueArchive_" + index;
  }
  else
  {
    index = iManager;
    sDpName = "_ValueArchive_" + iManager;
    if (dpExists(sSystemName + sDpName))
    {
      string sError = "Error in function unGenericUtilities_createArchive : " + getCatStr("unGenericUtilities","4");
      strreplace(sError, "DATAPOINT", sDpName);
      fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "4"
      );    
      return;
      
    }
  
  }
  
  // Now "_ValueArchive_index" does not exist yet.
  // We cannot use the function _vaCreateArchive since it is made to be used through the appropriate panel (it uses variables from this panel)
  // Thus we create it manually
  dpCreate(sDpName, "_ValueArchive", getSystemId(sSystemName));
  dpCreate(sDpName + "_2", "_ValueArchive", getSystemId(sSystemName)); // Redondancy
  
  // Set the names
  dpSet(sSystemName + sDpName + ".general.arName:_original.._value", sArchiveName);
  dpSet(sSystemName + sDpName + "_2" + ".general.arName:_original.._value", sArchiveName);
  
  // And the archive default values
  string sFourDigitsIndex;
  sprintf(sFourDigitsIndex, "%04d", index);
    
  dpSetWait(
      sSystemName + sDpName + ".setMgmt.fileSwitch.overflowPctSet:_original.._value", 5,
      sSystemName + sDpName + ".size.maxFillPctSet:_original.._value",    95,
      sSystemName + sDpName + ".size.maxDpElSet:_original.._value",       100,
      sSystemName + sDpName + ".size.maxDpElGet:_original.._value",       100,
      sSystemName + sDpName + ".setMgmt.fileSwitch.overlapPeriodSet:_original.._value", 600,
      sSystemName + sDpName + ".setMgmt.fileSwitch.expKeepPeriodSet:_original.._value", 3600,
      sSystemName + sDpName + ".size.maxCorrCacheSet:_original.._value",  4096,
      sSystemName + sDpName + ".size.maxValuesSet:_original.._value",     5000,
      sSystemName + sDpName + ".size.maxValuesGet:_original.._value",     5000,
      sSystemName + sDpName + ".size.maxCacheSet:_original.._value",      16384,
      sSystemName + sDpName + ".arNr:_original.._value",                  index,
      sSystemName + sDpName + ".general.filePathSet:_original.._value",   "",
      sSystemName + sDpName + ".general.fileNameSet:_original.._value",   "AR_"+sFourDigitsIndex+"_%YYYY.%MM.%DD.%HH.%MI.%SS",
      sSystemName + sDpName + ".action.media.hostName:_original.._value", sSystemName,
      sSystemName + sDpName + ".general.filePathGet:_original.._value", "",
      sSystemName + sDpName + ".setMgmt.fileDeletion.keepCountSet:_original.._value", 10 
  );
  
  
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileSwitch.switchTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileCompression.packTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileMerge.mergeTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileDeletion.removeTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + ".action.fileBackup.startTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileSwitch.switchTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileCompression.packTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileMerge.mergeTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + ".setMgmt.fileDeletion.removeTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + ".action.fileBackup.startTimeSet");
  
  
  dpSetWait(
      sSystemName + sDpName + "_2" + ".setMgmt.fileSwitch.overflowPctSet:_original.._value", 5,
      sSystemName + sDpName + "_2" + ".size.maxFillPctSet:_original.._value",    95,
      sSystemName + sDpName + "_2" + ".size.maxDpElSet:_original.._value",       100,
      sSystemName + sDpName + "_2" + ".size.maxDpElGet:_original.._value",       100,
      sSystemName + sDpName + "_2" + ".setMgmt.fileSwitch.overlapPeriodSet:_original.._value", 600,
      sSystemName + sDpName + "_2" + ".setMgmt.fileSwitch.expKeepPeriodSet:_original.._value", 3600,
      sSystemName + sDpName + "_2" + ".size.maxCorrCacheSet:_original.._value",  4096,
      sSystemName + sDpName + "_2" + ".size.maxValuesSet:_original.._value",     5000,
      sSystemName + sDpName + "_2" + ".size.maxValuesGet:_original.._value",     5000,
      sSystemName + sDpName + "_2" + ".size.maxCacheSet:_original.._value",      16384,
      sSystemName + sDpName + "_2" + ".arNr:_original.._value",                  index,
      sSystemName + sDpName + "_2" + ".general.filePathSet:_original.._value",   "",
      sSystemName + sDpName + "_2" + ".general.fileNameSet:_original.._value",   "AR_"+sFourDigitsIndex+"_%YYYY.%MM.%DD.%HH.%MI.%SS",
      sSystemName + sDpName + "_2" + ".action.media.hostName:_original.._value", sSystemName,
      sSystemName + sDpName + "_2" + ".general.filePathGet:_original.._value", "",
      sSystemName + sDpName + "_2" + ".setMgmt.fileDeletion.keepCountSet:_original.._value", 10 
  );
  
  
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileSwitch.switchTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileCompression.packTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileMerge.mergeTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileDeletion.removeTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".action.fileBackup.startTimeGet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileSwitch.switchTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileCompression.packTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileMerge.mergeTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".setMgmt.fileDeletion.removeTimeSet");
  _vaSetTimedStart(sSystemName + sDpName + "_2" + ".action.fileBackup.startTimeSet");  
  
  dpSetWait(
      sSystemName + sDpName + ".files.startTime:_original.._value",makeDynTime(makeTime(2000,1,1)),
      sSystemName + sDpName + "_2.files.startTime:_original.._value",makeDynTime(makeTime(2000,1,1)));   // For redundancy start time is 01/01/2000
  
  if (bDoDpConnect)
  {
    if (bActivateRedundancy)
    {
      dpConnect(
          "_vaArchiveSelectCB", 
          false,
            sSystemName + sDpName + ".general.arName:_online.._value",
            sSystemName + sDpName + ".size.maxDpElGet:_online.._value",
            sSystemName + sDpName + ".size.maxValuesGet:_online.._value",
            sSystemName + sDpName + ".size.maxFillPctGet:_online.._value",
            sSystemName + sDpName + ".size.maxHeapSizeGet:_online.._value",
            sSystemName + sDpName + ".state:_online.._value",
            sSystemName + sDpName + "_2.state:_online.._value");  
    }
    else
    {
      dpConnect(
          "_vaArchiveSelectCB",
          false,
            sSystemName + sDpName + ".general.arName:_online.._value",
            sSystemName + sDpName + ".size.maxDpElGet:_online.._value",
            sSystemName + sDpName + ".size.maxValuesGet:_online.._value",
            sSystemName + sDpName + ".size.maxFillPctGet:_online.._value",
            sSystemName + sDpName + ".size.maxHeapSizeGet:_online.._value",
            sSystemName + sDpName + ".state:_online.._value",
            sSystemName + sDpName + ".arNr:_online.._value");
    }
  }
  
  
  if (dynlen(getLastError()) > 0)
  {
    string sError = "Error in function unGenericUtilities_createArchive : " + getCatStr("unGenericUtilities","3");
    strreplace(sError, "ARCHIVE", sArchiveName);
    fwException_raise(
      dsExceptions,
      "ERROR",
      sError,
      "3"
    );    
    return;
  }
}

//unGenericUtilities_startManager
/**
 @par Description
  Helper function so easily start a manager. Warning, this functions does not check anything, it supposes the manager already exists.
    
  @par Usage
	public
    
  @param sManager: string, input: manager to start (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystemName: string, input: system on which this manager is to be started

*/
unGenericUtilities_startManager(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystemName)
{
  
  const int MANAGER_STARTED = 2;
  const int MANAGER_INIT = 1;
  const int MANAGER_STOPPED = 0;
  const int MANAGER_NOT_ADDED = -1;

  int iTimeNow = period(getCurrentTime());
  bool bManagerIsStarted = false;
  int iIndex;  
  
  unSystemIntergity_manageManager(sManager, sManagerCommand, SYSTEMINTEGRITY_START_COMMAND, iIndex, dsExceptions, sSystemName); 
  
}


//unGenericUtilities_startManagerWait
/**
 @par Description
  Helper function so easily start a manager. Waits for a given time to see if the manager is started.
     
  @par Usage
	public
   
  @param sManager: string, input: manager to start (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystemName: string, input: system on which this manager is to be started
  @param iTimeOut: int, input: time to wait for the manager to start (in seconds)
  @param bTimedOut: bool, output: indicates if the timer expired or not.

*/
unGenericUtilities_startManagerWait(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystemName, int iTimeOut, bool &bTimedOut)
{
  int iTimeNow = period(getCurrentTime());
  bool bManagerIsStarted = false;
  int iIndex;  
  
  // Execute command
  unSystemIntergity_manageManager(sManager, sManagerCommand, SYSTEMINTEGRITY_START_COMMAND, iIndex, dsExceptions, sSystemName); 
  while (!bManagerIsStarted)
  {
    // Wait 2 seconds for the manager to start
    delay(2);
    
    // Check new state
    int iState;
    dyn_string dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, sSystemName);    
    if(dynlen(dsResults)==5)
    {
      switch(dsResults[1])
      {
        case 2:
          bManagerIsStarted = true;
          break;
        default:
          bManagerIsStarted = false;        
          break;
      }               
    }
    else 
    {
      // Manager does not exist      
      string sError = "Error in function unGenericUtilities_startManagerWait : " + getCatStr("unGenericUtilities","5");
      strreplace(sError, "MANAGER", sManager + " " + sManagerCommand);
      fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "5"
      );  
      
      return;
    }
    
    
    // Check timer expiration
    int iTimeThen = period(getCurrentTime());
    if (iTimeThen > iTimeNow + iTimeOut)
    {
      // Expired
      bTimedOut = true;
      return;      
    }
  }
  
  
  bTimedOut = false;
  
}

//unGenericUtilities_removeManager
/**
 @par Description
  Remove the wanted manager
     
  @par Usage
	public
   
  @param sManager: string, input: manager to start (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystemName: string, input: system on which this manager is to be started

*/
unGenericUtilities_removeManager(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystemName)
{
  int iIndex;  
  
  // Execute empty command to get the index
  unSystemIntergity_manageManager(sManager, sManagerCommand, "", iIndex, dsExceptions, sSystemName); 
  
  string sUser;
  string sPwd;
  if (0 != fwInstallation_getPmonInfo(sUser, sPwd))
  {
    fwException_raise(dsExceptions, "ERROR", "unGenericUtilities_removeManager() - Unable to get the user and passwd!", "");		
    return;
  }
  

  
  
  bool failed, disabled;
  string str;
  dyn_mixed managerInfo;
  string sHost, sIP;
  int iPort;
  
  string sSystem = sSystemName;
  if (sSystem=="")	
    sSystem=getSystemName();
	
  unGenericDpFunctions_getHostName(sSystem, sHost, sIP);	
  unGenericDpFunctions_getpmonPort(sSystem, iPort);
  
  if (sHost=="" || iPort==0)
  {
    sHost=getHostname();
    iPort=pmonPort();
  }
  
//   fwInstallationManager_getProperties(sManager, sManagerCommand, managerInfo, sHost, iPort, user, pwd);
  
  string sCommand;
  sprintf(sCommand, "SINGLE_MGR:DEL %d ", iIndex);
  sCommand = sUser + "#" + sPwd + "#" + sCommand;
  
  if(pmon_command(sCommand, sHost, iPort, FALSE, TRUE))
  {
    fwException_raise(
        dsExceptions, 
        "ERROR", 
        "unGenericUtilities_removeManager(): Failed to delete manager: "  + sManager + " " + sManagerCommand,"");
  }
}

//unGenericUtilities_getManagerStartMode
/**
 @par Description
  Returns the start mode of a manager, i.e. always, manual, once
  
@par Usage
	public  
	  
  @param sManager: string, input: manager to start (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystemName: string, input: system on which this manager is to be started
  
  @return the manager start mode, GENERIC_UTILITIES_START_MODE_ALWAYS, GENERIC_UTILITIES_START_MODE_ONCE, GENERIC_UTILITIES_START_MODE_MANUAL, or -1 in case of error

*/
int unGenericUtilities_getManagerStartMode(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystemName)
{
  int iIndex;  
  
  // Execute empty command to get the index
  unSystemIntergity_manageManager(sManager, sManagerCommand, "", iIndex, dsExceptions, sSystemName); 
  
  // Check new state
  int iState;
  dyn_string dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, sSystemName);    
  if(dynlen(dsResults)==5)
  {
    return dsResults[3];              
  }
  else 
  {
    // Manager does not exist      
    string sError = "Error in function unGenericUtilities_getManagerStartMode : " + getCatStr("unGenericUtilities","5");
    strreplace(sError, "MANAGER", sManager + " " + sManagerCommand);
    fwException_raise(
      dsExceptions,
      "ERROR",
      sError,
      "5"
    );  
      
    return -1;
  }  
}

//unGenericUtilities_stopManagerWait
/**
 @par Description
  Helper function so easily stop a manager. Waits until the program stop for a given time, if it is not stopped after this period a kill signal is sent.
  
  @par Usage
	public
    
  @param sManager: string, input: manager to start (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystem: string, input: system on which this manager is to be stopped
  @param iTimeOut: int, input: if set to a value > 0, the function will wait untill the manager is stopped up to iTimeOut seconds
  
  
  @return When timeout value is set, returns true if the manager is properly stopped, or false if it failed to stop. When no timeout is set, useless value.

*/
unGenericUtilities_stopManagerWaitKill(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystem, int iTimeOutKill = 10)
{
  
/*
  Reminder:
  Started = 2
  Init = 1
  Stopped = 0    
*/
  
  int iTimeNow = period(getCurrentTime());
  bool bManagerIsStopped = false;
  int iIndex;  
  
  // Execute command
  unSystemIntergity_manageManager(sManager, sManagerCommand, SYSTEMINTEGRITY_STOP_COMMAND, iIndex, dsExceptions, sSystem);  
  while (!bManagerIsStopped)
  {
    // Wait 2 seconds for the manager to stop
    delay(2);
    
    // Check new state
    int iState;
    dyn_string dsResults = unSystemIntergity_getManagerState(iIndex, iState, dsExceptions, sSystem);    
    if(dynlen(dsResults)==5)
    {
      switch(dsResults[1])
      {
        case 0:
          bManagerIsStopped = true;
          break;
        default:
          bManagerIsStopped = false;        
          break;
      }               
    }
    else 
    {
      // Manager does not exist      
      string sError = "Error in function unGenericUtilities_stopManagerWaitKill : " + getCatStr("unGenericUtilities","5");
      strreplace(sError, "MANAGER", sManager + " " + sManagerCommand);
      fwException_raise(
        dsExceptions,
        "ERROR",
        sError,
        "5"
      );  
      
      return;
    }
    
    
    // Check timer expiration
    int iTimeThen = period(getCurrentTime());
    if (iTimeThen > iTimeNow + iTimeOutKill)
    {
      // Expired => force kill
      unSystemIntergity_manageManager(sManager, sManagerCommand, SYSTEMINTEGRITY_KILL_COMMAND, iIndex, dsExceptions, sSystem);  
      bManagerIsStopped = true; 
    }
  }  
}


//unGenericUtilities_setManagerManual
/**
 @par Description
  Helper function to set a manager start mode to manual. No check is done, the manager is supposed to exist.

  @par Usage
	public  
	  
  @param sManager: string, input: manager to change (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystem: string, input: system on which this manager is to be changed

*/
unGenericUtilities_setManagerManual(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystem)
{
  int iIndex;  
  unSystemIntergity_manageManager(sManager, sManagerCommand, SYSTEMINTEGRITY_SETMANUAL_COMMAND, iIndex, dsExceptions, sSystem);    
}


//unGenericUtilities_setManagerAlways
/**
 @par Description
  Helper function to set a manager start mode to always. No check is done, the manager is supposed to exist.

  @par Usage
	public  
	  
  @param sManager: string, input: manager to change (i.e. WCCOActrl, WCCOAui, ...)
  @param sManagerCommand: string, input: command line to add to the manager (e.g. -f myscript.lst)
  @param dsExceptions: dyn_string, output: list of encountered errors
  @param sSystem: string, input: system on which this manager is to be changed

*/
unGenericUtilities_setManagerAlways(string sManager, string sManagerCommand, dyn_string &dsExceptions, string sSystem)
{
  int iIndex;  
  unSystemIntergity_manageManager(sManager, sManagerCommand, SYSTEMINTEGRITY_SETALWAYS_COMMAND, iIndex, dsExceptions, sSystem);    
}
