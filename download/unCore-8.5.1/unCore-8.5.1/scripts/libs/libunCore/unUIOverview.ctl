// Debug flag
const string UN_UI_OVERVIEW_DEBUG = "unUIOverview_debug";

/** Structure to store data of a distributed system.
  */
struct UnUIOverviewDistSystem{
  string name;
  int number;
  string hostname;
  int port;
};

/** Returns UnUIOverviewDistSystem objects with distributed systems data.
  * @return List of UnUIOverviewDistSystem objects
  */
dyn_anytype unUIOverview_getDistSystemsList(){
  dyn_string distSystemsNames, distSystemsHosts;
  dyn_int distSystemsNums, distSystemsPorts;
  unDistributedControl_getAllDeviceConfig(distSystemsNames, distSystemsNums, distSystemsHosts, distSystemsPorts);

  dyn_anytype distSystemsList;
	int distSystemsLen = dynlen(distSystemsNames);
	for(int i=1;i<=distSystemsLen;i++){
    distSystemsList[i] = unUIOverview_getDistSystem(
        unUIOverview_formatDistSystemName(distSystemsNames[i]),
        distSystemsNums[i], distSystemsHosts[i], distSystemsPorts[i]);
	}
	return distSystemsList;
}

/** Creates UnUIOverviewDistSystem object containg data of distributed system.
  * @param name (in)  Distributed system name
  * @param number (in)  Distributed system number
  * @param hostname (in)  Hostname
  * @param port (in)  Dist manager port number
  * @return Distributed system data in UnUIOverviewDistSystem object
  */
UnUIOverviewDistSystem unUIOverview_getDistSystem(string name, int number, string hostname, int port){
  UnUIOverviewDistSystem distSystem;
  distSystem.name = name;
  distSystem.number = number;
  distSystem.hostname = hostname;
  distSystem.port = port;
  return distSystem;
}

/** Puts each property of given UnUIOverviewDistSystem objects in a separate list.
  * @param distSystemsList (in)  List of UnUIOverviewDistSystem objects
  * @param distSystemsNames (out)  Returned list of distributed systems names
  * @param distSystemsNums (out)  Returned list of distributed systems numbers
  * @param distSystemsHosts (out)  Returned list of hostnames
  * @param distSystemsPorts (out)  Returned list of port numbers
  */
void unUIOverview_distSystemsToPropertiesList(const dyn_anytype &distSystemsList, dyn_string &distSystemsNames,
                                              dyn_int &distSystemsNums, dyn_string &distSystemsHosts, dyn_int &distSystemsPorts){
  int distSystemsListLen = dynlen(distSystemsList);
  for(int i=1;i<=distSystemsListLen;i++){
    distSystemsNames[i] = distSystemsList[i].name;
    distSystemsNums[i] = distSystemsList[i].number;
    distSystemsHosts[i] = distSystemsList[i].hostname;
    distSystemsPorts[i] = distSystemsList[i].port;
  }
}

/** Returns given distributed system name with appended trailing colon if not yet there
  * @param distSystemName (in)  Distributed system name
  * @return Distributed system name with trailing colon
  */
private string unUIOverview_formatDistSystemName(string distSystemName){
  if(strpos(distSystemName, ":") < 0){
    distSystemName = distSystemName + ":";
  }
  return distSystemName;
}

//-------------------------------------------------------------------------------------------------

/** Returns list of given length and with all elements set to given value.
  * @param value (in)  Value to set
  * @param len (in)  List length
  * @return List of given length initialized with value.
  */
dyn_anytype unUIOverview_dynInitWithValue(anytype value, int len){
  dyn_anytype dynArray;
  for(int i=1;i<=len;i++){
    dynArray[i] = value;
  }
  return dynArray;
}

/** Returns list of given length and with all elements having default values.
  * @param len (in)  List length
  * @return List of given length with default values
  */
dyn_anytype unUIOverview_dynInitDefault(int len){
  dyn_anytype dynArray;
  anytype value;
  dynArray[len] = value;
  return dynArray;
}

/** Removes first occurence of given element from the list.
  * @param list (out)  List of elements
  * @param element (in)  Element to be removed
  */
void unUIOverview_removeElementFromList(dyn_anytype &list, anytype element){
  int elementPosition = dynContains(list, element);
  if(elementPosition > 0){
    dynRemove(list, elementPosition);
  }
}

//-------------------------------------------------------------------------------------------------

/** Checks whether system with given number is a local system.
  * @param systemNumber (in)  Number of the system
  * @return true if it is a local system, false otherwise
  */
bool unUIOverview_isLocalSystem(int systemNumber){
  return (getSystemId() == systemNumber);
}
