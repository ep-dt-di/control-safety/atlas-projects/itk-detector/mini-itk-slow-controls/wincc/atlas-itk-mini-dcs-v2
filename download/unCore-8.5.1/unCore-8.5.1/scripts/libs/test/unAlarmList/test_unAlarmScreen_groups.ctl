 
#uses "fwUnitTestComponentAsserts.ctl" 
#uses "unAlarmScreenGroups.ctl"


//test DP 
//cpc632:un-PLC_TEST_BEN-CPC_TEST_BEN_S7-CPC_Local-00001

/*
 * 
 * When testing the alarm screen grouping, one should make
 * sure to test all combination of filter state (*, PENDING, UNACK, U+P) 
 * and with the different alarm circle CAME/UNACK, CAME/ACK, WENT/UNACK, 
 * 
 * These tests should obviously be automated :)
 * 
 */

//global variable declared in a pnl file...
mapping g_mAlertStateInfo;

/** 
 * @brief Heper function to initialize the global mapping with some fake alarms
 * @return value of type 'void'
 */
private void _testunAlarmScreen_groups_calculateCounters_helper()
{
	mappingClear(g_mAlertStateInfo);
	dyn_anytype daAlertState;
	
	dynAppend(daAlertState, (time)"2013.02.02 11:40:18.150000000");
	dynAppend(daAlertState, 80);
	dynAppend(daAlertState, 1);
	dynAppend(daAlertState, 1);
	dynAppend(daAlertState, "alertCamAckn");
	g_mAlertStateInfo["cpc632:un-PLC_TEST_BEN-CPC_TEST_BEN_S7-CPC_AnalogAlarm-00004.ProcessInput.LLAlSt:_alert_hdl."] = daAlertState;
	dynClear(daAlertState);
	
	dynAppend(daAlertState, (time)"2015.02.02 11:41:51.679000000");
	dynAppend(daAlertState, 80);
	dynAppend(daAlertState, 1);
	dynAppend(daAlertState, 0);
	dynAppend(daAlertState, "alertCamUna");
	g_mAlertStateInfo["cpc632:un-PLC_TEST_BEN-CPC_TEST_BEN_S7-CPC_Local-00001.ProcessInput.PosAl:_alert_hdl."] = daAlertState;
	dynClear(daAlertState);

}

/** 
 * @brief Heper function to initialize a filter definition
 * @return value of type 'void'
 */
private dyn_anytype _testunAlarmScreen_groups_buildGroupDefinition_helper()
{
	dyn_anytype daFilter;
	
	daFilter[UNALARMSCREEN_GROUPS_INDEX_ID] = 1;
	daFilter[UNALARMSCREEN_GROUPS_INDEX_CHILDREN] = makeDynAnytype();
	daFilter[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD] = 0;
	daFilter[UNALARMSCREEN_GROUPS_INDEX_FILTER_REF] = "testS7_Pending";
	daFilter[UNALARMSCREEN_GROUPS_INDEX_TITLE] = "testS7_Pending";
	daFilter[UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME] = (time)"2014.03.23 11:21:05.730000000";
	
	return daFilter;
}

//-------------------------------------------------------

/**
@brief Basic test of counters
*/ 
void testunAlarmScreen_groups_calculateCounters()
{
  	dyn_anytype daGroup = _testunAlarmScreen_groups_buildGroupDefinition_helper();
  	
  	//fill the mapping with fake alarm
  	_testunAlarmScreen_groups_calculateCounters_helper();
  	
	int iActiveAlarmCount, expected_iActiveAlarmCount = 2;
	int iUnackAlarmCount, expected_iUnackAlarmCount = 1;
	int iNewAlarmCount, expected_iNewAlarmCount = 1;
	string sDate, expected_sDate = "2015.02.02 - 11:41:51";
	string sInfoText, expected_sInfoText = "";
	string sBackColour, expected_sBackColour = "alertCamUna"; 
  	
  	_unAlarmScreen_groups_calculateCounters(daGroup, 
											  iActiveAlarmCount,
											  iUnackAlarmCount,
											  iNewAlarmCount,
											  sDate,
											  sInfoText,
											  sBackColour);
											  
	assertEqual(expected_iActiveAlarmCount, iActiveAlarmCount, "Error in iActiveAlarmCount");
	assertEqual(expected_iUnackAlarmCount, iUnackAlarmCount, "Error in iUnackAlarmCount");
	assertEqual(expected_iNewAlarmCount, iNewAlarmCount, "Error in iNewAlarmCount");
	assertEqual(expected_sDate, sDate, "Error in sDate");
	assertEqual(expected_sInfoText, sInfoText, "Error in sInfoText");
	assertEqual(expected_sBackColour, sBackColour, "Error in sBackColour"); 
											 
}