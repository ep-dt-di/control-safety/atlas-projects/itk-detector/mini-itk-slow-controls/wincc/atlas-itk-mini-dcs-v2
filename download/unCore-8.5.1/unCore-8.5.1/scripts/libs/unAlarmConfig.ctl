/**@file
  
// unAlarmConfig.ctl
This library is used by the unicos programs to update alarm configuration on DPE.

@par Creation Date
  13/09/2002

@par Modification History
  - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity
  
@par Constraints
  . operating system: WXP and Linux.
  . distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Vincent Forest (LHC-IAS)
*/

//@{

//------------------------------------------------------------------------------------------------------------------------
// unAlarmConfig_acknowledge
/** Acknowledge current alarm

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpAlarmName input, DPE whose alarm must be acknowledged
@param exceptionInfo output, for errors
*/
unAlarmConfig_acknowledge(string sDpAlarmName, dyn_string &exceptionInfo)
{
  bool isSomethingToAck;
  bool isAlarmLocked;
  int iDpGet, alarmType = DPCONFIG_NONE;
  dyn_errClass error;
  
  // 1. Get current active range, lock state and ackowledge state
  if (dpExists(sDpAlarmName + ":_alert_hdl.._type"))
  {
    dpGet(sDpAlarmName + ":_alert_hdl.._type", alarmType);
    if (alarmType == DPCONFIG_NONE)
    {
      fwException_raise(exceptionInfo,"EXPERTINFO","unAlarmConfig_acknowledge : " + sDpAlarmName + getCatStr("unSystemIntegrity","UNKNOWNALARM"),"");
      return;
    }
    
    iDpGet = dpGet(  sDpAlarmName + ":_lock._alert_hdl._locked", isAlarmLocked,
                     sDpAlarmName + ":_alert_hdl.._ack_possible",isSomethingToAck);
    if (iDpGet < 0)
      fwException_raise(exceptionInfo,"ERROR","unAlarmConfig_acknowledge: " + sDpAlarmName + " " + getCatStr("unSystemIntegrity","DPGETFAILED"),"");
    else
    {
      // 2. Acknowledge alarm if lock state = false
      if (isSomethingToAck)
      {
        if(isAlarmLocked)
          fwException_raise(exceptionInfo,"EXPERTINFO","unAlarmConfig_acknowledge: " + sDpAlarmName + " " + getCatStr("unSystemIntegrity","ALARMLOCKED"),"");
        else
        {
          dpSetWait(sDpAlarmName + ":_alert_hdl.._ack", DPATTR_ACKTYPE_MULTIPLE);
          error = getLastError();
          if(dynlen(error) > 0)
          {
            throwError(error);
            fwException_raise(exceptionInfo,"ERROR","unAlarmConfig_acknowledge: " + sDpAlarmName + " " + getCatStr("unSystemIntegrity","ACKFAILED"),"");
          }
        }
      }
    }
  }
  else
    fwException_raise(exceptionInfo,"ERROR","unAlarmConfig_acknowledge: " + sDpAlarmName + ":_alert_hdl.._type" + " " + getCatStr("para","dpnotexists"),"");  
}

//------------------------------------------------------------------------------------------------------------------------
// unAlarmConfig_mask
/** mask / unmask current alarm

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpAlarmName input, DPE whose alarm must be masked / unmasked
@param bMasked input, true if alarm must be masked, false if alarm must be unmasked
@param exceptionInfo output, for errors
*/
unAlarmConfig_mask(string sDpAlarmName, bool bMasked, dyn_string &exceptionInfo)
{
  bool isAlarmLocked, isAlarmActive;
  int iDpGet, alarmType = DPCONFIG_NONE;
  dyn_errClass error;
  
  // 1. Get alarm mask state and lock state
  dpGet(sDpAlarmName + ":_alert_hdl.._type", alarmType);
  if (alarmType == DPCONFIG_NONE)
  {
    fwException_raise(exceptionInfo,"EXPERTINFO","unAlarmConfig_mask : " + sDpAlarmName + getCatStr("unSystemIntegrity","UNKNOWNALARM"),"");
    return;
  }
  iDpGet = dpGet(sDpAlarmName + ":_alert_hdl.._active",isAlarmActive,
             sDpAlarmName + ":_lock._alert_hdl._locked", isAlarmLocked);
  if (iDpGet < 0)
  {
    fwException_raise(exceptionInfo,"ERROR","unAlarmConfig_mask : " + sDpAlarmName + getCatStr("unSystemIntegrity","DPGETFAILED"),"");
  }
  else
  {
    
  // 2. Set alarm mask state
    if (isAlarmActive == bMasked)
    {
      if(isAlarmLocked)
      {
        fwException_raise(exceptionInfo,"EXPERTINFO","unAlarmConfig_mask : " + sDpAlarmName + getCatStr("unSystemIntegrity","ALARMLOCKED"),"");
      }
      else
      {
        dpSetWait(sDpAlarmName + ":_alert_hdl.._ack", DPATTR_ACKTYPE_MULTIPLE);
        dpSetWait(sDpAlarmName + ":_alert_hdl.._active",!bMasked);
        error = getLastError();
        if(dynlen(error) > 0) {
          throwError(error);
          if (bMasked)
          {
            fwException_raise(exceptionInfo,"ERROR","unAlarmConfig_mask : " + sDpAlarmName + getCatStr("unSystemIntegrity","MASKFAILED"),"");
          }
          else
          {
            fwException_raise(exceptionInfo,"ERROR","unAlarmConfig_mask : " + sDpAlarmName + getCatStr("unSystemIntegrity","UNMASKFAILED"),"");
          }
        }
      }
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------


//@}
