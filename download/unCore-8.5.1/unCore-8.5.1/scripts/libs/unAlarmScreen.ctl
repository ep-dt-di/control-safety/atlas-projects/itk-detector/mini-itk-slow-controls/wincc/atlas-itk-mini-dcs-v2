/**
  @file unAlarmScreen.ctl
  
  @par Description:
  This library contains all the functions used by the UNICOS alarm screen.
  Most of the code comes from the former UNICOS libraries but has been rearranged and cleaned.
  
  @par Creation Date
	05/02/2013

  @par Modification History
  IS-1952 - JCT: Modify the function unAlarmScreen_invokedAESUserFunc 
                 to allow alarm acknowledgement by clicking on the ack status cell


  @author 
	Cyril Caillaba (EN-ICE-SCD)

  UNICOS
   Copyright CERN 2013 all rights reserved
*/
#uses "fwGeneral/fwProgressBar.ctl"
#uses "fwAlarmHandling/fwAlarmScreenGeneric.ctl"
#uses "unAlarmScreenGroups.ctl"
#uses "unPanel.ctl"
#uses "unSendMessage.ctl"
#uses "ctrlEventList"

// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// -------------------------------- CONSTANTS------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------

const string  UNALARMSCREEN_PANEL_FILE_NAME           = "vision/unAlertPanel/unAlertScreen.pnl";    // Name of the alarm panel file.
const string  UNALARMSCREEN_PANEL_OLD_FILE_NAME       = "vision/unAlertPanel/unAlertScreenOld.pnl"; // Name of the old alarm panel file.
const int     UNALARMSCREEN_PANEL_ALL_ALARMS          = 1;                                          // Value for unAlarmScreen_g_iAlarmScreenType. Alarm panel is the classic UNICOS alarm list.
const int     UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY    = 2;                                          // Value for unAlarmScreen_g_iAlarmScreenType. Alarm panel is the system integrity alarm list.
const int     UNALARMSCREEN_PANEL_NON_INITIALIZED     = -1;                                         // Value for unAlarmScreen_g_iAlarmScreenType. Alarm panel type is not initialized yet.
const int     UNALARMSCREEN_DELAY_REFRESH             = 2;                                          // Delay for refresh (milliseconds).
const int     UNALARMSCREEN_TIMEOUT                   = 100;                                        // Time-out value (milliseconds).
const int     UNALARMSCREEN_DELAY                     = 100;                                        // Common delay (milliseconds).
const int     UNALARMSCREEN_POPUPMENU_INDEX_COPY_LINE = 100000;                                     // Index of the right click pop up menu for line copy/paste.
const string  UNALARMSCREEN_ACTIVE_FILTER_LABEL       = "Some alarms might be hidden";              // Text to show when a filter is active.


// -----------------------------
// ---------- Widgets ----------
// -----------------------------
const string  UNALARMSCREEN_WIDGET_FILTER_APPLICATION = "unAlarmScreenrefAlertFilter.unAlarmScreen_applicationSelection";       // Combo-box widget for application selection.
const string  UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE = "unAlarmScreenrefAlertFilter.unAlarmScreen_deviceTypeSelection";        // Combo-box widget for device type selection.
const string  UNALARMSCREEN_WIDGET_FILTER_NATURE      = "unAlarmScreenrefAlertFilter.unAlarmScreen_natureSelection";            // Combo-box widget for nature selection.
const string  UNALARMSCREEN_WIDGET_FILTER_DOMAIN      = "unAlarmScreenrefAlertFilter.unAlarmScreen_domainSelection";            // Combo-box widget for domain selection.
const int     UNALARMSCREEN_PANEL_DEFAULT_WIDTH       = 1269;
const int     UNALARMSCREEN_PANEL_DEFAULT_HEIGHT      = 835;
const int     UNALARMSCREEN_PANEL_FOOTER_HEIGHT       = 35;

// -----------------------------
// --------- Datapoints --------
// -----------------------------
const string  UNALARMSCREEN_PROPERTIES_DP             = "_AESProperties_unAlertScreen";             // UNICOS AES properties DP.
const string  UNALARMSCREEN_DP_FILTER_PREFIX          = "unAlertPanel_Filter_Objects_";             // Filter DP prefix.
const string  UNALARMSCREEN_DP_FILTER_TYPE            = "_UnAlertPanel_Filters";                    // Filter DP type.
const string  UNALARMSCREEN_DP_USE_OLD_PANEL          = "_unApplication.unAlerts.useOldAlarmPanel"; // DP to store whether to use the old alarm panel or not.


// -----------------------------
// ---------- Columns ----------
// -----------------------------
const string  UNALARMSCREEN_COLUMN_ID_SHORT_SIGN      = "abbreviation";
const string  UNALARMSCREEN_COLUMN_ID_PRIORITY        = "priority";
const string  UNALARMSCREEN_COLUMN_ID_UTC_TIME        = "UTCtimeStr";
const string  UNALARMSCREEN_COLUMN_ID_LOCAL_TIME      = "timeStr";
const string  UNALARMSCREEN_COLUMN_ID_ALIAS           = "alias";
const string  UNALARMSCREEN_COLUMN_ID_DESCRIPTION     = "description";
const string  UNALARMSCREEN_COLUMN_ID_DOMAIN          = "domain";
const string  UNALARMSCREEN_COLUMN_ID_NATURE          = "nature";
const string  UNALARMSCREEN_COLUMN_ID_NAME            = "name";
const string  UNALARMSCREEN_COLUMN_ID_VALUE           = "value";
const string  UNALARMSCREEN_COLUMN_ID_DIRECTION       = "direction";
const string  UNALARMSCREEN_COLUMN_ID_ACK             = "acknowledge";
const string  UNALARMSCREEN_COLUMN_ID_S               = "s";
const string  UNALARMSCREEN_COLUMN_ID_VTIME           = "__V_time";
const string  UNALARMSCREEN_COLUMN_ID_SYSTEM_NAME     = "__V_sysName";
const string  UNALARMSCREEN_COLUMN_ID_ACKNOWLEDGEABLE = "__V_ackable";
const string  UNALARMSCREEN_COLUMN_ID_COMMENTS        = "__V_comments";
const string  UNALARMSCREEN_COLUMN_ID_OLDEST_ACK      = "__V_oldestAck";
const string  UNALARMSCREEN_COLUMN_ID_ACK_STATE       = "__V_ackState";
const string  UNALARMSCREEN_COLUMN_ID_DPID            = "__V_dpid";
const string  UNALARMSCREEN_COLUMN_ID_COUNT           = "__V_count";


const string  UNALARMSCREEN_FILTER_DELIMITER          = "~";
const string  UNALARMSCREEN_LIST_NAME                 = "List...";
const string  UNALARMSCREEN_FILTER_ALL                = "*";
const string  UNALARMSCREEN_FILTER_DEFAULT            = "0~100~~*~*~*~*~*~*~1~*~*~*";
const string  UNALARMSCREEN_DESCRIPTION_DELIMITER     = "~";
const string  UNALARMSCREEN_FILTER_COMBO_DELIMITER    = ";";


// -----------------------------
// ------- Filter content ------
// -----------------------------
const int     UNALARMSCREEN_FILTER_FIELD_TIMERANGE    = 1;
const int     UNALARMSCREEN_FILTER_FIELD_MAXLINES     = 2;
const int     UNALARMSCREEN_FILTER_FIELD_TIMEREQUEST  = 3;
const int     UNALARMSCREEN_FILTER_FIELD_ALIAS        = 4;
const int     UNALARMSCREEN_FILTER_FIELD_DESCRIPTION  = 5;
const int     UNALARMSCREEN_FILTER_FIELD_DOMAIN       = 6;
const int     UNALARMSCREEN_FILTER_FIELD_NATURE       = 7;
const int     UNALARMSCREEN_FILTER_FIELD_NAME         = 8;
const int     UNALARMSCREEN_FILTER_FIELD_ALERTTEXT    = 9;
const int     UNALARMSCREEN_FILTER_FIELD_ALERTSTATE   = 10;
const int     UNALARMSCREEN_FILTER_FIELD_OBJECT       = 11;
const int     UNALARMSCREEN_FILTER_FIELD_SYSTEMS      = 12;
const int     UNALARMSCREEN_FILTER_FIELD_LENGTH       = 12;


// --------------------------------------------
// ----------- Managing the date/time picker --
// ---------------------------------------------
const string    UNALARMSCREEN_DATETIMEPICKER_PANELNAME                  = "DATE_TIME_WIDGET"; //name of the panel used with addSymbol to add the date time picker
const string    UNALARMSCREEN_DATETIMEPICKER_WIDGETNAME1                = "startTimeField" ; //name of a widget of date/time picker to check if the ref panel is present or not
/*
  This is a list of variables defined for the alert panel but not used by the panel itself (can be used to open the panel or in the alarm row)  
  One day they will have to be updated to get rid of the old alarm screen.
  
  const string  UN_ALERTPANEL_TITLE         = "PROCESS Alarm List";
  const string  UN_ALERTPANEL_SYSTEM_TITLE  = "SYSTEM Alarm List";
  const string  UN_ALERT_AES_ROW            = "_AESProperties_unAlertRow";
  const string  UN_ALERT_AES_SCREEN         = "_AESProperties_unAlertScreen";
*/


// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ----------------------------- GLOBAL VARIABLES--------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// Note: only one instance of the UNICOS alarm panel can be opened at once, that's why it is possible to use global variables.


global int    unAlarmScreen_g_iAlarmScreenType = UNALARMSCREEN_PANEL_NON_INITIALIZED; // Indicates whether the panel is for system integrity or normal one.
global bool   unAlarmScreen_g_bGranted;                                               // Access is granted.
global bool   unAlarmScreen_g_bState;                                                 // State enabled/disabled.
global bool   unAlarmScreen_g_bUTC;                                                   // Using UTC time or not (local).
global bool   unAlarmScreen_g_bAck;                                                   // It is possible to ack the alarms (set to false when in history mode).


/**
  @par Description:
  Initialize the view of the UNICOS alarm screen.
  
  @par Usage:
  Internal.
  
  @param[in]  iAlarmScreenType  int,  The type of the alarm screen: UNALARMSCREEN_PANEL_ALL_ALARMS or UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY.
*/
void        unAlarmScreen_initUIElements(int iAlarmScreenType)
{
  // If panel type has not been set, by default it is set to classic alert panel
  if (-1 == iAlarmScreenType)
  {
    unAlarmScreen_g_iAlarmScreenType = UNALARMSCREEN_PANEL_ALL_ALARMS;
  }
  else
  {
    unAlarmScreen_g_iAlarmScreenType = iAlarmScreenType;
  }
  
  dyn_string dsSystemNames;
  dyn_string dsNames;
  dyn_string dsHost;
  dyn_string dsApplication;
  
  string sFilter;
  int iXPos, iYPos;
  
  dyn_int diPort;
  
  dyn_uint duSystemIds;


  // 1. set global
  synchronized(g_dsTreeApplication) 
  {    
    if (UNALARMSCREEN_PANEL_ALL_ALARMS == unAlarmScreen_g_iAlarmScreenType)
    {
      // Init device type list 
      dyn_string dsDeviceTypeList;
      unGenericDpFunctions_getUnicosObjects(dsDeviceTypeList);
      dynUnique(dsDeviceTypeList);        
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, true);
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, dsDeviceTypeList);
      fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, makeDynString(), true);
              
      // Init alert text list
      filter_AlertText.items(makeDynString(UNALARMSCREEN_FILTER_ALL, "LL", "L", "H", "HH", "Bad", "Ok"));
      filter_AlertText.enabled = true;
      
      // Init alert state list
      filter_AlertState.items(makeDynString(UNALARMSCREEN_FILTER_ALL, "Unacknowledged", "Pending", "Unack. + Pending"));
      filter_AlertState.enabled = true;
    
      // Init alert name list
      if (dpExists(UN_APPLICATION_DPNAME)) // Always local
      {
        dpGet(UN_APPLICATION_DPNAME + ".unAlerts.description.names", dsNames);
      }    
      dynInsertAt(dsNames, UNALARMSCREEN_FILTER_ALL, 1);
      dynUnique(dsNames);
      filter_Name.enabled = true;
      filter_Name.items(dsNames);  

      
      unDistributedControl_getAllDeviceConfig(dsSystemNames, duSystemIds, dsHost, diPort);          // System names        
    
      dynInsertAt(dsSystemNames, UNALARMSCREEN_FILTER_ALL, 1);
      dynAppend(dsSystemNames, UNALARMSCREEN_LIST_NAME);
      dynUnique(dsSystemNames);      
        
      // Init domain list
      // Remove "*" and "List"
      dyn_string dsDomainList = g_dsTreeSubsystem1;
      int iAllPos = dynContains(dsDomainList, UNALARMSCREEN_FILTER_ALL);
      if (iAllPos > 0)
      {
        dynRemove(dsDomainList, iAllPos);
      }      
      int iListPos = dynContains(dsDomainList, UNALARMSCREEN_LIST_NAME);
      if (iListPos > 0)
      {
        dynRemove(dsDomainList, iListPos);
      }      
      
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, true);   
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, dsDomainList);
      fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, makeDynString(), true);
      
      // Init nature list 
      // Remove "*" and "List"
      dyn_string dsNatureList = g_dsTreeSubsystem2;
      iAllPos = dynContains(dsNatureList, UNALARMSCREEN_FILTER_ALL);
      if (iAllPos > 0)
      {
        dynRemove(dsNatureList, iAllPos);
      }      
      iListPos = dynContains(dsNatureList, UNALARMSCREEN_LIST_NAME);
      if (iListPos > 0)
      {
        dynRemove(dsNatureList, iListPos);
      }    
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_NATURE, true);   
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_NATURE, dsNatureList);
      fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_NATURE, makeDynString(), true);
    
      // Init alias field
      filter_Alias.enabled = true;
          
      // Init description field
      filter_Description.enabled = true;
    
      // Init application list
      dsApplication = g_dsTreeApplication;
      
      // Load application filter
      if (g_unicosHMI_bApplicationFilterInUse)
      {
        synchronized(g_unicosHMI_dsApplicationList)
        {
          // The "*" disappears, which is necessary to ignore unwanted applications
          dsApplication = dynIntersect(dsApplication, g_unicosHMI_dsApplicationList);    
                
          // If only not visible (not connected) applications are filtered display the information
          if (dynlen(dsApplication) < 1)
          {
            dynAppend(dsApplication,"--Empty filter--");
          }
          else
          {        
            dynInsertAt(dsApplication, "*", 1);
            dynAppend(dsApplication, UNALARMSCREEN_LIST_NAME);
          }
        }
      }
      
      iAllPos = dynContains(dsApplication, UNALARMSCREEN_FILTER_ALL);
      if (iAllPos > 0)
      {
        dynRemove(dsApplication, iAllPos);
      }      
      iListPos = dynContains(dsApplication, UNALARMSCREEN_LIST_NAME);
      if (iListPos > 0)
      {
        dynRemove(dsApplication, iListPos);
      }         
      
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, dsApplication);
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, true);
      fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, makeDynString(), true);
    }
    else // System integrity
    {
      // Init device type list
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, false);
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, makeDynString());
      
      // Init domain list
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, false);
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, makeDynString());
      
      // Init nature list
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_NATURE, false);
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_NATURE, makeDynString());
      
      // Init name list
      filter_Name.enabled = false;
      filter_Name.items(makeDynString(UNALARMSCREEN_FILTER_ALL));
      
      // Init alert text list
      filter_AlertText.items(makeDynString(UNALARMSCREEN_FILTER_ALL, "SI1", "SI2"));    // Alert Texts
      filter_AlertText.enabled = true;
      
      // Init alert state list
      filter_AlertState.items(makeDynString(UNALARMSCREEN_FILTER_ALL));  // Alert State
      filter_AlertState.enabled = false;
      
      // Init application list
      fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, false);
      fwAlarmScreenGeneric_combocheckbox_setItems(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, makeDynString());
       
      unDistributedControl_getAllDeviceConfig(dsSystemNames, duSystemIds, dsHost, diPort);          // System names
      dynInsertAt(dsSystemNames, UNALARMSCREEN_FILTER_ALL, 1);
      dynAppend(dsSystemNames, UNALARMSCREEN_LIST_NAME);
      dynUnique(dsSystemNames);
    
      filter_Alias.enabled = false;
      filter_Description.enabled = true;
    }
    

    // Display date time picker
    if (!shapeExists(UNALARMSCREEN_DATETIMEPICKER_PANELNAME+"."+UNALARMSCREEN_DATETIMEPICKER_WIDGETNAME1))
    {
      getValue("dateTimeWidgetPlaceholder", "position", iXPos, iYPos);
      addSymbol(
            myModuleName(), 
            myPanelName(), 
            "fwGeneral/fwGeneralDateTimeWidget.pnl",
            UNALARMSCREEN_DATETIMEPICKER_PANELNAME, 
            makeDynString("$sStartDate:", "$sEndDate:",
                         "$sStartTime:", "$sEndTime:", 
                         "$bDateAndTime:TRUE", "$bShowTimeZone:TRUE", "$sTimeZone:", 
                         "$iTimePeriod:600", "$bEnabled:"+FALSE), 
            iXPos,
            iYPos, 
            0, 
            1, 
            1
          );
          
      if (fwAlarmScreenGeneric_isAlarmFilterReduced())
      {
        delay(0,100);
        fwGeneral_dateTimeWidget_setVisible(FALSE);
      }
    }
  }
  
}

/**
  @par Description:
  Initialize the UNICOS alarm screen.
  
  @par Usage:
  Internal.
  
  @param[in]  sLoadFilter string, The filter to load.
*/
void        unAlarmScreen_initAES(string sLoadFilter = "")
{
  dyn_string dsExceptions; 
  string sPropertiesDp;
  
  // ----------
  // 1) Check if AES is properly configured
  // ----------
  if (!unAlarmScreen_checkAESConfig())
  {
    bool bOk;
    fwGeneral_openMessagePanel("The Alarm Screen configuation is not for UNICOS.\nUNICOS configuration will be loaded.",bOk, dsExceptions, "Alert Screen Configuration");
    if(bOk)
    {
      fwOpenProgressBar("Alarm Screen configs", "Loading Alarm Screen configs, please wait...", 1); 
      unAlarmScreen_makeAESConfig(dsExceptions);
      fwCloseProgressBar();
      fwGeneral_openMessagePanel("UNICOS configuration was loaded.\nPlease restart the Alarm Screen\nand hide/show the Alarm Row.",bOk, dsExceptions, "Alert Screen Configuration", true);
      PanelOff();
    }
  }
  
  // Disable while starting
  unAlarmScreen_enable(false);
  
  // ----------
  // 2) Wait for AES Initialization by PVSS
  // ----------
  sPropertiesDp = getSystemName() + aes_getPropDpName(AES_DPTYPE_PROPERTIES, true, AESTAB_TOP, false, false);
  int iTimeout = 0;	
  while ((!dpExists("_AESProperties_unAlertScreen") || !dpExists(sPropertiesDp)) && iTimeout<UNALARMSCREEN_TIMEOUT)
  {
    delay(0, UNALARMSCREEN_DELAY);	
    iTimeout++;
  }
  
  if (iTimeout < UNALARMSCREEN_TIMEOUT)
  {			
    
  // ----------
  // 3) Connect on trigger to display ProgressBar
  // ----------
    dpConnect("unAlarmScreen_busyCallBack", false, sPropertiesDp + ".Settings.BusyTrigger");
    
    iTimeout=0;
    shape aesTable = getShape(AES_TABLENAME_TOP);
    while(!reg_main.visible && !aesTable.visible && iTimeout < UNALARMSCREEN_TIMEOUT)
    {
      delay(0, UNALARMSCREEN_DELAY);
      iTimeout++;			
    }
			
    if (iTimeout>=UNALARMSCREEN_TIMEOUT)
    {
      fwException_raise(dsExceptions, "ERROR", "Alerts Screen EventInitialize: Alerts Screen badly initialized!","");	
    }
    else
    {
      setValue("reg_main", "visible", FALSE);
    }
        
  // ----------
  // 4) Init User permission
  // ----------
    unAlarmScreen_userChanged(getUserName());
    
  // ----------
  // 5) Init application filter callback
  // ----------
  
    // Connect application filter CB
    if (g_unicosHMI_bApplicationFilterInUse)
    {
      // Finally connect the callback
      unApplicationFilter_registerAppListChangedCB("unAlarmScreen_applicationFilterChangedCallBack");
    }
   
  // ----------
  // 6) Display filter 
  // ----------    
      
    textFieldCurrentFilter.text() = sLoadFilter;      
		unAlarmScreen_displayFilter(sLoadFilter);

    if(dpExists(UN_APPLICATION_DPNAME))
    {
      dpGet(UN_APPLICATION_DPNAME + ".UTC", unAlarmScreen_g_bUTC);
    }
    
    time requestEnd = getCurrentTime();
    time requestBegin = requestEnd - UN_EVENT_LIST_PERIOD;
    
    if (unAlarmScreen_g_bUTC)
    {
      long q1;
      short q2;
      // selectedTimeZone.text="UTC TIME";
      q1 = (long)requestEnd;
      q2 = milliSecond(requestEnd);
      requestEnd = CtrlEventList_displayUTCTime(q1 + "." + q2);		//dll: return Time as UTC display
      q1 = (long)requestBegin;
      q2 = milliSecond(requestBegin);
      requestBegin = CtrlEventList_displayUTCTime(q1 + "." + q2);	//dll				
      
      fwGeneral_dateTimeWidget_setTimeZone(FW_GENERAL_TIMEZONE_UTC);
      
      // Show UTC column      
      setValue(AES_TABLENAME_TOP, "namedColumnVisibility", UNALARMSCREEN_COLUMN_ID_UTC_TIME, true);      
      bool bVisibleTemp = false;
      do
      {
        getValue(AES_TABLENAME_TOP, "namedColumnVisibility", UNALARMSCREEN_COLUMN_ID_UTC_TIME, bVisibleTemp);
      }
      while (!bVisibleTemp);
      int iWidth;
      getValue(AES_TABLENAME_TOP, "namedColumnWidth", UNALARMSCREEN_COLUMN_ID_UTC_TIME, iWidth);
      
      if (0 == iWidth)
      {
        setValue(AES_TABLENAME_TOP, "namedColumnWidth", UNALARMSCREEN_COLUMN_ID_UTC_TIME, 200);
      } 
    }
    else
    {
      setValue(AES_TABLENAME_TOP, "namedColumnVisibility", UNALARMSCREEN_COLUMN_ID_UTC_TIME, false);   
      // selectedTimeZone.text="LOCAL TIME";
      fwGeneral_dateTimeWidget_setTimeZone(FW_GENERAL_TIMEZONE_LOCAL);
    }
    
    // Fomat date    
    fwGeneral_dateTimeWidget_setStartDateTime(requestBegin);
    fwGeneral_dateTimeWidget_setEndDateTime(requestEnd);    
    
    if (unAlarmScreen_g_iAlarmScreenType == UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY)
    {
      comboVisibleColumnList.items() = makeDynString(
          UNALARMSCREEN_COLUMN_ID_SHORT_SIGN, 
          UNALARMSCREEN_COLUMN_ID_UTC_TIME, 
          UNALARMSCREEN_COLUMN_ID_LOCAL_TIME, 
          UNALARMSCREEN_COLUMN_ID_DESCRIPTION, 
          UNALARMSCREEN_COLUMN_ID_VALUE, 
          UNALARMSCREEN_COLUMN_ID_ACK
        );
    }
    else
    {
      comboVisibleColumnList.items() = makeDynString(
          UNALARMSCREEN_COLUMN_ID_SHORT_SIGN, 
          UNALARMSCREEN_COLUMN_ID_UTC_TIME, 
          UNALARMSCREEN_COLUMN_ID_LOCAL_TIME, 
          UNALARMSCREEN_COLUMN_ID_ALIAS, 
          UNALARMSCREEN_COLUMN_ID_DESCRIPTION, 
          UNALARMSCREEN_COLUMN_ID_DOMAIN, 
          UNALARMSCREEN_COLUMN_ID_NATURE, 
          UNALARMSCREEN_COLUMN_ID_NAME, 
          UNALARMSCREEN_COLUMN_ID_VALUE, 
          UNALARMSCREEN_COLUMN_ID_ACK, 
          UNALARMSCREEN_COLUMN_ID_S
        );
    }
    
    listColumnsView.items = comboVisibleColumnList.items();	

				
    			
	
  // ----------
  // 7) Start AES
  // ----------
  
    string sName;
    dpGet(UNALARMSCREEN_PROPERTIES_DP + ".Name", sName);
    
    if (sName!="")
    {
      bool bState;
      unsigned uRunMode;
      aes_getActivity(sPropertiesDp, bState);			
      aes_getRunMode(sPropertiesDp, uRunMode);			
      int iChange;
      dpGet(sPropertiesDp + ".Settings.Changed", iChange);
      
      
      // int iError;  
      // dpCopyOriginal(UNALARMSCREEN_PROPERTIES_DP, sPropertiesDp, iError);
      
  // ----------
  // 8) Apply Filter in PropDp
  // ----------
      
      dpSetWait(sPropertiesDp + ".Settings.Config", sName);		
    }
    else
    {
      fwException_raise(dsExceptions, "ERROR", "Alerts Screen EventInitialize: no Name defined for _AESProperties!", "");
    }
  }
  else
  {
    fwException_raise(dsExceptions, "ERROR", "Alerts Screen EventInitialize: At least one internal Dp is missing","");	
  }
  
  // ----------
  // 10) Color filter
  // ----------
  unAlarmScreen_colorFilter();

  
  // ----------
  // 11) Manage exceptions
  // ----------
  if (dynlen(dsExceptions) > 0)
  {
    if(isFunctionDefined("unMessageText_sendException"))
    {
      unMessageText_sendException("*", "*", "Alerts Screen", "user", "*", dsExceptions);
    }
  }
  
  // ----------
  // 12) Resize the table
  // ----------
  fwAlarmScreenGeneric_resizeTable();
  
  // ----------
  // 13) Start AES
  // ----------
  unAlarmScreen_applyFilter(false);
  aes_doStart(sPropertiesDp); 
  
  // ----------
  // 14) Resize the panel if needed
  // ----------
  unAlarmScreen_resizePanel();  
}

/**
  @par Description:
  Check if the AES is currently configured for UNICOS.
  
  @par Usage:
  Public.
  
  @return True if the AES is configured for UNICOS, false otherwise.
*/
bool        unAlarmScreen_checkAESConfig()
{
 bool bReturn;
  dyn_string dsExtFunc;  
  string sAES = "_AESConfig.functions.alerts.extFunc";
  
  if(!dpExists(sAES))
  {
    return false;
  }
  
  dpGet(sAES, dsExtFunc);
  
  if(dynlen(dsExtFunc))
  {
    if(patternMatch("unAS_*", dsExtFunc[1])) 
    {
      bReturn = true;
    }
  }
  
  // Check if Screen is correct.
  dyn_string dsDpList = dpNames("*", "_AEScreen");  
	int iLength = dynlen(dsDpList); 
  bool bThereIsDefault = false;
	for (int i = 1; i <= iLength; i++) 
  {
    bool bDefault;
    dpGet(dsDpList[i] + ".UseAsDefault", bDefault);	
    if (bDefault)
    {
      if ((getSystemName() + "_AEScreen_unAlertScreen" != dsDpList[i]) && (getSystemName() + "_AEScreen_unAlertRow" != dsDpList[i]))
      {
        bThereIsDefault = false;
      }
      else
      {
        bThereIsDefault = true;
      }
    }
  }
  
  return bReturn && bThereIsDefault;
}

/**
  @par Description:
  Configure the AES for UNICOS.
  
  @par Usage:
  Public.
  
  @param[out] dsExceptions  dyn_string, List of errors that happened during the execution.
  
  @return True if the configuration was done properly, false if there were some errors.
*/
bool        unAlarmScreen_makeAESConfig(dyn_string &dsExceptions)
{
  bool b_ret;
  
  // Initialize Config dp.
  string s_dpSource = "_AESConfig_unAlertScreen";
  string s_dpDest = "_AESConfig";
  
  // Set alarm panel config
  b_ret = aes_copyDp( s_dpSource, s_dpDest );
  if(!b_ret)
  {
    fwException_raise(dsExceptions,"ERROR","unAlarmScreen_makeAESConfig - ERROR in copying the dp from " + s_dpSource + " to " +s_dpDest,"");
  }
  
  // Set alarm row config
  s_dpSource = "_AESConfigRow_unAlertRow";
  s_dpDest = "_AESConfigRow";
  b_ret = aes_copyDp( s_dpSource, s_dpDest );
  
  if(!b_ret)
  {
    fwException_raise(dsExceptions,"ERROR","unAlarmScreen_makeAESConfig - ERROR in copying the dp from " + s_dpSource + " to " +s_dpDest,"");
  }
  
  // Initialize Screen dp.
  string sSystem = getSystemName();
	dyn_string dsDpList = dpNames("*", "_AEScreen");  
  int iRes;  
	int iLength = dynlen(dsDpList); 
	for (int i = 1; i <= iLength; i++) 
  {
    bool bDefault;
    if (dsDpList[i] == sSystem + "_AEScreen_unAlertRow" || dsDpList[i] == sSystem + "_AEScreen_unAlertScreen")
    {
      bDefault = true;
    }
    else
    {
      bDefault = false;
    }
    
    iRes = iRes + dpSetWait(dsDpList[i] + ".UseAsDefault", bDefault);	
  }

	if (iRes !=0)
  {
		string sMessage = "_AEScreen Default - dpSet failed";
		fwException_raise(dsExceptions, "ERROR", "unAlarmScreen_makeAESConfig: " + sMessage, "");
  }
  
	string sDp = "_Config";
	int MaxDpe_SQL_FROM     = 10000;
	int MaxLine_SQL_Result  = 10000;
	int MaxLine_Close_Mode  = 10000;
	
	if(dpExists(sDp))
  {
		dpSetWait(
        sDp + ".SysMgmStayOnTop",     false,
        sDp + ".QueryHLBlockedTime",  2000,
        sDp + ".MaxLines",            MaxLine_Close_Mode, 
        sDp + ".MaxLinesToDisplay",   MaxLine_SQL_Result, 
        sDp + ".MaxDpeToDisplay",     MaxDpe_SQL_FROM
      );
  }
	else
  {
    string sMessage = " AES General Settings  - dpSet failed";
		fwException_raise(dsExceptions, "ERROR", "unAlarmScreen_makeAESConfig: " + sMessage, "");
  }
    
    
    
  return b_ret;
}

/**
  @par Description:
  Checks if the current user has administration rights. Called from fwAlarmScreenGeneric.
  
  @par Usage:
  Internal.
  
  @return True if the current user has administration rights.
*/
bool        unAlarmScreen_isAdmin()
{
  return unAlarmScreen_g_bState && unAlarmScreen_g_bGranted;
}

string      unAlarmScreen_getAlarmScreenPanel()
{
  bool bUseOldPanel;
  dpGet(UNALARMSCREEN_DP_USE_OLD_PANEL, bUseOldPanel);
  if (bUseOldPanel)
  {
    return UNALARMSCREEN_PANEL_OLD_FILE_NAME;
  }
  else
  {
    return UNALARMSCREEN_PANEL_FILE_NAME;
  }
}

// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ----------------------------- ACTION FUNCTIONS--------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------

/**
  @par Description:
  Acknowledge the alarm at the given row.
  
  @par Usage:
  Public.
  
  @param  iRow  int input,  The row to acknowledge.
*/
void        unAlarmScreen_acknowledgeAlarm(const int iRow, const string sAlarmTable = AES_TABLENAME_TOP)
{
  string sValue;
  string sTime;

  int iScreenType = 1;
  int iTabType = 1;
  int iColumn = 12;
  
  getMultiValue(
      sAlarmTable, "cellValueRC", iRow, UNALARMSCREEN_COLUMN_ID_ACK, sValue,
      sAlarmTable, "cellValueRC", iRow, UNALARMSCREEN_COLUMN_ID_UTC_TIME, sTime
    );

  // We need to set the current column to the "Ack." column, otherwise unAlarmScreen_invokedAESUserFunc will not work
  setValue(sAlarmTable, "currentColumn", UNALARMSCREEN_COLUMN_ID_ACK);
          
  if ((sValue == " !!! ") && (sTime != ""))
  {				
    mapping mTableRow;
    unAlarmScreen_invokedAESUserFunc(sAlarmTable, iScreenType, iTabType, iRow, iColumn, sValue, mTableRow);
  }
}

/**
  @par Description:
  Enable/disable the UNICOS alarm screen. Disabled means it is not possible to click anywhere.
  
  @par Usage:
  Internal.
  
  @param[in]  bEnable bool, True to enable, false to disable.
*/
void        unAlarmScreen_enable(bool bEnable)
{
  unAlarmScreen_g_bState = bEnable;
  
  
  if (shapeExists("saveConfig"))
  {
    setValue("saveConfig", "enabled", bEnable & unAlarmScreen_g_bGranted);
  }
  
  if (shapeExists("openConfig"))
  {
    setValue("openConfig", "enabled", bEnable);
  }
  
  if (shapeExists("refreshConfig"))
  {
    setValue("refreshConfig", "enabled", bEnable);
  }
  
  if (shapeExists("TimeRange"))
  {
    setValue("TimeRange", "enabled", bEnable);
  }
  
  if (shapeExists("table_top"))
  {
    setValue("table_top", "enabled", bEnable);
  }
  
  if (shapeExists("table_bot"))
  {
    setValue("table_bot", "enabled", bEnable);
  }
  
  
  if(TimeRange.number()==2)				//History Mode
  {
    fwGeneral_dateTimeWidget_setEnabled(true);
  }
  else
  {
    fwGeneral_dateTimeWidget_setEnabled(false);
  }
  
  if (unAlarmScreen_g_iAlarmScreenType == UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY) 
  {
    filter_Description.enabled = unAlarmScreen_g_bState;
    filter_AlertText.enabled = unAlarmScreen_g_bState;
  }
  else 
  {
    fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, unAlarmScreen_g_bState);
    fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, unAlarmScreen_g_bState);
    fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_NATURE, unAlarmScreen_g_bState);
    filter_Name.enabled = unAlarmScreen_g_bState;
    filter_AlertText.enabled = unAlarmScreen_g_bState;
    filter_AlertState.enabled = unAlarmScreen_g_bState;
    fwAlarmScreenGeneric_combocheckbox_enableWidget(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, unAlarmScreen_g_bState);
    filter_Alias.enabled = unAlarmScreen_g_bState;
    filter_Description.enabled = unAlarmScreen_g_bState;
    acknowledgeButton.enabled = unAlarmScreen_g_bState & unAlarmScreen_g_bGranted & unAlarmScreen_g_bAck;
  }
}

/**
  @par Description:
  Change the settings when switching to/from history/current/append mode.
  
  @par Usage:
  Internal.
  
  @param[in]  iMode int,  The new mode. 0 = current, 1 = append, 2 = history.
*/
void        unAlarmScreen_changeMode(int iMode)
{
  bool bEnableAppend;
  bool bEnableHistory;
  
  switch (iMode)
  {
    case 0: // Current
    {
      bEnableAppend = false;
      bEnableHistory = false;
      break;
    }
    case 1: // Append
    {
      bEnableAppend = true;
      bEnableHistory = false;
      break;
    }
    case 2: // History
    {
      bEnableAppend = false;
      bEnableHistory = true;
      break;
    }
  }
  
  // To add again if the buttons are requested back.
  /*
  setMultiValue(
    ,
    "btTop",       "enabled", !bEnableAppend,
    "btUpUp",      "enabled", !bEnableAppend,
    "btUp",        "enabled", !bEnableAppend,
    "btBot",       "enabled", !bEnableAppend,
    "btDownDown",  "enabled", !bEnableAppend,
    "btDown",      "enabled", !bEnableAppend    
  );   
  */
  fwGeneral_dateTimeWidget_setEnabled(bEnableHistory);  
}

/**
  @par Description:
  Rearranges footer to correctly align it in resized panel. To be called on expanding event.
  
  @par Usage:
  Public.
*/
void        unAlarmScreen_resizePanel()
{
  // Note:
  // The inner-widgets of the reference to the generic alarm panel have been modified.
  // It results an error when setting positions. Anything that is a reference has to be added with a 100 pixel difference on the Y axis, equivalent to the 100 pixels that the widgets have been moved from.
  const int DIFF_TO_ORIGINAL = 100;
  int iOldX;
  int iOldY;
  int iNewX;
  int iLineX;
  int iLineY;
  int iPanelWidth;
  int iPanelHeight;
  
  getValue("lineBottomLimit", "position", iLineX, iLineY);
  
  dyn_string dsFooterRef = makeDynString(
    "alarmInfoWidget",
    FW_ALARM_GENERIC_WIDGET_SYSTEM_INFO_REF//,
    //"btExportData" // has a separate code for setting its position 
  );
    
  // Panel is reduced.
  // Reduce to UNICOS size
  if (FW_ALARM_GENERIC_PANEL_BOTTOM == iLineY)
  {
    iLineY = UNALARMSCREEN_PANEL_DEFAULT_HEIGHT - UNALARMSCREEN_PANEL_FOOTER_HEIGHT;
    setPanelSize(myModuleName(), myPanelName(), false, UNALARMSCREEN_PANEL_DEFAULT_WIDTH, UNALARMSCREEN_PANEL_DEFAULT_HEIGHT);
    
    setValue("lineBottomLimit", "position", iLineX, iLineY);  
    
    int iBusyBarX;
    int iBusyBarY;
    getValue("rectangleBusyBar", "position", iBusyBarX, iBusyBarY);  
    setValue("rectangleBusyBar", "position", iBusyBarX, iLineY - 20);  
    
    
  }
  
  for (int i = 1 ; i <= dynlen(dsFooterRef) ; i++)
  {
    if (shapeExists(dsFooterRef[i]))
    {
      int iX;
      int iY;
      getValue(dsFooterRef[i], "position", iX, iY);
      setValue(dsFooterRef[i], "position", iX, iLineY + DIFF_TO_ORIGINAL);
    }
  } 

  panelSize("", iPanelWidth, iPanelHeight);
  
  if(shapeExists("closeButton"))
  {
    getValue("closeButton", "position", iOldX, iOldY);
    iNewX = iPanelWidth - 85;
    setValue("closeButton", "position", iNewX, iLineY);
  }
  
  if(shapeExists("btExportData"))
  {
    getValue("btExportData", "position", iOldX, iOldY);
    iNewX = iPanelWidth - 131;
    setValue("btExportData", "position", iNewX, iLineY + DIFF_TO_ORIGINAL);
  }
  
  if(shapeExists("btprint"))
  {
    getValue("btprint", "position", iOldX, iOldY);
    iNewX = iPanelWidth - 164;
    setValue("btprint", "position", iNewX, iLineY);
  }
  
  if(shapeExists("infoMask"))
  {
    getValue("infoMask", "position", iOldX, iOldY);
    iNewX = iPanelWidth - 197;
    setValue("infoMask", "position", iNewX, iLineY);
  }
  
  
  
}

/**
  @par Description:
  Acknowledge all rows from the given table. Called from fwAlarmScreenGeneric.
  
  @par Usage:
  Internal.
  
  @param[in]  sTable            string, The table in which the row is shown.
  @param[in]  bVisibleRangeOnly bool,   Only acknowledge visible range if true.
*/
void        unAlarmScreen_acknowledgeAll(const string sTable, const bool bVisibleRangeOnly)
{
  int iFirstLine;
  int iLastLine;
  
  // Disable visual updates to prevent lines from disappearing.
  addGlobal("g_alertRow", BOOL_VAR);
  g_alertRow = false; // the table, not the row
  string sPropertiesDp = aes_getPropDpName(AES_DPTYPE_PROPERTIES, true, AESTAB_TOP, false, false);
  aes_doStop(sPropertiesDp);

  if (bVisibleRangeOnly)
  {
    getValue(sTable, "lineRangeVisible", iFirstLine, iLastLine);
  }
  else
  {
    iFirstLine = 0;
    getValue(sTable, "lineCount", iLastLine);
    iLastLine--;
  }

  // Acknowledging takes time. We have to do it in parallel otherwise we have to wait around a second for each thread.
  dyn_int diThreadIds;
  for (int i = iFirstLine ; i <= iLastLine ; i++)
  {
    dynAppend(diThreadIds, startThread("unAlarmScreen_acknowledgeAlarm", i, sTable));
    delay(0, UNALARMSCREEN_DELAY); // To limit the number of threads (otherwise we could have thousands of threads when acking all alarms).
  } 
  
  // Re-enable visual updates
  aes_doRestart(sPropertiesDp);
}

// ---------------------------------------
// ---------------------------------------
// -- Functions for filter manipulation --
// ---------------------------------------
// ---------------------------------------

/**
  @par Description:
  Build a filter from the different UI filter-elements.
  
  @par Usage:
  Internal.

  @return The filter read.
*/
string      unAlarmScreen_makeFilter()
{
  string sFilter;

  // ----- 1) Read alias filter
  string sAlias = filter_Alias.text;
  
  // ----- 2) Read description filter
  string sDescription = filter_Description.text;
  
  // ----- 3) Read alert text filter
  string sAlertText = filter_AlertText.text;
  
  // ----- 4) Read alert state filter
  string sAlertState = filter_AlertState.selectedPos();
  
  // ----- 5) Read domain filter
  string sDomain = _unAlarmScreen_getMultipleChoiceFilterValue(UNALARMSCREEN_WIDGET_FILTER_DOMAIN);
    
  // ----- 6) Read nature filter
  string sNature = _unAlarmScreen_getMultipleChoiceFilterValue(UNALARMSCREEN_WIDGET_FILTER_NATURE);
  
  // ----- 7) Read name filter
  string sName = filter_Name.text;
  
  
  // ----- 8) Read device type filter
  string sObject = _unAlarmScreen_getMultipleChoiceFilterValue(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE);

  // ----- 9) Read system name filter
  // Not in use any more, system is always "*"
  string sSystem = UNALARMSCREEN_FILTER_ALL;
    
  // ----- 10) Read application filter
  string sApplication = _unAlarmScreen_getMultipleChoiceFilterValue(UNALARMSCREEN_WIDGET_FILTER_APPLICATION);
  
  // ----- 11) Read time range type
  string sTimeRange = TimeRange.number();
  
  // ----- 12) Read max lines
  string sMaxLines = AppendLines.text;
    
  // ----- 13) Build full filter string
  sFilter = 
      sTimeRange   + UNALARMSCREEN_FILTER_DELIMITER + 
      sMaxLines    + UNALARMSCREEN_FILTER_DELIMITER + 
      ""           + UNALARMSCREEN_FILTER_DELIMITER + // Request time
      sAlias       + UNALARMSCREEN_FILTER_DELIMITER + 
      sDescription + UNALARMSCREEN_FILTER_DELIMITER +
      sDomain      + UNALARMSCREEN_FILTER_DELIMITER + 
      sNature      + UNALARMSCREEN_FILTER_DELIMITER +
      sName        + UNALARMSCREEN_FILTER_DELIMITER + 
      sAlertText   + UNALARMSCREEN_FILTER_DELIMITER + 
      sAlertState  + UNALARMSCREEN_FILTER_DELIMITER + 
      sObject      + UNALARMSCREEN_FILTER_DELIMITER + 
      sSystem      + UNALARMSCREEN_FILTER_DELIMITER + 
      sApplication;      
      
  return sFilter;
}

/**
  @par Description:
  Pop-up a panel to save the current filter.
  
  @par Usage:
  Internal.  
*/
void        unAlarmScreen_saveFilter()
{
  string sFilter = unAlarmScreen_makeFilter();
  switch (unAlarmScreen_g_iAlarmScreenType)
  {
    case UNALARMSCREEN_PANEL_ALL_ALARMS:
    {
      dyn_string dsReturn;
      dyn_float dfReturn;
      
      unGraphicalFrame_ChildPanelOnCentralModalReturn(
          "vision/unAlertPanel/unAlertScreenSaveFilter.pnl", 
          "Select",
          makeDynString(
              "$sFilter:" + sFilter
            ),
          dfReturn,
          dsReturn
        );
      
      if (dynContains(dfReturn, 1) > 0)
      {
        unAlarmScreen_groups_createGroup(dsReturn[1], dsReturn[2]);        
      }
      
      break;
    }
    case UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY:
    {
      unGraphicalFrame_ChildPanelOnCentralModal(
          "vision/unEventList/unEventList_SelectFilter.pnl", 
          "Select",
          makeDynString(
              "$sConfig:Save", 
              "$sPrefixFilter:unAlertPanel_Filter_SystemAlarms_"
              "$sFilterType:" + UNALARMSCREEN_DP_FILTER_TYPE, 
              "$sFilter:" + sFilter
              )
          );                                                      
      
      break;
    }
    default:
    {
      break;
    }
  }
  
  unAlarmScreen_groups_showGroups();
}

/**
  @par Description:
  Pop-up a panel to load a filter.
  
  @par Usage:
  Internal.  
*/
void        unAlarmScreen_loadFilter()
{
  dyn_float dfReturn;
  dyn_string dsReturn;
    
  switch (unAlarmScreen_g_iAlarmScreenType)
  {
    case UNALARMSCREEN_PANEL_ALL_ALARMS:
    {
      unGraphicalFrame_ChildPanelOnCentralModalReturn(
          "vision/unEventList/unEventList_SelectFilter.pnl",
          "Select",
          makeDynString(
              "$sConfig:Load",
              "$sPrefixFilter:" + UNALARMSCREEN_DP_FILTER_PREFIX,
              "$sFilterType:" + UNALARMSCREEN_DP_FILTER_TYPE
              ),
          dfReturn,
          dsReturn
      );
      
      break;
    }
    case UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY:
    {
      unGraphicalFrame_ChildPanelOnCentralModalReturn(
          "vision/unEventList/unEventList_SelectFilter.pnl", 
          "Select",
          makeDynString(
              "$sConfig:Load", 
              "$sPrefixFilter:unAlertPanel_Filter_SystemAlarms_",
              "$sFilterType:" + UNALARMSCREEN_DP_FILTER_TYPE
              ), 
          dfReturn, 
          dsReturn
        );    
      
      break;
    }
    default:
    {
      break;
    }
  }
  
  unAlarmScreen_groups_showGroups();
  
  if (dynlen(dsReturn) >= 1)
  {
    unAlarmScreen_displayFilter(dsReturn[1]);
  }
}

/**
  @par Description:
  Apply the current filter to the AES table.
  
  @par Usage:
  Internal.
  
  @param[in] bDoRestart bool, True to do an AES restart at the end, false not to.
*/
void        unAlarmScreen_applyFilter(bool bDoRestart = true)
{
  bool bDefaultFilter;
  
  // ------------------------------------------
  // Save the filter in the AES properties DP  
  // ------------------------------------------
  string sPropertiesDp = getSystemName() + aes_getPropDpName(AES_DPTYPE_PROPERTIES, true, AESTAB_TOP, false, false);  
  
  // ----- 1) Get filter
  
  string sCurrentFilter = unAlarmScreen_makeFilter();
  bDefaultFilter = UNALARMSCREEN_FILTER_DEFAULT == sCurrentFilter;
  
  textFieldCurrentFilter.text(sCurrentFilter);
  dyn_string dsFilter = strsplit(textFieldCurrentFilter.text(), UNALARMSCREEN_FILTER_DELIMITER);
    
  dyn_string dsNature = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE], UNALARMSCREEN_FILTER_COMBO_DELIMITER);
  dyn_string dsDomain = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN], UNALARMSCREEN_FILTER_COMBO_DELIMITER);
  
  bool bMultiDomain = (dynContains(dsDomain, UNALARMSCREEN_FILTER_ALL) < 1) && (dynlen(dsDomain) > 1);
  bool bMultiNature = (dynContains(dsNature, UNALARMSCREEN_FILTER_ALL) < 1) && (dynlen(dsNature) > 1);
  
  bool bStart = true;  
  
  // ----- 2) Set alarm filter to fit dpType _AESProperties
  // ---------- 2.1)  Description filter
  string sFilterDpComment  ;  
  // --------------- 2.1.1) Pattern for domain
  if (bMultiDomain)  
  {
    // List
    sFilterDpComment = UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
  }
  else
  {
    if (dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN] == "")
    {
      sFilterDpComment = UNALARMSCREEN_FILTER_DELIMITER;
    }
    else if (dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN] == UNALARMSCREEN_FILTER_ALL)
    {
      sFilterDpComment = dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN] + UNALARMSCREEN_FILTER_DELIMITER;    
    }
    else
    {
      // At least one domain => filter with dpList
      bMultiDomain = true;
      sFilterDpComment = UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
    }
  }

  // --------------- 2.1.2) Pattern for nature
  if (bMultiNature)
  {
    sFilterDpComment = sFilterDpComment + UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
  }
  else
  {
    if (dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE]=="")
    {
      sFilterDpComment = sFilterDpComment + UNALARMSCREEN_FILTER_DELIMITER;
    }
    else if (dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE]==UNALARMSCREEN_FILTER_ALL)
    {
      sFilterDpComment = sFilterDpComment + dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE] + UNALARMSCREEN_FILTER_DELIMITER;        
    }
    else    // At leat one nature => filter with dpList
    {
      bMultiNature=true;
      sFilterDpComment = sFilterDpComment + UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
    }
  }  
  
  // --------------- 2.1.3) Pattern for the remaining fields
  switch (unAlarmScreen_g_iAlarmScreenType)
  {
    case UNALARMSCREEN_PANEL_ALL_ALARMS:
    {
      sFilterDpComment =   
          sFilterDpComment + 
          dsFilter[UNALARMSCREEN_FILTER_FIELD_ALIAS]       + UNALARMSCREEN_FILTER_DELIMITER +
          dsFilter[UNALARMSCREEN_FILTER_FIELD_DESCRIPTION] + UNALARMSCREEN_FILTER_DELIMITER + 
          dsFilter[UNALARMSCREEN_FILTER_FIELD_NAME]        + UNALARMSCREEN_FILTER_DELIMITER + 
          UNALARMSCREEN_FILTER_ALL;
      break;
    }
    case UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY:
    {
      sFilterDpComment = dsFilter[UNALARMSCREEN_FILTER_FIELD_DESCRIPTION];
      break;
    }
    default:
      sFilterDpComment = UNALARMSCREEN_FILTER_ALL;
      break;
    }
    

  // ---------- 2.2)  System filter    
  // Note: no more system filtering, always choose all connected systems.
  dyn_string dsSystems = fwAlarmScreenGeneric_getConnectedSystems();
  
  // ---------- 2.3)  dpList filter  
  dyn_string dsFilterDps = _unAlarmScreen_dpListFilter(dsFilter, bMultiDomain, bMultiNature);

  // ----- 3)  Set _AESProperties associated datapoint
  if (dpExists(sPropertiesDp))
  {
    unsigned uFilterState = (unsigned)dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTSTATE];
    uFilterState = (unsigned)(uFilterState - 1u);
    time tEnd;
    time tBegin;
    if(dsFilter[UNALARMSCREEN_FILTER_FIELD_TIMERANGE] == 2) // Only with History Mode
    {
      bool bDateOk;     
      dyn_string dsExceptions;
      
      tEnd = fwGeneral_dateTimeWidget_getEndDateTime(bDateOk, dsExceptions);
      if (!bDateOk)
      {
        fwExceptionHandling_display(dsExceptions);
        return;
      }
      
      tBegin = fwGeneral_dateTimeWidget_getStartDateTime(bDateOk, dsExceptions);
      if (!bDateOk)
      {
        fwExceptionHandling_display(dsExceptions);
        return;
      }
      
      bDateOk = fwGeneral_dateTimeWidget_positivePeriodSelected(dsExceptions);
      if (!bDateOk)
      {
        fwExceptionHandling_display(dsExceptions);
        return;
      }
      
      time tNow = getCurrentTime();	//local time
	
      if (FW_GENERAL_TIMEZONE_UTC == fwGeneral_dateTimeWidget_getTimeZone())
      {
        tEnd = CtrlEventList_displayLocalTimefromTime(tEnd);
        tBegin = CtrlEventList_displayLocalTimefromTime(tBegin);
      }		
      
      if (tEnd > tNow)
      {
        tEnd = tNow;
      }
	
      if (CtrlEventList_checkTimePeriod(tBegin, tEnd))
      {
        bStart = false;
        unGraphicalFrame_ChildPanelOnModal(
            "vision/MessageInfo", 
            "Information",
            makeDynString(
                "$1:\"From Time\" request is between Summer -> Winter or Winter -> Summer switch period. To get all data set \"From Time\" <= 23:59:59 (UTC time format).", 
                "$2:CONTINUE", 
                "$3:ABORT"
              ),
            50, 
            50
          );
      }
    }
    
    if (bStart)
    {
      dpSetCache(  
          sPropertiesDp + ".Alerts.FilterState.State",                      uFilterState,
          sPropertiesDp + ".Alerts.Filter.DpComment",                       sFilterDpComment,
          sPropertiesDp + ".Alerts.Filter.DpList",                          dsFilterDps,
          sPropertiesDp + ".Alerts.Filter.Shortcut",                        dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTTEXT], //! we set Short Sign and not Alert Text
          sPropertiesDp + ".Alerts.FilterTypes.Selections",                 makeDynInt(0,0,0,0,0),
          sPropertiesDp + ".Alerts.FilterTypes.AlertSummary",               2,
          sPropertiesDp + ".Both.Sorting.SortList",                         makeDynString("timeStr"),
          sPropertiesDp + ".Both.Visible.VisibleColumns:_original.._value", comboVisibleColumnList.items(),
          sPropertiesDp + ".Both.Timerange.Type",                           dsFilter[UNALARMSCREEN_FILTER_FIELD_TIMERANGE],
          sPropertiesDp + ".Both.Timerange.Begin",                          tBegin,
          sPropertiesDp + ".Both.Timerange.End",                            tEnd,
          sPropertiesDp + ".Both.Timerange.MaxLines",                       dsFilter[UNALARMSCREEN_FILTER_FIELD_MAXLINES],
          sPropertiesDp + ".Both.Timerange.Selection",                      6,
          sPropertiesDp + ".Both.Timerange.Shift",                          1,
          sPropertiesDp + ".Both.Systems.CheckAllSystems",                  FALSE,
          sPropertiesDp + ".Both.Systems.Selections",                       dsSystems
        );     
        
        
      // To use the system info widget, we have to simulate that all systems should be filtered.
      delay(1); // Leave time for the default callback to pass.
      dyn_string dsConnectedSystems     = fwAlarmScreenGeneric_onlineSystemList.items();
      dyn_string dsDisconnectedSystems  = fwAlarmScreenGeneric_offlineSystemList.items();
      dyn_string dsFilteredSystems;
      dynAppend(dsFilteredSystems, dsConnectedSystems);
      dynAppend(dsFilteredSystems, dsDisconnectedSystems);        
      fwAlarmScreenGeneric_filteredSystemList.items(dsFilteredSystems);
      _fwAlarmScreenGeneric_updateSystemState();
  
    }
  }
    

  if (bDefaultFilter)
  {
    fwAlarmScreenGeneric_setActiveFilter("");
  }
  else
  {
    fwAlarmScreenGeneric_setActiveFilter(UNALARMSCREEN_ACTIVE_FILTER_LABEL);
  }
  
  dpSetWait(sPropertiesDp + ".Settings.Config", "aes_prop_unAlertScreen");

  if (bDoRestart)
  {
    aes_doRestart(sPropertiesDp, FALSE);
  }
}

/**
  @par Description:
  Colour the filter fields if they are different from "*".
  
  @par Usage:
  Internal.
*/
void        unAlarmScreen_colorFilter()
{
  string sColorBad = "{255,154,52}";
  string sColorOk = "_Window";
  dyn_string dsShapeText = makeDynString(
      "filter_AlertText", 
      "filter_Alias", 
      "filter_Description",
      "filter_AlertState",
      "filter_Name"
    );
  
  dyn_string dsMethod = makeDynString(
      "selectedText", 
      "text", 
      "text",
      "selectedText", 
      "selectedText"
    );
  
  int i, len;
  string sSelectedText, sColor;
  
  len = dynlen(dsShapeText);
  for(i = 1 ; i <= len ; i++) 
  {
    getValue(dsShapeText[i], dsMethod[i], sSelectedText);
    if(sSelectedText != "*")
    {
      sColor = sColorBad;
    }
    else
    {
      sColor = sColorOk;
    }
    
    setValue(dsShapeText[i], "backCol", sColor);
  }
}

/**
  @par Description:
  Show a filter parameters in the appropriate fields.
  
  @par Usage:
  Internal.
  
  @param[in]  sFilter string, The filter to show.
*/
void        unAlarmScreen_displayFilter(const string sFilter)
{
  dyn_string dsFilter = strsplit(sFilter, UNALARMSCREEN_FILTER_DELIMITER);
  dyn_string dsRequestSystem;
  string sCurrentFilter;

  // If wrong filter format in memory -> default one...
  if(dynlen(dsFilter) == UNALARMSCREEN_FILTER_FIELD_LENGTH)
  {
    dynAppend(dsFilter, "*");
  }
  
  if (dynlen(dsFilter) != (UNALARMSCREEN_FILTER_FIELD_LENGTH + 1))
  {
    dsFilter = strsplit(UNALARMSCREEN_FILTER_DEFAULT, UNALARMSCREEN_FILTER_DELIMITER);
    
    // Get systems set in unApplication
    dpGet(getSystemName() + UN_APPLICATION_DPNAME + ".unAlerts.filter.systems", dsRequestSystem);
    
    sCurrentFilter = "";
    for (int i = 1; i <= dynlen(dsRequestSystem) ; i++)
    {
      int iPos = strpos(dsRequestSystem[i], ":");
      if (iPos>=0)
      {
        dsRequestSystem[i] = substr(dsRequestSystem[i], 0, iPos);
      }
      
      if (strlen(sCurrentFilter) == 0)
      {
        sCurrentFilter = dsRequestSystem[i];
      }
      else
      {
        sCurrentFilter = sCurrentFilter + UNALARMSCREEN_FILTER_COMBO_DELIMITER + dsRequestSystem[i];
      }
    }
    
    dsFilter[UNALARMSCREEN_FILTER_FIELD_SYSTEMS] = sCurrentFilter;
    dsFilter[UNALARMSCREEN_FILTER_FIELD_LENGTH + 1] = "*";
  }    
  
  
  // -------------------------
  // 1) Fill device type field
  // -------------------------
  string sDeviceTypeFilter = dsFilter[UNALARMSCREEN_FILTER_FIELD_OBJECT];
  dyn_string dsDeviceTypes = strsplit(sDeviceTypeFilter, UNALARMSCREEN_FILTER_COMBO_DELIMITER);  
  if (dynContains(dsDeviceTypes, "*") > 0)
  {
    fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, makeDynString(), true);
  }
  else
  {
    fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, dsDeviceTypes, false);
  }
  
  // -------------------------
  // 2) Fill alias field
  // -------------------------
  if (strltrim(strrtrim(dsFilter[UNALARMSCREEN_FILTER_FIELD_ALIAS])) == "")
  {
    filter_Alias.text = UNALARMSCREEN_FILTER_ALL;
  }
  else
  {
    filter_Alias.text = dsFilter[UNALARMSCREEN_FILTER_FIELD_ALIAS];
  }
    
  // -------------------------
  // 3) Fill description field
  // -------------------------
  if (strltrim(strrtrim(dsFilter[UNALARMSCREEN_FILTER_FIELD_DESCRIPTION])) == "")
  {
    filter_Description.text = UNALARMSCREEN_FILTER_ALL;
  }
  else
  {
    filter_Description.text = dsFilter[UNALARMSCREEN_FILTER_FIELD_DESCRIPTION];
  }
    
  // -------------------------
  // 4) Fill domain field
  // -------------------------  
  string sDomain = dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN];
  dyn_string dsDomains = strsplit(sDomain, UNALARMSCREEN_FILTER_COMBO_DELIMITER);  
  
  if (dynContains(dsDomains, "*") > 0)
  {
    fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, makeDynString(), true);
  }
  else
  {
    fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, dsDomains, false);
  }
  
  // -------------------------
  // 5) Fill nature field
  // -------------------------  
  string sNature = dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE];
  dyn_string dsNature = strsplit(sNature, UNALARMSCREEN_FILTER_COMBO_DELIMITER);  
  
  if (dynContains(dsNature, "*") > 0)
  {
    fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_NATURE, makeDynString(), true);
  }
  else
  {
    fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_NATURE, dsNature, false);
  }  

  // -------------------------
  // 6) Fill name field
  // -------------------------
  if (strltrim(strrtrim(dsFilter[UNALARMSCREEN_FILTER_FIELD_NAME])) == "")
  {
    filter_Name.text = UNALARMSCREEN_FILTER_ALL;
  }
  else
  {
    filter_Name.text = dsFilter[UNALARMSCREEN_FILTER_FIELD_NAME];
  }
    
    
  // -------------------------
  // 7) Fill alert text field
  // -------------------------
  if (strltrim(strrtrim(dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTTEXT])) == "")
  {
    filter_AlertText.text = UNALARMSCREEN_FILTER_ALL;
  }
  else
  {
    filter_AlertText.text = dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTTEXT];
  }
    
    
  // -------------------------
  // 8) Fill alert state field
  // -------------------------
  if (strltrim(strrtrim(dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTSTATE])) == "")
  {
    filter_AlertState.selectedPos(1);
  }
  else
  {
    int iTemp = (int)dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTSTATE];
    if ((iTemp > 0) && (iTemp <= filter_AlertState.itemCount()))
    {
      filter_AlertState.selectedPos(iTemp);
    }
    else
    {
      filter_AlertState.selectedPos(1);
    }
  }
    
    
  // -------------------------
  // 9) Fill system name field
  // -------------------------
  // Not in use any more, ignore.
  string sSystemFilter = dsFilter[UNALARMSCREEN_FILTER_FIELD_SYSTEMS];
  dyn_string dsSystemFilter = strsplit(sSystemFilter, UNALARMSCREEN_FILTER_COMBO_DELIMITER);
    
    
  // -------------------------
  // 10) Fill application field
  // -------------------------
  if(dynlen(dsFilter) == (UNALARMSCREEN_FILTER_FIELD_LENGTH + 1))
  {
    string sApplication = dsFilter[UNALARMSCREEN_FILTER_FIELD_LENGTH + 1];
    dyn_string dsApplications = strsplit(sApplication, UNALARMSCREEN_FILTER_COMBO_DELIMITER);
    
    if (dynContains(dsApplications, "*") > 0)
    {
      fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, makeDynString(), true);
    }
    else
    {
      fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, dsApplications, false);
    }
  }
  else
  {
    fwAlarmScreenGeneric_combocheckbox_setSelectedItems(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, makeDynString(), true);
  }
   
   
  // -------------------------
  // 11) Fill time range field
  // -------------------------
  TimeRange.number(0);
  unAlarmScreen_changeMode(0);

  
  // -------------------------
  // 12) Fill max lines field
  // -------------------------
  if (strltrim(strrtrim(dsFilter[UNALARMSCREEN_FILTER_FIELD_MAXLINES])) == "")
  {
    AppendLines.text = "100";
  }
  else
  {
    AppendLines.text = dsFilter[UNALARMSCREEN_FILTER_FIELD_MAXLINES];
  }
}



// --------------------------------------
// --------------------------------------
// --------- Internal functions ---------
// --------------------------------------
// --------------------------------------

/**
  @par Description:
  Get the default UNICOS DP, system and comment filters for the AES.
  
  @par Usage:
  Public.
  
  @param[out] dsSystems dyn_string, The filtered systems.
  @param[out] dsList    dyn_string, The filtered DPs.
  @param[out] sComment  dyn_string, The filtered comments.    
*/
void        _unAlarmScreen_getInitDpProperties(dyn_string &dsSystems, dyn_string &dsList, string &sComment)
{
  if (dpExists(UN_APPLICATION_DPNAME))
  {
    dpGet(UN_APPLICATION_DPNAME + ".unAlerts.filter.systems", dsSystems);
      
    switch(unAlarmScreen_g_iAlarmScreenType)
    {
      case UNALARMSCREEN_PANEL_ALL_ALARMS:
      {
        dpGet(
            UN_APPLICATION_DPNAME + ".unAlerts.filter.dplist", dsList, 
            UN_APPLICATION_DPNAME + ".unAlerts.filter.dpcomment", sComment
          );
        break;
      }
      case UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY:
      {
        dpGet(UN_APPLICATION_DPNAME + ".unSystemAlerts.filter.dplist", dsList); 
        sComment = "";
        break;
      }
      default:
      {
        dsList = makeDynString(UNALARMSCREEN_FILTER_ALL);
        sComment = "";
      }
    }
  }
  else
  {
    dsList = makeDynString(UNALARMSCREEN_FILTER_ALL);
    sComment = "";
  }
}

/**
  @par Description:
  Get the content of a multiple-choice combo-box.
  
  @par Usage:
  Internal.
  
  @param[in]  sWidgetReference  string, The widget to read from.
  
  @return "*" if all selected, the selected elements otherwise (see function _unAlarmScreen_listToString if multiple elements selected).
*/
string      _unAlarmScreen_getMultipleChoiceFilterValue(const string sWidgetReference)
{
  string sValue;
  bool bAllItemsSelected;
  dyn_string dsItemList = fwAlarmScreenGeneric_combocheckbox_getSelectedItems(sWidgetReference, bAllItemsSelected);
  
  // If all or no item selected: filter all.
  // If one, filter this one.
  // If more than one, get the list.
  if (bAllItemsSelected || (dynlen(dsItemList) == 0))
  {
    sValue = UNALARMSCREEN_FILTER_ALL;
  }
  else if (dynlen(dsItemList) == 1)
  {
    sValue = dsItemList[1];
  }
  else
  {
    sValue = _unAlarmScreen_listToString(dsItemList);
  }
  
  return sValue;
}

/**
  @par Description:
  Convert a dyn_string to a string compatible with UNICOS alarm filter string.
  
  @par Usage:
  Internal.
  
  @param  dsList  dyn_string input, The list to flatten.
  
  @return The list a string separated by UNALARMSCREEN_FILTER_COMBO_DELIMITER.
*/
string      _unAlarmScreen_listToString(dyn_string dsList)
{
  string sReturn;
  for (int i = 1 ; i <= dynlen(dsList) ; i++)
  {
    sReturn += UNALARMSCREEN_FILTER_COMBO_DELIMITER + dsList[i];
  }
  sReturn += UNALARMSCREEN_FILTER_COMBO_DELIMITER;
  sReturn = strltrim(sReturn, UNALARMSCREEN_FILTER_COMBO_DELIMITER);
  
  return sReturn;
}

/**
  @par Description:
  Function called when clicking on some of the alarm columns. See function fwAlarmScreenGeneric_invokedAESUserFunc.
  
  @par Usage:
  Internal.
  
  @param[in]  sShapeName  string,   The table that was clicked.
  @param[in]  iScreenType int,      The scree type. Irrelevant (always alarms).
  @param[in]  iTabType    int,      The table type. Irrelevant.
  @param[in]  iRow        int,      The row that was clicked.
  @param[in]  iColumn     string,   The column that was clicked. Not used.
  @param[in]  sValue      string,   The value in the cell that was clicked.
  @param[in]  mTableRow   mapping,  The value of each of the columns of the clicked row.
*/
void        unAlarmScreen_invokedAESUserFunc(string sShapeName, int iScreenType, int iTabType, int iRow, int iColumn, string sValue, mapping mTableRow)
{
  bool bGranted;
  
  int iNb;
  int iHistoric;  
  
  string sDpe;  
  string sDeviceName;  
  string sDescription;
  
  dyn_string dsExceptions;

  dyn_string dsUnicosObjects;
  
	getValue(sShapeName, "cellValueRC", iRow, _DPID_, sDpe);
	sDeviceName = unGenericDpFunctions_getDpName(sDpe);  

  //IS-1952 Rely on column header rather than column as it changes with the version of WinCC OA
  // also uses currentColumn instead of iColumn row as the later is not correct
  string sColName, sColHeader;
  getValue(sShapeName, "currentColumn", sColName);
  getValue(sShapeName, "namedColumnHeader", sColName, sColHeader);
  
  switch (sColHeader)
  {
		// "S" -> Open default panel
		case "S":
    {    
      string sCellValue;
  		getValue (sShapeName, "cellValueRC", iRow, "s", sCellValue); // Get S
  		if (sCellValue == "S")
      {
	  		getValue(sShapeName, "cellValueRC", iRow, "description", sDescription);  // Get Alias and Description
				_unAlarmScreen_defaultPanel(sDeviceName, sDescription);
      }
			break;
		}
		// "Ack." -> Acknowledgment
		case "Ack.":
    {
			if (sValue == " !!! " && sDeviceName != "")
      {				
				if (shapeExists("TimeRange"))
        {
					getValue("TimeRange", "number", iHistoric);	
        }
				else
        {
					iHistoric =- 1;
        }
				
				if (iHistoric != 2) // No ack possible in history mode
        {
					string sType = dpTypeName(sDeviceName);
							
					unGenericDpFunctions_getUnicosObjects(dsUnicosObjects);
          
					if (dynContains(dsUnicosObjects, sType) > 0)
          {						
						unGenericButtonFunctionsHMI_isAccessAllowed(sDeviceName, sType, UN_FACEPLATE_BUTTON_ACK_ALARM, bGranted, dsExceptions);
            
						if (bGranted)
            {
							unGenericObject_Acknowledge(dpSubStr(sDpe, DPSUB_SYS_DP_EL), dsExceptions);
            }
          }
					else
          {
						unGenericButtonFunctionsHMI_isAccessAllowed(sDeviceName, UN_GENERIC_USER_ACCESS, UN_FACEPLATE_BUTTON_ACK_ALARM, bGranted, dsExceptions);
						if (bGranted)
            {
							unAlarmConfig_acknowledge(dpSubStr(sDpe, DPSUB_SYS_DP_EL), dsExceptions);
            }
          }
					
					if (!bGranted)
          {
						fwException_raise(dsExceptions, "ERROR", "unAlarmScreen_invokedAESUserFunc(): You don't have permission to acknowledge: " + unGenericDpFunctions_getAlias(sDeviceName), "");
          }
	
					if (dynlen(dsExceptions) > 0)
          {
						if(isFunctionDefined("unMessageText_sendException"))
            {
							unMessageText_sendException("*", "*", "unAlertScreen", "user", "*", dsExceptions);
            }
          }						
        }
      }
			break;
		}
		default:
    {
			DebugTN(__FUNCTION__, "Not action associated with the column", sColName, sColHeader);
    }
  }
}

/**
  @par Description:
  Action for click on "S" column of the alarm screen.
  
  @param  sDeviceName   string input, The DPE of the clicked device.
  @param  sDescription  string input, The description of the clicked device.
  
  @param Usage:
  Internal.
*/
void        _unAlarmScreen_defaultPanel(string sDeviceName, string sDescription)
{
  string sPath; 
  
  dyn_string dsParameters;
  dyn_string dsExceptions;
  
  
  unGenericDpFunctions_getParameters(sDeviceName, dsParameters);
  sPath = dsParameters[UN_PARAMETER_PANEL];
  if (getPath(PANELS_REL_PATH, sPath) == "" || sPath=="")
  {
    bool bBad = true;
    if(dpExists(sPath)) 
    { 
      // Check if of type unTreeWidget_treeNodeDPT
      if(dpTypeName(sPath) == unTreeWidget_treeNodeDPT) 
      {
        string sPanelDp;
        string sDpType; 
        string sPanelFile;
        dyn_string dsUserData;
        
        fwTree_getNodeUserData(_fwTree_getNodeName(sPath), dsUserData, dsExceptions);
        fwTree_getNodeDevice(_fwTree_getNodeName(sPath), sPanelDp, sDpType, dsExceptions);
        sPanelFile = unPanel_convertDpPanelNameToPanelFileName(sPanelDp);
        
        int iUserDataLength = dynlen(dsUserData);
        if(iUserDataLength <= 0) 
        { 
          // Case no userData
          dsUserData[1] = "";
          iUserDataLength = dynlen(dsUserData);
        }
        
        if((getPath(PANELS_REL_PATH, sPanelFile) != "") && (iUserDataLength > 0) && (sDpType == UN_PANEL_DPTYPE) && dpExists(sPanelDp))
        {
          string sPanelButton;
          unGenericDpFunctions_getContextPanel(sPanelDp, sPanelButton, dsExceptions);
          bBad = false;
          
          if(iUserDataLength > c_unPanelConfiguration_maxPanelNavigation) 
          {
            // 11, give the sDpNodeName
            unGraphicalFrame_showUserPanel(sPanelFile, dsUserData[1], sPath);
            unGraphicalFrame_showContextualPanel(sPanelButton, sPath);
          }
          else 
          {
            unGraphicalFrame_showUserPanel(sPanelFile, dsUserData[1], sPanelDp);
            unGraphicalFrame_showContextualPanel(sPanelButton, sPanelDp);
          }
        }
      }
    }
    
    if(bBad)
    {
      unGraphicalFrame_ChildPanelOnCentralModal("vision/MessageWarning","Warning",makeDynString("$1:" + unGenericDpFunctions_getAlias(sDeviceName) + getCatStr("unDB","EMPTYSYNOPTIC")));
    }
  }
  else
  {
    string sDpPanel = unPanel_convertPanelFileNameToDp(sPath);
    
    if(!dpExists(sDpPanel)) 
    {
      unPanel_create(sPath, sDpPanel, dsExceptions);
    }
    
    if (dynlen(dsExceptions) > 0)
    {
      unGraphicalFrame_ChildPanelOnCentral(sPath, unGenericDpFunctions_getAlias(sDeviceName) + " " + sDescription + " Synoptic", makeDynString());
    }
    else
    {
      unGraphicalFrame_showUserPanel(sPath, "", sDpPanel);
      
      string sPanelName; 
      unGenericDpFunctions_getContextPanel(sDpPanel, sPanelName, dsExceptions);
      
      if (dynlen(dsExceptions) <= 0)
      {
        unGraphicalFrame_showContextualPanel(sPanelName, sDpPanel);
      }
    }
  }
}

/**
  @par Description:
  Get a list of DP matching the given filter.
  
  @par Usage:
  Internal.
  
  @param[in]  dsFilter            dyn_string, The filter to find DP for.
  @param[in]  bMultiDomain        bool,       Indicates whether or not more than one domain are filtered.
  @param[in]  bMultiNature        bool,       Indicates whether or not more than one nature are filtered.
  @param[in]  dsFilterSystemNames dyn_string, On which systems to check (default: all connected).
*/
dyn_string  _unAlarmScreen_dpListFilter(const dyn_string dsFilter, const bool bMultiDomain, const bool bMultiNature, dyn_string dsFilterSystemNames = fwAlarmScreenGeneric_getConnectedSystems())
{
  dyn_string dsFilterDps;
  
  dyn_string dsRequestSystem;
  dyn_string dsDpList;
  string sDpComment;
  _unAlarmScreen_getInitDpProperties(dsRequestSystem, dsDpList, sDpComment);
    
  if (unAlarmScreen_g_iAlarmScreenType != UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY)
  {
    string sQueryWHERE;
    string sSystem;
    dyn_string dsFilterDpsDomain;
    dyn_string dsFilterDpsNature;
    dyn_string dsObjects = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_OBJECT] , UNALARMSCREEN_FILTER_COMBO_DELIMITER);  // Look for Object filter
    
    if (dynlen(dsObjects) > 0 && dynContains(dsObjects, UNALARMSCREEN_FILTER_ALL) == 0)
    {
      sQueryWHERE = " AND (";
      for(int i = 1; i <= dynlen(dsObjects) ; i++)
      {
        sQueryWHERE = sQueryWHERE + "_DPT = \"" + dsObjects[i] + "\" OR ";
      }
      sQueryWHERE = substr(sQueryWHERE, 0, strlen(sQueryWHERE) - 3) + ") ";  // Delete "OR "
    }
    else
    {
      sQueryWHERE = "";
    }
  
    // For each system, get dpName according to Domain and Nature filters    
    for (int i = 1 ; i <= dynlen(dsFilterSystemNames) ; i++)
    {
      if (strpos(dsFilterSystemNames[i], ":")>=0)
      {
        sSystem = dsFilterSystemNames[i];
      }
      else
      {
        sSystem = dsFilterSystemNames[i] + ":";
      }
      
      // "group:::..." DOESN'T WORK IN DISTRIBUTED MODE!!!
      if (dynlen(dsFilterSystemNames) == 1 && bMultiDomain && !bMultiNature && sQueryWHERE == "")    // Used directly DpGroup in Alerts Query - Normally faster
      {
        dyn_string dsTemp;
        dyn_string dsSplit = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN] , UNALARMSCREEN_FILTER_COMBO_DELIMITER);
        for (int j=1; j<=dynlen(dsSplit); j++)
        {
          dsTemp[j] = "group:::Domain:" + dsSplit[j];
        }        
        dynAppend(dsFilterDpsDomain, dsTemp);        
      }
      else if (dynlen(dsFilterSystemNames) == 1 && !bMultiDomain && bMultiNature && sQueryWHERE=="")  //used directly DpGroup in Alerts Query
      {
        dyn_string dsTemp;
        dyn_string dsSplit = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE] ,UNALARMSCREEN_FILTER_COMBO_DELIMITER);
        for (int j = 1; j <= dynlen(dsSplit) ; j++)
        {
          dsTemp[j] = "group:::Nature:" + dsSplit[j];
        }        
        dynAppend(dsFilterDpsNature, dsTemp);        
      }        
      else if (bMultiDomain || bMultiNature)  // Used dpN list
      {
        dyn_string dsTemp = _unAlarmScreen_getList(dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN], bMultiDomain, sQueryWHERE, UN_DOMAIN_PREFIX, sSystem);
        dynAppend(dsFilterDpsDomain, dsTemp);
        
        dsTemp = _unAlarmScreen_getList(dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE], bMultiNature, sQueryWHERE, UN_NATURE_PREFIX, sSystem);
        dynAppend(dsFilterDpsNature, dsTemp);              
      }
    }
      
    // Merge the results  
    dynUnique(dsFilterDpsDomain);
    dynUnique(dsFilterDpsNature);
    
    int iLenDomain = dynlen(dsFilterDpsDomain);
    int iLenNature = dynlen(dsFilterDpsNature);
     
    if (iLenDomain>0 && iLenNature>0) // If a query was done on Domains and Natures
    {
      dsFilterDps = dynIntersect(dsFilterDpsDomain, dsFilterDpsNature);
    }
    else if (iLenDomain>0 && iLenNature==0 && !bMultiNature) // If a query was done for Domains and Natures
    {
      dsFilterDps = dsFilterDpsDomain;
    }
    else if (iLenDomain>0 && iLenNature==0 && bMultiNature) // If a query was done only for Domains
    {
      dynClear(dsFilterDps);
    }
    else if (iLenDomain==0 && iLenNature>0 && !bMultiDomain) // If a query was done only for Natures
    {
      dsFilterDps = dsFilterDpsNature;
    }
    else if (iLenDomain == 0 && iLenNature > 0 && bMultiDomain) // If a query was done for Domains and Natures
    {
      dynClear(dsFilterDps);
    }
    else if ((iLenDomain == 0) && (iLenNature == 0) && (bMultiDomain || bMultiNature))
    {
      dynClear(dsFilterDps);
    }    
        
    if (dynlen(dsFilterDps) == 0 && !bMultiDomain && !bMultiNature) // No filter for the moment  
    {
      if ((dsFilter[UNALARMSCREEN_FILTER_FIELD_OBJECT] != UNALARMSCREEN_FILTER_ALL) && (strltrim(dsFilter[UNALARMSCREEN_FILTER_FIELD_OBJECT]) != ""))   //if filter on Object          
      {
        //pattern on DpName (5 fields)
        dyn_string dsPattern;
        for(int i = 1 ; i <= UN_DPNAME_FIELD_LENGTH ; i++)
        {
          dsPattern[i] = UNALARMSCREEN_FILTER_ALL;
        }
    
        //built object pattern
        dsObjects = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_OBJECT] , UNALARMSCREEN_FILTER_COMBO_DELIMITER);
        for(int i = 1 ; i<= dynlen(dsObjects) ; i++)
        {
          dsPattern[UN_DPNAME_FIELD_OBJECT] = dsObjects[i];
          string sObjectPattern = "";
          for(int j = 1 ; j <= UN_DPNAME_FIELD_LENGTH ; j++)
          {
            sObjectPattern = sObjectPattern + dsPattern[j] + UN_DPNAME_SEPARATOR;
          }
          
          sObjectPattern = substr(sObjectPattern, 0, strlen(sObjectPattern) - strlen(UN_DPNAME_SEPARATOR));
          
          //update existing list
          for(int j = 1; j <= dynlen(dsDpList) ; j++)
          {
            dynAppend(dsFilterDps, sObjectPattern + substr(dsDpList[j], strpos(dsDpList[j], ".")));
          }
        }
      }
      else  // no new filter => set filter from memory
      {
        dsFilterDps = dsDpList;
      }
    }
    else if (dynlen(dsFilterDps) == 0)
    {
      dsFilterDps = makeDynString("NOT-VALID-DP");
    }
    else 
    {
      dsFilterDps = _unAlarmScreen_checkDeviceType(dsFilterDps, dsObjects);
    }
    
    string sApplication = dsFilter[UNALARMSCREEN_FILTER_FIELD_LENGTH + 1];
    dyn_string dsApplications = strsplit(sApplication, UNALARMSCREEN_FILTER_COMBO_DELIMITER);
    
    if ((dynContains(dsApplications, "*") < 1) || g_unicosHMI_bApplicationFilterInUse)
    {
      dsFilterDps = _unAlarmScreen_checkApplication(dsFilterDps, dsApplications);
    }
    
    if (dynlen(dsFilterDps) == 0)
    {
      dsFilterDps = makeDynString("NOT-VALID-DP");
    }
  }
  else // UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY  
  {
    // Update existing list
    for(int i = 1; i <= dynlen(dsDpList) ; i++)
    {
      dynAppend(dsFilterDps, dsDpList[i]);
    }
  }
  
  dynUnique(dsFilterDps);
  return dsFilterDps;
}

/**
  @par Description:
  Check which of the given DPs is of any of the given device types.
  
  @par Usage:
  Internal.
  
  @param[in]  dsDp      dyn_string, The list of DP in which to check.
  @param[in]  dsDevType dyn_string, The list of device types.
  
  @return The list of DP from the input that are of any of the given device types.
*/
dyn_string  _unAlarmScreen_checkDeviceType(dyn_string dsDp, dyn_string dsDevType)
{
  dyn_string dsRes;
  
  if(dynlen(dsDevType) == 1) 
  {
    if(dsDevType[1] == "*")
    {
      return dsDp;
    }
  }
  
  for(int i = 1 ; i <= dynlen(dsDp) ; i++) 
  {
    if(dynContains(dsDevType, dpTypeName(dpSubStr(dsDp[i], DPSUB_SYS_DP))) > 0)
    {
      dynAppend(dsRes, dsDp[i]);
    }
  }
  
  return dsRes;
}

/**
  @par Description:
  Check which of the given DPs is of any of the given applications.
  
  @par Usage:
  Internal.
  
  @param[in]  dsDp        dyn_string, The list of DP in which to check.
  @param[in]  dsApplParam dyn_string, The list of applications.
  
  @return The list of DP from the input that are of any of the given applications.
*/
dyn_string  _unAlarmScreen_checkApplication(dyn_string dsDp, dyn_string dsApplParam)
{
  dyn_string dsResult;  
  
  dyn_string dsAppl = dsApplParam;
  if (g_unicosHMI_bApplicationFilterInUse)
  {
    synchronized(g_unicosHMI_dsApplicationList)
    {
      if (dynContains(dsApplParam, "*") > 0)
      {
        dsAppl = g_unicosHMI_dsApplicationList;
      }
      else
      {
        dsAppl = dynIntersect(dsAppl, g_unicosHMI_dsApplicationList);
      }
    }
  }
  
  // For each DP in the filter
  for(int i = 1 ; i <= dynlen(dsDp) ; i++) 
  {
    // Current dp
    dyn_string dsTemp = strsplit(dsDp[i], UN_DPNAME_SEPARATOR);
      
      
    // For each application in the list
    for (int j = 1 ; j <= dynlen(dsAppl) ; j++)
    {
      // Current application      
      string sAppl = dsAppl[j];
    
      if(dynlen(dsTemp) == UN_DPNAME_FIELD_LENGTH) 
      {
        if(dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION] == sAppl)
        {
          dynAppend(dsResult, dsDp[i]);
        }
        else if(dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION] == "*")
        {
          dynAppend(
              dsResult, 
              dsTemp[UN_DPNAME_FIELD_PREFIX]  + UN_DPNAME_SEPARATOR + 
              dsTemp[UN_DPNAME_FIELD_PLCNAME] + UN_DPNAME_SEPARATOR+
              sAppl + UN_DPNAME_SEPARATOR +
              dsTemp[UN_DPNAME_FIELD_OBJECT] + UN_DPNAME_SEPARATOR +
              dsTemp[UN_DPNAME_FIELD_NUMBER]);
        }
      }
      else // case no standard UNICOS DP format --> rebuilt it!
      { 
        int iPos = strpos(dsDp[i], ".");
        string sTemp = substr(dsDp[i], iPos, strlen(dsDp[i]) - iPos);
        dynAppend(
            dsResult, 
            "*" + UN_DPNAME_SEPARATOR +
            "*" + UN_DPNAME_SEPARATOR +
            sAppl + UN_DPNAME_SEPARATOR +
            "*" + UN_DPNAME_SEPARATOR +
            "*"+sTemp
          );
      }
    } // End application loop
  } // End DP loop
  dynUnique(dsResult);
  
  return dsResult;
}

/**
  @par Description:
  Get a list of DP that match the given parameters.
  
  @par Usage:
  Internal.
  
  @param[in]  sField      string, Domain or Nature.
  @param[in]  bMulti      bool,   Whether or not several elements are part of the query.
  @param[in]  sQueryWHERE string, The query to get DPs.
  @param[in]  sPrefix     string, Domain/Nature.
  @param[in]  sSystem     string, The system on which to check.
*/
dyn_string  _unAlarmScreen_getList(const string sField, const bool bMulti, const string sQueryWHERE, const string sPrefix, const string sSystem)
{  
  dyn_string dsFilterDps;
  dyn_string dsGroup;
    
  if ((bMulti) && (sField != UNALARMSCREEN_FILTER_ALL) && (strltrim(sField) != ""))
  {
    dsGroup = strsplit(sField , UNALARMSCREEN_FILTER_COMBO_DELIMITER);
  }
  else
  {
    dsGroup = makeDynString();
  }
  
  string sQueryFROM = _unAlarmScreen_QueryGroup(dsGroup, sPrefix, sSystem);
  if (sQueryFROM != "") // Full query
  {
    string sQueryGroup = "SELECT \'_alert_hdl.._type\' ";
    
    sQueryGroup = sQueryGroup + sQueryFROM;
    
    sQueryGroup = sQueryGroup + "REMOTE \'" + sSystem + "\' ";
          
    sQueryGroup = sQueryGroup + " WHERE \'_alert_hdl.._type\' != 0 ";
    
    if (sQueryWHERE != "")
    {
      sQueryGroup = sQueryGroup + sQueryWHERE;
    }   
    
    dyn_dyn_anytype ddDps;
    dpQuery(sQueryGroup, ddDps);
  
    for(int i = 2 ; i <= dynlen(ddDps) ; i++)
    {
      dynAppend(dsFilterDps, dpSubStr(ddDps[i][1],DPSUB_SYS_DP_EL));
    }
  }
    
  return dsFilterDps;  
}

/**
  @par Description:
  Transform a DP group list to a dpQuery 'FROM' clause with a list of DPs.
  
  @par Usage:
  Internal.
  
  @param[in]  dsFilter  dyn_string, The list of DP groups.
  @param[in]  sPrefix   string,     Nature/domain indicator.
  @param[in]  sSystem   string,     System on which to look for.
*/
string      _unAlarmScreen_QueryGroup(const dyn_string dsFilter, const string sPrefix, const string sSystem)
{
  string sFROM;

  // Get DP group
  dyn_string dsDpGroup;  
  int iFilterLength = dynlen(dsFilter);
  for(int i = 1 ; i <= iFilterLength ; i++)                  
  {
    dsDpGroup[i] = unGenericDpFunctions_groupNameToDpName(sPrefix + dsFilter[i], sSystem);
  }

  // Delete groups that doesn't have valid DPE -> they make the dpQuery crash
  int iIndex = 1;
  while (iIndex <= iFilterLength)
  {
    if (dsDpGroup[iIndex] != "")
    {
      if (dpExists(dsDpGroup[iIndex]))
      {
        dyn_string dsCurrentDpGroupDpList;
        int iRes = dpGet(dsDpGroup[iIndex] + ".Dps", dsCurrentDpGroupDpList); 
        int iDpGroupListLength = dynlen(dsCurrentDpGroupDpList);
        
        if (iRes < 0 || iDpGroupListLength == 0)
        {
          dynRemove(dsDpGroup, iIndex);
        }
        else
        {
          int iIndex2 = 1;
          bool bDpExists = false;
          while ((iIndex2 <= iDpGroupListLength) && (!bDpExists))
          {
            bDpExists = dpExists(dsCurrentDpGroupDpList[iIndex2]);
            iIndex2++;
          }
          
          if (!bDpExists)
          {
            dynRemove(dsDpGroup, iIndex);
          }
          else
          {
            iIndex++;
          }  
        }
      }  
      else
      {
        dynClear(dsDpGroup);
        iIndex = iFilterLength + 1; // Exit
      }
    }
    else
    {
      dynRemove(dsDpGroup, iIndex);
    }
      
    iFilterLength = dynlen(dsDpGroup);
  }

  // Now Dps from Groups can be use for FROM in query
  if (dynlen(dsDpGroup) > 0)                    
  {
    sFROM = " FROM \'{";
    for(int i = 1 ; i <= dynlen(dsDpGroup) ; i++)
    {
      sFROM = sFROM + "DPGROUP(" + substr(dsDpGroup[i],strpos(dsDpGroup[i],":")+1) + "),";
    }
    
    sFROM = substr(sFROM, 0, strlen(sFROM) - 1) + "}\' ";  // Delete last ","
    if (dynlen(dsDpGroup) == 1)
    {
      strreplace(sFROM, "{", "");
      strreplace(sFROM, "}", "");
    }
  }
  else
  {
    sFROM = "";
  }

  return sFROM;
} 

// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// -------------------------- RIGHT CLICK FUNCTIONS------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// All the code in here comes from the old UNICOS alert panel.
// It should be redone entirely because it's badly implemented (uselessly complex and hard to understand) but there is a high risk of breaking it.

dyn_string  unAlarmScreen_getRightClickMenuOptions(int iRow, string sColumn)
{
  dyn_string dsMenu;
  
	comboVisibleColumnList.items() = listColumnsView.items;
      
  const string sDpProp = aes_getPropDpName(AES_DPTYPE_PROPERTIES, true, AESTAB_TOP, false, false); 
  const string sTable = AES_TABLENAME_TOP;
  const bool bAESRow = g_alertRow;
    
  unsigned iRunMode;  
  aes_getRunMode(sDpProp, iRunMode);
  if((iRunMode != AES_RUNMODE_RUNNING) && !AES_FORCE_CLICK)
  {
    return makeDynString();
  }

  setInputFocus(myModuleName(), myPanelName(), sTable);

  int iScreenType;
  aes_getScreenType(sDpProp, iScreenType);
  
  string sContent;
  getValue(sTable, "cellValueRC", iRow, _DPID_, sContent);
  
  if(sContent != "" || g_alertRow)
  {
    bool bEmptyRow = false;

    // Freeze table
    setValue(sTable, "stop", true);

    if(g_alertRow && (sContent == "" ))
    {
      bEmptyRow = true;
    }
    
    dsMenu = unAlarmScreen_displayMenu(iScreenType, iRow, bEmptyRow, sDpProp, sTable, sContent, bAESRow, unAlarmScreen_g_iAlarmScreenType);
  }
  
  return dsMenu;
}

void        unAlarmScreen_treatRightClickAnswer(int iAnswer, int iRow, string sColumn, dyn_string dsMenu)
{
  const string sPropertiesDp = aes_getPropDpName(AES_DPTYPE_PROPERTIES, true, AESTAB_TOP, false, false); 
  string sDpe;
  getValue(AES_TABLENAME_TOP, "cellValueRC", iRow, _DPID_, sDpe);
  
  string sDpName = unGenericDpFunctions_getDpName(sDpe);  // Get DpName
  string sAlias = unGenericDpFunctions_getAlias(sDpName); // Get alias
  string sDpType = dpTypeName(sDpName);
  
  dyn_string dsFunctions;
  dyn_string dsExceptions;
  dyn_string dsExceptionsBis;
  unGenericObject_GetFunctions(sDpType, dsFunctions, dsExceptions);
  
  string sFunction;
  if (dynlen(dsFunctions) > 0)    
  {
    sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_HANDLEMENUANSWER];
  }
   
  if (sFunction != "" && sDpName !="")
  {
    // Common part
    evalScript(
        dsExceptionsBis, 
        "dyn_string main(string deviceName, string sDpType, dyn_string dsMenuList, int menuAnswer)" + 
        "{" + 
        "  dyn_string dsExceptions;" +
        "  if (isFunctionDefined(\"" + sFunction + "\"))" +
        "  {" +
        "    " + sFunction + "(deviceName, sDpType, dsMenuList, menuAnswer);" +
        "  }" +
        "  else " +
        "  {" +
        "    dsExceptions = makeDynString();" +
        "  }" +
        "  return makeDynString();" +
        "}", 
        makeDynString(), 
        sDpName, 
        sDpType, 
        dsMenu, 
        iAnswer
      );
    
    dynAppend(dsExceptions, dsExceptionsBis);
  }

  // Columns part
  unAlarmScreen_actionRightClick(iAnswer, false, iRow, sPropertiesDp , AES_TABLENAME_TOP);  

  
  if (dynlen(dsExceptions) > 0)
  {
    unSendMessage_toExpertException("Right Click", dsExceptions);
  }
}

dyn_string  unAlarmScreen_displayMenu(const unsigned screenType, const int row, bool emptyRow, const string sDpProp, const string sTable, const string sDpe, const bool bAESRow, const int iUnAlertPanel)
{
  string sDpName = unGenericDpFunctions_getDpName(sDpe);  // Get DpName
  string sAlias = unGenericDpFunctions_getAlias(sDpName); // Get alias
  string sDpType = dpTypeName(sDpName);
  string sFunction;
  
  dyn_string dsMenuList;
  dyn_string dsFunctions;
  dyn_string dsAccessOk;
  dyn_string dsExceptions;
  dyn_string dsExceptionsBis;
  
  
  
  
  if (iUnAlertPanel == UNALARMSCREEN_PANEL_ALL_ALARMS)
  {
    if (sAlias != "" && sDpType != "")
    {    
      unGenericButtonFunctionsHMI_isAccessAllowedMultiple(sDpName, sDpType, dsAccessOk, dsExceptions);
      
      // --------------------------------------
      // --- 1) Build menu
      // --------------------------------------
      unGenericObject_GetFunctions(sDpType, dsFunctions, dsExceptions);
      if (dynlen(dsFunctions)>0)
      {
        sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_MENUCONFIG];
      }
    }
    
    if (sFunction != "" && sDpName !="")
    {
      if (substr(sDpName, strlen(sDpName)-1, strlen(sDpName))==".")
      {
        sDpName = substr(sDpName, 0, strlen(sDpName) - 1);
      }
      evalScript(
          dsMenuList, 
          "dyn_string main(string device, string sType, dyn_string dsAccessOk)" +
          "{" + 
          "  dyn_string dsMenuTemp;" +
          "  if (isFunctionDefined(\"" + sFunction + "\"))" +
          "  {" +
          "    " + sFunction + "(device, sType, dsAccessOk, dsMenuTemp);" +
          "  }" +
          "  else " +
          "  {" +
          "    dsMenuTemp = makeDynString();" +
          "  }" +
          "  return dsMenuTemp;" + 
          "}", 
          makeDynString(), 
          sDpName, 
          sDpType, 
          dsAccessOk
        );
    }
  }
  
  // Copy line to clipboard
  dynInsertAt(dsMenuList, "SEPARATOR", 1);
  dynInsertAt(dsMenuList, "PUSH_BUTTON, Copy line to clipboard, " + UNALARMSCREEN_POPUPMENU_INDEX_COPY_LINE +", 1", 1);
  
  if (sAlias != "")  
  {
    dynInsertAt(dsMenuList, "PUSH_BUTTON, " + sAlias + ", 1000, 1", 1);
  }

  if (iUnAlertPanel == UNALARMSCREEN_PANEL_ALL_ALARMS)
  {
    dynInsertAt(dsMenuList, "SEPARATOR", 2);
  }
    
  unsigned uMode;
  aes_getPropMode(sDpProp, uMode);   // (type) open, closed, current
  
  unsigned uRunMode;
  aes_getRunMode(sDpProp, uRunMode);
  unAlarmScreen_viewRightClick(dsMenuList, uMode, uRunMode, screenType, row, sTable, bAESRow);

  return dsMenuList;
}

void        unAlarmScreen_viewRightClick(dyn_string &menuList, string sMode, string sRunMode, unsigned sScreenType, int iRow, string sTable, bool bRow)
{
  dyn_string dsColumnsList;
  dyn_string dsMenu_text;
  
  int iLen;
  
  bool bInsertComment = true;
  bool bAck = true;
  
  // Insert ack
  if(sMode == AES_MODE_CLOSED || sRunMode != AES_RUNMODE_RUNNING)
  {
    bAck = false;
  }
 
  // Insert comment - not possible at sum alert types
  if(sScreenType == AESTYPE_ALERTS )
  {
    if(sRunMode != AES_RUNMODE_RUNNING )
    {
      bInsertComment = false;
    }
  }

  dsMenu_text[MENU_PB] = "PUSH_BUTTON";
  dsMenu_text[MENU_CB] = "CASCADE_BUTTON";
  dsMenu_text[MENU_SP] = "SEPARATOR";

  // 1) Build columns view
  getValue(sTable, "columnCount", iLen);
  
  for(int i = 1 ; i <= iLen ; i++)
  {
    string sMessage;
    string sColumn;
    bool bVisible;    
    
    getMultiValue(
        sTable, "columnVisibility", i - 1, bVisible, 
        sTable, "columnHeader", i - 1, sMessage, 
        sTable, "columnName", i - 1, sColumn
      );
      
    // Set colors to see selected line
    if (sColumn != "abbreviation" && sColumn != "acknowledge" && !bRow && sColumn != "s")
    {    
      setMultiValue(
          sTable, "cellBackColRC", iRow, sColumn, "_WindowText",
          sTable, "cellForeColRC", iRow, sColumn, "_Window"
        );
    }
    
    int iPos = dynContains(comboVisibleColumnList.items(), sColumn);

    if(iPos > 0 && sMessage != "")
    {
      if (bVisible)
      {
        sMessage = dsMenu_text[MENU_PB] + ",Hide " + sMessage;
      }
      else
      {
        sMessage = dsMenu_text[MENU_PB] + ",View " + sMessage;
      }
      
      int n = iPos + UN_NUM_AESVIEW;  
      sMessage = sMessage + "," + n + ",1";
      
      dynAppend(dsColumnsList, sMessage);    //Column Text
    }
  }

  dynAppend(dsColumnsList, dsMenu_text[MENU_SP]);
  dynAppend(dsColumnsList, dsMenu_text[MENU_PB] + ",View All, " + (UN_NUM_AESVIEW + 100) + ", 1");

  // Manage autorizations
  string sAck;
  if (bAck)
  {
    sAck = "1";
  }
  else
  {
    sAck = "0";
  }
  
  string sAdd;
  if (bInsertComment)
  {
    sAdd = "1";
  }
  else
  {
    sAdd = "0";  
  }
  
  int iAck;
  sscanf(sAck, "%d", iAck);
  if (iAck == 0)
  {
    for (int i = 1; i <= dynlen(menuList) ; i++)
    {
      if (patternMatch("PUSH_BUTTON, Ack. Alarm*", menuList[i]))
      {
        dyn_string dsSplit = strsplit(menuList[i], ",");
        if (dynlen(dsSplit) >= 4)
        {
          menuList[i] = dsSplit[1] + "," + dsSplit[2] + "," + dsSplit[3] + "," + sAck;
          break; // Exit for loop
        }
      }
    }
    
    
    for (int i = 1; i <= dynlen(menuList) ; i++)
    {
      if (patternMatch("PUSH_BUTTON, Mask PVSS Alarm*", menuList[i]))
      {
        dyn_string dsSplit = strsplit(menuList[i], ",");
        if (dynlen(dsSplit) >= 4)
        {
          menuList[i] = dsSplit[1] + "," + dsSplit[2] + "," + dsSplit[3] + "," + sAck;
          break; // Exit for loop
        }
      }
    }
  }
  
  // Insert AES items
  iLen = dynlen(menuList);
  int iPos = iLen;
  for (int i = 1; i <= iLen && iPos == iLen ; i++)
  {
    if (patternMatch("CASCADE_BUTTON*", menuList[i]))
    {
      iPos = i;
    }
  }
  
  dynInsertAt(menuList, "CASCADE_BUTTON, View, 1", iPos+1);
  dynInsertAt(menuList, dsMenu_text[MENU_SP], iPos+1);

  // IS-669: problem in case of distributed
  dynInsertAt(menuList, dsMenu_text[MENU_PB] + ", Add Comment, " + (UN_NUM_AESVIEW + 300) + ", 0" /*+ sAdd*/, iPos + 1);
  dynInsertAt(menuList, dsMenu_text[MENU_PB] + ", Details, " + (UN_NUM_AESVIEW + 200) + ", 0", iPos + 1);
  dynInsertAt(menuList, dsMenu_text[MENU_SP], iPos + 1);

  dynAppend(menuList, "View");    
                              
  iLen = dynlen(dsColumnsList);
  for (int i = 1 ; i <= iLen ; i++)
  {
    dynAppend(menuList, dsColumnsList[i]);
  }
}

void        unAlarmScreen_actionRightClick(int iAnswer, bool bAESRow, int iRow, string sDpProp, string sTable)
{
  int iLen;
  getValue(sTable, "columnCount", iLen); 

  if (UNALARMSCREEN_POPUPMENU_INDEX_COPY_LINE == iAnswer)
  {
    unAlarmScreen_rightClickCopyLineToClipboard(iRow);
  }
  else if (iAnswer == (UN_NUM_AESVIEW + 100)) // View all
  {
    for (int i = 0 ; i < iLen ; i++)
    {
      string sColumn;
      getValue(sTable, "columnName", i, sColumn);
      if (dynContains(comboVisibleColumnList.items(), sColumn))
      {
        setValue(sTable, "columnVisibility", i, TRUE);
      }
    }
    
    if (bAESRow)
    {
      unLists_initTimeInfo(-1);
    }
  }
  else if (iAnswer > UN_NUM_AESVIEW && iAnswer < (UN_NUM_AESVIEW + 100)) // One by one
  {
    int iPos = iAnswer - UN_NUM_AESVIEW;
    string sAction = comboVisibleColumnList.items()[iPos];
    bool bVisible;
    int k = 0;
    int iTemp;
    string sColumn;
    for (iTemp = 0; iTemp < iLen && k == 0 ; iTemp++)
    {
      getValue(sTable, "columnName", iTemp, sColumn);
      
      if (sAction == sColumn)
      {
        getValue(sTable, "columnVisibility", iTemp, bVisible);        
        setValue(sTable, "columnVisibility", iTemp, !bVisible);                      
        k = iLen;
      }
    }
    
    iTemp--;
    
    if (bAESRow)
    {      
      if (sColumn == "timeStr")                //local time
      {
        setValue(sTable, "columnVisibility", iTemp-1, bVisible);
        unLists_initTimeInfo(bVisible);
      }            
      else if (sColumn=="UTCtimeStr")        //UTC time
      {
        setValue(sTable, "columnVisibility", iTemp+1, bVisible);
        unLists_initTimeInfo(!bVisible);        
      }        
    }
  }
  else if (iAnswer == (UN_NUM_AESVIEW + 200))
  {
    string sPanel;
    string sDpId;
    string sTime;
    string sCount;
    
    int iDpeType;
    
    getValue(AES_TABLENAME_TOP, 
        "cellValueRC", iRow, _DPID_, sDpId,
        "cellValueRC", iRow, _TIME_, sTime 
      );

    iDpeType = dpElementType(sDpId);

    // Check alarmtype - if sumalert / display sumalertdetails instead of details !    
    if( aes_checkSumAlert( iDpeType ) )
    {
      aes_displaySumalertDetails( sDpId, iRow, sDpProp );
    }
    else
    {
      sPanel = "vision/unAlertPanel/unAlertPanel_Details.pnl";
      getValue("", "cellValueRC", iRow, _COUNT_, sCount);

      // Start child panel for detail information
      unGraphicalFrame_ChildPanelOnCentralModal(
          sPanel, 
          "",
          makeDynString(
              "$dpid:" + dpSubStr(sDpId, DPSUB_SYS_DP_EL),
              "$time:" + sTime,
              "$count:" + sCount
            )
        );
    }
  }
  else if (iAnswer == (UN_NUM_AESVIEW + 300))
  {
    mapping mTableRow;
  
    synchronized(g_bTableLineSynchronisation)
    { 
      int length;
      dyn_anytype alertRow;  
    
      alertRow = this.getLineN(iRow);
    
      length = dynlen(alertRow);
      for(int i = 1; i <= length ; i++)
      {
        mTableRow[this.columnName(i-1)] = alertRow[i];
      }
    }
    
      aes_insertComment(iRow, mTableRow);  
   }
  
  // Reset colors for selected line
  if(!bAESRow)
  {
    for (int i = 0; i < iLen; i++)
    {
      int iLineCount;
      getValue(sTable, "lineCount", iLineCount);
      if (iRow < iLineCount) // To fix a bug I do not understand because of those complex menu functions.
      {
        string sColumn;
        getValue(sTable, "columnName", i, sColumn);
        if (sColumn != "abbreviation" && sColumn != "acknowledge" && sColumn != "s")
        {
          setMultiValue(
              sTable, "cellBackColRC", iRow, sColumn, "_3DFace",
              sTable, "cellForeColRC", iRow, sColumn, "_WindowText"
            );
        }
      }
      else
      {
        return;
      }
    }
  }
      
}  

/**
  @par Description:
  Action when right-click "Copy line to clipboard".
  
  @par Usage:
  Internal.
  
  @param[in]  iRow  int The row clicked.
*/
void        unAlarmScreen_rightClickCopyLineToClipboard(int iRow)
{
  fwAlarmScreenGeneric_copyLineToClipboard(makeDynInt(iRow), AES_TABLENAME_TOP);
}

// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ---------------------------- CALLBACK FUNCTIONS-------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------

/**
  @par Description:
  Callback triggered when the application filter changes.
  
  @par Usage:
  Internal.
  
  @param[in]  sDp     string  The DPE that triggered the callback.
  @param[in]  bIgnore bool    Ignore the callback if set to true.
*/
void        unAlarmScreen_applicationFilterChangedCallBack(string sDp, bool bIgnore)
{
  if (!bIgnore)
  {
    unAlarmScreen_initUIElements(unAlarmScreen_g_iAlarmScreenType);
    unAlarmScreen_applyFilter();
    unAlarmScreen_colorFilter();
  }
}

/**
  @par Description:
  Callback triggered when the busy state of the AES changes.
  
  @par Usage:
  Internal.
  
  @param[in]  sDataPoint  string  The DPE that triggered the CB.
  @param[in]  iBusy       int     The busy state.
*/
void        unAlarmScreen_busyCallBack(string sDataPoint, int iBusy)
{
  if (iBusy == AES_BUSY_START)
  {
    unAlarmScreen_initState.state(0, true);
    fwAlarmScreenGeneric_wait(true); // Show wait progress bar
    
		  
    unAlarmScreen_enable(false);
  }
  else
  {
    delay(0, UNALARMSCREEN_DELAY * 10);	
    fwAlarmScreenGeneric_wait(false); 
    
    unAlarmScreen_enable(true);
  }
}	

/**
  @par Description:
  Callback triggered when a new user logs in.
  
  @par Usage:
  Internal.
  
  @param[in]  sUser string, The user that logged in.
*/
void        unAlarmScreen_userChanged(const string sUser)
{
  dyn_string dsExceptions;
  bool bFileExists;
  
  
  if(isFunctionDefined("unGenericButtonFunctionsHMI_isAccessAllowed"))
  {
    unGenericButtonFunctionsHMI_isAccessAllowed(
        UNALARMSCREEN_PANEL_FILE_NAME, 
        UN_GENERIC_USER_ACCESS, 
        unGenericButtonFunctionsHMI_getPanelAccessLevel(
            UNALARMSCREEN_PANEL_FILE_NAME, 
            bFileExists
          ), 
        unAlarmScreen_g_bGranted, 
        dsExceptions
	  );
  }														
  else
  {
    if(sUser == c_systemIntegrity_noFwAccessControl_userAdmin) 
    {
      unAlarmScreen_g_bGranted = true;
    }
    else
    {
      unAlarmScreen_g_bGranted = false;
    }
  }

  if (shapeExists("saveConfig"))
  {
    setValue("saveConfig", "enabled", unAlarmScreen_g_bState & unAlarmScreen_g_bGranted);
  }
  
  if (unAlarmScreen_g_iAlarmScreenType == UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY)
  {
    acknowledgeButton.visible = false;
  }
  else
  { 
    acknowledgeButton.enabled = unAlarmScreen_g_bState & unAlarmScreen_g_bGranted & unAlarmScreen_g_bAck;
  }
  
  if (fwAlarmScreenGeneric_isTableFilterReduced())
  {
    unAlarmScreen_groups_removeGroups();
  }
  else
  {
    unAlarmScreen_groups_showGroups();  
  }
  
  unAlarmScreen_resizePanel();
}

/**
  @par Description:
  Function called when a system connects.
  
  @par Usage:
  Internal.
  
  @param[in]  sSystem string, The system that was connected.
*/
void        unAlarmScreen_systemConnected(const string sSystem)
{
  _unAlarmScreen_onSystemStateChange();
}

/**
  @par Description:
  Function called when a system disconnects.
  
  @par Usage:
  Internal.
  
  @param[in]  sSystem string, The system that was disconnected.
*/
void        unAlarmScreen_systemDisconnected(const string sSystem)
{
  _unAlarmScreen_onSystemStateChange();
}

/**
  @par Description:
  Reset the alarm screen when a system is connected or disconnected.
  
  @par Usage:
  Internal.
*/
void        _unAlarmScreen_onSystemStateChange()
{
  if (unAlarmScreen_initState.state(0))
  {
    unAlarmScreen_initUIElements(unAlarmScreen_g_iAlarmScreenType);
    unAlarmScreen_applyFilter();
    unAlarmScreen_colorFilter();
  }
}

/**
  @par Description:
  Event handler. See fwAlarmScreenGeneric documentation.
  
  @par Usage:
  Internal.
  
  @param[in]  sDp     string, The DPE that triggered the CB.
  @param[in]  iEVent  int,    The event.
*/
void        unAlarmScreen_eventHandler(const string sDp, const int iEvent)
{
  switch(iEvent)
  {
    case FWALARMSCREEN_GENERIC_CONFIG_EVENT_EXPAND_TABLE_FILTER:
    {  
      if (fwAlarmScreenGeneric_isTableFilterReduced())
      {
        unAlarmScreen_groups_removeGroups();
      }
      else
      {
        if (UNALARMSCREEN_PANEL_NON_INITIALIZED != unAlarmScreen_g_iAlarmScreenType)
        {
          unAlarmScreen_groups_showGroups();
        }
      }
      
      break;
    }
    case FWALARMSCREEN_GENERIC_CONFIG_EVENT_EXPAND_WHOLE:
    {
      unAlarmScreen_resizePanel();
      break;
    }
    default:
    {
      break;
    }
  }
}


// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------- UI FUNCTIONS----------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
// All those functions are called via exec/evalScript from fwAlarmScreenGeneric.

const int UNALARMSCREEN_HEADER_HEIGHT       = 100;
const int UNALARMSCREEN_INFO_HEIGHT         = 100;
const int UNALARMSCREEN_TABLEFILTER_HEIGHT  = 63;
const int UNALARMSCREEN_ALERTFILTER_HEIGHT  = 159;

/**
  @par Description:
  Get the list of panels to show in the configuration window.
  
  @par Usage:
  Internal.
  
  @return The list of panels to show in the configuration window.
*/
dyn_string  unAlarmScreen_getConfigPanels()
{
  dyn_string dsConfigPanels;
  
  if (unAlarmScreen_g_bState && unAlarmScreen_g_bGranted)
  {
    dynAppend(dsConfigPanels, UNALARMSCREEN_GROUPS_CONFIG_PANEL + FWALARMSCREEN_GENERIC_CONFIGURATION_PANEL_SEPARATOR + "Group settings");
  }
  
  
  return dsConfigPanels;  
}

/**
  @par Description:
  Get the columns that a user can show/hide.
  
  @par Usage:
  Internal.
  
  @return The columns that a user can show/hide.
*/
dyn_string  unAlarmScreen_getShowableColumns()
{
  dyn_string dsColumns;
  if (unAlarmScreen_g_iAlarmScreenType == UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY)
  {
    dsColumns = makeDynString(
        UNALARMSCREEN_COLUMN_ID_UTC_TIME, 
        UNALARMSCREEN_COLUMN_ID_LOCAL_TIME, 
        UNALARMSCREEN_COLUMN_ID_DESCRIPTION, 
        UNALARMSCREEN_COLUMN_ID_VALUE, 
        UNALARMSCREEN_COLUMN_ID_ACK
      );
  }
  else
  {
    dsColumns = makeDynString(
        UNALARMSCREEN_COLUMN_ID_UTC_TIME, 
        UNALARMSCREEN_COLUMN_ID_LOCAL_TIME, 
        UNALARMSCREEN_COLUMN_ID_ALIAS, 
        UNALARMSCREEN_COLUMN_ID_DESCRIPTION, 
        UNALARMSCREEN_COLUMN_ID_DOMAIN, 
        UNALARMSCREEN_COLUMN_ID_NATURE, 
        UNALARMSCREEN_COLUMN_ID_NAME, 
        UNALARMSCREEN_COLUMN_ID_VALUE, 
        UNALARMSCREEN_COLUMN_ID_ACK, 
        UNALARMSCREEN_COLUMN_ID_S
      );
  }
  
  return dsColumns;
}

/*
mapping unAlarmScreen_getColumnsSize()
{
  mapping mColumnsSize;  
  // Some columns have fixed size
  mColumnsSize["abbreviation"] = 40;
  mColumnsSize["UTCtimeStr"]   = 150;
  mColumnsSize["timeStr"]      = 125;
  mColumnsSize["nature"]       = 60;
  mColumnsSize["value"]        = 60;
  mColumnsSize["acknowledge"]  = 50;
  mColumnsSize["domain"]       = 80;

  // The rest take 1/3 of what is left each  
  mColumnsSize["alias"]        = 0.33;
  mColumnsSize["description"]  = 0.33;
  mColumnsSize["name"]         = 0.33;
  
  
  return mColumnsSize;
}
*/
void        unAlarmScreen_closePanel()
{
  dyn_string dsFilter;
  dyn_float df;
  
  dsFilter = makeDynString(textFieldCurrentFilter.text());
  
  _fwAlarmScreenGeneric_saveColumnsSize();
  
  PanelOffReturn(df, dsFilter);
}

// ---------------------------------------
// ---------------------------------------
// -- Functions for header manipulation --
// ---------------------------------------
// ---------------------------------------

dyn_string _unAlarmScreen_getHeaderWidgets()
{
  return makeDynString(
      // no items in template
  );
}

int unAlarmScreen_getHeaderHeight()
{
  return UNALARMSCREEN_HEADER_HEIGHT;
}

void unAlarmScreen_showHeader(bool bShow)
{
  dyn_string dsWidgets = _unAlarmScreen_getHeaderWidgets();
  for (int i = 1 ; i <= dynlen(dsWidgets) ; i++)
  {
    setValue(dsWidgets[i], "visible", bShow);
  }
}

// -------------------------------------
// -------------------------------------
// -- Functions for info manipulation --
// -------------------------------------
// -------------------------------------

dyn_string _unAlarmScreen_getInfoWidgets()
{
  return makeDynString(
	);
}

int unAlarmScreen_getInfoHeight()
{
  return UNALARMSCREEN_INFO_HEIGHT;
}

void unAlarmScreen_showInfo(bool bShow)
{
  dyn_string dsWidgets = _unAlarmScreen_getInfoWidgets();
  for (int i = 1 ; i <= dynlen(dsWidgets) ; i++)
  {
    setValue(dsWidgets[i], "visible", bShow);
  }
}

void unAlarmScreen_moveInfo(int iYDiff)
{
  dyn_string dsWidgets = _unAlarmScreen_getInfoWidgets();
  for (int i = 1 ; i <= dynlen(dsWidgets) ; i++)
  {
    int iOldX;
	int iOldY;
	getValue(dsWidgets[i], "position", iOldX, iOldY);
	
	int iNewY = iOldY + iYDiff;	
	setValue(dsWidgets[i], "position", iOldX, iNewY);
  }
}

// ---------------------------------------------
// ---------------------------------------------
// -- Functions for table filter manipulation --
// ---------------------------------------------
// ---------------------------------------------

dyn_string _unAlarmScreen_getTableFilterWidgets()
{  
  dyn_string dsWidgets = makeDynString(
      UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET,
      UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET,
      UNALARMSCREEN_GROUPS_GROUP_POS_ORIG
    );
    
  return dsWidgets;
}

int unAlarmScreen_getTableFilterHeight()
{
  return UNALARMSCREEN_TABLEFILTER_HEIGHT;
}

void unAlarmScreen_showTableFilter(bool bShow)
{
  dyn_string dsWidgets = _unAlarmScreen_getTableFilterWidgets();
  
  for (int i = 1 ; i <= dynlen(dsWidgets) ; i++)
  {
    setValue(dsWidgets[i], "visible", bShow);
  }
  
  if (!bShow)
  {
    setValue(UNALARMSCREEN_GROUPS_NO_GROUP_TEXT, "visible", false);
    setValue(UNALARMSCREEN_GROUPS_GROUPS_DISABLED_TEXT, "visible", false);
  }
  
  setMultiValue(
      UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "visible", false,
      UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET, "visible", false,
      UNALARMSCREEN_GROUPS_GROUP_HIGHLIGHT, "visible", false,
      UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT, "visible", false,
      UNALARMSCREEN_GROUPS_GROUP_POS_ORIG, "visible", false
    );
}

void unAlarmScreen_moveTableFilter(int iYDiff)
{
  dyn_string dsWidgets = _unAlarmScreen_getTableFilterWidgets();  
  
  
  dyn_string dsGroupsIds = listVisibleGroups.items();
  for (int i = 1 ; i <= dynlen(dsGroupsIds) ; i++)
  {
    dsGroupsIds[i] = UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + dsGroupsIds[i];
  }
  
  dynAppend(dsWidgets, dsGroupsIds);
  dynAppend(dsWidgets, UNALARMSCREEN_GROUPS_NO_GROUP_TEXT);
  dynAppend(dsWidgets, UNALARMSCREEN_GROUPS_GROUPS_DISABLED_TEXT);
  
  for (int i = 1 ; i <= dynlen(dsWidgets) ; i++)
  {
    if (shapeExists(dsWidgets[i]))
    {
      int iOldX;
      int iOldY;
      getValue(dsWidgets[i], "position", iOldX, iOldY);	

      int iNewY = iOldY + iYDiff;	
      setValue(dsWidgets[i], "position", iOldX, iNewY);
    }    
  }
}



// ---------------------------------------------
// ---------------------------------------------
// -- Functions for alarm filter manipulation --
// ---------------------------------------------
// ---------------------------------------------

dyn_string _unAlarmScreen_getAlertFilterWidgets()
{
  return makeDynString(
      "applicationLabel",
      "deviceTypeLabel",
      "aliasLabel",
      "descriptionLabel",
      "alarmTextLabel",
      "filter_Alias",
      "filter_Description",
      "filter_AlertText",
      "domainLabel",
      "natureLabel",
      "alarmStateLabel",
      "nameLabel",
      "filter_AlertState",
      "filter_Name",
      "TimeRange",
      "openConfig",
      "saveConfig",
      "refreshConfig",
      "clearFilter",
      "quickFilterLabel",
      
      "AppendText",
      "AppendLines",

      "dateTimeWidgetPlaceholder",
      "dateTimeWidgetPlaceholderText",
 
			UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".selectedTimeZone",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".startNowButton",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".endNowButton",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".startDateChooserButton",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".endDateChooserButton",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".startTimeSpin",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".startTimeLabel",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".endTimeLabel",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".startTimeField",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".startDateField",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".endDateField",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".endTimeSpin",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".endTimeField",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".dateTimeSeparator",
      UNALARMSCREEN_DATETIMEPICKER_PANELNAME+".timeZoneLabel"
      
      
	);
}

int unAlarmScreen_getAlertFilterHeight()
{
  return UNALARMSCREEN_ALERTFILTER_HEIGHT;
}

void _unAlarmScreen_handleDisplayNumberLinesWidgetsVisibility( bool bShow)
{
  if ( shapeExists("TimeRange") == false)
  {
    return;
  }
  
  if ( shapeExists("AppendText") == false)
  {
    return;
  }
  
  if ( shapeExists("AppendLines") == false)
  {
    return;
  }
  
  //To be displayed only if the filter is expanded and the selection is different from Current Alarms
  bool bShowDisplayLines = TimeRange.number != 0 && bShow == true ? true : false;
  
  setValue( "AppendText", "visible", bShowDisplayLines);
  setValue( "AppendLines", "visible", bShowDisplayLines);
}

void unAlarmScreen_showAlertFilter(bool bShow)
{
  dyn_string dsWidgets = _unAlarmScreen_getAlertFilterWidgets();
  for (int i = 1 ; i <= dynlen(dsWidgets) ; i++)
  {
    if (shapeExists(dsWidgets[i]) 
      && (strpos(dsWidgets[i], "DATE_TIME_WIDGET") < 0) //Date time widgets is treated afterwards
      && (strpos(dsWidgets[i], "dateTimeWidget") < 0)   //Date time widget is treated afterwards
      && (strpos(dsWidgets[i], "AppendText") < 0)       //Display # lines is treated aftewards
      && (strpos(dsWidgets[i], "AppendLines") < 0)      //Display # lines is treated aftewards
      )
    {
      setValue(dsWidgets[i], "visible", bShow);
    }
  }
  
  //treat the display # lines visibility
  _unAlarmScreen_handleDisplayNumberLinesWidgetsVisibility( bShow);
  
  //treat the date time widget visibility
  if (shapeExists(UNALARMSCREEN_DATETIMEPICKER_PANELNAME+"."+UNALARMSCREEN_DATETIMEPICKER_WIDGETNAME1))
  {
    fwGeneral_dateTimeWidget_setVisible(bShow);
  }
  
  fwAlarmScreenGeneric_combocheckbox_showWidget(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, bShow);
  fwAlarmScreenGeneric_combocheckbox_showWidget(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, bShow);
  fwAlarmScreenGeneric_combocheckbox_showWidget(UNALARMSCREEN_WIDGET_FILTER_NATURE, bShow);
  fwAlarmScreenGeneric_combocheckbox_showWidget(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, bShow);
  
  // Never visible:
  setMultiValue(
      "txtView", "visible", false,
      "listColumnsView", "visible", false
	);
  
  
}

void unAlarmScreen_moveAlertFilter(int iYDiff)
{
  int iOldX;
  int iOldY; 
  int iNewY; 
    
  dyn_string dsWidgets = _unAlarmScreen_getAlertFilterWidgets();
   for (int i = 1 ; i <= dynlen(dsWidgets) ; i++)
  {
    if(shapeExists(dsWidgets[i]))
    {
      getValue(dsWidgets[i], "position", iOldX, iOldY);    
      iNewY = iOldY + iYDiff;	
      setValue(dsWidgets[i], "position", iOldX, iNewY);
    } 
  }
  
  fwAlarmScreenGeneric_combocheckbox_moveWidget(UNALARMSCREEN_WIDGET_FILTER_APPLICATION, 0, iYDiff);
  fwAlarmScreenGeneric_combocheckbox_moveWidget(UNALARMSCREEN_WIDGET_FILTER_DEVICE_TYPE, 0, iYDiff);
  fwAlarmScreenGeneric_combocheckbox_moveWidget(UNALARMSCREEN_WIDGET_FILTER_NATURE, 0, iYDiff);
  fwAlarmScreenGeneric_combocheckbox_moveWidget(UNALARMSCREEN_WIDGET_FILTER_DOMAIN, 0, iYDiff);
}

