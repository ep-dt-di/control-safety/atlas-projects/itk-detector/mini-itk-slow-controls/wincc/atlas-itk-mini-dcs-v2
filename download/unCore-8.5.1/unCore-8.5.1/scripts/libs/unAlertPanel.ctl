/**@name LIBRARY: unAlertPanel.ctl

@author: Vincent Forest (AB-CO)

Creation Date: 10 03 2003

Modification History:

  07/06/2017: Jean-Charles
    -@jira{FWAH-364} add function unAlertPanel_getCleanModuleName to support alarm screen tabulaiton of devices with . characters

  19/06/2013: Marco Boccioli
    -@jira{IS-1136} unAlert_actionRightClick: The columns were accessed from 0 to maxCol. 
     Must be accessed from 1 to maxCol-1.
    
  21/11/2011: Jean-Charles
    -fix the call to insert comments associated to alerts

  25/09/2008: Herve
    - add application in filter
    - synchronized for reading data
    - faster update of domain/nature/application
    
  30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

  25/01/2008: Herve
    - bug in unAlert_displayMenu when RC on a device
    
  04/12/2007: Frederic
    - update arguments in Alerts.Filter.DpList and .Alerts.Filter.DpComment dpe for UnSystemAlarm List (fix bug with several Sustems and comment with *)
    
  22/10/2007: Herve
    - unAlertPanel_DefaultPanel allow WT link
    
  11/07/2007: Frederic
    dpList for AES unSystemAlarm is now configurable from unApplication DpN

  07/04/2006: Frederic
    update right click function (integrate device right clik)
  
  31/01/2006: Frederic
    change unAlertPanel_applyFilter() =>
    add   unAlert_dpListFilter()
          unAlert_descriptionFilter()
          unAlert_getList()
          unAlertPanel_displayTime()
  
  08/12/2005: Celine
    unAlert_GetSystemsFilter: add systemName in the title of warningMessage. 

  08/11/2005: Frederic
    updates to implement UTC time format

  17/11/2004: Frederic
    add    unAlert_QueryGroup()
  
  17/10/2004: Frederic
    add    unAlert_GetSystemsFilter()
          unAlert_SetSystemsFilter()
  
  14/10/2004: Frederic
    add   unAlert_displayMenu()
          unAlert_onRightClick()
          unAlert_displayDetails()
  
  27/09/2004: Herve
    Modify the unPanel_create

  23/08/2004: Frederic
    Updates to PVSS II 3.0 (update, remove and add functions)
    remove =>
    unAlertPanel_updateTable()
    unAlertPanel_cellAction()
    new =>
    unAlert_refreshCounters()
    unAlertPanel_updateCounters()
    unAlertPanel_getInitDpProperties()
    unAlertPanel_setInitDpProperties()
  
  03/05/2004: Herve
    replace c_unSystemIntegrity_applicationDPName by UN_SYSTEMINTEGRITY_applicationDPName
    
  18/02/2004: Herve
    unPanel_create function: UN_PANEL_WINDOW_TREE_PREFIX prefix missing

version 2.0

External Function : 
  
Internal Functions :
  . unAlertPanel_applyFilter : apply current filter
  . unAlertPanel_displayFilter : display current filter
  . unAlertPanel_DefaultPanel: open Default Panel
  . unAlertPanel_changeMode : set enable status of shapes depending on current mode
  . unAlertPanel_updateCounters : update info on number and unack. alarms
  . unAlertPanel_setEnableHistory : set enable status of history shapes
  . unAlertPanel_setEnableAppend : set enable status of append shapes
  . unAlertPanel_init : check whether unicos or pvss alert panel has to be displayed
  . unAlertPanel_saveFilter : save current filter
  . unAlertPanel_loadFilter : load filter configuration
  . unAlertPanel_timeRequest : set time request
  . unAlertPanel_makeFilter : create the filter string using the shape contents
  . unAlertPanel_getInitDpProperties : load the defined properties
  . unAlertPanel_setInitDpProperties : set the defined properties
  . unAlert_refreshCounters() : update counters every X secondes
  
Purpose: 
This library contains functions used to modify the PVSS alert screen and alert row

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes
*/

#uses "unPanel.ctl"
#uses "unLists.ctl"
#uses "unSendMessage.ctl"

//@{

//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_init
/**
Purpose:  init AES panel

Parameters: None

Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . This function is launched during table init of panel alertScreen / alertRow : it needs the "table" variable
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/
unAlertPanel_init(int iUnAlertPanel)
{
  dyn_string dsUnicosObjects, dsDomain, dsNature, dsSystemNames, dsNames, dsHost, dsTempDomain, dsTempNature, dsSystemConnected;
  dyn_uint duSystemIds;
  int i, iLen;
  string sFilter;
  dyn_int diPort;
  dyn_string dsApplication;


  // 1. set global
  synchronized(g_dsTreeApplication) 
  {
    //DebugTN("Start INIT");
    if (!g_alertRow)
    {
      if ((iUnAlertPanel == UN_ALERTPANEL_CONFIG) || (iUnAlertPanel == UN_ALERTPANEL_SYSTEM_INTEGRITY))
      {
        g_bUnAlertPanel = true;
        g_iUnAlertPanel = iUnAlertPanel;
      }
      else
      {
        g_bUnAlertPanel = true;
        g_iUnAlertPanel = -1;
      }
    }
    else if (g_alertRow)
    {
      g_bUnAlertPanel = false;
      g_iUnAlertPanel = UN_ALERTPANEL_CONFIG;
    }
    else
    {
      g_bUnAlertPanel = false;
      g_iUnAlertPanel = -1;
    }
    
    if (!g_alertRow)
    {
      txtView.text=g_iUnAlertPanel;
    
      // 3. Filter menus
      switch (g_iUnAlertPanel)
      {
        case UN_ALERTPANEL_CONFIG:
        {
          unGenericDpFunctions_getUnicosObjects(dsUnicosObjects);        // Objects
          dynInsertAt(dsUnicosObjects, UN_ALERTPANEL_FILTER_ALL, 1);
          dynAppend(dsUnicosObjects, UN_ALERTPANEL_LIST_NAME);    
          dynUnique(dsUnicosObjects);
          filter_Object.enabled = true;
          filter_Object.items(dsUnicosObjects);        
                  
          filter_AlertText.items(makeDynString(UN_ALERTPANEL_FILTER_ALL, "LL", "L", "H", "HH", "Bad", "Ok"));    // Alert Texts
          filter_AlertText.enabled = true;
          filter_AlertState.items(makeDynString(UN_ALERTPANEL_FILTER_ALL, "Unacknowledged", "Pending", "Unack. + Pending"));  // Alert State
          filter_AlertState.enabled = true;
        
          if (dpExists(UN_APPLICATION_DPNAME))      //always local
          {
            dpGet(UN_APPLICATION_DPNAME + ".unAlerts.description.names", dsNames);    // Alert names
          }
        
          dynInsertAt(dsNames, UN_ALERTPANEL_FILTER_ALL, 1);
          dynUnique(dsNames);
          filter_Name.enabled = true;
          filter_Name.items(dsNames);  

          unDistributedControl_getAllDeviceConfig(dsSystemNames, duSystemIds, dsHost, diPort);          // System names        
        
          dynInsertAt(dsSystemNames, UN_ALERTPANEL_FILTER_ALL, 1);
          dynAppend(dsSystemNames, UN_ALERTPANEL_LIST_NAME);
          dynUnique(dsSystemNames);
          filter_SystemNames.items(dsSystemNames);
          filter_SystemNames.enabled = true;        
            
          filter_Domain.items(g_dsTreeSubsystem1);
          filter_Domain.selectedPos(1);
          filter_Domain.enabled = true;
          filter_Nature.items(g_dsTreeSubsystem2);
          filter_Nature.selectedPos(1);
          filter_Nature.enabled = true;        
        
          filter_Alias.enabled = true;
              
          filter_Description.enabled = true;
        
          
          dsApplication = g_dsTreeApplication;
          // dynRemove(dsApplication, dynlen(dsApplication)); // Remove "List..." at the end
          
          // Load application filter
          if (g_unicosHMI_bApplicationFilterInUse)
          {
            synchronized(g_unicosHMI_dsApplicationList)
            {
              // The "*" disappears, which is necessary to ignore unwanted applications
              dsApplication = dynIntersect(dsApplication, g_unicosHMI_dsApplicationList);    
              
              // Temporary copy of the list (because dynAppend will clear it)
              // /!\ Change : do not display pattern with "*" anymore (e.g. APPL2_*)
              // dyn_string dsTemp = g_unicosHMI_dsMultiAlarmFilter;
              // dynAppend(dsApplication, dsTemp);
            
              // If only not visible (not connected) applications are filtered display the information
              if (dynlen(dsApplication) < 1)
              {
                dynAppend(dsApplication,"--Empty filter--");
              }
              else
              {        
                dynInsertAt(dsApplication, "*", 1);
                dynAppend(dsApplication, UN_ALERTPANEL_LIST_NAME);
              }
            }
          }          
                          
          application_list.items(dsApplication);
          application_list.selectedPos(1);
          application_list.enabled = true;
          break;
        }
        case UN_ALERTPANEL_SYSTEM_INTEGRITY:
        {
          filter_Object.enabled = false;
          filter_Object.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_Domain.enabled = false;
          filter_Domain.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_Nature.enabled = false;
          filter_Nature.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_Name.enabled = false;
          filter_Name.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_AlertText.items(makeDynString(UN_ALERTPANEL_FILTER_ALL, "SI1", "SI2"));    // Alert Texts
          filter_AlertText.enabled = true;
          filter_AlertState.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));  // Alert State
          filter_AlertState.enabled = false;
          application_list.enabled = false;
          application_list.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
           
          unDistributedControl_getAllDeviceConfig(dsSystemNames, duSystemIds, dsHost, diPort);          // System names
          dynInsertAt(dsSystemNames, UN_ALERTPANEL_FILTER_ALL, 1);
          dynAppend(dsSystemNames, UN_ALERTPANEL_LIST_NAME);
          dynUnique(dsSystemNames);
          filter_SystemNames.items(dsSystemNames);
        
          filter_SystemNames.enabled = true;
          filter_Alias.enabled = false;
          filter_Description.enabled = true;
          break;
        }
        default:
        {
          filter_Object.enabled = false;
          filter_Object.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_Domain.enabled = false;
          filter_Domain.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_Nature.enabled = false;
          filter_Nature.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_Name.enabled = false;
          filter_Name.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_AlertText.enabled = false;
          filter_AlertText.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_AlertState.enabled = false;
          filter_AlertState.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_SystemNames.enabled = false;
          filter_SystemNames.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
          filter_Alias.enabled = false;
          filter_Description.enabled = false;
          application_list.enabled = false;
          application_list.items(makeDynString(UN_ALERTPANEL_FILTER_ALL));
        
          break;
        }
        }
      }
  //DebugTN("End INIT");
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_saveFilter
/**
Purpose: save current filter

Parameters: None
    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/
unAlertPanel_saveFilter()
{
  string sFilter;
  unAlertPanel_makeFilter(sFilter);
  
  switch (g_iUnAlertPanel)
  {
    case UN_ALERTPANEL_CONFIG:
    {
      unGraphicalFrame_ChildPanelOnCentralModal(
          "vision/unEventList/unEventList_SelectFilter.pnl", 
          "Select",
          makeDynString(
              "$sConfig:Save", 
              "$sPrefixFilter:unAlertPanel_Filter_Objects_",
              "$sFilterType:_UnAlertPanel_Filters", 
              "$sFilter:" + sFilter
              )
          );
        
      break;
    }
    case UN_ALERTPANEL_SYSTEM_INTEGRITY:
    {
      unGraphicalFrame_ChildPanelOnCentralModal(
          "vision/unEventList/unEventList_SelectFilter.pnl", 
          "Select",
          makeDynString(
              "$sConfig:Save", 
              "$sPrefixFilter:unAlertPanel_Filter_SystemAlarms_"
              "$sFilterType:_UnAlertPanel_Filters", 
              "$sFilter:" + sFilter
              )
          );                                                      
      
      break;
    }
    default:
    {
      break;
    }
  }
}
    
//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_applyFilter
/**
Purpose: apply current filter

Parameters: sFilter, string, output, filter
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/
int unAlertPanel_applyFilter(string sDpProp, string &sFilter)
{
  dyn_dyn_anytype ddDps;
  dyn_string dsFilterDps, dsFilter, dsRequestSystemNames;
  string sFilterDpComment;
  unsigned uFilterState;
  time tBegin, tEnd;
  bool bMultiDomain=false, bMultiNature=false, bStart;
  int iRes;

  // 1.   Get filter
  unAlertPanel_makeFilter(sFilter);
  
  dsFilter = strsplit(sFilter, UN_ALERTPANEL_FILTER_DELIMITER);

  bMultiDomain = (dsFilter[UN_ALERTPANEL_FILTER_FIELD_DOMAIN] == filter_Domain2.text);
  bMultiNature = (dsFilter[UN_ALERTPANEL_FILTER_FIELD_NATURE] == filter_Nature2.text);
  
  //DebugTN("bMultiDomain: "+bMultiDomain);
  //DebugTN("bMultiNature: "+bMultiNature);
  bStart = true;  
  
  // 2.   set Alarm filter to fit dpType _AESProperties
  // 2.1  Description filter
  sFilterDpComment = unAlert_descriptionFilter(dsFilter, bMultiDomain, bMultiNature);

  // 2.2  System filter    
  dsRequestSystemNames = strsplit(dsFilter[UN_ALERTPANEL_FILTER_FIELD_SYSTEMS], UN_ALERTPANEL_FILTER_COMBO_DELIMITER);    
  unAlert_GetSystemsFilter(dsRequestSystemNames, g_dsFilterSystemNames);

  // 2.3  dpList filter  
  dsFilterDps = unAlert_dpListFilter(dsFilter, bMultiDomain, bMultiNature);

  // 3.  Set _AESProperties associated datapoint
  if (dpExists(sDpProp))
  {
    uFilterState = (unsigned)dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALERTSTATE];
    uFilterState = (unsigned)(uFilterState - 1u);

    if(dsFilter[UN_ALERTPANEL_FILTER_FIELD_TIMERANGE] == 2) //only with History Mode
    {
      tEnd = makeTime(
          HistoryEndTimeYear.text, 
          HistoryEndTimeMonth.text, 
          HistoryEndTimeDay.text,
          HistoryEndTimeHour.text, 
          HistoryEndTimeMinute.text, 
          HistoryEndTimeSecond.text, 
          0
        );
      
      tBegin = makeTime(
          HistoryBeginTimeYear.text, 
          HistoryBeginTimeMonth.text, 
          HistoryBeginTimeDay.text,
          HistoryBeginTimeHour.text,
          HistoryBeginTimeMinute.text,
          HistoryBeginTimeSecond.text, 
          0
        );

      unLists_getTimeRequest(tBegin, tEnd, bStart);
    }
    
    if (bStart)
    {
      iRes=dpSetWait(  
          sDpProp + ".Alerts.FilterState.State",                      uFilterState,
          sDpProp + ".Alerts.Filter.DpComment",                       sFilterDpComment,
          sDpProp + ".Alerts.Filter.DpList",                          dsFilterDps,
          sDpProp + ".Alerts.Filter.Shortcut",                        dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALERTTEXT], //! we set Short Sign and not Alert Text
          sDpProp + ".Alerts.FilterTypes.Selections",                 makeDynInt(0,0,0,0,0),
          sDpProp + ".Alerts.FilterTypes.AlertSummary",               2,
          sDpProp + ".Both.Sorting.SortList",                         makeDynString("timeStr"),
          sDpProp + ".Both.Visible.VisibleColumns:_original.._value", g_dsViewColumns,
          sDpProp + ".Both.Timerange.Type",                           dsFilter[UN_ALERTPANEL_FILTER_FIELD_TIMERANGE],
          sDpProp + ".Both.Timerange.Begin",                          tBegin,
          sDpProp + ".Both.Timerange.End",                            tEnd,
          sDpProp + ".Both.Timerange.MaxLines",                       dsFilter[UN_ALERTPANEL_FILTER_FIELD_MAXLINES],
          sDpProp + ".Both.Timerange.Selection",                      6,
          sDpProp + ".Both.Timerange.Shift",                          1,
          sDpProp + ".Both.Systems.CheckAllSystems",                  FALSE,
          sDpProp + ".Both.Systems.Selections",                       g_dsFilterSystemNames);                    
      }
    }
  return iRes;
}

//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_displayTime
/**
Purpose: display Time column

Parameters: 
    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/

void unAlertPanel_displayTime()
{  
int iWidth;
  if (g_iUnAlertPanel == UN_ALERTPANEL_SYSTEM_INTEGRITY) iWidth=660;
  else iWidth=150;    //=default defined in AES settings panel

  setMultiValue(  g_sTable, "namedColumnVisibility", "UTCtimeStr", g_bUTC, 
                  g_sTable, "namedColumnVisibility", "timeStr", !g_bUTC,
                  g_sTable, "namedColumnWidth", "description", iWidth);        //Manage Time columns
}

//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_displayFilter
/**
Purpose: display current filter

Parameters: sFilter, string, input, filter
    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/
unAlertPanel_displayFilter(const string sFilter)
{
  dyn_string dsFilter, dsTemp, exceptionInfo, dsRequestSystem;
  string currentFilter, filterAll;
  bool bStart;
  int iTemp, i, len, iRes, iPos;
  time tBegin, tEnd;
  
  dsFilter = strsplit(sFilter, UN_ALERTPANEL_FILTER_DELIMITER);
  filterAll = UN_ALERTPANEL_FILTER_ALL;

  //DebugTN("unAlertPanel_displayFilter", UN_ALERTPANEL_FILTER_DELIMITER, sFilter, dsFilter);
  // if wrong filter format in memory -> default one...
  if(dynlen(dsFilter) == UN_ALERTPANEL_FILTER_FIELD_LENGTH)
  {
    dynAppend(dsFilter, "*");
  }
  if (dynlen(dsFilter) != (UN_ALERTPANEL_FILTER_FIELD_LENGTH+1))
  {
    dsFilter = strsplit(UN_ALERTPANEL_FILTER_DEFAULT, UN_ALERTPANEL_FILTER_DELIMITER);
    
    //get Systems set in unApplication
    iRes=dpGet(getSystemName() + UN_APPLICATION_DPNAME + ".unAlerts.filter.systems", dsRequestSystem);
    len=dynlen(dsRequestSystem);
    for (i = 1; i <= len ; i++)
    {
      iPos=strpos(dsRequestSystem[i], ":");
      if (iPos>=0)
      {
        dsRequestSystem[i]=substr(dsRequestSystem[i], 0, iPos);
      }
      
      if (strlen(currentFilter)==0)
      {
        currentFilter = dsRequestSystem[i];
      }
      else
      {
        currentFilter = currentFilter + UN_ALERTPANEL_FILTER_COMBO_DELIMITER + dsRequestSystem[i];
      }
    }
    
    dsFilter[UN_ALERTPANEL_FILTER_FIELD_SYSTEMS] = currentFilter;
    dsFilter[UN_ALERTPANEL_FILTER_FIELD_LENGTH+1] = "*";
  }    
  
  //DebugTN("unAlertPanel_displayFilter aft", sFilter, dsFilter);
  
  // 1. Object
  currentFilter = dsFilter[UN_ALERTPANEL_FILTER_FIELD_OBJECT];
  filter_Object2.text = "";
  if (strltrim(strrtrim(currentFilter)) == "")
  {
    filter_Object.text = filterAll;
  }
  else
  {
    dsTemp = strsplit(currentFilter, UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
    if (dynlen(dsTemp) > 1)
    {
      filter_Object.text = UN_ALERTPANEL_LIST_NAME;
      filter_Object2.text = currentFilter;
    }
    else
    {
      filter_Object.text = currentFilter;
    }
  }

  // 2. Alias
  if (strltrim(strrtrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALIAS])) == "")
  {
    filter_Alias.text = filterAll;
  }
  else
  {
    filter_Alias.text = dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALIAS];
  }
    
  // 3. Description
  if (strltrim(strrtrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_DESCRIPTION])) == "")
  {
    filter_Description.text = filterAll;
  }
  else
  {
    filter_Description.text = dsFilter[UN_ALERTPANEL_FILTER_FIELD_DESCRIPTION];
  }
    
  // 4. Domain
  currentFilter = dsFilter[UN_ALERTPANEL_FILTER_FIELD_DOMAIN];
  filter_Domain2.text = "";
  dsTemp = strsplit(currentFilter, UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
  
  if (dynlen(dsTemp) > 1)
  {
    filter_Domain.text = UN_ALERTPANEL_LIST_NAME;
    filter_Domain2.text = currentFilter;
  }
  else
  {
    strreplace(currentFilter, UN_ALERTPANEL_FILTER_COMBO_DELIMITER, "");
    filter_Domain.text = currentFilter;
    filter_Domain2.text = "-1";
  }
  
  // 5. Nature
  currentFilter = dsFilter[UN_ALERTPANEL_FILTER_FIELD_NATURE];
  filter_Nature2.text = "";
  dsTemp = strsplit(currentFilter, UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
  
  if (dynlen(dsTemp) > 1)
  {
    filter_Nature.text = UN_ALERTPANEL_LIST_NAME;
    filter_Nature2.text = currentFilter;
  }
  else
  {
    strreplace(currentFilter, UN_ALERTPANEL_FILTER_COMBO_DELIMITER, "");
    filter_Nature.text = currentFilter;
    filter_Nature2.text = "-1";
  }
  
  // 6. Name
  if (strltrim(strrtrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_NAME])) == "")
  {
    filter_Name.text = filterAll;
  }
  else
  {
    filter_Name.text = dsFilter[UN_ALERTPANEL_FILTER_FIELD_NAME];
  }
    
  // 7. Alert Text
  if (strltrim(strrtrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALERTTEXT])) == "")
  {
    filter_AlertText.text = filterAll;
  }
  else
  {
    filter_AlertText.text = dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALERTTEXT];
  }
    
  // 8. Alert State
  if (strltrim(strrtrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALERTSTATE])) == "")
  {
    filter_AlertState.selectedPos(1);
  }
  else
  {
    iTemp = (int)dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALERTSTATE];
    if ((iTemp > 0) && (iTemp <= filter_AlertState.itemCount()))
    {
      filter_AlertState.selectedPos(iTemp);
    }
    else
    {
      filter_AlertState.selectedPos(1);
    }
  }
    
  // 9. System Names
  currentFilter = dsFilter[UN_ALERTPANEL_FILTER_FIELD_SYSTEMS];
  filter_SystemNames2.text = "";
  if (strltrim(strrtrim(currentFilter)) == "")
  {
    filter_SystemNames.text = filterAll;
  }
  else
  {
    dsTemp = strsplit(currentFilter, UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
    if (dynlen(dsTemp) > 1)
    {
      filter_SystemNames.text = UN_ALERTPANEL_LIST_NAME;
      filter_SystemNames2.text = currentFilter;
    }
    else
    {
      filter_SystemNames.text = currentFilter;
    }
  }
    
  // 10. Application
  if(dynlen(dsFilter) == (UN_ALERTPANEL_FILTER_FIELD_LENGTH+1))
  {
    string sApplication = dsFilter[UN_ALERTPANEL_FILTER_FIELD_LENGTH+1];
    dyn_string dsApplications = strsplit(sApplication, UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
    
    if (dynContains(dsApplications, "*") > 0)
    {
      application_list.selectedPos(1);
    }
    else if (dynlen(dsApplications) > 1)
    {
      application_list.text(UN_ALERTPANEL_LIST_NAME);
      application_list2.text(sApplication);
    }
    else if ((dynlen(dsApplications) == 1) && (dynContains(application_list.items(), dsApplications[1]) > 0))
    {
      application_list.text() = dsApplications[1];
    }
    else
    {
      application_list.text() = "*";
    }
  }
  else
  {
    application_list.text() = "*";
  }
   
  // 11. Time Range
  if (strltrim(strrtrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_TIMERANGE])) == "")
  {
    TimeRange.number(0);
  }
  else
  {
    //iTemp = dsFilter[UN_ALERTPANEL_FILTER_FIELD_TIMERANGE];
    //TimeRange.number(iTemp);
    TimeRange.number(0);
  }
  
  unAlertPanel_changeMode(TimeRange.number());
  // 12. Max lines
  if (strltrim(strrtrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_MAXLINES])) == "")
  {
    AppendLines.text = "100";
  }
  else
  {
    AppendLines.text = dsFilter[UN_ALERTPANEL_FILTER_FIELD_MAXLINES];
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_loadFilter
/**
Purpose: load filter configuration

Parameters: None

Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/
unAlertPanel_loadFilter()
{
  dyn_float dfReturn;
  dyn_string dsReturn, dsFilter;
  int lengthMessages;
  string sFilter;
  
  switch (g_iUnAlertPanel)
  {
    case UN_ALERTPANEL_CONFIG:
    {
      unGraphicalFrame_ChildPanelOnCentralModalReturn(
          "vision/unEventList/unEventList_SelectFilter.pnl",
          "Select",
          makeDynString(
              "$sConfig:Load",
              "$sPrefixFilter:unAlertPanel_Filter_Objects_",
              "$sFilterType:_UnAlertPanel_Filters"
              ),
          dfReturn,
          dsReturn
      );
      
      break;
    }
    case UN_ALERTPANEL_SYSTEM_INTEGRITY:
    {
      unGraphicalFrame_ChildPanelOnCentralModalReturn(
          "vision/unEventList/unEventList_SelectFilter.pnl", 
          "Select",
          makeDynString(
              "$sConfig:Load", 
              "$sPrefixFilter:unAlertPanel_Filter_SystemAlarms_",
              "$sFilterType:_UnAlertPanel_Filters"
              ), 
          dfReturn, 
          dsReturn
        );    
      
      break;
    }
    default:
    {
      break;
    }
  }
  if (dynlen(dsReturn) >= 1)
  {
    sFilter = dsReturn[1];
    unAlertPanel_displayFilter(sFilter);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_makeFilter
/**
Purpose: create the filter string using the shape contents

Parameters: sFilter, string, output, filter
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/
unAlertPanel_makeFilter(string &sFilter)
{
  string sFilterTemp, sAlias, sDescription, sDomain, sNature, sName, sAlertText, sAlertState, sObject, sSystem;
  string sTimeRange, sMaxLines;
  string sRequestTime;
  string sApplication;

  // 1. Get filter
  sAlias = filter_Alias.text;
  sDescription = filter_Description.text;
  sAlertText = filter_AlertText.text;
  sAlertState = filter_AlertState.selectedPos();
  
  // Domain
  sDomain = filter_Domain.text;
  if (sDomain == UN_ALERTPANEL_LIST_NAME)
  {
    sDomain = filter_Domain2.text;
  }

  // Nature
  sNature = filter_Nature.text;
  if (sNature == UN_ALERTPANEL_LIST_NAME)
  {
    sNature = filter_Nature2.text;
  }

  // Name
  sName = filter_Name.text;
  
  // Object
  sObject = filter_Object.text;
  if (sObject == UN_ALERTPANEL_LIST_NAME)
  {
    sObject = filter_Object2.text;
  }

  // System
  sSystem = filter_SystemNames.text;
  if (sSystem == UN_ALERTPANEL_LIST_NAME)
  {
    sSystem = filter_SystemNames2.text;
  }
  
  // Application  
  sApplication = application_list.selectedText();
  if (UN_ALERTPANEL_LIST_NAME == sApplication)
  {
    sApplication = application_list2.text();
  }
  
  sFilterTemp = 
      sAlias + UN_ALERTPANEL_FILTER_DELIMITER + 
      sDescription + UN_ALERTPANEL_FILTER_DELIMITER +
      sDomain + UN_ALERTPANEL_FILTER_DELIMITER + 
      sNature + UN_ALERTPANEL_FILTER_DELIMITER +
      sName + UN_ALERTPANEL_FILTER_DELIMITER + 
      sAlertText + UN_ALERTPANEL_FILTER_DELIMITER + 
      sAlertState + UN_ALERTPANEL_FILTER_DELIMITER + 
      sObject + UN_ALERTPANEL_FILTER_DELIMITER + 
      sSystem;
                        
  // 2. Get time range & max lines
  sTimeRange = TimeRange.number();
  sMaxLines = AppendLines.text;
  
  // 3. Build filter
  sFilter = 
      sTimeRange + UN_ALERTPANEL_FILTER_DELIMITER + 
      sMaxLines + UN_ALERTPANEL_FILTER_DELIMITER + 
      sRequestTime +  UN_ALERTPANEL_FILTER_DELIMITER + 
      sFilterTemp;
  
  sFilter = 
      sFilter + UN_ALERTPANEL_FILTER_DELIMITER + 
      sApplication;
}

//------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_setEnableAppend
/**
Purpose: set enable status of append shapes

Parameters: enable, bool, input, enable status
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/

unAlertPanel_setEnableAppend(bool enable)
{
setMultiValue("AppendLines", "enabled", enable,
        "AppendText", "enabled", enable,
		"appendButton", "enabled", enable,
        "btTop", "enabled",  !enable,
        "btUpUp", "enabled",  !enable,
        "btUp", "enabled",  !enable,
        "btBot", "enabled",  !enable,
        "btDownDown", "enabled",  !enable,
        "btDown", "enabled",  !enable);
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_setEnableHistory
/**
Purpose: set enable status of history shapes

Parameters: enable, bool, input, enable status
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/

unAlertPanel_setEnableHistory(bool enable)
{
  setMultiValue("HistoryBeginTimeDay", "enabled", enable, "HistoryBeginTimeHour", "enabled", enable, 
                "HistoryBeginTimeMinute", "enabled", enable, "HistoryBeginTimeMonth", "enabled", enable, 
                "HistoryBeginTimeSecond", "enabled", enable, "HistoryBeginTimeYear", "enabled", enable, 
                "HistoryEndTimeDay", "enabled", enable, "HistoryEndTimeHour", "enabled", enable, 
                "HistoryEndTimeMinute", "enabled", enable, "HistoryEndTimeMonth", "enabled", enable, 
                "HistoryEndTimeSecond", "enabled", enable, "HistoryEndTimeYear", "enabled", enable, 
                "HistoryNow", "enabled", enable, "HistoryNow2", "enabled", enable, 
                "txtStart", "enabled", enable, "txtEnd", "enabled", enable, 
                "dayLabel1", "enabled", enable, "dayLabel2", "enabled", enable, 
                "monthLabel1", "enabled", enable, "monthLabel2", "enabled", enable,
                "yearLabel1", "enabled", enable, "yearLabel2", "enabled", enable, 
                "hourLabel1", "enabled", enable, "hourLabel2", "enabled", enable,
                "minuteLabel1", "enabled", enable, "minuteLabel2", "enabled", enable,
				"secondLabel1", "enabled", enable, "secondLabel2", "enabled", enable,
				"timeDateSeparator", "enabled", enable);
               
  if (g_iUnAlertPanel != UN_ALERTPANEL_SYSTEM_INTEGRITY) 
    setMultiValue("ackVisible", "enabled", !enable);
  else
    setMultiValue("ackVisible", "enabled", false);

}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_changeMode
/**
Purpose: set enable status of shapes depending on current mode

Parameters: button, int, input, current mode
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/

unAlertPanel_changeMode(int button)
{
  switch (button)
    {
    case 0:          // Current
      unAlertPanel_setEnableAppend(false);
      unAlertPanel_setEnableHistory(false);
      break;
    case 1:          // Append
      unAlertPanel_setEnableAppend(true);
      unAlertPanel_setEnableHistory(false);
      break;
    case 2:          // History
      unAlertPanel_setEnableAppend(false);
      unAlertPanel_setEnableHistory(true);
      break;
    }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_updateCounters
/**
Purpose: update info on unack. alarms

Parameters: none
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS version: 3.0.1
  . operating system: XP and W2000
  . distributed system: yes.
*/

void unAlertPanel_updateCounters(const string sTable)
{
  int i, length, iAckable = 0;
  string sAck;
  getValue(sTable, "lineCount", length);
  
  int iColumnIndex;
  getValue(sTable, "nameToColumn", "acknowledge", iColumnIndex);
  
  dyn_string dsAckColumn;
  getValue(sTable, "getColumnN", iColumnIndex, dsAckColumn);
  
  iAckable = dynCount(dsAckColumn, " !!! ");
  
  if(shapeExists("un_as_lineCount")) // Only in AES Screen
  {
    setValue("un_as_lineCount", "text", length);
  }
  
  setValue("as_needAck", "text", iAckable);    //for AES Row and Screen
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_DefaultPanel
/**
Purpose: table cell action for "S" column

Parameters: DataPoint name
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

unAlertPanel_DefaultPanel(string sDeviceName, string sDescription)
{
  string sPath, sPanelName, sDpPanel, panelDp, sDpType, panelFile, buttonPanel;
  dyn_string dsParameters, exceptionInfo, dsTemp1, dsTemp2, dsUserData;
  bool bBad;
  int len;
  
  unGenericDpFunctions_getParameters(sDeviceName, dsParameters);
  sPath = dsParameters[UN_PARAMETER_PANEL];
  if (getPath(PANELS_REL_PATH, sPath) == "" || sPath=="")
  {
    bBad = true;
    if(dpExists(sPath)) { //check if of type unTreeWidget_treeNodeDPT
      if(dpTypeName(sPath) == unTreeWidget_treeNodeDPT) {
        fwTree_getNodeUserData(_fwTree_getNodeName(sPath), dsUserData, exceptionInfo);
        fwTree_getNodeDevice(_fwTree_getNodeName(sPath), panelDp, sDpType, exceptionInfo);
        panelFile = unPanel_convertDpPanelNameToPanelFileName(panelDp);
        len = dynlen(dsUserData);
        if(len <= 0) { // case no userData
          dsUserData[1] = "";
          len = dynlen(dsUserData);
        }
        if((getPath(PANELS_REL_PATH, panelFile) != "") && (len > 0) 
                          && (sDpType == UN_PANEL_DPTYPE) && dpExists(panelDp)){
          unGenericDpFunctions_getContextPanel(panelDp, buttonPanel, exceptionInfo);
          bBad = false;
          if(len > c_unPanelConfiguration_maxPanelNavigation) {// 11, give the sDpNodeName
            unGraphicalFrame_showUserPanel(panelFile, dsUserData[1], sPath);
            unGraphicalFrame_showContextualPanel(buttonPanel, sPath);
          }
          else {
            unGraphicalFrame_showUserPanel(panelFile, dsUserData[1], panelDp);
            unGraphicalFrame_showContextualPanel(buttonPanel, panelDp);
          }
        }
      }
    }
    if(bBad)
      unGraphicalFrame_ChildPanelOnCentralModal("vision/MessageWarning","Warning",makeDynString("$1:" + unGenericDpFunctions_getAlias(sDeviceName) + getCatStr("unDB","EMPTYSYNOPTIC")));
  }
  else
  {
    sDpPanel=unPanel_convertPanelFileNameToDp(sPath);
    if(!dpExists(sDpPanel)) {
      unPanel_create(sPath, sDpPanel, exceptionInfo);
    }
    if (dynlen(exceptionInfo) > 0)
    {
      unGraphicalFrame_ChildPanelOnCentral(sPath, unGenericDpFunctions_getAlias(sDeviceName) + " " + sDescription + " Synoptic", makeDynString());
    }
    else
    {
      unGraphicalFrame_showUserPanel(sPath, "", sDpPanel);
      unGenericDpFunctions_getContextPanel(sDpPanel, sPanelName, exceptionInfo);
      if (dynlen(exceptionInfo) <= 0)
      {
        unGraphicalFrame_showContextualPanel(sPanelName, sDpPanel);
      }
    }
  }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_getInitDpProperties
/**
Purpose: load defined properties

Parameters: dsList, dyn_string, output, DP Pattern List
            sComment, string, output, Filter ...
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

unAlertPanel_getInitDpProperties(dyn_string &dsSystems, dyn_string &dsList, string &sComment)
{
int iRes;
string sSystem, sDp;

  sDp = sSystem + UN_APPLICATION_DPNAME;

  if (dpExists(sDp))
    {
    
    iRes=dpGet(sDp + ".unAlerts.filter.systems", dsSystems);
      
    switch(g_iUnAlertPanel)
      {
      case UN_ALERTPANEL_CONFIG:
        iRes=dpGet(  sDp + ".unAlerts.filter.dplist", dsList, 
                    sDp + ".unAlerts.filter.dpcomment", sComment);
        break;
      
      case UN_ALERTPANEL_SYSTEM_INTEGRITY:
        iRes=dpGet(  sDp + ".unSystemAlerts.filter.dplist", dsList); 
        sComment="";
        break;
    
      default:
        dsList=makeDynString(UN_ALERTPANEL_FILTER_ALL);
        sComment="";
      }
    }
  else
    {
    dsList=makeDynString(UN_ALERTPANEL_FILTER_ALL);
    sComment="";
    }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlertPanel_setInitDpProperties
/**
Purpose: load defined properties

Parameters: dsList, dyn_string, input, DP Pattern List
            sComment, string, input, Filter ...
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

unAlertPanel_setInitDpProperties(const string sDpProp, const dyn_string dsList, const string sComment)
{
int iRes;
  
  if (dpExists(sDpProp))
    {
    iRes=dpSetWait(  sDpProp + ".Alerts.Filter.DpList", dsList,
                    sDpProp + ".Alerts.Filter.DpComment", sComment);
    }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_refreshCounters
/**
Purpose: start unAlertPanel_updateCounters() every X secondes

Parameters: 
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

void unAlert_refreshCounters(const string sTable)
{  
  // If we get to this function too quick, the acknowledge column doesn't exist. We have to wait for the table to be ready.
  bool bAcknowledgeColumnFound = false;
  
  do
  {
    int iColumnCount;
    getValue(sTable, "columnCount", iColumnCount);
    
    for (int i = 0 ; i < iColumnCount ; i++)
    {
      string sColumnName;
      getValue(sTable, "columnName", i, sColumnName);
      
      if ("acknowledge" == sColumnName)
      {
        bAcknowledgeColumnFound = true;
        break;
      }
    }
  }
  while (!bAcknowledgeColumnFound);
  
  while (1)
  {
    delay(UN_DELAY_REFRESH_ALERT_TABLE);
    unAlertPanel_updateCounters(sTable);
  }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_onRightClick
/**
Purpose: This function builds the menustring and displaies the popup menu

Parameters: 
        
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

void unAlert_onRightClick(const string sDpProp, const string sTable, const bool bAESRow, const int iUnAlertPanel)
{
  int row, column, screenType, iTotRows;
  anytype val;
  string content;
  unsigned runMode;
  
  aes_getRunMode(sDpProp, runMode);

  if((runMode != AES_RUNMODE_RUNNING) && !AES_FORCE_CLICK)
    return;

  setInputFocus(myModuleName(), myPanelName(), sTable);
  getValue("", "currentCell", row, column );
  val=this.cellValue;
  iTotRows=this.lineCount;  

  aes_getScreenType(sDpProp, screenType );
  if(row<iTotRows && row>=0)
    content=this.cellValueRC(row, _DPID_);
  
  if(content!="" || g_alertRow)
    {
    bool emptyRow=false;

    // freeze table
    this.stop=true;

    if(g_alertRow && (content == "" ))
      emptyRow=true;
    
    unAlert_displayMenu(screenType, row, emptyRow, sDpProp, sTable, content, bAESRow, iUnAlertPanel);
    }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_displayMenu
/**
Purpose: This function builds the menustring and displaies the popup menu
      
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

void unAlert_displayMenu(const unsigned screenType, const int row, bool emptyRow, const string sDpProp, const string sTable, const string sDpe, const bool bAESRow, const int iUnAlertPanel)
{
string sAlias, sType;
unsigned mode, runMode;
dyn_string menuList, columnsMenuList, dsFunctions, dsAccessOk, exceptionInfo, exceptionInfoBis, parameters;
string sFunction, sDpType, sDpName;
int answer;

  aes_getPropMode(sDpProp, mode);   // (type) open, closed, current
  aes_getRunMode(sDpProp, runMode);

  sDpName=unGenericDpFunctions_getDpName(sDpe);    //get DpName
  sAlias=unGenericDpFunctions_getAlias(sDpName);  //get Alias
  sDpType=dpTypeName(sDpName);
  
  if (iUnAlertPanel==UN_ALERTPANEL_CONFIG)
    {
    if (sAlias!="" && sDpType!="")
      {    
      unGenericButtonFunctionsHMI_isAccessAllowedMultiple(sDpName, sDpType, dsAccessOk, exceptionInfo);
  
      // 1. Build menu
      unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
      if (dynlen(dsFunctions)>0)
        sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_MENUCONFIG];
      }
    
    if (sFunction != "" && sDpName !="")
      {
      if (substr(sDpName, strlen(sDpName)-1, strlen(sDpName))==".")
        sDpName=substr(sDpName, 0, strlen(sDpName)-1);
        
      evalScript(menuList, "dyn_string main(string device, string sType, dyn_string dsAccessOk) {" + 
                    "dyn_string dsMenuTemp;" +
                   "if (isFunctionDefined(\"" + sFunction + "\"))" +
                   "{" +
                   "    " + sFunction + "(device, sType, dsAccessOk, dsMenuTemp);" +
                   "}" +
                   "else " +
                   "{" +
                   "    dsMenuTemp = makeDynString();" +
                   "}" +
                   "return dsMenuTemp; }", makeDynString(), sDpName, sDpType, dsAccessOk);
      }
    }
  
  if (sAlias!="")  
    dynInsertAt(menuList, "PUSH_BUTTON, " + sAlias + ", 1000, 1", 1);

  if (iUnAlertPanel==UN_ALERTPANEL_CONFIG)
    dynInsertAt(menuList, "SEPARATOR", 2);

  unAlert_viewRightClick(menuList, mode, runMode, screenType, row, sTable, bAESRow);

  // 2. Pop up menu
  this.stop=false;
  answer=0;
  if (dynlen(menuList) > 0)
    popupMenu(menuList, answer);

  // 3. Action
  if (dynlen(dsFunctions)>0)    
    sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_HANDLEMENUANSWER];
    
  if (sFunction != "" && sDpName !="")
    {
    //Common part
    evalScript(exceptionInfoBis, "dyn_string main(string deviceName, string sDpType, dyn_string menuList, int menuAnswer) {" + 
                  "dyn_string exceptionInfo;" +
                 "if (isFunctionDefined(\"" + sFunction + "\"))" +
                 "{" +
                 "    " + sFunction + "(deviceName, sDpType, menuList, menuAnswer);" +
                 "}" +
                 "else " +
                 "{" +
                 "    exceptionInfo = makeDynString();" +
                 "}" +
                 "return makeDynString(); }", makeDynString(), sDpName, sDpType, menuList, answer);
    dynAppend(exceptionInfo, exceptionInfoBis);
    }

  //Columns part
  unAlert_actionRightClick(answer, bAESRow, row, sDpProp ,sTable);  

  if (dynlen(exceptionInfo) > 0)
    unSendMessage_toExpertException("Right Click", exceptionInfo);
}

//-----------------------------------------------------------------------------------------------------------------------------

void unAlert_viewRightClick(dyn_string &menuList, string sMode, string sRunMode, unsigned sScreenType, int iRow, string sTable, bool bRow)
{
dyn_string dsColumnsList;
dyn_string dsMenu_text;
string sMessage, sColumn, sAck, sAdd;
bool bAck, bInsertComment, bVisible;
int iLen, i, iPos, n;

  bAck=TRUE;
  bInsertComment=TRUE;
  
  //insert ack
  if(sMode == AES_MODE_CLOSED || sRunMode != AES_RUNMODE_RUNNING)
      bAck=false;
 
  //insert comment - not possible at sum alert types
  if(sScreenType == AESTYPE_ALERTS )
    {
    if(sRunMode != AES_RUNMODE_RUNNING )
      bInsertComment=false;
    }

  dsMenu_text[MENU_PB]="PUSH_BUTTON";
  dsMenu_text[MENU_CB]="CASCADE_BUTTON";
  dsMenu_text[MENU_SP]="SEPARATOR";

  // 1. Build columns view
  getValue(sTable, "columnCount", iLen);
  
  for(i=1;i<=iLen;i++)
    {    
    getMultiValue(  sTable, "columnVisibility", i - 1, bVisible, 
                    sTable, "columnHeader", i - 1, sMessage, 
                    sTable, "columnName", i - 1, sColumn);  
        
    //set colors to see selected line
    if (sColumn!="abbreviation" && sColumn!="acknowledge" && !bRow && sColumn!="s")
      {    
      setMultiValue(  sTable, "cellBackColRC", iRow, sColumn, "_WindowText",
                      sTable, "cellForeColRC", iRow, sColumn, "_Window");
      }
    
    iPos=dynContains(g_dsViewColumns, sColumn);

    if(iPos>0 && sMessage!="")
      {
      if (bVisible)
        sMessage = dsMenu_text[MENU_PB] + ",Hide " + sMessage;
      else
        sMessage = dsMenu_text[MENU_PB] + ",View " + sMessage;
      
      n=iPos+UN_NUM_AESVIEW;  
      sMessage = sMessage + "," + n + ",1";
      
      dynAppend(dsColumnsList, sMessage);    //Column Text
      }
    }

  dynAppend(dsColumnsList, dsMenu_text[MENU_SP]);
  dynAppend(dsColumnsList, dsMenu_text[MENU_PB] + ",View All, " + (UN_NUM_AESVIEW+100) + ", 1");

  //manage autorizations
  if (bAck)
    sAck="1";
  else
    sAck="0";
  
  if (bInsertComment)
    sAdd="1";
  else
    sAdd="0";  
  
  unAlert_manageItem(menuList, "PUSH_BUTTON, Ack. Alarm*", sAck);
  unAlert_manageItem(menuList, "PUSH_BUTTON, Mask PVSS Alarm*", sAck);
    
  //insert AES items
  iLen=dynlen(menuList);
  iPos=iLen;
  for (i=1; i<=iLen && iPos==iLen; i++)
    {
    if (patternMatch("CASCADE_BUTTON*", menuList[i]))
      iPos=i;
    }
  
  dynInsertAt(menuList, "CASCADE_BUTTON, View, 1", iPos+1);
  dynInsertAt(menuList, dsMenu_text[MENU_SP], iPos+1);

// IS-669: problem in case of distributed
  dynInsertAt(menuList, dsMenu_text[MENU_PB] + ", Add Comment, " + (UN_NUM_AESVIEW+300) + ", 0" /*+ sAdd*/, iPos+1);
  dynInsertAt(menuList, dsMenu_text[MENU_PB] + ", Details, " + (UN_NUM_AESVIEW+200) + ", 0", iPos+1);
  dynInsertAt(menuList, dsMenu_text[MENU_SP], iPos+1);  

  dynAppend(menuList, "View");    
                              
  iLen=dynlen(dsColumnsList);
  for (i=1;i<=iLen;i++)
    {
    dynAppend(menuList, dsColumnsList[i]);
    }  

}

//-----------------------------------------------------------------------------------------------------------------------------

void unAlert_manageItem(dyn_string &menuList, const string sPattern, const string sNew)
{
int iLen, iLine, i, iNew;
dyn_string dsSplit;

  sscanf(sNew, "%d", iNew);

  if (iNew==0)
    {
    iLen=dynlen(menuList);
    for (i=1; i<=iLen; i++)
      {
      if (patternMatch(sPattern, menuList[i]))
        {
        dsSplit=strsplit(menuList[i], ",");
        iLine=dynlen(dsSplit);
        if (iLine>=4)
          {
          menuList[i]=dsSplit[1] + "," + dsSplit[2] + "," + dsSplit[3] + "," + sNew;
          i=iLen;      //to exit
          }
        }
      }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------

void unAlert_actionRightClick(int iAnswer, bool bAESRow, int iRow, string sDpProp, string sTable)
{
int i, iLen, iPos, k;
string sColumn, sAction;
bool bVisible;

  getValue(sTable, "columnCount", iLen); 

  if (iAnswer == (UN_NUM_AESVIEW+100))                          //view all
    {
    for (i=0; i<iLen; i++)
      {
      getValue(sTable, "columnName", i, sColumn);
      if (dynContains(g_dsViewColumns, sColumn))
        setValue(sTable, "columnVisibility", i, TRUE);
      }
    
    if (bAESRow)
      unLists_initTimeInfo(-1);
    }
  else if (iAnswer > UN_NUM_AESVIEW && iAnswer < (UN_NUM_AESVIEW+100))    //one by one
    {
    iPos=iAnswer-UN_NUM_AESVIEW;
    sAction=g_dsViewColumns[iPos];
    k=0;
    for (i=0; i<iLen && k==0; i++)
      {
      getValue(sTable, "columnName", i, sColumn);
      if (sAction==sColumn)
        {
        getValue(sTable, "columnVisibility", i, bVisible);        
        setValue(sTable, "columnVisibility", i, !bVisible);                      
        k=iLen;
        }
      }
    
    i--;
    
    if (bAESRow)
      {      
      if (sColumn=="timeStr")                //local time
        {
        setValue(sTable, "columnVisibility", i-1, bVisible);
        unLists_initTimeInfo(bVisible);
        }            
      else if (sColumn=="UTCtimeStr")        //UTC time
        {
        setValue(sTable, "columnVisibility", i+1, bVisible);
        unLists_initTimeInfo(!bVisible);        
        }        
      }
    }
  else if (iAnswer==(UN_NUM_AESVIEW+200))
    unAlert_displayDetails(iRow, sDpProp);  
  else if (iAnswer==(UN_NUM_AESVIEW+300))
  {
    mapping mTableRow;
  
    synchronized(g_bTableLineSynchronisation)
    { 
      int length;
      dyn_anytype alertRow;  
    
      alertRow = this.getLineN(iRow);
    
      length = dynlen(alertRow);
      for(i=1; i<=length; i++)
        mTableRow[this.columnName(i-1)] = alertRow[i];
    }
    
      aes_insertComment(iRow, mTableRow);  
   }
  
  //reset colors for selected line
  if(!bAESRow)
    {
    for (i=1; i<=iLen; i++)
      {
      getValue(sTable, "columnName", i-1, sColumn);
      if (sColumn!="abbreviation" && sColumn!="acknowledge" && sColumn!="s" &&
          sColumn!="" && sColumn!="-1")
        {
        setMultiValue(  sTable, "cellBackColRC", iRow, sColumn, "_3DFace",
                        sTable, "cellForeColRC", iRow, sColumn, "_WindowText");
        }
      }
    }
      
}  

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_displayDetails
/**    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

void unAlert_displayDetails(int row, string propDpName)
{
string panel, dpid, tim, count, val;
anytype value;
int dpeType;

  getValue("", "cellValueRC", row, _DPID_, dpid,
               "cellValueRC", row, _TIME_, tim );

  dpeType=dpElementType( dpid );

  // check alarmtype - if sumalert / display sumalertdetails instead of details !
  if( aes_checkSumAlert( dpeType ) )
    aes_displaySumalertDetails( dpid, row, propDpName );
  else
    {
      panel="vision/unAlertPanel/unAlertPanel_Details.pnl";
      getValue( "", "cellValueRC", row, _COUNT_, count);

      // start child panel for detail information
      unGraphicalFrame_ChildPanelOnCentralModal(  panel, "",
                                makeDynString("$dpid:" + dpSubStr(dpid, DPSUB_SYS_DP_EL),
                                              "$time:" + tim,
                                              "$count:" + count));
    }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_SetSystems
/**    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

unAlert_SetSystemsFilter(const string sDpProp, const dyn_string dsRequest, dyn_string &dsMySystemNames)
{
int iRes;
  
  unAlert_GetSystemsFilter(dsRequest, dsMySystemNames);
  iRes=dpSetWait(sDpProp + ".Both.Systems.Selections", dsMySystemNames);
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_GetSystemsFilter
/**    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

unAlert_GetSystemsFilter(dyn_string dsRequest, dyn_string &dsSystem)
{
int i, len, iPos;
dyn_string dsAllSystems;
  
  dynClear(dsSystem);
  
  len=dynlen(dsRequest);
  for (i=1; i<=len; i++)
    {
    if (dsRequest[i]!=UN_ALERTPANEL_FILTER_ALL && strltrim(dsRequest[i])!="")
      {
      iPos=strpos(dsRequest[i], ":");
      if (iPos>=0)
        dsRequest[i] = substr(dsRequest[i], 0, iPos);
        
      dynAppend(dsSystem, dsRequest[i]);
      }
    else if (dsRequest[i]==UN_ALERTPANEL_FILTER_ALL)
      {
      dsAllSystems=mappingKeys(g_bSystemState);
      dynAppend(dsSystem, dsAllSystems);
      i=len;
      }      
    }
  dynUnique(dsSystem);

  dsAllSystems=mappingKeys(g_bSystemState);

  len=dynlen(dsSystem);  
  
  for (i=len; i>=1; i--)
    {
    iPos=dynContains(dsAllSystems, dsSystem[i]);
    if (iPos>0)
      {
      if (!mappingGetValue(g_bSystemState, iPos))  //Not connected
        {
        /*if (!g_bInit || mappingGetValue(g_bSystemState, iPos)!=mappingGetValue(g_bOldSystemState, iPos))
          {
          unGraphicalFrame_ChildPanelOnCentral("vision/MessageWarning", "Information System " + dsSystem[i], makeDynString("$1:" + dsSystem[i] + " is not reachable!"));
          }*/
        dynRemove(dsSystem, i);      
        }
      }
    else  //Not defined
      {
      unGraphicalFrame_ChildPanelOnCentral("vision/MessageWarning", "Warning System " + dsSystem[i], makeDynString("$1:" + dsSystem[i] + " is not defined!"));
      dynRemove(dsSystem, i);      
      }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_QueryGroup
/**    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

string unAlert_QueryGroup(const dyn_string dsFilter, const string sPrefix, const string sSystem)
{
bool bDpExists;
int len, len_2, i, j, iRes;
string sFROM;
dyn_string dsCurrentDpGroupDpList, dsDpGroup;

  len = dynlen(dsFilter);  
  // Get Dp group
  for(i=1;i<=len;i++)                  
    {
    dsDpGroup[i] = unGenericDpFunctions_groupNameToDpName(sPrefix + dsFilter[i], sSystem);
    }

  // Delete groups that doesn't have valid DPE -> they make the dpQuery crash
  i = 1;
  while (i<=len)
    {
    if (dsDpGroup[i]!="")
      {
      if (dpExists(dsDpGroup[i]))
        {
        iRes = dpGet(dsDpGroup[i] + ".Dps", dsCurrentDpGroupDpList); 
        len_2=dynlen(dsCurrentDpGroupDpList);
        if (iRes<0 || len_2==0)
          {
          dynRemove(dsDpGroup, i);
          }
        else
          {
          j=1;
          bDpExists = false;
          while ((j<=len_2) && (!bDpExists))
            {
            bDpExists = dpExists(dsCurrentDpGroupDpList[j]);
            j++;
            }
          if (!bDpExists)
            {
            dynRemove(dsDpGroup, i);
            }
          else
            {
            i++;
            }  
          }
        }  
      else
        {
        dynClear(dsDpGroup);
        i=len+1;  //exit
        }
      }
    else
      {
      dynRemove(dsDpGroup, i);
      }
      
    len = dynlen(dsDpGroup);
    }

  // Now Dps from Groups can be use for FROM in query
  len = dynlen(dsDpGroup);              
  if (len>0)                    
    {
    sFROM = " FROM \'{";
    for(i=1;i<=len;i++)
      {
      sFROM = sFROM + "DPGROUP(" + substr(dsDpGroup[i],strpos(dsDpGroup[i],":")+1) + "),";
      }
    sFROM = substr(sFROM, 0, strlen(sFROM) - 1) + "}\' ";  // Delete last ","
    if (len == 1)
      {
      strreplace(sFROM, "{", "");
      strreplace(sFROM, "}", "");
      }
    }
  else
    {
    sFROM = "";
    }

return sFROM;
}  

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_getList
/**    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

dyn_string unAlert_getList(const string sField, const bool bMulti, const string sQueryWHERE, const string sPrefix, const string sSystem)
{  
string sQueryFROM, sQueryGroup;
dyn_dyn_anytype ddDps;
dyn_string dsFilterDps, dsGroup;
int i, iLen;
    
  if ((bMulti) && 
     (sField != UN_ALERTPANEL_FILTER_ALL) && 
     (strltrim(sField) != ""))
    {
    dsGroup = strsplit(sField , UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
    }
  else
    {
    dsGroup = makeDynString();
    }
  
  sQueryFROM = unAlert_QueryGroup(dsGroup, sPrefix, sSystem);
  if (sQueryFROM != "")      // Full query
    {
    sQueryGroup = "SELECT \'_alert_hdl.._type\' ";
    
    sQueryGroup = sQueryGroup + sQueryFROM;
    
    sQueryGroup = sQueryGroup + "REMOTE \'" + sSystem + "\' ";
          
    sQueryGroup = sQueryGroup + " WHERE \'_alert_hdl.._type\' != 0 ";
    
    if (sQueryWHERE != "")
      {
      sQueryGroup = sQueryGroup + sQueryWHERE;
      }   
    dpQuery(sQueryGroup, ddDps);

    iLen = dynlen(ddDps);
    for(i=2;i<=iLen;i++)
      {
      dynAppend(dsFilterDps, dpSubStr(ddDps[i][1],DPSUB_SYS_DP_EL));
      }
    }
    
  return dsFilterDps;  
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_descriptionFilter
/**    
Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/

string unAlert_descriptionFilter(const dyn_string dsFilter, bool &bMultiDomain, bool &bMultiNature)
{
string sFilterDpComment;

  //Pattern for DOMAIN
  if (bMultiDomain)                                                                    // List
    sFilterDpComment = UN_ALERTPANEL_FILTER_ALL + UN_ALERTPANEL_FILTER_DELIMITER;
  else
    {
    if (dsFilter[UN_ALERTPANEL_FILTER_FIELD_DOMAIN]=="")
      sFilterDpComment = UN_ALERTPANEL_FILTER_DELIMITER;
    else if (dsFilter[UN_ALERTPANEL_FILTER_FIELD_DOMAIN]==UN_ALERTPANEL_FILTER_ALL)    // *
      sFilterDpComment = dsFilter[UN_ALERTPANEL_FILTER_FIELD_DOMAIN] + UN_ALERTPANEL_FILTER_DELIMITER;    
    else                                                                              // At leat one domain => filter with dpList
      {
      bMultiDomain=true;
      sFilterDpComment = UN_ALERTPANEL_FILTER_ALL + UN_ALERTPANEL_FILTER_DELIMITER;
      }
    }

  //Complete with Pattern for NATURE      
  if (bMultiNature)
    sFilterDpComment = sFilterDpComment + UN_ALERTPANEL_FILTER_ALL + UN_ALERTPANEL_FILTER_DELIMITER;
  else
    {
    if (dsFilter[UN_ALERTPANEL_FILTER_FIELD_NATURE]=="")
      sFilterDpComment = sFilterDpComment + UN_ALERTPANEL_FILTER_DELIMITER;
    else if (dsFilter[UN_ALERTPANEL_FILTER_FIELD_NATURE]==UN_ALERTPANEL_FILTER_ALL)
      sFilterDpComment = sFilterDpComment + dsFilter[UN_ALERTPANEL_FILTER_FIELD_NATURE] + UN_ALERTPANEL_FILTER_DELIMITER;        
    else    // At leat one nature => filter with dpList
      {
      bMultiNature=true;
      sFilterDpComment = sFilterDpComment + UN_ALERTPANEL_FILTER_ALL + UN_ALERTPANEL_FILTER_DELIMITER;
      }
    }
  
  //Complete with others fields
  switch (g_iUnAlertPanel)
    {
    case UN_ALERTPANEL_CONFIG:
      sFilterDpComment =   sFilterDpComment + 
                          dsFilter[UN_ALERTPANEL_FILTER_FIELD_ALIAS] + 
                          UN_ALERTPANEL_FILTER_DELIMITER +
                          dsFilter[UN_ALERTPANEL_FILTER_FIELD_DESCRIPTION] + 
                          UN_ALERTPANEL_FILTER_DELIMITER + 
                          dsFilter[UN_ALERTPANEL_FILTER_FIELD_NAME] + 
                          UN_ALERTPANEL_FILTER_DELIMITER + 
                          UN_ALERTPANEL_FILTER_ALL;
      break;
    case UN_ALERTPANEL_SYSTEM_INTEGRITY:
      //sFilterDpComment =   UN_ALERTPANEL_FILTER_ALL + dsFilter[UN_ALERTPANEL_FILTER_FIELD_DESCRIPTION] + UN_ALERTPANEL_FILTER_ALL;
      sFilterDpComment =   dsFilter[UN_ALERTPANEL_FILTER_FIELD_DESCRIPTION];
      break;
    default:
      sFilterDpComment =   UN_ALERTPANEL_FILTER_ALL;
      break;
    }
    
  return sFilterDpComment;
}

//-----------------------------------------------------------------------------------------------------------------------------

// unAlert_dpListFilter
/**    
Usage: Internal function
  New in 5.3.1 : Parameter dsApplicationsToFilter, dyn_string input, when selecting "List..." in application combobox in alarm panel 

PVSS manager usage: Ctrl, NG, NV

Constraints:
  . PVSS II version: 3.0
  . operating system: Wds 2000 and XP
  . distributed system: yes.
*/
dyn_string unAlert_dpListFilter(const dyn_string dsFilter, const bool bMultiDomain, const bool bMultiNature)
{
  int iLendp, iLenobject, iLensystem, iLenDomain, iLenNature, iLensplit;
  int i, j, iAlertType, iRes;
  string sObjectPattern, sQueryWHERE, sSystem;
  dyn_string dsObjects, dsPattern, dsFilterDps;
  dyn_string dsFilterDpsDomain, dsFilterDpsNature, dsTemp, dsSplit;  
    
  if (g_iUnAlertPanel != UN_ALERTPANEL_SYSTEM_INTEGRITY)
  {
    //look for Object filter
    dsObjects = strsplit(dsFilter[UN_ALERTPANEL_FILTER_FIELD_OBJECT] , UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
    iLenobject = dynlen(dsObjects);            
    if (iLenobject > 0 && dynContains(dsObjects, UN_ALERTPANEL_FILTER_ALL) == 0)
    {
      sQueryWHERE = " AND (";
      for(i=1;i<=iLenobject;i++)
      {
        sQueryWHERE = sQueryWHERE + "_DPT = \"" + dsObjects[i] + "\" OR ";
      }
      sQueryWHERE = substr(sQueryWHERE, 0, strlen(sQueryWHERE) - 3) + ") ";  // Delete "OR "
    }
    else
    {
      sQueryWHERE = "";
    }
  
    //for each system, get dpName according to Domain and Nature filters
    dynClear(dsFilterDpsDomain);
    dynClear(dsFilterDpsNature);    
    
    iLensystem = dynlen(g_dsFilterSystemNames);    
    for (i = 1 ; i <= iLensystem ; i++)
    {
      if (strpos(g_dsFilterSystemNames[i], ":")>=0)
      {
        sSystem = g_dsFilterSystemNames[i];
      }
      else
      {
        sSystem = g_dsFilterSystemNames[i] + ":";
      }
      
      // "group:::..." DOESN'T WORK IN DISTRIBUTED MODE!!!
      if (iLensystem==1 && bMultiDomain && !bMultiNature && sQueryWHERE=="")    //used directly DpGroup in Alerts Query - Normally faster
      {
        dynClear(dsTemp);
        dsSplit = strsplit(dsFilter[UN_ALERTPANEL_FILTER_FIELD_DOMAIN] , UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
        iLensplit=dynlen(dsSplit);
        for (j=1; j<=iLensplit; j++)
        {
          dsTemp[j]="group:::Domain:" + dsSplit[j];
        }        
        dynAppend(dsFilterDpsDomain, dsTemp);        
      }
      else if (iLensystem==1 && !bMultiDomain && bMultiNature && sQueryWHERE=="")  //used directly DpGroup in Alerts Query
      {
        dynClear(dsTemp);
        dsSplit = strsplit(dsFilter[UN_ALERTPANEL_FILTER_FIELD_NATURE] , UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
        iLensplit=dynlen(dsSplit);
        for (j=1; j<=iLensplit; j++)
        {
          dsTemp[j]="group:::Nature:" + dsSplit[j];
        }        
        dynAppend(dsFilterDpsNature, dsTemp);        
      }        
      else if (bMultiDomain || bMultiNature)  //used dpN list
      {
        dsTemp=unAlert_getList(dsFilter[UN_ALERTPANEL_FILTER_FIELD_DOMAIN], bMultiDomain, sQueryWHERE, UN_DOMAIN_PREFIX, sSystem);
        dynAppend(dsFilterDpsDomain, dsTemp);
        dsTemp=unAlert_getList(dsFilter[UN_ALERTPANEL_FILTER_FIELD_NATURE], bMultiNature, sQueryWHERE, UN_NATURE_PREFIX, sSystem);
        dynAppend(dsFilterDpsNature, dsTemp);              
      }
    }
      
    //merge the results  
    dynUnique(dsFilterDpsDomain);
    dynUnique(dsFilterDpsNature);
    
    iLenDomain=dynlen(dsFilterDpsDomain);
    iLenNature=dynlen(dsFilterDpsNature);
          
    if (iLenDomain>0 && iLenNature>0)                          // If a query was done on Domains and Natures
    {
      dsFilterDps = dynIntersect(dsFilterDpsDomain, dsFilterDpsNature);
    }
    else if (iLenDomain>0 && iLenNature==0 && !bMultiNature)  // If a query was done for Domains and Natures
    {
      dsFilterDps = dsFilterDpsDomain;
    }
    else if (iLenDomain>0 && iLenNature==0 && bMultiNature)    // If a query was done only for Domains
    {
      dynClear(dsFilterDps);
    }
    else if (iLenDomain==0 && iLenNature>0 && !bMultiDomain)  // If a query was done only for Natures
    {
      dsFilterDps = dsFilterDpsNature;
    }
    else if (iLenDomain==0 && iLenNature>0 && bMultiDomain)    // If a query was done for Domains and Natures
    {
      dynClear(dsFilterDps);
    }
    else if ((iLenDomain==0) && (iLenNature==0) && (bMultiDomain || bMultiNature))
    {
      dynClear(dsFilterDps);
    }    
    
    
    iLendp=dynlen(dsFilterDps);
        
    if (iLendp==0 && !bMultiDomain && !bMultiNature) //no filter for the moment  
    {
      if ((dsFilter[UN_ALERTPANEL_FILTER_FIELD_OBJECT] != UN_ALERTPANEL_FILTER_ALL) &&     //if filter on Object
          (strltrim(dsFilter[UN_ALERTPANEL_FILTER_FIELD_OBJECT]) != ""))
      {
        //pattern on DpName (5 fields)
        for(i = 1 ; i <= UN_DPNAME_FIELD_LENGTH ; i++)
        {
          dsPattern[i] = UN_ALERTPANEL_FILTER_ALL;
        }
    
        //built object pattern
        dsObjects = strsplit(dsFilter[UN_ALERTPANEL_FILTER_FIELD_OBJECT] , UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
        iLenobject = dynlen(dsObjects);   
        for(i = 1 ; i<= iLenobject ; i++)
        {
          dsPattern[UN_DPNAME_FIELD_OBJECT] = dsObjects[i];
          sObjectPattern = "";
          for(j = 1 ; j <= UN_DPNAME_FIELD_LENGTH ; j++)
          {
            sObjectPattern = sObjectPattern + dsPattern[j] + UN_DPNAME_SEPARATOR;
          }
          
          sObjectPattern = substr(sObjectPattern, 0, strlen(sObjectPattern) - strlen(UN_DPNAME_SEPARATOR));
          
          //update existing list
          iLendp=dynlen(g_dsDpList);
          for(j = 1; j <= iLendp ; j++)
          {
            dynAppend(dsFilterDps, sObjectPattern + substr(g_dsDpList[j], strpos(g_dsDpList[j], ".")));
          }
        }
      }
      else  // no new filter => set filter from memory
      {
        dsFilterDps=g_dsDpList;
      }
    }
    else if (iLendp==0)
    {
      dsFilterDps = makeDynString("NOT-VALID-DP");
    }
    else 
    {
      dsFilterDps = unAlertPanel_checkDeviceType(dsFilterDps, dsObjects);
    }
    
    string sApplication = dsFilter[UN_ALERTPANEL_FILTER_FIELD_LENGTH+1];
    dyn_string dsApplications = strsplit(sApplication, UN_ALERTPANEL_FILTER_COMBO_DELIMITER);
    
    if ((dynContains(dsApplications, "*") < 1) || g_unicosHMI_bApplicationFilterInUse)
    {
      dsFilterDps = unAlertPanel_checkApplication(dsFilterDps, dsApplications);
    }
    
    if (dynlen(dsFilterDps) == 0)
    {
      dsFilterDps = makeDynString("NOT-VALID-DP");
    }
  }
  else //UN_ALERTPANEL_SYSTEM_INTEGRITY  
  {
    dynClear(dsFilterDps);
    
    //update existing list
    iLendp=dynlen(g_dsDpList);
    iLensystem = dynlen(g_dsFilterSystemNames);
    for (i=1;i<=iLensystem;i++)
    {
      if (strpos(g_dsFilterSystemNames[i], ":")>=0)
        sSystem=g_dsFilterSystemNames[i];
      else
        sSystem=g_dsFilterSystemNames[i] + ":";
              
      for(j=1; j<=iLendp ; j++)
      {
        //dynAppend(dsFilterDps, sSystem + g_dsDpList[j]);
        dynAppend(dsFilterDps, g_dsDpList[j]);
      }
    }
  }
  
  dynUnique(dsFilterDps);

  return dsFilterDps;
}

dyn_string unAlertPanel_checkDeviceType(dyn_string dsDp, dyn_string dsDevType)
{
  dyn_string dsRes;
  int i, len=dynlen(dsDp);
  
  if(dynlen(dsDevType) == 1) {
    if(dsDevType[1] == "*")
      return dsDp;
  }
  for(i=1;i<=len;i++) {
    if(dynContains(dsDevType, dpTypeName(dpSubStr(dsDp[i], DPSUB_SYS_DP))) > 0)
      dynAppend(dsRes, dsDp[i]);
  }
  return dsRes;
}

// Build filter on applications
// New in 5.3.1 : there can be several applications
dyn_string unAlertPanel_checkApplication(dyn_string dsDp, dyn_string dsApplParam)
{
  dyn_string dsResult;  
  
  dyn_string dsAppl = dsApplParam;
  if (g_unicosHMI_bApplicationFilterInUse)
  {
    synchronized(g_unicosHMI_dsApplicationList)
    {
      if (dynContains(dsApplParam, "*") > 0)
      {
        dsAppl = g_unicosHMI_dsApplicationList;
      }
      else
      {
        dsAppl = dynIntersect(dsAppl, g_unicosHMI_dsApplicationList);
      }
    }
  }
  
  // For each DP in the filter
  for(int i = 1 ; i <= dynlen(dsDp) ; i++) 
  {
    // Current dp
    dyn_string dsTemp = strsplit(dsDp[i], UN_DPNAME_SEPARATOR);
      
      
    // For each application in the list
    for (int j = 1 ; j <= dynlen(dsAppl) ; j++)
    {
      // Current application      
      string sAppl = dsAppl[j];
    
      if(dynlen(dsTemp) == UN_DPNAME_FIELD_LENGTH) 
      {
        if(dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION] == sAppl)
        {
          dynAppend(dsResult, dsDp[i]);
        }
        else if(dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION] == "*")
        {
          dynAppend(
              dsResult, 
              dsTemp[UN_DPNAME_FIELD_PREFIX]  + UN_DPNAME_SEPARATOR + 
              dsTemp[UN_DPNAME_FIELD_PLCNAME] + UN_DPNAME_SEPARATOR+
              sAppl + UN_DPNAME_SEPARATOR +
              dsTemp[UN_DPNAME_FIELD_OBJECT] + UN_DPNAME_SEPARATOR +
              dsTemp[UN_DPNAME_FIELD_NUMBER]);
        }
      }
      else // case no standard UNICOS DP format --> rebuilt it!
      { 
        int iPos = strpos(dsDp[i], ".");
        string sTemp = substr(dsDp[i], iPos, strlen(dsDp[i]) - iPos);
        dynAppend(
            dsResult, 
            "*" + UN_DPNAME_SEPARATOR +
            "*" + UN_DPNAME_SEPARATOR +
            sAppl + UN_DPNAME_SEPARATOR +
            "*" + UN_DPNAME_SEPARATOR +
            "*"+sTemp
          );
      }
    } // End application loop
  } // End DP loop
  dynUnique(dsResult);
  
  return dsResult;
}


/**
  FWAH-364: Make sure that the name does not contain any fancy character when assigning
            a name to a module.  
  
  **/
string unAlertPanel_getCleanModuleName(string sOrigName)
{
  string sCleanName = sOrigName;
  
  strreplace( sCleanName, ".", "_dot_");
  strreplace( sCleanName, "/", "_slash_");
  strreplace( sCleanName, "\\", "_bslash_");
  strreplace( sCleanName, ":", "_semicol_");
  strreplace( sCleanName, "*", "_star_");
  strreplace( sCleanName, "#", "_pound_"); 
  strreplace( sCleanName, ",", "_comma_"); 
  strreplace( sCleanName, ";", "_coldot_");    
  
  return sCleanName;
}
