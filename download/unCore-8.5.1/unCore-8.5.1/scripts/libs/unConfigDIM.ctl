/**@name LIBRARY: unConfigDIM.ctl

@author: Frederic BERNARD (EN-ICE)

Creation Date: 19/04/2010

Modification History:
  30/09/2011: Herve
  - IS-611: remove the deprecated function calls of the fwInstallation

  30/05/2011: Herve
    - IS-519: Remove all dependencies on graphical object from the Import device panel in the core import 
    functions, to be able to use the unicos front-end/device check/import functions from a WCCOActrl 
                    
External Function :
  . unConfigDIM_checkDeleteCommand : check the coherency of the delete command
  . unConfigDIM_deleteCommand : delete function
  . unConfigDIM_check : check the coherency of the Front-end config
  . unConfigDIM_set : set Front-end config

        
Internal Functions:
        
Purpose:
This library contains the function used to set a DIM Front End.

Usage: Public

PVSS manager usage: Ctrl, UI

Constraints:
  . unGeneration.cat
  . UNICOS-JCOP framework
  . PVSS version: 3.6.2
  . operating system: SLC4/5 and XP
  . distributed system: yes.
*/

private const bool unConfigDim_fwDIMLibLoaded = fwGeneral_loadCtrlLib("fwDIM/fwDIM.ctl",false);
// Constants

// constant for import of UNICOS DIM FE
const string UN_DIM_DPTYPE = "DIM";
const unsigned UN_CONFIG_DIM_LENGTH = 7;
const unsigned UN_CONFIG_DIM_CONFIG = 7;//??

const unsigned UN_CONFIG_DIM_FE_NAME = 2;
const unsigned UN_CONFIG_DIM_APPLICATION = 3;
const unsigned UN_CONFIG_DIM_HEARTBEAT = 4;
const unsigned UN_CONFIG_DIM_VERSION = 5;
const unsigned UN_CONFIG_DIM_DNS = 6;
const unsigned UN_CONFIG_DIM_INFO = 7;
const string UN_CONFIG_DIM_INFO_DELIMITER = ",";
const unsigned UN_CONFIG_DIM_INFO_MAX = 4;

// constant for UNDIM system integrity
const string c_unSystemIntegrity_UNDIM = "DIM_";

// global variable used during the import to keep the UNDIM data, must be global and of course unique.
global dyn_string g_dsImport_UnDIM_config;
const string DIM_PVSS_PREFIX = "DIM_";

//@{

//------------------------------------------------------------------------------------------------------------------------

// unConfigDIM_checkDeleteCommand
/**
Purpose: check the coherency of the delete command

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  dsDeleteDps, dyn_string, output, list of dp that will be deleted
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/

unConfigDIM_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo)
{
string sFeName, sPLCSubApplication = "", sObjectType = "", sNumber = "", sDpPlcName, sPlcModPlc;
dyn_string dsAlarmDps;
	
  //DebugN("unConfigDIM_checkDeleteCommand");

	if (dynlen(dsConfig) >= 2)				// At least, delete instruction and DIM FE name
  	{
// get DIM FE Name
		sFeName = dsConfig[2];
		if ((sFeName != "") && (dsConfig[1] == UN_DELETE_COMMAND))
	  	{
			if (dynlen(dsConfig) >= 3)
		  	{
        // get Application
				sPLCSubApplication = dsConfig[3];
			  }
                        
			if (dynlen(dsConfig) >= 4)
			  {
        // get DeviceType
				sObjectType = dsConfig[4];
			  }
                        
			if (dynlen(dsConfig) >= 5)
  			{
        // get device number
				sNumber = dsConfig[5];
			  }
                        
			unGenericDpFunctions_getPlcDevices("", sFeName, sPLCSubApplication, sObjectType, sNumber, dsDeleteDps);
      // if only delete key word then delete also modbus and systemalarm
			if ((sFeName != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == ""))
			  {
        // sDpPlc is UNDIM_plcName
				sDpPlcName = c_unSystemIntegrity_UNDIM+sFeName;
				if (dpExists(sDpPlcName))
				  {
					dynAppend(dsDeleteDps, sDpPlcName);
					dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1),c_unSystemAlarm_dpType);
					dynAppend(dsDeleteDps, dsAlarmDps);
				  }
			  }
		  }
		else
		  {
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
		  }
	  }
	else
  	{
		fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
	  }
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigDIM_check
/**
Purpose: check the coherency of the DIM front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  dsDeleteDps: dyn_string, input, dp to be deleted
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
        . PVSS version: 3.6.2
        . operating system: NT and W2000, but tested only under W2000.
        . distributed system: yes.
*/
unConfigDIM_check(string currentObject, dyn_string currentLineSplit, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
dyn_dyn_string ddsComponentInfo;
bool bDIMInstalled=false;
int i,len;

  //DebugN("unConfigDIM_check", currentObject, currentLineSplit, dsDeleteDps);

	// get the list of component installed
	fwInstallation_getInstalledComponents(ddsComponentInfo);
	len = dynlen(ddsComponentInfo);

	for(i=1;i<=len; i++)
	{
		if(ddsComponentInfo[i][1] == "fwDIM")
			bDIMInstalled = true;
	}
	if(bDIMInstalled) {
		switch(currentObject) {
			case UN_PLC_COMMAND:
				unConfigDIM_checkApplication(currentLineSplit, dsDeleteDps, exceptionInfo);
				break;
			default:
	 			fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
				break;
		}
	}
	else
	 	fwException_raise(exceptionInfo, "ERROR", "fwDIM not installed", "");
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigDIM_deleteCommand
/**
Purpose: delete function

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/

unConfigDIM_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo)
{
string sPLCSubApplication = "", sObjectType = "", sNumber = "";
dyn_string dsDpeList, exceptionInfoTemp;

  //DebugN("unConfigDIM_deleteCommand");
	
	unConfigDIM_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
	if (dynlen(exceptionInfoTemp) > 0)
	  {
		dynAppend(exceptionInfo, exceptionInfoTemp);
		fwException_raise(exceptionInfo,"ERROR","unConfigDIM_deleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
	  }
	else
	  {
		if (dynlen(dsConfig) >= 2)				// At least, delete instruction and DIM FE name
		  {
			if (dynlen(dsConfig) >= 3)
				sPLCSubApplication = dsConfig[3];

			if (dynlen(dsConfig) >= 4)
				sObjectType = dsConfig[4];

			if (dynlen(dsConfig) >= 5)
				sNumber = dsConfig[5];

// delete first all the subscriptions before deleting the devices
			unConfigDIM_deleteSubscription(dsDpeList, exceptionInfo);
				
			unGenericDpFunctions_deletePlcDevices(dsConfig[2], sPLCSubApplication, sObjectType, sNumber, "progressBar", exceptionInfo);
			if ((dsConfig[2] != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == ""))
				unConfigDIM_deleteFrontEnd(dsConfig[2], exceptionInfo);
  		}
	  }
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigDIM_deleteSubscription
/**
Purpose: delete the DIM subscriptions

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/
unConfigDIM_deleteSubscription(dyn_string dsDp, dyn_string &exceptionInfo)
{
dyn_string dsDpe, dsServices, dsDefault;
dyn_int diTimeout, diFlags, diUpdates;
string sConfig, sFE;
int i, len, j, jLen;
	
	len = dynlen(dsDp);
	for(i=1;i<=len;i++)
    {
    if(dpExists(sFE))
      {                
      sFE=unGenericObject_GetPLCNameFromDpName(dsDp[i]);
      dpGet(sFE+".configuration._fwDimConfig", sConfig);
      if(dpExists(sConfig))
        {          
    		switch(dpTypeName(dsDp[i]))
          {
    			case "FEPLC":
    			case "DIM":                          
              fwDim_getSubscribedServices(sConfig, dsServices, dsDpe, dsDefault, diTimeout, diFlags, diUpdates);        
              jLen=dynlen(dsDpe);
              for(j=1;j<=jLen;j++)
                {
                if(patternMatch(dsDp[i]+"*",dsDpe[j]))
                  fwDim_unSubscribeServices(sConfig, dsServices[j], 0); 
                }
    				break;                                
    			default:
            //DebugN("unConfigDIM_deleteSubscription - unknown Type: " + dpTypeName(dsDp[i]));                          
    				break;
          }                                  
        }
      }
	  }
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigDIM_setfwDimConfig
/**
Purpose: set internal fwDimConfig DpN

Parameters:
  dsFE: dyn_string, input, FE configuration line in file + additional parameters
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/
unConfigDIM_setfwDimConfig(dyn_string dsFE, dyn_string &exceptionInfo)
{
dyn_string dsAdditionalParameters, dsInfoData;
string sDimConfig, sDns, sManager, sDpN, sInfo, sService;
int i, iRes, iManNum, iLen;
 
	if (dynlen(dsFE) == (UN_CONFIG_DIM_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
	  {
    sDimConfig=dsFE[UN_CONFIG_PLC_NAME];
    sDns=dsFE[UN_CONFIG_DIM_DNS];
    sInfo=dsFE[UN_CONFIG_DIM_INFO];          
    iManNum=(int)dsFE[UN_CONFIG_DIM_LENGTH + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]; 
                         
    //1. Configuration
    fwDim_setDimDnsNode(sDimConfig, sDns);
    fwDim_setManNum(sDimConfig, iManNum);
    
    //2. Add and Start DIM Manager 
    //!!! need to be completed to manage the correct version of the DIM manager (3.6 or 3.8 ?)
    sManager="PVSS00dim3.6";
		iRes = fwInstallationManager_add(sManager,
                                     "always", 2, 2, 30,
                                     "-num " + iManNum + " -dim_dp_config " + sDimConfig + " -dim_dns_node " + sDns);    
                                                             
     //3. Subscribe to Services
     sDpN = getSystemName() + DIM_PVSS_PREFIX + sDimConfig;
     if(dpExists(sDpN))
       {           
       fwDim_subscribeService(sDimConfig, dsFE[UN_CONFIG_DIM_HEARTBEAT], sDpN + ".communication.counter", -1, -2, 0, 1, 1);
       dpSetWait(DIM_PVSS_PREFIX + sDimConfig + ".version.publisher", "");  //reset version of the publisher
       
       if(dsFE[UN_CONFIG_DIM_VERSION]!="")
         fwDim_subscribeService(sDimConfig, dsFE[UN_CONFIG_DIM_VERSION], sDpN + ".version.publisher", -1, -2, 0, 1, 1);           
       else
         fwDim_unSubscribeServicesByDp(sDimConfig, sDpN + ".version.publisher", 1);
       
       dsInfoData=strsplit(sInfo, UN_CONFIG_DIM_INFO_DELIMITER);
       iLen=dynlen(dsInfoData);         
       for(i=1;i<=UN_CONFIG_DIM_INFO_MAX;i++)
         {
         if(i<=iLen)  
           sService=dsInfoData[i];
         else
           sService="";
           
         if(sService!="")  
           fwDim_subscribeService(sDimConfig, sService, sDpN + ".communication.info["+i+"]", -1, -2, 0, 1, 1);
         else
           fwDim_unSubscribeServicesByDp(sDimConfig, sDpN + ".communication.info["+i+"]", 1);
           
         dpSetDescription(sDpN+".communication.info["+i+"]", sService);           
         }
       }
     else
      fwException_raise(exceptionInfo,"ERROR","unConfigDIM_setfwDimConfig: " + sDpN + " doesn't exist.","");       
    }
}
  
//------------------------------------------------------------------------------------------------------------------------
//unConfigDIM_set
/**
Purpose: set DIM front-end config

Parameters:
  dsFe: dyn_string, input, config
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/
unConfigDIM_set(string currentObject, dyn_string currentLineSplit, dyn_string &exceptionInfo)
{
string currentDevice, sPlcName, sPlcType, sAnalogArchive;
int iPlcNumber, driver;
bool bEvent16;
dyn_dyn_string ddsComponentInfo;
bool bDIMInstalled=false;
int i,len;

  //DebugN("unConfigDIM_set", currentObject, currentLineSplit); 	

	// get the list of component installed
	fwInstallation_getInstalledComponents(ddsComponentInfo);
	len = dynlen(ddsComponentInfo);

	for(i=1;i<=len; i++)
  	{
		if(ddsComponentInfo[i][1] == "fwDIM")
			bDIMInstalled = true;
  	}
	if(bDIMInstalled)
    {
		switch(currentObject)
      {
			case UN_PLC_COMMAND:
				g_dsImport_UnDIM_config = makeDynString();
				unConfigDIM_setApplication(currentLineSplit, exceptionInfo);
        
        //manage the fwDimConfig DpN creation and configuration                                                        
        unConfigDIM_setfwDimConfig(currentLineSplit, exceptionInfo);
                                     
				break;
			default:
	 			fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
				break;
		  }
	  }
	else
	 	fwException_raise(exceptionInfo, "ERROR", "fwDIM not installed", "");
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigDIM_checkApplication
/**
Purpose: check data for a sub application (not comm)

Parameters:
		dsFE: dyn_string, input, DIM configuration line in file + additional parameters
			dsFE must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, DIM FE name
			3. string, DIM FE subapplication
			4. int, DIM FE address = DIM FE number
			(additional parameters)
			5. driver number 
			6. boolean archive name
			7. analog archive name
		dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2 
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/

unConfigDIM_checkApplication(dyn_string dsFE, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
dyn_string dsFELine, dsFEAdditionalData;
int i, iRes, iDriverNum;
string sFE, sHostname, sAliasDp, tempDp;

  //DebugN("unConfigDIM_checkApplication", dsFE, UN_CONFIG_DIM_LENGTH, UN_CONFIG_PLC_ADDITIONAL_LENGTH);
  
	if (dynlen(dsFE) == (UN_CONFIG_DIM_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
		{
		dsFELine = dsFE;
		for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
			{
			dynAppend(dsFEAdditionalData, dsFELine[UN_CONFIG_DIM_LENGTH+1]);
			dynRemove(dsFELine,UN_CONFIG_DIM_LENGTH+1);
			}
		unConfigDIM_checkData(dsFELine, exceptionInfo);
		unConfigDIM_checkAdditionalData(dsFEAdditionalData, exceptionInfo);

// first check if the DIM FE name is correct:
		tempDp = dsFELine[UN_CONFIG_PLC_NAME];
		if(!unConfigGenericFunctions_nameCheck(tempDp))
      {
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkApplication: " + dsFELine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADPLCNAME"),"");
			return;
  		}
    if(strpos(tempDp, "_")!=-1)                
      {
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkApplication: " + dsFELine[UN_CONFIG_PLC_NAME] + " bad FE name, _ is forbidden","");
			return;
  		}
      
// check if the DIM FE Application Name is correct:
		tempDp = dsFELine[UN_CONFIG_PLC_SUBAPPLICATION];
		if(!unConfigGenericFunctions_nameCheck(tempDp))
      {
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkApplication: " + dsFELine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
			return;
  		}

// sFE is DIM FE
		sFE = getSystemName() +  c_unSystemIntegrity_UNDIM + dsFELine[UN_CONFIG_PLC_NAME];
//		sAliasDp = dpAliasToName(dsFELine[UN_CONFIG_PLC_NAME]);
		sAliasDp = unGenericDpFunctions_dpAliasToName(dsFELine[UN_CONFIG_PLC_NAME]);
//DebugN(sAliasDp, dsFELine[UN_CONFIG_PLC_NAME]);

		if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".")
			sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);

		if ((sAliasDp != "") && (sAliasDp != sFE))
  		{
			if (dynContains(dsDeleteDps, sAliasDp) <= 0)		// Alias exists, is not used by the "DIM" config datapoint and will not be deleted
				fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkApplication: " + dsFELine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADHOSTNAMEALIAS") + sAliasDp,"");
  		}
                
		if (dpExists(sFE) && (dynContains(dsDeleteDps, sFE) <= 0))
  		{
			sHostname = unGenericDpFunctions_getAlias(sFE);
			iRes = dpGet(sFE + ".communication.driver_num", iDriverNum);
			if (iRes >= 0)
	  		{
				if (iDriverNum != dsFEAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM])
					fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCDRIVER")+iDriverNum,"");

				if (sHostname != dsFELine[UN_CONFIG_PLC_NAME])
					fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCHOSTNAME"),"");
  			}
	  	}
	  }
	else
		fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkApplication: " + getCatStr("unGeneration","BADINPUTS"),"");

}

//------------------------------------------------------------------------------------------------------------------------

// unConfigDIM_checkData
/**
Purpose: check data for configuration

Parameters:
		dsFE: dyn_string, input, DIM FE configuration line in file
			dsFE must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, name
			3. string, subApplication
			4. int, DIM FE address = DIM FE number
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2 
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/

unConfigDIM_checkData(dyn_string dsFE, dyn_string &exceptionInfo)
{
unsigned uPLCNumber;
	
  //DebugN("unConfigDIM_checkData", dsFE);

	if (dynlen(dsFE) != UN_CONFIG_DIM_LENGTH)
		fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkData:" + getCatStr("unGeneration","BADINPUTS"),"");
	else
  	{
		unConfigGenericFunctions_checkPLCType(dsFE[UN_CONFIG_PLC_TYPE], exceptionInfo);

		if (strrtrim(dsFE[UN_CONFIG_PLC_NAME]) == "")
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkData:" + getCatStr("unGeneration","ERRORPLCNAME"),"");
		
		if (strrtrim(dsFE[UN_CONFIG_PLC_SUBAPPLICATION]) == "")
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkData:" + getCatStr("unGeneration","ERRORSUBAPPLI"),"");

		if (strrtrim(dsFE[UN_CONFIG_DIM_DNS]) == "")
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkData: no DIM DNS defined","");
                
		if (strrtrim(dsFE[UN_CONFIG_DIM_HEARTBEAT]) == "")
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkData: no HeartBeat defined","");                
	  }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigDIM_checkAdditionalData
/**
Purpose: check additional data for configuration

Parameters:
		dsFE: dyn_string, input, DIM FE additional data
			dsFE must contains :
			1. unsigned, driver number
			2. string, boolean archive
			3. string, analog archive
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 3.6.2 
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/

unConfigDIM_checkAdditionalData(dyn_string dsFE, dyn_string &exceptionInfo)
{
unsigned uPLCDriver;

  //DebugN("unConfigDIM_checkAdditionalData");
	
	if (dynlen(dsFE) != UN_CONFIG_PLC_ADDITIONAL_LENGTH)
		fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkAdditionalData:" + getCatStr("unGeneration","BADINPUTS"),"");
	else
  	{
		if (dsFE[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "")
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkAdditionalData: bool archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");

    if (dsFE[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "")
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkAdditionalData: analog archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");

    if (dsFE[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "")
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_checkAdditionalData: event archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
	  }
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigDIM_setApplication
/**
Purpose: set DIM FE configuration for a sub application (not comm)

Parameters:
		dsFE: dyn_string, input, FE configuration line in file + additional parameters
			dsFE must contains :
			(FE config line)
			1. string, "DIM"
			2. string, FE name
			3. string, FE subapplication
			4. int, FE Heartbeat address
			5. int, DIM DNS name
      //additionnal parameters
			6. driver number 
			7. boolean archive name
			8. analog archive name
			9. ?
			10. event archive name                                                
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. First line of a generation file must contain the DIM FE description. Fields are defined in constants at this library beginning
	. PVSS version: 3.6.2 
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/
unConfigDIM_setApplication(dyn_string dsFEData, dyn_string &exceptionInfo)
{
int iPLCNumber, iRes, i, position;
string sDpName, addressReference, tempDp, sfwDimConfig;
dyn_string dsFE, dsAdditionalParameters, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
dyn_string dsEventArchives;
	
  //DebugN("unConfigDIM_setApplication", dsFEData);        
        
	if (dynlen(dsFEData) == (UN_CONFIG_DIM_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
	  {
	// 1. Separate config file info and additional parameters
		dsFE = dsFEData;
		dynClear(dsAdditionalParameters);
		for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
		  {
			dynAppend(dsAdditionalParameters, dsFE[UN_CONFIG_DIM_LENGTH+1]);
			dynRemove(dsFE,UN_CONFIG_DIM_LENGTH+1);
		  }
	
    sfwDimConfig = dsFE[UN_CONFIG_PLC_NAME];                
	// check if the DIM FE Application Name is correct:
		tempDp = dsFEData[UN_CONFIG_PLC_SUBAPPLICATION];
		if(!unConfigGenericFunctions_nameCheck(tempDp))
      {
			fwException_raise(exceptionInfo,"ERROR","unConfigDIM_setApplication: " + dsFEData[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
			return;
	  	}
	
	// 3. Set DIM FE configuration in DIM dpType
		sDpName = c_unSystemIntegrity_UNDIM + dsFE[UN_CONFIG_PLC_NAME];
		tempDp = sDpName;
		unConfigGenericFunctions_createDp(sDpName, UN_DIM_DPTYPE, exceptionInfoTemp);

		if (dynlen(exceptionInfoTemp) <= 0)
		  {
			sDpName = getSystemName() + sDpName;
			unConfigGenericFunctions_setAlias(sDpName, dsFE[UN_CONFIG_PLC_NAME], exceptionInfo);
			iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
						       sDpName + ".configuration.importTime", dsImportTimes,
						       sDpName + ".configuration.archive_bool", dsBoolArchives,
						       sDpName + ".configuration.archive_analog", dsAnalogArchives,
						       sDpName + ".configuration.archive_event", dsEventArchives);
                        
			position = dynContains(dsSubApplications, dsFE[UN_CONFIG_PLC_SUBAPPLICATION]);
			if (position <= 0)
				position = dynlen(dsSubApplications) + 1;

// if the event archive is empty, set it to bool archive
			if(dynlen(dsEventArchives) <= 0)
				dsEventArchives = dsBoolArchives;
				
			dsSubApplications[position] = dsFE[UN_CONFIG_PLC_SUBAPPLICATION];
			dsImportTimes[position] = (string)getCurrentTime();
			dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
			dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
			dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];
	    iRes = dpSetWait(sDpName + ".configuration.importTime", dsImportTimes,
							         sDpName + ".configuration.archive_bool", dsBoolArchives,
        							 sDpName + ".configuration.archive_analog", dsAnalogArchives,
        							 sDpName + ".configuration.archive_event", dsEventArchives,
                 	  	 sDpName + ".configuration.subApplications", dsSubApplications,
                       sDpName + ".configuration._fwDimConfig", sfwDimConfig,
        							 sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]);
            
			if (iRes < 0)
				fwException_raise(exceptionInfo,"ERROR","unConfigDIM_setApplication: " + getCatStr("unPVSS","DPSETFAILED"),"");

	// 4. Create _UnSystemAlarm datapoints
      if(!dpExists(tempDp))
        {                          
  			unSystemIntegrity_createSystemAlarm(tempDp,
	  																				DS_pattern,
		  																			getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsFE[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
			  																		exceptionInfo);
                        
  			dpSet(c_unSystemAlarm_dpPattern+DS_pattern+tempDp+".enabled", true, 
	  					c_unSystemAlarm_dpPattern+DS_pattern+tempDp+".alarm", c_unSystemIntegrity_no_alarm_value);
        }                    
                        
	// 5. Create _FwDimConfig
      fwDim_createConfig(sfwDimConfig);        
        
                                      
  		}
		else
			dynAppend(exceptionInfo, exceptionInfoTemp);
  	}
	else
		fwException_raise(exceptionInfo,"ERROR","unConfigDIM_setApplication: " + getCatStr("unGeneration","BADINPUTS"),"");

}

//---------------------------------------------------------------------------------------------------------------------------------------

// unConfigDIM_deleteFrontEnd
/**
Purpose: delete Front-end config

Parameters : 
	sPlcName, string, input, DIM FE name
	exceptionInfo, dyn_string, output, for errors
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.6.2 
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/

unConfigDIM_deleteFrontEnd(string sPlcName, dyn_string& exceptionInfo)
{
string sDpPlcName, sPlcModPlc="", exceptionText;
int i, length, iRes;
dyn_string dsAlarmDps;

  //DebugN("unConfigDIM_deleteFrontEnd"); 	

	sDpPlcName = c_unSystemIntegrity_UNDIM+sPlcName;
	if (!dpExists(sDpPlcName))
		sDpPlcName = "";

	if (sDpPlcName != "")
	  {
		if (unGenericDpFunctions_getSystemName(sDpPlcName) == getSystemName())
  		{
	// 2. Delete DIM FE and _FwDimConfig
			iRes = dpDelete(substr(sDpPlcName, strpos(sDpPlcName, ":") + 1));
      fwDim_deleteConfig(sPlcName);                        
			if (iRes < 0)
				fwException_raise(exceptionInfo, "ERROR", "unConfigDIM_deleteFrontEnd: " + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");

	// 3. Delete alarm system 
			dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1),c_unSystemAlarm_dpType);
			length = dynlen(dsAlarmDps);
			for(i=1;i<=length;i++)
		  	{
				_unSystemIntegrity_modifyApplicationAlertList(dsAlarmDps[i], false, exceptionInfo);
				iRes = dpDelete(dsAlarmDps[i]);
				if (iRes < 0)
					fwException_raise(exceptionInfo, "ERROR", "unConfigDIM_deleteFrontEnd: " + dsAlarmDps[i] + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
	  		}	
		  }
		else
			fwException_raise(exceptionInfo, "ERROR", "unConfigDIM_deleteFrontEnd: " + sDpPlcName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
  	}
}

//------------------------------------------------------------------------------------------------------------------------

// DIM_getFrontEndArchiveDp
/**
Purpose: return the list of Dps that wil be archived during the set of the DIM front-end

Parameters:
		sCommand: string, input, the current entry in the import file
		sDp: string, input, the given dp: DIM FE or SystemAlarm  Dp
		sFEType: string, input, the front-end type
		dsArchiveDp: dyn_string, output, list of archivedDp

Usage: External function

PVSS manager usage: Ctrl, UI

Constraints:
	. PVSS version: 2.12.1 
	. operating system: SLC4/5 and XP
	. distributed system: yes.
*/

DIM_getFrontEndArchiveDp(string sCommand, string sDp, string sFEType, dyn_string &dsArchiveDp)
{
	switch(sCommand) {
		case UN_PLC_COMMAND:
			dsArchiveDp = makeDynString();
			break;
		default:
			dsArchiveDp = makeDynString();
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------

//@}

