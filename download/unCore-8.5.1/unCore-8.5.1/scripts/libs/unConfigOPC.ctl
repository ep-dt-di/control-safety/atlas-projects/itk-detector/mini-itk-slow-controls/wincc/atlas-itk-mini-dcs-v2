/**@name LIBRARY: unConfigOPC.ctl

@author: Herve Milcent (AB/CO)

Creation Date: 15/04/2005

Modification History:
  30/05/2011: Herve
    - IS-519: Remove all dependencies on graphical object from the Import device panel in the core import 
    functions, to be able to use the unicos front-end/device check/import functions from a WCCOActrl 
    
  02/05/2011: Herve
  - IS-510: CPC 5 and CPC 6 compatibility of the export/import of the CPC device configuration with all the front-end types
  
  20/07/2009: Herve
    set LLC for the input address config
    
  19/02/2009: Herve
    - include unOPC.ctl libs
    
	01/07/2005: Herve
		- unConfigOPC_setApplication: set configuration.OPCConfiguration to OPCServer$OPCGroupIn$OPCGroupOut 

	12/05/2004: Herve
		- DigitalInput: delete archive on evStsReg01 if no archive on PosSt
		
	22/04/2005:
		- implement the event archive: check the event archive name
		- remove UN_CONFIG_DEFAULT_ARCHIVE_SMOOTHING and replace in libs by DPATTR_ARCH_PROC_SIMPLESM
		- add new function OPC_getFrontEndArchiveDp: return the list of Dp that will be archive for the front-end

version 1#

External Function :
  . unConfigOPC_checkDeleteCommand : check the coherency of the delete command
  . unConfigOPC_deleteCommand : delete function
  . unConfigOPC_check : check the coherency of the Front-end config
  . unConfigOPC_set : set Front-end config

        
Internal Functions:
        
Purpose:
This library contains the function used to set the OPC.

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . unGeneration.cat
  . UNICOS-JCOP framework
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

// Constants
// Data type OPC constant
const int OPC_INT16 = 481;
const int OPC_INT32 = 482;
const int OPC_UINT16 = 488;
const int OPC_BOOL = 486;
const int OPC_FLOAT = 484;

// constant for import of UNOPC
const string UNOPC_DPTYPE = "OPC";
const unsigned UN_CONFIG_OPC_LENGTH = 6;
const unsigned UN_CONFIG_UNOPC_SERVERNAME = 4;
const unsigned UN_CONFIG_UNOPC_GROUPINNAME = 5;
const unsigned UN_CONFIG_UNOPC_GROUPOUTNAME = 6;

// constant for UNOPC system integrity
const string c_unSystemIntegrity_UNOPC = "OPC_";

// global variable used during the import to keep the UNOPC data, must be global and of course unique.
global dyn_string g_dsImport_UnOPC_config;
const unsigned UNOPC_SERVERNAME = 1;
const unsigned UNOPC_GROUPINNAME = 2;
const unsigned UNOPC_GROUPOUTNAME = 3;
const string PVSS_OPCSERVERDPTYPE = "_OPCServer";
const string OPC_PVSS_PREFIX = "_";
const string PVSS_OPCGROUPDPTYPE = "_OPCGroup";

//@{

//------------------------------------------------------------------------------------------------------------------------

// unConfigOPC_checkDeleteCommand
/**
Purpose: check the coherency of the delete command

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  dsDeleteDps, dyn_string, output, list of dp that will be deleted
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigOPC_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo)
{
	string sFeName, sPLCSubApplication = "", sObjectType = "", sNumber = "", sDpPlcName, sPlcModPlc;
	dyn_string dsAlarmDps;
	
	if (dynlen(dsConfig) >= 2)				// At least, delete instruction and plc name
	{
// get PLC Name
		sFeName = dsConfig[2];
		if ((sFeName != "") && (dsConfig[1] == UN_DELETE_COMMAND))
		{
			if (dynlen(dsConfig) >= 3)
			{
// get Application
				sPLCSubApplication = dsConfig[3];
			}
			if (dynlen(dsConfig) >= 4)
			{
// get DeviceType
				sObjectType = dsConfig[4];
			}
			if (dynlen(dsConfig) >= 5)
			{
// get device number
				sNumber = dsConfig[5];
			}
			unGenericDpFunctions_getPlcDevices("", sFeName, sPLCSubApplication, sObjectType, sNumber, dsDeleteDps);
// if only delete key word then delete also modbus and systemalarm
			if ((sFeName != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == ""))
			{
// sDpPlc is UNOPC_plcName
				sDpPlcName = c_unSystemIntegrity_UNOPC+sFeName;
				if (dpExists(sDpPlcName))
				{
					dynAppend(dsDeleteDps, sDpPlcName);
					dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1),c_unSystemAlarm_dpType);
					dynAppend(dsDeleteDps, dsAlarmDps);
				}
			}
		}
		else
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
		}
	}
	else
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigOPC_check
/**
Purpose: check the coherency of the OPC front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  dsDeleteDps: dyn_string, input, dp to be deleted
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: NT and W2000, but tested only under W2000.
        . distributed system: yes.
*/
unConfigOPC_check(string currentObject, dyn_string currentLineSplit, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
//DebugN(currentObject, currentLineSplit);
	switch(currentObject) {
		case UN_PLC_COMMAND:
			unConfigOPC_checkApplication(currentLineSplit, dsDeleteDps, exceptionInfo);
			break;
		default:
 			fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigOPC_deleteCommand
/**
Purpose: delete function

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigOPC_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo)
{
	string sPLCSubApplication = "", sObjectType = "", sNumber = "";
	dyn_string dsDpeList, exceptionInfoTemp;
	
	unConfigOPC_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
	if (dynlen(exceptionInfoTemp) > 0)
	{
		dynAppend(exceptionInfo, exceptionInfoTemp);
		fwException_raise(exceptionInfo,"ERROR","unConfigOPC_deleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
	else
	{
		if (dynlen(dsConfig) >= 2)				// At least, delete instruction and plc name
		{
			if (dynlen(dsConfig) >= 3)
			{
				sPLCSubApplication = dsConfig[3];
			}
			if (dynlen(dsConfig) >= 4)
			{
				sObjectType = dsConfig[4];
			}
			if (dynlen(dsConfig) >= 5)
			{
				sNumber = dsConfig[5];
			}
			unGenericDpFunctions_deletePlcDevices(dsConfig[2], sPLCSubApplication, sObjectType, sNumber, "progressBar", exceptionInfo);
			if ((dsConfig[2] != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == ""))
			{
				unConfigOPC_deleteFrontEnd(dsConfig[2], exceptionInfo);
			}
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigOPC_set
/**
Purpose: set OPC front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigOPC_set(string currentObject, dyn_string currentLineSplit, dyn_string &exceptionInfo)
{
	string currentDevice, sPlcName, sPlcType, sAnalogArchive;
	int iPlcNumber, driver;
	bool bEvent16;

//DebugN(currentObject);
	switch(currentObject) {
		case UN_PLC_COMMAND:
			g_dsImport_UnOPC_config = makeDynString();
			unConfigOPC_setApplication(currentLineSplit, exceptionInfo);
	// keep the PVSS OPC Server name for future use in import of the device.
			if(dynlen(exceptionInfo) <= 0) {
				g_dsImport_UnOPC_config[UNOPC_SERVERNAME] = currentLineSplit[UN_CONFIG_UNOPC_SERVERNAME];
				g_dsImport_UnOPC_config[UNOPC_GROUPINNAME] = currentLineSplit[UN_CONFIG_UNOPC_GROUPINNAME];
				g_dsImport_UnOPC_config[UNOPC_GROUPOUTNAME] = currentLineSplit[UN_CONFIG_UNOPC_GROUPOUTNAME];
			}
//DebugN(g_dsImport_UnOPC_config);
			break;
		default:
 			fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigOPC_checkApplication
/**
Purpose: check data for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, OPC configuration line in file + additional parameters
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number 
			6. boolean archive name
			7. analog archive name
		dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigOPC_checkApplication(dyn_string dsPLC, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
	dyn_string dsPLCLine, dsPLCAdditionalData;
	int i, iRes, iDriverNum;
	string sPLCDp, sHostname, sAliasDp, tempDp;

//DebugN("unConfigOPC_checkApplication", dsPLC, UN_CONFIG_OPC_LENGTH, UN_CONFIG_PLC_ADDITIONAL_LENGTH);
	if (dynlen(dsPLC) == (UN_CONFIG_OPC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
		{
		dsPLCLine = dsPLC;
		for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
			{
			dynAppend(dsPLCAdditionalData, dsPLCLine[UN_CONFIG_OPC_LENGTH+1]);
			dynRemove(dsPLCLine,UN_CONFIG_OPC_LENGTH+1);
			}
		unConfigOPC_checkData(dsPLCLine, exceptionInfo);
		unConfigOPC_checkAdditionalData(dsPLCAdditionalData, exceptionInfo);

// first check if the PLC name is correct:
		tempDp = dsPLCLine[UN_CONFIG_PLC_NAME];
		if(!unConfigGenericFunctions_nameCheck(tempDp)) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADPLCNAME"),"");
			return;
		}

// check if the PLC Application Name is correct:
		tempDp = dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION];
		if(!unConfigGenericFunctions_nameCheck(tempDp)) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
			return;
		}

// sPLCDp is UNOPC_plcName
		sPLCDp = getSystemName() +  c_unSystemIntegrity_UNOPC + dsPLCLine[UN_CONFIG_PLC_NAME];
//		sAliasDp = dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
		sAliasDp = unGenericDpFunctions_dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
//DebugN(sAliasDp, dsPLCLine[UN_CONFIG_PLC_NAME]);

		if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".")
		{
			sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);
		}
		if ((sAliasDp != "") && (sAliasDp != sPLCDp))
		{
			if (dynContains(dsDeleteDps, sAliasDp) <= 0)		// Alias exists, is not used by the "OPC" config datapoint and will not be deleted
			{
				fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADHOSTNAMEALIAS") + sAliasDp,"");
			}
		}
		if (dpExists(sPLCDp) && (dynContains(dsDeleteDps, sPLCDp) <= 0))
		{
			sHostname = unGenericDpFunctions_getAlias(sPLCDp);
			iRes = dpGet(sPLCDp + ".communication.driver_num", iDriverNum);
			if (iRes >= 0)
			{
				if (iDriverNum != dsPLCAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM])
				{
					fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCDRIVER")+iDriverNum,"");
				}
				if (sHostname != dsPLCLine[UN_CONFIG_PLC_NAME])
				{
					fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCHOSTNAME"),"");
				}
			}
		}
	}
	else
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: " + getCatStr("unGeneration","BADINPUTS"),"");
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigOPC_checkData
/**
Purpose: check PLC data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, name
			3. string, subApplication
			4. int, PLC address = PLC number
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigOPC_checkData(dyn_string dsPLC, dyn_string &exceptionInfo)
{
	unsigned uPLCNumber;
	
	if (dynlen(dsPLC) != UN_CONFIG_OPC_LENGTH)
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkData:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
	else
	{
		unConfigGenericFunctions_checkPLCType(dsPLC[UN_CONFIG_PLC_TYPE], exceptionInfo);

		if (strrtrim(dsPLC[UN_CONFIG_PLC_NAME]) == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkData:" + getCatStr("unGeneration","ERRORPLCNAME"),"");
		}
		
		if (strrtrim(dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]) == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkData:" + getCatStr("unGeneration","ERRORSUBAPPLI"),"");
		}			

// check if the OPCServer DP is existing.
		if(!dpExists(OPC_PVSS_PREFIX+dsPLC[UN_CONFIG_UNOPC_SERVERNAME])) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: bad " + dsPLC[UN_CONFIG_UNOPC_SERVERNAME],"");
		}
		else if (dpTypeName(OPC_PVSS_PREFIX+dsPLC[UN_CONFIG_UNOPC_SERVERNAME]) != PVSS_OPCSERVERDPTYPE) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: bad type _" + dsPLC[UN_CONFIG_UNOPC_SERVERNAME],"");
		}

// check if the OPCGroup for inputs DP is existing.
		if(!dpExists(OPC_PVSS_PREFIX+dsPLC[UN_CONFIG_UNOPC_GROUPINNAME])) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: bad " + dsPLC[UN_CONFIG_UNOPC_GROUPINNAME],"");
		}
		else if (dpTypeName(OPC_PVSS_PREFIX+dsPLC[UN_CONFIG_UNOPC_GROUPINNAME]) != PVSS_OPCGROUPDPTYPE) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: bad type _" + dsPLC[UN_CONFIG_UNOPC_GROUPINNAME],"");
		}

// check if the OPCGroup for oututs DP is existing.
		if(!dpExists(OPC_PVSS_PREFIX+dsPLC[UN_CONFIG_UNOPC_GROUPOUTNAME])) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: bad " + dsPLC[UN_CONFIG_UNOPC_GROUPOUTNAME],"");
		}
		else if (dpTypeName(OPC_PVSS_PREFIX+dsPLC[UN_CONFIG_UNOPC_GROUPOUTNAME]) != PVSS_OPCGROUPDPTYPE) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkApplication: bad type _" + dsPLC[UN_CONFIG_UNOPC_GROUPOUTNAME],"");
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigOPC_checkAdditionalData
/**
Purpose: check additional data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC additional data
			dsPLC must contains :
			1. unsigned, driver number
			2. string, boolean archive
			3. string, analog archive
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigOPC_checkAdditionalData(dyn_string dsPLC, dyn_string &exceptionInfo)
{
	unsigned uPLCDriver;
	
	if (dynlen(dsPLC) != UN_CONFIG_PLC_ADDITIONAL_LENGTH)
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkAdditionalData:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
	else
	{
		if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkAdditionalData: bool archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
		}
		if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkAdditionalData: analog archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
		}
		if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_checkAdditionalData: event archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigOPC_setApplication
/**
Purpose: set PLC configuration for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file + additional parameters
			dsPLC must contains :
			(Plc config line)
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number 
			6. boolean archive name
			7. analog archive name
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. First line of a generation file must contain the PLC description. Fields are defined in constants at this library beginning
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigOPC_setApplication(dyn_string dsPLCData, dyn_string &exceptionInfo)
{
	int iPLCNumber, iRes, i, position;
	string sDpName, addressReference, tempDp;
	dyn_string dsPLC, dsAdditionalParameters, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
	dyn_string dsEventArchives;
	// Check
	
	if (dynlen(dsPLCData) == (UN_CONFIG_OPC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
	{
	// 1. Separate config file info and additional parameters
		dsPLC = dsPLCData;
		dynClear(dsAdditionalParameters);
		for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
		{
			dynAppend(dsAdditionalParameters, dsPLC[UN_CONFIG_OPC_LENGTH+1]);
			dynRemove(dsPLC,UN_CONFIG_OPC_LENGTH+1);
		}
	
	// check if the PLC Application Name is correct:
		tempDp = dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION];
		if(!unConfigGenericFunctions_nameCheck(tempDp)) {
			fwException_raise(exceptionInfo,"ERROR","unConfigOPC_setApplication: " + dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
			return;
		}
	
	// 3. Set PLC configuration in UnOPC
		sDpName = c_unSystemIntegrity_UNOPC + dsPLC[UN_CONFIG_PLC_NAME];
		tempDp = sDpName;
		unConfigGenericFunctions_createDp(sDpName, UNOPC_DPTYPE, exceptionInfoTemp);

		if (dynlen(exceptionInfoTemp) <= 0)
		{
			sDpName = getSystemName() + sDpName;
			unConfigGenericFunctions_setAlias(sDpName, dsPLC[UN_CONFIG_PLC_NAME], exceptionInfo);
			iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
						 sDpName + ".configuration.importTime", dsImportTimes,
						 sDpName + ".configuration.archive_bool", dsBoolArchives,
						 sDpName + ".configuration.archive_analog", dsAnalogArchives,
						 sDpName + ".configuration.archive_event", dsEventArchives);
			position = dynContains(dsSubApplications, dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]);
			if (position <= 0)
			{
				position = dynlen(dsSubApplications) + 1;
			}
// if the event archive is empty, set it to bool archive
			if(dynlen(dsEventArchives) <= 0)
				dsEventArchives = dsBoolArchives;
				
			dsSubApplications[position] = dsPLC[UN_CONFIG_PLC_SUBAPPLICATION];
			dsImportTimes[position] = (string)getCurrentTime();
			dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
			dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
			dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];
	    iRes = dpSetWait(sDpName + ".configuration.importTime", dsImportTimes,
							 sDpName + ".configuration.archive_bool", dsBoolArchives,
							 sDpName + ".configuration.archive_analog", dsAnalogArchives,
							 sDpName + ".configuration.archive_event", dsEventArchives,
         	  	 sDpName + ".configuration.subApplications",dsSubApplications,
							 sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
							 sDpName + ".configuration.OPCConfiguration", dsPLCData[UN_CONFIG_UNOPC_SERVERNAME]+"$"+
							 																							dsPLCData[UN_CONFIG_UNOPC_GROUPINNAME]+"$"+
							 																							dsPLCData[UN_CONFIG_UNOPC_GROUPOUTNAME]);

			if (iRes < 0)
			{
				fwException_raise(exceptionInfo,"ERROR","unConfigOPC_setApplication: " + getCatStr("unPVSS","DPSETFAILED"),"");
			}	

	// 4. Create _UnSystemAlarm datapoints
			unSystemIntegrity_createSystemAlarm(tempDp,
																					DS_pattern,
																					getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
																					exceptionInfo);
			dpSet(c_unSystemAlarm_dpPattern+DS_pattern+tempDp+".enabled", true, 
										c_unSystemAlarm_dpPattern+DS_pattern+tempDp+".alarm", c_unSystemIntegrity_no_alarm_value);
		}
		else
		{
			dynAppend(exceptionInfo, exceptionInfoTemp);
		}
	}
	else
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigOPC_setApplication: " + getCatStr("unGeneration","BADINPUTS"),"");
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unConfigOPC_deleteFrontEnd
/**
Purpose: delete Front-end config

Parameters : 
	sPlcName, string, input, plc name
	exceptionInfo, dyn_string, output, for errors
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigOPC_deleteFrontEnd(string sPlcName, dyn_string& exceptionInfo)
{
	string sDpPlcName, sPlcModPlc="", exceptionText;
	int i, length, iRes;
	dyn_string dsAlarmDps;
	

	sDpPlcName = c_unSystemIntegrity_UNOPC+sPlcName;
	if (!dpExists(sDpPlcName))
		sDpPlcName = "";

	if (sDpPlcName != "")
	{
		if (unGenericDpFunctions_getSystemName(sDpPlcName) == getSystemName())
		{
	// 2. Delete _Un_Plc
			iRes = dpDelete(substr(sDpPlcName, strpos(sDpPlcName, ":") + 1));
			if (iRes < 0)
			{
				fwException_raise(exceptionInfo, "ERROR", "unConfigOPC_deleteFrontEnd: " + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
			}
	// 3. Delete alarm system 
			dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1),c_unSystemAlarm_dpType);
			length = dynlen(dsAlarmDps);
			for(i=1;i<=length;i++)
			{
				_unSystemIntegrity_modifyApplicationAlertList(dsAlarmDps[i], false, exceptionInfo);
				iRes = dpDelete(dsAlarmDps[i]);
				if (iRes < 0)
				{
					fwException_raise(exceptionInfo, "ERROR", "unConfigOPC_deleteFrontEnd: " + dsAlarmDps[i] + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
				}
			}
			
		}
		else
		{
			fwException_raise(exceptionInfo, "ERROR", "unConfigOPC_deleteFrontEnd: " + sDpPlcName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
		}
	}
}


//------------------------------------------------------------------------------------------------------------------------

// OPC_getFrontEndArchiveDp
/**
Purpose: return the list of Dps that wil be archived during the set of the OPC front-end

Parameters:
		sCommand: string, input, the current entry in the import file
		sDp: string, input, the given dp: PLC or SystemAlarm  Dp
		sFEType: string, input, the front-end type
		dsArchiveDp: dyn_string, output, list of archivedDp

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

OPC_getFrontEndArchiveDp(string sCommand, string sDp, string sFEType, dyn_string &dsArchiveDp)
{
	switch(sCommand) {
		case UN_PLC_COMMAND:
			dsArchiveDp = makeDynString();
			break;
		default:
			dsArchiveDp = makeDynString();
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------
#uses "unOPC.ctl"

//@}

