/**@name LIBRARY: unConfigSOFT_FE.ctl

@author: Herve Milcent (AB/CO)

Creation Date: 05/07/2007

Modification History:
  30/05/2011: Herve
    - IS-519: Remove all dependencies on graphical object from the Import device panel in the core import 
    functions, to be able to use the unicos front-end/device check/import functions from a WCCOActrl 
    
  02/05/2011: Herve
  - IS-510: CPC 5 and CPC 6 compatibility of the export/import of the CPC device configuration with all the front-end types
  
  28/04/2010: Herve
  - add the description in WordStatus import and export
  
  15/06/2009: Quique
    - include the digital parameter as device 
  19/02/2009: Herve
    - include unSOFT_FE.ctl libs
    
	11/12/2007: Herve
		- add archive on Diagnostic info
		
	12/11/2007:	Quique	
		- Adding AnalogParameter device (Options: Autonomous (automatic feedback) or Not)

version 1#

External Function :
  . unConfigSOFT_FE_checkDeleteCommand : check the coherency of the delete command
  . unConfigSOFT_FE_deleteCommand : delete function
  . unConfigSOFT_FE_check : check the coherency of the Front-end config
  . unConfigSOFT_FE_set : set Front-end config

        
Internal Functions:
        
Purpose:
This library contains the function used to set the SOFT_FE.

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . unGeneration.cat
  . UNICOS-JCOP framework
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

// constant for import of SOFT_FE
const string SOFT_FE_DPTYPE = "SOFT_FE";
const unsigned UN_CONFIG_SOFT_FE_LENGTH = 4;
const unsigned UN_CONFIG_SOFT_FE_SYSTEMINTEGRITY = 4;

// constant for SOFT_FE system integrity
const string c_unSystemIntegrity_SOFT_FE = "SOFT_FE_";

//@{

//------------------------------------------------------------------------------------------------------------------------

// unConfigSOFT_FE_checkDeleteCommand
/**
Purpose: check the coherency of the delete command

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  dsDeleteDps, dyn_string, output, list of dp that will be deleted
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo)
{
	string sFeName, sPLCSubApplication = "", sObjectType = "", sNumber = "", sDpPlcName, sPlcModPlc;
	dyn_string dsAlarmDps;
	
	if (dynlen(dsConfig) >= 2)				// At least, delete instruction and plc name
	{
// get PLC Name
		sFeName = dsConfig[2];
		if ((sFeName != "") && (dsConfig[1] == UN_DELETE_COMMAND))
		{
			if (dynlen(dsConfig) >= 3)
			{
// get Application
				sPLCSubApplication = dsConfig[3];
			}
			if (dynlen(dsConfig) >= 4)
			{
// get DeviceType
				sObjectType = dsConfig[4];
			}
			if (dynlen(dsConfig) >= 5)
			{
// get device number
				sNumber = dsConfig[5];
			}
			unGenericDpFunctions_getPlcDevices("", sFeName, sPLCSubApplication, sObjectType, sNumber, dsDeleteDps);
// if only delete key word then delete also modbus and systemalarm
			if ((sFeName != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == ""))
			{
// sDpPlc is SOFT_FE_plcName
				sDpPlcName = c_unSystemIntegrity_SOFT_FE+sFeName;
				if (dpExists(sDpPlcName))
				{
					dynAppend(dsDeleteDps, sDpPlcName);
					dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1),c_unSystemAlarm_dpType);
					dynAppend(dsDeleteDps, dsAlarmDps);
				}
			}
		}
		else
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
		}
	}
	else
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigSOFT_FE_checkFrontEnd
/**
Purpose: check the coherency of the SOFT_FE front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  dsDeleteDps: dyn_string, input, dp to be deleted
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: NT and W2000, but tested only under W2000.
        . distributed system: yes.
*/
unConfigSOFT_FE_checkFrontEnd(string currentObject, dyn_string currentLineSplit, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
	dyn_dyn_string ddsComponentInfo;
//DebugN(currentObject, currentLineSplit);

	switch(currentObject) {
		case UN_PLC_COMMAND:
			unConfigSOFT_FE_checkApplication(currentLineSplit, dsDeleteDps, exceptionInfo);
			break;
		default:
 			fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigSOFT_FE_deleteCommand
/**
Purpose: delete function

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo)
{
	string sPLCSubApplication = "", sObjectType = "", sNumber = "";
	dyn_string dsDpeList, exceptionInfoTemp;
	
	unConfigSOFT_FE_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
	if (dynlen(exceptionInfoTemp) > 0)
	{
		dynAppend(exceptionInfo, exceptionInfoTemp);
		fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_deleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
	else
	{
		if (dynlen(dsConfig) >= 2)				// At least, delete instruction and plc name
		{
			if (dynlen(dsConfig) >= 3)
			{
				sPLCSubApplication = dsConfig[3];
			}
			if (dynlen(dsConfig) >= 4)
			{
				sObjectType = dsConfig[4];
			}
			if (dynlen(dsConfig) >= 5)
			{
				sNumber = dsConfig[5];
			}

			unGenericDpFunctions_deletePlcDevices(dsConfig[2], sPLCSubApplication, sObjectType, sNumber, "progressBar", exceptionInfo);
			if ((dsConfig[2] != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == ""))
			{
				unConfigSOFT_FE_deleteFrontEnd(dsConfig[2], exceptionInfo);
			}
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigSOFT_FE_setFrontEnd
/**
Purpose: set SOFT_FE front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_setFrontEnd(string currentObject, dyn_string currentLineSplit, dyn_string &exceptionInfo)
{
	string currentDevice, sPlcName, sPlcType, sAnalogArchive;
	int iPlcNumber, driver;
	bool bEvent16;

//DebugN(currentObject, currentLineSplit);
	switch(currentObject) {
		case UN_PLC_COMMAND:
			unConfigSOFT_FE_setApplication(currentLineSplit, exceptionInfo);
			break;
		default:
 			fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigSOFT_FE_checkApplication
/**
Purpose: check data for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, SOFT_FE configuration line in file + additional parameters
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number 
			6. boolean archive name
			7. analog archive name
		dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_checkApplication(dyn_string dsPLC, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
	dyn_string dsPLCLine, dsPLCAdditionalData;
	int i, iRes, iDriverNum;
	string sPLCDp, sHostname, sAliasDp, tempDp;

//DebugN("unConfigSOFT_FE_checkApplication", dsPLC, UN_CONFIG_SOFT_FE_LENGTH, UN_CONFIG_PLC_ADDITIONAL_LENGTH);
	if (dynlen(dsPLC) == (UN_CONFIG_SOFT_FE_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
		{
		dsPLCLine = dsPLC;
		for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
			{
			dynAppend(dsPLCAdditionalData, dsPLCLine[UN_CONFIG_SOFT_FE_LENGTH+1]);
			dynRemove(dsPLCLine,UN_CONFIG_SOFT_FE_LENGTH+1);
			}
		unConfigSOFT_FE_checkData(dsPLCLine, exceptionInfo);
		unConfigSOFT_FE_checkAdditionalData(dsPLCAdditionalData, exceptionInfo);

// first check if the PLC name is correct:
		tempDp = dsPLCLine[UN_CONFIG_PLC_NAME];
		if(!unConfigGenericFunctions_nameCheck(tempDp)) {
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADPLCNAME"),"");
			return;
		}

// check if the PLC Application Name is correct:
		tempDp = dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION];
		if(!unConfigGenericFunctions_nameCheck(tempDp)) {
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
			return;
		}

// sPLCDp is SOFT_FE_plcName
		sPLCDp = getSystemName() +  c_unSystemIntegrity_SOFT_FE + dsPLCLine[UN_CONFIG_PLC_NAME];
//		sAliasDp = dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
		sAliasDp = unGenericDpFunctions_dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
//DebugN(sAliasDp, dsPLCLine[UN_CONFIG_PLC_NAME]);

		if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".")
		{
			sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);
		}
		if ((sAliasDp != "") && (sAliasDp != sPLCDp))
		{
			if (dynContains(dsDeleteDps, sAliasDp) <= 0)		// Alias exists, is not used by the "SOFT_FE" config datapoint and will not be deleted
			{
				fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADHOSTNAMEALIAS") + sAliasDp,"");
			}
		}
		if (dpExists(sPLCDp) && (dynContains(dsDeleteDps, sPLCDp) <= 0))
		{
			sHostname = unGenericDpFunctions_getAlias(sPLCDp);
			iRes = dpGet(sPLCDp + ".communication.driver_num", iDriverNum);
			if (iRes >= 0)
			{
				if (iDriverNum != dsPLCAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM])
				{
					fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCDRIVER")+iDriverNum,"");
				}
				if (sHostname != dsPLCLine[UN_CONFIG_PLC_NAME])
				{
					fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCHOSTNAME"),"");
				}
			}
		}
	}
	else
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkApplication: " + getCatStr("unGeneration","BADINPUTS"),"");
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigSOFT_FE_checkData
/**
Purpose: check PLC data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, name
			3. string, subApplication
			4. int, PLC address = PLC number
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_checkData(dyn_string dsPLC, dyn_string &exceptionInfo)
{
	unsigned uPLCNumber;
	
	if (dynlen(dsPLC) != UN_CONFIG_SOFT_FE_LENGTH)
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkData:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
	else
	{
		unConfigGenericFunctions_checkPLCType(dsPLC[UN_CONFIG_PLC_TYPE], exceptionInfo);

		if (strrtrim(dsPLC[UN_CONFIG_PLC_NAME]) == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkData:" + getCatStr("unGeneration","ERRORPLCNAME"),"");
		}
		
		if (strrtrim(dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]) == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkData:" + getCatStr("unGeneration","ERRORSUBAPPLI"),"");
		}			
		// check systemIntegrity
		if((dsPLC[UN_CONFIG_SOFT_FE_SYSTEMINTEGRITY]!="Y") && (dsPLC[UN_CONFIG_SOFT_FE_SYSTEMINTEGRITY]!="N"))
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkData:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigSOFT_FE_checkAdditionalData
/**
Purpose: check additional data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC additional data
			dsPLC must contains :
			1. unsigned, driver number
			2. string, boolean archive
			3. string, analog archive
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_checkAdditionalData(dyn_string dsPLC, dyn_string &exceptionInfo)
{
	if (dynlen(dsPLC) != UN_CONFIG_PLC_ADDITIONAL_LENGTH)
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkAdditionalData:" + getCatStr("unGeneration","BADINPUTS"),"");
	}
	else
	{
		if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkAdditionalData: bool archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
		}
		if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkAdditionalData: analog archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
		}
		if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "")
		{
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_checkAdditionalData: event archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigSOFT_FE_setApplication
/**
Purpose: set PLC configuration for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file + additional parameters
			dsPLC must contains :
			(Plc config line)
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number 
			6. boolean archive name
			7. analog archive name
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. First line of a generation file must contain the PLC description. Fields are defined in constants at this library beginning
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_setApplication(dyn_string dsPLCData, dyn_string &exceptionInfo)
{
	int iPLCNumber, iRes, i, position, iDriverNumber;
	string sDpName, addressReference, tempDp;
	dyn_string dsPLC, dsAdditionalParameters, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
	dyn_string dsEventArchives, dsSplit;
	bool bSystemIntegrity;
	
	// Check
	
	if (dynlen(dsPLCData) == (UN_CONFIG_SOFT_FE_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
	{
	// 1. Separate config file info and additional parameters
		dsPLC = dsPLCData;
		dynClear(dsAdditionalParameters);
		for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
		{
			dynAppend(dsAdditionalParameters, dsPLC[UN_CONFIG_SOFT_FE_LENGTH+1]);
			dynRemove(dsPLC,UN_CONFIG_SOFT_FE_LENGTH+1);
		}
	
	// check if the PLC Application Name is correct:
		tempDp = dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION];
		if(!unConfigGenericFunctions_nameCheck(tempDp)) {
			fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_setApplication: " + dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
			return;
		}
	
	// 3. Set PLC configuration in SOFT_FE
		sDpName = c_unSystemIntegrity_SOFT_FE + dsPLC[UN_CONFIG_PLC_NAME];
		tempDp = sDpName;
		unConfigGenericFunctions_createDp(sDpName, SOFT_FE_DPTYPE, exceptionInfoTemp);

		if (dynlen(exceptionInfoTemp) <= 0)
		{
			sDpName = getSystemName() + sDpName;
			unConfigGenericFunctions_setAlias(sDpName, dsPLC[UN_CONFIG_PLC_NAME], exceptionInfo);
			iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
						 sDpName + ".configuration.importTime", dsImportTimes,
						 sDpName + ".configuration.archive_bool", dsBoolArchives,
						 sDpName + ".configuration.archive_analog", dsAnalogArchives,
						 sDpName + ".configuration.archive_event", dsEventArchives);
			position = dynContains(dsSubApplications, dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]);
			if (position <= 0)
			{
				position = dynlen(dsSubApplications) + 1;
			}
// if the event archive is empty, set it to bool archive
			if(dynlen(dsEventArchives) <= 0)
				dsEventArchives = dsBoolArchives;
				
			dsSubApplications[position] = dsPLC[UN_CONFIG_PLC_SUBAPPLICATION];
			dsImportTimes[position] = (string)getCurrentTime();
			dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
			dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
			dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];
			if(dsPLC[UN_CONFIG_SOFT_FE_SYSTEMINTEGRITY] == "Y")
				bSystemIntegrity = true;
			iRes = dpSetWait(sDpName + ".configuration.importTime", dsImportTimes,
							sDpName + ".configuration.archive_bool", dsBoolArchives,
							sDpName + ".configuration.archive_analog", dsAnalogArchives,
							sDpName + ".configuration.archive_event", dsEventArchives, 
							sDpName + ".configuration.subApplications",dsSubApplications,
							sDpName + ".configuration.systemIntegrityAllowed", bSystemIntegrity,
							sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]);
			if (iRes < 0)
			{
				fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_setApplication: " + getCatStr("unPVSS","DPSETFAILED"),"");
			}	

			// 4. Create _UnSystemAlarm datapoints
			unSystemIntegrity_createSystemAlarm(tempDp,
																					DS_pattern,
																					getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
																					exceptionInfo);
			fwArchive_set(c_unSystemAlarm_dpPattern+DS_pattern+tempDp+".alarm",
						  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
						  DPATTR_ARCH_PROC_SIMPLESM,
						  DPATTR_COMPARE_OLD_NEW,
						  0,
						  0,
						  exceptionInfo);

	// add archive on Diagnostic data
			fwArchive_set(sDpName+".Diagnostic.userData",
						  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG],
						  DPATTR_ARCH_PROC_SIMPLESM,
						  DPATTR_COMPARE_OLD_NEW,
						  0,
						  0,
						  exceptionInfo);
			fwArchive_set(sDpName+".Diagnostic.float1",
						  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG],
						  DPATTR_ARCH_PROC_SIMPLESM,
						  DPATTR_COMPARE_OLD_NEW,
						  0,
						  0,
						  exceptionInfo);
			fwArchive_set(sDpName+".Diagnostic.float2",
						  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG],
						  DPATTR_ARCH_PROC_SIMPLESM,
						  DPATTR_COMPARE_OLD_NEW,
						  0,
						  0,
						  exceptionInfo);
			fwArchive_set(sDpName+".Diagnostic.float3",
						  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG],
						  DPATTR_ARCH_PROC_SIMPLESM,
						  DPATTR_COMPARE_OLD_NEW,
						  0,
						  0,
						  exceptionInfo);
			fwArchive_set(sDpName+".Diagnostic.float4",
						  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG],
						  DPATTR_ARCH_PROC_SIMPLESM,
						  DPATTR_COMPARE_OLD_NEW,
						  0,
						  0,
						  exceptionInfo);
			fwArchive_set(sDpName+".Diagnostic.float5",
						  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG],
						  DPATTR_ARCH_PROC_SIMPLESM,
						  DPATTR_COMPARE_OLD_NEW,
						  0,
						  0,
						  exceptionInfo);
		}
		else
		{
			dynAppend(exceptionInfo, exceptionInfoTemp);
		}
	}
	else
	{
		fwException_raise(exceptionInfo,"ERROR","unConfigSOFT_FE_setApplication: " + getCatStr("unGeneration","BADINPUTS"),"");
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unConfigSOFT_FE_deleteFrontEnd
/**
Purpose: delete Front-end config

Parameters : 
	sPlcName, string, input, plc name
	exceptionInfo, dyn_string, output, for errors
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigSOFT_FE_deleteFrontEnd(string sPlcName, dyn_string& exceptionInfo)
{
	string sDpPlcName, sPlcModPlc="", exceptionText;
	int i, length, iRes;
	dyn_string dsAlarmDps;
	

	sDpPlcName = c_unSystemIntegrity_SOFT_FE+sPlcName;
	if (!dpExists(sDpPlcName))
		sDpPlcName = "";

	if (sDpPlcName != "")
	{
		if (unGenericDpFunctions_getSystemName(sDpPlcName) == getSystemName())
		{
	// 2. Delete _Un_Plc
			iRes = dpDelete(substr(sDpPlcName, strpos(sDpPlcName, ":") + 1));
			if (iRes < 0)
			{
				fwException_raise(exceptionInfo, "ERROR", "unConfigSOFT_FE_deleteFrontEnd: " + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
			}
	// 3. Delete alarm system 
			dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1),c_unSystemAlarm_dpType);
			length = dynlen(dsAlarmDps);
			for(i=1;i<=length;i++)
			{
				_unSystemIntegrity_modifyApplicationAlertList(dsAlarmDps[i], false, exceptionInfo);
				iRes = dpDelete(dsAlarmDps[i]);
				if (iRes < 0)
				{
					fwException_raise(exceptionInfo, "ERROR", "unConfigSOFT_FE_deleteFrontEnd: " + dsAlarmDps[i] + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
				}
			}
			
		}
		else
		{
			fwException_raise(exceptionInfo, "ERROR", "unConfigSOFT_FE_deleteFrontEnd: " + sDpPlcName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_Com_ExportConfig
/**
Purpose: Export the SOFT_FE front-end config 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
SOFT_FE_Com_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo)
{
	string sPlcName, sPlcDrv;
	string sDp, sSystemIntegrityAllowed;
	dyn_string dsDpParameters, dsBoolArchive, dsAnalogArchive, dsEventArchive, dsAddDpParameters;
	bool bSystemIntegrityAllowed;
	
	// DebugN("FUNCTION: SOFT_FE_Com_ExportConfig(dsParam= "+dsParam);
	
	if (dynlen(dsParam)==UN_CONFIG_EXPORT_LENGTH)
	{
		sPlcName = dsParam[UN_CONFIG_EXPORT_PLCNAME];
		sDp = c_unSystemIntegrity_SOFT_FE + sPlcName;
		dpGet(sDp + ".configuration.archive_bool", dsBoolArchive,
									sDp + ".configuration.archive_analog", dsAnalogArchive,
									sDp + ".configuration.archive_event", dsEventArchive,
									sDp + ".communication.driver_num", sPlcDrv,
									sDp + ".configuration.systemIntegrityAllowed", bSystemIntegrityAllowed);
		TextFieldDriver.text = sPlcDrv;
	
		if (dynlen(dsBoolArchive)>0 && dynlen(dsAnalogArchive)>0 && dynlen(dsEventArchive)>0)
		{
			TextFieldBoolArchive.text = dsBoolArchive[1];
			TextFieldAnalogArchive.text = dsAnalogArchive[1];
			TextFieldEventArchive.text = dsEventArchive[1];
		}

		//Delete
		dynClear(dsDpParameters);
		dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
		unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
	
		//PLCCONFIG
		dynClear(dsDpParameters);
		dynAppend(dsDpParameters, UN_PLC_COMMAND);
		dynAppend(dsDpParameters, "SOFT_FE");
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);

		if(bSystemIntegrityAllowed)
			sSystemIntegrityAllowed = "Y";
		else
			sSystemIntegrityAllowed = "N";
		dynAppend(dsDpParameters, sSystemIntegrityAllowed);

// write to file
		unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
	}
	else
		fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_Com_ExportConfig(): Wrong Parameters", "");		
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_getFrontEndArchiveDp
/**
Purpose: return the list of Dps that wil be archived during the set of the SOFT_FE front-end

Parameters:
		sCommand: string, input, the current entry in the import file
		sDp: string, input, the given dp: PLC or SystemAlarm  Dp
		sFEType: string, input, the front-end type
		dsArchiveDp: dyn_string, output, list of archivedDp

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
SOFT_FE_getFrontEndArchiveDp(string sCommand, string sDp, string sFEType, dyn_string &dsArchiveDp)
{
	switch(sCommand) {
		case UN_PLC_COMMAND:
			dsArchiveDp = makeDynString(getSystemName()+c_unSystemAlarm_dpPattern+DS_pattern+sDp+".alarm");
			break;
		default:
			dsArchiveDp = makeDynString();
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------
#uses "unSOFT_FE.ctl"

//@}
