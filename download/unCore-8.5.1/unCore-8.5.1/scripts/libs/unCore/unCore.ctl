// get the dependencies on JCOP Fw components
//#uses "fwCore/fwCore.ctl" // loaded globally
//#uses "fwAccessControl/fwAccessControl.ctc" //loaded globally
#uses "unDistributedControl/unDistributedControl.ctl"
#uses "fwTrending/fwTrending.ctl"


// UNICOS core libs
#uses "unDB.ctl"
#uses "unBackup.ctl"

//UNICOS import/export
#uses "unImportDevice.ctl"
#uses "unExportDevice.ctl"

//UNICOS devices generic functions
#uses "unGenericObject.ctl"
#uses "unGenericDpFunctions.ctl"

//UNICOS constant and global variables
#uses "unicos_declarations.ctl"

//Generic utilities lib
#uses "libunCore/unGenericUtilities.ctl"

//Graphical utilities
#uses "unGraphicalFrame.ctl"
#uses "unGenericButtonFunctionsHMI.ctl"

//Application configuration lib
#uses "libunCore/unApplicationConfiguration.ctl"

//UNICOS devices generic and unPLC functions
#uses "unConfigGenericFunctions.ctl"

//UNICOS PLC S7 libs
#uses "S7Constant_declarations.ctl"
