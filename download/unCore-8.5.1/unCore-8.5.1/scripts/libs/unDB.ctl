/**@name LIBRARY: unDB.ctl

@authors: Frederic BERNARD AB-CO

Creation Date: 19/04/2007
Modification History:

External Functions: 
	- unDB_open_LSA_Connection:  to connect to the LSA database
	- unDB_run_Command: prepare the SQL statement on the database and execute it
	- unDB_close_Connection: close the database connection

Internal Functions:
	- unDB_open_Connection: to connect to the database

Purpose: 
This library contain generic functions to access Oracle DataBases and to run command

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
		. PVSS version: 3.1 
		. CtrlRDBAccess patch must be installed
		. Oracle Instant Client CMF package must be installed
		. operating system: tested only under XP
*/

// ------------------------------------------------------------------------------------------------------------------------------------
//@{

// unDB_run_Command
/**
Purpose:	prepare the SQL statement on the database and execute it 

Usage: 		Public

PVSS manager usage: UI and CTRL

Parameter:
			string sQuery, input, database query (e.g.: "SELECT soc_name FROM hwc_socs ORDER BY soc_name")
			dbConnection connDatabase, input, "handle" to your database connection
			bool bRowwise, input, specifies if the data should be returned row-wise or column-wise
			dyn_dyn_mixed data, output, contain the rows and column of the data (cf. bRowwise)
			int iRes, output, -1 if parameters are incorrect /	0 on success

*/

int unDB_run_Command(string sQuery, dbConnection connDatabase, bool bRowwise, dyn_dyn_mixed &data)
{
dbCommand cmd;
string errTxt;
int iRes;

	dynClear(data);
		
   rdbStartCommand(connDatabase, sQuery, cmd);
    if (rdbCheckError(errTxt, cmd))
    	{
    	DebugN("unDB_run_Command: DB ERROR rdbStartCommand", errTxt);
    	iRes=-1;
    	}
		else
			{
	    rdbExecuteCommand(cmd);
	    if (rdbCheckError(errTxt, cmd))
	    	{
	    	DebugN("unDB_run_Command: DB ERROR rdbExecuteCommand", errTxt);
	    	iRes=-1;
	    	}
			else
				{
	      //the data is extracted column-wise
	      rdbGetData(cmd, data, bRowwise, 0, makeDynInt(STRING_VAR));
	      if (rdbCheckError(errTxt,cmd))
	      	{
	      	DebugN("unDB_run_Command: DB ERROR rdbGetData", errTxt);
   	    	iRes=-1;
	      	}
	      else
		     	iRes=0;
	      			  
		    rdbFinishCommand(cmd);
				}
			}

return iRes;
}

// ------------------------------------------------------------------------------------------------------------------------------------

// unDB_open_Connection
/**
Purpose:	To connect to the database, and check that the actual connection was succesfull

Usage: 		Public

PVSS manager usage: UI and CTRL

Parameters:
			string sDriver, const input, database driver used
			string sDB, input, connect string
			dbConnection connDatabase, output, "handle" to your database connection
			int iRes, output, -1 if parameters are incorrect /	0 on success

*/

int unDB_open_Connection(const string sDriver, string sDB, dbConnection &connDatabase)
{
int iRes;
string errTxt;
mixed dbDrivers;

	if (!isFunctionDefined("rdbOption"))
		{
    DebugN("unDB_open_Connection: ERROR CtrlRDBAccess library is not available!");
    iRes=-1;
    }
  else
  	{ 	
	rdbOption("GetDrivers", 0, dbDrivers);
  	if(dynContains(dbDrivers, sDriver))
  		{	
			rdbOption("Reset",0); // reset the connection manager - terminate all connections that are still present
	 	 	rdbOpenConnection(sDB, connDatabase);
	    if (rdbCheckError(errTxt, connDatabase))
	    	{
	    	DebugN("unDB_open_Connection: ERROR WHILE OPENING DATABASE: " + sDB, errTxt);
				iRes=-1;
	    	}
			else
				{
				DebugN("unDB_open_Connection: Connecting to the database OK");
		    iRes=0;
				}
  		}
  	else
  		{
			DebugN("unDB_open_Connection: ERROR DATABASE DRIVER NOT FOUND: " + sDriver);  		
      iRes=-1;
  		}
  	}

return iRes;
}

// ------------------------------------------------------------------------------------------------------------------------------------

// unDB_close_Connection
/**
Purpose:	close the database connection

Usage: 		Public

PVSS manager usage: UI and CTRL

Parameters:
			dbConnection connDatabase, input, "handle" to your database connection
			int iRes, output, -1 if parameters are incorrect /	0 on success

*/

int unDB_close_Connection(dbConnection connDatabase)
{
int iRes;

	iRes=rdbCloseConnection(connDatabase);
	return iRes;
}

//--------------------------------------------------------------------------------------------

// UNCORE-32 we do not distribute this file anymore
//#uses "unDB_util.ctc"

//--------------------------------------------------------------------------------------------

//@}
