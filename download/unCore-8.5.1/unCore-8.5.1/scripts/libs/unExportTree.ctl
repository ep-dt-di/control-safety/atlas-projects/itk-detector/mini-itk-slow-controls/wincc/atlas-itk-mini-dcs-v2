/**@name LIBRARY: unExportTree.ctl

@author: Daniel Davids and Herve Milcent (EN/ICE)

Creation Date: 03/09/2010

Modification History:

  22/11/2013: Danny
    - IS-685: Using synchronized function unExportTree_uniqueTreeTagNames() to load read-only variables
    - IS-686: Using synchronized function unExportTree_uniqueGetAccess() already used in previous release
    
  01/02/2011: Danny
    - IS-422: Modifications to use the new Trending Xml Structure
    
  01/11/2010: Danny
    - IS-297: Modifications to use PVSS' XML libraries

  25/10/2010: Herve
    - IS-421: Import/Export TrendTree/WindowTree: replace if required the local system name by #local in the DPE description
    modified function: unExportTree_FwTrendingPlot_exportDeviceData

  30/10/2008: Herve
    - Export FwTrendingPage: add export the FwTrendingPlot
    
  19/09/2008: Herve
    - missing marker type in export
    
  05/09/2006: Herve
    - bug in case of a treeNode with dsUserData[1] = ""

  29/06/2006: Herve
    add doNotTreeDef option:
    - case Tree selected --> enabled
    - case node selected disabled
			
  31/10/2006: Herve
    - remove messages call
    - do not replace #local in plot
    - g_stop global variable of the panel encapsulated by  if(isFunctionDefined("PanelOff"))
    - do not use the global variable of the panel: g_dsExportTree_exportedDevice, g_dsExportTree_navigationDevice

version 1#

External Function :
        
Internal Functions:
        
Purpose:
This library contains the function used to export the tree and tree node linked to FwTrendingPlot, FwTrendingPage and _UnPanel devices

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . UNICOS-JCOP framework
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.
*/

#uses "unPanel.ctl"
#uses "unImportTree.ctl"
#uses "fwTrending/fwTrending.ctl"

// Constants
// global variable 

//@{


//------------------------------------------------------------------------------------------------------------------------


//_unExportTree_checkEightElements
/**
Purpose: 
Request for unique access to export a tree.
 	
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

  @param table: dyn_dyn_string, input/output, trend plot database
  @param entry: int, input, index in the trend plot database
  @param value: string, input, value for the trend plot database
  @param incre: bool, input, needing incremental data for the value
*/
void _unExportTree_checkEightElements ( dyn_dyn_string & table , int entry , string value , bool incre = FALSE )
{
int len = dynlen(table[entry]);

  if ( len != 8 )
  {
    if ( len < 8 ) {
      for ( int i = len ; i < 8 ; ++i ) {
        if ( ! incre ) dynAppend ( table[entry] , value );
        else {
          string datas;
            sprintf(datas,value,i+1);
            dynAppend ( table[entry] , datas );
        }
      }
    }
    else {
      for ( int i = len ; i > 8 ; --i ) dynRemove ( table[entry] , i );
    }
  }
} 
  
  
//------------------------------------------------------------------------------------------------------------------------


//unExportTree_uniqueTreeTagNames
/**
Purpose: 
Request for unique global variables that contain the Tree TagNames.
 	
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

*/
synchronized void unExportTree_uniqueTreeTagNames()
{
  // bool fwTrending_isPageTreeTagExportable(int tagIndex)
  if ( ! globalExists("g_dsExportTree_pageTreeTagNames") )
  { 
    addGlobal("g_dsExportTree_pageTreeTagNames", DYN_STRING_VAR);
    g_dsExportTree_pageTreeTagNames = fwTrending_getPageTreeTagNames();
    for ( int idx = 1 ; idx <= dynlen(g_dsExportTree_pageTreeTagNames) ; ++idx )
        g_dsExportTree_pageTreeTagNames[idx] = substr(g_dsExportTree_pageTreeTagNames[idx],1);
    // DebugN(g_dsExportTree_pageTreeTagNames);
  }
  // bool fwTrending_isPlotTreeTagExportable(int tagIndex)
  if ( ! globalExists("g_dsExportTree_plotTreeTagNames") )
  { 
    addGlobal("g_dsExportTree_plotTreeTagNames", DYN_STRING_VAR);
    g_dsExportTree_plotTreeTagNames = fwTrending_getPlotTreeTagNames();
    for ( int idx = 1 ; idx <= dynlen(g_dsExportTree_plotTreeTagNames) ; ++idx )
        g_dsExportTree_plotTreeTagNames[idx] = substr(g_dsExportTree_plotTreeTagNames[idx],1);
    // DebugN(g_dsExportTree_plotTreeTagNames);
  }
  
  if ( ! globalExists("g_dsImportTree_pageTreeDataSize") )
  { 
    addGlobal("g_dsImportTree_pageTreeDataSize", INT_VAR);
    g_dsImportTree_pageTreeDataSize = -1;
  }
  if ( ! globalExists("g_dsImportTree_plotTreeDataSize") )
  { 
    addGlobal("g_dsImportTree_plotTreeDataSize", INT_VAR);
    g_dsImportTree_plotTreeDataSize = -1;
  }
}   
    
   
//------------------------------------------------------------------------------------------------------------------------


//unExportTree_uniqueGetAccess
/**
Purpose: 
Request for unique access to export a tree.
 	
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

  @param access: bool, input, request unique access
*/
synchronized bool unExportTree_uniqueGetAccess(bool reqaccess)
{
  if ( ! globalExists("g_bExportTree_uniqueAccess") ) {
    
    addGlobal("g_bExportTree_uniqueAccess", BOOL_VAR);
    
    g_bExportTree_uniqueAccess = FALSE;
            
    addGlobal("g_bExportTree_stopExporting", BOOL_VAR);
    addGlobal("g_dsExportTree_exportedDevice", DYN_STRING_VAR);
    addGlobal("g_dsExportTree_navigationDevice", DYN_STRING_VAR); 
  }

  if ( reqaccess == FALSE )
  {
    g_bExportTree_uniqueAccess = FALSE;
    
    return FALSE;
  }
  
  if ( g_bExportTree_uniqueAccess == TRUE )
  {
    return FALSE;
  }
  
  g_bExportTree_uniqueAccess = TRUE;
  
  g_bExportTree_stopExporting = FALSE;
  g_dsExportTree_exportedDevice = makeDynString();
  g_dsExportTree_navigationDevice = makeDynString();
  
  return TRUE;
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_indentXmlFile
/**
Purpose: 
Replaces leading spaces into tabulators.
 	
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

  @param sFileName: string, input, the original filename
*/
void unExportTree_indentXmlFile(string sFileName, string iFileName)
{
string temporary;
file fi, fo;
int offset;
bool outside = TRUE;

  fi = fopen(iFileName,"r");
  fo = fopen(sFileName,"w");
  // fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n",fo);
  while ( feof(fi) == 0 )
  {
    fgets ( temporary , 1000000 , fi );
    offset = strpos(temporary,"<");
    if ( offset >= 0 )
    {
      if ( ( ! outside ) && ( substr(temporary,offset+1,1) == "/" ) ) { offset = -1; }
                            
      for ( int i = 0 ; i < offset ; ++i ) { if ( substr(temporary,i,1) != " " ) offset = -1; }
                                    
      if ( offset >= 0 )
      {
        temporary = substr(temporary,offset);
        for ( int i = 0 ; i < offset ; ++i ) temporary = "\t" + temporary;
      }
    }
    fputs(temporary,fo);
    outside = ( substr(temporary,strlen(temporary)-2,1) == ">" );
  }
  fclose(fo);
  fclose(fi);
}

                        
//------------------------------------------------------------------------------------------------------------------------           
                        
                        
//unExportTree_exportTreeDataXml
/**
Purpose: 
Export the hierarchy and the tree configuration.
 	
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier
  @param dsTreeData: dyn_string, output, the exported data
  @param dpTree: string, input, the data point name of the tree
  @param sNodeName: string, input, the node nameof the tree
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param noSilent: bool, input, flag to indicate if messages are printed to the log-screen
  @param exInfo: dyn_string, output, exception - returns a warning if node does not exist
*/
unExportTree_exportTreeDataXml(int xmlDoc, int xmlTop, string dpTree, string sNodeName, bool bKeepLocalSystem, string sSystemNameToReplace, bool noSilent, dyn_string &exInfo)
{
string sTreeName, root, nodeTypes, nodeNames, sConf;
int refresh, i, len;
dyn_string dsMode;
string sXMLName;
        
int subchild, child;
        
  unExportTree_uniqueTreeTagNames();

  if(dpTree != "") {
    // get all info on tree
    dpGet(dpTree+".name", sTreeName, dpTree+".root", root, dpTree+".nodeTypes", nodeTypes, dpTree+".nodeNames", nodeNames,
    dpTree+".refresh", refresh, dpTree+".modes", dsMode);
    // if the root is the current system name replace it by #local
    if(root == sSystemNameToReplace)
      root = "#local:";
  }
  sXMLName = sTreeName;

  child = xmlAppendChild(xmlDoc,xmlTop,XML_ELEMENT_NODE,"name");
  child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,sXMLName);
        
  child = xmlAppendChild(xmlDoc,xmlTop,XML_ELEMENT_NODE,"treeparameters");
  child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,
                         root+UN_PARAMETER_DELIMITER+nodeTypes+UN_PARAMETER_DELIMITER+
                         nodeNames+UN_PARAMETER_DELIMITER+refresh+UN_PARAMETER_DELIMITER);
        
  len = dynlen(dsMode);
  for(i=1;i<=len;i++) {
    if(sConf == "") 
      sConf = dsMode[i];
    else
      sConf = sConf+UN_PARAMETER_DELIMITER+dsMode[i];
  }
  if(sConf != "")
    sConf +=UN_PARAMETER_DELIMITER;
  child = xmlAppendChild(xmlDoc,xmlTop,XML_ELEMENT_NODE,"treemodes");
  child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,sConf);

  if ( noSilent ) {
    messages.appendItem("Exporting from top-level tree node data: " + sTreeName);
    messages.bottomPos(messages.itemCount);
  }
  unExportTree_exportSubTreeDataXml(xmlDoc, xmlTop, sNodeName, bKeepLocalSystem, sSystemNameToReplace, noSilent, exInfo);

  subchild = xmlAppendChild(xmlDoc,xmlTop,XML_ELEMENT_NODE,"otherdevice");
        
  if ( noSilent ) {
    messages.appendItem("Exporting the other missing devices data... ");
    messages.bottomPos(messages.itemCount);
  }
  unExportTree_exportMissingDeviceXml(xmlDoc, subchild, bKeepLocalSystem, sSystemNameToReplace, noSilent);
        
  if ( xmlFirstChild(xmlDoc,subchild) < 0 )
  {
    xmlRemoveNode(xmlDoc, subchild);
  }
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_exportSubTreeDataXml
/**
Purpose: 
Export the hierarchy and the configuration of a sub tree.
 	
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier
  @param dsTreeData: dyn_string, output, the exported data
  @param sNodeName: string, input, the node nameof the tree
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param noSilent: bool, input, flag to indicate if messages are printed to the log-screen
  @param exInfo: dyn_string, output, exception - returns a warning if node does not exist
*/
unExportTree_exportSubTreeDataXml(int xmlDoc, int xmlTop, string sNodeName, bool bKeepLocalSystem, string sSystemNameToReplace, bool noSilent, dyn_string &exInfo)
{
	dyn_string childrenNodes;
	int len, i;
	
  if ( g_bExportTree_stopExporting ) return;
	
  // get tree node data
	unExportTree__FwTreeNode_exportDeviceDataXml(_fwTree_makeNodeName(sNodeName), bKeepLocalSystem, sSystemNameToReplace, xmlDoc, xmlTop);
        
  if ( noSilent ) {
	   messages.appendItem("Exporting tree node data: " + sNodeName);
	   messages.bottomPos(messages.itemCount);
  }

  // get all children with clipboard
	fwTree_getChildrenWithClipboard(sNodeName, childrenNodes, exInfo);
        
	len = dynlen(childrenNodes);
	for ( i = 1 ; i <= len ; i++ ) {
		unExportTree_exportSubTreeDataXml(xmlDoc, xmlTop, childrenNodes[i], bKeepLocalSystem, sSystemNameToReplace, noSilent, exInfo);
	}
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_exportMissingDeviceXml
/**
Purpose: 
Export the device that are used as horizontal navigation link and not yet exported.
 	
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier
  @param dsTreeData: dyn_string, output, the exported data
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param noSilent: bool, input, flag to indicate if messages are printed to the log-screen
*/
unExportTree_exportMissingDeviceXml(int xmlDoc, int xmlTop, bool bKeepLocalSystem, string sSystemNameToReplace, bool noSilent)
{
int i, j, pos, devices, missing;
dyn_string dsNavigationDevice, dsExportedDevice;
string sFunction;
	
  if ( g_bExportTree_stopExporting ) return;
	
  dsExportedDevice = g_dsExportTree_exportedDevice;
  dsNavigationDevice = g_dsExportTree_navigationDevice;
  g_dsExportTree_navigationDevice = makeDynString();
	
  // export the devices that are not in the list of exported devices (already exported)
  devices = dynlen(dsExportedDevice);
  // DebugN("unExportTree_exportMissingDeviceXml - "+devices+" Entries");


	for ( i = 1 ; i <= devices ; i++ ) {
    pos = dynContains(dsNavigationDevice, dsExportedDevice[i]);
    if ( pos > 0 ) dynRemove(dsNavigationDevice, pos);
  }
        
  if ( ( missing = dynlen(dsNavigationDevice) ) == 0 ) return;
        
  for ( i = 1 ; i <= missing ; i++ ) {
    
    if ( g_bExportTree_stopExporting ) return;
    
    // call the device function if it exists
    if(dpExists(dsNavigationDevice[i])) {
    sFunction = "unExportTree_"+dpTypeName(dsNavigationDevice[i])+"_exportDeviceDataXml";
    if(isFunctionDefined(sFunction)) {
       if ( noSilent ) {
	        messages.appendItem("Exporting tree node data: " + dsNavigationDevice[i]);
	        messages.bottomPos(messages.itemCount);
       }
				 execScript(
         "void main(int xmlDoc, int xmlTop, string sDeviceName, bool bKeepLocalSystem, string sSystemNameToReplace)" + 
         "{"+
         "  "+sFunction+"(sDeviceName, bKeepLocalSystem, sSystemNameToReplace, xmlDoc, xmlTop);"+
         "}",
					  makeDynString(), xmlDoc, xmlTop, dsNavigationDevice[i], bKeepLocalSystem, sSystemNameToReplace);
			}
		}
	}
                
  // call recursively the funtion until there are no more devices to export. 
	unExportTree_exportMissingDeviceXml(xmlDoc , xmlTop, bKeepLocalSystem, sSystemNameToReplace, noSilent);
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree__FwTreeNode_exportDeviceDataXml
/**
Purpose: 
Export the device data of _FwTreeNode device

Parameters:
  @param sDpNodeName: string, input, the dp node name
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
*/
unExportTree__FwTreeNode_exportDeviceDataXml(string sDpNodeName, bool bKeepLocalSystem, string sSystemNameToReplace, int xmlDoc, int xmlTop)
{
string sNodeName = _fwTree_getNodeName(sDpNodeName);
int len, i, iNodeCU;
string sParentNodeName, sDeviceName, sDeviceType, sConf, sXML;
string sFunction;
dyn_string childrenNodes, exInfo, dsUserData, dsSplit;
dyn_dyn_string ddsData;
int topnode, subchild, child;

  if(dpExists(sDpNodeName)) {
    if(dpTypeName(sDpNodeName) == unTreeWidget_treeNodeDPT) {
                  
      topnode = xmlAppendChild(xmlDoc,xmlTop,XML_ELEMENT_NODE,unTreeWidget_treeNodeDPT);
                        
      // add the current device to the list of exported device
      unExportTree_addDeviceToExportedDeviceListXml(sDpNodeName);
                        
      // get all the data of the current node
      fwTree_getParent(sNodeName, sParentNodeName, exInfo);
      fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
      fwTree_getNodeUserData(sNodeName, dsUserData, exInfo);
      fwTree_getNodeCU(sNodeName, iNodeCU, exInfo);
      // get all children with clipboard
      fwTree_getChildrenWithClipboard(sNodeName, childrenNodes, exInfo);
      sXML = sNodeName;
      child = xmlAppendChild(xmlDoc,topnode,XML_ELEMENT_NODE,"name");
      child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,sXML);
		
      sXML = sParentNodeName;
      child = xmlAppendChild(xmlDoc,topnode,XML_ELEMENT_NODE,"parentname");
      child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,sXML);

      len=dynlen(childrenNodes);
      for(i=1;i<=len;i++) {
        sXML = childrenNodes[i];
        if(sConf == "") 
          sConf = sXML;
        else
        sConf = sConf+UN_PARAMETER_DELIMITER+sXML;
      }
      if(sConf != "") sConf +=UN_PARAMETER_DELIMITER;
      child = xmlAppendChild(xmlDoc,topnode,XML_ELEMENT_NODE,"children");
      child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,sConf);
                                                
      sConf = sDeviceName;
      // if the system name is present and equals to the current system, replace it by the key #local:
      if(unExportTree_getSystemName(sConf) == sSystemNameToReplace)
        sConf = "#local:"+unExportTree_removeSystemName(sDeviceName);
      child = xmlAppendChild(xmlDoc,topnode,XML_ELEMENT_NODE,"devicedata");
      child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,
      sConf+UN_PARAMETER_DELIMITER+sDeviceType+UN_PARAMETER_DELIMITER+iNodeCU+UN_PARAMETER_DELIMITER);
                        
      len = dynlen ( dsUserData );
      // if more than 1 entry in dsUserData --> assume panel navigation
      sConf = "";
      for(i=1;i<=len;i++) {
        // add the device to the list to export
        if(i>1) {
          dsSplit = strsplit(dsUserData[i], c_panel_navigation_delimiter);
          while(dynlen(dsSplit) <3)
            dynAppend(dsSplit, ""); 
          if(dsSplit[2] != "") {
            unExportTree_addDeviceToNavigationDeviceListXml(dsSplit[2]);
            if(unExportTree_getSystemName(dsSplit[2]) == sSystemNameToReplace)
              dsSplit[2] = "#local:"+unExportTree_removeSystemName(dsSplit[2]);
          }
          dsUserData[i]=dsSplit[1]+c_panel_navigation_delimiter+dsSplit[2]+c_panel_navigation_delimiter+dsSplit[3];
        }
        // if empty force it to * in order to have an entry
        if(dsUserData[i] == "") dsUserData[i] = "*";
        if(sConf == "") sConf = dsUserData[i];
        else sConf = sConf+"~"+dsUserData[i];
      }
      if(sConf != "") sConf +="~";
      // if * then replace it by ""
      strreplace(sConf, "*", "");
     
      child = xmlAppendChild(xmlDoc,topnode,XML_ELEMENT_NODE,"userdata");
      child = xmlAppendChild(xmlDoc,child,XML_TEXT_NODE,sConf);
                 
      subchild = xmlAppendChild(xmlDoc,topnode,XML_ELEMENT_NODE,"deviceconfiguration");
                        
      if ( sDeviceType != "" ) {
        // call the device function to export the device content
        sFunction = "unExportTree_"+sDeviceType+"_exportDeviceDataXml";
        if(isFunctionDefined(sFunction)) {
          execScript( 
            "void main(int xmlDoc, int xmlTop, string sDeviceName, bool bKeepLocalSystem, string sSystemNameToReplace)" + 
            "{"+
            "  "+sFunction+"(sDeviceName, bKeepLocalSystem, sSystemNameToReplace, xmlDoc, xmlTop);"+
            "}",
            makeDynString(), xmlDoc, subchild, sDeviceName, bKeepLocalSystem, sSystemNameToReplace);
        }
      }
                                   
      if ( xmlFirstChild(xmlDoc,subchild) < 0 )
      {
        xmlRemoveNode(xmlDoc, subchild);
      }
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree__UnPanel_exportDeviceDataXml
/**
Purpose: 
Export the device data of _UnPanel device

Parameters:
  @param sDeviceName: string, input, the device name
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
        
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unExportTree__UnPanel_exportDeviceDataXml(string sDeviceName, bool bKeepLocalSystem, string sSystemNameToReplace, int xmlDoc, int xmlTop)
{
	dyn_dyn_string ddsPlotData;
	string sConf;
	int i;
	dyn_string exInfo;
        int topnode, child;

	if(dpExists(sDeviceName)) {
		if(dpTypeName(sDeviceName) == UN_WINDOWTREE_PANEL) {
                  
                        // add the current device to the list of exported device
			unExportTree_addDeviceToExportedDeviceListXml(sDeviceName);
                        
                        topnode = xmlAppendChild(xmlDoc, xmlTop, XML_ELEMENT_NODE, UN_WINDOWTREE_PANEL);
                        // if the system name is present and equals to the current system, replace it by the key #local:
			sConf = sDeviceName;
			if(unExportTree_getSystemName(sConf) == sSystemNameToReplace)
				sConf = "#local:"+unExportTree_removeSystemName(sDeviceName);
                        
                        child = xmlAppendChild(xmlDoc, topnode, XML_ELEMENT_NODE, "panelname");
                        child = xmlAppendChild(xmlDoc, child, XML_TEXT_NODE, unPanel_convertDpPanelNameToPanelFileName(sDeviceName));
			
                        // export the horizontal navigation link of the current device
			unExportTree_exportDeviceNavigationDataXml(sDeviceName, bKeepLocalSystem, sSystemNameToReplace, xmlDoc, topnode);
		}
	}
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_exportDeviceNavigationDataXml
/**
Purpose: 
Export the horizontal navigation links of a device

Parameters:
  @param sDeviceName: string, input, the device name
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
*/
unExportTree_exportDeviceNavigationDataXml(string sDeviceName, bool bKeepLocalSystem, string sSystemNameToReplace, int xmlDoc, int xmlTop)
{
	dyn_string panelLabel, panelNameList, panelDollarParam, exceptionInfo;
	int i;
	string sConf, sXMLName;
        int topnode, child;
		
        // get the horizontal navigation links of the device
	unPanel_getPanelList(sDeviceName, panelLabel, panelNameList, panelDollarParam, exceptionInfo);
        
        topnode = xmlAppendChild(xmlDoc, xmlTop, XML_ELEMENT_NODE, "devicenavigation");
	for(i=1; i<=c_unPanelConfiguration_maxPanelNavigation; i++) {
		sXMLName = panelNameList[i];
		if(unExportTree_getSystemName(sXMLName) == sSystemNameToReplace)
			sXMLName = "#local:"+unExportTree_removeSystemName(sXMLName);
		sConf = panelLabel[i]+c_panel_navigation_delimiter+sXMLName+c_panel_navigation_delimiter+panelDollarParam[i];
                child = xmlAppendChild(xmlDoc, topnode, XML_ELEMENT_NODE, "link"+i);
                child = xmlAppendChild(xmlDoc, child, XML_TEXT_NODE, sConf);
                
                // add the linked device to the list of devices of read from the horizontal navigation links
		unExportTree_addDeviceToNavigationDeviceListXml(panelNameList[i]);
	}
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_FwTrendingPage_exportDeviceDataXml
/**
Purpose: 
Export the device data of FwTrendingPage device

Parameters:
  @param sDeviceName: string, input, the device name
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unExportTree_FwTrendingPage_exportDeviceDataXml(string sDeviceName, bool bKeepLocalSystem, string sSystemNameToReplace, int xmlDoc, int xmlTop)
{
	dyn_dyn_string ddsPageData;
	dyn_string exInfo;
	int iCol, iRow, lenPS, lenTP, i, nRows, nCols;
	string sConf;
        
  int topnode, p_child, subchild, child;
  int pagelen, trendlen, itemslen;

  if( ! dpExists(sDeviceName)) return;
  
  if(dpTypeName(sDeviceName) != UN_TRENDTREE_PAGE) return;
    
  // add the current device to the list of exported device
  unExportTree_addDeviceToExportedDeviceListXml(sDeviceName);
  // get the device data
  fwTrending_getPage(sDeviceName, ddsPageData, exInfo);
  
  topnode = xmlAppendChild(xmlDoc, xmlTop, XML_ELEMENT_NODE, UN_TRENDTREE_PAGE);

  sConf = sDeviceName;
  // if the system name is present and equals to the current system, replace it by the key #local:
  if(unExportTree_getSystemName(sConf) == sSystemNameToReplace)
				sConf = "#local:"+unExportTree_removeSystemName(sConf);
  
  child = xmlAppendChild(xmlDoc, topnode, XML_ELEMENT_NODE, "dpname");
  child = xmlAppendChild(xmlDoc, child, XML_TEXT_NODE, sConf);

  p_child = xmlAppendChild(xmlDoc, topnode, XML_ELEMENT_NODE, "pageconfiguration");
                        
  pagelen = dynlen(g_dsExportTree_pageTreeTagNames);
  trendlen = dynlen(ddsPageData);
  
  // if ( pagelen != trendlen ) DebugN("trendPageData Length only '"+trendlen+"' instead of '"+pagelen+"'");
  itemslen = ( trendlen < pagelen ? trendlen : pagelen );
  
  // Reduce the number of Plots...
  lenPS = dynlen(ddsPageData[fwTrending_PAGE_OBJECT_PLOTS]);
  if ( lenPS > 6 ) {
    while ( ddsPageData[fwTrending_PAGE_OBJECT_PLOTS][lenPS] == "" )
          { dynRemove ( ddsPageData[fwTrending_PAGE_OBJECT_PLOTS] , lenPS ); if ( --lenPS == 6 ) { break; } }
  }
  lenTP = dynlen(ddsPageData[fwTrending_PAGE_OBJECT_PLOT_TEMPLATE_PARAMETERS]);
  if ( lenTP != lenPS )
  {
    if ( lenPS > lenTP ) {
      for ( i = lenTP ; i < lenPS ; ++i ) dynAppend ( ddsPageData[fwTrending_PAGE_OBJECT_PLOT_TEMPLATE_PARAMETERS] , "" );
    }
    else {
      for ( i = lenTP ; i > lenPS ; --i ) dynRemove ( ddsPageData[fwTrending_PAGE_OBJECT_PLOT_TEMPLATE_PARAMETERS] , i );
    }
  }
  
  for ( int idx = 1 ; idx <= itemslen ; ++idx )
  {
    if ( ! fwTrending_isPageTreeTagExportable(idx) ) { continue; }
        
    if ( dynlen(ddsPageData[idx]) <= 1 ) { subchild = p_child; }
    else { subchild = xmlAppendChild(xmlDoc, p_child, XML_ELEMENT_NODE, g_dsExportTree_pageTreeTagNames[idx]); }
      
    for ( int cnt = 1 ; cnt <= dynlen(ddsPageData[idx]) ; ++cnt )
    {
      sConf = ddsPageData[idx][cnt];
      if ( idx == fwTrending_PAGE_OBJECT_PLOTS )
      {
        if ( dpExists(sConf) )
           unExportTree_addDeviceToNavigationDeviceListXml(sConf);
        // if the system name is present and equals to the current system, replace it by the key #local:
        if ( unExportTree_getSystemName(sConf) == sSystemNameToReplace )
           sConf = "#local:"+unExportTree_removeSystemName(sConf);
      }
      
      child = xmlAppendChild(xmlDoc, subchild, XML_ELEMENT_NODE, g_dsExportTree_pageTreeTagNames[idx]);
      child = xmlAppendChild(xmlDoc, child, XML_TEXT_NODE, sConf);
    }
  }
          
  // export the horizontal navigation link of the current device
  unExportTree_exportDeviceNavigationDataXml(sDeviceName, bKeepLocalSystem, sSystemNameToReplace, xmlDoc, topnode);                        
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_FwTrendingPlot_exportDeviceDataXml
/**
Purpose: 
Export the device data of FwTrendingPlot device

Parameters:
  @param sDeviceName: string, input, the device name
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param sSystemNameToReplace: string, input, the system name to replace by #local
  @param xmlDoc: int, input, Xml document identifier
  @param xmlTop: int, input, Xml top-node identifier

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
        
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unExportTree_FwTrendingPlot_exportDeviceDataXml(string sDeviceName, bool bKeepLocalSystem, string sSystemNameToReplace, int xmlDoc, int xmlTop)
{
	dyn_dyn_string ddsPlotData;
	string sPlotConf, sDpeConf, sConf, sLegend;
	int i;
	dyn_string exInfo;
	
  int topnode, p_child, subchild, child;
  int plotlen, trendlen, itemslen;
        
  if ( ! dpExists(sDeviceName) ) return;
          
  if( dpTypeName(sDeviceName) != UN_TRENDTREE_PLOT ) return;
  
  // add the current device to the list of exported device
  unExportTree_addDeviceToExportedDeviceListXml(sDeviceName);
  // get the device data
  fwTrending_getPlot(sDeviceName, ddsPlotData, exInfo);
  topnode = xmlAppendChild(xmlDoc, xmlTop, XML_ELEMENT_NODE, UN_TRENDTREE_PLOT);

  sConf = sDeviceName;
  // if the system name is present and equals to the current system, replace it by the key #local:
  if ( unExportTree_getSystemName(sConf) == sSystemNameToReplace )
     sConf = "#local:"+unExportTree_removeSystemName(sConf);
  
  child = xmlAppendChild(xmlDoc, topnode, XML_ELEMENT_NODE, "dpname");
  child = xmlAppendChild(xmlDoc, child, XML_TEXT_NODE, sConf);
       
  p_child = xmlAppendChild(xmlDoc, topnode, XML_ELEMENT_NODE, "plotconfiguration");
                        
  plotlen = dynlen(g_dsExportTree_plotTreeTagNames);
  trendlen = dynlen(ddsPlotData);
  
  // if ( plotlen != trendlen ) DebugN("trendPlotData Length only '"+trendlen+"' instead of '"+plotlen+"'");
  itemslen = ( trendlen < plotlen ? trendlen : plotlen );
  
  
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_DPES_X,"");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_DPES,"");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_LEGENDS_X,"");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_LEGENDS,"");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_COLORS,"FwtrendingCurve%d",TRUE);
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_AXII_X,"FALSE");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_AXII,"FALSE");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_CURVES_HIDDEN,"FALSE");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_RANGES_MIN_X,"0");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_RANGES_MAX_X,"0");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_RANGES_MIN,"0");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_RANGES_MAX,"0");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_CURVE_TYPES,"1");
  
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_AXII_POS,"0");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_AXII_LINK,"0");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_AXII_X_FORMAT,"");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_AXII_Y_FORMAT,"");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_LEGEND_VALUES_FORMAT,"");
  _unExportTree_checkEightElements(ddsPlotData,fwTrending_PLOT_OBJECT_ALARM_LIMITS_SHOW,"FALSE");
  
  for ( int idx = 1 ; idx <= itemslen ; ++idx )
  {
    if ( ! fwTrending_isPlotTreeTagExportable(idx) ) { continue; }
        
    if ( dynlen(ddsPlotData[idx]) <= 1 ) { subchild = p_child; }
    else { subchild = xmlAppendChild(xmlDoc, p_child, XML_ELEMENT_NODE, g_dsExportTree_plotTreeTagNames[idx]); }
    
    if ( dynlen(ddsPlotData[idx]) == 0 )
    {
      // DebugN("Plot Empty field for "+sDeviceName+" Item "+g_dsExportTree_plotTreeTagNames[idx]);
      
      child = xmlAppendChild(xmlDoc, subchild, XML_ELEMENT_NODE, g_dsExportTree_plotTreeTagNames[idx]);
      child = xmlAppendChild(xmlDoc, child, XML_TEXT_NODE, "");
    }
    
    for ( int cnt = 1 ; cnt <= dynlen(ddsPlotData[idx]) ; ++cnt )
    {
      sConf = ddsPlotData[idx][cnt];
      if ( ! bKeepLocalSystem ) {
         if ( idx == fwTrending_PLOT_OBJECT_DPES ) {
            if ( unExportTree_getSystemName(sConf) == sSystemNameToReplace )
							     sConf = "#local:"+unExportTree_removeSystemName(sConf);
         }
         if ( idx == fwTrending_PLOT_OBJECT_LEGENDS ) {
            if ( strpos(sConf, sSystemNameToReplace ) >= 0 )
               strreplace(sConf, sSystemNameToReplace, "#local:");
         }
      }
        
      child = xmlAppendChild(xmlDoc, subchild, XML_ELEMENT_NODE, g_dsExportTree_plotTreeTagNames[idx]);
      child = xmlAppendChild(xmlDoc, child, XML_TEXT_NODE, sConf);
    }
  }
       
  // export the horizontal navigation link of the current device
  unExportTree_exportDeviceNavigationDataXml(sDeviceName, bKeepLocalSystem, sSystemNameToReplace, xmlDoc, topnode);
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_addDeviceToExportedDeviceListXml
/**
Purpose: 
Add the device in the list of exported device

Parameters:
  @param sDeviceName: string, input, the device name

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
*/
unExportTree_addDeviceToExportedDeviceListXml(string sDeviceName)
{
string sTemp=dpSubStr(sDeviceName, DPSUB_SYS_DP);

    if ( sTemp != "" ) {
    if ( dynContains(g_dsExportTree_exportedDevice, sTemp) <= 0 ) {
      // device not exported - add the device
      dynAppend(g_dsExportTree_exportedDevice, sTemp);
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_addDeviceToNavigationDeviceListXml
/**
Purpose: 
Add the device in the list of devices read from the horizontal navigation links

Parameters:
  @param sDeviceName: string, input, the device name

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
*/
unExportTree_addDeviceToNavigationDeviceListXml(string sDeviceName)
{
	string sTemp = dpSubStr(sDeviceName, DPSUB_SYS_DP);
	
	if(sTemp == "") {
                // if empty, it is a panel or a wrong device
                // convert it to a panel
		sTemp = unPanel_convertPanelFileNameToDp(sDeviceName);
                // check the panel dp is existing, if empty again==>wrong dp or dp on a remote system
		sTemp = dpSubStr(sTemp, DPSUB_SYS_DP);
	}
        
	if(sTemp != "") {
		if ( dynContains(g_dsExportTree_exportedDevice, sDeviceName) <= 0 ) { // 
                        // device not exported - check if it is already in the navigationDevice list
                        if ( dynContains(g_dsExportTree_navigationDevice, sTemp) <= 0 ) {
                                // device not in the navigationDevice list - add the device
			        dynAppend(g_dsExportTree_navigationDevice, sTemp);
                        }
		}
	}
}


//------------------------------------------------------------------------------------------------------------------------

// Below this are all the routines whicch have been untouched from the pre-Xml world

//------------------------------------------------------------------------------------------------------------------------


// unExportTree_initialize
/**
Purpose:
Executes the initialization of the unTreeWidget used to view the list of trees. This function is executed at startup of the unTreeWidget.
The unTreeWidget tree is disabled during the operation. This function is started in a separate thread.

	@param sGraphTree: string, input: the name of the unTreeWidget graphical element
	@param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
	@param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget and 
	the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{EvalScript}
*/
unExportTree_initialize(string sGraphTree, string sBar, bool bClipboard)
{
	export.enabled = false;
	unImportTree_initialize(sGraphTree, sBar, bClipboard);
	export.enabled = true;
}


//------------------------------------------------------------------------------------------------------------------------


// unExportTree_EventNodeLeft
/**
Purpose:
Function called called when a left click on a node of the unTreeWidget is done during the operation of the 
tree hierarchy. This function get the operation panel of the node, show it in the main frame.

	@param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
	@param sMode: string, input: the type of mouse click
	@param sCompositeLabel: string, input: the composite label of the node
	@param sGraphTree: string, input: the name of the unTreeWidget graphical element
	@param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
	@param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
	@param iPosX: int, input: x position of the mouse
	@param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget and 
	the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{EvalScript}
*/
unExportTree_EventNodeLeft(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
	string sTreeName;
	
	export.enabled = false;
	doNotTreeDef.enabled = false;
	doNotTreeDef.state(0) = false;
	unImportTree_EventNodeLeft(sSelectedKey, sMode, sCompositeLabel, sGraphTree, sBar, bClipboard, iPosX, iPosY);
	export.enabled = true;
	sTreeName = dpTreeName.text;
	if(sTreeName != "")
		doNotTreeDef.enabled = true;
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_removeSystemName
/**
Purpose: 
Remove the system name

Parameters:
  sName: string, input, the name

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
*/
string unExportTree_removeSystemName(string sName)
{
	int pos;
	string sTemp;
	
	pos = strpos(sName, ":");
	if(pos>0)
		sTemp = substr(sName, pos+1, strlen(sName));
	else
		sTemp = sName;
		
	return sTemp;
}


//------------------------------------------------------------------------------------------------------------------------


//unExportTree_getSystemName
/**
Purpose: 
Remove the system name

Parameters:
  sName: string, input, the name

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: WXP.
        . distributed system: yes.
*/
string unExportTree_getSystemName(string sName)
{
	int pos;
	string sTemp;
	
	pos = strpos(sName, ":");
	if(pos>0)
		sTemp = substr(sName, 0, strpos(sName,":")+1);
	else
		sTemp = "";
		
	return sTemp;
}


//------------------------------------------------------------------------------------------------------------------------

// The Silent Versions of Export

//------------------------------------------------------------------------------------------------------------------------


// unExportTree_exportSilent
/**
Purpose: export in silent mode

Important-Note: Note that this function uses the three following global variables:
                g_bExportTree_stopExporting
	        g_dsExportTree_exportedDevice
                g_dsExportTree_navigationDevice
                They must be created before one calls this function...

Parameters:
		@param bNoTreeDef, bool, input, true=do not export the tree definition/false=export the tree definition
		@param sNodeNameToExport, string , input, the node name to export
		@param sDpNodeNameToExport, string , input, the dp node name to export
		@param bKeepSystemName, bool , input, true=keep the system name for the plot DPE/false=replace the system name by #local:
		@param sSystemName, string , input, the system name to replace and the system name to export sNodeNameToExport
		@param fileName, string , input, the filename of the file containing the exported data
		@param exInfo, dyn_string, output, for errors
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

*/

unExportTree_exportSilent(bool bNoTreeDef, string sNodeNameToExport, string sDpNodeNameToExport,
                          bool bKeepSystemName, string sSystemName, string sFileName, dyn_string &exInfo)
{
string sNodeName = sNodeNameToExport;
string sText, sParentNodeName;
int i, len;
string sTreeDp = sDpNodeNameToExport;
bool bConnected, bRemote;
		
int xmlDoc, xmlTop;
        
  if ( unExportTree_uniqueGetAccess ( TRUE ) == FALSE ) {
          
    fwException_raise(exInfo, "ERROR", "unExportTree_exportSilent: Concurrent export already running, try again later!", "");
          
    return;
  }
        
  if(sSystemName == "")
    sSystemName = getSystemName();
		
  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote) 
    unDistributedControl_isConnected(bConnected, sSystemName);
  else
    bConnected = true;

  if(bConnected) {
    if(bNoTreeDef) {
      if(dpExists(sDpNodeNameToExport)) {
        if(dpTypeName(sDpNodeNameToExport) == "_FwTreeType")
          sTreeDp = "";
        else
          bNoTreeDef = false;
      }
      else
        fwException_raise(exInfo, "ERROR", "unExportTree_exportSilent: dp not existing "+sDpNodeNameToExport, "");
    }			
    if(sNodeName != "") {

      xmlDoc = xmlNewDocument();
      xmlTop = xmlAppendChild(xmlDoc,-1,XML_PROCESSING_INSTRUCTION_NODE,"xml version=\"1.0\" encoding=\"UTF-8\"");
      xmlTop = xmlAppendChild(xmlDoc,-1,XML_ELEMENT_NODE,"tree");
      xmlSetElementAttribute(xmlDoc,xmlTop,"version","2");
                  
      unExportTree_exportTreeDataXml(xmlDoc, xmlTop, sTreeDp, sNodeName, bKeepSystemName, sSystemName, FALSE, exInfo);

      if ( bNoTreeDef ) {
        dyn_int children, elements;
                                        
        xmlChildNodes(xmlDoc,xmlTop,children);
        xmlChildNodes(xmlDoc,children[4],elements);
        for ( i = 1 ; i <= dynlen(elements) ; ++i ) {
          if ( xmlNodeName(xmlDoc,elements[i]) == "parentname" ) {
            xmlSetNodeValue(xmlDoc,xmlFirstChild(xmlDoc,elements[i]),"");
          }
        }
      }

      xmlDocumentToFile(xmlDoc,sFileName+"_temporary");

      xmlCloseDocument(xmlDoc);
                        
      unExportTree_indentXmlFile(sFileName,sFileName+"_temporary");
     
      remove(sFileName+"_temporary");
    }
    else
      fwException_raise(exInfo, "ERROR", "unExportTree_exportSilent: empty node name", "");
  }
  else
    fwException_raise(exInfo, "ERROR", "unExportTree_exportSilent: system not connected "+sSystemName, "");
        
  unExportTree_uniqueGetAccess ( FALSE );
}


//------------------------------------------------------------------------------------------------------------------------


// unExportTree_exportImportSilent
/**
Purpose: export/check/import in silent mode with no tree definition and keep the system name

Parameters:
		@param sNodeNameToExport, string , input, the node name to export
		@param sNodeDpToExport, string , input, the dp node name to export
		@param sSystemName, string , input, the system name to replace and the system name to export sNodeNameToExport
		@param bImportInto, bool, input, true=import into sImportIntoNodeName/false=do not import into sImportIntoNodeName
		@param sImportIntoNodeName, string , input, the node name to import the data into
		@param exError, dyn_string, output, for errors
		@param exWarning, dyn_string, output, for warnings
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unExportTree_exportImportSilent(string sNodeNameToExport, string sNodeDpToExport, 
                                bool bKeepSystemName, string sSystemName, bool bImportInto, 
                                string sImportIntoNodeName, dyn_string &exError, dyn_string &exWarning)
{
int i,len;
int iIndex;
bool bDoNotTreeDef = false;
dyn_string exWarningTemp;
	
string sFileName = getPath(DATA_REL_PATH) + "_temporary_import_export_" + (int)getCurrentTime();
        
  // Exporting data
  unExportTree_exportSilent(bDoNotTreeDef, sNodeNameToExport, sNodeDpToExport, bKeepSystemName, sSystemName, sFileName, exError);
        
  if(dynlen(exError) <= 0) {
    // Checking data
    unImportTree_checkSilent(bKeepSystemName, bImportInto, sImportIntoNodeName, sFileName, exError, exWarningTemp);
    if(dynlen(exError) <= 0) {
      dynAppend(exWarning, exWarningTemp);
      exWarningTemp = makeDynString();
      // Importing data
      unImportTree_importSilent(bKeepSystemName, bImportInto, sImportIntoNodeName, sFileName, exError, exWarningTemp);
      dynAppend(exWarning, exWarningTemp);
    }
  }

  remove ( sFileName );
}


//------------------------------------------------------------------------------------------------------------------------


//@}


