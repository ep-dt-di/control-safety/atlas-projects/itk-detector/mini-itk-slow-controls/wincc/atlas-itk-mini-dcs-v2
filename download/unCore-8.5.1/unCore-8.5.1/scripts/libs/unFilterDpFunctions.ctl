/**@name LIBRARY: unFilterDpFunctions.ctl

@author: Geraldine Thomas
Creation Date: 29/10/2002

Modification History: 
version 2.0: 12/11/2007 Herve
		. optmization for re-use, new function _unFilterDpFunctions_dpSelector called by unFilterDpFunctions_dpSelector
version 1.0
version 1.0.1: 03/02/2003 by Herve
		. modify unFilterDpFunctions_dpSelector: remove the call to get alias of DP and add it in the unFilterDpFunctions_FilterAlias
		. in unFilterDpFunctions_FilterAlias call dpGetAlias
		. modify the call to unFilterDpFunctions_FilterDescription: call unGenericDpFunction_getDescription
		the unFilterDpFunctions_dpSelector function returns the dp name and the alias (if there is no alias for the dp, "" is returned)
		. unFilterDpFunctions_FilterAlias and unFilterDpFunctions_FilterDescription are always called
version 1.1: 03/03/2004 Herve
		. in unFilterDpFunctions_FilterAlias and unFilterDpFunctions_FilterDescription use patternMatch instead of strpos.
version 2.0: 15/04/2004 Herve
		. get the DPE search criteria from _unApplication instead of being hardcoded.

version 2.0: Herve
- upgrade to PVSS II 3.0
- use UN_APPLICATION_DPNAME  instead of _unApplication

version 3.0: Herve:
- in case of distributed system: do not query if the remote system is not accessible: use the DistributedControl
- correct the query when more than one groups is used.
- new function added: unFilterDpFunctions_dpQuery
- remove the functions unFilterDpFunctions_GetDPNamesFromGroups and unFilterDpFunctions_GetDPNameFromDPE: not used.

	31/03/2006: Herve
		- optimize the description filtering
		
External Functions: 
	. unFilterDpFunctions_dpSelector 		main function which returns a list of DPs and its corresponding aliases according 
											to the filter options set  
	. unFilterDpFunctions_FilterAlias       Do a filter on an aliasPattern and return a dyn list of dpNames & dpAliases
	. unFilterDpFunctions_FilterDescription Do a filter on a DescriptionPattern and return a dyn list of dpNames & dpAliases
	. unFilterDpFunctions_dpQuery does a dpQuery and returns the list of dpes.
	
Internal Functions: 
	
Purpose: 
This library contains functions to retrieve DataPoints from given filter options selected

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

//@{

// unFilterDpFunctions_dpSelector
/**
Purpose:
This functions looks for a specific datapoints or list of datapoinst according to the filter options selected 
and returns a list of DpNames and Aliases that matches the most the criteria entered

	@param   systemName : string,input, the system name 
	@param   bDistributed: bool, input, the distributed state of the system
	@param   dsGroupName: dyn_string, input, takes a list of dpGroups present on the system, 
			 if there are no group the empty string "" is passed else one or several are passed.			 
    @param   dsDeviceTypeFilter:dyn_string, input, take a list of the device type on the system
    		 if there are no device type selected the empty string is passed and 
    		 therefore it will look in all types even none UNICOS one (WHERE string is emptyin this case)
    		 Else one or more than one can be passed
	@param   sAliasPattern:string, input, a string pattern corresponding to am alias
	@param   sDescriptionPattern:string, input, a string pattern corresponding to a description
	@param   sDpNamePattern:string,input, a string pattern corresponding to a dpName 
	@param   dsAliases:dyn_string, output a list of Aliases found corresponding to the filter options
	@param   dsDpNames:dyn_string, output a list of dpNames found corresponding to the filter options
	@param   exceptionInfo :dyn_string, output any errors

	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unFilterDpFunctions_dpSelector(string	systemName, bool bDistributed, dyn_string dsGroupName,
															   dyn_string dsDeviceTypeFilter, string sAliasPattern,
															   string sDescriptionPattern, string sDpNamePattern,
															   dyn_string	&dsAliases, dyn_string &dsReturnDpNames, dyn_string &exceptionInfo)

{
	dyn_string dsDpNames;

	_unFilterDpFunctions_dpSelector(systemName, bDistributed, dsGroupName,
															   dsDeviceTypeFilter, sDpNamePattern,
															   dsDpNames, exceptionInfo);

	// Reduce the lists of DPNames and Aliases according to the Alias pattern if any
	unFilterDpFunctions_FilterAlias(sAliasPattern, dsDpNames, dsAliases);

	// Reduce the lists of DPNames and Aliases according to the Description pattern if any
	unFilterDpFunctions_FilterDescription(sDescriptionPattern, dsDpNames, dsAliases);
	dsReturnDpNames = dsDpNames;
}

// _unFilterDpFunctions_dpSelector
/**
Purpose:
This functions looks for a specific datapoints or list of datapoinst according to the filter options selected 
and returns a list of DpNames that matches the most the criteria entered

	@param   systemName : string,input, the system name 
	@param   bDistributed: bool, input, the distributed state of the system
	@param   dsGroupName: dyn_string, input, takes a list of dpGroups present on the system, 
			 if there are no group the empty string "" is passed else one or several are passed.			 
    @param   dsDeviceTypeFilter:dyn_string, input, take a list of the device type on the system
    		 if there are no device type selected the empty string is passed and 
    		 therefore it will look in all types even none UNICOS one (WHERE string is emptyin this case)
    		 Else one or more than one can be passed
	@param   sDpNamePattern:string,input, a string pattern corresponding to a dpName 
	@param   dsDpNames:dyn_string, output a list of dpNames found corresponding to the filter options
	@param   exceptionInfo :dyn_string, output any errors

	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

_unFilterDpFunctions_dpSelector(string	systemName, bool bDistributed, dyn_string dsGroupName,
															   dyn_string dsDeviceTypeFilter, string sDpNamePattern,
															   dyn_string &dsReturnDpNames, dyn_string &exceptionInfo)
{
	string remoteString, whereString, fromString, selectString, likeString, selectQuery, sDPESearch, sGroupName;
	int	length, i, numberOfGroups;
	bool bConnected;
	dyn_string dsDpNames, dsTempDpNames;
					
	// Build the remoteString if the search is applied to a distributed system
	if (bDistributed == false)
	{
		remoteString = "";
	}
	else
	{
		remoteString = "REMOTE " + "'" + systemName + "'";
		unDistributedControl_isConnected(bConnected, systemName);

		if(!bConnected)
			return;
	}

	// Build the whereString if some types have been specified
	length = dynlen (dsDeviceTypeFilter);
	if (length == 0) //will look in all type even non Unicos one.
	{
		whereString = "";
	}
	else
	{
		whereString = "WHERE _DPT IN (";
		whereString = whereString + "\"" + dsDeviceTypeFilter[1] + "\"";
		if (length > 1)
		{
			for (i = 2; i <= length; i++)
			{
				whereString =  whereString + "," + "\"" + dsDeviceTypeFilter[i] + "\"";
			}
		}
		
		whereString = whereString + ")";
	}
	
	// Reduce the lists of DPNames and Aliases according to the DPName pattern if any
	if (sDpNamePattern != "")
	{
		likeString = "&& _DP LIKE " + "\"" + sDpNamePattern + "\"";
	}
	else
	{
		likeString = "";
	}

	// Build the approriate query according to the number of specified groups
	// There are three cases 0,1 or n
	numberOfGroups = dynlen (dsGroupName);

	//		When no group is specified the fromString is built with the DPE StstReg01
	//		there can not be several returned rows for a given DP
	if ( numberOfGroups == 0 )
	{
// get the DPE search criteria
		if(dpExists(UN_APPLICATION_DPNAME)) {
			dpGet(UN_APPLICATION_DPNAME + ".dpFilterSearchDPE", sDPESearch);
		}
// if empty set it to default
		if(sDPESearch == "")
			sDPESearch = UN_DEFAULT_DPFILTER_DPE;
		fromString = "FROM '*."+sDPESearch+"'";

	// build the query
		selectQuery = "SELECT '_online.._value'" + " " + fromString + " " + remoteString + " "  + whereString + " " + likeString ; //original
		unFilterDpFunctions_dpQuery(selectQuery, dsDpNames);		
	}
	//When one group at least is defined, the fromString is built with the first group
	//there can be several returned rows for a given DP
	else
	{
		for(i=1; i<= numberOfGroups;i++) {
			dsTempDpNames = makeDynString();
		// get the group DP name
			sGroupName = unGenericDpFunctions_groupNameToDpName(dsGroupName[i], systemName);
//DebugN(sGroupName, dsGroupName[i], systemName, dpSubStr(sGroupName, DPSUB_DP));
			sGroupName = dpSubStr(sGroupName, DPSUB_DP);
			if(sGroupName != "") { // group exists
				fromString = "FROM 'DPGROUP(" + sGroupName + ")'";
		// build the query
				selectQuery = "SELECT '_online.._value'" + " " + fromString + " " + remoteString + " "  + whereString + " " + likeString ; //original
				unFilterDpFunctions_dpQuery(selectQuery, dsTempDpNames);
			}
			if(i == 1)
				dsDpNames = dsTempDpNames;
			else {
				dsDpNames = dynIntersect(dsDpNames, dsTempDpNames);
				dynUnique(dsDpNames);
			}	
		}
	}

//	DebugN(" dsDpNames AFTER FUNCTION", dsDpNames);

	dsReturnDpNames = dsDpNames;
}

//unFilterDpFunctions_dpQuery
/**
Purpose: 
This function executes the dpQuery and returns the list of Dps. there is no redundant dp in the list.
	
	@param  selectQuery : string,input,the query statetement
	@param  dsDpNames : dyn string,output,take a list of datapoint name
	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unFilterDpFunctions_dpQuery(string selectQuery, dyn_string &dsDpNames)
{
	int i, length;
	dyn_dyn_anytype tabResult;
	string dpName;
	
//DebugN("selectQuery",selectQuery);

	dpQuery(selectQuery, tabResult);

//DebugN(tabResult);

	// Remove all columns except the first one
	length = dynlen (tabResult);
	for (i= 2; i<= length; i++)   //remove first line which is not relevant
	{
		dpName = unGenericDpFunctions_getDpName(tabResult[i][1]);
		dynAppend(dsDpNames, dpName);
	}
	dynUnique(dsDpNames);
//DebugN("result query", dsDpNames);
}

// unFilterDpFunctions_FilterAlias
/**
Purpose: 
	This function does a filtering on the alias pattern. It takes as an input an aliaspattern and an array of dpnames,
	it retrieves the aliases of the array of dpnames and build an array of aliases that matches the aliasPattern. 
	It then returns a list on aliases and its corresponding list of dpnames.

	@param  sAliasPattern: string, input, pattern that corespond the most to an Alias
	@param  dsDpNames: dyn_string, input & output reference variable that takes as input a list of DPs and returned 
	 		 			 a Dps list build from the filtering 
	@param 	dsAliases: dyn_string, output, return a list of Aliases

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unFilterDpFunctions_FilterAlias(string sAliasPattern, dyn_string &dsDpNames, dyn_string &dsAliases)
{
	string		sAlias;
	dyn_string 	dsReducedAliases,
				sDpReducedNames/*, tempDs1, tempDs2*/;
	int 		iLength, i, pos;

//DebugN("*****", dsDpNames);	
	iLength = dynlen(dsDpNames);
	for(i = 1;i<=iLength;i++) {
		sAlias = dpGetAlias(dsDpNames[i]+".");

		if(sAliasPattern == "*") {
			dynAppend( sDpReducedNames, dsDpNames[i] );
			dynAppend( dsReducedAliases, sAlias );
		}
		else {
			if(patternMatch(strtolower(sAliasPattern), strtolower(sAlias))) {
				dynAppend( sDpReducedNames, dsDpNames[i] );
				dynAppend( dsReducedAliases, sAlias );
	//DebugN("adding", dsDpNames[i], sAlias);
			}
		}
	}
	dsDpNames = sDpReducedNames;
	dsAliases = dsReducedAliases;
}


// unFilterDpFunctions_FilterDescription
/**
Purpose: 
	This function do a filtering on the Description. It takes as an input an Descriptionpattern and an array of dpnames,
	it retrieves the Description of the array of dpnames and build an array of Descriptions that match the aliasPattern. 
	It returns a dyn_string of Description and the corresponding dyn_string of dpnames.

	@param sDescriptionPattern: string, input 
	@param dsDpNames: dyn_string, input & output of data point names

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unFilterDpFunctions_FilterDescription(string sDescriptionPattern, dyn_string &dsDpNames, dyn_string &dsAliases)
{
	string sDescription;
	dyn_string sDpReducedNames, dsReducedAliases;
	int iLength, i, pos;
	
	if(sDescriptionPattern != "*") {
		iLength = dynlen(dsDpNames);
		for(i = 1;i<=iLength;i++) {
			sDescription = unGenericDpFunctions_getDescription(dsDpNames[i]);
			if(patternMatch(sDescriptionPattern, sDescription)) {
				dynAppend( sDpReducedNames, dsDpNames[i] );
				dynAppend( dsReducedAliases, dsAliases[i] );
			}
		}
		dsDpNames = sDpReducedNames;
		dsAliases = dsReducedAliases;
	}
}

//@}

