/**@name LIBRARY: unFipDiag.ctl

@author: Pablo ORIOL BITAUBE (AB/CO)

Creation Date: 24/04/2007

Modification History: 
  17/01/2014: Herve
  - IS-1328 - FipDiag expert interface call modified with FESA3

	16/07/2007: Herve
		- use jnlp from web site
		
version 1.0

External Function : 
	. unFipDiag_openFipDiagExpert : open Fip Information Console.

Internal Functions :
		
Purpose: This library contains Fip Information functions.

Usage: Public

PVSS manager usage: NG, NV

Constraints:

	. Global variables and $parameters defined in Analog faceplate and widget
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

//---------------------------------------------------------------------------------------------------------------------------------------

// unFipDiag_openFipDiagExpert
/**
Purpose: Open Fip Information Console.

Parameters:
	- sDeviceName, string, input, device name
	- sFECName, string, input, FEC name
	- sCMWName,  string, input, CMW name

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.1
	. operating system: WinXP
	. distributed system: yes.
  
  @reviewed 2018-06-25 @whitelisted{FalsePositive}
*/
void unFipDiag_openFipDiagExpert (string sFECName, string sCMWName, string sDeviceName)
{
  system(FIP_DIAG_JAVA+"\""+FIP_DIAG_WEB+sCMWName+"\"");
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "FipDiag "+sCMWName, "unFipDiag_openFipDiagExpert", FIP_DIAG_JAVA+"\""+FIP_DIAG_WEB+sCMWName+"\"");
}

//---------------------------------------------------------------------------------------------------------------------------------------
//@{
