/**@name LIBRARY: unGenericButtonFunctionsHMI.ctl

@author: Herve Milcent (LHC-IAS) 

Creation Date: 21/08/2002

Modification History: 
  17/06/2010: Herve
  - improvement IS-76, check access right slow when a user is in a lot of access control domain.
  modified function: unGenericButtonFunctionsHMI_getUnicosPriviledges
  
 11/09/2009: Frederic
   - update to have 5 levels of permission (no user - monitor - operator - expert - admin) instead of   monitor = no user
	Functions updated:
	unGenericButtonFunctionsHMI_ButtonUserAccess
	unGenericButtonFunctionsHMI_getPanelAccessControl
	unGenericButtonFunctionsHMI_getUnicosPriviledges
	unGenericButtonFunctionsHMI_getPanelAccessLevel

  30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

  08/02/2008: Herve
    - default domain as string instead of int.
    
  04/02/2008: Herve
    - JCOP trending user callback function
      unGenericButtonFunctionsHMI_plotConfPanelselectCB, unGenericButtonFunctionsHMI_plotsPageselectCB
      unGenericButtonFunctionsHMI_TrendingPageselectCB, unGenericButtonFunctionsHMI_TrendingHistogramConfigselectCB
      unGenericButtonFunctionsHMI_TrendingTrendselectCB, unGenericButtonFunctionsHMI_TrendingHistogramselectCB
    
  28/12/2007: Herve
    - device acccess control
    
	10/07/2007: Herve
		- unGenericButtonFunctionsHMI_isAccessAllowedMultiple: bug in case device does not have function.
		
version 1.0

External Functions: 
	. unGenericButtonFunctionsHMI_isAccessAllowed: is the access allowed, for one button
	. unGenericButtonFunctionsHMI_isAccessAllowedMultiple: is the access allowed, for many buttons
	. unGenericButtonFunctionsHMI_ButtonUserAccess : generic button user access
	. unGenericButtonFunctionsHMI_getUnicosPriviledges : get Unicos monitor, operator, expert and admin access
	. unGenericButtonFunctionsHMI_closePanel: close a panel depending on the value of a variable.
	
Internal Functions: 
	
Purpose: 
This library contains generic functions to be used by the UNICOS buttons. These functions can be overwritten if a 
different user access is needed.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 2.12.1 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
//@{

#uses "fwTrending/fwTrending.ctl"

// unGenericButtonFunctionsHMI_isAccessAllowed
/**
Purpose: check if specific button can be operated by the current user
	@param sDpName: string, input, device
	@param sDpType: string, input, dptype
	@param sButtonType: string, input, the type of button
	@param granted: bool, output, is the access allowed for the user who is logged in
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

@reviewed 2020-04-03 PG; removed code duplication by a direct call
*/
void unGenericButtonFunctionsHMI_isAccessAllowed(string sDpName, string sDpType, string sButtonType, bool &granted, dyn_string &exceptionInfo) 
{
	dyn_string dsButtonsAllowedTemp;
	unGenericButtonFunctionsHMI_isAccessAllowedMultiple(sDpName, sDpType, dsButtonsAllowedTemp, exceptionInfo);
	granted = dynContains(dsButtonsAllowedTemp, sButtonType);	

  //DebugN("unGenericButtonFunctionsHMI_isAccessAllowed", sDpName, dynContains(dsButtonsAllowedTemp, sButtonType), sButtonType);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericButtonFunctionsHMI_isAccessAllowedMultiple
/**
Purpose: retrieve the list of buttons that can be used by the current user
	@param sDpName: string, input, device
	@param sDpType: string, input, dptype
	@param dsButtonsAllowed: dyn_string, output, list of buttons
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

@reviewed 2020-04-03 PG
*/
void unGenericButtonFunctionsHMI_isAccessAllowedMultiple(string sDpName, string sDpType, dyn_string &dsButtonsAllowed, dyn_string &exceptionInfo)
{

	// 2020-04-03,PG: fast path - call the function directly instead of evalScript...	
	if (sDpType == UN_GENERIC_USER_ACCESS) {
		unGenericButtonFunctionsHMI_ButtonUserAccess(sDpName, sDpType, dsButtonsAllowed);
		return;
	}

	// otherwise, if custom one is in use, find it out and exec through evalScript
	dyn_string dsButtonsAllowedTemp;
	dyn_string dsFunctions;
	string sFunction;

	if (sDpType != "") {
		unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
		if (dynlen(dsFunctions) >= UN_GENERICOBJECT_FUNCTION_BUTTONUSERACCESS) sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_BUTTONUSERACCESS];
	}
	
	if (sFunction != "") {
		evalScript(dsButtonsAllowedTemp, "dyn_string main(string sDpName, string sDpType) {" + 
										 "dyn_string dsAccess;" +
										 "if (isFunctionDefined(\"" + sFunction + "\"))" +
										 "    {" +
										 "    " + sFunction + "(sDpName, sDpType, dsAccess);" +
										 "    }" +
										 "return dsAccess; }", makeDynString(), sDpName, sDpType);
	}

	dsButtonsAllowed = dsButtonsAllowedTemp;
  //DebugN("unGenericButtonFunctionsHMI_isAccessAllowedMultiple", sDpName, dsButtonsAllowed);        
}        

//------------------------------------------------------------------------------------------------------------------------

// unGenericButtonFunctionsHMI_ButtonUserAccess
/**
Purpose: generic button user access

Parameters: 
	- sDpName, string, input, device
	- sDpType, string, input, dptype
	- dsAccess, dyn_string, output, list of allowed buttons
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{EvalScript}
*/

unGenericButtonFunctionsHMI_ButtonUserAccess(string sDpName, string sDpType, dyn_string &dsAccess)
{
bool monitor, operator, expert, admin;
int i, length;
dyn_bool dbPermissions;
dyn_string exceptionInfo;
string sPanelFileName, sOperAction, sExpAction, sAdminAction, sMoniAction;
bool bActionDefined = false;
   
  unGenericDpFunctions_getAccessControlPrivilegeRight(sDpName, g_dsDomainAccessControl, dbPermissions, sMoniAction, sOperAction, sExpAction, sAdminAction, exceptionInfo);
  if (dynlen(exceptionInfo)) {
      fwExceptionHandling_display(exceptionInfo);
      dynClear(exceptionInfo);
      // no return here because we need to propagate the logic. In any case, the access rights returned will be 4xfalse
  }
  
  if((sMoniAction != UNICOS_ACCESSCONTROL_DOMAINNAME) ||
     (sOperAction != UNICOS_ACCESSCONTROL_DOMAINNAME) ||
     (sExpAction != UNICOS_ACCESSCONTROL_DOMAINNAME) ||
     (sAdminAction != UNICOS_ACCESSCONTROL_DOMAINNAME))
      bActionDefined=true;
  	  
	monitor = dbPermissions[1]; 
  operator = dbPermissions[2];
	expert = dbPermissions[3];
	admin = dbPermissions[4];
	dsAccess = makeDynString(UN_FACEPLATE_BUTTON_NEXT, UN_FACEPLATE_BUTTON_BACK);
        	
  if (monitor)        
  {
    if(bActionDefined)
      dynAppend(dsAccess, strsplit(sMoniAction, UN_ACCESS_CONTROL_SEPARATOR));
    else
      dynAppend(dsAccess, makeDynString(UN_USER_MONITOR));      
  }
    
  if (operator)
	{
          if(bActionDefined)
            dynAppend(dsAccess, strsplit(sOperAction, UN_ACCESS_CONTROL_SEPARATOR));
          else
	    dynAppend(dsAccess, makeDynString(UN_FACEPLATE_BUTTON_AUTO_MODE, UN_FACEPLATE_BUTTON_MANUAL_MODE, UN_FACEPLATE_BUTTON_REGULATION_MODE,
				  UN_FACEPLATE_BUTTON_ONOPEN_REQUEST, UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST, UN_FACEPLATE_BUTTON_CONTROLLED_STOP_REQUEST,
				  UN_FACEPLATE_BUTTON_MANUAL_AUTO_TO_DEPENDENT_OBJECT, UN_FACEPLATE_BUTTON_ACK_ALARM, UN_FACEPLATE_BUTTON_SELECT,
				  UN_FACEPLATE_BUTTON_SET_VALUE, UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS, UN_FACEPLATE_BUTTON_SET_STATUS_LIMITS,
				  UN_FACEPLATE_BUTTON_PID_PARAMETERS, UN_FACEPLATE_BUTTON_SETPOINT, UN_FACEPLATE_BUTTON_MASK_ALARM,
				  UN_FACEPLATE_BUTTON_SETOUTPUT, UN_FACEPLATE_BUTTON_OPTION_MODES, UN_FACEPLATE_BUTTON_SET_PID_LIMITS, 
				  UN_FACEPLATE_BUTTON_INC_VALUE, UN_FACEPLATE_BUTTON_DEC_VALUE, UN_FACEPLATE_BUTTON_UNMASK_ALARM, UN_USER_OPERATOR));
	}
  
	if (expert)
	{
          if(bActionDefined)
            dynAppend(dsAccess, strsplit(sExpAction, UN_ACCESS_CONTROL_SEPARATOR));
          else
	    dynAppend(dsAccess, makeDynString(UN_FACEPLATE_BUTTON_RESET_TEMPORARY_AS_FULL_STOP, UN_FACEPLATE_BUTTON_FORCED_MODE, 
							UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION, UN_FACEPLATE_BUTTON_DEBLOCK_ALARM_ACTION, 
							UN_FACEPLATE_BUTTON_SET_TEMPORARY_AS_FULL_STOP, UN_USER_EXPERT));
	}
        
	if (admin)
	{
          if(bActionDefined)
            dynAppend(dsAccess, strsplit(sAdminAction, UN_ACCESS_CONTROL_SEPARATOR));
          else
	    dynAppend(dsAccess, makeDynString(UN_GENERAL_CONFIGURATION, UN_USER_ADMIN));	
	}
//DebugN("unGenericButtonFunctionsHMI_ButtonUserAccess", sDpName, sDpType, dsAccess, sOperAction, sExpAction, sAdminAction, operator, expert, admin);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericButtonFunctionsHMI_getPanelFileName
/**
Purpose: get the current panel name

Parameters:
  sPanelFileName, string, output, panel file name

  
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericButtonFunctionsHMI_getPanelFileName(string &sPanelFileName)
{
  int iTime, iTimeChild;
  string sModuleName, sFileName;

  dpGet( myUiDpName()+".ChildPanelOn.ModuleName", sModuleName, 
                  myUiDpName()+".ChildPanelOn.FileName", sFileName, 
                  myUiDpName()+".ChildPanelOn.ModuleName:_online.._stime", iTimeChild);
  iTime = (int)getCurrentTime();
  if((sModuleName == myModuleName()) && ((iTime-iTimeChild) < 5))
    sPanelFileName = sFileName;
  else
    sPanelFileName = "";
//DebugN("unGenericButtonFunctionsHMI_getPanelFileName", sPanelFileName, sModuleName, sFileName, iTime, iTimeChild, iTime-iTimeChild);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericButtonFunctionsHMI_setPanelAccessControlList
/**
Purpose: set the list of access control domain and priviledge action of the panel

Parameters:
  sPanelFileName, string, input, panel file name
  sAccessControlDomainList, string, input, access control domain list
  sOperator, string, input, action list for operator priviledge
  sExpert, string, input, action list for expert priviledge
  sAdmin, string, input, action list for admin priviledge


Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericButtonFunctionsHMI_setPanelAccessControlList(dyn_string dsMenuFileAccessControl) synchronized(g_dsMenuFileAccessControl)
{
  g_dsMenuFileAccessControl = dsMenuFileAccessControl;
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericButtonFunctionsHMI_getPanelAccessControl
/**
Purpose: get the list of access control domain and priviledge action of the panel

Parameters:
  sPanelFileName, string, input, panel file name
  sAccessControlDomainList, string, output, access control domain list
  sOperator, string, output, action list for operator priviledge
  sExpert, string, output, action list for expert priviledge
  sAdmin, string, output, action list for admin priviledge
  bFileExists, bool, output, true if file is in list/false if not


Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericButtonFunctionsHMI_getPanelAccessControl(string sPanelFileName, string &sAccessControlDomainList, string &sMonitor, string &sOperator, string &sExpert, string &sAdmin, bool &bFileExists) synchronized(g_dsMenuFileAccessControl)
{
  dyn_string dsFileName, dsRes, dsConf;
  int i, len;
  
  sAccessControlDomainList = "";
  sMonitor = "";
  sOperator = "";
  sExpert = "";
  sAdmin = "";
  dsFileName = g_dsMenuFileAccessControl;
  bFileExists = false;
  
  if(sPanelFileName != "") {
    dsRes = dynPatternMatch("*"+sPanelFileName+"*", dsFileName);
    if(dynlen(dsRes)>0){
      dsConf = strsplit(dsRes[1], UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
      // must be fileName|domainList|operPriv|expPriv|adminPriv
      
      //DebugN("unGenericButtonFunctionsHMI_getPanelAccessControl", dsConf);
      
      if(dynlen(dsConf) == 5) {
        bFileExists = true;
        sAccessControlDomainList = dsConf[2];
        sOperator = dsConf[3];
        sExpert = dsConf[4];
        sAdmin = dsConf[5];
        
        if(sOperator=="" && sExpert=="" && sAdmin=="")
          sMonitor = UN_USER_MONITOR;  
      }
    }
  }
  else { //g_dsDomainAccessControl, 
    len = dynlen(g_dsDomainAccessControl);
    for(i=1;i<=len;i++) {
      if(sAccessControlDomainList == "")
        sAccessControlDomainList = g_dsDomainAccessControl[i];
      else
        sAccessControlDomainList+=UN_ACCESS_CONTROL_SEPARATOR+g_dsDomainAccessControl[i];
    }
//DebugN("unGenericButtonFunctionsHMI_getPanelAccessControl empty", g_dsDomainAccessControl, sAccessControlDomainList);
  }
//if(sPanelFileName == "vision/Device.pnl")
//DebugTN("unGenericButtonFunctionsHMI_getPanelAccessControl", sPanelFileName/*, dsFileName*/, dsRes, sAccessControlDomainList, sOperator, sExpert, sAdmin, bFileExists);
//if(sAccessControlDomainList == "")
//  DebugN(sPanelFileName, dsRes);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericButtonFunctionsHMI_getUnicosPriviledges
/**
Extract standard UNICOS privileges (for specified domain) from a larger list of access rights, and returns them into a dyn_bool.

Typical use case is to extract a longer list of all user privileges (for all domains, for instance), then pass it
as the dsDomainPriviledges param, and have the 4 unicos privileges for the specified domain extracted.

If called with empty dsDomainPriviledgeList (default), a dyn_bool with 4 FALSE values will be returned

 @param dbPermissions, output, list of permissions for the 4 UNICOS privileges([1]=monitor, [2]=operator, [3]=expert, [4]=admin
 @param exceptionInfo, output, standard exception handling variable
 @param sDomain, input, optional: default="UNICOS", specify the domain in which privileges will be looked up
 @param dsDomainPriviledgeList, input, optional: defaults is empty, should contain the list of access rights names
                                (in the Domain:Privilege form) from which the UNICOS privileges are extracted
*/
unGenericButtonFunctionsHMI_getUnicosPriviledges(dyn_bool &dbPermissions, dyn_string &exceptionInfo, string sDomain = "UNICOS", dyn_string dsAccessRightList = makeDynString())
{
  bool hasOperator, hasExpert, hasAdmin, hasMonitor;

  // note: g_dsPrivilegeAccessControl is a global dyn_string with default names of UNICOS privs: "monitor","operator","expert","admin"

  if(dynlen(g_dsPrivilegeAccessControl) >=UNICOS_ACCESSCONTROL_PRIVILEDGE_NUMBER) {
    hasMonitor =  (dynContains(dsAccessRightList, sDomain+":"+g_dsPrivilegeAccessControl[1]) > 0);
    hasOperator = (dynContains(dsAccessRightList, sDomain+":"+g_dsPrivilegeAccessControl[2]) > 0);
    hasExpert =   (dynContains(dsAccessRightList, sDomain+":"+g_dsPrivilegeAccessControl[3]) > 0);
    hasAdmin =    (dynContains(dsAccessRightList, sDomain+":"+g_dsPrivilegeAccessControl[4]) > 0);
  }

  dbPermissions = makeDynBool(hasMonitor, hasOperator, hasExpert, hasAdmin);

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unGenericButtonFunctionsHMI_getUnicosPriviledges", "unGenericButtonFunctionsHMI_getUnicosPriviledges", sDomain,
                               dsAccessRightList, dbPermissions);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_closePanel
/**
Purpose: close a panel depending on the value of a variable. Closing is refused if the variable is set to false.

Parameters:	
	@param bClosePanel: bool, input, true: can close the panel, false, can not close the panel
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericButtonFunctionsHMI_closePanel(bool bClosePanel)
{
	int iRes;
	string deviceName, panelName;
	
	if(bClosePanel) 
		PanelOff();
	else 
		unGraphicalFrame_ChildPanelOnCentralModal("vision/unicosObject/General/MessageWait.pnl",myPanelName()+"/Info",makeDynString("$1:" + getCatStr("unGeneral","CANNOTCLOSEPANEL")));
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_getPanelAccessLevel
/**
Purpose: Get the panel access level

Parameters:	
Get the access priv of a file.

Parameters:	
	@param sFileName: string, input: file name
        @param bFileExists: bool, output, true if file is in list/false if not
	@param return, dyn_string, output: the file access priv
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
string unGenericButtonFunctionsHMI_getPanelAccessLevel(string sFileName, bool &bFileExists)
{
string sAccessControlDomainList, sOperator, sExpert, sAdmin, sMonitor;
string sFileAccess;
  
  unGenericButtonFunctionsHMI_getPanelAccessControl(sFileName, sAccessControlDomainList, sMonitor, sOperator, sExpert, sAdmin, bFileExists);
  if(sMonitor != "") 
    sFileAccess = sMonitor;     
  else if(sOperator != "")
    sFileAccess = sOperator;
  else if(sExpert != "")
    sFileAccess = sExpert;
  else if(sAdmin != "")
    sFileAccess = sAdmin;
  if(sFileAccess == "")
    sFileAccess = UN_USER_MONITOR;
  
//DebugN("unGenericButtonFunctionsHMI_getPanelAccessLevel", sFileName, sFileAccess);
  return sFileAccess;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_plotConfPanelselectCB
/**
Purpose: Get the access control for fwTrendingPlotConfPanel.pnl from JCOP

Parameters:	
Get the access priv of a file.

Parameters:	
	@param sUi: string, input: manager name
        @param sUser: string, input: user name
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericButtonFunctionsHMI_plotConfPanelselectCB(string sUi, string sUser)
{
  dyn_string exceptionInfo;
  bool bFileExists;
  string sPanelFileName = "fwTrending/fwTrendingPlotConfPanel.pnl";
  bool granted = FALSE;
  int i;

  unGenericButtonFunctionsHMI_isAccessAllowed(sPanelFileName, UN_GENERIC_USER_ACCESS, 
                unGenericButtonFunctionsHMI_getPanelAccessLevel(sPanelFileName, bFileExists), granted, exceptionInfo);
  setMultiValue("ButtonOK", "enabled", granted, "ButtonApply", "enabled", granted, "saveAsButton", "enabled", granted);
	
  for( i = 1; i <= fwTrending_MAX_NUM_CURVES; i++)
  {
    setValue("ArchiveButton" + i, "enabled", granted);		
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_plotsPageselectCB
/**
Purpose: Get the access control for fwTrendingPlotsPage.pnl from JCOP

Parameters:	
Get the access priv of a file.

Parameters:	
	@param sUi: string, input: manager name
        @param sUser: string, input: user name
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericButtonFunctionsHMI_plotsPageselectCB(string sUi, string sUser)
{
  dyn_string exceptionInfo;
  bool bFileExists;
  string sPanelFileName = "fwTrending/fwTrendingPlotsPage.pnl";
  bool granted = FALSE;
  int i;

  unGenericButtonFunctionsHMI_isAccessAllowed(sPanelFileName, UN_GENERIC_USER_ACCESS, 
                unGenericButtonFunctionsHMI_getPanelAccessLevel(sPanelFileName, bFileExists), granted, exceptionInfo);
  setMultiValue("ButtonOK", "enabled", granted, "ButtonApply", "enabled", granted, "saveAsButton", "enabled", granted);	
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_TrendingPageselectCB
/**
Purpose: Get the access control for fwTrendingPage.pnl from JCOP

Parameters:	
Get the access priv of a file.

Parameters:	
	@param sUi: string, input: manager name
        @param sUser: string, input: user name
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericButtonFunctionsHMI_TrendingPageselectCB(string sUi, string sUser)
{
  dyn_string exceptionInfo;
  bool bFileExists;
  string sPanelFileName = "fwTrending/fwTrendingPage.pnl";
  bool granted = FALSE;
  int i;

  unGenericButtonFunctionsHMI_isAccessAllowed(sPanelFileName, UN_GENERIC_USER_ACCESS, 
                unGenericButtonFunctionsHMI_getPanelAccessLevel(sPanelFileName, bFileExists), granted, exceptionInfo);
// disable /enalbe page conf options
  pageButton.enableItem(CASCADE_SAVE_PLOTS, granted);
  pageButton.enableItem(CASCADE_PAGE_CONFIG, granted);
  pageButton.enableItem(CASCADE_SAVE_AS, granted);

  g_bAccessGranted = granted;
  if(g_bDpAvailable)
  pageButton.enableItem(CASCADE_SAVE, granted);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_TrendingHistogramConfigselectCB
/**
Purpose: Get the access control for fwTrendingHistogramConfig.pnl from JCOP

Parameters:	
Get the access priv of a file.

Parameters:	
	@param sUi: string, input: manager name
        @param sUser: string, input: user name
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericButtonFunctionsHMI_TrendingHistogramConfigselectCB(string sUi, string sUser)
{
  dyn_string exceptionInfo;
  bool bFileExists;
  string sPanelFileName = "fwTrending/fwTrendingHistogramConfig.pnl";
  bool granted = FALSE;
  int i;

  unGenericButtonFunctionsHMI_isAccessAllowed(sPanelFileName, UN_GENERIC_USER_ACCESS, 
                unGenericButtonFunctionsHMI_getPanelAccessLevel(sPanelFileName, bFileExists), granted, exceptionInfo);
  setMultiValue("ButtonOK", "enabled", granted, "ButtonApply", "enabled", granted, "saveAsButton", "enabled", granted);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_TrendingTrendselectCB
/**
Purpose: Get the access control for fwTrendingTrend.pnl from JCOP

Parameters:	
Get the access priv of a file.

Parameters:	
	@param sUi: string, input: manager name
        @param sUser: string, input: user name
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericButtonFunctionsHMI_TrendingTrendselectCB(string sUi, string sUser)
{
  dyn_string exceptionInfo;
  bool bFileExists;
  string sPanelFileName = "fwTrending/fwTrendingTrend.pnl";
  bool granted = FALSE;
  int i;
  string ref=$sRefName, tooltip;
  shape cascadeButton;

  if(ref!="")
    ref+=".";

  unGenericButtonFunctionsHMI_isAccessAllowed(sPanelFileName, UN_GENERIC_USER_ACCESS, 
                unGenericButtonFunctionsHMI_getPanelAccessLevel(sPanelFileName, bFileExists), granted, exceptionInfo);
  g_bAccessGranted = granted;
  if(g_bDpAvailable)
    setValue(ref+"saveSettings", "enabled", granted);

  cascadeButton = getShape(ref+"OtherCascadeButton");
  cascadeButton.enableItem("8", granted);	
  cascadeButton.enableItem("9", granted);	
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericButtonFunctionsHMI_TrendingHistogramselectCB
/**
Purpose: Get the access control for fwTrendingHistogram.pnl from JCOP

Parameters:	
Get the access priv of a file.

Parameters:	
	@param sUi: string, input: manager name
        @param sUser: string, input: user name
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericButtonFunctionsHMI_TrendingHistogramselectCB(string sUi, string sUser)
{
  dyn_string exceptionInfo;
  bool bFileExists;
  string sPanelFileName = "fwTrending/fwTrendingHistogram.pnl";
  bool granted = FALSE;
  int i;
  string ref=$sRefName, tooltip;
  shape cascadeButton;

  if(ref!="")
    ref+=".";

  unGenericButtonFunctionsHMI_isAccessAllowed(sPanelFileName, UN_GENERIC_USER_ACCESS, 
                unGenericButtonFunctionsHMI_getPanelAccessLevel(sPanelFileName, bFileExists), granted, exceptionInfo);
  g_bAccessGranted = granted;
  if(g_bDpAvailable)
    setValue(ref+"saveSettings", "enabled", granted);

  cascadeButton = getShape(ref+"OtherCascadeButton");
  cascadeButton.enableItem("7", granted);	
  cascadeButton.enableItem("8", granted);	
}

//------------------------------------------------------------------------------------------------------------------------

//@}

