/**@name LIBRARY: unGenericDpFunctions.ctl

@author: Herve Milcent (LHC-IAS)

Creation Date: 31/04/2002 

Modification History:

 - @jira{IS-986}: decreased delay in delete function unGenericDpFunctions_deleteDevice()
  
 - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity
  
  12/05/2011: Herve
    - IS-519: Remove all dependencies on graphical object from the Import device panel in the core import 
    functions, to be able to use the unicos front-end/device check/import functions from a WCCOActrl 
    
  06/05/2011: Herve
  -IS-527: Configuration dpe (dyn_string) for all UNICOS types
  -IS-529: Import: delete devices and front-end, remove the device and any associated DP from all the mail/SMS category and application
  
  16/11/2010: Frederic
    - update unGenericDpFunctions_getHostName() - the getHostByName() function does not return the same result with PVSS 3.8 when other network devices are installed on the local machine

  17/06/2010: Herve
  - improvement IS-76, check access right slow when a user is in a lot of access control domain.
  modified function: unGenericDpFunctions_getAccessControl4PriviledgeRigth and unGenericDpFunctions_getAccessControlPriviledgeRigth
  
  07/06/2010: Herve
  - add function used in postInstall script: unGenericDpFunctions_InstallFile, unGenericDpFunctions_getFile
  
  09/02/2010: Herve
  - fix bug in unGenericDpFunctions_getAccessControlPriviledgeRigth missing parameter when calling the function
  unGenericButtonFunctionsHMI_getPanelAccessControl
  
  11/09/2009: Frederic
	add unGenericDpFunctions_getAccessControl4PriviledgeRigth
 
  10/08/2009: Herve
    - front-end proxy and trend implementation
    . modified functions: unGenericDpFunctions_getFaceplateRange, unGenericDpFunctions_setFaceplateRange
    
  04/08/2009: Herve
    - device proxy implementation
    new functions: unGenericDpFunctions_deviceTypeHasProxy, unGenericDpFunctions_getProxyDeviceList,
    unGenericDpFunctions_hasProxy, unGenericDpFunctions_setProxyDeviceList, unGenericDpFunctions_getProxyDP
    modified functions: unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE, unGenericDpFunctions_getFaceplateRange
    
  17/02/2009: Herve
    - FE widget & faceplate implementation
	modified functions: unGenericDpFunctions_getFaceplate, unGenericDpFunctions_getDeviceWidgetList, unGenericDpFunctions_getDeviceWidget
	new function: unGenericDpFunctions_isFrontEnd
	
  26/11/2008: Herve
    - unGenericDpFunctions_deletePlcDevices: use Qt progress bar widget
    
  08/08/2008: Frederic
	- add the function unGenericDpFunctions_resetHostName to reset the Data Server HostName
		
  23/04/2008: Herve
    - utility debug function
  
  18/02/2008: Herve
    - unGenericDpFunctions_createUser, unGenericDpFunctions_setPrivDomain: deprecated functions
    
  08/02/2008: Herve
    - default domain as string instead of int in unGenericDpFunctions_getAccessControlPriviledgeRigth.
    
  07/02/2008: Herve
    - add default domain number to unGenericDpFunctions_getAccessControlPriviledgeRigth
    
  28/12/2007: Herve
    - device acccess control
    
	14/11/2007: Frederic
		- update unGenericDpFunctions_addDpeinDpFunction

	13/07/2007: Frederic
		- update unGenericDpFunctions_setHostName() to also save the pmonPort
		- add unGenericDpFunctions_getpmonPort()

	26/07/2007: Herve
		- rename unGenericDpFunctions_dpAliasToName to _unGenericDpFunctions_dpAliasToName
		- optimize unGenericDpFunctions_dpAliasToName: call dpAliasToName and if "" result call the _unGenericDpFunctions_dpAliasToName
		
	14/12/2006: Herve
		- new function to create user and set group priviledge
		- bug in unGenericDpFunctions_checkDeviceType: add system name
		
	27/09/2006: Herve
		- in unGenericDpFunctions_getTrendDPE add DPEL_TIME.
		
	29/06/2006: Frederic
		- new functions: unGenericDpFunctions_createDpeDpFunction, unGenericDpFunctions_deleteDpFunction, unGenericDpFunctions_addDpeinDpFunction

	09/05/2006: Herve
		- new function: unGenericDpFunctions_checkDeviceType
		
	04/05/2006: Herve
		- new function unGenericDpFunctions_isMaskEvent
		
	27/04/2006: Herve
		- mask event: new function unGenericDpFunctions_getMaskEvent
		
	10/04/2006: Herve
		- implementation of link between devices
			. new function: unGenericDpFunctions_getDeviceLink

	14/01/2006: Herve
		- implementation of multi domain, multi nature. Function modified: unGenericDpFunctions_deleteDevice

	26/09/2005: Frederic
		- add the function unGenericDpFunctions_getHostName to get the Data Server HostName
		- add the function unGenericDpFunctions_setHostName to set the Data Server HostName

	12/07/2005: Herve
		- add the function unGenericDpFunctions_getDeviceDefinitionDPEDescription to get the description from the FwDeviceDefinition
		
	8/07/2005: Herve
		- unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE: bug if alias like a.b.c and a.b exists. 
		WARNING: still bug if the UNICOS DPE a.b.c.d and a.b.d exists: if d is the PVSS DPE leaf and present in both dptype.
		
	11/04/2005: Frederic
		- add unGenericDpFunctions_getApplication
	
	11/04/2005: Herve
		- remove unGenericDpFunctions_deletePlc from unGenericDpFunctions_deletePlcDevices and put it into unConfigGenericFunctions_deleteCommand
		
	11/03/2004: Herve
		- unGenericDpFunctions_getDescription: bug, if description is "" the function retunrs DPE.
		
	04/03/2005: Herve
		- superbug in the unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE if the alias contains .
			look for all the possible case of alias by doing a split on .
		limitation: e.g. AA.DD.FF.PosSt if AA.DD and AA.DD.FF exist then AA.DD will be taken first ==> may be the wrong alias. 
		
	03/03/2005: Herve
		- bug unGenericDpFunctions_getDPE: remove all the empty dyn_string before getting the dpe list
		
	10/02/2005: Herve
		- optimize the unGenericDpFunctions_dpAliasToName
		
	28/01/2005: Herve
		- bug of the dpAliasToName, replace the call by unGenericDpFunctions_dpAliasToName. 
		- unGenericDpFunctions_dpAliasToName: call the dpGetAllAliases with alias filter set to the alias and dp filter set to the systemName:*.**
		
	06/12/2004: Herve
		-signature of unGenericDpFunctions_getPlcDevices modifed: system name added

	18/11/2004: Herve
		- unGenericDpfunctions_groupGetNames renamed to unGenericDpFunctions_groupGetNames
		- unGenericDpfunctions_groupNameToDpName renamed to unGenericDpFunctions_groupNameToDpName
		- add a new function unGenericDpFunctions_getDomainNatureListFromSystem
		- unGenericDpFunctions_groupGetNames, unGenericDpFunctions_groupNameToDpName new parameter: systemName
		- the function unGenericDpFunctions_getDomainNatureList calls unGenericDpFunctions_getDomainNatureListFromSystem
		
	05/11/2004: Herve
		- bug for the dpAliasToName call the system name must be given otherwise the alias on the remote system can be converted to a dp.
		function unGenericDpFunctions_getWidgetDpName modified.
		
	20/10/2004: Frederic
		- add unGenericDpFunctions_getAlarmDescription
	
	15/10/2004: Herve
		- add unGenericDpFunctions_getTrendDPE and unGenericDpFunctions_getDPE
		
	22/09/2004: Herve
		migration to v3.0
		change _UnPanel, _FwTrendingPlot, _FwTrendingPage by constant

	02/04/2004: Herve
		add unGenericDpFunctions_getFrontEndDeviceType: get the list of all front-end devices
		add unGenericObject_getFrontEndFunctions: get the list of functions

	17/02/2004: Herve
		add unGenericDpFunctions_ as a prefix for all the functions that modify the config file
		
	25/11/2003: Herve
		modify the _UnPlc dpName: use PLC Name instead PLC number. Function modified:
		unGenericDpFunctions_deletePlc 

	5/09/2003 Herve: remove the UN_WIDGET_PREFIX and UN_WIDGET_PATH for this 
		add the following function unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName
		modify the function unGenericDpFunctions_setDeviceWidget to call unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName
		modify unGenericDpFunctions_getDeviceWidget add the dsWidgetFileName param. This function is only used in the DeviceOverview
	16/09/2003 Herve:
		add the update of the progressbar while deleting devices: modification in funcion unGenericDpFunctions_deletePlcDevices

version 1.0

External Functions: 
	. unGenericDpFunctions_getFaceplate: to get the faceplate panel of a device
	. unGenericDpFunctions_getContextPanel: to get the contextual panel of a device
	. unGenericDpFunctions_setCallBack_lock: set a call back function when a device is selected or deselected
	. unGenericDpFunctions_disconnectCallBack_lock: disconnect the call back function when a device is selected or deselected
	. unGenericDpFunctions_getSystemName: to get the DP system name substring, to be used instead of dpSubStr in distributed system 
	. unGenericDpFunctions_getDpName: to get the DP name substring, to be used instead of dpSubStr in distributed system 
	. unGenericDpFunctions_getParameters: to get diagnostic, html link, default panel, domain, nature, name
	. unGenericDpFunctions_setParameters: to set diagnostic, html link, default panel, domain, nature, name
	. unGenericDpFunctions_getDescription: this function works like dpGetDescription() if description exists, else return an empty string.
	. unGenericDpFunctions_getAlias: this function works like dpGetAlias() if alias exists, else return the DPE name.
	. unGenericDpFunctions_getUnicosObjects: returns the unicos objects name.
	. unGenericDpFunctions_getFaceplateRange: returns the faceplate range configuration.
	. unGenericDpFunctions_setFaceplateRange: sets the faceplate range configuration.
	. unGenericDpFunctions_getGroups: get device domain and nature.
	. unGenericDpFunctions_getDomainNatureList: get defined domains for unicos.
	. unGenericDpFunctions_waitForSingleValue: wait for new dpe value (with timeout) (anytype dpe)
	. unGenericDpFunctions_waitForDynValue: wait for new dpe value (with timeout) (dyn_anytype dpe)
	. unGenericDpFunctions_getWidgetDpName: get device name from widget identifier 
	. unGenericDpFunctions_getDeviceWidgetList: get available widgets for current device 
	. unGenericDpFunctions_getDeviceWidget: get device widget
	. unGenericDpFunctions_setDeviceWidget: set device widget
	. unGenericDpFunctions_changeDpParameter: change one of device parameter
	. unGenericDpFunctions_groupGetNames: return all groups
	. unGenericDpFunctions_groupNameToDpName: convert group name to dp name (based on PVSS function groupNameToDpName() from dpGroups.ctl)
	. unGenericDpFunctions_deleteDevice: delete unicos device
	. unGenericDpFunctions_deletePlc: delete Plc config
	. unGenericDpFunctions_deletePlcDevices: delete devices that belongs to a specified PLC, subapplication, object type, object number
	. unGenericDpFunctions_getPlcDevices: get devices that belongs to a specified PLC, subapplication, object type
	. unGenericDpFunctions_getBitDescription: get current bit description (saved in _UnObject dp)
	. unGenericDpFunctions_getAvailableRanges: get ranges defined for this device
	. unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName: returns the widget name from the widget file name
	. unGenericDpFunctions_getApplication: returns Application name from DpName
	. unGenericDpFunctions_getHostName: to get the Data Server HostName
	. unGenericDpFunctions_setHostName: to set the Data Server HostName
	. unGenericDpFunctions_resetHostName: to reset the Data Server HostName
	. unGenericDpFunctions_createDpeDpFunction: to create Datapoint function configuration
	. unGenericDpFunctions_deleteDpFunction: to delete Datapoint function configuration
	. unGenericDpFunctions_addDpeinDpFunction: to add Dpe in a Datapoint function configuration
	
Internal Functions: 
	. unGenericDpFunctions_waitForSingleValueCB: callback function for unGenericDpFunctions_waitForSingleValue function
	. unGenericDpFunctions_waitForDynValueCB: callback function for unGenericDpFunctions_waitForDynValue function
	
Purpose: 
This library contains generic functions.

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

// to use the _acGetDptStructureFromDp()
#uses "ac.ctl"
#uses "unProgressBar.ctl"

#uses "libunCore/unCoreDeprecated.ctl"

//@{

// unGenericDpFunctions_getFaceplate
/**
Purpose:
to get the faceplate panel of a device.

	@param sDeviceName: string, input, the name of the device
	@param sDeviceName: string, output, the panel name of the faceplate of the device is returned here
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctions_getFaceplate(string sDeviceName, string &sPanelName, dyn_string &exceptionInfo)
{
  string sTypeName;
  dyn_string exceptionInfoTemp;
  int iObject;

  sTypeName = dpTypeName(sDeviceName);
  // 1. Initialize data if not already done
  sPanelName = "";
  if (globalExists("g_dsObjectList"))
  {
    if (dynlen(g_dsObjectList) <= 0)
    {
      unGenericObject_InitializeObjects(exceptionInfoTemp);
    }
  }
  else
  {
    unGenericObject_InitializeObjects(exceptionInfoTemp);
  }
// 2. Get object
  iObject = dynContains(g_dsObjectList, sTypeName);
  if(iObject <= 0)
  {
    if(mappingHasKey(g_mFrontEndConfigs, sTypeName))
      sPanelName = g_mFrontEndConfigs[sTypeName][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_FACEPLATE];
    else
      fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_getFaceplate: unknown object", "");
//DebugN(g_ddsObjectConfigs[4], g_mFrontEndConfigs[sTypeName], sPanelName, g_mFrontEndConfigs[sTypeName][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_FACEPLATE], UN_GENERICOBJECT_FUNCTION_LENGTH, UN_GENERICOBJECT_FIELD_FACEPLATE);
  }
  else
  {
// 3. Get select field
    sPanelName = g_ddsObjectConfigs[iObject][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_FACEPLATE];
  }
}

// unGenericDpFunctions_getContextPanel
/**
Purpose:
to get the contextual panel of a device.

	@param sDeviceName: string, input, the name of the device
	@param sDeviceName: string, output, the panel name of the contextual panel of the device is returned here
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctions_getContextPanel(string sDeviceName, string &sPanelName, dyn_string &exceptionInfo)
{
	string sTypeName;
	dyn_string exceptionInfoTemp;
	int iObject;
	
	sTypeName = dpTypeName(sDeviceName);
	switch(sTypeName)
		{
		case UN_PANEL_DPTYPE:
		case UN_TRENDTREE_PAGE:
		case UN_TRENDTREE_PLOT:
			sPanelName = "vision/graphicalFrame/contextualButton.pnl";
			break;
		default:
// 1. Initialize data if not already done
			sPanelName = "";
			if (globalExists("g_dsObjectList"))
				{
				if (dynlen(g_dsObjectList) <= 0)
					{
					unGenericObject_InitializeObjects(exceptionInfoTemp);
					}
				}
			else
				{
				unGenericObject_InitializeObjects(exceptionInfoTemp);
				}
// 2. Get object
			iObject = dynContains(g_dsObjectList, sTypeName);
			if (iObject <= 0)
				{
				fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_getFaceplate: unknown object", "");
				}
			else
				{
// 3. Get select field
				sPanelName = g_ddsObjectConfigs[iObject][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_BUTTON];
				}
			break;
		}
}

// unGenericDpFunctions_getParameters
/**
Purpose: to get diagnostic, html link, default panel, domain, nature

Parameter:
	@param sDp: string, input, device name
	@param dsParameters: dyn_string, output, returns the parameters. Always : dynlen(dsParameters) = UN_PARAMETER_LENGTH
	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. Use constants defined at the beginning of the library
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctions_getParameters(string sDp, dyn_string &dsParameters)
{
	string deviceName, description;
	int lengthDifference, i;
	
	// 1. Get deviceName + ".ProcessInput" description that contains parameters (string UN_PARAMETER_DELIMITER delimited)
	deviceName = unGenericDpFunctions_getDpName(sDp);
	description = unGenericDpFunctions_getDescription(deviceName + ".ProcessInput");
		
	// 2. Split description and complete with empty string in order to obtain an UN_PARAMETER_LENGTH length dyn_string
	dsParameters = strsplit(description, UN_PARAMETER_DELIMITER);
	lengthDifference = UN_PARAMETER_LENGTH - dynlen(dsParameters);
	for(i=1; i<=lengthDifference; i++)
		{
		dynAppend(dsParameters,"");
		}
}

//--------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_setParameters
/**
Purpose: to set diagnostic, html link, default panel, domain, nature

Parameter:
	@param sDp: string, input, device name
	@param dsParameters: dyn_string, input, contains the parameters
	@param exceptionInfo: dyn_string, output, for errors
	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl) and UI (WCCOAui)

Constraints:
	. Use constants defined at the beginning of the library
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctions_setParameters(string sDp, dyn_string dsParameters, dyn_string &exceptionInfo)
{
	string deviceName, description = "";
	int length, i, iRes;
	
	// 1. Build parameters string
	length = dynlen(dsParameters);
	if (length > UN_PARAMETER_LENGTH)
		{
		length = UN_PARAMETER_LENGTH;
		}

	for(i=1; i<=length; i++)
		{
		//Remove empty domain/nature in multi domain/nature
		if ((i==UN_PARAMETER_DOMAIN) || (i==UN_PARAMETER_NATURE))
			dsParameters[i]=unConfigGenericFunctions_FormatDomainNature(dsParameters[i]);
			
		description = description + UN_PARAMETER_DELIMITER + dsParameters[i];
		}

	if (description != "")
		{
		description = substr(description,1);
		}
	
	// 2. Set deviceName + ".ProcessInput" description that contains parameters
	deviceName = unGenericDpFunctions_getDpName(sDp);
	iRes = dpSetDescription(deviceName + ".ProcessInput", description);
	if (iRes < 0)
		{
		fwException_raise(exceptionInfo,"ERROR","unGenericDpFunctions_setParameters: " + deviceName + getCatStr("unGeneral", "ERRORSETPARAMETERS"),"");
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getAlarmDescription
/**
Purpose: get Alarm descriptions of given datapoint element

Parameters:
		sDpe: string, input, datapoint element
		dsDescription: dyn_string, output, descriptions

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000 and XP
	. distributed system: yes.
*/

unGenericDpFunctions_getAlarmDescription(const string sDpe, dyn_string &dsDescription)
{
string sDescription, shortDp;
int i, len;
	
	dynClear(dsDescription);

	//Get descritpion
	sDescription = dpGetDescription(sDpe,0); // return description or sDp (without system name) if empty
		
	//Description is empty ?
	shortDp = substr(sDpe, strpos(sDpe,":") + 1);
	
	if (shortDp != sDescription)
		{
		dsDescription=strsplit(sDescription, UN_ALERT_DESCRIPTION_DELIMITER);				
		len=dynlen(dsDescription);

		if (len<UN_CONFIG_ALARM_DESCRIPTION_LENGTH)
			{
			for(i=len+1; i<=UN_CONFIG_ALARM_DESCRIPTION_LENGTH; i++)
					{
					dsDescription[i]="";		
					}
			}
		}
	else
		{
		for (i=1; i<UN_CONFIG_ALARM_DESCRIPTION_LENGTH; i++)
			{
			dsDescription[i]="";
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getUnicosObjects
/**
Purpose: return the unicos objects name.

Parameter: dsUnicosObjects, dyn_string, output, result of the function
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. For each unicos object, a DPE must be created in DP type "_UnObjects". 
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctions_getUnicosObjects(dyn_string &dsUnicosObjects)
{
	dyn_string dsTemp;
	int i, length;
	string sSystemName;
	
	sSystemName = getSystemName();
	dsTemp = dpNames("*" + UN_OBJECT_EXTENSION, UN_OBJECT_DPTYPE);	// Get DPEs
	length = dynlen(dsTemp);
	for(i=1;i<=length;i++)					// For each element, delete system name
		{
		dsTemp[i] = substr(dsTemp[i], strlen(sSystemName));
		dsTemp[i] = substr(dsTemp[i], 0, strlen(dsTemp[i]) - strlen(UN_OBJECT_EXTENSION));
		}
	dynUnique(dsTemp);
	dynSortAsc(dsTemp);
	dsUnicosObjects = dsTemp;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getFrontEndFunctions
/**
Purpose: return a list of available functions for a FrontEnd device

Parameters:
	- sDpType, string, input, dpType
	- dsFunctions, dyn_string, output, list of functions that can be accessible using constants defined in unicos_declarations.ctl, unicos_declarations_core.ctl
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getFrontEndFunctions(string sDpType, dyn_string &dsFunctions, dyn_string &exceptionInfo)
{
	dyn_string exceptionInfoTemp, dsTemp;
	int i, iObject, iRes;

// 1. Initialize data if not already done
	dynClear(dsFunctions);
	unGenericDpFunctions_getFrontEndDeviceType(dsTemp);
	
// 2. Get object
	iObject = dynContains(dsTemp, sDpType);
	if (iObject <= 0)
		{
		fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_getFrontEndFunctions: unknown object "+sDpType, "");
		}
	else
	{
// 3. Get object functions
		iRes = dpGet(getSystemName() + sDpType + UN_OBJECT_EXTENSION + ".config.generationFunctions", dsTemp);
	}

	while(dynlen(dsTemp)<UN_FRONTEND_FUNCTION_LENGTH)
		dsTemp[dynlen(dsTemp)+1] = "";
	dsFunctions = dsTemp;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getFrontEndDeviceType
/**
Purpose: return the front-end device names.

Parameter: dsUnicosObjects, dyn_string, output, result of the function
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. For each front-end device, a DPE must be created in DP type "_UnFrontEnd". 
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctions_getFrontEndDeviceType(dyn_string &dsFrontEnd)
{
	dyn_string dsTemp;
	int i, length;
	string sSystemName;
	
	sSystemName = getSystemName();
	dsTemp = dpNames("*" + UN_OBJECT_EXTENSION, UN_FRONTEND_DPTYPE);	// Get DPEs
	length = dynlen(dsTemp);
	for(i=1;i<=length;i++)					// For each element, delete system name
		{
		dsTemp[i] = substr(dsTemp[i], strlen(sSystemName));
		dsTemp[i] = substr(dsTemp[i], 0, strlen(dsTemp[i]) - strlen(UN_OBJECT_EXTENSION));
		}
	dynUnique(dsTemp);
	dynSortAsc(dsTemp);
	dsFrontEnd = dsTemp;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_isFrontEnd
/**
Purpose: return true if sDpName contains Front-End dpType starting at the first character position.

Parameter: 
  - sDpName: string, input, the dpName
  - return value: bool, output, true=if the sDpName contains a Front-end type DP starting at first position/false otherwise
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. For each front-end device, a DPE must be created in DP type "_UnFrontEnd". 
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
bool unGenericDpFunctions_isFrontEnd(string sDpName)
{
  string dpName, dpNoSystem, sSystemName, sKey;
  int i, len, iPos;
  bool bFound;
  
  dpName = substr(sDpName, 0, strpos(sDpName,"."));
  sSystemName = unGenericDpFunctions_getSystemName(sDpName);
	
  if (dpName == "")
  {
    dpName = sDpName;
  }
  dpNoSystem = substr(dpName, strpos(dpName,":") + 1, strlen(dpName));
  if (dpNoSystem == "")
  {
    dpNoSystem = dpName;
  }
  len = mappinglen(g_mFrontEndConfigs);
  for(i=1;(i<=len) & !bFound;i++)
  {
    sKey = mappingGetKey(g_mFrontEndConfigs, i);
    if(sKey == "_UnPlc")
       sKey = "_unPlc";
    iPos = strpos(dpNoSystem, sKey);
    if(iPos == 0)
      bFound = true;
  }
//DebugN("unGenericDpFunctions_isFrontEnd", bFound, iPos, sDpName, dpNoSystem, dpNoSystem, sKey);

  return bFound;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getFaceplateRange
/**
Purpose: returns the faceplate range configuration.

Parameter: sDp, string, input, device name
		   dsDpe, dyn_string, output, list of DPEs with range configuration
		   dfMax, dyn_float, output, range max of dpe defined in dsDpe
		   dfMin, dyn_float, output, range min of dpe defined in dsDpe
		   
dsDpe, dfMax, dfMin have same length. 
dsDpe contains "relative" dpe ie PCLHC601:AnalogInput1.ProcesInput.PosSt is represented by the string "PosSt" if PCLHC601:AnalogInput1 
		is the string given in parameter sDp.
The faceplate range configuration is saved in the 7th parameter of the device parameters. It is represented by a ":" string delimited :
dpe1:max1:min1:dpe2:max2:min2 etc.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getFaceplateRange(string sDp, dyn_string &dsDpe, dyn_float &dfMax, dyn_float &dfMin)
{
	string deviceName, rangeConfig, currentDpe;
	dyn_string dsParameters;
	dyn_string rangeConfigSplit;
	float currentMax, currentMin;
	int i, length;
	bool analogOk;
  string sTypeName, sDescription;
  dyn_string exceptionInfoTemp;
  int iObject;
	
	dynClear(dsDpe);
	dynClear(dfMax);
	dynClear(dfMin);
	deviceName = unGenericDpFunctions_getDpName(sDp);
        
  sTypeName = dpTypeName(deviceName);
  // 1. Initialize data if not already done
  if (globalExists("g_dsObjectList"))
  {
    if (dynlen(g_dsObjectList) <= 0)
    {
      unGenericObject_InitializeObjects(exceptionInfoTemp);
    }
  }
  else
  {
    unGenericObject_InitializeObjects(exceptionInfoTemp);
  }
// 2. Get object
  iObject = dynContains(g_dsObjectList, sTypeName);
  if(iObject <= 0)
  {
    sDescription = unGenericDpFunctions_getDescription(deviceName+".configuration");
    dsParameters[UN_PARAMETER_FACEPLATE_RANGE]=sDescription;
//DebugN(deviceName, sDescription, UN_PARAMETER_FACEPLATE_RANGE, dsParameters);
  }        
  else
  {
	unGenericDpFunctions_getParameters(deviceName, dsParameters);		// Get parameters, range config is in position 7
  }
	rangeConfig = dsParameters[UN_PARAMETER_FACEPLATE_RANGE];
	if (rangeConfig != "")
		{
		rangeConfigSplit = strsplit(rangeConfig,UN_PARAMETER_RANGE_DELIMITER);
		length = dynlen(rangeConfigSplit);
		length = length / 3;
		for(i=1;i<=length;i++)
			{
			currentDpe = rangeConfigSplit[3*i-2];
			if (dpExists(deviceName + ".ProcessInput." + currentDpe) ||
				dpExists(deviceName + ".ProcessOutput." + currentDpe))	// Check if dpe exists
				{
				analogOk = true;
				if (sscanf(rangeConfigSplit[3*i-1], "%f", currentMax) <= 0)	// Check if max could be read as a float value
					{
					analogOk = false;
					}
				if (sscanf(rangeConfigSplit[3*i], "%f", currentMin) <= 0)	// Check if min could be read as a float value
					{
					analogOk = false;
					}
				if (analogOk)
					{
					if (currentMax > currentMin)
						{
						dynAppend(dsDpe, currentDpe);
						dynAppend(dfMax, currentMax);
						dynAppend(dfMin, currentMin);
						}
					}
				}
                        else { // check if proxy
                          if(strpos(currentDpe, UN_PROXY_DELIMITER) > 0) {
				analogOk = true;
				if (sscanf(rangeConfigSplit[3*i-1], "%f", currentMax) <= 0)	// Check if max could be read as a float value
					{
					analogOk = false;
					}
				if (sscanf(rangeConfigSplit[3*i], "%f", currentMin) <= 0)	// Check if min could be read as a float value
					{
					analogOk = false;
					}
				if (analogOk)
					{
					if (currentMax > currentMin)
						{
						dynAppend(dsDpe, currentDpe);
						dynAppend(dfMax, currentMax);
						dynAppend(dfMin, currentMin);
						}
					}
                          }
                          else if (dpExists(deviceName + "." + currentDpe)){
				analogOk = true;
				if (sscanf(rangeConfigSplit[3*i-1], "%f", currentMax) <= 0)	// Check if max could be read as a float value
					{
					analogOk = false;
					}
				if (sscanf(rangeConfigSplit[3*i], "%f", currentMin) <= 0)	// Check if min could be read as a float value
					{
					analogOk = false;
					}
				if (analogOk)
					{
					if (currentMax > currentMin)
						{
						dynAppend(dsDpe, currentDpe);
						dynAppend(dfMax, currentMax);
						dynAppend(dfMin, currentMin);
						}
					}
                          }
                        }
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_setFaceplateRange
/**
Purpose: sets the faceplate range configuration.

Parameter: sDp, string, input, device name
		   dsDpe, dyn_string, input, list of DPEs with range configuration
		   dfMax, dyn_float, input, range max of dpe defined in dsDpe
		   dfMin, dyn_float, input, range min of dpe defined in dsDpe
		   exceptionInfo, dyn_string, output, for errors
		   
dsDpe, dfMax, dfMin must have same length. 
dsDpe contains "relative" dpe ie PCLHC601:AnalogInput1.ProcesInput.PosSt is represented by the string "PosSt" if PCLHC601:AnalogInput1 
		is the string given in parameter sDp.
The faceplate range configuration is saved in the 7th parameter of the device parameters. It is represented by a ":" string delimited :
dpe1:max1:min1:dpe2:max2:min2 etc.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_setFaceplateRange(string sDp, dyn_string dsDpe, dyn_float dfMax, dyn_float dfMin, dyn_string &exceptionInfo)
{
	string deviceName, rangeConfig = "";
	int i, length;
	dyn_string dsParameters;
  string sTypeName;
  dyn_string exceptionInfoTemp;
  int iObject;
	
	deviceName = unGenericDpFunctions_getDpName(sDp);
	
	// 1. Build the range config parameter (dyn_strings to string)
	length = dynlen(dsDpe);
	if (length > dynlen(dfMax))
		{
		length = dynlen(dfMax);
		}
	if (length > dynlen(dfMin))
		{
		length = dynlen(dfMin);
		}
	for(i=1;i<=length;i++)
		{
		rangeConfig = rangeConfig + UN_PARAMETER_RANGE_DELIMITER + dsDpe[i] + 
									UN_PARAMETER_RANGE_DELIMITER + dfMax[i] + 
									UN_PARAMETER_RANGE_DELIMITER + dfMin[i];
		}
	if (rangeConfig != "")
		{
		rangeConfig = substr(rangeConfig,1);
		}
        
  sTypeName = dpTypeName(deviceName);
  // 1. Initialize data if not already done
  if (globalExists("g_dsObjectList"))
  {
    if (dynlen(g_dsObjectList) <= 0)
    {
      unGenericObject_InitializeObjects(exceptionInfoTemp);
    }
  }
  else
  {
    unGenericObject_InitializeObjects(exceptionInfoTemp);
  }
// 2. Get object
  iObject = dynContains(g_dsObjectList, sTypeName);
  if(iObject <= 0)
  {
    dpSetDescription(deviceName+".configuration", rangeConfig);
  }        
  else
  {
	// 2. Get old parameters and set new config
	unGenericDpFunctions_changeDpParameter(deviceName, UN_PARAMETER_FACEPLATE_RANGE, rangeConfig, exceptionInfo);
  }
}


//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_waitForDynValue
/**
Purpose: wait for new dpe value (with timeout) (dyn_anytype dpe)

Parameter: sDp, string, input, dpe name
		   timeout, unsigned, input, request timeout
		   dpValue, dyn_anytype, output, returned value
		   timeExpired, bool, output, request time expired
		   exceptionInfo, dyn_string, output, for errors
			
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_waitForDynValue(string sDp, unsigned timeout, dyn_anytype &dpValue, bool &timeExpired, dyn_string &exceptionInfo)
{
	int iRes;
	time begin, now;
	float timeDiff;
	
	timeExpired = false;
	g_unGenericDpFunctions_waitForDynValue_change = false;
	begin = getCurrentTime();
	if (dpExists(sDp))
		{
		iRes = dpConnect("unGenericDpFunctions_waitForDynValueCB", false, sDp);
		if (iRes >= 0)
			{
			while(!g_unGenericDpFunctions_waitForDynValue_change)
				{
				now = getCurrentTime();
				timeDiff = (float)(now - begin);
				if (timeDiff > timeout)
					{
					timeExpired = true;
					g_unGenericDpFunctions_waitForDynValue = makeDynString();
					g_unGenericDpFunctions_waitForDynValue_change = true;
					}
				delay(1, 100);
				}
			dpDisconnect("unGenericDpFunctions_waitForDynValueCB", sDp);
			dpValue = g_unGenericDpFunctions_waitForDynValue;
			}
		else
			{
			fwException_raise(exceptionInfo, "unGenericDpFunctions_waitForDynValue", getCatStr("unPVSS", "DPCONNECTFAILED"),"");
			}
		}
	else
		{
		fwException_raise(exceptionInfo, "unGenericDpFunctions_waitForDynValue", getCatStr("unFunctions", "BADINPUTS"),"");
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_waitForDynValueCB
/**
Purpose: callback function for unGenericDpFunctions_waitForDynValue function

Usage: Internal function

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-26 @whitelisted{Callback}
*/

unGenericDpFunctions_waitForDynValueCB(string sDp, dyn_anytype value)
{
	g_unGenericDpFunctions_waitForDynValue = value;
	g_unGenericDpFunctions_waitForDynValue_change = true;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getGroups
/**
Purpose: get device domain and nature.

Parameters :
	sDp, string, input, device name
	sDomain, string, output device domain
	sNature, string, output, device nature	

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getGroups(string sDp, string &sDomain, string &sNature)
{
	string domainTemp, natureTemp;
	dyn_string dsParameters;
	
	unGenericDpFunctions_getParameters(sDp, dsParameters);
	sDomain = dsParameters[UN_PARAMETER_DOMAIN];
	sNature = dsParameters[UN_PARAMETER_NATURE];
}

//---------------------------------------------------------------------------------------------------------------------------------------


// backward-compatibility with old name; deprecated
unGenericDpFunctions_getAccessControlPriviledgeRigth(string sDeviceDpName, dyn_string dsAllACDomains, dyn_bool &dbPriviledgeRigths, string &sOper, string &sExp, string &sAdmin, dyn_string &exceptionInfo, string sDefaultDomain="UNICOS")
{
    string sMoni;
    unGenericDpFunctions_getAccessControlPrivilegeRight(sDeviceDpName, dsAllACDomains, dbPriviledgeRigths, sMoni, sOper, sExp, sAdmin, exceptionInfo, sDefaultDomain);
}

// backward-compatibility; deprecated
unGenericDpFunctions_getAccessControl4PriviledgeRigth(string sDeviceDpName, dyn_string dsDomainAccessControl, dyn_bool &dbPriviledgeRigths, string &sMoni, string &sOper, string &sExp, string &sAdmin, dyn_string &exceptionInfo, string sDefaultDomain="UNICOS")
{
    unGenericDpFunctions_getAccessControlPrivilegeRight(sDeviceDpName, dsDomainAccessControl, dbPriviledgeRigths, sMoni, sOper, sExp, sAdmin, exceptionInfo, sDefaultDomain);
}

/**
Get unicos access rights for a device or a panel, plus a list of actions permitted for the rights

  @param sDeviceDpName, string, input, device DP name or panel name
  @param dsAllACDomains, dyn_string, list of all access control domain of the local system to be checked
  @param dbPrivilegeRigths, dyn_bool, output, device access control priviledge rigths
  @param sOper, string, output, Operator action
  @param sExp, string, output, Expert action
  @param sAdmin, string, output, Admin action  
  @param exceptionInfo, dyn_string, output, standard exception handling variable
  @param sDefaultDomain, string, input, default domain name to be used when not set in device/panel configuration, defaults to "UNICOS"
*/
unGenericDpFunctions_getAccessControlPrivilegeRight(string sDeviceDpName, dyn_string dsAllACDomains, dyn_bool &dbPrivilegeRigths, string &sMoni, string &sOper, string &sExp, string &sAdmin, dyn_string &exceptionInfo, string sDefaultDomain="UNICOS")
{
  string sDomainNames;
  dyn_string dsDomain;
  dyn_bool db;
  int i, len, iPos;
  bool bFileExists;
  dyn_string dsDomainPriv;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, __FUNCTION__+" "+sDeviceDpName, __FUNCTION__, sDeviceDpName, dsAllACDomains, sDefaultDomain);


  // we will create a list based on g_dsPrivilegeAccessControl 
  // which is a global dyn_string with default names of UNICOS privs: "monitor","operator","expert","admin"
  // firstly, reset to empty;
  db[dynlen(g_dsPrivilegeAccessControl)]=false; // fills all the ones in the middle to false
  dbPrivilegeRigths=db;
  
  // retrieve the names of actions permitted for this device or panel, as string with a list of domains
  if(dpExists(sDeviceDpName)) {
    unConfigGenericFunctions_getDeviceAccessControl(sDeviceDpName, sDomainNames, sOper, sExp, sAdmin);
  } else {
    // assume this is a panel
    unGenericButtonFunctionsHMI_getPanelAccessControl(sDeviceDpName, sDomainNames, sMoni, sOper, sExp, sAdmin, bFileExists);
  }
    
  if(sDomainNames == UNICOS_ACCESSCONTROL_DOMAINNAME) {
    // no domain use default domain "UNICOS"
    dynAppend(dsDomain, sDefaultDomain);
  } else {
    dyn_string ds = strsplit(sDomainNames, UN_ACCESS_CONTROL_SEPARATOR);
    for(i=1;i<=dynlen(ds);i++) {
      if (dynContains(dsAllACDomains, ds[i])) dynAppend(dsDomain, ds[i]);
    }
  }

  // do not process the SYSTEM domain
  int idx=dynContains(dsDomain,"SYSTEM");
  if (idx>0) dynRemove(dsDomain,idx);
  
  for (int i=1;i<=dynlen(dsDomain);i++) {
    for (int j=1;j<=dynlen(g_dsPrivilegeAccessControl);j++) {
      bool isGranted=false;
      dyn_string lExceptionInfo;
      fwAccessControl_checkUserPrivilege("", dsDomain[i], g_dsPrivilegeAccessControl[j], isGranted, lExceptionInfo);
      // we need to catch the exception when checking privilege name in non-UNICOS domains
      if (dynlen(lExceptionInfo)>2) {
          // keep all exceptions that do not have this text...
          if (strpos(lExceptionInfo[2], "Access Control: cannot find privilege")<0) {
            dynAppend(exceptionInfo, lExceptionInfo);        
          }
      }
      if (isGranted) db[j]=true;
    }
  }
  if (dynlen(exceptionInfo)) return;
  
  // otherwise make them effective
  dbPrivilegeRigths=db;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, __FUNCTION__+" "+sDeviceDpName, __FUNCTION__,sDeviceDpName,
                               getUserName(), sDeviceDpName, sDomainNames, dsDomain, dbPrivilegeRigths, sOper, sExp, sAdmin, dsDomainPriv);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getDomainList
/**
Purpose: get defined domains & natures for unicos.

Parameters :
	dsDomain, dyn_string, output, domain list
	dsNature, dyn_string, output, nature list

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getDomainNatureList(dyn_string &dsDomain, dyn_string &dsNature)
{
	unGenericDpFunctions_getDomainNatureListFromSystem("*:", dsDomain, dsNature);
//DebugN("unGenericDpFunctions_getDomainNatureList", dsDomain, dsNature);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getDomainNatureListFromSystem
/**
Purpose: get defined domains & natures for unicos from a given system.

Parameters :
	dsDomain, dyn_string, output, domain list
	dsNature, dyn_string, output, nature list

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getDomainNatureListFromSystem(string sSystemName, dyn_string &dsDomain, dyn_string &dsNature)
{
	dyn_string dsTemp;
	string current, sTempSystemName=sSystemName;
	int i, length;
	
// check if : is present and add it if not
	if(strpos(sTempSystemName, ":") < 0) 
		sTempSystemName = sTempSystemName+":";
		
	dsTemp = unGenericDpFunctions_groupGetNames(sTempSystemName);
//DebugN("unGenericDpFunctions_getDomainNatureListFromSystem", sTempSystemName, dsTemp);
	length = dynlen(dsTemp);
	for(i=1;i<=length;i++)
		{
		if (substr(dsTemp[i], 0, strlen(UN_DOMAIN_PREFIX)) == UN_DOMAIN_PREFIX)
			{
			current = substr(dsTemp[i], strlen(UN_DOMAIN_PREFIX));
			dynAppend(dsDomain, current);
			}
		if (substr(dsTemp[i], 0, strlen(UN_NATURE_PREFIX)) == UN_NATURE_PREFIX)
			{
			current = substr(dsTemp[i], strlen(UN_NATURE_PREFIX));
			dynAppend(dsNature, current);
			}
		}
	dynSortAsc(dsDomain);
	dynSortAsc(dsNature);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getWidgetDpName
/**
Purpose: get device name from widget identifier

Parameters :
	sWidgetIdentifier, string, input, widget identifier
Widget identifier is a string <sys name>:Alias. 
Firstly, the function extracts the alias. Then, the function returns the alias associated dp. 
In case of error or undefined alias, an empty string is returned.
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

string unGenericDpFunctions_getWidgetDpName(string sWidgetIdentifier)
{
	string dpName, systemName;
	
	dpName = "";
//	dpName = dpAliasToName(sWidgetIdentifier);
	dpName = unGenericDpFunctions_dpAliasToName(sWidgetIdentifier);
	dpName = unGenericDpFunctions_getDpName(dpName);
	systemName = unGenericDpFunctions_getSystemName(dpName);
	if ((systemName != unGenericDpFunctions_getSystemName(sWidgetIdentifier)) || (systemName == dpName))
		{
		dpName = "";
		}

	return dpName;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGenericDpFunctions_dpAliasToName
/**
Purpose: convert the alias to dp name, "" is returned if the alias does not exists.

Parameters :
	sWidgetIdentifier, string, input, widget identifier
Widget identifier is a string <sys name>:Alias. 
Firstly, the function extracts the alias. Then, the function returns the alias associated dp. 
In case of error or undefined alias, an empty string is returned.
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

string _unGenericDpFunctions_dpAliasToName(string sAlias)
{
	dyn_string dsDp, dsAlias, exceptionInfo;
	string sDpeFilter, sResult="", sAliasTemp, sSystemName;
	int pos = strpos(sAlias, ":"), iCounter=0, iRes;
	bool bConnected;
	
//DebugTN("unGenericDpFunctions_dpAliasToName", sAlias);
	if(sAlias == "")
		return sAlias;
		
	if(pos>0)
		sAliasTemp = substr(sAlias, pos+1, strlen(sAlias));
	else
		sAliasTemp = sAlias;
		
	sSystemName = unGenericDpFunctions_getSystemName(sAlias);
	if(isModeExtended()) {
		if(sSystemName != "")
			sDpeFilter =sSystemName+"*.**";
	
		dpGetAllAliases(dsDp, dsAlias, sAliasTemp, sDpeFilter);
//DebugN(dsDp, dsAlias, sAlias, sAliasTemp, sDpeFilter);
		if(dynlen(dsDp) > 0)
			sResult = dsDp[1];
		else 
			sResult = "";
	}
	else {
		if(!mappingHasKey(g_m_unicosGraphicalFrame_bUpdate, sSystemName)) {
// no key for the requied system -> re-initialize the list of aliases, and dpnames
//DebugTN(sAlias, sSystemName, "No key");
			iRes = unGenericDpFunctions_InitializeAliasDp(sSystemName);
		}
		else {
			if(g_m_unicosGraphicalFrame_bUpdate[sSystemName]) {
	// update on the system -> re-initialize the list of aliases, and dpnames
//DebugTN(sAlias, sSystemName, "New update", sSystemName, g_m_unicosGraphicalFrame_bUpdate[sSystemName]);
				iRes = unGenericDpFunctions_InitializeAliasDp(sSystemName);
			}
		}
// check if the lock on the global var was released max 1sec.
		while(g_m_unicosGraphicalFrame_bLock[sSystemName] && (iCounter < 10)) {
			delay(0, 100);
			iCounter++;
//DebugTN(sAlias, sSystemName, "Initializing waiting ...", iCounter);
		}
// if the lock is still taken something strange: panel was colse before the end of inialization
// so redo the initialization
		if(g_m_unicosGraphicalFrame_bLock[sSystemName]){
// and reset the lock just in case the panel that called the function was closed
			g_m_unicosGraphicalFrame_bLock[sSystemName] = false;
//DebugTN(sAlias, sSystemName, "Lock taken", sSystemName, g_m_unicosGraphicalFrame_bUpdate[sSystemName]);
			iRes = unGenericDpFunctions_InitializeAliasDp(sSystemName);
		}
		else {
// if the system is connected and the list of dp is empty re-initialize it
			unDistributedControl_isConnected(bConnected, sSystemName);
			if(bConnected && ((dynlen(g_m_unicosGraphicalFrame_dsDp[sSystemName]) <= 0) || 
												(dynlen(g_m_unicosGraphicalFrame_dsAlias[sSystemName]) <= 0) ||
												(dynlen(g_m_unicosGraphicalFrame_dsDp[sSystemName]) != dynlen(g_m_unicosGraphicalFrame_dsAlias[sSystemName]))
												)) {
//DebugTN(sAlias, sSystemName, "List empty", sSystemName, bConnected, dynlen(g_m_unicosGraphicalFrame_dsDp[sSystemName]), dynlen(g_m_unicosGraphicalFrame_dsAlias[sSystemName]));
				iRes = unGenericDpFunctions_InitializeAliasDp(sSystemName);
			}
		}
// from here there should be a list of alias and associated dp.
// however if the dynlen of the list are different do not search->possible case of error
		if(dynlen(g_m_unicosGraphicalFrame_dsDp[sSystemName]) == dynlen(g_m_unicosGraphicalFrame_dsAlias[sSystemName])) {
			pos = dynContains(g_m_unicosGraphicalFrame_dsAlias[sSystemName], sAliasTemp);
			if(pos>0) { // alias found
				sResult = g_m_unicosGraphicalFrame_dsDp[sSystemName][pos];
			}
		}
//DebugN("Res:", dynlen(g_m_unicosGraphicalFrame_dsAlias[sSystemName]), dynlen(g_m_unicosGraphicalFrame_dsDp[sSystemName]), sResult, pos);			
	}

	return sResult;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_InitializeAliasDp
/**
Purpose: initialize the aliases and Dps of a given system

Parameters :
	- sSystemName, string, input, the system name

	return 0
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

int unGenericDpFunctions_InitializeAliasDp(string sSystemName)
{
	int iRes;
	
	if(sSystemName == "")
		sSystemName = getSystemName();

	if(!mappingHasKey(g_m_unicosGraphicalFrame_bLock, sSystemName)) {
		g_m_unicosGraphicalFrame_bLock[sSystemName] = true;
		iRes = unGenericDpFunctions_InitializeAliasDpList(sSystemName);
	}
	else if(!g_m_unicosGraphicalFrame_bLock[sSystemName]) {
// take the lock
		g_m_unicosGraphicalFrame_bLock[sSystemName] = true;
		iRes = unGenericDpFunctions_InitializeAliasDpList(sSystemName);
	}
	return iRes;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_InitializeAliasDpList
/**
Purpose: initialize the aliases and Dps List of a given system

Parameters :
	- sSystemName, string, input, the system name
	
	return 0
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

int unGenericDpFunctions_InitializeAliasDpList(string sSystemName)
{
	dyn_string dsDp, dsAlias;

	g_m_unicosGraphicalFrame_bUpdate[sSystemName] = true;
	g_m_unicosGraphicalFrame_dsAlias[sSystemName] = dsAlias;
	g_m_unicosGraphicalFrame_dsDp[sSystemName] = dsDp;
	
//DebugTN(sSystemName, "Update start", sSystemName, g_m_unicosGraphicalFrame_bUpdate[sSystemName]);
// update the lists
	dpGetAllAliases(dsDp, dsAlias, "", sSystemName+"*.**");
	if(g_m_unicosGraphicalFrame_bLock[sSystemName]) {
// just in case few threads are entering, the first finishing updates
		g_m_unicosGraphicalFrame_dsAlias[sSystemName] = dsAlias;
//delay(3);
		g_m_unicosGraphicalFrame_dsDp[sSystemName] = dsDp;
		g_m_unicosGraphicalFrame_bUpdate[sSystemName] = false;
// release the lock
		g_m_unicosGraphicalFrame_bLock[sSystemName] = false;
//DebugTN(sSystemName, "Update done", sSystemName, dynlen(dsAlias), dynlen(dsDp), g_m_unicosGraphicalFrame_bUpdate[sSystemName]);
	}
	return 0;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getDeviceWidgetList
/**
Purpose: get available widgets for current device

Parameters :
	sType, string, input, unicos object
	dsWidgets, dyn_string, output, result of the function
The result is empty in case of error (undefined object).

This function returns the full relative path for each widget. ex: objects/UN_OBJECTS/unWidget_Alarm.pnl

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getDeviceWidgetList(string sType, dyn_string &dsWidgets)
{
  int iObject;
  dyn_string exceptionInfoTemp;
  
// 1. Initialize data if not already done
  if (globalExists("g_dsObjectList"))
  {
    if (dynlen(g_dsObjectList) <= 0)
    {
      unGenericObject_InitializeObjects(exceptionInfoTemp);
    }
  }
  else
  {
    unGenericObject_InitializeObjects(exceptionInfoTemp);
  }
// 2. Get object
  iObject = dynContains(g_dsObjectList, sType);
  if (iObject <= 0)
  {
//DebugN(sType, g_mFrontEndWidgetList);
    if(mappingHasKey(g_mFrontEndWidgetList, sType))
      dsWidgets = g_mFrontEndWidgetList[sType];
    else
      dsWidgets = makeDynString();
  }
  else
  {
  // 3. Get select field
    dsWidgets = g_ddsObjectWidgetList[iObject];
//DebugN("Found", g_ddsObjectWidgetList);
  }
//DebugN(sType, dsWidgets, iObject);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getDeviceWidget
/**
Purpose: get device widget

Parameters :
	sDp, string, input, device name
	sWidget, string, output, result of the function
	dsWidgetFileName, dyn_string, input, list of widget filename
The result is empty in case of error (undefined object).

This function returns the full relative path for the widget. ex: objects/UN_OBJECTS/unWidget_Alarm.pnl

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getDeviceWidget(string sDp, string &sWidget, dyn_string dsWidgetFileName)
{
  string deviceName, sTemp, sType;
  dyn_string dsParameters;
  int i, len, iObject;
  bool found=false;

  sWidget = "";
  deviceName = unGenericDpFunctions_getDpName(sDp);
  sType = dpTypeName(deviceName);
  iObject = dynContains(g_dsObjectList, sType);
  if(iObject > 0)
  {
    unGenericDpFunctions_getParameters(deviceName, dsParameters);		// Get parameters
  }
  else {
    dsParameters[UN_PARAMETER_WIDGET] = sType;
  }
	
  len = dynlen(dsWidgetFileName);
  for(i = 1; (i<=len) && (!found);i++) {
    if(dsParameters[UN_PARAMETER_WIDGET] == unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName(dsWidgetFileName[i])){
      sWidget = dsWidgetFileName[i];
      found = true;
    }
  }
//DebugN(sType, dsParameters[UN_PARAMETER_WIDGET], sWidget, dsWidgetFileName);
}


//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_changeDpParameter
/**
Purpose: change one of device parameter

Parameters :
	sDpName, string, input, device name
	iParameter, unsigned, input, parameter number
	sParameterValue, string, input, parameter value
	exceptionInfo, dyn_string, output, for errors

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_changeDpParameter(string sDpName, unsigned uParameter, string sParameterValue, dyn_string &exceptionInfo)
{
	int iRes;
	string deviceName;
	dyn_string dsParameters;
	
	deviceName = unGenericDpFunctions_getDpName(sDpName);
	if ((uParameter <= UN_PARAMETER_LENGTH) && (uParameter > 0) && (dpExists(deviceName)))
		{
		unGenericDpFunctions_getParameters(deviceName, dsParameters);			// Get parameters
		dsParameters[uParameter] = sParameterValue;
		unGenericDpFunctions_setParameters(deviceName, dsParameters, exceptionInfo);		// Set parameters
		}
	else
		{
		fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_changeDpParameter:" + getCatStr("unFunctions","BADINPUTS"), "");
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_groupGetNames
/**
Purpose: return all groups. To be used instead of PVSS groupGetNames that doesn't work in distributed system

Parameters : 
	sSystemName: string, input, the system name from which to get the groups.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

dyn_string unGenericDpFunctions_groupGetNames(string sSystemName)
{
	dyn_string dsGroups, dsNames;
	int i, length;
	string sDescription;
	
	dsGroups = dpNames(sSystemName+"_DpGroup*","_DpGroup");
	length = dynlen(dsGroups);
	for(i=1;i<=length;i++)
		{
    	sDescription = unGenericDpFunctions_getDescription(dsGroups[i]);
    	if (sDescription != "")
    		{
    		dynAppend(dsNames, sDescription);
    		}
		}
	dynSortAsc(dsNames);
	
 	return dsNames;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_groupNameToDpName
/**
Purpose: convert group name to dp name (based on PVSS function groupNameToDpName() from dpGroups.ctl)

Parameters : 
	name, string, input, group name
	sSystemName: string, input, the system name from which to get the groups.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

string unGenericDpFunctions_groupNameToDpName(string name, string sSystemName)
{
  	int i, length;
  	dyn_string dsGroups;
  
	dsGroups = dpNames(sSystemName+"_DpGroup*","_DpGroup");
	length = dynlen(dsGroups);
  	for(i=1;i<=length;i++)
  		{
    	if(unGenericDpFunctions_getDescription(dsGroups[i]) == name)
	    return(dsGroups[i]);
  		}  
    return(""); // name not found  
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_deleteDevice
/**
Purpose: delete unicos device

Parameters : 
	name, string, input, device name
	exceptionInfo, dyn_string, output, for errors
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_deleteDevice(string name, dyn_string& exceptionInfo)
{
	string deviceName, systemName, sDomain, sNature, sFunction, sDeviceType;
	int iRes;
	dyn_string dsRet;

	deviceName = unGenericDpFunctions_getDpName(name);
	systemName = unGenericDpFunctions_getSystemName(deviceName);	// System name
	
	if (systemName == getSystemName())
		{
		unGenericDpFunctions_getGroups(deviceName, sDomain, sNature);
		if (sDomain != "")
			{
			unConfigGenericFunctions_deleteDpInMultiGroup(deviceName, UN_DOMAIN_PREFIX, sDomain, exceptionInfo);
			}
		if (sNature != "")
			{
			unConfigGenericFunctions_deleteDpInMultiGroup(deviceName, UN_NATURE_PREFIX, sNature, exceptionInfo);
			}
    _unSystemIntegrity_modifyApplicationAlertList(deviceName, false, exceptionInfo);
		deviceName = substr(deviceName, strpos(deviceName, ":") + 1);	// Delete system name
		sDeviceType = dpTypeName(deviceName);
		sFunction = sDeviceType+"_processDeviceToDelete";
		if(isFunctionDefined(sFunction))
		{
			evalScript(dsRet, "dyn_string main() { dyn_string dsRet="+sFunction+"($deviceName); return dsRet;}", makeDynString("$deviceName:"+deviceName));
		}	
		if(dpExists(deviceName))
		{
			iRes = dpDelete(deviceName);
		}
		if (iRes < 0)
			{
			fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_deleteDevice: " + name + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
			}
		else
			{
			delay(0,10);
			}
		}
	else
		{
		fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_deleteDevice: " + name + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_deletePlc
/**
Purpose: delete Plc config

Parameters : 
	sPlcName, string, input, plc name
	exceptionInfo, dyn_string, output, for errors
	
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_deletePlc(string sPlcName, dyn_string& exceptionInfo)
{
	string sDpPlcName, sPlcModPlc="", exceptionText;
	int i, length, iRes;
	dyn_string dsAlarmDps;
	

	sDpPlcName = c_unSystemIntegrity_UnPlc+sPlcName;
	if (!dpExists(sDpPlcName))
		sDpPlcName = "";

	if (sDpPlcName != "")
		{
		if (unGenericDpFunctions_getSystemName(sDpPlcName) == getSystemName())
			{
      
      // 1. Remove system integrity for this front-end (via RPC call)
      dpSetWait("unicosPLC_systemIntegrityInfo.interface.command", UN_SYSTEMINTEGRITY_DELETE
               ,"unicosPLC_systemIntegrityInfo.interface.parameters", makeDynString(sDpPlcName));
      delay(5);
    
	// 1. Delete _Mod_Plc
			dpGet(sDpPlcName+".configuration.mod_PLC", sPlcModPlc);
			iRes = dpDelete(sPlcModPlc);
			if (iRes < 0)
				{
				fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_deletePlc: " + sPlcModPlc + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
				}
	// 2. Delete _Un_Plc
			iRes = dpDelete(substr(sDpPlcName, strpos(sDpPlcName, ":") + 1));
			if (iRes < 0)
				{
				fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_deletePlc: " + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
				}

	// 4. Delete config line in config file
			unGenericDpFunctions_removeLinesWithPatternFromFile(getPath(CONFIG_REL_PATH,"config"), makeDynString("plc = \"" + substr(sPlcModPlc, strpos(sPlcModPlc, ":") + 1) + "\""), exceptionText);
			unGenericDpFunctions_removeLinesWithPatternFromFile(getPath(CONFIG_REL_PATH,"config"), makeDynString("plc = \"" + substr(sPlcModPlc, strpos(sPlcModPlc, ":") + 1) + "\""), exceptionText);
			unGenericDpFunctions_cleanEmptySectionFromFile(getPath(CONFIG_REL_PATH,"config"), exceptionText);
			}
		else
			{
			fwException_raise(exceptionInfo, "ERROR", "unGenericDpFunctions_deletePlc: " + sDpPlcName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_deletePlcDevices
/**
Purpose: delete devices that belongs to a specified PLC, subapplication, object type, object number

Parameters : 
	sPlcName, string, input, PLC name
	sPlcSubApplication, string, input, PLC subapplication
	sObjectType, string, input, unicos object type
	sNumber, string, input, object number
	sProgresBarIn, string, input, progress bar graphical element name, if "" then no update
	exceptionInfo, dyn_string, output, for errors

!!! WARNING : this function will delete LOCAL devices !!!

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_deletePlcDevices(string sPlcName, string sPlcSubApplication, string sObjectType, string sNumber, string sProgresBarIn, dyn_string &exceptionInfo)
{
	dyn_string sPlcDevices;
	int i,length;
  int iH, iM, iS;
  int tCurrent, tPrevious;
  int tRemaining, tDiff;
  float fRemaining;
  string sProgresBar;

  if(isFunctionDefined("shapeExists"))
  {
    if(shapeExists("progressBar"))
      sProgresBar=sProgresBarIn;
  }
// get all instance of the current system	
	unGenericDpFunctions_getPlcDevices("", sPlcName, sPlcSubApplication, sObjectType, sNumber, sPlcDevices);
	length = dynlen(sPlcDevices);
  if(sProgresBar != "") {
    unProgressBar_setMax("progressBar", length);
    unProgressBar_setPosition("progressBar", 0);
    tCurrent = (int)getCurrentTime();
    tPrevious = tCurrent;
  }
	for(i=1;i<=length;i++)
	{
		if(sProgresBar != "") {
      tCurrent = (int)getCurrentTime();
      tDiff = tCurrent - tPrevious;
      tPrevious = tCurrent;
      fRemaining = length - i;
      unProgressBar_setPosition("progressBar", i);
      // if(tDiff > 0)
        // tRemaining = tDiff*fRemaining;
      // else
        // tRemaining = fRemaining;
      // iH = tRemaining/3600;
      // iM = (tRemaining-iH*3600)/60;
      // iS = tRemaining-iH*3600-iM*60;
      // remainingTime.text = iH+"h "+iM+"m "+iS+"s remaining";
    }

		if (unGenericDpFunctions_getSystemName(sPlcDevices[i]) == getSystemName())		// Delete only LOCAL devices
		{
			unGenericDpFunctions_deleteDevice(sPlcDevices[i], exceptionInfo);
		}
	}
  if(sProgresBar != "")
    unProgressBar_setPosition("progressBar", 0);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getPlcDevices
/**
Purpose: get devices that belongs to a specified PLC, subapplication, object type

Parameters : 
	sSystemName, string, input, PVSS system name
	sPlcName, string, input, PLC name
	sPlcSubApplication, string, input, PLC subapplication
	sObjectType, string, input, unicos object type
	sNumber, string, input, object number
	sPlcDevices, dyn_string, output, result of the function

sPlcName, sPlcSubApplication, sObjectType and sNumber could be empty. 
In case of error, sPlcDevices is empty.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getPlcDevices(string sSystemName, string sPlcName, string sPlcSubApplication, string sObjectType, string sNumber, dyn_string &dsPlcDevices)
{
	string sPattern, sDpPlcName, sPlcModPlc;
	
	dynClear(dsPlcDevices);
	sPattern = sSystemName+"*" + UN_DPNAME_SEPARATOR;
	if (sPlcName != "")
		{
		sPattern = sPattern + sPlcName + UN_DPNAME_SEPARATOR;
		}
	else
		{
		sPattern = sPattern + "*" + UN_DPNAME_SEPARATOR;
		}
	if (sPlcSubApplication != "")
		{
		sPattern = sPattern + sPlcSubApplication + UN_DPNAME_SEPARATOR;
		}
	else
		{
		sPattern = sPattern + "*" + UN_DPNAME_SEPARATOR;
		}
	if (sObjectType != "")
		{
		sPattern = sPattern + sObjectType + UN_DPNAME_SEPARATOR;
		}
	else
		{
		sPattern = sPattern + "*" + UN_DPNAME_SEPARATOR;
		}
	if (sNumber != "")
		{
		sPattern = sPattern + sNumber;
		}
	else
		{
		sPattern = sPattern + "*";
		}
	dsPlcDevices = dpNames(sPattern);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getBitDescription
/**
Purpose: get current bit description (saved in _UnObject dp)

Parameters : 
	sObject, string, input, device type
	sStsReg, string, input, stsreg name ex: "StsReg01"
	position, int, input, bit position in stsreg
	
	bitDescription, string, output, result of the function
In case of error, bitDescription is empty.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getBitDescription(string sObject, string sStsReg, int position, string &bitDescription)
{
	dyn_string dsBitNames, dsBitPositions;
	string sBitNames, sBitPositions, sTruePosition;
	int iRes, iStsReg, iTruePosition, iNamePosition;

	bitDescription = "";
	if (dpExists (sObject + UN_OBJECT_EXTENSION))
		{
		if (dpTypeName(sObject + UN_OBJECT_EXTENSION) == UN_OBJECT_DPTYPE)
			{
			iRes = dpGet(sObject + UN_OBJECT_EXTENSION + ".unEventList.Names", sBitNames,
						 sObject + UN_OBJECT_EXTENSION + ".unEventList.Positions", sBitPositions);
			dsBitNames = strsplit(sBitNames, ";");
			dsBitPositions = strsplit(sBitPositions, ";");
			iStsReg = (int)substr(sStsReg, strlen(sStsReg) - 2);
			iTruePosition = position + 16*(iStsReg-1);
			sTruePosition = iTruePosition;
			iNamePosition = dynContains(dsBitPositions, sTruePosition);
			if (iNamePosition > 0)
				{
				if (dynlen(dsBitNames) >= iNamePosition)
					{
					bitDescription = dsBitNames[iNamePosition];
					}
				}
			}
		}
}

// unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName
/**
Purpose: returns the widget name from the widget file name

Parameters : 
	sWidget, string, input, the widget file name
	return value, string, the widget name

In case of error, "" is returned.

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
string unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName(string sWidget)
{
	string sWidgetFormatted;
	dyn_string dsParameters, dsTemp;
	
	sWidgetFormatted = sWidget;
	strreplace(sWidgetFormatted, "\\", "/");
	dsTemp = strsplit(sWidgetFormatted, "/");
	if (dynlen(dsTemp) == 0)
		{
		sWidgetFormatted = "";
		}
	else
		{
		sWidgetFormatted = dsTemp[dynlen(dsTemp)];			// Delete reference directories
		}

	strreplace(sWidgetFormatted , UN_WIDGET,"!");
	dsTemp = strsplit(sWidgetFormatted , "!");
	if(dynlen(dsTemp) > 1) {
		sWidgetFormatted = dsTemp[dynlen(dsTemp)];
		if (substr(sWidgetFormatted, strlen(sWidgetFormatted) - 4) == ".pnl")
			{
			sWidgetFormatted = substr(sWidgetFormatted, 0, strlen(sWidgetFormatted) - 4);	// Delete .pnl
			}
	}
	else 
		sWidgetFormatted = "";
	return sWidgetFormatted;
}

// unGenericDpFunctions_getDeviceDefinitionDPEDescription
/**
Purpose: Get the description from the _FwDeviceDefinition

Parameters : 
	sDeviceType, string, input, the DPE in UNICOS format
	dsDPE, dyn_string, output, the PVSS DPEs list of the device type
	dsDPEDescription, dyn_string, output, the PVSS DPEs description list of the device type

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctions_getDeviceDefinitionDPEDescription(string sDeviceType, dyn_string &dsDPE, dyn_string &dsDPEDescription)
{
	int len;
	dyn_string dsTempDPE, dsTempDescription;
	string sDp = sDeviceType+"Info";
	
	if(!mappingHasKey(g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPE, sDeviceType)) {
// get the DPEs and description
		if(dpExists(sDp)) {
			if(dpTypeName(sDp) == "_FwDeviceDefinition") {
				dpGet(sDp+".properties.dpes", dsTempDPE, sDp+".properties.description", dsTempDescription);
			}
		}
		len = dynlen(dsTempDPE);
		while(dynlen(dsTempDescription) < len)
			dynAppend(dsTempDescription, "");
	
// update the list
		g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPE[sDeviceType] = dsTempDPE;
		g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPEdescription[sDeviceType] = dsTempDescription;
	}
	dsDPE = g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPE[sDeviceType];
	dsDPEDescription = g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPEdescription[sDeviceType];
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getDeviceLink
/**
Purpose: get the device links

Parameters:
		sDpName: string, input, datapoint name
		sDeviceLink: string, output, list of device links

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getDeviceLink(string sDpName, string &sDeviceLink)
{
	dpGet(sDpName+".statusInformation.deviceLink", sDeviceLink);
//DebugN("unGenericDpFunctions_getDeviceLink", sDeviceLink);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getLinkedDevicesDps
/**
Purpose: Get datapoints of linked devices for given device datapoint
  
Parameters:
    sDeviceDp: string, input, name of the device dp
    return value, dyn_string, list of linked devices dps

Usage: External function
    
PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.15 
	. operating system: Linux, Win7, WS2008
	. distributed system: yes.
    
  **/
dyn_string unGenericDpFunctions_getLinkedDevicesDps(string sDeviceDp)
{
  string sLinkedDevices;
  unGenericDpFunctions_getDeviceLink(sDeviceDp, sLinkedDevices);
  if(sLinkedDevices == "")
  {
    return makeDynString();
  }

  string deviceSystemName = unGenericDpFunctions_getSystemName(sDeviceDp);
  dyn_string dsSplit = strsplit(sLinkedDevices, UN_CONFIG_GROUP_SEPARATOR);

  dyn_string dsLinkedDevicesDp;
  int dsSplitLen = dynlen(dsSplit);
  for(int i=1;i<=dsSplitLen;i++)
  {
    string sLinkedDevice = dsSplit[i];
    if(strpos(sLinkedDevice, ":") < 0)
    {
      sLinkedDevice = deviceSystemName + sLinkedDevice;
    }
    string sLinkedDeviceDp = unGenericDpFunctions_getWidgetDpName(sLinkedDevice);
    
    if(dpExists(sLinkedDeviceDp))
    {
      dynAppend(dsLinkedDevicesDp, sLinkedDeviceDp);
    }
  }
  return dsLinkedDevicesDp;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getApplication
/**
Purpose: this function returns Application name according to a device (DpName)

Parameter:
	-> dsDp, string input, datapoint name = device

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, XP
	. distributed system: yes.
*/

string unGenericDpFunctions_getApplication(string sDevice)
{
dyn_string dsTemp;

dsTemp=strsplit(sDevice, UN_DPNAME_SEPARATOR);

if (dynlen(dsTemp)>=UN_DPNAME_FIELD_LENGTH)
	return dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION];	
else
	return "";
}


//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getMaskEvent
/**
Purpose: mask/unmask the event

Parameters:
		sDpName: string, input, the device dpname
		iMask: integer, output, the event mask value
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericDpFunctions_getMaskEvent(string sDpName, int &iMask)
{
	dpGet(sDpName+".eventMask", iMask);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_isMaskEvent
/**
Purpose: check if the device supports the mask/unmask the event

Parameters:
		sDpName: string, input, the device dpname
		return value: bool, output, true if the device supports the mask/unmask the event/false if not
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

bool unGenericDpFunctions_isMaskEvent(string sDpName)
{
	if(dpExists(sDpName+".eventMask"))
		return true;
	else
		return false;
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_checkDeviceType
/**
Purpose: check if one device in the list of device is of a certain type

Parameters:
		dsDeviceName: dyn_string, input, the list of device names
		sDeviceType: string, input the type of device
		return value: bool, output, true if the all the devices are of type sDeviceType/false if not
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

bool unGenericDpFunctions_checkDeviceType(string sSystemName, dyn_string dsDeviceName, string sDeviceType)
{
	bool bReturn;
	string sDp;
	int i, len, iPos;
	
	len = dynlen(dsDeviceName);
	for(i=1;(i<=len) && (!bReturn);i++) {
		iPos = strpos(dsDeviceName[i], ":");
		if(iPos <=0) 
			dsDeviceName[i] = sSystemName+dsDeviceName[i];
		sDp = unGenericDpFunctions_getWidgetDpName(dsDeviceName[i]);
		if(dpExists(sDp)) {
			if(dpTypeName(sDp) == sDeviceType) {
				bReturn = true;
			}
		}
	}
	return bReturn;
}


//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_createDpeDpFunction
/**
Purpose: to create Datapoint function configuration

Parameters:

		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: XP
	. distributed system: yes.
*/

int unGenericDpFunctions_createDpeDpFunction(const string sDp)
{
int iRes;

	iRes=dpSetWait(sDp + ":_dp_fct.._type", DPCONFIG_DP_FUNCTION);
	return iRes;
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_deleteDpFunction
/**
Purpose:

Parameters: to delete Datapoint function configuration
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.1
	. operating system:XP
	. distributed system: yes.
  
  @reviewed 2018-06-26 @whitelisted{FalsePositive}
*/

int unGenericDpFunctions_deleteDpFunction(const string sDp)
{
int iRes;

	iRes=dpSetWait(sDp + ":_dp_fct.._type", DPCONFIG_NONE);
	return iRes;
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_addDpeinDpFunction
/**
Purpose: to add Dpe in a Datapoint function configuration

Parameters:
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.1
	. operating system:XP
	. distributed system: yes.
*/

int unGenericDpFunctions_addDpeinDpFunction(const string sDp, const dyn_string dsNewDpe, const string sOperator="&")
{
dyn_string dsDpe;
string sFunction;
int iLen, i, iRes, iType, k;

	iRes=dpGet(sDp + ":_dp_fct.._type", iType);
	if(iRes>=0)
		{
		if(iType==DPCONFIG_NONE)
			{
			iRes=unGenericDpFunctions_createDpeDpFunction(sDp);
			if (iRes>=0)
				iRes=dpGet(sDp + ":_dp_fct.._param", dsDpe);
			}
		else
			iRes=dpGet(sDp + ":_dp_fct.._param", dsDpe);
	
		if (iRes>=0)
			{
			dynAppend(dsDpe, dsNewDpe);
			dynUnique(dsDpe);
			
			k=0;
			iLen=dynlen(dsDpe);	
			for(i=iLen; i>=1; i--)
				{
				if(dpExists(dsDpe[i]))
					{
					k++;
					sFunction+="p" + k + sOperator;
					}
				else
					dynRemove(dsDpe, i);
				}
			
			sFunction=substr(sFunction, 0, strlen(sFunction)-strlen(sOperator));
			iRes=dpSetWait(	sDp + ":_dp_fct.._param", dsDpe, sDp + ":_dp_fct.._fct", sFunction);							
			}
		}
	return iRes;
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_debugN
/**
Purpose: debugN function, equivalent to DebugN but with debug level and filter

Parameters:
	@param b32DebugLevel: bit32, input, the debug level
	@param sDebugFilter: string, input, the debug filter, "*" for everything
	@param ...: anytype, input, list of parameter to print in the PVSS_II.log
		
Usage: External function

PVSS manager usage: Ctrl, Ui

Constraints:
	. PVSS version: 3.6
	. operating system: XP, Linux
	. distributed system: yes.
  
@reviewed 2018-09-17 @whitelisted{FalsePositive}
*/
unGenericDpFunctions_debugN(bit32 b32DebugLevel, string sDebugFilter, ...)
{
  int i, len;
  va_list parameters;

//DebugN(g_b32DebugLevel&b32DebugLevel, b32DebugLevel, sDebugFilter);

  if(g_b32DebugLevel&b32DebugLevel)
  {
    if(patternMatch(g_sDebugFilter, sDebugFilter)) {
      len = va_start(parameters); 
      for(i=1;i<=len;i++) {
        Debug(va_arg(parameters));
      }
      DebugN("");
      va_end(parameters); 
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_deviceTypeHasProxy
/**
Purpose: Check if the device type can accept proxy

Parameters:
  @param sDpType: string, input, the device type
  @param return value: bool, true=the device type can accept proxy/false=the device type cannot accept proxy
		
Usage: External function

PVSS manager usage: Ctrl, Ui

Constraints:
  . PVSS version: 3.6
  . operating system: XP, Linux
*/
bool unGenericDpFunctions_deviceTypeHasProxy(string sDpType)
{
  dyn_string dsDpel;
  dyn_int diDpelType;
  bool bProxy=false;
  int iPos;
  
  unGenericDpFunctions_getDPE(sDpType, dsDpel, diDpelType);
  iPos = dynContains(dsDpel, sDpType+".proxy");
  bProxy = (iPos > 0);
  if(bProxy) {
    bProxy = (diDpelType[iPos] == DPEL_DYN_STRING);
  }
  return bProxy;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getProxyDeviceList
/**
Purpose: Get the proxy device list

Parameters:
  @param sDp: string, input, the device DP name
  @param bProxy: bool, output, true=the device can accept proxy/false=the device cannot accept proxy
  @param dsProxy: dyn_string, output, the proxy device list
		
Usage: External function

PVSS manager usage: Ctrl, Ui

Constraints:
  . PVSS version: 3.6
  . operating system: XP, Linux
  . distributed: yes
*/
unGenericDpFunctions_getProxyDeviceList(string sDp, bool &bProxy, dyn_string &dsProxy)
{
  bProxy = unGenericDpFunctions_hasProxy(sDp);
  if(bProxy) 
    dpGet(sDp+".proxy", dsProxy);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_hasProxy
/**
Purpose: Does the device have proxy

Parameters:
  @param sDp: string, input, the device DP name
  @param return value: bool, true=the device can accept proxy/false=the device cannot accept proxy
		
Usage: External function

PVSS manager usage: Ctrl, Ui

Constraints:
  . PVSS version: 3.6
  . operating system: XP, Linux
  . distributed: yes
*/
bool unGenericDpFunctions_hasProxy(string sDp)
{
  bool bProxy=false;
  
  bProxy = dpExists(sDp+".proxy");
  if(bProxy) {
    bProxy = (dpElementType(sDp+".proxy") == DPEL_DYN_STRING);
  }
  return bProxy;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_setProxyDeviceList
/**
Purpose: Set the proxy device list

Parameters:
  @param sDp: string, input, the device DP name
  @param dsProxy: dyn_string, input, the proxy device list
		
Usage: External function

PVSS manager usage: Ctrl, Ui

Constraints:
  . PVSS version: 3.6
  . operating system: XP, Linux
  . distributed: yes
*/
unGenericDpFunctions_setProxyDeviceList(string sDp, dyn_string dsProxy)
{
  bool bProxy = unGenericDpFunctions_hasProxy(sDp);
  if(bProxy) {
    dpSet(sDp+".proxy", dsProxy);
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getProxyDP
/**
Purpose: Get a proxy device DP

Parameters:
  @param sSystemName: string, input, the system name
  @param sProxyIn: string, input, the proxy device DP or alias
  @param sProxyDp: string, output, the proxy device DP
		
Usage: External function

PVSS manager usage: Ctrl, Ui

Constraints:
  . PVSS version: 3.6
  . operating system: XP, Linux
  . distributed: yes
*/
unGenericDpFunctions_getProxyDP(string sSystemName, string sProxyIn, string &sProxyDp)
{
  string sProxy = sProxyIn, sDp;
  // check first if DP and then if it is not a DP check for alias
  int iPos = strpos(sProxy, ":");
  if(iPos <=0)
    sProxy = sSystemName+sProxy;
  if(dpExists(sProxy))
    sDp = sProxy;
  else {
    sDp = unGenericDpFunctions_getWidgetDpName(sProxy);
  }
  sProxyDp = sDp;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_setKeyUnicosConfiguration
/** Set the device dpe unicos configuration for a given key, an empty dyn_string for a key remove the key entry
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sKey input, the key
@param dsConfig input, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_setKeyUnicosConfiguration(string sDeviceDpName, string sKey, dyn_string dsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_setKeyConfiguration(sDeviceDpName, UN_UNICOS_CONFIGURATION, sKey, dsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getKeyUnicosConfiguration
/** Get the device dpe unicos configuration for a given key
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sKey input, the key
@param dsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_getKeyUnicosConfiguration(string sDeviceDpName, string sKey, dyn_string &dsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_getKeyConfiguration(sDeviceDpName, UN_UNICOS_CONFIGURATION, sKey, dsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_setKeyDeviceConfiguration
/** Set the device dpe device configuration for a given key, an empty dyn_string for a key remove the key entry
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sKey input, the key
@param dsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_setKeyDeviceConfiguration(string sDeviceDpName, string sKey, dyn_string dsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_setKeyConfiguration(sDeviceDpName, UN_DEVICE_CONFIGURATION, sKey, dsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getKeyDeviceConfiguration
/** Get the device dpe device configuration for a given key
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sKey input, the key
@param dsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_getKeyDeviceConfiguration(string sDeviceDpName, string sKey, dyn_string &dsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_getKeyConfiguration(sDeviceDpName, UN_DEVICE_CONFIGURATION, sKey, dsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_setUnicosConfiguration
/** Set the device dpe unicos configuration, an empty dyn_string for a key remove the key entry
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param dsKey input, the key
@param ddsConfig input, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_setUnicosConfiguration(string sDeviceDpName, dyn_string dsKey, dyn_dyn_string ddsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_setConfiguration(sDeviceDpName, UN_UNICOS_CONFIGURATION, dsKey, ddsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_setDeviceConfiguration
/** Set the device dpe device configuration, an empty dyn_string for a key remove the key entry
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param dsKey input, the key
@param ddsConfig input, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_setDeviceConfiguration(string sDeviceDpName, dyn_string dsKey, dyn_dyn_string ddsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_setConfiguration(sDeviceDpName, UN_DEVICE_CONFIGURATION, dsKey, ddsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getUnicosConfiguration
/** Get the device dpe unicos configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param dsKey output, the key
@param ddsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_getUnicosConfiguration(string sDeviceDpName, dyn_string &dsKey, dyn_dyn_string &ddsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_getConfiguration(sDeviceDpName, UN_UNICOS_CONFIGURATION, dsKey, ddsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getDeviceConfiguration
/** Get the device dpe device configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param dsKey output, the key
@param ddsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
unGenericDpFunctions_getDeviceConfiguration(string sDeviceDpName, dyn_string &dsKey, dyn_dyn_string &ddsConfig, dyn_string &exceptionInfo)
{
  _unGenericDpFunctions_getConfiguration(sDeviceDpName, UN_DEVICE_CONFIGURATION, dsKey, ddsConfig, exceptionInfo);
}

// -------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------
// for config file edition, will be replaced
// BEGIN
// -------------------------------------------------------------------------------------
unGenericDpFunctions_cleanEmptySectionFromFile(string fileName, string &exceptionText)
{
	string fileInString;
	dyn_string dynFileLines, section;
	int pos, i, j, nLines;
	bool bNotEmpty;

	if(!access(fileName, F_OK)) {
		if(fileToString(fileName, fileInString)) {
			dynFileLines = strsplit(fileInString, "\n");
//DebugN("start", dynFileLines);
			for(i=1;i<=dynlen(dynFileLines);i++) {
				pos = strpos(dynFileLines[i], "[");
				if(pos>=0) {
// get the next section pos
					unGenericDpFunctions_getNextSection(dynFileLines, i+1, section, nLines);
// look if this section is empty
					bNotEmpty = false;
					for(j=1;j<=nLines; j++) {
						if(section != "")
							bNotEmpty = true;
					}
					if(!bNotEmpty) {
						unGenericDpFunctions_removeLinesFrom(dynFileLines, i, nLines);
					}
				}
//DebugN(pos, dynFileLines[i], patternToRemove[1]);
			}
//DebugN("end", dynFileLines);
// save the file
			unGenericDpFunctions_fwInstallation_saveFile(dynFileLines, fileName, exceptionText);
		}
		else
			exceptionText = "cannot read file: "+fileName;
	}
	else
			exceptionText = "wrong file: "+fileName;
}
unGenericDpFunctions_getNextSection(dyn_string dynFileLines, int posToStart, dyn_string &section, int &nb)
{
	int i, pos;
	nb = 0;
	section = makeDynString();
	
	for(i=posToStart;i<=dynlen(dynFileLines);i++) {
		pos = strpos(dynFileLines[i], "[");
		if(pos<0) {
			section[dynlen(section)+1] = dynFileLines[i];
			nb++;
		}
		else
			i = dynlen(dynFileLines)+1;
	}
//DebugN("getNextSec", dynFileLines[posToStart-1], section, nb);
}

unGenericDpFunctions_removeLinesFrom(dyn_string &dynFileLines, int posToStart, int nb)
{
	int i, j, len;
	dyn_string temp;
	
//	DebugN("replace", posToStart, dynFileLines[posToStart], dynFileLines[posToStart+nb], nb);
	len = dynlen(dynFileLines);
	for(i=1;i<posToStart;i++) {
		temp[i] = dynFileLines[i];
	}
	for(i=(posToStart+nb+1); i<=dynlen(dynFileLines);i++)
		temp[dynlen(temp)+1] = dynFileLines[i];
	dynFileLines = temp;
}
unGenericDpFunctions_removeLinesWithPatternFromFile(string fileName, dyn_string patternToRemove, string &exceptionText)
{
	string fileInString;
	dyn_string dynFileLines;
	int pos, i, j;

	if(!access(fileName, F_OK)) {
		if(fileToString(fileName, fileInString)) {
			dynFileLines = strsplit(fileInString, "\n");
//DebugN("start", dynFileLines);
			for(j=1;j<=dynlen(patternToRemove);j++) {
				for(i=1;i<=dynlen(dynFileLines);i++) {
					pos = strpos(dynFileLines[i], patternToRemove[j]);
					if(pos>=0)
						dynRemove( dynFileLines, i);
//DebugN(pos, dynFileLines[i], patternToRemove[1]);
				}
			}
//DebugN("end", dynFileLines);
// save the file
			unGenericDpFunctions_fwInstallation_saveFile(dynFileLines, fileName, exceptionText);

		}
		else
			exceptionText = "cannot read file: "+fileName;
	}
	else
		exceptionText = "wrong read file: "+fileName;
}

int unGenericDpFunctions_fwInstallation_saveFile( dyn_string & configLines, string filename, string &exceptionText)
{
	int i;
	string strLinesToSave;
	

	file fileHdlConfig;
	
	int writeResult;

			// open the file for writing
			fileHdlConfig = fopen(filename, "w");
			
			// if the file is not opened
			if(fileHdlConfig == 0)
			{
				exceptionText = "cannot open file: "+filename;
			}
			else
			{
					// copy each line from a dyn_string into string and separate the lines with newline character
					for(i = 1; i <= dynlen(configLines); i++)
					{
						strLinesToSave += configLines[i] + "\n";
					}
					
					// save the string into the file
					writeResult = fputs(strLinesToSave , fileHdlConfig);
			
					// check the result of writing to a file
					if (writeResult < 0)
					{
						exceptionText = "cannot save file: "+filename;
					}
					
					// close the file					
					fclose(fileHdlConfig);
			}
		
}

bool unGenericDpFunctions_InstallFile(string sComponent, dyn_string dsFilesToCreate)
{
  int i, len, iRes;
  string sError;
  string sSrcFile, sDstFile, installPath;
  dyn_dyn_string componentsInfo;
  bool bResult;
  
  // check if the component is installed
  if(!isFunctionDefined("fwInstallation_copyFile")) {
    DebugN("ERROR in "+sComponent+" postInstall -> fwInstallation_copyFile() function not available - installation tool is out of date");
    sError = "To complete the installation you need to copy:";
    len = dynlen(dsFilesToCreate);
    for(i=1;i<=len;i++)
    {
      // get the file name
      unGenericDpFunctions_getFile(sComponent, dsFilesToCreate[i], sSrcFile, sDstFile, "your components' bin");
      sError += "\n"+sSrcFile+" to "+sDstFile;
    }
    sError +="\n";
    
    DebugN(sError);
    return false;
  }
  
  // get the installed component path
  iRes = fwInstallation_getInstalledComponents(componentsInfo);
  if(iRes == -1) {
    DebugN("ERROR in "+sComponent+" postInstall -> cannot get installed components");
    return false;
  }
  len=dynlen(componentsInfo);
  for(i=1;i<=len;i++){
    if(componentsInfo[i][1]== sComponent) {
      installPath = componentsInfo[i][3];
      i = len+1;
    }
  }
  if(installPath=="") {
    DebugN("ERROR in "+sComponent+" postInstall -> cannot get the installation path");
    return false;
  }
  installPath +="/bin";
  // create all libs.
  len = dynlen(dsFilesToCreate);
  bResult=true;
  for(i=1;(i<=len) & bResult;i++)
  {
    // check if the file exits
    unGenericDpFunctions_getFile(sComponent, dsFilesToCreate[i], sSrcFile, sDstFile, installPath);
    // delete it if it exists
    if(access(sDstFile,F_OK) == 0) {
      DebugN("INFO: "+sComponent+".postInstall","deleting "+sDstFile);
      remove(sDstFile);
      delay(2);
    }
    // copy the new one
    DebugN("INFO: "+sComponent+".postInstall","copyFile "+sSrcFile+" -> "+sDstFile);
    iRes=fwInstallation_copyFile(sSrcFile, sDstFile);
    if(iRes != 0) {
      bResult = false;
      DebugN("WARNING: "+sComponent+".postInstall the installation of "+sDstFile+" has failed");
    }
  }
  return bResult;
}

unGenericDpFunctions_getFile(string sComponent, string sFileToCreate, string &sSrcFile, string &sDstFile, string sFolder)
{
  string sFile;
  
  // get the correct file version
  sFile = sFileToCreate + "_"+VERSION;
  sSrcFile = sFolder+"/"+sComponent+"/"+sFile;
  sDstFile = sFolder+"/"+sFileToCreate;
}

// -------------------------------------------------------------------------------------
// END
// for config file edition, will be replaced

// -------------------------------------------------------------------------------------

//@}



