/**@name LIBRARY: unGenericDpFunctionsHMI.ctl

@author: Herve Milcent (LHC-IAS)

Creation Date: 31/04/2002

Modification History: 
  11/02/2008: Herve
    - use JCOP Access contorl function

version 1.0

External Functions: 
	. unGenericDpFunctionsHMI_setCallBack_user: set a call back function when a user logs in or logs out

Internal Functions: 
	
Purpose: 
This library contains generic functions.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
//@{

// unGenericDpFunctionsHMI_setCallBack_user
/**
Purpose:
set a call back function when a user logs in or logs out.

In case of error, iResult is set to -1 and an exception is raised in exceptionInfo.

The sCallBackFunction must be like:
sCallBackFunction(string sUi, string sUser)
{
}
sUi: manager data point name
sUser: the user name of the user logs in.

	@param sCallBackFunction: string, input, the name of the call back function
	@param iResult: int, output, the result of the dpConnect
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: Linux, NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericDpFunctionsHMI_setCallBack_user(string sCallBackFunction, int &iResult, dyn_string &exceptionInfo) 
{
  if(sCallBackFunction == "") 
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneral", "CBLOCKWRONGCB"),"");
  else
    fwAccessControl_setupPanel(sCallBackFunction, exceptionInfo);
}

//@}
