//---------------------------------------------------------------------------------------------------------------------------------------

/** Checks if there is at least one active comment for the provided device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui Ctrl

@param sDeviceDp   string, the DP name of the device
@return does device have at least one active comment

@deprecated 2018-08-02

*/
bool unGenericObject_hasActiveComment(string sDeviceDp)
{

  FWDEPRECATED();

    bool hasActiveComment;
    
    dpGet(sDeviceDp + "statusInformation.deviceLogActive", hasActiveComment);
    
    return hasActiveComment;
}







//------------------------------------------------------------------------------------------------------------------------
// unGenericObject_multipleSelect
/** multiple selection of device
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDpName  input, list of device name DP name
@param bSelect input, true=select/false=de-select
@param exceptionInfo output, errors are returned here

@deprecated 2018-08-02

*/
unGenericObject_multipleSelect(dyn_string dsDpName, bool bSelect, dyn_string &exceptionInfo)
{

  FWDEPRECATED();

  bool bRes;
  int i, len=dynlen(dsDpName);
  dyn_string dsDpToSelect;
  string deviceName, sAlias, message, sSystem;
  
  for(i=1;i<=len;i++)
  {
    if(dpExists(dsDpName[i]))
    {
      deviceName = unGenericDpFunctions_getDpName(dsDpName[i]);
      unGenericObject_NeedSelect(dpTypeName(deviceName), bRes, exceptionInfo);
      if(bRes)
      {
        dynAppend(dsDpToSelect, deviceName);
        if(sAlias == "")
          sAlias = unSendMessage_getDeviceDescription(deviceName);
        else
          sAlias += " ,"+unSendMessage_getDeviceDescription(deviceName);
      }
    }
  }
  if(dynlen(dsDpToSelect) > 0)
  {
    unSelectDeselectHMI_multipleSelect(bSelect, exceptionInfo, dsDpToSelect);
    message = bSelect ? "Multiple Select " : "Multiple Deselect ";
    unSendMessage_toAllUsers(message + sAlias , exceptionInfo);
  }
}

/**Is bit set in StsReg value
      
@TODO: review if function can be replaced by getBit() with value casted to bit32
    
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param value (StsReg) value
@param bitPosition bit position

@deprecated 2019-06-04

*/
bool unGenericObject_getBit(int value, int bitPosition) {
    FWDEPRECATED();
    int pattern = (int)(pow(2, bitPosition));
    return value & pattern;
}