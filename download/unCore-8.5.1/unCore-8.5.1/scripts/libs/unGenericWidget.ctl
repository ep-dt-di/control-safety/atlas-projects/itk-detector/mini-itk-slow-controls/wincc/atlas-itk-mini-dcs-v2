/**@name LIBRARY: unGenericWidget.ctl
 
@author: Vincent Forest (AB-CO)

Creation Date: 21 05 2003 

Modification History: 
  02/02/2009: Herve
    - do a beep in case of alarm device in the unicosHMI animationCB
    
	01/10/2007: Herve
		- bug bargraph in case of DPE in a struct type DPE
		
	19/07/2007: Herve
		- use unGenericObject_GetTrendDPEList instead of unGenericObject_GetFaceplateTrendConfig
		
	26/06/2007: Herve
		- move all analogInput bargraph widget function to unGenericWidget
		
	19/02/2007: Herve
		- Bit animation: tooltiptext use alias of DPE if exists (replace / by :) if not --> alias device
	
	05/02/2007:
		- unGenericWidget_BitWidgetAnimationCB: use in insead of bit32 in case DPE of type float
		
	23/10/2006: Herve
		- add 2 texts
		
	16/10/2006: Herve
		- implement beep animation for unicosHMI
		
	12/10/2006: Herve
		- use g_sDPE instead of stsReg01

version 1.0

Purpose: This library contains generic functions for generic widget like Bit widget.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. Unicos objects (DPType etc.)
	. PVSS version: 3.0.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

//@{

//------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_WidgetRegisterCB
/**
Purpose: register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in the widget
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{UNICOSWidget}
*/

unGenericWidget_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
	switch(g_sWidgetType) {
		case "unicosHMI":
			unGenericWidget_unicosHMIWidgetRegisterCB(sDp, bSystemConnected);
			break;
		case "Bit":
			unGenericWidget_BitWidgetRegisterCB(sDp, bSystemConnected);
			break;
		case "bargraph":
		case "bargraphSimple":
			unGenericWidget_bargraphWidgetRegisterCB(sDp, bSystemConnected);
			break;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_WidgetDisconnection
/**
Purpose: Animate widget disconnection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{UNICOSWidget}
*/

unGenericWidget_WidgetDisconnection(string sWidgetType)
{
	switch(sWidgetType) {
		case "unicosHMI":
			unGenericWidget_unicosHMIWidgetDisconnection(sWidgetType);
			break;
		case "Bit":
			unGenericWidget_BitWidgetDisconnection(sWidgetType);
			break;
		case "bargraph":
		case "bargraphSimple":
			unGenericWidget_bargraph_disconnection(sWidgetType);
			break;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_BitWidgetRegisterCB
/**
Purpose: register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in the widget
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
*/

unGenericWidget_BitWidgetRegisterCB(string sDp, bool bSystemConnected)
{
	string deviceName, sAlias;
	int iAction;
	dyn_string exceptionInfo;
	string sPlcName, sSystemName;
	bool bRemote;

//DebugN(g_iBit, g_sDPE, g_sValue_1_Color, g_sValue_0_Color);
	deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
	if (deviceName == "")	// In case of disconnection
		{
		deviceName = g_sDpName;
		}
// get the systemName
	sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
	sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);
	
	unDistributedControl_isRemote(bRemote, sSystemName);
	if(bRemote)
		g_bSystemConnected = bSystemConnected;
	else
		g_bSystemConnected = true;

	if(sPlcName != "") {
		if (dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName)) {
			g_bUnSystemAlarmPlc = true;
		}
		else {
			g_bUnSystemAlarmPlc = false;
		}
	}
	else 
		g_bUnSystemAlarmPlc = false;
		
	unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
	switch (iAction)
		{
		case UN_ACTION_DISCONNECT:
			unGenericWidget_BitWidgetDisconnection(g_sWidgetType);
			break;
		case UN_ACTION_DPCONNECT:
			g_sDpName = deviceName;
			unGenericWidget_BitWidgetConnect(deviceName, sPlcName, sSystemName);
			sAlias = unGenericDpFunctions_getAlias(deviceName + g_sDPE);
//DebugN(sAlias);
			if(sAlias == deviceName + g_sDPE)
				sAlias = substr($sIdentifier, strpos($sIdentifier, ":") + 1);
			else
				strreplace(sAlias, "/", ":");
			Body1.toolTipText = sAlias;
			break;
		case UN_ACTION_DPDISCONNECT_DISCONNECT:
			unGenericWidget_BitWidgetDisconnect(deviceName, sPlcName, sSystemName);
			unGenericWidget_BitWidgetDisconnection(g_sWidgetType);
			break;
		case UN_ACTION_NOTHING:
		default:
			break;
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_BitWidgetConnect
/**
Purpose: Connect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
*/
unGenericWidget_BitWidgetConnect(string deviceName, string sPlcName, string sSystemName)
{
	int iRes;
	
	if(g_bUnSystemAlarmPlc)
	{
		if(dpExists(deviceName+g_sDPE))
		{
			iRes = dpConnect("unGenericWidget_BitWidgetAnimationCB", 
								deviceName + g_sDPE,
								deviceName + g_sDPE+":_online.._invalid",
								sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
								sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");

		}
		else
			unGenericWidget_BitWidgetDisconnection(g_sWidgetType);
	}
	else 
		unGenericWidget_BitWidgetDisconnection(g_sWidgetType);
	g_bCallbackConnected = (iRes >= 0);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_BitWidgetDisconnect
/**
Purpose: Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
*/
unGenericWidget_BitWidgetDisconnect(string deviceName, string sPlcName, string sSystemName)
{
	int iRes;

	if(g_bUnSystemAlarmPlc)
	{
		iRes = dpDisconnect("unGenericWidget_BitWidgetAnimationCB", 
								deviceName + g_sDPE,
								deviceName + g_sDPE+":_online.._invalid",
								sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
								sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
		Body1.toolTipText = "";
	}
	g_bCallbackConnected = !(iRes >= 0);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_BitWidgetAnimationCB
/**
Purpose: Animate widget

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/

unGenericWidget_BitWidgetAnimationCB(string sDpe, int iDPE, string sDpeInv, bool bInvalid,
																				string sDpAl, int iAlValue, string sDpEnabled, bool bPlcEnabled)
{
	string sWarningLetter, sWarningColor, sForeColor, sBodyColor;
	bit32 bit32DPE=(bit32)iDPE;
	
	if ((!bPlcEnabled) || (iAlValue > c_unSystemIntegrity_no_alarm_value))
	{
		sWarningLetter = UN_WIDGET_TEXT_OLD_DATA;
		sWarningColor = "unDataNotValid";
	}

	if (bInvalid)
	{
		sWarningLetter = UN_WIDGET_TEXT_INVALID;
		sWarningColor = "unDataNotValid";
	}

	unGenericWidget_BitWidgetGetColor(g_bBADState, getBit(bit32DPE, g_iBit), g_sValue_OK_Color, g_sValue_BAD_Color, 
																			sBodyColor, sForeColor);

	if(g_bSystemConnected)
		setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor, 
										"Body1", "backCol", sBodyColor, "Body1", "foreCol", sForeColor);
	
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_BitWidgetGetColor
/**
Purpose: Get the colors

Parameters:

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
*/

unGenericWidget_BitWidgetGetColor(bool bBADState, bool bValue, string sValue_OK_Color, string sValue_BAD_Color, string &sBodyColor, string &sForeColor)
{
	if(bBADState) {
		if(bValue) {
			sForeColor = sValue_BAD_Color;
			sBodyColor = sValue_BAD_Color;
		}
		else
			sForeColor = sValue_OK_Color;
	}
	else {
		if(bValue) {
			sForeColor = sValue_OK_Color;
			sBodyColor = sValue_OK_Color;
		}
		else
			sForeColor = sValue_BAD_Color;
	}		
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_BitWidgetDisconnection
/**
Purpose: Animate widget disconnection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
*/

unGenericWidget_BitWidgetDisconnection(string sWidgetType)
{
	setMultiValue("WarningText", "text", "", "Body1", "backCol", "unDataNoAccess", "Body1", "foreCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unAlarm_WidgetRegisterCB
/**
Purpose: register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm widget
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericWidget_unicosHMIWidgetRegisterCB(string sDp, bool bSystemConnected)
{
	string deviceName;
	int iAction, iRes, iAlertType1;
	dyn_string exceptionInfo, split;
	string sPlcName, sSystemName;
	bool bRemote;
		
	deviceName = unGenericDpFunctions_getWidgetDpName(g_sBeepAlarmDevice);
	if (deviceName == "")	// In case of disconnection
		{
		deviceName = g_sDpName;
		}
// get the systemName
	sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
	sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);
	unDistributedControl_isRemote(bRemote, sSystemName);
	if(bRemote)
		g_bSystemConnected = bSystemConnected;
	else
		g_bSystemConnected = true;

	if(sPlcName != "") {
		if (dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName)) {
			g_bUnSystemAlarmPlc = true;
		}
		else {
			g_bUnSystemAlarmPlc = false;
		}
	}
	else
		g_bUnSystemAlarmPlc = false;

	unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
	switch (iAction)
		{
		case UN_ACTION_DISCONNECT:
			unGenericWidget_unicosHMIWidgetDisconnection(g_sWidgetType);
			break;
		case UN_ACTION_DPCONNECT:
			g_sDpName = deviceName;
			
//  check if the device name is of type "Alarm" if not like of no PLC. -> not available.
			if(dpTypeName(deviceName) != "Alarm")
				g_bUnSystemAlarmPlc = false;
				
			if(g_bUnSystemAlarmPlc) {
				iRes = dpConnect("unGenericWidget_unicosHMIWidgetAnimationCB",
							deviceName + ".ProcessInput.StsReg01",
							deviceName + ".ProcessInput.StsReg01:_online.._invalid");
			}
			else 
				unGenericWidget_unicosHMIWidgetDisconnection(g_sWidgetType);

			g_bCallbackConnected = (iRes >= 0);
			break;
		case UN_ACTION_DPDISCONNECT_DISCONNECT:
			if(g_bUnSystemAlarmPlc) {
				iRes = dpDisconnect("unGenericWidget_unicosHMIWidgetAnimationCB",
							deviceName + ".ProcessInput.StsReg01",
							deviceName + ".ProcessInput.StsReg01:_online.._invalid");				
			}
			g_bCallbackConnected = !(iRes >= 0);
			unGenericWidget_unicosHMIWidgetDisconnection(g_sWidgetType);
			break;
		case UN_ACTION_NOTHING:
		default:
			break;
		}
		Beep.visible = true;
		BeepText1.visible = true;
		BeepText2.visible = true;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_unicosHMIWidgetAnimationCB
/**
Purpose: Animate widget

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in Alarm faceplate
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/

unGenericWidget_unicosHMIWidgetAnimationCB(string sDpStsReg01, bit32 bit32StsReg01, string sDpInv, bool bStsReg01Invalid)
{
	string beepColor;
	
	if (bStsReg01Invalid)
	{
		beepColor = "unDataNotValid";
	}
	else
	{
		if(getBit(bit32StsReg01, UN_STSREG01_ALMSKST) == 1)
		{
			beepColor = "unAlarmMasked";
		}
		else if(getBit(bit32StsReg01, UN_STSREG01_ALUNACK) == 1)
		{
			beepColor = "unWidget_AlarmNotAck";
		}
		else if(getBit(bit32StsReg01, UN_STSREG01_POSST) == 1) 
		{
			beepColor = "unFaceplate_AlarmActive";
                        beep(400,2000);
		}
		else
		{
			beepColor = "unAlarm_Ok";
		}
	}
	if(g_bSystemConnected)
		setMultiValue("Beep", "backCol", beepColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_unicosHMIWidgetDisconnection
/**
Purpose: Animate widget disconnection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0.1 
	. operating system: XP.
	. distributed system: yes.
*/

unGenericWidget_unicosHMIWidgetDisconnection(string sWidgetType)
{
	setMultiValue("Beep", "backCol", "unDataNoAccess");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraphWidgetRegisterCB
/**
Purpose: register callback function for bargraph

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in AnalogInput widget
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericWidget_bargraphWidgetRegisterCB(string sDp, bool bSystemConnected)
{
	string deviceName, deviceType, sSystemName, sPlcName, sDeviceDPE;
	int iAction, iRes;
	dyn_string exceptionInfo;
	bool bRemote;
	
//	deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
	unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE($sIdentifier, sDeviceDPE);
//DebugN($sIdentifier, sDeviceDPE, deviceName);
	if(sDeviceDPE == $sIdentifier)
		deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
	else if (sDeviceDPE != "")
		deviceName = unGenericDpFunctions_getDpName(sDeviceDPE);
	else
		deviceName == "";
	
	if (deviceName == "")	// In case of disconnection
		{
		deviceName = g_sDpName;
		}

// get the systemName
	sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
	sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);
	
	//////////////// 
	unDistributedControl_isRemote(bRemote, sSystemName);
	if(bRemote)
		g_bSystemConnected = bSystemConnected;
	else
		g_bSystemConnected = true;
	////////////////
	if(sPlcName != "") {
		if (dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName)) {
			g_bUnSystemAlarmPlc = true;
		}
		else {
			g_bUnSystemAlarmPlc = false;
		}
	}
	else
		g_bUnSystemAlarmPlc = false;
		
	unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
//DebugN($sIdentifier, deviceName, sDeviceDPE);
	switch (iAction)
		{
		case UN_ACTION_DISCONNECT:
			unGenericWidget_bargraph_disconnection(g_sWidgetType);
			break;
		case UN_ACTION_DPCONNECT:
//DebugN("unAnalogInput_WidgetRegisterCB: connect");

			g_sDpName = deviceName;
			g_bDisconnectCB = false;

		if(g_bUnSystemAlarmPlc) {
	//  check if the device name is of type "AnalogInput" or "AnalogOutput" if not like if no PLC. -> not available.
				deviceType = dpTypeName(deviceName);
				g_sDeviceType = deviceType;
				g_sDeviceDPE = sDeviceDPE;
				if((deviceType == "AnalogInput") || (deviceType == "AnalogOutput"))
					iRes = dpConnect("unAnalogInput_WidgetLockCallBack", deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
				else{
					unGenericWidget_bargraph_WidgetConnect(deviceName);
//					unGenericWidget_bargraph_disconnection(g_sWidgetType);
				}

				g_bCallbackConnected = (iRes >= 0);
			}
			else
					unGenericWidget_bargraph_disconnection(g_sWidgetType);
			break;
		case UN_ACTION_DPDISCONNECT_DISCONNECT:
//DebugN("unAnalogInput_WidgetRegisterCB: disconnect");
			if((g_bUnSystemAlarmPlc) && ((g_sDeviceType == "AnalogInput") || (deviceType == "AnalogOutput")))
				iRes = dpDisconnect("unAnalogInput_WidgetLockCallBack", deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");

			if(g_bDisconnectCB) {
				switch(g_sWidgetType) {
					case "bargraph":
					case "bargraphSimple":
						unGenericWidget_bargraph_WidgetDisconnect(g_sDpName, iRes);
						break;
					default:
						break;
				}
			}
			g_bCallbackConnected = !(iRes >= 0);
			g_bDisconnectCB = false;

			unGenericWidget_bargraph_disconnection(g_sWidgetType);
			break;
		case UN_ACTION_NOTHING:
		default:
			break;
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_WidgetConnect
/**
Purpose: Animate bargraph widget connection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_bargraph_WidgetConnect(string deviceName)
{
	switch(g_sDeviceType) {
		case "AnalogInput":
		case "AnalogOutput":
			unGenericWidget_AnalogInputAnalogOutput_bargraph_WidgetConnect(deviceName);
			break;
		default:
			unGenericWidget_AnyDevice_bargraph_WidgetConnect(deviceName);
			break;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_AnyDevice_bargraph_WidgetConnect
/**
Purpose: Animate bargraph widget connection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_AnyDevice_bargraph_WidgetConnect(string deviceName)
{
	int iRes, iRangeType, iPos, len;
	dyn_string exceptionInfo;
	string sPlcName, sSystemName, sTemp;
	dyn_string dsAnalog, dsRange;
		
// get the systemName
	sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
	sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);
	if(sPlcName != "") {
		if (dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName)) {
			g_bUnSystemAlarmPlc = true;
		}
		else {
			g_bUnSystemAlarmPlc = false;
		}
	}
	else
		g_bUnSystemAlarmPlc = false;

	if(g_bUnSystemAlarmPlc) {
		unGenericObject_GetTrendDPEList(g_sDeviceType, dsAnalog, dsRange, exceptionInfo);
		sTemp = g_sDeviceDPE;
		strreplace(sTemp, deviceName+".ProcessInput.", "");
		iPos = dynContains(dsAnalog, sTemp);
		if(iPos > 0) {
			g_sDeviceRangeDPE = g_sDeviceDPE;
			strreplace(g_sDeviceRangeDPE, dsAnalog[iPos], dsRange[iPos]);
			if(dpExists(g_sDeviceRangeDPE)) {
				iRes = dpGet(g_sDeviceRangeDPE + ":_pv_range.._type", iRangeType);
				g_sPosStFormat = dpGetFormat(g_sDeviceDPE);
				g_sPosStUnit = dpGetUnit(g_sDeviceDPE);

				if ((iRes >= 0) && (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK))
					g_iRangeType = DPCONFIG_MINMAX_PVSS_RANGECHECK;
				else {
					g_iRangeType = DPCONFIG_NONE;
					g_bUnSystemAlarmPlc = false;
				}
			}
			else
				g_bUnSystemAlarmPlc = false;
		}
		else
			g_bUnSystemAlarmPlc = false;
	}
//DebugN("ANY", deviceName, g_sDeviceDPE, dsAnalog, dsRange, g_bUnSystemAlarmPlc, g_sDeviceRangeDPE);
	if(g_bUnSystemAlarmPlc) {
//DebugN(deviceName, g_sDeviceDPE, g_sDeviceRangeDPE);
		iRes = dpConnect("unGenericWidget_bargraph_noAlertCB", g_sDeviceDPE, 
												g_sDeviceDPE + ":_online.._invalid",
												g_sDeviceRangeDPE + ":_pv_range.._min",
												g_sDeviceRangeDPE + ":_pv_range.._max",
												sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
												sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
		g_bDisconnectCB = true;
	}
	else {
		unGenericWidget_bargraph_disconnection();
		g_bDisconnectCB = false;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_AnalogInputAnalogOutput_bargraph_WidgetConnect
/**
Purpose: Animate bargraph widget connection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_AnalogInputAnalogOutput_bargraph_WidgetConnect(string deviceName)
{
	int iRes, iAlertType1, iRangeType;
	dyn_string exceptionInfo;
	string sPlcName, sSystemName;
		
//DebugN("AIAO", deviceName);
// get the systemName
	sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
	sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);
	if(sPlcName != "") {
		if (dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName)) {
			g_bUnSystemAlarmPlc = true;
		}
		else {
			g_bUnSystemAlarmPlc = false;
		}
	}
	else
		g_bUnSystemAlarmPlc = false;
		
	unGenericObject_CheckAnalogAlarmType(deviceName + ".ProcessInput.PosSt", iAlertType1, exceptionInfo);
	g_bBoolAlert = (iAlertType1 == DPCONFIG_ALERT_NONBINARYSIGNAL);

	iRes = dpGet(deviceName + ".ProcessInput.PosSt:_pv_range.._type", iRangeType);
	g_sPosStFormat = dpGetFormat(deviceName + ".ProcessInput.PosSt");
	g_sPosStUnit = dpGetUnit(deviceName + ".ProcessInput.PosSt");
	
	if ((iRes >= 0) && (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
		g_iRangeType = DPCONFIG_MINMAX_PVSS_RANGECHECK;
		}
	else
		{
		g_iRangeType = DPCONFIG_NONE;
		}
	if(g_bUnSystemAlarmPlc) {
		if ((g_bBoolAlert) && (g_iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
			switch(g_iAlertRange) {
				case UN_AIAO_ALARM_5_HH_H_L_LL:
					iRes = dpConnect("unGenericWidget_bargraph_rangeAlertCB", deviceName + ".ProcessInput.PosSt", 
														deviceName + ".ProcessInput.PosSt:_online.._invalid",
														deviceName + ".ProcessInput.PosSt:_pv_range.._min",
														deviceName + ".ProcessInput.PosSt:_pv_range.._max",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.2._u_limit",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.4._l_limit",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.5._l_limit",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
				case UN_AIAO_ALARM_4_HH_H_L:
				case UN_AIAO_ALARM_4_H_L_LL:
					iRes = dpConnect("unGenericWidget_bargraph_rangeAlertCB_4_ranges", deviceName + ".ProcessInput.PosSt", 
														deviceName + ".ProcessInput.PosSt:_online.._invalid",
														deviceName + ".ProcessInput.PosSt:_pv_range.._min",
														deviceName + ".ProcessInput.PosSt:_pv_range.._max",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.2._u_limit",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.4._l_limit",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
				case UN_AIAO_ALARM_3_H_L:
				case UN_AIAO_ALARM_3_HH_H:
				case UN_AIAO_ALARM_3_LL_L:
					iRes = dpConnect("unGenericWidget_bargraph_rangeAlertCB_3_ranges", deviceName + ".ProcessInput.PosSt", 
														deviceName + ".ProcessInput.PosSt:_online.._invalid",
														deviceName + ".ProcessInput.PosSt:_pv_range.._min",
														deviceName + ".ProcessInput.PosSt:_pv_range.._max",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.3._l_limit",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
				case UN_AIAO_ALARM_2_H:
				case UN_AIAO_ALARM_2_L:
					iRes = dpConnect("unGenericWidget_bargraph_rangeAlertCB_2_ranges", deviceName + ".ProcessInput.PosSt", 
														deviceName + ".ProcessInput.PosSt:_online.._invalid",
														deviceName + ".ProcessInput.PosSt:_pv_range.._min",
														deviceName + ".ProcessInput.PosSt:_pv_range.._max",
														deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
			}
		}
		if ((!g_bBoolAlert) && (g_iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
			iRes = dpConnect("unGenericWidget_bargraph_noAlertCB", deviceName + ".ProcessInput.PosSt", 
													deviceName + ".ProcessInput.PosSt:_online.._invalid",
													deviceName + ".ProcessInput.PosSt:_pv_range.._min",
													deviceName + ".ProcessInput.PosSt:_pv_range.._max",
													sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
													sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
		}	
		if ((g_bBoolAlert) && (g_iRangeType != DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
			unGenericWidget_bargraph_disconnection();
		}
		if ((!g_bBoolAlert) && (g_iRangeType != DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
			unGenericWidget_bargraph_disconnection();
		}
	}
	else
		unGenericWidget_bargraph_disconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_WidgetDisconnect
/**
Purpose: Animate bargraph widget disconnect

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_bargraph_WidgetDisconnect(string deviceName, int &iRes)
{
	if(g_bUnSystemAlarmPlc) {
		switch(g_sDeviceType) {
			case "AnalogInput":
			case "AnalogOutput":
				unGenericWidget_AnalogInputOutput_bargraph_WidgetDisconnect(deviceName, iRes);
				break;
			default:
				unGenericWidget_AnyDevice_bargraph_WidgetDisconnect(deviceName, iRes);
				break;
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_AnyDevice_bargraph_WidgetDisconnect
/**
Purpose: Animate bargraph widget disconnect

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_AnyDevice_bargraph_WidgetDisconnect(string deviceName, int &iRes)
{
	string sPlcName, sSystemName;

	if(g_bUnSystemAlarmPlc) {
// get the systemName
		sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
		sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);

		iRes = dpDisconnect("unGenericWidget_bargraph_noAlertCB", g_sDeviceDPE, 
												g_sDeviceDPE + ":_online.._invalid",
												g_sDeviceRangeDPE + ":_pv_range.._min",
												g_sDeviceRangeDPE + ":_pv_range.._max",
												sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
												sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_AnalogInputOutput_bargraph_WidgetDisconnect
/**
Purpose: Animate bargraph widget disconnect

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_AnalogInputOutput_bargraph_WidgetDisconnect(string deviceName, int &iRes)
{
	string sPlcName, sSystemName;

	if(g_bUnSystemAlarmPlc) {
// get the systemName
		sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
		sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);

		if ((g_bBoolAlert) && (g_iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
			switch(g_iAlertRange) {
				case UN_AIAO_ALARM_5_HH_H_L_LL:
					iRes = dpDisconnect("unGenericWidget_bargraph_rangeAlertCB", deviceName + ".ProcessInput.PosSt", 
																	deviceName + ".ProcessInput.PosSt:_online.._invalid",
																	deviceName + ".ProcessInput.PosSt:_pv_range.._min",
																	deviceName + ".ProcessInput.PosSt:_pv_range.._max",
																	deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
																	deviceName + ".ProcessInput.PosSt:_alert_hdl.2._u_limit",
																	deviceName + ".ProcessInput.PosSt:_alert_hdl.4._l_limit",
																	deviceName + ".ProcessInput.PosSt:_alert_hdl.5._l_limit",
																	sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
																	sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
				case UN_AIAO_ALARM_4_HH_H_L:
				case UN_AIAO_ALARM_4_H_L_LL:
					iRes = dpDisconnect("unGenericWidget_bargraph_rangeAlertCB_4_ranges", deviceName + ".ProcessInput.PosSt", 
																deviceName + ".ProcessInput.PosSt:_online.._invalid",
																deviceName + ".ProcessInput.PosSt:_pv_range.._min",
																deviceName + ".ProcessInput.PosSt:_pv_range.._max",
																deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
																deviceName + ".ProcessInput.PosSt:_alert_hdl.2._u_limit",
																deviceName + ".ProcessInput.PosSt:_alert_hdl.4._l_limit",
																sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
																sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
				case UN_AIAO_ALARM_3_H_L:
				case UN_AIAO_ALARM_3_HH_H:
				case UN_AIAO_ALARM_3_LL_L:
						iRes = dpDisconnect("unGenericWidget_bargraph_rangeAlertCB_3_ranges", deviceName + ".ProcessInput.PosSt", 
																deviceName + ".ProcessInput.PosSt:_online.._invalid",
																deviceName + ".ProcessInput.PosSt:_pv_range.._min",
																deviceName + ".ProcessInput.PosSt:_pv_range.._max",
																deviceName + ".ProcessInput.PosSt:_alert_hdl.3._l_limit",
																deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
																sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
																sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
				case UN_AIAO_ALARM_2_H:
				case UN_AIAO_ALARM_2_L:
						iRes = dpDisconnect("unGenericWidget_bargraph_rangeAlertCB_2_ranges", deviceName + ".ProcessInput.PosSt", 
																deviceName + ".ProcessInput.PosSt:_online.._invalid",
																deviceName + ".ProcessInput.PosSt:_pv_range.._min",
																deviceName + ".ProcessInput.PosSt:_pv_range.._max",
																deviceName + ".ProcessInput.PosSt:_alert_hdl.1._u_limit",
																sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
																sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
					break;
			}
		}
		if ((!g_bBoolAlert) && (g_iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK))
		{
			iRes = dpDisconnect("unGenericWidget_bargraph_noAlertCB", deviceName + ".ProcessInput.PosSt", 
												deviceName + ".ProcessInput.PosSt:_online.._invalid",
												deviceName + ".ProcessInput.PosSt:_pv_range.._min",
												deviceName + ".ProcessInput.PosSt:_pv_range.._max",
												sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
												sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");
		}	
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_disconnection
/**
Purpose: Animate bargraph widget disconnection

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_bargraph_disconnection()
{
	unGenericWidget_bargraph_viewRange(false);
	unGenericWidget_bargraph_viewAlarm(false);
	levelBargraph.scale(1, 1);
	levelBargraph.backCol = "unDataNoAccess";
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_noAlertCB
/**
Purpose: callback function for bargraph case no alert

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericWidget_bargraph_noAlertCB(string sDp1, float fPosSt, string sDpInv, bool bInvalid, string sDp2, float fMin, string sDp3, float fMax, string sDpAl, int iAlValue, string sDpEnabled, bool bPlcEnabled)
{
	unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, "", 0, "", 0, "", 0, "", 0, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_rangeAlertCB_4_ranges
/**
Purpose: callback function for bargraph case alert 3 ranges: H L, HH H, LL L

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericWidget_bargraph_rangeAlertCB_4_ranges(string sDp1, float fPosSt, string sDpInv, bool bInvalid, string sDp2, float fMin, string sDp3, float fMax, string sDp4, float f3, string sDp6, float f2, string sDp5, float f1, string sDpAl, int iAlValue, string sDpEnabled, bool bPlcEnabled)
{
	switch(g_iAlertRange)
	{
		case UN_AIAO_ALARM_4_HH_H_L:
			unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, "", 0, sDp4, f1, sDp6, f2, sDp5, f3, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
			break;
		case UN_AIAO_ALARM_4_H_L_LL:
			unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, sDp4, f1, sDp6, f2, sDp5, f3, "", 0, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
			break;
	}
}

// unGenericWidget_bargraph_rangeAlertCB_3_ranges
/**
Purpose: callback function for bargraph case alert 3 ranges: H L, HH H, LL L

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericWidget_bargraph_rangeAlertCB_3_ranges(string sDp1, float fPosSt, string sDpInv, bool bInvalid, string sDp2, float fMin, string sDp3, float fMax, string sDp4, float f3, string sDp5, float f1, string sDpAl, int iAlValue, string sDpEnabled, bool bPlcEnabled)
{
	switch(g_iAlertRange)
	{
		case UN_AIAO_ALARM_3_H_L:
			unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, "", 0, sDp4, f1, sDp5, f3, "", 0, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
		break;
		case UN_AIAO_ALARM_3_HH_H:
			unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, "", 0, "", 0, sDp4, f3, sDp5, f1, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
		break;
		case UN_AIAO_ALARM_3_LL_L:
			unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, sDp5, f1, sDp4, f3, "", 0, "", 0, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
		break;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_rangeAlertCB_2_ranges
/**
Purpose: callback function for bargraph case alert 2 ranges: H, L

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Callback}
*/
unGenericWidget_bargraph_rangeAlertCB_2_ranges(string sDp1, float fPosSt, string sDpInv, bool bInvalid, string sDp2, float fMin, string sDp3, float fMax, string sDp4, float f1, string sDpAl, int iAlValue, string sDpEnabled, bool bPlcEnabled)
{
	switch(g_iAlertRange)
	{
		case UN_AIAO_ALARM_2_H:
			unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, "", 0, "", 0, sDp4, f1, "", 0, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
		break;
		case UN_AIAO_ALARM_2_L:
			unGenericWidget_bargraph_rangeAlertCB(sDp1, fPosSt, sDpInv, bInvalid, sDp2, fMin, sDp3, fMax, "", 0, sDp4, f1, "", 0, "", 0, sDpAl, iAlValue, sDpEnabled, bPlcEnabled);
		break;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_rangeAlertCB
/**
Purpose: callback function for bargraph case alert all possible cases

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_bargraph_rangeAlertCB(string sDp1, float fPosSt, string sDpInv, bool bInvalid, string sDp2, float fMin, string sDp3, float fMax,
			 string sDp4, float fLL, string sDp5, float fL, string sDp6, float fH, string sDp7, float fHH,
			 string sDpAl, int iAlValue, string sDpEnabled, bool bPlcEnabled)
{
	string formattedValue;
	float fScale, fScaleX_max, fScaleY_max;
	int iPosX_min, iPosY_min, iPosX_max, iPosY_max, iPosX_repere, iPosY_repere;
	int iPosX_HH, iPosY_HH, iPosX_H, iPosY_H, iPosX_L, iPosY_L, iPosX_LL, iPosY_LL;
	float fMax_fMin;

	if ((sDp2 == "") && (sDp3 == ""))
	{
		unGenericWidget_bargraph_disconnection();
		return;
	}
// 1. Set range values
	formattedValue = unGenericObject_FormatValue(g_sPosStFormat, fMin);
	if(g_bSystemConnected && (g_sWidgetType == "bargraph"))
		setValue("rangeMin", "text", formattedValue + " " + g_sPosStUnit);
	formattedValue = unGenericObject_FormatValue(g_sPosStFormat, fMax);
 	if(g_bSystemConnected && (g_sWidgetType == "bargraph"))
		setValue("rangeMax", "text", formattedValue + " " + g_sPosStUnit);
// 2. Set scale
//DebugN(fMax, fMin, fMax - fMin);
	fMax_fMin = fMax - fMin;

	if(fMax_fMin != 0) 
		fScale = (fPosSt - fMin) / fMax_fMin;
	if (fScale > 1)
		{
		fScale = 1;
		}
	if (fScale < 0)
		{
		fScale = 0;
		}
	levelBargraph.scale(1, fScale);
// 3. Set label for PosSt
	formattedValue = unGenericObject_FormatValue(g_sPosStFormat, fPosSt);
 	if(g_bSystemConnected && (g_sWidgetType == "bargraph"))
		setValue("posSt", "text", formattedValue + " " + g_sPosStUnit);
	if((g_sWidgetType == "bargraph"))
		getValue("rangeMin", "position", iPosX_min, iPosY_min);
	if((g_sWidgetType == "bargraph")) {
		getValue("rangeMax", "position", iPosX_max, iPosY_max);
		getValue("rangeMax", "scale", fScaleX_max, fScaleY_max);
	}
	if((g_sWidgetType == "bargraph"))
		getValue("repere", "position", iPosX_repere, iPosY_repere);
 	if(g_bSystemConnected && (g_sWidgetType == "bargraph"))	
		setValue("posSt", "position", iPosX_repere, fScale * (iPosY_max - iPosY_min) + iPosY_min);
	if((g_sWidgetType == "bargraph"))
		getValue("lineMin", "position", iPosX_min, iPosY_min);
	if((g_sWidgetType == "bargraph"))
		getValue("lineMax", "position", iPosX_max, iPosY_max);
 	if(g_bSystemConnected && (g_sWidgetType == "bargraph"))	
		setValue("linePosSt", "position", iPosX_repere, fScale * (iPosY_max - iPosY_min) + iPosY_min);
// 4. Set alert lines
	if (g_bBoolAlert)
		{
		if ((fHH >= fMin) && (fHH <= fMax) && 
			((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_HH_H)
				|| (g_iAlertRange == UN_AIAO_ALARM_4_HH_H_L)))
			{
			g_bViewAlert_HH = true;
			if((g_sWidgetType == "bargraph"))
				getValue("lineHH", "position", iPosX_HH, iPosY_HH);

			if(fMax_fMin != 0)
				fScale = (fHH - fMin) / fMax_fMin;
			iPosY_HH = (int)(fScale * (iPosY_max - iPosY_min) + iPosY_min);
			 if(g_bSystemConnected && (g_sWidgetType == "bargraph"))
				setValue("lineHH", "position", iPosX_HH, iPosY_HH);
			}
		else
			{
			g_bViewAlert_HH = false;
			}
		if ((fH >= fMin) && (fH <= fMax) &&
				((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_HH_H) 
				|| (g_iAlertRange == UN_AIAO_ALARM_3_H_L) || (g_iAlertRange == UN_AIAO_ALARM_2_H)
				|| (g_iAlertRange == UN_AIAO_ALARM_4_HH_H_L) || (g_iAlertRange == UN_AIAO_ALARM_4_H_L_LL)))
			{
			g_bViewAlert_H = true;
			if((g_sWidgetType == "bargraph"))
				getValue("lineH", "position", iPosX_H, iPosY_H);

			if(fMax_fMin != 0)
				fScale = (fH - fMin) / fMax_fMin;	
			iPosY_H = (int)(fScale * (iPosY_max - iPosY_min) + iPosY_min);
			 if(g_bSystemConnected && (g_sWidgetType == "bargraph"))
			setValue("lineH", "position", iPosX_H, iPosY_H);
			}
		else
			{
			g_bViewAlert_H = false;
			}
		if ((fL >= fMin) && (fL <= fMax) && 
				((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_LL_L) 
				|| (g_iAlertRange == UN_AIAO_ALARM_3_H_L) || (g_iAlertRange == UN_AIAO_ALARM_2_L)
				|| (g_iAlertRange == UN_AIAO_ALARM_4_HH_H_L) || (g_iAlertRange == UN_AIAO_ALARM_4_H_L_LL)))
			{
			g_bViewAlert_L = true;
			if((g_sWidgetType == "bargraph"))
				getValue("lineL", "position", iPosX_L, iPosY_L);

			if(fMax_fMin != 0)
				fScale = (fL - fMin) / fMax_fMin;
			iPosY_L = (int)(fScale * (iPosY_max - iPosY_min) + iPosY_min);
			 if(g_bSystemConnected && (g_sWidgetType == "bargraph"))
				setValue("lineL", "position", iPosX_L, iPosY_L);
			}
		else
			{
			g_bViewAlert_L = false;
			}
		if ((fLL >= fMin) && (fLL <= fMax) && 
				((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_LL_L)
					|| (g_iAlertRange == UN_AIAO_ALARM_4_H_L_LL)))
			{
			g_bViewAlert_LL = true;
			if((g_sWidgetType == "bargraph"))
			getValue("lineLL", "position", iPosX_LL, iPosY_LL);

			if(fMax_fMin != 0)
				fScale = (fLL - fMin) / fMax_fMin;	
			iPosY_LL = (int)(fScale * (iPosY_max - iPosY_min) + iPosY_min);
			if(g_bSystemConnected && (g_sWidgetType == "bargraph"))
			setValue("lineLL", "position", iPosX_LL, iPosY_LL);
			}
		else
			{
			g_bViewAlert_LL = false;
			}
		}
	unGenericWidget_bargraph_viewRange(g_bViewValues);
	unGenericWidget_bargraph_viewAlarm(g_bViewAlerts&&g_bBoolAlert);
	
	if (bInvalid || (!bPlcEnabled) || (iAlValue > c_unSystemIntegrity_no_alarm_value))
		levelBargraph.backCol = "unDataNotValid";
	else
		levelBargraph.backCol = g_sColor;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericWidget_bargraph_viewRange
/**
Purpose: show/hide the range and value lines of the bargraph

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericWidget_bargraph_viewRange(bool bView)
{
	if(g_sWidgetType == "bargraph") {
		lineMax.visible = bView;
		lineMin.visible = bView;
		rangeMax.visible = bView;
		rangeMin.visible = bView;
		linePosSt.visible = bView;
		posSt.visible = bView;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGenericWidget_bargraph_viewAlarm
/**
Purpose: show/hide the alarm lines of the bargraph

Parameters:
	- sWidgetType, string, input, widget type. ex: "heater"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericWidget_bargraph_viewAlarm(bool bView)
{
	if(g_sWidgetType == "bargraph") {
		lineHH.visible = g_bBoolAlert && bView && ((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_HH_H)
												|| (g_iAlertRange == UN_AIAO_ALARM_4_HH_H_L))
							&& g_bViewAlert_HH;
		lineH.visible = g_bBoolAlert && bView && ((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_HH_H) 
												|| (g_iAlertRange == UN_AIAO_ALARM_3_H_L) || (g_iAlertRange == UN_AIAO_ALARM_2_H)
												|| (g_iAlertRange == UN_AIAO_ALARM_4_HH_H_L) || (g_iAlertRange == UN_AIAO_ALARM_4_H_L_LL)) 
							&& g_bViewAlert_H;
		lineL.visible = g_bBoolAlert && bView && ((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_LL_L) 
												|| (g_iAlertRange == UN_AIAO_ALARM_3_H_L) || (g_iAlertRange == UN_AIAO_ALARM_2_L)
												|| (g_iAlertRange == UN_AIAO_ALARM_4_HH_H_L) || (g_iAlertRange == UN_AIAO_ALARM_4_H_L_LL)) 
							&& g_bViewAlert_L;
		lineLL.visible = g_bBoolAlert && bView && ((g_iAlertRange == UN_AIAO_ALARM_5_HH_H_L_LL) || (g_iAlertRange == UN_AIAO_ALARM_3_LL_L)
												|| (g_iAlertRange == UN_AIAO_ALARM_4_H_L_LL)) 
							&& g_bViewAlert_LL;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

//@}

