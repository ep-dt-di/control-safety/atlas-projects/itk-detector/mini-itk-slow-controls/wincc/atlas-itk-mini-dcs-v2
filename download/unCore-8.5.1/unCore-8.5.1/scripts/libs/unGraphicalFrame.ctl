#uses "fwGeneral/fwManager.ctl"
#uses "unImportTree.ctl"
#uses "unAlarmScreen.ctl"
#uses "fwAccessControl/fwAccessControl.ctc"
#uses "libunCore/unApplicationFilter.ctl"
#uses "unicos_declarations.ctl"
#uses "unGraphicalFrameDeprecated.ctl"

private const bool unGraphicalFrame_unHMILibLoaded = fwGeneral_loadCtrlLib("unHMI/unHMI.ctl",false);

/**@name LIBRARY: unGraphicalFrame.ctl

@author: Herve Milcent and Daniel Davids (LHC-IAS)

Creation Date: 19/04/2002

Modification History:
  19/02/2015: Piotr Golonka
  - IS-1624 bug unGraphicalFrame_isCtrlScriptRunning

  15/10/2013: Marco Boccioli
  - IS-1248 Remove sending of mail to Factorysystem.Support@cern.ch from function unGraphicalFrame_checkInstallation()

  12/06/2012: Marco Boccioli on behalf of Lyuba Petrova
  -  IS-779 unCore - unGraphicalFrame Cannot close the UI after opening Project Info panel (from configuration mode)

  28/03/2012: Herve
  - IS-674  unCore - unGraphicalFrame  Internal DP for alarmList in faceplate created at startup of unicosHMI

  07/02/2012: Herve
  - IS-709  unCore - unGraphicalFrame  GraphicalFrame fix: initialization of the variable even if the xml file does not exists

  12/10/2011: Herve
  - IS-636  unCore - unGraphicalFrame  unGraphicalFrame_isCtrlScriptRunning: do request the list of running script if the DEBUG dp does not exist.

  03/10/2011: Herve
  - IS-627: unicosHMI multiple screen error: only the first screen is initialised

  28/06/2011: Herve
    - IS-568: unicosHMI startup: UI cannot start if the fwInstallation version is < 5.0.3

  07/04/2011: Danny
    - IS-514: Modification to automatically convert the XML File Format

  21/03/2011: Herve
    - IS-485: From the menu and file access configuration call if it configured the default for each component

  25/11/2010: Herve
    - IS-459: disable the alarm and value archive from the menu if RDB is used

  01/11/2010: Danny
    - IS-297: Modifications to use PVSS' XML libraries

  20/10/2010: Herve
    - IS-412: new function unGraphicalFrame_isCtrlScriptRunning to check if ctrl script(s) are started

  14/10/2010: Herve:
    - IS-413: at startup of unicosHMI: check if all the component were sucessfully installed
    (installationOK DPE: feature with fwInstallation version >= 4.2.6)

  29/09/2009: Frederic
    - add vision/systemIntegrity/unBackup.pnl in unGraphicalFrame_getUNICOSDefaultMenuConfiguration
    and unGraphicalFrame_getUNICOSDefaultFileAccessControl

  10/02/2009: Herve
    - add vision/unicosObject/General/unSetDeviceDPEConfigs.pnl in unGraphicalFrame_getUNICOSDefaultMenuConfiguration
    and unGraphicalFrame_getUNICOSDefaultFileAccessControl

  29/01/2009: Herve
    - synchronise button+cascademenu+icon: do not allow button/menu/cascade click during update of access control

  05/12/2008: Herve
    - autologout: use list of group instead of list of user.
    modified function: unGraphicalFrame_InactivityCB

  01/12/2008: Herve
    - unGraphicalFrame_getUNICOSDefaultFileAccessControl: fwTrending/fwTrendingPlotConfPanel.pnl and fwTrending/fwTrendingPlotsPage.pnl
    move to expert level

  28/10/2008: Herve
    - unGraphicalFrame_setPopupMenuText: bug fix check if shape exists before setting shape value

  30/09/2008: Herve
    - unGraphicalFrame_showContextualPanel: check if contextual area exists
    - set of function to build cascade menu and button popup menu
    - set of functions for HMI.

  24/09/2008: Herve
    - proto to dynamically build the menu.
      modified function: unGraphicalFrame_buildMenu, unGraphicalFrame_deleteMenu,
        unGraphicalFrame_menuConfigurationCB, unGraphicalFrame_menuUserCB
      new function: unGraphicalFrame_resizeMenuButton
      global mapping g_m_sMenuConfigurationFile
      *** TO DO: keep current shape and rmv even removed menu.

  30/06/2008: Herve
    - bug childPanel:
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel
      . new functions: unGraphicalFrame_isPanelOpen, _unGraphicalFrame_getModuleId,
        unGraphicalFrame_ChildPanelOnCentral, unGraphicalFrame_ChildPanelOnCentralModal
        unGraphicalFrame_ChildPanelOn, unGraphicalFrame_ChildPanelOnCentralModalReturn,
        unGraphicalFrame_ChildPanelOnCentralReturn, unGraphicalFrame_ChildPanelOnModal,
        unGraphicalFrame_ChildPanelOnReturn, unGraphicalFrame_PanelOffPanel,
        unGraphicalFrame_ChildPanelOnModalReturn

  27/06/2008: Herve
    - unModifyConfig.pnl: put in expert

  09/06/2008: Herve
    - _unGraphicalFrame_saveUserPanel, unGraphicalFrame_showContextualPanel
      case internal DP --> error in MesageText

  30/05/2008: Herve
    - vision/systemIntegrity/unConfigMail.pnl added in default menu
    - vision/systemIntegrity/unConfigMail.pnl for expert.
    - vision/unDiagnostics/unArchiveStatistics.pnl for expert.

  23/04/2008: Herve
    - add Debug utility in the Menu configuration

  15/04/2008: Herve
    - new function check if libs loaded: UNICOS

  20/03/2008: Herve
    - faceplate trend config panel added in unGraphicalFrame_getUNICOSDefaultFileAccessControl
    and unGraphicalFrame_getUNICOSDefaultMenuConfiguration

  19/02/3008: Herve
    - new panel for list of masked event & alert

  14/02/2008: Herve
    - in unGraphicalFrame_menuUserCB: calculate the enalbe state of all the items and then set them once

  04/02/2008: Herve
    - add plot/page file access

  01/2008: Herve
    - menu and file access function

        13/11/2007: Herve
          - bug in addImageClass

	25/05/2005: Herve
		- unGraphicalFrame_showUserPanel: wrong moduleName in case of base panel for the global variable used in printing

	18/02/2005: Herve.
		- wrong call of unGraphicalFrame_showContextualPanel
		- unGraphicalFrame_showContextualPanel: add alos _FwTreeNode type dp to the last opened panel.

	10/11/2004: Herve
		- in function unGraphicalFrame_getPanel: old JCOP fwGenericDpFunctions_getPanelList function called, use instead unPanel_convertDpPanelNameToPanelFileName

	21/10/2004: Herve
		- add definition for background printing

	20/09/2004: Herve
		add function for the unicosSystemDP panel
		remove unGraphicalFrame_setHerarchyConfig and unGraphicalFrame_getHerarchyConfig

	23/07/2004: Herve, update to 3.0
		- function unGraphicalFrame_getHierarchyConfig modified

	24/02/2004: Herve
		in function unGraphicalFrame_setHerarchyConfig do not allow to have the same config.
	22/02/2004: Herve
		add: accept * in unGraphicalFrame_getTreeConfig
	20/02/2004: Herve
		add get the Tree config, new functions: unGraphicalFrame_getTreeConfig, unGraphicalFrame_setHerarchyConfig
	07-06-2002:
		add function for the last opened panel
		add comments.
	30-06-2002:
		add domainList in unGraphicalFrame_showContextualPanel call.
	21-08-2002:
		remove domainList in unGraphicalFrame_showContextualPanel call.
	13-02-2003:
		in showUserPanel the panelName is dpName+fileName.

version 1.0

External Functions:
	. unGraphicalFrame_showContextualPanel: show the contextual panel
	. unGraphicalFrame_showUserPanel: show the user defined panel
	. unGraphicalFrame_restoreLastPanelDpContextualPanel: to restore the contextual panel of the last panel DP opened
	. unGraphicalFrame_select: select or deselect a device
	. unGraphicalFrame_setSelectMessage: set the message without selecting it to show the message in the selection area.
	. unGraphicalFrame_getPanel: get the last panel dp and file name opened for this module
	. unGraphicalFrame_getHerarchyConfig: get the Herarchy configuration to display
	. unGraphicalFrame_setHerarchyConfig: set the Herarchy configuration

Internal Functions:
	. _unGraphicalFrame_addToLastPanel: to set the last panel opened for this module
	. _unGraphicalFrame_getAndResetLastPanel: to get the last panel opened for this module

Purpose:
This library contain generic functions to be use with the graphical frame component.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. global variable: the following variables are global to the manager:
		. g_unGraphicalFrame_lastPanelDpOpened: dyn_string, this is a list of last data point of data point type
		_UnPanel representing a panel that was opened via the GraphicalFrame component in a module listed in the
		variable g_unGraphicalFrame_openedModule. There is one entry per module opened. The same entry index is
		used in the g_unGraphicalFrame_openedModule global variable to get the module name.
		. g_unGraphicalFrame_openedModule: dyn_string, this is a list of opened module of this manager.
	. the modules with the following names will be opened if there are not already opened:
		. myModuleName()+";unicosGFContextPanel"
		. myModuleName()+";unicosGFPanel"
	. the following panels must exist:
		. vision/graphicalFrame/contextualButton.pnl: contextual buttons for the _UnPanel data point type
		. vision/graphicalFrame/userPanel.pnl: panel used by the GraphicalFrame component to show the panel selected
		by the operator from the windowTree.
	. PVSS version: 3.0
	. operating system: WXP.
	. distributed system: yes.
*/

//@{

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showContextualPanel
/**
Purpose:
to show the contextual button panel: for panels or dps.

	@param sPanelFileName: string, input, the panel file name to show
	@param sDpName: string, input, the dpname
        @param sNewModuleName: string, input, the module name, default = ""

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the module with the following name will be opened if it is not already opened:
		. myModuleName()+";unicosGFContextPanel"
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showContextualPanel(string sPanelFileName, string sDpName, string sNewModuleName="")
{
  dyn_string split, exceptionInfo;
  string moduleName, sDpType = dpTypeName(sDpName), sTrigger;
  int moduleNumber, splitterPos, res;

  if(sPanelFileName == "")
    return;
  if (sNewModuleName == "") {
	if (globalExists("g_unHMI_RunsInNewHMI")){
		split = strsplit(myModuleName(),";");
		splitterPos = strpos(split[1], UNHMI_SPLITTER_FLAG + "_frame");
		if (splitterPos > 0) {
			strreplace(split[1], UNHMI_SPLITTER_FLAG + "_frame", "");
		}
		moduleNumber = substr(split[1], strlen(split[1])-1, 1);
		split[1] = UNHMI_COMMAND_MODULE_NAME + moduleNumber;
	}
	else {
		split = strsplit(myModuleName(),";");
	}
  }
  else {
    split = strsplit(sNewModuleName,";");
  }
  moduleName = split[1]+";unicosGFContextPanel";
  sTrigger = _unGraphicalFrame_getInternalTriggerName(moduleName);
  if(dpExists(sTrigger)) {
    if(isModuleOpen(moduleName))
    {
      if((sDpType == "_UnPanel") || (sDpType == unTreeWidget_treeNodeDPT))
        res = _unGraphicalFrame_addToLastPanel(moduleName, sDpName);
    /*
    	dpSet("_Ui_"+myManNum()+".RootPanelOrigOn.ModuleName", moduleName,
    			"_Ui_"+myManNum()+".RootPanelOrigOn.FileName", sPanelFileName,
    			"_Ui_"+myManNum()+".RootPanelOrigOn.PanelName", "contextualButton.pnl;"+sDpName,
    			"_Ui_"+myManNum()+".RootPanelOrigOn.Parameter", makeDynString("$sDpName:"+sDpName));
    */
      RootPanelOnModule(sPanelFileName, "contextualButton.pnl;"+sDpName, moduleName, makeDynString("$sDpName:"+sDpName));
    }
  }
  else {
    fwException_raise(exceptionInfo, "ERROR", sDpName+", show contextual button: internal DP does not exist "+sTrigger, "");
    unMessageText_sendException(getSystemName(), myManNum(), "contextualButton", "user", "*", exceptionInfo);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showUserPanel
/**
Purpose:
to show the contextual button panel: for panels or dps.

	@param sPanelFileName: string, input, the panel file name to show
	@param sPanelDollarParmeters: string, input, the $param of the panel separated by ;
	@param sDpName: string, input, the dpname
        @param sNewModuleName: string, input, the module name, default = ""

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the module with the following name will be opened if it is not already opened:
		. myModuleName()+";unicosGFPanel"
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showUserPanel(string sPanelFileName, string sPanelDollarParmeters, string sDpName, string sNewModuleName = "")
{

  if (globalExists("g_unHMI_RunsInNewHMI"))
  {
    dyn_string split;
    string moduleName, sDollarParmeters;
    int moduleNumber;
    if (strpos(sPanelDollarParmeters,"number")>=0)
    {
      dyn_string temp = strsplit(sPanelDollarParmeters, ":;");

      if (dynContains(temp,"$number"))
      {
        moduleNumber = temp[dynContains(temp,"$number") + 1];
      } else
      {
        //ERROR
        DebugFTN("unHMI", "ERROR: $number undefined");
      }

      invokeMethod(UNHMI_MAIN_MODULE_NAME + ".mainWindow:", "unHMI_setCommandModuleId", moduleNumber);
      //Extra handling in case we are in Splitter Mode
      if (strpos(sPanelDollarParmeters, UNHMI_SPLITTER_FLAG + ":true") >=0){
        invokeMethod(UNHMI_MAIN_MODULE_NAME + ".mainWindow:", "unHMI_setCommandModuleId", moduleNumber + UNHMI_SPLITTER_FLAG);
      }
    }
  }
  if(sPanelFileName == "")
    return;
  _unGraphicalFrame_saveUserPanel(sPanelFileName, sPanelDollarParmeters, sDpName, sNewModuleName);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_showUserPanel
/**
Purpose:
to show the contextual button panel: for panels or dps.

	@param sPanelFileName: string, input, the panel file name to show
	@param sPanelDollarParmeters: string, input, the $param of the panel separated by ;
	@param sDpName: string, input, the dpname
        @param sNewModuleName: string, input, the module name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the module with the following name will be opened if it is not already opened:
		. myModuleName()+";unicosGFPanel"
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
_unGraphicalFrame_showUserPanel(string sPanelFileName, string sPanelDollarParmeters, string sDpName, string sNewModuleName)
{
  dyn_string split, dsDollar, exceptionInfo;
  string moduleName, sDollarParmeters;

  if(sPanelFileName == "")
    return;
  sDollarParmeters = sPanelDollarParmeters;
  if((sNewModuleName == "") || (sNewModuleName == myModuleName()) || (sNewModuleName == (myModuleName()+UN_UNICOSHMI_SYNOPTIC_MODULENAME))) {
    split = strsplit(myModuleName(),";");

	unSelectDeselectHMI_deselectOldDp_select("", exceptionInfo, true);
    if(dynlen(exceptionInfo)>0) {
      unMessageText_sendException(getSystemName(), myManNum(), "userPanel.pnl terminate", "user", "*", exceptionInfo);
    }

    // In case of NEW HMI moduleName needs extra care in case of splitter mode
    //TODO look into if split[1] check is needed
	if (globalExists("g_unHMI_RunsInNewHMI")) { // && (split[1] == sNewModuleName))
      string commandModuleID;
      commandModuleID  = invokeMethod(UNHMI_MAIN_MODULE_NAME + ".mainWindow:", "unHMI_getCommandModuleId");

      string moduleNumber = substr(split[1], strlen(split[1])-1, 1);
      if (strlen(commandModuleID) > 1) {
        moduleNumber = moduleNumber + UNHMI_SPLITTER_FLAG;
      }
	  moduleName = UNHMI_ZOOMED_MODULE_PREFIX + moduleNumber;

	  // handle here any error while if the sendmessage failed
      if(dynlen(exceptionInfo)>0)
		fwExceptionHandling_display(exceptionInfo);
	  if(isModuleOpen(moduleName)) {
		unGraphicalFrame_closeAllChildPanel();
		gOpenedPanel[UNHMI_MAIN_MODULE_NAME+UN_UNICOSHMI_SYNOPTIC_MODULENAME] = sDpName+";"+sPanelFileName+";"+sDollarParmeters;
		dsDollar = strsplit(sPanelDollarParmeters, ";");

		int moduleNumber = 0;
		string tempModuleName = moduleName;

		//case SPLIT mode
		strreplace(tempModuleName, UNHMI_SPLITTER_FLAG, "");
		moduleNumber = substr(tempModuleName, strlen(tempModuleName) -1 , 1);
		//Opening the Panel means doing dpSet to respective datapoint for navigation Bar
		//Always make sure it is a preview module with the moduleNumber lower/upper limit
		if (moduleNumber > 0 && moduleNumber <= UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES) {
			DebugFTN("unHMI", __FUNCTION__, __FILE__, navigationBarDp[moduleNumber], moduleName, sPanelFileName, sDpName);
			dpSet("_Ui_"+myManNum()+ navigationBarDp[moduleNumber] + ".ModuleName", moduleName,
				"_Ui_"+myManNum()+ navigationBarDp[moduleNumber] + ".FileName", sPanelFileName,
				"_Ui_"+myManNum()+ navigationBarDp[moduleNumber] + ".PanelName", sDpName,
				"_Ui_"+myManNum()+ navigationBarDp[moduleNumber] + ".Parameter", sDollarParmeters);//makeDynString("$sDpName:"+sDpName));
			invokeMethod(UNHMI_PREVIEW_MODULE_DOCK_NAMES[moduleNumber] + "." + UNHMI_PREVIEW_PANEL_NAME + ":", "unHMI_setZoomedFileName", sPanelFileName, sDpName);
			gPanelNameOnModule[moduleNumber] = sDpName+";"+sPanelFileName+";"+sDollarParmeters;
		} else {
			DebugFTN("unHMI", "Error Should not reach here", moduleName);
		}
	  }
	} else {
		moduleName = split[1]+";unicosGFPanel";
		// handle here any error while if the sendmessage failed
		if(dynlen(exceptionInfo)>0)
			fwExceptionHandling_display(exceptionInfo);
		if(isModuleOpen(moduleName)) {
			unGraphicalFrame_closeAllChildPanel();
			gOpenedPanel[split[1]+UN_UNICOSHMI_SYNOPTIC_MODULENAME] = sDpName+";"+sPanelFileName+";"+sDollarParmeters;
			dsDollar = strsplit(sPanelDollarParmeters, ";");
		}
	}
	RootPanelOnModule(sPanelFileName, sDpName+";"+sPanelFileName+";"+sDollarParmeters, moduleName, dsDollar);
  }
  else {
    split = strsplit(sNewModuleName,";");
    moduleName = split[1];
    if(!isModuleOpen(moduleName))
      ModuleOn(moduleName, 0, 0, 0, 0, 0, 0, "");
    if(isModuleOpen(moduleName)) {
      unGraphicalFrame_closeAllChildPanel();
    }
    dsDollar = strsplit(sPanelDollarParmeters, ";");
    RootPanelOnModule(sPanelFileName, sPanelFileName, moduleName, dsDollar);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getInternalTriggerName
/**
Purpose:
get the internal trigger for the graphical frame.

	@param return value: string, output, the DP trigger name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
string unGraphicalFrame_getInternalTriggerName()
{
  dyn_string dsSplit=strsplit(myModuleName(), ";");

  if(dynlen(dsSplit)<1)
    dsSplit[1] = "";
  return _unGraphicalFrame_getInternalTriggerName(dsSplit[1]);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_getInternalTriggerName
/**
Purpose:
get the internal trigger for the graphical frame, based on module name.

	@param return value: string, output, the DP trigger name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
string _unGraphicalFrame_getInternalTriggerName(string sModule)
{
  string sTriggerName;
  dyn_string dsSplit=strsplit(sModule, ";");

  if(dynlen(dsSplit)<1)
    dsSplit[1] = "";

  sTriggerName = GRAPHICALFRAME_DPPREFIX+dsSplit[1]+"_"+myManNum();
  return sTriggerName;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_saveUserPanel
/**
Purpose:
set the panel to show.

	@param sPanelFileName: string, input, the panel file name to show
	@param sPanelDollarParmeters: string, input, the $param of the panel separated by ;
	@param sDpName: string, input, the dpname
        @param sNewModuleName: string, input, the module name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
_unGraphicalFrame_saveUserPanel(string sPanelFileName, string sPanelDollarParmeters, string sDpName, string sNewModuleName)
{
  string sMod = sNewModuleName, sTrigger;
  dyn_string split, exceptionInfo;
  int moduleNumber, splitterPos;

  if (sMod == "") {
	if (globalExists("g_unHMI_RunsInNewHMI")){
		split = strsplit(myModuleName(),";");
		splitterPos = strpos(split[1], UNHMI_SPLITTER_FLAG + "_frame");
		if (splitterPos > 0) {
			strreplace(split[1], UNHMI_SPLITTER_FLAG + "_frame", "");
		}
		moduleNumber = substr(split[1], strlen(split[1])-1, 1);
		sMod = UNHMI_COMMAND_MODULE_NAME + moduleNumber;
	}
	else {
		sMod = myModuleName();
	}
  }


  sTrigger = _unGraphicalFrame_getInternalTriggerName(sMod);
  if(dpExists(sTrigger))
    dpSet(sTrigger+".userPanelToShow", sPanelFileName+"|"+sPanelDollarParmeters+"|"+sDpName+"|"+sNewModuleName+"|");
  else {
    // use myModuleName()
    sMod = myModuleName();
    sTrigger = _unGraphicalFrame_getInternalTriggerName(sMod);
    if(dpExists(sTrigger))
      dpSet(sTrigger+".userPanelToShow", sPanelFileName+"|"+sPanelDollarParmeters+"|"+sDpName+"|"+sNewModuleName+"|");
    else {// still error
      fwException_raise(exceptionInfo, "ERROR", sDpName+", show user panel: internal DP does not exist "+sTrigger, "");
      unMessageText_sendException(getSystemName(), myManNum(), "userpanel", "user", "*", exceptionInfo);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_initUserPanel
/**
Purpose:
initialise the graphical frame show panel utility.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_initUserPanel()
{
  dpConnect("_unGraphicalFrame_showUserPanelCB", false, unGraphicalFrame_getInternalTriggerName()+".userPanelToShow");
  dpConnect("_unGraphicalFrame_childOn", false, myUiDpName()+".ChildPanelOn.ModuleName", myUiDpName()+".ChildPanelOn.PanelName");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_setCorporateColors
/**
Purpose:
Set the application's colors for the shown panel

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
	. UI elements: backgroundRectangle, title
*/
unGraphicalFrame_setCorporateColors()
{
  string sColor;

  if(dpExists(UN_APPLICATION_DPNAME))
  {
	dpGet(UN_APPLICATION_DPNAME + ".corporateStyle.backgroundColor", sColor);
	if(shapeExists("backgroundRectangle"))
	{
		backgroundRectangle.backCol = sColor;
	}
	// special elements
	if(shapeExists("deviceTreeArrow"))
	{
		deviceTreeArrow.backCol = sColor;
	}
	if(shapeExists("deviceTreeArrow2"))
	{
		deviceTreeArrow2.backCol = sColor;
	}
	if(shapeExists("bgElementSeparator"))
	{
		bgElementSeparator.backCol = sColor;
	}

	dpGet(UN_APPLICATION_DPNAME + ".corporateStyle.foregroundColor", sColor);
	if(shapeExists("title"))
	{
		title.foreCol = sColor;
	}
	// special elements
	if(shapeExists("breadcrumbTrail"))
	{
		breadcrumbTrail.foreCol = sColor;
	}
	if(shapeExists("systemsLabel"))
	{
		systemsLabel.foreCol = sColor;
	}
	if(shapeExists("sortByText"))
	{
		sortByText.foreCol = sColor;
	}
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_showUserPanelCB
/**
Purpose:
Call back on the panel to show.

	@param sDp: string, input, Dp name
	@param sUserPanelToShow: string, input, the panel to show

  @reviewed 2018-09-05 @whitelisted{Callback}

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
_unGraphicalFrame_showUserPanelCB(string sDp, string sUserPanelToShow)
{
  string sPanelFileName, sPanelDollarParmeters, sDpName, sNewModuleName;
  if (globalExists("g_unHMI_RunsInNewHMI")) {
    bool isSplitterVisible = invokeMethod(UNHMI_MAIN_MODULE_NAME + ".mainWindow:", "unHMI_getVisible", UNHMI_SPLITTER_MODULE_NUMBER);
    _unGraphicalFrame_getUserPanel(sUserPanelToShow, sPanelFileName, sPanelDollarParmeters, sDpName, sNewModuleName);
    _unGraphicalFrame_showUserPanel(sPanelFileName, sPanelDollarParmeters, sDpName, sNewModuleName);

    //Process the view port's zoom and position for unHMI
    dyn_string split = strsplit(myModuleName(),";");
    string moduleNumber = substr(split[1], strlen(split[1])-1, 1);
	if(isSplitterVisible) {
      invokeMethod(UNHMI_ZOOMED_MODULE_PREFIX + moduleNumber + UNHMI_SPLITTER_FLAG + "_frame" + "." + splitterPanelName + moduleNumber + ":", "unHMI_loadPanelZoom");
    } else {
      invokeMethod(UNHMI_MAIN_VIEW_EMBEDDED_MODULES[moduleNumber] + "." + EMBEDDED_PANEL_NAME + ":", "unHMI_loadPanelZoom");
    }
  } else {
    _unGraphicalFrame_getUserPanel(sUserPanelToShow, sPanelFileName, sPanelDollarParmeters, sDpName, sNewModuleName);
    _unGraphicalFrame_showUserPanel(sPanelFileName, sPanelDollarParmeters, sDpName, sNewModuleName);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_getUserPanel
/**
Purpose:
Extract the panel file name, dollar parameter, dpName and module name.

        @param s: string, inputm the user panel to show separated by |
	@param sPanelFileName: string, output, the panel file name to show
	@param sPanelDollarParmeters: string, output, the $param of the panel separated by ;
	@param sDpName: string, output, the dpname
        @param sNewModuleName: string, output, the module name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
_unGraphicalFrame_getUserPanel(string s, string &sPanelFileName, string &sPanelDollarParmeters, string &sDpName, string &sNewModuleName)
{
  dyn_string dsSplit = strsplit(myModuleName(),";");
  string sModule;

    dsSplit = strsplit(s, "|");
    if(dynlen(dsSplit) < 4) {
      sPanelFileName = "";
      sPanelDollarParmeters = "";
      sDpName = "";
      sNewModuleName = "";
    }
    else {
      sPanelFileName = dsSplit[1];
      sPanelDollarParmeters = dsSplit[2];
      sDpName = dsSplit[3];
      sNewModuleName = dsSplit[4];
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_childOn
/**
Purpose:
Callback on the child on call.

	@param sDp: string, input, the dp name
	@param sModuleName: string, input, the module name
	@param sDp2: string, input, the dpname
	@param sPanelName: string, input, the panel name

  @reviewed 2018-09-05 @whitelisted{Callback}

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
_unGraphicalFrame_childOn(string sDp, string sModuleName, string sDp2, string sPanelName)
{
  dyn_string dsChildOn, dsSplit, dsModule, dsSplit2;
  bool bAppend;
  int iPos;

  if(strpos(sModuleName, UN_UNICOSHMI_SYNOPTIC_MODULENAME) > 0) {
    dsSplit=strsplit(myModuleName(), ";");
    if(dynlen(dsSplit)<1)
      dsSplit[1] = myModuleName();
    dsSplit2 = strsplit(sModuleName, ";");
    if(dynlen(dsSplit2)<1)
      dsSplit2[1] = sModuleName;
    if(dsSplit[1] == dsSplit2[1]) {
      synchronized(g_m_dsChildOn) {
        if(mappingHasKey(g_m_dsChildOn, dsSplit[1])) {
          dsChildOn = g_m_dsChildOn[dsSplit[1]];
          dsModule = g_m_dsChildModule[dsSplit[1]];
        }
        iPos = dynContains(dsChildOn, sPanelName);
        if(iPos <= 0){
          bAppend = true;
        }
        else if (dsModule[iPos] != sModuleName) {
          bAppend = true;
        }
        if(bAppend) {
          dynAppend(dsChildOn, sPanelName);
          dynAppend(dsModule, sModuleName);
          g_m_dsChildModule[dsSplit[1]] = dsModule;
          g_m_dsChildOn[dsSplit[1]] = dsChildOn;
        }
      }
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_closeAllChildPanel
/**
Purpose:
Close all child of the current module.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_closeAllChildPanel()
{
  int iPanel, lenPanel;
  string sKey;
  dyn_string dsChild, dsSplit, dsModule;

  dsSplit=strsplit(myModuleName(), ";");
  if(dynlen(dsSplit)<1)
    dsSplit[1] = myModuleName();
  sKey = dsSplit[1];
  synchronized(g_m_dsChildOn) {
    if(mappingHasKey(g_m_dsChildOn, sKey)) {
      dsChild = g_m_dsChildOn[sKey];
      dsModule = g_m_dsChildModule[sKey];
      lenPanel = dynlen(dsChild);
      for(iPanel=lenPanel;iPanel>=1;iPanel--) {
        if(isPanelOpen(dsChild[iPanel], dsModule[iPanel])) {
          PanelOffModule(dsChild[iPanel], dsModule[iPanel]);
          delay(0, 10);
        }
      }
      dynClear(dsChild);
      g_m_dsChildOn[sKey] = dsChild;
      g_m_dsChildModule[sKey] = dsChild;
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_restoreLastPanelDpContextualPanel
/**
Purpose:
to restore the contextual panel of the last panel DP type that was opened. This function is called when a deselect is
done on a DP or if a close faceplate is done.

	@param sDpName: the dpname

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the following panels must exist:
		. vision/graphicalFrame/contextualButton.pnl: contextual buttons for the _UnPanel data point type
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_restoreLastPanelDpContextualPanel(string sDpName)
{
	string dpPanel;
	int moduleNumber, splitterPos, res;
	dyn_string split;
	string moduleName;

  if(globalExists("g_unHMI_RunsInNewHMI")) {
  	split = strsplit(myModuleName(),";");
  	splitterPos = strpos(split[1], UNHMI_SPLITTER_FLAG);
  	if (splitterPos > 0) {
    		strreplace(split[1], UNHMI_SPLITTER_FLAG, "");
	}
    moduleNumber = substr(split[1], strlen(split[1])-1, 1);
    moduleName = UNHMI_COMMAND_MODULE_NAME + moduleNumber;
  } else {
	  moduleName = myModuleName();
  }
  res = _unGraphicalFrame_getAndResetLastPanel(moduleName, dpPanel);
// if there was no opened panel then remove the contextualButton panel
  if(dpPanel != "") {
		unGraphicalFrame_showContextualPanel("vision/graphicalFrame/contextualButton.pnl", dpPanel, moduleName);
  }
	else {
		split = strsplit(moduleName,";");
		moduleName = split[1]+";unicosGFContextPanel";

		PanelOffModule("contextualButton.pnl;"+sDpName, moduleName);
	}
}



//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_setSelectMessage
/**
Purpose:
	set the message without selecting it to show the message in the selection area.

	@param sDpElementName: string, input, the dpElement to select

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. data point type needed: _UnApplication
	. data point: the following data point is needed, it must exist, no checking is done
		. _unApplication
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_setSelectMessage(string sDpElementName)
{
	dpSet(c_applicationDPE, UN_MY_MANNUM+myManNum()+"/"+myModuleName()+"/"+sDpElementName);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getPanel
/**
Purpose:
indivisible function to get the last panel dp and file name opened for this module.

	@param sModuleName: intput, the module name
	@param sDpName: output, the dpname
	@param sPanelName: output, the panel name

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. global variable:
		. g_unGraphicalFrame_lastPanelDpOpened: dyn_string, this is a list of last data point of data point type
		_UnPanel representing a panel that was opened via the GraphicalFrame component in a module listed in the
		variable g_unGraphicalFrame_openedModule. There is one entry per module opened. The same entry index is
		used in the g_unGraphicalFrame_openedModule global variable to get the module name.
		. g_unGraphicalFrame_openedModule: dyn_string, this is a list of opened module of this manager.
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int unGraphicalFrame_getPanel(string sModuleName, string &sDpName, string &sPanelName)
{
	int pos;
	dyn_string split, exceptionInfo, panelDollarParam;

	split = strsplit(sModuleName,";");

	pos = dynContains(g_unGraphicalFrame_openedModule, split[1]);
	if(pos > 0) {
		sDpName = g_unGraphicalFrame_lastPanelDpOpened[pos];
		sPanelName = unPanel_convertDpPanelNameToPanelFileName(sDpName);
	}
	else {
		sPanelName = "";
		sDpName = "";
	}
	return 0;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_addToLastPanel
/**
Purpose:
indivisible function to set the last panel opened for this module.

	@param sModuleName: intput, the module name
	@param sDpName: input, the dpname

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int _unGraphicalFrame_addToLastPanel(string sModuleName, string sDpName)
{
	int pos;
	dyn_string split;

	split = strsplit(sModuleName,";");

	pos = dynContains(g_unGraphicalFrame_openedModule, split[1]);
	if(pos > 0)
		g_unGraphicalFrame_lastPanelDpOpened[pos] = sDpName;
	else {
		dynAppend(g_unGraphicalFrame_openedModule, split[1]);
		dynAppend(g_unGraphicalFrame_lastPanelDpOpened, sDpName);
	}
	return 0;
}


//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_getAndResetLastPanel
/**
Purpose:
indivisible function to get the last panel opened for this module.

	@param sModuleName: intput, the module name
	@param sDpName: output, the dpname

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. global variable:
		. g_unGraphicalFrame_lastPanelDpOpened: dyn_string, this is a list of last data point of data point type
		_UnPanel representing a panel that was opened via the GraphicalFrame component in a module listed in the
		variable g_unGraphicalFrame_openedModule. There is one entry per module opened. The same entry index is
		used in the g_unGraphicalFrame_openedModule global variable to get the module name.
		. g_unGraphicalFrame_openedModule: dyn_string, this is a list of opened module of this manager.
	. PVSS version: 3.0
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int _unGraphicalFrame_getAndResetLastPanel(string sModuleName, string &sDpName)
{
	int pos;
	dyn_string split;

	split = strsplit(sModuleName,";");

	pos = dynContains(g_unGraphicalFrame_openedModule, split[1]);
	if(pos > 0) {
		sDpName = g_unGraphicalFrame_lastPanelDpOpened[pos];
		dynRemove(g_unGraphicalFrame_openedModule, pos);
		dynRemove(g_unGraphicalFrame_lastPanelDpOpened, pos);
	}
	return 0;
}


//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_initialize
/**
Purpose:
Executes the initialization of the unTreeWidget used to view the hierarchy. This function is executed at startup of the unTreeWidget
a node shown in the unTreeWidget.
The unTreeWidget tree is disabled during the operation. This function is started in a separate thread.

	@param sGraphTree: string, input: the name of the unTreeWidget graphical element
	@param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
	@param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_initialize(string sGraphTree, string sBar, bool bClipboard)
{
	dyn_string dsTree = fwTreeDisplay_getTrees();
	dyn_string dsNode, exceptionInfo;
	dyn_string dsResult;
	int i, len, id;
	string sKey, imageClassParameters;
	shape aShape=getShape(sGraphTree);
	int iCurrentMousePointer, iMousePointer;

	id = unProgressBar_start(sBar);
	unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
	iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
	iMousePointer = unTreeWidget_ccHourglass;
	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
// do not allow the renaming of top node
	unTreeWidget_setPropertyValue(aShape, "set_bRenameRootNodeAllowed", false, true);

// get the icon list of the root folder device and load it in the unTreeWidget if there is no error
	imageClassParameters = getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenClose.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenOpen.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenPath.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithoutChildrenNotSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithoutChildrenSelected.bmp");

	unTreeWidget_setPropertyValue(aShape, "addImageClass",UN_WINDOWTREE_ROOT_FOLDER+";" + imageClassParameters, true);
	unTreeWidget_setPropertyValue(aShape, "addImageClass",UN_TRENDTREE_ROOT_FOLDER+";" + imageClassParameters, true);

	fwTree_getRootNodes(dsNode, exceptionInfo);

	if(dynlen(exceptionInfo) > 0) {
		fwExceptionHandling_display(exceptionInfo);
	}
	else {
		dsResult = dynIntersect(dsTree, dsNode);
	// add topNode: class~key where key is |labelOfTheNode;dpNameOfTheNode
		len = dynlen(dsResult);
		for(i=1;i<=len;i++) {
			sKey = unTreeWidget_compositeLabelSeparator+dsResult[i]+unTreeWidget_labelSeparator+unTreeWidget_treeNodeDpPrefix+dsResult[i];
			unTreeWidget_addNode(sGraphTree, unTreeWidget_treeNodeDpPrefix+dsResult[i], unTreeWidget_getIconClass(unTreeWidget_treeNodeDpPrefix+dsResult[i])+unTreeWidget_classSeparator+sKey, true, exceptionInfo);

// fill the g_dsTree: contains the list of tree that are present in the unTreeWidget.
			dynAppend(g_dsTree, dsResult[i]);
			dynAppend(g_sPasteNodeName, "");
			dynAppend(g_sPasteNodeDpName, "");
		}
	}

	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
	unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
	unProgressBar_stop(id, sBar);

// handle any errors here
	if(dynlen(exceptionInfo)>0) {
		fwExceptionHandling_display(exceptionInfo);
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_EventRigthClick
/**
Purpose:
Function executed when one does a right click on the tree in the unicosSystemDP panel. popupMenuXY can be used to show a popup menu.
WARNING: popuMenu cannot be used.

	@param sGraphTree: string, input: the name of the unTreeWidget graphical element
	@param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
	@param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
	@param iPosX: int, input: x position of the mouse
	@param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unGraphicalFrame_EventRightClick(string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
	string sKey, sCopiedNodeName;
	dyn_string dsMenu, exInfo, dsText;
	int res, ans;
	dyn_float dfFloat;
	shape aShape;
	int id, nodeCount;
	string sCompositeLabel, sNodeDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sTree, sTreeToDelete, sTreeDpToDelete;
	int iCurrentMousePointer, iMousePointer;
	string sGranted = granted.text();
        bool bGranted;

        if(strtoupper(sGranted) == "TRUE")
          bGranted = true;
        if(!bGranted)
          return;

// start the progress bar
	id = unProgressBar_start(sBar);
	aShape = getShape(sGraphTree);
// disable the unTreeWidget tree to have one action at a time.
	unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
	iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
	iMousePointer = unTreeWidget_ccHourglass;
	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
	nodeCount = unTreeWidget_getPropertyValue(aShape, "getNodeNumber", true);

	dynAppend(dsMenu, "PUSH_BUTTON, --- Tree menu ---, "+unTreeWidget_popupMenu_nodeName+", 0");
	dynAppend(dsMenu,"SEPARATOR");
	if(nodeCount <= 0) {
		dynAppend(dsMenu,"PUSH_BUTTON, get all trees with clipboard, "+unTreeWidget_popupMenu_initTree+", 1");
	}
	else {
		dynAppend(dsMenu,"PUSH_BUTTON, get all trees with clipboard, "+unTreeWidget_popupMenu_initTree+", 0");
	}
	dynAppend(dsMenu,"PUSH_BUTTON, Clear tree list, "+unTreeWidget_popupMenu_clearTree+", 1");
	dynAppend(dsMenu,"SEPARATOR");
	dynAppend(dsMenu,"PUSH_BUTTON, Add tree ..., "+unTreeWidget_popupMenu_addTree+", 1");
	dynAppend(dsMenu,"PUSH_BUTTON, Copy tree ..., "+unTreeWidget_popupMenu_copyTree+", 1");
	dynAppend(dsMenu,"PUSH_BUTTON, Remove tree ..., "+unTreeWidget_popupMenu_removeTree+", 1");

// show it
	res = popupMenuXY(dsMenu,iPosX, iPosY, ans);

	switch(ans) {
		case unTreeWidget_popupMenu_addTree:
			unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Add Tree", makeDynString("$sTextQuestion:New tree name:"), dfFloat, dsText);
			if(dynlen(dfFloat)>0) {
// new node.
				if(dsText[1] != "") {
					unTreeWidget_addTree(sGraphTree, bClipboard, dsText[1], exInfo);
				}
			}
			break;
		case unTreeWidget_popupMenu_copyTree:
			unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetList.pnl", "Select Tree", makeDynString("$sTitle:Select tree to copy:"), dfFloat, dsText);
			if(dynlen(dfFloat)>0) {
				if(dfFloat[1] == 1) {
					sCopiedNodeName = dsText[2];
					unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Copy Tree", makeDynString("$sTextQuestion:New name for copy of \""+dsText[1]+"\":"), dfFloat, dsText);
					if(dynlen(dfFloat)>0) {
// new tree.
						if(dsText[1] != "") {
							unTreeWidget_copyTree(sGraphTree, bClipboard, sCopiedNodeName, dsText[1], exInfo);
						}
					}
				}
			}
			break;
		case unTreeWidget_popupMenu_removeTree:
			sCompositeLabel = unTreeWidget_getPropertyValue(aShape, "selectedCompositeLabel", true);
			if(sCompositeLabel != "") {
				unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sNodeDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
// get the tree
				fwTree_getTreeName(_fwTree_getNodeName(sNodeDpName), sTree, exInfo);
			}
			unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetList.pnl", "Remove Tree", makeDynString("$sTitle:Select tree to remove:"), dfFloat, dsText);
			if(dynlen(dfFloat)>0) {
				if(dfFloat[1] == 1) {
					sTreeDpToDelete = dsText[1];
					sTreeToDelete = dsText[2];
// ask confirmation
					dfFloat = makeDynFloat();
					unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/MessageInfo", "remove tree confirmation",
																makeDynString("$1:Are you sure you want to remove the tree \n"+sTreeToDelete+"?", "$2:Ok", "$3:Cancel"), dfFloat, dsText);
					if(dynlen(dfFloat)>0) {
						if(dfFloat[1] == 1) {
// got confirmation
		// build the key of the root node and delete it:
							sKey = unTreeWidget_compositeLabelSeparator+sTreeToDelete+unTreeWidget_labelSeparator+sTreeDpToDelete;
							unTreeWidget_removeTree(sGraphTree, bClipboard, sKey, sTreeDpToDelete, sTreeToDelete, exInfo);
							if(sTree == sTreeToDelete) {
		// reset the global variable Lesparent inside the unTreeWidget, this global variable is used ot change the icon of the parent node
		// if one delete a tree from the right click and a node in the tree is selected then when one changes the node after the
		// delete, the unTreeWidget crashs with an error: element not found
								aShape.resetParentCollection(true);
							}
						}
					}
				}
			}
			break;
		case unTreeWidget_popupMenu_initTree:
// set the show clipboard to true
			unTreeWidget_setPropertyValue(aShape, "set_bClipboard", true, true);
			g_sPasteNodeName= makeDynString();
			g_sPasteNodeDpName = makeDynString();
			g_dsTree = makeDynString();
			unGraphicalFrame_initialize(sGraphTree, sBar, true);
			break;
		case unTreeWidget_popupMenu_clearTree:
			unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
			break;
		default:
			break;
	}

	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
	unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
	unProgressBar_stop(id, sBar);

// display the errors here.
	if(dynlen(exInfo) > 0)
		fwExceptionHandling_display(exInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_EventNodeRigthClick
/**
Purpose:
Function executed when one does a right click on a node of the tree in the unicosSystemDp panel. popupMenuXY can be used to show a popup menu.
WARNING: popuMenu cannot be used.

	@param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
	@param sMode: string, input: the type of mouse click
	@param sCompositeLabel: string, input: the composite label of the node
	@param sGraphTree: string, input: the name of the unTreeWidget graphical element
	@param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
	@param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
	@param iPosX: int, input: x position of the mouse
	@param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unGraphicalFrame_EventNodeRigthClick(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
	string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sParentKey, sTree, sCopiedNodeName, sNodeName;
	dyn_string dsMenu, exInfo, dsChildren;
	int res, ans, paste_flag;
	shape aShape;
	int id, pos;
	dyn_float dfFloat;
	dyn_string dsText;
	string sKey;
	int isClipboard, isRoot;
	string sCurrentSelectedNodeKey;
	bool isInClipboard;
	int iCurrentMousePointer, iMousePointer;
	string sGranted = granted.text();
        bool bGranted;

        if(strtoupper(sGranted) == "TRUE")
          bGranted = true;
        if(!bGranted)
          return;

// start the progress bar
	id = unProgressBar_start(sBar);
	aShape = getShape(sGraphTree);
// disable the unTreeWidget tree to have one action at a time.
	unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
	iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
	iMousePointer = unTreeWidget_ccHourglass;
	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);

	sParentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
	sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey", true);

	unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
	sNodeName = _fwTree_getNodeName(sDpName);
	isClipboard = fwTree_isClipboard(sNodeName, exInfo);
	isInClipboard = fwTree_isInClipboard(sNodeName, exInfo);
	isRoot = fwTree_isRoot(sNodeName, exInfo);

// get the tree
	fwTree_getTreeName(sNodeName, sTree, exInfo);
	pos = dynContains(g_dsTree, sTree);
// build the popup menu

////!!!!!!!!! take care with &000 when calling isRoot.

	dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
	dynAppend(dsMenu,"SEPARATOR");
	if(isRoot) {
		dynAppend(dsMenu,"PUSH_BUTTON, Remove Tree, "+unTreeWidget_popupMenu_removeTree+", 1");
		dynAppend(dsMenu,"SEPARATOR");
		dynAppend(dsMenu,"PUSH_BUTTON, Copy Tree: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copyTree+", 1");
	}

// show it
	res = popupMenuXY(dsMenu,iPosX, iPosY, ans);

	switch(ans) {
		case unTreeWidget_popupMenu_removeTree:
// ask confirmation
				unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/MessageInfo", "remove tree confirmation",
															makeDynString("$1:Are you sure you want to remove the tree \n"+sDisplayNodeName+"?", "$2:OK", "$3:Cancel"), dfFloat, dsText);
				if(dynlen(dfFloat)>0) {
					if(dfFloat[1] == 1) {
// got confirmation
						unTreeWidget_removeTree(sGraphTree, bClipboard, sSelectedKey, sDpName, sDisplayNodeName, exInfo);
					}
				}
			break;
		case unTreeWidget_popupMenu_copyTree:
			unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Copy Tree",
																makeDynString("$sTextQuestion:New name for copy of \""+sDisplayNodeName+"\":"), dfFloat, dsText);
			if(dynlen(dfFloat)>0) {
// new tree.
				if(dsText[1] != "") {
					unTreeWidget_copyTree(sGraphTree, bClipboard, sDisplayNodeName, dsText[1], exInfo);
				}
			}
			break;
		case unTreeWidget_popupMenu_rename:
			unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Rename Node", makeDynString("$sTextQuestion:New name:", "$currentName:"+sDisplayNodeName), dfFloat, dsText);
			if(dynlen(dfFloat)>0) {
				if(dfFloat[1] == 1) {
					if(dsText[1]!= "") {
						unTreeWidget_rename(sGraphTree, sSelectedKey, sParentKey, sCompositeLabel,
														sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, bClipboard,
														dsText[1], sCurrentSelectedNodeKey, sParentKey, exInfo);
					}
				}
			}
			break;
		default:
			break;
	}

	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
	unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
	unProgressBar_stop(id, sBar);

// display the errors here.
	if(dynlen(exInfo) > 0)
		fwExceptionHandling_display(exInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getMenu
/**
Purpose:
Check and expand a menu, wrong menu return empty.

	@param sMenu: string, input: one menu line of the menu configuration
	@param return, dyn_string, output: result

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
dyn_string unGraphicalFrame_getMenu(string sMenu)
{
  dyn_string dsMenu = strsplit(sMenu, ";");
  dyn_string dsReturn;

  if(dynlen(dsMenu)>=3){
    switch(dsMenu[2]) {
      case "text":
        dsReturn = dsMenu;
        break;
      case "insertItemId":
        if(dynlen(dsMenu)>=8) {
          dsReturn = dsMenu;
        }
        break;
      default:
        break;
    }
  }
  return dsReturn;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getMenuFileAccess
/**
Purpose:
Get the access priv of a file.

	@param sFileName: string, input: file name
        @param bFileExists: bool, output, true if file is in the list/false if not
	@param return, dyn_string, output: the file access priv or "" if no priv

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
string unGraphicalFrame_getMenuFileAccess(string sFileName, bool &bFileExists)
{
  string sFileAccess;

  sFileAccess = unGenericButtonFunctionsHMI_getPanelAccessLevel(sFileName, bFileExists);
  if(sFileAccess == UN_USER_MONITOR)
    sFileAccess = "";
  return sFileAccess;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unCore_getDefaultFileAccessControl
/**
Purpose:
Get the default file access control.

	@param dsFileAccess: dyn_string, output: default UNICOS file access

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unCore_getDefaultFileAccessControl(dyn_string &dsFileAccess)
{
  unGraphicalFrame_getUNICOSDefaultFileAccessControl(dsFileAccess);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getUNICOSDefaultFileAccessControl
/**
Purpose:
Get the default UNICOS file access control.

	@param dsFileAccess: dyn_string, output: default UNICOS file access

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_getUNICOSDefaultFileAccessControl(dyn_string &dsFileAccess)
{
  string sDomain, sOperator, sExpert,sAdmin;

  // monitor access
  sOperator = "";
  sExpert = "";
  sAdmin = "";
/*
  dynAppend(dsFileAccess, "vision/graphicalFrame/unApplicationConfigurationOverview.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unDiagnostics/unArchiveStatistics.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/distributedControl/unDistributedControl_operation.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/Redundanz"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/Connection.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/graphicalFrame/applicationOperation.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unAlertPanel/unAlertScreen.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/graphicalFrame/plcDiagnostic.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/select/unSelectDeselect_diagnostic.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/systemIntegrity/systemIntegrityOperation.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "fwInstallation/fwInstallationList.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
*/
  // operator access
  sOperator = UN_USER_OPERATOR;
  sExpert = "";
  sAdmin = "";
  dynAppend(dsFileAccess, "vision/unAlertPanel/unAlertScreen.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unEventList/unEventList.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unEventList/unEventList_SelectFilter.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unObjectList/unObjectList.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "fwTrending/fwTrendingPage.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "fwTrending/fwTrendingHistogramConfig.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "fwTrending/fwTrendingTrend.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "fwTrending/fwTrendingHistogram.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);

  // expert access
  sOperator = "";
  sExpert = UN_USER_EXPERT;
  sAdmin = "";
  dynAppend(dsFileAccess, "vision/windowTree/unWindowTreeConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/trendTree/unTrendTreeConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/graphicalFrame/unTreeDeleteDps.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unLaser/unLaserStatus.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWClient/unCMWClientStatus.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWServer/unCMWServerStatus.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/systemIntegrity/unConfigMail.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/systemIntegrity/unMailCategoryConfig.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unicosObject/General/unModifyConfig.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "fwTrending/fwTrendingPlotConfPanel.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "fwTrending/fwTrendingPlotsPage.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);

  // admin access
  sOperator = "";
  sExpert = "";
  sAdmin = UN_USER_ADMIN;
  dynAppend(dsFileAccess, "vision/graphicalFrame/applicationConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/graphicalFrame/applicationConfigurationSetDefaultSettings.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/graphicalFrame/addSystemIntegrity.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/systemIntegrity/systemIntegrityConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/systemIntegrity/unSystemIntegrityConfigurationHMI.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/systemIntegrity/stopDriverAndCtrl.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unAlertPanel/unAlertPanel_ConfigureNames.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unicosObject/General/unTypeConfigurator.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unLaser/unLaserManager.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unLaser/unLaserConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unLaser/unLaserConfigFile.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unLaser/unLaserConfigFile_Check.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unLaser/unLaserExpertSetting.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWClient/unCMWClientDriver.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWServer/unCMWServerManager.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWServer/unCMWServerConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWServer/unCMWServerConfigFile.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWServer/unCMWServerConfigFile_Check.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unCMWServer/unCMWServerExpertSetting.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/distributedControl/unDistributedControl_configuration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unicosObject/textLabel/unTextLabel_modify.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unicosObject/General/unOptionModes_Configure.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unGeneration/unGenerateDatabase.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unGeneration/unImportTree.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/graphicalFrame/unFileAccessControlConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/graphicalFrame/unMenuConfiguration.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unDiagnostics/unUIOverview.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/unicosObject/General/unSetDeviceDPEConfigs.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
  dynAppend(dsFileAccess, "vision/systemIntegrity/unBackup.pnl"+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sDomain+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unCore_getDefaultMenuConfiguration
/**
Purpose:
Get the default menu configuration.

	@param dsMenu: dyn_string, output: default UNICOS menu configuration

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unCore_getDefaultMenuConfiguration(dyn_string &dsMenu)
{
  unGraphicalFrame_getUNICOSDefaultMenuConfiguration(dsMenu);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getUNICOSDefaultMenuConfiguration
/**
Purpose:
Get the default UNICOS menu configuration.

	@param dsMenu: dyn_string, output: default UNICOS menu configuration

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_getUNICOSDefaultMenuConfiguration(dyn_string &dsMenu)
{
  dynAppend(dsMenu, "Configuration;text;Configuration   ;");
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Alarm Bits (local)",
                  UN_MENU_ITEM, "vision/unAlertPanel/unAlertPanel_ConfigureNames.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Application (local)",
                  UN_MENU_ITEM, "vision/graphicalFrame/applicationConfiguration.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Backup (local)",
                  UN_MENU_ITEM, "vision/systemIntegrity/unBackup.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "CMW Client",
                  UN_MENU_ITEM, "vision/unCMWClient/unCMWClientDriver.pnl",
                  "unCMWClient", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "CMW Server",
                  UN_MENU_ITEM, "vision/unCMWServer/unCMWServerManager.pnl",
                  "unCMWServer", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Devices (local)",
                  UN_MENU_ITEM, "vision/unicosObject/General/unModifyConfig.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Device DPEs (local)",
                  UN_MENU_ITEM, "vision/unicosObject/General/unSetDeviceDPEConfigs.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Device Types (local)",
                  UN_MENU_ITEM, "vision/unicosObject/General/unTypeConfigurator.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Distributed Systems (local)",
                  UN_MENU_ITEM, "vision/distributedControl/unDistributedControl_configuration.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "File Access (local)",
                  UN_MENU_ITEM, "vision/graphicalFrame/unFileAccessControlConfiguration.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Import Database (local)",
                  UN_MENU_ITEM, "vision/unGeneration/unGenerateDatabase.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Import Tree (local)",
                  UN_MENU_ITEM, "vision/unGeneration/unImportTree.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "LHC Laser",
                  UN_MENU_ITEM, "vision/unLaser/unLaserManager.pnl",
                  "unLHCLASER", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Mail/SMS (local)",
                  UN_MENU_ITEM, "vision/systemIntegrity/unConfigMail.pnl",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Menus (local)",
                  UN_MENU_ITEM, "vision/graphicalFrame/unMenuConfiguration.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "System Integrity (local)",
                  UN_MENU_ITEM, "vision/systemIntegrity/systemIntegrityConfiguration.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Text Labels (local)",
                  UN_MENU_ITEM, "vision/unicosObject/textLabel/unTextLabel_modify.pnl",
                  "", UN_USER_ADMIN));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Trend Tree (local)",
                  UN_MENU_ITEM, "vision/trendTree/unTrendTreeConfiguration.pnl",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Configuration", "",
                  "Window Tree (local)",
                  UN_MENU_ITEM, "vision/windowTree/unWindowTreeConfiguration.pnl",
                  "", UN_USER_EXPERT));


  dynAppend(dsMenu, "Management;text;Management  ;");

  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "",
                  "Diagnostic",
                  UN_MENU_POPUP));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Application",
                  UN_MENU_ITEM, "vision/graphicalFrame/applicationOperation.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Application Overview (local)",
                  UN_MENU_ITEM, "vision/graphicalFrame/unApplicationConfigurationOverview.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Archives",
                  UN_MENU_ITEM, "vision/unDiagnostics/unArchiveStatistics.pnl",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "CMW Client",
                  UN_MENU_ITEM, "vision/unCMWClient/unCMWClientStatus.pnl",
                  "unCMWClient", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "CMW Server",
                  UN_MENU_ITEM, "vision/unCMWServer/unCMWServerStatus.pnl",
                  "unCMWServer", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Disk Space (local)",
                  UN_MENU_ITEM, "vision/Device.pnl",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Distributed Systems (local)",
                  UN_MENU_ITEM, "vision/distributedControl/unDistributedControl_operation.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Ev Connections (local)",
                  UN_MENU_ITEM, "vision/Connection.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Front-Ends",
                  UN_MENU_ITEM, "vision/graphicalFrame/plcDiagnostic.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "LHC Laser",
                  UN_MENU_ITEM, "vision/unLaser/unLaserStatus.pnl",
                  "unLHCLASER", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "Memory (local)",
                  UN_MENU_ITEM, "vision/DeviceVirtual.pnl",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "System Alarms",
                  UN_MENU_ITEM, "vision/systemIntegrity/unSystemIntegrity_getStats.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "System Integrity",
                  UN_MENU_ITEM, "vision/systemIntegrity/systemIntegrityOperation.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "System Overview (local)",
                  UN_MENU_ITEM, "vision/Redundanz",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "UNICOS Select",
                  UN_MENU_ITEM, "vision/select/unSelectDeselect_diagnostic.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Diagnostic",
                  "User Interfaces",
                  UN_MENU_ITEM, "vision/unDiagnostics/unUIOverview.pnl",
                  "", UN_USER_ADMIN));


  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "",
                  "Utilities (local)",
                  UN_MENU_POPUP));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "Alarm Archive",
                  UN_MENU_ITEM, "vision/ArchivControl",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "Export Database",
                  UN_MENU_ITEM, "vision/unGeneration/unExportDatabase.pnl",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "Export Tree",
                  UN_MENU_ITEM, "vision/unGeneration/unExportTree.pnl",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "List of Masked Alarms",
                  UN_MENU_ITEM, "vision/unicosObject/General/unDevice_MaskedAlert.pnl",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "List of Masked Events",
                  UN_MENU_ITEM, "vision/unicosObject/General/unDevice_MaskedEvent.pnl",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "Log Statistics",
                  UN_MENU_ITEM, "vision/UserConnections",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "MODBUS Driver",
                  UN_MENU_ITEM, "para/modbus.pnl",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "Online Backup",
                  UN_MENU_ITEM, "vision/OnlineBackup.pnl",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "Reset Managers",
                  UN_MENU_ITEM, "vision/systemIntegrity/stopDriverAndCtrl.pnl",
                  "", UN_USER_OPERATOR));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "S7 Driver",
                  UN_MENU_ITEM, "para/s7.pnl",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "System Management",
                  UN_MENU_ITEM, "vision/SysMgm",
                  "", UN_USER_EXPERT));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "Utilities (local)",
                  "Value Archive",
                  UN_MENU_ITEM, "para/archive/archive_select.pnl",
                  "", UN_USER_EXPERT));


  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "",
                  "Debug Utility (local)",
                  UN_MENU_ITEM, "vision/unCore/unDebugUtility.pnl",
                  "", ""));
  dynAppend(dsMenu, unGraphicalFrame_addUNICOSDefaultMenuLine("Management", "",
                  "List of Components (local)",
                  UN_MENU_ITEM, "fwInstallation/fwInstallationList.pnl",
                  "", ""));
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_addUNICOSDefaultMenuLine
/**
Purpose:
Add one default UNICOS menu line into the menu.

	@param sShape: string, input: shape menu name
	@param sParent: string, input: parent menu
	@param sItem: string, input: item name
	@param sType: string, input: type of menu, UN_MENU_POPUP=pop-up, UN_MENU_ITEM=item or UN_MENU_SEP=separator
	@param sFile: string, input: file name to be opened
	@param sShape: sComponent, input: component name needed to validate the entry, "" for none
	@param sAccess: string, input: file access priviledge
	@param return: string, input: menu line configuration

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
string unGraphicalFrame_addUNICOSDefaultMenuLine(string sShape, string sParent, string sItem, string sType, string sFile="", string sComponent="", string sAccess="")
{
  bool bFileExists;
  string sPriv;
  dyn_string ds;

  ds[1] = sShape;
  ds[2] = "insertItemId";
  ds[3] = sParent;
  ds[5] = sItem;
  if(sType == UN_MENU_ITEM) {
    ds[4] = "0";
    ds[6] = sFile;
    ds[7] = sComponent;
    sPriv = unGraphicalFrame_getMenuFileAccess(sFile, bFileExists);
    if(bFileExists)
      ds[8] = sPriv;
    else
      ds[8] = sAccess;
  }
  else if(sType == UN_MENU_POPUP) {
    ds[4] = "1";
    ds[6] = "";
    ds[7] = "";
    ds[8] = "";
  }
  else if(sType == UN_MENU_SEP) {
    ds[4] = "2";
    ds[6] = "";
    ds[7] = "";
    ds[8] = "";
  }
  return ds[1]+";"+ds[2]+";"+ds[3]+";"+ds[4]+";"+ds[5]+";"+ds[6]+";"+ds[7]+";"+ds[8]+";";
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_menuConfigurationCB
/**
Purpose:
Callback when the menu configuration or the file access configuration is modified.

	@param sDpe1: string, input: dpname
	@param dsMenuFileAccessControl: dyn_string, input: file access control
	@param sDpe2: string, input: dpname
	@param dsMenuConfiguration: dyn_string, input: configuration of the menu

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unGraphicalFrame_menuConfigurationCB(string sDpe1, dyn_string dsMenuFileAccessControl, string sDpe2, dyn_string dsMenuConfiguration)
{
  synchronized(g_m_dsMenuItemId)
  {
    unGraphicalFrame_resetMenuVar();
    unGenericButtonFunctionsHMI_setPanelAccessControlList(dsMenuFileAccessControl);
    g_dsMenuConfiguration = dsMenuConfiguration;
    unGraphicalFrame_buildMenuList(dsMenuConfiguration, g_dsComponentInfo);
  }
  unGraphicalFrame_menuUserCB("", "");
  g_b_unicosHMI_menu = true;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_buildMenuList
/**
Purpose:
Build the menu.

	@param dsMenuConfiguration: dyn_string, input: configuration of the menu
	@param dsComponentInfo: dyn_string, input: list of component installed

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_buildMenuList(dyn_string dsMenuConfiguration, dyn_string dsComponentInfo)
{
  int i, len=dynlen(dsMenuConfiguration);
  dyn_string dsMenu;
  int iType;
  bool bAdd;
  bool bFileExists, bEnable;
  string sPriv, sItemId;

  for(i=1;i<=len;i++) {
    dsMenu = unGraphicalFrame_getMenu(dsMenuConfiguration[i]);
    if(dynlen(dsMenu)>0) {
      switch(dsMenu[2]) {
        case "text":
//          if(shapeExists("topMenu")) {
          g_m_sTextMenu[""] = "";
          g_m_sFileName[""] = "";
          unGraphicalFrame_addMenuItem("", 1, dsMenu[1], dsMenu[3], "", "", "", true);
//          }
          break;
        case "insertItemId":
          bAdd = true;
          if(dsMenu[7]!="") {
            if(dynContains(dsComponentInfo, dsMenu[7]) <= 0)
              bAdd = false;
          }
          if(bAdd){
            sscanf(dsMenu[4], "%d", iType);
            if(iType == 0)
              sItemId = i+"-"+dsMenu[5];
            else
              sItemId = dsMenu[5];
            sItemId = dsMenu[1]+"-"+sItemId;
            if(dsMenu[3] =="")
              dsMenu[3] = dsMenu[1];
            else
              dsMenu[3] = dsMenu[1]+"-"+dsMenu[3];
            dsMenu[1] = "";
            bEnable = true;
            if((dsMenu[6] != "") && (iType == 0)) {
              bEnable = false;
              sPriv = unGraphicalFrame_getMenuFileAccess(dsMenu[6], bFileExists);
              if(bFileExists) {
                dsMenu[8] = sPriv;
              }
            }
            if(dsMenu[8] == "")
              dsMenu[8] = UN_USER_MONITOR;
            unGraphicalFrame_addMenuItem(dsMenu[3], iType, sItemId, dsMenu[5], dsMenu[6], dsMenu[8], bEnable);
          }
          break;
        default:
          break;
      }
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_deleteAndBuildMenu
/**
Purpose:
Delete the cascade menu

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_dsCascadeMenu, g_m_dsCascadeMenuKey
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_deleteAndBuildMenu()
{
  int i, len, iDs, lenDs;
  string sMenuItem, sChild, sKey;
  int iType;
  dyn_string dsMenuShapeName, dsTemp, dsInKey, dsTempKey;

  len = mappinglen(g_m_dsCascadeMenu);
  for(i=1;i<=len;i++) {
    dsTemp = mappingGetValue(g_m_dsCascadeMenu, i);
    dsTempKey = mappingGetValue(g_m_dsCascadeMenuKey, i);
    lenDs = dynlen(dsTemp);
    sKey = mappingGetKey(g_m_dsCascadeMenu, i);
    for(iDs=1;iDs<=lenDs;iDs++) {
      dynAppend(dsMenuShapeName, sKey+":"+dsTemp[iDs]);
      dynAppend(dsInKey, dsTempKey[iDs]);
    }
  }
//  if(mappingHasKey(g_m_dsCascadeMenu, myModuleName()))
//    dsMenuShapeName = g_m_dsCascadeMenu[myModuleName()];
  len=dynlen(dsMenuShapeName);
  for(i=1;i<=len;i++) {
    if(shapeExists(dsMenuShapeName[i]+".menuButton")) {
      getValue(dsMenuShapeName[i]+".menuButton", "firstChild", "", sChild, iType);
      while(sChild != "") {
        setValue(dsMenuShapeName[i]+".menuButton", "removeItemId", sChild);
        getValue(dsMenuShapeName[i]+".menuButton", "firstChild", "", sChild, iType);
      }
      setValue(dsMenuShapeName[i]+".menuButton", "removeItemId", sChild);
      unGraphicalFrame_showCascadeMenu(dsInKey[i], dsMenuShapeName[i]);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_disableCascadeMenuButton
/**
Purpose:
disable all the cascade menu buttons that are already initialised.

  @param dsShapeName: dyn_string, output: list of shape name

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_disableCascadeMenuButton(dyn_string &dsShapeName)
{
  bool bEnabled;
  int i, len, iDs, lenDs;
  string sMenuItem, sChild, sKey;
  int iType;
  dyn_string dsMenuShapeName, dsTemp, dsInKey, dsTempKey;


  len = mappinglen(g_m_dsCascadeMenu);
  for(i=1;i<=len;i++) {
    dsTemp = mappingGetValue(g_m_dsCascadeMenu, i);
    dsTempKey = mappingGetValue(g_m_dsCascadeMenuKey, i);
    lenDs = dynlen(dsTemp);
    sKey = mappingGetKey(g_m_dsCascadeMenu, i);
    for(iDs=1;iDs<=lenDs;iDs++) {
      dynAppend(dsMenuShapeName, sKey+":"+dsTemp[iDs]);
      dynAppend(dsInKey, dsTempKey[iDs]);
    }
  }
  len=dynlen(dsMenuShapeName);
  for(i=1;i<=len;i++) {
    if(shapeExists(dsMenuShapeName[i]+".menuButton")) {
      getValue(dsMenuShapeName[i]+".menuButton", "enabled", bEnabled);
      if(bEnabled)
      {
        setValue(dsMenuShapeName[i]+".menuButton", "enabled", false);
        bEnabled = true;
        dynAppend(dsShapeName, dsMenuShapeName[i]+".menuButton");
      }
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_enableCascadeMenuButton
/**
Purpose:
enable all the cascade menu buttons that are already initialised.

  @param dsShapeName: dyn_string, input: list of shape name

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_enableCascadeMenuButton(dyn_string dsShapeName)
{
  int i, len;
  len = dynlen(dsShapeName);

  for(i=1;i<=len;i++) {
    setValue(dsShapeName[i], "enabled", true);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_setPopupMenuText
/**
Purpose:
Set the text of the menu button

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_dsPopupMenu, g_m_dsPopupMenuKey
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_setPopupMenuText()
{
  int i, len, iDs, lenDs;
  string sMenuItem, sChild, sKey;
  int iType;
  dyn_string dsMenuShapeName, dsTemp, dsInKey, dsTempKey;

  len = mappinglen(g_m_dsPopupMenu);
  for(i=1;i<=len;i++) {
    dsTemp = mappingGetValue(g_m_dsPopupMenu, i);
    dsTempKey = mappingGetValue(g_m_dsPopupMenuKey, i);
    lenDs = dynlen(dsTemp);
    sKey = mappingGetKey(g_m_dsPopupMenu, i);
    for(iDs=1;iDs<=lenDs;iDs++) {
      dynAppend(dsMenuShapeName, sKey+":"+dsTemp[iDs]);
      dynAppend(dsInKey, dsTempKey[iDs]);
    }
  }
  len=dynlen(dsMenuShapeName);
  for(i=1;i<=len;i++) {
    if(shapeExists(dsMenuShapeName[i]+".menuButton")) {
      if(mappingHasKey(g_m_sTextMenu, dsInKey[i]))
        setValue(dsMenuShapeName[i]+".menuButton", "text", g_m_sTextMenu[dsInKey[i]]);
      else
        setValue(dsMenuShapeName[i]+".menuButton", "text", "not found "+dsInKey[i]);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_resetMenuVar
/**
Purpose:
Reset the global var

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_dsMenuItemId, g_m_diType, g_m_sTextMenu, g_m_sFileName, g_m_bEnabled, g_m_sMenuAccess
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_resetMenuVar()
{
  mapping m;
  g_m_dsMenuItemId = m;
  g_m_diType = m;

  g_m_sTextMenu = m;
  g_m_sFileName = m;
  g_m_bEnabled = m;
  g_m_sMenuAccess = m;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_menuUserCB
/**
Purpose:
Callback function on the user who is logon to set the menu.

	@param dsMenuConfiguration: dyn_string, input: configuration of the menu
	@param dsComponentInfo: dyn_string, input: list of component installed

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_menuUserCB(string sUi, string sUser)
{
  int i, len;
  string sMenuShapeName, sMenuItem, sMenuFile;
  bool bAllowed;
  dyn_string exceptionInfo;
  dyn_string dsShapeName;

  synchronized(g_m_dsCascadeMenu) {
    unGraphicalFrame_disableCascadeMenuButton(dsShapeName);
  }
  synchronized(g_m_dsMenuItemId)
  {
    len = mappinglen(g_m_sFileName);
    for(i=1;i<=len;i++) {
      sMenuItem = mappingGetKey(g_m_sFileName, i);
      sMenuFile = mappingGetValue(g_m_sFileName, i);
      bAllowed = true;
      if(sMenuFile != "") {
        unGenericButtonFunctionsHMI_isAccessAllowed(sMenuFile, UN_GENERIC_USER_ACCESS, g_m_sMenuAccess[sMenuItem], bAllowed, exceptionInfo);
        if(g_bUnGraphicalFrame_RDB && ((sMenuFile == "para/archive/archive_select.pnl") || (sMenuFile == "vision/ArchivControl")))
          bAllowed = false;
      }
      g_m_bEnabled[sMenuItem] = bAllowed;
    }
    synchronized(g_m_dsCascadeMenu) {
      unGraphicalFrame_deleteAndBuildMenu();
    }
    synchronized(g_m_dsPopupMenu) {
      unGraphicalFrame_setPopupMenuText();
    }
  }
  synchronized(g_m_dsCascadeMenu) {
    unGraphicalFrame_enableCascadeMenuButton(dsShapeName);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_addMenuItem
/**
Purpose:
Add one menu item in global var.

	@param sKey: string, input: menu item key (unique)
	@param iType: int, input: type of cascade item, 0-item, 1=popup, 2=separator
	@param sItemId: string, input: item of the cascade menu
	@param sTextMenu: string, input: text of the item
	@param sFile: string, input: file to open if the item is selected
	@param sAccess: string, input: access contreol of the item
	@param bEnable: bool, input: true=item enabled, false=item disabled

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_dsMenuItemId, g_m_diType, g_m_sTextMenu, g_m_sFileName, g_m_bEnabled, g_m_sMenuAccess
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_addMenuItem(string sKey, int iType, string sItemId, string sTextMenu, string sFile, string sAccess, bool bEnable)
{
  dyn_string dsMenu, dsMenuFileName, dsMenuAccess, dsTextMenu;
  dyn_int diType;
  dyn_bool dbEnabled;
  if(mappingHasKey(g_m_dsMenuItemId, sKey))
  {
    dsMenu = g_m_dsMenuItemId[sKey];
    diType = g_m_diType[sKey];
  }
  dynAppend(dsMenu, sItemId);
  dynAppend(diType, iType);
  dynAppend(dsTextMenu, sTextMenu);
  dynAppend(dbEnabled, bEnable);
  dynAppend(dsMenuFileName, sFile);
  dynAppend(dsMenuAccess, sAccess);

  g_m_dsMenuItemId[sKey] = dsMenu;
  g_m_diType[sKey] = diType;

  g_m_sTextMenu[sItemId] = sTextMenu;
  g_m_sFileName[sItemId] = sFile;
  g_m_bEnabled[sItemId] = bEnable;
  g_m_sMenuAccess[sItemId] = sAccess;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showCascadeMenu
/**
Purpose:
Show the cascade menu.

	@param sInKey: string, input: menu item key (unique)
	@param sShapeName: string, input: cascade menu shape name

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_dsMenuItemId, g_m_diType, g_m_sTextMenu, g_m_sFileName, g_m_bEnabled, g_m_sMenuAccess
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showCascadeMenu(string sInKey, string sShapeName)
{
  int i, len;
  dyn_string dsMenu, dsFile, dsKey, dsChildKey;

  dsKey = makeDynString(sInKey);
  len = dynlen(dsKey);
  while(len > 0) {
    dsChildKey = makeDynString();
    for(i=1;i<=len;i++) {
      dynAppend(dsChildKey, unGraphicalFrame_buildCascadeMenu(dsKey[i], sShapeName, sInKey));
    }
    dsKey = dsChildKey;
    len = dynlen(dsKey);
  }
  if(mappingHasKey(g_m_sTextMenu, sInKey))
  {
    setValue(sShapeName+".menuButton", "text", g_m_sTextMenu[sInKey]);
  }
  else
    setValue(sShapeName+".menuButton", "text", "not found "+sInKey);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_buildCascadeMenu
/**
Purpose:
Build the cascade menu.

	@param sKey: string, input: menu item key (unique)
	@param sShapeName: string, input: cascade menu shape name
	@param sTopKey: string, input: used to set the cascade menu text
        @param return value: dyn_string, list of item key

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
dyn_string unGraphicalFrame_buildCascadeMenu(string sKey, string sShapeName, string sTopKey)
{
  int i, len;
  dyn_string dsMenuItemId, dsMenuFileName, dsMenuAccess, dsTextMenu;
  dyn_int diType;
  dyn_bool dbEnable;
  string sEnable;
  string sParent;

  unGraphicalFrame_getMenuVar(sKey, dsMenuItemId, diType, dbEnable, dsMenuFileName, dsMenuAccess, dsTextMenu);
  len=dynlen(dsMenuItemId);
  if(sKey != sTopKey)
    sParent = sKey;
  for(i=1;i<=len;i++) {
    switch(diType[i])
    {
      case 0: //item
        setValue(sShapeName+".menuButton", "insertItemId", sParent, diType[i], -1, dsMenuItemId[i], dsTextMenu[i]);
        setValue(sShapeName+".menuButton", "enableItemId", dsMenuItemId[i], dbEnable[i]);
        break;
      case 1: // popup
        setValue(sShapeName+".menuButton", "insertItemId", sParent, diType[i], -1, dsMenuItemId[i], dsTextMenu[i]);
        break;
      case 2: // sep
        setValue(sShapeName+".menuButton", "insertItemId", sParent, diType[i], -1, dsMenuItemId[i], dsTextMenu[i]);
        break;
    }
  }
  return dsMenuItemId;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showPopupMenu
/**
Purpose:
Show the popup menu.

	@param dsInKey: dyn_string, input: list of menu item key (unique)
	@param sFileName: string, output: selected file name
	@param sPanelName: string, output: selected panel name
	@param id: string, output: selected item key

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showPopupMenu(dyn_string dsInKey, string &sFileName, string &sPanelName, string &id)
{
  int i, len;
  dyn_string dsMenu, dsFile, dsKey, dsChildKey, dsPanelName, dsMenuId;
  int iPanelX, iPanelY, iW, iH, iSizeX, iSizeY;

  int iOffset = 3;

  dsKey = dsInKey;
  len = dynlen(dsKey);
  synchronized(g_m_dsMenuItemId){
    while(len > 0) {
      dsChildKey = makeDynString();
      for(i=1;i<=len;i++) {
        dynAppend(dsChildKey, unGraphicalFrame_buildPopupMenu(dsKey[i], dsMenu, dsFile, dsPanelName, dsMenuId));
      }
      dsKey = dsChildKey;
      len = dynlen(dsKey);
    }
  }
  dynRemove(dsMenu, 1);
  dynRemove(dsFile, 1);
  dynRemove(dsPanelName, 1);
  dynRemove(dsMenuId, 1);
  if(dynlen(dsMenu) > 0) {
    panelPosition(myModuleName(),"", iPanelX, iPanelY);
    getValue("", "position", iW, iH);
    getValue("", "size", iSizeX, iSizeY);
    iPanelX+=iW;
    iPanelY += iH+ iSizeY+iOffset;
    popupMenuXY(dsMenu, iPanelX, iPanelY, len);
    if(len>0) {
      sFileName = dsFile[len];
      sPanelName = dsPanelName[len];
      id = dsMenuId[len];
    }
    else {
      sFileName = "";
      sPanelName = "";
      id = "";
    }
  }

}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_buildPopupMenu
/**
Purpose:
Build the popup menu.

	@param sKey: string, input: menu item key (unique)
	@param dsFileName: dyn_string, output: list of file names
	@param dsPanelName: dyn_string, output: list of panel names
	@param dsMenuId: dyn_string, output: list of item keys
        @param return value: dyn_string, list of item key

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
dyn_string unGraphicalFrame_buildPopupMenu(string sKey, dyn_string &dsMenu, dyn_string &dsFile, dyn_string &dsPanelName, dyn_string &dsMenuId)
{
  int i, len;
  dyn_string dsMenuItemId, dsMenuFileName, dsMenuAccess, dsTextMenu;
  dyn_int diType;
  dyn_bool dbEnable;
  string sEnable;

  if(mappingHasKey(g_m_dsMenuItemId, sKey)) {
    unGraphicalFrame_getMenuVar(sKey, dsMenuItemId, diType, dbEnable, dsMenuFileName, dsMenuAccess, dsTextMenu);
    if(mappingHasKey(g_m_sTextMenu, sKey))
      dynAppend(dsMenu, g_m_sTextMenu[sKey]);
    else
      dynAppend(dsMenu, "");
    dynAppend(dsMenuId, sKey);
    dynAppend(dsFile, "");
    dynAppend(dsPanelName, "");
  }
  len=dynlen(dsMenuItemId);
  for(i=1;i<=len;i++) {
    if(dbEnable[i])
      sEnable = "1";
    else
      sEnable = "0";
    switch(diType[i])
    {
      case 0: //item
        dynAppend(dsMenu, "PUSH_BUTTON, "+dsTextMenu[i]+", "+dynlen(dsMenu)+", "+sEnable);
        dynAppend(dsFile, dsMenuFileName[i]);
        dynAppend(dsPanelName, dsTextMenu[i]);
        dynAppend(dsMenuId, dsMenuItemId[i]);
        break;
      case 1: // popup
        dynAppend(dsMenu, "CASCADE_BUTTON, "+dsTextMenu[i]+", 1");
        dynAppend(dsFile, "");
        dynAppend(dsPanelName, "");
        dynAppend(dsMenuId, dsMenuItemId[i]);
        break;
      case 2: // sep
        dynAppend(dsMenu, "SEPARATOR");
        dynAppend(dsFile, "");
        dynAppend(dsPanelName, "");
        dynAppend(dsMenuId, dsMenuItemId[i]);
        break;
    }
  }
  return dsMenuItemId;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getMenuVar
/**
Purpose:
Get one menu item from global var.

	@param sKey: string, input: menu item key (unique)
	@param dsItemId: dyn_string, output: item of the cascade menu
	@param diType: dyn_int, output: type of cascade item, 0-item, 1=popup, 2=separator
	@param dbEnable: dyn_bool, output: true=item enabled, false=item disabled
	@param dsMenuFileName: dyn_string, output: file to open if the item is selected
	@param dsMenuAccess: dyn_string, output: access contreol of the item
	@param dsTextMenu: dyn_string, output: text of the item

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_dsMenuItemId, g_m_diType, g_m_sTextMenu, g_m_sFileName, g_m_bEnabled, g_m_sMenuAccess
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_getMenuVar(string sKey, dyn_string &dsMenuItemId, dyn_int &diType, dyn_bool &dbEnabled,
                            dyn_string &dsMenuFileName, dyn_string &dsMenuAccess, dyn_string &dsTextMenu)
{
  dyn_bool dbIdEnabled;
  dyn_string dsIdMenuFileName, dsIdMenuAccess, dsIdTextMenu;
  int i, len;

  if(mappingHasKey(g_m_dsMenuItemId, sKey)) {
    dsMenuItemId=g_m_dsMenuItemId[sKey];
    diType = g_m_diType[sKey];
    len = dynlen(dsMenuItemId);
    for(i=1;i<=len;i++) {
      dynAppend(dsIdMenuFileName, g_m_sFileName[dsMenuItemId[i]]);
      dynAppend(dbIdEnabled, g_m_bEnabled[dsMenuItemId[i]]);
      dynAppend(dsIdTextMenu, g_m_sTextMenu[dsMenuItemId[i]]);
      dynAppend(dsIdMenuAccess, g_m_sMenuAccess[dsMenuItemId[i]]);
    }
    dbEnabled = dbIdEnabled;
    dsMenuFileName = dsIdMenuFileName;
    dsMenuAccess = dsIdMenuAccess;
    dsTextMenu = dsIdTextMenu;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_checkLibLoaded
/**
Purpose:
check if UNICOS default libs and extra libs are loaded correctly.

	@param dsExtraLibs: dyn_string, input: extra libs function
	@param return: bool, output: true=all libs loaded/false=some libs not loaded

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool unGraphicalFrame_checkLibLoaded(dyn_string dsExtraLibs)
{
  bool bResult=true;
  dyn_string dsLib=dsExtraLibs;
  int i, len;


  dynAppend(dsLib, "CtrlEventList_displayUTCTime");
  dynAppend(dsLib, "CtrlTreeDeviceOverview_getSelectedDeviceList");
 // check ctrlEventList, Ctrl_TreeDeviceOverview
  len = dynlen(dsLib);
  for(i=1;i<=len;i++) {
    if(!isFunctionDefined(dsLib[i])) {
      DebugTN("!!!!!!!!!!! LIB NOT DEFINED", myModuleName(), myManNum(), dsLib[i]);
      bResult = false;
    }
  }

  return bResult;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showButtonPanel
/**
Purpose:
Open the panel function called from cascade menu, icon, button.

	@param sFileName: string, input: file name
	@param sButton: string, input: button name
	@param id: string, input: selected id menu
	@param sPanelName: string, input: panel name

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName and global variable to keep panel settings
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showButtonPanel(string sFileName, string id, string sPanelName)
{
  int iPos;
  string sButton;

  if(sFileName != "") {
    iPos = strpos(id, "-");
    sButton = substr(id, 0, iPos);
    switch(sButton) {
      case "Configuration":
      // open only one Import even if two screen.
        if(sPanelName == "Import Database (local)") {
          if(dynlen(dynPatternMatch("*Import Database (local)", g_unGraphicalFrame_childOpenedPanel)) <= 0) {
            unGraphicalFrame_ChildPanelOn(sFileName, sPanelName,
                           makeDynString("$sModuleName:"+myModuleName(),"$sPanelName:"+myPanelName()),
                           UN_BASE_PANEL_LEFT, UN_BASE_PANEL_TOP);
            dynAppend(g_unGraphicalFrame_childOpenedPanel, myModuleName()+"/"+_unGraphicalFrame_getModuleId()+sPanelName);
            dynUnique(g_unGraphicalFrame_childOpenedPanel);
          }
          else {
            unGraphicalFrame_ChildPanelOnCentralModal("vision/unicosObject/General/MessageWait.pnl",
            				myPanelName()+"/Info",
            				makeDynString("$1:Import Database panel already open"));
          }
        }
        else {
          unGraphicalFrame_ChildPanelOn(sFileName, sPanelName,
                       makeDynString("$sModuleName:"+myModuleName(),"$sPanelName:"+myPanelName()),
                       UN_BASE_PANEL_LEFT, UN_BASE_PANEL_TOP);
          dynAppend(g_unGraphicalFrame_childOpenedPanel, myModuleName()+"/"+_unGraphicalFrame_getModuleId()+sPanelName);
          dynUnique(g_unGraphicalFrame_childOpenedPanel);
        }
        break;
      case "Menu":
        switch(sPanelName) {
          case "Alarm List":
            unGraphicalFrame_showAlarmList();
            break;
          case "Event List":
            unGraphicalFrame_showEventList();
            break;
          case "Object List":
            unGraphicalFrame_showObjectList();
            break;
          case "Device Ov":
            unGraphicalFrame_DeviceOv("DevOv");
            break;
          case "Device Tree Ov":
            unGraphicalFrame_DeviceOv("TreeDevOv");
            break;
          default:
            unGraphicalFrame_ChildPanelOn(sFileName, sPanelName,
                         makeDynString("$sModuleName:"+myModuleName(),"$sPanelName:"+myPanelName()),
                         UN_BASE_PANEL_LEFT, UN_BASE_PANEL_TOP);
            break;
        }
        break;
      case "Management":
      default:
        unGraphicalFrame_ChildPanelOn(sFileName, sPanelName,
                     makeDynString("$sModuleName:"+myModuleName(),"$sPanelName:"+myPanelName()),
                     UN_BASE_PANEL_LEFT, UN_BASE_PANEL_TOP);
        break;
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showAlarmList
/**
Purpose:
Show the alarm list from cascade menu, icon, button.

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName and global variable to keep panel settings
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showAlarmList()
{
  const string sAlarmPanel = unAlarmScreen_getAlarmScreenPanel();
	dyn_string parameters;
	dyn_float temp_g_AlarmListdfNumberOfLinesMax;
	dyn_string temp_g_AlarmListdsFilter;

	if (unGraphicalFrame_isPanelOpen(UN_ALERTPANEL_SYSTEM_TITLE, myModuleName()))
  {
		unGraphicalFrame_ChildPanelOnCentralModal("vision/MessageInfo1","Info",makeDynString("$1:Alarm List is already opened"));
  }
	else
  {
		if (dynlen(g_AlarmListdsFilter) == 0)
    {
			g_AlarmListdsFilter = makeDynString("");
    }

		parameters = makeDynString("$sFilter:" + g_AlarmListdsFilter[1], "$iUnAlertPanel:" + UN_ALERTPANEL_CONFIG);

		unGraphicalFrame_ChildPanelOnCentralReturn(
        sAlarmPanel,
        UN_ALERTPANEL_TITLE,
        parameters,
        temp_g_AlarmListdfNumberOfLinesMax,
        temp_g_AlarmListdsFilter
      );

   // if the panel is already opened, it is not opened again but the return variables are reseted. Temporary variable are used
    // and copied to the global variables if they have a size different from 0.

		if(dynlen(temp_g_AlarmListdsFilter) > 0)
    {
			g_AlarmListdsFilter = temp_g_AlarmListdsFilter;
      g_AlarmListdfNumberOfLinesMax = temp_g_AlarmListdfNumberOfLinesMax;
		}
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showEventList
/**
Purpose:
Show the event list from cascade menu, icon, button.

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName and global variable to keep panel settings
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showEventList()
{
	dyn_string parameters;
	dyn_float temp_g_EventListdfNumberOfLinesMax;
	dyn_string temp_g_EventListdsFilter;

	if (dynlen(g_EventListdfNumberOfLinesMax) == 0)
		{
		g_EventListdfNumberOfLinesMax = makeDynFloat(100);
		}
	if (dynlen(g_EventListdsFilter) == 0)
		{
		g_EventListdsFilter = makeDynString(" ");
		}
	parameters = makeDynString("$iLinesMax:" + g_EventListdfNumberOfLinesMax[1], "$sFilter:" + g_EventListdsFilter[1]);
	unGraphicalFrame_ChildPanelOnCentralReturn("vision/unEventList/unEventList.pnl","Event List", parameters,
				temp_g_EventListdfNumberOfLinesMax, temp_g_EventListdsFilter);
// if the panel is already opened, it is not opened again but the return variables are reseted. Temporary variable are used
// and copied to the global variables if they have a size different from 0.
	if(dynlen(temp_g_EventListdfNumberOfLinesMax) > 0) {
		g_EventListdfNumberOfLinesMax = temp_g_EventListdfNumberOfLinesMax;
		g_EventListdsFilter = temp_g_EventListdsFilter;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_showObjectList
/**
Purpose:
Show the object list from cascade menu, icon, button.

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName and global variable to keep panel settings
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_showObjectList()
{
	dyn_string parameters;
	dyn_float temp_g_ObjectListdfNumberOfLinesMax;
	dyn_string temp_g_ObjectListdsFilter;

	if (dynlen(g_ObjectListdfNumberOfLinesMax) == 0)
		{
		g_ObjectListdfNumberOfLinesMax = makeDynFloat(100);
		}
	if (dynlen(g_ObjectListdsFilter) == 0)
		{
		g_ObjectListdsFilter = makeDynString(" ");
		}
	parameters = makeDynString("$iLinesMax:" + g_ObjectListdfNumberOfLinesMax[1], "$sFilter:" + g_ObjectListdsFilter[1]);
	unGraphicalFrame_ChildPanelOnCentralReturn("vision/unObjectList/unObjectList.pnl","Object List", parameters,
				temp_g_ObjectListdfNumberOfLinesMax, temp_g_ObjectListdsFilter);
// if the panel is already opened, it is not opened again but the return variables are reseted. Temporary variable are used
// and copied to the global variables if they have a size different from 0.
	if(dynlen(temp_g_ObjectListdfNumberOfLinesMax) > 0) {
		g_ObjectListdfNumberOfLinesMax = temp_g_ObjectListdfNumberOfLinesMax;
		g_ObjectListdsFilter = temp_g_ObjectListdsFilter;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_DeviceOv
/**
Purpose:
Show the device over or device tree overview from cascade menu, icon, button.

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . global variable mapping g_m_sMenuConfigurationFile, g_m_sMenuAccessType, g_m_sMenuShapeName,
        g_m_sMenuPanelFileName and global variable to keep panel settings
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_DeviceOv(string id)
{
	string fileName, sDpPanel;
	dyn_string exceptionInfo;

	switch(id) {
		case "DevOv":
			fileName = UN_PANEL_DEVICE_OVERVIEW;
			sDpPanel=unPanel_convertPanelFileNameToDp(UN_PANEL_DEVICE_OVERVIEW);
			if(dpExists(sDpPanel)) {
				unGraphicalFrame_showUserPanel(UN_PANEL_DEVICE_OVERVIEW, "", sDpPanel);
				unGenericDpFunctions_getContextPanel(sDpPanel, fileName, exceptionInfo);
				if (dynlen(exceptionInfo) <= 0)
				{
					if(fileName != "")
						unGraphicalFrame_showContextualPanel(fileName, sDpPanel);
				}
			}
			break;
		case "TreeDevOv":
			fileName = UN_PANEL_TREE_DEVICE_OVERVIEW;
			sDpPanel=unPanel_convertPanelFileNameToDp(UN_PANEL_TREE_DEVICE_OVERVIEW);
			if(dpExists(sDpPanel)) {
				unGraphicalFrame_showUserPanel(UN_PANEL_TREE_DEVICE_OVERVIEW, "", sDpPanel);
				unGenericDpFunctions_getContextPanel(sDpPanel, fileName, exceptionInfo);
				if (dynlen(exceptionInfo) <= 0)
				{
					if(fileName != "")
						unGraphicalFrame_showContextualPanel(fileName, sDpPanel);
				}
			}
			break;
		default:
			break;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGraphicalFrame_setLoadingText
/**
Purpose:
set the log text.

	@param sText: string, input: the text to show

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . use shape: loadingText
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_setLoadingText(string sText)
{
  if(shapeExists("loadingText"))
    loadingText.text = sText;
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGraphicalFrame_setVisibleLoadingText
/**
Purpose:
set the log text visible.

	@param bVisible: bool, input: true/false

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
        . use shape: loadingText
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_setVisibleLoadingText(bool bVisible)
{
  if(shapeExists("loadingText"))
    loadingText.visible = bVisible;
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGraphicalFrame_translateHmiNewFormat
/**
Purpose:
Translate the 'unicosHMI' XML Files to the new format and the old one is renamed

        @param sFile: string, input: XML Filename

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP.
	. distributed system: yes.
*/
unGraphicalFrame_translateHmiNewFormat(string sFile)
{
file fi, fo;
string str, tFile;
int p, p1 = -1, p2 = 0;

  p = strlen(sFile);
  while ( p > 0 )
  {
    --p;
    str = substr(sFile,p,1);
    if ( str == "." ) { p2 = p; }
    if ( str == "/" ) { p1 = p; break; }
  }
  if ( p2 == 0 )
    { tFile = sFile + "_OldFormat"; }
  else
    { tFile = substr(sFile, 0, p2) + "_OldFormat" + substr(sFile, p2); }

  DebugN("XML HMI Config file to be recreated: "+sFile);
  DebugN("XML HMI Config old to be renamed to: "+tFile);

  if ( rename ( sFile , tFile ) != 0 )
  {
    DebugN("Could NOT Rename Old XML HMI ConfFile: "+sFile);
    return;
  }

  if ( ( fo = fopen ( sFile , "w" ) ) == 0 )
  {
    DebugN("Could NOT Create New XML HMI ConfFile: "+sFile);
    return;
  }
  if ( ( fi = fopen ( tFile , "r" ) ) == 0 ) return;
  fputs ( "<HMI-configuration>\n" , fo );
  while ( feof(fi) == 0 )
  {
    fgets ( str , 10000 , fi );
    if ( str != "" ) fputs ( "\t" + str , fo );
  }
  fputs ( "</HMI-configuration>\n" , fo );
  fclose ( fi );
  fclose ( fo );
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGraphicalFrame_retrieveXmlElementTags
/**
Purpose:
Return the Children's Text-Nodes as a mapping

        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
        @param tagName: string, input: the tag-Name to search for
        @param tagValues: dyn_string, input/output: the returned Tag-values

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: WXP.
	. distributed system: yes.
*/
unGraphicalFrame_retrieveXmlElementTags(int xmlDoc, int xmlTop, string tagName, dyn_string & tagValues)
{
int node;
string name;
dyn_int children;

  xmlChildNodes(xmlDoc, xmlTop, children);
  for ( int index = 1 ; index <= dynlen(children) ; ++index )
  {
    if ( ( name = xmlNodeName(xmlDoc,children[index]) ) != tagName ) continue;

    if (  ( ( node = xmlFirstChild(xmlDoc,children[index]) ) < 0 )
       || ( xmlNodeType(xmlDoc,node) != XML_TEXT_NODE ) ) continue;

    name = xmlNodeValue(xmlDoc,node);

    dynAppend ( tagValues , name );
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGraphicalFrame_loadHMI
/**
Purpose:
Load HMI.

	@param bPreview: bool, input: true=preview/false=normal
        @param exInfo: dyn_string, output, errors returned here

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_loadHMI(dyn_string &exInfo, bool bPreview=false)
{
  string sScreen, sFileName, sName;
  int iX, iY;
  string sModule;

  int xmlDoc,xmlTop,xmlNod;
  string errMsg, sFile;
  int errLine,errColumn;
  int xmlLen,xmlIdx;
  dyn_int xmlEls,xmlChn,xmlDev;
  mapping elements;

  sFile = substr(g_s_unicosHMI_previewConfig, strlen(DATA_REL_PATH) );
  sFile = getPath(DATA_REL_PATH, sFile);

  if ( ( xmlDoc = xmlDocumentFromFile(sFile,errMsg,errLine,errColumn) ) < 0 ) {
	fwException_raise(exInfo, "ERROR", "Error while reading " + sFile +
	": " + errMsg + " in line " + errLine + " character " + errColumn + ".", "");
	return;
  }

  if ( (  ( xmlTop = xmlFirstChild(xmlDoc) ) < 0 )
       || ( xmlNodeType(xmlDoc,xmlTop) != XML_ELEMENT_NODE ) ) {
	fwException_raise(exInfo, "ERROR", sFile + "Error while reading " + sFile +
	": The file has no child nodes or the node type is incorrect.", "");
	return;
  }

  if ( xmlNodeName(xmlDoc,xmlTop) != "HMI-configuration" )
  {
    if ( xmlNodeName(xmlDoc,xmlTop) != "HMI" ) {
		fwException_raise(exInfo, "ERROR", "Error while reading " + sFile +
		":  Make sure the utmost node is named 'HMI-configuration' or 'HMI'.", "");
		return;
	  }

    unGraphicalFrame_translateHmiNewFormat(sFile);

    if ( ( xmlDoc = xmlDocumentFromFile(sFile,errMsg,errLine,errColumn) ) < 0 ) {
		fwException_raise(exInfo, "ERROR", "Error while reading " + sFile +
		": " + errMsg + " in line " + errLine + " character " + errColumn + ".", "");
		return;
	  }

    if ( (  ( xmlTop = xmlFirstChild(xmlDoc) ) < 0 )
       || ( xmlNodeType(xmlDoc,xmlTop) != XML_ELEMENT_NODE ) ) {
		fwException_raise(exInfo, "ERROR", "Error while reading " + sFile +
		": The file has no child nodes or the node type is incorrect.", "");
		return;
	}
  }

  if ( xmlChildNodes(xmlDoc,xmlTop,xmlEls) < 0 ) {
	fwException_raise(exInfo, "ERROR", "Error while reading " + sFile +
	": Could not write all child nodes of the given node into the nodes' " +
	"reference parameter.", "");
	return;
  }

  xmlLen = dynlen(xmlEls);
  for ( xmlIdx = 1 ; xmlIdx <= xmlLen ; ++xmlIdx )
  {
    if (  ( ( xmlNod = xmlFirstChild(xmlDoc,xmlEls[xmlIdx]) ) < 0 )
       || ( xmlNodeType(xmlDoc,xmlNod) != XML_ELEMENT_NODE )
       || ( xmlNodeName(xmlDoc,xmlNod) != "properties" ) ) {
		fwException_raise(exInfo, "ERROR", "Error while reading " + sFile +
		": The file is missing the mandatory properties tag.", "");
		break;
	  }

    elements = unImportTree_xmlChildrenMapping(xmlDoc,xmlNod);

    sScreen = elements["screen"];
    sFileName = elements["filename"];
    sName = elements["name"];
    iX = elements["x"];
    iY = elements["y"];



    if(sFileName != "")
    {
      if(bPreview)
        sModule = UNICOSHMI_PREVIEW_MODULE_NAME;
      else
        sModule = sName;

      if (g_unicosHMI_bApplicationFilterInUse)
      {
        if (g_unicosHMI_sApplicationFilterName != "")
        {
          sName = g_unicosHMI_sApplicationFilterName;
          sModule = g_unicosHMI_sApplicationFilterName;
        }
      }
      else
      {
        sName = unBackup_getProjName();
      }

	  string modName=sModule + "_" + sScreen;
      ModuleOnWithPanel(modName, iX, iY, 0, 0, 1, 1, "", sFileName, sName, makeDynString());

	  string sAppName=unGraphicalFrame_getApplicationName();
	  // append screen number?
	  //sAppName+="_" + sScreen;

	  // wait for the module to open
	  for (int i=1;i<=10;i++) {
	  	if (isModuleOpen(modName)) {
	  		setWindowTitle(modName, sAppName);
	  		break;
	  	}
	  	delay(0,200);
	  }

    }
  }

  xmlCloseDocument(xmlDoc);
}

/** Returns the current application name

The function returns the current application name, taking into account the
"filtered applications", etc

*/
string unGraphicalFrame_getApplicationName()
{
	string sAppName="UNICOS";

	// we want to set the window title to the default frontend app or $sApplication
	// based on the code e.g. in objects/UN_GRAPHICALFRAME/unApplicationName.pnl
	string sAppDp=UN_APPLICATION_DPNAME;

	if (g_unicosHMI_bApplicationFilterInUse) {
		// Warning, the app dp can be on another system.
		dyn_string dsDps = dpNames("*:" + g_unicosHMI_sApplicationDpName + g_unicosHMI_sDefaultApplication);
		if (dynlen(dsDps) > 0 && dsDps[1]!="" && dpExists(dsDps[1])) sAppDp = dsDps[1];
	}

	// get the application name and show it
	dpGet(sAppDp + ".applicationName", sAppName);
	dyn_errClass err=getLastError();
	if (dynlen(err)) {
		DebugTN("ERROR in "+_FUNCTION_, "Could not get application name from "+sAppDp + ".applicationName",getErrorText(err));
		sAppName="UNICOS";
	}
	return sAppName;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_triggerDisplay
/**
Purpose:
Trigger the exceution of the callback handling the the tree device display

  @param iCommand: int, input:  the command to execute.
  @param sSystemName: string, input: the system name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unGraphicalFrame_triggerDisplay(int iCommand, string sSystemName)
{
  int i, len;

  len = dynlen(g_dsTreeDeviceOverview_triggerDisplayDP);
  for(i=1;i<=len;i++)
   _unTreeDeviceOverview_triggerDisplay(g_dsTreeDeviceOverview_triggerDisplayDP[i], iCommand, sSystemName);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_loadHMIXMLConfig
/**
Purpose:
Load the xml config for the HMI.

	@param sFile: string, input: xml config file
        @param exInfo: dyn_string, output, errors are returned here

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_loadHMIXMLConfig(string sFile, dyn_string &exInfo)
{
  int pos;
  int iH, iV;
  string sValue;
  float fAngle;
  int iX, iY, sizeX, sizeY;
  dyn_string dsDollar;
  string sDollar, sScreen;
  string sFileName, sPanelName;

  int xmlDoc,xmlTop,xmlNod;
  string errMsg;
  int errLine,errColumn;
  int xmlLen,xmlIdx;
  int node, child;
  dyn_int xmlEls,xmlChn,xmlDev;
  mapping elements;
  int xmlHmi = -1;

  // get the panel size
  panelSize("", iH, iV);

  // get the screen number-> must be in the module name (las 2 digits of the module name)
  sScreen = myModuleName();
  while ( ( pos = strpos(sScreen, "_") ) >= 0 )
        sScreen = substr(sScreen, pos + 1);

  sFile = substr(sFile, strlen(DATA_REL_PATH) );
  sFile = getPath(DATA_REL_PATH, sFile);

  if ( ( xmlDoc = xmlDocumentFromFile(sFile,errMsg,errLine,errColumn) ) < 0 ) return;

  if ( (  ( xmlTop = xmlFirstChild(xmlDoc) ) < 0 )
       || ( xmlNodeType(xmlDoc,xmlTop) != XML_ELEMENT_NODE ) ) return;

  if ( xmlNodeName(xmlDoc,xmlTop) != "HMI-configuration" )
  {
    if ( xmlNodeName(xmlDoc,xmlTop) != "HMI" ) return;

    unGraphicalFrame_translateHmiNewFormat(sFile);

    if ( ( xmlDoc = xmlDocumentFromFile(sFile,errMsg,errLine,errColumn) ) < 0 ) return;

    if ( (  ( xmlTop = xmlFirstChild(xmlDoc) ) < 0 )
       || ( xmlNodeType(xmlDoc,xmlTop) != XML_ELEMENT_NODE ) ) return;
  }

  if ( xmlChildNodes(xmlDoc,xmlTop,xmlEls) != 0 ) return;

  xmlLen = dynlen(xmlEls);
  for ( xmlIdx = 1 ; xmlIdx <= xmlLen ; ++xmlIdx )
  {
    if (  ( ( xmlNod = xmlFirstChild(xmlDoc,xmlEls[xmlIdx]) ) < 0 )
       || ( xmlNodeType(xmlDoc,xmlNod) != XML_ELEMENT_NODE )
       || ( xmlNodeName(xmlDoc,xmlNod) != "properties" ) ) break;

    elements = unImportTree_xmlChildrenMapping(xmlDoc,xmlNod);

    if ( sScreen == elements["screen"] )
    {
      if ( mappingHasKey(elements,"libs") )
      {
        unGraphicalFrame_retrieveXmlElementTags(xmlDoc, xmlNod, "libs", g_dsLibs);
      }
      else
      {
        elements["initFunction"] = "";
        elements["applicationUpdateFunction"] = "";
        elements["registerFunction"] = "";
        elements["waitRegisterFunction"] = "";
      }
      g_dsHMIFunctions[HMI_INIT_FUNCTION] =               elements["initFunction"];
      g_dsHMIFunctions[HMI_APPLICATION_UPDATE_FUNCTION] = elements["applicationUpdateFunction"];
      g_dsHMIFunctions[HMI_REGISTER_FUNCTION] =           elements["registerFunction"];
      g_dsHMIFunctions[HMI_WAIT_REGISTER_FUNCTION] =      elements["waitRegisterFunction"];

      xmlHmi = xmlEls[xmlIdx];

      break;
    }
  }

  if ( xmlHmi < 0 ) return;

  sizeX = elements["sizeX"];
  sizeY = elements["sizeY"];
  if ( (sizeX != iH) || (sizeY != iV) )
  {
    fwException_raise(exInfo, "ERROR", "unGraphicalFrame_loadHMIXMLConfig: wrong file size "+elements["filename"], "");

    return;
  }

  // if all correct add the graphical components
  while ( ( xmlNod = xmlNextSibling(xmlDoc,xmlNod) ) >= 0 )
  {
    if ( xmlNodeName(xmlDoc,xmlNod) != "component" ) continue;

    if ( ( node = xmlFirstChild(xmlDoc,xmlNod) ) < 0 ) continue;
    if ( xmlNodeName(xmlDoc,node) != "filename" ) continue;
    if ( ( child = xmlFirstChild(xmlDoc,node) ) < 0 ) continue;

    sFileName = xmlNodeValue(xmlDoc,child);

    if ( ( node = xmlNextSibling(xmlDoc,node) ) < 0 ) continue;
    if ( xmlNodeName(xmlDoc,node) != "properties" ) continue;

    elements = unImportTree_xmlChildrenMapping(xmlDoc,node);

    sPanelName = elements["name"];
    iX =         elements["x"];
    iY =         elements["y"];
    fAngle =   ( mappingHasKey(elements,"angle") ? elements["angle"] : 0.0 );
    sizeX =    ( mappingHasKey(elements,"sizeX") ? elements["sizeX"] :   0 );
    sizeY =    ( mappingHasKey(elements,"sizeY") ? elements["sizeY"] :   0 );

    dsDollar = makeDynString();
    if ( mappingHasKey(elements,"dollar") )
    {
      unGraphicalFrame_retrieveXmlElementTags(xmlDoc, node, "dollar", dsDollar);
    }

    unGraphicalFrame_checkAndAddHMIComponent(sFileName, sPanelName, iX, iY, fAngle, sizeX, sizeY, dsDollar, exInfo);
  }

  xmlCloseDocument(xmlDoc);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_checkAndAddHMIComponent
/**
Purpose:
Check and add a HMI component inot the HMI.

	@param sFileName: string, input: component file name
	@param sPanelName: string, input: panel name
	@param iX: int, input: X position into the HMI
	@param iY: int, input: Y position into the HMI
	@param fAngle: float, input: rotation of the component
	@param sizeX: int, input: size X in pixel
	@param sizeY: int, input: size Y in pixel
	@param dsDollar: dyn_string, input: dollar parameters of the component
        @param exInfo: dyn_string, output, errors are returned here

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_checkAndAddHMIComponent(string sFileName, string sPanelName, int iX, int iY, float fAngle,
                          int sizeX, int sizeY, dyn_string dsDollar, dyn_string &exInfo)
{
  dyn_int diSize;
  float fScaleX=1, fScaleY=1;

  // get the panel size and calculate the scale X and scale Y
  if(getFileSize(getPath(PANELS_REL_PATH, sFileName)) < 0)
    fwException_raise(exInfo, "ERROR", "unGraphicalFrame_checkAndAddHMIComponent: wrong file name "+sFileName, "");
  else {
    if(sPanelName == "")
      sPanelName = sFileName;
    if((sizeX != 0) && (sizeY != 0)) {
      diSize = getPanelSize(sFileName);
      if(dynlen(diSize) >=2) {
        fScaleX = (float)sizeX/(float)diSize[1];
        fScaleY = (float)sizeY/(float)diSize[2];
      }
    }
    addSymbol(myModuleName(), myPanelName(), sFileName, sPanelName, dsDollar, iX, iY, fAngle, fScaleX, fScaleY);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_checkInstallation
/**
Purpose:
Check the necessaray DP, libs and function, etc.


Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
void unGraphicalFrame_checkInstallation()
{
  bool bRes;
  int iRes, iLoop, iLen;
  string sTreeDevOvDp, sSystemName, sManagerNumber, sComponentName, sMinVersion;
  dyn_float dfResult;
  dyn_string dsTemp, exceptionInfo, dsResult, dsComponentInfo, dsErrors;
  dyn_dyn_string ddsComponentInfo;


  // Add wait screen
  unGraphicalFrame_setLoadingText("Loading/initialising .... Please wait ....");

  // Get device tree overview DP
  sTreeDevOvDp = unTreeDeviceOverview_getInternalTriggerName();

  // Extract the manager number from the device tree overview DP
  sManagerNumber = substr(myModuleName(), strlen(myModuleName()) - 2, strlen(myModuleName()));

  // Load libs and checks if the functions exists
  bRes = unGraphicalFrame_loadLibAndCheckFunctions(g_dsLibs, g_dsHMIFunctions);

  // If is the first manager number, launch the HMI init function
  if( sManagerNumber == "_1" )
  {
    // Call the HMI_INIT_FUNCTION
    if( bRes )
    {
      bRes = unGraphicalFrame_executeHMIFunction(g_dsHMIFunctions[HMI_INIT_FUNCTION]);
    }
  }

  // In case of error we exit the UI
  if( bRes == FALSE )
  {
    fwException_raise(exceptionInfo,
                      "ERROR",
                      "GUI not correctly initialized, please report to UNICOS support \nPlease restart the GUI",
                      "");
    sSystemName = getSystemName();
    sSystemName = substr(sSystemName, 0, strpos(sSystemName, ":"));
    if( sManagerNumber == "_1" )
    {
      unGraphicalFrame_ChildPanelOnCentralModalReturn("fwGeneral/fwExceptionDisplay.pnl",
                                                      "Exception Details # 1",
                                                      makeDynString("$asExceptionInfo:" + exceptionInfo,
                                                                    "$iListIndex:1"),
                                                      dfResult,
                                                      dsResult);
    }
    unGraphicalFrame_closeUI(TRUE);
  }


  // 1 - Get all installed components
  if( fwInstallation_getInstalledComponents(ddsComponentInfo) < 0 )
  {
    dynAppend(dsErrors, "Error getting all installed components.\nPlease get in contact with:\nicecontrols.support@cern.ch");
  }
  else
  {
    // 2 - Check all components are properly installed
    iLen = dynlen(ddsComponentInfo);
    for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
    {
      dynClear(dsTemp);

      dynAppend(dsComponentInfo, ddsComponentInfo[iLoop][1]);

      // Get if component is required
      iRes = fwInstallation_getComponentInfo (ddsComponentInfo[iLoop][1], "requiredinstalled", dsTemp);
      if( iRes != 0 )
      {
        dynAppend(dsErrors, "Error getting " + ddsComponentInfo[iLoop][1] + " component information.\nPlease get in contact with:\nicecontrols.support@cern.ch");
      }
      else
      {
        if( dynlen(dsTemp) >= 1 )
        {
          if( strtoupper(dsTemp[1]) != "TRUE" )
          {
            dynAppend(dsErrors, ddsComponentInfo[iLoop][1] + getCatStr("unGeneral", "NOTCORRECLTINSTALLED"));
          }
          else
          {
            if( ddsComponentInfo[iLoop][5] == "1" )  //  fwInstallationInfo_[COMPONENT].installationNotOK
            {
              dynAppend(dsErrors, ddsComponentInfo[iLoop][1] + getCatStr("unGeneral", "NOTCORRECLTINSTALLED"));
            }
          }
        }
      }
    }

    // Synchronize installed components
    synchronized( g_m_dsMenuItemId )
    {
      dynAppend(g_dsComponentInfo, dsComponentInfo);
      dynUnique(g_dsComponentInfo);
    }

    // 3 - Check fwInstallationTool version is at least 8.4.0
    sMinVersion = "8.4.0";
    if( fwInstallation_checkToolVersion(sMinVersion) == 0 )
    {
      dynAppend(dsErrors, "Wrong fwInstallation version (must be >= " + sMinVersion + ")");
    }

    // 4 - Check fwFSM-33.4.1 version is at least installed
    sComponentName = "fwFSM";
    sMinVersion    = "34.1.1";
    if( fwInstallation_checkInstalledComponent(sComponentName) != -1 )
    {
      if( fwInstallation_checkInstalledComponent(sComponentName, sMinVersion) != 0 )
      {
        dynAppend(dsErrors, "Wrong " + sComponentName + " version (must be >= " + sMinVersion + ")");
      }
    }

    // 5 - Check fwDeviceEditorNavigator-8.4.0 is at least installed
    sComponentName = "fwDeviceEditorNavigator";
    sMinVersion    = "8.4.0";
    if( fwInstallation_checkInstalledComponent(sComponentName) != -1 )
    {
      if( fwInstallation_checkInstalledComponent(sComponentName, sMinVersion) != 0 )
      {
        dynAppend(dsErrors, "Wrong " + sComponentName + " version (must be >= " + sMinVersion + ")");
      }
    }

    // 6 - Check that all postInstall scripts were executed
    iRes = fwInstallation_postInstallToRun(dsTemp);
    if( dynlen(dsTemp) >= 1 )
    {
      dynAppend(dsErrors, getCatStr("unGeneral", "POSINSTALLNOTEXECUTED"));
    }
  }

  // Present all errors found, before closing avoiding many UI restarts
  iLen = dynlen(dsErrors);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/MessageWarning",
                                                    "Error",
                                                    makeDynString("$1:" + dsErrors[iLoop]),
                                                    dfResult,
                                                    dsResult);

    ModuleOff(myModuleName());
  }



}// unGraphicalFrame_checkInstallation()





//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_loadLibAndCheckFunctions
/**
Purpose:
Load the libs and check that the HMI functions exits.

	@param dsLibs: dyn_string, input: list of libs
	@param dsHMIFunctions: dyn_string, input: list of HMI function

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool unGraphicalFrame_loadLibAndCheckFunctions(dyn_string dsLibs, dyn_string dsHMIFunctions)
{
  int iPos, i, len;
  string sScript;

  len = dynlen(dsLibs);
  for(i=1;i<=len;i++)
  {
    sScript += "#uses \""+dsLibs[i]+"\"\n";
  }
  sScript += "main() {}";

  execScript(sScript, makeDynString());
  dynUnique(dsHMIFunctions);
  iPos = dynContains(dsHMIFunctions, "");
  if(iPos>0)
    dynRemove(dsHMIFunctions, iPos);
  return unGraphicalFrame_checkLibLoaded(dsHMIFunctions);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_executeHMIFunction
/**
Purpose:
Execute a HMI function.

	@param sFunction: string, input: function name
	@param return value: bool, output: the result is returned here, true/false

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool unGraphicalFrame_executeHMIFunction(string sFunction)
{
  string sScript;
  bool bRes=true;

  if(sFunction != "") {
    sScript = "bool main() \n{";
    sScript += "  bool bRes = "+sFunction+"();";
    sScript += "  return bRes;\n}\n";
    evalScript(bRes, sScript, makeDynString());
  }
  return bRes;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_initHMI
/**
Purpose:
Init the HMI. register and application update function are executed only once, for the screen 1

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_initHMI()
{
  dyn_dyn_string ddsComponentInfo;
  dyn_string exceptionInfo, dsTemp, dsFullDomain;
  int iRes, i, len;
  bool bRes, bConnected;
  string applicationDpName, sArPropDp, sError;
  dyn_float dfFloat;
  bool bRegisterTree;
  string sTreeDevOvDp=unTreeDeviceOverview_getInternalTriggerName();
  bool bDoNotAdAlertRow;
  dyn_string exInfo;
  dyn_float df;
  dyn_string ds, dsEmail;
  string sSystemName;

  // initialize the g_bUnGraphicalFrame_RDB
  dpGet("_DataManager.UseRDBArchive", g_bUnGraphicalFrame_RDB);
  // get the list of installed device type
  unGenericDpFunctions_getUnicosObjects(g_dsDeviceType);

  sError = substr(myModuleName(), strlen(myModuleName()) - 2, strlen(myModuleName()));
  if(sError == "_1")
// start TreeDevRegistration
     bRegisterTree = true;
  sError = "";
  if(bRegisterTree) { // load all access control domain, tree icons, etc. only once
      dpConnect("unGraphicalFrame_domainCB", "_Areas.UserName");
      unTrendTree_loadImage("", "", exceptionInfo);
      unWindowTree_loadImage("", "", exceptionInfo);
   }

// create the internal AES DP used by the alarm screen. Bug in some case when the computer is slow.
  sArPropDp = "_AESPropertiesRT_"+myManNum()+"_"+myModuleName()+"_Top";
  if(!dpExists(sArPropDp))
   dpCreate(sArPropDp, "_AESProperties");
  sArPropDp = "_AESPropertiesRT_"+myManNum()+"_"+myModuleName()+"_Bot";
  if(!dpExists(sArPropDp))
   dpCreate(sArPropDp, "_AESProperties");

// create the internal AES DP used by the alarm screen in faceplate. Bug in some case when the computer is slow.
  sArPropDp = "_AESPropertiesRT_"+myManNum()+"_Unicos_Faceplate_Popup_"+myManNum()+"_Alerts_Top";
  if(!dpExists(sArPropDp))
   dpCreate(sArPropDp, "_AESProperties");
  sArPropDp = "_AESPropertiesRT_"+myManNum()+"_Unicos_Faceplate_Popup_"+myManNum()+"_Alerts_Bot";
  if(!dpExists(sArPropDp))
   dpCreate(sArPropDp, "_AESProperties");

  // create the TreeDevOv trigger, graphical frame trigger
  unConfigGenericFunctions_createDp(sTreeDevOvDp, DEVUN_DPTYPE, exceptionInfo);
  unConfigGenericFunctions_createDp(unGraphicalFrame_getInternalTriggerName(), GRAPHICALFRAME_DPTYPE, exceptionInfo);
  // setup call back for the user panel.
  unGraphicalFrame_initUserPanel();

  // load the device type config in memory
  unGenericObject_InitializeObjects(exceptionInfo);

  // set up call back for autologout user list
  dpConnect("unGraphicalFrame_autologoutUser", UN_APPLICATION_DPNAME+".autologoutUsers");

  // 1. Get the DP name of the application DP and get the statup panel, if dp cannot be found then forget it.
  if(dpExists(UN_APPLICATION_DPNAME))
  {
   iRes = dpGet(UN_APPLICATION_DPNAME+".manRegTimeOut", g_unGraphicalFrame_manRegTimeOut);
   if(iRes < 0)
   g_unGraphicalFrame_manRegTimeOut = (unsigned)0;

   dpConnect("unGraphicalFrame_manRegCB", UN_APPLICATION_DPNAME+".manRegTimeOut");
  }

  dpGet("_Ui_"+myManNum()+".Inactivity.WarningTimeout", g_iWarningTime,
    "_Ui_"+myManNum()+".Inactivity.CommitTimeout", g_iCommitTime);

  // set up call back for autologout
  dpConnect("unGraphicalFrame_InactivityCB", false, "_Ui_"+myManNum()+".Inactivity.Warning",
      "_Ui_"+myManNum()+".Inactivity.Commit",
      "_Ui_"+myManNum()+".Inactivity.WarningTimeout",
      "_Ui_"+myManNum()+".Inactivity.CommitTimeout");

  // loat in memory the list of front-end device type
  unGenericDpFunctions_getFrontEndDeviceType(g_dsConfiguredFrontEndType);

  // first level initialization done
  g_b_unicosHMI_init1 = true;

// start TreeDevRegistration
  if(bRegisterTree)
  {
  // connect to the update dev list dpe
    if(g_dsHMIFunctions[HMI_APPLICATION_UPDATE_FUNCTION] != "")
      dpConnect(g_dsHMIFunctions[HMI_APPLICATION_UPDATE_FUNCTION], false, "_unApplication.deviceList.command", "_unApplication.deviceList.systemName");
  // execute the register HMI function
    if(g_dsHMIFunctions[HMI_REGISTER_FUNCTION] != "")
      startThread(g_dsHMIFunctions[HMI_REGISTER_FUNCTION]);
  }

  // execute the wait register HMI function
  bRes = unGraphicalFrame_executeHMIFunction(g_dsHMIFunctions[HMI_WAIT_REGISTER_FUNCTION]);
  unGraphicalFrame_setVisibleLoadingText(false);

  // initialization done
  g_b_unicosHMI_initEnd = true;
  if(bRegisterTree) {
  // setup callback for menu and file access control
    unGenericDpFunctionsHMI_setCallBack_user("unGraphicalFrame_menuUserCB", iRes, exceptionInfo);
    dpConnect("unGraphicalFrame_menuConfigurationCB", "_unApplication.menu.menuFileAccessControl",
              "_unApplication.menu.menuConfiguration");
  }
  // Init the application filter if there is one
  unApplicationFilter_initFilterCallback();

  if(dynlen(exceptionInfo)>0) {
   unMessageText_sendException(getSystemName(), myManNum(), "HMI initialisation", "user", "*", exceptionInfo);
  }
// handle here any error while if the sendmessage failed
  if(dynlen(exceptionInfo)>0) {
   fwExceptionHandling_display(exceptionInfo);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_domainCB
/**
Purpose:
Callback function on the list of access control domain, , keep the list of domain in a global var.

	@param sDp: string, input: dp name
	@param sValue: string, input: trigger

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unGraphicalFrame_domainCB(string sDp, string sValue)
{
  dyn_string dsFullDomain, exceptionInfo;
  fwAccessControl_getAllDomains(g_dsDomainAccessControl, dsFullDomain, exceptionInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_autologoutUser
/**
Purpose:
Callback function on the list of autologout user, keep the list in a global var.

	@param sDp: string, input: dp name
	@param dsAutologoutUsers: dyn_string, input: list of autologout user

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unGraphicalFrame_autologoutUser(string sDp, dyn_string dsAutologoutUsers)
{
  g_dsAutologoutUsers = dsAutologoutUsers;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_InactivityCB
/**
Purpose:
Callback function trigger when the autologout warning or commit warining is triggered.
Logout and close warning only if Import not started (because otherwise no rigth to create DPs) and if the current user in the list of autologout user

	@param sDp1: string, input: dp name
	@param bWarning: bool, input: autologout warning
	@param sDp2: string, input: dp name
	@param bCommit: bool, input: autologout commit
	@param sDp3: string, input: dp name
	@param iWarningTime: int, input: autologout warning timeout
	@param sDp4: string, input: dp name
	@param iCommitTime: int, input: autologout commit timeout

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unGraphicalFrame_InactivityCB(string sDp1, bool bWarning, string sDp2, bool bCommit,
       string sDp3, int iWarningTime, string sDp4, int iCommitTime)
{
  int iRemainingTime;
  bool bAutologout;
  string userFullName, description;
  int userId;
  bool enabled;
  dyn_string groupNames, exceptionInfo;

  // keep the warning and commit time in global var, calculate the remaining time before autologout
  g_iWarningTime = iWarningTime;
  g_iCommitTime = iCommitTime;
  iRemainingTime = (g_iCommitTime - g_iWarningTime)/60;

  // do nothing if the current user is not is the list of autologout user
  if(dynlen(g_dsAutologoutUsers) <= 0)
   bAutologout = true;
  else {
   if(g_dsAutologoutUsers[1] == "*")
     bAutologout = true;
   else
   {
      fwAccessControl_getUser (getUserName(), userFullName, description, userId, enabled, groupNames, exceptionInfo);
      if(dynlen(dynIntersect(g_dsAutologoutUsers, groupNames)) > 0)
         bAutologout = true;
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "HMI unGraphicalFrame_InactivityCB", "unGraphicalFrame_InactivityCB", g_dsAutologoutUsers, groupNames, dynIntersect(g_dsAutologoutUsers, groupNames));
   }
  }
  if(bCommit) {
 // logout and close warning only if Import not started (because otherwise no rigth to create DPs)
   if(dynlen(dynPatternMatch("*Import * (local)", g_unGraphicalFrame_childOpenedPanel)) <= 0) {
     if(unGraphicalFrame_isPanelOpen(myPanelName()+"/Info")) {
       unGraphicalFrame_PanelOffPanel(myPanelName()+"/Info");
     }
     if(bAutologout)
      fwAccessControl_logout();
   }
  }
  else {
   if(bWarning) {
  // warning autologout in x min only if Import not started
  // open panel
     if(dynlen(dynPatternMatch("*Import * (local)", g_unGraphicalFrame_childOpenedPanel)) <= 0) {
      if(bAutologout)
       unGraphicalFrame_ChildPanelOnCentralModal("vision/MessageInfo1",
                 myPanelName()+"/Info",
                 makeDynString("$1:Autologout in "+iRemainingTime+" minute(s)"));
     }
   }
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "HMI unGraphicalFrame_InactivityCB", "unGraphicalFrame_InactivityCB", sDp1, bWarning, bCommit, iRemainingTime, g_dsAutologoutUsers, getUserName(), bAutologout, g_unGraphicalFrame_childOpenedPanel);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_manRegCB
/**
Purpose:
Callback function on the man reg timeout value, keep the value in a global var.

	@param sDp: string, input: dp name
	@param value: int, input: manreg timeout value

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unGraphicalFrame_manRegCB(string sdpe, int value)
{
  g_unGraphicalFrame_manRegTimeOut = (unsigned)value;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_closeUI
/**
Purpose:
Close the UI

	@param bExit: bool, input: true=exit requested/false=just close the module

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_closeUI(bool bExit)
{
  if(bExit)
  {
    dyn_string dsVisions = getVisionNames();
    for(int i =  dynlen(dsVisions); i >= 1; i--)
    {
	  if(isModuleOpen(dsVisions[i]))
		ModuleOff(dsVisions[i]);
    }
    fwAccessControl_logout();    // PG, 20160721, replace STD_LogoutCurrentUser() with fwAccessControl function; see also FWAC-462
    dpSetWait("_Managers.Exit", convManIdToInt(UI_MAN, myManNum()));
  }

  g_bExitRequested = true;
  ModuleOff(myModuleName());
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_isCtrlScriptRunning
/**
Purpose:
check if the corresponding scripts are started

@param sSystemName: string, input: the systemName
@param diCtrlMan: dyn_int, input: the list of CTRL manager number started
@param dsCtrlScriptToCheck: dyn_string, input: the list of CTRL script name to check;
       passing an empty list will check for default scripts executed by unScripts.lst

For each of requested script names a check is made whether a running script is found
with specified path name, or with any subfolder name; for instance a chech for
"unSelectDeselect.ctl" will yield OK if either "unSelectDeselect.ctl"
or "unSelectDeselect/unSelectDeselect.ctl" is running.

Usage: Internal
PVSS manager usage: UI (WCCOAui)
*/
bool unGraphicalFrame_isCtrlScriptRunning(string sSystemName, dyn_int diCtrlMan, dyn_string dsCtrlScriptToCheck=makeDynString())
{

  if(dynlen(dsCtrlScriptToCheck) <=0) {
    dsCtrlScriptToCheck = makeDynString("unSelectDeselect.ctl", "unDistributedControl.ctl", "unSystemIntegrity.ctl",
                                        "unSendMail.ctl", "scheduler.ctc", "unDeviceSet.ctl");
  }

  dyn_string allRunningCtrlScripts;

  for (int i=1;i<=dynlen(diCtrlMan);i++) {
    dyn_string exceptionInfo;
    dyn_string runningScripts = fwManager_getScriptsRunByCtrlMan(sSystemName, diCtrlMan[i], exceptionInfo);
    if (dynlen(exceptionInfo)) {
      // IS-1790: avoid popup messages - log the exceptions instead...
      DebugTN(exceptionInfo[1]+" in "+__FUNCTION__,exceptionInfo[2]);
      continue; // keep checking the other scripts
    }
    dynAppend(allRunningCtrlScripts, runningScripts);
  }

	for(int i=1;i<=dynlen(dsCtrlScriptToCheck);i++) {
		if (dynContains(allRunningCtrlScripts, dsCtrlScriptToCheck[i]) <=0 ) {
			// also check for same script name yet in a subfolder,
			if (dynlen(dynPatternMatch("*/"+dsCtrlScriptToCheck[i],allRunningCtrlScripts))<1) return false;
		}
	}

  return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// !!!!!!!!!!!!!! function to remove when case CT534479 solved

// unGraphicalFrame_ChildPanelOnCentralModal
/**
Purpose:
Equivalent ChildPanelOnCentralModal.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOnCentralModal(string sFileName, string sPanelName, dyn_string dsParameter)
{
  ChildPanelOnCentralModal(sFileName, _unGraphicalFrame_getModuleId()+sPanelName, dsParameter);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_ChildPanelOnCentral
/**
Purpose:
Equivalent ChildPanelOnCentral.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOnCentral(string sFileName, string sPanelName, dyn_string dsParameter)
{
  ChildPanelOnCentral(sFileName, _unGraphicalFrame_getModuleId()+sPanelName, dsParameter);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGraphicalFrame_getModuleId
/**
Purpose:
Get module id.

	@param return: string, module id

Usage: Internal

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
string _unGraphicalFrame_getModuleId()
{
  string sModNbr;
  dyn_string ds = strsplit(myModuleName(), ";");

  sModNbr = substr(ds[1], strlen(ds[1]) - 1, strlen(ds[1]));
  sModNbr += " - ";
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "_unGraphicalFrame", "_unGraphicalFrame_getModuleId", sModNbr, myModuleName());

  return sModNbr;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_isPanelOpen
/**
Purpose:
Equivalent isPanelOpen.

	@param sPanelName: string, input: panel name
	@param sModuleName: string, input: module name
	@param return: bool, output: true=all panel opened/false=panel not opened

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
bool unGraphicalFrame_isPanelOpen(string sPanelName, string sModuleName = "")
{
  string sOpenModule = sModuleName;
  bool bOpen;

  if(sOpenModule == "")
    sOpenModule = myModuleName();
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "_unGraphicalFrame", "unGraphicalFrame_isPanelOpen", sPanelName, sModuleName, sOpenModule);
  return isPanelOpen(_unGraphicalFrame_getModuleId()+sPanelName, sOpenModule);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_ChildPanelOn
/**
Purpose:
Equivalent ChildPanelOn.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter
	@param xPos: int, input: x coordinate
	@param yPos: int, input: y coordinate

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOn(string sFileName, string sPanelName, dyn_string dsParameter, int xPos, int yPos)
{
  ChildPanelOn(sFileName, _unGraphicalFrame_getModuleId()+sPanelName, dsParameter, xPos, yPos);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_ChildPanelOnCentralModalReturn
/**
Purpose:
Equivalent ChildPanelOnCentralModalReturn.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter
	@param dfReturn: dyn_float, output: result
	@param dsReturn: dyn_string, output: result

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOnCentralModalReturn(string sFileName, string sPanelName, dyn_string dsParameter, dyn_float &dfReturn, dyn_string &dsReturn)
{
  ChildPanelOnCentralModalReturn(sFileName, _unGraphicalFrame_getModuleId()+sPanelName, dsParameter, dfReturn, dsReturn);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_ChildPanelOnCentralReturn
/**
Purpose:
Equivalent ChildPanelOnCentralReturn.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter
	@param dfReturn: dyn_float, output: result
	@param dsReturn: dyn_string, output: result

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOnCentralReturn(string sFileName, string sPanelName, dyn_string dsParameter, dyn_float &dfReturn, dyn_string &dsReturn)
{
  ChildPanelOnCentralReturn(sFileName, _unGraphicalFrame_getModuleId()+sPanelName, dsParameter, dfReturn, dsReturn);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_ChildPanelOnModal
/**
Purpose:
Equivalent ChildPanelOnModal.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter
	@param xPos: int, input: x coordinate
	@param yPos: int, input: y coordinate

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOnModal(string sFileName, string sPanelName, dyn_string dsParameter, int xPos, int yPos)
{
  ChildPanelOnModal(sFileName, _unGraphicalFrame_getModuleId()+sPanelName, dsParameter, xPos, yPos);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_ChildPanelOnReturn
/**
Purpose:
Equivalent ChildPanelOnReturn.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter
	@param xPos: int, input: x coordinate
	@param yPos: int, input: y coordinate
	@param dfReturn: dyn_float, output: result
	@param dsReturn: dyn_string, output: result

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOnReturn(string sFileName, string sPanelName, dyn_string dsParameter, int xPos, int yPos, dyn_float &dfReturn, dyn_string &dsReturn)
{
  ChildPanelOnReturn(sFileName, _unGraphicalFrame_getModuleId()+sPanelName, dsParameter, xPos, yPos, dfReturn, dsReturn);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_PanelOffPanel
/**
Purpose:
Equivalent PanelOffPanel.

	@param sPanelName: string, input: panel name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_PanelOffPanel(string sPanelName)
{
  PanelOffPanel(_unGraphicalFrame_getModuleId()+sPanelName);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_ChildPanelOnModalReturn
/**
Purpose:
Equivalent ChildPanelOnModalReturn.

	@param sFileName: string, input: file name
	@param sPanelName: string, input: panel name
	@param dsParameter: dyn_string, input: dollar parameter
	@param xPos: int, input: x coordinate
	@param yPos: int, input: y coordinate
	@param dfReturn: dyn_float, output: result
	@param dsReturn: dyn_string, output: result

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.6
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unGraphicalFrame_ChildPanelOnModalReturn(string sFileName, string sPanelName, dyn_string dsParameter, int xPos, int yPos, dyn_float &dfReturn, dyn_string &dsReturn)
{
  ChildPanelOnModalReturn(sFileName,_unGraphicalFrame_getModuleId()+sPanelName, dsParameter,xPos, yPos, dfReturn, dsReturn);
}

// !!!!!!!!!!!!!! end function to remove when case CT534479 solved

//---------------------------------------------------------------------------------------------------------------------------------------

//@}
