#uses "unImportDeviceDeprecated.ctl"



/**@file

// unImportDevice.ctl
This library contains the functions for the import of device and front-end

@par Creation Date
  26/05/2011

@par Modification History

  26/01/2017 Jean-Charles Tournier
  - @jira{IS-1855} Fix the function unImportDevice_checkCtrl which did not report correctly the state of the manager running unicos_scripts.lst

  15/08/2012 Marco Boccioli
  - @jira(IS-1371) Allow to import without delete after an address shift.
  Modified unImportDevice_importFile(): added pre-scan of existing devices that match the devices in the file. Disable the address of all those devices.

  15/08/2012 Marco Boccioli
  - @jira{IS-792}: improve documentation

  15/08/2011 Herve
  - @jira{IS-591}: during import phase, no 'bad alias' error for a device with an existing alias and device link

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  Herve Milcent (EN-ICE)
*/


// value archive lib for file switch
#uses "va.ctl"
#uses "dpGroups.ctl"
#uses "fwAccessControl/fwAccessControl.ctc"

#uses "libunCore/unCoreDeprecated.ctl"

//@{
// constant
const string IMPORTDEVICE_EXCEPTION_INFO_SEP = "@!~";
const int IMPORTDEVICE_DEVICETYPE_ARCHIVE_BOOL_DPE = 1;
const int IMPORTDEVICE_DEVICETYPE_ARCHIVE_ANA_DPE = 2;
const int IMPORTDEVICE_DEVICETYPE_ARCHIVE_EVT_DPE = 3;
const int IMPORTDEVICE_RDB_MAX_DP = 2000000;
const int IMPORTDEVICE_WAIT_SYSTEMINTEGRITY = 5; // time out for checking system integrity
const int IMPORTDEVICE_TIMEOUT_VALARCH_FILE_SWITCH = 60;

const int IMPORTDEVICE_DEBUG_ONLY = 1;
const int IMPORTDEVICE_DEBUG_ALL  = 2;

const string IMPORTDEVICE_CREATE_ACTION = "create";
const string IMPORTDEVICE_UPDATE_ACTION = "update";
//@} // end of constant

global string g_unImportDevice_charset="UTF-8"; // default for recoding

//@{
//functions
//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_initialize
/**
Function to initialize the fwDevice and value archive global variables. It is mandatory to call this function
  before a call to unImportDevice_checkArchive and unImportDevice_valarchFileSwitch.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL
*/
unImportDevice_initialize()
{
  fwDevice_initialize();
  initHosts();
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_valarchFileSwitch
/**
Do a file switch of a valarch archive

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sArchive  input, the archive name or archive DP name
@param exceptionInfo output, for errors
@param iTimeOut  input, maximum time in second to wait for the completion of the file switch, default value 60 sec.
*/
unImportDevice_valarchFileSwitch(string sArchive, dyn_string &exceptionInfo, int iTimeOut=IMPORTDEVICE_TIMEOUT_VALARCH_FILE_SWITCH)
{
  string sArchiveDP;
  int i, length, active;
  dyn_string archives;
  string archiveName;
  dyn_anytype diCond, diRet;
  dyn_string dsRet;
  dyn_string dsName;
  bool bTimeout;

  // if dpExist and of type _ValueArchive: use it
  // else assume it is the archive name

  if(dpExists(sArchive))
  {
    if(dpTypeName(sArchive) == "_ValueArchive")
      sArchiveDP = sArchive;
  }
  if(sArchiveDP == "")
  {
    archives = dpNames("*","_ValueArchive");
    length = dynlen(archives);
    for(i=1;(i<=length) & (sArchiveDP == "");i++)
    {
      active = 0;
      dpGet(archives[i] + ".state", active);
      if (active == 1)
      {
        archiveName = "";
        dpGet(archives[i] + ".general.arName", archiveName);
        if((archiveName != "") && (archiveName == sArchive))
          sArchiveDP = archives[i];
      }
    }
  }
  if(sArchiveDP != "")
  {
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_valarchFileSwitch", "unImportDevice_valarchFileSwitch", sArchiveDP, sArchive, iTimeOut, "-");
    dsName = makeDynString(sArchiveDP+".action.fileSwitch.progress:_original.._value");
    diCond = makeDynInt(0);
    _vaFileSwitch(sArchiveDP);
    dpWaitForValue(dsName, diCond, dsRet, diRet, iTimeOut, bTimeout);
    if((bTimeout) || (dynMax(diRet) == 1))
      fwException_raise(exceptionInfo, "ERROR", "unImportDevice_valarchFileSwitch: "+sArchive+"("+sArchiveDP+")"+" file switch failed","");;
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "unImportDevice_valarchFileSwitch: "+sArchive+" unknown","");
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkArchive
/**
Check if there is enough space in the archives forthe  front-end, _UnSystemAlarm, SystemAlarm and device, the mArchiveRequiredSpace variable contains the required space
 for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsRunningArchive  input, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS)
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7)
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
@param mArchiveRequiredSpace  output, the list of needed space in the needed archived (not all the running archives)
*/
unImportDevice_checkArchive(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix, int iDriver,
                      string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName,
                      bool bEvent16, dyn_string dsRunningArchive, int iPlcNumber, bool bCheckBeforeImport, string sFrontEndVersion,
                      mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts,
                      mapping &mArchiveRequiredSpace)
{
  if(dynlen(dsConfig) >=1)
  {
    switch(dsConfig[1])
    {
      case UN_PLC_COMMAND:
      case UN_PLC_COMMAND_EXTENDED:
        unImportDevice_checkFrontEndArchive(dsConfig, sFrontEndType, sFEName, sFEApplication, sPrefix, iDriver,
                                            bEvent16,dsRunningArchive, iPlcNumber,
                                            sBoolArchiveName, sAnalogArchiveName, sEventArchiveName,
                                            mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts,
                                            mArchiveRequiredSpace, bCheckBeforeImport, sFrontEndVersion);
        break;
      case c_unSystemAlarm_dpType:
      case UN_FESYSTEMALARM_COMMAND:
        unImportDevice_checkFrontEndSystemAlarmArchive(dsConfig, sFrontEndType, sFEName, sFEApplication, sPrefix,
                                                        dsRunningArchive,
                                                        sBoolArchiveName, sAnalogArchiveName, sEventArchiveName,
                                                        mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts,
                                                        mArchiveRequiredSpace,
                                                        bCheckBeforeImport, sFrontEndVersion);
        break;
      case UN_DELETE_COMMAND:
        // fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkArchive: "+dsConfig[1]+" unknown (or deprecated)","");
	// TODO: fwException_raise has been commented out - check if it affects any ?
        break;
      default:
        unImportDevice_checkDeviceArchive(dsConfig, sFrontEndType, sFEName, sFEApplication, sPrefix,
                                          dsRunningArchive,
                                          sBoolArchiveName, sAnalogArchiveName, sEventArchiveName,
                                          mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts,
                                          mArchiveRequiredSpace, bCheckBeforeImport, sFrontEndVersion);
        break;
    }
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkArchive: empty config","");
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkFrontEndSystemAlarmArchive
/** Check if there is enough space in the archives for a System alarm device type (_UnSystemAlarm and SystemAlarm device type),
  the mArchiveRequiredSpace variable contains the required space for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param dsArchive  input, the list of running archive
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
@param mArchiveRequiredSpace  output, the list of needed space in the needed archived (not all the running archives)
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7)
*/
unImportDevice_checkFrontEndSystemAlarmArchive(dyn_string dsConfig, string sFrontEndType, string sFEName, string sFEApplication, string sPrefix,
                                  dyn_string dsArchive,
                                  string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName,
                                  mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts,
                                  mapping &mArchiveRequiredSpace, bool bCheckBeforeImport, string sFrontEndVersion)
{
  int i, len;
  string sArchiveList;
  string sDeviceType, sDeviceNumber, sDeviceAlias, sDeviceDpName;
  dyn_string dsDeviceConfig = dsConfig;
  dyn_string dsDollar;

  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");
  dsDollar = makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType));
  while(dynlen(dsDeviceConfig) < 3)
    dynAppend(dsDeviceConfig, "");
  sDeviceType = dsDeviceConfig[1]; // Object name
  sDeviceNumber = dsDeviceConfig[2]; // Object number
  sDeviceAlias = dsDeviceConfig[3]; // Alias
  dynRemove(dsDeviceConfig, 1); // Delete first config == object type
  dynRemove(dsDeviceConfig, 1); // Delete second config == number
  switch(sDeviceType)
  {
    case c_unSystemAlarm_dpType:
      sDeviceDpName = unImportDevice_makeFESystemAlarmDpNameForCheck(PLC_PLC_pattern, sFrontEndType, sFEName, sDeviceAlias, getSystemName());
      break;
    case UN_FESYSTEMALARM_COMMAND:
      sDeviceDpName = unImportDevice_makeFESystemAlarmDpNameForCheck(FE_pattern, sFrontEndType, sFEName, sDeviceAlias, getSystemName());
      break;
  }
  _unImportDevice_checkFrontEndArchive(dsDollar, sDeviceType, unImportDevice_getFrontEndDeviceType(sFrontEndType), sDeviceDpName, sFrontEndType, dsDeviceConfig,
                                    sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkFrontEndArchive
/** Check if there is enough space in the archives for a front-end device type, the mArchiveRequiredSpace variable contains the required space
  for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsArchive  input, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS)
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
@param mArchiveRequiredSpace  output, the list of needed space in the needed archived (not all the running archives)
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7)
*/
unImportDevice_checkFrontEndArchive(dyn_string dsConfig, string sFrontEndType, string sFEName, string sFEApplication, string sPrefix, int iDriver,
                                  bool bEvent16, dyn_string dsArchive, int iPlcNumber,
                                  string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName,
                                  mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts,
                                  mapping &mArchiveRequiredSpace, bool bCheckBeforeImport, string sFrontEndVersion)
{
  int i, len;
  string sArchiveList, sCommandLine, sFrontEndDpName;
  dyn_string dsFEConfig = dsConfig;
  dyn_string dsDollar;

  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");
  dsDollar = makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType));
  sCommandLine = dsFEConfig[1];
  dynRemove(dsFEConfig, 1);
  if(sFEApplication == "COMMUNICATION")
  {
    dynInsertAt(dsFEConfig, makeDynString(iDriver, sBoolArchiveName, sAnalogArchiveName, bEvent16, sEventArchiveName), 1);
    if(sFrontEndVersion != "")
      dynInsertAt(dsFEConfig, sFrontEndVersion, 1);
    dynInsertAt(dsFEConfig, makeDynString(sFrontEndType, sFEName, sFEApplication, iPlcNumber), 1);
  }
  else
  {
    dynAppend(dsFEConfig, iDriver); // Additional parameters
    dynAppend(dsFEConfig, sBoolArchiveName);
    dynAppend(dsFEConfig, sAnalogArchiveName);
    dynAppend(dsFEConfig, bEvent16);
    dynAppend(dsFEConfig, sEventArchiveName);
  }
  if(unImportDevice_getFrontEndDeviceType(sFrontEndType) == UN_PLC_DPTYPE)
    sFrontEndDpName = c_unSystemIntegrity_UnPlc+sFEName;
  else
    sFrontEndDpName = unImportDevice_getFrontEndDeviceType(sFrontEndType)+"_"+sFEName;

  _unImportDevice_checkFrontEndArchive(dsDollar, sCommandLine, unImportDevice_getFrontEndDeviceType(sFrontEndType), sFrontEndDpName, sFrontEndType, dsFEConfig,
                                    sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
  unImportDevice_checkProxyArchive(dsDollar, unImportDevice_getFrontEndDeviceType(sFrontEndType), sFrontEndDpName, dsFEConfig,
                    sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts,
                    mArchiveRequiredSpace);
}

//------------------------------------------------------------------------------------------------------------------------
// _unImportDevice_checkFrontEndArchive
/** Check if there is enough space in the archives for a front-end and System alarm device type (_UnSystemAlarm and SystemAlarm device type) device type, the mArchiveRequiredSpace variable contains the required space
  for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsDollar  input, dollar parameter given to the evalScript
@param sCommand  input, the type of device or PLCCONFIG
@param sFrontEndDeviceType  input, the front-end device type
@param sDp  input, the dp name of the device
@param sFrontEndType  input, the front-end type
@param dsConfig  input, device or front-end config
@param sArchiveBoolName  input, the boolean archive name
@param sArchiveAnalogName  input, the analog archive name
@param sArchiveEventName  input, the event archive name
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
@param mArchiveRequiredSpace  output, the list of needed space in the needed archived (not all the running archives)
*/
_unImportDevice_checkFrontEndArchive(dyn_string dsDollar, string sCommand, string sFrontEndDeviceType, string sDp, string sFrontEndType, dyn_string dsConfig,
                                    string sArchiveBoolName, string sArchiveAnalogName, string sArchiveEventName,
                                    mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts,
                                    mapping &mArchiveRequiredSpace)
{
  dyn_string dsArchiveDp;
  string sFunction = sFrontEndDeviceType+UN_CONFIG_FRONTEND_GETARCHIVEDPFUNCTION;

// execute the script per front-end that returns the list of archiveDp: FEType_getFrontEndArchiveDp
// and assume there are all in the same archive: the bool archive
  evalScript(dsArchiveDp,"dyn_string main(string sCommand, string sDp, string sFrontEndType) {" +
                "  dyn_string dsResult;"
                "    if(isFunctionDefined(\"" + sFunction + "\"))" +
                "    { " +
                 "      " + sFunction + "(sCommand, sDp, sFrontEndType, dsResult);" +
                "    }" +
                "    return dsResult;" +
                "}",
                 dsDollar, sCommand, sDp, sFrontEndType);

// check bool archive
  _unImportDevice_checkDpeInArchive(sArchiveBoolName, dsArchiveDp, mArchiveDp, mArchiveMaxElts,
                                  mArchiveRequiredSpace);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_checkFrontEndArchive", "unImportDevice_checkFrontEndArchive", sFrontEndDeviceType, sCommand, sDp, mArchiveRequiredSpace, "-");
}

//------------------------------------------------------------------------------------------------------------------------
// _unImportDevice_checkDpeInArchive
/**
Check if a DPE is in alread in an archive and set the mArchiveRequiredSpace accordingly, the mArchiveRequiredSpace variable contains the required space
for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sArchiveName  input, the name of the archvie
@param dsArchiveDp  input, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE of the archive
@param mArchiveMaxElts  output, the list of max element per running archive
@param mArchiveRequiredSpace  output, the list of needed space in the needed archived (not all the running archives)
*/
_unImportDevice_checkDpeInArchive(string sArchiveName, dyn_string dsArchiveDp, mapping &mArchiveDp, mapping &mArchiveMaxElts,
                                  mapping &mArchiveRequiredSpace)
{
  if (!mappingHasKey(mArchiveRequiredSpace, sArchiveName)) {
    mArchiveRequiredSpace[sArchiveName] = 0;
  }

  for(int i = 1; i <= dynlen(dsArchiveDp); i++) {
    if (strpos(dsArchiveDp[i], ":") < 0) {
      dsArchiveDp[i] = getSystemName()+dsArchiveDp[i];
    }
    if (dynContains(mArchiveDp[sArchiveName], dsArchiveDp[i]) == 0) {
      mArchiveRequiredSpace[sArchiveName]++;
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "_unImportDevice_checkDpeInArchive", "_unImportDevice_checkDpeInArchive", "new elem in archive", sArchiveName, dsArchiveDp[i]);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkDeviceArchive
/**
Check if there is enough space in the archives for a device type, the mArchiveRequiredSpace variable contains the required space
for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param dsArchive  input, the list of running archive
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
@param mArchiveRequiredSpace  output, the list of needed space in the needed archived (not all the running archives)
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7)
*/
unImportDevice_checkDeviceArchive(dyn_string dsConfig, string sFrontEndType, string sFEName, string sFEApplication, string sPrefix,
                                  dyn_string dsArchive,
                                  string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName,
                                  mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts,
                                  mapping &mArchiveRequiredSpace, bool bCheckBeforeImport, string sFrontEndVersion)
{
  string sDeviceType, sDeviceNumber, sDeviceAlias, sFunction;
  dyn_string dsFunctions, dsDeviceConfig = dsConfig, dsResult;
  string sSystemName=getSystemName(), sDeviceDpName;
  string sFrontEndDeviceType=unImportDevice_getFrontEndDeviceType(sFrontEndType);
  string sArchiveList;
  int i, len, iArchiveActive = -2;
  string sBoolDeviceArchiveName, sAnalogDeviceArchiveName, sEventDeviceArchiveName;
  string sTemp;
  dyn_string dsArchiveSplit;
  dyn_string dsDollar;

  len = dynlen(dsArchive);
  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");

  dsDollar = makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+sFrontEndDeviceType);
  while(dynlen(dsDeviceConfig) < 3)
    dynAppend(dsDeviceConfig, "");
  sDeviceType = dsDeviceConfig[1]; // Object name
  sDeviceNumber = dsDeviceConfig[2]; // Object number
  sDeviceAlias = dsDeviceConfig[3]; // Alias
  dynRemove(dsDeviceConfig, 1); // Delete first config == object type
  dynRemove(dsDeviceConfig, 1); // Delete second config == number

  sDeviceDpName = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, sDeviceType, sDeviceNumber, sSystemName);
  unImportDevice_getArchiveName(sFrontEndDeviceType, sDeviceType, dsDeviceConfig, sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, sBoolDeviceArchiveName, sAnalogDeviceArchiveName, sEventDeviceArchiveName);

  sTemp = sFrontEndDeviceType;
  if((sTemp == UN_PLC_DPTYPE) || (sTemp == S7_PLC_DPTYPE))
    sTemp = strtoupper(sDeviceType);
  else
    sTemp = sTemp+"_" + strtoupper(sDeviceType);
// check if a constant UN_CONFIG_FEType_DeviceTYPE_ARCHIVE_ACTIVE exists:
// if yes check in the config line if the value is Y or N
// if it is not existing use the default setting from the list of archived element and assume that there are archived

  evalScript(iArchiveActive,"int main(dyn_string configLine) {" +
                "  bool bArchiveActive;" +
                "  int archiveTimeFilter, iArchiveType, iSmoothProcedure;" +
                "  if (globalExists(\"UN_CONFIG_" + sTemp + "_ARCHIVE_ACTIVE\"))" +
                "    { " +
                "    unConfigGenericFunctions_getArchiving(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_" + sTemp + "_ARCHIVE_ACTIVE], bArchiveActive, archiveTimeFilter, iArchiveType, iSmoothProcedure);" +
                "    return bArchiveActive;" +
                "    }" +
                "  else" +
                "    {" +
                "    return 1;" +
                "    }}",
                dsDollar, dsDeviceConfig);

  if(iArchiveActive > 0) {
// check bool archive
    sTemp = mDeviceTypeArchive[sDeviceType][IMPORTDEVICE_DEVICETYPE_ARCHIVE_BOOL_DPE];
    dsArchiveSplit = strsplit(sTemp,";");    // Check archive bool
    len = dynlen(dsArchiveSplit);
    for(i=1;i<=len;i++)
      dsArchiveSplit[i] = sDeviceDpName + dsArchiveSplit[i];
    _unImportDevice_checkDpeInArchive(sBoolDeviceArchiveName, dsArchiveSplit, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
// check analog archive
    sTemp = mDeviceTypeArchive[sDeviceType][IMPORTDEVICE_DEVICETYPE_ARCHIVE_ANA_DPE];
    dsArchiveSplit = strsplit(sTemp,";");    // Check archive analog
    len = dynlen(dsArchiveSplit);
    for(i=1;i<=len;i++)
      dsArchiveSplit[i] = sDeviceDpName + dsArchiveSplit[i];
    _unImportDevice_checkDpeInArchive(sAnalogDeviceArchiveName, dsArchiveSplit, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
// check event archive
    sTemp = mDeviceTypeArchive[sDeviceType][IMPORTDEVICE_DEVICETYPE_ARCHIVE_EVT_DPE];
    dsArchiveSplit = strsplit(sTemp,";");    // Check archive event
    len = dynlen(dsArchiveSplit);
    for(i=1;i<=len;i++)
      dsArchiveSplit[i] = sDeviceDpName + dsArchiveSplit[i];
    _unImportDevice_checkDpeInArchive(sEventDeviceArchiveName, dsArchiveSplit, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_checkDeviceArchive", "unImportDevice_checkDeviceArchive", sDeviceType, sDeviceNumber, sDeviceAlias, iArchiveActive, mArchiveRequiredSpace, "-");
  unImportDevice_checkProxyArchive(dsDollar, sDeviceType, sDeviceDpName, dsDeviceConfig,
                    sBoolDeviceArchiveName, sAnalogDeviceArchiveName, sEventDeviceArchiveName,
                    mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts,
                    mArchiveRequiredSpace);
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkProxyArchive
/**
Check if there is enough space in the archives for the proxy of a front-end type or a device type (_UnSystemAlarm and SystemAlarm device type) device type, the mArchiveRequiredSpace variable contains the required space
  for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDollar  input, dollar parameter given to the evalScript
@param sDeviceType  input, the front-end device type or device type
@param sDeviceDpName  input, the dp name of the front-end device or device
@param dsConfig  input, device or front-end config
@param sArchiveBoolName  input, the boolean archive name
@param sArchiveAnalogName  input, the analog archive name
@param sArchiveEventName  input, the event archive name
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
@param mArchiveRequiredSpace  output, the list of needed space in the needed archived (not all the running archives)
*/
unImportDevice_checkProxyArchive(dyn_string dsDollar, string sDeviceType, string sDeviceDpName, dyn_string dsConfig,
                  string sArchiveBoolName, string sArchiveAnalogName, string sArchiveEventName,
                  mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts,
                  mapping &mArchiveRequiredSpace)
{
  dyn_string dsArchiveSplit;
  int iArchiveIndex;
  string sFunction;
  bool bCheckAna, bCheckBool, bCheckEvt;
  dyn_dyn_string ddsRet;

  // get proxy DP
  sFunction = sDeviceType+"_getArchiveProxyDPE";
  if(isFunctionDefined(sFunction)) {
    evalScript(ddsRet, "dyn_dyn_string main(dyn_string dsConfig) { dyn_dyn_string ddsRet="+sFunction+"(dsConfig); return ddsRet;}", dsDollar, dsConfig);
//    DebugN(ddsRet);
    if(dynlen(ddsRet) >= UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH) {
      bCheckBool =(dynlen(ddsRet[UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL]) > 0);
      bCheckAna =(dynlen(ddsRet[UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG]) > 0);
      bCheckEvt =(dynlen(ddsRet[UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT]) > 0);
    }
  }
  if(bCheckBool)
  {
    dsArchiveSplit = ddsRet[UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL];
    _unImportDevice_checkDpeInArchive(sArchiveBoolName, dsArchiveSplit, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
  }
  // check analog archive for proxy
  if(bCheckAna)
  {
    dsArchiveSplit = ddsRet[UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG];
    _unImportDevice_checkDpeInArchive(sArchiveAnalogName, dsArchiveSplit, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
  }
  if(bCheckEvt) {
    dsArchiveSplit = ddsRet[UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT];
    _unImportDevice_checkDpeInArchive(sArchiveEventName, dsArchiveSplit, mArchiveDp, mArchiveMaxElts,
                                    mArchiveRequiredSpace);
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_checkProxyArchive", "unImportDevice_checkProxyArchive", sFunction, sDeviceType, sDeviceDpName, bCheckAna, bCheckBool, bCheckEvt, mArchiveRequiredSpace, "-");
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_getArchiveName
/**
Get the archive name of a given device config.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFrontEndType  input, the front-end device type
@param sDeviceType  input, the device type
@param configLine  input, config line
@param defaultBoolArchive, input, default bool archive name
@param defaultAnalogArchive, input, default analog archive name
@param defaultEventArchive, input, default event archive name
@param sArchiveBoolName  output, the bool archive name
@param sArchiveAnalogName  output, the analog archive name
@param sArchiveEventName  output, the event archive name
*/
void unImportDevice_getArchiveName(string sFrontEndType, string sDeviceType, dyn_string configLine, string defaultBoolArchive, string defaultAnalogArchive, string defaultEventArchive, string &sArchiveBoolName, string &sArchiveAnalogName, string &sArchiveEventName) {
    string lengthConstName = "UN_CONFIG_" + unImportDevice_getDeviceConstantRoot(sFrontEndType, sDeviceType) + "_LENGTH";
    string addLengthConstName = "UN_CONFIG_" + unImportDevice_getDeviceConstantRoot(sFrontEndType, sDeviceType) + "_ADDITIONAL_LENGTH";
    if (globalExists(lengthConstName)) {
        int configLength = unGenerateDatabase_getConstantByName(lengthConstName);
        int expectedLengthWithArchives = configLength + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH;
        if (globalExists(addLengthConstName)) expectedLengthWithArchives += unGenerateDatabase_getConstantByName(addLengthConstName);
        if (dynlen(configLine) == expectedLengthWithArchives) {
            sArchiveBoolName   = configLine[UN_CONFIG_COMMON_LENGTH + configLength + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL];
            sArchiveAnalogName = configLine[UN_CONFIG_COMMON_LENGTH + configLength + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG];
            sArchiveEventName  = configLine[UN_CONFIG_COMMON_LENGTH + configLength + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT];
        }
    }

    if (sArchiveBoolName == "")   sArchiveBoolName   = defaultBoolArchive;
    if (sArchiveAnalogName == "") sArchiveAnalogName = defaultAnalogArchive;
    if (sArchiveEventName == "")  sArchiveEventName  = defaultEventArchive;
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_getArchiveName", sDeviceType, sArchiveBoolName, sArchiveAnalogName, sArchiveEventName, "-");
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_getArchiveConfig
/**
Get the archive config, the mArchiveRequiredSpace variable contains the required space
  for the archived used for the front-end and device not for all the running archives.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sArchiveName  input, the archive name
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type (not used)
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
*/
unImportDevice_getArchiveConfig(string sArchiveName, mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts, mapping &mArchiveDpAvail)
{
  dyn_string dsValArchDp = dpNames("*","_ValueArchive"), dsDp, dsTemp, exceptionInfo;
  bool bFound, bTimeExpired;
  string archiveName;
  int iMaxElts, iElementCount, iElemAvailable;

  // find the sArhive name
  for (int i = 1; i <= dynlen(dsValArchDp) & !bFound; i++) {
    dpGet(dsValArchDp[i] + ".general.arName", archiveName);
    if(sArchiveName == archiveName) {
      bFound =true;
      dpGet(dsValArchDp[i] + ".files.fileName:_online.._value", dsTemp);
      dpSet(dsValArchDp[i] + ".statistics.index:_original.._value", dynlen(dsTemp));
      unGenericDpFunctions_waitForDynValue(dsValArchDp[i] + ".statistics.dpElements", 20, dsDp, bTimeExpired, exceptionInfo);
      if(bTimeExpired) {
        iMaxElts = 0;
        iElementCount = 0;
        dynClear(dsDp);
      } else {
        dpGet(dsValArchDp[i] + ".size.maxDpElGet", iMaxElts,
              dsValArchDp[i] + ".statistics.dpElementCount", iElementCount);
      }
      iElemAvailable = iMaxElts - iElementCount;
    }
  }
  if (!bFound) {
    iMaxElts = IMPORTDEVICE_RDB_MAX_DP;
    iElemAvailable = IMPORTDEVICE_RDB_MAX_DP;
  }
  mArchiveDp[sArchiveName] = dsDp;
  mArchiveMaxElts[sArchiveName] = iMaxElts;
  mArchiveDpAvail[sArchiveName] = iElemAvailable;
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_getAllArchiveConfig
/**
Get the all archive config and device type DPE list to be archived.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsRunningArchive  output, the list of running archive name
@param mDeviceTypeArchive  output, the list of DPE to archive for each device type
@param mArchiveDp  output, the list of archived DPE per running archive
@param mArchiveMaxElts  output, the list of max element per running archive
*/
unImportDevice_getAllArchiveConfig(dyn_string &dsRunningArchive, mapping &mDeviceTypeArchive, mapping &mArchiveDp, mapping &mArchiveMaxElts, mapping &mArchiveDpAvail)
{
  int i, len, j, length, k, lengthk;
  dyn_string dsArchive = dsRunningArchive, dsDeviceType, dsRet, dsArchiveElements, exceptionInfo, dsDpel;
  dyn_int diDpelType;
  string sArchiveBoolNames, sArchiveAnalogNames, sArchiveEventNames, sFunction;

  // get the running archive
  len = dynlen(dsArchive);
  if(len <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }
  // get the list of DPE already archived
  len = dynlen(dsArchive);
  for(i=1;i<=len;i++)
    unImportDevice_getArchiveConfig(dsArchive[i], mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveDpAvail);

  // get the archived DPE per device type
  unGenericDpFunctions_getUnicosObjects(dsDeviceType);
  len = dynlen(dsDeviceType);
  for(i=1;i<=len;i++)
  {
    sArchiveBoolNames = "";
    sArchiveAnalogNames = "";
    sArchiveEventNames = "";
    sFunction = dsDeviceType[i]+"_getArchiveDPE";
    if(isFunctionDefined(sFunction))
    {
        evalScript(dsRet, "dyn_string main() { dyn_string dsRet="+sFunction+"(); return dsRet;}", makeDynString());
        if(dynlen(dsRet) >= 1)
          sArchiveBoolNames = sArchiveBoolNames + dsRet[1];
        if(dynlen(dsRet) >= 2)
          sArchiveAnalogNames = sArchiveAnalogNames + dsRet[2];
        if(dynlen(dsRet) >= 3)
          sArchiveEventNames = sArchiveEventNames + dsRet[3];
    }
    else
    {
      fwDevice_getArchiveElements(dsDeviceType[i], dsArchiveElements, exceptionInfo);
      unGenericDpFunctions_getDPE(dsDeviceType[i], dsDpel, diDpelType);
      length = dynlen(dsArchiveElements);
      lengthk = dynlen(dsDpel);
      for(j=1;j<=length;j++)
      {
        for(k=1;k<=lengthk;k++) {
          if((dsDeviceType[i]+dsArchiveElements[j]) == dsDpel[k]) {
            switch(diDpelType[k]){
              case DPEL_BOOL:
              case DPEL_CHAR:
                sArchiveBoolNames = sArchiveBoolNames + dsArchiveElements[j] + ";";
                break;
              case DPEL_FLOAT:
                sArchiveAnalogNames = sArchiveAnalogNames + dsArchiveElements[j] + ";";
                break;
              case DPEL_INT:
              case DPEL_UINT:
                if (patternMatch(".ProcessInput.evStsReg*", dsArchiveElements[j]))
                {
                  sArchiveEventNames = sArchiveEventNames + dsArchiveElements[j] + ";";
                }
                else
                {
                  sArchiveAnalogNames = sArchiveAnalogNames + dsArchiveElements[j] + ";";
                }
                break;
              case DPEL_BOOL_STRUCT:
                sArchiveEventNames = sArchiveEventNames + dsArchiveElements[j] + ";";
                break;
            }
          }
        }
      }
    }
    mDeviceTypeArchive[dsDeviceType[i]] = makeDynString(sArchiveBoolNames, sArchiveAnalogNames, sArchiveEventNames);
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkFrontEndState
/**
Check system integrity for specific front-end.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFrontEndDpType  input, the front-end device type
@param sFEName  input, the front-end name
@param exceptionInfo  output, for errors
*/
unImportDevice_checkFrontEndState(string sFrontEndDpType, string sFEName, dyn_string &exceptionInfo)
{
  string sFrontEnd;
  string sSystemIntegrity;
  int iResult, iCmd, iTime;
  dyn_string dsResult;

  switch(sFrontEndDpType)
  {
    // case SIEMENS S7
    case S7_PLC_DPTYPE:
      sSystemIntegrity = UN_SYSTEM_INTEGRITY_unicosS7PLC + UN_SYSTEMINTEGRITY_EXTENSION;
      sFrontEnd = sFrontEndDpType + "_";
      break;
    // case SCHNEIDER
    case UN_PLC_DPTYPE:
      sSystemIntegrity = UN_SYSTEM_INTEGRITY_unicosPLC + UN_SYSTEMINTEGRITY_EXTENSION;
      sFrontEnd = c_unSystemIntegrity_UnPlc;
      break;
    default:
      sSystemIntegrity = sFrontEndDpType + UN_SYSTEMINTEGRITY_EXTENSION;
      sFrontEnd = sFrontEndDpType + "_";
      break;
  }

  if(!dpExists(sSystemIntegrity)) {
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","NOSYSTEMINTEGRITY"), "");
    return;
  }

  // set diagnostic
  dpSet(sSystemIntegrity+".interface.command", UN_SYSTEMINTEGRITY_DIAGNOSTIC);
  // wait few seconds and read data, if data not new error
  delay(IMPORTDEVICE_WAIT_SYSTEMINTEGRITY);
  dpGet(sSystemIntegrity+".diagnostic.commandResult",  iCmd,
        sSystemIntegrity+".diagnostic.result", dsResult,
        sSystemIntegrity+".diagnostic.result:_online.._stime", iResult);
  iTime = (int)getCurrentTime();

  if ((iTime - iResult) > (IMPORTDEVICE_WAIT_SYSTEMINTEGRITY+2)) {
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","OLDSYSTEMINTEGRITY"), "");
    return;
  }

  if(iCmd != UN_SYSTEMINTEGRITY_DIAGNOSTIC) {
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","WRONGSYSTEMINTEGRITY"), "");
    return;
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkDriver
/**
Check if the right driver for a given front end is is started.
If the driver is not started an error is returned in exceptionInfo.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param iDriverNumber  input, the driver number
@param frontEndType   input,
@param exceptionInfo  output, for errors
*/
public void unImportDevice_checkDriver(int iDriverNumber, string frontEndType, dyn_string &exceptionInfo)
{
  dyn_int dsDrivers;
  bool bOK = false;

  dpGet(getSystemName() + fwInstallationRedu_getLocalDp("_Connections")+".Driver.ManNums", dsDrivers);
  if (dynContains(dsDrivers, iDriverNumber) > 0) // Driver running
  {
    string driverDpType = "_DriverCommon";
    string driverDpName = "_Driver"+iDriverNumber;
    string driverStatisticsDpType = "_Statistics_DriverConfigs";
    string driverStatisticsDpName = "_Stat_Configs_driver_"+iDriverNumber;

    if(!dpExists(driverDpName)) {
      dpCreate(driverDpName, driverDpType);
    }
    if(!dpExists(driverStatisticsDpName)) {
      dpCreate(driverStatisticsDpName, driverStatisticsDpType);
    }

    string driverType;
    dpGet(driverDpName+".DT", driverType);
    if(driverType=="SIM") {
      // all front-ends can be imported using simulation driver
      bOK = true;
    }else{
      // when importing with production driver, drivertype must match frontend type
      frontEndType = unImportDevice_getFrontEndDeviceType(frontEndType); // convert name, so that we can use constants to compare output
      if(frontEndType==S7_PLC_DPTYPE && driverType=="S7") {
        bOK = true;
      }else if(frontEndType==UN_PLC_DPTYPE && driverType=="MODBUS"){
        bOK = true;
      } else if (frontEndType == "BACnet") {
        bOK = true;
      } else if (frontEndType == "OPCUA" && driverType == "OPCUAC") {
        bOK = true;
      }else{
        // TODO: IS-1716 treat remaining front-end types
      }
    }
  }
  if(!bOK)
    fwException_raise(exceptionInfo, "ERROR", __FUNCTION__+": driver "+iDriverNumber+", "+getCatStr("unGeneration","DEBUGDRIVERFAILED"),"");
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkCtrl
/**
Check if unSystemIntegrity.ctl script is started. If the unSystemIntegrity.ctl script is not started an error is returned in exceptionInfo.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param exceptionInfo  output, for errors
*/
unImportDevice_checkCtrl(dyn_string &exceptionInfo)
{
  dyn_int diCtrl;
  bool bResult = false;
  int i,len, iCount, iCurrent, iTime;
  string sDp;
  dyn_string dsDebugResult;

  dpGet(getSystemName() +  fwInstallationRedu_getLocalDp("_Connections")+".Ctrl.ManNums", diCtrl);
  len = dynlen(diCtrl);
  for(i=1;(i<=len) && (!bResult);i++) {
    sDp = "_CtrlDebug_CTRL_"+diCtrl[i];
    dsDebugResult = makeDynString();
    iCount = 0;
    if(dpExists(sDp)) {
      dpSetWait(sDp+".Result", makeDynString());
      dpSetWait(sDp+".Command", "info scripts"); // get the list of scripts
      while ( dynlen( dsDebugResult) < 1 && iCount < 75)  // max 15 seconds
      {
        delay(0,200);
        dpGet(sDp+".Result", dsDebugResult, sDp+".Result:_online.._stime", iTime);
        iCount ++;
      }
      iCurrent = (int)getCurrentTime();
      if((iCurrent-iTime) < 10) { // if correct time
        // IS-1855 as unSystemIntegrity.ctl now be a standalone manager, we rely on unSelectDeselect.ctl as it is only started by unicos_scripts.lst
        if(dynlen(dynPatternMatch("*unSelectDeselect.ctl*", dsDebugResult)) > 0)
          bResult = true;
      }
//DebugN(i, len, bResult, dsDebugResult, iTime, iCurrent, dynPatternMatch("*unSystemIntegrity.ctl*", dsDebugResult));
    }
  }
  if(!bResult)
    fwException_raise(exceptionInfo, "ERROR", __FUNCTION__+": "+getCatStr("unGeneration","DEBUGCTRLFAILED"),"");
}





//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_triggerUpdate
/**
Trigger the internal update of the UNICOS device list and configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL
*/
unImportDevice_triggerUpdate()
{
  // trigger the update of the aliases if a GUI is connected, should not be...
  dpSet("_unApplication.triggerUpdate", true);
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_getApplicationCharacteristics
/**
Get the application characteristics

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param bEvent16  output, true=16 bit event type/false=32 bit event type
@param sPrefix  output, the prefix of the application
*/
unImportDevice_getApplicationCharacteristics(bool &bEvent16, string &sPrefix)
{
  if(dpExists(UN_APPLICATION_DPNAME))
  {
    dpGet(UN_APPLICATION_DPNAME + ".event16", bEvent16, UN_APPLICATION_DPNAME + ".devicePrefix", sPrefix);
  }
  else
  {
    bEvent16 = false;
    sPrefix = "un";
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_getFrontEndDeviceType
/**
Get the front-end device type depending on the front-end type.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFrontEndType  input, the front-end type
@return the front-end device type
*/
string unImportDevice_getFrontEndDeviceType(string sFrontEndType)
{
  string sReturn = sFrontEndType;
  string sFeProtocol=unConfigGenericFunctions_extractFeProtocol(sFrontEndType);
  if((sFeProtocol == UN_CONFIG_PREMIUM) || (sFeProtocol == UN_CONFIG_QUANTUM) || (sFeProtocol == UN_CONFIG_UNITY) || //old version support
     (sFeProtocol == UN_CONFIG_PROTOCOL_MODBUS)) //unified protocol for modbus
    sReturn = UN_PLC_DPTYPE;
  else if ((sFeProtocol == UN_CONFIG_S7_400) || (sFeProtocol == UN_CONFIG_S7_300) ||  //old version support
     (sFeProtocol == UN_CONFIG_PROTOCOL_S7)) //unified protocol for S7
    sReturn = S7_PLC_DPTYPE;
//DebugN("unImportDevice_getFrontEndDeviceType","sFrontEndType",sFrontEndType, "sFeProtocol",sFeProtocol,"sReturn",sReturn);
  return sReturn;
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_makeUnicosDeviceDpName
/**
Make the front-end or device UNICOS DP name.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sPrefix  input, the prefix of the application
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sDeviceType  input, the device type or PLCCONFIG for a front-end
@param sDeviceNumber  input, the device number
@param sSystemName  input, the system name (localsystem by default)
@return the front-end or device UNICOS DP name
*/
string unImportDevice_makeUnicosDeviceDpName(string sPrefix, string sFEName, string sFEApplication, string sDeviceType, string sDeviceNumber, string sSystemName=getSystemName())
{
  string sResult;
  string sep=UN_DPNAME_SEPARATOR; // shorter name for separator

  switch(sDeviceType)
  {
    case UN_PLC_COMMAND:
      sResult = unImportDevice_makeFrontEndDeviceDpName(sDeviceNumber, sFEName, sSystemName);
      break;
    case c_unSystemAlarm_dpType:
    case UN_FESYSTEMALARM_COMMAND:
    case UN_DELETE_COMMAND:
    case UN_PLC_COMMAND_EXTENDED:
      break;
    default:
        // because of bug ETM-1374 we replace strexpand with sprintf, and make the code clearer
        sprintf(sResult,"%s"+        "%s"   +sep+ "%s"   +sep+ "%s"          +sep+ "%s"       +sep+ "%05d",
                        sSystemName, sPrefix,     sFEName,     sFEApplication,     sDeviceType,     (int)sDeviceNumber);
      break;
  }
  return sResult;
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_makeFrontEndDeviceDpName
/**
Make the front-end UNICOS DP name.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sSystemName  input, the system name (localsystem by default)
@return the front-end UNICOS DP name
*/
string unImportDevice_makeFrontEndDeviceDpName(string sFrontEndType, string sFEName, string sSystemName=getSystemName())
{
  string sFeProtocol = unConfigGenericFunctions_extractFeProtocol(sFrontEndType);
  string sFrontEndDpType = unConfigGenericFunctions_convertPlcTypeToLegacyType(sFeProtocol)+"_";

  if((sFeProtocol == UN_CONFIG_PREMIUM) || (sFeProtocol == UN_CONFIG_QUANTUM) || (sFeProtocol == UN_CONFIG_UNITY) || //old version support
     (sFeProtocol == UN_CONFIG_PROTOCOL_MODBUS))
    sFrontEndDpType = c_unSystemIntegrity_UnPlc;
  else if ((sFeProtocol == UN_CONFIG_S7_400) || (sFeProtocol == UN_CONFIG_S7_300) || //old version support
     (sFeProtocol == UN_CONFIG_PROTOCOL_S7))
    sFrontEndDpType = S7_PLC_DPTYPE+"_";

  return sSystemName + sFrontEndDpType + sFEName;
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_makeFESystemAlarmDpNameForCheck
/**
Make the _UnSystemAlarm UNICOS DP name used for the check phase of the _UnSystemAlarm and SystemAlarm device type.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFESystemAlarmType  input, the system alarm type
@param sFrontEndType  input, the front-end type
@param sFEName  input, the system alarm name
@param sFESystemAlarmName  input, the front-end name
@param sSystemName  input, the system name (localsystem by default)
@return the _UnSystemAlarm UNICOS DP name
*/
string unImportDevice_makeFESystemAlarmDpNameForCheck(string sFESystemAlarmType, string sFrontEndType, string sFEName, string sFESystemAlarmName, string sSystemName=getSystemName())
{
  return sSystemName + c_unSystemAlarm_dpPattern + sFESystemAlarmType + unImportDevice_makeFESystemAlarmDpName(sFrontEndType, sFEName, sFESystemAlarmName);
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_makeFESystemAlarmDpName
/**
Make the _UnSystemAlarm UNICOS DP name used for the import phase of the _UnSystemAlarm and SystemAlarm device type.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFrontEndType  input, the front-end type
@param sFEName  input, the system alarm name
@param sFESystemAlarmName  input, the front-end name
@return the _UnSystemAlarm name
*/
string unImportDevice_makeFESystemAlarmDpName(string sFrontEndType, string sFEName, string sFESystemAlarmName)
{
  string sSystemAlarm;

  if(unImportDevice_getFrontEndDeviceType(sFrontEndType) == UN_PLC_DPTYPE)
    sSystemAlarm = c_unSystemIntegrity_UnPlc;
  else
    sSystemAlarm = unImportDevice_getFrontEndDeviceType(sFrontEndType)+"_";

  return sSystemAlarm + sFEName + "_" + sFESystemAlarmName;
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_check
/**
Check the front-end, _UnSystemAlarm, SystemAlarm and device configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsRunningArchive  input, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS)
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
dyn_string unImportDevice_check(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix, int iDriver,
                      string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool bEvent16,
                      dyn_string &dsRunningArchive, int iPlcNumber,
                      bool bCheckBeforeImport = false, string sFrontEndVersion="")
{
  dyn_string dsDeleteDps;
  if(dynlen(dsConfig) >=1)
  {
    switch(dsConfig[1])
    {
      case UN_PLC_COMMAND:
      case UN_PLC_COMMAND_EXTENDED:
        unImportDevice_checkFrontEnd(dsConfig, exceptionInfo, sFrontEndType, sFEName,
                                       sFEApplication, sPrefix, iDriver,
                                       sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16,
                                       dsRunningArchive,  iPlcNumber,
                                       bCheckBeforeImport, sFrontEndVersion);
        break;
      case c_unSystemAlarm_dpType:
      case UN_FESYSTEMALARM_COMMAND:
        unImportDevice_checkFrontEndSystemAlarm(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, dsRunningArchive, bCheckBeforeImport, sFrontEndVersion);
        break;
      case UN_DELETE_COMMAND:
        unImportDevice_checkDeleteFrontEnd(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, dsRunningArchive, dsDeleteDps, bCheckBeforeImport, sFrontEndVersion);
        break;
      default:
        unImportDevice_checkDevice(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, dsRunningArchive, bCheckBeforeImport, sFrontEndVersion);
        break;
    }
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "unImportDevice_check: empty config","");

  return dsDeleteDps;
}


//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkFrontEnd
/**
Check the front-end configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsRunningArchive  output, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS)
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
unImportDevice_checkFrontEnd(dyn_string dsConfig, dyn_string &exceptionInfo,
                             string sFrontEndType, string sFEName, string sFEApplication, string sPrefix, int iDriver,
                             string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool bEvent16,
                             dyn_string &dsRunningArchive,  int iPlcNumber,
                             bool bCheckBeforeImport = false, string sFrontEndVersion="")
{
  dyn_string dsFrontEndFunctions, dsResult, dsFEConfig = dsConfig, dsDeleteDps, dsArchive = dsRunningArchive;
  string sFunction, sCommandLine;
  string sArchiveList;
  int i, len;

  len = dynlen(dsArchive);
  if(len <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }
  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");
  unGenericDpFunctions_getFrontEndFunctions(unImportDevice_getFrontEndDeviceType(sFrontEndType), dsFrontEndFunctions, exceptionInfo);
  if(dynlen(exceptionInfo) > 0) {
    dsFrontEndFunctions = makeDynString("", "", "", "", "");
  }
  sFunction = dsFrontEndFunctions[UN_FRONTEND_FUNCTION_CHECK_FRONTEND];
  if(dynlen(exceptionInfo) <=0)
  {
    sCommandLine = dsFEConfig[1];
    dynRemove(dsFEConfig, 1);
    if(sFEApplication == "COMMUNICATION")
    {
      dynInsertAt(dsFEConfig, makeDynString(iDriver, sBoolArchiveName, sAnalogArchiveName, bEvent16, sEventArchiveName), 1);
      if(sFrontEndVersion != "")
        dynInsertAt(dsFEConfig, sFrontEndVersion, 1);
      dynInsertAt(dsFEConfig, makeDynString(sFrontEndType, sFEName, sFEApplication, iPlcNumber), 1);
    }
    else
    {
      dynAppend(dsFEConfig, iDriver); // Additional parameters
      dynAppend(dsFEConfig, sBoolArchiveName);
      dynAppend(dsFEConfig, sAnalogArchiveName);
      dynAppend(dsFEConfig, bEvent16);
      dynAppend(dsFEConfig, sEventArchiveName);
    }
    evalScript(dsResult,"dyn_string main(string currentObject, dyn_string configLine, dyn_string dsDeleteDps) {" +
                      "dyn_string exceptionInfo;" +
                      "if (isFunctionDefined(\"" + sFunction + "\"))" +
                      "    {" +
                      "    " + sFunction + "(currentObject, configLine, dsDeleteDps, exceptionInfo);" +
                      "    }" +
                      "else " +
                      "    {" +
                      "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNCHECK\"),\"\");" +
                      "    }" +
                      "return exceptionInfo; }",
                      makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType)),
                      sCommandLine, dsFEConfig, dsDeleteDps);
    dynAppend(exceptionInfo, dsResult);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_checkFrontEnd", "unImportDevice_checkFrontEnd", sFunction, sCommandLine, dsFEConfig, dsDeleteDps, exceptionInfo, "-");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkFrontEndSystemAlarm
/**
Check the _UnSystemAlarm and SystemAlarm configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param dsRunningArchive  input, the list of running archive
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
unImportDevice_checkFrontEndSystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName, string sFEApplication, string sPrefix, dyn_string &dsRunningArchive, bool bCheckBeforeImport = false, string sFrontEndVersion="")
{
  string sDeviceType, sDeviceNumber, sDeviceAlias, sFunction;
  dyn_string dsFrontEndFunctions, dsDeviceConfig = dsConfig, dsResult, dsArchive = dsRunningArchive, dsDeleteDps;
  string sSystemName=getSystemName(), sDeviceDpName;
  string sArchiveList;
  int i, len;

  len = dynlen(dsArchive);
  if(len <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }

  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");

  while(dynlen(dsDeviceConfig) < 3)
    dynAppend(dsDeviceConfig, "");
  sDeviceType = dsDeviceConfig[1]; // Object name
  sDeviceNumber = dsDeviceConfig[2]; // Object number
  sDeviceAlias = dsDeviceConfig[3]; // Alias
  dynRemove(dsDeviceConfig, 1); // Delete first config == object type
  dynRemove(dsDeviceConfig, 1); // Delete second config == number
  unGenericDpFunctions_getFrontEndFunctions(unImportDevice_getFrontEndDeviceType(sFrontEndType), dsFrontEndFunctions, exceptionInfo);
  if(dynlen(exceptionInfo) > 0) {
    dsFrontEndFunctions = makeDynString("", "", "", "", "");
  }
  if(dynlen(exceptionInfo) <= 0)
    sFunction = dsFrontEndFunctions[UN_FRONTEND_FUNCTION_CHECK_FRONTEND];

  if(dynlen(exceptionInfo) <=0)
  {
    dynInsertAt(dsDeviceConfig, sFrontEndType, 1);
    switch(sDeviceType)
    {
      case c_unSystemAlarm_dpType:
        if(dynlen(dsDeviceConfig) >= UN_CONFIG_CHECKSYSTEM_ALARM_PLC_NAME)
          sDeviceDpName = unImportDevice_makeFESystemAlarmDpNameForCheck(PLC_PLC_pattern, sFrontEndType, sFEName, dsDeviceConfig[UN_CONFIG_CHECKSYSTEM_ALARM_PLC_NAME], sSystemName);
        else
          fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkFrontEndSystemAlarm: "+sDeviceType+ getCatStr("unGeneration","BADINPUTS"),"");
        break;
      case UN_FESYSTEMALARM_COMMAND:
        if(dynlen(dsDeviceConfig) >= UN_CONFIG_CHECKFESYSTEM_ALARM_NAME)
          sDeviceDpName = unImportDevice_makeFESystemAlarmDpNameForCheck(FE_pattern, sFrontEndType, sFEName, dsDeviceConfig[UN_CONFIG_CHECKFESYSTEM_ALARM_NAME], sSystemName);
        else
          fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkFrontEndSystemAlarm: "+sDeviceType+ getCatStr("unGeneration","BADINPUTS"),"");
        break;
      default:
        fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkFrontEndSystemAlarm: "+sDeviceType+ getCatStr("unGeneration","BADINPUTS"),"");
        break;
    }
    if(dynlen(exceptionInfo) <=0)
    {
      evalScript(dsResult,"dyn_string main(string currentObject, dyn_string configLine, dyn_string dsDeleteDps) {" +
                        "dyn_string exceptionInfo;" +
                        "if (isFunctionDefined(\"" + sFunction + "\"))" +
                        "    {" +
                        "    " + sFunction + "(currentObject, configLine, dsDeleteDps, exceptionInfo);" +
                        "    }" +
                        "else " +
                        "    {" +
                        "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNCHECK\"),\"\");" +
                        "    }" +
                        "return exceptionInfo; }",
                        makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType)),
                        sDeviceType, dsDeviceConfig, dsDeleteDps);
      dynAppend(exceptionInfo, dsResult);
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_checkFrontEndSystemAlarm", "unImportDevice_checkFrontEndSystemAlarm", sFunction, sDeviceType, dsDeviceConfig, dsDeleteDps, exceptionInfo, "-");
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkDevice
/**
Check the device configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param dsRunningArchive  input, the list of running archive
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
unImportDevice_checkDevice(dyn_string dsConfig,
                           dyn_string &exceptionInfo,
                           string sFrontEndType,
                           string sFEName,
                           string sFEApplication,
                           string sPrefix,
                           dyn_string &dsRunningArchive,
                           bool bCheckBeforeImport = false,
                           string sFrontEndVersion="",
                           bool bIgnoreDeviceId = false)
{
  string sDeviceType, sDeviceNumber, sDeviceAlias, sFunction;
  dyn_string dsFunctions, dsDeviceConfig = dsConfig, dsResult, dsArchive = dsRunningArchive;
  string sSystemName=getSystemName(), sDeviceDpName;
  string sArchiveList;
  int i, len;

  // TODO: WTF
  len = dynlen(dsArchive);
  if(len <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }

  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");

  while(dynlen(dsDeviceConfig) < 3)
    dynAppend(dsDeviceConfig, "");
  sDeviceType = dsDeviceConfig[1]; // Object name
  sDeviceNumber = dsDeviceConfig[2]; // Object number
  sDeviceAlias = dsDeviceConfig[3]; // Alias
  dynRemove(dsDeviceConfig, 1); // Delete first config == object type
  dynRemove(dsDeviceConfig, 1); // Delete second config == number
  unGenericObject_GetFunctions(sDeviceType, dsFunctions, exceptionInfo);
  if(dynlen(exceptionInfo) <= 0)
    sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_GENERATIONCHECK];

  //if ignore device id, then check if alias is alreatdy in system. if so, then use its dp as device dp
  if(bIgnoreDeviceId)
  {
    sDeviceDpName = _unImportDevice_makeUnicosDeviceDpNameFromAlias(sPrefix, sFEName, sFEApplication, sDeviceType, sDeviceNumber, sSystemName, sDeviceAlias);
  }
  else
  {
    sDeviceDpName = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, sDeviceType, sDeviceNumber, sSystemName);
  }
  if(dynlen(exceptionInfo) <=0)
  {
    evalScript(dsResult,"dyn_string main(dyn_string configLine) {" +
                       "dyn_string exceptionInfo;" +
                       "if (isFunctionDefined(\"" + sFunction + "\"))" +
                       "    {" +
                       "    " + sFunction + "(configLine, exceptionInfo);" +
                       "    }" +
                       "else " +
                       "    {" +
                       "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNFUNCTION\"),\"\");" +
                       "    }" +
                       "return exceptionInfo; }",
                       makeDynString("$sDeviceType:"+sDeviceType, "$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType)),
                       dsDeviceConfig);
    dynAppend(exceptionInfo, dsResult);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_checkDevice", "unImportDevice_checkDevice", sFunction, dsDeviceConfig, exceptionInfo, "-");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_delete
/**
Check and delete the front-end, _UnSystemAlarm, SystemAlarm or device.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param dsRunningArchive  input, the list of running archive
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
dyn_string unImportDevice_delete(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix,
                      dyn_string &dsRunningArchive,
                      bool bCheckBeforeImport = false, string sFrontEndVersion="")
{
  dyn_string dsDeleteDps;

  if(dynlen(dsConfig) >=1)
  {
    switch(dsConfig[1])
    {
      case UN_DELETE_COMMAND:
        unImportDevice_deleteFrontEnd(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, dsRunningArchive, bCheckBeforeImport, sFrontEndVersion);
        break;
      default:
        fwException_raise(exceptionInfo, "ERROR", "unImportDevice_delete: "+dsConfig[1]+" unknown","");
        break;
    }
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "unImportDevice_delete: empty config","");

    return dsDeleteDps;
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkDeleteFrontEnd
/**
heck the deletion the front-end, _UnSystemAlarm, SystemAlarm or device and returns the list of Dps to delete.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param dsRunningArchive  input, the list of running archive
@param dsDeleteDps  input, the list Dps to delete
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
unImportDevice_checkDeleteFrontEnd(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName, string sFEApplication, string sPrefix, dyn_string dsRunningArchive, dyn_string &dsDeleteDps, bool bCheckBeforeImport = false, string sFrontEndVersion="")
{
  dyn_string dsFrontEndFunctions, dsResult;
  string sArchiveList;

  for(int i = 1; i <= dynlen(dsRunningArchive); i++) {
    sArchiveList +=dsRunningArchive[i]+";";
  }
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");
  string deleteFEAlias = dsConfig[2];
  string deleteFEName = unGenericDpFunctions_dpAliasToName(deleteFEAlias);
  if (deleteFEName == "") return; // can't find dpe
  string deleteFEType = dpTypeName(deleteFEName);
  unImportDevice_checkFrontEndState(deleteFEType, deleteFEAlias, exceptionInfo);
  if (dynlen(exceptionInfo) > 0) return;
  unGenericDpFunctions_getFrontEndFunctions(deleteFEType, dsFrontEndFunctions, exceptionInfo);
  if (dynlen(exceptionInfo) > 0) return;

  string sFunction = dsFrontEndFunctions[UN_FRONTEND_FUNCTION_CHECK_DELETE];
  evalScript(dsResult,"dyn_string main(dyn_string configLine) {" +
                    "dyn_string exceptionInfo, dsDeleteDpsTemp;" +
                    "if (isFunctionDefined(\"" + sFunction + "\"))" +
                    "    {" +
                    "    " + sFunction + "(configLine, dsDeleteDpsTemp, exceptionInfo);" +
                    "    }" +
                    "else " +
                    "    {" +
                    "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNCHECKDEL\"),\"\");" +
                    "    }" +
                    "dynAppend(exceptionInfo, IMPORTDEVICE_EXCEPTION_INFO_SEP);" +
                    "dynAppend(exceptionInfo, dsDeleteDpsTemp);" +
                    "return exceptionInfo; }",
                    makeDynString("$sFrontEndType:"+deleteFEType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+deleteFEType),
                    dsConfig);

  for(int i = 1; i <= dynlen(dsResult); i++) {
    if (i < dynContains(dsResult, IMPORTDEVICE_EXCEPTION_INFO_SEP)) {
      dynAppend(exceptionInfo, dsResult[i]);
    }
    if (i > dynContains(dsResult, IMPORTDEVICE_EXCEPTION_INFO_SEP)) {
      dynAppend(dsDeleteDps, dsResult[i]);
    }
  }

  dynUnique(dsDeleteDps);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_checkDeleteFrontEnd", "unImportDevice_checkDeleteFrontEnd", sFunction, dsConfig, dsDeleteDps, exceptionInfo, "-");
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_deleteFrontEnd
/**
Delete the front-end, _UnSystemAlarm, SystemAlarm or device.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param dsRunningArchive  input, the list of running archive
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
// TODO: remove sFEApplication, sPrefix and sFEName
// TODO: rename as function deletes not only FE
unImportDevice_deleteFrontEnd(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix,
                      dyn_string &dsRunningArchive,
                      bool bCheckBeforeImport = false, string sFrontEndVersion="")
{
  dyn_string dsFrontEndFunctions, dsResult, dsArchive = dsRunningArchive;
  string sFunction;
  string sArchiveList;

  if(dynlen(dsArchive) <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }

  for (int i = 1; i <= dynlen(dsArchive); i++) {
    sArchiveList +=dsArchive[i]+";";
  }
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");
  unGenericDpFunctions_getFrontEndFunctions(unImportDevice_getFrontEndDeviceType(sFrontEndType), dsFrontEndFunctions, exceptionInfo);
  if(dynlen(exceptionInfo) > 0) {
    dsFrontEndFunctions = makeDynString("", "", "", "", "");
  }
  sFunction = dsFrontEndFunctions[UN_FRONTEND_FUNCTION_DELETE];
  if(dynlen(exceptionInfo) <=0) {
    evalScript(dsResult,"dyn_string main(dyn_string configLine) {" +
                      "dyn_string exceptionInfo, dsDeleteDpsTemp;" +
                      "if (isFunctionDefined(\"" + sFunction + "\"))" +
                      "    {" +
                      "    " + sFunction + "(configLine, dsDeleteDpsTemp, exceptionInfo);" +
                      "    }" +
                      "else " +
                      "    {" +
                      "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNDEL\"),\"\");" +
                      "    }" +
                      "return exceptionInfo; }",
                      makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType)),
                      dsConfig);
    dynAppend(exceptionInfo, dsResult);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_deleteFrontEnd", "unImportDevice_deleteFrontEnd", sFunction, dsConfig, exceptionInfo, "-");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_import
/**
Import the front-end, _UnSystemAlarm, SystemAlarm and device configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsRunningArchive  input, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS), default 0
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
@param bIgnoreDriverRelatedConfigs input, ignore settings which require a driver running, i.e. periphery address
*/
unImportDevice_import(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix, int iDriver,
                      string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool bEvent16,
                      dyn_string &dsRunningArchive,
                      int iPlcNumber=0, bool bCheckBeforeImport = false, string sFrontEndVersion="", bool bIgnoreDriverRelatedConfigs = false)
{
  if(dynlen(dsConfig) >=1)
  {
    switch(dsConfig[1])
    {
      case UN_PLC_COMMAND:
      case UN_PLC_COMMAND_EXTENDED:
        unImportDevice_importFrontEnd(dsConfig, exceptionInfo, sFrontEndType, sFEName,
                                      sFEApplication, sPrefix, iDriver,
                                      sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16,
                                      dsRunningArchive,
                                      iPlcNumber, bCheckBeforeImport, sFrontEndVersion, bIgnoreDriverRelatedConfigs);
        break;
      case UN_DELETE_COMMAND:
        // ignore
        //fwException_raise(exceptionInfo, "ERROR", "unImportDevice_import: "+dsConfig[1]+" unknown (or deprecated)","");
        break;
      case c_unSystemAlarm_dpType:
      case UN_FESYSTEMALARM_COMMAND:
        unImportDevice_importFrontEndSystemAlarm(dsConfig, exceptionInfo, sFrontEndType, sFEName,
                                                  sFEApplication, sPrefix, iDriver,
                                                  sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16,
                                                  dsRunningArchive,
                                                  iPlcNumber, bCheckBeforeImport, sFrontEndVersion);
        break;
      default:
        unImportDevice_importDevice(dsConfig, exceptionInfo, sFrontEndType, sFEName,
                                      sFEApplication, sPrefix, iDriver,
                                      sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16,
                                      dsRunningArchive,
                                      iPlcNumber, bCheckBeforeImport, sFrontEndVersion, bIgnoreDriverRelatedConfigs);
        break;
    }
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "unImportDevice_import: empty config","");
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_importFrontEnd
/**
Import the front-end configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsRunningArchive  output, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS), default 0
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
@param bIgnoreDriverRelatedConfigs input, ignore settings which require a driver running, i.e. periphery address
*/
unImportDevice_importFrontEnd(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix, int iDriver,
                      string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool bEvent16,
                      dyn_string &dsRunningArchive,
                      int iPlcNumber=0, bool bCheckBeforeImport = false, string sFrontEndVersion="", bool bIgnoreDriverRelatedConfigs = false)
{
  dyn_string dsFrontEndFunctions, dsResult, dsFEConfig = dsConfig, dsDeleteDps, dsArchive = dsRunningArchive;
  string sFunction, sCommandLine;
  string sArchiveList;
  int i, len;

  len = dynlen(dsArchive);
  if(len <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }

  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");
  unGenericDpFunctions_getFrontEndFunctions(unImportDevice_getFrontEndDeviceType(sFrontEndType), dsFrontEndFunctions, exceptionInfo);
  if(dynlen(exceptionInfo) > 0) {
    dsFrontEndFunctions = makeDynString("", "", "", "", "");
  }
  sFunction = dsFrontEndFunctions[UN_FRONTEND_FUNCTION_SET_FRONTEND];
  if(dynlen(exceptionInfo) <=0)
  {
    sCommandLine = dsFEConfig[1];
    dynRemove(dsFEConfig, 1);
    if(sFEApplication == "COMMUNICATION")
    {
      dynInsertAt(dsFEConfig, makeDynString(iDriver, sBoolArchiveName, sAnalogArchiveName, bEvent16, sEventArchiveName), 1);
      if(sFrontEndVersion != "")
        dynInsertAt(dsFEConfig, sFrontEndVersion, 1);
      dynInsertAt(dsFEConfig, makeDynString(sFrontEndType, sFEName, sFEApplication, iPlcNumber), 1);
    }
    else
    {
      dynAppend(dsFEConfig, iDriver); // Additional parameters
      dynAppend(dsFEConfig, sBoolArchiveName);
      dynAppend(dsFEConfig, sAnalogArchiveName);
      dynAppend(dsFEConfig, bEvent16);
      dynAppend(dsFEConfig, sEventArchiveName);
    }
    evalScript(dsResult,"dyn_string main(string currentObject, dyn_string configLine) {" +
                      "dyn_string exceptionInfo;" +
                      "if (isFunctionDefined(\"" + sFunction + "\"))" +
                      "    {" +
                      "    " + sFunction + "(currentObject, configLine, exceptionInfo);" +
                      "    }" +
                      "else " +
                      "    {" +
                      "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNSET\"),\"\");" +
                      "    }" +
                      "return exceptionInfo; }",
                      makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType), "$bIgnoreDriverRelatedConfigs:" + bIgnoreDriverRelatedConfigs),
                      sCommandLine, dsFEConfig);
    dynAppend(exceptionInfo, dsResult);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_importFrontEnd", "unImportDevice_importFrontEnd", sFunction, sCommandLine, dsFEConfig, exceptionInfo, "-");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_importFrontEndSystemAlarm
/**
Import the _UnSystemAlarm and SystemAlarm configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsRunningArchive  output, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS), default 0
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
*/
unImportDevice_importFrontEndSystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix, int iDriver,
                      string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool bEvent16,
                      dyn_string &dsRunningArchive,
                      int iPlcNumber=0, bool bCheckBeforeImport = false, string sFrontEndVersion="")
{
  string sDeviceType, sDeviceNumber, sDeviceAlias, sFunction;
  dyn_string dsFrontEndFunctions, dsDeviceConfig = dsConfig, dsResult, dsArchive = dsRunningArchive;
  string sSystemName=getSystemName(), sDeviceDpName;
  string sArchiveList;
  int i, len;
  dyn_string dsAdditionalParameters;

  len = dynlen(dsArchive);
  if(len <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }

  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");

  while(dynlen(dsDeviceConfig) < 3)
    dynAppend(dsDeviceConfig, "");
  sDeviceType = dsDeviceConfig[1]; // Object name
  sDeviceNumber = dsDeviceConfig[2]; // Object number
  sDeviceAlias = dsDeviceConfig[3]; // Alias
  dynRemove(dsDeviceConfig, 1); // Delete first config == object type
  dynRemove(dsDeviceConfig, 1); // Delete second config == number
  unGenericDpFunctions_getFrontEndFunctions(unImportDevice_getFrontEndDeviceType(sFrontEndType), dsFrontEndFunctions, exceptionInfo);
  if(dynlen(exceptionInfo) > 0) {
    dsFrontEndFunctions = makeDynString("", "", "", "", "");
  }
  if(dynlen(exceptionInfo) <= 0)
    sFunction = dsFrontEndFunctions[UN_FRONTEND_FUNCTION_SET_FRONTEND];

  if(dynlen(exceptionInfo) <=0)
  {
    switch(sDeviceType)
    {
      case c_unSystemAlarm_dpType:
        if(dynlen(dsDeviceConfig) >= UN_CONFIG_SETSYSTEM_ALARM_PLC_NAME)
          sDeviceDpName = unImportDevice_makeFESystemAlarmDpName(sFrontEndType, sFEName, dsDeviceConfig[UN_CONFIG_SETSYSTEM_ALARM_PLC_NAME], sSystemName);
        else
          fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkFrontEndSystemAlarm: "+sDeviceType+ getCatStr("unGeneration","BADINPUTS"),"");
        break;
      case UN_FESYSTEMALARM_COMMAND:
        if(dynlen(dsDeviceConfig) >= UN_CONFIG_SETFESYSTEM_ALARM_NAME)
          sDeviceDpName = unImportDevice_makeFESystemAlarmDpName(sFrontEndType, sFEName, dsDeviceConfig[UN_CONFIG_SETFESYSTEM_ALARM_NAME]);
        else
          fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkFrontEndSystemAlarm: "+sDeviceType+ getCatStr("unGeneration","BADINPUTS"),"");
        break;
      default:
        fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkFrontEndSystemAlarm: "+sDeviceType+ getCatStr("unGeneration","BADINPUTS"),"");
        break;
    }
    if(dynlen(exceptionInfo) <=0)
    {
      dynAppend(dsAdditionalParameters, sDeviceDpName); // Add device name
      dynAppend(dsAdditionalParameters, sFEName); // event 16 or 32
      dynAppend(dsAdditionalParameters, sFrontEndType); // Add PLC type
      dynAppend(dsAdditionalParameters, iPlcNumber); // Add PLC number
      dynAppend(dsAdditionalParameters, iDriver); // Add driver
      dynAppend(dsAdditionalParameters, bEvent16); // event 16 or 32
      dynAppend(dsAdditionalParameters, sBoolArchiveName); // Add bool archive name
      dynAppend(dsAdditionalParameters, sAnalogArchiveName); // Add analog archive name
      dynAppend(dsDeviceConfig, dsAdditionalParameters);
      evalScript(dsResult,"dyn_string main(string currentObject, dyn_string configLine) {" +
                         "dyn_string exceptionInfo;" +
                         "if (isFunctionDefined(\"" + sFunction + "\"))" +
                         "    {" +
                         "    " + sFunction + "(currentObject, configLine, exceptionInfo);" +
                         "    }" +
                         "else " +
                         "    {" +
                         "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNSET\"),\"\");" +
                         "    }" +
                         "return exceptionInfo; }",
                         makeDynString("$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType)),
                         sDeviceType, dsDeviceConfig);
      dynAppend(exceptionInfo, dsResult);
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_importFrontEndSystemAlarm", "unImportDevice_importFrontEndSystemAlarm", sFunction, sDeviceType, dsDeviceConfig, exceptionInfo, "-");
    }
  }
}


string _unImportDevice_makeUnicosDeviceDpNameFromAlias(string sPrefix,
                                                       string sFEName,
                                                       string sFEApplication,
                                                       string sDeviceType,
                                                       string sDeviceNumber,
                                                       string sSystemName=getSystemName(),
                                                       string sDeviceAlias)
{
    string sDeviceDpName = "";
    dyn_string dsAliasSplit = strsplit(sDeviceAlias, UN_CONFIG_GROUP_SEPARATOR);
    string sDeviceExistingAlias;
    if(dynlen(dsAliasSplit)>0)//if alias contains device links as well, remove them
    {
      sDeviceExistingAlias = dsAliasSplit[1];
      sDeviceDpName = unGenericDpFunctions_dpAliasToName(sDeviceExistingAlias);
//       unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "Import", "_unImportDevice_makeUnicosDeviceDpNameFromAlias - update device by alias: "+sDeviceExistingAlias+ " dp name: "+sDeviceDpName);
// DebugN("sDeviceAlias",sDeviceExistingAlias , "sDeviceDpName", sDeviceDpName  );
    }
    if(dpExists(sDeviceDpName))
    {
//       sDeviceDpName = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, sDeviceType, sDeviceNumber, sSystemName);
// DebugN("std sDeviceDpName", sDeviceDpName  );
      strreplace(sDeviceDpName,".","");
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "Import", "_unImportDevice_makeUnicosDeviceDpNameFromAlias - device to update: "+sDeviceExistingAlias+" to dp name: "+sDeviceDpName);
// DebugN("sDeviceExistingAlias",sDeviceExistingAlias,"sDeviceDpName", sDeviceDpName  );
    }
    else
    {
      sDeviceDpName = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, sDeviceType, sDeviceNumber, sSystemName);
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "Import", "_unImportDevice_makeUnicosDeviceDpNameFromAlias - device does not exist yet: "+sDeviceExistingAlias +" Generate new device under dp name "+sDeviceDpName);
// DebugN("DEVICE NOT FOUND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",sDeviceExistingAlias, sDeviceDpName  );
    }
    return sDeviceDpName;
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_importDevice
/**
Import the device configuration.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, device or front-end config
@param exceptionInfo output, for errors
@param sFrontEndType  input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param sPrefix  input, the prefix of the application
@param iDriver  input, the driver number
@param sBoolArchiveName  input, the boolean archive name
@param sAnalogArchiveName  input, the analog archive name
@param sEventArchiveName  input, the event archive name
@param bEvent16  input, true=16 bit event type/false=32 bit event type
@param dsRunningArchive  output, the list of running archive
@param iPlcNumber  input, the PLC number (only for MODBUS), default 0
@param bCheckBeforeImport  input, true=check the device configuration before the import/false=no check before the import, default false
@param sFrontEndVersion  input, the front-end version (only MODBUS and S7), default ""
@param bIgnoreDriverRelatedConfigs input, ignore settings which require a driver running, i.e. periphery address
*/
unImportDevice_importDevice(dyn_string dsConfig, dyn_string &exceptionInfo, string sFrontEndType, string sFEName,
                      string sFEApplication, string sPrefix, int iDriver,
                      string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool bEvent16,
                      dyn_string &dsRunningArchive,
                      int iPlcNumber=0, bool bCheckBeforeImport = false, string sFrontEndVersion="", bool bIgnoreDriverRelatedConfigs = false,
                      bool bIgnoreDeviceId = false)
{
  string sDeviceType, sDeviceNumber, sDeviceAlias, sFunction;
  dyn_string dsFunctions, dsDeviceConfig = dsConfig, dsResult, dsArchive = dsRunningArchive;
  string sSystemName=getSystemName(), sDeviceDpName;
  string sArchiveList;
  int i, len;
  dyn_string dsAdditionalParameters;
  dyn_string dsAliasSplit;
  string sDeviceExistingAlias;

  len = dynlen(dsArchive);
  if(len <=0) {
    unImportDevice_getRunningArchiveName(dsArchive);
    dsRunningArchive = dsArchive;
  }

  for(i=1;i<=len;i++)
    sArchiveList +=dsArchive[i]+";";
  sArchiveList = strrtrim(strltrim(sArchiveList, ";"), ";");

  while(dynlen(dsDeviceConfig) < 3)
    dynAppend(dsDeviceConfig, "");
  sDeviceType = dsDeviceConfig[1]; // Object name
  sDeviceNumber = dsDeviceConfig[2]; // Object number
  sDeviceAlias = dsDeviceConfig[3]; // Alias
  dynRemove(dsDeviceConfig, 1); // Delete first config == object type
  dynRemove(dsDeviceConfig, 1); // Delete second config == number
  unGenericObject_GetFunctions(sDeviceType, dsFunctions, exceptionInfo);
  if(dynlen(exceptionInfo) <= 0)
    sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_GENERATIONIMPORT];
  //if ignore device id, then check if alias is alreatdy in system. if so, then use its dp as device dp
  if(bIgnoreDeviceId)
  {
    sDeviceDpName = _unImportDevice_makeUnicosDeviceDpNameFromAlias(sPrefix, sFEName, sFEApplication, sDeviceType, sDeviceNumber, sSystemName, sDeviceAlias);
  }
  else
  {
    sDeviceDpName = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, sDeviceType, sDeviceNumber, sSystemName);
  }

  if(dynlen(exceptionInfo) <=0)
  {
    dynAppend(dsAdditionalParameters, iDriver); // Add driver
    dynAppend(dsAdditionalParameters, sFrontEndType); // Add PLC type
    dynAppend(dsAdditionalParameters, iPlcNumber); // Add PLC number
    dynAppend(dsAdditionalParameters, sBoolArchiveName); // Add bool archive name
    dynAppend(dsAdditionalParameters, sAnalogArchiveName); // Add analog archive name
    dynAppend(dsAdditionalParameters, sEventArchiveName); // Add event archive
    dynAppend(dsAdditionalParameters, sDeviceType); // Add Object
    dynAppend(dsAdditionalParameters, sDeviceDpName); // Add device name
    dynAppend(dsAdditionalParameters, bEvent16); // event 16 or 32
    dynAppend(dsAdditionalParameters, sFEName); // event 16 or 32
    dynInsertAt(dsDeviceConfig, dsAdditionalParameters, 1);
    evalScript(dsResult,"dyn_string main(dyn_string configLine) {" +
                       "dyn_string exceptionInfo;" +
                       "if (isFunctionDefined(\"" + sFunction + "\"))" +
                       "    {" +
                       "    " + sFunction + "(configLine, exceptionInfo);" +
                       "    }" +
                       "else " +
                       "    {" +
                       "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNFUNCTION\"),\"\");" +
                       "    }" +
                       "return exceptionInfo; }",
                       makeDynString("$sDeviceType:"+sDeviceType,"$sFrontEndType:"+sFrontEndType, "$dsArchiveList:"+sArchiveList, "$sFrontEndVersion:"+sFrontEndVersion, "$bCheckBeforeImport:"+bCheckBeforeImport, "$sFrontEndDeviceType:"+unImportDevice_getFrontEndDeviceType(sFrontEndType), "$bIgnoreDriverRelatedConfigs:" + bIgnoreDriverRelatedConfigs),
                       dsDeviceConfig);
    dynAppend(exceptionInfo, dsResult);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice_importDevice", "unImportDevice_importDevice", sFunction, dsDeviceConfig, exceptionInfo, "-");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_getRunningArchiveName
/**
Get all the running archive name.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsArchive output, the list of running archive name.
*/
unImportDevice_getRunningArchiveName(dyn_string &dsArchive)
{
  int i, length, active;
  dyn_string archives;
  string archiveName;

  dyn_string dsRDBClasses;
  dyn_string dsRDBGroupDps;
  dyn_string dsExceptionInfo;

  archives = dpNames("*","_ValueArchive");

  length = dynlen(archives);
  for(i=1;i<=length;i++)
  {
    active      = 0;
    archiveName = "";
    dpGet(archives[i] + ".state", active,
          archives[i] + ".general.arName", archiveName);
    if (active == 1)
    {
      if (archiveName != "")
      {
        dynAppend(dsArchive, archiveName);
      }
    }
  }

// Get all RDB archive classes
  fwArchive_getAllRDBArchiveClasses(makeDynString(getSystemName()),
                                    dsRDBClasses,
                                    dsRDBGroupDps,
                                    dsExceptionInfo);

  length = dynlen(dsRDBClasses);
  for(i=1;i<=length;i++)
  {
    dynAppend(dsArchive, dsRDBClasses[i]);
  }
  dynUnique(dsArchive);
}

//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_checkDeviceAlias
/**
Check if the device name (alias) is already used by anther DP.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param allAliasesNames  output, list of all defined and checked alias, if the device alias is added in the list if it is not in
@param sDeviceDpName  input, the device DP name
@param sDeviceNameIn  input, the device name (alias)
@param dsDeleteDps  input, list of the device DP name to be deleted
@param exceptionInfo output, for errors
*/
bool unImportDevice_checkDeviceAlias(dyn_string &allAliasesNames,
                                     string sDeviceDpName,
                                     string sDeviceNameIn,
                                     dyn_string dsDeleteDps,
                                     dyn_string& exceptionInfo,
                                     bool bIgnoreDeviceId = false)
{
    bool isDeviceNew = true;
  bool belong;
  string existingAliasDp;
  string sDeviceName;
  dyn_string dsSplit;

	dsSplit = strsplit(sDeviceNameIn, UN_CONFIG_GROUP_SEPARATOR);
	if(dynlen(dsSplit) >=1) {
    sDeviceName = dsSplit[1];
  }
  if(sDeviceName == "")
    fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkDeviceAlias: empty device name (device alias)","");
  else {
    if (dynContains(allAliasesNames, sDeviceName) > 0) // Alias is already existing
    {
      isDeviceNew = false;
      existingAliasDp = unGenericDpFunctions_dpAliasToName(sDeviceName);
      if (substr(existingAliasDp, strlen(existingAliasDp) - 1) == ".")
      {
        existingAliasDp = substr(existingAliasDp, 0, strlen(existingAliasDp) - 1);
      }
      if (existingAliasDp == "") // If existingAliasDp is empty, alias is used by another device in the file to import
      {
        fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkDeviceAlias: " + sDeviceName + getCatStr("unGeneration","DEBUGALIASUSEDINFILE"),"");
      }
      else // Alias already used by existing Dp
      {
        if (!bIgnoreDeviceId && (existingAliasDp != sDeviceDpName) && (dynContains(dsDeleteDps, existingAliasDp) <= 0))
        { // It is not an update of an existing device and existingAliasDp will not be deleted
          fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkDeviceAlias: " + sDeviceName + getCatStr("unGeneration","DEBUGALIASUSEDINPVSS"),"");
        }
        else
        {
          dynAppend(allAliasesNames, sDeviceName);
        }
      }
    }
    else // New alias
    {
        if (bIgnoreDeviceId)//standard check: check if alias already taken by another device dp name
        {
          existingAliasDp = unGenericDpFunctions_dpAliasToName(sDeviceName);
          if ((existingAliasDp == "") && (dpExists(sDeviceDpName)))
          { // alias is new but dp with that name is already taken
            fwException_raise(exceptionInfo, "ERROR", "unImportDevice_checkDeviceAlias: " + sDeviceName + getCatStr("unGeneration","DEBUGALIASUSEDINPVSS"),"");
          }
          else
          {
            dynAppend(allAliasesNames, sDeviceName);
          }
        }
        else// check ignoring the device dp name
        {
          dynAppend(allAliasesNames, sDeviceName);
        }
    }
  }
  return isDeviceNew;
}

//------------------------------------------------------------------------------------------------------------------------

//@}



// TODO: AM: throw an error instead
// TODO: Marco: move me to generic - where?
/** Check if a file is available. Function returns an error string or empty string if success.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param fileName full file path
@return result string
*/
string unImportDevice_isFileAccessible(string fileName) {
    string error = "DEBUGFILEEXISTS";
    int fileExists = access(fileName, F_OK);
    if (fileExists != 0) { // File doesn't exist
        error = "DEBUGFILEEMPTY";
    } else { // File exists
        file ff = fopen(fileName,"r");
        if (ff == 0) {                                                           // File can't be opened
            error = "DEBUGFILENOTOPENED";
        } else {
            fclose(ff);
        }
    }
    return error;
}

/** Returns next meaningful line split or empty array if end on file

It goes to the next valuable line
Assigns an empty array to lineSplit when reach the end of the file.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param ff file reference, file must be open
@param lineSplit result
@param lineIndex would be incremented accordingly

It takes into account the value of g_unImportDevice_charset variable
To make the on-the-flight recoding if necessary (it defaults to g_unImportDevice_charset="UTF-8",
and another option is "ISO-8859-1")
*/
unImportDevice_getNextLine(file ff, dyn_string& lineSplit, int &lineIndex) {
    while (feof(ff) == 0) {
        string currentLine;
        fgets(currentLine, 100000, ff);
        lineIndex++;
        currentLine = strrtrim(strltrim(currentLine));
        if (g_unImportDevice_charset!="") currentLine=recode (currentLine,g_unImportDevice_charset);
        strreplace(currentLine, "\\r", "");
        strreplace(currentLine, "\\n", "");
        if (currentLine != "" && substr(currentLine, 0, 1) != "#") {
            lineSplit = strsplit(currentLine, ";");
            return;
        }
    }
    lineSplit = makeDynString();
}


// TODO: AM: check if possible to have int& errorLines in the end as optional parameter
unImportDevice_report(file logFile, string message) {
    if (unGenericObject_shapeExists("messages") && unGenericObject_shapeExists("debugLevel")) {
        bool isDebugMessage  = strpos(message, "DEBUG:") == 0;
        //if (isDebugMessage) message = substr(message, 6);
        if (!isDebugMessage || IMPORTDEVICE_DEBUG_ALL == debugLevel.selectedPos) {
            messages.appendItem(message);
            messages.bottomPos(0);
        }
    }
    if (logFile != 0) {
        fputs(formatTime("[%Y/%m/%d %H:%M:%S] ", getCurrentTime()) + message + "\n", logFile);
    }
}

unImportDevice_printExceptionToFile(file logFile, dyn_string &exceptionInfo) {
    if (logFile != 0) {
        fputs("    Details: ", logFile);
        for(int i = 1; i <= dynlen(exceptionInfo); i++) {
            fputs(exceptionInfo[i] + "; ", logFile);
        }
        fputs("\n", logFile);
    }
    dynClear(exceptionInfo);
}
bool unImportDevice_isWarningInLog(string logFilePath)
{
    file logFile = fopen(logFilePath, "r");
    if (logFile == 0) {
        DebugTN("can't open file: " + logFilePath);
        return true;
    }
    while (feof(logFile) == 0) {
        string currentLine;
        fgets(currentLine, 100000, logFile);
        if (strpos(currentLine, "WARNING:") >= 0) {
            fclose(logFile);
            return true;
        }
    }
    fclose(logFile);
    return false;
}

bool unImportDevice_isErrorInLog(string logFilePath)
{
    file logFile = fopen(logFilePath, "r");
    if (logFile == 0) {
        DebugTN("can't open file: " + logFilePath);
        return true;
    }
    while (feof(logFile) == 0) {
        string currentLine;
        fgets(currentLine, 100000, logFile);
        if (strpos(currentLine, "ERROR:") >= 0) {
            fclose(logFile);
            return true;
        }
    }
    fclose(logFile);
    return false;
}

bool unImportDevice_checkFile(string fileName, string logFilePath, string sFrontEndType, int driver, string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool& deleteRequested) {
    dyn_string exceptionInfo, dsDeleteDps;
    file logFile = fopen(logFilePath, "a");
    bool checkOK = false;
    int iErrorCount;
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEINIT"));
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLECHECKDATA") + formatTime("%Y/%m/%d %H:%M:%S", getCurrentTime()));
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGFILE") + fileName);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGDRIVER") + driver);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGBOOLARCH") + sBoolArchiveName);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGANALOGARCH") + sAnalogArchiveName);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGEVENTARCH") + sEventArchiveName);

    // Check if Driver is running (either SIM or real Frond-End driver)
    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEDRIVER"));
    unImportDevice_checkDriver(driver, sFrontEndType, exceptionInfo); //REVIEW: original function, user has less informative message
    if (dynlen(exceptionInfo) > 0) {
        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGDRIVERFAILED"));
        unImportDevice_printExceptionToFile(logFile, exceptionInfo);
        iErrorCount++;
    } else {
        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGDRIVEROK"));
    }

    // Check if CTRL _unSystemIntetrity.ctl started
    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLECTRL"));
    unImportDevice_checkCtrl(exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGCTRLFAILED"));
        unImportDevice_printExceptionToFile(logFile, exceptionInfo);
        iErrorCount++;
    } else {
        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGCTRLOK"));
    }

    if (iErrorCount > 0) {
      if (logFile != 0) {
          fclose(logFile);
      }
      return checkOK;
    }

    // Check File
    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEFILE"));
    string fileAccesError = unImportDevice_isFileAccessible(fileName);
    unImportDevice_report(logFile, getCatStr("unGeneration", fileAccesError));
    if (fileAccesError == "DEBUGFILEEXISTS") {
        unImportDevice_report(logFile, "File character encoding selected:"+g_unImportDevice_charset);

        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGCHECKDEVICE"));

        file ff = fopen(fileName, "r");
        int fileSize = getFileSize(fileName);
        int lineIndex = 0;
        dyn_string dsConfig, dsAllAliasesDpe, dsAlias;
        string sFEName = "", sFEApplication = "", sPrefix, sFEVersion;
        bool bEvent16;
        dyn_string dsRunningArchive, dsArchive, unicosObjectsNames;
        mapping mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveDpAvail, mArchiveRequiredSpace, objectInfo;
        int iPlcNumber;
        bool plcIsDefined = false;
        dpGetAllAliases(dsAllAliasesDpe, dsAlias);
        unImportDevice_getRunningArchiveName(dsArchive);
        unImportDevice_getAllArchiveConfig(dsRunningArchive, mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveDpAvail);
        unGenericDpFunctions_getUnicosObjects(unicosObjectsNames);

        unImportDevice_getApplicationCharacteristics(bEvent16, sPrefix);
        dyn_string checkedFrontEnds;

        while (feof(ff) == 0) {
            unImportDevice_getNextLine(ff, dsConfig, lineIndex);
            if (dynlen(dsConfig) == 0) continue;
            if (isFunctionDefined("reportProgress")) execScript("main(int pos, int max) { reportProgress(pos, max); }", makeDynString(), ftell(ff), fileSize);

            bool isNewDevice = true;
            bool bContinue = false;

            switch (dsConfig[1]){
               case UN_PLC_COMMAND:
                    if (plcIsDefined) {
                        unImportDevice_report(logFile, "ERROR: Line " + lineIndex + " skipped, second PLC definition.");
                        bContinue=true;
                        break;
                    }

                    sFEName = dsConfig[UN_CONFIG_PLC_NAME+1];
                    sFEApplication = dsConfig[UN_CONFIG_PLC_SUBAPPLICATION+1];
                    isNewDevice = !unImportDevice_checkFEExists("*" + dsConfig[UN_CONFIG_PLC_NAME+1], sFrontEndType);
                    plcIsDefined = true;
                    unImportDevice_checkFrontEndState(sFrontEndType, sFEName, exceptionInfo);
                    if (dynlen(exceptionInfo) > 0) {
                      unImportDevice_report(logFile, "ERROR: Line " + lineIndex + ": " + getCatStr("unGeneration","DEBUGPLCBAD"));
                      unImportDevice_printExceptionToFile(logFile, exceptionInfo);
                      if (logFile != 0) {
                          fclose(logFile);
                      }
                      fclose(ff);
                      return checkOK;
                    }
                    break;
                case UN_DELETE_COMMAND:
                    if(dynlen(dsConfig)==2) {
                      unImportDevice_report(logFile, "WARNING: Delete front-end command detected. Really delete the front-end? If no, use different delete command.");
                    }
                    break;
                default:
                    if (!plcIsDefined) {
                        unImportDevice_report(logFile, "ERROR: Line " + lineIndex + " skipped, device is defined before plc.");
                        bContinue = true;
                    }
                    break;
            }
            if(bContinue)
            {
              continue;
            }

            // TODO: move this to unImportDevice_check
            if (dynContains(unicosObjectsNames, dsConfig[1])) {
                if (dynlen(dsConfig) < 3) {
                    unImportDevice_report(logFile, "ERROR: Line " + lineIndex + getCatStr("unGeneration", "DEBUGBADFORMAT"));
                    continue;
                }
                string sDeviceDpName = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, dsConfig[1], dsConfig[2]);
                isNewDevice = unImportDevice_checkDeviceAlias(dsAlias, sDeviceDpName, dsConfig[3], dsDeleteDps, exceptionInfo);
            }
            //TODO: sFEName and sFEApplication should be reviewed
            dyn_string dsDeleteDpsTmp = unImportDevice_check(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, driver,
                      sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16, dsRunningArchive, iPlcNumber, false, sFEVersion);
            dynAppend(dsDeleteDps, dsDeleteDpsTmp);
            if (dsConfig[1] == UN_PLC_COMMAND) {
                if (mappingHasKey(g_mImportFrontEndVersion, dsConfig[UN_CONFIG_PLC_NAME+1])) {
                    if (unGenericObject_shapeExists("frontEndVersion")) {
                      frontEndVersion.text = g_mImportFrontEndVersion[dsConfig[UN_CONFIG_PLC_NAME+1]];
                    }
                }
            }
            if (dynlen(exceptionInfo) == 0) {
                unImportDevice_checkArchive(dsConfig, exceptionInfo, sFrontEndType, sFEName, sFEApplication, sPrefix, driver,
                        sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16, dsArchive, iPlcNumber, false, sFEVersion,
                        mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveRequiredSpace);
            }
            if (dynlen(exceptionInfo) > 0) {
                string errorMsg = "ERROR: Line " + lineIndex + ": " + exceptionInfo[2];
                unImportDevice_report(logFile, errorMsg);
                unImportDevice_printExceptionToFile(logFile, exceptionInfo);
            } else {
                unImportDevice_incrementDeviceCounter(dsConfig[1], isNewDevice, objectInfo);
            }
        }
        fclose(ff);

        // Display delete dpes
        if (dynlen(dsDeleteDps) > 0) {
            unImportDevice_report(logFile, "INFO: " + dynlen(dsDeleteDps) + getCatStr("unGeneration", "DEBUGDELETEDPS"));
            for (int i = 1; i <= dynlen(dsDeleteDps); i++) {
                unImportDevice_report(logFile, "DEBUG: " + dsDeleteDps[i] + " will be deleted");
            }
        }

        // Display results
        dyn_string objectInfoKeys = mappingKeys(objectInfo);
        unImportDevice_report(logFile, "");
        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLERESULTS"));
        unImportDevice_report(logFile, "INFO: " + unImportDevice_getDeviceCounterSum(true, objectInfo) + getCatStr("unGeneration", "DEBUGOBJECTCREATED"));
        for (int i = 1; i <= dynlen(objectInfoKeys); i++) {
            if (objectInfo[objectInfoKeys[i]][IMPORTDEVICE_CREATE_ACTION] > 0)
                unImportDevice_report(logFile, "INFO:      " + objectInfoKeys[i] + " " + objectInfo[objectInfoKeys[i]][IMPORTDEVICE_CREATE_ACTION] + getCatStr("unGeneration", "DEBUGOBJECTGENERATION"));
        }
        unImportDevice_report(logFile, "INFO: " + unImportDevice_getDeviceCounterSum(false, objectInfo) + getCatStr("unGeneration", "DEBUGOBJECTSUPDATE"));
        for (int i = 1; i <= dynlen(objectInfoKeys); i++) {
            if (objectInfo[objectInfoKeys[i]][IMPORTDEVICE_UPDATE_ACTION] > 0)
                unImportDevice_report(logFile, "INFO:      " + objectInfoKeys[i] + " " + objectInfo[objectInfoKeys[i]][IMPORTDEVICE_UPDATE_ACTION] + getCatStr("unGeneration", "DEBUGOBJECTSUPDATE"));
        }

        // Display archive stats
        unImportDevice_report(logFile, "");
        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEARCHIVES"));
        dyn_string usedArchives = mappingKeys(mArchiveMaxElts);
        for (int i = 1; i <= dynlen(usedArchives); i++) {
            int required = 0;
            if (mappingHasKey(mArchiveRequiredSpace, usedArchives[i])) required = mArchiveRequiredSpace[usedArchives[i]];
            unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGARCHIVE") + usedArchives[i] +
                                                getCatStr("unGeneration","DEBUGARCHIVENEEDED") + required +
                                                getCatStr("unGeneration","DEBUGARCHIVEFREE") + mArchiveDpAvail[usedArchives[i]]);
            string archiveMsgKey = required > mArchiveDpAvail[usedArchives[i]] ? "DEBUGARCHIVEBAD" : "DEBUGARCHIVEOK";
            unImportDevice_report(logFile, getCatStr("unGeneration", archiveMsgKey));
        }
    }

    // Footer
    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLECHECKDATAEND") + formatTime("%Y/%m/%d %H:%M:%S", getCurrentTime()));
    unImportDevice_report(logFile, "Front-end type: " + sFrontEndType);
    unImportDevice_report(logFile, "");

    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGCHECKFILE") + logFilePath);
    unImportDevice_report(logFile, "");

    deleteRequested = dynlen(dsDeleteDps) > 0;

    if (logFile != 0) {
        fclose(logFile);
    }

    checkOK = !unImportDevice_isErrorInLog(logFilePath);
    logFile = fopen(logFilePath, "a");
    if (!checkOK) {
        unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGCHECKFAILED"));
    } else {
        if (dynlen(dsDeleteDps) > 0) {
            unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUG2IMPORT"));
            unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGCHECKDELETEOK"));
        } else {
            unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGCHECKIMPORTOK"));
        }
    }
    if (logFile != 0) {
        fclose(logFile);
    }

    return checkOK;
}

int unImportDevice_getDeviceCounterSum(bool isNewDevice, mapping& objectInfo) {
    int sum = 0;
    dyn_string keys = mappingKeys(objectInfo);
    for (int i = 1; i <= dynlen(keys); i++) {
        sum += objectInfo[keys[i]][isNewDevice ? IMPORTDEVICE_CREATE_ACTION : IMPORTDEVICE_UPDATE_ACTION];
    }
    return sum;
}
unImportDevice_incrementDeviceCounter(string currentObject, bool isNewDevice, mapping& objectInfo) {
    if (currentObject == UN_DELETE_COMMAND) return;
    if (!mappingHasKey(objectInfo, currentObject)) {
        mapping devInfo;
        devInfo[IMPORTDEVICE_CREATE_ACTION] = 0;
        devInfo[IMPORTDEVICE_UPDATE_ACTION] = 0;
        objectInfo[currentObject] = devInfo;

    }
    objectInfo[currentObject][isNewDevice ? IMPORTDEVICE_CREATE_ACTION : IMPORTDEVICE_UPDATE_ACTION] += 1;
}
// TODO: Marco: make it generic?
bool unImportDevice_checkFEExists(string sFEPattern, string sFEType) {
    dyn_string dsFe=dpNames(sFEPattern);
    for(int i = 1;i <= dynlen(dsFe); i++) {
        if(dpTypeName(dsFe[i]) == sFEType)
            return true;
    }
    return false;
}
// TODO: Marco: make it generic?
// TODO: AM : write doc
string unImportDevice_getDeviceConstantRoot(string sFrontEndType, string sCurrentDeviceType) {
    if (sFrontEndType == UN_PLC_DPTYPE || sFrontEndType == S7_PLC_DPTYPE)
        return strtoupper(sCurrentDeviceType);
    else
        return sFrontEndType + "_" + strtoupper(sCurrentDeviceType);
}
/**Returns a value of constant by name

Returns 0 if value is not defined.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param name constant name
@return constant's value
*/
// TODO: Marco: make it generic?
anytype unGenerateDatabase_getConstantByName(string name) {
    anytype result;
    evalScript(result, "anytype main() { anytype result = 0; try { result = " + name + "; } catch { result = 0;} return result; }", makeDynString());
    return result;
}

bool unImportDevice_deleteFromFile(string fileName, string logFilePath, string sFrontEndType, int driver) {
    dyn_string exceptionInfo;
    bool deleteOK = true;
    file logFile = fopen(logFilePath, "a");

    fwAccessControl_SuspendModifications(true, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      unImportDevice_report(logFile, "ERROR: Can't suspend access control modifications: " + exceptionInfo);
      return false;
    }
    int commitTimeout = 0;
    dpGet("_Ui_"+myManNum()+".Inactivity.CommitTimeout", commitTimeout);
    if (commitTimeout > 0) {
      dpSet("_Ui_"+myManNum()+".Inactivity.CommitTimeout", 18000); // set auto-logout to 5 hours
    }
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEINIT"));
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEDELETEDATA") + formatTime("%Y/%m/%d %H:%M:%S", getCurrentTime()));
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGFILE") + fileName);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGDRIVER") + driver);
    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEFILE"));
    unImportDevice_report(logFile, "File character encoding selected:"+g_unImportDevice_charset);

    file ff = fopen(fileName, "r");
    int fileSize = getFileSize(fileName);
    int lineIndex = 0;
    dyn_string dsConfig;
    dyn_string dsRunningArchive, dsDeleteDps;
    mapping mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveDpAvail;

    unImportDevice_getRunningArchiveName(dsRunningArchive);
    unImportDevice_getAllArchiveConfig(dsRunningArchive, mDeviceTypeArchive, mArchiveDp, mArchiveMaxElts, mArchiveDpAvail);

    string sFrontEndVersion; // TODO: init me?

    while (feof(ff) == 0) {
        unImportDevice_getNextLine(ff, dsConfig, lineIndex);
        if (dynlen(dsConfig) == 0) continue;
        if (isFunctionDefined("reportProgress")) execScript("main(int pos, int max) { reportProgress(pos, max); }", makeDynString(), ftell(ff), fileSize);

        if (dsConfig[1] == UN_DELETE_COMMAND) {
            dyn_string dsDeleteDpsTmp = unImportDevice_delete(dsConfig, exceptionInfo, sFrontEndType, "",  "", "", dsRunningArchive, false, sFrontEndVersion);
            dynAppend(dsDeleteDps, dsDeleteDpsTmp);
            if (dynlen(exceptionInfo) > 0) {
                deleteOK = false;
                unImportDevice_report(logFile, "ERROR: Line " + lineIndex + getCatStr("unGeneration","DEBUGDELETEBAD"));
                unImportDevice_printExceptionToFile(logFile, exceptionInfo);
            }
        }
    }


    dyn_string archiveToSwitch = unImportDevice_getArchiveToSwitch(dsDeleteDps, dsRunningArchive, mArchiveDp);
    for (int i = 1; i <= dynlen(archiveToSwitch); i++) {
        if (isFunctionDefined("reportProgress")) execScript("main(int pos, int max) { reportProgress(pos, max); }", makeDynString(), i, dynlen(archiveToSwitch));
        unImportDevice_report(logFile, "INFO: Start switch archive => " + archiveToSwitch[i]);
        unImportDevice_valarchFileSwitch(archiveToSwitch[i], exceptionInfo);
        if (dynlen(exceptionInfo) > 0) {
            deleteOK = false;
            unImportDevice_report(logFile, "ERROR: Switch archive not correctly done, timeout expired");
            unImportDevice_printExceptionToFile(logFile, exceptionInfo);
        } else {
            unImportDevice_report(logFile, "INFO: Switch archive done successfully");
        }
    }

    unImportDevice_triggerUpdate();

    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGTITLEDELETEDATAEND") + formatTime("%Y/%m/%d %H:%M:%S",getCurrentTime()));
    unImportDevice_report(logFile, "File: " + fileName);
    unImportDevice_report(logFile, "Front-end type: " + sFrontEndType);
    unImportDevice_report(logFile, "");
    if (deleteOK) {
        unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGDELETEDATAOK"));
        unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGAFTERDELETE"));
    } else {
        unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGDELETEDATABAD"));
    }

    unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGCHECKFILE") + logFilePath);
    unImportDevice_report(logFile, "");

    fwAccessControl_SuspendModifications(false, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      unImportDevice_report(logFile, "ERROR: Can't resume access control modifications: " + exceptionInfo);
      deleteOK = false;
    }
    if (commitTimeout > 0) {
      dpSet("_Ui_"+myManNum()+".Inactivity.CommitTimeout", commitTimeout); // set auto-logout to 5 hours
    }

    if (logFile != 0) {
        fclose(logFile);
    }

    return deleteOK;
}

// TODO: AM: make internal and document
dyn_string unImportDevice_getArchiveToSwitch(dyn_string dsDeleteDps, dyn_string dsName, mapping mDeviceTypeArchive)
{
    dyn_string dsNameToSwitch;
    dyn_string dsTemp, dsRes;

    for(int i = 1; i <= dynlen(dsName); i++) {
        if(mappingHasKey(mDeviceTypeArchive, dsName[i])) {
            dsTemp = mDeviceTypeArchive[dsName[i]];
            for(int j = 1; j <= dynlen(dsDeleteDps); j++) {
                dsRes = dynPatternMatch(dsDeleteDps[j]+".*", dsTemp);
                if(dynlen(dsRes) > 0) {
                    dynAppend(dsNameToSwitch, dsName[i]);
                }
            }
        }
    }
    dynUnique(dsNameToSwitch);
    return dsNameToSwitch;
}

unImportDevice_disablePeripheryAddress(string fileName, string sPrefix, string sSystemName, file logFile, dyn_string& exceptionInfo)
{
    int lineIndex = 0;

    dyn_string dsDpeWithAddress;
    dyn_string dsConfig;
    string sFEName, sFEApplication;

    unImportDevice_report(logFile,  getCatStr("unGeneration","DEBUGADDRESSSCAN"));
    file ff = fopen(fileName, "r");
    while (feof(ff) == 0) {
        unImportDevice_getNextLine(ff, dsConfig, lineIndex);
        if (dynlen(dsConfig) == 0) continue;

        if (dsConfig[1] == UN_PLC_COMMAND) {
            sFEName = dsConfig[UN_CONFIG_PLC_NAME+1];
            sFEApplication = dsConfig[UN_CONFIG_PLC_SUBAPPLICATION+1];
        }
        string sQueryPattern = unImportDevice_makeUnicosDeviceDpName(sPrefix, sFEName, sFEApplication, dsConfig[1], dsConfig[2], sSystemName);
        if(dpExists(sQueryPattern))
        {
          //query only dpes that may have address configured with Output direction
          dynAppend(dsDpeWithAddress, dpNames(sQueryPattern+".ProcessOutput.**"));
          dynAppend(dsDpeWithAddress, dpNames(sQueryPattern+".Diagnostic.*"));
          dynAppend(dsDpeWithAddress, dpNames(sQueryPattern+".communication.**"));
        }
    }
    fclose(ff);

    if (dynlen(dsDpeWithAddress)) {
      dyn_bool dbConfigExists;
      dyn_bool dbIsActive, dbSetActive;
      dyn_int diAddressType;
      dyn_string dsDpeToDisable, dsDpeCleaned;
      unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGADDRESSDISABLE"));
      //remove structure dp (keep only dpes)
      for(int i=1;i<=dynlen(dsDpeWithAddress);i++) {
        if (!(dpElementType(dsDpeWithAddress[i]) == DPEL_BIT32_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_BLOB_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_BOOL_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_CHAR_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_BIT32_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_BLOB_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_BOOL_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_CHAR_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_FLOAT_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_INT_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_STRING_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_TIME_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_DYN_UINT_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_FLOAT_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_INT_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_STRING_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_TIME_STRUCT ||
           dpElementType(dsDpeWithAddress[i]) == DPEL_UINT_STRUCT))
        {
          dynAppend(dsDpeCleaned, dsDpeWithAddress[i]);
        }
      }
      // UNCORE-119: in PIC and WIC many devices may point to the same
      // frontends, yielding duplicates in the list; eliminate them,
      // or otherwise the dpSetWait() below would effectively fail
      dynUnique(dsDpeCleaned);

      dyn_string diAddressTypeDPE;
      for(int i=1 ; i <= dynlen(dsDpeCleaned) ; i++)
      {
          dynAppend(diAddressTypeDPE, dsDpeCleaned[i]+":_address.._type");
      }
      dpGet(diAddressTypeDPE, diAddressType);
      //collect dpes with address
      for(int i=1 ; i <= dynlen(dsDpeCleaned) ; i++)
      {
        if(diAddressType[i] > 0) {
          dynAppend(dsDpeToDisable, dsDpeCleaned[i]+":_address.._active");
          dynAppend(dbSetActive, false);
        }
      }
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "Import", "unImportDevice_importFile - addresses to disable", dsDpeToDisable);
      //disable address
      dpSetWait(dsDpeToDisable, dbSetActive);
      exceptionInfo = getLastError();
    }
    else
    {
      unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGNOADDRESSTODISABLE"));
    }
}

bool unImportDevice_importFile(string fileName, string logFilePath, int driver, string sBoolArchiveName, string sAnalogArchiveName, string sEventArchiveName, bool checkBeforeImport) {
    dyn_string exceptionInfo;
    bool importOK = true;
    file logFile = fopen(logFilePath, "a");

    int fileSize = getFileSize(fileName);
    int lineIndex = 0;
    dyn_string dsConfig;
    dyn_string dsRunningArchive;

    string sFEName, sFEApplication, sFrontEndType, sPrefix;
    bool bEvent16;
    int iPlcNumber = 0;
    string sFrontEndVersion = "";
    string sSystemName = getSystemName();


    fwAccessControl_SuspendModifications(true, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      unImportDevice_report(logFile, "ERROR: Can't suspend access control modifications: " + exceptionInfo);
      return false;
    }

    // on UIs set autologout to 5 hours
    int commitTimeout = 0;
    if (myManType()==UI_MAN) {
        dpGet("_Ui_"+myManNum()+".Inactivity.CommitTimeout", commitTimeout);
        if (commitTimeout > 0) {
          unImportDevice_report(logFile,"Temporarily setting auto-logout to 5 hours");
          dpSet("_Ui_"+myManNum()+".Inactivity.CommitTimeout", 18000);
        }
    }

    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEINIT"));
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEIMPORTDATA") + formatTime("%Y/%m/%d %H:%M:%S", getCurrentTime()));
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGFILE") + fileName);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGDRIVER") + driver);
    unImportDevice_report(logFile, "File character encoding selected:"+g_unImportDevice_charset);

    // Check if the right driver is running
    file ff = fopen(fileName, "r");
    while (feof(ff) == 0) {
        unImportDevice_getNextLine(ff, dsConfig, lineIndex);
        if (dynlen(dsConfig) == 0) continue;
        if (dsConfig[1] == UN_PLC_COMMAND) {
            sFrontEndType = dsConfig[UN_CONFIG_PLC_TYPE+1];
            break;
        }
    }
    fclose(ff);
    bool bIgnoreDriverRelatedConfigs = false;
    dyn_string driverExceptionInfo;
    unImportDevice_checkDriver(driver, sFrontEndType, driverExceptionInfo);
    if (dynlen(driverExceptionInfo) > 0) {
      bIgnoreDriverRelatedConfigs = true;
      unImportDevice_report(logFile, "WARNING: address configuration will be skipped because simulation driver is not running.");
    }
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGBOOLARCH") + sBoolArchiveName);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGANALOGARCH") + sAnalogArchiveName);
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGEVENTARCH") + sEventArchiveName);

    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration", "DEBUGTITLEFILE"));

    // Get the archives
    unImportDevice_getRunningArchiveName(dsRunningArchive);

    // Get some application characteristics
    unImportDevice_getApplicationCharacteristics(bEvent16, sPrefix);

    // disable periphery address if devices already exist
    unImportDevice_disablePeripheryAddress(fileName, sPrefix, sSystemName, logFile, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        importOK = false;
        unImportDevice_report(logFile, "ERROR: Could not disable all the periphery addesses. See WinCC OA Log for more info");
        unImportDevice_printExceptionToFile(logFile, exceptionInfo);
    } else {
        unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "Import", "unImportDevice_importFile - ", getCatStr("unGeneration","DEBUGADDRESSDISABLED"));
    }

    // Do the actual import
    lineIndex=0; // IS-1829, reset the line counter
    ff = fopen(fileName, "r");
    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGIMPORTDEVICE"));
    while (feof(ff) == 0) {
        unImportDevice_getNextLine(ff, dsConfig, lineIndex);
        if (dynlen(dsConfig) == 0) continue;
        if (isFunctionDefined("reportProgress")) execScript("main(int pos, int max) { reportProgress(pos, max); }", makeDynString(), ftell(ff), fileSize);

        if (dsConfig[1] == UN_PLC_COMMAND) {
            sFEName = dsConfig[UN_CONFIG_PLC_NAME+1];
            sFEApplication = dsConfig[UN_CONFIG_PLC_SUBAPPLICATION+1];
            sFrontEndType = dsConfig[UN_CONFIG_PLC_TYPE+1];
            iPlcNumber = (int)dsConfig[UN_CONFIG_PLC_NUMBER+1];
        }

        unImportDevice_import(dsConfig, exceptionInfo, sFrontEndType, sFEName,
                      sFEApplication, sPrefix, driver,
                      sBoolArchiveName, sAnalogArchiveName, sEventArchiveName, bEvent16,
                      dsRunningArchive, iPlcNumber, checkBeforeImport, sFrontEndVersion, bIgnoreDriverRelatedConfigs);


        if (dynlen(exceptionInfo) > 0) {
            importOK = false;
            unImportDevice_report(logFile, "ERROR: Line " + lineIndex + " skipped," + getCatStr("unGeneration","DEBUGBADFORMAT"));
            unImportDevice_report(logFile, "DEBUG: Bad line: " + dsConfig);
            unImportDevice_printExceptionToFile(logFile, exceptionInfo);
        }
    }
    fclose(ff);

    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGTITLEIMPORTDATAEND") + formatTime("%Y/%m/%d %H:%M:%S", getCurrentTime()));
    unImportDevice_report(logFile, "File: "+fileName);
    unImportDevice_report(logFile, "Front-end type: "+sFrontEndType);
    unImportDevice_report(logFile, "");
    if (!importOK) {
        unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGIMPORTDATABAD"));
    } else {
        unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGIMPORTDATAOK"));
        unImportDevice_triggerUpdate();

        // Do a GQ to get most recent PLC state
        unImportDevice_requestAllData(sFEName, sFrontEndType, iPlcNumber, exceptionInfo);
    }
    unImportDevice_report(logFile, "");
    unImportDevice_report(logFile, getCatStr("unGeneration","DEBUGCHECKFILE") + logFilePath);

    fwAccessControl_SuspendModifications(false, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      unImportDevice_report(logFile, "ERROR: Can't resume access control modifications: " + exceptionInfo);
      importOK = false;
    }

    // restore original auto-logout settings
    if (myManType()==UI_MAN) {
        if (commitTimeout > 0) {
            unImportDevice_report(logFile,"Restoring the original auto-logout of "+commitTimeout+" seconds");
            dpSet("_Ui_"+myManNum()+".Inactivity.CommitTimeout", commitTimeout);
        }
    }

    if (logFile != 0) {
        fclose(logFile);
    }

    return importOK;
}

private void unImportDevice_requestAllData(string frontEndName, string frontEndType, int frontEndNumber, dyn_string &exInfo)
{
  frontEndType = unImportDevice_getFrontEndDeviceType(frontEndType);

  switch(frontEndType)
  {
    case S7_PLC_DPTYPE:
      unImportDevice_requestAllData_S7PLC(frontEndName, exInfo);
      break;

    case UN_PLC_DPTYPE:
      unImportDevice_requestAllData_UnPLC(frontEndName, frontEndNumber, exInfo);
      break;

    default:
      return;
  }
}

// TODO: put into front-end library
private void unImportDevice_requestAllData_S7PLC(string frontEndName, dyn_string &exInfo)
{
    string dpS7Conn = "_"+frontEndName;
    string dpS7PLC  = c_unSystemIntegrity_S7 + frontEndName;

    if(!dpExists(dpS7Conn)) {
        fwException_raise(exInfo, "ERROR", __FUNCTION__, dpS7Conn + "does not exist");
        return;
    }
    if(!dpExists(dpS7PLC)) {
        fwException_raise(exInfo, "ERROR", __FUNCTION__, dpS7PLC + "does not exist");
        return;
    }

    int drvNum = 0;
    dpGet(dpS7PLC+".communication.driver_num",drvNum);
    if (drvNum > 0) {
        dpSet("_Driver" + drvNum + ".SM:_original.._value", 1);
		    dpSet(dpS7Conn + ".DoGeneralQuery:_original.._value", 1);
    }

    unSystemIntegrity_sendCmdToPLC(dpS7PLC, c_unSystemIntegrity_PLCGetAll_BS_AS_table, makeDynInt(0, 0, 0, 0, 0) , exInfo);
}

// TODO: put in front-end library
private void unImportDevice_requestAllData_UnPLC(string frontEndName, int frontEndNumber, dyn_string &exInfo)
{
    string dpUnPLC = c_unSystemIntegrity_UnPlc+frontEndName;

    if(!dpExists(dpUnPLC)) {
        fwException_raise(exInfo, "ERROR", __FUNCTION__, dpUnPLC + "does not exist");
        return;
    }

    int drvNum = 0;
    dpGet(dpUnPLC+".communication.driver_num",drvNum);
    if (drvNum > 0) {
  			dpSet("_Driver" + drvNum + ".SM:_original.._value", 1);
        dpSet("_Driver" + drvNum + ".GQ:_original.._value", frontEndNumber+256);
    }

    unSystemIntegrity_sendCmdToPLC(dpUnPLC, c_unSystemIntegrity_PLCGetAll_BS_AS_table, makeDynInt(0, 0, 0, 0, 0) , exInfo);
}


