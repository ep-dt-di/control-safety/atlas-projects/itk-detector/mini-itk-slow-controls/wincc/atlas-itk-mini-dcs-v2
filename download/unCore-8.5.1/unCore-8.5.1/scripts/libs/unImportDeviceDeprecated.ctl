//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_makeMandatoryDeviceConfiguration
/**
Create the mandatory configuration for a device.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceType input, the device type
@param iDeviceNumber  input, the device number
@param sDeviceName  input, the device name
@param sDescription  input, the device description
@param sDiagnostic  input, the diagnostic panel
@param sHTML  input, the HTML page
@param sDefaultPanel  input, the default panel
@param dsSubSystem1  input, the list of subsystem1 (UNICOS domain)
@param dsSubSystem2  input, the list of subsystem2 (UNICOS nature)
@param sWidgetType  input, the device widget name
@param dsConfig  output, the device config as a dyn_string
@return the device config as a string (each configuration separated by ;)

@Deprecated 2018-08-14

*/
string unImportDevice_makeMandatoryDeviceConfiguration(string sDeviceType, int iDeviceNumber, string sDeviceName, 
                                                string sDescription, string sDiagnostic, string sHTML, string sDefaultPanel, 
                                                dyn_string dsSubSystem1, dyn_string dsSubSystem2, string sWidgetType, 
                                                dyn_string &dsConfig)
{

  FWDEPRECATED();

  string sConfig, sSubSystem1, sSubSystem2, sDeviceWidgetType;
  int i, len;
  dyn_string exceptionInfo;
  dyn_string dsWidgets;
  
  len = dynlen(dsSubSystem1);
  for(i=1;i<=len;i++)
  {
    if(sSubSystem1 == "")
      sSubSystem1 = dsSubSystem1[i];
    else
      sSubSystem1 += UN_CONFIG_GROUP_SEPARATOR+dsSubSystem1[i];
  }
  len = dynlen(dsSubSystem2);
  for(i=1;i<=len;i++)
  {
    if(sSubSystem2 == "")
      sSubSystem2 = dsSubSystem2[i];
    else
      sSubSystem2 += UN_CONFIG_GROUP_SEPARATOR+dsSubSystem2[i];
  }
  if(sWidgetType != "")
  {
    unConfigGenericFunctions_checkWidget(sWidgetType, sDeviceType, exceptionInfo);
    if(dynlen(exceptionInfo) <= 0)
      sDeviceWidgetType = sWidgetType;
  }
  else
  {
    unGenericDpFunctions_getDeviceWidgetList(sDeviceType, dsWidgets);
    if(dynlen(dsWidgets) >= 1)
      sDeviceWidgetType = unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName(dsWidgets[1]);
  }
  if(dynlen(exceptionInfo) <= 0)
    sConfig = sDeviceType+";"+iDeviceNumber+";"+sDeviceName+";"+sDescription+";"+sDiagnostic+";"+sHTML+";"+sDefaultPanel+";"+
              sSubSystem1+";"+sSubSystem2+";"+sDeviceWidgetType+";";
  dsConfig = strsplit(sConfig, ";");
  
  return sConfig;
}





//------------------------------------------------------------------------------------------------------------------------
// unImportDevice_makeMandatoryFrontEndConfiguration
/**
Create the mandatory configuration for a front-end.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFrontEndType input, the front-end type
@param sFEName  input, the front-end name
@param sFEApplication  input, the front-end application name
@param dsConfig  output, the device config as a dyn_string
@return the device config as a string (each configuration separated by ;)

@Deprecated 2018-08-14

*/
string unImportDevice_makeMandatoryFrontEndConfiguration(string sFrontEndType, string sFEName, string sFEApplication,
                                                         dyn_string &dsConfig)
{

  FWDEPRECATED();

  string sConfig;
  
  sConfig = UN_PLC_COMMAND+";"+sFrontEndType+";"+sFEName+";"+sFEApplication+";";
  dsConfig = strsplit(sConfig, ";");
  
  return sConfig;
}