/**@name LIBRARY: unObject_parampanels.ctl

@author: Herve Milcent (AB-CO), Vincent Forest (AB-CO)

Creation Date: 04/02/2003

Modification History: 
  14/09/2010: Herve
  - bug fix, IS-399: when one cancel the dpSelector, the selected Device must not be modified.
  
	09/12/2004: Herve
		if $param is given: do not add the systemName in the function unObject_parampanels_eventCommand
		
	17/11/2004: Herve
		childPanelOnReturn: check if dynlen > 0

version 1.0

External Functions: 
	. unObject_parampanels_initialize: initialisation function, called in each widget param panel.
	. unObject_parampanels_dpSelectorButton: start the UNICOS DP Selector
	. unObject_parampanels_eventCommand: triggered when the enter key is pressed in the identifier text field
	. unObject_parampanels_SetDollarParam: set the dollar parameters of the param panel
Internal Functions: 

Purpose: 
This library implements the logic used in the widget param panels.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. global variables: these two variables must be defined in the panel calling this function
		. sDeviceType: string, contains the dp type of the dp to be used
		. sDp: string, contains the current dp/alias used.
	. PVSS version: 3.0 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

//@{

//------------------------------------------------------------------------------------------------------------------------

// unObject_parampanels_initialize
/**
Purpose:
	initialisation function, called in each widget param panel.

Parameters:
	sPrefix: string, input, prefix name to be put in front of the panel title

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. global variables: these two variables must be defined in the panel calling this function
		. sDeviceType: string, contains the dp type of the dp to be used
		. sDp: string, contains the current dp/alias used.
	. PVSS version: 3.0 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unObject_parampanels_initialize(string sPrefix = "un")
{
	string  refName,
			sPanelrefName,
			sPanelFrameTitle;  
	
	dyn_string dollars, 
			   values;        

		
	//display the device type in the textfield dptNameBox & in the frame main panel
	//DebugN("sDeviceType=",sDeviceType);
	setValue("dptNameBox","text",sDeviceType);
	
	sPanelFrameTitle = sPrefix + sDeviceType + " " + "Parametrization"; 
	setValue("frameParamTitle","text",sPanelFrameTitle);
			
	//insert an image on the filter button
	setValue("FilterButton", "fill","[pattern,[tile,gif,dpeMon_tree.gif]]");
	
	//gets the values of the reference, dollarparameter names and its value
	// this function can only be used by WCCOAui
	getDollarParams(refName,dollars,values);
	
	// get the device dp name
	sDp = values[dynContains(dollars,"$sIdentifier")];
    if (sDp == "$sIdentifier")
    	sDp = "";
    
	//To allow the user to set the (Panel Name) of the widget
	setValue("refNameBox","text",refName);
	  
	//shows the $-parameters and the corresponding values
	setValue("deviceNameBox","text",sDp);
	setInputFocus(myModuleName(), myPanelName(), "deviceNameBox");
}

// unObject_parampanels_dpSelectorButton
/**
Purpose:
	start the UNICOS DP Selector.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. global variables: these two variables must be defined in the panel calling this function
		. sDeviceType: string, contains the dp type of the dp to be used
		. sDp: string, contains the current dp/alias used.
	. PVSS version: 3.0 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unObject_parampanels_dpSelectorButton()
{
	dyn_float df;
	dyn_string dpSelected;
	
	string sPanelrefName;
  		 
	//Call the filter panel, pass the device type and return the selected Datapoint found by the filtering 
	ChildPanelOnCentralModalReturn("vision/unicosObject/configuration/unDpFilterGeneral.pnl", "Data Point Selector", makeDynString("$sDeviceTypeName:"+sDeviceType),df, dpSelected);
	if(dynlen(dpSelected)>0) {
		if (dpSelected[1] != "")
			deviceNameBox.text = dpSelected[1];
//		else
//			deviceNameBox.text = sDp;
	}
}

// unObject_parampanels_eventCommand
/**
Purpose:
	triggered when the enter key is pressed in the identifier text field.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. global variables: these two variables must be defined in the panel calling this function
		. sDeviceType: string, contains the dp type of the dp to be used
		. sDp: string, contains the current dp/alias used.
	. PVSS version: 3.0 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unObject_parampanels_eventCommand()
{

	string sDpName;
	dyn_string ds;
	int pos;
  
	
    //get the datapoint 
	sDpName = this.text;

//	if	no sys name is defined then should take the local 
	if (strrtrim(strltrim(sDpName)) != "")
		{
		if( strpos(sDpName,":") <0) 
		{
			//no check on the system is done 
			//because this is to allow the user to develop in his own lab without having to be connected to the hardware
			
// if there is a $ given assume that this is a dolalr parameter
			if(strpos(sDpName,"$") <0)
				sDpName = getSystemName() + sDpName;

		}
		setValue("","text",sDpName);	
		}
}

// unObject_parampanels_SetDollarParam
/**
Purpose:
	Set the dollar parameters of the param panel.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unObject_parampanels_SetDollarParam()
{
	dyn_string dollars, values;
	string refName;
	string sDpSel;
	
	
	// get the device name
	getValue("deviceNameBox","text",sDpSel);
	 
	// here are the $parameters of the ref panel
	dollars[1] = "$sIdentifier";
	
	//set the value of the $parameters, the $sDomainList is the list of domain seperated by ;
	values[1] = strltrim(strrtrim(sDpSel));
	
	//gets the assigned name for the reference panel
	getValue("refNameBox","text",refName);
	
	// set the $parameters and name of this ref panel
	setDollarParams(refName, dollars, values);
}

//@}
