/**@name LIBRARY: unPanelAccessControl.ctl

@author: Riku-Pekka Silvola (BE-ICS-SDS)

Creation Date: 08/02/2017

Modification History: 

*/

global const string gunPanelAccessControl_panelAccessControlDPE="_unApplication.menu.menuFileAccessControl";

// unPanelAccessControl_addPanelAccessControlEntry
/**
Purpose:
to add Panel Access Control entries to Panel Access Control List.
	@param dsMenuAccess list of panel access control entries. Only one entry per file is accepted.
	@return int, number of entries added.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public int unPanelAccessControl_addPanelAccessControlEntry(dyn_string dsMenuAccess) {
	string path, pattern;
	int iNofdsMenuAccessEntries, iNofDuplicates, ret, iNofAdded = 0;
	dyn_string dsCurrentMenuAccess = makeDynString();
        unsigned uINT_MAX = 2147483647; // as dynlen will return an int, it is safe to say the for loop should not continue further than INT_MAX
	unPanelAccessControl_cleanPanelAccessControlList(dsMenuAccess);
	iNofdsMenuAccessEntries = dynlen(dsMenuAccess);
	unPanelAccessControl_checkPanelAccessControlDPE();
	unPanelAccessControl_dpGetPanelAccessControlList(dsCurrentMenuAccess);
	for (unsigned i = 1; i <= uINT_MAX && i <= iNofdsMenuAccessEntries; i++) {
		path = unPanelAccessControl_getPanelFromEntry(dsMenuAccess[i]);
		pattern = unPanelAccessControl_getPatternFromPath(path);
		iNofDuplicates = dynlen(dynPatternMatch(pattern, dsCurrentMenuAccess));
		if (iNofDuplicates < 0) {
			throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_addPanelAccessControlEntry: Failed to get the number of duplicate Panel Access Control entries."));
		}
		if (iNofDuplicates > 0) {
			continue; // entry for this panel already exists	
		}
		DebugTN("unPanelAccessControl_addPanelAccessControlEntry: Appending new entry: " + dsMenuAccess[i]);
		ret = dynAppend(dsCurrentMenuAccess, dsMenuAccess[i]); // Add new entry
		if (ret < 0) {
			throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_addPanelAccessControlEntry: Failed to append an entry to Panel Access Control list."));
		}
		iNofAdded += 1;
	}
	ret = dpSetWait(gunPanelAccessControl_panelAccessControlDPE, dsCurrentMenuAccess);
	if (ret < 0) {
		throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_addPanelAccessControlEntry: Failed to set new Panel Access Control list."));
	}
	return iNofAdded;
}

// unPanelAccessControl_cleanPanelAccessControlList
/**
Purpose:
To deduplicate Panel Acces Control list passed as a dyn_string
	@param dsMenuAccess list of panel access control entries. Returned list will only include one entry per filename

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_cleanPanelAccessControlList(dyn_string &dsMenuAccess) {
	string path, pattern;
	int iNofdsMenuAccessEntries, iNofMatches, iMatchIdxInList, ret;
	unsigned uINT_MAX = 2147483647; // as dynlen will return an int, it is safe to say the for loop should not continue further than INT_MAX
	if (dynUnique(dsMenuAccess) < 0) {
		throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "_unPanelAccessControl_cleanPanelAccessControlList: Failed to make list unique. dynUnique failed."));
	}
	dyn_string dsNewDsMenuAccess = makeDynString();
	iNofdsMenuAccessEntries = dynlen(dsMenuAccess);
	for (unsigned i=1; i <= uINT_MAX && i <= iNofdsMenuAccessEntries; i++) {
		path = unPanelAccessControl_getPanelFromEntry(dsMenuAccess[i]);
		pattern = unPanelAccessControl_getPatternFromPath(path);
		dyn_string matches = dynPatternMatch(pattern, dsMenuAccess);
		iNofMatches = dynlen(matches);
		for (unsigned j = 1; j <= uINT_MAX && j <= iNofMatches; j++) {
			if (matches[j] == dsMenuAccess[i]) {
				continue; // Do not remove the actual entry used for pattern
			}
			iMatchIdxInList = dynContains(dsMenuAccess, matches[j]);
			if (iMatchIdxInList <= 0 || iMatchIdxInList > iNofdsMenuAccessEntries) {
				throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "_unPanelAccessControl_cleanPanelAccessControlList: Got index is out of bounds of Panel Access Control list."));
			}
			ret = dynRemove(dsMenuAccess, iMatchIdxInList);
			if (ret < 0) {
				throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "_unPanelAccessControl_cleanPanelAccessControlList: Failed to remove entry from the Panel Access Control list."));
			}
			// update number of dsMenuAccess entries as we removed some
			iNofdsMenuAccessEntries = dynlen(dsMenuAccess);
			if (iNofdsMenuAccessEntries < 0) {
				throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "_unPanelAccessControl_cleanPanelAccessControlList: Failed to get the new length of Panel Access Control list."));
			}
		}
	}
}

// unPanelAccessControl_getPanelFromEntry
/**
Purpose:
To get the panel path from the Panel Access Control list entry
        @param sEntry: string, input, panel access control entry included in the Panel Access Control list
	@return string file path in Panel Access Control entry

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public string unPanelAccessControl_getPanelFromEntry(string sEntry) {
	string path="";
	dyn_string dsEntry = strsplit(sEntry, "|");
	if (dynlen(dsEntry) <= 0 || sEntry == "") {
		throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_addPanelAccessControlEntry: Malformed Panel Access Control entry."));
	}
	else { 
		path = dsEntry[1];
	}
	return path;
}

// unPanelAccessControl_getPatternFromPath
/**
Purpose:
To get the regex pattern to match Panel Access Control entries with same path
        @param sPath: string, input, panel file path included in the Panel Access Control list
	@return string rexep pattern for matching file path in Panel Access Control entry list	

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public string unPanelAccessControl_getPatternFromPath(string sPath) {
		string pattern = "";
		if (sPath == "") {
			throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_addPanelAccessControlEntry: Malformed Panel Access Control entry."));
		}
		else {
			pattern = sPath+"|*"; // e.g. vision/unRecipe/UnRcpClass/unUnRcpClass_NewRcpClass.pnl|* 
		}
		return pattern;
}

// unPanelAccessControl_checkAccessControlDPE
/**
Purpose:
To check that Access Control DPE exists

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_checkPanelAccessControlDPE() {
	bool bRet = dpExists(gunPanelAccessControl_panelAccessControlDPE);
	if (bRet == false) {
		throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_addPanelAccessControlEntry: DPE for File Access Control does not exist."));
	}
}

// unPanelAccessControl_dpGetAccessControlList
/**
Purpose:
To get current Panel Access Control List from DPE
	@param dsMenuAccess: dyn_string, output, list of panel access control entries. Returned list will only include one entry per filename

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_dpGetPanelAccessControlList(dyn_string &dsMenuAccess) {
	int ret = dpGet(gunPanelAccessControl_panelAccessControlDPE, dsMenuAccess);
        if (ret < 0) {
        	throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_addPanelAccessControlEntry: Failed to get the number of current Panel Access Control entries."));
        }

}

// unPanelAccessControl_getAccessControlEntryFromParam
/**
Purpose:
To make an Access Control Entry from panel path, domain string and access level
	@param panel, input, panel path
	@param level, input, access level string 
	@param domain, input, domain string
	@return string, panel access control entry

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public string unPanelAccessControl_getAccessControlEntryFromParam(string panel, string level, string domain = "") {
        if (panel == "" || level == "") {
                throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_getAccessControlEntryFromParam: You must pass a valid panel path and access level to the function."));
        }
	string sOperator = level == UN_USER_OPERATOR ? UN_USER_OPERATOR : "";
	string sExpert = level == UN_USER_EXPERT ? UN_USER_EXPERT : "";
	string sAdmin = level == UN_USER_ADMIN ? UN_USER_ADMIN : "";
	string sEntry = panel 	+ UN_ACCESS_CONTROL_DOMAIN_SEPARATOR + 
		domain 		+ UN_ACCESS_CONTROL_DOMAIN_SEPARATOR + 
		sOperator 	+ UN_ACCESS_CONTROL_DOMAIN_SEPARATOR +
		sExpert		+ UN_ACCESS_CONTROL_DOMAIN_SEPARATOR +
		sAdmin	 	+ UN_ACCESS_CONTROL_DOMAIN_SEPARATOR;
	return sEntry;
}

// unPanelAccessControl_addPanelAccessibleByUser
/**
Purpose:
To add entry to Panel Access Control list
	@param panel: dyn_string, input, paths of panels to add
	@param level: dyn_string, input, ACL level of panel. Pass only one to apply to all.
	@param domain: dyn_string, input, domains for panels to add. Pass only one to apply to all. [optional]

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_addPanelAccessibleByUser(dyn_string panel, dyn_string level, dyn_string domain = makeDynString()) {
	int iNofPanels, iNofLevels, iNofDomains, iPanelInACL;
	dyn_string dsPACL = makeDynString();
	dyn_string dsNewEntries = makeDynString();
	string sPattern, sDomain, sLevel, sEntry;
        unPanelAccessControl_checkPanelAccessControlDPE();
	unPanelAccessControl_dpGetPanelAccessControlList(dsPACL);
	iNofLevels = dynlen(level);
	if (iNofLevels < 0) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_addPanelAccessibleByUser: Failed to get the number of levels for panels.")); }
	iNofPanels = dynlen(panel);
	if (iNofPanels < 0) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_addPanelAccessibleByUser: Failed to get the number of panels to add.")); }
	iNofDomains = dynlen(domain);
	if (iNofDomains < 0) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_addPanelAccessibleByUser: Failed to get the number of domains for panels.")); }
	if (iNofDomains > 1 && iNofDomains < iNofPanels) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_addPanelAccessibleByUser: Inconsistent number of domains for panels."));} 
	if (iNofLevels > 1 && iNofLevels < iNofPanels) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_addPanelAccessibleByUser: Inconsistent number of levels for panels."));} 
        unsigned uINT_MAX = 2147483647; // as dynlen will return an int, it is safe to say the for loop should not continue further than INT_MAX
	for (int i = 1; i <= iNofPanels && i <= uINT_MAX; i++) {
		sPattern = unPanelAccessControl_getPatternFromPath(panel[i]);
		iPanelInACL = dynContains(dsPACL, sPattern);
		if (iPanelInACL < 0) {
			throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_addPanelAccessibleByUser: Failed to check if panel is already in list. Try again.")); 
		}
		if (iPanelInACL > 0) {
			continue; // panel already exists
		}
		sLevel = iNofLevels >= i ? level[i] : iNofLevels > 0 ? level[1] : "";
		sDomain = iNofDomains >= i ? domain[i] : iNofDomains > 0 ? domain[1] : "";
		sEntry = unPanelAccessControl_getAccessControlEntryFromParam(panel[i], sLevel, sDomain);
		dynAppend(dsNewEntries, sEntry);
	}
	unPanelAccessControl_addPanelAccessControlEntry(dsNewEntries);
}

// unPanelAccessControl_addPanelAccessibleByOperator
/**
Purpose:
To add entry to Panel Access Control list
	@param panel: dyn_string, input, paths of panels to add
	@param domain: dyn_string, input, domains for panels to add. Pass only one to apply to all. [optional]

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_addPanelAccessibleByOperator(dyn_string panel, dyn_string domain = makeDynString()) {
	unPanelAccessControl_addPanelAccessibleByUser(panel, makeDynString(UN_USER_OPERATOR), domain);	
}

// unPanelAccessControl_addPanelAccessibleByExpert
/**
Purpose:
To add entry to Panel Access Control list
	@param panel: dyn_string, input, paths of panels to add
	@param domain: dyn_string, input, domains for panels to add. Pass only one to apply to all. [optional]

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_addPanelAccessibleByExpert(dyn_string panel, dyn_string domain = makeDynString()) {
	unPanelAccessControl_addPanelAccessibleByUser(panel, makeDynString(UN_USER_EXPERT), domain);	
}

// unPanelAccessControl_addPanelAccessibleByAdmin
/**
Purpose:
To add entry to Panel Access Control list
	@param panel: dyn_string, input, paths of panels to add
	@param domain: dyn_string, input, domains for panels to add. Pass only one to apply to all. [optional]

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_addPanelAccessibleByAdmin(dyn_string panel, dyn_string domain = makeDynString()) {
	unPanelAccessControl_addPanelAccessibleByUser(panel, makeDynString(UN_USER_ADMIN), domain);	
}

// unPanelAccessControl_deletePanelAccessLevel
/**
Purpose:
To add entry to Panel Access Control list
	@param panel: dyn_string, input, paths of panels to delete

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
        . PVSS version: 3.15
        . operating system: Linux, NT and W2008
        . distributed system: yes.
*/
public void unPanelAccessControl_deletePanelAccessLevel(dyn_string panel) {
	string pattern;
	dyn_string dsPACL = makeDynString();
	dyn_string dsMatches = makeDynString();
	unPanelAccessControl_dpGetPanelAccessControlList(dsPACL);
	int iMatchIdx, iRet, iNofMatches;
	int iNofPanels = dynlen(panel);
	int iNofEntries = dynlen(dsPACL);
	if (iNofEntries < 0 ) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_deletePanelAccessLevel: Failed to get the number of current entries.")); }
        unsigned uINT_MAX = 2147483647; // as dynlen will return an int, it is safe to say the for loop should not continue further than INT_MAX
	for (int i = 1; i <= iNofPanels && i <= uINT_MAX; i++) { 
		pattern = unPanelAccessControl_getPatternFromPath(panel[i]);	
		dsMatches = dynPatternMatch(pattern, dsPACL);
		iNofMatches = dynlen(dsMatches);
		if (iNofMatches < 0 ) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_deletePanelAccessLevel: Failed to get the number of matching entries.")); }
		if (iNofMatches == 0 ) {
			continue; // no matches
		}
		iMatchIdx = dynContains(dsPACL, dsMatches[1]); // Assume only one match
		if (iMatchIdx < 0 || iMatchIdx > iNofEntries) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccesControl_deletePanelAccessLevel: Match index out of bounds of Panel Access Control list. ["+iMatchIdx+"]")); }
		if (iMatchIdx == 0) {
			continue; // nothing to remove
		}
		if (dynRemove(dsPACL, iMatchIdx) < 0) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_deletePanelAccessLevel: Failed to delete an entry from Panel Access Control list.")); }
		iNofEntries = dynlen(dsPACL);
		if (iNofEntries < 0) { throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_deletePanelAccessLevel: Failed to update new length of Panel Access Control list.")); }
	}
	iRet = dpSetWait(gunPanelAccessControl_panelAccessControlDPE, dsPACL);
	if (iRet < 0) {
		throw(makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unPanelAccessControl_deletePanelAccessControlLevel: Failed to set new Panel Access Control list."));
	}
}
