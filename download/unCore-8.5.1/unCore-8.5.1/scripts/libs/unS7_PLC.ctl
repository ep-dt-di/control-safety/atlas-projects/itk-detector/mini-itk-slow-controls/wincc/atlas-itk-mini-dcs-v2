/**@name LIBRARY: unS7_PLC.ctl

@author: Herve Milcent (EN/ICE)

Creation Date: 23 02 2009

Modification History:

version 1.0

External Function : 
  . S7_PLC_ObjectListGetValueTime
  . S7_PLC_MenuConfiguration
  . S7_PLC_HandleMenu
  . S7_PLC_WidgetRegisterCB
  . S7_PLC_WidgetDisconnection

Internal Functions :

Purpose: This library contains S7_PLC device functions.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in S7_PLC faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

//@{

//---------------------------------------------------------------------------------------------------------------------------------------

// S7_PLC_ObjectListGetValueTime
/**
Purpose: Function called from the objectList to return the time and value

Parameters:
  - sDeviceName, string, input, device name
  - sDeviceType, string, input, unicos object
  - dsReturnData, dyn_string, output, return value

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

S7_PLC_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  _UnPlc_ObjectListGetValueTime(sDeviceName, sDeviceType, dsReturnData);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// S7_PLC_MenuConfiguration
/**
Purpose: configuration of popup menu

Parameters:
  - sDpName, string, input, device name
  - sDpType, string, input, unicos object
  - dsAccessOk, dyn_string, input, authorized action
  - menuList, dyn_string, output, menu to pop-up
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Constants UN_POPUPMENU_****
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
S7_PLC_MenuConfiguration(string sDpName, string sDpType, dyn_string dsAccessOk, dyn_string &menuList)
{
//  DebugN(sDpName, sDpType, dsAccessOk);
  _UnPlc_MenuConfiguration(sDpName, sDpType, dsAccessOk, menuList);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// S7_PLC_HandleMenu
/**
Purpose: handle the answer of the popup menu

Parameters:
  - deviceName, string, input, the device dp name
  - sDpType, string, input, the device dp type
  - menuList, dyn_string, input, list of requested action
  - menuAnswer, int, input, selected action from the menuList
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Constants UN_POPUPMENU_****
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
S7_PLC_HandleMenu(string deviceName, string sDpType, dyn_string menuList, int menuAnswer)
{
  _UnPlc_HandleMenu(deviceName, sDpType, menuList, menuAnswer);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// S7_PLC_WidgetRegisterCB
/**
Purpose: register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in S7_PLC faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

S7_PLC_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
  _UnPlc_WidgetRegisterCB(sDp, bSystemConnected);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// S7_PLC_WidgetDisconnection
/**
Purpose: Animate widget disconnection

Parameters:
  - sWidgetType, string, input, widget type. 
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in S7_PLC faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

S7_PLC_WidgetDisconnection(string sWidgetType)
{
  _UnPlc_WidgetDisconnection(sWidgetType);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// S7_PLC_FaceplateRegisterCB
/**
Purpose: faceplate register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in S7_PLC faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

S7_PLC_FaceplateRegisterCB(string sDp, bool bSystemConnected)
{
  _UnPlc_FaceplateRegisterCB(sDp, bSystemConnected);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// S7_PLC_FaceplateDisconnection
/**
Purpose: Animate Faceplate disconnection

Parameters:
   
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in S7_PLC faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

S7_PLC_FaceplateDisconnection()
{
  _UnPlc_FaceplateDisconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------
//@}

