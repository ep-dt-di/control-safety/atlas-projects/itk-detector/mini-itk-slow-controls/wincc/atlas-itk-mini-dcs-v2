/**@name LIBRARY: unSOFT_FE.ctl

@author: Herve Milcent (EN/ICE)

Creation Date: 23 02 2009

Modification History:

version 1.0

External Function : 
  . SOFT_FE_ObjectListGetValueTime
  . SOFT_FE_MenuConfiguration
  . SOFT_FE_HandleMenu
  . SOFT_FE_WidgetRegisterCB
  . SOFT_FE_WidgetDisconnection

Internal Functions :

Purpose: This library contains SOFT_FE device functions.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in SOFT_FE faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

//@{

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_ObjectListGetValueTime
/**
Purpose: Function called from the objectList to return the time and value

Parameters:
  - sDeviceName, string, input, device name
  - sDeviceType, string, input, unicos object
  - dsReturnData, dyn_string, output, return value

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

SOFT_FE_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  string sTime="0", sSystemName, sFrontEnd;
  int iCom;
  bool bEnable;
  string sWarningLetter, sWarningColor, sBodyColor="unDataNoAccess", sText = "???", sCompleteText;
  dyn_string dsColor;
  
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(sDeviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(sDeviceName);
  if(dpExists(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm")) {
    dpGet(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm", iCom,
          sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".enabled", bEnable,
          sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm:_online.._stime", sTime);
    unGenericObject_getFrontEndColorState(makeDynString(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm"), dsColor);
    unGenericObject_getFrontState(makeDynString("COM ERR"),
                                  makeDynString(dsColor[1]), 
                                  makeDynInt(iCom), 
                                  iCom, bEnable, sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
  }
  
  dsReturnData[1] = sTime;
  dsReturnData[2] = sText;
  dsReturnData[3] = "FALSE";
  dsReturnData[4] = sCompleteText;
  dsReturnData[5] = sBodyColor;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_MenuConfiguration
/**
Purpose: configuration of popup menu

Parameters:
  - sDpName, string, input, device name
  - sDpType, string, input, unicos object
  - dsAccessOk, dyn_string, input, authorized action
  - menuList, dyn_string, output, menu to pop-up
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Constants UN_POPUPMENU_****
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
SOFT_FE_MenuConfiguration(string sDpName, string sDpType, dyn_string dsAccessOk, dyn_string &menuList)
{
//  DebugN(sDpName, sDpType, dsAccessOk);
  _UnPlc_MenuConfiguration(sDpName, sDpType, dsAccessOk, menuList);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_HandleMenu
/**
Purpose: handle the answer of the popup menu

Parameters:
  - deviceName, string, input, the device dp name
  - sDpType, string, input, the device dp type
  - menuList, dyn_string, input, list of requested action
  - menuAnswer, int, input, selected action from the menuList
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Constants UN_POPUPMENU_****
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
SOFT_FE_HandleMenu(string deviceName, string sDpType, dyn_string menuList, int menuAnswer)
{
  _UnPlc_HandleMenu(deviceName, sDpType, menuList, menuAnswer);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_WidgetRegisterCB
/**
Purpose: register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in SOFT_FE faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

SOFT_FE_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction;
  dyn_string exceptionInfo;
  string sFrontEnd, sSystInt;
  bool bRemote;
  
  deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
  if (deviceName == "")  // In case of disconnection
  {
    deviceName = g_sDpName;
  }
  g_bUnSystemAlarmPlc = false;
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);

  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;
    
  if(sFrontEnd != ""){
    sSystInt = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
//DebugN(sSystemName, sFrontEnd);
    if(dpExists(sSystInt)) {
      g_bUnSystemAlarmPlc = true;
    }
  }

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
//DebugN("REG", deviceName, sFrontEnd, sSystInt);
  switch (iAction)
    {
    case UN_ACTION_DISCONNECT:
      SOFT_FE_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPCONNECT:
      g_sDpName = deviceName;
      SOFT_FE_WidgetConnect(deviceName, sFrontEnd, sSystInt);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      SOFT_FE_WidgetDisconnect(deviceName, sFrontEnd, sSystInt);
      SOFT_FE_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_WidgetDisconnection
/**
Purpose: Animate widget disconnection

Parameters:
  - sWidgetType, string, input, widget type. 
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in SOFT_FE faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

SOFT_FE_WidgetDisconnection(string sWidgetType)
{
  _UnPlc_WidgetDisconnection(sWidgetType);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_WidgetConnect
/**
Purpose: widget connect function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables defined in Analog faceplate
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

SOFT_FE_WidgetConnect(string deviceName, string sFrontEnd, string sSystInt)
{
  int iRes;
  string sSystemName;
  dyn_string dsColor;
  
//  check if the device name is of type "SOFT_FE" if not like of no PLC. -> not available.
  if((dpTypeName(deviceName) != "SOFT_FE") && (dpTypeName(deviceName) != "DIP") && (dpTypeName(deviceName) != "OPC") && (dpTypeName(deviceName) != "BACnet"))
    g_bUnSystemAlarmPlc = false;
  
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  if(g_bUnSystemAlarmPlc)
  {
    unGenericObject_getFrontEndColorState(makeDynString(sSystInt+".alarm"), dsColor);
    g_sCommColor = dsColor[1];
    iRes = dpConnect("SOFT_FE_WidgetCB", sSystInt+".alarm", sSystInt+".enabled");
    g_bCallbackConnected = (iRes >= 0);    
  }else{
    SOFT_FE_WidgetDisconnection(g_sWidgetType);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_WidgetDisconnect
/**
Purpose: widget disconnect function 

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables defined in Analog faceplate
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

SOFT_FE_WidgetDisconnect(string deviceName, string sFrontEnd, string sSystInt)
{
  int iRes;
  string sSystemName;

  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  if(g_bUnSystemAlarmPlc)
  {
    iRes = dpDisconnect("SOFT_FE_WidgetCB", sSystInt+".alarm", sSystInt+".enabled");
    g_bCallbackConnected = !(iRes >= 0);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_WidgetCB
/**
Purpose: widget animation function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

@reviewed 2018-06-22 @whitelisted{Callback}

Constraints:
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

SOFT_FE_WidgetCB(string sDpSystemIntegrityAlarmValue, int iFESystemIntegrityAlarmValue, 
                    string sDpSystemIntegrityAlarmEnabled, bool bFESystemIntegrityAlarmEnabled)
{
//DebugTN(sDpSystemIntegrityAlarmValue, iFESystemIntegrityAlarmValue, sDpSystemIntegrityAlarmEnabled, bFESystemIntegrityAlarmEnabled);
  string sWarningLetter, sWarningColor, sBodyColor="white", sText = "???", sCompleteText;
  if(g_bSystemConnected) {
    unGenericObject_getFrontState(makeDynString("COM ERR"),
                                  makeDynString(g_sCommColor),
                                  makeDynInt(iFESystemIntegrityAlarmValue), 
                                  iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled,
                                  sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
    setMultiValue("WidgetArea", "visible", true, 
                  "WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor, 
                  "Body1", "foreCol", sBodyColor, "Body1", "text", sText);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_FaceplateRegisterCB
/**
Purpose: faceplate register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in SOFT_FE faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

SOFT_FE_FaceplateRegisterCB(string sDp, bool bSystemConnected)
{
  _UnPlc_FaceplateRegisterCB(sDp, bSystemConnected);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// SOFT_FE_FaceplateDisconnection
/**
Purpose: Animate Faceplate disconnection

Parameters:
   
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in SOFT_FE faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

SOFT_FE_FaceplateDisconnection()
{
  _UnPlc_FaceplateDisconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------
//@}

