/**@file
 * unSelectDeselectDeprecated.ctl
 *
 * Creation date: 2018-06-22
 *
 * Purpose:
 * This library contains deprecated functions from unSelectDeselect libraries, 
 * that may be eventually removed from component package.
 *
 */

//------------------------------------------------------------------------------------------------------------------------
// _unSelectDeselectHMI_get_dpElementSelected
/** indivisible function: to get the last dp selected.

@par Constraints:
  - global variable: the following variable are declared:
    . g_dpElementSelected: string, to store the latest selected element
    . g_openedModule: dyn_string, to store the opened module
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpElementName output, the device data point element name
@param sModuleName input, the module name
@return 0

@deprecated 2018-06-22
*/
int _unSelectDeselectHMI_get_dpElementSelected(string &sDpElementName, string sModuleName) 
{
  FWDEPRECATED();
  
  int pos;
  dyn_string split;
  
//DebugN("_unSelectDeselectHMI_get_dpElementSelected", sModuleName, g_openedModule);
  split = strsplit(sModuleName, ";");
  pos = dynContains(g_openedModule, split[1]);
  if(pos > 0) 
    sDpElementName = g_dpElementSelected[pos];
  else
    sDpElementName = "";
  return 0;
}

