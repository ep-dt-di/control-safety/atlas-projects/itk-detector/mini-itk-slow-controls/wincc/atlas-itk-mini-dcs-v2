/**@name LIBRARY: unSetDeviceDPEConfigs.ctl

@author: Herve Milcent (EN-ICE)

Creation Date: 06/02/2009

Modification History: 

version 1.1

External Functions: 
  . unSetDeviceDPEConfigs_initialize
  . unSetDeviceDPEConfigs_displayCallBack
  . unSetDeviceDPEConfigs_handleEvent
  
Internal Functions: 

Purpose: 
This library contains the functions to be used in the panel unSetDeviceDPEConfigs.pnl.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . use the unDeviceTree.pnl widget
  . PVSS version: 3.6 
  . PVSS manager type: WCCOAui
  . operating system: WXP, Linux.
  . distributed system: yes.
*/

//@{

//--------------------------------------------------------------------------------------------------------------------------------
//unSetDeviceDPEConfigs_initialize
/**
Purpose: Initialise function.

Parameters:
	- sIdentifier: string, input, device identifier
	- sDpType, string, input, object
	- exceptionInfo: dyn_string, output, details of any exceptions are returned here

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP.
	. PVSS manager type: WCCOAui
	. distributed system: yes.
  
@reviewed 2018-09-17 @whitelisted{LiteralString}
*/
unSetDeviceDPEConfigs_initialize()
{
//  DebugTN("unSetDeviceDPEConfigs_initialize");
}

//--------------------------------------------------------------------------------------------------------------------------------
//unSetDeviceDPEConfigs_displayCallBack
/**
Purpose: Callback function trigger from the unDeviceTree panel widget

Parameters:
  - sCommandDp: string, input, callback DP
  - iCommand, int, input, the command to execute
  - sSystemNameDp: string, input, callback DP
  - sSystemName, string, input, the system name to use for the iCommand
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP.
	. PVSS manager type: WCCOAui
	. distributed system: yes.
  
@reviewed 2018-09-17 @whitelisted{LiteralString}
*/
unSetDeviceDPEConfigs_displayCallBack(string sCommandDp, int iCommand, string sSystemNameDp, string sSystemName)
{
//DebugTN("unSetDeviceDPEConfigs_displayCallBack start");
  unTreeDeviceOverview_displayCallBackTreeDeviceWidget(sCommandDp, iCommand, sSystemNameDp, sSystemName);
//DebugTN("unSetDeviceDPEConfigs_displayCallBack end");
}

//--------------------------------------------------------------------------------------------------------------------------------
//unSetDeviceDPEConfigs_handleEvent
/**
Purpose: Function triggerd by the unDeviceTree widget to handle the event from the device tree

Parameters:
  - sCommand: string, input, the command to execute
  - sSelectedKey, string, input, the key of the selected node of the device tree
  - sCompositeLabel: string, input, the composite label of the selected node of the device tree
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP.
	. PVSS manager type: WCCOAui
	. distributed system: yes.
  
@reviewed 2018-09-17 @whitelisted{LiteralString}
*/
unSetDeviceDPEConfigs_handleEvent(string sCommand, string sSelectedKey, string sCompositeLabel)
{
  switch(sCommand) {
      case DEVUN_TREEDISPLAY_NODELEFTCLICK:
        treeDevKey_graph.text = sSelectedKey;
        treeDevCompositeLabel_graph.text = sCompositeLabel;
        triggerCommand.text = sCommand;
//  DebugTN("unSetDeviceDPEConfigs_handleEvent: nodeleft", sCommand, sSelectedKey, sCompositeLabel);
        break;
      default:
        break;
    }
}

//--------------------------------------------------------------------------------------------------------------------------------

//@}
