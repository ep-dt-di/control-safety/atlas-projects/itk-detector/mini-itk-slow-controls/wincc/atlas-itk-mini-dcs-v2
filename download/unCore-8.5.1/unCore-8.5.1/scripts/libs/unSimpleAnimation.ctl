/**@file

Those files generify routine of register/create/receive callback.
The routine is implemented for widget, faceplate and buttons.

Basic structure:
- ...DPEs specifies list of DPEs that are used for animation
- ...LockDPEs specifies list DPEs with alert handlers that can be modified (interlocks should not be in this list)
- ...RegisterCB does standart routine on callback registration; according to the LockDPE list it connects to ...LockCallBack() or ...Connect()
- ...LockCallBack intermediate callback for LockDPEs; connects to ...Connect()
- ...Connect calls ...InitStatics() and dpConnect to ...AnimationCB()
- ...Disconnection does dpDisconnect for DPEs
- ...Disconnect in charge of disconnect animation

@author
  Alexey Merezhin (EN-ICE-PLC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

*/

// #### WIDGET FUNCTIONS ####

unSimpleAnimation_WidgetInit(string sIdentifier, string sDpType, dyn_string &exceptionInfo)
{
    g_sDpType = sDpType;

    unGenericObject_WidgetInit(sIdentifier, sDpType, exceptionInfo);
}


/**Fetch widget DPEs from the device.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param frontEnd front-end name
*/
dyn_string unSimpleAnimation_WidgetDPEs(string deviceName, string frontEnd) {
  dyn_string dpes = makeDynString(frontEnd+".alarm", frontEnd+".enabled",
                                  deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                                  deviceName + ".statusInformation.selectedManager",
                                  deviceName + ".statusInformation.deviceLogActive");
  string function = unSimpleAnimation_WidgetDevicePrefix(deviceName) + "_WidgetDPEs";
  if (isFunctionDefined(function)) {
    evalScript(dpes, "dyn_string main(string deviceName, dyn_string dpes) {" + 
                     function  + "(deviceName, dpes);" +
                     "return dpes; }", makeDynString(), deviceName, dpes);
  }
  return dpes;
}

/**Fetch widget LockDPEs from the device.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
*/
dyn_string unSimpleAnimation_WidgetLockDPEs(string deviceName) {
  dyn_string dpes;
  string function = unSimpleAnimation_WidgetDevicePrefix(deviceName) + "_WidgetLockDPEs";
  if (isFunctionDefined(function)) {
    evalScript(dpes, "dyn_string main(string deviceName, dyn_string dpes) {" + 
                     function  + "(deviceName, dpes);" +
                     "return dpes; }", makeDynString(), deviceName, dpes);
  }
  return dpes;
}

/**widget register callback
Does not work. Need to extend unGenericObject_WidgetInit to pass sDpType parameter.
Otherwise impossible to call disconnection function if system is never being connected.

@todo update unCore for this
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@reviewed 2018-06-22 @whitelisted{Callback}

@param sDp the DistributedControl system name DP name
@param bSystemConnected the state of the system name
*/
void unSimpleAnimation_WidgetRegisterCB(string sDp, bool bSystemConnected) {
  string deviceName, systemName, frontEnd;
  int iAction, iRes;
  dyn_string exceptionInfo;
  bool bRemote;

  deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
  if (deviceName == "") deviceName = g_sDpName; // In case of disconnection
  systemName = unGenericDpFunctions_getSystemName(deviceName);
  frontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
  if (frontEnd != "") { frontEnd = systemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + frontEnd; }

  unDistributedControl_isRemote(bRemote, systemName);
  g_bSystemConnected = bRemote ? bSystemConnected : true;
  g_bUnSystemAlarmPlc = frontEnd != "" && dpExists(frontEnd);
  if (deviceName != "") {
      try {
        if (dpTypeName(deviceName) != g_sDpType) { g_bUnSystemAlarmPlc = false; }
      } catch {
        DebugTN("unSimpleAnimation_WidgetRegisterCB: g_sDpType is not defined!");
      }
  }

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  dyn_string lockDPEs;
  switch (iAction) {
    case UN_ACTION_DISCONNECT:
      unSimpleAnimation_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPCONNECT:
      g_sDpName = deviceName;
      if (g_bUnSystemAlarmPlc) {
        lockDPEs = unSimpleAnimation_WidgetLockDPEs(deviceName);
        if (dynlen(lockDPEs) > 0) {
          iRes = dpConnect("unSimpleAnimation_WidgetLockCallBack", lockDPEs);
          g_bCallbackConnected = (iRes >= 0);
        } else {
          unSimpleAnimation_WidgetConnect(deviceName, frontEnd);
        }
      } else {
        unSimpleAnimation_WidgetDisconnection(g_sWidgetType);
      }
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      lockDPEs = unSimpleAnimation_WidgetLockDPEs(deviceName);
      if (dynlen(lockDPEs) > 0) {
        if (g_bUnSystemAlarmPlc) {
          iRes = dpDisconnect("unSimpleAnimation_WidgetLockCallBack", lockDPEs);
        }
        if (g_bDisconnectCB) {
          unSimpleAnimation_WidgetDisconnect(deviceName, frontEnd);
        }
        g_bCallbackConnected = !(iRes >= 0);
        g_bDisconnectCB = false;
      } else {
        unSimpleAnimation_WidgetDisconnect(deviceName, frontEnd);
      }
      unSimpleAnimation_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_NOTHING:
      break;
    default:
      break;
  }
}

/**dpConnect to the alert handler's lock properties
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL
  
@reviewed 2018-09-05 @whitelisted{Callback}

@param dpes lock names
@param values lock values
*/
void unSimpleAnimation_WidgetLockCallBack(dyn_string dpes, dyn_bool values) {
    int iRes;
    string deviceName = unGenericDpFunctions_getDpName(dpes[1]);
  bool bLockState = false;
  for (int i = 1; i <= dynlen(values); i++) {
    bLockState = bLockState || values[i];
  }
  string frontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
  string  systemName = unGenericDpFunctions_getSystemName(deviceName);
  if (frontEnd != "") frontEnd = systemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+frontEnd;

    if (bLockState) { // lock set so disconnect if not already disconnected
    // no need to check if it was already set before because it is not possible to do 2 dpSet on the lock the Ev reject the message
        unSimpleAnimation_WidgetDisconnect(deviceName, frontEnd);
        unSimpleAnimation_WidgetDisconnection(deviceName);
    } else { // lock not set so connect if not already connected
    // no need to check if it was already set before because it is not possible to do 2 dpSet on the lock the Ev reject the message
        unSimpleAnimation_WidgetConnect(deviceName, frontEnd);
    }
}

/**Widget connect routine.

Init animation statics (with $DT$_WidgetInitStatics function) and does dpConnect for unSimpleAnimation_WidgetDPEs.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param frontEnd front-end name
*/
void unSimpleAnimation_WidgetConnect(string deviceName, string frontEnd) {
  int iAction, iRes;
  dyn_string exceptionInfo, lastError;
  
  if (g_bUnSystemAlarmPlc) {    
    string function = unSimpleAnimation_WidgetDevicePrefix(deviceName) + "_WidgetInitStatics";
    if (isFunctionDefined(function)) {
      execScript("main(string deviceName) {" +  function  + "(deviceName); }", makeDynString(), deviceName);
    }

    dyn_string dpes = unSimpleAnimation_WidgetDPEs(deviceName, frontEnd);
    iRes = dpConnect("unSimpleAnimation_WidgetAnimationCB", dpes);
    
    lastError = getLastError();
    if (dynlen(lastError) > 0) {
      DebugN(lastError);
      fwException_raise(exceptionInfo,"ERROR", "unSimpleAnimation_WidgetConnect: dpConnect error: " + lastError, "EMPTY");
    }
    g_bCallbackConnected = (iRes >= 0) && dynlen(lastError) == 0;
  } else {
    unSimpleAnimation_WidgetDisconnection(deviceName);
  }
}

/**Widget disconnect routine.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param frontEnd front-end name
*/
void unSimpleAnimation_WidgetDisconnect(string deviceName, string frontEnd) {
  int iRes;
  
  if(g_bUnSystemAlarmPlc) {
    dyn_string dpes = unSimpleAnimation_WidgetDPEs(deviceName, frontEnd);
    iRes = dpDisconnect("unSimpleAnimation_WidgetAnimationCB", dpes);

    g_bCallbackConnected = !(iRes >= 0);
  }
}

/**Widget animation routine.

This functions is an animation callback. 
It tries to animate a widget with following function respection an order (if first exists, than just with first):
-# $DT$_WidgetAnimation_$WDIGET_TYPE$
-# $DT$_WidgetAnimation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@reviewed 2018-06-22 @whitelisted{Callback}

@param dpes dpes names
@param values dpes values
*/
void unSimpleAnimation_WidgetAnimationCB(dyn_string dpes, dyn_anytype values) {
  string devicePrefix = unSimpleAnimation_WidgetDevicePrefix(dpes[3]);
  string function = devicePrefix + "_WidgetAnimation" + "_" + g_sWidgetType;
  if (g_sWidgetType != "" && isFunctionDefined(function)) { // TODO: test if g_sWidgetType != "" works
    execScript("main(dyn_string dpes, dyn_anytype values) {" + function + "(dpes, values);}", makeDynString(), dpes, values);
  } else {
    function = devicePrefix + "_WidgetAnimation";
    if (isFunctionDefined(function)) {
      execScript("main(dyn_string dpes, dyn_anytype values, string widgetType) {" + function + "(dpes, values, widgetType);}", makeDynString(), dpes, values, g_sWidgetType);
    } else {
      DebugN("Can't find widget animation function", function);
    }
  }
}

/**Widget disconnection routine.

@todo update description
@todo fix me!!!

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void unSimpleAnimation_WidgetDisconnection(string deviceName) {
  string devicePrefix = unSimpleAnimation_WidgetDevicePrefix(deviceName);
  string function = devicePrefix + "_WidgetDisconnection" + "_" + g_sWidgetType;

  if (g_sWidgetType != "" && isFunctionDefined(function)) { // TODO: test if g_sWidgetType != "" works
    execScript("main(string widgetType) {" +  function  + "(widgetType); }", makeDynString(), g_sWidgetType);
  } else {
    function = devicePrefix + "_WidgetDisconnection";
    if (isFunctionDefined(function)) {
      execScript("main(string widgetType) {" +  function  + "(widgetType); }", makeDynString(), g_sWidgetType);
    } else {
      DebugN("Can't find widget disconnection function", function);
    }
  }
}

// #### FACEPLATE FUNCTIONS ####
/**Fetch faceplate DPEs from the device.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param frontEnd front-end name
*/
dyn_string unSimpleAnimation_FaceplateDPEs(string deviceName, string frontEnd) {
  dyn_string dpes = makeDynString(frontEnd+".alarm", frontEnd+".enabled");
  string function = unSimpleAnimation_DevicePrefix(deviceName) + "_FaceplateDPEs";
  if (isFunctionDefined(function)) {
    evalScript(dpes, "dyn_string main(string deviceName, dyn_string dpes) {" + 
                     function  + "(deviceName, dpes);" +
                     "return dpes; }", makeDynString(), deviceName, dpes);
  }
  return dpes;
}

/**Fetch faceplate LockDPEs from the device.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
*/
dyn_string unSimpleAnimation_FaceplateLockDPEs(string deviceName) {
  dyn_string dpes;
  string function = unSimpleAnimation_DevicePrefix(deviceName) + "_FaceplateLockDPEs";
  if (isFunctionDefined(function)) {
    evalScript(dpes, "dyn_string main(string deviceName, dyn_string dpes) {" + 
                     function  + "(deviceName, dpes);" +
                     "return dpes; }", makeDynString(), deviceName, dpes);
  }
  return dpes;
}

/**Faceplate register routine 

Faceplate DistributedControl callback of the faceplate status panel.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@reviewed 2018-06-22 @whitelisted{Callback}

@param sDp the DistributedControl system name DP name
@param bSystemConnected the state of the system name
*/
void unSimpleAnimation_FaceplateStatusRegisterCB(string sDp, bool bSystemConnected) {
  string deviceName, systemName, frontEnd;
  int iAction, iRes;
  dyn_string exceptionInfo;
  bool bRemote;
  dyn_string lockDPEs;
  
  deviceName = unGenericDpFunctions_getDpName($sDpName);
  systemName = unGenericDpFunctions_getSystemName(deviceName);
  frontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
  unDistributedControl_isRemote(bRemote, systemName);
  g_bSystemConnected = bRemote ? bSystemConnected : true;
  g_bUnSystemAlarmPlc = frontEnd != "" && dpExists(systemName+c_unSystemAlarm_dpPattern+DS_pattern+frontEnd);
  
  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  switch (iAction) {
    case UN_ACTION_DISCONNECT:
      unSimpleAnimation_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_DPCONNECT:
      if (g_bUnSystemAlarmPlc) {
        lockDPEs = unSimpleAnimation_FaceplateLockDPEs(deviceName);
        if (dynlen(lockDPEs) > 0) {
          iRes = dpConnect("unSimpleAnimation_FaceplateLockCallBack", lockDPEs);
          g_bCallbackConnected = (iRes >= 0);
        } else {
          unSimpleAnimation_FaceplateConnect(deviceName);
        }
      } else {
        unSimpleAnimation_FaceplateStatusDisconnection();
      }
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      lockDPEs = unSimpleAnimation_FaceplateLockDPEs(deviceName);
      if (dynlen(lockDPEs) > 0) {
        if (g_bUnSystemAlarmPlc) {
          iRes = dpDisconnect("unSimpleAnimation_FaceplateLockCallBack", lockDPEs);
        }
        unSimpleAnimation_FaceplateDisconnect(deviceName);
        g_bCallbackConnected = !(iRes >= 0);
      } else {
        unSimpleAnimation_FaceplateDisconnect(deviceName);
      }
      unSimpleAnimation_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}

/**Faceplate callback of the alert handler's lock properties
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@reviewed 2018-06-22 @whitelisted{Callback}

@param dpes lock names
@param values lock values
*/
void unSimpleAnimation_FaceplateLockCallBack(dyn_string dpes, dyn_bool values) {
    int iRes;
    string deviceName;
  bool bLockState = false;
  for (int i = 1; i <= dynlen(values); i++) {
    bLockState = bLockState || values[i];
  }
    deviceName = unGenericDpFunctions_getDpName(dpes[1]);
    if (bLockState) { // lock set so disconnect if not already disconnected
        unSimpleAnimation_FaceplateDisconnect(deviceName);
        unSimpleAnimation_FaceplateStatusDisconnection();
    } else { // lock not set so connect if not already connected
        unSimpleAnimation_FaceplateConnect(deviceName);
    }
}

/**Faceplate connect routine.

Init animation statics (with $DT$_FaceplateInitStatics function) and does dpConnect for unSimpleAnimation_FaceplateDPEs.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
*/
void unSimpleAnimation_FaceplateConnect(string deviceName) {
  int iAction, iRes;
  dyn_string exceptionInfo, lastError;
  string frontEnd, systemName;
  
  g_sDeviceAlias = unGenericDpFunctions_getAlias(deviceName);
  frontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
  systemName = unGenericDpFunctions_getSystemName(deviceName);
  g_bUnSystemAlarmPlc = frontEnd != "" && dpExists(systemName+c_unSystemAlarm_dpPattern+DS_pattern+frontEnd);

  if (g_bUnSystemAlarmPlc) {    
    frontEnd = systemName+c_unSystemAlarm_dpPattern+DS_pattern+frontEnd;
    string function = unSimpleAnimation_DevicePrefix(deviceName) + "_FaceplateInitStatics";
    if (isFunctionDefined(function)) {
      execScript("main(string deviceName) {" +  function  + "(deviceName);}", makeDynString(), deviceName);
    }

    dyn_string dpes = unSimpleAnimation_FaceplateDPEs(deviceName, frontEnd);
    iRes = dpConnect(unSimpleAnimation_DevicePrefix(deviceName) + "_FaceplateStatusAnimationCB", dpes);
    
    lastError = getLastError();
    g_bCallbackConnected = (iRes >= 0) && dynlen(lastError) == 0;
    if (dynlen(lastError) > 0) {
      DebugN(lastError);
      fwException_raise(exceptionInfo,"ERROR", "unSimpleAnimation_FaceplateConnect: dpConnect error: " + lastError, "EMPTY");
    }

    g_bCallbackConnected = (iRes >= 0);
  } else {
    unSimpleAnimation_FaceplateStatusDisconnection();
  }
}

/**Faceplate disconnection routine.

If device has $DT$_FaceplateStatusDisconnection that it will be triggered.
Otherwise $DT$_FaceplateStatusAnimationCB will be called.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void unSimpleAnimation_FaceplateStatusDisconnection() {
  string deviceName = unGenericDpFunctions_getDpName($sDpName);
  string function = unSimpleAnimation_DevicePrefix(deviceName) + "_FaceplateStatusDisconnection";
  if (isFunctionDefined(function)) {
    execScript("main(string deviceName) {" +  function  + "(deviceName); }", makeDynString(), deviceName);
  } else {
    function = unSimpleAnimation_DevicePrefix(deviceName) + "_FaceplateStatusAnimationCB";
    if (isFunctionDefined(function)) {
      execScript("main() {" +  function  + "(makeDynString(), makeDynAnytype()); }", makeDynString());
    }
  }
}

/**Faceplate disconnect routine.

DP disconnect FaceplateDPEs from $DT$_FaceplateStatusAnimationCB

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName device name
*/
void unSimpleAnimation_FaceplateDisconnect(string deviceName) {
  int iRes;
  string frontEnd, systemName;
  
  if(g_bUnSystemAlarmPlc) {
    systemName = unGenericDpFunctions_getSystemName(deviceName);
    frontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
    frontEnd = systemName+c_unSystemAlarm_dpPattern+DS_pattern+frontEnd;

    dyn_string dpes = unSimpleAnimation_FaceplateDPEs(deviceName, frontEnd);
    iRes = dpDisconnect(unSimpleAnimation_DevicePrefix(deviceName) + "_FaceplateStatusAnimationCB", dpes);

    g_bCallbackConnected = !(iRes >= 0);
  }
}

// #### BUTTON FUNCTIONS ####
/**Fetch a list of dpe that need to be connected for button animation

Those DPEs should be defined in $DT$_ButtonDPEs function.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName the device name
*/
dyn_string unSimpleAnimation_ButtonDPEs(string deviceName) {
  dyn_string dpes = makeDynString(deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                                  deviceName + ".statusInformation.selectedManager");
  string function = unSimpleAnimation_DevicePrefix(deviceName) + "_ButtonDPEs";
  if (isFunctionDefined(function)) {
    evalScript(dpes, "dyn_string main(string deviceName, dyn_string dpes) {" + 
                     function  + "(deviceName, dpes);" +
                     "return dpes; }", makeDynString(), deviceName, dpes);
  }
  return dpes;
}

/**Fetch a map of button properties for given device

This mapping is defined in $DT$_ButtonConfig function

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName the device name
*/
mapping unSimpleAnimation_ButtonConfig(string deviceName) {
  mapping buttons;
  string function = unSimpleAnimation_DevicePrefix(deviceName) + "_ButtonConfig";
  if (isFunctionDefined(function)) {
    evalScript(buttons, "mapping main(string deviceName) {" + 
                     "return " + function  + "(deviceName);" +
                     "}", makeDynString(), deviceName);
  }
  return buttons;
}

/**Return a list of button names for given device

This list is equals to keys of unSimpleAnimation_ButtonConfig mapping

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName the device name
*/
dyn_string unSimpleAnimation_ButtonList(string deviceName) {
  return mappingKeys(unSimpleAnimation_ButtonConfig(deviceName));
}

/**Button register routine 

DistributedControl callback of the contextual device button panel.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@reviewed 2018-06-22 @whitelisted{Callback}

@param sDp the DistributedControl system name DP name
@param bSystemConnected the state of the system name
*/
void unSimpleAnimation_ButtonRegisterCB(string sDp, bool bSystemConnected) {
  string deviceName, sDpType, sFunction;
  int iAction, iRes;
  dyn_string exceptionInfo, dsFunctions, dsUserAccess;
  
  deviceName = unGenericDpFunctions_getDpName($sDpName);
  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  switch (iAction) {
    case UN_ACTION_DISCONNECT:
      unSimpleAnimation_ButtonDisconnection();
      break;
    case UN_ACTION_DPCONNECT:
      sDpType = dpTypeName(deviceName);

      unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
      sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_BUTTONUSERACCESS];
             
      if (sFunction != "") {
        evalScript(dsUserAccess, "dyn_string main(string deviceNameTemp, string sDpTypeTemp) {" + 
               "dyn_string dsUserAccessReturn;" +
               "if (isFunctionDefined(\"" + sFunction  + "\"))" +
               "    {" +
               sFunction  + "(deviceNameTemp, sDpTypeTemp, dsUserAccessReturn);" +
               "    }" +
               "return dsUserAccessReturn; }", makeDynString(), deviceName, sDpType);
        g_dsUserAccess=dsUserAccess;  // keep the list of the device allowed action 
      } else {
        fwException_raise(exceptionInfo,"ERROR", "unSimpleAnimation:" + getCatStr("unGeneration","UNKNOWNFUNCTION"), "EMPTY");
      }

      iRes = dpConnect("unSimpleAnimation_ButtonAnimationCB", unSimpleAnimation_ButtonDPEs(deviceName));
      g_bCallbackConnected = (iRes >= 0);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      iRes = dpDisconnect("unSimpleAnimation_ButtonAnimationCB", unSimpleAnimation_ButtonDPEs(deviceName));
      g_bCallbackConnected = !(iRes >= 0);
      unSimpleAnimation_ButtonDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}

/**Button animation routine.

Triggers $DT$_ButtonSetState function.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@reviewed 2018-06-22 @whitelisted{Callback}

@param dpes dpes names
@param values dpes values
*/
void unSimpleAnimation_ButtonAnimationCB(dyn_string dpes, dyn_mixed values) {
  string deviceName = unGenericDpFunctions_getDpName(dpes[1]);
  // todo: remove on https://icecontrols.cern.ch/jira/browse/IS-895 closed
  for (int i = 1; i <= dynlen(dpes); i++) {
    int pos = strpos(dpes[i], ".StsReg01:_online.._value");
    if (pos == -1) pos = strpos(dpes[i], ".StsReg02:_online.._value");
    if (pos > 0) {
      values[i] = (bit32) values[i];
    }
  }
  string function = unSimpleAnimation_DevicePrefix(deviceName) + "_ButtonSetState";
  if (isFunctionDefined(function)) {
    execScript("main(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {" +  function  + "(deviceName, dpType, dsUserAccess, dsData); }",
          makeDynString(), deviceName, dpTypeName(deviceName), g_dsUserAccess, values);
  }
}

/**Button disconnection routine.

Proxy to cpcGenericObject_ButtonDisconnect

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
void unSimpleAnimation_ButtonDisconnection() {
  string deviceName = unGenericDpFunctions_getDpName($sDpName);
  dyn_string buttons = unSimpleAnimation_ButtonList(deviceName);
  
  // TODO: fix me
  // PG,20160721, to review! Added isFunctionDefined() to protect it
  if (isFunctionDefined("cpcGenericObject_ButtonDisconnect")) cpcGenericObject_ButtonDisconnect(buttons);
}

/**Returns the list of allowed action on the device for a user logged in
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName the device DP name
@param sDpType the device type
@param dsAccess list of allowed action on the device
*/
void unSimpleAnimation_ButtonUserAccess(string sDpName, string sDpType, dyn_string &dsAccess, string sDefaultDomain = "UNICOS") {
  bool operator, expert, admin;
  dyn_bool dbPermissions;
  dyn_string exceptionInfo;
  string sMoniAction, sOperAction, sExpAction, sAdminAction;

  // get the configured list of authorized action per privilege
  unGenericDpFunctions_getAccessControlPrivilegeRight(sDpName, g_dsDomainAccessControl, dbPermissions, sMoniAction, sOperAction, sExpAction, sAdminAction, exceptionInfo, sDefaultDomain);
  bool bActionDefined = (sOperAction != UNICOS_ACCESSCONTROL_DOMAINNAME) || (sExpAction != UNICOS_ACCESSCONTROL_DOMAINNAME) || (sAdminAction != UNICOS_ACCESSCONTROL_DOMAINNAME);

  operator = dbPermissions[2];
  expert = dbPermissions[3];
  admin = dbPermissions[4];
  dsAccess = makeDynString();
  
  mapping all_defaults;
  all_defaults[UN_ACCESS_RIGHTS_OPERATOR]  = makeDynString();
  all_defaults[UN_ACCESS_RIGHTS_EXPERT]    = makeDynString();
  all_defaults[UN_ACCESS_RIGHTS_ADMIN]     = makeDynString();
  all_defaults[UN_ACCESS_RIGHTS_NOONE]     = makeDynString();
  mapping buttons = unSimpleAnimation_ButtonConfig(sDpName);
  for (int i = 1; i <= mappinglen(buttons); i++) {
    dynAppend(all_defaults[mappingGetValue(buttons, i)], mappingGetKey(buttons, i));
  }
  
  if (operator) {
    if (bActionDefined)
      dynAppend(dsAccess, strsplit(sOperAction, UN_ACCESS_CONTROL_SEPARATOR));
    else
      dynAppend(dsAccess, all_defaults[UN_ACCESS_RIGHTS_OPERATOR]);
  }
  if (expert) {
    if (bActionDefined)
      dynAppend(dsAccess, strsplit(sExpAction, UN_ACCESS_CONTROL_SEPARATOR));
    else 
      dynAppend(dsAccess, all_defaults[UN_ACCESS_RIGHTS_EXPERT] );
  }
  if (admin) {
    if (bActionDefined)
      dynAppend(dsAccess, strsplit(sAdminAction, UN_ACCESS_CONTROL_SEPARATOR));
    else
      dynAppend(dsAccess, all_defaults[UN_ACCESS_RIGHTS_ADMIN]);
  }
}

// #### GENERIC FUNCTIONS ####
/**Returns the device prefix for given device.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName the device DP name
*/
string unSimpleAnimation_DevicePrefix(string deviceName) {
  return dpTypeName(deviceName);
}
string unSimpleAnimation_WidgetDevicePrefix(string deviceName) {
  try {
    if (g_sDpType != "") {
      return g_sDpType;
    }
  } catch {
    DebugTN("unSimpleAnimation_WidgetDevicePrefix: g_sDpType is not defined!");
  }

  return dpTypeName(deviceName);
}



