/**@name LIBRARY: unSystemIntegrity_BACnet.ctl

@author: Alexey Merezhin (EN/ICE)

Creation Date: 25/09/2015

version 1.0

External Functions:

Internal Functions:

Purpose:
	Library of the BACnet component used by the systemIntegrity.

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: BACnet_systemIntegrity
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

const int c_unSystemIntegrity_defaultBACnetCheckingDelay = 10;
const string UN_SYSTEM_INTEGRITY_BACNET = "BACnet";
const string UN_SYSTEM_INTEGRITY_BACNET_check = "unSystemIntegrity_BACnetCheck";

// global declaration
global dyn_string g_unSystemIntegrity_BACnet_DSList; // list of the BACnet-DS dp checked
global dyn_int g_unSystemIntegrity_BACnet_DSThreadId; // list of the BACnet-DS thread Id checking the counter
global int g_unSystemIntegrity_BACnet_CheckingDelay;
global bool g_bBACNET_NotStarted = true;

//@{

//------------------------------------------------------------------------------------------------------------------------
// BACnet_systemIntegrityInfo
/** Return the list of systemAlarm pattern.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string BACnet_systemIntegrityInfo() {
    return makeDynString(DS_pattern, DS_Time_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_BACnet_Initialize
/**
Purpose:
Get the list of BACnet check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled BACnet that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_BACnet_Initialize(dyn_string &dsResult) {
    dyn_string dsList;
    int len, i;
    string dpToCheck;
    bool enabled;

    dpGet(UN_SYSTEM_INTEGRITY_BACNET + UN_SYSTEMINTEGRITY_EXTENSION + ".config.data", dsList);
    unSystemIntegrity_BACnet_DataCallback("", dsList);

    dsList = dpNames(c_unSystemAlarm_dpPattern + DS_pattern + "*", c_unSystemAlarm_dpType);
    len = dynlen(dsList);
    for (i = 1; i <= len; i++) {
        dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
        dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern + DS_pattern), strlen(dpToCheck));
        if (dpExists(dpToCheck)) {
            if (dpTypeName(dpToCheck) == UNBACNET_DPTYPE) {
                dpGet(dsList[i] + ".enabled", enabled);
                if (enabled) {
                    dynAppend(dsResult, dpToCheck);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_BACnet_DataCallback
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param sConfigData: string, input, the config data

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_BACnet_DataCallback(string sDpe1, dyn_string dsConfigData) {
    dyn_string dsTemp = dsConfigData;

    while (dynlen(dsTemp) <= 2) {
        dynAppend(dsTemp, "0");
    }

    g_unSystemIntegrity_BACnet_CheckingDelay = (int)dsTemp[1];
    if (g_unSystemIntegrity_BACnet_CheckingDelay <= 0) {
        g_unSystemIntegrity_BACnet_CheckingDelay = c_unSystemIntegrity_defaultBACnetCheckingDelay;
    }

//DebugN(dsConfigData, g_unSystemIntegrity_BACnet_CheckingDelay);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_BACnet_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_BACnet_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters) {
    dyn_string exceptionInfo;
    int i, len = dynlen(parameters);

    switch (command) {
        case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
            for (i = 1; i <= len; i++) {
                unSystemIntegrity_BACnet_checking(parameters[i], true, true, exceptionInfo);
            }
            break;
        case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
            for (i = 1; i <= len; i++) {
                unSystemIntegrity_BACnet_checking(parameters[i], false, false, exceptionInfo);
                // remove dp from the alert list of applicationDP if it is in
                _unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern + DS_pattern + parameters[i], false, exceptionInfo);
                // delete the _UnSystemAlarm dps
                if (dpExists(c_unSystemAlarm_dpPattern + DS_pattern + parameters[i])) {
                    dpDelete(c_unSystemAlarm_dpPattern + DS_pattern + parameters[i]);
                }

                // remove dp from the alert list of applicationDP if it is in
                _unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern + DS_Time_pattern + parameters[i], false, exceptionInfo);
                // delete the _UnSystemAlarm dps
                if (dpExists(c_unSystemAlarm_dpPattern + DS_Time_pattern + parameters[i])) {
                    dpDelete(c_unSystemAlarm_dpPattern + DS_Time_pattern + parameters[i]);
                }
            }
            break;
        case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
            for (i = 1; i <= len; i++) {
                if (dpExists(c_unSystemAlarm_dpPattern + DS_pattern + parameters[i])) {
                    unSystemIntegrity_BACnet_checking(parameters[i], false, true, exceptionInfo);
                }
            }
            break;
        case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
            for (i = 1; i <= len; i++) {
                if (dpExists(c_unSystemAlarm_dpPattern + DS_pattern + parameters[i])) {
                    unSystemIntegrity_BACnet_checking(parameters[i], false, false, exceptionInfo);
                }
            }
            break;
        case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
            dpSet(UN_SYSTEM_INTEGRITY_BACNET + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.commandResult", command,
                  UN_SYSTEM_INTEGRITY_BACNET + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.result", g_unSystemIntegrity_BACnet_DSList);
            break;
        default:
            break;
    }

    if (dynlen(exceptionInfo) > 0) {
        if (isFunctionDefined("unMessageText_sendException")) {
            unMessageText_sendException("*", "*", "unSystemIntegrity_BACnet_HandleCommand", "user", "*", exceptionInfo);
        }
// handle any error uin case the send message failed
        if (dynlen(exceptionInfo) > 0) {
            DebugN(getCurrentTime(), exceptionInfo);
        }
    }

//DebugN(sDpe1, command, parameters, g_unSystemIntegrity_BACnet_DSList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_BACnet_checking
/**
Purpose:
This function register/de-register the callback funtion of BACnet manager. This function can also create the _unSystemAlarm_BACnet dp
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_BACnet_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo) {
    string sDpToChek, dp, dpTime, dpError, dpPlc, plcHostname, description;
    int res, thId, pos, drvNum, len, i;
    bool bError = false;
    dyn_string plc_plc_list;

    string alertPanel, alertHelp;
    bool configExists, isActive;
    int alertConfigType;
    dyn_string alertTexts, alertClasses, summaryDpeList, alertPanelParameters;
    dyn_float alertLimits;

// remove the system name
    res = strpos(sDp, ":");
    if (res >= 0) {
        dp = substr(sDp, res + 1, strlen(sDp));
    } else {
        dp = sDp;
    }
    sDpToChek = dp;
    dpTime = c_unSystemAlarm_dpPattern + DS_Time_pattern + dp;
    dp = c_unSystemAlarm_dpPattern + DS_pattern + dp;
// DebugN("unSystemIntegrity_BACnet_checking", dp, bCreate, bRegister, sDpToChek, dpTime);

// in case the Dp is not of type _UnPlc do nothing. Should not happen, just for security.
    if (dpTypeName(sDpToChek) != UNBACNET_DPTYPE) {
        return;
    }
    string backnet_device = "_" + substr(sDpToChek, 7); // remove BACnet_ prefix from sDpToChek => get front end name
    string backnet_pool_group = BACNET_POLL_PREFIX + substr(sDpToChek, 7);

    if (bCreate) {
// get the plc hostname and the driver number
        plcHostname = unGenericDpFunctions_getAlias(sDpToChek);
// create the sDp and its alarm config if it is not existing
        if (!dpExists(dp)) {
            description = getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + plcHostname + " -> DS driver " + drvNum;
            unSystemIntegrity_createSystemAlarm(sDpToChek, DS_pattern, description, exceptionInfo);
        }
    }

    if (bRegister) {
// connect to the callback function if not already done
        pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_BACnet_DSList, dp);
        if (pos <= 0) {
            dpSetWait(backnet_device + ".Active", true,
                      backnet_pool_group + ".Active", true);
            // add it in the list, because one of the callback function needs it.
            pos = _unSystemIntegrity_setList(g_unSystemIntegrity_BACnet_DSList, dp, true);

// start the thread for checking the counter
            thId = startThread(UN_SYSTEM_INTEGRITY_BACNET_check, sDpToChek, drvNum);
            g_unSystemIntegrity_BACnet_DSThreadId[pos] = thId;
            if (thId < 0) {
                bError = true;
            }

            if (bError) {

                pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_BACnet_DSList, dp);
                if (pos > 0) {
                    // kill the threads if they were started
                    res = stopThread(g_unSystemIntegrity_BACnet_DSThreadId[pos]);
// and then remove it from the list
                    pos = _unSystemIntegrity_setList(g_unSystemIntegrity_BACnet_DSList, dp, false);
//DebugN("rmv", res);
                    dynRemove(g_unSystemIntegrity_BACnet_DSThreadId, pos);
                }

                fwException_raise(exceptionInfo, "ERROR",
                                  "unSystemIntegrity_BACnet_checking():" + getCatStr("unSystemIntegrity", "CANNOT_DP_CONNECT") + dp, "");
            } else {
//DebugN("Enable", g_unSystemIntegrity_BACnet_DSThreadId, "end");
                // set the enable to true and activate the alarm if it is not activated.
                dpSet(dp + ".enabled", true);
                unAlarmConfig_mask(dp + ".alarm", false, exceptionInfo);
                // get all the PLC-PLC dp, and enable them
                plc_plc_list = dpNames(c_unSystemAlarm_dpPattern + PLC_PLC_pattern + sDpToChek + "*", c_unSystemAlarm_dpType);
                len = dynlen(plc_plc_list);
                for (i = 1; i <= len; i++) {
                    dpSet(plc_plc_list[i] + ".enabled", true);
                    unAlarmConfig_mask(plc_plc_list[i] + ".alarm", false, exceptionInfo);
                }

                // get all the FESystemAlarm dp, and enable them, check the Ok state and set the correct value
                plc_plc_list = dpNames(c_unSystemAlarm_dpPattern + FE_pattern + sDpToChek + "*", c_unSystemAlarm_dpType);
                len = dynlen(plc_plc_list);
                for (i = 1; i <= len; i++) {
                    dpSet(plc_plc_list[i] + ".enabled", true);
                    unAlarmConfig_mask(plc_plc_list[i] + ".alarm", false, exceptionInfo);
                }
            }
        }
    } else {
// disconnect the callback function
//DebugN("Disable", g_unSystemIntegrity_BACnet_DSThreadId, "end");
        pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_BACnet_DSList, dp);
        if (pos > 0) {
            dpSetWait(backnet_device + ".Active", false,
                      backnet_pool_group + ".Active", false);
// kill the thread
            res = stopThread(g_unSystemIntegrity_BACnet_DSThreadId[pos]);
            if (res < 0) {
                bError = true;
            }

            if (bError) {
                fwException_raise(exceptionInfo, "ERROR",
                                  "unSystemIntegrity_BACnet_checking():" + getCatStr("unSystemIntegrity", "CANNOT_DP_DISCONNECT") + dp, "");
            } else {
                // remove from list
                pos = _unSystemIntegrity_setList(g_unSystemIntegrity_BACnet_DSList, dp, false);
//DebugN("rmv", res);
                dynRemove(g_unSystemIntegrity_BACnet_DSThreadId, pos);
                // set the enable to false and de-activate the alarm and acknowledge it if necessary
                dpSet(dp + ".enabled", false, dp + ".alarm", c_unSystemIntegrity_no_alarm_value);
                unAlarmConfig_mask(dp + ".alarm", true, exceptionInfo);
                // get all the PLC-PLC dp, and disable them
                plc_plc_list = dpNames(c_unSystemAlarm_dpPattern + PLC_PLC_pattern + sDpToChek + "*", c_unSystemAlarm_dpType);
                len = dynlen(plc_plc_list);
                for (i = 1; i <= len; i++) {
// do not reset the alarm value because this DPE should be connected to an address config
                    dpSet(plc_plc_list[i] + ".enabled", false);
                    unAlarmConfig_mask(plc_plc_list[i] + ".alarm", true, exceptionInfo);
                }

                // get all the FESystemAlarm dp, and disable them, check the Ok state and set the correct value
                plc_plc_list = dpNames(c_unSystemAlarm_dpPattern + FE_pattern + sDpToChek + "*", c_unSystemAlarm_dpType);
                len = dynlen(plc_plc_list);
                for (i = 1; i <= len; i++) {
// do not reset the alarm value because this DPE should be connected to an address config
                    dpSet(plc_plc_list[i] + ".enabled", false);
                    unAlarmConfig_mask(plc_plc_list[i] + ".alarm", true, exceptionInfo);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_BACnetCheck
/**
Purpose:
This function checks if the counter of the BACnet dp is modified periodically. If not it generates and alarm.

	@param dp: string, input, data point name

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_BACnetCheck(string dp, int iDrvNum) {
    int waitingTime, oldAlarmValue = -1;
    int oldValue = -1, newValue, alarmValue;
    bool bEnabled;
    string sModPlc;
    int res, iPlcNumber;

    while (true) {
        // if 0 set it to g_unSystemIntegrity_BACnet_CheckingDelay
        waitingTime = g_unSystemIntegrity_BACnet_CheckingDelay;
        if (waitingTime <= 0) {
            waitingTime = c_unSystemIntegrity_defaultBACnetCheckingDelay;
        }
        // wait
        delay(waitingTime);

        // get the value of the counter and the timeout PLCChecking
        dpGet(dp + ".communication.counter:_original.._stime", newValue);
        if (newValue == oldValue) { // if same value, check if same time
            // counter was not modified and same timestamp, set an alarm
            alarmValue = c_unSystemIntegrity_alarm_value_level1;
        } else
            // counter was modified, reset the alarm
        {
            alarmValue = c_unSystemIntegrity_no_alarm_value;
        }

        if (oldAlarmValue != alarmValue) {
            dpSet(c_unSystemAlarm_dpPattern + DS_pattern + dp + ".alarm", alarmValue);
        }

//DebugN("Check counter", dp, oldValue, newValue, alarmValue);
        oldValue = newValue;
        oldAlarmValue = alarmValue;
    }
}

//------------------------------------------------------------------------------------------------------------------------

//@}

