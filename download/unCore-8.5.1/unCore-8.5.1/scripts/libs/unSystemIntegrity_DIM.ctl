/**@file

// unSystemIntegrity_DIM.ctl
This library contains the system integrity functions of the DIM FE.

@par Creation Date
  19/04/2010

@par Modification History
  16/06/2011: Herve
  - IS-547: get the list of systemAlarm pattern
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@author 
  Frederic BERNARD (EN-ICE)
*/

private const bool unSystemIntegrity_DIM_fwDIMLibLoaded = fwGeneral_loadCtrlLib("fwDIM/fwDIM.ctl",false);

//@{
// global declaration
global dyn_string g_unSystemIntegrity_DIM_List;     // list of the System Alarm DIM DpN checked
global dyn_int g_unSystemIntegrity_DIM_DSThreadId;  // list of the DIM thread Id
global int g_unSystemIntegrity_DIMCheckingDelay;
//@}

//@{
// declaration
int g_iOldAlarm;
//@}

//@{
// constant declaration
const string UN_SYSTEM_INTEGRITY_DIM = "DIM";
const string UN_SYSTEM_INTEGRITY_DIM_THREAD = "unSystemIntegrity_DIMCheck";
const string UN_SYSTEMINTEGRITY_DIM_CALLBACK = "unSystemIntegrity_DIMCB";
const int UN_DEFAULT_DIM_CHECKINGDELAY = 30; // default value for the checking in sec.
//@}

//@{

//------------------------------------------------------------------------------------------------------------------------
// DIM_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string DIM_systemIntegrityInfo()
{
  return makeDynString(DS_pattern);
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_DIM_Initialize
/** Get the list of DIM checked and enabled by the systemIntegrity. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@param dsResult output, list of enabled front-end systemIntegrity
*/
unSystemIntegrity_DIM_Initialize(dyn_string &dsResult)
{
dyn_string dsList;
int len, i;
string dpToCheck;
bool enabled;

  dpGet(UN_SYSTEM_INTEGRITY_DIM+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
  unSystemIntegrity_DIM_DataCallback("", dsList);

  dsList = dpNames(c_unSystemAlarm_dpPattern+DS_pattern+"*", c_unSystemAlarm_dpType);
  len = dynlen(dsList);
  for(i = 1; i<=len; i++)
    {
    dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
    dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+DS_pattern),strlen(dpToCheck));
    if(dpExists(dpToCheck))
      {
      if(dpTypeName(dpToCheck) == UN_SYSTEM_INTEGRITY_DIM)
        {
        dpGet(dsList[i]+".enabled", enabled);
        if(enabled)
          dynAppend(dsResult, dpToCheck);
        }
      }
    }
}

//------------------------------------------------------------------------------------------------------------------------
// OBSOLETE unSystemIntegrity_DIMCB
/** Callback on the DIM status data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

*/
unSystemIntegrity_DIMCB(string sDp1, bool bState)
{
int iAlarmValue, iOldAlarm, iRes;
string sDp, sDpN;

  if(!bState)  iAlarmValue = c_unSystemIntegrity_alarm_value_level1;
  else  iAlarmValue = c_unSystemIntegrity_no_alarm_value;

 	// get System Alarm DpN
 	sDp = dpSubStr(sDp1, DPSUB_DP);
  iRes = _unSystemIntegrity_isInList(g_unSystemIntegrity_DIM_List, sDp); 
  //DebugN("unSystemIntegrity_DIMCB", g_unSystemIntegrity_DIM_List, sDp);           
 	if(iRes > 0) 
    {
    sDpN = g_unSystemIntegrity_DIM_List[iRes];
    sDpN = g_unSystemIntegrity_DIM_List[iRes];      
    }
        
  //DebugN("unSystemIntegrity_DIMCB", bState, sDpN);
          
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un systemIntegrity " + sDp1, "unSystemIntegrity_DIMCB check", bState);

  if(dpExists(sDpN))    
    {
    dpGet(sDpN + ".alarm", iOldAlarm);    
    if(iOldAlarm != iAlarmValue)
      dpSet(sDpN + ".alarm", iAlarmValue);
    }
  
}  

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_DIM_DataCallback
/** Callback on the configuration data (Checking delay)
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@param sDpe1  input, dpe
@param dsConfigData input, configuration data
*/
unSystemIntegrity_DIM_DataCallback(string sDpe1, dyn_string dsConfigData)
{
dyn_string dsTemp=dsConfigData;

  // check the size of the configuration data and add default value if some data are missing
  while(dynlen(dsTemp) <=1)
    dynAppend(dsTemp, "0");

  g_unSystemIntegrity_DIMCheckingDelay = (int)dsTemp[1];
  if(g_unSystemIntegrity_DIMCheckingDelay <= 0)
    g_unSystemIntegrity_DIMCheckingDelay = UN_DEFAULT_DIM_CHECKINGDELAY;
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "DIM systemIntegrity", "unSystemIntegrity_DIM_DataCallback", dsConfigData, g_unSystemIntegrity_DIMCheckingDelay);
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_DIM_HandleCommand
/** Callback on the systemIntegrity command.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@param sDpe1  input, dpe
@param command input, type of command
@param sDpe2  input, dpe
@param parameters input, parameters of the command
*/
unSystemIntegrity_DIM_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
dyn_string exceptionInfo;
int i, len =dynlen(parameters);
dyn_string dsResult;
string sResult;
 
  switch(command)
    {
    case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
      for(i=1; i<=len; i++)
        unSystemIntegrity_DIM_checking(parameters[i], true, true, exceptionInfo);
      break;
      
    case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
      for(i=1; i<=len; i++)
        {
        unSystemIntegrity_DIM_checking(parameters[i], false, false, exceptionInfo);
        // remove all _UnSystemAlarm dp from the alert list of applicationDP if it is in
        _unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i], false, exceptionInfo);
        // delete all the _UnSystemAlarm dps
        if(dpExists(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]))
          dpDelete(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]);
        }
      break;
      
    case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
      for(i=1; i<=len; i++)
        {
        if(dpExists(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]))
          unSystemIntegrity_DIM_checking(parameters[i], false, true, exceptionInfo);
        }
      break;
      
    case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
      for(i=1; i<=len; i++)
        {
        if(dpExists(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]))
          unSystemIntegrity_DIM_checking(parameters[i], false, false, exceptionInfo);
        }
      break;
      
    case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
      dpSet(UN_SYSTEM_INTEGRITY_DIM+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
          UN_SYSTEM_INTEGRITY_DIM+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_DIM_List);
      break;
      
    case 10: // extended diagnostic
      dynAppend(dsResult, getCurrentTime());
	  
      dynAppend(dsResult, "g_unSystemIntegrity_DIM_List");
      len = dynlen(g_unSystemIntegrity_DIM_List);
      for(i=1;i<=len;i++)
        {
        sResult = g_unSystemIntegrity_DIM_List[i];
        dynAppend(dsResult, sResult);
        }
		
      dynAppend(dsResult, "g_unSystemIntegrity_DIM_DSThreadId");
      len = dynlen(g_unSystemIntegrity_DIM_DSThreadId);
      for(i=1;i<=len;i++)
        {
        sResult = g_unSystemIntegrity_DIM_DSThreadId[i];
        dynAppend(dsResult, sResult);
        }
		
      dynAppend(dsResult, "g_unSystemIntegrity_DIMCheckingDelay");
	  dynAppend(dsResult, g_unSystemIntegrity_DIMCheckingDelay);
	  
      dpSet(UN_SYSTEM_INTEGRITY_DIM+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
          UN_SYSTEM_INTEGRITY_DIM+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", dsResult);
      break;   
    case 11:
      if(dynlen(parameters) >= 2)
        {
        g_b32DebugLevel = (bit32)parameters[1];
        g_sDebugFilter = parameters[2];
        }
      else
        {
        g_b32DebugLevel = (bit32)0;
        g_sDebugFilter = "";
        }
		dpSet(UN_SYSTEM_INTEGRITY_DIM+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
				UN_SYSTEM_INTEGRITY_DIM+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(g_b32DebugLevel, g_sDebugFilter));
      break;     
    default:
      break;
    }

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "DIM systemIntegrity", "unSystemIntegrity_DIM_HandleCommand", command, parameters);
  if(dynlen(exceptionInfo) > 0)
    {
    if(isFunctionDefined("unMessageText_sendException"))
      unMessageText_sendException("*", "*", "unSystemIntegrity_DIM_HandleCommand", "user", "*", exceptionInfo);
    // handle any error uin case the send message failed
    if(dynlen(exceptionInfo) > 0)
      DebugN(getCurrentTime(), exceptionInfo);
    }
  
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_DIM_checking
/** This function register/de-register the callback funtion of DIM manager and creates the _unSystemAlarm_DIM dp 
with the alarm config but it does not delete it.

  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  CTRL

@param sDp  input, data point name
@param bCreate  input, true create the dp and the alarm config if it does not exist, enable it
@param bRegister  input, true do a dpConnect, false do a dpDisconnect
@param exceptionInfo output, exception are returned here
*/
unSystemIntegrity_DIM_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
string sDpToChek, dp, dpPlc, plcHostname, description, sfwDimConfig;
int res, thId, pos, drvNum, len,i;
bool bError = false;
dyn_string plc_plc_list;
string alertPanel,alertHelp;
bool configExists, isActive;
int alertConfigType;
dyn_string alertTexts,alertClasses,summaryDpeList,alertPanelParameters;
dyn_float alertLimits;
  
// remove the system name
  res = strpos(sDp, ":");
  if(res >= 0)  dp = substr(sDp, res+1, strlen(sDp));
  else  dp = sDp;

  sDpToChek = dp;
  dp = c_unSystemAlarm_dpPattern+DS_pattern+dp;

// in case the Dp is not of type DIM do nothing. Should not happen, just for security.
  if(dpTypeName(sDpToChek) != UN_SYSTEM_INTEGRITY_DIM)
    return;
    
  dpGet(sDpToChek+".communication.driver_num", drvNum); 
  
  if(bCreate)
    {
    // get the plc hostname and the driver number
    plcHostname = unGenericDpFunctions_getAlias(sDpToChek);
    // create all the _UnSystemIntegrity Dp and its alarm config if it is not existing
    if(!dpExists(dp))
      {
      description = getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION")+plcHostname+" -> DS driver "+ drvNum;
      unSystemIntegrity_createSystemAlarm(sDpToChek, DS_pattern, description, exceptionInfo);
      }
    }

  if(bRegister)
    {
    // connect to the callback function if not already done
    pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_DIM_List, dp);       
    if(pos <= 0)
      {
      // add it in the list, because one of the callback function needs it.        
      pos = _unSystemIntegrity_setList(g_unSystemIntegrity_DIM_List, dp, true);

      dpGet(sDpToChek + ".configuration._fwDimConfig", sfwDimConfig);
      if(dpExists(sfwDimConfig))   bError = FALSE;
      else bError = TRUE;
      
      if(!bError)
        {              
        // start the thread for checking the counter
        thId = startThread(UN_SYSTEM_INTEGRITY_DIM_THREAD, sDpToChek, drvNum, sfwDimConfig);
        g_unSystemIntegrity_DIM_DSThreadId[pos] = thId;
        if(thId<0)  bError = true;
        }

     /*if(!bError) OBSOLETE
        {
 				res = dpConnect(UN_SYSTEMINTEGRITY_DIM_CALLBACK, 
	  											sfwDimConfig + ".ApiInfo.manState");
        }*/

      if(bError)
        {
        pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_DIM_List, dp);
        if(pos > 0)
          {
          // kill the threads if they were started
          res = stopThread(g_unSystemIntegrity_DIM_DSThreadId[pos]);
          // and then remove it from the list
          pos = _unSystemIntegrity_setList(g_unSystemIntegrity_DIM_List, dp, false);
          dynRemove(g_unSystemIntegrity_DIM_DSThreadId, pos);
          }

        fwException_raise(exceptionInfo, "ERROR", 
              "unSystemIntegrity_DIM_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_CONNECT") +dp,"");
        }
      else
        {
        //DebugN("Enable", g_unSystemIntegrity_DIM_DSThreadId, "end");
        // set the enable to true and activate the alarm if it is not activated of all the _UnSystemIntegrity DP.
        dpSet(dp+".enabled", true);
        unAlarmConfig_mask(dp+".alarm", false, exceptionInfo);
        // get all the PLC-PLC dp, and enable them
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++)
          {
          dpSet(plc_plc_list[i]+".enabled", true);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
          }

        // get all the FESystemAlarm dp, and enable them, check the Ok state and set the correct value
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++)
          {
          dpSet(plc_plc_list[i]+".enabled", true);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
          }
      }
    }
    
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un systemIntegrity", "unSystemIntegrity_DIM_checking register", sDp,
                                 g_unSystemIntegrity_DIM_DSThreadId, g_unSystemIntegrity_DIM_DSThreadId);
    }
  else
    {
    // disconnect the callback function
    pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_DIM_List, dp);
    if(pos > 0)
      {
      // kill the thread
      res = stopThread(g_unSystemIntegrity_DIM_DSThreadId[pos]);
      if(res<0)  bError = true;

      dpGet(sDpToChek + ".configuration._fwDimConfig", sfwDimConfig);      
  		//res = dpDisconnect(UN_SYSTEMINTEGRITY_DIM_CALLBACK, sfwDimConfig + ".ApiInfo.manState");

      if(bError)  fwException_raise(exceptionInfo, "ERROR", 
              "unSystemIntegrity_DIM_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_DISCONNECT") +dp,"");
      else
        {
        // remove from list
        pos = _unSystemIntegrity_setList(g_unSystemIntegrity_DIM_List, dp, false);
        dynRemove(g_unSystemIntegrity_DIM_DSThreadId, pos);
        // set the enable to false and de-activate the alarm and acknowledge it if necessary of all the _UnSystemIntegrity DP.
        dpSet(dp+".enabled", false, dp+".alarm", c_unSystemIntegrity_no_alarm_value);
        unAlarmConfig_mask(dp+".alarm", true, exceptionInfo);
        // get all the PLC-PLC dp, and disable them
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++)
          {
          // do not reset the alarm value because this DPE should be connected to an address config
          dpSet(plc_plc_list[i]+".enabled", false);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
          }

        // get all the FESystemAlarm dp, and disable them, check the Ok state and set the correct value
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++)
          {
          // do not reset the alarm value because this DPE should be connected to an address config
          dpSet(plc_plc_list[i]+".enabled", false);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
          }
        }
      }
    
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un systemIntegrity", "unSystemIntegrity_DIM_checking deregister", sDp, 
                                 g_unSystemIntegrity_DIM_List, g_unSystemIntegrity_DIM_DSThreadId);
    }
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_DIMCheck
/** This function checks if the counter of the DIM dp is modified periodically. 
A _UnSystemAlarm is set if the counter is not updated periodically.

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  CTRL

@param sFeDp  input, data point name
@param iDrvNum  input, the driver number
*/
unSystemIntegrity_DIMCheck(string sFeDp, int iDrvNum, string sfwDimDpN)
{
int waitingTime, oldAlarmValue = -1, oldTimingAlarm = -1;
int oldValue = -1, newValue, alarmValue, iTime;
bool bEnabled, bState;
string sModPlc;
int res, iPlcNumber;
time tlastUpdate, tOldlastUpdate;
  
  while(true)
    {
    // if 0 set it to g_unSystemIntegrity_DIMCheckingDelay
    waitingTime = g_unSystemIntegrity_DIMCheckingDelay;
    if(waitingTime <= 0)  waitingTime = UN_DEFAULT_DIM_CHECKINGDELAY;
    // wait
    delay(waitingTime);
  
    // get the value of the counter and the timeout PLCChecking
    dpGet(sFeDp + ".communication.counter", newValue,
          sfwDimDpN + ".ApiInfo.lastUpdate", tlastUpdate,
          sfwDimDpN + ".ApiInfo.manState", bState);

    if(newValue == oldValue)  alarmValue = c_unSystemIntegrity_alarm_value_level1;  // the counter was not modified, set an alarm
    else
      {
      // the counter was modified
      if(tlastUpdate == tOldlastUpdate)  alarmValue = c_unSystemIntegrity_alarm_value_level1;  //no refresh of the manager
      else
        {
        if(bState) alarmValue = c_unSystemIntegrity_no_alarm_value;
        else alarmValue = c_unSystemIntegrity_alarm_value_level1;
        }
      }    
        
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un systemIntegrity "+sFeDp, "unSystemIntegrity_DIMCheck check", sFeDp, 
                                 waitingTime, oldValue, newValue, oldAlarmValue, alarmValue, tlastUpdate);
    
    if(oldAlarmValue != alarmValue)
      {
      dpSet(c_unSystemAlarm_dpPattern+DS_pattern+sFeDp+".alarm", alarmValue);
      oldAlarmValue = alarmValue;
      }
    
    tOldlastUpdate = tlastUpdate;    
    oldValue = newValue;
    }
}

//------------------------------------------------------------------------------------------------------------------------

//@}
