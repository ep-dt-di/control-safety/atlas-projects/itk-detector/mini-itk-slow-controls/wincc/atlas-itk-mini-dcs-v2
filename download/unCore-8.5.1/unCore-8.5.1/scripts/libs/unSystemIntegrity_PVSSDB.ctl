/**@name LIBRARY: unSystemIntegrity_PVSSDB.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 25/11/2005

Modification History: 
  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  
  16/06/2011: Herve
  - IS-561: get the list of systemAlarm pattern

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the PVSSDB component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: PVSSDB_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_PVSSDB = "PVSSDB";
const string UN_SYSTEM_INTEGRITY_PVSSDB_checkFileSize = "unSystemIntegrity_PVSSDB_checkFileSize";
const string PVSSDB_pattern = "PVSSDB_";
const int c_unSystemIntegrity_defaultPVSSDBCheckingDelay = 30;
const int UN_SYSTEMINTEGRITY_PVSSDB_FILE_SIZE = 100000000; // 100Mb
// 
//------------------

// global declaration
global int g_unSystemIntegrity_PVSSDB_ThreadId; // list of the thread Id checking the PVSSDB
global int g_unSystemIntegrity_PVSSDBCheckingDelay;
global dyn_string g_dsPVSSDBFileToCheck;
global dyn_string g_dsFileToCheck; // list of file to check

//@{

//------------------------------------------------------------------------------------------------------------------------
// PVSSDB_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string PVSSDB_systemIntegrityInfo()
{
  return makeDynString(PVSSDB_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_PVSSDB_Initialize
/**
Purpose:
Get the list of PVSSDB check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled PVSSDB that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_PVSSDB_Initialize(dyn_string &dsResult)
{
	dyn_string dsList;
	int len, i;
	string dpToCheck;
	bool enabled;
	
	dpGet(UN_SYSTEM_INTEGRITY_PVSSDB+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
	unSystemIntegrity_PVSSDB_DataCallback("", dsList);

// get the list of PVSSDB manager dp to checked
	dsList = dpNames(c_unSystemAlarm_dpPattern+PVSSDB_pattern+"*", c_unSystemAlarm_dpType);
//DebugN("PVSSDB:", dsList);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+PVSSDB_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
	}
//DebugN(dsResult);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_PVSSDB_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_PVSSDB_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo, dsTemp;
	int i, len =dynlen(parameters);
  string sMessage;

	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_PVSSDB_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_PVSSDB_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PVSSDB_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PVSSDB_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PVSSDB_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+PVSSDB_pattern+parameters[i]))
					unSystemIntegrity_PVSSDB_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+PVSSDB_pattern+parameters[i]))
					unSystemIntegrity_PVSSDB_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			if(g_unSystemIntegrity_PVSSDB_ThreadId > 0)
				dsTemp = makeDynString(c_unSystemAlarm_dpPattern+PVSSDB_pattern+UN_SYSTEM_INTEGRITY_PVSSDB);
			dpSet(UN_SYSTEM_INTEGRITY_PVSSDB+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_PVSSDB+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", dsTemp);
			break;
    case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
      dpSet(UN_SYSTEM_INTEGRITY_PVSSDB+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_PVSSDB+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
		default:
			break;
	}

	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_PVSSDB_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error in case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_PVSSDB_HandleCommand", exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_PVSSDB);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_PVSSDB_DataCallback
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param dsConfigData: dyn_string, input, the config data

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_PVSSDB_DataCallback(string sDpe1, dyn_string dsConfigData)
{
	int PVSSDBCheckingDelay;
	dyn_string dsFileToCheck;
	int len;
	
	len = dynlen(dsConfigData);
	if(len >= 1) {
		PVSSDBCheckingDelay = (int)dsConfigData[1];
		dsFileToCheck = dsConfigData;
		dynRemove(dsFileToCheck, 1);
	}
	g_dsFileToCheck = dsFileToCheck;
//DebugN(dsConfigData);
	g_unSystemIntegrity_PVSSDBCheckingDelay = PVSSDBCheckingDelay;
	if(g_unSystemIntegrity_PVSSDBCheckingDelay <= 0)
		g_unSystemIntegrity_PVSSDBCheckingDelay = c_unSystemIntegrity_defaultPVSSDBCheckingDelay;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_PVSSDB_checking
/**
Purpose:
This function register/de-register the callback funtion of PVSSDB. This function can also create the _unSystemAlarm_PVSSDB dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_PVSSDB_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string dpToCheck, dp, description;
	int res;
	
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;

	dpToCheck = dp;
	dp = c_unSystemAlarm_dpPattern+PVSSDB_pattern+dp;
//DebugN("unSystemIntegrity_PVSSDB_checking", dp, bCreate, bRegister, dpToCheck);
	if(bCreate) {
// create the sDp and its alarm config if it is not existing
		if(!dpExists(dp)) {
			description = "PVSS DB file size";

			unSystemIntegrity_createSystemAlarm(dpToCheck, PVSSDB_pattern, description, exceptionInfo);
		}
	}
	if(dynlen(exceptionInfo)<=0)
		unSystemAlarm_PVSSDB_register(bRegister, dp, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemAlarm_PVSSDB_register
/**
Purpose:
Register function, does the register for the PVSSDB file size.

	@param bRegister: bool, input, true: add, false: remove from sGlobalVar
	@param sDp: string, input, the alarm dp
	@param exceptionInfo: dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemAlarm_PVSSDB_register(bool bRegister, string sDp, dyn_string &exceptionInfo)
{
	int res, thId;
	
	if(bRegister) {
		if(g_unSystemIntegrity_PVSSDB_ThreadId <= 0) {
// start the thread for checking the counter
			thId = startThread(UN_SYSTEM_INTEGRITY_PVSSDB_checkFileSize, sDp);
			g_unSystemIntegrity_PVSSDB_ThreadId = thId;
//DebugTN("ALARM", thId);
			dpSet(sDp+".enabled", true);
			unAlarmConfig_mask(sDp+".alarm", false, exceptionInfo);
		}
	}
	else {
// kill the thread
		res = stopThread(g_unSystemIntegrity_PVSSDB_ThreadId);
		g_unSystemIntegrity_PVSSDB_ThreadId = -1;
		// set the enable to false and de-activate the alarm and acknowledge it if necessary
		dpSet(sDp+".enabled", false, sDp+".alarm", c_unSystemIntegrity_no_alarm_value);
		unAlarmConfig_mask(sDp+".alarm", true, exceptionInfo);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_PVSSDB_checkFileSize
/**
Purpose:
Check the file size

	@param sDp: string, input, the unSystem alarm dp

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_PVSSDB_checkFileSize(string sDp)
{
	int waitingTime;
	int alarmValue, oldAlarmValue = -1;
	dyn_string dsFileList;
	int len, i, iSize;
		
	while(true) {
		waitingTime = g_unSystemIntegrity_PVSSDBCheckingDelay;
	// wait
		delay(waitingTime);

		dsFileList = g_dsFileToCheck;
		len = dynlen(dsFileList);
		alarmValue = c_unSystemIntegrity_no_alarm_value;
		for(i=1;(i<=len) && (alarmValue==c_unSystemIntegrity_no_alarm_value); i++) {
			iSize = getFileSize(PROJ_PATH+dsFileList[i]);
//DebugN(dsFileList[i], iSize);
			if(iSize > UN_SYSTEMINTEGRITY_PVSSDB_FILE_SIZE)
				alarmValue = c_unSystemIntegrity_alarm_value_level1;
		}
		if(alarmValue != oldAlarmValue) {
			dpSet(sDp+".alarm", alarmValue);
		}
		oldAlarmValue = alarmValue;
		
//DebugTN("unSystemIntegrity_PVSSDB_checkFileSize", sDp, iSize, alarmValue);	
	}
}

//------------------------------------------------------------------------------------------------------------------------

//@}
