/**@name LIBRARY: unSystemIntegrity_RDB.ctl

@author: 
Josef Hofer (EN-ICE-SCD)

Modification History: 
  28/08/2014: Josef Hofer: 
  - unSystemIntegrity_RDB_checking(): change alert classes to 
    "_unSystemIntegrityAlarm_lvl1." and "_unSystemIntegrityAlarm_lvl2." 
    so that sending of mails works 
  - unSystemIntegrity_RDB_postInstall():
    Recreate alert config of RDB system alarm dps to update it in case of change in structure
  23/06/2014: Josef Hofer: Complete revision of the RDB system integrity plugin:
  - Implement easy way of checking 
    (only check in polling loop, don't use asynchronous checking with dpConnects)
  - Kill manager only if it is blocking longer than a specified time
  - Use discrete alarm config instead of analog one
  - Create one test datapoint per archive group
  - At each round trip check increment all datapoints of test dp type
  28/09/2011: Herve
  - IS-611: unSystemIntegrity  remove the deprecated function calls of the fwInstallation  
  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  
Creation Date: 28/03/2008

version 1.0
	
Purpose: 
	Library of the RDB component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

*/

#uses "fwConfigs/fwArchive.ctl"

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------

// string constants for adressing datapoints
const string c_SysInt_RDB = "RDB";
const string RDB_pattern  = "RDB";

// RDB archive datapoints
const string c_SysInt_RDB_ArchiveDp                   = "_RDBArchive";
const string c_SysInt_RDB_ArchiveDpe_ConnectionStatus = ".dbConnection.connected";
const string c_SysInt_RDB_ArchiveDpe_CurrentBuffers   = ".buffer.currentBlocks";
const string c_SysInt_RDB_ArchiveDpe_FlushInterval    = ".buffer.flushInterval";

// test datapoints
const string c_SysInt_RDB_TestDp_Type    = "_UnSystemIntegrityRDB";
const string c_SysInt_RDB_TestDp_Prefix  = "UnSysInt";
const string c_SysInt_RDB_TestDpe_Value  = ".value";

// indexes of settings in .config.data
const int c_SysInt_RDB_ConfigIndex_Count                 = 6;
const int c_SysInt_RDB_ConfigIndex_LoopDelay             = 1;
const int c_SysInt_RDB_ConfigIndex_RoundtripInterval     = 2;
const int c_SysInt_RDB_ConfigIndex_MaxManagerBlockTime   = 3;
const int c_SysInt_RDB_ConfigIndex_MaxManagerRestarts    = 4;
const int c_SysInt_RDB_ConfigIndex_MaxBuffersWarning     = 5;
const int c_SysInt_RDB_ConfigIndex_MaxBuffersError       = 6;

// default values for .config.data
const int c_SysInt_RDB_DefaultConfig_LoopDelay           = 10;
const int c_SysInt_RDB_DefaultConfig_RoundtripInterval   = 120;
const int c_SysInt_RDB_DefaultConfig_MaxManagerBlockTime = 300;
const int c_SysInt_RDB_DefaultConfig_MaxManagerRestarts  = 6;
const int c_SysInt_RDB_DefaultConfig_MaxBuffersWarning   = 12;
const int c_SysInt_RDB_DefaultConfig_MaxBuffersError     = 4000;

// default values for hard coded settings
const int c_SysInt_RDB_DefaultConfig_ReadWriteSafetyTime = 5;
const int c_SysInt_RDB_DefaultConfig_BufferHysteresis    = 4;

// alert ranges of system integrity alarm datapoint
const int c_SysInt_RDB_AlertLevel_OK               = 0;
const int c_SysInt_RDB_AlertLevel_RoundtripFailed  = 5;  
const int c_SysInt_RDB_AlertLevel_BufferWarning    = 10;  
const int c_SysInt_RDB_AlertLevel_NoDbConnection   = 15;
const int c_SysInt_RDB_AlertLevel_BufferError      = 20;
const int c_SysInt_RDB_AlertLevel_ManagerStopped   = 25;
const int c_SysInt_RDB_AlertLevel_ManagerBlocked   = 30;

// message indexes in RDB catalogue
const int c_SysInt_RDB_Catalogue_ErrorNotFound     = 1;
const int c_SysInt_RDB_Catalogue_ManagerNotRunning = 2;
const int c_SysInt_RDB_Catalogue_ManagerIsBlocking = 3;
const int c_SysInt_RDB_Catalogue_KillingManager    = 4;
const int c_SysInt_RDB_Catalogue_StartingManager   = 5;
const int c_SysInt_RDB_Catalogue_MaxRetriesReached = 6;
const int c_SysInt_RDB_Catalogue_NoDbConnection    = 7;
const int c_SysInt_RDB_Catalogue_BufferWarning     = 8;
const int c_SysInt_RDB_Catalogue_BufferError       = 9;
const int c_SysInt_RDB_Catalogue_RoundtripFailed   = 10;
const int c_SysInt_RDB_Catalogue_TestDpNotFound    = 11;
const int c_SysInt_RDB_Catalogue_ReadPmonInfoFailed= 12;

//------------------

// global declaration
global dyn_string g_SysInt_RDB_archiveList;     // List of _RDBArchives to check

global int g_SysInt_RDB_monitorThreadId;        // ID of the monitoring thread
global int g_SysInt_RDB_bufferToDiskMode = -1;  // BufferToDisk setting from config file 
global int g_SysInt_RDB_dbConnectionStatus;     // Value of the internal dpe .dbConnection
global int g_SysInt_RDB_writeReadInterval;      // Time between write and read form a DB, internal dpe

global int g_SysInt_RDB_loopDelay;              // Delay after each check cycle
global int g_SysInt_RDB_roundTripInterval;      // Interval after which a round trip should be done again   
global int g_SysInt_RDB_maxManagerBlockingTime; // Manager gets killed if it blocks longer than this time   
global int g_SysInt_RDB_maxManagerRestarts;     // Max. number of retries to restart manager
global int g_SysInt_RDB_maxBufferWarning;       // Buffer threshold value to raise a warning
global int g_SysInt_RDB_maxBufferError;         // Buffer threshold value to raise an error 

//@{

//---------------------------------------------------------------------------------------------------------------------------------


// unSystemIntegrity_RDB_Initialize
/**
* Get the list of _RDBArchives to be checked by systemIntegrity.
*
* @param dsResult   dyn_string, output, the list of enabled RDB manager DPs to check
*/
public void unSystemIntegrity_RDB_Initialize(dyn_string &dsResult)
{  
  // Read config file and get buffer to disc mode  
  g_SysInt_RDB_bufferToDiskMode = unSystemIntegrity_RDB_getBufferConfig();
  
  // Create test datapoints
  unSystemIntegrity_RDB_createTestDPs();

  // Get configuration data from RDB_systemIntegrityInfo
  unSystemIntegrity_RDB_getConfigData(true);
  
  // Get the list of _RDBArchives to check by looking for existing unSystemAlarms_RDB_*  and extracting the last pattern ('*')
  dyn_string dsList = dpNames(c_unSystemAlarm_dpPattern + RDB_pattern + "*", c_unSystemAlarm_dpType);
  for(int i=1; i<=dynlen(dsList); i++) 
  {
    string dpToCheck;
    dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
    dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern + RDB_pattern), strlen(dpToCheck));              
 
    bool enabled;
    dpGet(dsList[i] + ".enabled", enabled);    
    if(enabled) {
      dynAppend(dsResult, dpToCheck);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_HandleCommand
/**
* Handle systemIntegrity commands from configuration panel
*
* @param sDpe1      input, dpe
* @param command    input, the systemIntegrity command
* @param sDpe2      input, dpe
* @param parameters input, the list of parameters of the systemIntegrity command
*/
public void unSystemIntegrity_RDB_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo;
	int i, len=dynlen(parameters);

	 switch(command) 
  {
	   case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
    {
			   for(i=1; i<=len; i++) {
				    unSystemIntegrity_RDB_checking(parameters[i], true, true, exceptionInfo);
			   }
      unSystemIntegrity_RDB_manageMonitoringThread("start");
			   break;
    } 
    case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
    {
			   for(i=1; i<=len; i++) {
				    unSystemIntegrity_RDB_checking(parameters[i], false, false, exceptionInfo);
				    // remove dp from the alert list of applicationDP if it is in
				    _unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern + RDB_pattern + parameters[i], false, exceptionInfo);
				    // delete the _UnSystemAlarm dps
				    if(dpExists(c_unSystemAlarm_dpPattern + RDB_pattern + parameters[i]))
				      dpDelete(c_unSystemAlarm_dpPattern + RDB_pattern + parameters[i]);
			   }
      delay(1);
      unSystemIntegrity_RDB_manageMonitoringThread("stop");
			   break;
    }      
    case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
    {
		    for(i=1; i<=len; i++) {
				    if(dpExists(c_unSystemAlarm_dpPattern + RDB_pattern + parameters[i]))
				      unSystemIntegrity_RDB_checking(parameters[i], false, true, exceptionInfo);
			   }
      unSystemIntegrity_RDB_manageMonitoringThread("start");
		    break;
    } 
	  	case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
    {
		    for(i=1; i<=len; i++) {
				    if(dpExists(c_unSystemAlarm_dpPattern + RDB_pattern + parameters[i]))
				      unSystemIntegrity_RDB_checking(parameters[i], false, false, exceptionInfo);
			   }
      delay(1);
      unSystemIntegrity_RDB_manageMonitoringThread("stop");
		    break;
    }    
  		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
    {
  		  dpSet(c_SysInt_RDB + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.commandResult", command, 
  			       c_SysInt_RDB + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.result", g_SysInt_RDB_archiveList);
  		  break;
    }    
  		default:
    {  
  		  break;
    }
	 }

  if(dynlen(exceptionInfo) > 0) {
	   if(isFunctionDefined("unMessageText_sendException")) {
		    unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_RDB_HandleCommand", "user", "*", exceptionInfo);
		  }
    // handle any error in case the send message failed
	   if(dynlen(exceptionInfo) > 0) {
		    DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_RDB_HandleCommand", exceptionInfo);
	   }
  }
}


//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_DataCallback
/**
* Callback function to get most recent configuration data
*
* @param sDPe       
* @param dsResult    
*/
public void unSystemIntegrity_RDB_DataCallback(string sDPe, dyn_string &dsResult)
{
  unSystemIntegrity_RDB_getConfigData(false);      
}


//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_postInstall
/**
* Delete delete old test datapoints after installing the new version of systemIntegrity for RDB
* Recreate systemIntegrity alarm datapoints with up to date alert configuration
*/
public void unSystemIntegrity_RDB_postInstall(int managerNumber = SYSTEMINTEGRITY_DEFAULT_MANAGER_NUMBER_RDB)
{
  DebugTN("unSystemIntegrity_RDB_postInstall(): performing post installation steps");     
  
  // Delete old test datapoint wich was used in former versions
  const string c_SysInt_RDB_TestDpNameDeprecated = "RDBConnection_systemIntegrity";    
  if(dpExists(c_SysInt_RDB_TestDpNameDeprecated))
    dpDelete(c_SysInt_RDB_TestDpNameDeprecated);   
        
  // Recreate alert config of RDB system alarm dps to update it in case of change in structure
  dyn_string dsRdbAlarmDps = dpNames(c_unSystemAlarm_dpPattern + RDB_pattern + "*", c_unSystemAlarm_dpType); 
  dyn_errClass err = getLastError();  
  if(dynlen(err)==0) {
    for(int i=1;i<=dynlen(dsRdbAlarmDps);i++) {
      dyn_string dsExInfo;  
      fwAlertConfig_delete(dsRdbAlarmDps[i]+".alarm", dsExInfo) ;    
      unSystemIntegrity_RDB_addAlertConfig(dsRdbAlarmDps[i]+".alarm", dsExInfo);    
    }         
  } 
}   

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_createTestDPs
/**
* Create test datapoints for each archive class, if they do not exist yet
*/
private void unSystemIntegrity_RDB_createTestDPs()
{
  dyn_string dsArchiveClasses, dsArchiveGroupDps, dsExInfo;
  fwArchive_getAllRDBArchiveClasses(makeDynString(getSystemName()), dsArchiveClasses, dsArchiveGroupDps, dsExInfo);
  
  for(int i=1;i<=dynlen(dsArchiveClasses);i++)
  {
    // For the time being we just create the test datapoints for _RdbArchive, later we maybe have to crate them for RdbArchive_2 too, to support redundancy ... 
    string sTestDP = c_SysInt_RDB_TestDp_Prefix + c_SysInt_RDB_ArchiveDp + unSystemIntegrity_removeSystemName(dsArchiveGroupDps[i]);    
    if(!dpExists(sTestDP))
    {
      dpCreate(sTestDP, c_SysInt_RDB_TestDp_Type);

      int iArchiveType = false;
      dpGet(sTestDP + c_SysInt_RDB_TestDpe_Value + ":_" + "archive.._type", iArchiveType);    
      if(iArchiveType == 0)
      {     
        if(dynlen(dsArchiveClasses) > 0)
        {
          int archiveType = DPATTR_ARCH_PROC_VALARCH;
          int smoothProcedure = 0;
          float deadband = 0;
          float timeInterval = 0; 
          fwArchive_set(sTestDP + c_SysInt_RDB_TestDpe_Value, dsArchiveClasses[i], archiveType, smoothProcedure, deadband, timeInterval, dsExInfo); 
          if(dynlen(dsExInfo)) {
            return;
          }    
        }
        else
        {
          DebugN("unSystemIntegrity_RDB_createTestDPs(): RDB archive could not be set on " + sTestDP + c_SysInt_RDB_TestDpe_Value + " ! " + 
                 "archiveClasses, archiveGroupDps: ", dsArchiveClasses[i], dsArchiveGroupDps[i]);
        }       
      }
    } 
  } 
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_getConfigData
/**
* Get configuration data and settings from RDB_systemIntegrityInfo
*
* @param   bResetOnError   input,  true: set default values on error, false: don't set default values on error
*/
private void unSystemIntegrity_RDB_getConfigData(bool bResetOnError)
{
  if(dpExists(c_SysInt_RDB + UN_SYSTEMINTEGRITY_EXTENSION))
  {
    dyn_string configData;  
    dpGet(c_SysInt_RDB + UN_SYSTEMINTEGRITY_EXTENSION + ".config.data", configData);

    if(dynlen(configData) == c_SysInt_RDB_ConfigIndex_Count)
    {   
      g_SysInt_RDB_loopDelay = (int)configData[c_SysInt_RDB_ConfigIndex_LoopDelay]; 
      g_SysInt_RDB_roundTripInterval = (int)configData[c_SysInt_RDB_ConfigIndex_RoundtripInterval];     
      g_SysInt_RDB_maxManagerBlockingTime = (int)configData[c_SysInt_RDB_ConfigIndex_MaxManagerBlockTime];     
      g_SysInt_RDB_maxManagerRestarts = (int)configData[c_SysInt_RDB_ConfigIndex_MaxManagerRestarts]; 
      g_SysInt_RDB_maxBufferWarning = (int)configData[c_SysInt_RDB_ConfigIndex_MaxBuffersWarning]; 
      g_SysInt_RDB_maxBufferError = (int)configData[c_SysInt_RDB_ConfigIndex_MaxBuffersError]; 
    }
    else  
    {
      if(bResetOnError==true)
      {
        dpSet(c_SysInt_RDB + UN_SYSTEMINTEGRITY_EXTENSION + ".config.data", 
              makeDynString(c_SysInt_RDB_DefaultConfig_LoopDelay, 
                            c_SysInt_RDB_DefaultConfig_RoundtripInterval, 
                            c_SysInt_RDB_DefaultConfig_MaxManagerBlockTime,
                            c_SysInt_RDB_DefaultConfig_MaxManagerRestarts,
                            c_SysInt_RDB_DefaultConfig_MaxBuffersWarning,
                            c_SysInt_RDB_DefaultConfig_MaxBuffersError)
             );        
        g_SysInt_RDB_loopDelay = c_SysInt_RDB_DefaultConfig_LoopDelay; 
        g_SysInt_RDB_roundTripInterval = c_SysInt_RDB_DefaultConfig_RoundtripInterval;     
        g_SysInt_RDB_maxManagerBlockingTime = c_SysInt_RDB_DefaultConfig_MaxManagerBlockTime;     
        g_SysInt_RDB_maxManagerRestarts = c_SysInt_RDB_DefaultConfig_MaxManagerRestarts; 
        g_SysInt_RDB_maxBufferWarning = c_SysInt_RDB_DefaultConfig_MaxBuffersWarning; 
        g_SysInt_RDB_maxBufferError = c_SysInt_RDB_DefaultConfig_MaxBuffersError; 
      }
      else
      {
        DebugTN("unSystemIntegrity_RDB_getConfigData(): Inconsistency found in " + c_SysInt_RDB + UN_SYSTEMINTEGRITY_EXTENSION + ".config.data" + " No action has been taken.");
      }
    } 
  } 
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_checking
/**
* This function register/de-register the callback funtion of RDB. 
* It can also create the _unSystemAlarm_RDB dp with the alarm config but it cannot delete it.
*
* @param sDp             input, data point name
* @param bCreate         input, true create the dp and the alarm config if it does not exist, enable it
*	@param bEnable         input, true: enable alarm, false: disable alarm
*	@param exceptionInfo   output, exception are returned here
*/
private void unSystemIntegrity_RDB_checking(string sDp, bool bCreate, bool bEnable, dyn_string &exceptionInfo)
{
  // Remove the system name
  string sDpToCheck = unSystemIntegrity_removeSystemName(sDp); 
  string sSysAlarmDp = c_unSystemAlarm_dpPattern + RDB_pattern + sDpToCheck;
        
  if(bCreate) 
  {
    if(!dpExists(sSysAlarmDp)) 
    { 
      dpCreate(sSysAlarmDp, c_unSystemAlarm_dpType);  
      string sSysAlarmDpe = sSysAlarmDp + ".alarm";              
      dpSetDescription(sSysAlarmDpe, "RDB archiving (" + sDpToCheck + ")");          
      unSystemIntegrity_RDB_addAlertConfig(sSysAlarmDpe, exceptionInfo);  
    } 
  }  

  // Register dp and dpconnect to dsParam   
  if(dynlen(exceptionInfo)<=0) {     
    // add/remove integrity classes
    _unSystemIntegrity_setList(g_SysInt_RDB_archiveList, sSysAlarmDp, bEnable); 
    
    // enable/disable and unmask/mask 
    dpSet(sSysAlarmDp + ".enabled", bEnable);
    unAlarmConfig_mask(sSysAlarmDp + ".alarm", !bEnable, exceptionInfo);
  } 
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_addAlertConfig
/**
* Add discrete alert config to system alarm datapoint element
*
* @param sSysAlarmDpe    input, dpe for which we want to create the alert config
*	@param exceptionInfo   output, exception are returned here
*/
private void unSystemIntegrity_RDB_addAlertConfig(string sSysAlarmDpe, dyn_string &exceptionInfo)
{           
  // set discrete alarm config
  dyn_mixed alarmObject;
  fwAlertConfig_objectCreateDiscrete( 
    alarmObject, 
    makeDynString("OK",                        
                  "Roundtrip failed",
                  "Buffering to disk",
                  "No connection to database",
                  "Excessive buffering to disk",                        
                  "Manager is stopped",
                  "Manager is blocking"), 
    makeDynString("*", 
                  c_SysInt_RDB_AlertLevel_RoundtripFailed, 
                  c_SysInt_RDB_AlertLevel_BufferWarning, 
                  c_SysInt_RDB_AlertLevel_NoDbConnection, 
                  c_SysInt_RDB_AlertLevel_BufferError,                         
                  c_SysInt_RDB_AlertLevel_ManagerStopped, 
                  c_SysInt_RDB_AlertLevel_ManagerBlocked), 
    makeDynString("",
                  getSystemName()+"_unSystemIntegrityAlarm_lvl1.",
                  getSystemName()+"_unSystemIntegrityAlarm_lvl1.",
                  getSystemName()+"_unSystemIntegrityAlarm_lvl2.",
                  getSystemName()+"_unSystemIntegrityAlarm_lvl2.",
                  getSystemName()+"_unSystemIntegrityAlarm_lvl2.",
                  getSystemName()+"_unSystemIntegrityAlarm_lvl2."),
    "", 
    makeDynString(""), 
    "", 
    false, 
    makeDynBool(false,false,false,false,false,false,false), 
    "", 
    makeDynString("","","","","","",""), 
    exceptionInfo,
    false,
    false,
    "",
    false); //exception info returned here
  if(dynlen(exceptionInfo)){ return;}       
  //set the alarm to the dpe                                        
  fwAlertConfig_objectSet(sSysAlarmDpe, alarmObject, exceptionInfo);   
  if(dynlen(exceptionInfo)){ return;}     
  //activate it
  fwAlertConfig_activate(sSysAlarmDpe, exceptionInfo); 
  if(dynlen(exceptionInfo)){ return;}     
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_manageMonitoringThread
/**
* Starts and Stops the thread in which the checking is done
*
* @param command         input, start or stop the thread
*/
private void unSystemIntegrity_RDB_manageMonitoringThread(string command)
{  
  dyn_string dsList = dpNames(c_unSystemAlarm_dpPattern + RDB_pattern + "*", c_unSystemAlarm_dpType);
  
  switch(command)
  {
    case "start":
      if(g_SysInt_RDB_monitorThreadId == 0)
      {
        if(dynlen(dsList)> 0)
        {
          g_SysInt_RDB_monitorThreadId = startThread("unSystemIntegrity_RDB_checkArchiving");
          //DebugN("***********STARTING THREAD*********");
        }   
      }
      break;
      
    case "stop":  
      for(int i =1; i<=dynlen(dsList); i++)
      {
        bool enable;
        dpGet(dsList[i] + ".enabled", enable);
        if(enable == true)
          return;
      }
  
      int err;
      if(g_SysInt_RDB_monitorThreadId != 0)
      {
        err = stopThread(g_SysInt_RDB_monitorThreadId);
        //DebugN("***********STOPPING THREAD*********");
      }
        
      if(err<0)
        DebugN("unSystemIntegrity_RDB_manageMonitoringThread() - stopping thread failed!");
      else 
        g_SysInt_RDB_monitorThreadId =0;
      
      break;
      
    default:
      break;
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_checkArchiving
/**
* Check in a periodic cycle if RDB Archiving works properly
*
* Following checks are done:
* ----------------------------------------------------------
* 1. Check the current state of the archive manager 
*    Kill it if it is blocking longer than a specified time
* 2. Check the DB connection
*    Set an error if disconnected
* 3. Check the current current buffer usage
*    Set an error if it is too high
* 4. Do the round trip check 
*    Set an error if test values were not found in RDB archive 
*/
private void unSystemIntegrity_RDB_checkArchiving()
{  
  const string sManagerName = "WCCOArdb";          
  const string sManagerOption = "";  
  
  string sManagerStartMode = "";
  int iManagerState;  
  int iManagerIndex;
  
  int iRestartCounter = 0;  
  bool bBlocking = false;
  bool bRoundtripFailed = false;
  bool bCheckRoundtrip = false;
  time tBlockingStart;  
  time tLastRoundTrip; 
  string sRoundtripFailedDPs="";
  
  bool bBufferGreaterThresholdWarning = false;
  bool bBufferGreaterThresholdError = false;
   
  while(1)
  { 
    // DebugTN("Checking: manager state, DB connection, current used buffer");   
    
    int iErrorCode;
    unSystemIntegrity_RDB_presetErrorCode(iErrorCode, c_SysInt_RDB_AlertLevel_OK, true);
    
    // --------------------------------------------------- 1. Check the manager state -----------------------------------------------------------------       
    // Get current manager status and manager process information
    unSystemIntegrity_getState(sManagerName, sManagerOption, iManagerState);
    if(unSystemIntegrity_RDB_findManager(sManagerName, sManagerOption, sManagerStartMode, iManagerIndex) == true)
    { 
      // Only pay attention to process information if PMON query is successful
      
      string sManagerAction = "";
    
      switch(iManagerState)
      {
        case SYSTEMINTEGRITY_MANAGER_STOPPED:
        {
          unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_ManagerNotRunning);
          unSystemIntegrity_RDB_presetErrorCode(iErrorCode, c_SysInt_RDB_AlertLevel_ManagerStopped);
        
          if (sManagerStartMode == "always") {
            // Start manager if it is not running and start mode requires a start          
            sManagerAction = "start";  
          }        
          bBlocking = false;
          break;
        }
        case SYSTEMINTEGRITY_MANAGER_BLOCKED:
        {  
          unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_ManagerIsBlocking);   
          unSystemIntegrity_RDB_presetErrorCode(iErrorCode, c_SysInt_RDB_AlertLevel_ManagerBlocked);
                     
          if(bBlocking == false) {
            tBlockingStart = getCurrentTime(); 
            bBlocking = true;
          }       
          time tCurrent = getCurrentTime();
          if(((tCurrent - tBlockingStart) > g_SysInt_RDB_maxManagerBlockingTime) && (g_SysInt_RDB_maxManagerBlockingTime > 0)) {
            // Kill manager if it is blocking for more than a specified time
            sManagerAction = "kill";
          }   
          break;
        }  
        default: // SYSTEMINTEGRITY_MANAGER_RUNNING
        {
          bBlocking = false;
          iRestartCounter = 0;
          break;  
        }    
      }

      // Execute sManagerAction
      if((iRestartCounter < g_SysInt_RDB_maxManagerRestarts) || (g_SysInt_RDB_maxManagerRestarts < 0))    
      { 
        // g_SysInt_RDB_maxManagerRestarts = 0 --> kill never
        // g_SysInt_RDB_maxManagerRestarts < 0 --> kill always
        // g_SysInt_RDB_maxManagerRestarts > 0 --> kill couple of times 
      
        switch(sManagerAction)
        {
          case "kill":
          {
            dyn_string dsExInfo;
            unSystemIntegrity_RDB_throwError("INFO", c_SysInt_RDB_Catalogue_KillingManager);
            unSystemIntergity_manageManager(sManagerName, sManagerOption, SYSTEMINTEGRITY_KILL_COMMAND, iManagerIndex, dsExInfo);
            iRestartCounter++;          
            break;
          }
          case "start":
          {
            dyn_string dsExInfo;
            unSystemIntegrity_RDB_throwError("INFO", c_SysInt_RDB_Catalogue_StartingManager);
            unSystemIntergity_manageManager(sManagerName, sManagerOption, SYSTEMINTEGRITY_START_COMMAND, iManagerIndex, dsExInfo);
            iRestartCounter++;
            break;
          }
        }      
      }
      else
      {
        unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_MaxRetriesReached, "(" + g_SysInt_RDB_maxManagerRestarts + ") Aborting current action: " + sManagerAction + " RDB manager.");
      }    
    }
    else
    {
      unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_ReadPmonInfoFailed, "Process: " + sManagerName); 
    }

    
    // --------------------------------------------------- 2. Check the DB connection ---------------------------------------------------------------------   
    dpGet(c_SysInt_RDB_ArchiveDp + c_SysInt_RDB_ArchiveDpe_ConnectionStatus, g_SysInt_RDB_dbConnectionStatus);
    if(g_SysInt_RDB_dbConnectionStatus == 0) {
      unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_NoDbConnection);
      unSystemIntegrity_RDB_presetErrorCode(iErrorCode, c_SysInt_RDB_AlertLevel_NoDbConnection);
    }
    
    
    // --------------------------------------------------- 3. Check the buffer ---------------------------------------------------------------------   
    if(g_SysInt_RDB_bufferToDiskMode)
    {
      int iCurrentBuffer;
      dpGet(c_SysInt_RDB_ArchiveDp + c_SysInt_RDB_ArchiveDpe_CurrentBuffers, iCurrentBuffer);  
      
      unSystemIntegrity_RDB_compareValueWithMaxAndConsiderHysteresis(
        bBufferGreaterThresholdWarning, 
        iCurrentBuffer, 
        g_SysInt_RDB_maxBufferWarning,
        c_SysInt_RDB_DefaultConfig_BufferHysteresis);
     
      unSystemIntegrity_RDB_compareValueWithMaxAndConsiderHysteresis(
        bBufferGreaterThresholdError, 
        iCurrentBuffer, 
        g_SysInt_RDB_maxBufferError,
        c_SysInt_RDB_DefaultConfig_BufferHysteresis);
      
      if(bBufferGreaterThresholdWarning && !bBufferGreaterThresholdError) {
        unSystemIntegrity_RDB_throwError("WARNING", c_SysInt_RDB_Catalogue_BufferWarning); 
        unSystemIntegrity_RDB_presetErrorCode(iErrorCode, c_SysInt_RDB_AlertLevel_BufferWarning);
      } 
      else if(bBufferGreaterThresholdError) {
        unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_BufferError); 
        unSystemIntegrity_RDB_presetErrorCode(iErrorCode, c_SysInt_RDB_AlertLevel_BufferError);
      }   
    }
    
    
    // --------------------------------------------------- 4. Do the round trip check -----------------------------------------------------------------
    time tNow = getCurrentTime(); 
    if(tNow - tLastRoundTrip > g_SysInt_RDB_roundTripInterval) 
    {      
      tLastRoundTrip = tNow;         
      bRoundtripFailed = false;
    
      // Increment and set values of test datapoints        
      dyn_string dsTestDPEs = dpNames("*" + c_SysInt_RDB_TestDpe_Value, c_SysInt_RDB_TestDp_Type); 
      dyn_errClass err = getLastError();
      if(dynlen(err)==0 && dynlen(dsTestDPEs)>0)
      {
        dyn_int diTestValues; 
        dpGet(dsTestDPEs, diTestValues);   
        for(int i=1;i<=dynlen(diTestValues);i++) {
          if(diTestValues[i] >= 10000) diTestValues[i]=0;     
          diTestValues[i]++;         
        }
        time t1 = getCurrentTime(); 
        dpSetWait(dsTestDPEs, diTestValues);     
        dpGet(c_SysInt_RDB_ArchiveDp + c_SysInt_RDB_ArchiveDpe_FlushInterval, g_SysInt_RDB_writeReadInterval);
        delay(g_SysInt_RDB_writeReadInterval/1000 + c_SysInt_RDB_DefaultConfig_ReadWriteSafetyTime);    
        time t2 = getCurrentTime();      
        sRoundtripFailedDPs="";     
        
        if(bCheckRoundtrip) {  // don't do the checking at the first run of the loop
        // Retrieve values from RDB Archive and compare them with previously set values
          for(int i=1;i<=dynlen(dsTestDPEs);i++) {
            dyn_int diDbValues;  
            dyn_time dtDbTimes;          
            dpGetPeriod(t1, t2, 0, dsTestDPEs[i], diDbValues, dtDbTimes);          
            err = getLastError();
            if(dynlen(err) > 0) DebugTN(err); 
            if(!dynContains(diDbValues, diTestValues[i])) { //roundtrip failed
              bRoundtripFailed = true;
              sRoundtripFailedDPs += (dpSubStr(dsTestDPEs[i], DPSUB_DP) + ", "); 
            }   
          }      
        }  
        bCheckRoundtrip = true;
        if(bRoundtripFailed) {
          unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_RoundtripFailed, "Failed test DPs: " + sRoundtripFailedDPs);   
        } 
      }
      else
      {
        unSystemIntegrity_RDB_throwError("ERROR", c_SysInt_RDB_Catalogue_TestDpNotFound);          
      }                
    }     
    if(bRoundtripFailed) {
      unSystemIntegrity_RDB_presetErrorCode(iErrorCode, c_SysInt_RDB_AlertLevel_RoundtripFailed);      
    }
    
    
    // Write ErrorCode into alarm datapoint of c_SysInt_RDB_ArchiveDp
    unSystemIntegrity_RDB_writeErrorCode(c_SysInt_RDB_ArchiveDp, iErrorCode);
    
    delay(g_SysInt_RDB_loopDelay); 
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_presetErrorCode
/**
* Compare error code and set it if is higher than current error code
*
* @param iErrorCode     input/output, current error code
* @param iNewCode       input, new error code
* @param bForceNewCode  output, false: Write iErrorCode only if iNewCode is higher, true: Always write iErrorCode
*/
private void unSystemIntegrity_RDB_presetErrorCode(int &iErrorCode, int iNewCode, bool bForceNewCode=false)
{
  if(bForceNewCode) {
    iErrorCode = iNewCode;
  }
  else {    
    if(iNewCode > iErrorCode) {
      iErrorCode = iNewCode;
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_writeErrorCode
/**
* Write error code to alarm datapoint of the specified RDB archive 
*
* @param sRDBArchive    input, Name of the RDB archive
* @param iErrorCode     input, error code to write
*/
private void unSystemIntegrity_RDB_writeErrorCode(string sRDBArchive, int iErrorCode)
{
  dpSet( c_unSystemAlarm_dpPattern + RDB_pattern + sRDBArchive + ".alarm", iErrorCode);  
}

//------------------------------------------------------------------------------------------------------------------------


// unSystemIntegrity_RDB_throwError
/**
* Write special error to message log
*
* @param severity     input, severity of the message
* @param code         input, error code to write
* @param sDetails     input, message details
*/
private void unSystemIntegrity_RDB_throwError(string severity = "ERROR", int code = 1, string sDetails="")
{
  int prio = PRIO_WARNING; //seems that throwing a fatal error makes Pmon stop monitoring this manager????
  int type = ERR_CONTROL;
  
  switch(strtoupper(severity))
  {
    case "INFO":    prio=PRIO_INFO;    break;
    case "WARNING": prio=PRIO_WARNING; break;
    case "ERROR":   prio=PRIO_SEVERE;  break;
  }
  
  errClass err = makeError("unSystemIntegrity_RDB", prio, type, code, sDetails);
  throwError(err);
  
  return;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_findManager
/**
* Get process information of a specific manager
*
* @param manager     input, manager name
* @param commandLine input, manager option
* @param startMode   output, manager start mode
* @param managerPos  output, manager pmon idx
* 
* @return            output, true=OK, false=ERROR
*/
private bool unSystemIntegrity_RDB_findManager(string manager, string commandLine, string &startMode, int &managerPos)
{
  dyn_mixed managerInfo;  
  fwInstallationManager_getProperties(manager, commandLine, managerInfo);  
  int len = dynlen(managerInfo);  
  if(len >= FW_INSTALLATION_MANAGER_PMON_IDX && len >= FW_INSTALLATION_MANAGER_START_MODE ) {
    managerPos = managerInfo[FW_INSTALLATION_MANAGER_PMON_IDX];
    startMode = managerInfo[FW_INSTALLATION_MANAGER_START_MODE];
    return true;
  }    
  return false; 
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_RDB_getBufferConfig
/**
* Read config file and get buffer to disc mode
*
* @return iBufferToDisk   output, mode 0,1,2 according to entry in config file
*/
private int unSystemIntegrity_RDB_getBufferConfig()
{  
  int iBufferToDisk;   
  string fileName =  getPath(CONFIG_REL_PATH, "config");  
  paCfgReadValue(fileName, "ValueArchiveRDB", "bufferToDisk", iBufferToDisk);    
  if(iBufferToDisk == -1) {
    iBufferToDisk = 1;
  }  
  return iBufferToDisk;
}

// unSystemIntegrity_RDB_compareValueWithMaxAndConsiderHysteresis
/**
* Compare a value with a maximum value and take hysteresis into account
*
* @param bState       input, previous state of the comparison (needed if we're in the hysteresis)
* @param iValueAct    input, actual value, this is the value we want to compare 
* @param iValueMax    input, maximum value, this is the value we compare iValueAct with
* @param iHysteresis  input, hyseresis (beneath iValueMax)
* 
* @return iBufferToDisk   output, mode 0,1,2 according to entry in config file
*/
private bool unSystemIntegrity_RDB_compareValueWithMaxAndConsiderHysteresis(bool &bState, int iValueAct, int iValueMax, int iHysteresis)
{
  if(iValueAct >= iValueMax) {
    bState = true;
  }
  else if(iValueAct < iValueMax-iHysteresis) {
    bState = false;  
  }
  return bState;
}

