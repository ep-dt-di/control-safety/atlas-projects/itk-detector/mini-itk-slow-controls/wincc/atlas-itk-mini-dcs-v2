/**@name LIBRARY: unSystemIntegrity_ctrl.ctl

@author: Piotr Kozlowski (EN-ICE-SCD)

Creation Date: 24/04/2013

Modification History: 
  24/04/2013: P. Kozlowski
  - PSEN-214: Initial version, 1-to-1 copy of unSystemIntegrity_ctrl.ctl
  
version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the ctrl component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: ctrl_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_CTRL = "ctrl";
const string UN_SYSTEM_INTEGRITY_CTRL_callback = "unSystemIntegrity_ctrl_CheckingCallback";
const string ctrl_pattern = "ctrl_";

// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_ctrlList; // list of the ctrl dp checked
global mapping g_mSystemIntegrityCtrl;

//@{

//------------------------------------------------------------------------------------------------------------------------
// ctrl_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string ctrl_systemIntegrityInfo()
{
  return makeDynString(ctrl_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_ctrl_Initialize
/**
Purpose:
Get the list of ctrl check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled ctrl that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_ctrl_Initialize(dyn_string &dsResult)
{
  //DebugN("unSystemIntegrity_ctrl_Initialize");
	dyn_string dsList;
	int len, i;
	string dpToCheck;
	bool enabled;
	
// get the list of ctrl manager dp to checked
	dsList = dpNames(c_unSystemAlarm_dpPattern+ctrl_pattern+"*", c_unSystemAlarm_dpType);
//DebugN("ctrl:", dsList);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+ctrl_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
	}
//DebugN(dsResult);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_ctrl_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_ctrl_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
  //DebugN("unSystemIntegrity_ctrl_HandleCommand");
	dyn_string exceptionInfo;
	int i, len =dynlen(parameters);
  string sMessage;
	
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_ctrl_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_ctrl_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+ctrl_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+ctrl_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+ctrl_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+ctrl_pattern+parameters[i]))
					unSystemIntegrity_ctrl_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+ctrl_pattern+parameters[i]))
					unSystemIntegrity_ctrl_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(UN_SYSTEM_INTEGRITY_CTRL+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_CTRL+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_ctrlList);
			break;
    case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
      dpSet(UN_SYSTEM_INTEGRITY_CTRL+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_CTRL+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
		default:
			break;
	}

	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_ctrl_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error in case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_ctrl_HandleCommand", exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_ctrlList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_ctrl_checking
/**
Purpose:
This function register/de-register the callback funtion of ctrl manager. This function can also create the _unSystemAlarm_ctrl dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_ctrl_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
  //DebugN("unSystemIntegrity_ctrl_checking");
	string sCtrlNum, dp, description;
	int res;
	
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;
	sCtrlNum = dp;
	dp = c_unSystemAlarm_dpPattern+ctrl_pattern+dp;
//DebugN("_unSystemIntegrity_set_ctrl_checking", dp, bCreate, bRegister, sCtrlNum);
	g_mSystemIntegrityCtrl[sCtrlNum] = -1;
	if(bCreate) {
// create the sDp and its alarm config if it is not existing
		if(!dpExists(dp)) {
			description = getCatStr("unSystemIntegrity", "CTRL_DESCRIPTION")+sCtrlNum;
			unSystemIntegrity_createSystemAlarm(sCtrlNum, ctrl_pattern, description, exceptionInfo);
		}
	}

	if(dynlen(exceptionInfo)<=0)
		_unSystemIntegrity_registerFunction(bRegister, UN_SYSTEM_INTEGRITY_CTRL_callback, makeDynString("_Connections.Ctrl.ManNums"), 
					g_unSystemIntegrity_ctrlList, dp, exceptionInfo);

}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_ctrl_CheckingCallback
/**
Purpose:
Callback function. This function is called when the system alarm is enabled for this device.

	@param sDp1: string, input, data point name
	@param manNum: dyn_int, input, list of ctrl numbers connected to the event manager

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_ctrl_CheckingCallback(string sDp1, dyn_int manNum)
{
   //DebugN("unSystemIntegrity_ctrl_CheckingCallback");
	int value, i, len, ctrlNum, pos;
	dyn_string dsTemp;
	string sCtrlNum;
	
	dsTemp = g_unSystemIntegrity_ctrlList;
//	DebugN(getCurrentTime(), "CTRL", sDp1, manNum, dsTemp);
	len = dynlen(dsTemp);
	for(i=1; i<=len; i++) {
		sCtrlNum = substr(dsTemp[i], strlen(c_unSystemAlarm_dpPattern+ctrl_pattern),strlen(dsTemp[i]));
		sscanf(sCtrlNum, "%d", ctrlNum);
		pos = dynContains(manNum, ctrlNum);
		if(pos>0) { // ctrl found
			value = c_unSystemIntegrity_no_alarm_value;
		}
		else { //ctrl not found
			value = c_unSystemIntegrity_alarm_value_level1;
		}
		if(g_mSystemIntegrityCtrl[sCtrlNum] != value) {
			dpSet( c_unSystemAlarm_dpPattern+ctrl_pattern+sCtrlNum+".alarm", value);
			g_mSystemIntegrityCtrl[sCtrlNum] = value;
		}
	}
}

//@}
