/**@name LIBRARY: unSystemIntegrity_unicosS7PLC.ctl

@author: Enrique Blanco (AB-CO)

Creation Date: 12/07/2002

Modification History: 
  31/01/2012: Herve
  - IS-706  unSystemIntegrity  S7 PLC systemIntegrity fix: add a S7 PLC, activates the comm of the S7 TSPP and S7 POLLING 
 
  16/06/2011: Herve
  - IS-547: get the list of systemAlarm pattern
  
  10/03/2011: Herve
    - IS-505: Enable/Disable the regular GQ opening in the driver
    
  20/12/2010: Herve
    - IS-468]: periodic S7 systemIntegrity ERROR alarm linked with the periodic general query, ConnState set t0 3 during a GQ.
    
  25/08/2010: Herve
    - IS-372: 
        extended diagnostic
        unSystemIntegrity_unicosS7PLCErrorCallback: update only if error is different instead of every time
        exclusive lock on send ip
        bug fix: "_Connections.Driver.ManNums" CB started at each call to unSystemIntegrity_unicosS7PLC_HandleCommand
        when the com is restored, launch a time synchro, delay 500msec & request all data 
        every multiple of check S7 PLC open the GQ at the driver level, a 0 value means default (=30)
    
	29/10/2008: Frederic
		- Update unSystemIntegrity_synchronizedTimeDateOfPlc_S7()  - use the DLL functions to get the correct UTC time (cf. CtrlTimeUtils.dll or .so)
	
	30/07/2007: Herve
		- set _Driverx.SM just before a GQ
		
	24/11/2005: Herve
		- add dpExists in unSystemIntegrity_unicosPLC_Initialize

	26/08/2005: Herve
		- bug in unSystemIntegrity_unicosS7PLCTimeCallback: set of the alarm everytime the counter is updated, should be
		if there is an alarm or if the alarm is reseted (use a global variable).
		

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the S7_PLC component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: unicosS7PLC_systemIntegrityInfo
	. PVSS version: 3.0 
	. operating system: Win XP
	. distributed system: yes.
*/

# uses "CtrlTimeUtils"

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_unicosS7PLC = "unicosS7PLC";
const string UN_SYSTEM_INTEGRITY_unicosS7PLC_check = "unSystemIntegrity_unicosS7PLCCheck";
const string UN_SYSTEM_INTEGRITY_unicosS7PLC_IPSending = "unSystemIntegrity_unicosS7PLCIPSending";
const string UN_SYSTEM_INTEGRITY_unicosS7PLC_TimeSynchro = "unSystemIntegrity_unicosS7PLCTimeSynchro";

const string c_unSystemIntegrity_S7PLC_DS_Time_callback = "unSystemIntegrity_unicosS7PLCTimeCallback";
const string c_unSystemIntegrity_S7PLC_DS_Error_callback = "unSystemIntegrity_unicosS7PLCErrorCallback";
const string c_unSystemIntegrity_S7PLCRedu1_DS_Error_callback = "unSystemIntegrity_unicosS7PLCRedu1ErrorCallback";
const string c_unSystemIntegrity_S7PLCRedu2_DS_Error_callback = "unSystemIntegrity_unicosS7PLCRedu2ErrorCallback";

// Already defined in unSystemIntegrity_unicosPLC.ctl
// const string PLC_DS_pattern = "DS_Comm_";
// const string DS_pattern = "DS_Comm_";
// const string PLC_DS_Time_pattern = "DS_Time_";
// const string DS_Time_pattern = "DS_Time_";
// const string PLC_DS_Error_pattern = "PLC_DS_Error_";
// const string PLC_PLC_pattern = "PLC_PLC_Comm_";
// const string FE_pattern = "FESystemAlarm_";
// const string UN_PLC_DPTYPE = "_UnPlc";

// ???????????? Queseto ???????????
// const string c_unSystemIntegrity_ModPlc = "_Mod_Plc_";

// constant for S7 system integrity
// const string c_unSystemIntegrity_UnPlc = "_unPlc_";
const string c_unSystemIntegrity_S7 = "S7_PLC_";

// ???? no se si vale para algo...
const string UN_SYSTEM_INTEGRITY_S7 = "S7_PLC";

const string S7_Conn_Error_pattern = "S7_Conn_Error_";

// Already defined in unSystemIntegrity_unicosPLC.ctl 
const int c_unSystemIntegrity_defaultS7PLCCheckingDelay = 20; // default value for the checking in sec.
const int c_unSystemIntegrity_defaultS7PLCSendIpDelay = 4500; // default value for the send ip in msec.
const int c_unSystemIntegrity_defaultS7PLCTimeSynchroDelay = 300; // default value for the synchronizing the PLC time in sec.
const int c_unSystemIntegrity_functionParameterLenthS7 = 5; // 5 parameters must be given to the function
const int c_unSystemIntegrity_S7PLCSynchonizedTimeDate = 1; // synchronise the clock of the PLC
const int c_unSystemIntegrity_S7PLCGetAll_BS_AS_table = 2; // request all the AS and BS table
// defined already in unSystemIntegrity_unicosPLC.ctl
// const string CONNECTION_DRIVER = "_Connections.Driver.ManNums";
// const int CONST_CHANGE_DIFF = 600; // 10 min in te past is allowed for the time stamp of the PLC.
const int c_unSystemIntegrity_S7PLCStartComDelayBefore_GQ =500;
mapping g_unSystemIntegrity_S7reset;
mapping g_unSystemIntegrity_S7_PLC_TimeAlarm; // variable used to keep the latest alarm.
const int c_unSystemIntegrity_S7PLCGQThreadNbCheck = 30;
// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_S7PLC_DSList; // list of the PLC-DS dp checked
global dyn_int g_unSystemIntegrity_S7PLC_DSThreadId; // list of the PLC-DS thread Id checking the counter

global dyn_string g_unSystemIntegrity_S7PLC_PlcList; // list of the S7_Conn Dp.
global dyn_string g_unSystemIntegrity_S7PLC_PlcListPolling; // list of the S7_Conn Dp. (only for POLLING!!!)

global dyn_int g_unSystemIntegrity_S7PLC_OldError; // list of the PLC-DS thread Id
global dyn_int g_unSystemIntegrity_S7PLC_OldReduError; // list of the PLC-DS thread Id for redundant plc
global dyn_int g_unSystemIntegrity_S7PLCIPSending_ThreadId; // list of the thread Id sending the IP to the PLC
global dyn_int g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId; // list of the thread Id synchronising the data and time of the PLC
global int g_unSystemIntegrity_S7PLCCheckingDelay;
global int g_unSystemIntegrity_S7PLCIpSendingDelay;
global int g_unSystemIntegrity_S7PLCTimeSynchroDelay;
global int g_IPValueToSendS7;
global dyn_int g_unSystemIntegrity_S7PLC_state;

global dyn_int g_S7DriverConnection; // keep the list of driver num started.
global bool g_bUnicosS7PLCNotStarted = true;
global bool g_unSystemIntegrityS7PLCLock;
global mapping g_m_unSystemIntegrity_S7PLC_S7Started; // 
global int g_i_unSystemIntegrity_S7PLC_S7DriverGQThread; 
global int g_unSystemIntegrity_S7PLCGQThreadNbCheck;

//@{

//------------------------------------------------------------------------------------------------------------------------
// unicosS7PLC_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string unicosS7PLC_systemIntegrityInfo()
{
  return makeDynString(PLC_DS_pattern, PLC_DS_Error_pattern, PLCRedu1_DS_Error_pattern, PLCRedu2_DS_Error_pattern, PLC_DS_Time_pattern, FE_pattern, PLC_PLC_pattern, IMPORT_PATTERN);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLC_Initialize
/**
Purpose:
Get the list of unicosS7PLC check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled unicosPLC that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLC_Initialize(dyn_string &dsResult)
	{
		dyn_string dsList;
		int len, i;
		string dpToCheck, sIP, sHostName;
		bool enabled;
	
		// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLC_Initialize(dsResult= "+dsResult);
		g_unSystemIntegrity_S7PLCGQThreadNbCheck = c_unSystemIntegrity_S7PLCGQThreadNbCheck;
		// get the ip value to send (Funcitons OK for S7)
		unGenericDpFunctions_getHostName(getSystemName(), sHostName, sIP);
		g_IPValueToSendS7 = unConfigGenericFunctions_convertIP(sIP);
		// DebugN("---->> g_IPValueToSendS7= "+g_IPValueToSendS7);
		
		// Get the config data (adapted to S7)
		dpGet(UN_SYSTEM_INTEGRITY_unicosS7PLC+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
		unSystemIntegrity_unicosS7PLC_DataCallback("", dsList);
	
		dsList = dpNames(c_unSystemAlarm_dpPattern+PLC_DS_pattern+"*", c_unSystemAlarm_dpType);
		// DebugN("---->> PLC_DS:" + dsList);
		len = dynlen(dsList);
		for(i = 1; i<=len; i++) 
			{
				dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
				dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+PLC_DS_pattern),strlen(dpToCheck));
				// DebugN(" --->> dpToCheck = "+dpToCheck);
				if(dpExists(dpToCheck))
				{
					if(dpTypeName(dpToCheck) == S7_PLC_DPTYPE) 
						{
							dpGet(dsList[i]+".enabled", enabled);
							if(enabled)
								dynAppend(dsResult, dpToCheck);
						}
				}
			} 
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLC_ConnectionDriverCB
/**
Purpose:
callback on the list of connected driver.

@param sDpe1: 				string, 	input, dpe
@param diDriverNum: 	dyn_int, 	input, the list of driver number started

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLC_ConnectionDriverCB(string sDp, dyn_int diDriverNum)
{
	dyn_string dsPlcList = g_unSystemIntegrity_S7PLC_DSList;
	string sPlcDpName, s7PlcDp;
	int i, len, drvNbr, plcNbr;
	
	// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLC_ConnectionDriverCB(sDp= "+sDp+" diDriverNum= "+diDriverNum);
	
	g_S7DriverConnection = diDriverNum;
	len = dynlen(dsPlcList);
	for(i=1;i<=len;i++) 
		{
			sPlcDpName = substr(dsPlcList[i], strlen(c_unSystemAlarm_dpPattern+PLC_DS_pattern), strlen(dsPlcList[i]));
			if(dpExists(sPlcDpName)) 
				{
					// Adapt to S7 PLC (s7PlcDp does NOTHING here !!!!)
					dpGet(sPlcDpName+".communication.driver_num", drvNbr, sPlcDpName+".configuration.S7_Conn", s7PlcDp);
					if(!dynContains(diDriverNum, drvNbr)) 
						{
							// set the re-initialise send IP.
							g_unSystemIntegrity_S7reset[sPlcDpName] = true;
							// DebugN("--> g_unSystemIntegrity_S7reset= "+g_unSystemIntegrity_S7reset);
						}
				}
		}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLC_ConnectionDriverCB", "sDp= "+sDp+" diDriverNum= "+diDriverNum);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLC_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: 			string, input, dpe
@param command: 		int, input, the systemIntegrity command
@param sDpe2: 			string, input, dpe
@param parameters: 	dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLC_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo;
	string sPlc;
	int i, len =dynlen(parameters);
        dyn_string dsResult;
        string sResult;

	// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLC_HandleCommand(sDpe1= "+sDpe1+" command= "+command+" sDpe2= "+sDpe2+" parameters= "+parameters); 
	
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			// DebugN("---->>, command= UN_SYSTEMINTEGRITY_ADD");
			for(i=1; i<=len; i++) {
				unSystemIntegrity_unicosS7PLC_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			// DebugN("---->>, command= UN_SYSTEMINTEGRITY_DELETE");
			for(i=1; i<=len; i++) {
				unSystemIntegrity_unicosS7PLC_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]);

				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+parameters[i]);

				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+parameters[i]);

  				// remove redu dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+parameters[i]);

  				// remove redu dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			// DebugN("---->>, command= UN_SYSTEMINTEGRITY_ENABLE");
			for(i=1; i<=len; i++) 
				{
					if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]))
						{
							unSystemIntegrity_unicosS7PLC_checking(parameters[i], false, true, exceptionInfo);
						}
				}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			// DebugN("---->> , command= UN_SYSTEMINTEGRITY_DISABLE");
			for(i=1; i<=len; i++) 
				{
					if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]))
						{
							unSystemIntegrity_unicosS7PLC_checking(parameters[i], false, false, exceptionInfo);
						}	
				}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			// DebugN("---->>, command= UN_SYSTEMINTEGRITY_DIAGNOSTIC");
			dpSet(UN_SYSTEM_INTEGRITY_unicosS7PLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_unicosS7PLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_S7PLC_DSList);
			break;
		case UN_SYSTEMINTEGRITY_EXTENDED_DIAGNOSTIC: // extended diagnostic
			dynAppend(dsResult, getCurrentTime());
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLC_DSList");
			len = dynlen(g_unSystemIntegrity_S7PLC_DSList);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLC_DSList[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLC_DSThreadId");
			len = dynlen(g_unSystemIntegrity_S7PLC_DSThreadId);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLC_DSThreadId[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLC_PlcList");
			len = dynlen(g_unSystemIntegrity_S7PLC_PlcList);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLC_PlcList[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLC_PlcListPolling");
			len = dynlen(g_unSystemIntegrity_S7PLC_PlcListPolling);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLC_PlcListPolling[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLC_OldError");
			len = dynlen(g_unSystemIntegrity_S7PLC_OldError);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLC_OldError[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLC_OldReduError");
			len = dynlen(g_unSystemIntegrity_S7PLC_OldReduError);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLC_OldReduError[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLCIPSending_ThreadId");
			len = dynlen(g_unSystemIntegrity_S7PLCIPSending_ThreadId);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLCIPSending_ThreadId[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId");
			len = dynlen(g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId[i];
				dynAppend(dsResult, sResult);
			}
			
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLCCheckingDelay");
			dynAppend(dsResult, g_unSystemIntegrity_S7PLCCheckingDelay);
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLCIpSendingDelay");
			dynAppend(dsResult, g_unSystemIntegrity_S7PLCIpSendingDelay);
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLCTimeSynchroDelay");
			dynAppend(dsResult, g_unSystemIntegrity_S7PLCTimeSynchroDelay);
			dynAppend(dsResult, "g_IPValueToSendS7");
			dynAppend(dsResult, g_IPValueToSendS7);
			
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLC_state");
			len = dynlen(g_unSystemIntegrity_S7PLC_state);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_S7PLC_state[i];
				dynAppend(dsResult, sResult);
			}
			
			dynAppend(dsResult, "g_S7DriverConnection");
			len = dynlen(g_S7DriverConnection);
			for(i=1; i<=len; i++) {
				sResult = g_S7DriverConnection[i];
				dynAppend(dsResult, sResult);
			}
			
			len=mappinglen(g_unSystemIntegrity_S7reset);
			for(i=1;i<=len;i++)
			{
				dynAppend(dsResult, "g_unSystemIntegrity_S7reset["+mappingGetKey(g_unSystemIntegrity_S7reset, i)+"]");
				dynAppend(dsResult, mappingGetValue(g_unSystemIntegrity_S7reset, i));
			}
			
			len=mappinglen(g_unSystemIntegrity_S7_PLC_TimeAlarm);
			for(i=1;i<=len;i++)
			{
				dynAppend(dsResult, "g_unSystemIntegrity_S7_PLC_TimeAlarm["+mappingGetKey(g_unSystemIntegrity_S7_PLC_TimeAlarm, i)+"]");
				dynAppend(dsResult, mappingGetValue(g_unSystemIntegrity_S7_PLC_TimeAlarm, i));
			}
			
			dynAppend(dsResult, "g_i_unSystemIntegrity_S7PLC_S7DriverGQThread");
			dynAppend(dsResult, g_i_unSystemIntegrity_S7PLC_S7DriverGQThread);
			
			len=mappinglen(g_m_unSystemIntegrity_S7PLC_S7Started);
			for(i=1;i<=len;i++)
			{
				dynAppend(dsResult, "g_m_unSystemIntegrity_S7PLC_S7Started["+mappingGetKey(g_m_unSystemIntegrity_S7PLC_S7Started, i)+"]");
				dynAppend(dsResult, mappingGetValue(g_m_unSystemIntegrity_S7PLC_S7Started, i));
			}
			
			dynAppend(dsResult, "g_unSystemIntegrity_S7PLCGQThreadNbCheck");
			dynAppend(dsResult, g_unSystemIntegrity_S7PLCGQThreadNbCheck);
			
			dpSet(UN_SYSTEM_INTEGRITY_unicosS7PLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
				UN_SYSTEM_INTEGRITY_unicosS7PLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", dsResult);
			break;
		case UN_SYSTEMINTEGRITY_DEBUG:
			if(dynlen(parameters) >= 2)
			{
				g_b32DebugLevel = (bit32)parameters[1];
				g_sDebugFilter = parameters[2];
			}
			else
			{
				g_b32DebugLevel = (bit32)0;
				g_sDebugFilter = "";
			}

			dpSet(UN_SYSTEM_INTEGRITY_unicosS7PLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
				UN_SYSTEM_INTEGRITY_unicosS7PLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(g_b32DebugLevel, g_sDebugFilter));
			break;
		default:
			break;
	}

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLC_HandleCommand", command, parameters);
	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", "unSystemIntegrity_unicosS7PLC_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error uin case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			// DebugN(getCurrentTime(), exceptionInfo);
		}
	}

// do here the dpConnect to the list of drivers
	if(g_bUnicosS7PLCNotStarted) {
		dpConnect("unSystemIntegrity_unicosS7PLC_ConnectionDriverCB", CONNECTION_DRIVER);
		g_bUnicosS7PLCNotStarted = false;
	}
        if(g_i_unSystemIntegrity_S7PLC_S7DriverGQThread <=0)
          g_i_unSystemIntegrity_S7PLC_S7DriverGQThread = startThread("unSystemIntegrity_S7PLC_S7DriverGQThread");

//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_S7PLC_DSList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLC_DataCallback
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param sConfigData: string, input, the config data

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLC_DataCallback(string sDpe1, dyn_string dsConfigData)
{
	dyn_string dsTemp=dsConfigData;

	// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLC_DataCallback(sDpe1= "+sDpe1+" dsConfigData= "+dsConfigData);
	
	while(dynlen(dsTemp) <4)
		dynAppend(dsTemp, "0");

	g_unSystemIntegrity_S7PLCCheckingDelay = (int)dsTemp[1];
	if(g_unSystemIntegrity_S7PLCCheckingDelay <= 0)
		g_unSystemIntegrity_S7PLCCheckingDelay = c_unSystemIntegrity_defaultS7PLCCheckingDelay;	

	g_unSystemIntegrity_S7PLCIpSendingDelay = (int)dsTemp[2];
	if(g_unSystemIntegrity_S7PLCIpSendingDelay <= 0)
		g_unSystemIntegrity_S7PLCIpSendingDelay = c_unSystemIntegrity_defaultS7PLCSendIpDelay;
	
	g_unSystemIntegrity_S7PLCTimeSynchroDelay = (int)dsTemp[3];
	if(g_unSystemIntegrity_S7PLCTimeSynchroDelay <= 0)
		g_unSystemIntegrity_S7PLCTimeSynchroDelay = c_unSystemIntegrity_defaultS7PLCTimeSynchroDelay;
        
        g_unSystemIntegrity_S7PLCGQThreadNbCheck = (int)dsTemp[4];
        if(g_unSystemIntegrity_S7PLCGQThreadNbCheck == 0)
          g_unSystemIntegrity_S7PLCGQThreadNbCheck = c_unSystemIntegrity_S7PLCGQThreadNbCheck;

  // DebugN("---->>dsConfigData= "+dsConfigData+" g_unSystemIntegrity_S7PLCCheckingDelay="+g_unSystemIntegrity_S7PLCCheckingDelay+" g_unSystemIntegrity_S7PLCIpSendingDelay="+g_unSystemIntegrity_S7PLCIpSendingDelay+" g_unSystemIntegrity_S7PLCTimeSynchroDelay= "+g_unSystemIntegrity_S7PLCTimeSynchroDelay);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLC_DataCallback", dsConfigData, "---->>dsConfigData= "+dsConfigData+" g_unSystemIntegrity_S7PLCCheckingDelay="+g_unSystemIntegrity_S7PLCCheckingDelay+" g_unSystemIntegrity_S7PLCIpSendingDelay="+g_unSystemIntegrity_S7PLCIpSendingDelay+" g_unSystemIntegrity_S7PLCTimeSynchroDelay= "+g_unSystemIntegrity_S7PLCTimeSynchroDelay, "g_unSystemIntegrity_S7PLCGQThreadNbCheck = "+g_unSystemIntegrity_S7PLCGQThreadNbCheck);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLC_checking
/**
Purpose:
This function register/de-register the callback funtion of unicosS7PLC manager. 
This function can also create the _unSystemAlarm_unicosS7PLC dp 
with the alarm config but it cannot delete it.

	@param sDp:						string, 		input, 	data point name
	@param bCreate: 			bool, 			input, 	true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: 		bool, 			input, 	true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP
	. distributed system: yes.
*/

unSystemIntegrity_unicosS7PLC_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string sDpToChek, dp, dpTime, dpError, dpPlc, plcHostname, description;
	int res, thId, pos, drvNum, len,i;
	bool bError = false;
	dyn_string plc_plc_list;

	string alertPanel,alertHelp;
	bool configExists, isActive;
	int alertConfigType;
	dyn_string alertTexts,alertClasses,summaryDpeList,alertPanelParameters;
	dyn_float alertLimits;
 
 int iReduDevNr;
 bool bReduPlc;
 string dpRedu1Error,dpRedu2Error;
	
// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLC_checking(sDp= "+sDp+" bCreate= "+bCreate+" bRegister= "+bRegister);
	
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;
	sDpToChek = dp;
	
	// DS_Time_, PLC_DS_Error_, DS_Comm_
	dpTime = c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+dp;
	dpError = c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+dp;
	dpRedu1Error = c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+dp;
	dpRedu2Error = c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern+dp;
	dp = c_unSystemAlarm_dpPattern+PLC_DS_pattern+dp;

	// DebugN("--->> dpTime= "+dpTime+" dpError= "+dpError+" dp= "+dp);
	
// in case the Dp is not of type S7_PLC do nothing. Should not happen, just for security.
	if(dpTypeName(sDpToChek) != S7_PLC_DPTYPE)
		{
			fwException_raise(exceptionInfo, "ERROR", 
							"unSystemIntegrity_unicosS7PLC_checking(): "+"Dp is not of type: "+S7_PLC_DPTYPE+" "+dp,"");
			return;
		}
	
	// Get the PLC name. Maybe two entries are needed to catch both TSPP and Polling connections to the PLC.
	dpGet(sDpToChek+".configuration.S7_Conn", dpPlc,
       sDpToChek+".communication.driver_num", drvNum);
 //check if it is Redundant PLC
 dpGet(dpPlc+".ReduCP.DevNr",iReduDevNr);  
 bReduPlc = iReduDevNr>0;
 
// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLC_checking - bReduPlc",bReduPlc);
 
	// Create if does not exists and enable
	// DebugN("----> create and enable systemAlarms");
	if(bCreate) 
		{
			// get the plc hostname and the driver number
			plcHostname = unGenericDpFunctions_getAlias(sDpToChek);
			//		dpGet( sDpToChek+".communication.counter:_distrib.._driver", drvNum);
			// create the sDp and its alarm config if it is not existing
			if(!dpExists(dp)) 
				{
					description = getCatStr("unSystemIntegrity", "S7_PLC_DS_DESCRIPTION")+plcHostname+" -> DS driver "+ drvNum;
					unSystemIntegrity_createSystemAlarm(sDpToChek, PLC_DS_pattern, description, exceptionInfo);
				}
			// create the sDp time and its alarm config if it is not existing
			if(!dpExists(dpTime)) 
				{
					description = getCatStr("unSystemIntegrity", "S7_PLC_DS_TIME_DESCRIPTION")+plcHostname+" -> DS driver "+ drvNum;
					unSystemIntegrity_createSystemAlarm(sDpToChek, PLC_DS_Time_pattern, description, exceptionInfo);
				}
			// create the sDp Error and its alarm config if it is not existing
			// if redundant PLC, create the sDp Error for the second device and its alarm config if it is not existing
			if(bReduPlc)
   {
      if(!dpExists(dpRedu1Error)) 
  				{
  					description = getCatStr("unSystemIntegrity", "S7_PLC_DS_ERROR_DESCRIPTION") +drvNum+" -> "+plcHostname + " 1";
  					unSystemIntegrity_createSystemAlarm(sDpToChek, PLCRedu1_DS_Error_pattern, description, exceptionInfo);
  				}
      if(!dpExists(dpRedu2Error)) 
  				{
  					description = getCatStr("unSystemIntegrity", "S7_PLC_DS_ERROR_DESCRIPTION") +drvNum+" -> "+plcHostname + " 2";
  					unSystemIntegrity_createSystemAlarm(sDpToChek, PLCRedu2_DS_Error_pattern, description, exceptionInfo);
  				}
   }
   else //the PLC is not redundant
   {
  			if(!dpExists(dpError)) 
  				{
  					description = getCatStr("unSystemIntegrity", "S7_PLC_DS_ERROR_DESCRIPTION")+drvNum+" -> "+plcHostname;
  					unSystemIntegrity_createSystemAlarm(sDpToChek, PLC_DS_Error_pattern, description, exceptionInfo);
  				}   
   }
		}

	// DebugN("--> dpConnects, counter, ip, syncro");
	// Connection or disconnection
	if(bRegister) 
		{
			// connect to the callback function if not already done
			pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_S7PLC_DSList, dp);
			if(pos <= 0) 
				{
					// add it in the list, because one of the callback function needs it.				
					pos = _unSystemIntegrity_setList(g_unSystemIntegrity_S7PLC_DSList, dp, true);
	
					// start the thread for checking the counter (first modification for S7: UN_SYSTEM_INTEGRITY_unicosS7PLC)
					// DebugN(" ===>> Start thread counter");
					thId = startThread(UN_SYSTEM_INTEGRITY_unicosS7PLC_check, sDpToChek, drvNum);
					g_unSystemIntegrity_S7PLC_DSThreadId[pos] = thId;
					if(thId<0)
						bError = true;
	
					if(!bError) 
						{
							// start the thread for sending the ip
							g_unSystemIntegrity_S7reset[sDpToChek] = true;
							// first modif for S7 (UN_SYSTEM_INTEGRITY_unicosS7PLC)
							// DebugN(" ===>> Start thread IP send");
							thId = startThread(UN_SYSTEM_INTEGRITY_unicosS7PLC_IPSending, sDpToChek, drvNum);
							if(thId<0)
								bError = true;
							g_unSystemIntegrity_S7PLCIPSending_ThreadId[pos] = thId;
						}
					if(!bError) 
						{
							// start the thread for synchronising the PLC
							// first modif for S7 (UN_SYSTEM_INTEGRITY_unicosS7PLC)
							// DebugN(" ===>> Start thread syncronize");
							thId = startThread(UN_SYSTEM_INTEGRITY_unicosS7PLC_TimeSynchro, sDpToChek, drvNum);
							if(thId<0)
								bError = true;
							g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId[pos] = thId;
						}
				
					if(!bError) 
						{
		
							g_unSystemIntegrity_S7_PLC_TimeAlarm[dpSubStr(sDpToChek, DPSUB_SYS_DP)] = -1;
							// dpConnect to the stime of the counter
							// DebugN("===>> dpConnect time of counter");
							res = dpConnect(c_unSystemIntegrity_S7PLC_DS_Time_callback, 
																sDpToChek+".communication.counter:_online.._stime_inv",
																sDpToChek+".communication.counter:_online.._stime");
							if(res<0)
								bError = true;
			
							if(!bError) 
								{
									// dpConnect to _S7_Conn dp for communication error codes troubleshooting.
									g_unSystemIntegrity_S7PLC_PlcList[pos] = dpPlc;
									g_unSystemIntegrity_S7PLC_OldError[pos] = -1;
									g_unSystemIntegrity_S7PLC_OldReduError[pos] = -1;
									g_unSystemIntegrity_S7PLC_state[pos] = -1;
									
									// DebugN("---> dpConnect(c_unSystemIntegrity_S7PLC_DS_Error_callback="+c_unSystemIntegrity_S7PLC_DS_Error_callback+
									//				" dpPlc= "+dpPlc+" dpPlc+S7_POLLING_COMMUNICATION= "+dpPlc+S7_POLLING_COMMUNICATION);

        								
         //if it is Redundant PLC, also check for the second connection. In this case, only POLLING is checked
          if(bReduPlc)
          {
      									res = dpConnect(c_unSystemIntegrity_S7PLCRedu1_DS_Error_callback, 
      																	dpPlc+".LastError", 
      																	dpPlc+".ConnState",
      																	dpPlc+S7_POLLING_COMMUNICATION+".LastError", 
      																	dpPlc+S7_POLLING_COMMUNICATION+".ConnState");																
      									if(res<0)
      										bError = true;  
               
               res = dpConnect(c_unSystemIntegrity_S7PLCRedu2_DS_Error_callback, 
      																	dpPlc+".ReduCP.LastError", 
      																	dpPlc+".ReduCP.ConnState",
      																	dpPlc+S7_POLLING_COMMUNICATION+".ReduCP.LastError", 
      																	dpPlc+S7_POLLING_COMMUNICATION+".ReduCP.ConnState");	                 
      									if(res<0)
      										bError = true;                            
           }
          //if it is not redundant PLC, check the one and only PLC, TSPP and POLLING
          else
          {
      									res = dpConnect(c_unSystemIntegrity_S7PLC_DS_Error_callback, 
      																	dpPlc+".LastError", 
      																	dpPlc+".ConnState",
      																	dpPlc+S7_POLLING_COMMUNICATION+".LastError", 
      																	dpPlc+S7_POLLING_COMMUNICATION+".ConnState");															
	
      									if(res<0)
      										bError = true;            
          }
      
								}	
						}
					
					if(bError) 
						{
							pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_S7PLC_DSList, dp);
							if(pos > 0) 
								{
									// kill the threads if they were started
									res = stopThread(g_unSystemIntegrity_S7PLC_DSThreadId[pos]);
									res = stopThread(g_unSystemIntegrity_S7PLCIPSending_ThreadId[pos]);
									res = stopThread(g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId[pos]);
									// and then remove it from the list
									pos = _unSystemIntegrity_setList(g_unSystemIntegrity_S7PLC_DSList, dp, false);
									//DebugN("rmv", res);
									dynRemove(g_unSystemIntegrity_S7PLC_DSThreadId, pos);
									dynRemove(g_unSystemIntegrity_S7PLC_PlcList, pos);
									dynRemove(g_unSystemIntegrity_S7PLC_OldError, pos); 
									dynRemove(g_unSystemIntegrity_S7PLC_OldReduError, pos); 
									dynRemove(g_unSystemIntegrity_S7PLC_state, pos); 
									dynRemove(g_unSystemIntegrity_S7PLCIPSending_ThreadId, pos);
									dynRemove(g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId, pos);
								}
		
							fwException_raise(exceptionInfo, "ERROR", 
									"unSystemIntegrity_unicosS7PLC_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_CONNECT") +dp,"");
						}
					else 
						{
							// activate PLC connection (TSPP and POLLING) 	           
            dpSet(dpPlc+".Active",true, //TSPP
                  dpPlc+S7_POLLING_COMMUNICATION+".Active",true); // POLLING     
            
         //if it is Redundant PLC, activate also the secondary connection  
            if(bReduPlc)
            {
              dpSet(dpPlc+".ReduCP.Active",true, //TSPP
                    dpPlc+S7_POLLING_COMMUNICATION+".ReduCP.Active",true); // POLLING                   
            }
            
							// DebugN("===> Enable", g_unSystemIntegrity_S7PLC_DSThreadId, g_unSystemIntegrity_S7PLCIPSending_ThreadId, "end");
							// set the enable to true and activate the alarm if it is not activated.
							dpSet(dp+".enabled", true);
							unAlarmConfig_mask(dp+".alarm", false, exceptionInfo);
							dpSet(dpTime+".enabled", true);
							unAlarmConfig_mask(dpTime+".alarm", false, exceptionInfo);
       if(bReduPlc)
       {
  							dpSet(dpRedu1Error+".enabled", true);
  							unAlarmConfig_mask(dpRedu1Error+".alarm", false, exceptionInfo);
  							dpSet(dpRedu2Error+".enabled", true);
  							unAlarmConfig_mask(dpRedu2Error+".alarm", false, exceptionInfo);
       }
       else
       {
  							dpSet(dpError+".enabled", true);
  							unAlarmConfig_mask(dpError+".alarm", false, exceptionInfo);         
       }
							// get all the PLC-PLC dp, and enable them
							plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"_*", c_unSystemAlarm_dpType);
							len = dynlen(plc_plc_list);
							for(i=1;i<=len;i++) 
								{
									dpSet(plc_plc_list[i]+".enabled", true);
									unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
								}
			
							// get all the FESystemAlarm dp, and enable them, check the Ok state and set the correct value
							plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"_*", c_unSystemAlarm_dpType);
							len = dynlen(plc_plc_list);
							for(i=1;i<=len;i++) 
								{
									dpSet(plc_plc_list[i]+".enabled", true);
									unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
								}
                                            synchronized(g_m_unSystemIntegrity_S7PLC_S7Started)
                                            {
                                              g_m_unSystemIntegrity_S7PLC_S7Started[dpPlc] = drvNum;
                                            }
						}
				}
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLC_checking register", sDp);
		}
	else 
		{
			// disconnect the callback function
			// DebugN("====> Disable", g_unSystemIntegrity_S7PLC_DSThreadId, g_unSystemIntegrity_S7PLCIPSending_ThreadId, "end");
			pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_S7PLC_DSList, dp);
			if(pos > 0) 
				{
							// de-activate PLC connection (TSPP and POLLING) 	
            dpSet(dpPlc+".Active",false,
                  dpPlc+S7_POLLING_COMMUNICATION+".Active",false); // POLLING
            
            if(bReduPlc)
            {
              dpSet(dpPlc+".ReduCP.Active",false, //TSPP
                    dpPlc+S7_POLLING_COMMUNICATION+".ReduCP.Active",false); // POLLING                   
            }            
            
            synchronized(g_m_unSystemIntegrity_S7PLC_S7Started)
            {
              mappingRemove(g_m_unSystemIntegrity_S7PLC_S7Started, dpPlc);
            }
					// kill the thread
					res = stopThread(g_unSystemIntegrity_S7PLC_DSThreadId[pos]);
					if(res<0)
						bError = true;

					res = stopThread(g_unSystemIntegrity_S7PLCIPSending_ThreadId[pos]);
					if(res<0)
						bError = true;

					res = stopThread(g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId[pos]);
					if(res<0)
						bError = true;

					// dpDisconnect stime of the counter
					res = dpDisconnect(c_unSystemIntegrity_S7PLC_DS_Time_callback, 
													sDpToChek+".communication.counter:_online.._stime_inv",
													sDpToChek+".communication.counter:_online.._stime");
					if(res<0)
						bError = true;


     
     //if it is Redundant PLC, also disconnect the second connection
      if(bReduPlc)
      {
  									res = dpDisconnect(c_unSystemIntegrity_S7PLCRedu1_DS_Error_callback, 
    																	dpPlc+".LastError", 
    																	dpPlc+".ConnState",
    																	dpPlc+S7_POLLING_COMMUNICATION+".LastError", 
    																	dpPlc+S7_POLLING_COMMUNICATION+".ConnState");                 
    						if(res<0)
    								bError = true; 
          
  									res = dpDisconnect(c_unSystemIntegrity_S7PLCRedu2_DS_Error_callback, 
  																	dpPlc+".ReduCP.LastError", 
  																	dpPlc+".ReduCP.ConnState",
  																	dpPlc+S7_POLLING_COMMUNICATION+".ReduCP.LastError", 
  																	dpPlc+S7_POLLING_COMMUNICATION+".ReduCP.ConnState");	                 
    						if(res<0)
    								bError = true;         
      }
      else//non redundant PLC
      {
    					// dpDisconnect to _S7_Conn dp for error.
    					dpGet(sDpToChek+".configuration.S7_Conn", dpPlc);
    					res = dpDisconnect(c_unSystemIntegrity_S7PLC_DS_Error_callback, 
    																	dpPlc+".LastError", 
    																	dpPlc+".ConnState",
    																	dpPlc+S7_POLLING_COMMUNICATION+".LastError", 
    																	dpPlc+S7_POLLING_COMMUNICATION+".ConnState");
    					if(res<0)
    						bError = true;        
      }
     

					if(bError) 
						{
							fwException_raise(exceptionInfo, "ERROR", 
								"unSystemIntegrity_unicosS7PLC_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_DISCONNECT") +dp,"");
						}
					else 
						{
							// remove from list
							pos = _unSystemIntegrity_setList(g_unSystemIntegrity_S7PLC_DSList, dp, false);
							//DebugN("rmv", res);
							dynRemove(g_unSystemIntegrity_S7PLC_DSThreadId, pos);
							dynRemove(g_unSystemIntegrity_S7PLC_PlcList, pos);
							dynRemove(g_unSystemIntegrity_S7PLC_OldError, pos); 
							dynRemove(g_unSystemIntegrity_S7PLC_OldReduError, pos); 
							dynRemove(g_unSystemIntegrity_S7PLC_state, pos); 
							dynRemove(g_unSystemIntegrity_S7PLCIPSending_ThreadId, pos);
							dynRemove(g_unSystemIntegrity_S7PLCTimeSynchro_ThreadId, pos);
							// set the enable to false and de-activate the alarm and acknowledge it if necessary
							dpSet(dp+".enabled", false, dp+".alarm", c_unSystemIntegrity_no_alarm_value);
							unAlarmConfig_mask(dp+".alarm", true, exceptionInfo);
							dpSet(dpTime+".enabled", false, dpTime+".alarm", c_unSystemIntegrity_no_alarm_value);
							unAlarmConfig_mask(dpTime+".alarm", true, exceptionInfo);
       if(bReduPlc)
       {
  							dpSet(dpRedu1Error+".enabled", false);
  							unAlarmConfig_mask(dpRedu1Error+".alarm", true, exceptionInfo);
  							dpSet(dpRedu2Error+".enabled", false);
  							unAlarmConfig_mask(dpRedu2Error+".alarm", true, exceptionInfo);
       }
       else
       {
  							dpSet(dpError+".enabled", false, dpError+".alarm", c_unSystemIntegrity_no_alarm_value);
  							unAlarmConfig_mask(dpError+".alarm", true, exceptionInfo);         
       }       
							// get all the PLC-PLC dp, and disable them
							plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"_*", c_unSystemAlarm_dpType);
							len = dynlen(plc_plc_list);
							for(i=1;i<=len;i++) 
								{
									// do not reset the alarm value because this DPE should be connected to an address config
									dpSet(plc_plc_list[i]+".enabled", false);
									unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
								}
			
							// get all the FESystemAlarm dp, and disable them, check the Ok state and set the correct value
							plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"_*", c_unSystemAlarm_dpType);
							len = dynlen(plc_plc_list);
							for(i=1;i<=len;i++) 
								{
									// do not reset the alarm value because this DPE should be connected to an address config
									dpSet(plc_plc_list[i]+".enabled", false);
									unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
								}
						}
				}
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLC_checking deregister", sDp);
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLCCheck
/**
Purpose:
This function checks if the counter of the S7_PLC dp is modified periodically. If not it generates and alarm.

	@param sPlcDp: string, input, data point name

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLCCheck(string sPlcDp, int iDrvNum)
{
	int waitingTime, oldAlarmValue = -1;
	int oldValue = -1, newValue, alarmValue;
	bool bEnabled;
	string sS7Conn;
	int res, iPlcNumber;
 dyn_string exceptionInfo;
	
	// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLCCheck(sPlcDp= "+sPlcDp+" iDrvNum= "+iDrvNum);
	
	// get the value of the counter and the timeout PLCChecking
	dpGet(sPlcDp+".communication.counter", oldValue, sPlcDp+".configuration.S7_Conn", sS7Conn);

	while(true) 
		{
			// if 0 set it to c_unSystemIntegrity_defaultS7PLCCheckingDelay
			waitingTime = g_unSystemIntegrity_S7PLCCheckingDelay;
			if(waitingTime <= 0)
				waitingTime = c_unSystemIntegrity_defaultS7PLCCheckingDelay;
			delay(waitingTime);
	
			// get the value of the counter and the timeout PLCChecking
			dpGet(sPlcDp+".communication.counter", newValue);
			if(newValue == oldValue) 
				{
					// counter was not modified, set an alarm
					alarmValue = c_unSystemIntegrity_alarm_value_level1;
				}
			else	
				// counter was modified, reset the alarm
				alarmValue = c_unSystemIntegrity_no_alarm_value;

		if(oldAlarmValue != alarmValue) 
			{
				dpSet(c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcDp+".alarm", alarmValue);
				if(alarmValue != c_unSystemIntegrity_no_alarm_value) 
					{					
						// CASE OF S7 driver !!!!!!!!!!!!!!
						dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
						dpSet(sS7Conn + ".DoGeneralQuery:_original.._value", 1);
						// set the GQ bit so all data will bypass the smoothing
//DebugTN("--> SM, GQ", "_Driver" + iDrvNum + ".SM:_original.._value", sPlcDp, iDrvNum);
						// dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", 1);
                                            unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCCheck open GQ", sPlcDp, iDrvNum, sS7Conn);
					}
                                else
                                {
                                            if(dynContains(g_S7DriverConnection, iDrvNum) > 0)
                                              unSystemIntegrity_synchronizedTimeDateOfPlc_S7(sPlcDp, exceptionInfo);
                                            // wait c_unSystemIntegrity_S7PLCStartComDelayBefore_GQ = 500msec.
                                            delay(0, c_unSystemIntegrity_S7PLCStartComDelayBefore_GQ);
						// CASE OF S7 driver !!!!!!!!!!!!!!
						dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
						dpSet(sS7Conn + ".DoGeneralQuery:_original.._value", 1);
						// set the GQ bit so all data will bypass the smoothing
//DebugTN("--> SM, GQ", "_Driver" + iDrvNum + ".SM:_original.._value", sPlcDp, iDrvNum);
						// dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", 1);
                                            unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCCheck open GQ & request data", sPlcDp, iDrvNum, sS7Conn);
                                            unSystemIntegrity_sendCmdToPLC(sPlcDp, c_unSystemIntegrity_PLCGetAll_BS_AS_table, makeDynInt(0, 0, 0, 0, 0) , exceptionInfo);
                                }
			}
		
		res = _unSystemIntegrity_isInList(g_unSystemIntegrity_S7PLC_PlcList, sS7Conn);
		if(res > 0) {
			g_unSystemIntegrity_S7PLC_state[res] = alarmValue;
		}

		// DebugN("--> Check counter sPlcDp= "+sPlcDp+" oldValue= "+oldValue+" newValue= "+newValue+" alarmValue= "+alarmValue+ 
		//			 " c_unSystemAlarm_dpPattern= "+c_unSystemAlarm_dpPattern+" drv_pattern+iDrvNum= "+drv_pattern+iDrvNum);	
					 
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCCheck", sPlcDp, oldValue, newValue, oldAlarmValue, alarmValue);
		oldValue = newValue;
	  oldAlarmValue = alarmValue;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLCTimeSynchro
/**
Purpose:
This function synchronize the clock of the PLC.

	@param sPlcDp: string, input, data point name

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLCTimeSynchro(string sPlcDp, int iDrvNum)
{
	int waitingTime;
	dyn_string exceptionInfo;

	// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLCTimeSynchro(sPlcDp= "+sPlcDp+" iDrvNum="+iDrvNum);
		
	while(true) 
		{
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCTimeSynchro", sPlcDp);
			// synchronized only if the driver is started
			if(dynContains(g_S7DriverConnection, iDrvNum) > 0)
				unSystemIntegrity_synchronizedTimeDateOfPlc_S7(sPlcDp, exceptionInfo);
		
			waitingTime = g_unSystemIntegrity_S7PLCTimeSynchroDelay;
			if(waitingTime <= 0)
				waitingTime = c_unSystemIntegrity_defaultS7PLCTimeSynchroDelay;
			delay(waitingTime);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLCIPSending
/**
Purpose:
This function checks if the counter of the S7_PLC dp is modified periodically. If not it generates and alarm.

	@param sPlcDp: string, input, data point name

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLCIPSending(string sPlcDp, int iDrvNum)
{
	int waitingTime;
	int oldValue = -1, newValue;
	bool bFirstTime = true;
	string sS7Conn;
	int iPlcNumber;
	
	// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLCIPSending(sPlcDp="+sPlcDp+" iDrvNum= "+iDrvNum);
		
	// get the value of the counter and the timeout PLCChecking
	dpGet(sPlcDp+".configuration.S7_Conn", sS7Conn);
	
	while(true) 
		{
			// if 0 set it to c_unSystemIntegrity_defaultS7PLCCheckingDelay
			waitingTime = g_unSystemIntegrity_S7PLCIpSendingDelay;
			if(waitingTime <= 0)
				waitingTime = c_unSystemIntegrity_defaultS7PLCSendIpDelay;
			// wait
			delay(0, waitingTime);
		
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCIPSending", sPlcDp);
			newValue = g_IPValueToSendS7;	
			if(newValue == oldValue) 
				{
					newValue = 0;
				}
			if(g_unSystemIntegrity_S7reset[sPlcDp])
				bFirstTime = true;
//DebugN("--> sPlcDp", sPlcDp, g_S7DriverConnection, iDrvNum, dynContains(g_S7DriverConnection, iDrvNum), "bFirstTime = "+bFirstTime);
			if(dynContains(g_S7DriverConnection, iDrvNum) > 0) 
				{
					// synchronized only if the driver is started
					if(!bFirstTime)
                                        {
                                          synchronized(g_unSystemIntegrityS7PLCLock)
                                          {
						dpSet(sPlcDp+".communication.send_IP", newValue);
                                          }
                                        }
					else 
						{
							bFirstTime = false;
							g_unSystemIntegrity_S7reset[sPlcDp] = false;
							// Case S7
							dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
							dpSet(sS7Conn + ".DoGeneralQuery:_original.._value", 1);
//DebugN("--> FirstTime sPlcDp", sPlcDp, "reset _Driver" + iDrvNum + ".SM:_original.._value", sPlcDp, iDrvNum, g_unSystemIntegrity_S7reset);				
							// set the GQ bit so all data will bypass the smoothing
							// dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", 1);
						}
				}
			else
				bFirstTime = true;
			// DebugN("--> SendIP", sPlcDp, oldValue, newValue);	
			oldValue = newValue;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLCTimeCallback
/**
Purpose:
Callback function to check if the time is correctly set in the PLC.

	@param sDp1: 						string, input, data point name
	@param bStimeInvalid: 	bool, 	input, state of the stime bit of the counter
	@param sDp2: 						string, input, data point name
	@param iTime: 					int, 		input, the timestamp of the counter

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosS7PLCTimeCallback(string sDp1, bool bStimeInvalid, string sDpe2, int iTime)
{
	string sDp, sKey = dpSubStr(sDp1, DPSUB_SYS_DP);
	int value, oldValue = -1;
	int iCurrentTime;
	bool bHasKey;
		
	// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLCTimeCallback(sDp1= "+sDp1+" bStimeInvalid= "+bStimeInvalid+" sDpe2= "+sDpe2+" iTime="+iTime);

	if(mappingHasKey(g_unSystemIntegrity_S7_PLC_TimeAlarm, sKey)) {
		oldValue = g_unSystemIntegrity_S7_PLC_TimeAlarm[sKey];
		bHasKey = true;
	}
//DebugN("S7", g_unSystemIntegrity_S7_PLC_TimeAlarm, "UN", g_unSystemIntegrity_UnPlc_TimeAlarm);
	// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);
	// set the alarm 
	if(bStimeInvalid)  // bad time synchro
		value = c_unSystemIntegrity_alarm_value_level1;
	else // good time (invalid bit OK)
		{ 
			// check if too late in the past
			iCurrentTime = (int)getCurrentTime();
			if(iTime <= iCurrentTime) 
				{
					if((iCurrentTime - iTime) > CONST_CHANGE_DIFF)  			// too far in the past
						{
							//DebugN("alarm too late");
							value = c_unSystemIntegrity_alarm_value_level1;
						}
					else
						value = c_unSystemIntegrity_no_alarm_value;
				}
			else
				value = c_unSystemIntegrity_no_alarm_value;
		}
	if(oldValue != value)
		dpSet( c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+sDp+".alarm", value);
		
	oldValue = value;

	if(bHasKey)
		g_unSystemIntegrity_S7_PLC_TimeAlarm[sKey] = value;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCTimeCallback", sKey, value);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLCErrorCallback
/**
Purpose:
Callback function to check if there is no error in the _S7_Conn data point (both TSPP and polling connections!!!!)

	@param sDp1: 				string, 	input, data point name
	@param error: 			unsigned, input, error set by the S7 driver (connection TSPP)
	@param sDp2: 				string, 	input, data point name
	@param state: 			unsigned, input, communication state of the S7 PLC (connection TSPP)
	@param sDp1_poll: 	string, 	input, data point name
	@param error_poll: 	unsigned, input, error set by the S7 driver (connection Polling)
	@param sDp2_poll: 	string, 	input, data point name
	@param state_poll: 	unsigned, input, communication state of the PLC (connection Polling)

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
																	
unSystemIntegrity_unicosS7PLCErrorCallback(string sDp1, unsigned error, string sDp2, unsigned state,string sDp1_poll, unsigned error_poll, string sDp2_poll, unsigned state_poll)
{
	string sDp, sAlPlc;
	int value, res;

// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLCErrorCallback(sDp1= "+sDp1+" error= "+error+" sDp2= "+sDp2+" state= "+state+" sDp1_poll= "+sDp1_poll+" error_poll= "+error_poll+" sDp2_poll= "+sDp2_poll+" state_poll= "+state_poll); 
	
	// There is a new additional list with the Dp for the Polling connections: g_unSystemIntegrity_S7PLC_PlcListPolling
	// First Approach: both TSPP and polling are grouped in only one SystemAlarm type.
	
	// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);
	value = c_unSystemIntegrity_no_alarm_value;
	
	if (error_poll!=0) 		// Polling  
		value = (int)fabs(error_poll);		// In case of disconnection the error shows the value 15
			
	if	((state == 1) || (state == 2)) // connected or GQ
		{		// TSPP connnected 
		if (error!=0) 		
			value = (int)fabs(error);		// error is converted to a positive value if ever negative (i.e.: -720892)
		}
	else						 // TSPP not connected
		value = c_unSystemIntegrity_alarm_value_level1;					// default 10
				
	// PLC name (connection)
	res = _unSystemIntegrity_isInList(g_unSystemIntegrity_S7PLC_PlcList, sDp);
	
	if(res > 0) 
		{
  			sAlPlc = g_unSystemIntegrity_S7PLC_DSList[res];				
  			// _unSystemAlarm_DS_Comm_
  			strreplace(sAlPlc, c_unSystemAlarm_dpPattern+PLC_DS_pattern, c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern);
  			// DebugN("----> case TSPP:  res="+res+" sAlPlc= "+sAlPlc);
    if(g_unSystemIntegrity_S7PLC_OldError[res] != value)
    {
      			  dpSet( sAlPlc+".alarm", value);
      g_unSystemIntegrity_S7PLC_OldError[res] = value;
    }
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCErrorCallback", "----> case TSPP:  res="+res+" sAlPlc= "+sAlPlc, value);
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLCErrorCallback
/**
Purpose:
Callback function to check if there is no error in the _S7_Conn data point (both TSPP and polling connections!!!!)

	@param sDp1: 				string, 	input, data point name
	@param error: 			unsigned, input, error set by the S7 driver (connection TSPP)
	@param sDp2: 				string, 	input, data point name
	@param state: 			unsigned, input, communication state of the S7 PLC (connection TSPP)
	@param sDp1_poll: 	string, 	input, data point name
	@param error_poll: 	unsigned, input, error set by the S7 driver (connection Polling)
	@param sDp2_poll: 	string, 	input, data point name
	@param state_poll: 	unsigned, input, communication state of the PLC (connection Polling)

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
																	
unSystemIntegrity_unicosS7PLCRedu1ErrorCallback(string sDp1, long error, string sDp2, unsigned state,string sDp1_poll, long error_poll, string sDp2_poll, unsigned state_poll)
{
	string sDp, sAlPlc;
	int value, res;

// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLCRedu1ErrorCallback(sDp1= "+sDp1+" error= "+error+" sDp2= "+sDp2+" state= "+state+" sDp1_poll= "+sDp1_poll+" error_poll= "+error_poll+" sDp2_poll= "+sDp2_poll+" state_poll= "+state_poll); 
	
	// There is a new additional list with the Dp for the Polling connections: g_unSystemIntegrity_S7PLC_PlcListPolling
	// First Approach: only polling are grouped in only one SystemAlarm type.
	
	// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);
	value = c_unSystemIntegrity_no_alarm_value;
	
	if (error_poll!=0) 		// Polling  
		value = (int)fabs(error_poll);		// In case of disconnection the error shows the value 15
	else if (state_poll < 1)
  value = c_unSystemIntegrity_alarm_value_level1;					// default 10 	

	// PLC name (connection)
	res = _unSystemIntegrity_isInList(g_unSystemIntegrity_S7PLC_PlcList, sDp);
	
	if(res > 0) 
		{
			sAlPlc = g_unSystemIntegrity_S7PLC_DSList[res];				
			// _unSystemAlarm_DS_Comm_
			strreplace(sAlPlc, c_unSystemAlarm_dpPattern+PLC_DS_pattern, c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern);
			// DebugN("----> case TSPP:  res="+res+" sAlPlc= "+sAlPlc);
    if(g_unSystemIntegrity_S7PLC_OldError[res] != value)
    {
  			  dpSet( sAlPlc+".alarm", value);
      g_unSystemIntegrity_S7PLC_OldError[res] = value;
    }
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCRedu1ErrorCallback", "----> case POLL:  res="+res+" sAlPlc= "+sAlPlc, value);
		}
}


//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosS7PLCErrorCallback
/**
Purpose:
Callback function to check if there is no error in the _S7_Conn data point (both TSPP and polling connections!!!!)

	@param sDp1: 				string, 	input, data point name
	@param error: 			unsigned, input, error set by the S7 driver (connection TSPP)
	@param sDp2: 				string, 	input, data point name
	@param state: 			unsigned, input, communication state of the S7 PLC (connection TSPP)
	@param sDp1_poll: 	string, 	input, data point name
	@param error_poll: 	unsigned, input, error set by the S7 driver (connection Polling)
	@param sDp2_poll: 	string, 	input, data point name
	@param state_poll: 	unsigned, input, communication state of the PLC (connection Polling)

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
																	
unSystemIntegrity_unicosS7PLCRedu2ErrorCallback(string sDp1, long error, string sDp2, unsigned state,string sDp1_poll, long error_poll, string sDp2_poll, unsigned state_poll)
{
	string sDp, sAlPlc;
	int value, res;

// DebugN("FUNCTION: unSystemIntegrity_unicosS7PLCRedu2ErrorCallback(sDp1= "+sDp1+" error= "+error+" sDp2= "+sDp2+" state= "+state+" sDp1_poll= "+sDp1_poll+" error_poll= "+error_poll+" sDp2_poll= "+sDp2_poll+" state_poll= "+state_poll); 
	
	// There is a new additional list with the Dp for the Polling connections: g_unSystemIntegrity_S7PLC_PlcListPolling
	// First Approach: only polling are grouped in only one SystemAlarm type.
	
	// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);
	value = c_unSystemIntegrity_no_alarm_value;
	
	if (error_poll!=0) 		// Polling  
		value = (int)fabs(error_poll);		// In case of disconnection the error shows the value 15
	else if (state_poll < 1)
  value = c_unSystemIntegrity_alarm_value_level1;					// default 10   

	// PLC name (connection)
	res = _unSystemIntegrity_isInList(g_unSystemIntegrity_S7PLC_PlcList, sDp);
// DebugN("unSystemIntegrity_unicosS7PLCRedu2ErrorCallback 1:  res=",res,"g_unSystemIntegrity_S7PLC_PlcList",g_unSystemIntegrity_S7PLC_PlcList,
//        "g_unSystemIntegrity_S7PLC_PlcListPolling",g_unSystemIntegrity_S7PLC_PlcListPolling,"sDp",sDp);
	
	if(res > 0) 
		{
			sAlPlc = g_unSystemIntegrity_S7PLC_DSList[res];				
			// _unSystemAlarm_DS_Comm_
			strreplace(sAlPlc, c_unSystemAlarm_dpPattern+PLC_DS_pattern, c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern);
// DebugN("unSystemIntegrity_unicosS7PLCRedu2ErrorCallback 2:  res=",res," sAlPlc= ",sAlPlc,"value",value, 
//        "g_unSystemIntegrity_S7PLC_OldReduError",g_unSystemIntegrity_S7PLC_OldReduError);
    if(g_unSystemIntegrity_S7PLC_OldReduError[res] != value)
    {
  			  dpSet( sAlPlc+".alarm", value);
      g_unSystemIntegrity_S7PLC_OldReduError[res] = value;
    }
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_unicosS7PLCRedu2ErrorCallback", "----> case POLL:  res="+res+" sAlPlc= "+sAlPlc, value);
		}
}


//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_sendCmdToPLC_S7
/**
Purpose:
send a command to PLC.

	@param sPlcDp: string, input, PLC data point name of type _UnPlc
	@param iFunction: int, input, function 
	@param diParam: dyn_int, input, parameter list
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/
unSystemIntegrity_sendCmdToPLC_S7(string sPlcDp, int iFunction, dyn_int diParam, dyn_string &exceptionInfo)
{
	int len;
	string dp=dpSubStr(sPlcDp, DPSUB_SYS_DP);
	
	// DebugN("FUNCTION: unSystemIntegrity_sendCmdToPLC_S7(sPlcDp="+sPlcDp+" iFunction="+iFunction+" diParam="+diParam);
	
	len = dynlen(diParam);
	
	if(len != c_unSystemIntegrity_functionParameterLenthS7) {
		fwException_raise(exceptionInfo, "ERROR", 
						"unSystemIntegrity_sendCmdToPLC():"+getCatStr("unSystemIntegrity", "WRONG_PARAM_LENGTH"),"");
		return;
	}
	if(dpExists(dp)) {
          synchronized(g_unSystemIntegrityS7PLCLock)
          {
		dpSet(dp+".communication.commandInterface.function", iFunction, 
										dp+".communication.commandInterface.param0", diParam[1], 
										dp+".communication.commandInterface.param1", diParam[2], 
										dp+".communication.commandInterface.param2", diParam[3], 
										dp+".communication.commandInterface.param3", diParam[4], 
										dp+".communication.commandInterface.param4", diParam[5]);
          }
	}
	else {
		fwException_raise(exceptionInfo, "ERROR", 
						"unSystemIntegrity_sendCmdToPLC():"+getCatStr("unSystemIntegrity", "PLCDP_DOES_NOT_EXIST") + sPlcDp,"");
	}
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_synchronizedTimeDateOfPlc_S7
/**
Purpose:
Synchronized the PLC clock.

	@param sPlcDp: 				string, 		input, 	PLC data point name of type S7_PLC
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP
	. distributed system: yes.
*/
unSystemIntegrity_synchronizedTimeDateOfPlc_S7(string sPlcDp, dyn_string &exceptionInfo)
{
time tLocal;
int iMS,iS, iH, iM, iD, iMM, iY, iRes, iWeekDay;
dyn_int param;
long qUTC;
string temp;
int tempCalc;

	tLocal = getCurrentTime();
	qUTC = (int)tLocal;
	iRes=ctrlTimeUtils_parseUtcTime(qUTC, iY, iMM, iD, iWeekDay, iH, iM, iS);	
	iMS=milliSecond(tLocal);
	
	if(iWeekDay > 7) // PVSS starts from Monday but PLC from Sunday TO VERIFY in SIEMENS ?
		iWeekDay = 1;

	tempCalc=iY-2000;	// two digits: i.e.: 05 for 2005
	sprintf(temp,"%2d%2d",tempCalc,iMM);
	strreplace(temp, " ", "0");
	sscanf(temp, "%x", param[1]);
	
	sprintf(temp, "%2d%2d", iD, iH);
	strreplace(temp, " ", "0");
	sscanf(temp, "%x", param[2]);
	
	sprintf(temp, "%2d%2d", iM, iS);
	strreplace(temp, " ", "0");
	sscanf(temp, "%x", param[3]);
	
	sprintf(temp,"%3d%1d",iMS,iWeekDay);
	strreplace(temp, " ", "0");
	sscanf(temp, "%x", param[4]);
	
	param[5]=0;
	
	// DebugN(" --> iMS="+iMS+" iS= "+iS+" iM="+iM+" iH="+iH+" iD="+iD+" iMM="+iMM+" iY="+iY);	
	unSystemIntegrity_sendCmdToPLC_S7(sPlcDp, c_unSystemIntegrity_S7PLCSynchonizedTimeDate, param, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_S7PLC_S7DriverGQThread
/**
Purpose:
Periodically open GQ.


Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP
	. distributed system: yes.
*/
unSystemIntegrity_S7PLC_S7DriverGQThread()
{
  int i, len;
  int iDrvNum;
  string sS7Conn;
  int iWaitingDelay, iNb;
  
  while(true)
  {
    iWaitingDelay = g_unSystemIntegrity_S7PLCCheckingDelay;
    if(iWaitingDelay <= 0)
      iWaitingDelay = c_unSystemIntegrity_defaultS7PLCCheckingDelay;
    delay(iWaitingDelay);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_S7PLC_S7DriverGQThread", iNb, g_unSystemIntegrity_S7PLCGQThreadNbCheck);
    if((g_unSystemIntegrity_S7PLCGQThreadNbCheck>0) && (iNb >= g_unSystemIntegrity_S7PLCGQThreadNbCheck))
    {
      iNb = 0;
      synchronized(g_m_unSystemIntegrity_S7PLC_S7Started)
      {
        len=mappinglen(g_m_unSystemIntegrity_S7PLC_S7Started);
        for(i=1;i<=len;i++)
        {
          sS7Conn = mappingGetKey(g_m_unSystemIntegrity_S7PLC_S7Started, i);
          iDrvNum = mappingGetValue(g_m_unSystemIntegrity_S7PLC_S7Started, i);
          dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
          dpSet(sS7Conn + ".DoGeneralQuery:_original.._value", 1);
          unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7PLC systemIntegrity", "unSystemIntegrity_S7PLC_S7DriverGQThread on PLC", sS7Conn, iDrvNum);
        }
      }
    }
    if(g_unSystemIntegrity_S7PLCGQThreadNbCheck>0)
      iNb++;
    else
      iNb = 0;
  }
}


//@}
