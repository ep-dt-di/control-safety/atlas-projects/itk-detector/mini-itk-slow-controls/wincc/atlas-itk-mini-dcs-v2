/**@name LIBRARY: unTextLabel.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 14/01/2005

Modification History: 
	16/11/2011: Alexey
		[IS-668] added support of unCPC6
		
	20/02/2007: Herve
		- color
		
	12/02/2007: Frederic
		- TexLabel can be used with the WordStatus devices
		
	15/04/2005: Herve
		- unGenericObject_GetPLCNameFromDpName: return the PLCtype+"_"+plcName, in case of _UnPlc it returns _unPlc_+plcName
		
version 1.0
		
Purpose: This library contains functions for the TextLabel widget.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. UNICOS (DPType etc.)
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/

//@{

//---------------------------------------------------------------------------------------------------------------------------------------

// unTextLabel_initialize
/**
Purpose: Function used by the unTextLabel widget during initialization

Parameters:
	- identifier, string, input, device name
	- textlabel, string, input, Textlabel device name

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in TextLabel widget
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/

unTextLabel_initialize(string identifier, string textlabel)
{
	string sTextLabel;
	string deviceSystemName;
	dyn_string exceptionInfo, dsText, dsSplit;
	bool bConnected, bInitOk;
	int i, len, iKey;

	deviceSystemName = unGenericDpFunctions_getSystemName(identifier);
// remove the system name and take the system of the identifier by default
	sTextLabel = deviceSystemName+dpSubStr(textlabel, DPSUB_DP)+".textList";
//DebugN(textlabel, sTextLabel);

// if TextLabel exists
	if(dpExists(sTextLabel)) {
//DebugN(sTextLabel);
// get the textlabel
		dpGet(sTextLabel, dsText);
		len = dynlen(dsText);
		for(i=1;i<=len;i++) {
			dsSplit = strsplit(dsText[i], UN_PARAMETER_DELIMITER);
			if(dynlen(dsSplit) >1) {
				iKey = dsSplit[1];
				g_mText[iKey] = dsSplit[2];
				if(dynlen(dsSplit) > 2)
					g_mColor[iKey] = dsSplit[3];
				else
					g_mColor[iKey] = "unSynoptic_staticText";
			}
		}
		
// get systemName of device
		if (strltrim(identifier) != "")
		{
// register to systemName
			unDistributedControl_register ("unTextLabel_Register", bInitOk, bConnected, deviceSystemName, exceptionInfo);
		}
		else
			bInitOk = false;
		
		if(!bInitOk) {
			setMultiValue("", "text", "WRONG SYSTEM NAME", "", "color", "unDataNoAccess", "WarningText", "visible", false);
		}
	}
	else 
		setMultiValue("", "text", "NO TEXT LABEL", "", "color", "unDataNoAccess", "WarningText", "visible", false);
}

// unTextLabel_Register
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose: register callback function

Parameters:
	- sDp, string, input, dp
	- bSystemConnected, bool, input, state of the remote system

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in TextLabel widget
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unTextLabel_Register(string sDp, bool bSystemConnected)
{
string deviceName, sType, sDpe;
int iAction, iRes;
dyn_string exceptionInfo;
string sPlcName, sSystemName;
bool bRemote;
		
	deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
	if (deviceName == "")	// In case of disconnection
		{
		deviceName = g_sDpName;
		}
// get the systemName
	sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
	sPlcName = unGenericObject_GetPLCNameFromDpName(deviceName);
//get the DpType
	sType=dpTypeName(deviceName);

	if (sType=="AnalogInput" || sType=="AnalogOutput")
		sDpe=deviceName + ".ProcessInput.PosSt";
	else if (sType=="WordStatus")
		sDpe=deviceName + ".ProcessInput.AuPosRSt";
	else if (sType=="CPC_AnalogInput" || sType=="CPC_AnalogOutput" || sType=="CPC_WordStatus")
		sDpe=deviceName + ".ProcessInput.PosSt";
	else
		sDpe="";
	
	unDistributedControl_isRemote(bRemote, sSystemName);
	if(bRemote)
		g_bSystemConnected = bSystemConnected;
	else
		g_bSystemConnected = true;

	if(sPlcName != "")
		{
		if (dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName))
			g_bUnSystemAlarmPlc = true;
		else
			g_bUnSystemAlarmPlc = false;
		}
	else
		g_bUnSystemAlarmPlc = false;

	unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
	switch (iAction)
		{
		case UN_ACTION_DISCONNECT:
			setMultiValue("", "text", "????????????", "", "color", "unDataNoAccess", "WarningText", "visible", false);
			break;
		case UN_ACTION_DPCONNECT:
			g_sDpName = deviceName;				
			//  check if the device name contains .ProcessInput.PosSt if not like no PLC
			if(!dpExists(sDpe))
				g_bUnSystemAlarmPlc = false;
				
			if(g_bUnSystemAlarmPlc)
				{
				iRes = dpConnect(	"unTextLabel_WidgetCB", 
													sDpe,
													sDpe + ":_online.._invalid",
													sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
													sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");				
				}
			else 
				setMultiValue("", "text", "????????????", "", "color", "unDataNoAccess", "WarningText", "visible", false);

			g_bCallbackConnected = (iRes >= 0);
			break;
		case UN_ACTION_DPDISCONNECT_DISCONNECT:
			if(g_bUnSystemAlarmPlc) {
				iRes = dpDisconnect("unTextLabel_WidgetCB", 
														sDpe,
														sDpe + ":_online.._invalid",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".alarm",
														sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcName+".enabled");				
			}
			g_bCallbackConnected = !(iRes >= 0);
			setMultiValue("", "text", "????????????", "", "color", "unDataNoAccess", "WarningText", "visible", false);
			break;
		case UN_ACTION_NOTHING:
		default:
			break;
		}
}

// unTextLabel_WidgetCB
/**
Purpose: Widget callback animation function

Parameters:
	- sDp, string, input, dp
	- iPosSt, int, input, postst value
	- sDpInv, string, input, dp for invalid data
	- bInvalid, bool, input, invalid state
	- sDpAl, string, input, PLC alarm dp
	- iValAlarm, int, input, PLC alarm value
	- sDpEnabled, string, input, PLC enable dp
	- bPlcEnabled, int, input, PLC enable value

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in TextLabel widget
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unTextLabel_WidgetCB(string sDp, int iPosSt, string sDpInv, bool bInvalid,
							string sDpAl, int iValAlarm, string sDpEnabled, bool bPlcEnabled)
{
string sColor, warningColor;
string sText, warningLetter;
bool bVisible=false;
	
	if(mappingHasKey(g_mText, iPosSt))
		{
		sColor = g_mColor[iPosSt];
		sText = g_mText[iPosSt];

		if(bInvalid)
			{
			sColor = "unDataNotValid";
			warningLetter = UN_WIDGET_TEXT_INVALID;
			warningColor = "unDataNotValid";
			bVisible = true;
			}
		else
			{
	
	// if the device is not correclty running (the iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value) 
	// or the checking is not enabled then set the letter to O and the color to not valid data
	
			if ((!bPlcEnabled) || (iValAlarm > c_unSystemIntegrity_no_alarm_value))
			{
				warningLetter = UN_WIDGET_TEXT_OLD_DATA;
				warningColor = "unDataNotValid";
				bVisible = true;
			}
		}
	}
	else
		{
		sColor = "unDataNoAccess";
		sText = iPosSt+": UNKNOWN key";
		}
	
	if(g_bSystemConnected)
	setMultiValue("", "text", sText, "", "color", sColor, 
						"WarningText", "text", warningLetter, "WarningText", "color", warningColor, "WarningText", "visible", bVisible);
}

//@}

