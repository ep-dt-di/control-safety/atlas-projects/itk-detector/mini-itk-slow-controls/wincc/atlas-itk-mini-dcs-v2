/**@name LIBRARY: unTree.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 09/10/2002

Modification History: 
  01/06/2012: Cyril
  - IS-684 Added multi application filter management when displaying window or trend tree.
  
  18/10/2011: Herve
  - IS-623  unCore - unTreeWidget, unCore - unWindowTree  WindowTree and TrendTree configuration: remove/cut/rename a node and then adding a new one provoke and error, showWindowTree does not show the top node 
 
  11/05/2009: Herve
    - bug fix: removeSymbol in PVSS 3.8
    new function: unTree_removeUserPanel
    modified function: unTree_OpenPanel
    
  30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

  18/02/2008: Herve
    - remove open tree configuration on rigth click
    
  02/04/2007: Herve
    new function unTree_EventNodeRigthClickOperation

  14/02/2005: Herve:
    bug: unTrendTreeConfiguration, unWindowTreeConfiguration: if both opened: triger in both config panel.
      - in function unTree_initializeInternalCallback, unTree_executeEventRigthClick, unTree_executeEventNodeRigthClick
      unTree_triggerEventRigthClick, unTree_triggerEventNodeRigthClick
      the dpname is UN_TREE_DPNAME_PREFIX+UN_TREE_DPNAME_SEPARATOR+myManNum()+UN_TREE_DPNAME_SEPARATOR+myModuleName()+UN_TREE_DPNAME_SEPARATOR+sTopDisplayNodeName
      where the sTopDisplayNodeName is read from unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayNodeName);
      the name is given by the function: unTree_getInternalTriggerName
  
  07/12/2004: Herve
    - Remove, cut and rename are considered with the same boolean: all allowed or not.
      call to function unTree_setHierarchyButtonGlobalVariable and unTree_setHierarchyButtonState
    - the link allowed is returned by the function UN_TREE_GETDEVICESTATE
  
  02/12/2004: Herve
    - add the new menu choice
    
  15/11/2004: Herve
    - new parameter for the function UN_TREE_OPENPANELGETDOLLARPARAM: bDetail
    
  24/09/2004: Herve
    - add device definition/device model

  27/08/2004: Herve
    - wrong function name: unProgessBar_start renamed to unProgressBar_start

  23/07/2004: Herve update 3.0
    - unTreeIntialize: signature and function modified
    - unTree_EventClickConfiguration renamed to unTree_EventLeftClickConfiguration
    
  23/02/2004: Herve
    add function unTree_getAllHierarchyDpType, unTree_EventClickHierarchyDisplayConfiguration
    
  01/09/2003: Herve 
    add the unTree_addRemoveNodeToTree function

version 1.1

External Functions: 

Internal Functions: 

Purpose: 
This library contain functions to be used with the UNICOS Tree utility.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _UnTree, JCOP trending DPTypes
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/

#uses "unProgressBar.ctl"


//------------
// constant definition
//------------

const string UN_TREE_DPTYPE = "_UnTree";
const string UN_TREE_DPNAME_PREFIX = "_unTree";
const string UN_TREE_DPNAME_SEPARATOR = "_";
const string UN_TREE_DEVICE_SEPARATOR = " -> ";

const string NO_DEVICE = "";

const int UN_TREE_newFolder = 20;
const int UN_TREE_addDevice = 21;
const int UN_TREE_addDevices = 29;
const int UN_TREE_horizontalNavigation = 22;
const int UN_TREE_linkToDevice = 23;
const int UN_TREE_unLinkToDevice = 24;
const int UN_TREE_deviceConfiguration = 25;
const int UN_TREE_openTreeConfiguration = 26;
const int UN_TREE_newDevice = 27;
const int UN_TREE_new = 28;
const int UN_TREE_changeLinkedDevice = 30;

const int UN_TREE_TREENODENAME = 1;
const int UN_TREE_LOADTREEIMAGE =2;
const int UN_TREE_EVENTRIGTHCLICKMENU = 3;
const int UN_TREE_PRE_EVENTRIGTHCLICK = 4;
const int UN_TREE_HANDLEEVENTRIGTHCLICK = 5;
const int UN_TREE_POST_EVENTRIGTHCLICK = 6;
const int UN_TREE_EVENTNODERIGTHCLICKMENU = 7;
const int UN_TREE_PRE_EVENTNODERIGTHCLICK = 8;
const int UN_TREE_HANDLEEVENTNODERIGTHCLICK = 9;
const int UN_TREE_POST_EVENTNODERIGTHCLICK = 10;
const int UN_TREE_GETHIERARCHYSTATE = 11;
const int UN_TREE_GETDEVICESTATE = 12;
const int UN_TREE_GETDISPLAYNODENAME = 13;
const int UN_TREE_NODELIST_LOADIMAGE = 14;
const int UN_TREE_NODELIST_EVENTRIGTHCLICKMENU = 15;
const int UN_TREE_NODELIST_EVENTNODERIGTHCLICKMENU = 16;
const int UN_TREE_NODELIST_EVENTDBLCLICKPRETEXECUTE = 17;
const int UN_TREE_NODELIST_EVENTDBLCLICKPOSTEXECUTE = 18;
const int UN_TREE_OPENPANELPREEXECUTE = 19;
const int UN_TREE_CONFIGURATION_PANEL = 20;
const int UN_TREE_HANDLEEVENTNODECLICKOPERATION = 21;
const int UN_TREE_OPENPANELGETDOLLARPARAM = 22;
const int UN_TREE_EVENTNODERIGTHCLICKOPERATIONMENU = 23;
const int UN_TREE_HANDLEEVENTNODERIGTHCLICKOPERATION = 24;


//--------------------------------------------------------------------------------------------------------------------------------

//@{

// unTree_EventInitializeConfiguration
/**
Purpose:
Initialize the tree configuration
Function called during the initialisation of the unTreeWidget. This function:
  - triggers the load image function for the defined tree.
  - triggers the loadTree function.
  - triggers the EventClick function.

  @param sGraphTree: string, input:  the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_EventInitializeConfiguration(string sGraphTree, string sBar, bool bClipboard)
{
  dyn_string exceptionInfo;
  shape aShape;
  int id;

  string imageClassParameters;
  string sDpName;
  string sDpNameKey;
  string sParentDpName;
  string sParentDpNameKey;
  string sDeviceName;
  string sDeviceType;
  string sCompositeLabel;
  string sCurrentSelectedNodeKey;
  
  int iCurrentPointer, iMousePointer;
  string sFunctionName, sTopDisplayNodeName;

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayNodeName);
  
  //beginning working on IS-1469
//   dyn_string dsNodes;
//   fwTree_getRootNodes(dsNodes,exceptionInfo);
//   DebugN("unTree_EventInitializeConfiguration","dsNodes",dsNodes);
  
  
  
  // start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);
  
  // disable the unTreeWidget tree to have one action at a time.
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentPointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);

  // disable the cascade buttons  
  hierarchyCascade.enabled(false);
  deviceCascade.enabled(false);
  
  // allow the renaming of top node in the unTreeWidget 
  unTreeWidget_setPropertyValue(aShape, "set_bRenameRootNodeAllowed", true, true);

  unTree_getConfigurationVariable(sTopDisplayNodeName, UN_TREE_LOADTREEIMAGE, sFunctionName);
  if(isFunctionDefined(sFunctionName)) 
  {
    evalScript(
        exceptionInfo, 
        "dyn_string main(string sDeviceIdentifier, string sGraphTree)" +
        "{" +
        "    dyn_string exceptionInfo;" +
        "    " + sFunctionName + "(sDeviceIdentifier, sGraphTree, exceptionInfo);" +
        "    return exceptionInfo;" +
        "}", 
        makeDynString(), 
        sTopDisplayNodeName, 
        sGraphTree
      );
  }
    
  unTree_loadTree(sGraphTree, sBar, bClipboard);
  
  unTree_getGlobalValue(
      sDpName, 
      sDpNameKey, 
      sParentDpName, 
      sParentDpNameKey,
      sDeviceName, 
      sDeviceType, 
      sCompositeLabel, 
      sCurrentSelectedNodeKey
    );
  
  if(sDpName != "") 
  {
    // show the WindowTree content
    unTree_EventClick(sGraphTree, sBar, true, 0, 0);
  }
  
  // handle any errors here
  if(dynlen(exceptionInfo)>0) 
  {
    fwExceptionHandling_display(exceptionInfo);
  }

  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentPointer, true);
  
  // enable the unTreeWidget tree.
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);

  // stop the progress bar.
  unProgressBar_stop(id, sBar);

  // enable the cascade buttons  
  hierarchyCascade.enabled(true);
  deviceCascade.enabled(true);

}

// unTree_EventInitializeOperation
/**
Purpose:
Initialize the tree operation
Function called during the initialisation of the unTreeWidget. This function:
  - triggers the load image function for the defined tree.
  - triggers the loadTree function.
  - triggers the EventClick function.

  @param sGraphTree: string, input:  the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTree_EventInitializeOperation(string sGraphTree, string sBar, bool bClipboard)
{
  dyn_string dsExceptions;
	
  shape aShape;
	
  int id;
  int iCurrentPointer;
	int iMousePointer;
	
  string sFunctionName;
	string sTopDisplayNodeName;
	string sNodeDpName;
  string sDpName;

	
  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayNodeName);
  
	// start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);
	
	// disable the unTreeWidget tree to have one action at a time.
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentPointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);

	// allow the renaming of top node in the unTreeWidget 
  unTreeWidget_setPropertyValue(aShape, "set_bRenameRootNodeAllowed", false, true);

  unTree_getConfigurationVariable(sTopDisplayNodeName, UN_TREE_LOADTREEIMAGE, sFunctionName);

  if(isFunctionDefined(sFunctionName)) 
	{
    evalScript(
        dsExceptions, 
        "dyn_string main(string sDeviceIdentifier, string sGraphTree)" +
        "{"+
        "    dyn_string dsExceptions;"+
        "    " + sFunctionName + "(sDeviceIdentifier, sGraphTree, dsExceptions);"+
        "    return dsExceptions;"+
        "}", 
        makeDynString(), 
        sTopDisplayNodeName, 
        sGraphTree
      );
  }	

  sNodeDpName = unTreeWidget_treeNodeDpPrefix+sTopDisplayNodeName;
  
  if(dpExists(sNodeDpName)) 
	{
    unTree_showNodeContent(sGraphTree, sTopDisplayNodeName, sNodeDpName, false, bClipboard, dsExceptions);
	}
	
	// handle any errors here
  if(dynlen(dsExceptions)>0) 
	{
    fwExceptionHandling_display(dsExceptions);
  }

  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentPointer, true);
	
	// enable the unTreeWidget tree.
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
	
	// stop the progress bar.
  unProgressBar_stop(id, sBar);

}

// unTree_loadTree
/**
Purpose:
This function loads the node into the unTreeWidget
Function called during the initialisation of the unTreeWidget. 

  @param sGraphTree: string, input:  the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTree_loadTree(string sGraphTree, string sBar, bool bClipboard)
{
  dyn_string exceptionInfo, dsChildren;
  string sKey, sMenuText="--- No Tree ---";
  int i, len;
  shape aShape;
  string sDisplayNodeName;
  string sNodeDpName;
  dyn_bool dbShow;
  bool bGranted = false, bReorder = false;
  string sFunctionName;
  dyn_string dsReturnData;
  bool bAdd=false, bAddFolder=false, bAddDevice=false;
  
  aShape = getShape(sGraphTree);

  unTree_isUserAllowed(bGranted, exceptionInfo);

  unTree_setUserPanelOpened(false);
  
  unTree_setDeviceButtonGlobalVariable(
      false, 
      false, 
      false, 
      false
    );
  
  unTree_setDeviceButtonState(
      false, 
      false, 
      false, 
      false
    );
  
  unTree_setHierarchyButtonGlobalVariable(
      false, 
      false, 
      false, 
      false, 
      false, 
      false,
      false, 
      false, 
      false
    );
  
  unTree_setHierarchyButtonState(
      false, 
      false,
      false,
      false,
      false,
      false,
      false, 
      false, 
      false
    );

  unTree_setHierarchyButtonText("", "");

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sDisplayNodeName);
  sNodeDpName = unTreeWidget_treeNodeDpPrefix + sDisplayNodeName;
    
  
  if(dpExists(sNodeDpName)) 
  {
    unTree_showNodeContent(sGraphTree, sDisplayNodeName, sNodeDpName, false, bClipboard, exceptionInfo);

    sKey = unTreeWidget_compositeLabelSeparator+sDisplayNodeName+
            unTreeWidget_labelSeparator+sNodeDpName;
    unTree_setGlobalValue(sNodeDpName, sKey, "", "", "", "", "", "");

    fwTree_getChildren(sDisplayNodeName, dsChildren, exceptionInfo);
    if(dynlen(dsChildren) > 0) 
      bReorder = true;

    unTree_getConfigurationVariable(sDisplayNodeName, UN_TREE_GETHIERARCHYSTATE, sFunctionName);
    
    if(isFunctionDefined(sFunctionName)) {
      evalScript(dsReturnData, "dyn_string main(string sDisplayNodeName, string sDpName)"+
                    "{"+
                    "    dyn_string dsReturnData;"+
                    "    " + sFunctionName + "(dsReturnData, sDisplayNodeName, sDpName);"+
                    "    return dsReturnData;"+
                    "}", makeDynString(), sDisplayNodeName, sNodeDpName);
      if(dynlen(dsReturnData) >= 3) 
      {
        if((dsReturnData[1] == "TRUE") || (dsReturnData[1] == "true"))
        {
          bAdd = true;        
        }
        if((dsReturnData[2] == "TRUE") || (dsReturnData[2] == "true"))
        {
          bAddFolder = true;
        }
        if((dsReturnData[3] == "TRUE") || (dsReturnData[3] == "true"))
        {
          bAddDevice = true;
        }
      }
    }

    unTree_setHierarchyButtonGlobalVariable(bAdd, bAddFolder, bAddDevice, false, false, false,
                                        false, false, bReorder);
    unTree_setHierarchyButtonState(bAdd&bGranted, bAddFolder&bGranted, bAddDevice&bGranted, false, false, false,
                                        false, false, bGranted&bReorder);
    sMenuText = "--- "+sDisplayNodeName+" ---";
  }
  else
  {
    unTree_setGlobalValue("", "", "", "", "", "", "", "");
  }

  unTree_setPasteGlobalValue("", "");
  unTree_setButtonFirstName(sMenuText);
}

// unTree_initializeInternalCallback
/**
Purpose:
Initialze the internal Dp, global variable and callback

  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTree_initializeInternalCallback(dyn_string &exceptionInfo)
{
  string sDpName = unTree_getInternalTriggerName();
  bool bConnect = false;
  int iRes;
  
  if(!dpExists(sDpName)) {
    dpCreate(sDpName, UN_TREE_DPTYPE);
    if(!dpExists(sDpName)) {
      fwException_raise(exceptionInfo , "ERROR", getCatStr("unPanel", "WRONGINTERNAL"), "");
    }
    else {
      bConnect = true;
    }
  }
  else
    bConnect = true;

  if(bConnect) {
// create the global variable if it does not exists
    bConnect = false;
    if(!globalExists(sDpName))
      iRes = addGlobal(sDpName, DYN_STRING_VAR);

    if(iRes == 0)
      bConnect = true;
  }
  if(bConnect) {
// setup the callbacks
    dpConnect("unTree_executeEventRigthClick", false, sDpName+".eventRigthClick.command");
    dpConnect("unTree_executeEventNodeRigthClick", false, sDpName+".eventNodeRigthClick.command");
  }
}

// unTree_getInternalTriggerName
/**
Purpose:
get the internal trigger name

  @param return: string, the name of the trigger

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
string unTree_getInternalTriggerName()
{
  string sTopDisplayNodeName, sTriggerName;

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayNodeName);
  if(!unConfigGenericFunctions_nameCheck(sTopDisplayNodeName))
    sTopDisplayNodeName = "";
  sTriggerName = UN_TREE_DPNAME_PREFIX+UN_TREE_DPNAME_SEPARATOR+myManNum()+UN_TREE_DPNAME_SEPARATOR+myModuleName()+UN_TREE_DPNAME_SEPARATOR+sTopDisplayNodeName;
  return sTriggerName;
}

// unTree_executeEventRigthClick
/**
Purpose:
Callback to handle the internal rigth click event

  @param sDpe1: string, input: the dpe name
  @param iCommand: int, input: the command to execute

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_executeEventRigthClick(string sDpe1, int iCommand)
{
  bool bDone=false, bUserPanelOpened=false;
  string sSelectedKey, sDpName, sPasteNodeDpName, sPasteNodeName, sParentDpName;
  dyn_string exInfo;
  int idBar;
  string sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName, 
        sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey;
  int iCurrentPointer, iMousePointer;
  string sFunctionName, sTopDisplayNodeName;
  bool bGranted;
  dyn_string dsArguments;
  
  string globalVarName = unTree_getInternalTriggerName();

// get the arguments of the current command
  evalScript(dsArguments, "dyn_string main()"+
             "{"+
             "  return "+globalVarName+";"+
             "}",
             makeDynString());

  if(dynlen(dsArguments) >= 5) {
// get the command and if not the same as the one received in the callback exit
    sDpName = iCommand;
    if(sDpName != dsArguments[1])
      return;

  // correct command so start the work
  // start the progress bar
    idBar = unProgressBar_start("treeBar");  
  // disable the unTreeWidget tree to have one action at a time.
    unTreeWidget_setPropertyValue("tree", "Enabled", false);
    iCurrentPointer = unTreeWidget_getPropertyValue("tree", "getMousePointer");
    iMousePointer = unTreeWidget_ccHourglass;
    unTreeWidget_setPropertyValue("tree", "setMousePointer", iMousePointer);

  // disable the cascade buttons
    hierarchyCascade.enabled(false);
    deviceCascade.enabled(false);
// disable the nodeList to have one action at a time
    unTree_getUserPanelOpened(bUserPanelOpened);
    if((bUserPanelOpened) && shapeExists("nodeList"))
      unTreeWidget_setPropertyValue("nodeList", "Enabled", false);

    sSelectedKey = dsArguments[2];
    sDpName = dsArguments[3];
    sPasteNodeDpName = dsArguments[4];
    sPasteNodeName = dsArguments[5];
    
    unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayNodeName);

    unTree_getGlobalValue(sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName, 
                      sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey);

    unTree_getConfigurationVariable(sTopDisplayNodeName, UN_TREE_PRE_EVENTRIGTHCLICK, sFunctionName);
    if(isFunctionDefined(sFunctionName)) {
      execScript("main(int iCommand)"+
                  "{"+
                  "    "+sFunctionName+"(iCommand);"+
                  "}", 
                  makeDynString(), iCommand);
    }

    unTree_getConfigurationVariable(sTopDisplayNodeName, UN_TREE_HANDLEEVENTRIGTHCLICK, sFunctionName);
    if(isFunctionDefined(sFunctionName)) {
      evalScript(exInfo, "dyn_string main(int iCommand, string sSelectedKey, string sDpName, string sPasteNodeDpName, string sPasteNodeName)"+
                          "{"+
                          "    dyn_string exInfo;"+
                          "    bool bDone=false;"+
                          "    "+sFunctionName+"(iCommand, \"tree\", \"treeBar\", sSelectedKey, sDpName, sPasteNodeDpName, sPasteNodeName, true, bDone, exInfo);"+
                          "    unTree_setHandleEventData(bDone, sPasteNodeDpName, sPasteNodeName);"+
                          "    return exInfo;"+
                          "}", 
                          makeDynString(), iCommand, sSelectedKey, sDpName, 
                                          sPasteNodeDpName, sPasteNodeName);
    }
    if(dynlen(exInfo) > 0)
      fwExceptionHandling_display(exInfo);
    else {
// get the paste config and bDone;
      unTree_getHandleEventData(bDone, sPasteNodeDpName, sPasteNodeName);
      if(bDone) { // if correctly executed then redraw.
        unTree_getUserPanelOpened(bUserPanelOpened);
        unTree_isUserAllowed(bGranted, exInfo);
        switch(iCommand) {
          case unTreeWidget_popupMenu_add:
          case UN_TREE_newFolder:
          case UN_TREE_addDevice:
          case UN_TREE_addDevices:
          case UN_TREE_newDevice:
          case UN_TREE_new:
            if(sCurrentDpName == sDpName) {
              if(bUserPanelOpened) {
  //DebugN(sCurrentDpName, sDpName);
                unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
                unTree_setUserPanelOpened(bUserPanelOpened);
              }

              unTree_setHierarchyButtonReorderGlobalVariable(true);
              unTree_setHierarchyButtonReorderState(bGranted);
            }
            break;
          case unTreeWidget_popupMenu_clearTree:
          case unTreeWidget_popupMenu_initTree:
/*            if((bUserPanelOpened) && (sCurrentDpName == sDpName)){
//DebugN(sCurrentDpName, sDpName);
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
*/
            break;
          case unTreeWidget_popupMenu_reorder:
            if(sCurrentDpName == sDpName){
//              unTree_getAndShow(sDpName, "", bUserPanelOpened, false, exInfo);
//              unTree_setUserPanelOpened(bUserPanelOpened);
            }
            break;
          case unTreeWidget_popupMenu_paste:
    // show the Tree content 
            if((bUserPanelOpened) && (sCurrentDpName == sDpName)){
              unTree_getAndShow(sDpName, "", bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
            if(sPasteNodeDpName == "") {
              unTree_setPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
              unTree_setHierarchyPasteButtonGlobalVariable(false);
              unTree_setHierarchyPasteButtonState("", false);
            }
            break;
          default:
            break;
        }

        unTree_getConfigurationVariable(sTopDisplayNodeName, UN_TREE_POST_EVENTRIGTHCLICK, sFunctionName);
        if(isFunctionDefined(sFunctionName)) {
          evalScript(exInfo, "dyn_string main(int iCommand, string sSelectedKey, string sDpName, string sPasteNodeDpName, string sPasteNodeName)"+
                              "{"+
                              "    dyn_string exInfo;"+
                              "    "+sFunctionName+"(iCommand, \"tree\", \"treeBar\", sSelectedKey, sDpName, sPasteNodeDpName, sPasteNodeName, true, exInfo);"+
                              "    return exInfo;"+
                              "}", 
                              makeDynString(), iCommand, sSelectedKey, sDpName, 
                                              sPasteNodeDpName, sPasteNodeName);
        }
        
      }
    }

    unTreeWidget_setPropertyValue("tree", "setMousePointer", iCurrentPointer);
  // enable the unTreeWidget tree.
    unTreeWidget_setPropertyValue("tree", "Enabled", true);
  // stop the progress bar
    unProgressBar_stop(idBar, "treeBar");
  // enable the cascade buttons
    hierarchyCascade.enabled(true);
    deviceCascade.enabled(true);

    unTree_getUserPanelOpened(bUserPanelOpened);
    if((bUserPanelOpened) && shapeExists("nodeList"))
      unTreeWidget_setPropertyValue("nodeList", "Enabled", true);
  }

// handle any errors here
  if(dynlen(exInfo)>0)
    fwExceptionHandling_display(exInfo);

}
// unTree_EventRigthClickOperation
/**
Purpose:
Function executed when one does a right click on the tree operation. popupMenuXY can be used to show a popup menu. 
WARNING: popuMenu cannot be used.

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_EventRigthClickOperation(string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{ 
  dyn_string dsMenu, exInfo;
  int res, ans;
  string sTopDisplayName, sPanelName;
  shape aShape;
  
//DebugN("unTree_EventRigthClick", bClipboard, iPosX, iPosY);

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_CONFIGURATION_PANEL, sPanelName);
  
  if(dpExists(unTreeWidget_treeNodeDpPrefix + sTopDisplayName)) {  
    dynAppend(dsMenu, "PUSH_BUTTON, --- "+sTopDisplayName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Refresh Tree, "+unTreeWidget_popupMenu_clearTree+", 1");
    dynAppend(dsMenu,"CASCADE_BUTTON, Sort, 1");
    dynAppend(dsMenu,"Sort");
    dynAppend(dsMenu,"PUSH_BUTTON, A-Z, "  +unTreeWidget_popupMenu_sortAZ+", 1");
    dynAppend(dsMenu,"PUSH_BUTTON, Z-A, "  +unTreeWidget_popupMenu_sortZA+", 1");
    dynAppend(dsMenu,"PUSH_BUTTON, Custom,"+unTreeWidget_popupMenu_sortCustom+", 1");

//    if ((getPath(PANELS_REL_PATH,sPanelName)!="") && (sPanelName != ""))
//      dynAppend(dsMenu,"PUSH_BUTTON, Open Tree configuration, "+UN_TREE_openTreeConfiguration+", 1");
//    else
//      dynAppend(dsMenu,"PUSH_BUTTON, Open Tree configuration, "+UN_TREE_openTreeConfiguration+", 0");  
          
// show it
    res = popupMenuXY(dsMenu,iPosX, iPosY, ans);
    
    if(ans != 0) {
      aShape = getShape(sGraphTree);
      switch(ans) {
        case unTreeWidget_popupMenu_clearTree:
            unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
            unTree_EventInitializeOperation(sGraphTree, sBar, bClipboard);
            if(g_iSortMode!=unTreeWidget_popupMenu_sortCustom)
            {
              unTreeWidget_setPropertyValue(aShape, "sort", g_iSortMode, true);
            }
        break;
        case unTreeWidget_popupMenu_sortAZ:
            g_iSortMode = unTreeWidget_popupMenu_sortAZ;
            unTreeWidget_setPropertyValue(aShape, "sort", g_iSortMode, true);
        break;
        case unTreeWidget_popupMenu_sortZA:
            g_iSortMode = unTreeWidget_popupMenu_sortZA;
            unTreeWidget_setPropertyValue(aShape, "sort", g_iSortMode, true);      
        break;
        case unTreeWidget_popupMenu_sortCustom:
            aShape.sortingEnabled = false;
            g_iSortMode = unTreeWidget_popupMenu_sortCustom;
            unTreeWidget_setPropertyValue(aShape, "sort", g_iSortMode, true);
            unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
            unTree_EventInitializeOperation(sGraphTree, sBar, bClipboard);
        break;
        default:
        break;
      }
    }
  }
  else {
    dynAppend(dsMenu, "PUSH_BUTTON, --- No Tree ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  }
  
// handle any exception here
  if(dynlen(exInfo) > 0) {
    fwExceptionHandling_display(exInfo);
  }
}
// unTree_EventRigthClick
/**
Purpose:
Function executed when one does a right click on the tree. popupMenuXY can be used to show a popup menu. 
WARNING: popuMenu cannot be used.

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_EventRigthClick(string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{ 
  dyn_string dsMenu;
  dyn_string exInfo;
  int res;
  int ans;
  
  string sSelectedKey;
  string sDpName;
  string sPasteNodeName;
  string sPasteNodeDpName;
  string sFunctionName;
  string sTopDisplayName;
  
  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  sDpName = unTreeWidget_treeNodeDpPrefix + sTopDisplayName;
  unTree_getPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
  
  if(dpExists(sDpName)) 
  {
    unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_EVENTRIGTHCLICKMENU, sFunctionName);
    
    if(isFunctionDefined(sFunctionName)) 
    {
      evalScript(
          dsMenu, 
          "dyn_string main(string sDisplayNodeName, string sPasteNodeDpName, string sPasteNodeName)" +
          "{"+
          "    dyn_string dsMenu, exInfo;" +
          "    " + sFunctionName + 
          "(dsMenu, sDisplayNodeName, sPasteNodeDpName, sPasteNodeName, exInfo);" +
          "    return dsMenu;" +
          "}", 
          makeDynString(), 
          sTopDisplayName, 
          sPasteNodeDpName, 
          sPasteNodeName
        );
    }
    
    if(dynlen(dsMenu) > 0) 
    {
      sSelectedKey = unTreeWidget_compositeLabelSeparator + sTopDisplayName + unTreeWidget_labelSeparator + sDpName;

      // show it
      res = popupMenuXY(dsMenu,iPosX, iPosY, ans);
      
      if(ans != 0)
      {
        unTree_triggerEventRigthClick(
            ans, 
            makeDynString(
                sSelectedKey, 
                sDpName, 
                sPasteNodeDpName, 
                sPasteNodeName
              ), 
            exInfo
          );
      }

    }
  }
  else {
    dynAppend(dsMenu, "PUSH_BUTTON, --- No Tree ---, "+unTreeWidget_popupMenu_nodeName+", 0");
    unTree_setGlobalValue("", "", "", "", "", "", "", "");
  }
  
  // handle any exception here
  if(dynlen(exInfo) > 0) {
    fwExceptionHandling_display(exInfo);
  }
}

// unTree_EventNodeRigthClick
/**
Purpose:
Function executed when one does a right click on a node of the Tree configuration. popupMenuXY can be used to show a popup menu. 
WARNING: popuMenu cannot be used.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_EventNodeRigthClick(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{ 
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sParentKey;
  dyn_string dsMenu, exInfo;
  int res, ans;
  shape aShape;
  string sPasteNodeName, sPasteNodeDpName;
  string sParentKeyToAddInNewKey, sCurrentSelectedNodeKey;
  string sFunctionName;
  string sTopDisplayName;
  
//DebugN("unTree_EventNodeRigthClick", sFunctionName, sSelectedKey, sMode, sCompositeLabel, bClipboard, iPosX, iPosY);
  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  aShape = getShape(sGraphTree);
  sParentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
  sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey", true);

  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);

// build the popup menu
////!!!!!!!!! take care with &000 when calling isRoot.
  unTree_getPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);

  unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_EVENTNODERIGTHCLICKMENU, sFunctionName);
  if(isFunctionDefined(sFunctionName)) {
    evalScript(dsMenu, "dyn_string main(string sDisplayNodeName, string sDpName, string sPasteNodeDpName, string sPasteNodeName)"+
                  "{"+
                  "    dyn_string dsMenu, exInfo;"+
                  "    "+sFunctionName+"(dsMenu, sDisplayNodeName, sDpName, sPasteNodeDpName, sPasteNodeName, exInfo);"+
                  "    return dsMenu;"+
                  "}", makeDynString(), sDisplayNodeName, sDpName, sPasteNodeDpName, sPasteNodeName);
  }
  
  if(dynlen(dsMenu)>0) {    
    if(sParentKey == "") { // case of node in Tree, set the parentKey to use for rename
      sParentKeyToAddInNewKey = unTreeWidget_compositeLabelSeparator+sTopDisplayName+
                                  unTreeWidget_labelSeparator+
                                  unTreeWidget_treeNodeDpPrefix+sTopDisplayName;
    }
    else {
      sParentKeyToAddInNewKey = sParentKey;
    }
  
  // show it
    res = popupMenuXY(dsMenu,iPosX, iPosY, ans);
    
    if(ans != 0)
      unTree_triggerEventNodeRigthClick(ans, makeDynString(sSelectedKey, sDpName, sDisplayNodeName, 
                                              sCompositeLabel, sParentKey, sParentDpName, 
                                              sParentNodeName, sPasteNodeDpName, 
                                              sPasteNodeName, sCurrentSelectedNodeKey, 
                                              sParentKeyToAddInNewKey), exInfo);
  }
// handle any exception here
  if(dynlen(exInfo) > 0) {
    fwExceptionHandling_display(exInfo);
  }
}

// unTree_EventClick
/**
Purpose:
Function executed when one does a click on the tree. popupMenuXY can be used to show a popup menu. 
WARNING: popuMenu cannot be used.

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_EventClick(string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
  string sTopDisplayName;
  string sSelectedKey;
  string sNodeNameAdded;
  string sDpName;
  string sPasteNodeName;
  string sPasteNodeDpName;
  string sMenuText = "--- No Tree ---";
  string sFunctionName;
  
  bool bAdd = false;
  bool bPaste = false;
  bool bUserPanelOpened;
  bool bGranted = false;
  bool bReorder = false;
  bool bAddFolder = false;
  bool bAddDevice = false;
  
  dyn_string exInfo;
  dyn_string dsChildren;
  dyn_string dsReturnData;
  
  shape aShape;
  int id;
  int iCurrentPointer, iMousePointer;
  
  // start the progress bar
  id = unProgressBar_start(sBar);  

  aShape = getShape(sGraphTree);
  
  // disable the unTreeWidget tree to have one action at a time.
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentPointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);

  // disable the cascade buttons  
  hierarchyCascade.enabled(false);
  deviceCascade.enabled(false);

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);

  sDpName = unTreeWidget_treeNodeDpPrefix + sTopDisplayName;
  unTree_getPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);

  if(dpExists(sDpName)) 
  {
    // show the Tree content
    unTree_getAndShow(sDpName, "", bUserPanelOpened, false, exInfo);
    
    unTree_setUserPanelOpened(bUserPanelOpened);

    if((sPasteNodeName != "")  && (sPasteNodeDpName!=sDpName))
    {
      bPaste = true;
    }

    sMenuText = "--- " + sTopDisplayName + " ---";
    sSelectedKey = unTreeWidget_compositeLabelSeparator + sTopDisplayName + unTreeWidget_labelSeparator + sDpName;
    unTree_setGlobalValue(sDpName, sSelectedKey, "", "", "", "", "", "");
    
    fwTree_getChildren(sTopDisplayName, dsChildren, exInfo);

    if(dynlen(dsChildren) > 0) 
    {
      bReorder = true;    
    }

    unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_GETHIERARCHYSTATE, sFunctionName);
    
    if(isFunctionDefined(sFunctionName)) 
    {
      evalScript(
          dsReturnData, 
          "dyn_string main(string sDisplayNodeName, string sDpName)" +
          "{"+
          "    dyn_string dsReturnData;" +
          "    " + sFunctionName + "(dsReturnData, sDisplayNodeName, sDpName);" +
          "    return dsReturnData;" +
          "}", 
          makeDynString(), 
          sTopDisplayName, 
          sDpName
        );
      
      if(dynlen(dsReturnData) >= 3) 
      {
        if((dsReturnData[1] == "TRUE") || (dsReturnData[1] == "true"))
        {
          bAdd = true;        
        }
        if((dsReturnData[2] == "TRUE") || (dsReturnData[2] == "true"))
        {
          bAddFolder = true;
        }
        if((dsReturnData[3] == "TRUE") || (dsReturnData[3] == "true"))
        {
          bAddDevice = true;
        }
      }
    }
    
    unTree_isUserAllowed(bGranted, exInfo);

  }
  else {
    unTree_setGlobalValue("", "", "", "", "", "", "", "");
    unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);

  }
  
  unTree_setDeviceButtonGlobalVariable(false, false, false, false);
  unTree_setDeviceButtonState(false, false, false, false);

  unTree_setHierarchyButtonGlobalVariable(bAdd, bAddFolder, bAddDevice, false, false, bPaste,
                                    false, false, bReorder);  
  unTree_setHierarchyButtonState(bAdd&bGranted, bAddFolder&bGranted, bAddDevice&bGranted, false, false, bPaste&bGranted,
                                    false, false, bReorder&bGranted);  

  unTree_setHierarchyButtonText("", sPasteNodeName);
  unTree_setButtonFirstName(sMenuText);

  // stop the progress bar
  unProgressBar_stop(id, sBar);
  
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentPointer, true);
  
  // enable the unTreeWidget tree
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);

  // enable the cascade buttons  
  hierarchyCascade.enabled(true);
  deviceCascade.enabled(true);
}

// unTree_EventNodeLeftClickOperation
/**
Purpose:
Function called called when a left click on a node of the unTreeWidget is done during the operation of the 
tree hierarchy. This function get the operation panel of the node, show it in the main frame.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
  
@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_EventNodeLeftClickOperation(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sNodeName;
  dyn_string exInfo;
  shape aShape;
  int id;
  string sParentKey, sCurrentSelectedNodeKey, sTopDisplayName;
  int iCurrentPointer, iMousePointer;
  string sFunctionName;
    
//DebugN("unTree_EventNodeLeftClickOperation", sSelectedKey, sMode, sCompositeLabel, sGraphTree, sBar, bClipboard, iPosX, iPosY);
  
// start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);
//!!!!!!!!!!!!!! Do not disable the unTreeWidget tree because otherwise the dbl click to expand or collapse will not work.
//  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentPointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
  
// The unTreeWidget is not disable to allow a doubleClick as also expand

  sParentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
  sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey", true);

  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
// get the device name & check it exists
  sNodeName = _fwTree_getNodeName(sDpName);

  switch(sMode){
    case "NodeLeftClick":
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
// get the function to handle the nodeLeftClick
      unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_HANDLEEVENTNODECLICKOPERATION, sFunctionName);

      if(isFunctionDefined(sFunctionName)) {
        evalScript(exInfo, "dyn_string main(string sNodeName, string sDpName, string sSelectedKey)"+
                      "{"+
                      "    dyn_string exInfo;"+
                      "    "+sFunctionName+"(sNodeName, sDpName, sSelectedKey, exInfo);"+
                      "    return exInfo;"+
                      "}", makeDynString(), sNodeName, sDpName, sSelectedKey);
      }
      break;
    default:
      break;
  }
  
// handle any errors here
  if(dynlen(exInfo)>0)
    fwExceptionHandling_display(exInfo);

// stop the progress bar
  unProgressBar_stop(id, sBar);
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentPointer, true);
//!!!!!!!!!!! as the unTreeWidget is not enabled then do not re-enabled it.
//  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
}

// unTree_EventNodeRigthClickOperation
/**
Purpose:
Function executed when one does a right click on a node of the Tree. popupMenuXY can be used to show a popup menu. 
WARNING: popuMenu cannot be used.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_EventNodeRigthClickOperation(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{ 
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sParentKey, sNodeName;
  dyn_string dsMenu, exInfo;
  int res, ans;
  shape aShape;
  string sPasteNodeName, sPasteNodeDpName;
  string sParentKeyToAddInNewKey, sCurrentSelectedNodeKey;
  string sFunctionName;
  string sTopDisplayName;
  
//DebugN("unTree_EventNodeRigthClickOperation", sFunctionName, sSelectedKey, sMode, sCompositeLabel, bClipboard, iPosX, iPosY);
  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  aShape = getShape(sGraphTree);
  sParentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
  sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey", true);

  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
// get the device name & check it exists
  sNodeName = _fwTree_getNodeName(sDpName);

// build the popup menu

  unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_EVENTNODERIGTHCLICKOPERATIONMENU, sFunctionName);
  if(isFunctionDefined(sFunctionName)) {
    evalScript(dsMenu, "dyn_string main(string sNodeName, string sDpName, string sSelectedKey)"+
                  "{"+
                  "    dyn_string dsMenu, exInfo;"+
                  "    "+sFunctionName+"(dsMenu, sNodeName, sDpName, sSelectedKey, exInfo);"+
                  "    return dsMenu;"+
                  "}", makeDynString(), sDisplayNodeName, sDpName, sPasteNodeDpName, sPasteNodeName);

  // show it
    res = popupMenuXY(dsMenu,iPosX, iPosY, ans);
    
    if(ans != 0) {
      unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_HANDLEEVENTNODERIGTHCLICKOPERATION, sFunctionName);
      if(isFunctionDefined(sFunctionName)) {
        evalScript(exInfo, "dyn_string main(string sNodeName, string sDpName, string sSelectedKey, int ans)"+
                      "{"+
                      "    dyn_string exInfo;"+
                      "    "+sFunctionName+"(sNodeName, sDpName, sSelectedKey, ans, exInfo);"+
                      "    return exInfo;"+
                      "}", makeDynString(), sNodeName, sDpName, sSelectedKey, ans);
      }
    }
  }
    
  // handle any exception here
  if(dynlen(exInfo) > 0) {
    fwExceptionHandling_display(exInfo);
  }
}

// unTree_EventNodeLeftClickConfiguration
/**
Purpose:
Function called called when a left click on a node of the unTreeWidget is done during the configuration of the 
tree hierarchy. This function get the configurationPanel of the node and show it.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_EventNodeLeftClickConfiguration(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sNodeName, sDeviceName, sDeviceType;
  dyn_string exInfo, dsChildren;
  shape aShape;
  int id, i, len;
  int isClipboard;
  bool isInClipboard;
  bool bNavigation=false, bLink=false, bUnlink=false, bGranted=false, bDollar=false, bReorder=false, bRename=false;
  bool bPaste = false, bUserPanelOpened=false, bAdd=false, bAddFolder=false, bAddDevice=false, bLinkAllowed=false;
  string sPasteNodeName, sPasteNodeDpName, sParentKey, sCurrentSelectedNodeKey;
  int iCurrentPointer, iMousePointer;
  string sFunctionName, sTextToShow, sTopDisplayName;
  dyn_string dsReturnData;
    
  // start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);

  //!!!!!!!!!!!!!! Do not disable the unTreeWidget tree because otherwise the dbl click to expand or collapse will not work.
  //  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentPointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
  
	// The unTreeWidget is not disable to allow a doubleClick as also expand
	// disable the cascade buttons  
  hierarchyCascade.enabled(false);
  deviceCascade.enabled(false);

  sParentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
  sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey", true);

  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
	
	// get the device name & check it exists
  sNodeName = _fwTree_getNodeName(sDpName);
  isClipboard = fwTree_isClipboard(sNodeName, exInfo);
  isInClipboard = fwTree_isInClipboard(sNodeName, exInfo);


  switch(sMode){
    case "NodeLeftClick":
      unTree_isUserAllowed(bGranted, exInfo);

      unTree_getPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
      if((sPasteNodeName != "") && (!isInClipboard) && (sPasteNodeDpName!=sDpName))
        bPaste = true;
        
      fwTree_getChildren(sNodeName, dsChildren, exInfo);
      if(dynlen(dsChildren) > 0) 
        bReorder = true;
          
			// look in the device model to find the list of panels
			// show the configuration panel
      if(!isClipboard) {
        fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);

        unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
				
				// get the state of hierarchy add buttons
        unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_GETHIERARCHYSTATE, sFunctionName);
        if(isFunctionDefined(sFunctionName)) {
          evalScript(dsReturnData, "dyn_string main(string sDisplayNodeName, string sDpName)"+
                        "{"+
                        "    dyn_string dsReturnData;"+
                        "    "+sFunctionName+"(dsReturnData, sDisplayNodeName, sDpName);"+
                        "    return dsReturnData;"+
                        "}", makeDynString(), sDisplayNodeName, sDpName);
          if(dynlen(dsReturnData) >= 4) {
            if((dsReturnData[1] == "TRUE") || (dsReturnData[1] == "true"))
              bAdd = true;        
            if((dsReturnData[2] == "TRUE") || (dsReturnData[2] == "true"))
              bAddFolder = true;
            if((dsReturnData[3] == "TRUE") || (dsReturnData[3] == "true"))
              bAddDevice = true;
            if((dsReturnData[4] == "TRUE") || (dsReturnData[4] == "true"))
              bRename = true;          }
        }

        if(sDeviceType != NO_DEVICE) {
          bUnlink = true;

          unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_GETDEVICESTATE, sFunctionName);
					
					// get the state of the device button
          if(isFunctionDefined(sFunctionName)) {
            evalScript(dsReturnData, "dyn_string main(string sDisplayNodeName, string sDpName, string sDeviceName, string sDeviceType)"+
                          "{"+
                          "    dyn_string dsReturnData;"+
                          "    "+sFunctionName+"(dsReturnData, sDisplayNodeName, sDpName, sDeviceName, sDeviceType);"+
                          "    return dsReturnData;"+
                          "}", makeDynString(), sDisplayNodeName, sDpName, sDeviceName, sDeviceType);
            if(dynlen(dsReturnData) >= 4) {
              if((dsReturnData[1] == "TRUE") || (dsReturnData[1] == "true"))
                bNavigation = true;        
              if((dsReturnData[2] == "TRUE") || (dsReturnData[2] == "true"))
                bDollar = true;
              if((dsReturnData[3] == "TRUE") || (dsReturnData[3] == "true"))
                bLink = true;
              if((dsReturnData[4] == "TRUE") || (dsReturnData[4] == "true"))
                bLinkAllowed = true;        
            }
          }
          
					// get the node display name to show          
          sTextToShow = sDisplayNodeName;
          unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_GETDISPLAYNODENAME, sFunctionName);
          if(isFunctionDefined(sFunctionName)) {
            evalScript(dsReturnData, "dyn_string main(string sDisplayNodeName, string sDpName, string sDeviceName, string sDeviceType)"+
                          "{"+
                          "    dyn_string dsReturnData;"+
                          "    "+sFunctionName+"(dsReturnData, sDisplayNodeName, sDpName, sDeviceName, sDeviceType);"+
                          "    return dsReturnData;"+
                          "}", makeDynString(), sDisplayNodeName, sDpName, sDeviceName, sDeviceType);
            if(dynlen(dsReturnData) >= 1) {
              sTextToShow = dsReturnData[1];
            }
          }
        }
        else {
          bLink = true;

          unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_GETDEVICESTATE, sFunctionName);
					
					// get the state of the device button
          if(isFunctionDefined(sFunctionName)) {
            evalScript(dsReturnData, "dyn_string main(string sDisplayNodeName, string sDpName, string sDeviceName, string sDeviceType)"+
                          "{"+
                          "    dyn_string dsReturnData;"+
                          "    "+sFunctionName+"(dsReturnData, sDisplayNodeName, sDpName, sDeviceName, sDeviceType);"+
                          "    return dsReturnData;"+
                          "}", makeDynString(), sDisplayNodeName, sDpName, sDeviceName, sDeviceType);
            if(dynlen(dsReturnData) >= 4) {
              if((dsReturnData[4] == "TRUE") || (dsReturnData[4] == "true"))
                bLinkAllowed = true;        
            }
          }
        }
          
        unTree_setHierarchyButtonGlobalVariable(bAdd, bAddFolder, bAddDevice, bRename, true, bPaste,
                                            bRename, bRename, bReorder);
        unTree_setHierarchyButtonState(bAdd&bGranted, bAddFolder&bGranted, bAddDevice&bGranted, bRename&bGranted, bGranted, bPaste&bGranted,
                                            bRename&bGranted, bGranted&bRename, bGranted&bReorder);
        unTree_setDeviceButtonGlobalVariable(bNavigation&bLinkAllowed, bLink&bLinkAllowed, bUnlink&bLinkAllowed, bDollar&bLinkAllowed);
        unTree_setDeviceButtonState(bNavigation&bGranted&bLinkAllowed, bLink&bGranted&bLinkAllowed, 
                                              bUnlink&bGranted&bLinkAllowed, bDollar&bGranted&bLinkAllowed);
      }
      else {	
				// only paste is allowed          
        unTree_setHierarchyButtonGlobalVariable(false, false, false, false, false, bPaste,
                                false, false, bReorder);
        unTree_setHierarchyButtonState(false, false, false, false, false, bPaste&bGranted,
                                false, false, bGranted&bReorder);
        unTree_setDeviceButtonGlobalVariable(false, false, false, false);
        unTree_setDeviceButtonState(false, false, false, false);
      }
                
      unTree_setHierarchyButtonText(sDisplayNodeName, sPasteNodeName);
      unTree_setGlobalValue(sDpName, sSelectedKey, sParentDpName, sParentKey, 
                                    sDeviceName, sDeviceType, sCompositeLabel, sCurrentSelectedNodeKey);
      unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
      unTree_setUserPanelOpened(bUserPanelOpened);
      

      unTree_setButtonFirstName("--- "+sDisplayNodeName+" ---");
      break;
    default:
      unTree_setGlobalValue("", "", "", "", "", "", "", "");
      unTree_setDeviceButtonGlobalVariable(false, false, false, false);
      unTree_setDeviceButtonState(false, false, false, false);
      unTree_setHierarchyButtonGlobalVariable(false, false, false, false, false, false,
                                          false, false, false);
      unTree_setHierarchyButtonState(false, false, false, false, false, false,
                                          false, false, false);
      unTree_setHierarchyButtonText("", "");
      unTree_setButtonFirstName("--- ---");
      unTree_setUserPanelOpened(false);
      break;
  }
  
	// handle any errors here
  if(dynlen(exInfo)>0)
    fwExceptionHandling_display(exInfo);

	// stop the progress bar
  unProgressBar_stop(id, sBar);
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentPointer, true);
	
	// !!!!!!!!!!! as the unTreeWidget is not enabled then do not re-enabled it.
	// unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);

	// enable the cascade buttons  
  hierarchyCascade.enabled(true);
  deviceCascade.enabled(true);
}

// unTree_handleDefaultEventRigthClick
/**
Purpose:
Basic unTree function: handle the result of the event rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sPasteNodeDpName: string, input/output: the node dp name to paste
  @param sPasteNodeName: string, input/output: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param bDone: bool, output: action executed
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_handleDefaultEventRigthClick(int ans, string sGraphTree, string sBar, string sSelectedKey,
                                    string sDpName, string &sPasteNodeDpName, string &sPasteNodeName, 
                                    bool bClipboard, bool &bDone, dyn_string &exInfo)
{
  dyn_float dfFloat;
  dyn_string dsText;
  int i, len;
  shape aShape = getShape(sGraphTree);
  string sNodeNameAdded;
  string sTopDisplayName;
  
  bDone = false;
/*
DebugN("unTree_handleDefaultEventRigthClick", ans, sGraphTree, sBar, sSelectedKey,
        sDpName, sPasteNodeDpName, sPasteNodeName, bClipboard);  
*/
  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
        
  switch(ans) {
    case UN_TREE_newFolder:
      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "New Folder", makeDynString("$sTextQuestion:New folder name:"), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
        if(dfFloat[1] == 1) {
      // new node.
          bDone = true;
          len = dynlen(dsText);
          for(i=1;i<=len;i++) {
            if(dsText[i] != "") {
              unTree_add(sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, dsText[i], true, sNodeNameAdded, exInfo);
            }
          }
        }
      }
      break;
    case unTreeWidget_popupMenu_clearTree:
      bDone = true;
      unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
    case unTreeWidget_popupMenu_initTree:
      unTree_setPasteGlobalValue("", "");
      unTree_loadTree(sGraphTree, sBar, bClipboard);
// show the WindowTree content
      unTree_EventClick(sGraphTree, sBar, bClipboard, 0, 0);
      break;
    case unTreeWidget_popupMenu_paste:
    // take the node name from sPasteNodeName and sPasteNodeDpName and paste it
      unTree_paste(sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, 
                                    sPasteNodeDpName, sPasteNodeName, 
                                    bClipboard, true, exInfo);

    // if no error reset sPasteNodeName and sPasteNodeDpName
      if(dynlen(exInfo) <= 0) {
        sPasteNodeName = "";
        sPasteNodeDpName = "";
        bDone = true;
      }
      break;
    case unTreeWidget_popupMenu_reorder: 
      unTree_reorder(sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, "", 
                              "", bClipboard, true, bDone, exInfo);
      if(bDone){
        unTree_loadTree(sGraphTree, sBar, bClipboard);

// show the WindowTree content
        unTree_EventClick(sGraphTree, sBar, bClipboard, 0, 0);
      }
      bDone = true;
      break;
    default:
      break;
  }
}

// unTree_executeEventNodeRigthClick
/**
Purpose:
Callback to handle the internal node rigth click event

  @param sDpe1: string, input: the dpe name
  @param iCommand: int, input: the command to execute

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{Callback}

*/
unTree_executeEventNodeRigthClick(string sDpe1, int iCommand)
{
  int idBar;
  bool bClipboard=unTreeWidget_getPropertyValue("tree", "get_bClipboard");
  dyn_string exInfo;
  string sPasteNodeName, sPasteNodeDpName;
  string sDpName, sDpNameKey, sParentDpName, sParentNodeName, sDisplayNodeName, sNodeName; 
  string sParentDpNameKey, sDeviceName, sDeviceType, sCompositeLabel, sCurrentSelectedNodeKey, sParentKeyToAddInNewKey;
  bool bCut = false, bDone = false, bUserPanelOpened=false, bGranted=false;
  string sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName, 
        sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel;
  int iCurrentPointer, iMousePointer;
  string sFunctionName, sTextToShow, sTopDisplayName;
  dyn_string dsReturnData;
  bool bDollar=false, bNavigation = false, bLink=false;
  dyn_string dsArguments;
  string sSelectedItem;

  string globalVarName = unTree_getInternalTriggerName();
  
// get the arguments of the current command
  evalScript(dsArguments, "dyn_string main()"+
             "{"+
             "  return "+globalVarName+";"+
             "}",
             makeDynString());

  if(dynlen(dsArguments) >= 12) {
// get the command if not the same as the one received then exit
    sDpName = iCommand;
    if(sDpName != dsArguments[1])
      return;
  
  // start the progress bar
    idBar = unProgressBar_start("treeBar");  
  // disable the unTreeWidget tree to have one action at a time.
    unTreeWidget_setPropertyValue("tree", "Enabled", false);
    iCurrentPointer = unTreeWidget_getPropertyValue("tree", "getMousePointer");
    iMousePointer = unTreeWidget_ccHourglass;
    unTreeWidget_setPropertyValue("tree", "setMousePointer", iMousePointer);

  // disable the cascade buttons
    hierarchyCascade.enabled(false);
    deviceCascade.enabled(false);
  // disable the nodeList to have one action at a time
    unTree_getUserPanelOpened(bUserPanelOpened);
    if((bUserPanelOpened) && shapeExists("nodeList"))
      unTreeWidget_setPropertyValue("nodeList", "Enabled", false);
      
    sDpNameKey = dsArguments[2];
    sDpName = dsArguments[3];
    sDisplayNodeName = dsArguments[4];
    sCompositeLabel = dsArguments[5];
    sParentDpNameKey = dsArguments[6];
    sParentDpName = dsArguments[7];
    sParentNodeName = dsArguments[8];
    sPasteNodeDpName = dsArguments[9];
    sPasteNodeName = dsArguments[10];
    sCurrentSelectedNodeKey = dsArguments[11];
    sParentKeyToAddInNewKey = dsArguments[12];
    
    unTree_getGlobalValue(sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName, 
                      sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey);

    unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
    unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_PRE_EVENTNODERIGTHCLICK, sFunctionName);
    if(isFunctionDefined(sFunctionName)) {	
      execScript("main(int iCommand, string sCurrentDpName, string sDpName)"+
                  "{"+
                  "    "+sFunctionName+"(iCommand, sCurrentDpName, sDpName);"+
                  "}", 
                  makeDynString(), iCommand, sCurrentDpName, sDpName);
    }

    unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_HANDLEEVENTNODERIGTHCLICK, sFunctionName);
    if(isFunctionDefined(sFunctionName)) {
      evalScript(exInfo, "dyn_string main(int iCommand, string sDpNameKey, string sDpName, string sDisplayNodeName, "+
                                            " string sCompositeLabel, string sParentDpNameKey, string sParentDpName, "+
                                            "string sParentNodeName, string sPasteNodeDpName, "+
                                            "string sPasteNodeName, bool bClipboard, string sCurrentSelectedNodeKey, "+
                                            "string sParentKeyToAddInNewKey)"+
                          "{"+
                          "    dyn_string exInfo;"+
                          "    bool bDone=false;"+
                          "    "+sFunctionName+"(iCommand, \"tree\", \"treeBar\", sDpNameKey, "+
                          "                                              sDpName, sDisplayNodeName,"+
                          "                                              sCompositeLabel, sParentDpNameKey, sParentDpName,"+
                          "                                              sParentNodeName, sPasteNodeDpName,"+
                          "                                              sPasteNodeName, bClipboard, bDone, sCurrentSelectedNodeKey,"+
                          "                                              sParentKeyToAddInNewKey, exInfo);"+
                          "    unTree_setHandleEventData(bDone, sPasteNodeDpName, sPasteNodeName);"+
                          "    return exInfo;"+
                          "}", 
                          makeDynString(), iCommand, sDpNameKey, sDpName, sDisplayNodeName, 
                                            sCompositeLabel, 
                                            sParentDpNameKey, sParentDpName, 
                                            sParentNodeName, sPasteNodeDpName, 
                                            sPasteNodeName, bClipboard, sCurrentSelectedNodeKey, 
                                            sParentKeyToAddInNewKey);

    }

    sNodeName = _fwTree_getNodeName(sDpName);
//DebugN(sCurrentDpName, sDpName, sNodeName);
// display the errors here.
    if(dynlen(exInfo) > 0) {
      fwExceptionHandling_display(exInfo);
    }
    else {
      unTree_getHandleEventData(bDone, sPasteNodeDpName, sPasteNodeName);
      if(bDone) {
        unTree_getUserPanelOpened(bUserPanelOpened);
        unTree_isUserAllowed(bGranted, exInfo);
  // close the user panel or not 
        switch(iCommand) {
          case unTreeWidget_popupMenu_add:
          case UN_TREE_newFolder:
          case UN_TREE_addDevice:
          case UN_TREE_addDevices:
          case UN_TREE_newDevice:
          case UN_TREE_new:
            if(sCurrentDpName == sDpName) {
              if(bUserPanelOpened) {
                unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
                unTree_setUserPanelOpened(bUserPanelOpened);
              }
              
              unTree_setHierarchyButtonReorderGlobalVariable(true);
              unTree_setHierarchyButtonReorderState(bGranted);
            }
            break;
          case unTreeWidget_popupMenu_reorder: 
// redraw trigger automatically by the EventNodeLeftClick if current node
// but in case of error or cancel ->redraw
            if(sCurrentDpName == sDpName) {
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
            break;
          case unTreeWidget_popupMenu_paste:
            if((bUserPanelOpened) && (sCurrentDpName == sDpName)){
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
            
            if(sPasteNodeDpName == "") {
              unTree_setPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
              unTree_setHierarchyPasteButtonGlobalVariable(false);
              unTree_setHierarchyPasteButtonState("", false);
            }
            break;
          case unTreeWidget_popupMenu_remove:
            if(sPasteNodeDpName == sDpName) { // the current selected node to delete is also in the paste
              unTree_setHierarchyPasteButtonGlobalVariable(false);
              unTree_setHierarchyPasteButtonState("", false);
              unTree_setPasteGlobalValue("", "");
            }
// redraw trigger automatically by the LeftClick
            if(unTree_isFromNodeList()) {
              sDpName = sParentDpName;
              _fwTree_getParent(sDpName, sParentDpName);
              if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
                sParentDpName = "";
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
         // IS-623  unCore - unTreeWidget, unCore - unWindowTree  WindowTree and TrendTree configuration: removing a node and adding a new one provoke and error, showWindowTree does not show the top node 
            // select clipboard
          unTreeWidget_setPropertyValue("tree", "setNodeState_selected", 
                    sTopDisplayName+unTreeWidget_labelSeparator+_fwTree_makeNodeName(sTopDisplayName)+
                        unTreeWidget_compositeLabelSeparator+
                        _fwTree_getClipboard(sTopDisplayName)+unTreeWidget_labelSeparator+_fwTree_makeNodeName(_fwTree_getClipboard(sTopDisplayName))+
                        unTreeWidget_keySeparator+unTreeWidget_compositeLabelSeparator+sTopDisplayName+unTreeWidget_labelSeparator+_fwTree_makeNodeName(sTopDisplayName));
              // select TopDisplayNode.
          sSelectedItem = unTreeWidget_getPropertyValue("tree", "getNodeSelectedkey"); 
          unTreeWidget_setPropertyValue("tree", "setNodeState_notSelected", sSelectedItem);
            break;
          case unTreeWidget_popupMenu_cut:
//DebugN(sCurrentDpName, sDpName, sParentDpName);
// even for the TopTree node, there is always the clipboard so the re-order is OK
// for any other node, the nodeleftclick is triggered if the parent node is selected.
// so re-order is always correct.
            unTree_setPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
            unTree_setHierarchyPasteButtonGlobalVariable(sPasteNodeDpName != "");
            if(unTree_isFromNodeList() && !fwTree_isClipboard(sParentNodeName, exInfo)) {
              sDpName = sParentDpName;
              _fwTree_getParent(sDpName, sParentDpName);

              if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
                sParentDpName = "";
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
              if (!fwTree_isInClipboard(_fwTree_getNodeName(sCurrentDpName), exInfo)){
                unTree_setHierarchyPasteButtonState(sPasteNodeName, (sPasteNodeDpName != "") & bGranted);
              }
              
            }
            else if (!fwTree_isInClipboard(_fwTree_getNodeName(sCurrentDpName), exInfo)){
              unTree_setHierarchyPasteButtonState(sPasteNodeName, (sPasteNodeDpName != "") & bGranted);
            }
// in the other case the redraw trigger automatically by the EventNodeLeftClick
         // IS-623  unCore - unTreeWidget, unCore - unWindowTree  WindowTree and TrendTree configuration: removing a node and adding a new one provoke and error, showWindowTree does not show the top node 
            // select clipboard
          unTreeWidget_setPropertyValue("tree", "setNodeState_selected", 
                    sTopDisplayName+unTreeWidget_labelSeparator+_fwTree_makeNodeName(sTopDisplayName)+
                        unTreeWidget_compositeLabelSeparator+
                        _fwTree_getClipboard(sTopDisplayName)+unTreeWidget_labelSeparator+_fwTree_makeNodeName(_fwTree_getClipboard(sTopDisplayName))+
                        unTreeWidget_keySeparator+unTreeWidget_compositeLabelSeparator+sTopDisplayName+unTreeWidget_labelSeparator+_fwTree_makeNodeName(sTopDisplayName));
              // select TopDisplayNode.
          sSelectedItem = unTreeWidget_getPropertyValue("tree", "getNodeSelectedkey"); 
          unTreeWidget_setPropertyValue("tree", "setNodeState_notSelected", sSelectedItem);
            break;
          case unTreeWidget_popupMenu_copy:
            unTree_setPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
            unTree_setHierarchyPasteButtonGlobalVariable(sPasteNodeDpName != "");
            if(!fwTree_isInClipboard(sNodeName, exInfo)) 
              unTree_setHierarchyPasteButtonState(sPasteNodeName, (sPasteNodeDpName != "") & bGranted);
            
            if(unTree_isFromNodeList() && fwTree_isClipboard(sParentNodeName, exInfo)) {
              sDpName = sParentDpName;
              _fwTree_getParent(sDpName, sParentDpName);
              if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
                sParentDpName = "";
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
            else if(fwTree_isClipboard(sParentNodeName, exInfo) && (bUserPanelOpened)&&
                        (sParentDpName == sCurrentDpName)) {
              sDpName = sParentDpName;
              _fwTree_getParent(sDpName, sParentDpName);
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }

            break;
          case UN_TREE_horizontalNavigation:
            break;
          case UN_TREE_deviceConfiguration:
            break;
          case UN_TREE_linkToDevice:
            break;
          case UN_TREE_unLinkToDevice:		  
            if(unTree_isFromNodeList() || (sCurrentDpName == sParentDpName)) {
              sDpName = sParentDpName;
              _fwTree_getParent(sDpName, sParentDpName);
              if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
                sParentDpName = "";
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
// set link to true and unlink and navigation to false
            else if(sCurrentDpName == sDpName) {
              unTree_setDeviceButtonGlobalVariable(false, true, false, false);
              unTree_setDeviceButtonState(false, bGranted, false, false);
// open the user panel with no detail
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
            break;
          case UN_TREE_changeLinkedDevice:		  
            if(unTree_isFromNodeList() || (sCurrentDpName == sParentDpName)) {
              sDpName = sParentDpName;
              _fwTree_getParent(sDpName, sParentDpName);
              if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
                sParentDpName = "";
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
// set link to true and unlink and navigation to false
            else if(sCurrentDpName == sDpName) {
              unTree_setDeviceButtonGlobalVariable(false, true, false, false);
              unTree_setDeviceButtonState(false, bGranted, false, false);
// open the user panel with no detail
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
            break;
          case unTreeWidget_popupMenu_rename:
            if(sPasteNodeDpName == sDpName) { // the current selected node to delete is also in the paste
              unTree_setHierarchyPasteButtonGlobalVariable(false);
              unTree_setHierarchyPasteButtonState("", false);
              unTree_setPasteGlobalValue("", "");
            }
            if(unTree_isFromNodeList() || (sCurrentDpName == sParentDpName)) {
              sDpName = sParentDpName;
              _fwTree_getParent(sDpName, sParentDpName);
              if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
                sParentDpName = "";
              unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
              unTree_setUserPanelOpened(bUserPanelOpened);
            }
         // IS-623  unCore - unTreeWidget, unCore - unWindowTree  WindowTree and TrendTree configuration: removing a node and adding a new one provoke and error, showWindowTree does not show the top node 
            // select clipboard
          unTreeWidget_setPropertyValue("tree", "setNodeState_selected", 
                    sTopDisplayName+unTreeWidget_labelSeparator+_fwTree_makeNodeName(sTopDisplayName)+
                        unTreeWidget_compositeLabelSeparator+
                        _fwTree_getClipboard(sTopDisplayName)+unTreeWidget_labelSeparator+_fwTree_makeNodeName(_fwTree_getClipboard(sTopDisplayName))+
                        unTreeWidget_keySeparator+unTreeWidget_compositeLabelSeparator+sTopDisplayName+unTreeWidget_labelSeparator+_fwTree_makeNodeName(sTopDisplayName));
              // select TopDisplayNode.
          sSelectedItem = unTreeWidget_getPropertyValue("tree", "getNodeSelectedkey"); 
          unTreeWidget_setPropertyValue("tree", "setNodeState_notSelected", sSelectedItem);
            break;
          default:
            break;
        }
        
        unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_POST_EVENTNODERIGTHCLICK, sFunctionName);
        if(isFunctionDefined(sFunctionName)) {
          evalScript(exInfo, "dyn_string main(int iCommand, string sDpNameKey, string sDpName, string sDisplayNodeName, "+
                                                " string sCompositeLabel, string sParentDpNameKey, string sParentDpName, "+
                                                "string sParentNodeName, string sPasteNodeDpName, "+
                                                "string sPasteNodeName, bool bClipboard, string sCurrentSelectedNodeKey, "+
                                                "string sParentKeyToAddInNewKey)"+
                              "{"+
                              "    dyn_string exInfo;"+
                              "    "+sFunctionName+"(iCommand, \"tree\", \"treeBar\", sDpNameKey, "+
                              "                                              sDpName, sDisplayNodeName,"+
                              "                                              sCompositeLabel, sParentDpNameKey, sParentDpName,"+
                              "                                              sParentNodeName, sPasteNodeDpName,"+
                              "                                              sPasteNodeName, bClipboard, sCurrentSelectedNodeKey,"+
                              "                                              sParentKeyToAddInNewKey, exInfo);"+
                              "    return exInfo;"+
                              "}", 
                              makeDynString(), iCommand, sDpNameKey, sDpName, sDisplayNodeName, 
                                                sCompositeLabel, 
                                                sParentDpNameKey, sParentDpName, 
                                                sParentNodeName, sPasteNodeDpName, 
                                                sPasteNodeName, bClipboard, sCurrentSelectedNodeKey, 
                                                sParentKeyToAddInNewKey);
    
        }
        
      }
    }

    unTree_setNodeList(false);
    unTreeWidget_setPropertyValue("tree", "setMousePointer", iCurrentPointer);
  // enable the unTreeWidget tree.
    unTreeWidget_setPropertyValue("tree", "Enabled", true);
  // stop the progress bar
    unProgressBar_stop(idBar, "treeBar");
  // enable the cascade buttons
    hierarchyCascade.enabled(true);
    deviceCascade.enabled(true);

    unTree_getUserPanelOpened(bUserPanelOpened);
    if((bUserPanelOpened) && shapeExists("nodeList"))
      unTreeWidget_setPropertyValue("nodeList", "Enabled", true);
  }

// handle any errors here
  if(dynlen(exInfo)>0)
    fwExceptionHandling_display(exInfo);
}

// unTree_handleDefaultEventNodeRigthClick
/**
Purpose:
Basic unTree function: handle the result of the event node rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the display node name
  @param sCompositeLabel: string, input: the composite label of the node name
  @param sParentKey: string, input: the key of the parent
  @param sParentDpName: string, input: the parent data point name
  @param sParentNodeName: string, input: the parent node name
  @param sPasteNodeDpName: string, input/output: the node dp name to paste
  @param sPasteNodeName: string, input/output: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param bDone: bool, output: action executed
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_handleDefaultEventNodeRigthClick(int ans, string sGraphTree, string sBar, 
                                        string sSelectedKey, string sDpName, string sDisplayNodeName, 
                                        string sCompositeLabel, string sParentKey, string sParentDpName, 
                                        string sParentNodeName, string &sPasteNodeDpName, 
                                        string &sPasteNodeName, bool bClipboard, bool &bDone, 
                                        string sCurrentSelectedNodeKey, string sParentKeyToAddInNewKey, 
                                        dyn_string &exInfo)
{
  dyn_float dfFloat,dfPositionToRemove;
  dyn_string dsText, dsChildren, dsChildrenDisplayName,dsTemp;
  string sCopiedNodeName, sParent, sSelected, sPanelPath;
  int i, len, ref;
  bool bUserPanelOpened;
  int pos;
  string sNodeName, sDeviceName, sDeviceType, sNodeNameAdded, node_name, ref_obj;
  dyn_string dsUserData;
  
  bDone = false;
  dfPositionToRemove = "";

  switch(ans) {
    case UN_TREE_newFolder:
      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "New Folder", makeDynString("$sTextQuestion:New folder name:"), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
        if(dfFloat[1] == 1) {
          bDone = true;
      // new node.
          len = dynlen(dsText);
          for(i=1;i<=len;i++) {
            if(dsText[i] != "") {
              unTree_add(sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, dsText[i], false, sNodeNameAdded, exInfo);
            }
          }
        }
      }
      break;
    case unTreeWidget_popupMenu_remove:   
      dsTemp =  strsplit(sCompositeLabel, unTreeWidget_labelSeparator);
      sSelected = _fwTree_getNodeName(dsTemp[dynlen(dsTemp)]);
      fwTree_getChildren(_fwTree_getNodeName(sParentDpName), dsChildren,exInfo);
      for (int i = 1; i<=dynlen(dsChildren); i++) {
        if (sSelected == dsChildren[i]) {
          sSelected = i;
          break;
        }
      }
      if (dynlen(exInfo)>0) {
        DebugN("----  ERROR  ----");
        DebugN(exInfo);
        fwExceptionHandling_display(exInfo);
        return; 
      }  
      for(int i = 1; i <= dynlen(dsChildren); i++)
    	{
    		node_name = dsChildren[i];
    		ref_obj = dsChildren[i];
    		node_name = fwTree_getNodeDisplayName(dsChildren[i], exInfo);
    		if((ref = fwTreeUtil_isObjectReference(ref_obj)))
    		{
    			node_name = "&"+node_name;
    			node_name += "("+fwTreeUtil_getReferencedObjectDevice(ref_obj)+")";
    		}
        
    		dynAppend(dsChildrenDisplayName, node_name);
    	}
      if (dynlen(exInfo)>0) {
        DebugN("----  ERROR  ----");
        DebugN(exInfo);
        fwExceptionHandling_display(exInfo);
        return; 
      } 
      sPanelPath = "vision/unTree/removePanels.pnl";

      unGraphicalFrame_ChildPanelOnCentralModalReturn (sPanelPath, "remove", 
                            makeDynString("$sChildren:"+dsChildrenDisplayName,"$iSelectedPosition:"+sSelected), dfPositionToRemove, dsText);
      for (int i = 1 ; i<=dynlen(dfPositionToRemove); i++) {
        if (sParentKey == "") {
          sSelectedKey = sParentNodeName + unTreeWidget_labelSeparator + sParentDpName + unTreeWidget_compositeLabelSeparator 
                       +dsChildrenDisplayName[dfPositionToRemove[i]] + unTreeWidget_labelSeparator + _fwTree_makeNodeName(dsChildren[dfPositionToRemove[i]]) + "@"
                       +unTreeWidget_compositeLabelSeparator + sParentNodeName + unTreeWidget_labelSeparator + sParentDpName;
        } else {
          sSelectedKey = sParentNodeName + unTreeWidget_labelSeparator + sParentDpName + unTreeWidget_compositeLabelSeparator 
                       +dsChildrenDisplayName[dfPositionToRemove[i]] + unTreeWidget_labelSeparator + _fwTree_makeNodeName(dsChildren[dfPositionToRemove[i]]) + "@" + sParentKey;
        }
        sDpName = _fwTree_makeNodeName(dsChildren[dfPositionToRemove[i]]);
        sDisplayNodeName = dsChildrenDisplayName[dfPositionToRemove[i]];
       
        unTreeWidget_remove(sGraphTree, sSelectedKey, sParentKey, sDpName, sDisplayNodeName, sParentDpName, 
                                                              sParentNodeName, bClipboard, exInfo);
       
        delay(0,300);
        bDone = true;
      }
     
      break;
      
      
    case unTreeWidget_popupMenu_cut:
      unTreeWidget_cut(sGraphTree, sSelectedKey, sParentKey, sDpName, sDisplayNodeName, sParentDpName, 
                                                        sParentNodeName, bClipboard, exInfo);
    // if no error put the node in sPasteNodeName and sPasteNodeDpName
      if(dynlen(exInfo) <= 0) {
        sPasteNodeName = sDisplayNodeName;
        sPasteNodeDpName = sDpName;
        bDone = true;
      }
      break;
    case unTreeWidget_popupMenu_copy:
      unTreeWidget_copy(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sParentDpName, 
                                      sParentNodeName, bClipboard, "Copy_of_"+sDisplayNodeName, sCopiedNodeName, exInfo);
    // if no error put the copied node in sPasteNodeName and sPasteNodeDpName
      if(dynlen(exInfo) <= 0) {
        sPasteNodeName = fwTree_getNodeDisplayName(sCopiedNodeName, exInfo);;
        sPasteNodeDpName = _fwTree_makeNodeName(sCopiedNodeName);
        bDone = true;
      }
      break;
    case unTreeWidget_popupMenu_paste:
      if(dpExists(sPasteNodeDpName)) {
      // take the node name from sPasteNodeName and sPasteNodeDpName and paste it
        unTreeWidget_paste(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sPasteNodeDpName, 
                                                          sPasteNodeName, bClipboard, exInfo);
      // if no error reset sPasteNodeName and sPasteNodeDpName
        if(dynlen(exInfo) <= 0) {
          sPasteNodeName = "";
          sPasteNodeDpName = "";
          bDone = true;
        }
      }
      else
        fwException_raise(exInfo, "ERROR", getCatStr("unPanel", "WRONGPASTENODE")+sPasteNodeDpName, "");
      break;
    case UN_TREE_unLinkToDevice:
      unTree_unLinkToDevice(sGraphTree, sDpName, sSelectedKey, exInfo);
      bDone = true;

      break;
    case UN_TREE_changeLinkedDevice:
      unTree_changedLinkedDevice(sDpName ,bDone, exInfo);
      break;
    case unTreeWidget_popupMenu_rename:
      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Rename Node", makeDynString("$sTextQuestion:New name:", "$currentName:"+sDisplayNodeName), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
        if(dfFloat[1] == 1) {
          if(dsText[1]!= "") {
            unTreeWidget_rename(sGraphTree, sSelectedKey, sParentKey, sCompositeLabel, 
                            sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, bClipboard, 
                            dsText[1], sCurrentSelectedNodeKey, sParentKeyToAddInNewKey, exInfo);
            bDone = true;
          }
        }
      }
      break;
    case unTreeWidget_popupMenu_reorder: 
      bDone = true;
      unTreeWidget_reorder(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sParentDpName, 
                                                        sParentNodeName, bClipboard, exInfo);
      break;
    default:
      break;
  }
}
// unTree_changedLinkedDevice
/**
Purpose:
Function changes device linked to node

  @param sDpName: string, input: data point name
  @param bDone: bool, output: action executed
  @param exceptionInfo: dyn_string, output: the errors are returned here


Usage: Public

PVSS manager usage: UI (WCCOAui)
*/
unTree_changedLinkedDevice( string sDpName, bool &bDone, dyn_string &exInfo) {
  string sPanelPath, sDevice, sType;
  int iStatus;
  string sNodename = _fwTree_getNodeName(sDpName);
  // set the sDpName device
  sType = "_UnPanel";
  iStatus = panelSelector(sPanelPath);
  if (iStatus != 0) {
      DebugN("---  ERROR ---");
      exInfo[1] = "Problem with the file: ";
      exInfo[2] = "sPanelPath";
      DebugN(exInfo);
      fwExceptionHandling_display(exInfo);
      bDone = false;
      return;
  }
  if (sPanelPath == "") {
    bDone = false;
    return;
  }  
  sDevice = getSystemName()+unPanel_convertPanelFileNameToDp(sPanelPath);
  fwTree_setNodeDevice(sNodename, sDevice, sType, exInfo);
  if (dynlen(exInfo)>0) {
      DebugN("---  ERROR ---");
      DebugN(exInfo);
      fwExceptionHandling_display(exInfo);
      bDone = false;
      return;
  }
  bDone = true;
// reset the user data
  fwTree_setNodeUserData(sNodename, makeDynString(), exInfo); 
}

// unTree_showNodeContent
/**
Purpose:
Function called during the initialisation of the unTreeWidget. This function gets all the node the requested dp and adds them into the 
unTreeWidget or add the dp into the unTreeWidget.

  @param sGraphTree: string, input:  the name of the unTreeWidget graphical element
  @param sDpHierarchyPattern: string, input: the pattern of the dp to look for
  @param sDpHierarchyType: string, input: the DPT of sDpHierarchyPattern
  @param sContext: string, input: the context of sDpHierarchyPattern
  @param exceptionInfo: dyn_string, input: exception are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the unTreeWidget
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.
*/
unTree_showNodeContent(string sGraphTree, string sDisplayNodeName, string sNodeDpName, bool bShowNode, bool bClipboard, dyn_string &exceptionInfo)
{
  int i;
  int len;
  
  string sKey;
  string dpChild;
  string sChildNodeName;
  
  shape aShape = getShape(sGraphTree);
  
  dyn_string dsChildrenNodeName;

  // show the node
  if(bShowNode) 
  {
    // add topNode: class~key where key is |labelOfTheNode;dpNameOfTheNode
    sKey = unTreeWidget_compositeLabelSeparator + sDisplayNodeName + unTreeWidget_labelSeparator + sNodeDpName;
    unTreeWidget_addNode(sGraphTree, sNodeDpName, unTreeWidget_getIconClass(sNodeDpName) + unTreeWidget_classSeparator + sKey, true, exceptionInfo);
    
    // get children but don't show the children of the children
    unTreeWidget_showChildren(false, bClipboard, sGraphTree, sNodeDpName, sKey, sDisplayNodeName, exceptionInfo);
  }
  else 
  {
    // get the children and show them
    if(bClipboard)
    {
      fwTree_getChildrenWithClipboard(_fwTree_getNodeName(sNodeDpName), dsChildrenNodeName, exceptionInfo);
    }
    else
    {
      fwTree_getChildren(_fwTree_getNodeName(sNodeDpName), dsChildrenNodeName, exceptionInfo);
    }
    
    len = dynlen(dsChildrenNodeName);
    
    for(i = 1; i <= len ; i++) 
    {
      // build the child key, the key is: contextOfTheParent|compositeChildLabel@parentKey
      // contextOfTheParent = parentNodeName;dpParent
      // compositeChildLabel = childrenNodeName;dpChildren

      // show the current position
      unTreeWidget_showText(sDisplayNodeName, i + "/" + len);

      dpChild = _fwTree_makeNodeName(dsChildrenNodeName[i]);
      sChildNodeName = fwTree_getNodeDisplayName(dsChildrenNodeName[i], exceptionInfo);
      
      // e.g.  sClipBoardKey = clipboard;dpCliboard|sTree;dpTree@|sTree;dpTree

      sKey = 
          sDisplayNodeName + unTreeWidget_labelSeparator + 
          sNodeDpName + unTreeWidget_compositeLabelSeparator+
          sChildNodeName + unTreeWidget_labelSeparator +
          dpChild + unTreeWidget_keySeparator + 
          unTreeWidget_compositeLabelSeparator + 
          sDisplayNodeName + unTreeWidget_labelSeparator + 
          sNodeDpName;
      
      // Actions differ according to what tree is to be displayed
      switch (sNodeDpName)
      {
        case "fwTN_WindowTree": // WindowTree
        case "fwTN_TrendTree": // TrendTree (behaviour is the same as WindowTree)
        {
          if (g_unicosHMI_bApplicationFilterInUse)
          {
            synchronized(g_unicosHMI_dsApplicationList)
            {
              // How it works:
              // If the node name is an application name
              // -- If it is a visible application, it is displayed
              // -- else it is hidden
              // If it is not an application name, display it no matter what it is
              
              // Build a list of applications
              dyn_string dsAllApplicationList = g_dsTreeApplication;
              
              // Remove "*" and "List..."
              dynRemove(dsAllApplicationList, dynlen(dsAllApplicationList));
              dynRemove(dsAllApplicationList, 1);
              
              if (
                  (dynContains(dsAllApplicationList, sChildNodeName) > 0) && 
                  !(("fwTN_WindowTree" == sNodeDpName) && ("---ClipboardWindowTree---" == sChildNodeName)) &&
                  !(("---ClipboardTrendTree---" == sChildNodeName) && ("Predefined_Trends" == sChildNodeName) && ("User_Defined_Trends" == sChildNodeName))
                )
              {
                if (dynContains(g_unicosHMI_dsApplicationList, sChildNodeName) > 0)
                {
                  unTreeWidget_addNode(
                    sGraphTree, 
                    dpChild, 
                    unTreeWidget_getIconClass(dpChild) + unTreeWidget_classSeparator + sKey, 
                    true, 
                    exceptionInfo
                  );

                  // get children but and show the children of the children       
                  unTreeWidget_showChildren(true, bClipboard, sGraphTree, dpChild, sKey, sChildNodeName, exceptionInfo);
                }
                // Else ignore
              }
              else 
              {
                // Else display it if it not in the forced hidden application list
                if(dynContains(g_unicosHMI_dsTreeHiddenNames, sChildNodeName) < 1)
                {
                  unTreeWidget_addNode(
                      sGraphTree, 
                      dpChild, 
                      unTreeWidget_getIconClass(dpChild) + unTreeWidget_classSeparator + sKey, 
                      true, 
                      exceptionInfo
                    );

                  // get children but and show the children of the children       
                  unTreeWidget_showChildren(true, bClipboard, sGraphTree, dpChild, sKey, sChildNodeName, exceptionInfo);
                }
              }
            }
          }
          else
          {
            unTreeWidget_addNode(
                sGraphTree, 
                dpChild, 
                unTreeWidget_getIconClass(dpChild) + unTreeWidget_classSeparator + sKey, 
                true, 
                exceptionInfo
              );

            // get children but and show the children of the children       
            unTreeWidget_showChildren(true, bClipboard, sGraphTree, dpChild, sKey, sChildNodeName, exceptionInfo);
            
          }
          break;
        }          
        default: // Any other: proceed normally
        {
          unTreeWidget_addNode(
              sGraphTree, 
              dpChild, 
              unTreeWidget_getIconClass(dpChild) + unTreeWidget_classSeparator + sKey, 
              true, 
              exceptionInfo
            );

          // get children but and show the children of the children       
          unTreeWidget_showChildren(true, bClipboard, sGraphTree, dpChild, sKey, sChildNodeName, exceptionInfo);
          
          break;
        }
      }
    }
  }
  
  unTreeWidget_hideText();
}

// unTree_OpenPanel
/**
Purpose:
Open a configuration panel as a new symbol into the tree configuration panel

Parameters:
  @param panel: string, input: panel to open
  @param dsDollarParameters: dyn_string, input: the dollar paramters
  @param bPanelOpened: boolean, output: true if panel opened, false if not
  @param bChildPanel: boolean, input: true open as child panel if any, false add it as symbol
  @param exceptionInfo: dyn_string, output: for errors

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.8 
  . operating system: WXP & Linux.
  . distributed system: yes.
*/
unTree_OpenPanel(string panel, dyn_string dsDollarParameters, bool &bPanelOpened, bool bChildPanel, dyn_string &exceptionInfo)
{
  int xPos, yPos;
  bool isRemovable = TRUE;
  dyn_float dfFloat;
  dyn_string dsText;
  string sFunctionName, sTopDisplayName;

  if(!bChildPanel) 
  {
    unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
    
    unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_OPENPANELPREEXECUTE, sFunctionName);
        
    if(isFunctionDefined(sFunctionName)) 
    {
      execScript(
          "main()" + 
          "{" +
          "    " + sFunctionName + "();" +
          "}", 
          makeDynString()
        );
    }

    isRemovable = unTree_isRemovable("userPanelProof");

    if(isRemovable && unTree_removeUserPanel())
    {
      removeSymbol(myModuleName(), myPanelName(), "userPanel");
    }
    
    if ((getPath(PANELS_REL_PATH,panel) != "") && (panel != "")) 
    {
      getValue("userPanel_position", "position", xPos, yPos);

	  if(isRemovable)
	  {
		  addSymbol(myModuleName(), myPanelName(), panel, "userPanel", dsDollarParameters, xPos, yPos, 0, 1, 1);
		  if(shapeExists("reloadBlocked"))
		  {
			reloadBlocked.text = FALSE;
		  }
	  }
    else
    {
      if(shapeExists("tree") && shapeExists("reloadBlocked"))
      {
        reloadBlocked.text = TRUE;
        tree.setSelectedItem(g_sOldDpNameKey.text, TRUE);        
      }
    }
      
      bPanelOpened = true;
    }
    else 
    {
      fwException_raise (exceptionInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL")+ panel, "");
      bPanelOpened = false;
    }
  }
  else 
  {
    unGraphicalFrame_ChildPanelOnCentralModalReturn(panel, "Detail panel", dsDollarParameters, dfFloat, dsText);
  }
}

// unTree_removeUserPanel
/**
Purpose:
Check if any graphical shape exists

Parameters:
  @param return value: bool, output: true=there is shapes/false=there is no shapes

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.8 
  . operating system: WXP & Linux.
  . distributed system: yes.
*/
bool unTree_removeUserPanel()
{
  return (shapeExists("nodeList") || shapeExists("TextFieldName") || shapeExists("TextFieldPageName") || shapeExists("userPanelProof"));
}

// unTree_isRemovable
/**
Purpose:
Check if provided shape is allowed to be removed

Parameters:
  @param sShapeName: string, input: the name of the shape to be checked
  @param return value: bool, output: shape removable or not

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.8 
  . operating system: WXP & Linux.
  . distributed system: yes.
*/
bool unTree_isRemovable(string sShapeName)
{
  dyn_float fReturnValue;
  bool isRemovable = TRUE; 
  dyn_string sClose = "";
  string infoText = "Change selection without saving changes?";
  
  if(shapeExists(sShapeName))
  {
    if(unTree_isEdited(sShapeName))
  	{
  	  ChildPanelOnCentralModalReturn("vision/MessageWarning2", "Warning", makeDynString("$1:"+infoText,"$2:OK","$3:Cancel"), fReturnValue, sClose);   
    	
  	  if(sClose[1] == "false")
      {
  	   	isRemovable = FALSE;
  	  } 		
  	}
  }
  
  return isRemovable;
}

// unTree_isEdited
/**
Purpose:
Check if provided shape is edited

Parameters:
  @param sShapeName: string, input: the name of the shape to be checked
  @param return value: bool, output: shape edited or not

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.8 
  . operating system: WXP & Linux.
  . distributed system: yes.
*/
bool unTree_isEdited(string sShapeName)
{
  bool bEdited = FALSE;
  
  if(shapeExists(sShapeName) && shapeExists("settingsEdited")) 
  {  
  	 bEdited = settingsEdited.text();  
  } 
 
  return bEdited;
}

// unTree_getAndShow
/**
Purpose:
get the panel from the device model and show it.

  @param sDpName: string, input: the node data point name
  @param sParentDpName: string, input: the data point name of the parent node
  @param bPanelOpened: boolean, output: true if panel opened, false if not
  @param bDetail: boolean, input: true open the detail panel if any, false open normal one to show children
  @param exInfo: dyn_string, output: exception are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the unTreeWidget 
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.
*/
unTree_getAndShow(string sDpName, string sParentDpName, bool &bUserPanelOpened, bool bDetail, dyn_string &exInfo)
{  
  dyn_string dsPanelList, dsChildren, exceptionInfo, dsDollar;
  string sNodeName = _fwTree_getNodeName(sDpName), sDeviceName, sDeviceType, sTopDisplayName, sFunctionName;
  
  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exceptionInfo);
  fwTree_getChildren(sNodeName, dsChildren, exceptionInfo);

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);

  // get the panels
  unTree_getDeviceTypePanel(sTopDisplayName,  sDeviceType,  dsPanelList); 

  // get the dollar:
  unTree_getConfigurationVariable(sTopDisplayName, UN_TREE_OPENPANELGETDOLLARPARAM, sFunctionName);

  if(dynlen(dsPanelList) >= 1) 
  {
    
    switch(sDeviceType) 
    {
      case NO_DEVICE:
      {
        
        if(!bDetail) 
        {
          
          // get the dollar:
          if(isFunctionDefined(sFunctionName)) 
          {
            evalScript(
                dsDollar, 
                "dyn_string main(string sPanel, string sDpName, string sDeviceName, string sDeviceType, string sParentDpName, bool bDetail)" +
                "{" +
                "    return " + sFunctionName + "(sPanel, sDpName, sDeviceName, sDeviceType, sParentDpName, bDetail);" +
                "}", 
                makeDynString(), 
                dsPanelList[1], 
                sDpName, 
                sDeviceName, 
                sDeviceType, 
                sParentDpName, 
                bDetail
              );
          }

          unTree_OpenPanel(dsPanelList[1], dsDollar, bUserPanelOpened, false, exInfo);
        }
        break;
      }
      default:
      {
        if(dpExists(sDeviceName)) 
        {
          if(bDetail) 
          {
            // get the dollar:
            if(isFunctionDefined(sFunctionName)) 
            {
              evalScript(
                  dsDollar, 
                  "dyn_string main(string sPanel, string sDpName, string sDeviceName, string sDeviceType, string sParentDpName, bool bDetail)" +
                  "{" +
                  "    return " + sFunctionName + "(sPanel, sDpName, sDeviceName, sDeviceType, sParentDpName, bDetail);" +
                  "}", 
                  makeDynString(), 
                  dsPanelList[2], 
                  sDpName, 
                  sDeviceName, 
                  sDeviceType, 
                  sParentDpName, 
                  bDetail
                );
            }

            unTree_OpenPanel(dsPanelList[2], dsDollar, bUserPanelOpened, true, exInfo);
          }
          else 
          {
            if(dynlen(dsChildren) > 0) 
            {
              // get the dollar:
              if(isFunctionDefined(sFunctionName)) 
              {
                evalScript(
                    dsDollar, 
                    "dyn_string main(string sPanel, string sDpName, string sDeviceName, string sDeviceType, string sParentDpName, bool bDetail)" +
                    "{" +
                    "    return " + sFunctionName + "(sPanel, sDpName, sDeviceName, sDeviceType, sParentDpName, bDetail);" +
                    "}", 
                    makeDynString(), 
                    dsPanelList[1], 
                    sDpName, 
                    sDeviceName, 
                    sDeviceType, 
                    sParentDpName, 
                    bDetail
                  );
              }

              unTree_OpenPanel(dsPanelList[1], dsDollar, bUserPanelOpened, false, exInfo);
            }
            else 
            {
              // get the dollar:
              if(isFunctionDefined(sFunctionName)) 
              {
                evalScript(
                    dsDollar, 
                    "dyn_string main(string sPanel, string sDpName, string sDeviceName, string sDeviceType, string sParentDpName, bool bDetail)" +
                    "{" +
                    "    return " + sFunctionName + "(sPanel, sDpName, sDeviceName, sDeviceType, sParentDpName, bDetail);" +
                    "}", 
                    makeDynString(), 
                    dsPanelList[2], 
                    sDpName, 
                    sDeviceName, 
                    sDeviceType, 
                    sParentDpName, 
                    bDetail
                  );
              }

              unTree_OpenPanel(dsPanelList[2], dsDollar, bUserPanelOpened, false, exInfo);
            }
          }
        }
        break;
      }
    }
  }
  else
  {
    bUserPanelOpened = false;
  }
}

// unTree_linkToDevice
/**
Purpose:
Basic unTree function: link a node to a device

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sDpName: string, input: data point name
  @param sDpNameKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDeviceName: string, input: the device data point name
  @param sDeviceType: string, input: the device data point type
  @param dsUserData: dyn_string, input: the device user data
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_linkToDevice(string sGraphTree, string sDpName, string sDpNameKey, string sDeviceName, string sDeviceType, dyn_string dsUserData, dyn_string &exInfo)
{
  shape aShape = getShape(sGraphTree);
  string sNodename = _fwTree_getNodeName(sDpName);
  
//DebugN("unTree_linkToDevice", sDpName, sDpNameKey, sDeviceName, sDeviceType, dsUserData);

// set the device name and type
  fwTree_setNodeDevice(sNodename, sDeviceName, sDeviceType, exInfo);

// set the user data
  fwTree_setNodeUserData(sNodename, dsUserData, exInfo);
//DebugN("unTree_linkToDevice", exInfo, unTreeWidget_getIconClass(sDpName), sDpName, sNodename);
// get the new iconClass and set it to the tree
  if(dynlen(exInfo) <= 0) 
    unTreeWidget_setPropertyValue(aShape, "setTagAndImage", sDpNameKey+unTreeWidget_classSeparator+unTreeWidget_getIconClass(sDpName), true);
}

// unTree_unLinkToDevice
/**
Purpose:
Basic unTree function: unlink a node to a panel

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sDpName: string, input: data point name
  @param sDpNameKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_unLinkToDevice(string sGraphTree, string sDpName, string sDpNameKey, dyn_string &exInfo)
{
  shape aShape = getShape(sGraphTree);
  string sNodename = _fwTree_getNodeName(sDpName);
  
// set the sDpName device
  fwTree_setNodeDevice(sNodename, "", "", exInfo);
//  dpSet(sDpName+".device","", sDpName+".type","");

// reset the user data
  fwTree_setNodeUserData(sNodename, makeDynString(), exInfo);

// get the new iconClass and set it to the tree
  if(dynlen(exInfo) <= 0) 
    unTreeWidget_setPropertyValue(aShape, "setTagAndImage", sDpNameKey+unTreeWidget_classSeparator+unTreeWidget_getIconClass(sDpName), true);
}

// unTree_triggerEventRigthClick
/**
Purpose:
trigger the eventRigthClick callback

  @param iCommand: int, input:  the command to send
  @param dsArgs: dyn_string, input: the arguments
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTree_triggerEventRigthClick(int iCommand, dyn_string dsArgs, dyn_string &exceptionInfo) 
{
  string sDpName = unTree_getInternalTriggerName();

  if(dpExists(sDpName) && globalExists(sDpName)) {
    dpSet(sDpName+".eventRigthClick.command", iCommand);
    execScript("main(int iCommand, dyn_string dsArgs)"+
               "{"+
               "  "+sDpName+"=dsArgs;"+
               "  dynInsertAt("+sDpName+", iCommand, 1);"+
               "}",
               makeDynString(), iCommand, dsArgs);
  }
  else 
    fwException_raise(exceptionInfo, "ERROR", "INTERNAL TREE DP does not exist", "");
  
}

// unTree_triggerEventNodeRigthClick
/**
Purpose:
trigger the eventNodeRigthClick callback

  @param iCommand: int, input:  the command to send
  @param dsArgs: dyn_string, input: the arguments
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTree_triggerEventNodeRigthClick(int iCommand, dyn_string dsArgs, dyn_string &exceptionInfo) 
{
  string sDpName = unTree_getInternalTriggerName();

  if(dpExists(sDpName) && globalExists(sDpName)) {
    dpSet(sDpName+".eventNodeRigthClick.command", iCommand);
    execScript("main(int iCommand, dyn_string dsArgs)"+
               "{"+
               "  "+sDpName+"=dsArgs;"+
               "  dynInsertAt("+sDpName+", iCommand, 1);"+
               "}",
               makeDynString(), iCommand, dsArgs);
  }
  else 
    fwException_raise(exceptionInfo, "ERROR", "INTERNAL TREE DP does not exist", "");
}

// unTree_reorder
/**
Purpose:
Basic unTree function: reorder the children of the node

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sParentDpName: string, input: data point name of the parent
  @param sParentNodeName: string, input: the node name of sParentDpName
  @param bClipboard: bool, input: show clipboard
  @param bTop: bool, input: true, issue from top node, false issue from other node.
  @param bDone: bool, output: true, if reordered.
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_reorder(string sGraphTree, string sBar, string sSelectedKey, string sDpName, 
          string sDisplayNodeName, string sParentDpName, string sParentNodeName, bool bClipboard, 
          bool bTop, bool &bDone, dyn_string &exceptionInfo)
{
  dyn_string ret, children, dsDisplayNodeName, dsNodeDpName;
  dyn_float res;
  string sCorrectNodeName=_fwTree_getNodeName(sDpName);
  shape aShape;
  dyn_bool dbShow;
  int pos;
  
//DebugN("unTree_reorder", sDisplayNodeName, sCorrectNodeName, sDpName);

  fwTree_getChildren(sCorrectNodeName, children, exceptionInfo);
  unGraphicalFrame_ChildPanelOnReturn("fwTreeDisplay/fwTreeReorderNodeStd.pnl","Re-order Children",
    makeDynString(sDisplayNodeName),
    100,60, res, ret);
  if(dynlen(res)>0)
  {
    if(res[1] == 1) {
      if(children != ret)
      {
        bDone = true;
        aShape = getShape(sGraphTree);
      // reorder the children
        fwTree_reorderChildren(sCorrectNodeName, ret, exceptionInfo);
      // add the children.
  
        if(bTop) {
  // clear tree and initialize it again
  // set the show clipboard to true
            unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);

            unTree_setPasteGlobalValue("", "");
        }
        else {
  //normal re-order
  // remove the children from the unTreeWidget
            unTreeWidget_setPropertyValue(aShape, "deleteChildrenNode", sSelectedKey, true);
      
            unTreeWidget_showChildren(true, bClipboard, sGraphTree, sDpName, 
                                            sSelectedKey, sDisplayNodeName, exceptionInfo);
        }
      }
    }
  }
}

// unTree_add
/**
Purpose:
Basic Tree function: add a new node 

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sDisplayNodeNameToAdd: string, input: the node name to add
  @param sNodeNameAdded: string, input: the node name that was added
  @param bTop: bool, input: true, issue from top node, false issue from other node.
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_add(string sGraphTree, string sBar, string sSelectedKey, string sDpName, string sDisplayNodeName, 
                  string sDisplayNodeToAdd, bool bTop, string &sNodeNameAdded, dyn_string &exceptionInfo)
{
  dyn_string dsText, dsDisplayNodeName, dsNodeDpName;
  dyn_float dfFloat;
  string dpChild, sChildKey;
  int pos;
  dyn_bool dbShow;
  shape aShape;
  string sNodeName = _fwTree_getNodeName(sDpName);

//DebugN("unTree_add", sDisplayNodeName, sNodeName, sDpName, exceptionInfo);
  if(bTop) {
    pos = strpos(sDisplayNodeToAdd, unTreeWidget_CLIPBOARD);
    if(pos >= 0) {
        fwException_raise(exceptionInfo, "ERROR", getCatStr("unTreeWidget", "CLIPBOARDNODE")+sDisplayNodeToAdd, "");
    }
    else {
      unTreeWidget_checkNodeName(sDisplayNodeToAdd, exceptionInfo);
      if(dynlen(exceptionInfo) <= 0) {
    // make the node and add it in the tree
        sNodeNameAdded = fwTree_createNode(sNodeName, sDisplayNodeToAdd, exceptionInfo);
        if(dynlen(exceptionInfo) <= 0) {

// make the link to the panel if needed

          unTree_setPasteGlobalValue("", "");

          dpChild = _fwTree_makeNodeName(sNodeNameAdded);

          sChildKey = sDisplayNodeName+unTreeWidget_labelSeparator+sDpName+
                unTreeWidget_compositeLabelSeparator+
                sDisplayNodeToAdd+unTreeWidget_labelSeparator+dpChild+
                unTreeWidget_keySeparator+unTreeWidget_compositeLabelSeparator+sDisplayNodeName+unTreeWidget_labelSeparator+sDpName;
          unTreeWidget_addNode(sGraphTree, dpChild, unTreeWidget_getIconClass(dpChild)+unTreeWidget_classSeparator+sChildKey, true, exceptionInfo);
        }
      }
    }
  }
  else {
    unTreeWidget_add(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sDisplayNodeToAdd, sNodeNameAdded, exceptionInfo);
  }
}

// unTree_paste
/**
Purpose:
Basic Tree function: paste the node that was copied/cut into the clipboard the node, the node name

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sDpNameToPaste: string, input: data point name of the node to paste
  @param sDisplayNodeNameToPaste: string, input: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param bTop: bool, input: true, issue from top node, false issue from other node.
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_paste(string sGraphTree, string sBar, string sSelectedKey, string sDpName, 
              string sDisplayNodeName, string sDpNameToPaste, string sDisplayNodeNameToPaste, bool bClipboard, bool bTop, dyn_string &exceptionInfo)
{
// paste the node that was cut and saved in sPasteNodeName
  shape aShape=getShape(sGraphTree);
  string clipboard, sNodeKey, sNodeToPaste=_fwTree_getNodeName(sDpNameToPaste);
  string sTree, sClipBoardKey, sClipboardDp, sTreeDp;
  
  if(dpExists(sDpNameToPaste)) {
    if(bTop) {
      if(bClipboard) {
    // make the node key
    // get the tree
        sTreeDp = _fwTree_getTree(sDpNameToPaste);
        sTree = _fwTree_getNodeName(sTreeDp);
      }
    // cut the node from the clipboard and paste it in the node
    // refresh the clipboard and the parent node
    //DebugN("unTreeWidget_paste", sDisplayNodeName, _fwTree_getNodeName(sDpName), sDisplayNodeNameToPaste, sNodeToPaste, sDpNameToPaste, bClipboard);
      fwTree_pasteNode(_fwTree_getNodeName(sDpName), sNodeToPaste, exceptionInfo);
    //DebugN(exceptionInfo);
      exceptionInfo = makeDynString();
  //    if(dynlen(exceptionInfo) <=0) {
    // no error
        if(bClipboard) {
      // get the clipboard of the node
          clipboard = _fwTree_makeClipboard(sTree);
          sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;
      // delete all the children of the clipboard 
      //    sClipBoardKey = clipboard;dpCliboard|sTree;dpTree@|sTree;dpTree
          sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;
          sClipBoardKey = sTree+unTreeWidget_labelSeparator+sTreeDp+
                            unTreeWidget_compositeLabelSeparator+
                            clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                            unTreeWidget_keySeparator+
                            unTreeWidget_compositeLabelSeparator+
                            sTree+unTreeWidget_labelSeparator+sTreeDp;
          sNodeKey = clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                            unTreeWidget_compositeLabelSeparator+
                            sDisplayNodeNameToPaste+unTreeWidget_labelSeparator+sDpNameToPaste+
                            unTreeWidget_keySeparator+
                            sClipBoardKey;
    // remove the node from clipboard in the unTreeWidget
          unTreeWidget_setPropertyValue(aShape, "deleteNode", sNodeKey, true);
  //DebugN("pasteNode", sTreeDp, sTree, sClipboardDp, clipboard);
  //DebugN(sNodeKey);
        }
  
        unTree_setPasteGlobalValue("", "");

        sNodeKey = sDisplayNodeName+unTreeWidget_labelSeparator+sDpName+
                unTreeWidget_compositeLabelSeparator+
                sDisplayNodeNameToPaste+unTreeWidget_labelSeparator+sDpNameToPaste+
                unTreeWidget_keySeparator+unTreeWidget_compositeLabelSeparator+sDisplayNodeName+unTreeWidget_labelSeparator+sDpName;
        unTreeWidget_addNode(sGraphTree, sDpNameToPaste, unTreeWidget_getIconClass(sDpNameToPaste)+unTreeWidget_classSeparator+sNodeKey, true, exceptionInfo);

// get children but and show the children of the children 
        unTreeWidget_showChildren(true, bClipboard, sGraphTree, sDpNameToPaste, sNodeKey, sDisplayNodeNameToPaste, exceptionInfo);
  //    }
    }
    else {
        unTreeWidget_paste(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sDpNameToPaste, 
                                            sDisplayNodeNameToPaste, bClipboard, exceptionInfo);
    }
  }
  else {
//DebugN("ERROR", getCatStr("unPanel", "WRONGPASTENODE")+sDpNameToPaste);
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "WRONGPASTENODE")+sDpNameToPaste, "");
  }
}

// unTree_setGlobalValue
/**
Purpose:
Set the graphical global value used by the cascade buttons.

  @param sDpName: string, input: the data point name of the current selected node
  @param sDpNameKey: string, input: the key of the current selected node
  @param sParentDpName: string, input: the data point name of the parent
  @param sParentDpNameKey: string, input: the key of the parent
  @param sDeviceName: string, input: the device associated to the current node
  @param sDeviceType: string, input: the device type
  @param sCompositeLabel: string, input: the composite label
  @param sCurrentSelectedNodeKey: string, input: the key of the last node that was selected

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setGlobalValue(string sDpName, string sDpNameKey, string sParentDpName, 
                            string sParentDpNameKey, string sDeviceName, string sDeviceType,
                            string sCompositeLabel, string sCurrentSelectedNodeKey)
{
  g_sDpName.text = sDpName;
  g_sOldDpNameKey.text = g_sDpNameKey.text;
  g_sDpNameKey.text = sDpNameKey;
  g_sParentDpName.text = sParentDpName;
  g_sParentDpNameKey.text = sParentDpNameKey;
  g_sDeviceName.text = sDeviceName;
  g_sDeviceType.text = sDeviceType;
  g_sCompositeLabel.text = sCompositeLabel;
  g_sCurrentSelectedNodeKey.text = sCurrentSelectedNodeKey;
}

// unTree_getGlobalValue
/**
Purpose:
Get the graphical global value used by the cascade buttons.

  @param sDpName: string, output: the data point name of the current selected node
  @param sDpNameKey: string, output: the key of the current selected node
  @param sParentDpName: string, output: the data point name of the parent
  @param sParentDpNameKey: string, output: the key of the parent
  @param sDeviceName: string, output: the device associated to the current node
  @param sDeviceType: string, output: the device type
  @param sCompositeLabel: string, output: the composite label
  @param sCurrentSelectedNodeKey: string, output: the key of the last node that was selected

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getGlobalValue(string &sDpName, string &sDpNameKey, string &sParentDpName, 
                            string &sParentDpNameKey, string &sDeviceName, string &sDeviceType,
                            string &sCompositeLabel, string &sCurrentSelectedNodeKey)
{
  sDpName = g_sDpName.text;
  sDpNameKey = g_sDpNameKey.text;
  sParentDpName = g_sParentDpName.text;
  sParentDpNameKey = g_sParentDpNameKey.text;
  sDeviceName = g_sDeviceName.text;
  sDeviceType = g_sDeviceType.text;
  sCompositeLabel = g_sCompositeLabel.text;
  sCurrentSelectedNodeKey = g_sCurrentSelectedNodeKey.text;
}

// unTree_setPasteGlobalValue
/**
Purpose:
Set the graphical global value used by the cascade buttons to keep if there are node to paste.

  @param sNodeNameToPaste: string, input: the node name to paste
  @param sNodeDpNameToPaste: string, input: the node data point name to paste

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setPasteGlobalValue(string sNodeNameToPaste, string sNodeDpNameToPaste)
{
  g_sNodeNameToPaste.text = sNodeNameToPaste;
  g_sNodeDpNameToPaste.text = sNodeDpNameToPaste;
}

// unTree_getPasteGlobalValue
/**
Purpose:
Get the graphical global value used by the cascade buttons to keep if there are node to paste.

  @param sNodeNameToPaste: string, output: the node name to paste
  @param sNodeDpNameToPaste: string, output: the node data point name to paste

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getPasteGlobalValue(string &sNodeNameToPaste, string &sNodeDpNameToPaste)
{
  sNodeNameToPaste = g_sNodeNameToPaste.text;
  sNodeDpNameToPaste = g_sNodeDpNameToPaste.text;
}

// unTree_setDeviceButtonGlobalVariable
/**
Purpose:
Set the state of the panel cascade button and the global variables.

  @param bNavigation: bool, input: state of the navigation panel button
  @param bLink: bool, input: state of the link panel button
  @param bUnlink: bool, input: state of the unlink panel button
  @param bConfiguration: bool, input: state of the configuration device button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setDeviceButtonGlobalVariable(bool bNavigation, bool bLink, bool bUnlink, bool bConfiguration)
{
  g_horizontal.text = bNavigation;
  g_link.text = bLink;
  g_unlink.text = bUnlink;
  g_configuration.text = bConfiguration;
}

// unTree_setDeviceButtonState
/**
Purpose:
Set the state of the panel cascade button but not the global variables.

  @param bNavigation: bool, input: state of the navigation panel button
  @param bLink: bool, input: state of the link panel button
  @param bUnlink: bool, input: state of the unlink panel button
  @param bConfiguration: bool, input: state of the configuration device button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setDeviceButtonState(bool bNavigation, bool bLink, bool bUnlink, bool bConfiguration)
{
  bool bState;
  
  bState = bNavigation;
  deviceCascade.enableItem("2", bState);

  bState = bLink;
  deviceCascade.enableItem("3", bState);

  bState = bUnlink;
  deviceCascade.enableItem("4", bState);
}

// unTree_getPanelButton
/**
Purpose:
Get the state of the panel cascade button.

  @param bNavigation: bool, output: state of the navigation panel button
  @param bLink: bool, output: state of the link panel button
  @param bUnlink: bool, output: state of the unlink panel button
  @param bConfiguration: bool, output: state of the device configuration button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getPanelButton(bool &bNavigation, bool &bLink, bool &bUnlink, bool &bConfiguration)
{
  string sState;
  
  sState = g_horizontal.text;
  if((sState == "true") || (sState == "TRUE"))
    bNavigation = true;
  else
    bNavigation = false;

  sState = g_link.text;
  if((sState == "true") || (sState == "TRUE"))
    bLink = true;
  else
    bLink = false;

  sState = g_unlink.text;
  if((sState == "true") || (sState == "TRUE"))
    bUnlink = true;
  else
    bUnlink = false;

  sState = g_configuration.text;
  if((sState == "true") || (sState == "TRUE"))
    bConfiguration = true;
  else
    bConfiguration = false;
}

// unTree_setHierarchyButtonGlobalVariable
/**
Purpose:
Set the global variables of the hierarchy cascade button.

  @param bAdd: bool, input: state of the add panel button
  @param bAddFolder: bool, input: state of the add folder button
  @param bAddPanel: bool, input: state of the add panel button
  @param bCut: bool, input: state of the cut button
  @param bCopy: bool, input: state of the copy button
  @param bPaste: bool, input: state of the paste button
  @param bRemove: bool, input: state of the remove button
  @param bRename: bool, input: state of the rename button
  @param bReorder: bool, input: state of the reorder button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyButtonGlobalVariable(bool bAdd, bool bAddFolder, bool bAddPanel, 
                                bool bCut, bool bCopy, bool bPaste,
                                bool bRemove, bool bRename, bool bReorder)
{
  g_add.text = bAdd;
  g_addFolder.text = bAddFolder;
  g_addPanel.text = bAddPanel;
  g_cut.text = bCut;
  g_copy.text = bCopy;
  g_paste.text = bPaste;
  g_remove.text = bRemove;
  g_rename.text = bRename;
  g_reorder.text = bReorder;
}

// unTree_setHierarchyButtonState
/**
Purpose:
Set the state of the hierarchy cascade button but not the global variables.

  @param bAdd: bool, input: state of the add panel button
  @param bAddFolder: bool, input: state of the add folder button
  @param bAddDevice: bool, input: state of the add device button
  @param bCut: bool, input: state of the cut button
  @param bCopy: bool, input: state of the copy button
  @param bPaste: bool, input: state of the paste button
  @param bRemove: bool, input: state of the remove button
  @param bRename: bool, input: state of the rename button
  @param bReorder: bool, input: state of the reorder button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyButtonState(bool bAdd, bool bAddFolder, bool bAddDevice, 
                                bool bCut, bool bCopy, bool bPaste,
                                bool bRemove, bool bRename, bool bReorder)
{
  hierarchyCascade.enableItem("2", bAddFolder); //new folder
  hierarchyCascade.enableItem("3", bAddDevice); // new device
  hierarchyCascade.enableItem("4", bAdd);        // new
  hierarchyCascade.enableItem("6", bAddDevice);  // add device
  hierarchyCascade.enableItem("7", bAdd);        // add

  hierarchyCascade.enableItem("9", bCut);
  hierarchyCascade.enableItem("10", bCopy);
  hierarchyCascade.enableItem("11", bPaste);

  hierarchyCascade.enableItem("13", bRemove);
  hierarchyCascade.enableItem("14", bRename);
  hierarchyCascade.enableItem("15", bReorder);
}

// unTree_getHierarchyButton
/**
Purpose:
Get the state of the hierarchy cascade button.

  @param bAdd: bool, output: state of the add panel button
  @param bAddFolder: bool, output: state of the add folder button
  @param bAddPanel: bool, output: state of the add panel button
  @param bCut: bool, output: state of the cut button
  @param bCopy: bool, output: state of the copy button
  @param bPaste: bool, output: state of the paste button
  @param bRemove: bool, output: state of the remove button
  @param bRename: bool, output: state of the rename button
  @param bReorder: bool, output: state of the reorder button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getHierarchyButton(bool &bAdd, bool &bAddFolder, bool &bAddPanel, 
                                bool &bCut, bool &bCopy, bool &bPaste,
                                bool &bRemove, bool &bRename, bool &bReorder)
{
  string sState;
  
  sState = g_add.text;
  if((sState == "true") || (sState == "TRUE"))
    bAdd = true;
  else
    bAdd = false;

  sState = g_addFolder.text;
  if((sState == "true") || (sState == "TRUE"))
    bAddFolder = true;
  else
    bAddFolder = false;

  sState = g_addPanel.text;
  if((sState == "true") || (sState == "TRUE"))
    bAddPanel = true;
  else
    bAddPanel = false;

  sState = g_cut.text;
  if((sState == "true") || (sState == "TRUE"))
    bCut = true;
  else
    bCut = false;

  sState = g_copy.text;
  if((sState == "true") || (sState == "TRUE"))
    bCopy = true;
  else
    bCopy = false;

  sState = g_paste.text;
  if((sState == "true") || (sState == "TRUE"))
    bPaste = true;
  else
    bPaste = false;

  sState = g_remove.text;
  if((sState == "true") || (sState == "TRUE"))
    bRemove = true;
  else
    bRemove = false;

  sState = g_rename.text;
  if((sState == "true") || (sState == "TRUE"))
    bRename = true;
  else
    bRename = false;

  sState = g_reorder.text;
  if((sState == "true") || (sState == "TRUE"))
    bReorder = true;
  else
    bReorder = false;
}

// unTree_setHierarchyButtonText
/**
Purpose:
Set the text of the hierarchy cut/copy and paste button.

  @param sCut: string, input: text of the cut and copy hierarchy button
  @param sPaste: string, input: text of the paste hierarchy button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyButtonText(string sCut, string sPaste)
{
  if(sCut != "") {
    hierarchyCascade.textItem("9", "Cut: "+sCut);
    hierarchyCascade.textItem("10", "Copy: "+sCut);
  }
  else {
    hierarchyCascade.textItem("9", "Cut");
    hierarchyCascade.textItem("10", "Copy");
  }
  if(sPaste != "")
    hierarchyCascade.textItem("11", "Paste: "+sPaste);
  else
    hierarchyCascade.textItem("11", "Paste");
}

// unTree_setHierarchyPasteButtonGlobalVariable
/**
Purpose:
Set global variable of the paste hierarchy button.

  @param sPaste: string, input: text of the paste hierarchy button
  @param bPaste: bool, input: state of the paste button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyPasteButtonGlobalVariable(bool bPaste)
{
  g_paste.text = bPaste;
}

// unTree_setHierarchyPasteButtonState
/**
Purpose:
Set the text and the state of the paste hierarchy button.

  @param sPaste: string, input: text of the paste hierarchy button
  @param bPaste: bool, input: state of the paste button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyPasteButtonState(string sPaste, bool bPaste)
{
  if(sPaste != "")
    hierarchyCascade.textItem("11", "Paste: "+sPaste);
  else
    hierarchyCascade.textItem("11", "Paste");

  hierarchyCascade.enableItem("11", bPaste);
}

// unTree_setButtonFirstName
/**
Purpose:
Set the first name of the two cascade buttons.

  @param sText: string, input: the text to put

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setButtonFirstName(string sText)
{
  hierarchyCascade.textItem("0", sText);
  deviceCascade.textItem("0", sText);
}

// unTree_setUserPanelOpened
/**
Purpose:
Set the variable to keep the open state of the user panel.

  @param bUserPanelOpened: bool, input: true user panel opened, false user panel not opened

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setUserPanelOpened(bool bUserPanelOpened)
{
  g_bUserPanelOpened.text = bUserPanelOpened;
}

// unTree_getUserPanelOpened
/**
Purpose:
Get the variable to keep the open state of the user panel.

  @param bUserPanelOpened: bool, output: true user panel opened, false user panel not opened

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getUserPanelOpened(bool &bUserPanelOpened)
{
  bUserPanelOpened = g_bUserPanelOpened.text;
}

// unTree_isUserAllowed
/**
Purpose:
check if the user is allowed to configure the Tree.

  @param bUserAllowed: bool, output: true the user is allowed, false the user is not allowed
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_isUserAllowed(bool &bUserAllowed, dyn_string &exceptionInfo)
{
  bool granted = false;
  bool bFileExists;
  string sPanelFileName;
  string sAccess;
  
  sPanelFileName = g_sPanelFileName.text();
  
//DebugTN("start unTree_isUserAllowed", myPanelName(), myModuleName(), sPanelFileName);  
  sAccess = unGenericButtonFunctionsHMI_getPanelAccessLevel(sPanelFileName, bFileExists);
  unGenericButtonFunctionsHMI_isAccessAllowed(sPanelFileName, UN_GENERIC_USER_ACCESS, sAccess, granted, exceptionInfo);
  bUserAllowed = granted;
//DebugTN("end unTree_isUserAllowed", bUserAllowed, sAccess, bFileExists, myPanelName(), myModuleName(), sPanelFileName);  
}

// unTree_setNodeList
/**
Purpose:
set the variable to true when the command are triggered from the nodeList panel.

  @param b: bool, input: true/false, from node list/not fromnode list

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setNodeList(bool b)
{
  g_bNodeList.text = b;
}

// unTree_isFromNodeList
/**
Purpose:
true if the command are triggered from the nodeList panel

  @param b: bool, input: true/false, from node list/not from node list

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
bool unTree_isFromNodeList()
{
  if((g_bNodeList.text == "TRUE") || (g_bNodeList.text == "true"))
    return true;
  else
    return false;
}

// unTree_setHierarchyCascade
/**
Purpose:
set the title and text of the 3 first commands of the hierarchy cascade button

  @param sTitle: string, input: title of the cascade button
  @param sNewAddFolderText: string, input: text to be added to first new/add
  @param sNewAddDeviceText: string, input: text to be added to second new/add
  @param sNewAddText: string, input: text to be added to third new/add

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyCascade(string sTitle, string sNewAddFolderText, string sNewAddDeviceText, string sNewAddText)
{
  if(sTitle != "")
    hierarchyCascade.text = sTitle;

  if(sNewAddFolderText != "")
    hierarchyCascade.textItem("2", "New "+sNewAddFolderText);
  if(sNewAddDeviceText != "") {
    hierarchyCascade.textItem("3", "New "+sNewAddDeviceText);
    hierarchyCascade.textItem("6", "Add "+sNewAddDeviceText);
  }
  if(sNewAddText != "") {
    hierarchyCascade.textItem("4", "New "+sNewAddText);
    hierarchyCascade.textItem("7", "Add "+sNewAddText);
  }
}

// unTree_setDeviceCascade
/**
Purpose:
set the title and text of the commands of the hierarchy cascade button

  @param sTitle: string, input: title of the cascade button
  @param sText1: string, input: title of the first text of the cascade button
  @param sText2: string, input: title of the second text of the cascade button
  @param sText3: string, input: title of the third text of the cascade button
  @param sText4: string, input: title of the fourth text of the cascade button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setDeviceCascade(string sTitle, string sText1, string sText2, string sText3)
{
  if(sTitle != "")
    deviceCascade.text = sTitle;

  if(sText1 != "")
    deviceCascade.textItem("2", sText1);
  if(sText2 != "")
    deviceCascade.textItem("3", sText2);
  if(sText3 != "")
    deviceCascade.textItem("4", sText3);
}

// unTree_setHandleEventData
/**
Purpose:
save the temporary data.

  @param bDone: bool, input: boolean value
  @param sPasteNodeDpName: string, input: the dp name to pase
  @param sPasteNodeName: string, input: the node name of the dp

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@reviewed 2018-08-02 @whitelisted{FalsePositive}

*/
unTree_setHandleEventData(bool bDone, string sPasteNodeDpName, string sPasteNodeName)
{
  g_temp_bDone.text = bDone;
  g_temp_pasteNodeName.text = sPasteNodeName;
  g_temp_pasteNodeDpName.text = sPasteNodeDpName;
}

// unTree_setHandleEventData
/**
Purpose:
get the temporary data.

  @param bDone: bool, output: boolean value
  @param sPasteNodeDpName: string, output: the dp name to pase
  @param sPasteNodeName: string, output: the node name of the dp

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getHandleEventData(bool &bDone, string &sPasteNodeDpName, string &sPasteNodeName)
{
  if((g_temp_bDone.text == "TRUE") || (g_temp_bDone.text == "true"))
    bDone = true;
  else
    bDone = false;
  sPasteNodeName = g_temp_pasteNodeName.text;
  sPasteNodeDpName = g_temp_pasteNodeDpName.text;
}

// unTree_getConfigurationVariable
/**
Purpose:
get the configuration data.

  @param sTreeType: string, input: the type of tree
  @param iIdentifier: int, input: the identifier to retrieve
  @param sValue: string, output: the value

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getConfigurationVariable(string sTreeType, int iIdentifier, string &sValue)
{
  switch(iIdentifier) {
    case UN_TREE_TREENODENAME:
      sValue = g_tree_TreeNodeName.text;
      break;
    case UN_TREE_LOADTREEIMAGE:
      sValue = g_tree_loadTreeImage.text;
      break;
    case UN_TREE_EVENTRIGTHCLICKMENU:
      sValue = g_tree_EventRigthClickMenu.text;
      break;
    case UN_TREE_PRE_EVENTRIGTHCLICK:
      sValue = g_tree_preExecuteEventRigthClick.text;
      break;
    case UN_TREE_HANDLEEVENTRIGTHCLICK:
      sValue = g_tree_handleEventRigthClick.text;
      break;
    case UN_TREE_EVENTNODERIGTHCLICKMENU:
      sValue = g_tree_EventNodeRigthClickMenu.text;
      break;
    case UN_TREE_PRE_EVENTNODERIGTHCLICK:
      sValue = g_tree_preExecuteEventNodeRigthClick.text;
      break;
    case UN_TREE_HANDLEEVENTNODERIGTHCLICK:
      sValue = g_tree_handleEventNodeRigthClick.text;
      break;
    case UN_TREE_GETHIERARCHYSTATE:
      sValue = g_tree_getHierarchyState.text;
      break;
    case UN_TREE_GETDEVICESTATE:
      sValue = g_tree_getDeviceState.text;
      break;
    case UN_TREE_GETDISPLAYNODENAME:
      sValue = g_tree_getDisplayNodeName.text;
      break;
    case UN_TREE_NODELIST_LOADIMAGE:
      sValue = g_nodeList_loadImage.text;
      break;
    case UN_TREE_NODELIST_EVENTRIGTHCLICKMENU:
      sValue = g_nodeList_EventRigthClickMenu.text;
      break;
    case UN_TREE_NODELIST_EVENTNODERIGTHCLICKMENU:
      sValue = g_nodeList_EventNodeRigthClickMenu.text;
      break;
    case UN_TREE_CONFIGURATION_PANEL:
      sValue = g_tree_getConfigurationPanel.text;
      break;
    case UN_TREE_HANDLEEVENTNODECLICKOPERATION:
      sValue = g_tree_handleEventNodeClickOperation.text;
      break;
    case UN_TREE_NODELIST_EVENTDBLCLICKPOSTEXECUTE:
      sValue = g_nodeList_EventDblClickPostExecute.text;
      break;
    case UN_TREE_NODELIST_EVENTDBLCLICKPRETEXECUTE:
      sValue = g_nodeList_EventDblClickPreExecute.text;
      break;
    case UN_TREE_OPENPANELPREEXECUTE:
      sValue = g_nodeList_openPanelPreExecute.text;
      break;
    case UN_TREE_OPENPANELGETDOLLARPARAM:
      sValue = g_tree_openPanelGetDollarParam.text;
      break;
    case UN_TREE_POST_EVENTNODERIGTHCLICK:
      sValue = g_tree_postExecuteEventNodeRigthClick.text;
      break;
    case UN_TREE_POST_EVENTRIGTHCLICK:
      sValue = g_tree_postExecuteEventRigthClick.text;
      break;
    case UN_TREE_EVENTNODERIGTHCLICKOPERATIONMENU:
      sValue = g_tree_evtNodeRigthClickOperationMenu.text;
      break;
    case UN_TREE_HANDLEEVENTNODERIGTHCLICKOPERATION:
      sValue = g_tree_handleEvtNodeRigthClickOperation.text;
      break;
    default:
      sValue = "";
      break;
  }
}

// unTree_setConfigurationVariable
/**
Purpose:
set the configuration data.

  @param sTreeType: string, input: the type of tree
  @param iIdentifier: int, input: the identifier of the data
  @param sValue: string, input: the value

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setConfigurationVariable(string sTreeType, int iIdentifier, string sValue)
{
  switch(iIdentifier) {
    case UN_TREE_TREENODENAME:
      g_tree_TreeNodeName.text = sValue;
      break;
    case UN_TREE_LOADTREEIMAGE:
      g_tree_loadTreeImage.text = sValue;
      break;
    case UN_TREE_EVENTRIGTHCLICKMENU:
      g_tree_EventRigthClickMenu.text = sValue;
      break;
    case UN_TREE_PRE_EVENTRIGTHCLICK:
      g_tree_preExecuteEventRigthClick.text = sValue;
      break;
    case UN_TREE_HANDLEEVENTRIGTHCLICK:
      g_tree_handleEventRigthClick.text = sValue;
      break;
    case UN_TREE_EVENTNODERIGTHCLICKMENU:
      g_tree_EventNodeRigthClickMenu.text = sValue;
      break;
    case UN_TREE_PRE_EVENTNODERIGTHCLICK:
      g_tree_preExecuteEventNodeRigthClick.text = sValue;
      break;
    case UN_TREE_HANDLEEVENTNODERIGTHCLICK:
      g_tree_handleEventNodeRigthClick.text = sValue;
      break;
    case UN_TREE_GETHIERARCHYSTATE:
      g_tree_getHierarchyState.text = sValue;
      break;
    case UN_TREE_GETDEVICESTATE:
      g_tree_getDeviceState.text = sValue;
      break;
    case UN_TREE_GETDISPLAYNODENAME:
      g_tree_getDisplayNodeName.text = sValue;
      break;
    case UN_TREE_NODELIST_LOADIMAGE:
      g_nodeList_loadImage.text = sValue;
      break;
    case UN_TREE_NODELIST_EVENTRIGTHCLICKMENU:
      g_nodeList_EventRigthClickMenu.text = sValue;
      break;
    case UN_TREE_NODELIST_EVENTNODERIGTHCLICKMENU:
      g_nodeList_EventNodeRigthClickMenu.text = sValue;
      break;
    case UN_TREE_CONFIGURATION_PANEL:
      g_tree_getConfigurationPanel.text = sValue;
      break;
    case UN_TREE_HANDLEEVENTNODECLICKOPERATION:
      g_tree_handleEventNodeClickOperation.text = sValue;
      break;
    case UN_TREE_NODELIST_EVENTDBLCLICKPOSTEXECUTE:
      g_nodeList_EventDblClickPostExecute.text = sValue;
      break;
    case UN_TREE_NODELIST_EVENTDBLCLICKPRETEXECUTE:
      g_nodeList_EventDblClickPreExecute.text = sValue;
      break;
    case UN_TREE_OPENPANELPREEXECUTE:
      g_nodeList_openPanelPreExecute.text = sValue;
      break;
    case UN_TREE_OPENPANELGETDOLLARPARAM:
      g_tree_openPanelGetDollarParam.text = sValue;
      break;
    case UN_TREE_POST_EVENTNODERIGTHCLICK:
      g_tree_postExecuteEventNodeRigthClick.text = sValue;
      break;
    case UN_TREE_POST_EVENTRIGTHCLICK:
      g_tree_postExecuteEventRigthClick.text = sValue;
      break;
    case UN_TREE_EVENTNODERIGTHCLICKOPERATIONMENU:
      g_tree_evtNodeRigthClickOperationMenu.text = sValue;
      break;
    case UN_TREE_HANDLEEVENTNODERIGTHCLICKOPERATION:
      g_tree_handleEvtNodeRigthClickOperation.text = sValue;
      break;
    default:
      break;
  }
}

// unTree_setDeviceTypePanel
/**
Purpose:
set the configuration data.

  @param sTreeType: string, input: the type of tree
  @param sDeviceType: string, input: the device type
  @param dsValue: dyn_string, input: the value

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setDeviceTypePanel(string sTreeType, string sDeviceType, dyn_string dsValue)
{
  dyn_string dsTempName, dsTempPanels;
  string sTemp;
  int pos, i, len;
  
  len = dynlen(dsValue);
  for(i=1; i<=len; i++) {
    if(sTemp == "") 
      sTemp = dsValue[i];
    else
      sTemp = sTemp+unTreeWidget_keySeparator+dsValue[i];

  }
  dsTempName = g_tree_deviceTypeName.items;
  dsTempPanels = g_tree_deviceTypePanel.items;
  
  pos = dynContains(dsTempName, sDeviceType);
  if(pos > 0) { // already in list, just replace the panels
    dsTempPanels[pos] = sTemp;
  }
  else { // not in list, add at the end
    dynAppend(dsTempName, sDeviceType);
    dynAppend(dsTempPanels, sTemp);
  }
  g_tree_deviceTypeName.items = dsTempName;
  g_tree_deviceTypePanel.items = dsTempPanels;  
}

// unTree_getDeviceTypePanel
/**
Purpose:
get the configuration data.

  @param sTreeType: string, input: the type of tree
  @param sDeviceType: string, input: the device type
  @param dsValue: dyn_string, output: the value

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_getDeviceTypePanel(string sTreeType, string sDeviceType, dyn_string &dsValue)
{
  dyn_string dsTempName, dsTempPanels;
  string sTemp;
  int pos;
  
  dsTempName = g_tree_deviceTypeName.items;
  dsTempPanels = g_tree_deviceTypePanel.items;
  
  pos = dynContains(dsTempName, sDeviceType);
  if(pos > 0) { // in list, get the panels
    sTemp = dsTempPanels[pos];
    dsValue = strsplit(sTemp, unTreeWidget_keySeparator);
  }
}

// unTree_setHierarchyButtonReorderGlobalVariable
/**
Purpose:
Set the reorder global variable of hierarchy cascade button.

  @param bReorder: bool, input: state of the reorder button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyButtonReorderGlobalVariable(bool bReorder)
{
  g_reorder.text = bReorder;
}

// unTree_setHierarchyButtonReorderState
/**
Purpose:
Set the state of the reorder button of the hierarchy cascade button but not the global variables.

  @param bReorder: bool, input: state of the reorder button

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTree_setHierarchyButtonReorderState(bool bReorder)
{
  bool bState;
  
  bState = bReorder;
  hierarchyCascade.enableItem("12", bState);
}

//@}
