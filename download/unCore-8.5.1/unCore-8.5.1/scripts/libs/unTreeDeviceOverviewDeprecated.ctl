//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getAllApplication
/**
Purpose:
Get all the applications
  
  @param sSystemName: string, input: the systemName.
  @param dsApplication: dyn_string, output: the list of application.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
  
@ Deprecated 2018-08-14
  
*/
unTreeDeviceOverview_getAllApplication(string sSystemName, dyn_string &dsApplication)
{

  FWDEPRECATED();

  dyn_string dsFrontEnd, dsFrontEndDp, dsFrontEndType, dsTempApplication, dsReturn;
  int i, len;

  unTreeDeviceOverview_getFrontEndName(sSystemName, dsFrontEnd, dsFrontEndDp, dsFrontEndType);
  len = dynlen(dsFrontEnd);
  for(i=1;i<=len;i++) {
    dsTempApplication = makeDynString();
    unTreeDeviceOverview_getApplication(sSystemName, dsFrontEnd[i], dsTempApplication);
    dynAppend(dsReturn, dsTempApplication);
  }
  dynUnique(dsReturn);
  dsApplication = dsReturn;
}





//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getDeviceParams
/**
Purpose:
Get the front-end, the front-end application, the device type and the device number.
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
  
@Deprecated 2018-08-14

*/
unTreeDeviceOverview_getDeviceParams(string sDeviceDpName, string &sFrontEnd, string &sFEApplication, string &sDeviceType, int &iDeviceNumber)
{

  FWDEPRECATED();

  dyn_string dsSplit;
  string sDpName = unGenericDpFunctions_getDpName(sDeviceDpName);
  int iwf, imez, iCi;
  string sFE, sAppl, sDevType;
  int iDevNb;
  
  dsSplit = strsplit(sDpName, UN_DPNAME_SEPARATOR);
  if(dynlen(dsSplit) >= 5) { // prefix-FE-FEApplication-DevType-devNumber
    sFE = dsSplit[2]; // FE
    sAppl = dsSplit[3]; // appl
    sDevType = dsSplit[4]; // device type
    iDevNb = (int)dsSplit[5]; // dev number
  }
  sFrontEnd = sFE;
  sFEApplication = sAppl;
  sDeviceType = sDevType;
  iDeviceNumber = iDevNb;
}