#uses "unTreeWidgetDeprecated.ctl"


/**@name LIBRARY: unTreeWidget.ctl


@author: Herve Milcent (LHC-IAS)

Creation Date: 19/04/2002

Modification History: 
  17/02/2009: Herve
    - Bug PVSS 3.8: casting anytype to string, unTreeWidget_setPropertyValue
      
   30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

       05/11/2007: Herve
          - migration to QT widget
          
  19/09/2007: Herve
    - new method added in unTreeWidget_setPropertyValue: deleteNodeWithNoEvent, deleteNode, setNodeState_notSelected, addToTextLabelOfNode
    - new method added in unTreeWidget_getPropertyValue: getAllParentCompositeLabelOfNode, getChildrenCompositeLabelOfNode, 
                                                         getFirstChildrenOfNode
    - extra parameter in unTreeWidget_getPropertyValue

version 1.0

External Functions: 
  . unTreeWidget_showChildren,
  . unTreeWidget_EventAfterLabelEdit,
  . unTreeWidget_EventCollapse,
  . unTreeWidget_EventExpand,
  . unTreeWidget_modifyLabel
  . unTreeWidget_getIconClass
  . unTreeWidget_getNodeNameAndNodeDp
  . unTreeWidget_addNode
                  
Internal Functions: 

Purpose:
This library contains general purpose function to be used with the unTreeWidget

Constraints: 

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/

#uses "libunCore/unApplicationFilter.ctl"
#uses "unProgressBar.ctl"

// constant declaration
const string unTreeWidget_keySeparator = "@";
const string unTreeWidget_classSeparator = "~";
const string unTreeWidget_compositeLabelSeparator = "|";
const string unTreeWidget_labelSeparator = ";";
const string unTreeWidget_treeNodeDpPrefix = "fwTN_";
const string unTreeWidget_treeTypeDpPrefix = "fwTT_";
const string unTreeWidget_CLIPBOARD = "Clipboard";
const string unTreeWidget_NODE = "node";
const string unTreeWidget_treeNodeDPT = "_FwTreeNode";
const int unTreeWidget_ccHourglass = 11;

// constant for the popupmenu
const int unTreeWidget_popupMenu_nodeName=1;
const int unTreeWidget_popupMenu_add=2;
const int unTreeWidget_popupMenu_remove=3;
const int unTreeWidget_popupMenu_cut=4;
const int unTreeWidget_popupMenu_copy=5;
const int unTreeWidget_popupMenu_paste=6;
const int unTreeWidget_popupMenu_rename=7;
const int unTreeWidget_popupMenu_reorder=8;
const int unTreeWidget_popupMenu_initTree=9;
const int unTreeWidget_popupMenu_initTreeNoClipboard = 10;
const int unTreeWidget_popupMenu_clearTree=11;
const int unTreeWidget_popupMenu_removeTree=12;
const int unTreeWidget_popupMenu_addTree=13;
const int unTreeWidget_popupMenu_copyTree=14;
const int unTreeWidget_popupMenu_sortAZ=15;
const int unTreeWidget_popupMenu_sortZA=16;
const int unTreeWidget_popupMenu_sortCustom=17;


const int unTreeWidget_classTypeLength = 7;

const int UN_TREEWIDGET_ICON_MAX_FILE = 6;
const int UN_TREEWIDGET_ICON_CHILDREN_CLOSED=1;
const int UN_TREEWIDGET_ICON_CHILDREN_OPENED=2;
const int UN_TREEWIDGET_ICON_CHILDREN_SELECTED=3;
const int UN_TREEWIDGET_ICON_CHILDREN_PATH=4;
const int UN_TREEWIDGET_ICON_NOCHILDREN_NOTSELECTED=5;
const int UN_TREEWIDGET_ICON_NOCHILDREN_SELECTED=6;

// global variable
global mapping g_m_unTreeWidget_dsImage;
global mapping g_m_unTreeWidget_dsNodeList, g_m_unTreeWidget_dsNodeListClass, g_m_unTreeWidget_dsExpandedNodeId, g_m_unTreeWidget_dsParentIdOfSelectedNode;
    
//@{

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_showChildren
/**
Purpose:
Shows the children of a node of the unTreeWidget used to view the hierarchy. This will allow to show the + in the unTreeWidget when 
a click, expand or double click is done.
The sKey is: sContext|childrenLabel@parentKey

  @param expand: bool, input: true (1): show the children of the children, fasle (0) don't show the children of the children
  @param bClipboard: bool, input: true (1): show the clipboard, fasle (0) don't show the clipboard
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sDpName: string, input: the name of the data point
  @param sKey: string, input: the key of the data point sDpName
  @param sDisplayNodeName: string, input: the node name of the sDpName
  @param exceptionInfo: dyn_string, output: Details of any exceptions are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_showChildren(bool expand, bool bClipboard, string sGraphTree, string sDpName, string sKey, string sDisplayNodeName, dyn_string &exceptionInfo, bool bShowOneChildren=false)
{
  dyn_string dsChildrenNodeName;
  string dpChild, currentKey, sChildNodeName;;
  int i, len;
  shape aShape;
  
  aShape = getShape(sGraphTree);
  
  // get all the children of the sDpName
  if(bClipboard)
  {
    fwTree_getChildrenWithClipboard(_fwTree_getNodeName(sDpName), dsChildrenNodeName, exceptionInfo);
  }
  else
  {
    fwTree_getChildren(_fwTree_getNodeName(sDpName), dsChildrenNodeName, exceptionInfo);
  }
  
  len = dynlen(dsChildrenNodeName);
    
  if(bShowOneChildren) 
  { 
    // show only one children 
    if(len > 1) // more than one children, just show one
    {
      len = 1;
    }
  }
  
  for(i = 1 ; i <= len ; i++) 
  {
    // build the child key, the key is: contextOfTheParent|compositeChildLabel@parentKey
    // contextOfTheParent = parentNodeName;dpParent
    // compositeChildLabel = childrenNodeName;dpChildren
    dpChild = _fwTree_makeNodeName(dsChildrenNodeName[i]);
    sChildNodeName = fwTree_getNodeDisplayName(dsChildrenNodeName[i], exceptionInfo);
    

      
    if(dpExists(dpChild)) 
    {
      currentKey = 
          sDisplayNodeName + unTreeWidget_labelSeparator +
          sDpName + unTreeWidget_compositeLabelSeparator +
          sChildNodeName + unTreeWidget_labelSeparator + 
          dpChild + unTreeWidget_keySeparator +
          sKey;
      
      // add the children, contextOfTheParent|compositeChildLabel@parentKey is given to the tree, this will be the key used by the tree to store
      // the node element.

      // do not add the node if there is an error;
      if(dynlen(exceptionInfo) <= 0) 
      {
        if (g_unicosHMI_bApplicationFilterInUse)
        {
          synchronized(g_unicosHMI_dsApplicationList)
          {
            // How it works:
            // If the node name is an application name
            // -- If it is a visible application, it is displayed
            // -- else it is hidden
            // If it is an element we want to force hide: hide
            // Else: display it no matter what it is
              
            // Build a list of applications
            dyn_string dsAllApplicationList = g_dsTreeApplication;
              
            // Remove "*" and "List..."
            dynRemove(dsAllApplicationList, dynlen(dsAllApplicationList));
            dynRemove(dsAllApplicationList, 1);
              
            if (
              (
                (dynContains(dsAllApplicationList, sChildNodeName) > 0) &&                 
                (dynContains(g_unicosHMI_dsApplicationList, sChildNodeName) < 1) && 
                !bShowOneChildren
              ) ||                
              (
                dynContains(g_unicosHMI_dsTreeHiddenNames, sChildNodeName) > 0)
              )              
            {
              // Do not display : go to next iteration of the loop
              continue;
            }
          }
        }
        
        unTreeWidget_addNode(sGraphTree, dpChild, unTreeWidget_getIconClass(dpChild) + unTreeWidget_classSeparator + currentKey, false, exceptionInfo);
              
        if(expand) 
        {
          
              
          // show the children of the children
          unTreeWidget_showChildren(false, bClipboard, sGraphTree, dpChild, currentKey, sChildNodeName, exceptionInfo, true);
        }
      }
    }
  }
}



//------------------------------------------------------------------------------------------------------------------------

//unTreeWidget_EventCollapse
/**
Purpose:
Executes the EventCollapse of the unTreeWidget used to view the hierarchy. This function is executed when one collapse 
a node shown in the unTreeWidget. The unTreeWidget deletes the node which is collapsed, this guarantess that the correct state 
of the ndoe is shown, because the children will be read again (this avoid cases such as children were added to the 
unTreeWidget and afterwards they were deleted, if the unTreeWidget is not refreshed then, the state of the node is wrong).
The unTreeWidget tree is disabled during the operation. This function is started in a separate thread.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
  
@Reviewed 2018-08-14 @Whitelisted{Callback}
 
*/
unTreeWidget_EventCollapse(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard)
{
  dyn_string exceptionInfo;
  shape aShape;
  int id;
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName;
  int iCurrentMousePointer, iMousePointer;


// start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);
// disable the unTreeWidget and get the key of the node which is collapsed
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
  
//  DebugN("expand", mode, selectedKey, unTreeWidget_getPropertyValue(aShape, "selectedLabel", true), compositeLabel);
// dpName to look for is after | in split [2] and in split[2];
// the key is contextOfTheParent|compositeChildLabel@parentKey, contextOfTheParent is: labelOfParent;dpParent
// and compositeChildLabel is childLabel;dpChild

  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
//DebugN("collapse", unTreeWidget_getPropertyValue(aShape, "selectedCompositeLabel", true), sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);

// the unTreeWidget automatically deletes the children of the node which is collapsed, so they are shown again.
//DebugN(unTreeWidget_getPropertyValue(aShape, "selectedCompositeLabel", true), sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
  if((sDpName != "") && (sDisplayNodeName != ""))
    unTreeWidget_showChildren(false, bClipboard, sGraphTree, sDpName, sSelectedKey, sDisplayNodeName, exceptionInfo);

  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
// enable the unTreeWidget and stop the progess bar.
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
  unProgressBar_stop(id, sBar);

// handle any errors here
  if(dynlen(exceptionInfo)>0)
    fwExceptionHandling_display(exceptionInfo);
}

// unTreeWidget_EventExpand
/**
Purpose:
Executes the EventExpand of the unTreeWidget used to view the hierarchy. This function is executed when one expands 
a node shown in the unTreeWidget. The unTreeWidget deletes the node which is expanded, this guarantess that the correct state 
of the ndoe is shown, because the children will be read again (this avoid cases such as children were added to the 
unTreeWidget and afterwards they were deleted, if the unTreeWidget is not refreshed then, the state of the node is wrong).
The unTreeWidget tree is disabled during the operation. This function is started in a separate thread.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unTreeWidget_EventExpand(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard)
{
  dyn_string exceptionInfo;
  shape aShape;
  int id;
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName;
  int iCurrentMousePointer, iMousePointer;

  // start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);
  
  // disable the unTreeWidget tree and get the key of the node which is collapsed
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
  
  // dpName to look for is after | in split [2] and in split[2];
  // the key is contextOfTheParent|compositeChildLabel@parentKey, contextOfTheParent is: labelOfParent;dpParent
  // and compositeChildLabel is childLabel;dpChild
  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);

  // the unTreeWidget automatically deletes the children of the node which is collapsed, so they are shown again.
  if((sDpName != "") && (sDisplayNodeName != ""))
  {
    unTreeWidget_showChildren(true, bClipboard, sGraphTree, sDpName, sSelectedKey, sDisplayNodeName, exceptionInfo);
  }

  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
  
  // enable the unTreeWidget and stop the progess bar.
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
  unProgressBar_stop(id, sBar);

  // handle any errors here
  if(dynlen(exceptionInfo)>0)
  {
    fwExceptionHandling_display(exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_modifyLabel
/**
Purpose:
Modifies the labels of the dp, this functions deletes the current node and adds it again, all the children node
are therefore automatically deleted thay must be added afterwards.

  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param dpName: string, input: the data point anme
  @param sDisplayNodeName: string, input: the current nodeName of the dpName
  @param sNewNodeName: string, input: the new node name of the dpName
  @param sContext: string, input: the context of the dpName (composite parent label: parentLabel;dpParent)
  @param sParentKey: string, input: the key of the parent of the dpName read from the unTreeWidget
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param exceptionInfo: dyn_string, output: Details of any exceptions are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget, it is preferable that this function is called while the 
  unTreeWidget is disabled to guarante the uniqueness of the execution.
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_modifyLabel(bool bClipboard, string sGraphTree, string dpName, 
                                    string sDisplayNodeName, string sNewNodeName, string sContext,  
                                    string sParentKey, string sSelectedKey, string sCurrentSelectedNodeKey, 
                                    string sParentKeyToAddInNewKey, dyn_string &exceptionInfo)
{
  string sNewKey, compositeLabel;
  shape aShape;
  dyn_string split;
  string newDpName;
  bool bTopNode=false;
  string sReturnedNameNode;
  
//  DebugN("modlabel", dpName, sDisplayNodeName, sNewNodeName, sContext, sParentKey);

  aShape = getShape(sGraphTree);
// this is not a top node, it has a parent node.
//    DebugN("del and add node", sDisplayNodeName, sNewNodeName);
// the label is modified
  sReturnedNameNode = fwTree_renameNode(_fwTree_getNodeName(dpName), sNewNodeName, exceptionInfo);
  newDpName = _fwTree_makeNodeName(sReturnedNameNode);
  compositeLabel = sNewNodeName+unTreeWidget_labelSeparator+newDpName;
// do not add the node if there is an error;
  if(dynlen(exceptionInfo) <= 0) {
// the new key is build and the node is shown again
// the key is contextOfTheParent|compositeChildLabel@parenKey, contextOfTheParent is: labelOfParent;dpParent
// and compositeChildLabel is childLabel;dpChild
// the node is deleted
    unTreeWidget_setPropertyValue(aShape, "deleteNode", sSelectedKey, true);
    sNewKey = sContext+unTreeWidget_compositeLabelSeparator+compositeLabel+unTreeWidget_keySeparator+sParentKeyToAddInNewKey;

    if(sParentKey == "") 
      bTopNode = true;

    unTreeWidget_addNode(sGraphTree, newDpName, unTreeWidget_getIconClass(newDpName) + unTreeWidget_classSeparator + sNewKey, bTopNode, exceptionInfo);
//      addNode(unTreeWidget_getIconClass(dpName) + unTreeWidget_classSeparator + sNewKey);
// the children are shown.
    unTreeWidget_showChildren(false, bClipboard, sGraphTree, newDpName, sNewKey, compositeLabel, exceptionInfo);
//DebugN("after sho", dpName, compositeLabel, exceptionInfo);
  // if the current key is the same as sCurrentSelectedNodeKey, then re-trigger the select so the NodeLeftClick event will be raised
  // this will happen when the rename is done on the current selected node, so the node is redrawn and selected
//DebugN("****************", sSelectedKey, sParentKeyToAddInNewKey, sCurrentSelectedNodeKey, sParentKey);
//DebugN("****************", sNewKey, sParentKeyToAddInNewKey, sCurrentSelectedNodeKey, sParentKey);
//    if(sCurrentSelectedNodeKey == sSelectedKey) {
//      unTreeWidget_setPropertyValue(aShape, "mouseEvt", "Left", true);
//      unTreeWidget_setPropertyValue(aShape, "setNodeState_selected", sNewKey, true);
//    }
  // if the parentKey is the same as sCurrentSelectedNodeKey, then re-trigger the select of the parent so the NodeLeftClick event will be raised
  // this will happen when the rename is done on a child of the current selected node, so the node is redrawn and selected
//    if((sCurrentSelectedNodeKey == sParentKey) && (sCurrentSelectedNodeKey != "")){
//      unTreeWidget_setPropertyValue(aShape, "mouseEvt", "Left", true);
//      unTreeWidget_setPropertyValue(aShape, "setNodeState_selected", sCurrentSelectedNodeKey, true);
//    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getIconClass
/**
Purpose:
Get the icon class of the unTreeWidget current node. The current node is represented by a DPE. The DP type of this DPE is the icon class.

  @param sDpName: string, input: unTreeWidget current node DPE
  @param return value: string: the class of the sDpName

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
string unTreeWidget_getIconClass(string sDpName)
{
  dyn_string exInfo;
  string dpType = sDpName, sDeviceName, sDeviceType, sNodeName;
/*
  - if node from the hierarchy:
    - if root, use the node name
    - if clipboard, use the the keyword: clipboard
    - if node with no device: use keyword node
    - if node with device use the device type
  - if not
    - if the dp exists return dpType
    - if not return the value
*/  
  if(dpExists(sDpName)) {
    sDeviceType = dpTypeName(sDpName);
    if(sDeviceType == unTreeWidget_treeNodeDPT) { // dp from hierarchy
      sNodeName=_fwTree_getNodeName(sDpName);
      if(fwTree_isRoot(sNodeName, exInfo)) {
        dpType = sNodeName;
      }
      else if (fwTree_isClipboard(sNodeName, exInfo)) {
        dpType = unTreeWidget_CLIPBOARD;
      }
      else {
  // get device reference, if not "" get dpType
        fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
        if(sDeviceName != "")
          dpType = sDeviceType;
        else 
          dpType = unTreeWidget_NODE;
          
      }
    }
    else { // other dpType
      dpType = sDeviceType;
    }
  }
  return(dpType);
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_addNode
/**
Purpose:
Add a node in the unTreeWidget

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sDpName: string, input: device name
  @param sKey: string, input: the device key.
  @param bTop: bool, input: top node (true)
  @param exceptionInfo: dyn_string, output: Details of any exceptions are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget 
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_addNode(string sGraphTree, string sDpName, string sKey, bool bTop, dyn_string &exceptionInfo)
{
  string deviceSystemName;
  bool bConnected=false;
  shape aShape;

  aShape = getShape(sGraphTree);

// check if the remote system is connected
  deviceSystemName = unGenericDpFunctions_getSystemName(sDpName);
  unDistributedControl_isConnected(bConnected, deviceSystemName);

// do not add the node if the remote system is not connected
  if(bConnected) {
    if(bTop) {
// add top node
      unTreeWidget_setPropertyValue(aShape, "addTopNode", sKey, true);
    }
    else {
// add any other node
      unTreeWidget_setPropertyValue(aShape, "addNode", sKey, true);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getNodeNameAndNodeDp
/**
Purpose:
get the node name and the node data point

  @param sCompositeLabel: string, input: parentNodeName;parentDpNodeName|nodeName;dpNodeName or |nodeName;dpNodeName if root node
  @param sDpName: string, output: data point name
  @param sDisplayNodeName: string, output: the node name of sDpName
  @param sParentDpName: string, output: data point name of the parent
  @param sParentNodeName: string, output: the node name of sParentDpName

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget 
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_getNodeNameAndNodeDp(string sCompositeLabel, string &sDpName, string &sDisplayNodeName, string &sParentDpName, string &sParentNodeName)
{
  dyn_string dsSplit, dsSplitBis;
  int len;
  string parentNodeName, parentDp, dpName, nodeName;
  
  dsSplit = strsplit(sCompositeLabel, unTreeWidget_compositeLabelSeparator);
  len = dynlen(dsSplit);
  
  if(len > 1) {
    dsSplitBis = strsplit(dsSplit[1], unTreeWidget_labelSeparator);
    if(dynlen(dsSplitBis) > 1) {
      parentNodeName = dsSplitBis[1];
      parentDp = dsSplitBis[2];
    }
    dsSplitBis = strsplit(dsSplit[2], unTreeWidget_labelSeparator);
    if(dynlen(dsSplitBis) > 1) {
      nodeName = dsSplitBis[1];
      dpName = dsSplitBis[2];
    }
  }
  sDpName = dpName;
  sDisplayNodeName = nodeName;
  sParentDpName = parentDp;
  sParentNodeName = parentNodeName;
}




//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_initialize
/**
Purpose:
Executes the initialization of the unTreeWidget used to view the hierarchy. This function is executed at startup of the unTreeWidget 
a node shown in the unTreeWidget.
The unTreeWidget tree is disabled during the operation. This function is started in a separate thread.

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_initialize(string sGraphTree, string sBar, bool bClipboard)
{
  dyn_string dsTree = fwTreeDisplay_getTrees();
  dyn_string dsNode, exceptionInfo;
  dyn_string dsResult;
  int i, len, id;
  string sKey;
  shape aShape=getShape(sGraphTree);
  int iCurrentMousePointer, iMousePointer;
  
  id = unProgressBar_start(sBar);  
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
// do not allow the renaming of top node
  unTreeWidget_setPropertyValue(aShape, "set_bRenameRootNodeAllowed", false, true);

  fwTree_getRootNodes(dsNode, exceptionInfo);
  
  if(dynlen(exceptionInfo) > 0) {
    fwExceptionHandling_display(exceptionInfo);
  }
  else {
    dsResult = dynIntersect(dsTree, dsNode);
  // add topNode: class~key where key is |labelOfTheNode;dpNameOfTheNode
    len = dynlen(dsResult);
    for(i=1;i<=len;i++) {
      sKey = unTreeWidget_compositeLabelSeparator+dsResult[i]+unTreeWidget_labelSeparator+unTreeWidget_treeNodeDpPrefix+dsResult[i];
      unTreeWidget_addNode(sGraphTree, unTreeWidget_treeNodeDpPrefix+dsResult[i], unTreeWidget_getIconClass(unTreeWidget_treeNodeDpPrefix+dsResult[i])+unTreeWidget_classSeparator+sKey, true, exceptionInfo);

// get children but don't show the children of the children and no clipboard
      unTreeWidget_showChildren(true, bClipboard, sGraphTree, unTreeWidget_treeNodeDpPrefix+dsResult[i], sKey, dsResult[i], exceptionInfo);
// fill the g_dsTree: contains the list of tree that are present in the unTreeWidget.
      dynAppend(g_dsTree, dsResult[i]);
      dynAppend(g_sPasteNodeName, "");
      dynAppend(g_sPasteNodeDpName, "");
    }
  }
  
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
  unProgressBar_stop(id, sBar);

// handle any errors here
  if(dynlen(exceptionInfo)>0) {
    fwExceptionHandling_display(exceptionInfo);
  }
}



//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_addTree
/**
Purpose: add a new tree 

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param sTreeName: string, input: the name of the tree to add
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_addTree(string sGraphTree, bool bClipboard, string sTreeName, dyn_string &exceptionInfo)
{
  string sKey;
  int pos;
  
  pos = strpos(sTreeName, unTreeWidget_CLIPBOARD);
  if(pos >= 0) {
      fwException_raise(exceptionInfo, "ERROR", getCatStr("unTreeWidget", "CLIPBOARDTREE")+sTreeName, "");
  }
  else {
    unTreeWidget_checkNodeName(sTreeName, exceptionInfo);
    if(dynlen(exceptionInfo) <= 0) {
      if(fwTree_isNode(sTreeName, exceptionInfo) == 0) {
        fwTreeDisplay_createTree(sTreeName, 2, sTreeName, makeDynString(), makeDynString());
      // add topNode: class~key where key is |labelOfTheNode;dpNameOfTheNode
        sKey = unTreeWidget_compositeLabelSeparator+sTreeName+unTreeWidget_labelSeparator+unTreeWidget_treeNodeDpPrefix+sTreeName;
        unTreeWidget_addNode(sGraphTree, unTreeWidget_treeNodeDpPrefix+sTreeName, unTreeWidget_getIconClass(unTreeWidget_treeNodeDpPrefix+sTreeName)+unTreeWidget_classSeparator+sKey, true, exceptionInfo);
  // get children but don't show the children of the children and no clipboard
        unTreeWidget_showChildren(true, bClipboard, sGraphTree, unTreeWidget_treeNodeDpPrefix+sTreeName, sKey, sTreeName, exceptionInfo);
  // add the tree in the list of trees and reset the corresponding pasteDp
        dynAppend(g_dsTree, sTreeName);
        dynAppend(g_sPasteNodeName, "");
        dynAppend(g_sPasteNodeDpName, "");
      }
      else
        fwException_raise(exceptionInfo, "ERROR", getCatStr("unTreeWidget", "TREEALREADYEXISTS")+sTreeName, "");
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_copyTree
/**
Purpose: copy a tree

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param sTreeToCopy: string, input: the tree name to copy
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_copyTree(string sGraphTree, bool bClipboard, string sTreeToCopy, string sTreeName, dyn_string &exceptionInfo)
{
  string sKey;

// check the name
  unTreeWidget_checkNodeName(sTreeName, exceptionInfo);
  if(dynlen(exceptionInfo) <= 0) {
    if(fwTree_isNode(sTreeName, exceptionInfo) == 0) {
      fwTree_copyTree(sTreeToCopy, sTreeName, exceptionInfo);
    // add topNode: class~key where key is |labelOfTheNode;dpNameOfTheNode
      sKey = unTreeWidget_compositeLabelSeparator+sTreeName+unTreeWidget_labelSeparator+unTreeWidget_treeNodeDpPrefix+sTreeName;
      unTreeWidget_addNode(sGraphTree, unTreeWidget_treeNodeDpPrefix+sTreeName, unTreeWidget_getIconClass(unTreeWidget_treeNodeDpPrefix+sTreeName)+unTreeWidget_classSeparator+sKey, true, exceptionInfo);
// get children but don't show the children of the children and no clipboard
      unTreeWidget_showChildren(true, bClipboard, sGraphTree, unTreeWidget_treeNodeDpPrefix+sTreeName, sKey, sTreeName, exceptionInfo);
// add the tree in the list of trees and reset the corresponding pasteDp
      dynAppend(g_dsTree, sTreeName);
      dynAppend(g_sPasteNodeName, "");
      dynAppend(g_sPasteNodeDpName, "");
    }
    else
      fwException_raise(exceptionInfo, "ERROR", getCatStr("unTreeWidget", "TREEALREADYEXISTS")+sTreeName, "");
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_removeTree
/**
Purpose: remove a tree

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param sSelectedKey: string, input: the key of the node to remove
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_removeTree(string sGraphTree, bool bClipboard, string sSelectedKey, 
                                        string sDpName, string sDisplayNodeName, dyn_string &exceptionInfo)
{
  string sNodeToRemove = _fwTree_getNodeName(sDpName);
  shape aShape;
  int pos;

  // remove the node and the subree.
  fwTree_removeTree(_fwTree_getNodeName(sDpName), exceptionInfo);
  if(dynlen(exceptionInfo) <= 0) 
  {
    // no error
    // remove the selected node from the unTreeWidget.
    aShape=getShape(sGraphTree);
    unTreeWidget_setPropertyValue(aShape, "deleteNode", sSelectedKey, true);

    // get the tree position from the list and remove the pasteDp.
    pos = dynContains(g_dsTree, sNodeToRemove);
    if(pos > 0) 
    {
      dynRemove(g_dsTree, pos);
      dynRemove(g_sPasteNodeName, pos);
      dynRemove(g_sPasteNodeDpName, pos);
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_add
/**
Purpose: add a new node 

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sDisplayNodeNameToAdd: string, input: the node name to add
  @param sNodeNameAdded: string, output: the node name that was added
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_add(string sGraphTree, string sSelectedKey, string sDpName, string sDisplayNodeName, 
              string sDisplayNodeToAdd, string &sNodeNameAdded, dyn_string &exceptionInfo)
{
  dyn_string dsText;
  dyn_float dfFloat;
  string nodeNameToAdd, dpChild, sChildKey;
  int pos;
  
  pos = strpos(sDisplayNodeToAdd, unTreeWidget_CLIPBOARD);
  if(pos >= 0) {
      fwException_raise(exceptionInfo, "ERROR", getCatStr("unTreeWidget", "CLIPBOARDNODE")+sDisplayNodeToAdd, "");
  }
  else {
    unTreeWidget_checkNodeName(sDisplayNodeToAdd, exceptionInfo);
    if(dynlen(exceptionInfo) <= 0) {
  // make the node and add it in the tree
      nodeNameToAdd = fwTree_createNode(_fwTree_getNodeName(sDpName), sDisplayNodeToAdd, exceptionInfo);
      if(dynlen(exceptionInfo) <= 0) {
        dpChild = _fwTree_makeNodeName(nodeNameToAdd);
  // create the child key is parentLabel;dpParent|childLabel;dpChild@parentKey 
        sChildKey = sDisplayNodeName+unTreeWidget_labelSeparator+sDpName+unTreeWidget_compositeLabelSeparator+
                      sDisplayNodeToAdd+unTreeWidget_labelSeparator+dpChild+
                      unTreeWidget_keySeparator+sSelectedKey;
        unTreeWidget_addNode(sGraphTree, dpChild, unTreeWidget_getIconClass(dpChild) + unTreeWidget_classSeparator + sChildKey, false, exceptionInfo);
        sNodeNameAdded = nodeNameToAdd;
      }
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_remove
/**
Purpose: remove the node and subtree and put it into the clipboard

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sParentKey: string, input: the key of the parent node of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sParentDpName: string, input: data point name of the parent
  @param sParentNodeName: string, input: the node name of sParentDpName
  @param bClipboard: bool, input: show clipboard
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_remove(string sGraphTree, string sSelectedKey, string sParentKey, string sDpName, 
              string sDisplayNodeName, string sParentDpName, string sParentNodeName, bool bClipboard, dyn_string &exceptionInfo)
{
  string sNodeToRemove = _fwTree_getNodeName(sDpName);
  shape aShape;
  
// remove the node and the subree.
  fwTree_removeSubTree(_fwTree_getNodeName(sParentDpName), _fwTree_getNodeName(sDpName), exceptionInfo);
  if(dynlen(exceptionInfo) <= 0) {
// no error
// remove the selected node from the unTreeWidget.
    aShape=getShape(sGraphTree);
    unTreeWidget_setPropertyValue(aShape, "deleteNode", sSelectedKey, true);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_cut
/**
Purpose: cut the node and subtree and put it into the clipboard

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sParentKey: string, input: the key of the parent node of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sParentDpName: string, input: data point name of the parent
  @param sParentNodeName: string, input: the node name of sParentDpName
  @param bClipboard: bool, input: show clipboard
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_cut(string sGraphTree, string sSelectedKey, string sParentKey, string sDpName, 
              string sDisplayNodeName, string sParentDpName, string sParentNodeName, bool bClipboard, dyn_string &exceptionInfo)
{
  shape aShape=getShape(sGraphTree);
  string sNodeToCut = _fwTree_getNodeName(sDpName), clipboard;
  string sTree, sClipBoardKey, sClipboardDp, sTreeDp, sNodeKey;

// get the tree
  fwTree_getTreeName(sNodeToCut, sTree, exceptionInfo);
  sTreeDp = unTreeWidget_treeNodeDpPrefix+sTree;
// get the clipboard of the node
  clipboard = _fwTree_makeClipboard(sTree);
  sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;

// cut the node and paste it in the clipboard,it is also saved in g_sPasteNodeName for future paste
// refresh the clipboard and the parent node
  if(sClipboardDp != sParentDpName) {
    fwTree_cutNode(_fwTree_getNodeName(sParentDpName), sNodeToCut, exceptionInfo);
    
  exceptionInfo = makeDynString();
//  if(dynlen(exceptionInfo) <=0) {
// no error
    if(bClipboard) {
  // delete all the children of the clipboard 
  //    sClipBoardKey = clipboard;dpCliboard|sTree;dpTree@|sTree;dpTree
      sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;
      sClipBoardKey = sTree+unTreeWidget_labelSeparator+sTreeDp+
                        unTreeWidget_compositeLabelSeparator+
                        clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                        unTreeWidget_keySeparator+
                        unTreeWidget_compositeLabelSeparator+
                        sTree+unTreeWidget_labelSeparator+sTreeDp;
//      unTreeWidget_setPropertyValue(aShape, "deleteChildrenNode", sClipBoardKey, true);
// create the new key node
      sNodeKey = clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                        unTreeWidget_compositeLabelSeparator+
                        sDisplayNodeName+unTreeWidget_labelSeparator+sDpName+
                        unTreeWidget_keySeparator+sClipBoardKey;
// add the node and show its children
      unTreeWidget_addNode(sGraphTree, sDpName, unTreeWidget_getIconClass(sDpName) + unTreeWidget_classSeparator + sNodeKey, false, exceptionInfo);
      unTreeWidget_showChildren(true, bClipboard, sGraphTree, sDpName, sNodeKey, sDisplayNodeName, exceptionInfo);

  // show the clipboard with expand
//      unTreeWidget_showChildren(true, bClipboard, sGraphTree, sClipboardDp, sClipBoardKey, clipboard, exceptionInfo);
    }
// remove the node from the unTreeWidget
    unTreeWidget_setPropertyValue(aShape, "deleteNode", sSelectedKey, true);
//  }
//DebugN("end", exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_copy
/**
Purpose: copy the node and subtree and put it into the clipboard the node

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sParentDpName: string, input: data point name of the parent
  @param sParentNodeName: string, input: the node name of sParentDpName
  @param bClipboard: bool, input: show clipboard
  @param sCopiedDisplayNodeName: string, input: the copied display node name.
  @param sNodeName: string, output: the copied node name, the complete one.
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_copy(string sGraphTree, string sSelectedKey, string sDpName, 
              string sDisplayNodeName, string sParentDpName, string sParentNodeName, bool bClipboard, 
              string sCopiedDisplayNodeName, string &sNodeName, dyn_string &exceptionInfo)
{
  shape aShape;
  string sTree, sClipBoardKey, sClipboardDp, sTreeDp, clipboard, sNodeKey, sCopiedNodeDpName;

// get the tree
  fwTree_getTreeName(_fwTree_getNodeName(sParentDpName), sTree, exceptionInfo);
  sTreeDp = unTreeWidget_treeNodeDpPrefix+sTree;
// get the clipboard of the node
  clipboard = _fwTree_makeClipboard(sTree);
  sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;
  
// copy the node dupplicate the node and the subtree
// and paste it in the clipboard,it is also saved in g_sPasteNodeName for future paste
// refresh the clipboard and the parent node
  fwTree_copySubTree(_fwTree_getNodeName(sDpName), sCopiedDisplayNodeName, clipboard, sNodeName, exceptionInfo);
//DebugN("unTreeWidget_copy", sDisplayNodeName, _fwTree_getNodeName(sDpName), sDpName, bClipboard, sNodeName, exceptionInfo);
  if(dynlen(exceptionInfo) <=0) {
// no error show clipboard
    if(bClipboard) {
      aShape=getShape(sGraphTree);
  // delete all the children of the clipboard 
  //    sClipBoardKey = clipboard;dpCliboard|sTree;dpTree@|sTree;dpTree
      sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;
      sClipBoardKey = sTree+unTreeWidget_labelSeparator+sTreeDp+
                        unTreeWidget_compositeLabelSeparator+
                        clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                        unTreeWidget_keySeparator+
                        unTreeWidget_compositeLabelSeparator+
                        sTree+unTreeWidget_labelSeparator+sTreeDp;

// create the new key node
      sCopiedNodeDpName = _fwTree_makeNodeName(sNodeName);
      sNodeKey = clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                        unTreeWidget_compositeLabelSeparator+
                        sCopiedDisplayNodeName+unTreeWidget_labelSeparator+sCopiedNodeDpName+
                        unTreeWidget_keySeparator+sClipBoardKey;
// add the node and show its children
      unTreeWidget_addNode(sGraphTree, sDpName, unTreeWidget_getIconClass(sDpName) + unTreeWidget_classSeparator + sNodeKey, false, exceptionInfo);
      unTreeWidget_showChildren(true, bClipboard, sGraphTree, sCopiedNodeDpName, sNodeKey, sCopiedDisplayNodeName, exceptionInfo);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_paste
/**
Purpose: paste the node that was copied/cut into the clipboard the node, the node name

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sDpNameToPaste: string, input: data point name of the node to paste
  @param sDisplayNodeNameToPaste: string, input: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_paste(string sGraphTree, string sSelectedKey, string sDpName, 
              string sDisplayNodeName, string sDpNameToPaste, string sDisplayNodeNameToPaste, bool bClipboard, dyn_string &exceptionInfo)
{
// paste the node that was cut and saved in g_sPasteNodeName
  shape aShape=getShape(sGraphTree);
  string clipboard, sNodeKey, sNodeToPaste=_fwTree_getNodeName(sDpNameToPaste);
  string sTree, sClipBoardKey, sClipboardDp, sTreeDp;
  
  if(bClipboard) {
// make the node key
// get the tree
    sTreeDp = _fwTree_getTree(sDpNameToPaste);
    sTree = _fwTree_getNodeName(sTreeDp);
  }
// cut the node from the clipboard and paste it in the node
// refresh the clipboard and the parent node
//DebugN("unTreeWidget_paste", sDisplayNodeName, _fwTree_getNodeName(sDpName), sDisplayNodeNameToPaste, sNodeToPaste, sDpNameToPaste, bClipboard);
  fwTree_pasteNode(_fwTree_getNodeName(sDpName), sNodeToPaste, exceptionInfo);
//DebugN(exceptionInfo);
  exceptionInfo = makeDynString();
//  if(dynlen(exceptionInfo) <=0) {
// no error
      if(bClipboard) {
    // get the clipboard of the node
        clipboard = _fwTree_makeClipboard(sTree);
        sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;
    // delete all the children of the clipboard 
    //    sClipBoardKey = clipboard;dpCliboard|sTree;dpTree@|sTree;dpTree
        sClipboardDp = unTreeWidget_treeNodeDpPrefix+clipboard;
        sClipBoardKey = sTree+unTreeWidget_labelSeparator+sTreeDp+
                          unTreeWidget_compositeLabelSeparator+
                          clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                          unTreeWidget_keySeparator+
                          unTreeWidget_compositeLabelSeparator+
                          sTree+unTreeWidget_labelSeparator+sTreeDp;
        sNodeKey = clipboard+unTreeWidget_labelSeparator+sClipboardDp+
                          unTreeWidget_compositeLabelSeparator+
                          sDisplayNodeNameToPaste+unTreeWidget_labelSeparator+sDpNameToPaste+
                          unTreeWidget_keySeparator+
                          sClipBoardKey;
  // remove the node from clipboard in the unTreeWidget
        unTreeWidget_setPropertyValue(aShape, "deleteNode", sNodeKey, true);
//DebugN("pasteNode", sTreeDp, sTree, sClipboardDp, clipboard);
//DebugN(sNodeKey);
      }
      
// build the new key, add it and display its children.
      sNodeKey = sDisplayNodeName+unTreeWidget_labelSeparator+sDpName+
                        unTreeWidget_compositeLabelSeparator+
                        sDisplayNodeNameToPaste+unTreeWidget_labelSeparator+sDpNameToPaste+
                        unTreeWidget_keySeparator+sSelectedKey;
//DebugN(sNodeKey);
      unTreeWidget_addNode(sGraphTree, sDpNameToPaste, unTreeWidget_getIconClass(sDpNameToPaste) + unTreeWidget_classSeparator + sNodeKey, false, exceptionInfo);
      unTreeWidget_showChildren(true, bClipboard, sGraphTree, sDpNameToPaste, sNodeKey, sDisplayNodeNameToPaste, exceptionInfo);

//  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_rename
/**
Purpose: rename the node

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sParentKey: string, input: the key of the parent of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sCompositeLabel: string, input: the composite label of the node
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sParentDpName: string, input: data point name of the parent
  @param sParentNodeName: string, input: the node name of sParentDpName
  @param sNewDisplayNodeName: string, input: the new display node name
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_rename(string sGraphTree, string sSelectedKey, string sParentKey, string sCompositeLabel, 
                                  string sDpName, string sDisplayNodeName, string sParentDpName, string sParentNodeName, 
                                  bool bClipboard, string sNewDisplayNodeName, string sCurrentSelectedNodeKey, 
                                  string sParentKeyToAddInNewKey, dyn_string &exceptionInfo)
{  
// rename the node
//DebugN("unTreeWidget_rename", sDisplayNodeName, _fwTree_getNodeName(sDpName), sDpName);
  if((!fwTree_isRoot(_fwTree_getNodeName(sDpName), exceptionInfo)) && (!fwTree_isClipboard(sDisplayNodeName, exceptionInfo))) {
  // check the name
    unTreeWidget_checkNodeName(sNewDisplayNodeName, exceptionInfo);
    if(dynlen(exceptionInfo) <= 0) {
  // refresh the parent
      unTreeWidget_modifyLabel(bClipboard, sGraphTree, sDpName, sDisplayNodeName, sNewDisplayNodeName, 
                    sParentNodeName+unTreeWidget_labelSeparator+sParentDpName, 
                    sParentKey, sSelectedKey, sCurrentSelectedNodeKey, sParentKeyToAddInNewKey, exceptionInfo);
    }
  }
  else {
        fwException_raise(exceptionInfo, "ERROR", getCatStr("unTreeWidget", "ROOTCLIPNODE"), "");
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_reorder
/**
Purpose: reorder the children of the node

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the node name of sDpName
  @param sParentDpName: string, input: data point name of the parent
  @param sParentNodeName: string, input: the node name of sParentDpName
  @param bClipboard: bool, input: show clipboard
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_reorder(string sGraphTree, string sSelectedKey, string sDpName, 
              string sDisplayNodeName, string sParentDpName, string sParentNodeName, bool bClipboard, dyn_string &exceptionInfo)
{
  dyn_string ret, children;
  dyn_float res;
  string sCorrectNodeName=_fwTree_getNodeName(sDpName);
  shape aShape;
  
//DebugN("unTreeWidget_reorder", sDisplayNodeName, sCorrectNodeName, sDpName);

  fwTree_getChildren(sCorrectNodeName, children, exceptionInfo);
  unGraphicalFrame_ChildPanelOnReturn("fwTreeDisplay/fwTreeReorderNodeStd.pnl","Re-order Children",
    makeDynString(sCorrectNodeName),
    100,60, res, ret);
  if(dynlen(res)>0)
  {
    if(res[1] == 1) {
      if(children != ret)
      {
  // remove the children from the unTreeWidget
      aShape = getShape(sGraphTree);
      unTreeWidget_setPropertyValue(aShape, "deleteChildrenNode", sSelectedKey, true);
  
  // reorder the children
        fwTree_reorderChildren(sCorrectNodeName, ret, exceptionInfo);
  // add the children.
  
        unTreeWidget_showChildren(true, bClipboard, sGraphTree, sDpName, sSelectedKey, sDisplayNodeName, exceptionInfo);
      }
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_checkNodeName
/**
Purpose: check if the node name is correct

  @param sNodeName: string, input: the node name of sDpName
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_checkNodeName(string sNodeName, dyn_string &exceptionInfo)
{
  if(!unConfigGenericFunctions_nameCheck(sNodeName)) {
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unTreeWidget", "WRONGNODENAME"), "");
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_showText
/**
Purpose: show the text on top of the unTreeWidget

  @param sText1: string, input: first text to set
  @param sText2: string, input: second text to set

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_showText (string sText1, string sText2)
{
  sTreeWidgetText1.text = sText1;
  sTreeWidgetText2.text = sText2;
  sTreeWidgetText1.visible = true;
  sTreeWidgetText2.visible = true;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_hideText
/**
Purpose: hide the text on top of the unTreeWidget

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeWidget_hideText ()
{
  sTreeWidgetText1.visible = false;
  sTreeWidgetText2.visible = false;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getPropertyValue
/**
Purpose:
Get the property of the tree widget.

  @param sTree: anytype, input: name/shape of the tree widget
  @param sProperty: string, input: property name
  @param bShape: bool, input: true = sTree is a shape, false=sTree is the name of the tree widget
  @param return value: anytype, property value

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
anytype unTreeWidget_getPropertyValue(anytype sTree, string sProperty, bool bShape=false, string sParam="")
{
  shape shTreeShape;
        dyn_string dsNodeId;
        string sTemp;
        string sCompositeLabel, sParentKey;
        string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName;
        int iCursor;
        
//DebugTN("---> unTreeWidget_getPropertyValue", sProperty, sParam);
  if(bShape)
    shTreeShape = sTree;
  else
    shTreeShape = getShape(sTree);
  switch(sProperty) {
    case "get_bRenameRootNodeAllowed":
            return bRenameRootNodeAllowed_graph.state(0);
//      return shTreeShape.get_bRenameRootNodeAllowed();
      break;
          case "get_bClipboard":
            return bClipboard_graph.state(0);
//      return shTreeShape.get_bClipboard();
      break;
          case "getAllNodeCompositeLabel":
            unTreeWidget_getAllNodeInTree(dsNodeId);
            return unTreeWidget_getCompositeLabel(dsNodeId);
//      return shTreeShape.getAllNodeCompositeLabel();
            break;
    case "getAllParentCompositeLabelOfSelectedNode":
            unTreeWidget_getParentOfNode(shTreeShape, shTreeShape.selectedItem(), dsNodeId);
            return unTreeWidget_getCompositeLabel(dsNodeId);
//      return shTreeShape.getAllParentCompositeLabelOfSelectedNode();
      break;
    case "getAllParentCompositeLabelOfNode":
            if(shTreeShape.itemExists(sParam))
              unTreeWidget_getParentOfNode(shTreeShape, sParam, dsNodeId);
            return unTreeWidget_getCompositeLabel(dsNodeId);
//      return shTreeShape.getAllParentCompositeLabelOfNode(sParam);
      break;
    case "getChildrenCompositeLabelOfSelectedNode":
            dsNodeId = shTreeShape.children(shTreeShape.selectedItem());
            return unTreeWidget_getCompositeLabel(dsNodeId);
//      return shTreeShape.getChildrenCompositeLabelOfSelectedNode();
      break;
    case "getChildrenCompositeLabelOfNode":
            if(shTreeShape.itemExists(sParam))
              dsNodeId = shTreeShape.children(sParam);
            return unTreeWidget_getCompositeLabel(dsNodeId);
//      return shTreeShape.getChildrenCompositeLabelOfNode(sParam);
      break;
          case "getFirstChildrenOfNode":
            if(shTreeShape.itemExists(sParam))
              dsNodeId = shTreeShape.children(sParam);
            if(dynlen(dsNodeId) > 0)
              return unTreeWidget_getCompositeLabel(makeDynString(dsNodeId[1]));
            else 
              return "";
//      return shTreeShape.getFirstChildrenOfNode(sParam);
      break;
    case "getHeight":
            return shTreeShape.height();
//      return shTreeShape.getHeight();
      break;
          case "getWidth":
            return shTreeShape.width();
//      return shTreeShape.getWidth();
      break;
          case "getMousePointer":
            iCursor = iCursorType_graph.text();
      return iCursor;
      break;
    case "getNodeNumber":
            return shTreeShape.childCount();
      break;
    case "getNodeSelectedkey":
            return shTreeShape.selectedItem();
//      return shTreeShape.getNodeSelectedkey();
      break;
    case "selectedCompositeLabel":
            sTemp = shTreeShape.selectedItem();
            unTreeWidget_getNodeCompositeLabelAndParentKey(sTemp, sCompositeLabel, sParentKey);
            return sCompositeLabel;
//      return shTreeShape.selectedCompositeLabel();
      break;
//    case "selectedIndex":
//      return shTreeShape.selectedIndex();
//      break;
    case "selectedKey":
            return shTreeShape.selectedItem();
//      return shTreeShape.selectedKey();
      break;
    case "selectedLabel":
            sTemp = shTreeShape.selectedItem();
            unTreeWidget_getNodeCompositeLabelAndParentKey(sTemp, sCompositeLabel, sParentKey);
            unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
            return sDisplayNodeName;
//      return shTreeShape.selectedLabel();
      break;
//    case "selectedNewLabel":
//      return shTreeShape.selectedNewLabel();
//      break;
//    case "selectedNodeKey":
//      return shTreeShape.selectedNodeKey();
//      break;
    case "selectedParent":
            return shTreeShape.parent(shTreeShape.selectedItem());
//      return shTreeShape.selectedParent();
      break;
    default:
DebugN("**************** unTreeWidget_getPropertyValue", sProperty, "not supported");
            return "";
      break;
  }
//DebugTN("---> end unTreeWidget_getPropertyValue", sProperty, sParam);
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getParentOfNode
/**
Purpose:
Get all the node of the tree widget.

  @param shTreeShape: shape, input: the tree shape
  @param sNodeId: string, input: the node
  @param dsParentId: dyn_string, output: list of parent of the node
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_getParentOfNode(shape shTreeShape, string sNodeId, dyn_string &dsParentId)
{
  dyn_string dsTemp;
  string sParent;

//  DebugN("unTreeWidget_getParentOfNode", sNodeId);  
//  if(sNodeId != "")
  if(shTreeShape.itemExists(sNodeId))
    sParent = shTreeShape.parent(sNodeId);
//DebugN("unTreeWidget_getParentOfNode", sNodeId, sParent);  
  while(sParent!= "") {
    dynAppend(dsTemp, sParent);
    sParent = shTreeShape.parent(sParent);
//DebugN("-->", sParent);  
  }
  
  dsParentId = dsTemp;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getAllNodeInTree
/**
Purpose:
Get all the node of the tree widget.

  @param dsNodeId: dyn_string, output: list of node in the tree
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_getAllNodeInTree(dyn_string &dsNodeId)
{
//  DebugN("unTreeWidget_getAllNodeInTree");
  dsNodeId = unTreeWidget_getNodeList();
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getCompositeLabel
/**
Purpose:
Get the composite label of nodes.

  @param dsNodeId: dyn_string, output: list of node in the tree
  @param return: string, output: list of composite label separated by @
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
string unTreeWidget_getCompositeLabel(dyn_string dsNodeId)
{ // unTreeWidget_keySeparator
  int i, len=dynlen(dsNodeId);
  string sResult;
  string sNodeCompositelabel, sParentNodeKey;
  
//DebugN("unTreeWidget_getCompositeLabel", dsNodeId);
  for(i=1;i<=len;i++) {
    unTreeWidget_getNodeCompositeLabelAndParentKey(dsNodeId[i], sNodeCompositelabel, sParentNodeKey);
    if(sResult == "")
      sResult = sNodeCompositelabel;
    else
      sResult += unTreeWidget_keySeparator+sNodeCompositelabel;
  }
  return sResult;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setPropertyValue
/**
Purpose:
Set the property of the tree widget.

  @param sTree: anytype, input: name/shape of the tree widget
  @param sProperty: string, input: property name
  @param sPropertyValue: anytype, input: the property value
  @param bShape: bool, input: true = sTree is a shape, false=sTree is the name of the tree widget
  @param sParam: string, input: parameter value 
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setPropertyValue(anytype sTree, string sProperty, anytype sPropertyValue, bool bShape=false, anytype sParam="")
{
  shape shTreeShape;
  int iProperty;
  dyn_string dsTemp;
        string sClass;
        dyn_string dsParentId, dsChildren, dsExpandedNodeId;
        string sSelectedNodeId, sOldSelectedNodeId;
        bool bProperty;
        bool bEvaluate;
        string sTreeValue;
  
//DebugTN("---> unTreeWidget_setPropertyValue", sProperty, sPropertyValue, sParam);
        switch(getType(sTree)) {
          case STRING_VAR:
            sTreeValue = sTree;
            if(sTreeValue != "")
              bEvaluate = true;
            break;
          case SHAPE_VAR:
            bEvaluate=true;
            break;
          default:
            break;
        }
        if(bEvaluate) {
          if(bShape)
    shTreeShape = sTree;
    else
    shTreeShape = getShape(sTree);
        }
  switch(sProperty) {
/*    case "addToTextLabelOfNode":
      shTreeShape.addToTextLabelOfNode(sPropertyValue, sParam);
      break;
    case "mouseEvt":
      shTreeShape.mouseEvt(sPropertyValue);
      break;
*/
          case "addImageClass":
            dsTemp = strsplit(sPropertyValue, ";");
            if(dynlen(dsTemp) == unTreeWidget_classTypeLength){
              sClass = dsTemp[1];
              dynRemove(dsTemp, 1);
              g_m_unTreeWidget_dsImage[sClass]=dsTemp;
            }
//DebugN(sProperty, g_m_unTreeWidget_dsImage);
            break;
    case "addNode":
            unTreeWidget_addNodeInTree(shTreeShape, sPropertyValue, false);
//      shTreeShape.addNode(sPropertyValue);
      break;
    case "addTopNode":
            unTreeWidget_addNodeInTree(shTreeShape, sPropertyValue, true);
//      shTreeShape.addTopNode(sPropertyValue);
      break;
          case "clearTree":
            unTreeWidget_setExpandedNodeId(makeDynString());
            unTreeWidget_setParentIdOfSelectedNode(makeDynString());
            unTreeWidget_setNodeList(makeDynString());
            unTreeWidget_setNodeListClass(makeDynString());
          case "clearTreeOnly":
      shTreeShape.clear();
            unTreeWidget_setSelectedNodeId("");
            sOldSelectedNodeId_graph.text = "";
          break;
    case "deleteChildrenNode":
            dsParentId = unTreeWidget_getParentIdOfSelectedNode();
            unTreeWidget_getSelectedNodeId(sSelectedNodeId, sOldSelectedNodeId);
            bProperty = false;
            // if current node is in list of path node, set it to selected
            if(dynContains(dsParentId, sPropertyValue)) {
              // --> if so change icons of all parents
              unTreeWidget_resetAllParentIcons(shTreeShape, dsParentId);
              // set current selected and old selected to ""
              unTreeWidget_setSelectedNodeId("");
              sOldSelectedNodeId_graph.text = "";
              // set node to selected.
              bProperty = true;
            }
            else if(sPropertyValue == sSelectedNodeId) {
              // if so set the icon to children selected
              unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(sPropertyValue), UN_TREEWIDGET_ICON_NOCHILDREN_SELECTED, sPropertyValue);
            }
            else {
              // if so set the icon to no children selected
              unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(sPropertyValue), UN_TREEWIDGET_ICON_NOCHILDREN_NOTSELECTED, sPropertyValue);
            }
            // get the children identifiers
            dsChildren = shTreeShape.children(sPropertyValue);
            // remove the node from all list
            unTreeWidget_removeFromList(dsChildren, "dsExpandedNodeId_graph", "");
            unTreeWidget_removeFromList(dsChildren, "dsParentIdOfSelectedNode_graph", "");
            unTreeWidget_removeFromList(dsChildren, "dsNodeList_graph", "dsNodeListClass_graph");
            // delete the node 
            shTreeShape.setExpandable(sPropertyValue, false);
            shTreeShape.removeItems(dsChildren);
            if(bProperty) {
              shTreeShape.setSelectedItem(sPropertyValue, true);
            }
//      shTreeShape.deleteChildrenNode(sPropertyValue);
      break;
    case "deleteNode":
            // get check if current node is in the list of path nodes or is the selected node
            // if so change icon of all from list
            // set selectedNode to ""
            // delete the node 
            // let selectionChange applied
            if(shTreeShape.itemExists(sPropertyValue))
              unTreeWidget_deleteNodeFromTree(false, sPropertyValue, shTreeShape);
//      shTreeShape.deleteNode(sPropertyValue);
            break;
    case "deleteNodeWithNoEvent":
            // set unTree to not run the click/nodeLeftClick evt of selectedChange
            // get check if current node is in the list of path nodes or is the selected node
            // if so change icon of all from list
            // set selectedNode to ""
            // delete the node 
            // let selectionChange applied
            if(shTreeShape.itemExists(sPropertyValue)){
              unTreeWidget_getSelectedNodeId(sSelectedNodeId, sOldSelectedNodeId);
              if(sSelectedNodeId == sPropertyValue)
                unTreeWidget_deleteNodeFromTree(true, sPropertyValue, shTreeShape);
              else
                unTreeWidget_deleteNodeFromTree(false, sPropertyValue, shTreeShape);
  //      shTreeShape.deleteNodeWithNoEvent(sPropertyValue);
            }
      break;
          case "Enabled":
            bProperty = sPropertyValue;
            shTreeShape.enabled(bProperty);
//      shTreeShape.Enabled(sPropertyValue);
            break;
    case "setImage":
// sKey+unTreeWidget_classSeparator+\"FOLDER\"+unTreeWidget_classSeparator+\"CLOSED\"+unTreeWidget_classSeparator+\"SELECTED\"
            dsTemp = strsplit(sPropertyValue, unTreeWidget_classSeparator);
            shTreeShape.setExpandable(dsTemp[1], true);
            unTreeWidget_getSelectedNodeId(sSelectedNodeId, sOldSelectedNodeId);
            if(sSelectedNodeId == dsTemp[1])
              unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(dsTemp[1]), UN_TREEWIDGET_ICON_CHILDREN_SELECTED, dsTemp[1]);
            else
              unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(dsTemp[1]), UN_TREEWIDGET_ICON_CHILDREN_CLOSED, dsTemp[1]);
//      shTreeShape.setImage(sPropertyValue);
      break;
    case "setNodeState_selected":
            bDoNothing_graph.state(0) = true;
            shTreeShape.setSelectedItem(sPropertyValue, false);
            shTreeShape.setSelectedItem(sPropertyValue, true);
//DebugN("!!!!!!!!!!!!!!!setNodeState_selected", sSelectedNodeId_graph.text());
//      shTreeShape.setNodeState_selected(sPropertyValue);
      break;
    case "setNodeState_notSelected":
            shTreeShape.setSelectedItem(sPropertyValue, false);
//      shTreeShape.setNodeState_notSelected(sPropertyValue);
      break;
          case "set_bNodeNotRemovedOnCollapseExpand": 
            bProperty = sPropertyValue;
            bNodeNotRemovedOnCollapseExpand_graph.state(0, bProperty);
//      shTreeShape.set_bNodeNotRemovedOnCollapseExpand(sPropertyValue);
       break;
          case "set_bClipboard":
            bProperty = sPropertyValue;
            bClipboard_graph.state(0, bProperty);
//      shTreeShape.set_bClipboard(sPropertyValue);
      break;
    case "set_bRenameRootNodeAllowed":
            bProperty = sPropertyValue;
            bRenameRootNodeAllowed_graph.state(0, bProperty);
//      shTreeShape.set_bRenameRootNodeAllowed(sPropertyValue);
      break;
          case "setMousePointer": 
      iProperty = sPropertyValue;
      if(iProperty == unTreeWidget_ccHourglass) // busy pointer
        shTreeShape.cursor(CURSOR_WAIT);
      else
        shTreeShape.cursor(iProperty);
      break;
          case "setTagAndImage":
// sDpNameKey+unTreeWidget_classSeparator+unTreeWidget_getIconClass(sDpName)
            dsTemp = strsplit(sPropertyValue, unTreeWidget_classSeparator);
            dsParentId = unTreeWidget_getParentIdOfSelectedNode();
            unTreeWidget_getSelectedNodeId(sSelectedNodeId, sOldSelectedNodeId);
            dsExpandedNodeId = unTreeWidget_getExpandedNodeId();
            // if node in list of parents --> set icon path
            if(dynContains(dsParentId, dsTemp[1]) > 0)
              unTreeWidget_setIcon(shTreeShape, dsTemp[2], UN_TREEWIDGET_ICON_CHILDREN_PATH, dsTemp[1]);
            else if(dsTemp[1] == sSelectedNodeId) {
              // else if node is selected & is expandable --> set children selected
              // else if node is selected & is not expandable --> set children not selected
              if(shTreeShape.isExpandable(dsTemp[1]))
                unTreeWidget_setIcon(shTreeShape, dsTemp[2], UN_TREEWIDGET_ICON_CHILDREN_SELECTED, dsTemp[1]);
              else
                unTreeWidget_setIcon(shTreeShape, dsTemp[2], UN_TREEWIDGET_ICON_NOCHILDREN_SELECTED, dsTemp[1]);
            }
            else if(shTreeShape.isExpandable(dsTemp[1])) {
            // else if node is expandable & expanded --> set opened
            // else if node is expandable & closed --> set closed
              if(dynContains(dsExpandedNodeId, dsTemp[1])>0)
                unTreeWidget_setIcon(shTreeShape, dsTemp[2], UN_TREEWIDGET_ICON_CHILDREN_OPENED, dsTemp[1]);
              else
                unTreeWidget_setIcon(shTreeShape, dsTemp[2], UN_TREEWIDGET_ICON_CHILDREN_CLOSED, dsTemp[1]);
            }
            // else --> set no children
            else
              unTreeWidget_setIcon(shTreeShape, dsTemp[2], UN_TREEWIDGET_ICON_NOCHILDREN_NOTSELECTED, dsTemp[1]);
            unTreeWidget_setClass(dsTemp[1], dsTemp[2]);
//                  shTreeShape.setTagAndImage(sPropertyValue);
            break;
    case "sort":
//             aShape.clearSelection();
            switch((int)sPropertyValue){
                case unTreeWidget_popupMenu_sortAZ:
                  shTreeShape.sortingEnabled = true;
                  shTreeShape.setSorting(0,true);   
                break;
                case unTreeWidget_popupMenu_sortZA:
                  shTreeShape.sortingEnabled = true;
                  shTreeShape.setSorting(0,false);   
                break;
                case unTreeWidget_popupMenu_sortCustom:
                  shTreeShape.sortingEnabled = false;  
                break;
            }         
      break;
            
    default:
      DebugTN("**************** unTreeWidget_setPropertyValue", sProperty, "not supported");
    break;
  }
//DebugTN("---> end unTreeWidget_setPropertyValue", sProperty, sPropertyValue, sParam);
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_deleteNodeFromTree
/**
Purpose:
Delete a node from the tree

  @param bNoTriggerEvent: bool, input: true=do not trigger event/false=trigger event
  @param sNodeDoDelete: string, input: identifier of the node to delete
  @param shTreeShape: shape, input: shape of the tree
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_deleteNodeFromTree(bool bNoTriggerEvent, string sNodeDoDelete, shape shTreeShape)
{
  dyn_string dsParentId;
  string sSelectedNodeId, sOldSelectedNodeId;
  dyn_string dsChildren;

//DebugN("unTreeWidget_deleteNodeFromTree", bNoTriggerEvent, sNodeDoDelete);  
  unTreeWidget_setTriggerEvent(bNoTriggerEvent);
  dsParentId = unTreeWidget_getParentIdOfSelectedNode();
  unTreeWidget_getSelectedNodeId(sSelectedNodeId, sOldSelectedNodeId);
  // get check if current node is in the list of path nodes or is the selected node
  if((dynContains(dsParentId, sNodeDoDelete) > 0) || (sNodeDoDelete == sSelectedNodeId)) {
    // if so change icon of all from list
    unTreeWidget_resetAllParentIcons(shTreeShape, dsParentId);
    // set selectedNode to ""
    unTreeWidget_setSelectedNodeId("");
    sOldSelectedNodeId_graph.text = "";
  }
  // set icon of parent if one child
  sSelectedNodeId = shTreeShape.parent(sNodeDoDelete);
  dsChildren = shTreeShape.children(sSelectedNodeId);
  if(dynlen(dsChildren)==1) {
    unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(sSelectedNodeId), UN_TREEWIDGET_ICON_NOCHILDREN_NOTSELECTED, sSelectedNodeId);  
    shTreeShape.setExpandable(sSelectedNodeId, false);
  }
  // remove the node from all list
  unTreeWidget_removeFromList(makeDynString(sNodeDoDelete), "dsExpandedNodeId_graph", "");
  unTreeWidget_removeFromList(makeDynString(sNodeDoDelete), "dsParentIdOfSelectedNode_graph", "");
  unTreeWidget_removeFromList(makeDynString(sNodeDoDelete), "dsNodeList_graph", "dsNodeListClass_graph");
  // delete the node 
  shTreeShape.removeItems(makeDynString(sNodeDoDelete));
  // let selectionChange applied
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_removeFromList
/**
Purpose:
Remove the node Ids from the list

  @param dsNodeDoDelete: string, input: list of node Id to remove
  @param sList1: string, input: list name
  @param sList2: string, input: list name
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_removeFromList(dyn_string dsNodeDoDelete, string sList1, string sList2)
{
   int iPos;
   dyn_string dsTemp;
   int i, len=dynlen(dsNodeDoDelete);
   dyn_string dsList1, dsList2;
   
//   DebugN("unTreeWidget_removeFromList", dsNodeDoDelete, sList1, sList2);
   if(sList1 != "")
     dsList1 = _unTreeWidget_getFromList(sList1);
   if(sList2 != "")
     dsList2 = _unTreeWidget_getFromList(sList2);
   for(i=1;i<=len;i++) {
     iPos = dynContains(dsList1, dsNodeDoDelete[i]);
     if(iPos > 0) {
       dynRemove(dsList1, iPos);
       if(sList2 != "")
         dynRemove(dsList2, iPos);
     }
   }
   if(sList1 != "")
     _unTreeWidget_setFromList(sList1, dsList1);
   if(sList2 != "")
     _unTreeWidget_setFromList(sList2, dsList2);
}

//------------------------------------------------------------------------------------------------------------------------

// _unTreeWidget_getFromList
/**
Purpose:
Get from internal list

  @param sList: string, input: the list name
  @param return: dyn_string, output: the list
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
dyn_string _unTreeWidget_getFromList(string sList)
{
  dyn_string ds;
  switch(sList)
  {
    case "dsExpandedNodeId_graph":
      ds = unTreeWidget_getExpandedNodeId();
      break;
    case "dsParentIdOfSelectedNode_graph":
      ds = unTreeWidget_getParentIdOfSelectedNode();
      break;
    case "dsNodeList_graph":
      ds = unTreeWidget_getNodeList();
      break;
    case "dsNodeListClass_graph":
      ds = unTreeWidget_getNodeListClass();
      break;
    default:
      break;
  }
  return ds;
}

//------------------------------------------------------------------------------------------------------------------------

// _unTreeWidget_setFromList
/**
Purpose:
Get from internal list

  @param sList: string, input: the list name
  @param dsList: dyn_string, input: the list
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
_unTreeWidget_setFromList(string sList, dyn_string dsList)
{
  switch(sList)
  {
    case "dsExpandedNodeId_graph":
      unTreeWidget_setExpandedNodeId(dsList);
      break;
    case "dsParentIdOfSelectedNode_graph":
      unTreeWidget_setParentIdOfSelectedNode(dsList);
      break;
    case "dsNodeList_graph":
      unTreeWidget_setNodeList(dsList);
      break;
    case "dsNodeListClass_graph":
      unTreeWidget_setNodeListClass(dsList);
      break;
    default:
      break;
  }
}
//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_resetAllParentIcons
/**
Purpose:
Reset all parent Icons

  @param shTreeShape: shape, input: shape of the tree
  @param dsParentId: string, input: list of parent identifier
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_resetAllParentIcons(shape shTreeShape, dyn_string dsParentId)
{
  int i, len=dynlen(dsParentId);
  dyn_string dsExpandedNodeId = unTreeWidget_getExpandedNodeId();
  
//DebugN("unTreeWidget_resetAllParentIcons", dsParentId);
  for(i=1;i<=len;i++) {
    if(dynContains(dsExpandedNodeId, dsParentId[i]) > 0)
      unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(dsParentId[i]), UN_TREEWIDGET_ICON_CHILDREN_OPENED, dsParentId[i]);  
    else
      unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(dsParentId[i]), UN_TREEWIDGET_ICON_CHILDREN_CLOSED, dsParentId[i]);  
  }
  unTreeWidget_setParentIdOfSelectedNode(makeDynString());
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setTriggerEvent
/**
Purpose:
Set the triger event boolean

  @param bNoTriggerEvent: bool, input: true=do not trigger event/false=trigger event
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setTriggerEvent(bool bNoTriggerEvent=false)
{
//  DebugN("unTreeWidget_setTriggerEvent", bNoTriggerEvent);
  bDoNotTriggerEvent_graph.state(0) = bNoTriggerEvent;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setAllParentIcon
/**
Purpose:
Set all the parent id of the current selected node

  @param shTreeShape: shape, input: shape of the tree
  @param dsParentId: dyn_string, output: the list of parent id
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setAllParentIcon(shape shTreeShape, dyn_string dsParentId)
{
  int i, len=dynlen(dsParentId);

//  DebugN("unTreeWidget_setAllParentIcon", dsParentId);  
  for(i=1;i<=len;i++) {
    unTreeWidget_setIcon(shTreeShape, unTreeWidget_getClass(dsParentId[i]), UN_TREEWIDGET_ICON_CHILDREN_PATH, dsParentId[i]);  
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setSelectedNodeId
/**
Purpose:
Set the current node Id to the selected one

  @param sSelectedNodeId: string, input: the node identifier

  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setSelectedNodeId(string sSelectedNodeId)
{
//DebugN("unTreeWidget_setSelectedNodeId", sSelectedNodeId);
  sOldSelectedNodeId_graph.text = sSelectedNodeId_graph.text;
  sSelectedNodeId_graph.text = sSelectedNodeId;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getSelectedNodeId
/**
Purpose:
Get the current selected node Id

  @param sSelectedNodeId: string, output: the current selected node identifier
  @param sOldSelectedNodeId: string, output: the previous selected node identifier
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_getSelectedNodeId(string &sSelectedNodeId, string &sOldSelectedNodeId)
{
//  DebugN("unTreeWidget_getSelectedNodeId");
  sOldSelectedNodeId = sOldSelectedNodeId_graph.text;
  sSelectedNodeId = sSelectedNodeId_graph.text;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getIcon
/**
Purpose:
Get the icon file list of the node

  @param sNodeKey: string, input: the node identifier
  @param dsIcon: dyn_string, output: the list of icon files
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_getIcon(string sNodeKey, dyn_string &dsIcon) synchronized(g_m_unTreeWidget_dsImage)
{
  string sClass;
  
//  DebugN("unTreeWidget_getIcon", sNodeKey);
  sClass = unTreeWidget_getClass(sNodeKey);
  if(mappingHasKey(g_m_unTreeWidget_dsImage, sClass))
    dsIcon = g_m_unTreeWidget_dsImage[sClass];
  else
    dsIcon = makeDynString("", "", "", "", "", "");
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getClass
/**
Purpose:
Get class type of the node

  @param sNodeKey: string, input: the node identifier
  @param return value: string, output: return the class of the node 
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
string unTreeWidget_getClass(string sNodeKey)
{
  string sClass;
  dyn_string dsNodeListAdded, dsClass;
  int iPos;
  
  dsNodeListAdded = unTreeWidget_getNodeList();
  dsClass = unTreeWidget_getNodeListClass();
  iPos = dynContains(dsNodeListAdded, sNodeKey);
  if(iPos > 0) 
    sClass = dsClass[iPos];
  return sClass;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setClass
/**
Purpose:
set class type of the node

  @param sNodeKey: string, input: the node identifier
  @param sClass: string, input: the class of the node 
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setClass(string sNodeKey, string sClass) synchronized(g_m_unTreeWidget_dsImage)
{
  dyn_string dsNodeListAdded, dsClass;
  int iPos;
  
  dsNodeListAdded = unTreeWidget_getNodeList();
  dsClass = unTreeWidget_getNodeListClass();
  iPos = dynContains(dsNodeListAdded, sNodeKey);
  if(iPos > 0) {
    dsClass[iPos] = sClass;
  }
  else {
    dynAppend(dsNodeListAdded, sNodeKey);
    dynAppend(dsClass, sClass);
  }
  unTreeWidget_setNodeList(dsNodeListAdded);
  unTreeWidget_setNodeListClass(dsClass);
//  DebugN("unTreeWidget_setClass", sNodeKey, sClass);
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setIcon
/**
Purpose:
Get the icon list of the node

  @param shTree: shape, input: shape of the tree widget
  @param sClass: string, input: the class of the node
  @param iPosition: int, input: the position of the icon file to show
  @param sNodeKey: string, input: the node identifier
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setIcon(shape shTree, string sClass, int iPosition, string sNodeKey)
{
  string imageClassParameters;
  dyn_string dsIcon;
  
  if(mappingHasKey(g_m_unTreeWidget_dsImage, sClass)) {
    dsIcon = g_m_unTreeWidget_dsImage[sClass];
  }
  else {
    imageClassParameters = getPath(PICTURES_REL_PATH,"node/iconWithChildrenClose.bmp") + ";" +
                            getPath(PICTURES_REL_PATH,"node/iconWithChildrenOpen.bmp") + ";" +
                            getPath(PICTURES_REL_PATH,"node/iconWithChildrenSelected.bmp") + ";" +
                            getPath(PICTURES_REL_PATH,"node/iconWithChildrenPath.bmp") + ";" +
                            getPath(PICTURES_REL_PATH,"node/iconWithoutChildrenNotSelected.bmp") + ";" +
                            getPath(PICTURES_REL_PATH,"node/iconWithoutChildrenSelected.bmp");
    unTreeWidget_setPropertyValue("", "addImageClass", sClass+";" + imageClassParameters, true);
    dsIcon = g_m_unTreeWidget_dsImage[sClass];
  }
  if((iPosition<=UN_TREEWIDGET_ICON_MAX_FILE) && (iPosition >0))
    shTree.setIcon(sNodeKey, 0, dsIcon[iPosition]);
  else 
    shTree.setIcon(sNodeKey, 0, dsIcon[UN_TREEWIDGET_ICON_NOCHILDREN_NOTSELECTED]);
//DebugN("unTreeWidget_setIcon", sClass, iPosition, sNodeKey/*, dsIcon*/);
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getNodeCompisteLabelAndParentKey
/**
Purpose:
Extract from the node identifier the composite label of the node and the parent node identifier
the composite label is a|b;c|d, for WindowTree this is labelOfParent;dpParent|labelOfNode;dpNode

  @param sTree: anytype, input: name/shape of the tree widget
  @param sProperty: string, input: property name
  @param sPropertyValue: anytype, input: the property value
  @param bShape: bool, input: true = sTree is a shape, false=sTree is the name of the tree widget
  @param sParam: string, input: parameter value 
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_getNodeCompositeLabelAndParentKey(string sNodeKey, string &sNodeCompositelabel, string &sParentNodeKey)
{
  int iPos = strpos(sNodeKey, unTreeWidget_keySeparator);
  
//  DebugN("unTreeWidget_getNodeCompositeLabelAndParentKey", sNodeKey);
  if(iPos > 0) {
    sNodeCompositelabel = substr(sNodeKey, 0, iPos);
    sParentNodeKey = substr(sNodeKey, iPos + 1, strlen(sNodeKey) - iPos -1);
  }
  else {
    sNodeCompositelabel = sNodeKey;
    sParentNodeKey = "";
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_addNodeInTree
/**
Purpose:
Add the node in the tree

  @param shTree: shape, input: shape of the tree widget
  @param sNodeToAdd: string, input: the node identifier to add in the tree
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_addNodeInTree(shape shTree, string sNodeToAdd, bool bTop)
{
  string sNodeName, sCompleteNodeKey, sParentNodeKey, sClass, sNodeCompositelabel;
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName;
  dyn_string dsSplit;
  dyn_string dsNodeListAdded, dsClass;
  dyn_string dsExpandedNode, dsParentId;
  
//  DebugN("unTreeWidget_addNodeInTree", sNodeToAdd, bTop);
  dsSplit = strsplit(sNodeToAdd, "~");
  sClass = dsSplit[1];
  sCompleteNodeKey = dsSplit[2];

  // get the composite label and the parent idendifier of the node to add
  unTreeWidget_getNodeCompositeLabelAndParentKey(sCompleteNodeKey, sNodeCompositelabel, sParentNodeKey);
  if(bTop)
    sParentNodeKey = "";
  // get the label and dp of the node to add
  // get the parent label and parent dp of the node to add
  unTreeWidget_getNodeNameAndNodeDp(sNodeCompositelabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
//   @param sCompositeLabel: string, input: parentNodeName;parentDpNodeName|nodeName;dpNodeName or |nodeName;dpNodeName if root node
// DebugN("---->", sNodeToAdd);
// DebugN(sClass, sCompleteNodeKey, sNodeCompositelabel, sParentNodeKey);
// DebugN(sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
// DebugN("to ADD: ", sClass, sDisplayNodeName, sCompleteNodeKey, sParentNodeKey); 
  // add the node in the tree
  shTree.appendItem(sParentNodeKey, sCompleteNodeKey, sDisplayNodeName);
  if(sParentNodeKey!="") shTree.setDragEnabled(sCompleteNodeKey,true);
  // set icon of current node and parent node.
  unTreeWidget_setIcon(shTree, sClass, UN_TREEWIDGET_ICON_NOCHILDREN_NOTSELECTED, sCompleteNodeKey);
  if(sParentNodeKey != "") {
    // if in parents --> set path
    // if expanded --> set opened icon
    // if selected --> set children selected
    // else --> set children closed
    dsParentId = unTreeWidget_getParentIdOfSelectedNode();
    dsExpandedNode = unTreeWidget_getExpandedNodeId();
    if(dynContains(dsParentId, sParentNodeKey) > 0) 
      unTreeWidget_setIcon(shTree, unTreeWidget_getClass(sParentNodeKey), UN_TREEWIDGET_ICON_CHILDREN_PATH, sParentNodeKey);
    else if(sParentNodeKey == sSelectedNodeId_graph.text())
      unTreeWidget_setIcon(shTree, unTreeWidget_getClass(sParentNodeKey), UN_TREEWIDGET_ICON_CHILDREN_SELECTED, sParentNodeKey);
    else if(dynContains(dsExpandedNode, sParentNodeKey) > 0) 
      unTreeWidget_setIcon(shTree, unTreeWidget_getClass(sParentNodeKey), UN_TREEWIDGET_ICON_CHILDREN_OPENED, sParentNodeKey);
    else
      unTreeWidget_setIcon(shTree, unTreeWidget_getClass(sParentNodeKey), UN_TREEWIDGET_ICON_CHILDREN_CLOSED, sParentNodeKey);
    shTree.setExpandable(sParentNodeKey, true);
  }
  // keep the list of node added and its class if not already added
  dsNodeListAdded = unTreeWidget_getNodeList();
  dsClass = unTreeWidget_getNodeListClass();
  if(dynContains(dsNodeListAdded, sCompleteNodeKey) <= 0) {
    dynAppend(dsNodeListAdded, sCompleteNodeKey);
    dynAppend(dsClass, sClass);
    unTreeWidget_setNodeList(dsNodeListAdded);
    unTreeWidget_setNodeListClass(dsClass);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setSelectedIcon
/**
Purpose:
Add the node in the tree

  @param shTreeShape: shape, input: shape of the tree widget
  @param sId: string, input: the node identifier 
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setSelectedIcon(shape shTreeShape, string sId)
{
  string sClass;
  int iPos;
  dyn_string dsExpandedNode = unTreeWidget_getExpandedNodeId();
//  dyn_string dsChildren;
  
  sClass= unTreeWidget_getClass(sId);
  iPos = dynContains(dsExpandedNode, sId);

//  dsChildren = shTreeShape.children(sId);
//DebugN(sId, shTreeShape.isExpandable(sId));
//  if(dynlen(dsChildren) <= 0) {
  if(!shTreeShape.isExpandable(sId)) {
    if(sId == sSelectedNodeId_graph.text())
      unTreeWidget_setIcon(shTreeShape, sClass, UN_TREEWIDGET_ICON_NOCHILDREN_SELECTED, sId);
    else
      unTreeWidget_setIcon(shTreeShape, sClass, UN_TREEWIDGET_ICON_NOCHILDREN_NOTSELECTED, sId);
  }
  else {
    if(sId == sSelectedNodeId_graph.text())
      unTreeWidget_setIcon(shTreeShape, sClass, UN_TREEWIDGET_ICON_CHILDREN_SELECTED, sId);
    else {
      if(iPos > 0)
        unTreeWidget_setIcon(shTreeShape, sClass, UN_TREEWIDGET_ICON_CHILDREN_OPENED, sId);
      else
        unTreeWidget_setIcon(shTreeShape, sClass, UN_TREEWIDGET_ICON_CHILDREN_CLOSED, sId);
    }
  }
//DebugN("unTreeWidget_setSelectedIcon", shTreeShape.isExpandable(sId), sId, sSelectedNodeId_graph.text(), sId == sSelectedNodeId_graph.text());
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getExpandedNodeId
/**
Purpose:
Get the list of expanded node Id

  @param return: dyn_string, output: list of Id
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
dyn_string unTreeWidget_getExpandedNodeId() synchronized(g_m_unTreeWidget_dsExpandedNodeId)
{
  string sKey = myModuleName()+ myPanelName();
  dyn_string ds;
//DebugN(myModuleName(), myPanelName());
  if(mappingHasKey(g_m_unTreeWidget_dsExpandedNodeId, sKey))
    ds = g_m_unTreeWidget_dsExpandedNodeId[sKey];
  else
    DebugN("unTreeWidget_getExpandedNodeId not found", sKey);
     
//  return dsExpandedNodeId_graph.items();
  return ds;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setExpandedNodeId
/**
Purpose:
Set the list of expanded node Id

  @param dsExpandedNodeId: dyn_string, input: list of Id
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setExpandedNodeId(dyn_string dsExpandedNodeId) synchronized(g_m_unTreeWidget_dsExpandedNodeId)
{
  string sKey = myModuleName()+ myPanelName();
//DebugN(myModuleName(), myPanelName());
//    dsExpandedNodeId_graph.items() = dsExpandedNodeId;
//  if(mappingHasKey(g_m_unTreeWidget_dsExpandedNodeId, sKey))
//    DebugN("unTreeWidget_setExpandedNodeId not found", sKey);
  g_m_unTreeWidget_dsExpandedNodeId[sKey] = dsExpandedNodeId;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_removeFromExpandedNodeId
/**
Purpose:
Remove the key from the g_m_unTreeWidget_dsExpandedNodeId

  @param sKey: dyn_string, input: the key
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_removeFromExpandedNodeId(string sKey) synchronized(g_m_unTreeWidget_dsExpandedNodeId)
{
  if(mappingHasKey(g_m_unTreeWidget_dsExpandedNodeId, sKey)) {
//    g_m_unTreeWidget_dsExpandedNodeId[sKey] = makeDynString();
    mappingRemove(g_m_unTreeWidget_dsExpandedNodeId, sKey);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getNodeList
/**
Purpose:
Get the list of node Id

  @param return: dyn_string, output: list of Id
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
dyn_string unTreeWidget_getNodeList() synchronized(g_m_unTreeWidget_dsNodeList)
{
  string sKey = myModuleName()+ myPanelName();
  dyn_string ds;
//DebugN(myModuleName(), myPanelName());
  if(mappingHasKey(g_m_unTreeWidget_dsNodeList, sKey))
    ds = g_m_unTreeWidget_dsNodeList[sKey];
  else
    DebugN("unTreeWidget_getNodeList not found", sKey);
  return ds;
//  return dsNodeList_graph.items();
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setNodeList
/**
Purpose:
Set the list of node list Id

  @param dsNodeId: dyn_string, input: list of Id
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraint
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setNodeList(dyn_string dsNodeId) synchronized(g_m_unTreeWidget_dsNodeList)
{
  string sKey = myModuleName()+ myPanelName();
//DebugN(myModuleName(), myPanelName());
//  if(mappingHasKey(g_m_unTreeWidget_dsNodeList, sKey))
//    DebugN("unTreeWidget_setNodeList not found", sKey);
  g_m_unTreeWidget_dsNodeList[sKey] = dsNodeId;
//    dsNodeList_graph.items() = dsNodeId;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_removeFromNodeList
/**
Purpose:
Remove the key from the g_m_unTreeWidget_dsNodeList

  @param sKey: dyn_string, input: the key
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraint
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_removeFromNodeList(string sKey) synchronized(g_m_unTreeWidget_dsNodeList)
{
  if(mappingHasKey(g_m_unTreeWidget_dsNodeList, sKey)){
//    g_m_unTreeWidget_dsNodeList[sKey] = makeDynString();
    mappingRemove(g_m_unTreeWidget_dsNodeList, sKey);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getNodeListClass
/**
Purpose:
Get the list of node Id

  @param return: dyn_string, output: list of class
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
dyn_string unTreeWidget_getNodeListClass() synchronized(g_m_unTreeWidget_dsNodeListClass)
{
  string sKey = myModuleName()+ myPanelName();
  dyn_string ds;
//DebugN(myModuleName(), myPanelName());
  if(mappingHasKey(g_m_unTreeWidget_dsNodeListClass, sKey))
    ds = g_m_unTreeWidget_dsNodeListClass[sKey];
  else
    DebugN("unTreeWidget_getNodeListClass not found", sKey);
  return ds;
//  return dsNodeListClass_graph.items();
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setNodeListClass
/**
Purpose:
Set the list of node class

  @param dsNodeId: dyn_string, input: list of class
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setNodeListClass(dyn_string dsNodeListClass) synchronized(g_m_unTreeWidget_dsNodeListClass)
{
  string sKey = myModuleName()+ myPanelName();
//DebugN(myModuleName(), myPanelName());
//  if(mappingHasKey(g_m_unTreeWidget_dsNodeListClass, sKey))
//    DebugN("unTreeWidget_setNodeListClass not found", sKey);
  g_m_unTreeWidget_dsNodeListClass[sKey] = dsNodeListClass;
//    dsNodeListClass_graph.items() = dsNodeListClass;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_removeFromNodeListClass
/**
Purpose:
Remove the key from the g_m_unTreeWidget_dsNodeListClass

  @param sKey: dyn_string, input: the key
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_removeFromNodeListClass(string sKey) synchronized(g_m_unTreeWidget_dsNodeListClass)
{
  if(mappingHasKey(g_m_unTreeWidget_dsNodeListClass, sKey)) {
//    g_m_unTreeWidget_dsNodeListClass[sKey] = makeDynString();
    mappingRemove(g_m_unTreeWidget_dsNodeListClass, sKey);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_getParentIdOfSelectedNode
/**
Purpose:
Get the list of parent of selected node Id

  @param return: dyn_string, output: list of parent id
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
dyn_string unTreeWidget_getParentIdOfSelectedNode() synchronized(g_m_unTreeWidget_dsParentIdOfSelectedNode)
{
  string sKey = myModuleName()+ myPanelName();
  dyn_string ds;
//DebugN(myModuleName(), myPanelName());
  if(mappingHasKey(g_m_unTreeWidget_dsParentIdOfSelectedNode, sKey))
    ds = g_m_unTreeWidget_dsParentIdOfSelectedNode[sKey];
  else
    DebugN("unTreeWidget_getParentIdOfSelectedNode not found", sKey);
  return ds;
//  return dsParentIdOfSelectedNode_graph.items();
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_setParentIdOfSelectedNode
/**
Purpose:
Set the list of parent of the current selected node

  @param dsParentIdOfSelectedNode: dyn_string, input: list of parent
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_setParentIdOfSelectedNode(dyn_string dsParentIdOfSelectedNode) synchronized(g_m_unTreeWidget_dsParentIdOfSelectedNode)
{
  string sKey = myModuleName()+ myPanelName();
//DebugN(myModuleName(), myPanelName());
//  if(mappingHasKey(g_m_unTreeWidget_dsParentIdOfSelectedNode, sKey))
//    DebugN("unTreeWidget_setParentIdOfSelectedNode not found", sKey);
  g_m_unTreeWidget_dsParentIdOfSelectedNode[sKey]= dsParentIdOfSelectedNode;
//  dsParentIdOfSelectedNode_graph.items() = dsParentIdOfSelectedNode;
}

//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_removeFromParentIdOfSelectedNode
/**
Purpose:
Remove the key from the g_m_unTreeWidget_dsParentIdOfSelectedNode

  @param dsParentIdOfSelectedNode: dyn_string, input: list of parent
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . to be used with the WidgetTree
  . PVSS version: 3.6 
  . operating system: NT and Linux, but tested only under WXP.
  . distributed system: yes.
*/
unTreeWidget_removeFromParentIdOfSelectedNode(string sKey) synchronized(g_m_unTreeWidget_dsParentIdOfSelectedNode)
{
  if(mappingHasKey(g_m_unTreeWidget_dsParentIdOfSelectedNode, sKey)) {
//    g_m_unTreeWidget_dsParentIdOfSelectedNode[sKey]= makeDynString();
    mappingRemove(g_m_unTreeWidget_dsParentIdOfSelectedNode, sKey);
  }
}

//------------------------------------------------------------------------------------------------------------------------

//@}



/* added by Herve 19/07/2004 */
//fwTree_removeSubTree: 
/**  remove a sub tree from a Tree.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param parent: The name of the parent
      @param node: The start node of the sub tree
      @param exInfo: exception - returns a warning if node does not exist
*/
fwTree_removeSubTree(string parent, string node, dyn_string &exInfo)
{
  dyn_string children;
  int i;

  fwTree_getChildrenWithClipboard(node, children, exInfo);
  for(i = 1; i <= dynlen(children); i++)
  {
    fwTree_removeSubTree(node, children[i], exInfo);
  }
  fwTree_removeNode(parent, node, exInfo);
}

//fwTree_removeTree: 
/**  remove tree.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param node: The start node of the sub tree
      @param exInfo: exception - returns a warning if node does not exist
*/
fwTree_removeTree(string node, dyn_string &exInfo)
{
  dyn_string children;
  int i;
  string sDpNodeName = "fwTT_"+node;

  fwTree_getChildrenWithClipboard(node, children, exInfo);
  for(i = 1; i <= dynlen(children); i++)
  {
    fwTree_removeSubTree(node, children[i], exInfo);
  }
  if(dynlen(exInfo) <= 0) {
    fwTree_removeNode("", node, exInfo);
    if(dpExists(sDpNodeName)) {
      dpDelete(sDpNodeName);
    }
    else {
      _fwTree_exception(exInfo,"WARNING","fwTree_removeTree(): tree "+
      node+" does not exist");
    }
  }
}

//fwTree_copyNode:
/**  copy a node and put in parent.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param sNodeToCopy: The node to copy, the real name
      @param sDisplayNodeName: The display node name of the copy of sNodeToCopy
      @param sParentNode: The parent node to receive sNodeToCopy, the real parent node name
      @param sNodeName: The real node name of the copy of sNodeToCopy
      @param exInfo: exception - returns a warning if node does not exist
*/
fwTree_copyNode(string sNodeToCopy, string sDisplayNodeName, string sParentNode, string &sNodeName, dyn_string &exInfo)
{
  string new_node, newDp, sourceDp;
  int iError;

// create the node
  new_node = fwTree_createNode(sParentNode, sDisplayNodeName, exInfo);
  newDp = _fwTree_makeNodeName(new_node);
  if(dynlen(exInfo) <= 0) {
    sourceDp = _fwTree_makeNodeName(sNodeToCopy);
    dpCopyOriginal(sourceDp, newDp, iError);
    if(iError != 0)
      _fwTree_exception(exInfo,"WARNING","fwTree_copyNode(): error: "+iError+" copying: "+
          sNodeToCopy+" to "+sDisplayNodeName);
    else {
// put it into parent if parent="" (root) then do nothing
      fwTree_addNode(sParentNode, new_node, exInfo, 1);
    }
  }
  sNodeName = new_node;
}

//fwTree_copySubTree:
/**  copy a sub tree and put in parent.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param sNodeToCopy: The node to copy, the real node
      @param sDisplayNodeName: The node name of the copy of sNodeToCopy
      @param sParentNode: The parent node to receive sNodeToCopy
      @param sNodeName: The real node name of the copy of sNodeToCopy
      @param exInfo: exception - returns a warning if node does not exist
*/
fwTree_copySubTree(string sNodeToCopy, string sDisplayNodeName, string sParentNode, string &sNodeName, dyn_string &exInfo)
{
  dyn_string childrenNodes, copyChildrenDpNodes;
  int i, len;
  string sChildNodeName, sDisplayChildNodeName;

// create the node and put it into parent
  fwTree_copyNode(sNodeToCopy, sDisplayNodeName, sParentNode, sNodeName, exInfo);

  if(dynlen(exInfo) <= 0) {
// get all children without clipboard
    fwTree_getChildren(sNodeToCopy, childrenNodes, exInfo);
// call fwTree_copyNode for all the children
    len=dynlen(childrenNodes);
    for(i=1; i<=len; i++) {
      sDisplayChildNodeName = fwTree_getNodeDisplayName(childrenNodes[i], exInfo);
      if(dynlen(exInfo) <= 0) {
        fwTree_copySubTree(childrenNodes[i], sDisplayChildNodeName, sNodeName, sChildNodeName, exInfo);
        dynAppend(copyChildrenDpNodes, _fwTree_makeNodeName(sChildNodeName));
      }
    }
  }
// set the children if no error
  if(dynlen(exInfo) <= 0) {
    _fwTree_setChildren(_fwTree_makeNodeName(sNodeName), copyChildrenDpNodes);
  }
}

//fwTree_copyTree:
/**  copy a tree.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param sTreeToCopy: The tree to copy
      @param sTreeName: The tree name of the copy of sTreeToCopy
      @param exInfo: exception - returns a warning if node does not exist
*/
fwTree_copyTree(string sTreeToCopy, string sTreeName, dyn_string &exInfo)
{
  string sNodeName, clipboard, clipboardToCopy;
  string sTreeDp;
  int iError;
  dyn_string dsChildren;

// create the fwTT_ dp
  sTreeDp= "fwTT_"+sTreeName;
  dpCreate(sTreeDp,"_FwTreeType");
  if(dpExists(sTreeDp)) {
    dpCopyOriginal("fwTT_"+sTreeToCopy, sTreeDp, iError);
    dpSet(sTreeDp+".name", sTreeName);
    if(iError != 0)
      _fwTree_exception(exInfo,"WARNING","fwTree_copyTree(): error: "+iError+" copying: "+
          "fwTT_"+sTreeToCopy+" to "+sTreeDp);
  }
  else {
    _fwTree_exception(exInfo,"WARNING","fwTree_copyTree(): tree not created: "+sTreeName);
  }
  if(dynlen(exInfo) <= 0) {
// copy the sub tree 
    fwTree_copySubTree(sTreeToCopy, sTreeName, "", sNodeName, exInfo);
    if(sNodeName != sTreeName) {
      _fwTree_exception(exInfo,"WARNING","fwTree_copyTree(): error: tree result different: "+sTreeName+
                      " " +sNodeName);
    }
    if(dynlen(exInfo) <= 0) {
// create the clipboard 
      clipboard = _fwTree_makeClipboard(sTreeName);
// get the clipboard of sTreeToCopy and copy the content into the new clipboard with fwTree_copySubTree  
      clipboardToCopy = _fwTree_makeClipboard(sTreeToCopy);

      fwTree_copySubTree(clipboardToCopy , clipboard , clipboard , sNodeName, exInfo);
      if(sNodeName != clipboard) {
        _fwTree_exception(exInfo,"WARNING","fwTree_copyTree(): error: clipboard result different: "+clipboard+
                      " " +sNodeName);
      }
      if(dynlen(exInfo) <=0) {
// add the clipboard, added at the end 
        fwTree_addNode(sTreeName, clipboard, exInfo, 1); 
// put it first
        fwTree_getChildren(sTreeName, dsChildren, exInfo);
        iError = dynlen(dsChildren);
        if(iError > 0) {
          clipboard = dsChildren[iError];
          dynRemove(dsChildren, iError);
          dynInsertAt(dsChildren, clipboard, 1);
          fwTree_reorderChildren(sTreeName, dsChildren, exInfo);
        }
      }
    }
  }
}

/* end added by Herve 19/07/2004 */


/* added by Herve 12/08/2004 */
//fwTree_isInClipboard: 
/**  Check if node is in a Trees's clipboard.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param node: The name of the node to check
      @param exInfo: exception return
      @returns: 1 for yes, 0 for no
*/
bool fwTree_isInClipboard(string sNode, dyn_string exInfo)
{
  int pos;
  string parent, node = sNode;
  bool bResult = false, bContinue = true;

  while(bContinue) {
    if(fwTree_isClipboard(node, exInfo)) {
      bResult = true;
      bContinue = false;
//DebugN("fwTree_isInClipboard STOP", node);
    }
    else {
      fwTree_getParent(node, parent, exInfo);
//DebugN("****fwTree_isInClipboard conti", node, parent);
      if(parent == "")
        bContinue = false;
      else 
        node = parent;
    }
  }
//DebugN("fwTree_isInClipboard STOP", node, sNode, bResult);

  return bResult;
}
/* end added by Herve 12/08/2004 */

/* added by Herve 12/09/2004 */
//fwTree_getAllNodesByDevice: 
/**  Get all node having a given device name and device type by device name or device type.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param bDeviceName: input, bool, tring, true, search on device name, false search on device type
      @param sRemoteSystem: input, string, remote system name or * for all system name
      @param sNodeNamePattern: input, string, the node name pattern
      @param sDeviceName: input, string, the device name
      @param sDeviceType: input, string, the device type
      @param dsResult: output, dyn_string, the list of node linked to sDeviceName and sDeviceType
      @param exInfo: output, dyn_string, exception return
*/
fwTree_getAllNodesByDevice(bool bDeviceName, string sRemoteSystem, string sNodeNamePattern, string sDeviceName, string sDeviceType, dyn_string &dsResult, dyn_string &exInfo)
{
  string sRemoteString, sFromString, sWhereString, sQuery;
  int iId, i, len;
  bool bAllSystem = false;
  dyn_string dsSystemNames;
  dyn_uint diIds;
  dyn_dyn_anytype tabResult;
  dyn_string dsTempResult;
  
  if(sRemoteSystem != "") {
    if(sRemoteSystem != getSystemName()) {
      if(sRemoteSystem != "*") {
        iId = getSystemId(sRemoteSystem);
        if(iId > 0) {
          sRemoteString = " REMOTE '"+sRemoteSystem+"'";
        }
        else 
          fwException_raise(exInfo, "ERROR", "fwTree_getAllInstance: wrong systemName or missing ':' "+sRemoteSystem,"");
      }
      else
        bAllSystem = true;
    }
  }
  if(bDeviceName)
    sFromString = " FROM 'fwTN_"+sNodeNamePattern+".device'";
  else
    sFromString = " FROM 'fwTN_"+sNodeNamePattern+".type'";
  
  sWhereString = " WHERE _DPT =\"_FwTreeNode\" AND _LEAF AND '_original.._value' == \""+sDeviceName+"\"";
  if(dynlen(exInfo) <= 0) {
    if(bAllSystem) {
      iId = getSystemNames(dsSystemNames, diIds);
      len = dynlen(dsSystemNames);
      for(i=1; i<=len; i++) {
        sRemoteString = " REMOTE '"+dsSystemNames[i]+"'";
        sQuery = "SELECT '_original.._value' "+ sFromString + sRemoteString + sWhereString;
//        DebugN(sQuery);
        iId = dpQuery(sQuery, tabResult);
        if(iId < 0) {
          fwException_raise(exInfo, "ERROR", "fwTree_getAllInstance: bad result in dpQuery","");
          i = len+1;
        }
        else {
//          DebugN(i, tabResult);
          _fwTree_getAllInstanceResult(bDeviceName, tabResult, dsResult, sDeviceName, sDeviceType, exInfo);
          dynAppend(dsResult, dsTempResult);
          dsTempResult = makeDynString();
        }
      }
    }
    else {
      sQuery = "SELECT '_original.._value' "+ sFromString + sRemoteString + sWhereString;
//      DebugN(sQuery);
      iId = dpQuery(sQuery, tabResult);
      if(iId < 0) {
        fwException_raise(exInfo, "ERROR", "fwTree_getAllInstance: bad result in dpQuery","");
      }
      else {
//        DebugN(tabResult);
        _fwTree_getAllInstanceResult(bDeviceName, tabResult, dsResult, sDeviceName, sDeviceType, exInfo);
      }
    }
  }
}

//_fwTree_getAllInstanceResult: 
/**  retrieve from the dpQuery result all the node.
   
Usage: JCOP framework internal, public

PVSS manager usage: VISION, CTRL

      @param bDeviceName: input, bool, tring, true, search on device name, false search on device type
      @param tabResult: input, dyn_dyn_string, result of the dpQuery
      @param dsTempResult: output, dyn_string, the list of node linked to sDeviceName and sDeviceType
      @param sDeviceName: input, string, the device name
      @param sDeviceType: input, string, the device type
      @param exInfo: output, dyn_string, exception return
*/
_fwTree_getAllInstanceResult(bool bDeviceName, dyn_dyn_string tabResult, dyn_string &dsTempResult, string sDeviceName, string sDeviceType, dyn_string &exInfo)
{
  dyn_string dsTemp;
  int i, len;
  string sNodeName, sTempDeviceName, sTempDeviceType;
  
//  DebugN(tabResult);
  len = dynlen(tabResult);

  for(i=2;i<=len;i++) {
//    DebugN(tabResult[i][1]);
    sNodeName = _fwTree_getNodeName(dpSubStr(tabResult[i][1], DPSUB_SYS_DP));
    fwTree_getNodeDevice(sNodeName, sTempDeviceName, sTempDeviceType, exInfo);
    if(!bDeviceName) {
      if(sTempDeviceName == sDeviceName)
        dynAppend(dsTempResult, sNodeName);  
    }
    else {
      if(sTempDeviceType == sDeviceType)
        dynAppend(dsTempResult, sNodeName);  
    }
  }
}
/* end added by Herve 12/09/2004 */

