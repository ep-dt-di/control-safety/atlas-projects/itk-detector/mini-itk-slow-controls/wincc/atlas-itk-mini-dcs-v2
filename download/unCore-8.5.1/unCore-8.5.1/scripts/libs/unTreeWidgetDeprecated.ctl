//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_EventAfterLabelEdit
/**
Purpose:
Executes the EventAfterLabelEdit of the unTreeWidget used to view the hierarchy. This function modifies the labels 
of a node when one change the text shown in the unTreeWidget. This function must be used 
with the unTreeWidget. The unTreeWidget tree is disabled during the operation
This function is started in a separate thread.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.


@Deprecated 2018-08-14


*/
unTreeWidget_EventAfterLabelEdit(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard)
{

  FWDEPRECATED();

//  string oldLabel, newLabel, parentKey;
  dyn_string exceptionInfo;
/*  shape aShape;
  int id;
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sCurrentSelectedNodeKey;
  int iCurrentMousePointer, iMousePointer;
*/
    
  fwException_raise(exceptionInfo, "ERROR", "unTreeWidget_EventAfterLabelEdit:deprecated function, not implemented", "");
/*
// start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);
// disable the unTreeWidget to have one action at a time.
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);

  oldLabel = unTreeWidget_getPropertyValue(aShape, "selectedLabel", true);
  newLabel = unTreeWidget_getPropertyValue(aShape, "selectedNewLabel", true);
  parentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
  sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey", true);
  
  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
  unTreeWidget_modifyLabel(bClipboard, sGraphTree, sDpName, oldLabel, newLabel, 
                        sParentNodeName+unTreeWidget_labelSeparator+sParentDpName, 
                        parentKey, sSelectedKey, sCurrentSelectedNodeKey, parentKey, exceptionInfo);

  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
// enable the unTreeWidget
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
// stop the progress bar
  unProgressBar_stop(id, sBar);
*/
        
// handle any errors here
  if(dynlen(exceptionInfo)>0)
    fwExceptionHandling_display(exceptionInfo);
}







//------------------------------------------------------------------------------------------------------------------------

// unTreeWidget_EventNodeRigthClick
/**
Purpose:
Function executed when one does a right click on a node of the tree. popupMenuXY can be used to show a popup menu. 
WARNING: popuMenu cannot be used.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the unTreeWidget and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Deprecated 2018-08-14

*/
unTreeWidget_EventNodeRigthClick(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{

  FWDEPRECATED();

  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sParentKey, sTree, sCopiedNodeName, sNodeName;
  dyn_string dsMenu, exInfo, dsChildren;
  int res, ans, paste_flag;
  shape aShape;
  int id, pos;
  dyn_float dfFloat;
  dyn_string dsText;
  string sKey;
  int isClipboard, isRoot;
  string sCurrentSelectedNodeKey;
  bool isInClipboard;
  int iCurrentMousePointer, iMousePointer;
  
// start the progress bar
  id = unProgressBar_start(sBar);  
  aShape = getShape(sGraphTree);
// disable the unTreeWidget tree to have one action at a time.
  unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
  iCurrentMousePointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);

  sParentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
  sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey" ,true);

  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
  sNodeName = _fwTree_getNodeName(sDpName);
  isClipboard = fwTree_isClipboard(sNodeName, exInfo);
  isInClipboard = fwTree_isInClipboard(sNodeName, exInfo);
  isRoot = fwTree_isRoot(sNodeName, exInfo);
  
  // get the tree
  fwTree_getTreeName(sNodeName, sTree, exInfo);
  pos = dynContains(g_dsTree, sTree);
  
  // build the popup menu

  ////!!!!!!!!! take care with &000 when calling isRoot.

  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  if(!isClipboard)
    dynAppend(dsMenu,"PUSH_BUTTON, Add..., "+unTreeWidget_popupMenu_add+", 1");
  if(!isRoot) {
    if(!isClipboard) {
      dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", 1");
      dynAppend(dsMenu,"SEPARATOR");
      if(pos>0) {
        dynAppend(dsMenu,"PUSH_BUTTON, Cut: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_cut+", 1");
        dynAppend(dsMenu,"PUSH_BUTTON, Copy: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copy+", 1");
      }
    }
    else
      dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", 0");
  }
  else {
    dynAppend(dsMenu,"PUSH_BUTTON, Remove Tree, "+unTreeWidget_popupMenu_removeTree+", 1");
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Copy Tree: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copyTree+", 1");
  }
  if(pos > 0) {
    if((g_sPasteNodeName[pos] != "") && (!isInClipboard) && (g_sPasteNodeDpName[pos]!=sDpName))
      dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+g_sPasteNodeName[pos]+", "+unTreeWidget_popupMenu_paste+", 1");
    else
      dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");
  }
  else
    dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  if((!isRoot) && (!isClipboard))
    dynAppend(dsMenu,"PUSH_BUTTON, Rename, "+unTreeWidget_popupMenu_rename+", 1");

  fwTree_getChildren(sNodeName, dsChildren, exInfo);
  if(dynlen(dsChildren) > 0) {
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", 1");
  }

// show it
  res = popupMenuXY(dsMenu,iPosX, iPosY, ans);

  switch(ans) {
    case unTreeWidget_popupMenu_add:
      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Add Node", makeDynString("$sTextQuestion:New node name:"), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
    // new node.
        if(dsText[1] != "") {
          unTreeWidget_add(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, 
                        dsText[1], sCopiedNodeName, exInfo);
        }
      }
      break;
    case unTreeWidget_popupMenu_remove:
// ask confirmation
      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/MessageInfo", "remove tree confirmation", 
                            makeDynString("$1:Are you sure you want to remove \n"+sDisplayNodeName+"?", "$2:Ok", "$3:Cancel"), dfFloat, dsText);

      if(dynlen(dfFloat)>0) {
        if(dfFloat[1] == 1) {
// got confirmation
          unTreeWidget_remove(sGraphTree, sSelectedKey, sParentKey, sDpName, sDisplayNodeName, sParentDpName, 
                                                              sParentNodeName, bClipboard, exInfo);
        }
      }
      break;
    case unTreeWidget_popupMenu_removeTree:
// ask confirmation
        unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/MessageInfo", "remove tree confirmation", 
                              makeDynString("$1:Are you sure you want to remove the tree \n"+sDisplayNodeName+"?", "$2:Ok", "$3:Cancel"), dfFloat, dsText);
        if(dynlen(dfFloat)>0) {
          if(dfFloat[1] == 1) {
// got confirmation
            unTreeWidget_removeTree(sGraphTree, bClipboard, sSelectedKey, sDpName, sDisplayNodeName, exInfo);
          }
        }
      break;
    case unTreeWidget_popupMenu_cut:
      unTreeWidget_cut(sGraphTree, sSelectedKey, sParentKey, sDpName, sDisplayNodeName, sParentDpName, 
                                                        sParentNodeName, bClipboard, exInfo);
    // if no error put the node in g_sPasteNodeName and g_sPasteNodeDpName
      if(dynlen(exInfo) <= 0) {
        g_sPasteNodeName[pos] = sDisplayNodeName;
        g_sPasteNodeDpName[pos] = sDpName;
      }
      break;
    case unTreeWidget_popupMenu_copyTree:
      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Copy Tree", makeDynString("$sTextQuestion:New name for copy of \""+sDisplayNodeName+"\":"), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
// new tree.
        if(dsText[1] != "") {
          unTreeWidget_copyTree(sGraphTree, bClipboard, sDisplayNodeName, dsText[1], exInfo);
        }
      }
      break;
    case unTreeWidget_popupMenu_copy:
      unTreeWidget_copy(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sParentDpName, 
                                      sParentNodeName, bClipboard, "Copy_of_"+sDisplayNodeName, sCopiedNodeName, exInfo);
    // if no error put the copied node in g_sPasteNodeName and g_sPasteNodeDpName
      if(dynlen(exInfo) <= 0) {
        g_sPasteNodeName[pos] = fwTree_getNodeDisplayName(sCopiedNodeName, exInfo);;
        g_sPasteNodeDpName[pos] = _fwTree_makeNodeName(sCopiedNodeName);
      }
      break;
    case unTreeWidget_popupMenu_paste:
    // take the node name from g_sPasteNodeName and g_sPasteNodeDpName and paste it
      unTreeWidget_paste(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, g_sPasteNodeDpName[pos], 
                                                        g_sPasteNodeName[pos], bClipboard, exInfo);
    // if no error reset g_sPasteNodeName and g_sPasteNodeDpName
      if(dynlen(exInfo) <= 0) {
        g_sPasteNodeName[pos] = "";
        g_sPasteNodeDpName[pos] = "";
      }
      break;
    case unTreeWidget_popupMenu_rename:
      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Rename Node", makeDynString("$sTextQuestion:New name:", "$currentName:"+sDisplayNodeName), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
        if(dfFloat[1] == 1) {
          if(dsText[1]!= "") {
            unTreeWidget_rename(sGraphTree, sSelectedKey, sParentKey, sCompositeLabel, 
                            sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, bClipboard, 
                            dsText[1], sCurrentSelectedNodeKey, sParentKey, exInfo);
          }
        }
      }
      break;
    case unTreeWidget_popupMenu_reorder: 
      unTreeWidget_reorder(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sParentDpName, 
                                                        sParentNodeName, bClipboard, exInfo);
      break;
    default:
      break;
  }

  unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentMousePointer, true);
  unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
  unProgressBar_stop(id, sBar);

// display the errors here.
  if(dynlen(exInfo) > 0)
    fwExceptionHandling_display(exInfo);
}
