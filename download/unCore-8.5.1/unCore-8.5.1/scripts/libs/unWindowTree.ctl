/**@name LIBRARY: unWindowTree.ctl
!!!!!!!!!!!!! the Add button is temporary disabled in the hierarchyCascade, RigthClick and NodeRightClick also in unTree/unNodeList.pnl

@author: Herve Milcent (LHC-IAS)

Creation Date: 19/04/2002

Modification History:
  11/05/2009: Herve
    - bug fix: removeSymbol in PVSS 3.8
    modified function: unWindowTree_preExecuteEventRigthClick, unWindowTree_preExecuteEventNodeRigthClick

  30/06/2008: Herve
    - bug childPanel:
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

  09/06/2008: Herve
    - in unWindowTree_nodeListLoadImage call unWindowTree_loadImage

  02/06/2008: Herve
    - unWindowTree_loadImage: remove getShape--> useless

  18/02/2008: Herve
    bug add panel

        13/11/2007: Herve
          - bug with addImageClass

  02/04/2007: Herve
    new function: unWindowTree_eventNodeRigthClickOperationMenu unWindowTree_handleEventNodeRigthClickOperation

  18/02/2005: Herve.
    implementation of panel ref: unWindowTree_handleEventNodeClickOperation call the unGraphicalFrame_showContextualPanel with
    deviceName or fwTreeNode Dp.
      - new function unWindowTree_handleEventNodeClickSelectNode.

  07/12/2004: Herve
    - unWindowTree_getDeviceState: return true for link allowed

  01/12/2004: Herve
    -replace add by new
    -add the add existing

  15/11/2004: Herve
    -new dollar parameter for the fwTrendingPlotConf.pnl in function unTrendTree_openPanelGetDollarParam

  24/09/2004: Herve
    - add new function to get the dollar parameter

  27/08/2004: Herve
    - wrong function name: unProgessBar_start renamed to unProgressBar_start
    - add the set of the mouse pointer

  23/07/2004: Herve update to 3.0
    - new functions unWindowTree_EventInitializeConfiguration, unWindowTree_EventRigthClick, , unWindowTree_EventNodeRigthClick

  19/02/2004: Herve
    display in the widget as start of hierarchy other device than _FwViews

  . 29-May-2002:
    . add unWindowTree_EventClickConfigure
    . remove comments
  . 09-October-2002:
    . remove the unWindowTree_EventClick, unWindowTree_EventClickConfiguration, unWindowTree_EventAfterLabelEditConfiguration and
    put them in the unTree.ctl
    . modify the unWindowTree_EventInitialize to call the unTree_Initialize funtion

version 1.1

External Functions:
  . unWindowTree_EventInitialize: function for initializing the widget tree.

Internal Functions:

Purpose:
This library contain functions to be used with the UNICOS windowTree utility.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . the following panels must exist:
    . vision/graphicalFrame/contextualButton.pnl: contextual buttons for the _UnPanel data point type
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.

*/
#uses "unTree.ctl"
#uses "unPanel.ctl"

private const bool unWindowTree_unHMILibLoaded = fwGeneral_loadCtrlLib("unHMI/unHMI.ctl",false);

const int UN_WINDOWTREE_OPENASPOPUP = 1;
const string unWindowTree_FILENAME_CONFIGPANEL = "vision/windowTree/unWindowTreeConfiguration.pnl";

//@{

// unWindowTree_loadImage
/**
Purpose:
Function to load the image of the WindowTree into the unTreeWidget.

  @param sDeviceIdentifier: string, input: the display node name
  @param sGraphTree: string, input: the graphical name of the unTreeWidget
  @param exceptionInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unWindowTree_loadImage(string sDeviceIdentifier, string sGraphTree, dyn_string &exceptionInfo)
{
  shape aShape;
  string imageClassParameters;

  // get the icon list of the root folder device and load it in the unTreeWidget if there is no error
  imageClassParameters =
      getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenClose.bmp")          + ";" +
      getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenOpen.bmp")           + ";" +
      getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenSelected.bmp")       + ";" +
      getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenPath.bmp")           + ";" +
      getPath(PICTURES_REL_PATH,"rootNode/iconWithoutChildrenNotSelected.bmp") + ";" +
      getPath(PICTURES_REL_PATH,"rootNode/iconWithoutChildrenSelected.bmp");

  unTreeWidget_setPropertyValue(aShape, "addImageClass", UN_WINDOWTREE_ROOT_FOLDER + ";" + imageClassParameters, true);

  // get the icon list of the node device and load it in the unTreeWidget if there is no error
  imageClassParameters =
      getPath(PICTURES_REL_PATH,"node/iconWithChildrenClose.bmp")          + ";" +
      getPath(PICTURES_REL_PATH,"node/iconWithChildrenOpen.bmp")           + ";" +
      getPath(PICTURES_REL_PATH,"node/iconWithChildrenSelected.bmp")       + ";" +
      getPath(PICTURES_REL_PATH,"node/iconWithChildrenPath.bmp")           + ";" +
      getPath(PICTURES_REL_PATH,"node/iconWithoutChildrenNotSelected.bmp") + ";" +
      getPath(PICTURES_REL_PATH,"node/iconWithoutChildrenSelected.bmp");

  unTreeWidget_setPropertyValue(aShape, "addImageClass", unTreeWidget_NODE      + ";" + imageClassParameters, true);
  unTreeWidget_setPropertyValue(aShape, "addImageClass", unTreeWidget_CLIPBOARD + ";" + imageClassParameters, true);

  // get the icon list of the _UnPanel device and load it in the widget if there is no error
  imageClassParameters =
      getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenClose.bmp")          + ";" +
      getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenOpen.bmp")           + ";" +
      getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenSelected.bmp")       + ";" +
      getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenPath.bmp")           + ";" +
      getPath(PICTURES_REL_PATH,"_UnPanel/iconWithoutChildrenNotSelected.bmp") + ";" +
      getPath(PICTURES_REL_PATH,"_UnPanel/iconWithoutChildrenSelected.bmp");

  unTreeWidget_setPropertyValue(aShape, "addImageClass", UN_WINDOWTREE_PANEL+";" + imageClassParameters, true);
}

// unWindowTree_EventRigthClickMenu
/**
Purpose:
Function to build the popup menu when for the eventRigthClick on the unTreeWidget.

  @param dsMenu: dyn_string, output: the popup menu is returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_EventRigthClickMenu(dyn_string &dsMenu, string sDisplayNodeName, string sPasteNodeDpName, string sPasteNodeName, dyn_string &exInfo)
{
  bool bGranted = false;
  int iAllowed;
  dyn_string dsChildren;
  string sTopDisplayName;
  unTree_isUserAllowed(bGranted, exInfo);

  if(bGranted)
  {
    iAllowed = 1;
  }
  else
  {
    iAllowed = 0;
  }

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sTopDisplayName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Refresh Tree, "+unTreeWidget_popupMenu_clearTree+", 1");
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, New folder, " + UN_TREE_newFolder + ", " + iAllowed);
  dynAppend(dsMenu,"PUSH_BUTTON, New panel, " + UN_TREE_newDevice + ", " + iAllowed);
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Add panel, "+UN_TREE_addDevice+", " + iAllowed);
  dynAppend(dsMenu,"PUSH_BUTTON, Add panels, "+UN_TREE_addDevices+", " + iAllowed);

  dynAppend(dsMenu,"SEPARATOR");
  if(sPasteNodeName != "") {
    dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
  }
  else
    dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");

  fwTree_getChildren(sTopDisplayName, dsChildren, exInfo);
  if(dynlen(dsChildren) > 0)
  {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", "+iAllowed);
  }
  else {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", 0");
  }
}

// unWindowTree_preExecuteEventRigthClick
/**
Purpose:
pre-prossessing function called before the handling of the event rigthClick

  @param iCommand: int, input: the command

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_preExecuteEventRigthClick(int iCommand)
{
  if((iCommand == unTreeWidget_popupMenu_reorder) ||
     (iCommand == unTreeWidget_popupMenu_clearTree) ||
     (iCommand == unTreeWidget_popupMenu_initTree)){
    unTree_setUserPanelOpened(false);
    unTree_setDeviceButtonGlobalVariable(false, false, false, false);
    unTree_setDeviceButtonState(false, false, false, false);
    unTree_setHierarchyButtonGlobalVariable(false, false, false, false, false, false,
                                            false, false, false);
    unTree_setHierarchyButtonState(false, false, false, false, false, false,
                                   false, false, false);

    unTree_setHierarchyButtonText("", "");
    unTree_setPasteGlobalValue("", "");

    if(unTree_removeUserPanel())
      removeSymbol(myModuleName(), myPanelName(), "userPanel");
  }
}

// unWindowTree_handleEventRigthClick
/**
Purpose:
Basic unWindowTree function: handle the result of the event rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sPasteNodeDpName: string, input/output: the node dp name to paste
  @param sPasteNodeName: string, input/output: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param bDone: bool, output: action executed
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_handleEventRigthClick(int ans, string sGraphTree, string sBar, string sSelectedKey,
                                    string sDpName, string &sPasteNodeDpName, string &sPasteNodeName,
                                    bool bClipboard, bool &bDone, dyn_string &exInfo)
{
  dyn_float dfFloat, dFloatReturn;
  dyn_string dsText, dStringReturn;
  int i, len, bRes;
  shape aShape = getShape(sGraphTree);
  string sNodeNameAdded, sTopDisplayName;
  string selectedFile, sFileName, sPanelPath;

  bDone = false;
/*
DebugN("unWindowTree_handleEventRigthClick", ans, sGraphTree, sBar, sSelectedKey,
        sDpName, sPasteNodeDpName, sPasteNodeName, bClipboard);
*/
  g_closeAllowed.text = false;
  switch(ans) {
    case UN_TREE_newDevice:
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);

      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "New Panel", makeDynString("$sTextQuestion:New panel name:"), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
        if(dfFloat[1] == 1) {
          selectedFile = dsText[1];
// add pnl is not.
          i = strpos(selectedFile, UN_PANEL_FILENAME_EXTENTION);
          if(i <= 0)
            selectedFile = selectedFile+UN_PANEL_FILENAME_EXTENTION;

          bRes = copyFile(getPath(PANELS_REL_PATH, c_template_panel), getPath(PANELS_REL_PATH)+selectedFile);
// bad bRes==> error
          if(bRes) {
            unWindowTree_addPanelInHierarchy(selectedFile, true, sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, true, bDone, exInfo);
          }
          else {
            fwException_raise(exInfo, "ERROR", selectedFile+" " +getCatStr("unPanel", "FILENOTCOPIED"), "");
          }
        }
      }
      break;
    case UN_TREE_addDevice:
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
      unWindowTree_addPanel(true, sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, true, bDone, exInfo);
      break;
    case UN_TREE_addDevices:
      sPanelPath = "vision/windowTree/addPanels.pnl";
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
      unGraphicalFrame_ChildPanelOnCentralModalReturn(sPanelPath,"Add panels",makeDynString(""),dFloatReturn,dStringReturn);
      if (dynlen(dFloatReturn)<1 || dFloatReturn[1]==0) break; // panel was cancelled
      for (int a =1; a<= dynlen(dStringReturn); a++) {
        sFileName = dStringReturn[a];
        unWindowTree_addPanelInHierarchy(sFileName, true, sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, true, bDone, exInfo);
        if (dynlen(exInfo)) return;
     }
      break;
    default:
      unTree_handleDefaultEventRigthClick(ans, sGraphTree, sBar, sSelectedKey,
                                          sDpName, sPasteNodeDpName, sPasteNodeName,
                                          bClipboard, bDone, exInfo);
      break;
  }
  if(!bDone)
    g_closeAllowed.text = true;
}

// unWindowTree_postExecuteEventRigthClick
/**
Purpose:
Basic unWindowTree function: handle the post execution of the rigth click of the event rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_postExecuteEventRigthClick(int ans, string sGraphTree, string sBar, string sSelectedKey,
                                    string sDpName, string sPasteNodeDpName, string sPasteNodeName,
                                    bool bClipboard, dyn_string &exInfo)
{
// do nothing for the time being except reset the g_closeAllowed.text
//DebugN("unWindowTree_postExecuteEventRigthClick", ans, sSelectedKey, sDpName, sPasteNodeDpName,sPasteNodeName);
  g_closeAllowed.text = true;
}

// unWindowTree_EventNodeRigthClickMenu
/**
Purpose:
Function to build the popup menu when for the eventNodeRigthClick on the unTreeWidget.

  @param dsMenu: dyn_string, input: the popup menu is returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_EventNodeRigthClickMenu(dyn_string &dsMenu, string sDisplayNodeName, string sDpName, string sPasteNodeDpName, string sPasteNodeName, dyn_string &exInfo)
{
  dyn_string dsChildren;
  string sNodeName, sDeviceName, sDeviceType;
  int isClipboard, isRoot, iAllowed;
  bool isInClipboard;
  bool bGranted = false;

  sNodeName = _fwTree_getNodeName(sDpName);
  isClipboard = fwTree_isClipboard(sNodeName, exInfo);
  isInClipboard = fwTree_isInClipboard(sNodeName, exInfo);
  isRoot = fwTree_isRoot(sNodeName, exInfo);

  unTree_isUserAllowed(bGranted, exInfo);
  if(bGranted)
  {
    iAllowed = 1;
  }
  else
  {
    iAllowed = 0;
  }



  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  if( !isClipboard) {
    dynAppend(dsMenu,"SEPARATOR");
  }
  if(!isRoot) {
    if(!isClipboard) {
      dynAppend(dsMenu,"PUSH_BUTTON, Cut: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_cut+", "+iAllowed);
      dynAppend(dsMenu,"PUSH_BUTTON, Copy: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copy+", "+iAllowed);

      if((sPasteNodeName != "") && (!isInClipboard) && (sPasteNodeDpName!=sDpName)) {
        dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
      }
      else
        dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");
      fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
      if(dpExists(sDeviceName) && (sDeviceType == UN_PANEL_DPTYPE)) {
        dynAppend(dsMenu,"SEPARATOR");
        dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", "+iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, Link to panel, "+UN_TREE_linkToDevice+", 0");
        dynAppend(dsMenu,"PUSH_BUTTON, Unlink from panel, "+UN_TREE_unLinkToDevice+", "+iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, Change linked panel, "+UN_TREE_changeLinkedDevice+", "+iAllowed);
      }
      else {
        dynAppend(dsMenu,"SEPARATOR");
        dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", 0");
        dynAppend(dsMenu,"PUSH_BUTTON, Link to panel, "+UN_TREE_linkToDevice+", "+iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, Unlink from panel, "+UN_TREE_unLinkToDevice+", 0");
        dynAppend(dsMenu,"PUSH_BUTTON, Change linked panel, "+UN_TREE_changeLinkedDevice+", 0");

        //UNHMI-48 Adding more options to right click of a folder ( new folder / panel etc)
        dynAppend(dsMenu,"SEPARATOR");
        dynAppend(dsMenu,"PUSH_BUTTON, New folder, " + UN_TREE_newFolder + ", " + iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, New panel, " + UN_TREE_newDevice + ", " + iAllowed);
        dynAppend(dsMenu,"SEPARATOR");
        dynAppend(dsMenu,"PUSH_BUTTON, Add panel, "+UN_TREE_addDevice+", " + iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, Add panels, "+UN_TREE_addDevices+", " + iAllowed);
      }
    }
    else {
      dynAppend(dsMenu,"SEPARATOR");
      if((sPasteNodeName != "") && (!isClipboard) && (sPasteNodeDpName!=sDpName)) {
        dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
      }
      else
        dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");
    }
  }

  dynAppend(dsMenu,"SEPARATOR");
  if((!isRoot) && (!isClipboard)) {
    dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, Rename, "+unTreeWidget_popupMenu_rename+", "+iAllowed);
  }
  else
    dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", 0");

  fwTree_getChildren(sNodeName, dsChildren, exInfo);
  if(dynlen(dsChildren) > 0) {
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", "+iAllowed);
  }
  else {
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", 0");
  }
}

// unWindowTree_preExecuteEventNodeRigthClick
/**
Purpose:
pre-prossessing function called before the handling of the event NodeRigthClick

  @param iCommand: int, input: the command
  @param sCurrentDpName: string, input: the current dpName
  @param sDpName: string, input: the dpName selected

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_preExecuteEventNodeRigthClick(int iCommand, string sCurrentDpName, string sDpName)
{
  if((iCommand == unTreeWidget_popupMenu_reorder) && (sCurrentDpName == sDpName)) {
    unTree_setHierarchyPasteButtonGlobalVariable(false);
    unTree_setHierarchyPasteButtonState("", false);
    unTree_setPasteGlobalValue("", "");
    if(sCurrentDpName == sDpName) {
      unTree_setUserPanelOpened(false);

      if(unTree_removeUserPanel())
        removeSymbol(myModuleName(), myPanelName(), "userPanel");
    }
  }
}

// unWindowTree_handleEventNodeRigthClick
/**
Purpose:
Basic unWindowTree function: handle the result of the event node rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the display node name
  @param sCompositeLabel: string, input: the composite label of the node name
  @param sParentKey: string, input: the key of the parent
  @param sParentDpName: string, input: the parent data point name
  @param sParentNodeName: string, input: the parent node name
  @param sPasteNodeDpName: string, input/output: the node dp name to paste
  @param sPasteNodeName: string, input/output: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param bDone: bool, output: action executed
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_handleEventNodeRigthClick(int ans, string sGraphTree, string sBar,
                                        string sSelectedKey, string sDpName, string sDisplayNodeName,
                                        string sCompositeLabel, string sParentKey, string sParentDpName,
                                        string sParentNodeName, string &sPasteNodeDpName,
                                        string &sPasteNodeName, bool bClipboard, bool &bDone,
                                        string sCurrentSelectedNodeKey, string sParentKeyToAddInNewKey,
                                        dyn_string &exInfo)
{
  dyn_float dfFloat, dFloatReturn;
  dyn_string dsText, dStringReturn;
  string sCopiedNodeName, sFileName;
  int i, len;
  bool bUserPanelOpened, bRes;
  int pos;
  string sNodeName, sDeviceName, sDeviceType, sNodeNameAdded, selectedFile, sPanelPath;
  dyn_string dsUserData;

  bDone = false;
/*
DebugN("unWindowTree_handleEventNodeRigthClick", ans, sGraphTree, sBar, sSelectedKey,
        sDpName, sDisplayNodeName, sCompositeLabel, sParentKey, sParentDpName,
        sParentNodeName, sPasteNodeDpName, sPasteNodeName, bClipboard);
*/

  g_closeAllowed.text = false;
  switch(ans) {
    case UN_TREE_newDevice:

      unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "New Panel", makeDynString("$sTextQuestion:New panel name:"), dfFloat, dsText);
      if(dynlen(dfFloat)>0) {
        if(dfFloat[1] == 1) {
          selectedFile = dsText[1];
// add pnl is not.
          i = strpos(selectedFile, UN_PANEL_FILENAME_EXTENTION);
          if(i <= 0)
            selectedFile = selectedFile+UN_PANEL_FILENAME_EXTENTION;

          bRes = copyFile(getPath(PANELS_REL_PATH, c_template_panel), getPath(PANELS_REL_PATH)+selectedFile);
// bad bRes==> error
          if(bRes) {
            unWindowTree_addPanelInHierarchy(selectedFile, true, sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, exInfo);
          }
          else {
            fwException_raise(exInfo, "ERROR", selectedFile+" " +getCatStr("unPanel", "FILENOTCOPIED"), "");
          }
        }
      }
      break;
    case UN_TREE_addDevice:
      unWindowTree_addPanel(true, sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, exInfo);
      break;
     case UN_TREE_addDevices:
      sPanelPath = "vision/windowTree/addPanels.pnl";
      unGraphicalFrame_ChildPanelOnCentralModalReturn(sPanelPath,"Add panels",makeDynString(""),dFloatReturn,dStringReturn);
      for (int a =1; a<= dynlen(dStringReturn); a++) {
        sFileName = dStringReturn[a];
        unWindowTree_addPanelInHierarchy(sFileName, true, sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, exInfo);
        if (dynlen(exInfo)>0) {
          DebugN("----  ERROR  ----");
          DebugN(exInfo);
          fwExceptionHandling_display(exInfo);
          return;
        }
     }

      break;
    case UN_TREE_horizontalNavigation:
      sNodeName = _fwTree_getNodeName(sDpName);
      fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
      if((dynlen(exInfo) <=0) && (sDeviceType == UN_PANEL_DPTYPE) && (dpExists(sDeviceName))){
        unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/unPanel/unPanelConfiguration.pnl", "Panel Configuration",
                            makeDynString("$sDpName:"+sDpName), dfFloat, dsText);
        bDone = true;
      }
      break;
    case UN_TREE_linkToDevice:
      unWindowTree_addPanel(false, sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, exInfo);
      break;
    default:
      unTree_handleDefaultEventNodeRigthClick(ans, sGraphTree, sBar,
                                              sSelectedKey, sDpName, sDisplayNodeName,
                                              sCompositeLabel, sParentKey, sParentDpName,
                                              sParentNodeName, sPasteNodeDpName,
                                              sPasteNodeName, bClipboard, bDone,
                                              sCurrentSelectedNodeKey, sParentKeyToAddInNewKey,
                                              exInfo);
      break;
  }
  if(!bDone)
    g_closeAllowed.text = true;
}

// unWindowTree_postExecuteEventNodeRigthClick
/**
Purpose:
Basic unWindowTree function: handle the post execution of the event node rigth click
WARNING: sDpName, sSelectedKey, etc. are value that come from the eventNodeRigthClick, if the node name was rename or deleted, an exception could be raise

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the display node name
  @param sCompositeLabel: string, input: the composite label of the node name
  @param sParentKey: string, input: the key of the parent
  @param sParentDpName: string, input: the parent data point name
  @param sParentNodeName: string, input: the parent node name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_postExecuteEventNodeRigthClick(int ans, string sGraphTree, string sBar,
                                        string sSelectedKey, string sDpName, string sDisplayNodeName,
                                        string sCompositeLabel, string sParentKey, string sParentDpName,
                                        string sParentNodeName, string sPasteNodeDpName,
                                        string sPasteNodeName, bool bClipboard,
                                        string sCurrentSelectedNodeKeyBis, string sParentKeyToAddInNewKey,
                                        dyn_string &exInfo)
{
  string sNodeName = _fwTree_getNodeName(sDpName);
  string sDeviceName, sDeviceType;
  string sTitle, sTopDisplayName;
  string sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
                      sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey;
  string sFunctionName, sTextToShow;
  dyn_string dsReturnData;
  bool bNavigation=false, bLink=false, bDollar=false, bGranted = true, bUserPanelOpened=false, bLinkAllowed=false;
/*
DebugN("unWindowTree_postExecuteEventNodeRigthClick", ans, sSelectedKey, sDpName, sDisplayNodeName,
                                        sCompositeLabel, sParentKey, sParentDpName,
                                        sParentNodeName, sPasteNodeDpName,
                                        sPasteNodeName, sCurrentSelectedNodeKey, sParentKeyToAddInNewKey);
*/
  switch(ans) {
    case UN_TREE_linkToDevice:
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
      unTree_getUserPanelOpened(bUserPanelOpened);
      unTree_isUserAllowed(bGranted, exInfo);
      unTree_getGlobalValue(sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
                      sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey);
      if(unTree_isFromNodeList()  || (sCurrentDpName == sParentDpName)) {
        sDpName = sParentDpName;
        _fwTree_getParent(sDpName, sParentDpName);
        if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
          sParentDpName = "";
        unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
        unTree_setUserPanelOpened(bUserPanelOpened);
      }
// set link to false and unlink and navigation to true
      else if(sCurrentDpName == sDpName) {
  // get the device name
        fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
// get the state of the device button
        unWindowTree_getDeviceState(dsReturnData, sDisplayNodeName, sDpName, sDeviceName, sDeviceType);
        if(dynlen(dsReturnData) >= 4) {
          if((dsReturnData[1] == "TRUE") || (dsReturnData[1] == "true"))
            bNavigation = true;
          if((dsReturnData[2] == "TRUE") || (dsReturnData[2] == "true"))
            bDollar = true;
          if((dsReturnData[3] == "TRUE") || (dsReturnData[3] == "true"))
            bLink = true;
          if((dsReturnData[4] == "TRUE") || (dsReturnData[4] == "true"))
            bLinkAllowed = true;
        }

// get the node display name to show
        sTextToShow = sDisplayNodeName;
        unWindowTree_getDisplayNodeName(dsReturnData, sDisplayNodeName, sDpName, sDeviceName, sDeviceType);
        if(dynlen(dsReturnData) >= 1) {
          sTextToShow = dsReturnData[1];
        }

        unTree_setDeviceButtonGlobalVariable(bNavigation, bLink, true, bDollar);
        unTree_setDeviceButtonState(bNavigation&bGranted&bLinkAllowed, bLink&bGranted&bLinkAllowed,
                                          bGranted&bLinkAllowed, bGranted&bDollar&bLinkAllowed);
// open the user panel with no detail
        unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
        unTree_setUserPanelOpened(bUserPanelOpened);
      }
      break;
    case UN_TREE_deviceConfiguration:
//DebugN(sCurrentDpName, sDpName);
      unTree_getGlobalValue(sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
                      sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey);
      if(unTree_isFromNodeList()  || (sCurrentDpName == sParentDpName)) {
        sDpName = sParentDpName;
        _fwTree_getParent(sDpName, sParentDpName);
        if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
          sParentDpName = "";
        unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
        unTree_setUserPanelOpened(bUserPanelOpened);
      }
      else if(sCurrentDpName == sDpName) {
  // get the device name
        fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
// get the node display name to show
        sTextToShow = sDisplayNodeName;
        unWindowTree_getDisplayNodeName(dsReturnData, sDisplayNodeName, sDpName, sDeviceName, sDeviceType);
        if(dynlen(dsReturnData) >= 1) {
          sTextToShow = dsReturnData[1];
        }
      }
      break;
    default:
      break;
  }
  g_closeAllowed.text = true;
}

// unWindowTree_getHierarchyState
/**
Purpose:
Function to get the state of the 3 add buttons of the hierarchy cascade button.

  @param dsReturnData: dyn_string, output: the state are returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_getHierarchyState(dyn_string &dsReturnData, string sDisplayNodeName, string sDpName)
{
  dsReturnData = makeDynString("FALSE","TRUE", "TRUE", "TRUE");
}

// unWindowTree_getDeviceState
/**
Purpose:
Function to get the state of the buttons of the device cascade button: the navigation and set dollar parameter

  @param dsReturnData: dyn_string, output: the state are returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param sDeviceName: string, input: the device name linke to the node
  @param sDeviceType: string, input: the device type

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unWindowTree_getDeviceState(dyn_string &dsReturnData, string sDisplayNodeName, string sDpName, string sDeviceName, string sDeviceType)
{
  dsReturnData = makeDynString("TRUE","TRUE", "FALSE", "TRUE");
}

// unWindowTree_getDisplayNodeName
/**
Purpose:
Function to get the display node name to show in the unTree.

  @param dsReturnData: dyn_string, output: the state are returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param sDeviceName: string, input: the device name linke to the node
  @param sDeviceType: string, input: the device type

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unWindowTree_getDisplayNodeName(dyn_string &dsReturnData, string sDisplayNodeName, string sDpName, string sDeviceName, string sDeviceType)
{
  string sNode;
  if(sDeviceType == UN_PANEL_DPTYPE) {
    sNode = sDisplayNodeName + UN_TREE_DEVICE_SEPARATOR+unPanel_convertDpPanelNameToPanelFileName(sDeviceName);
  }
  dsReturnData = makeDynString(sNode);
}

// unWindowTree_openPanelGetDollarParam
/**
Purpose:
Function to get the get the dollar parameter to be given to the panel to show.

  @param sPanel: string, input: the panel
  @param sDpName: string, input: the dp name of the node name
  @param sDeviceName: string, input: the device name linke to the node
  @param sDeviceType: string, input: the device type
  @param return value: dyn_string, the dollar parameter are returned here
  @param sParentDpName: string, input: the parent dp name of the node name
  @param bDetail: bool, input: true, show detail panel

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
dyn_string unWindowTree_openPanelGetDollarParam(string sPanel, string sDpName, string sDeviceName, string sDeviceType, string sParentDpName, bool bDetail)
{
//DebugN(sPanel);
  return makeDynString("$sDpName:" + sDpName,"$sParentDpName:" + sParentDpName);
}

// unWindowTree_nodeListLoadImage
/**
Purpose:
load the image class of the nodeList panel.

  @param sNodeList: string, input: the name of the unTreeWidget graphical element

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_nodeList_loadImage(string sNodeList)
{
  dyn_string exceptionInfo;

  unWindowTree_loadImage("", sNodeList, exceptionInfo);
}

// unWindowTree_nodeList_EventNodeRigthClickMenu
/**
Purpose:
popup menu of the nodeRigthClick event in the nodeList panel.

  @param dsMenuReturn: string, output: the popup menu
  @param sDisplayNodeName: string, input: the display node name of the node
  @param sNodeName: string, input: the node name of the node
  @param sDeviceName: string, input: the device name linked to the node
  @param sDeviceType: string, input: the device type of the device name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_nodeList_EventNodeRigthClickMenu(dyn_string &dsMenuReturn, string sDisplayNodeName, string sNodeName, string sDeviceName, string sDeviceType)
{
  bool bGranted = false;
  int iAllowed;
  dyn_string dsMenu, exInfo;

  unTree_isUserAllowed(bGranted, exInfo);
  if(bGranted)
    iAllowed=1;
  else
    iAllowed = 0;


  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Cut: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_cut+", "+iAllowed);
  dynAppend(dsMenu,"PUSH_BUTTON, Copy: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copy+", "+iAllowed);
  if(dpExists(sDeviceName) && (sDeviceType == UN_PANEL_DPTYPE)) {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, Link to panel, "+UN_TREE_linkToDevice+", 0");
    dynAppend(dsMenu,"PUSH_BUTTON, Unlink from panel, "+UN_TREE_unLinkToDevice+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, Change linked panel, "+UN_TREE_changeLinkedDevice+", "+iAllowed);
  }
  else {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", 0");
    dynAppend(dsMenu,"PUSH_BUTTON, Link to panel, "+UN_TREE_linkToDevice+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, Unlink from panel, "+UN_TREE_unLinkToDevice+", 0");
    dynAppend(dsMenu,"PUSH_BUTTON, Change linked panel, "+UN_TREE_changeLinkedDevice+", 0");
  }
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", "+iAllowed);
  dynAppend(dsMenu,"PUSH_BUTTON, Rename, "+unTreeWidget_popupMenu_rename+", "+iAllowed);

  dsMenuReturn = dsMenu;
}

// unWindowTree_nodeList_EventRigthClickMenu
/**
Purpose:
popup menu of the rigthClick event in the nodeList panel.

  @param dsMenuReturn: string, output: the popup menu
  @param sDisplayNodeName: string, input: the display node name of the node
  @param sNodeName: string, input: the display node name of the node
  @param sDpName: string, input: the node name of the node
  @param sPasteNodeName: string, input: the node name to paste
  @param sPasteNodeDpName: string, input: the node name dp name to paste

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_nodeList_EventRigthClickMenu(dyn_string &dsMenuReturn, string sDisplayNodeName, string sNodeName, string sDpName, string sPasteNodeName, string sPasteNodeDpName)
{
  bool isInClipboard, bGranted = false;
  int isClipboard, iAllowed;
  dyn_string exInfo, dsMenu, dsChildren;

  unTree_isUserAllowed(bGranted, exInfo);
  if(bGranted)
  {
    iAllowed = 1;
  }
  else
  {
    iAllowed = 0;
  }


  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  isClipboard = fwTree_isClipboard(sNodeName, exInfo);
  if(!isClipboard)
  {
    dynAppend(dsMenu,"PUSH_BUTTON, New folder, "+UN_TREE_newFolder+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, New panel, "+UN_TREE_newDevice+", "+iAllowed);
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Add panel, "+UN_TREE_addDevice+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, Add panels, "+UN_TREE_addDevices+", "+iAllowed);
    dynAppend(dsMenu,"SEPARATOR");
  }

  isInClipboard = fwTree_isInClipboard(sNodeName, exInfo);

  if((sPasteNodeName != "") && (!isInClipboard) && (sPasteNodeDpName!=sDpName)) {
    dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
  }
  else
    dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");

  fwTree_getChildren(sNodeName, dsChildren, exInfo);
  if(dynlen(dsChildren) > 0) {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", "+iAllowed);
  }
  else {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", 0");
  }
  dsMenuReturn = dsMenu;
}

// unWindowTree_addPanel
/**
Purpose:
Add a panel.

  @param bCreate: bool, input: true create the panel, false do not create the panel
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name to add the panel in
  @param sDisplayNodeName: string, input: the display node name
  @param bTop: bool, input: true add as top node in the unTreeWidget
  @param bDone: bool, output: true the panel was correctly added, false, it was not
  @param exInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unWindowTree_addPanel(bool bCreate, string sGraphTree, string sBar, string sSelectedKey, string sDpName,
  string sDisplayNodeName, bool bTop, bool &bDone, dyn_string &exInfo)
{
  int i;
  int iRes, pos;
  bool bConti;
  string sFileName, sPath, sPanelDpName;

  iRes = fileSelector(sFileName, getPath(PANELS_REL_PATH), true, "*.pnl *.xml");
  if((iRes==0) && (sFileName != "")){
  // create the data point
    bConti = true;
    for(i=1;(i<=SEARCH_PATH_LEN) && bConti;i++) {
      sPath = getPath(PANELS_REL_PATH, "", 1, i);
      pos = strpos(sFileName, sPath);
      if(pos >=0) {
        bConti = false;
        sFileName = substr(sFileName, strlen(sPath), strlen(sFileName) - strlen(sPath));
      }
    }
    if(!bConti) {
  // link the node to it
      unWindowTree_addPanelInHierarchy(sFileName, bCreate, sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, bTop, bDone, exInfo);
    }
    else
      fwException_raise(exInfo, "ERROR", getCatStr("unPanel", "WRONGPATH"), "");
  }
}


// unWindowTree_addPanelInHierarchy
/**
Purpose:
Add a panel.

  @param sFileName: string, input: the panel file name (relative path included)
  @param bCreate: bool, input: true create the panel, false do not create the panel
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name to add the panel in
  @param sDisplayNodeName: string, input: the display node name
  @param bTop: bool, input: true add as top node in the unTreeWidget
  @param bDone: bool, output: true the panel was correctly added, false, it was not
  @param exInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unWindowTree_addPanelInHierarchy(string sFileName, bool bCreate, string sGraphTree, string sBar, string sSelectedKey, string sDpName,
  string sDisplayNodeName, bool bTop, bool &bDone, dyn_string &exInfo)
{
  dyn_float dfFloat;
  dyn_string dsText;
  string sPanelDpName;
  string sNodeName, sDeviceName, sDeviceType, sNodeNameAdded, sDpNodeNameAdded, sDisplayNodeNameAdded;
  dyn_string dsUserData, dsTemp;

  // link the node to it
  unPanel_create(sFileName, sPanelDpName, exInfo);

  if(dynlen(exInfo)<=0) {
    if(bCreate) {
      dsTemp = strsplit(dpSubStr(sPanelDpName, DPSUB_DP), UN_PANEL_DPNAME_DELIMITER);
      unTree_add(sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, dsTemp[dynlen(dsTemp)], bTop, sNodeNameAdded, exInfo);
// make the new dpName
      sDpNodeNameAdded = _fwTree_makeNodeName(sNodeNameAdded);
      sDisplayNodeNameAdded = fwTree_getNodeDisplayName(sNodeNameAdded, exInfo);
// make the new key
      sSelectedKey = sDisplayNodeName+unTreeWidget_labelSeparator+sDpName+
                      unTreeWidget_compositeLabelSeparator+
                      sDisplayNodeNameAdded+unTreeWidget_labelSeparator+sDpNodeNameAdded+
                      unTreeWidget_keySeparator+
                      sSelectedKey;
    }
    else {
      sDpNodeNameAdded = sDpName;
    }
    if(dynlen(exInfo)<=0) {
      unTree_linkToDevice(sGraphTree, sDpNodeNameAdded, sSelectedKey, sPanelDpName, UN_PANEL_DPTYPE, dsUserData, exInfo);
      bDone = true;
    }
  }
}

// unWindowTree_handleEventNodeClickOperation
/**
Purpose:
Basic unWindowTree function: handle the event node left click

  @param sNodeName: string, input: the node name
  @param sDpName: string, input: data point name
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_handleEventNodeClickOperation(string sNodeName, string sDpName, string sSelectedKey, dyn_string &exInfo)
{
  string sDeviceName, sDeviceType, sDollarParam;
  string sPanelFileName, buttonPanel;
  dyn_string dsUserData;
  int len;

  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
  fwTree_getNodeUserData(sNodeName, dsUserData, exInfo);

//DebugN("unWindowTree_handleEventNodeClickOperation", sNodeName, sDpName, sSelectedKey, sDeviceName, sDeviceType);
  switch(sDeviceType) {
    case UN_PANEL_DPTYPE:
      if(dpExists(sDeviceName)) {
        len = dynlen(dsUserData);
        if(len > 0)
          sDollarParam = dsUserData[1];
// get the panel from the device definition, check it exists and show it.
        sPanelFileName = unPanel_convertDpPanelNameToPanelFileName(sDeviceName);
        if((getPath(PANELS_REL_PATH,sPanelFileName)!="") && (sPanelFileName != "")){
          //FLAG NEWHMI
          if (globalExists("g_unHMI_RunsInNewHMI")) {
            shape mainWindow = getShape(UNHMI_MAIN_MODULE_NAME + "." + UNHMI_MAIN_PANEL_NAME + ":");
            int currentModule = mainWindow.unHMI_getCurrentModule();
            if(len <= c_unPanelConfiguration_maxPanelNavigation) {
              sDpName = sDeviceName;
            }

            if ((currentModule >=1) && (currentModule <= UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES)) {
              unHMI_openPreviewModuleHelper(sPanelFileName, currentModule, sDollarParam, sDpName);
              // Persist splitter mode when left clicking from window tree
            }  else if (currentModule == UNHMI_SPLITTER_MODULE_NUMBER) {
              unHMI_openPreviewModuleHelper(sPanelFileName, currentModule, sDollarParam, sDpName);
            }  else {
              for (int i=1; i<=UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES; i++) {
                if (mainWindow.unHMI_getIsPreviewModuleOpen(i)){
                  unHMI_openPreviewModuleHelper(sPanelFileName, i, sDollarParam, sDpName);
                  break;
                }
              }
            }
            return;
          }
          // else
          if(len > c_unPanelConfiguration_maxPanelNavigation) // 11, give the sDpNodeName
            unGraphicalFrame_showUserPanel(sPanelFileName, sDollarParam, sDpName);
          else // give the deviceName
            unGraphicalFrame_showUserPanel(sPanelFileName, sDollarParam, sDeviceName);
          unGenericDpFunctions_getContextPanel(sDeviceName, buttonPanel, exInfo);
          if(buttonPanel != "") {
            if(len > c_unPanelConfiguration_maxPanelNavigation) // 11, give the sDpNodeName
              unGraphicalFrame_showContextualPanel(buttonPanel, sDpName);
            else // give the deviceName
              unGraphicalFrame_showContextualPanel(buttonPanel, sDeviceName);
          }
        }
        else {
          fwException_raise(exInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL") + sPanelFileName,"");
        }
      }
      break;
    case NO_DEVICE:
    default:
      break;
  }
}

// unWindowTree_eventNodeRigthClickOperationMenu
/**
Purpose:
Function to build the popup menu when for the eventNodeRigthClickOperationMenu on the unTreeWidget.

  @param dsMenu: dyn_string, input: the popup menu is returned here
  @param sNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_eventNodeRigthClickOperationMenu(dyn_string &dsMenu, string sNodeName, string sDpName, dyn_string &exInfo)
{
//DebugN("unWindowTree_eventNodeRigthClickOperationMenu");
  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Open on top, "+UN_WINDOWTREE_OPENASPOPUP+", 1");
  dynAppend(dsMenu,"SEPARATOR");
  //FLAG NEWHMI
  if (globalExists("g_unHMI_RunsInNewHMI")) {
    shape mainWindow = getShape(UNHMI_MAIN_MODULE_NAME + ".mainWindow:");
    for (int i=1; i<=UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES; i++) {
      if (mainWindow.unHMI_getIsPreviewModuleOpen(i)){
        dynAppend(dsMenu,"PUSH_BUTTON, Add to module" + i + ", " + (i+31) +", 1");
      }
    }
  }


 //append menu entry for opening configuration panel, protected by AC
 bool bGranted, bFileExists;
 string sAccessLevel;
 dyn_string exceptionInfo;
  sAccessLevel = unGenericButtonFunctionsHMI_getPanelAccessLevel(unWindowTree_FILENAME_CONFIGPANEL, bFileExists);
  unGenericButtonFunctionsHMI_isAccessAllowed(unWindowTree_FILENAME_CONFIGPANEL,
                                              UN_GENERIC_USER_ACCESS,
                                              sAccessLevel,
                                              bGranted,
                                              exceptionInfo);
  dynAppend(dsMenu,"PUSH_BUTTON, Window Tree Configuration..., "+UN_TRENDTREE_openEditor+", "+(int)(bFileExists && bGranted));

}

// unWindowTree_handleEventNodeRigthClickOperation
/**
Purpose:
Basic unWindowTree function: handle the event node rigth click

  @param sNodeName: string, input: the node name
  @param sDpName: string, input: data point name
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param iAnswer: int, input: selected menu
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_handleEventNodeRigthClickOperation(string sNodeName, string sDpName, string sSelectedKey, int iAnswer, dyn_string &exInfo)
{
  string sDeviceName, sDeviceType, sDollarParam;
  string sPanelFileName;
  dyn_string dsDollarParam, dsUserData;
  int len;
   string sGraphTree = "tree";
  shape aShape = getShape(sGraphTree);
  dyn_float dfRet;
  dyn_string dsRet;

//DebugN("unWindowTree_handleEventNodeRigthClickOperation", sNodeName, sDpName, sSelectedKey, sDeviceName, sDeviceType, iAnswer);
  switch(iAnswer){
    case UN_TRENDTREE_OPENASPOPUP:

		fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
		fwTree_getNodeUserData(sNodeName, dsUserData, exInfo);

		switch(sDeviceType) {
		  case UN_PANEL_DPTYPE:
			if(dpExists(sDeviceName)) {
			  len = dynlen(dsUserData);
			  if(len > 0)
				sDollarParam = dsUserData[1];
			  dsDollarParam = strsplit(sDollarParam, ";");
	// get the panel from the device definition, check it exists and show it.
			  sPanelFileName = unPanel_convertDpPanelNameToPanelFileName(sDeviceName);
			  if((getPath(PANELS_REL_PATH,sPanelFileName)!="") && (sPanelFileName != "")){
				ModuleOnWithPanel(sNodeName, 0,150,0,0,0,0,0, sPanelFileName, sNodeName, dsDollarParam);
				stayOnTop(true, sNodeName);
			  }
			  else {
				fwException_raise(exInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL") + sPanelFileName,"");
			  }
			}
			break;
		  case NO_DEVICE:
		  default:
			break;
		}
    break;
    case UN_TRENDTREE_openEditor:
      unGraphicalFrame_ChildPanelOnCentralReturn( unWindowTree_FILENAME_CONFIGPANEL,"WindowTreeConfiguration",makeDynString(),dfRet, dsRet);
      delay(0,100);
      //refresh
      unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
      unTree_EventInitializeOperation(sGraphTree, "treeBar", false);
      if(g_iSortMode!=unTreeWidget_popupMenu_sortCustom)
      {
        unTreeWidget_setPropertyValue(aShape, "sort", g_iSortMode, true);
      }
    break;
    //NEWHMI
    //case for docked module
    case UN_TRENDTREE_openInPreview1:
      _openInPreviewPanel(sNodeName, sDpName, 1, exInfo);
      break;
    case UN_TRENDTREE_openInPreview2:
      _openInPreviewPanel(sNodeName, sDpName, 2, exInfo);
      break;
    case UN_TRENDTREE_openInPreview3:
      _openInPreviewPanel(sNodeName, sDpName, 3, exInfo);
      break;
    case UN_TRENDTREE_openInPreview4:
      _openInPreviewPanel(sNodeName, sDpName, 4, exInfo);
      break;
    //end case
  }
}

private void _openInPreviewPanel(string sNodeName, string sDpName, int i, dyn_string &exInfo)
{
  string sDeviceName, sDeviceType;
  string sPanelFileName;
  dyn_string dsDollarParam, dsUserData;
  int len;

  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
  fwTree_getNodeUserData(sNodeName, dsUserData, exInfo);
  len = dynlen(dsUserData);
  if(len <= c_unPanelConfiguration_maxPanelNavigation) {
    sDpName = sDeviceName;
  }

  if(sDeviceType == UN_PANEL_DPTYPE) {
    if(dpExists(sDeviceName)) {
      sPanelFileName = unPanel_convertDpPanelNameToPanelFileName(sDeviceName);
      string dollarParams = (len>0) ? dsUserData[1] : "";
      unHMI_openPreviewModuleHelper(sPanelFileName, i, dollarParams, sDpName);
    }
  }
  return;
}

// unWindowTree_handleEventNodeClickSelectNode
/**
Purpose:
Basic unWindowTree function: handle the event node left click

  @param sNodeName: string, input: the node name
  @param sDpName: string, input: data point name
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_handleEventNodeClickSelectNode(string sNodeName, string sDpName, string sSelectedKey, dyn_string &exInfo)
{
  string sDeviceName, sDeviceType;
  string sDisplayNodeName;
  int len;

  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);

//DebugN("unWindowTree_handleEventNodeClickSelectNode", sNodeName, sDpName, sSelectedKey, sDeviceName, sDeviceType);
  switch(sDeviceType) {
    case UN_PANEL_DPTYPE:
      if(dpExists(sDeviceName)) {
        nodeDpSelected.text = sDpName;
        sDisplayNodeName = fwTree_getNodeDisplayName(sNodeName, exInfo);
        nodeSelected.text = sDisplayNodeName + UN_TREE_DEVICE_SEPARATOR+unPanel_convertDpPanelNameToPanelFileName(sDeviceName);
        displayNodeSelected.text = sDisplayNodeName;
      }
      break;
    case NO_DEVICE:
    default:
      break;
  }
//DebugN(exInfo);
}

// unWindowTree_openPanelPreExecute
/**
Purpose:
function executed before the unTree_openPanel execution in the case of adding the panel as symbol in the configuration panel.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unWindowTree_openPanelPreExecute()
{
  // User panel construction has changed. This method remains for legacy reasons.
  // Introduction:
  // unTreeConfiguration.pnl uses $-params that map to this function and stores them (via method in .ctl) in hidden textfields to execute them when necessary.
  // As named panel is the template for the Window Tree as well as the Trend Tree panel, one $-param value leads here, the other one to the sister method in unTrendTree.ctl.
  // In order to not break the structure of the code, this method has to remain until the particular part of unTrendTree.ctl is redesigned as well, though it remains empty.
}
//@}




