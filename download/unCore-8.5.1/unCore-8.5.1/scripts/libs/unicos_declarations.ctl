/**@name LIBRARY: unicos_declarations.ctl

@author: Vincent Forest (AB-CO)
 
Creation Date: 21/05/2003

Modification History: 
  02/03/2015: Jean-Charles
  - {IS-1781} unCore: Integration with fwSynopticLocator


  17/01/2014: Herve
  - IS-1328 - FipDiag expert interface call modified with FESA3

  15/10/2013: boccioli
  - ENS-7119 UN_FACEPLATE_CLOSE_DELAY set from 10 to 3.
  
  30/03/2012: Herve
  - IS-755  unCore - unFrontEnd  Front-end (SIEMENS and SCHNEIDER PLCS UCPC v6) Information updated: baseline and 
  application versions added; spec version dropped. Front-end diagnostic panel updated accordingly. 
  
  20/03/2012: Herve
  - IS-692  unCore - unFrontEnd  Schneider PLC front-end: Include Specs/App version and "PLC modbus address-of-the-application/specs version" 
  - IS-738  unCore - unFrontEnd  SCHNEIDER PLC: new field "resources package employed" used for the CPC 6
  - IS-739  unCore - unFrontEnd  mix Com and Data file for the _UnPlc front-end part of the device configuration
 
   14/10/2011: Herve
  - IS-593  unCore - import/export  stop/restart SIMULATOR/driver from import DB panel 
 
  08/2011: Herve
  - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity
  
  28/06/2011: Herve
  - IS-553: periodic report based on a user defined function
  
  06/05/2011: Herve
  -IS-527: Configuration dpe (dyn_string) for all UNICOS types
  
  30/03/2011: Herve
  - IS-472: constant for the unDeviceSet script
  
  25/11/2010: Herve
  - IS-459: global variable g_bUnGraphicalFrame_RDB
  
  15/09/2010: Herve
  - IS-400: new global in order to have only one information window poped-up on exit in case of multiple screen
  
  29/09/2009: Frederic
  - add BACKUP constants
  
  10/08/2009: Herve
    - front-end proxy and trend implementation
    new global: g_mFrontEndProxyList, g_mFrontEndTrendConfig
    
  04/08/2009: Herve
    - device proxy implementation
    new global: g_ddsObjectProxyList
    new constant: UN_PROXY_DELIMITER
    
  17/02/2009: Herve
    - new global var for device widget list
    - new global var for Front-end widget list and config
    
  05/02/2009 Quique
      - New tag for a new bit in the stsreg01 controller: UN_STSREG01_AUINHSAVRES
      
  01/12/2008: Herve
    - old/new smoothing: UN_CONFIG_DEADBAND_OLD_NEW
    
  21/11/08 Quique
      - New constants for StsReg02 (analog) + Controller...
      
  22/10/2008: Herve
    - new constants for HMI functions
    
  01/10/2008: Herve
    - new globals for menu, cascade button, etc.
    - new constant for Alarm
    
  23/05/2008: Herve
    - new global for the debug utility
    
  18/04/2008: Herve
    - DEVUN_OFFSET_HEIGHT + 30
    - DEVUN_OFFSET_WIDTH + 50
    
  07/04/2008: Herve
    - new constant for Graphical Frame
    
  20/03/2008: Herve
    - new global for TrendFaceplate config
    
  26/02/2008: Herve
    - new constant for Controller ActiveX widget
    
  28/12/2007: Herve
    - device access control
    
	18/12/2007: Herve
		- new constant for unDeviceListUpdate.ctl
		
	15/10/2007: Herve
		- new constant for nameCheck
		
	28/09/2007: Herve
		- remove g_m_bTreeDeviceOverviewSystemForceSelectSort
		 
	25/09/2007: Frederic
		- new constants for pcDevices (common with PIC, LHC_CIRCUIT)
	
	20/09/2007: Herve
		- new global for treedev overview.
		
	16/07/2007: Herve
		- new constant for FipDiag
		
	29/06/2007: Herve
		- new bit in STSREG01 for Controller device
		
	30/04/2007: Herve
		- new constant for ProfileGraph
	
	24/04/2007: Pablo
		- constants for Fip Info Expert Console.
		
	24/04/2007: Herve
		- new global var for treeDeviceOverview: g_m_bTreeDeviceOverview_deviceListAvailable, true if the device list of the 
		current system is initialised, false if not.
		- replace Devive by Device
		
	27/02/2007: Herve
		- constant for tree device overview
		
	20/02/2007: Herve
		- add global var for unicosHMI
		
	12/11/2007: Herve
		- new constant for treeDeviceOverview

	04/12/2006: Quique
		- new constant to extend PID default parameters functionality in S7 PLCs (ManReg01)
		
	01/11/2006: Herve
		- add new constant for extended archive config to import/export
		
	12/07/2006: Frederic
		- new constant for OWS-DS link: UN_SHAREFOLDER_OWS_TO_DS
	
	15/05/2006: Herve
		- new constants for TreeDeviceOverview
		- new constants for alarmLetter in AI/AO widget
		
	02/05/2006: Herve
		- add constant UN_POPUPMENU_DEVICELINK
		
	18/04/2006: Herve
		- implementation of mask event
		
	11/04/2006: Herve
		- modify constants UN_POPUPMENU_TREND_CSTE and UN_POPUPMENU_MIN

	05/04/2006: Herve
		- new variables for treeDeviceOverview
		
	29/03/2006: Frederic
		- new constant to build the filter on alias (EventList,...): UN_GRANTED_REJECTED_DELIMITER
				
	21/03/2006: Herve
		- new constant for treeDeviceOverview: filter on alias
		
	23/01/2006: Herve
		- add IBARWIDTH constant for profileGraph
		
	14/01/2006: Herve
		- add IMPORT_PATTERN and remove ImportFile_pattern
		- implementation of multi domain, multi nature: UN_CONFIG_GROUP_SEPARATOR
		
	11/11/2005: Celine
		- add CMWClient_pattern
	10/10/2005: Celine
		- add CMWServer_pattern 
	13/09/2005: Herve
		- add constant for Alias/Description used in unModifyConfig.pnl
		
	01/09/2005: Herve
		- add constant for tree device overview
		
	25/08/2005: Herve
		- add constants for device overview panels
		
	12/07/2005: Herve
		- add the global variable for the function unGenericDpFunctions_getDeviceDefinitionDPEDescription
				g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPE; // list of DPEs
				g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPEdescription; // list of description

	04/07/2005: Herve set UN_CONFIG_EXPORT_NONE to "" instead of "none"
	
	23/06/2005: Celine
		- add UN_PROCESSALARM_DIGITAL_ALERT_CLASS, UN_PROCESSALARM_ANALOG_ALERT_CLASS_LL, UN_PROCESSALARM_ANALOG_ALERT_CLASS_L, 
		UN_PROCESSALARM_ANALOG_ALERT_CLASS_H, UN_PROCESSALARM_ANALOG_ALERT_CLASS_HH
	22/06/2005: Herve
		- add UN_CONFIG_UNITY
		
	21/06/2005: Herve
		- increment UN_CONFIG_ALLCOMMON_LENGTH, UN_CONFIG_OBJECTGENERAL_LENGTH
		- add UN_CONFIG_OBJECTGENERAL_PLCNAME		
	17/06/2005: Celine
		- add UN_PROCESSALARM_EXTENSION, UN_PROCESSALARM_PATTERN
	01/06/2005: Herve
		- set UN_CONFIG_EXPORT_ARCHIVE_1, UN_CONFIG_EXPORT_ARCHIVE_2 and UN_CONFIG_EXPORT_ARCHIVE_3 to ""
		
	25/05/2005: Herve
		- add a new global: gPopUpOpenedPanel 

  19/05/2005: Enrique
  	- add new PLC types for S7 PLC:
  			UN_CONFIG_S7_400	"S7_400"	
  			UN_CONFIG_S7_300	"S7_300"
  	- add new S7 PLC dp type
			S7_PLC_DPTYPE	"S7_PLC"
  			
	02/05/2005: Frederic
		- add UN_CONFIG_EXPORT_ARCHIVE_1, 2, 3 and UN_CONFIG_EXPORT_LENGTH...
	
	29/04/2005: Frederic
		- add 3 constants for "Export Configuration"

	21/04/2005: Herve
		- implement the evnt archive: 
			. increment UN_CONFIG_PLC_ADDITIONAL_LENGTH
			. rename UN_CONFIG_OBJECTGENERAL_JCOPCONFIG to UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT
		- remove UN_CONFIG_DEFAULT_ARCHIVE_SMOOTHING and replace in libs by DPATTR_ARCH_PROC_SIMPLESM
		- allow archive in import file per device
			. add UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG
			UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT
		- add constant UN_CONFIG_FRONTEND_GETARCHIVEDPFUNCTION
		
	15/04/2005: Herve
		- new constant 
		
	22/02/2005: Herve
		- add constant UN_TREND_DEFAULT_FORMAT_FIXED_DISPLAY_EXTENTION
		- bug with description of the alarm
		
	15/02/2005: Herve
		- add constant for the generation of FESystemAlarm (SystemAlarm key word)
		
	10/02/2005: Herve
		- add global variables for the optimization of the unGenericDpFunctions_dpAliasToName
		
	08/02/2005: Herve
		- add constants for the ProfileGraph 
		
	04/02/2005: Herve
		- add the constants for the ProfileGraph widget
		
	14/12/2004: Herve 
		- add 3 constants: UN_DISPLAY_VALUE_DIGIT_FLOAT_TYPE, UN_DISPLAY_VALUE_DIGIT_FLOAT_FORMAT
		UN_DISPLAY_VALUE_DIGIT_TYPE for fixed digit display.
	
	02/12/2004: Herve
		- add the new menu choice
		
	29/11/2004: Herve
		move the LHCLogging_pattern and LASER_pattern into unicosObjects_declarations.ctl

	25/11/2004: Frederic
		add constants: 	UN_ALERT_DELAY
										UN_ALERT_TIMEOUT
										UN_ALERT_AES_ROW
										UN_ALERT_AES_SCREEN
	
	17/11/2004: Herve
		add constant for application colour calculation
	
	27/10/2004: Herve
		- add UN_GENERICOBJECT_FIELD_TRENDFACEPLATEPANEL for the faceplate panel config
		
	21/10/2004: Herve
		- add definition for background printing
		
	18/10/2004: Frederic
		- add unGeneration constants (event 32 mode)

	15/10/2004: Herve
		- add EventList constant
		- add ObjectList constant
		- add unModifyConfig.pnl constant
		
	22/09/2004: Frederic, update Alerts constants for 3.0
		- add UN_DELAY_REFRESH_ALERT_TABLE
		- change type from string to int -> UN_ALERTPANEL_CONFIG & UN_ALERTPANEL_SYSTEM_INTEGRITY

	22/07/2004: Herve, update to 3.0
		- contant from unPanel
		- remove the constant: UN_PANEL_VIEW_OPERATION, device model used instead
		- remove UN_PANEL_WINDOW_TREE_PREFIX
		- add constant: UN_PANEL_DPNAME_DELIMITER, UN_PANEL_FILENAME_EXTENTION, 
			UN_PANEL_FILENAME_PATH_DELIMITER, UN_PANEL_DPTYPE
		- add constant: UN_WINDOWTREE_ROOT_FOLDER, UN_WINDOWTREE_PANEL, UN_TRENDTREE_ROOT_FOLDER
		UN_TRENDTREE_PLOT, UN_TRENDTREE_PAGE
		- remove the following lines:
const string UN_TRENDTREE_REF = "TrendTree";		// ref to the TrendTree
const string UN_WINDOWTREE_REF = "WindowTree";		// ref to the WindowTree
const string UN_TRENDTREE_PREFIX = "trendTree_";	// dp pattern for TrendTree
const string UN_WINDOWTREE_PREFIX = "windowTree_";	// dp pattern of WindowTree
const string UN_TRENDTREE_TYPE = "_FwViews";		// DPT of TrendTree
const string UN_WINDOWTREE_TYPE = "_FwViews";		// DPT of WindowTree
const string UN_TRENDTREE_CONTEXT = "";			// context for start of TrendTree
const string UN_WINDOWTREE_CONTEXT = "";			// context for start of WindowTree
		
	06/05/2004: Frederic
		add UN_CONFIG_PLC_ADDITIONAL_EVENT16
			UN_CONFIG_OBJECTGENERAL_EVENT16
	
	15/04/2004: Herve
		add UN_DEFAULT_OBJECTLIST_DPE, UN_DEFAULT_DPFILTER_DPE, UN_DEFAULT_PREFIX

	13/04/2004: Herve
		add cste_select, cste_maskPVSS and cste_BlockPLC;

	02/04/2004: Herve
		add constant for FrontEnd

	01/04/2004: Herve
		set UN_WIDGET_TEXT_ALARM_DOES_NOT_EXIST to "" instead of "N"

	25/03/2004: Herve
		add HH, H, L and LL, L, H alarm case for AnalogInput/AnalogOutput device

	20/02/2004: Herve
		add constant for the Tree configuration.

	20/01/2004: Frederic
		add constant to define Deadband Type
			const int UN_CONFIG_DEADBAND_RELATIF = 2;
			const int UN_CONFIG_DEADBAND_VALUE = 1;
			const int UN_CONFIG_DEADBAND_NONE = 0;
		
	05/09/2003: Herve
		remove const string UN_DPNAME_PREFIX = "un";
		bug in case the prefix is not UN_DPNAME_PREFIX, case GCS for instance: see RAZOR version 2.6 of the file
		also in unConfigGenericFunction.ctl version 1.14
		remove the UN_WIDGET_PREFIX and UN_WIDGET_PATH
		add UN_WIDGET
		remove the two lines (constant not used and may confused the developer) the address are read from the config file
			const int UN_PREMIUM_SEND_IP_ADDRESS = 70;
			const int UN_QUANTUM_SEND_IP_ADDRESS = 55799;

	16/09/2003: Herve
		add constant UN_CONFIG_PLC_EXTENDED_ADDRESS_CMD: address for command interface 
		modify UN_CONFIG_PLC_EXTENDED_LENGTH

	07/10/2003: Herve
		move systemIntegrity from unicosObjects to unCore

	22/10/2003: Herve:
		add the following cases for alarm: H&L, HH&H, LL&L, H, L

version 1.0

External Functions: None

Internal Functions: None

Purpose: This library contains Unicos constants.

Usage: Public

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

const string UN_TEXT_EDITOR_WIN32 = "notepad.exe";
const string UN_TEXT_EDITOR_UNIX = "nedit";
global anytype g_unGenericDpFunctions_waitForSingleValue;
global dyn_anytype g_unGenericDpFunctions_waitForDynValue;
global bool g_unGenericDpFunctions_waitForDynValue_change;

//--------------------------------------------------------------------------------------------------------------------------------

const string UN_DEFAULT_DELIMITER = "~";

//---------
// OBJECTS
//---------

global dyn_string g_dsObjectList;
global dyn_dyn_string g_ddsObjectConfigs;
global dyn_dyn_string g_ddsObjectWidgetList;
global mapping g_mFrontEndConfigs, g_mFrontEndWidgetList, g_mFrontEndProxyList, g_mFrontEndTrendConfig;
global bool g_bObjectInitRecall = true;
global unsigned g_unGraphicalFrame_manRegTimeOut = 0; 			// global variable containing the timeout of manReg.
global dyn_dyn_string g_ddsObjectProxyList;

const string UN_PCO_OPTION_MODE_PATH = ".ProcessInput.OpMoSt";	// Labels are coded in the description of the DPE <PCO name> + UN_PCO_OPTION_MODE_PATH
const string UN_PCO_OPTION_MODE_DELIMITER = ";";				// ; delimited string
const unsigned UN_PCO_OPTION_MODE_NUMBER = 8;					// Max possible option mode

const int UN_ACTION_DPCONNECT = 1;
const int UN_ACTION_DISCONNECT = 2;
const int UN_ACTION_DPDISCONNECT_DISCONNECT = 3;
const int UN_ACTION_NOTHING = 4;

const string UN_ACKNOWLEDGE_PLC = "PLC";

const unsigned UN_GENERICOBJECT_FUNCTION_LENGTH = 15;
const unsigned UN_GENERICOBJECT_FUNCTION_ACKALARM = 1;
const unsigned UN_GENERICOBJECT_FUNCTION_FACEPLATESTATUSREGISTER = 2;
const unsigned UN_GENERICOBJECT_FUNCTION_FACEPLATESTATUSDISCONNECT = 3;
const unsigned UN_GENERICOBJECT_FUNCTION_BUTTONREGISTER = 4;
const unsigned UN_GENERICOBJECT_FUNCTION_BUTTONSETSTATE = 5;
const unsigned UN_GENERICOBJECT_FUNCTION_BUTTONDISCONNECT = 6;
const unsigned UN_GENERICOBJECT_FUNCTION_BUTTONUSERACCESS = 7;
const unsigned UN_GENERICOBJECT_FUNCTION_WIDGETREGISTER = 8;
const unsigned UN_GENERICOBJECT_FUNCTION_WIDGETDISCONNECT = 9;
const unsigned UN_GENERICOBJECT_FUNCTION_MENUCONFIG = 10;
const unsigned UN_GENERICOBJECT_FUNCTION_GENERATIONCHECK = 11;
const unsigned UN_GENERICOBJECT_FUNCTION_GENERATIONIMPORT = 12;
const unsigned UN_GENERICOBJECT_FUNCTION_USERCBGETBUTTONSTATE = 13;
const unsigned UN_GENERICOBJECT_FUNCTION_HANDLEMENUANSWER = 14;
const unsigned UN_GENERICOBJECT_FUNCTION_OBJECTLIST = 15;

const unsigned UN_GENERICOBJECT_FIELD_SELECT = 1;
const unsigned UN_GENERICOBJECT_FIELD_FACEPLATE = 2;
const unsigned UN_GENERICOBJECT_FIELD_BUTTON = 3;
const unsigned UN_GENERICOBJECT_FIELD_FACEPLATETREND = 4;
const unsigned UN_GENERICOBJECT_FIELD_TRENDDPE = 5;
const unsigned UN_GENERICOBJECT_FIELD_TRENDFACEPLATEPANEL = 6;

const string UN_DEFAULT_TRENDFACEPLATEPANEL = "objects/fwTrending/fwTrendingFaceplate.pnl";
const string UN_DEFAULT_FACEPLATESMALLTRENDPANEL = "fwTrending/fwTrendingZoomedWindow.pnl";

const unsigned UN_GENERICWIDGET_FUNCTION_LENGTH = 2;
const unsigned UN_GENERICWIDGET_FUNCTION_DISCONNECTION = 1;
const unsigned UN_GENERICWIDGET_FUNCTION_ANIMATION = 2;

const unsigned UN_PARAMETER_LENGTH = 8;					// Fields number
const string UN_PARAMETER_DELIMITER = ";";				// Field delimiter
const string UN_PARAMETER_SUBITEM_DELIMITER = "|";				// Sub-Field delimiter
const string UN_PARAMETER_RANGE_DELIMITER = ":";		// Field delimiter in faceplate range configuration
const unsigned UN_PARAMETER_DIAGNOSTIC = 1;				// Field 1 is a path to diagnostic panel
const unsigned UN_PARAMETER_HTML = 2;					// Field 2 is the html help link
const unsigned UN_PARAMETER_PANEL = 3;					// Field 3 is a path to default device panel
const unsigned UN_PARAMETER_DOMAIN = 4;					// Field 4 gives device domain
const unsigned UN_PARAMETER_NATURE = 5;					// Field 5 gives device nature
const unsigned UN_PARAMETER_NAME = 6;					// Field 6 gives device name (used for PCO object)
const unsigned UN_PARAMETER_FACEPLATE_RANGE = 7;		// Field 7 gives device range configuration for faceplates
const unsigned UN_PARAMETER_WIDGET = 8;					// Field 8 gives device widget type

const string UN_DOMAIN_PREFIX = "Domain:";
const string UN_NATURE_PREFIX = "Nature:";

const string UN_WIDGET = "Widget_";

const string UN_DPNAME_SEPARATOR = "-";
const unsigned UN_DPNAME_FIELD_LENGTH = 5;
const unsigned UN_DPNAME_FIELD_PREFIX = 1;
const unsigned UN_DPNAME_FIELD_PLCNAME = 2;
const unsigned UN_DPNAME_FIELD_PLCSUBAPPLICATION = 3;
const unsigned UN_DPNAME_FIELD_OBJECT = 4;
const unsigned UN_DPNAME_FIELD_NUMBER = 5;
const string UN_MY_MANNUM = "myManNum="; // constant key word used in to show the selected DP in the select area in unicosHMI

const string CSTE_ALERT_ACTIVE = "YES";
const string CSTE_ALERT_INACTIVE = "NO";
const string CSTE_OK_RANGE_1 = "ON";
const string CSTE_OK_RANGE_0 = "OFF";
const string CSTE_ARCHIVE = "Arch.";
const string CSTE_ADDRESS = "Address";
const string CSTE_SMOOTHING = "Smoothing";
const string CSTE_RANGE = "Range";
const string CSTE_ALERT = "Alert";
const string CSTE_ALIAS = "Alias/Descrip.";

const string UN_DEVICE_DYNAMICTREND = "_DynamicTrend";

//--------------------------------------------------------------------------------------------------------------------------------

//------------------------
// REGISTER BIT POSITIONS
//------------------------

const unsigned UN_STSREG01_ONST = 0;							// On status
const unsigned UN_STSREG01_POSST = 0;							// Position status
const unsigned UN_STSREG01_TST = 0;								// Tracking status
const unsigned UN_STSREG01_ACTIVE = 1;								// Active status
const unsigned UN_STSREG01_OFFST = 1;							// Off status
const unsigned UN_STSREG01_AUMOST = 2;							// Auto mode
const unsigned UN_STSREG01_MMOST = 3;							// Manual mode
const unsigned UN_STSREG01_FOMOST = 4;							// Forced mode
const unsigned UN_STSREG01_LDST = 5;							// Local drive status
const unsigned UN_STSREG01_REMOST = 5;							// Regulation mode status
const unsigned UN_STSREG01_IOERRORW = 6;						// IO error warning
const unsigned UN_STSREG01_IOSIMUW = 7;							// IO simulated warning
const unsigned UN_STSREG01_AUREGR = 8;							// Auto Regulation Request
const unsigned UN_STSREG01_AUMRW = 8;							// Auto manual request warning
const unsigned UN_STSREG01_FODIPRO = 8;							// Forced value <> process value
const unsigned UN_STSREG01_FODIAU = 8;							// Forced value <> auto value
const unsigned UN_STSREG01_POSALE = 8;							// Position Alarm Enabled
const unsigned UN_STSREG01_POSAL = 9;							// Position alarm
const unsigned UN_STSREG01_POSALW = 9;							// Position alarm warning
const unsigned UN_STSREG01_FUSTOPAL = 9;						// Full stop alarm
const unsigned UN_STSREG01_STARTIST = 10;						// Start interlock status
const unsigned UN_STSREG01_STARTAL = 10;						// Start interlock alarm
const unsigned UN_STSREG01_STOPIST = 11;						// Stop interlock status
const unsigned UN_STSREG01_TSTOPAL = 11;						// Temporary stop alarm
const unsigned UN_STSREG01_ALMSKST = 11;						// Alarm masked
const unsigned UN_STSREG01_ALUNACK = 12;						// Alarm unacknowledge
const unsigned UN_STSREG01_AUINHSAVRES = 12;						// Auto inhibit save/restore (controller)
const unsigned UN_STSREG01_AUINHFMO = 13;						// Auto inhibit forced mode
const unsigned UN_STSREG01_PFSPOSONST = 14;						// Parameter failsafe position on status
const unsigned UN_STSREG01_AUESPO = 14;							// Parameter failsafe position on status (Controller)
const unsigned UN_STSREG01_HFST = 14;							// Hardware feedback status
const unsigned UN_STSREG01_AUPOSRST = 14;						// Auto position request status
const unsigned UN_STSREG01_PHFONST = 14;						// Parameter Feed Back On
const unsigned UN_STSREG01_AUINHMMO = 15;						// Auto inhibit manual mode
const unsigned UN_STSREG01_PHFOFFST = 15;						// Parameter Feed Back OFF

const unsigned UN_STSREG02_OUTOVST = 0;							// Output order value status
const unsigned UN_STSREG02_RUNOST = 0;							// Run order status
const unsigned UN_STSREG02_STRPAREKP = 0;						// PID Kp parameter
const unsigned UN_STSREG02_DOUTONO = 0;							// Digital output on
const unsigned UN_STSREG02_AUONRST = 1;							// Auto on request status
const unsigned UN_STSREG02_STRPARETI = 1;						// PID Ti parameter
const unsigned UN_STSREG02_DOUTOFFO = 1;						// Digital output off
const unsigned UN_STSREG02_MONRST = 2;							// Manual on request status
const unsigned UN_STSREG02_STRPARETD = 2;						// PID Td parameter
const unsigned UN_STSREG02_AUOFFRST = 3;						// Auto off request status
const unsigned UN_STSREG02_STRPAREKD = 3;						// PID Kd parameter
const unsigned UN_STSREG02_MOFFRST = 4;							// Manual off request status
const unsigned UN_STSREG02_BOPKP = 4;							// By operator Kp parameter
const unsigned UN_STSREG02_HONRST = 5;							// Hardware on request status
const unsigned UN_STSREG02_BOPTI = 5;							// By operator Ti parameter
const unsigned UN_STSREG02_AUCSTOP = 5;							// Auto controlled stop order
const unsigned UN_STSREG02_HOFFRST = 6;							// Hardware off request status
const unsigned UN_STSREG02_ALACTB = 6;							// Alarm action blocked
const unsigned UN_STSREG02_BOPTD = 6;							// By operator Td parameter
const unsigned UN_STSREG02_PHFONST = 7;							// Parameter hardware feedback on status
const unsigned UN_STSREG02_TFUSTOP = 7;							// Temporary as full stop
const unsigned UN_STSREG02_BOPKD = 7;							// By operator Kd parameter
const unsigned UN_STSREG02_PHFOFFST = 8;						// Parameter hardware feedback off status
const unsigned UN_STSREG02_CSTOPOST = 8;						// Controlled stop order status
const unsigned UN_STSREG02_BOPSPOH = 8;							// By operator setpoint limit high
const unsigned UN_STSREG02_PPULSEST = 9;						// Parameter pulse status
const unsigned UN_STSREG02_BOPSPOL = 9;							// By operator setpoint limit low
const unsigned UN_STSREG02_PHLDST = 10;							// Parameter hardware local drive status
const unsigned UN_STSREG02_BOPOUTH = 10;						// By operator output limit high
const unsigned UN_STSREG02_PHLIST = 11;							// Parameter hardware local input status
const unsigned UN_STSREG02_BOPOUTL = 11;						// By operator output limit low
const unsigned UN_STSREG02_PHDOUST = 12;						// Parameter hardware digital output status
const unsigned UN_STSREG02_ATESPOH = 12;						// Enable setpoint limit high
const unsigned UN_STSREG02_ATESPOL = 13;						// Enable setpoint limit low
const unsigned UN_STSREG02_ATEOUTH = 14;						// Enable output limit high
const unsigned UN_STSREG02_ATEOUTL = 15;						// Enable output limit low
const unsigned UN_STSREG02_ALBW = 13;                                                                                                          // Dependent Alarm blocked warning

const unsigned UN_MANREG01_MAUMOR = 0;							// Auto mode bit position in ManReg01
const unsigned UN_MANREG01_MMMOR = 1;							// Manual mode bit position in ManReg01
const unsigned UN_MANREG01_MFOMOR = 2;							// Forced mode bit position in ManReg01
const unsigned UN_MANREG01_MREMOR = 3;							// Regulation mode bit position in ManReg01
const unsigned UN_MANREG01_MONR = 4;							// On Open request bit position in ManReg01
const unsigned UN_MANREG01_MDEFPAR = 4;							// Request restoring PID parameters
const unsigned UN_MANREG01_MDEFPARSAVE = 5;					        // Request SAVE PID parameters (version PID without addressing)
const unsigned UN_MANREG01_MOFFR = 5;							// Off Close request bit position in ManReg01
const unsigned UN_MANREG01_MCSTOPR = 6; 						// Manual controlled stop request bit position in Manreg01
const unsigned UN_MANREG01_MNEWMR = 6;							// Manual new manual request bit position in ManReg01
const unsigned UN_MANREG01_MNEWPOSR = 6;						// Manual new position request bit position in ManReg01
const unsigned UN_MANREG01_MSPINR = 7;							// Increase value
const unsigned UN_MANREG01_MMASKALR = 7;						// Mask alarm bit position in ManReg01
const unsigned UN_MANREG01_MNEWSPR = 7;							// Manual new setpoint request bit position in ManReg01
const unsigned UN_MANREG01_MSPDER = 8;							// Decrease value
const unsigned UN_MANREG01_MUNMASKALR = 8;						// Unmask alarm bit position in ManReg01
const unsigned UN_MANREG01_MSETTSASFS = 8;   					// Manual set temporary as full stop bit position in Manreg01
const unsigned UN_MANREG01_MNEWSPLHR = 8;						// Manual new setpoint limit high request bit position in ManReg01
const unsigned UN_MANREG01_MRSETTSASFS = 9; 					// Manual reset temporary as full stop bit position in Manreg01
const unsigned UN_MANREG01_MNEWSPLLR = 9;						// Manual new setpoint limit low request bit position in ManReg01
const unsigned UN_MANREG01_MBALACT = 10;						// Manual block alarm action bit position in ManReg01
const unsigned UN_MANREG01_MNEWPOSLHR = 10;						// Manual new position limit high request bit position in ManReg01
const unsigned UN_MANREG01_MDALACT = 11;						// Manual deblock alarm action bit position in ManReg01
const unsigned UN_MANREG01_MNEWPOSLLR = 11;						// Manual new position limit low request bit position in ManReg01
const unsigned UN_MANREG01_MOPMOR = 12;							// Manual option mode request bit position in ManReg01
const unsigned UN_MANREG01_MNEWKPR = 12;						// Manual new Kp parameter request bit position in ManReg01
const unsigned UN_MANREG01_MAUDEOR = 13; 						// Manual auto to dependent object request bit position in ManReg01
const unsigned UN_MANREG01_MNEWTDR = 13;						// Manual new Td parameter request bit position in ManReg01
const unsigned UN_MANREG01_MNEWTIR = 14;						// Manual new Ti parameter request bit position in ManReg01
const unsigned UN_MANREG01_MNEWKDR = 15;						// Manual new Kd parameter request bit position in ManReg01
const unsigned UN_MANREG01_MALACK = 15;							// Acknowledge alarm bit position in ManReg01

const int UN_CONTROLLER_SHAPE_LIMIT_YSIZE = 0;
//--------------------------------------------------------------------------------------------------------------------------------

//---------
// BUTTONS
//---------

const string UN_FACEPLATE_BUTTON_PREFIX = "button";				// Prefix of button shape name
const string UN_FACEPLATE_BUTTON_SELECT_TEXTSELECT = "Select";	// Select text
const string UN_FACEPLATE_BUTTON_SELECT_TEXTDESELECT = "Deselect";	// Deselect text
const string UN_FACEPLATE_BUTTON_AUTO_MODE = "Auto";			// Auto mode button
const string UN_FACEPLATE_BUTTON_MANUAL_MODE = "Manual";		// Manual mode button
const string UN_FACEPLATE_BUTTON_FORCED_MODE = "Forced";		// Forced mode button
const string UN_FACEPLATE_BUTTON_REGULATION_MODE = "Regulation";// Regulation mode button
const string UN_FACEPLATE_BUTTON_ONOPEN_REQUEST = "OnOpen";		// On Open request button
const string UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST = "OffClose";	// Off Close request button
const string UN_FACEPLATE_BUTTON_CONTROLLED_STOP_REQUEST = "ControlledStop"; 		// Manual controlled stop request button
const string UN_FACEPLATE_BUTTON_INC_VALUE = "Increase";		// Increase value
const string UN_FACEPLATE_BUTTON_MASK_ALARM = "MaskAlarm";		// Mask alarm button
const string UN_FACEPLATE_BUTTON_DEC_VALUE = "Decrease";		// Decrease value
const string UN_FACEPLATE_BUTTON_UNMASK_ALARM = "UnmaskAlarm";	// Unmask alarm button
const string UN_FACEPLATE_BUTTON_SET_TEMPORARY_AS_FULL_STOP = "SetTempAsFull";   	// Manual set temporary as full stop button
const string UN_FACEPLATE_BUTTON_RESET_TEMPORARY_AS_FULL_STOP = "ResetTempAsFull"; 	// Manual reset temporary as full stop button
const string UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION = "BlockAlarm";					// Manual block alarm action button
const string UN_FACEPLATE_BUTTON_DEBLOCK_ALARM_ACTION = "DeblockAlarm";				// Manual deblock alarm action button
const string UN_FACEPLATE_BUTTON_MANUAL_AUTO_TO_DEPENDENT_OBJECT = "AutoToDep"; 	// Manual auto to dependent object request button
const string UN_FACEPLATE_BUTTON_ACK_ALARM = "AckAlarm";		// Acknowledge alarm button
const string UN_FACEPLATE_BUTTON_SELECT = "Select";				// Select button
const string UN_FACEPLATE_BUTTON_SET_VALUE = "SetValue";							// Set value button
const string UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS = "AlarmLimits";					// Set alarm limits button
const string UN_FACEPLATE_BUTTON_SET_STATUS_LIMITS = "StatusLimits";				// Set status limits button
const string UN_GENERAL_CONFIGURATION = "GeneralConfig";							// Configure button, main synoptic
const string UN_FACEPLATE_BUTTON_OPTION_MODES = "OptionMode";	// PCO option modes button
const string UN_FACEPLATE_BUTTON_NEXT = "Next";					// PCO button "next"
const string UN_FACEPLATE_BUTTON_BACK = "Back";					// PCO button "back"
const string UN_FACEPLATE_BUTTON_SETPOINT = "Setpoint";			// PID setpoint button
const string UN_FACEPLATE_BUTTON_SETOUTPUT = "Output";			// PID set output button
const string UN_FACEPLATE_BUTTON_PID_PARAMETERS = "PIDParam";	// PID parameters button
const string UN_FACEPLATE_BUTTON_SET_PID_LIMITS = "PIDLimits";	// PID set limits button

const string UN_GENERIC_USER_ACCESS = "GenericUserAccess";		// Generic button user access
const string UN_USER_MONITOR = "AccessMonitor";				// User access
const string UN_USER_OPERATOR = "AccessOperator";				// User access
const string UN_USER_EXPERT = "AccessExpert";
const string UN_USER_ADMIN = "AccessAdmin";

const string UN_FACEPLATE_MASKEVENT = "Mask Event";

//--------------------------------------------------------------------------------------------------------------------------------

//------------
// FACEPLATES
//------------

const unsigned UN_FACEPLATE_PULSE_DELAY = 2;					// Pulse length in seconds
const unsigned UN_FACEPLATE_CLOSE_DELAY = 3;					// Close delay in seconds (better if greather than pulse length)
const unsigned UN_FACEPLATE_TRENDS_MAX = 8;						// Max number of curves in the trend tab of the faceplate
const string UN_FACEPLATE_TRENDS_DELIMITER = ";";
const string UN_FACEPLATE_DEFAULT_EXP_FORMAT = "%2.3e";
const string UN_FACEPLATE_EXP = "EXP";
const string UN_FACEPLATE_DEFAULT_FORMAT = "%3.2f";
const string UN_TREND_DEFAULT_FORMAT_FIXED_DISPLAY_EXTENTION = ".3f"; // default format extention used in the plot in case of fixed digits

const string UN_DISPLAY_VALUE_DIGIT_FLOAT_TYPE = "f";
const string UN_DISPLAY_VALUE_DIGIT_FLOAT_FORMAT = ".20";
const string UN_DISPLAY_VALUE_DIGIT_TYPE = "D";
const string UN_DISPLAY_VALUE_DIGIT_TOO_BIG_VALUE = "****";
const int UN_DISPLAY_VALUE_DIGIT_MAX_DIGIT = 101; // max 100 digits, theoretical max 1024 but bug: crash of WCCOAui

//--------------------------------------------------------------------------------------------------------------------------------

//---------
// WIDGETS
//---------

const string UN_WIDGET_TEXT_CONTROL_FORCED = "F";
const string UN_WIDGET_TEXT_CONTROL_LOCAL = "L";
const string UN_WIDGET_TEXT_CONTROL_MANUAL = "M";
const string UN_WIDGET_TEXT_CONTROL_REGULATION = "R";
const string UN_WIDGET_TEXT_CONTROL_TRACKING = "T";
const string UN_WIDGET_TEXT_ALARM_MASKED = "M";
const string UN_WIDGET_TEXT_ALARM_BLOCKED = "B";
const string UN_WIDGET_TEXT_ALARM_DOES_NOT_EXIST = "";
const string UN_WIDGET_TEXT_ALARM_STARTIST = "I";
const string UN_WIDGET_TEXT_ALARM_STOPIST = "S";
const string UN_WIDGET_TEXT_ALARM_POSAL = "P";
const string UN_WIDGET_TEXT_ALARM_STARTAL = "I";
const string UN_WIDGET_TEXT_ALARM_TSTOPAL = "S";
const string UN_WIDGET_TEXT_ALARM_FUSTOPAL = "F";
const string UN_WIDGET_TEXT_WARNING_DEFAULT = "W";
const string UN_WIDGET_TEXT_WARNING_SIMU = "S";
const string UN_WIDGET_TEXT_WARNING_ERROR = "E";
const string UN_WIDGET_TEXT_BLOCK_TFUSTOP = "T";
const string UN_WIDGET_TEXT_INVALID = "N";
const string UN_WIDGET_TEXT_OLD_DATA = "O";
const string UN_WIDGET_TEXT_WARNING_BLOCKED = "B";                                                                       // Dependent Alarm blocked warning               

//--------------------------------------------------------------------------------------------------------------------------------

//---------
// right click menu
//---------

const string cste_select = "Select";
const string cste_maskPVSS = "Mask PVSS Alarm";
const string cste_BlockPLC = "Block PLC Alarm";

const int UN_POPUPMENU_SELECT_ID       = 1;
const int UN_POPUPMENU_ACK_ID          = 2;
const int UN_POPUPMENU_MASK_POSST_ID   = 3;
const int UN_POPUPMENU_MASK_POSAL_ID   = 4;
const int UN_POPUPMENU_BLOCKPLC_ID     = 5;
const int UN_POPUPMENU_FACEPLATE_ID    = 6;
const int UN_POPUPMENU_DIAGNOSTIC_ID   = 7;

const int UN_POPUPMENU_SMS_ID          = 9;
const int UN_POPUPMENU_DEVICECONFIG_ID = 10;
const int UN_POPUPMENU_UNMASKEVENT_ID  = 11;
const int UN_POPUPMENU_MASKEVENT_ID    = 12;
const int UN_POPUPMENU_COMMENTS_ID     = 13;
const int UN_POPUPMENU_SYNOPTIC_ID     = 15;
const int UN_POPUPMENU_DEVICELINK_ID   = 20;
const int UN_POPUPMENU_SYNOPTICLOC_ID  = 99; //IS-1835 Temporary fix to avoid ID overlap with CPC
const int UN_POPUPMENU_TREND_REOPEN    = 249;
const int UN_POPUPMENU_TREND_CSTE      = 250;
const int UN_POPUPMENU_INFO_ID         = 300;
const int UN_POPUPMENU_MIN             = 400;



const string UN_POPUPMENU_ACK_LABEL          = "Ack. Alarm";
const string UN_POPUPMENU_BLOCKPLC_LABEL     = "Block PLC Alarm";
const string UN_POPUPMENU_FACEPLATE_LABEL    = "Faceplate";
const string UN_POPUPMENU_DIAGNOSTIC_LABEL   = "Diagnostic";
const string UN_POPUPMENU_INFO_LABEL         = "Info";
const string UN_POPUPMENU_SMS_LABEL          = "Mail-SMS";
const string UN_POPUPMENU_DEVICECONFIG_LABEL = "Device Configuration";
const string UN_POPUPMENU_UNMASKEVENT_LABEL  = "Unmask Event";
const string UN_POPUPMENU_MASKEVENT_LABEL    = "Mask Event";
const string UN_POPUPMENU_COMMENTS_LABEL     = "Comments";
const string UN_POPUPMENU_SYNOPTIC_LABEL     = "Synoptic";
const string UN_POPUPMENU_SYNOPTICLOC_LABEL  = "Locate on panel(s)";

const string UN_POPUPMENU_SELECT_TEXT        = "Select";
const string UN_POPUPMENU_ACK_TEXT           = "Acknowledge";
const string UN_POPUPMENU_MASK_POSST_TEXT    = "MaskPosSt";
const string UN_POPUPMENU_MASK_POSAL_TEXT    = "MaskPosAl";
const string UN_POPUPMENU_BLOCK_PLC_TEXT     = "BlockPlc";
const string UN_POPUPMENU_FACEPLATE_TEXT     = "Faceplate";
const string UN_POPUPMENU_TREND_TEXT         = "Trend";
const string UN_POPUPMENU_SMS_TEXT 			 = "SMS";

const int UN_NUM_AESVIEW = 1000;

//--------------------------------------------------------------------------------------------------------------------------------

//-------------
// FRONT_END CONFIGS
//-------------

// Siemens S7 PLC 
const string S7_PLC_DPTYPE = "S7_PLC";

const string UN_FRONTEND_DPTYPE = "_UnFrontEnd";
const unsigned UN_FRONTEND_FUNCTION_LENGTH = 4;
const unsigned UN_FRONTEND_FUNCTION_CHECK_DELETE = 1;
const unsigned UN_FRONTEND_FUNCTION_CHECK_FRONTEND = 2;
const unsigned UN_FRONTEND_FUNCTION_DELETE = 3;
const unsigned UN_FRONTEND_FUNCTION_SET_FRONTEND = 4;

//--------------------------------------------------------------------------------------------------------------------------------

//-------------
// DPE CONFIGS
//-------------

const int APPLICATION_PRIORITY_NOT_CONNECTED = 1;
const int APPLICATION_PRIORITY_MASKED = -1;
const int APPLICATION_PRIORITY_LOWER = -2;
const string APPLICATION_SYSTEM_NOT_CONNECTED = "unAlarm_DigitalBad";
const string APPLICATION_SYSTEM_OK = "unAlarm_Ok";
const int APPLICATION_PRIORITY_SCRIPT_NOT_RUNNING = 100;
const int UNICOS_SCRIPTS_MANAGER_NUMBER = 2;

const string c_applicationDPE = "_unApplication.graphicalFrameSelectObject";
const string UN_OBJECT_EXTENSION = "_unicosInfo";
const string UN_OBJECT_DPTYPE = "_UnObjects";

const string UN_DEFAULT_OBJECTLIST_DPE = "ProcessInput.StsReg0*";
const string UN_DEFAULT_DPFILTER_DPE = "statusInformation.selectedManager";
const string UN_DEFAULT_PREFIX = "un";
const string UN_DEFAULT_EVENTLIST_DPE = "ProcessInput.evStsReg0*";

const unsigned UN_AIAO_ALARM_5_RANGES = 5;						// Number of alert ranges case 5 ranges
const unsigned UN_AIAO_ALARM_5_RANGE_HH = 5;					// Number of the HH range case 5 ranges
const unsigned UN_AIAO_ALARM_5_RANGE_H = 4;						// Number of the H range case 5 ranges
const unsigned UN_AIAO_ALARM_5_RANGE_OK = 3;					// Number of the Ok range case 5 ranges
const unsigned UN_AIAO_ALARM_5_RANGE_L = 2;						// Number of the L range case 5 ranges
const unsigned UN_AIAO_ALARM_5_RANGE_LL = 1;					// Number of the LL range case 5 ranges

const unsigned UN_AIAO_ALARM_4_RANGES = 4;						// Number of alert ranges case 4 ranges
const unsigned UN_AIAO_ALARM_4_HH_H_L_RANGE_HH = 4;				// Number of the HH range case 4 ranges: HH, H, L
const unsigned UN_AIAO_ALARM_4_HH_H_L_RANGE_H = 3;				// Number of the H range case 4 ranges: HH, H, L
const unsigned UN_AIAO_ALARM_4_HH_H_L_RANGE_OK = 2;				// Number of the Ok range case 4 ranges: HH, H, L
const unsigned UN_AIAO_ALARM_4_HH_H_L_RANGE_L = 1;				// Number of the L range case 4 ranges: HH, H, L
const unsigned UN_AIAO_ALARM_4_H_L_LL_RANGE_H = 4;				// Number of the H range case 4 ranges: H, L, LL
const unsigned UN_AIAO_ALARM_4_H_L_LL_RANGE_OK = 3;				// Number of the Ok range case 4 ranges: H, L, LL
const unsigned UN_AIAO_ALARM_4_H_L_LL_RANGE_L = 2;				// Number of the L range case 4 ranges: H, L, LL
const unsigned UN_AIAO_ALARM_4_H_L_LL_RANGE_LL = 1;				// Number of the LL range case 4 ranges: H, L, LL

const unsigned UN_AIAO_ALARM_3_RANGES = 3;						// Number of alert ranges case 3 ranges
const unsigned UN_AIAO_ALARM_3_H_L_RANGE_H = 3;					// Number of the H range case 3 ranges: H, L
const unsigned UN_AIAO_ALARM_3_H_L_RANGE_OK = 2;				// Number of the Ok range case 3 ranges: H, L
const unsigned UN_AIAO_ALARM_3_H_L_RANGE_L = 1;					// Number of the L range case 3 ranges: H, L
const unsigned UN_AIAO_ALARM_3_HH_H_RANGE_HH = 3;				// Number of the HH range case 3 ranges: HH, H
const unsigned UN_AIAO_ALARM_3_HH_H_RANGE_H = 2;				// Number of the H range case 3 ranges: HH, H
const unsigned UN_AIAO_ALARM_3_HH_H_RANGE_OK = 1;				// Number of the OK range case 3 ranges: HH, H
const unsigned UN_AIAO_ALARM_3_LL_L_RANGE_LL = 1;				// Number of the LL range case 3 ranges: LL, L
const unsigned UN_AIAO_ALARM_3_LL_L_RANGE_L = 2;				// Number of the L range case 3 ranges: LL, L
const unsigned UN_AIAO_ALARM_3_LL_L_RANGE_OK = 3;				// Number of the OK range case 3 ranges: LL, L

const unsigned UN_AIAO_ALARM_2_RANGES = 2;						// Number of alert ranges case 2 ranges
const unsigned UN_AIAO_ALARM_2_H_RANGE_H = 2;					// Number of the H range case 2 ranges: H
const unsigned UN_AIAO_ALARM_2_H_RANGE_OK = 1;					// Number of the Ok range case 2 ranges: H, L
const unsigned UN_AIAO_ALARM_2_L_RANGE_L = 1;					// Number of the L range case 2 ranges: H
const unsigned UN_AIAO_ALARM_2_L_RANGE_OK = 2;					// Number of the Ok range case 2 ranges: H, L

const unsigned UN_AIAO_ALARM_4_H_L_LL = 8;						// case 4 ranges and H, L, LL
const unsigned UN_AIAO_ALARM_4_HH_H_L = 7;						// case 4 ranges and HH, H, L
const unsigned UN_AIAO_ALARM_5_HH_H_L_LL = 6;					// case 5 ranges and HH, H, L, LL
const unsigned UN_AIAO_ALARM_3_H_L = 5;							// case 3 ranges and H, L
const unsigned UN_AIAO_ALARM_3_HH_H = 4;						// case 3 ranges and HH, H
const unsigned UN_AIAO_ALARM_3_LL_L = 3;						// case 3 ranges and LL, L
const unsigned UN_AIAO_ALARM_2_H = 2;							// case 3 ranges and H
const unsigned UN_AIAO_ALARM_2_L = 1;							// case 3 ranges and L
const unsigned UN_AIAO_ALARM_NO_ALARM = 0;						// case no alarm

const string UN_AIAO_ALARM_HH = "HH";						// alarmLetter in AnalogInput/AnalogOutput widget
const string UN_AIAO_ALARM_H = "H";							// alarmLetter in AnalogInput/AnalogOutput widget
const string UN_AIAO_ALARM_L = "L";							// alarmLetter in AnalogInput/AnalogOutput widget
const string UN_AIAO_ALARM_LL = "LL";						// alarmLetter in AnalogInput/AnalogOutput widget

const float UN_RANGE_MIN_DEFAULT = 0;
const float UN_RANGE_MAX_DEFAULT = 100;
const float UN_PERCENT_RANGE_DEFAULT = 0.1;

const float UN_PIDCON_MAX_RANGE_DEFAULT = 100;
const float UN_PIDCON_MIN_RANGE_DEFAULT = 0;

//--------------------------------------------------------------------------------------------------------------------------------

//--------
// PANELS
//--------

const int UN_BASE_PANEL_LEFT = 0;								// Base panel
const int UN_BASE_PANEL_TOP = 91;
const int UN_BASE_PANEL_WIDTH = 1270;
const int UN_BASE_PANEL_HEIGHT = 835;
const int UN_BUTTON_WIDTH = 90;									// Button
const int UN_BUTTON_SPACE = 2;									// Space between two buttons

const int c_unGraphicalFrame_unicosChildPanel_Xpos = 0;
const int c_unGraphicalFrame_unicosChildPanel_Ypos = 90;

const int c_unGraphicalFrame_unicosHMIFirstModule_Xpos = 0;
const int c_unGraphicalFrame_unicosHMIFirstModule_Ypos = 0;
const int c_unGraphicalFrame_unicosHMISecondModule_Xpos = 1280;
const int c_unGraphicalFrame_unicosHMISecondModule_Ypos = 0;
const int c_unGraphicalFrame_unicosHMIThirdModule_Xpos = -1280;
const int c_unGraphicalFrame_unicosHMIThirdModule_Ypos = 0;

const int c_unGraphicalFrame_unicosHMIFirstModule_CCC_Xpos = -1280;
const int c_unGraphicalFrame_unicosHMISecondModule_CCC_Xpos = 0;
const int c_unGraphicalFrame_unicosHMIThirdModule_CCC_Xpos = 1280;

//const string c_unicosHMI_unicosGIF_default = "logo/CERNLogo_UNICOS.png";
const string c_unicosHMI_unicosGIF_default = "logo/newLogo.png";
const string c_unicosHMI_corporateStyle_backgroundColor = "unCorporateColor";
const string c_unicosHMI_corporateStyle_foregroundColor = "white";
const string GRAPHICALFRAME_DPTYPE = "_UnGraphicalFrame";
const string GRAPHICALFRAME_DPPREFIX = "_UnGraphicalFrame_";

const string UN_MENU_ITEM = "item";
const string UN_MENU_SEP = "sep";
const string UN_MENU_POPUP = "pop-up";
global mapping g_m_dsChildOn, g_m_dsChildModule;

global dyn_string g_unGraphicalFrame_lastPanelDpOpened, g_unGraphicalFrame_openedModule;
global dyn_string g_unGraphicalFrame_childOpenedPanel; // variable used to keep the panel that prevent unicosHMI to be closed if they are opened

global mapping gOpenedPanel, gPopUpOpenedPanel;

global dyn_string gPanelNameOnModule;

// global to stop/refresh screen
global dyn_bool g_unicosHMI_PanelOff;

const string UN_UNICOSHMI_SYNOPTIC_MODULENAME = ";unicosGFPanel";

const int c_graphicalFrame_unPanelHierarchy_max_panel = 20;
const int c_unPanelConfiguration_maxPanelNavigation = 10; // maximum of panels for navigation
const string c_unPanelConfiguration = "vision/unPanel/unPanelHierarchyConfiguration"; // panel name for configuration

const string UN_PANEL_CUSTOM_DP_SELECTOR = "vision/unicosObject/configuration/unTrendDpFilterGeneral.pnl";
const string UN_PANEL_MANAGE_CHILDREN = "fwTrending/fwTrendingManageChildren";
const string UN_PANEL_DEVICE_OVERVIEW = "vision/unicosObject/General/Overview.pnl";
const string UN_PANEL_TREE_DEVICE_OVERVIEW = "vision/unicosObject/General/unTreeDeviceOverview.pnl";

const string UN_DISTRIBUTED_CONTROL_OPERATION_PANEL = "vision/distributedControl/unDistributedControl_operation.pnl"; 
const string UN_DISTRIBUTED_CONTROL_OPERATION_PANEL_NAME = "Distributed System (local)";

//--------------------------------------------------------------------------------------------------------------------------------

//------------
// windowtree trendtree constants
//------------
// constant declaration
const string c_template_panel = "vision/template_panel.pnl";
const int c_min_manager_number = 1, c_max_manager_number = 128;
const string c_panel_navigation_delimiter = "|";

const string UN_PANEL_DPNAME_DELIMITER = "-";
const string UN_PANEL_FILENAME_EXTENTION = ".pnl";
const string UN_PANEL_FILENAME_PATH_DELIMITER = "/";
const string UN_PANEL_DPTYPE = "_UnPanel";

const string UN_TRENDTREE_PREDEFINED_TRENDS = "Predefined_Trends";
const string UN_TRENDTREE_USERDEFINED_TRENDS = "User_Defined_Trends";

// definition for get icon function.
const string UN_WINDOWTREE_ROOT_FOLDER = "WindowTree";
const string UN_WINDOWTREE_PANEL = "_UnPanel";
const string UN_TRENDTREE_ROOT_FOLDER = "TrendTree";
const string UN_TRENDTREE_PLOT = "FwTrendingPlot";
const string UN_TRENDTREE_PAGE = "FwTrendingPage";

const string UN_TRENDTREE_PLOT_CONFIGURATION_PANEL = "vision/trendTree/unTrendingPlotConfPanel";
const string UN_TRENDTREE_PAGE_CONFIGURATION_PANEL = "fwTrending/fwTrendingPlotsPage";
const string UN_TRENDTREE_NODELIST_CONFIGURATION_PANEL = "vision/trendTree/unTrendTreeNodeList";
const string UN_TREND_PLOT_CONF_PANEL = "vision/trendTree/unTrendingPlotConfPanel.pnl";

const string UN_TRENDTREE_PLOT_OPERATION_PANEL = "fwTrending/fwTrendingPlot.pnl";
const string UN_TRENDTREE_PAGE_OPERATION_PANEL = "fwTrending/fwTrendingPage.pnl";

const int UN_TRENDTREE_addPlot = 21; //correspond to UN_TREE_addDevice
const int UN_TRENDTREE_addPage = 2; // correspond to unTreeWidget_popupMenu_add
const int UN_TRENDTREE_dblClick = 30;
const int UN_TRENDTREE_openEditor = 31; //open trend Tree Editor panel
const int UN_TRENDTREE_openInPreview1 = 32;
const int UN_TRENDTREE_openInPreview2 = 33;
const int UN_TRENDTREE_openInPreview3 = 34;
const int UN_TRENDTREE_openInPreview4 = 35;

const int UN_TRENDTREE_newPlot = 27; // correspond UN_TREE_newDevice
const int UN_TRENDTREE_newPage = 28; // UN_TREE_new


//--------------------------------------------------------------------------------------------------------------------------------

//-------------
// OBJECT LIST
//-------------

// Herve: 03/03/2003: modify allowDisplay, use patternMatch instead of strpos
const string UN_OBJECT_LIST_DELIMITER = ";";		// Delimiter in g_tableHistory
const string UN_OBJECT_LIST_BITS_DELIMITER = "|";		// Delimiter in g_tableHistory for bit names
const string UN_OBJECT_LIST_CONFIGURATION_DELIMITER = ";";	// Delimiter in configuration DPEs
const string UN_OBJECT_LIST_FILTER_DELIMITER = "~";	// Delimiter in filter string
const string UN_OBJECT_LIST_FILTER_COMBO_DELIMITER = ";";	// Delimiter in filter combobox
const string UN_OBJECT_LIST_WAIT_PANEL_NAME = "<<Object List>>";
const string UN_OBJECT_LIST_LIST_NAME = "List...";
const string UN_OBJECT_LIST_FILTER_ALL = "*";

const int UN_OBJECT_LIST_FIELD = 11;				// 11 fields for history
const int UN_OBJECT_LIST_FIELD_OBJECT = 1;
const int UN_OBJECT_LIST_FIELD_ALIAS = 2;
const int UN_OBJECT_LIST_FIELD_DESCRIPTION = 3;
const int UN_OBJECT_LIST_FIELD_DOMAIN = 4;
const int UN_OBJECT_LIST_FIELD_NATURE = 5;
const int UN_OBJECT_LIST_FIELD_BITNAME = 6;		// List of bit=1 in StsReg0*, UN_OBJECT_LIST_BITS_DELIMITER delimited
const int UN_OBJECT_LIST_FIELD_SYSTEM = 7;
const int UN_OBJECT_LIST_FIELD_INVALID = 8;
const int UN_OBJECT_LIST_FIELD_COLOR = 9;
const int UN_OBJECT_LIST_FIELD_VALUE = 10;
const int UN_OBJECT_LIST_FIELD_TIME = 11;

const int UN_OBJECT_LIST_FILTER_FIELD = 9;			// 9 fields for filter
const int UN_OBJECT_LIST_FILTER_FIELD_OBJECT = 1;
const int UN_OBJECT_LIST_FILTER_FIELD_ALIAS = 2;
const int UN_OBJECT_LIST_FILTER_FIELD_DESCRIPTION = 3;
const int UN_OBJECT_LIST_FILTER_FIELD_DOMAIN = 4;
const int UN_OBJECT_LIST_FILTER_FIELD_NATURE = 5;
const int UN_OBJECT_LIST_FILTER_FIELD_BITNAME_1 = 6;
const int UN_OBJECT_LIST_FILTER_FIELD_BITNAME_2 = 7;
const int UN_OBJECT_LIST_FILTER_FIELD_BITNAME_3 = 8;
const int UN_OBJECT_LIST_FILTER_FIELD_INVALID = 9;

const string UN_OBJECT_LIST_FILTER_DEFAULT = "*~*~*~*~*~~~~*";
const string UN_OBJECT_LIST_COLOR_DEFAULT = "#Normal#";

//--------------------------------------------------------------------------------------------------------------------------------

//-------------
// ALERT PANEL
//-------------

const string UN_ALERT_DESCRIPTION_DELIMITER = "~";			// Delimiter of description alert dpe
//const int UN_ALERTPANEL_PERIOD = 3600;			// In seconds, the query time period
const string UN_ALERTPANEL_FILTER_COMBO_DELIMITER = ";";	// Delimiter in filter combobox MUST NOT BE CHANGED
const string UN_ALERTPANEL_LIST_NAME = "List...";	// List menu
const string UN_ALERTPANEL_FILTER_ALL = "*";		// Filter all
const string UN_ALERTPANEL_FILTER_DELIMITER = "~";	// Delimiter in filter string
const string UN_ALERTPANEL_FILTER_DEFAULT = "0~100~1~*~*~*~*~*~*~*~*~*";

const int UN_ALERTPANEL_FILTER_FIELD_LENGTH = 12;
const int UN_ALERTPANEL_FILTER_FIELD_TIMERANGE = 1;
const int UN_ALERTPANEL_FILTER_FIELD_MAXLINES = 2;
const int UN_ALERTPANEL_FILTER_FIELD_TIMEREQUEST = 3;
const int UN_ALERTPANEL_FILTER_FIELD_ALIAS = 4;
const int UN_ALERTPANEL_FILTER_FIELD_DESCRIPTION = 5;
const int UN_ALERTPANEL_FILTER_FIELD_DOMAIN = 6;
const int UN_ALERTPANEL_FILTER_FIELD_NATURE = 7;
const int UN_ALERTPANEL_FILTER_FIELD_NAME = 8;
const int UN_ALERTPANEL_FILTER_FIELD_ALERTTEXT = 9;
const int UN_ALERTPANEL_FILTER_FIELD_ALERTSTATE = 10;
const int UN_ALERTPANEL_FILTER_FIELD_OBJECT = 11;
const int UN_ALERTPANEL_FILTER_FIELD_SYSTEMS = 12;
const int UN_DELAY_REFRESH_ALERT_TABLE = 2;

const string UN_ALERTPANEL_TITLE = "PROCESS Alarm List";
const string UN_ALERTPANEL_SYSTEM_TITLE = "SYSTEM Alarm List";
const int UN_ALERTPANEL_CONFIG = 1;			// Unicos objects alert panel
const int UN_ALERTPANEL_SYSTEM_INTEGRITY = 2; // Unicos system integrity alert panel
const string UN_ALERTPANEL_SYSTEM_INTEGRITY_OBJECT = "SystemAlarm"; // Unicos system integrity alert panel

const int UN_ALERT_TIMEOUT = 100;
const int UN_ALERT_LINE = 33;

const string UN_ALERT_AES_ROW = "_AESProperties_unAlertRow";
const string UN_ALERT_AES_SCREEN = "_AESProperties_unAlertScreen";

//--------------------------------------------------------------------------------------------------------------------------------

//------------
// Import Configuration
//------------

const string UN_DELETE_COMMAND = "Delete";
const string UN_PLC_COMMAND = "PLCCONFIG";
const string UN_PLC_COMMAND_EXTENDED = "PLCCONFIG_extended";

const string UN_FESYSTEMALARM_COMMAND = "SystemAlarm";

const unsigned UN_CONFIG_PLC_LENGTH = 4;			// 4 parameters
const unsigned UN_CONFIG_PLC_TYPE = 1;				// PLC type = "PREMIUM","QUANTUM","S7-300","S7-400"
const unsigned UN_CONFIG_PLC_NAME = 2;				// PLC name
const unsigned UN_CONFIG_PLC_SUBAPPLICATION = 3;	// PLC subApplication
const unsigned UN_CONFIG_PLC_NUMBER = 4;			// PLC number

const int UN_CONFIG_FE_VERSION_ADDITIONAL_LENGTH = 1;
const int UN_CONFIG_FE_VERSION = 5;

const unsigned UN_CONFIG_PLC_ADDITIONAL_LENGTH = 5;
const unsigned UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM = 1;
const unsigned UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL = 2;
const unsigned UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG = 3;
const unsigned UN_CONFIG_PLC_ADDITIONAL_EVENT16 = 4;	// Event 16 or 32
const unsigned UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT = 5;

const unsigned UN_CONFIG_PLC_EXTENDED_LENGTH = 4;	// 4 parameters
const unsigned UN_CONFIG_PLC_EXTENDED_IP = 1;		// PLC IP address
const unsigned UN_CONFIG_PLC_EXTENDED_ADDRESS_SEND_IP = 2;	// Address in Plc for send IP
const unsigned UN_CONFIG_PLC_EXTENDED_ADDRESS_COUNTER = 3;	// Address in Plc for counter
const unsigned UN_CONFIG_PLC_EXTENDED_ADDRESS_CMD = 4;	// Address in Plc for the command interface

// CPC6
const unsigned UN_CONFIG_UNPLC_LENGTH_CPC6 = 14; 
const unsigned UN_CONFIG_UNPLC_CPC6_IP = 5;		// PLC IP address
const unsigned UN_CONFIG_UNPLC_CPC6_ADDRESS_SEND_IP = 6;	// Address in Plc for send IP
const unsigned UN_CONFIG_UNPLC_CPC6_ADDRESS_COUNTER = 7;	// Address in Plc for counter
const unsigned UN_CONFIG_UNPLC_CPC6_ADDRESS_CMD = 8;	// Address in Plc for the command interface
const unsigned UN_CONFIG_UNPLC_CPC6_PLC_BASELINE_ADDRESS = 9; // PLC baseline version
const unsigned UN_CONFIG_UNPLC_CPC6_PLC_APPLICATION_ADDRESS = 10; // PLC application version
const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_MAJOR_VERSION_ADDRESS = 11; // (TSPP address word) WORD, Addressing to read the major version of the Resource Package
const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_MINOR_VERSION_ADDRESS = 12; // (TSPP address word) WORD, Addressing to read the minor version of the Resource Package
const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_SMALL_VERSION_ADDRESS= 13; // (TSPP address word) WORD, Addressing to read the small version of the Resource Package
const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_VERSION = 14; // (String) : version of the Resource Package employed


const string UN_CONFIG_PLC_PORT = ":502";			// PLC port
const int UN_CONFIG_PLC_TIMEOUT = 1000;				// Timeout
const unsigned UN_CONFIG_PLC_FRAME_CODING = 1;		// RTU frame coding
const unsigned UN_CONFIG_PLC_FRAME_ENDIANITY = 2;	// Big endian
const bool UN_CONFIG_PLC_INVALID_BIT = false;		// Invalid bit
const bool UN_CONFIG_PLC_REDUNDANT = false;			// Redundant state
const dyn_string UN_CONFIG_PLC_REDUNDANT_HOSTNAMES = makeDynString();	// Redundant hostnames
const unsigned UN_CONFIG_PLC_REDUNDANT_ADDRESS = 0;		// Redundant PLC address
const unsigned UN_CONFIG_PLC_CONNSTATE = 0;			// Connstate
const unsigned UN_CONFIG_PLC_ERROR = 0;				// Error
const unsigned UN_CONFIG_PLC_SENTFRAMES = 0;		// Sent frames
const unsigned UN_CONFIG_PLC_RCVFRAMES = 0;			// Received frames
const unsigned UN_CONFIG_PLC_REJFRAMES = 0;			// Rejected frames

const unsigned UN_CONFIG_ALLCOMMON_LENGTH = 18;				// UN_CONFIG_ALLCOMMON_LENGTH = UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH

const unsigned UN_CONFIG_OBJECTGENERAL_LENGTH = 10;
const unsigned UN_CONFIG_OBJECTGENERAL_DRIVER = 1;			// Driver number
const unsigned UN_CONFIG_OBJECTGENERAL_PLC_TYPE = 2;		// PLC type
const unsigned UN_CONFIG_OBJECTGENERAL_PLC_NUMBER = 3;		// PLC number
const unsigned UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL = 4;	// Bool archive name
const unsigned UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG = 5;	// Analog archive name
const unsigned UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT = 6;		// Event archive name
const unsigned UN_CONFIG_OBJECTGENERAL_OBJECT = 7;			// Object type
const unsigned UN_CONFIG_OBJECTGENERAL_DPNAME = 8;			// Datapoint name for current object
const unsigned UN_CONFIG_OBJECTGENERAL_EVENT16 = 9;			// event 16 or 32 bits
const unsigned UN_CONFIG_OBJECTGENERAL_PLCNAME = 10;		// PLC name

const unsigned UN_CONFIG_COMMON_LENGTH = 8;
const unsigned UN_CONFIG_COMMON_ALIAS = 1;					// Alias
const unsigned UN_CONFIG_COMMON_DESCRIPTION = 2;			// Description
const unsigned UN_CONFIG_COMMON_DIAGNOSTIC = 3;				// Diagnostic
const unsigned UN_CONFIG_COMMON_HTML = 4;					// Html
const unsigned UN_CONFIG_COMMON_DEFAULTPANEL = 5;			// Default panel
const unsigned UN_CONFIG_COMMON_DOMAIN = 6;					// Domain
const unsigned UN_CONFIG_COMMON_NATURE = 7;					// Nature
const unsigned UN_CONFIG_COMMON_WIDGET = 8;					// Widget

const string UN_CONFIG_GROUP_SEPARATOR = ",";			// separator for multi domain and multi nature

const unsigned UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH = 3; // 3 extra parameters for the devices: bool, analog and event archive
const unsigned UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL = 1;
const unsigned UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG = 2;
const unsigned UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT = 3;

const unsigned UN_CONFIG_ALARM_DESCRIPTION_LENGTH = 6;		
const unsigned UN_CONFIG_ALARM_DOMAIN = 1;		
const unsigned UN_CONFIG_ALARM_NATURE = 2;		
const unsigned UN_CONFIG_ALARM_ALIAS = 3;		
const unsigned UN_CONFIG_ALARM_DESCRIPTION = 4;		
const unsigned UN_CONFIG_ALARM_NAME = 5;		
const unsigned UN_CONFIG_ALARM_DEFAULTPANEL = 6;		

const int UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME = 0;
const int UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME = 0;
const bool UN_CONFIG_DEFAULT_ADDRESS_ACTIVE = true;

const bool UN_CONFIG_DEFAULT_RANGE_NEGATE_RANGE = false;
const bool UN_CONFIG_DEFAULT_RANGE_IGNORE_OUTSIDE = false;
const bool UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MIN = true;
const bool UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MAX = true;

// WARNING const dyn_string doesn't work in PVSS 
//const dyn_string UN_CONFIG_DEFAULT_ALERT_5RANGES_TEXTS = makeDynString("LL","L","Ok","H","HH");
//const dyn_string UN_CONFIG_DEFAULT_ALERT_5RANGES_ALERT_CLASSES = makeDynString("_unAnalogLL.", "_unAnalogL.","","_unAnalogH.","_unAnalogHH.");
const string UN_CONFIG_DEFAULT_ALERT_DIGITAL_ALERT_CLASS = "_unDigitalBad.";
const string UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_OK = "Ok";
const string UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_BAD = "Bad";
const string UN_CONFIG_DEFAULT_ALERT_PANEL = "";
const dyn_string UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS = makeDynString();
const string UN_CONFIG_DEFAULT_ALERT_HELP = "";

const int UN_CONFIG_DEFAULT_SMOOTHING_TIME_FILTER = 0;	// No smoothing time filter

const string UN_CONFIG_ARCHIVE_ACTIVE = "Y";
const string UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_OR_TIME = "O";
const string UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_AND_TIME = "A";

const string UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR = ",";
const int UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_LENGTH = 3;
const int UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TYPE = 1;
const string UN_CONFIG_ARCHIVE_ACTIVE_VALUE_ABSOLUTE = "VA";
const string UN_CONFIG_ARCHIVE_ACTIVE_VALUE_RELATIVE = "VR";
const int UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_VALUE = 2;
const int UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TIME = 3;
const string UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_AND_TIME = "A";
const string UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_OR_TIME = "O";
const string UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_NO_TIME = "N";

const bool UN_CONFIG_LOWLEVEL_STSREG = true;
const bool UN_CONFIG_LOWLEVEL_EVSTSREG = true;
const bool UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_INPUT = true;
const bool UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_OUTPUT = false;
const bool UN_CONFIG_LOWLEVEL_MANREG = false;

const bool UN_CONFIG_GROUP_PRIVATE = false;
const bool UN_CONFIG_GROUP_MAIN = true;

const float UN_FACEPLATE_TREND_DIGITAL_DEFAULT_MIN = -0.5;
const float UN_FACEPLATE_TREND_DIGITAL_DEFAULT_MAX = 1.5;

const unsigned UN_CONFIG_CHECKSYSTEM_ALARM_LENGTH = 3;	// 3 parameters
const unsigned UN_CONFIG_CHECKSYSTEM_ALARM_FRONTEND = 1;	// front-end type
const unsigned UN_CONFIG_CHECKSYSTEM_ALARM_PLC_NAME = 2;	// Plc name
const unsigned UN_CONFIG_CHECKSYSTEM_ALARM_ADDRESS = 3;	// Address in Plc

const unsigned UN_CONFIG_CHECKFESYSTEM_ALARM_LENGTH = 5;	// 5 parameters
const unsigned UN_CONFIG_CHECKFESYSTEM_ALARM_FRONTEND = 1;	// front-end type
const unsigned UN_CONFIG_CHECKFESYSTEM_ALARM_NAME = 2;	// name
const unsigned UN_CONFIG_CHECKFESYSTEM_ALARM_ADDRESS = 3;	// Address in front-end
const unsigned UN_CONFIG_CHECKFESYSTEM_ALARM_OKALARM = 4;	// true=alarm if value>10, false (default) = alarm if value<=10
const unsigned UN_CONFIG_CHECKFESYSTEM_ALARM_DESCRIPTION = 5;	// description

const unsigned UN_CONFIG_SETSYSTEM_ALARM_LENGTH = 10;	// 10 parameters
const unsigned UN_CONFIG_SETFESYSTEM_ALARM_LENGTH = 12;	// 12 parameters 

const unsigned UN_CONFIG_SETSYSTEM_ALARM_PLC_NAME = 1;	// Plc name
const unsigned UN_CONFIG_SETSYSTEM_ALARM_ADDRESS = 2;	// Address in Plc

const unsigned UN_CONFIG_SETFESYSTEM_ALARM_NAME = 1;	// name
const unsigned UN_CONFIG_SETFESYSTEM_ALARM_ADDRESS = 2;	// Address in front-end
const unsigned UN_CONFIG_SETFESYSTEM_ALARM_OKALARM = 3;	// true=alarm if value>10, false (default) = alarm if value<=10
const unsigned UN_CONFIG_SETFESYSTEM_ALARM_DESCRIPTION = 4;	// description

const string UN_CONFIG_FRONTEND_GETARCHIVEDPFUNCTION = "_getFrontEndArchiveDp";
const string UN_CONFIG_FRONTEND_GETMANAGERCONFIG = "_getFrontEndManagerConfig";

const string UN_CONFIG_SYSTEM_ALARM_CHECKFUNCTION = "_checkSystemAlarm"; // function suffix called when checking SystemAlarm
const string UN_CONFIG_FESYSTEM_ALARM_CHECKFUNCTION = "_checkFESystemAlarm"; // function suffix called when checking FESystemAlarm
const string UN_CONFIG_SYSTEM_ALARM_SETFUNCTION = "_setSystemAlarm"; // function suffix called when creating SystemAlarm
const string UN_CONFIG_FESYSTEM_ALARM_SETFUNCTION = "_setFESystemAlarm"; // function suffix called when creating FESystemAlarm

const string UN_CONFIG_DEVICE_CHECKFUNCTIONSUFFIX = "_checkConfig"; // function suffix called when checking the device
const string UN_CONFIG_DEVICE_SETFUNCTIONSUFFIX = "_setConfig"; // function suffix called when setting the device 

const int UN_CONFIG_SYSTEMALARM_DEVICENAME = 3;
const int UN_CONFIG_SYSTEMALARM_PLCNAME = 4;
const int UN_CONFIG_SYSTEMALARM_TYPE = 5;
const int UN_CONFIG_SYSTEMALARM_PLCNUMBER = 6;
const int UN_CONFIG_SYSTEMALARM_DRIVER = 7;
const int UN_CONFIG_SYSTEMALARM_EVENT16 = 8;
const int UN_CONFIG_SYSTEMALARM_ARCHIVE = 9;

const int UN_CONFIG_ADDITIONAL_SYSTEMALARM_LENGTH = 7;
const unsigned UN_CONFIG_ADDITIONAL_SYSTEMALARM_DEVICENAME = 1;
const unsigned UN_CONFIG_ADDITIONAL_SYSTEMALARM_PLCNAME = 2;
const unsigned UN_CONFIG_ADDITIONAL_SYSTEMALARM_TYPE = 3;
const unsigned UN_CONFIG_ADDITIONAL_SYSTEMALARM_PLCNUMBER = 4;
const unsigned UN_CONFIG_ADDITIONAL_SYSTEMALARM_DRIVER = 5;
const unsigned UN_CONFIG_ADDITIONAL_SYSTEMALARM_ARCHIVE = 7;

const int UN_CONFIG_FESYSTEMALARM_DEVICENAME = 5;
const int UN_CONFIG_FESYSTEMALARM_PLCNAME = 6;
const int UN_CONFIG_FESYSTEMALARM_TYPE = 7;
const int UN_CONFIG_FESYSTEMALARM_PLCNUMBER = 8;
const int UN_CONFIG_FESYSTEMALARM_DRIVER = 9;
const int UN_CONFIG_FESYSTEMALARM_EVENT16 = 10;
const int UN_CONFIG_FESYSTEMALARM_ARCHIVE = 11;

const int UN_CONFIG_ADDITIONAL_FESYSTEMALARM_LENGTH = 7;
const unsigned UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DEVICENAME = 1;
const unsigned UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNAME = 2;
const unsigned UN_CONFIG_ADDITIONAL_FESYSTEMALARM_TYPE = 3;
const unsigned UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNUMBER = 4;
const unsigned UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DRIVER = 5;
const unsigned UN_CONFIG_ADDITIONAL_FESYSTEMALARM_ARCHIVE = 7;

const string UN_CONFIG_PREMIUM = "PREMIUM";
const string UN_CONFIG_QUANTUM = "QUANTUM";
const string UN_CONFIG_UNITY = "UNITY";
const string UN_CONFIG_PROTOCOL_MODBUS = "MODBUS";

const int UN_CONFIG_DEADBAND_RELATIF = 2;
const int UN_CONFIG_DEADBAND_VALUE = 1;
const int UN_CONFIG_DEADBAND_NONE = 0;
const int UN_CONFIG_DEADBAND_OLD_NEW = 3;

const int UN_TRANSITION_PLC_MAX_POSITION = 15;

const int UN_PREMIUM_INPUT_NB_EVENT32 = 17;
const int UN_QUANTUM_INPUT_NB_EVENT32 = 17;
const int UN_ADDRESS_PARAMETER_FIELD_EVENT16=6;

// Siemens S7 PLC 
const string UN_CONFIG_S7_400 = "S7-400";
const string UN_CONFIG_S7_300 = "S7-300";
const string UN_CONFIG_PROTOCOL_S7 = "S7";

// constant for nameCheck
const int UN_CONFIG_NAMECHECK_MAXLEN = 50;

// new variables for version in import file
global mapping g_mImportFrontEndVersion, g_mExportFrontEndVersion;

//--------------------------------------------------------------------------------------------------------------------------------

//------------
// Export Configuration
//------------

const string UN_CONFIG_EXPORT_FUNCTIONSUFFIX = "_ExportConfig";
const string UN_CONFIG_EXPORT_UNDEF = "#VALUE!";
const string UN_CONFIG_EXPORT_NONE = "";
const string UN_CONFIG_EXPORT_ARCHIVE_1 = "";
const string UN_CONFIG_EXPORT_ARCHIVE_2 = "";
const string UN_CONFIG_EXPORT_ARCHIVE_3 = "";
const int UN_CONFIG_EXPORT_LENGTH = 3;
const int UN_CONFIG_EXPORT_PLCNAME = 1;
const int UN_CONFIG_EXPORT_APPLICATION = 2;
const int UN_CONFIG_EXPORT_PLCTYPE = 3;


//--------------------------------------------------------------------------------------------------------------------------------

//------------
// Constant for the add alarm in application
//------------

const string CMWServer_pattern = "CMWServer_";
const string CMWClient_pattern = "CMWClient_";
const string LHCLogging_pattern = "LHCLogging_";
const string LASER_pattern = "Laser_";	
const string IMPORT_PATTERN = "import_";

// global variables for the optimization of the unGenericDpFunctions_dpAliasToName
global mapping g_m_unicosGraphicalFrame_bLock; // to set/unset a lock when updating the list of aliases and dps per system
global mapping g_m_unicosGraphicalFrame_bUpdate; // to that there was a modification of into a system
global mapping g_m_unicosGraphicalFrame_dsAlias; // list of aliases per system
global mapping g_m_unicosGraphicalFrame_dsDp; // list of dps per system

//--------------------------------------------------------------------------------------------------------------------------------

//------------
// Constant for the mail-sms
//------------

const string UN_PROCESSALARM_EXTENSION = "_Mail.";
const string UN_PROCESSALARM_CATEGORY = "unProcessAlarm";
const string UN_PROCESSALARM_PATTERN = "*_Mail*";
const string UN_PROCESSALARM_DIGITAL_ALERT_CLASS = "_unDigitalBad_Mail.";
const string UN_PROCESSALARM_ANALOG_ALERT_CLASS_LL ="_unAnalogLL_Mail.";
const string UN_PROCESSALARM_ANALOG_ALERT_CLASS_L ="_unAnalogL_Mail.";
const string UN_PROCESSALARM_ANALOG_ALERT_CLASS_H ="_unAnalogH_Mail.";
const string UN_PROCESSALARM_ANALOG_ALERT_CLASS_HH ="_unAnalogHH_Mail.";
const int UN_ALERT_DELAY = 100;

// global variable for the unGenericDpFunctions_getDeviceDefinitionDPEDescription
global mapping g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPE; // list of DPEs
global mapping g_m_unGenericDpFunctions_getDeviceDefinitionDPEDescription_DPEdescription; // list of description

//--------------------------------------------------------------------------------------------------------------------------------

//------------
// Constant for the device overview panels
//------------

const string WIDGET_REFERENCE = "widgetDisplay_";
const string ALIAS_REFERENCE = "aliasDisplay_";
const int HEIGHT_DEFAULT = 70;
const int WIDGET_WIDTH_SPACING = 10;
const int WIDGET_HEIGHT_SPACING = 10;
const int DELTA_LOCKBMP_WIDTH = 13;
const int DELTA_LOCKBMP_HEIGHT = 7;						// Lock height / 2

const int DEVUN_BASE_PANEL_WIDTH = 963;
const int DEVUN_BASE_PANEL_HEIGHT = 745;
const int DEVUN_OFFSET_WIDTH = 260;
const int DEVUN_MIN_LEFT = 285;
const int DEVUN_MIN_TOP = 60;
const int DEVUN_OFFSET_HEIGHT = 38;
const int DEVUN_APPLYFILTER_COMMAND = 3;
const int DEVUN_ADD_COMMAND = 1;
const int DEVUN_DELETE_COMMAND = 2;

const int FILTER_MAX = 6;
const int FILTER_PVSS_SYSTEM = 0;
const int FILTER_FRONT_END = 1;
const int FILTER_APPLICATION = 2;
const int FILTER_DEVICETYPE = 3;
const int FILTER_SUBSYSTEM1 = 4;
const int FILTER_SUBSYSTEM2 = 5;
const int FILTER_ALIAS = 6;
const int FILTER_ALIAS_EXCLUDED = 7;
const string NO_FILTER_SELECTED = " ";
const string NO_FILTER_OPTION = "Don't filter";
const string ALL_FILTER_SELECTED = "*";
const string TREE_NAME = "treeDevice.tree";
const string TREEBAR_NAME = "treeDevice.treeBar";
const string NOT_DP = "NOT_DP";
const string DEVUN_DPTYPE = "_UnTreeDevOv";
const string DEVUN_DPPREFIX = "_UnTreeDevOv_";
const string DEVUN_TREEDISPLAY_CALLBACK = "unTreeDeviceOverview_displayCallBack";

const string DEVUN_TREEDISPLAY_UPDATE_SNAPSHOT = "UPDATE_SNAPSHOT";
const string DEVUN_TREEDISPLAY_WIDGET = "WIDGET";
const string DEVUN_TREEDISPLAY_SNAPSHOT = "SNAPSHOT";
const string DEVUN_TREEDISPLAY_NODELEFTCLICK = "NODELEFTCLICK";
const string DEVUN_TREEDISPLAY_PAGE_WIDGET = "PAGE_WIDGET";
const string DEVUN_TREEDISPLAY_UNKNOWN_NODE = "-1;-1|-1;-1";
const string DEVUN_TREEDISPLAY_EXPAND = "EXPAND";

const int TREE_DEVICE_DPNAME = 1;
const int TREE_DEVICE_ALIAS = 2;
const int TREE_DEVICE_FE = 3;
const int TREE_DEVICE_FEDPNAME = 4;
const int TREE_DEVICE_FEDPTYPE = 5;
const int TREE_DEVICE_FEAPPLICATION = 6;
const int TREE_DEVICE_TYPE = 7;
const int TREE_DEVICE_SUBSYSTEM1 = 8;
const int TREE_DEVICE_SUBSYSTEM2 = 9;
const int TREE_DEVICE_SORTSTATE = 10;

const string TREE_DEVICE_WIDGET = "_Widget";
const string TREE_DEVICE_WIDGET_PANEL = "vision/unicosObject/General/treeDeviceOverviewWidget.pnl";

//------------
// global variables for the device overview panels
//------------

global mapping g_m_ddsTreeDeviceOverviewDeviceList; // list of all the devices for all the defined systems
global dyn_string g_dsTreeDeviceOverviewRegisteredSystem;
global mapping g_m_bTreeDeviceOverviewRegisteredSystem;
global mapping g_m_bTreeDeviceOverviewSystemConnected;
global dyn_string g_dsTreeDeviceOverview_triggerDisplayDP;
global mapping g_m_bTreeDeviceOverviewRegisteredSystemInitialized;
global mapping g_m_bTreeDeviceOverview_gettingDeviceRunning;
global mapping g_m_bTreeDeviceOverview_deviceListAvailable;

global mapping g_m_sTreePVSSSystem, g_m_dsTreeFrontEnd, g_m_dsTreeFrontEndType, g_m_dsTreeFrontEndDp, 
								g_m_dsTreeFrontEndApplication, g_m_dsTreeApplication, g_m_dsTreeSubsystem1, g_m_dsTreeSubsystem2;
global dyn_string g_dsTreeSubsystem1, g_dsTreeSubsystem2, g_dsTreePVSSSystem, g_dsTreeFrontEnd, g_dsTreeApplication;

// Bits configuration loaded during startup
global dyn_string g_dsEventBitNames;						// To save bit names config (list of available bit names in _UnObject DPs)
global dyn_string g_dsTreeDeviceOverviewUnicosObjects;					// List of unicos objects
global dyn_dyn_string g_ddsEventNames;						// A dyn_string for each object containing bit names
global dyn_dyn_int g_ddiEventPositions;						// A dyn_int for each object containing bit positions
global bool g_bEventInitRecall = true;
global dyn_dyn_string g_ddsDeviceTrendConfig;

//------------
// FIP INFO CONSOLE
//------------

const string DEVICE_UN_EXPERT_CONSOLE_FOLDER = "data/";
const string DEVICE_UN_EXPERT_CONSOLE_JNLP = "un_FipExpert.jnlp";
const string FIP_DIAG_WEB = "http://abwww.cern.ch/ap/dist/lhc/lhc-dqamx/PRO/expertQps.jnlp?arg1=openpanel:fipdiag&arg2=";
const string FIP_DIAG_JAVA = "jws ";
const string FIP_DIAG_EXPERT_PROPERTY = "/FipExpert";

//------------
// CIRCUIT INFO
//------------

const string CIRCUIT_A1 = "A1";
const string CIRCUIT_A2 = "A2";
const string CIRCUIT_A = "A";
const string CIRCUIT_B1 = "B1";
const string CIRCUIT_B2 = "B2";
const string CIRCUIT_C = "C";
const string CIRCUIT_D = "D";

const string TREE_DEVICE_FILE_SUBSYSTEM = "_unTreeDeviceOverview_subSystem.dat";
const string TREE_DEVICE_FILE_FEAPPLICATION = "_unTreeDeviceOverview_FECharacteristics.dat";

//------------
// device access conttrol
//------------
const string UN_ACCESS_CONTROL_SEPARATOR = ",";
const string UN_ACCESS_CONTROL_DOMAIN_SEPARATOR = "|";
const int UNICOS_ACCESSCONTROL_DOMAIN=1;
const int UNICOS_ACCESSCONTROL_PRIVILEDGE_NUMBER = 4;
const string UNICOS_ACCESSCONTROL_DOMAINNAME = "";
const string UN_ACCESS_CONTROL_ROOT_USER = "root";
const int UN_CONFIG_UNICOS_DOMAIN_AND_ACCESS_CONTROL_LEN = 2;
const int UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN = 4;
global dyn_string g_dsDomainAccessControl; // list of access control domain
global dyn_string g_dsPrivilegeAccessControl = makeDynString("monitor","operator","expert","admin"); // list of access control priviledge
global dyn_string g_dsMenuFileAccessControl; // list of access file control settings

//------------
//openend module in independant window
//------------
global int g_iMaxOpenedModule;
global int g_iOpenedModule;
global bool g_bMaxModuleActive=false;
const int MAX_OPENED_MODULE=10;

//------------
// Constant for proxy
//------------
const string UN_PROXY_DELIMITER="->";

//------------
// Constant: device configuration dpe
//------------
const string UN_UNICOS_CONFIGURATION = ".statusInformation.configuration.unicosConfiguration";
const string UN_DEVICE_CONFIGURATION = ".statusInformation.configuration.deviceConfiguration";
const int UN_DEVICE_MAXIMUM_COMMENTS = 10;
const string UN_UNICOS_DEVICENAME_KEY = "UNICOS Device Name";
const string UN_DEVICE_LOG_SCALE_CONFIGURATION = "LOG_SCALE";

//------------
//global for the graphical frame
//------------
// global set during init
global bool g_b_unicosHMI_init1, g_b_unicosHMI_init2, g_b_unicosHMI_initEnd, g_b_unicosHMI_menu;
const string UNICOSHMI_PREVIEW_MODULE_NAME="Preview";
//global variable containing the file used by unicosHMI
global string g_s_unicosHMI_previewConfig;
const string UN_GRAPHICALFRAME_DEFAULT_CONFIGURATION_FILE = "data/unicosHMI_default.xml";
global dyn_string g_dsComponentInfo;
// global var to keep the menu configuration split in term of cascade menu
global mapping g_m_dsMenuItemId, g_m_diType;
// global var to keep file access info and enabled state
global mapping g_m_sTextMenu, g_m_sFileName, g_m_bEnabled, g_m_sMenuAccess;
// global var to keep the list of cascade menu and button popup menu shape and key
global mapping g_m_dsCascadeMenu, g_m_dsCascadeMenuKey, g_m_dsPopupMenu, g_m_dsPopupMenuKey;
// global var to keep the menu configuration
global dyn_string g_dsMenuConfiguration;
// constant for HMI functions
const int HMI_INIT_FUNCTION = 1;
const int HMI_APPLICATION_UPDATE_FUNCTION = 2;
const int HMI_REGISTER_FUNCTION = 3;
const int HMI_WAIT_REGISTER_FUNCTION = 4;
const string UNICOSHMI_1SCREEN_CONFIGURATION_FILE = "data/unicosHMI_1screen.xml";
const string UNICOSHMI_2SCREEN_CONFIGURATION_FILE = "data/unicosHMI_2screen.xml";
const string UNICOSHMI_3SCREEN_CONFIGURATION_FILE = "data/unicosHMI_3screen.xml";

global bool g_bExitRequested;
global bool g_bExitInfoOpened = false;
global bool g_bUnGraphicalFrame_RDB = false;

// global for the console $startPanel parameters definition
global string g_unicosHMI_StartPanel;
global string g_unicosHMI_DollarParam;

// global for loading split mode
global bool g_unicosHMI_SplitMode;

//------------
// Constants for unBackup
//------------

const string UNBACKUP_SCHEDULER_CAT="unBackupCheck";
const string UNBACKUP_SCHEDULER_SCRIPT="unBackupCheck";
const string UNBACKUP_SYSTEMALARM_DPN="_unSystemAlarm_backup_check";
const string UNBACKUP_LOG_OUTPUT="unBackup.log";
const string UNBACKUP_SCHEDULER_FUNCTION="unBackup_checkNFS";
const string UNBACKUP_STATUS_START = "";
const string UNBACKUP_STATUS_RUNNING = "backup running";
const string UNBACKUP_STATUS_OK = "OK";
const string UNBACKUP_STATUS_ERROR = "BAD";
const int UNONLINE_BACKUP_STATUS_STOPPED = 1;
const int UNONLINE_BACKUP_STATUS_RUNNING = 2;
const int UNONLINE_BACKUP_STATUS_DONE = 3;
const int UNONLINE_BACKUP_STATUS_UNBACKUP_RUNNING = 4;
const int UNONLINE_BACKUP_STATUS_UNBACKUP_DONE = 5;
const int UNONLINE_BACKUP_STATUS_ALREADY_RUNNING = -1;
const int UNONLINE_BACKUP_STATUS_UNKOWN_START_ERROR = -2;
const int UNONLINE_BACKUP_STATUS_UNKOWN_ERROR = -3;
const int UNONLINE_BACKUP_COMMAND_START = 1;
const int UNONLINE_BACKUP_COMMAND_STOP = 0;

//------------
// Constants for unDeviceSet script 
const int DEVICESET_GETARCHIVE_DPE = 101;
const int DEVICESET_SET_ALIAS = 102;
const int DEVICESET_SET_DESCRIPTION = 103;


//------------
// Constants for unGenericUtilities script
const int GENERIC_UTILITIES_START_MODE_ALWAYS = 2;
const int GENERIC_UTILITIES_START_MODE_ONCE   = 1;
const int GENERIC_UTILITIES_START_MODE_MANUAL = 0;

const string UN_FACEPLATE_MISSED_VALUE = "missed"; //!< Used as missed value in dynamic callbacks

const string UN_ACCESS_RIGHTS_OPERATOR =    1;
const string UN_ACCESS_RIGHTS_EXPERT =      2;
const string UN_ACCESS_RIGHTS_ADMIN =       3;
const string UN_ACCESS_RIGHTS_NOONE =       4;

