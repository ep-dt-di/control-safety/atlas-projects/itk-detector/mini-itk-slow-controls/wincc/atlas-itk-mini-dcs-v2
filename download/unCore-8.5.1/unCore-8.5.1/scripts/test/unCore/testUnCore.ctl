/**@file testUnCore.ctl

@brief Test Suite for UNICOS Framework Panel Access Control
@par Creation Date 09/02/2016
*/

#uses "fwUnitTestComponentAsserts.ctl" 
#uses "unPanelAccessControl.ctl" // The library of code being tested.

const string sPanelAccessListDPE="_unApplication.menu.menuFileAccessControl";
const string TestDomainName="TEST";
const string EmptyDomainName="TEST";
dyn_string bak_dsMenuAccess = makeDynString();

/**
@brief This routine is optional.
Prepare the appropriate global environment for all of the test cases in this suite.
setupSuite() is called once only, before any of the test cases are called.
*/ 
void testUnCore_setupSuite()
{
	if (dpExists(sPanelAccessListDPE) == FALSE) {
		DebugTN("Problem in testUnCore_setupSuite. DPE: "+sPanelAccessListDPE+" does not exist!");
	}
	else { 
		dpGet(sPanelAccessListDPE, bak_dsMenuAccess);
	}
}

//-------------------------------------------------------

/**
@brief testExampleComponent_setup() ensures a consistent environment exists prior to calling each test case.
This routine is optional. If you declare one here, it should prepare any appropriate environment
that will be required before running each of the test cases in this suite.

*/ 
void testUnCore_setup()
{
   if (dpExists(sPanelAccessListDPE) == FALSE) {
	DebugTN("Problem in testUnCore_setup. DPE: "+sPanelAccessListDPE+" does not exist!");
   }
   else { 
	dpSet(sPanelAccessListDPE, makeDynString());
   }
}

//-------------------------------------------------------

/**
@brief Example of a developer's test routine. 
A test*.ctl file can contain as many test routines as the developer wants.
*/ 
void testUnCore_testcheckPanelAccessControlDPE()
{
    dyn_string exceptionInfo;
    try {
	    unPanelAccessControl_checkPanelAccessControlDPE(exceptionInfo);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
   //assertEmpty(exceptionInfo);

}

void testUnCore_testgetPanelFromEntry()
{
    dyn_string exceptionInfo;
    string panel;
    try {
    	panel = unPanelAccessControl_getPanelFromEntry("path/to/panel.pnl||AccessOperator|||");
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    //assertEmpty(exceptionInfo);
    assertEqual("path/to/panel.pnl", panel);
    panel = "";
    try {
    	panel = unPanelAccessControl_getPanelFromEntry("",exceptionInfo);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    //assertError("ERROR", "unPanelAccessControl_addPanelAccessControlEntry: Malformed Panel Access Control entry.", exceptionInfo);
    assertEqual("", panel);
 
}

void testUnCore_testgetPatternFromPath()
{
    dyn_string exceptionInfo;
    string pattern;
    try {
	    pattern = unPanelAccessControl_getPatternFromPath("path/to/panel.pnl");
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
   //assertEmpty(exceptionInfo);
    assertEqual("path/to/panel.pnl|*", pattern);
    pattern = "";
    try {
	    pattern = unPanelAccessControl_getPatternFromPath("");
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    //assertError("ERROR", "unPanelAccessControl_addPanelAccessControlEntry: Malformed Panel Access Control entry.", exceptionInfo);
    assertEqual("", pattern);
}

void testUnCore_testdpGetPanelAccessControlList() 
{
    dyn_string dsTestEntries = makeDynString(	"path/to/panel1.pnl||||AccessAdmin|",
						"path/to/panel2.pnl|||AccessExpert||",
						"path/to/panel3.pnl||AccessOperator|||");
    dyn_string exceptionInfo;
    dyn_string dsAccessList;
    if (dpSetWait(sPanelAccessListDPE, dsTestEntries) < 0) { 
	DebugTN("Problem in testUnCore_testdpGetPanelAccessControlList. Could not dp dpSet on "+sPanelAccessListDPE);
    }
    try {
	    unPanelAccessControl_dpGetPanelAccessControlList(dsAccessList);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
   //assertEmpty(exceptionInfo);
    assertEqual(dynlen(dsTestEntries), dynlen(dsAccessList));    
}
void testUnCore_testCleanPanelAccessControlList() 
{
    dyn_string dsTestEntries = makeDynString(	"path/to/panel1.pnl||||AccessAdmin|",
						"path/to/panel2.pnl|||AccessExpert||",
						"path/to/panel3.pnl||AccessOperator|||");
    dyn_string exceptionInfo;
    try {
	    unPanelAccessControl_cleanPanelAccessControlList(dsTestEntries);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
   //assertEmpty(exceptionInfo);
    assertEqual(3, dynlen(dsTestEntries));
    if (dynAppend(dsTestEntries,  dsTestEntries[1]) < 0) {
	DebugTN("Problem in testUnCore_testCleanPanelAccessControlList. Could not append to dsTestEntries");
    }
    try {
	    unPanelAccessControl_cleanPanelAccessControlList(dsTestEntries);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
   //assertEmpty(exceptionInfo);
    assertEqual(3, dynlen(dsTestEntries));
}

void testUnCore_testaddPanelAccessControlEntry() 
{
    dyn_string dsTestEntries = makeDynString(	"path/to/panel1.pnl|||AccessExpert||",
						"path/to/panel2.pnl||AccessOperator|||");
    dyn_string exceptionInfo;
    int iNofEntriesAdded;
    try {
	    iNofEntriesAdded = unPanelAccessControl_addPanelAccessControlEntry(dsTestEntries);
    } catch {
	errClass e = fwException_get();
	DebugTN(e);
    }
   //assertEmpty(exceptionInfo);
    assertEqual(2, iNofEntriesAdded);
    if (dynAppend(dsTestEntries,  "path/to/panel3.pnl||||AccessAdmin|") < 0) {
	DebugTN("Problem in testUnCore_setup. DPE: "+sPanelAccessListDPE+" does not exist!");
    }
    try {
	    iNofEntriesAdded = unPanelAccessControl_addPanelAccessControlEntry(dsTestEntries);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
   //assertEmpty(exceptionInfo);
    assertEqual(1, iNofEntriesAdded);

    try {
	    iNofEntriesAdded = unPanelAccessControl_addPanelAccessControlEntry(dsTestEntries);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
   //assertEmpty(exceptionInfo);
    assertEqual(0, iNofEntriesAdded);
}

void testUnCore_testAddPanelAccessibleByExpert() 
{
    string panel = "path/to/panel.pnl";
    string entry = "path/to/panel.pnl|||"+UN_USER_EXPERT+"||";
    dyn_string dsPACL;
    try {
      unPanelAccessControl_addPanelAccessibleByExpert(panel);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    if (dpGet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testAddPanelAccessibleByExpert(). Could not get Panel Access Control list from DPE.");
    };
    assert((bool)dynContains(dsPACL, entry));
}

void testUnCore_testAddPanelAccessibleByAdmin() 
{
    string panel = "path/to/panel.pnl";
    string entry = "path/to/panel.pnl||||"+UN_USER_ADMIN+"|";
    dyn_string dsPACL;
    try {
      unPanelAccessControl_addPanelAccessibleByAdmin(panel);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    if (dpGet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testAddPanelAccessibleByAdmin(). Could not get Panel Access Control list from DPE.");
    };
    assert((bool)dynContains(dsPACL, entry));
}

void testUnCore_testAddPanelAccessibleByOperator() 
{
    string panel = "path/to/panel.pnl";
    string entry = "path/to/panel.pnl||"+UN_USER_OPERATOR+"|||";
    dyn_string dsPACL;
    try {
      unPanelAccessControl_addPanelAccessibleByOperator(panel);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    if (dpGet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testAddPanelAccessibleByOperator(). Could not get Panel Access Control list from DPE.");
    };
    assert((bool)dynContains(dsPACL, entry));
}

void testUnCore_testAddPanelAccessibleByUser() 
{
    string panel = "path/to/panel.pnl";
    string entry = "path/to/panel.pnl||"+UN_USER_OPERATOR+"|||";
    dyn_string dsPACL;
    try {
      unPanelAccessControl_addPanelAccessibleByUser(makeDynString(panel), makeDynString(UN_USER_OPERATOR));
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    if (dpGet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testAddPanelAccessibleByUser(). Could not get Panel Access Control list from DPE.");
    };
    assert((bool)dynContains(dsPACL, entry));
}

void testUnCore_testAddPanelAccessibleByUserMANY()
{
    dyn_string panels = makeDynString( "path/to/panel.pnl1", "path/to/panel2.pnl", "path/to/panel3.pnl");
    dyn_string levels = makeDynString( UN_USER_OPERATOR, UN_USER_ADMIN, UN_USER_EXPERT );
    dyn_string dsPACL;
    try {
      unPanelAccessControl_addPanelAccessibleByUser(panels, levels);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    if (dpGet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testAddPanelAccessibleByUser(). Could not get Panel Access Control list from DPE.");
    };
    assertEqual(3, dynlen(dsPACL));
}
/* Not implemented yet
void testUnCore_testModifyPanelAccessLevel() 
{
    string panel = "path/to/panel.pnl";
    string newpanel = "path/to/panel2.pnl";
    string oldEntry = "path/to/panel.pnl||"+UN_USER_OPERATOR+"|||";
    string modEntry = "path/to/panel.pnl||||"+UN_USER_ADMIN+"|";
    string newEntry = "path/to/panel2.pnl||"+UN_USER_ADMIN+"|";
    dyn_string dsPACL = makeDynString(oldEntry);
    if (dpSet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testModifyPanelAccessLevel(). Could not set Panel Access Control list DPE value.");
    }
    try {
      unPanelAccessControl_modifyPanelAccessLevel(panel, UN_USER_ADMIN);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    try {
      unPanelAccessControl_modifyPanelAccessLevel(panel2, UN_USER_ADMIN);
    } catch {
      errClass e = fwException_get();
      DebugTN(e);
    }
    if (dpGet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testModifyPanelAccessLevel(). Could not get Panel Access Control list from DPE.");
    };
    assertEqual(0, dynContains(dsPACL, oldEntry));
    assert((bool)dynContains(dsPACL, modEntry));
    assertEqual(0, dynContains(dsPACL, newEntry));
}
*/
void testUnCore_testDeletePanelAccessLevel()
{
    string panel = "path/to/panel.pnl";
    string entry = "path/to/panel.pnl||"+UN_USER_OPERATOR+"|||";
    dyn_string dsPACL = makeDynString(entry);
    if (dpSet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testModifyPanelAccessLevel(). Could not set Panel Access Control list DPE value.");
    }
    try {
      unPanelAccessControl_deletePanelAccessLevel(panel);
    } catch {
	errClass e = fwException_get();
	DebugTN(e);
    }
    if (dpGet(sPanelAccessListDPE, dsPACL) < 0) {
	DebugTN("Problem in testUnCore_testAddPanelAccessibleByUser(). Could not get Panel Access Control list from DPE.");
    };
    assertEqual(0, dynContains(dsPACL, entry));
}

void testUnCore_getPanelAccessControlEntryFromParam()
{
	string sEntry = "";
	string sPanel = "path/to/panel.pnl";
	string sLevel = UN_USER_ADMIN;
	string sDomain = "";
	string sGood = sPanel+"||||"+UN_USER_ADMIN+"|";
	try {
		sEntry = unPanelAccessControl_getAccessControlEntryFromParam();
		// above should throw an exception
		DebugTN("Failed to throw an exception when calling unPanelAccessControl_getAccessControlEntryFromParam without arguments!");
		assert(FALSE);
	} catch {
		assert(TRUE);
    	}
	assertEqual("", sEntry);
	try {
		sEntry = unPanelAccessControl_getAccessControlEntryFromParam(sPanel, sLevel, sEntry);
	} catch {
		errClass e = fwException_get();
		DebugTN(e);
    	}
	assertEqual(sGood, sEntry);
	
}
//-------------------------------------------------------

/**
@brief This routine is optional. 
Called after each test case routine.
*/ 
void testUnCore_teardown()
{
}

//-------------------------------------------------------

/**
@brief This routine is optional. 
Called after all test case routines in this file have been run.
*/ 
void testUnCore_teardownSuite()
{
	int iRet = -1;
	//Create panel access control dpe if lost
	if (dpExists(sPanelAccessListDPE)) {
		iRet = dpSet(sPanelAccessListDPE, bak_dsMenuAccess);
	}
	if (iRet < 0) { DebugTN("Problem in testUnCore_teardownSuite"); }
	
}
