/**@name SCRIPT: unDeviceListUpdate.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 11/09/2007

Modification History: 
  24/11/2011: Herve
  - IS-675: UNICOS scripts: system name added in the MessageText log 
  
  10/10/2011: Herve
  - IS-609  unCore - General  system name and component version added in all unCore script 
 
	18/12/2007: Herve
		- add write subsystem1, subsystem2 and FE characteristics (FE, FE DP, FE DP type, FE Application) to file
		
	24/09/2007: Herve
		- add MessageText call
	08/10/2007: Herve
		- new parameter to unDeviceListUpdate_writeFile

version 1.0

Purpose: 
Update the device list file for the OWS client

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable used in the include files
	. PVSS version: 3.0 
	. operating system: WXP and Linux
	. distributed system: yes
*/

// include files
#uses "CtrlTreeDeviceOverview"
#uses "unTreeDeviceOverview.ctl"
#uses "unFilterDpFunctions.ctl"
#uses "unDeviceListUpdate.ctl"

//@{

main()
{
	dyn_string exceptionInfo;

// get the list of UNICOS device type
	unGenericDpFunctions_getUnicosObjects(g_dsDeviceType);

	unMessageText_send("*", "*", getSystemName()+"DeviceListUpdate", "user", "*", "EXPERTINFO", 
				"DeviceListUpdate initialisation successfully done" , exceptionInfo);
// register to all system for any triggerUpdate and re-create the corresponding file
	unDeviceListUpdate_registerTriggerUpdate("unDeviceListUpdate_applicationTrigger");

//// register to all system to get the device list and update the corresponding file
//	unDeviceListUpdate_register("unDeviceListUpdate_registerCB");

    string query = "SELECT '.config:_original.._value' FROM '*' WHERE _DPT = \"_UnDistributedControl\"";
    int iRes = dpQueryConnectSingle("DistConfigChangedCB",true,"Trace Changes in _UnDistributedControl DPs",query,3000);

    if(isFunctionDefined("unMessageText_send")) unMessageText_send("*", "*", getSystemName()+"unDeviceListUpdate", "user", "*", "INFO", "unDeviceListUpdate "+unGenericDpFunctions_getComponentsVersion(makeDynString("unCore"))+" loaded", exceptionInfo);

}


void DistConfigChangedCB(anytype userData, dyn_dyn_mixed data)
{
    dyn_string exceptionInfo;
    unDeviceListUpdate_register("unDeviceListUpdate_registerCB");
    delay(0, 10);
    if(isFunctionDefined("unMessageText_send")) unMessageText_send("*", "*", getSystemName()+"unDeviceListUpdate", "user", "*", "INFO", "unDeviceListUpdate "+unGenericDpFunctions_getComponentsVersion(makeDynString("unCore"))+" loaded", exceptionInfo);

}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_registerCB
/**
Purpose:
Distributed control register callback function: create the device list file

Parameters:
	@param sDpe: string, input, the distributed control DPE of a PVSS system
	@param bSystemConnected: bool, input, state of the system

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOACTRL
	. operating system: WXP and Linux
	. distributed system: yes
  
  @reviewed 2018-06-25 @whitelisted{Callback}
*/
unDeviceListUpdate_registerCB(string sDpe, bool bSystemConnected)
{
	string sSystemName;
	string sDp = dpSubStr(sDpe, DPSUB_DP);
	bool bRemote, localConnected;
	dyn_dyn_string ddsTemp;
	
// get the system name and check if it is local or remote
	sSystemName = substr(sDp, strlen(c_unDistributedControl_dpName), strlen(sDp))+":";
	unDistributedControl_isRemote(bRemote, sSystemName);
//DebugN("unTreeDeviceOverview_register", g_m_bTreeDeviceOverviewRegisteredSystem, sDp, bSystemConnected, bRemote, sSystemName, mappingHasKey(g_m_bTreeDeviceOverviewRegisteredSystem, sSystemName));
	if(mappingHasKey(g_m_bTreeDeviceOverviewRegisteredSystem, sSystemName)) {
//DebugN("has key");
		if(bRemote)
			localConnected = bSystemConnected;
		else
			localConnected = true;
		if(localConnected) { //the system is connected
	// if the system is not already connected add it: trigger the display cb
	// else do nothing
			if(!g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName]) {
				g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName] = true;
				g_m_bTreeDeviceOverviewSystemConnected[sSystemName] = true;
				g_m_ddsTreeDeviceOverviewDeviceList[sSystemName] = ddsTemp;
//DebugN("set connc", sSystemName);
				if(!g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName]) { // startup, start thread
					g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = true;
					startThread("unDeviceListUpdate_getAndWriteDevice", sSystemName, g_dsDeviceType);
				}
				else { // not at startup
					if(!g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName]) {
//DebugN("tree reg");
						g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = true;
						unDeviceListUpdate_getAndWriteDevice(sSystemName, g_dsDeviceType);
						g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = false;
					}
				}
			}
		}
		else { // the system is disconnected
	// if the system is connected delete it: trigger the display cb
	// else do nothing
			if(g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName]) {
				g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName] = false;
				g_m_bTreeDeviceOverviewSystemConnected[sSystemName] = false;
				g_m_ddsTreeDeviceOverviewDeviceList[sSystemName] = ddsTemp;
//DebugN("set disc", sSystemName);
			}
			g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName] = true;
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_applicationTrigger
/**
Purpose:
triggerUpdate CB function: update the device data list file

Parameters:
	@param sdpe: string, input, the dpe name
	@param bTriggerUpdate: bool, input, triggerUpdate value

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOACTRL, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
  
  @reviewed 2018-06-25 @whitelisted{Callback}
*/
unDeviceListUpdate_applicationTrigger(string sdpe, bool bTriggerUpdate)
{
	string sSystemName = unGenericDpFunctions_getSystemName(sdpe);

//DebugTN("start unDeviceListUpdate_applicationTrigger", sSystemName);
	g_m_unicosGraphicalFrame_bUpdate[sSystemName] = true;
	if(g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName]) {
		if(!g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName]) {
//DebugN("trigger upda", sSystemName);
			g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = true;
			unDeviceListUpdate_getAndWriteDevice(sSystemName, g_dsDeviceType, false);
			g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = false;
		}
	}
//DebugTN("end unDeviceListUpdate_applicationTrigger", sdpe);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_getAndWriteDevice
/**
Purpose:
Get and write the device list into a file

Parameters:
	@param sSystemName: string, input, the system name
	@param dsDeviceType: dyn_string, input, the list of device type

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOACTRL, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_getAndWriteDevice(string sSystemName, dyn_string dsDeviceType, bool bAlwaysWrite=true)
{
	dyn_dyn_string ddsTemp, ddsRead;
	bool bResult, bWrite=bAlwaysWrite;
	
//DebugTN("*** start unDeviceListUpdate_getAndWriteDevice", sSystemName, bAlwaysWrite);
	g_m_bTreeDeviceOverview_deviceListAvailable[sSystemName] = false;
	_unTreeDeviceOverview_getDeviceList(sSystemName, dsDeviceType, ddsTemp);
	
	if(!g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName])
		bWrite = true;
	if(!bWrite) {
		_unTreeDeviceOverview_getDeviceListFomFile(sSystemName, ddsRead);
		if(!unDeviceListUpdate_compareDeviceList(ddsRead, ddsTemp)) 
			bWrite = true;
	}
	if(bWrite)
	{
		g_m_ddsTreeDeviceOverviewDeviceList[sSystemName] = ddsTemp;
		unDeviceListUpdate_writeFile(sSystemName, ddsTemp, "DeviceListUpdate", "DeviceListUpdate writing tree device file for ");
		unDeviceListUpdate_writeSubsystemAndFECharacteristicsToFile(sSystemName, "unDeviceListUpdate", "unDeviceListUpdate writing sybsystems file for ", "unDeviceListUpdate writing FE characteristics file for ");
		unDeviceListUpdate_triggerHMI(DEVUN_ADD_COMMAND, sSystemName);
	}
	
	g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName] = true;
	g_m_bTreeDeviceOverview_deviceListAvailable[sSystemName] = true;
	g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = false;
//DebugTN("*** end unDeviceListUpdate_getAndWriteDevice", sSystemName, bWrite);
}

//-------------------------------------------------------------------------------------------------------------------------

//@}
