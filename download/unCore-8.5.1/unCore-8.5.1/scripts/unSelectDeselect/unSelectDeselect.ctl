
/**@name SCRIPT: unSelectDelesect.ctl

@author: Herve Milcent (LHC-IAS)

Creation Date: 19/04/2002

Modification History: 
  24/11/2011: Herve
  - IS-675: UNICOS scripts: system name added in the MessageText log 
  
  10/10/2011: Herve
  - IS-609  unCore - General  system name and component version added in all unCore script 
 
	30/06/2006: Herve
		remove all errorText function call and replace them by getCatStr
		
	02/12/2005: Celine
		change the message for device selection from INFO to EXPERTINFO in _unSelectDeselect_connectCB()

	09/12/2004: Herve
		bug PVSS: if WCCIList is killed then all the locks are removed. Implementation to handle this case:
			. before startThread:
				- add in a mapping the lock value of the dp and dpConnect to the lock
				- in the callback: set the lock into the mapping
			. in the thread:
				- check the value of the lock in the mapping, 
				- if false then reset the timer, dpDisconnect and remove from mapping
			. before killing the thread:
				- dpDisconnect and remove from mapping

	05/07/2004: Herve
		- remove all the errorText function call deprecated function
		- check if dp+".statusInformation.selectedManager" is existing, if not generate an error

	12/08/2002:	Herve 
		Only one manager per dp selected and one manager can have many dp selected
		Remove the deselect of the old dp selected, move it to the library.

	10/07/2002:	Herve 
		The set of the lock is done on ".statusInformation.selectedManager" and the the value is set to 
		ManagerNum;systemName where ManagerNum is the manager number of the PVSS manager that requested the lock and 
		systemName is the systemName of the PVSS system to which is connected the PVSS manager requesting the lock.
		This is to be able to show that an object is already locked by another manager.

version 1.1

Purpose: 
This script implements the select/deselect feature. One script per PVSS system must be started. In a distributed 
PVSS system, this script must be started per event manager.

Only one select per manager number and system name is allowed.

It uses the _unSelectDeselect data point type of type _UnSelectDeselect.
This data point is automatically created if it is not existing and a default value is set to the 
_unSelectDeselect.timeOut: c_defaultSelectDeselect_timeOut, this default can be changed by modifying the 
constant variable c_defaultSelectDeselect_timeOut.

The _UnSelectDeselect data point type has the following data point element:
	. dpElementName: string, the data point which has to be selected/deselected
	. remainingTime: the remaining time before an automatic delesect
	. timeOut: the timeOut value for the autoamtic deselect
	. select: 1=select the dpElementName, 0=deselect the dpElementName
	. messageText: the script writes here any message for the operator
	. managerId: the manager identifier requesting the select/deselect
	. diagnostic: structure used for diagnostic composed of:
		. threadId: list of thread started by the manager
		. managerIdList: list of manager that requested a select, this is systemName and manager number of the manager
		. dpElementSelected: list of data point element selected
		. request: to request the diagnostic.
		
The select and deselect is based on setting or resetting the .statusInformation.selectedManager:_lock._original._locked PVSS 
config of the dpName (_UnStatusInformation data point element type). The .statusInformation.selectedManager data point element is set to ManagerNum;systemName 
where ManagerNum is the manager number of the PVSS manager that requested the lock and 
systemName is the systemName of the PVSS system to which is connected the PVSS manager requesting the lock. 
It is important that the systemName of the dpElementName is the same as the systemName 
of the script because otherwise the lock will be rejected by the event manager. 
An error will be written to the PVSS log file if the dpElement is not a data point element: e.g.: analog.value, 
analog., etc.

During the initialisation, a dpConnect will be done to the dpElementName, managerId and select of the 
_unSelectDeselect data point. When the select is requested a thread is started in background and will deselect 
after the timeOut any selected element. The managerId must have the following format: managerNumber;systemName. 
The request will be rejected if the format is not correct.
Another dpConnect is set to a diagnostic function. This diagnostic can be initiated at any time by setting 
_unSelectDeselect.diagnostic.request to any value. The diagnostic.threadId, diagnostic.managerIdList and 
diagnostic.dpElementSelected of the _unSelectDeselect datapoint are set. This gives information on the internal 
state of the script.

The script is also writing to the _unSelectDeselect.messageText information message. The format is:
managerId;textString, where managerId is the manager number and system name concerned by the textString message. 
A value -1;0 for managerId means that textString is of general interest.

If a select/deselect on a data point element is requested and this data point element is already in used by another 
manager Id, the script will reject the request.

The scritps keeps 3 internal lists:
	. one for the threadId currently running (for automatic deselect)
	. one for the managerId that requested the select
	. one for the data point on which a select was requested
An entry of the 3 lists (same entry index of the 3 lists) represents the threadId that will deselect the data point element requested by the managerId.
The lists are written to the diagnostic structure on request.
A manager can have many dp selected but a dp is selected by one and only one manager.

If the _unSelectDeselect.select is 1, the script will 
	. get the managerId of the requested _unSelectDeselect.dpElementName, if any it stops any thread previously started
	. select the new _unSelectDeselect.dpElementName if not already selected
	. start a thread to automatically deselect the _unSelectDeselect.dpElementName after a timeOut.

If the _unSelectDeselect.select is 0, the script will 
	. stop any thread previously started for that _unSelectDeselect.dpElementName
	. deselect the _unSelectDeselect.dpElementName

The thread will deselect the data point element selected. The remaining time before the select is store in 
_unSelectDeselect.remainingTime. The format is managerId;remainingTime before deselect, managerId is the manager number   
and systemName is the system name of the manager that requested the select, -1;0 as a managerId means that all the remaining time are set.

The timeOut can be changed by modifying the _unSelectDeselect.timeOut, however this 
will be be only used for the next thread which will be started by a select.

Any lock taken is released by PVSS if the manager which started this script exits before 
releasing correctly the lock. 

If the lock is already taken by another manager, ie: _lock._original._man_type is the not 
equal to CTRL_MAN, and _lock._original.man_nr is not the same as the current manager number, the lock is neither 
released and neither taken, an error is written in the PVSS log file.

The function to kill the current existing thread and to reset the thread identifier are blocking 
function in order to implement a kind of lock mechanism (an exclusive write).

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unSelectDeselectDpName: string, this is the data point name which is used to hold 
		the configuration for the script: data point systemName:_unSelectDeselect.
		. g_unTimeOut: int, this is the global variable which is used to set the default value of
		_unSelectDeselect.timeOut when the data point _unSelectDeselect is created.
		. g_unSelectDeselect_managerIdList: dyn_string, this is the global variable holding the list of manager 
		number and system name that requested a select on a data point element
		. g_unSelectDeselect_dpElementSelected: dyn_string, this is the global variable holding the list of 
		data point that are currently selected.
		. g_unThreadId: dyn_int, this is the global variable holding the identifier of the started thread.
		. g_systemName: string, this is the global variable holding the system name of the script.
	. constant:
		. c_unSelectDeselectDpName: data point name for the configuration of the script
		. c_unSelectDeselect_dpType: data point type of the configuration data point  of the script
		. c_PVSS_MAX_UI: maximum PVSS manager Id that can trigger a select/deselect, any request with a manager number value 
		above this constant will be rejected 
		. c_PVSS_MIN_UI: minimum PVSS manager Id that can trigger a select/deselect, any request with a manager number value 
		below this constant will be rejected
		. c_defaultSelectDeselect_timeOut: default timeOut value set at creation time of the configuration data point 
		of the script.
	. data point type needed: _UnSelectDeselect, _UnStatusInformation
	. the data point that has to be selected/deselected must have a data point element of type _UnStatusInformation
	. data point: the following data point is needed, if it does not exist it is created
		. _unSelectDeselect: of type _UnSelectDeselect
	. the data point that has to be selected/deselected must have a data point element of type _UnStatusInformation
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
#uses "unSendMessage.ctl"

// constant declaration
const string c_unSelectDeselectDpName = "_unSelectDeselect"; // data point name for the manager: _unSelectDeselect
const string c_unSelectDeselect_dpType = "_UnSelectDeselect"; // type of the data point to be created
const int c_PVSS_MAX_UI = 255; // maximum PVSS UI for a PVSS system.
const int c_PVSS_MIN_UI = 1; // minumum PVSS UI for a PVSS system.
const int c_defaultSelectDeselect_timeOut = 120; // default timeOut set at creation time.
// end contant declaration

//global declaration
// the global variable g_unTimeOut, this variable is global to the manager
global int g_unTimeOut;
// the global variable g_unSelectDeselectDpName, this variable is global to the manager, this is the dpName of the dp,
global string g_unSelectDeselectDpName;
// global variable holding the system name
global string g_systemName;
// global variables used to store the list of manager number and system name, threadId and dpElementName selected for this managerId
// an entry corresponds to a select granted to a managerId
// the lists have the same length
global dyn_string g_unSelectDeselect_managerIdList;
global dyn_string g_unSelectDeselect_dpElementSelected;
global dyn_int g_unThreadId;
global mapping g_mDpNameList_lockState;

//end global declaration


//@{

// main
/**
Purpose:
This is the main of the script.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
main()
{
	dyn_string exceptionInfo; // to hold any errors returned by the function

// initialize the select/deselect mechanism
	unSelectDeselect_init(c_defaultSelectDeselect_timeOut, exceptionInfo);

// handle any error, send message to the MessageText component
	if(dynlen(exceptionInfo) > 0) {
		unMessageText_sendException("*", "*", getSystemName()+"SelectDeselect", "user", "*", exceptionInfo);
// handle any error uin case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugN(getCurrentTime(), exceptionInfo);
		}
	}
	else {
		unMessageText_send("*", "*", getSystemName()+"SelectDeselect", "user", "*", "EXPERTINFO", 
						getCatStr("unSelectDeselect", "STARTED"), exceptionInfo);
// handle any error uin case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugN(getCurrentTime(), getCatStr("unSelectDeselect", "STARTED"), exceptionInfo);
		}
	}
  
  delay(0, 10);
  if(isFunctionDefined("unMessageText_send"))
    unMessageText_send("*", "*", getSystemName()+"unSelectDeselect", "user", "*", "INFO", "unSelectDeselect "+unGenericDpFunctions_getComponentsVersion(makeDynString("unCore"))+" loaded", exceptionInfo);
}

// unSelectDeselect_init
/**
Purpose:
This function does the initialisation the select/deselect feature. 
This function creates the following data point if it does not exist:	
	. _unSelectDeselectn: of type _UnSelectDeselect. 
This function sets the unSelectDeselect_connectCB callback function with  
_unSelectDeselect.dpElementName, _unSelectDeselect.managerId and _unSelectDeselect.select
This function sets the _unSelectDeselect_connectDiagnostic callback function with _unSelectDeselect.request
	
	@param iDefaultTimeOut: int, input, default value of the timeOut when the data point is created
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unSelectDeselectDpName: string
		. g_unTimeOut: int
		. g_unSelectDeselect_managerIdList: dyn_string
		. g_unSelectDeselect_dpElementSelected: dyn_string
		. g_unThreadId: dyn_int
		. g_systemName: string
	. data point type needed: _UnSelectDeselect
	. data point: the following data point is needed, if it does not exist it is created
		. _unSelectDeselect: of type _UnSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
unSelectDeselect_init(int iDefaultTimeOut, dyn_string &exceptionInfo)
{
	int i;
	bool bError=false;

// the variables are initialised, the g_unTimeOut is used if the data point is created
	g_unTimeOut = iDefaultTimeOut;
	g_unSelectDeselectDpName = getSystemName()+c_unSelectDeselectDpName;
	g_systemName = getSystemName();
// the manager is of good type, check if the data point is existing: _unSelectDeselect 
// create it if not
	if(!dpExists(g_unSelectDeselectDpName)) {
//			DebugN(dpName+" not existing, creating..");
		dpCreate(c_unSelectDeselectDpName, c_unSelectDeselect_dpType);
// a second check is done after the creation to ensure that the data point is created (just in case of error)
// it is also a way to wait the creation which is asynchronous.
		if(dpExists(g_unSelectDeselectDpName))  {
// set the default timeOut value, the select is set to false, and the remaining time is reseted
			dpSetWait(g_unSelectDeselectDpName+".timeOut",g_unTimeOut, 
					g_unSelectDeselectDpName+".select",false);
			_unSelectDeselect_setMessage("*;*", "EXPERTINFO", getCatStr("unSelectDeselect", "INIT"));
		} // end second dpExists
		else {
			fwException_raise(exceptionInfo, "ERROR", 
						"unSelectDeselect_init(): the data point: "+g_unSelectDeselectDpName+" was not created","");
			bError = true;
		}
	}
	if(!bError) {
// the data point is existing, the remaningTime is resetted and the callback function 
// is instantiate, the select/deselect mechanism is implemented in the 
// _unSelectDeselect_connectCB function.
		dpSet(g_unSelectDeselectDpName+".remainingTime", "-1;0;0");
		if(dpConnect("_unSelectDeselect_connectCB", false,
						g_unSelectDeselectDpName+".dpElementName",
						g_unSelectDeselectDpName+".managerId",
						g_unSelectDeselectDpName+".select") == -1) {
			fwException_raise(exceptionInfo, "ERROR", 
						"unSelectDeselect_init(): the data point: failed dpConnect","");
			_unSelectDeselect_setMessage("*;*", "EXPERTINFO", getCatStr("unSelectDeselect", "DPCONFAILED"));
		} // end dpConnect
		else {
			_unSelectDeselect_setMessage("*;*", "EXPERTINFO", getCatStr("unSelectDeselect", "SELDSELSTARTED"));
// connect diagnostic
			if(dpConnect("_unSelectDeselect_connectDiagnostic", false,
						g_unSelectDeselectDpName+".diagnostic.request") == -1) {
				fwException_raise(exceptionInfo, "ERROR", 
						"unSelectDeselect_init(): the data point: failed dpConnect diagnostic","");
				_unSelectDeselect_setMessage("*;*", "EXPERTINFO", getCatStr("unSelectDeselect", "DIAGDPCONFAILED"));
			} // end dpConnect
			else 
				_unSelectDeselect_setMessage("*;*", "EXPERTINFO", getCatStr("unSelectDeselect", "DIAGSTARTED"));
		} // end else dpConnect
	} // end first dpExists
}

// _unSelectDeselect_connectDiagnostic
/**
Purpose:
This is the callback function for diagnostic.
It writes back to the diagnostic structure of the _unSelectDeselect data point element.
	
	@param sDp1: string, input, _unSelectDeselect.diagnostic:original..value
	@param request: int, input, value of sDp1

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are needed:
		. g_unSelectDeselect_dpElementSelected: dyn_string
		. g_unSelectDeselectDpName: string
		. g_unThreadId: dyn_int
		. g_unSelectDeselect_managerIdList: dyn_string 
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{Callback}
*/
_unSelectDeselect_connectDiagnostic(string sDp1, int request)
{
	dpSet(g_unSelectDeselectDpName+".diagnostic.threadId",g_unThreadId,
			g_unSelectDeselectDpName+".diagnostic.managerIdList",g_unSelectDeselect_managerIdList,
			g_unSelectDeselectDpName+".diagnostic.dpElementSelected",g_unSelectDeselect_dpElementSelected);
}

// _unSelectDeselect_setMessage
/**
Purpose:
set the message text.	

	@param manager: string, input, the manager identification, the format is managerNum;systemName
	@param sType: string, input, the type of message
	@param message: string, input, message to send


Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are needed:
		. g_unSelectDeselect_dpElementSelected: dyn_string
		. g_unSelectDeselectDpName: string
		. g_unThreadId: dyn_int
		. g_unSelectDeselect_managerIdList: dyn_string 
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
_unSelectDeselect_setMessage(string manager, string sType, string message)
{
	dyn_string exceptionInfo, split;
	string sManager, sSystemName;
	
//	dpSet(g_unSelectDeselectDpName+".messageText", manager+";"+message);
	split = strsplit(manager, ";");
	unMessageText_send(split[2], split[1], getSystemName()+"SelectDeselect", "user", "*", sType, 
		message, exceptionInfo);
		


// handle any error uin case the send message failed
	if(dynlen(exceptionInfo) > 0) {
		DebugN(getCurrentTime(), message, exceptionInfo);
	}
}

// _unSelectDeselect_connectCB
/**
Purpose:
This is the callback function, the select/deselect mechanism is implemented by this function.
The select and deselect is based on setting or resetting the .statusInformation.selectedManager._lock._original._locked PVSS 
config of the dpElementName. An error will be written to the PVSS log file if the dpElement
is not a data point element: e.g.: analog.value, analog., etc..

If a select/deselect on a data point element is requested and this data point element is already in used by another 
manager Id, the script will reject the request.

If the _unSelectDeselect.select is 1, the script will 
	. get the managerId of the requested _unSelectDeselect.dpElementName, if any it stops any thread previously started
	. select the new _unSelectDeselect.dpElementName if not already selected
	. start a thread to automatically deselect the _unSelectDeselect.dpElementName after a timeOut.

If the _unSelectDeselect.select is 0, the script will 
	. stop any thread previously started for that _unSelectDeselect.dpElementName
	. deselect the _unSelectDeselect.dpElementName

Any lock taken is released by PVSS if the manager which started this script exits before 
releasing correctly the lock. 

If the lock is already taken by another manager, ie: _lock._original._man_type is the not 
equal to CTRL_MAN, and _lock._original.man_nr is not the same as the current manager number, the lock is neither 
released and neither taken, an error is written in the PVSS log file.

If the system name of the sDpElementName is not the same as the system name of the script, the request is rejected 
because in a distributed system it is not possible to set a config of a data point element from another system that 
the one in which is the data point element.

The function to kill the current existing thread and to reset the thread identifier are blocking 
function: in order to implement a kind of lock mechanism (an exclusive write).

The script is also writing to the _unSelectDeselect.messageText information message. The format is:
managerId;textString, where managerId is the manager number, systemName is the system name of the 
manager concerned by the textString message. 

The managerId must have the following format: managerNumber;systemName. The request will be rejected if the format 
is not correct or if the managerNumber is not between c_PVSS_MIN_UI and c_PVSS_MAX_UI.

	@param sDp1: string, input, _unSelectDeselect.dpElementName:original..value
	@param sDpElementName: string, input, value of sDp1, this is the data point element to lock/unlock
	@param sDp2: string, input, _unSelectDeselect.managerId:original..value
	@param manager: string, input, value of the sDp2, manager id requesting the select/deselect
	@param sDp3: string, input, _unSelectDeselect.select:original..value
	@param bSelect: bool, input, value of the sDp2, 1=select (lock) sDpElementName,0=deselect (unlock) sDpElementName

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. data point type needed: _UnSelectDeselect, _UnStatusInformation
	. the data point that has to be selected/deselected must have a data point element of type _UnStatusInformation
	. global variable: the following variables are global to the script
		. g_unSelectDeselectDpName: string
		. g_systemName: string
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{Callback}
*/
_unSelectDeselect_connectCB(string sDp1, string sDpElementName, string sDp2, string manager, string sDp3, bool bSelect)
{
	bool lock; // internal variable to hold the lock value of sDpName
	int man_type, man_num, timeOut, result, pos, threadId, managerId;
	string /*sOldDpName="", */sManager;
	dyn_errClass error;
	string message = "deselect", sError;
	bool bError = false;
	dyn_string split;
	string sDpName, selectedManager="", deviceName;

	sDpName = dpSubStr(sDpElementName, DPSUB_SYS_DP);
	split = strsplit(manager,";");
// manager must be managerId;systemName, error otherwise and the request is rejected
	if(dynlen(split) != 2) {
		_unSelectDeselect_setMessage("*;*", "EXPERTINFO", getCatStr("unSelectDeselect", "WRONGMAN")+": "+manager);
		return;
	}
	managerId = (int)split[1];
// if managerId is not between c_PVSS_MIN_UI and c_PVSS_MAX_UI, the request is rejected
	if((managerId> c_PVSS_MAX_UI) || (managerId < c_PVSS_MIN_UI)) {
		_unSelectDeselect_setMessage("*;*", "EXPERTINFO", getCatStr("unSelectDeselect", "WRONGMANID")+": "+managerId);
		return;
	}
// check if the sDpName is existing
	if(!dpExists(sDpName+".statusInformation.selectedManager")) {
		_unSelectDeselect_setMessage(manager, "INFO", getCatStr("unSelectDeselect", "DPDOESNOTEXIST")+": "+sDpName+".statusInformation.selectedManager");
		return;
	}

// get the alias of the sDpName
	deviceName = unGenericDpFunctions_getAlias(sDpName);

// check if the system name of the sDpName is the same as the system name of the script
	if(dpSubStr(sDpName, DPSUB_SYS) != g_systemName) {
		_unSelectDeselect_setMessage(manager, "INFO", getCatStr("unSelectDeselect", "DPWRONGSYSNAM")+": "+
									 unSendMessage_getDeviceDescription(sDpName)+", "+g_systemName);
		return;
	}
	
// get the lock parameter of sDpName, do not process anymore if the requested manager is not allowed 
	dpGet(sDpName+".statusInformation.selectedManager:_lock._original._locked",lock,
			sDpName+".statusInformation.selectedManager:_lock._original._man_type",man_type,
			sDpName+".statusInformation.selectedManager:_lock._original._man_nr",man_num);
	if(lock) {
		if((man_type != CTRL_MAN) && (man_num != myManNum())) {
// this means that another manager is already using this data point element
			_unSelectDeselect_setMessage(manager, "INFO", unSendMessage_getDeviceDescription(sDpName)+": "+
										 getCatStr("unSelectDeselect", "DPSYSALSEL")+": "+man_num+", "+man_type);
			return;
		}
	}
// check if the requested dp element is in the list of dps
	pos = _unSelectDeselect_getManagerId(sDpName, sManager, threadId);
	if((pos != -1) && (sManager != manager)) {
		_unSelectDeselect_setMessage(manager, "INFO", unSendMessage_getDeviceDescription(sDpName)+": "+
									 getCatStr("unSelectDeselect", "DPALSEL")+": "+split[1]+" "+split[2]);
		return;
	}

	if(pos != -1) {
// stop any thread previously started, this is blocking call. Via this mechanism an exclusive write
		if(mappingHasKey(g_mDpNameList_lockState, sDpName)) {
// dpDisconnect if sDpName is in mapping
			dpDisconnect("_unSelectDeselect_LockStateCB", sDpName+".statusInformation.selectedManager:_lock._original._locked");
// remove from the mapping
			mappingRemove(g_mDpNameList_lockState, sDpName);
		}
//DebugN("deselect", g_mDpNameList_lockState);
// access to the g_unThreadId is done. If the thread is killed the g_unThreadId is set to -1
		g_unThreadId[pos] = _unSelectDeselect_killThread(manager, threadId);
	}
	
	if(bSelect) {
		message = "select";
		selectedManager = manager;
	}

// if bSelect is not the same as the actual lock
	if(bSelect != lock) {
		dpSetWait(sDpName+".statusInformation.selectedManager", selectedManager,
				sDpName+".statusInformation.selectedManager:_lock._original._locked",bSelect);
		error = getLastError();
		if(dynlen(error) > 0) {
			bError = true;
			throwError(error);
			sprintf(sError, "%05d", getErrorCode(error));
			_unSelectDeselect_setMessage(manager, "INFO", message+" "+unSendMessage_getDeviceDescription(sDpName)+
										 " failed: "+getCatStr("_errors", sError));
		}
		else {
			_unSelectDeselect_setMessage(manager, "EXPERTINFO", message+" "+unSendMessage_getDeviceDescription(sDpName));
		}
	} // end not same command
	else {
/*Change from INFO to EXPERTINFO*/
		_unSelectDeselect_setMessage(manager, "EXPERTINFO", unSendMessage_getDeviceDescription(sDpName)+" already "+message+"ed");
	}
// if no error and select then start the thread
	if(!bError) {
		if(bSelect) {
//			DebugN("startThread");

// add the lock state of the sDpName in the mapping variable
			g_mDpNameList_lockState[sDpName] = bSelect;
// do a dpConnect to the lock
			dpConnect("_unSelectDeselect_LockStateCB", sDpName+".statusInformation.selectedManager:_lock._original._locked");

			dpGet(g_unSelectDeselectDpName+".timeOut:_online.._value", timeOut);
// the thread is started, this is a blocking function, the identifier is returned by the function
// the timeOut is read from the _unSelectDeselect.timeOut
			if((threadId = startThread("_unSelectDeselect_thread", timeOut, manager, sDpName)) == -1) {
				_unSelectDeselect_setMessage(manager, "EXPERTINFO", getCatStr("unSelectDeselect", "THNOTSTARTED"));
// remove from the list the managerId and the sDpName
// indivisible function
				result = _unSelectDeselect_removeFromList(sDpName);
			}
			else {
				_unSelectDeselect_setMessage(manager, "EXPERTINFO", getCatStr("unSelectDeselect", "AUTODSEL")+" of "+
											 unSendMessage_getDeviceDescription(sDpName)+" in: "+timeOut+" seconds");
// add in the list the managerId and the sDpName
// indivisible function
				result = _unSelectDeselect_addInList(manager, sDpName, threadId);
			} // end select
		} // end bSelect
		else {
// remove from the list the managerId and the sDpName
// indivisible function
			result = _unSelectDeselect_removeFromList(sDpName);
		}
	}
}

// _unSelectDeselect_addInList
/**
Purpose:
This function add an entry or replace an existing entry in the g_unSelectDeselect_managerIdList, g_unSelectDeselect_dpElementSelected 
and g_unThreadId lists. The entry index is the index of the manager in the g_unSelectDeselect_managerIdList. The 
entry index is the same for the 3 lists.
This is an indivisible function.

	@param manager: string, input, the manager Id.
	@param sDpElementName: string, this is the data point element to be deselected by threadId, used to find the existing entry or add a new entry
	@param threadId: int, input, thread Id of the thread that will deselect sDpElementName
	@param return value: int, always 0

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unSelectDeselect_managerIdList: dyn_string
		. g_unSelectDeselect_dpElementSelected: dyn_string
		. g_unThreadId: dyn_int
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int _unSelectDeselect_addInList(string manager, string sDpElementName, int threadId)
{
	int pos;
	
// check if already in list, and replace or add
	pos = dynContains(g_unSelectDeselect_dpElementSelected, sDpElementName);
	if(pos>0) {
// already in list, just replace the manager
		g_unSelectDeselect_managerIdList[pos] = manager;
		g_unThreadId[pos] = threadId;
	}
	else {
// not in list append in all the lists
		dynAppend(g_unSelectDeselect_managerIdList, manager);
		dynAppend(g_unSelectDeselect_dpElementSelected, sDpElementName);
		dynAppend(g_unThreadId, threadId);
	}
	return 0;
}

// _unSelectDeselect_getManagerId
/**
Purpose:
This function returns the managerId that selected a data point element.
This is an indivisible function.

	@param sDpElementName: string, input, this is the data point element to be deselected by threadId
	@param sManager: string, output, this is the data point element to be deselected by threadId
	@param threadId: int, output, thread Id of the thread that will deselect sDpElementName
	@param return value: int, -1 if the sDpElementName is not selected by another manager, otherwise managerId of the manager that selected sDpElementName

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unSelectDeselect_managerIdList: dyn_string
		. g_unSelectDeselect_dpElementSelected: dyn_string
		. g_unThreadId: dyn_int
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int _unSelectDeselect_getManagerId(string sDpElementName, string &sManager, int &threadId)
{
	int pos;
	
// check if in list
	pos = dynContains(g_unSelectDeselect_dpElementSelected, sDpElementName);
	if(pos>0) {
		sManager = g_unSelectDeselect_managerIdList[pos];
		threadId = g_unThreadId[pos];
		return pos;
	}
	else
		return -1;
}

// _unSelectDeselect_removeFromList
/**
Purpose:
This function remove an existing entry in the g_unSelectDeselect_managerIdList, g_unSelectDeselect_dpElementSelected 
and g_unThreadId lists. The entry index is the index of the managerId in the g_unSelectDeselect_managerIdList. The 
entry index is the same for the 3 lists.
This is an indivisible function.

	@param sDpName: string, input, the data point element name selected used to find the existing entry.
	@param return value: int, always 0

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unSelectDeselect_managerIdList: dyn_string
		. g_unSelectDeselect_dpElementSelected: dyn_string
		. g_unThreadId: dyn_int
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int _unSelectDeselect_removeFromList(string sDpName)
{
	int pos;
	
// check if already in list, and replace or add
	pos = dynContains(g_unSelectDeselect_dpElementSelected, sDpName);
	if(pos>0) {
// in list, remove from both list
		dynRemove(g_unSelectDeselect_managerIdList, pos);
		dynRemove(g_unSelectDeselect_dpElementSelected, pos);
		dynRemove(g_unThreadId, pos);
	}
	return 0;
}

// _unSelectDeselect_thread
/**
Purpose:
This function is started by the _unSelectDeselect_connectCB in a seperate thread to 
automatically deselect the data point. The timeOut given as an argument is the number of seconds
before deselecting the data point. The _unSelectDeselect.remaingTime is decremented 
every seconds. At the end of the time out the lock is released on sDpElementName and 
the g_unThreadId is set to -1 by the _unSelectDeselect_resetThreadId (blocking function 
in order to implement an exclusive write access)

The thread will deselect the data point element selected. The remaining time before the select is store in 
_unSelectDeselect.remainingTime. The format is managerId;remainingTime before deselect, managerId is the id of 
the manager that requested the select, -1 as a managerId means that all the remaining time are set.

In all the cases the lock is cleared only if it is set, this is to avoid to have in the PVSS log file 
a message issued by PVSS when a lock is cleared on a data point element which does not have a lock.

	@param iTimeOut: int, input, time to wait before deselecting the sDpElementName
	@param sManager: string, input, manager Id that requested the select
	@param sDpName: string, input, dp name to deselect

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. data point type needed: _UnSelectDeselect, _UnStatusInformation
	. the data point that has to be deselected must have a data point element of type _UnStatusInformation
	. global variable: the following variables are needed:
		. g_unSelectDeselectDpName: string
	. data point: the following data point is needed
		. systemName:_unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{Thread}
*/
_unSelectDeselect_thread(int iTimeOut, string sManager, string sDpName)
{
	int i, remainingTime;
	bool bLock;
	dyn_errClass error;
	string deviceName, sError;

//	DebugN(sDp, iTimeOut, sDpName);
	remainingTime = iTimeOut;
// every seconds the _unSelectDeselect.remaingTime is decremented
	for(i=0;i<iTimeOut;i++)
	{
		bLock = false;
		if(mappingHasKey(g_mDpNameList_lockState, sDpName)) {
			bLock = g_mDpNameList_lockState[sDpName];
		}
		if(!bLock) {
			i = iTimeOut;
		}
		else {
			dpSetWait(g_unSelectDeselectDpName+".remainingTime:_original.._value",sManager+";"+remainingTime);
			delay(1);
			remainingTime--;
		}
	}
// get the alias of the sDpName
	deviceName = unGenericDpFunctions_getAlias(sDpName);

// at the end of the timeOut the lock is released on sDpName
	dpSetWait(g_unSelectDeselectDpName+".remainingTime:_original.._value",sManager+";"+0);

// if the sDPName is into the mapping, the lock was reseted by someone else, do a dpDisconnect and remove the entry from the mapping
	if(mappingHasKey(g_mDpNameList_lockState, sDpName)) {
//DebugN("DPDisconnect");
// dpDisconnect if sDpName is in mapping
		dpDisconnect("_unSelectDeselect_LockStateCB", sDpName+".statusInformation.selectedManager:_lock._original._locked");
// remove from the mapping
		mappingRemove(g_mDpNameList_lockState, sDpName);
	}

	if(bLock) {
	// resetting the lock, if the lock is already released an error will be generated by the Event Manager,
	// this error is returned back.
		dpSetWait(sDpName+".statusInformation.selectedManager", "", 
				sDpName+".statusInformation.selectedManager:_lock._original._locked",false);
		error = getLastError();
		if(dynlen(error) > 0) {
			throwError(error);
			sprintf(sError, "%05d", getErrorCode(error));
			_unSelectDeselect_setMessage(sManager, "INFO", getCatStr("unSelectDeselect", "AUTODSELFAILED")+": "+
										 unSendMessage_getDeviceDescription(sDpName)+": "+getCatStr("_errors", sError));
		}
		else
			_unSelectDeselect_setMessage(sManager, "INFO", getCatStr("unSelectDeselect", "AUTODSELSUCCESS")+" "+
										 unSendMessage_getDeviceDescription(sDpName));
	}
	else
		_unSelectDeselect_setMessage(sManager, "INFO", getCatStr("unSelectDeselect", "AUTODSELSUCCESS")+" "+
									 unSendMessage_getDeviceDescription(sDpName));

//DebugN("Thread", g_mDpNameList_lockState);
// remove from the list the sManager and the sDpName
// indivisible function
	i = _unSelectDeselect_removeFromList(sDpName);
}

// _unSelectDeselect_killThread
/**
Purpose:
This function kills any existing thread, reset the remainingTime variable. The format is managerId;remainingTime before deselect, 
managerId is the id of the manager that requested the select, -1 as a managerId means that all the remaining time 
are set.

This function must be used as an indivisible function 
(e.g.: value = _unSelectDeselect_resetThreadId() )to implement a kind of exclusive 
write access.
The PVSS interpreter execute this function without interruption event. This function is 
used by the callback function, it can happen that two callback function will be issued 
(in fact it will be queued because there is on thread started per callback function and 
only the thread execute the callback).
Refer to the PVSS documentation for more detail on synchronising two threads

	@param iThreadId: int, input, identifier of the thread to stop
	@param sManager: string, input, identifier of manager that requested the select
	@param return value: int, iThreadId if iThreadId less than 0, -1 otherwise

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	This function must be used as an indivisible function 
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
*/
int _unSelectDeselect_killThread(string sManager, int iThreadId)
{
// if the iThreadId is >=0 kill the thread
	if(iThreadId >= 0) {
//		DebugN("stopThread");
// rst remainigntime
		stopThread(iThreadId);
		iThreadId = -1;
// the _unSelectDeselect.remainingTime is reseted.
// dpSetWait is not used because this function is an indivisible function and it not allowed 
// to have a blocking function
		dpSet(g_unSelectDeselectDpName+".remainingTime:_original.._value",sManager+";"+0);
	} //end iThreadId

	return iThreadId;
}

// _unSelectDeselect_LockStateCB
/**
Purpose:
call back on the lock of a dp. this function is called when the lock is removed.

	@param manager: string, input, the manager Id.
	@param sDpElementName: string, this is the data point element to be deselected by threadId, used to find the existing entry or add a new entry
	@param threadId: int, input, thread Id of the thread that will deselect sDpElementName
	@param return value: int, always 0

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unSelectDeselect_managerIdList: dyn_string
		. g_unSelectDeselect_dpElementSelected: dyn_string
		. g_unThreadId: dyn_int
	. data point: the following data point is needed
		. _unSelectDeselect
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{Callback}
*/
_unSelectDeselect_LockStateCB(string sDp, bool bLock)
{
	string sDpName = dpSubStr(sDp, DPSUB_SYS_DP);
	
	if(mappingHasKey(g_mDpNameList_lockState, sDpName))
		g_mDpNameList_lockState[sDpName] = bLock;

//DebugN("_unSelectDeselect_LockStateCB", g_mDpNameList_lockState);
}

//@}

