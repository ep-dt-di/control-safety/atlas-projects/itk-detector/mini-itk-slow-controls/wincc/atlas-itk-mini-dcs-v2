/**@name SCRIPT: unSystemIntegrity.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 01/03/2003

Modification History:
  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  
  23/09/2009: Herve
    - add creation of the sceduler for the unSystemIntegrity_computeStatistics if not already existing
    
	26/09/2005: Frederic
		add unGenericDpFunctions_setHostName() in order to save the hostname	

	16/04/2004: Herve
		new version of the systemIntegrity, based on component of type comp_systemIntegrity, the script 
			-gets the list of components,
			-gets the list of enabled device to check
			-set up the callback for the systemIntegrity command
		all the job previously done by systemIntrgity is done by the lib for each component.

	15/04/2004: Frederic
		add checking man_Num for Logging (Start a Thread by Logging Manager)
	
	30/03/2004: Herve
		- in function _unSystemIntegrity_Logging_SetArchiveCheck: remove the stopThread from the loop and put it outside
		if the threadId exists so do dpDisconnect, it it does not so do nothing (there was no dpConnect before, this will be the 
		case at startup with LHCLogging disabled)
		- in function _unSystemIntegrity_set_LHCLogging_checking: add the check if dp_alarm in global list before dpDisconnect and dpConnect
		this global variable is set after. This avoid to do dpDisconenct if there was no dpConenct before (case startup and disabled) 
		and do twice dpConnect.
		- initialisation of the variable g_diLHCLoggingCritical_Alarm moved in the main
		- in _unSystemIntegrity_set_LHCLogging_checking: move the functions _unSystemIntegrity_Logging_SetArchiveCheck and 
		_unSystemIntegrity_Logging_SetManagerCheck into the if with dpConenct and dpDisconnect to avoid to call twice the functions.
		- in the function _unSystemIntegrity_set_LASER_checking: if the dp_alarm is in the list g_unSystemIntegrity_LASERList 
		then dpDisconnect and kill the thread in case of deregister and in case of register if dp_alarm is not in the list 
		dpConnect and start the thread.
				
	29/03/2004: Herve
		- in the function _unSystemIntegrity_PLCChecking: do not set everytime the _unSystemAlarm (comm state PLC->DS based on the counter). Do it 
		only when it is neccessary: when the state is different from the previous time. This is to avoid undesirable trigger of the widget and 
		faceplate callback because the animation depend on the value of the _unSystemAlarm state and enable state.
		- in the function _unSystemIntegrity_PLCChecking: remove the check on the driver state. This check is not useful because if the driver is not
		running then the counter will not be updated. Furthermore the _Mod_Plc.DrvNum is set to 0 when the driver stops 
		and set to driver num when the driver starts
		- remove the driver argument in the thread send_ip.

	05/03/2004: Frederic
		add checking for Laser
 
	02/03/2004: Herve
		for the DPE checking remove the stime check, set an alarm if the dpe value has not change

	24/02/2004: Herve
		replace exceptionInfo by exceptionInfoTemp in the function _unSystemIntegrity_Logging_SetArchiveCheck

	17/02/2004: Herve
		bug case of error mobus
		the unSystemAlarm value for the error on ModPlc Dp set to 11+error
			
	26/01/2004: Herve
		add check a DPE: if the time stamp was modified and the stime is false or if the dpe value vas modified then reset the alarm otherwise set it.
	
	22/01/2004: Herve
		add in the PLC DS checking the check of the state of the driver for the corresponding PLC.
	
	12/01/2004: Frederic
		add Timeout checking for LHClogging
	 
	19/12/2003: Herve
		merge Frederic and Herve development

	25/11/2003: Herve
		unPlc dpName use PLCName instead of PLC number
		
	29/10/2003: Frederic
		add code to check the state of logging (link with unLHCLogging.ctl)
	
	07/10/2003: Herve
		add checking if unicosObjects installed in order to not allow PLC checking
		add the function _unSystemIntegrity_registerFunction and modify the dist, archive and drv setup function
		add setup function for the api, logging and alarm.
	
	15/09/2003: Herve
		add synchro data&time of PLC.

version 2.0

Purpose: 
This script checks the integrity of a UNICOS PVSS system, it:
	 . checks to PLC-DS communication by mean of checking that a value from the PLC is periodically updated and its stime bit is 
	 not set. The result is put in _UnSystemAlarm_PLC_DS_plcDpName, plcDpName is the dpname of type _UnPlc
	 . checks the state of the declared archive manager. The result is put in _UnSystemAlarmy_archive_dpName, dpName is the data point 
	 name of the archive manager
	 . checks the state of the modbus driver. The result is put in _UnSystemAlarm_drv_dpName, dpName is the data point 
	 name of the driver manager
	 . checks the state of the distributed component dps, that's give the state of the remote systems. The result is put in 
	 _UnSystemAlarm_dist_dpName, dpName is the data point name of the distibutedComponent dp.
	 . synchronized periodically the data and time of the PLC
	
	The checking can be disabled/enabled, the enable bit of the corresponding _UnSystemAlarm DP is set to false/true. 
	New checking can also be added.
	
	The interface is via the command and the parameters dpe of the _UnSystemIntegrity.
	
	A diagnostic can be returned on the list of components that are checked.
	
	In the case of distributed system this script must run on each system.
	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable
	. this script must run on the same computer as the WCCOAMod driver receiving the data
	. data point type needed: _UnSystemIntegrity
	. data point: 
	. PVSS version: 2.12.1 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

// global declaration

//@{

// main
/**
Purpose:
This is the main of the script.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. no temporary data point, no graphical element
	. PVSS version: 2.12.1 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
main()
{
	dyn_string dsComponents, dsFunctions, dsResult, exceptionInfo;
	int i, len;
	bool bOk;
  string sMessage;
	
  g_SystemIntegrity_sSystemName = getSystemName();
  
	//save DS hostname in unApplication
	unGenericDpFunctions_setHostName();
	
  sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
          
	unSystemIntegrity_getComponents(dsComponents);
//DebugN(dsComponents);
  
  //add all required system integrity manager is they are not yet present:
//  addSystemIntegrityManagers();
  //find the manager number of unicos_scripts.lst:
  int currentManagerNumber = myManNum();
  dyn_string scripts;

//  fwInstallationManager_getScripts(currentManagerNumber, scripts, g_SystemIntegrity_sSystemName);
//DebugTN("getScripts gave", currentManagerNumber, scripts, g_SystemIntegrity_sSystemName);

  scripts=fwManager_getScriptsRunByCtrlMan(g_SystemIntegrity_sSystemName, currentManagerNumber, exceptionInfo);
  //DebugTN("getScripts gave", currentManagerNumber, scripts, g_SystemIntegrity_sSystemName);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo[1]+" in System Integrity: could not check if manager is running",exceptionInfo[2]);
  }

  // check if this script runs unSystemIntegrity.ctl at all...
  if (dynContains(scripts, "unSystemIntegrity.ctl")<=0) {
	DebugTN("unSystemIntegrity problem: manager "+currentManagerNumber+" does not execute unSystemIntegrity.ctl; aborting execution");
	return;
  }
  
  // we need to determine if this is run by the unicos_scripts.lst or through a dedicated manager.
  // There is no easy way to query this through the PMON, or retrieve it in any other way.
  // To do that we check the list of scripts executed by the manager,
  // and if it contains any script that starts with "un*", and also not the "unSystemIntegrity.ctl",
  // then we conclude that it is the unScripts.lst
  dynUnique(scripts); // remove multiple entries of startScript()
  int idx=dynContains(scripts,"unSystemIntegrity.ctl");
  if (idx) dynRemove(scripts,idx);
  dyn_string unScripts=dynPatternMatch("un*",scripts);

  if(dynlen(unScripts) >= 1 ) 
  {
    //this is the script running the unicos scripts, i.e. the default manager for system integrity. Let us fake that this manager is number 0.
    DebugTN("*************************************************************  Staring default unicos scripts **************************************");    
    currentManagerNumber = 0;
  }
    
	len = dynlen(dsComponents);
	for(i=1;i<=len;i++) {
    //Check if the component is to be run by this instance of the Ctrl Manager:
    int dedicatedManagerNum = 0;
    unSystemIntegrity_getComponentControlManagerNumber(dsComponents[i], dedicatedManagerNum, g_SystemIntegrity_sSystemName);
    if(currentManagerNumber != dedicatedManagerNum) 
    {
      //DebugN("INFO: System Integrity -> Ignoring SI function " + dsComponents[i] + " as it is to be excuted by Ctrl Manager number " + dedicatedManagerNum);
      continue;
    }
    
		bOk = false;
		unSystemIntegrity_getComponentsFunctions(dsComponents[i], dsFunctions);
//DebugN(dsComponents[i], dsFunctions);
		if(dynlen(dsFunctions) >= UN_SYSTEMINTEGRITY_MAX_FUNCTION) {
			if(dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_INITIALIZE] != "") {
				if(isFunctionDefined(dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_INITIALIZE])) {
					evalScript(dsResult, "dyn_string main()" +
											"{" +
												"dyn_string dsResult;"+
												dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_INITIALIZE]+"(dsResult);"+
												"return dsResult;" +
											"}",
											makeDynString());
		
		//DebugN("dpSet", dsComponents[i]);
					dpSet( dsComponents[i]+UN_SYSTEMINTEGRITY_EXTENSION+".interface.command", UN_SYSTEMINTEGRITY_ADD, 
							dsComponents[i]+UN_SYSTEMINTEGRITY_EXTENSION+".interface.parameters", dsResult);
				}
			}
//DebugN("register", dsComponents[i]);
			if(dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_COMMAND] != "") {
				if(isFunctionDefined(dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_COMMAND])) {
					_systemIntegrity_registerComponentCommand(dsComponents[i], dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_COMMAND]);
					bOk = true;
				}
			}
			
//DebugN("data", dsComponents[i]);
			if(dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_DATA] != "") {
				if(isFunctionDefined(dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_DATA])) {
					_systemIntegrity_registerComponentData(dsComponents[i], dsFunctions[UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_DATA]);
					bOk = true;
				}
			}
			if(bOk) {
				if(isFunctionDefined("unMessageText_send")) {
					unMessageText_send("*", "*", g_SystemIntegrity_sSystemName+"systemIntegrity", "user", "*", "INFO", 
								getCatStr("unSystemIntegrity", "STARTED") + dsComponents[i], exceptionInfo);
				}
				DebugN(getCurrentTime(), "----------"+ getCatStr("unSystemIntegrity", "STARTED") + dsComponents[i]);
				if(dynlen(exceptionInfo) > 0)
					DebugN(exceptionInfo);
			}
		}

	}
        if(!dpExists(SYSTEMINTEGRITY_COMPUTESTATISTICS+".time.timedFunc.interval"))
          _systemIntegrity_createComputeStats();
  delay(0, 10);
  if(isFunctionDefined("unMessageText_send"))
    unMessageText_send("*", "*", g_SystemIntegrity_sSystemName+"systemIntegrity", "user", "*", "INFO", "systemIntegrity "+sMessage+" loaded", exceptionInfo);
}

// _systemIntegrity_registerComponentCommand
/**
Purpose:
register a given component to handle the systemIntegrity command.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. no temporary data point, no graphical element
	. PVSS version: 2.12.1 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_systemIntegrity_registerComponentCommand(string sComponent, string sFunction)
{
	dpConnect(sFunction, sComponent+UN_SYSTEMINTEGRITY_EXTENSION+".interface.command", 
						sComponent+UN_SYSTEMINTEGRITY_EXTENSION+".interface.parameters");
}

// _systemIntegrity_registerComponentData
/**
Purpose:
register a given component to handle the systemIntegrity data.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. no temporary data point, no graphical element
	. PVSS version: 2.12.1 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_systemIntegrity_registerComponentData(string sComponent, string sFunction)
{
	dpConnect(sFunction, sComponent+UN_SYSTEMINTEGRITY_EXTENSION+".config.data");
}

// _systemIntegrity_createComputeStats
/**
Purpose:
This function creates the scheduler for the computerStats.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 Sp2 
  . operating system: WXP and Linux.
*/
_systemIntegrity_createComputeStats()
{
  int iEr;
  
  _unSystemIntegrity_createScriptScheduler(SYSTEMINTEGRITY_COMPUTESTATISTICS, iEr, SYSTEMINTEGRITY_COMPUTESTATISTICS+".ctl", 86400);
  _unSystemIntegrity_startScheduler(SYSTEMINTEGRITY_COMPUTESTATISTICS, SYSTEMINTEGRITY_COMPUTESTATISTICS);
}

/*
int addSystemIntegrityManagers()
{
  dyn_string dpes = dpNames("*_systemIntegrityInfo.config.dedicatedControlManager", "_UnSystemIntegrity");
  if(dynlen(dpes) <= 0)
  {
    return 0; //nothing to be done
  }
  
  dyn_int managerNums;
  dpGet(dpes, managerNums);
  dynUnique(managerNums);
  
  dynRemove(managerNums, dynContains(managerNums, 0));
  int n = dynlen(managerNums);
  
  for(int i = 1; i <= n; i++)
  {
    if(!dpExists("_CtrlDebug_CTRL_" + managerNums[i]))
    {
      dpCreate("_CtrlDebug_CTRL_" + managerNums[i], "_CtrlDebug");
    }    
    fwInstallationManager_add("WCCOActrl", "always", 2, 2, 30, "unSystemIntegrity.ctl -num " + managerNums[i]);
  }
  
  return 0;
}


*/


//@}


