﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixConfigGenericFunctions.ctl" concerns="0">
	<header><![CDATA[/**@file

// TAG_prefixConfigGenericFunctions.ctl
This library contains the generic function for the configuration of the package.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<function name="TAG_prefixConfigGenericFunctions_getArchiveNames">
			<header><![CDATA[
/** Get Archives names (Bool, Analog and Event)
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the device dp
@param sDeviceType input, the device type
@param dsNames output, the archive names (Bool, Analog and Event)
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigGenericFunctions_getArchiveNames(string sDp, string sDeviceType, dyn_string &dsNames)
{
  int i, j, iLen, iLen_j;
  string sArchiveClass, sName;
  dyn_string dsDpe, dsTemp, dsCheck;       
    
  switch(sDeviceType)
  {
    case "TAG_DeviceType":
      // put the DPE for the Bool archive, e.g.: ".ProcessInput...."+ DELIMITER
      dynAppend(dsDpe, UN_CONFIG_EXPORT_UNDEF);
      // put the DPE for the Ana archive, e.g.: ".ProcessInput...."+ DELIMITER
      dynAppend(dsDpe, UN_CONFIG_EXPORT_UNDEF);
      // put the DPE for the Event archive, e.g.: ".ProcessInput...."+ DELIMITER
      dynAppend(dsDpe, UN_CONFIG_EXPORT_UNDEF);      
      break;
    default:
      dynAppend(dsDpe, UN_CONFIG_EXPORT_UNDEF);
      dynAppend(dsDpe, UN_CONFIG_EXPORT_UNDEF);      
      dynAppend(dsDpe, UN_CONFIG_EXPORT_UNDEF);      
      break;
  }
  iLen=dynlen(dsDpe);
  for (i=1; i<=iLen; i++)
  {
    if (dsDpe[i]!=UN_CONFIG_EXPORT_UNDEF)
    {
      dynClear(dsTemp);
      dynClear(dsCheck);
        
      dsTemp=strsplit(dsDpe[i], DELIMITER);
      iLen_j=dynlen(dsTemp);
        
      for (j=1; j<=iLen_j; j++)
      {
        sName="";
        sArchiveClass="";
          
        dpGet(sDp + dsTemp[j] + ":_archive.1._class", sArchiveClass);
    
        if(dpExists(sArchiveClass + ".general.arName"))
           dpGet(sArchiveClass + ".general.arName:_online.._value", sName);
         else
         {
           if (i==1)
            sName=UN_CONFIG_EXPORT_ARCHIVE_1;
          else if (i==2)
            sName=UN_CONFIG_EXPORT_ARCHIVE_2;
          else if (i==3)
            sName=UN_CONFIG_EXPORT_ARCHIVE_3;
          else
            sName=UN_CONFIG_EXPORT_UNDEF;  
         }
           
         dynAppend(dsCheck, sName);  
      }
        
      dynUnique(dsCheck);
        
      if (dynlen(dsCheck)==1)    //check if same archive for same type
        sName=dsCheck[1];  
      else
        sName=UN_CONFIG_EXPORT_UNDEF;  
    }
    else    //default
    {
      if (i==1)
        sName=UN_CONFIG_EXPORT_ARCHIVE_1;
      else if (i==2)
        sName=UN_CONFIG_EXPORT_ARCHIVE_2;
      else if (i==3)
        sName=UN_CONFIG_EXPORT_ARCHIVE_3;
      else
        sName=UN_CONFIG_EXPORT_UNDEF;  
    }
    dynAppend(dsNames, sName);  
  }
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>