﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixConfigTAG_DeviceType.ctl" concerns="2" browseSuperTypes="true">
	<header><![CDATA[/**@file

// TAG_prefixConfigTAG_DeviceType.ctl
This library contains the function to configure the TAG_DeviceType.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<function name="TAG_prefixConfigTAG_DeviceType_checkConfig">
			<header><![CDATA[
/** check the device configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_DeviceType_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  if (dynlen(dsConfigs) >= UN_CONFIG_COMMON_LENGTH)
  {
  // 1. Common
    unConfigGenericFunctions_checkParameters(dsConfigs, "TAG_DeviceType", exceptionInfo);
  // 2. call the device check function
    unConfigGenericFunctions_checkDeviceConfig(dsConfigs, "TAG_DeviceType", exceptionInfo);
  }
  else
  {
    fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigConfigTAG_DeviceType_checkConfig: " + getCatStr("unGeneration","BADINPUTS"),"");
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_DeviceType_setConfig">
			<header><![CDATA[
/** set the device configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs  input, config line as dyn_string
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_DeviceType_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string sDpName;
  
// 1. Initialization : checking + dpCreate
  unConfigGenericFunctions_checkAll(dsConfigs, "TAG_DeviceType", exceptionInfo);
  if (dynlen(exceptionInfo) <= 0)    // Good inputs
  {
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    unConfigGenericFunctions_createDp(sDpName, "TAG_DeviceType", exceptionInfo);
    if (dynlen(exceptionInfo) <= 0)  // Creation ok or dp already exists
    {
// 2. Generate device
// 2.1. Parameters
      unConfigGenericFunctions_setCommon(dsConfigs, exceptionInfo);
  // 2.2 call the device check function
      unConfigGenericFunctions_setDeviceConfig(dsConfigs, "TAG_DeviceType", exceptionInfo);
    }    
  }
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_TAG_DeviceType_checkConfig" duplicate="true">
			<header><![CDATA[
/** check the TAG_DeviceType device configuration for the TAG_FrontEndType front-end
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_FrontEndType_TAG_DeviceType_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  bool bOk=false;
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType_TAG_DeviceType", "TAG_FrontEndType_TAG_DeviceType_checkConfig", dsConfigs);
  if (dynlen(dsConfigs) == (TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_COMMON_LENGTH))
    bOk = true;
  else if (dynlen(dsConfigs) == (TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH)) {
    bOk = true;
    unConfigGenericFunctions_checkArchive(dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL],
                                          dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG],
                                          dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT],
                                          exceptionInfo);
  }
  else
    bOk = false;
  
  if(bOk) {
    // check all the device configuration
    // if the archive config is defined check if the archive config is Ok 
    unConfigGenericFunctions_checkArchiveParameters(0, dsConfigs[UN_CONFIG_COMMON_LENGTH + TAG_PREFIX_CONFIG_TAG_DEVICETYPE_ARCHIVEACTIVE], exceptionInfo);
  }
  else
  {
    fwException_raise(exceptionInfo,"ERROR","TAG_FrontEndType_TAG_DeviceType_checkConfig: " + getCatStr("unGeneration","BADINPUTS"),"");
  }
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_TAG_DeviceType_setConfig" duplicate="true">
			<header><![CDATA[
/** set the TAG_DeviceType device configuration for the TAG_FrontEndType front-end
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_FrontEndType_TAG_DeviceType_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string sDpName;
  bool bArchive;
  int iArchiveTimeFilter, iArchiveType, iSmoothProcedure;
  float fDeadband;
    
  sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType_TAG_DeviceType "+sDpName, "TAG_FrontEndType_TAG_DeviceType_setConfig", dsConfigs);
// if archive name are in the config->overwrite panel setting.  
  if(dynlen(dsConfigs) == (TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_LENGTH)) {
    if(dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL] != "")
      dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL] = dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL];

    if(dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG] != "")
      dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG] = dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG];

    if(dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT] != "")
      dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT] = dsConfigs[TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH + UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT];
  }
  /* set all the device config that have to be set:
       -DPE alias: use unConfigGenericFunctions_setAlias
       -DPE description: use unConfigGenericFunctions_setDescription
       -periph address: use JCOP function
       -pv_range: use JCOP function
       -alarm:
       -archive:
// get archive config
  unConfigGenericFunctions_getArchiving(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + TAG_PREFIX_CONFIG_TAG_DEVICETYPE_ARCHIVEACTIVE], 
                                        bArchive, iArchiveTimeFilter,  iArchiveType, iSmoothProcedure);
  unConfigGenericFunctions_getArchivingDeadband(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + TAG_PREFIX_CONFIG_TAG_DEVICETYPE_ARCHIVEACTIVE], fDeadband);
// set archive config
  if(bArchive)
    fwArchive_set(sDpName + "...", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL], 
                  iArchiveType, iSmoothProcedure, fDeadband, iArchiveTimeFilter,exceptionInfo);
  else
    fwArchive_delete(sDpName + "...", exceptionInfo);
       -DPE value
       -proxy: create the proxy DP, set the proxy configuration, DPE config, etc.
       -set the faceplate limits for the DPE in faceplate: use unGenericDpFunctions_setFaceplateRange
       -etc.
  */
  
  // get archive config
  unConfigGenericFunctions_getArchiving(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + TAG_PREFIX_CONFIG_TAG_DEVICETYPE_ARCHIVEACTIVE], 
                                        bArchive, iArchiveTimeFilter,  iArchiveType, iSmoothProcedure);
  unConfigGenericFunctions_getArchivingDeadband(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + TAG_PREFIX_CONFIG_TAG_DEVICETYPE_ARCHIVEACTIVE], fDeadband);
	// set archive config
  if(bArchive)
    fwArchive_set(sDpName + ".ProcessInput.iData", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL], 
                  iArchiveType, iSmoothProcedure, fDeadband, iArchiveTimeFilter,exceptionInfo);
  else
    fwArchive_delete(sDpName + ".ProcessInput.iData", exceptionInfo);
	
	// JCOP setUnit(.ProcessInput.iData, UN_CONFIG_ALLCOMMON_LENGTH + TAG_PREFIX_CONFIG_TAG_DEVICETYPE_FIELD)
  fwUnit_set(sDpName + ".ProcessInput.iData", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + TAG_PREFIX_CONFIG_TAG_DEVICETYPE_FIELD], exceptionInfo);
}]]>
			</content>
		</function>
		<function name="TAG_DeviceType_getArchiveDPE">
			<header><![CDATA[
/** return the list of TAG_DeviceType DPE per archive (Bool, Ana, Event)
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@return output, list of DPE
*/]]>
			</header>
			<content><![CDATA[
/* Comment this function if you want to use the default setting: 
   Bool archive: bool, char DPE
   Ana archive: float DPE, int and uint DPE except evStsReg*
   Event archive: evStsReg* int and uint, struct DPE
*/
dyn_string TAG_DeviceType_getArchiveDPE()
{
  dyn_string dsRet;
  dyn_string dsArchiveElements, exceptionInfo, dsDpel;
  dyn_int diDpelType;
  int i, len, j, k, length, lengthk, pos;
  string archiveBoolNames, archiveEventNames, archiveAnalogNames;
  string sDeviceType="TAG_DeviceType";
  
//  dsRet[1] = ".ProcessInput.DPE1"+ ";"+".ProcessInput.DPE2";
//  dsRet[2] = archiveAnalogNames;
//  dsRet[3] = archiveEventNames;
  
  // the following code can also be used
  // in this example:
  // Bool archive: bool, char DPE
  // Ana archive: float DPE, blob DPE, all int DPE except evStsReg* 
  // Event archive: evStsReg* int, uint, struct DPE
     
  // get all the DPE having an archive config from the JCOP device definition
  fwDevice_getArchiveElements(sDeviceType, dsArchiveElements, exceptionInfo);
  // get all the DPE from the device type
  unGenericDpFunctions_getDPE(sDeviceType, dsDpel, diDpelType);
  
  length = dynlen(dsArchiveElements);
  lengthk = dynlen(dsDpel);
//DebugN(length, lengthk);
  for(j=1;j<=length;j++)
  {
    for(k=1;k<=lengthk;k++) {
//DebugN(dsDpel[k], dsArchiveElements[j]);
      if((sDeviceType+dsArchiveElements[j]) == dsDpel[k]) {
        switch(diDpelType[k]){
          case DPEL_CHAR:
          case DPEL_BOOL:
            archiveBoolNames = archiveBoolNames + dsArchiveElements[j] + ";";
            break;
          case DPEL_FLOAT:
            archiveAnalogNames = archiveAnalogNames + dsArchiveElements[j] + ";";
            break;
          case DPEL_INT:
            // if DPE like evStsReg --> put it in event archive
            if (patternMatch(".ProcessInput.evStsReg*", dsArchiveElements[j]))
            {
              archiveEventNames = archiveEventNames + dsArchiveElements[j] + ";";
            }
            else
            {
              archiveAnalogNames = archiveAnalogNames + dsArchiveElements[j] + ";";
            }
            break;
          case DPEL_UINT:
          case DPEL_BOOL_STRUCT:
            archiveEventNames = archiveEventNames + dsArchiveElements[j] + ";";
            break;
          case DPEL_BLOB:
            archiveAnalogNames = archiveAnalogNames + dsArchiveElements[j] + ";";
            break;
        }
      }
    }
  }
  dsRet[1] = archiveBoolNames;
  dsRet[2] = archiveAnalogNames;
  dsRet[3] = archiveEventNames;
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_DeviceType", "TAG_DeviceType_getArchiveDPE", dsRet);
  return dsRet;
}]]>
			</content>
		</function>
		<function name="TAG_DeviceType_getArchiveProxyDPE">
			<header><![CDATA[
/** return a dyn_dyn_string with the list of proxy DPE linked (or that will be linked) 
to the device for the three archives: boolean, analog and event

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigLine input, the device configuration
@return the list of DPE per archive (3)
*/]]>
			</header>
			<content><![CDATA[
dyn_dyn_string TAG_DeviceType_getArchiveProxyDPE(dyn_string dsConfigLine)
{
  dyn_dyn_string dds;
  
  //return here for the boolean archive the list of proxy DPEs linked to the device
  dds[UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL] = makeDynString();
  //return here for the analog archive the list of proxy DPEs linked to the device
  dds[UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG] = makeDynString();
  //return here for the event archive the list of proxy DPEs linked to the device
  dds[UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT] = makeDynString();
  
  return dds;
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>