﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixConfigTAG_FrontEndType.ctl" concerns="3">
	<header><![CDATA[/**@file

// TAG_prefixConfigTAG_FrontEndType.ctl
This library contains the function to configure the TAG_FrontEndType.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<constant name="">
			<comment><![CDATA[
// put here any constant if not in the constant declaration file]]>
			</comment>
			<content></content>
		</constant>
		<function name="TAG_prefixConfigTAG_FrontEndType_checkDeleteCommand">
			<header><![CDATA[
/** check the content of the delete command
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, config line as dyn_string
@param dsDeleteDps output, list of dp that will be deleted
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo)
{
  
  string sFeName, sFESubApplication = "", sDeviceType = "", sNumber = "", sDpFEName;
  dyn_string dsAlarmDps;
  
  if (dynlen(dsConfig) >= 2)        // At least, delete instruction and front-end name
  {
// get FE Name
    sFeName = dsConfig[2];
    if ((sFeName != "") && (dsConfig[1] == UN_DELETE_COMMAND))
    {
      if (dynlen(dsConfig) >= 3)
      {
// get Application
        sFESubApplication = dsConfig[3];
      }
      if (dynlen(dsConfig) >= 4)
      {
// get DeviceType
        sDeviceType = dsConfig[4];
      }
      if (dynlen(dsConfig) >= 5)
      {
// get device number
        sNumber = dsConfig[5];
      }
      unGenericDpFunctions_getPlcDevices("", sFeName, sFESubApplication, sDeviceType, sNumber, dsDeleteDps);
// if only delete key word then delete also modbus and systemalarm
      if ((sFeName != "") && (sFESubApplication == "") && (sDeviceType == "") && (sNumber == ""))
      {
// sDpFe is TAG_FrontEndType_feName
        sDpFEName = c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE+sFeName;
        if (dpExists(sDpFEName))
        {
          // add here any other DP tp delete
          dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpFEName, strpos(sDpFEName, ":") + 1) + "*",c_unSystemAlarm_dpType);
          dynAppend(dsDeleteDps, dsAlarmDps);
        }
      }
    }
    else
    {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
    }
  }
  else
  {
    fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkDeleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType "+sFeName, "TAG_prefixConfigTAG_FrontEndType_checkDeleteCommand", dsConfig, dsDeleteDps);
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_checkFrontEnd">
			<header><![CDATA[
/** check the content of the front-end
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sKeyWord  input, keyword
@param dsConfig input, config of the front-end
@param dsDeleteDps input, the list of dp to delete
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_checkFrontEnd(string sKeyWord, dyn_string dsConfig, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
  switch(sKeyWord) {
    case UN_PLC_COMMAND:
      TAG_prefixConfigTAG_FrontEndType_checkApplication(dsConfig, dsDeleteDps, exceptionInfo);
      break;
    case UN_FESYSTEMALARM_COMMAND:
//      unConfigGenericFunctions_checkFESystemAlarm(dsConfig, exceptionInfo);
      break;
    default:
       fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_deleteCommand">
			<header><![CDATA[
/** execute the delete command
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig input, config of the front-end
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo)
{
  string sFEApplication = "", sDeviceType = "", sNumber = "";
  dyn_string dsDpeList, exceptionInfoTemp;
  
  TAG_prefixConfigTAG_FrontEndType_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
  if (dynlen(exceptionInfoTemp) > 0)
  {
    dynAppend(exceptionInfo, exceptionInfoTemp);
    fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_deleteCommand:" + getCatStr("unGeneration","BADINPUTS"),"");
  }
  else
  {
    if (dynlen(dsConfig) >= 2)        // At least, delete instruction and fe name
    {
      if (dynlen(dsConfig) >= 3)
      {
        sFEApplication = dsConfig[3];
      }
      if (dynlen(dsConfig) >= 4)
      {
        sDeviceType = dsConfig[4];
      }
      if (dynlen(dsConfig) >= 5)
      {
        sNumber = dsConfig[5];
      }
        
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType "+dsConfig[2], "TAG_prefixConfigTAG_FrontEndType_deleteCommand", dsConfig);
      unGenericDpFunctions_deletePlcDevices(dsConfig[2], sFEApplication, sDeviceType, sNumber, "progressBar", exceptionInfo);
      if ((dsConfig[2] != "") && (sFEApplication == "") && (sDeviceType == "") && (sNumber == ""))
      {
        TAG_prefixConfigTAG_FrontEndType_deleteFrontEnd(dsConfig[2], exceptionInfo);
      }
    }
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_setFrontEnd">
			<header><![CDATA[
/** execute the set: set the front-end config
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sKeyWord  input, keyword
@param dsConfig  input, config line as dyn_string
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_setFrontEnd(string sKeyWord, dyn_string dsConfig, dyn_string &exceptionInfo)
{
  string sDeviceType, sFeName, sFeType, sAnalogArchive, sDpName;
  int iNumber, driver;
  bool bEvent16;
//  int iWorldFIP;

  switch(sKeyWord) {
    case UN_PLC_COMMAND:
      TAG_prefixConfigTAG_FrontEndType_setApplication(dsConfig, exceptionInfo);
      break;
    case UN_FESYSTEMALARM_COMMAND:
/*      if(dynlen(dsConfig) == UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_LENGTH) {
        sDeviceType = dsConfig[UN_TAG_FRONTENDTYPE_FESYSTEMALARM_DEVICENAME];
        sFeName = dsConfig[UN_TAG_FRONTENDTYPE_FESYSTEMALARM_PLCNAME];
        sFeType = dsConfig[UN_TAG_FRONTENDTYPE_FESYSTEMALARM_TYPE];
        iNumber = dsConfig[UN_TAG_FRONTENDTYPE_FESYSTEMALARM_PLCNUMBER];
        driver = dsConfig[UN_TAG_FRONTENDTYPE_FESYSTEMALARM_DRIVER];
        bEvent16 = dsConfig[UN_TAG_FRONTENDTYPE_FESYSTEMALARM_EVENT16];
        sAnalogArchive = dsConfig[UN_TAG_FRONTENDTYPE_FESYSTEMALARM_ARCHIVE];
        iWorldFIP = dsConfig[UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_ADDRESS];

        unConfigGenericFunctions_setFESystemAlarm(dsConfig, makeDynString(sDeviceType, sFeName, sFeType, iNumber, driver, bEvent16, sAnalogArchive), exceptionInfo);
        sDpName = c_unSystemAlarm_dpPattern + FE_pattern + sDeviceType + ".";
        unConfigGenericFunctions_setDescription(sDpName, iWorldFIP, exceptionInfo);
      }
      else
        fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_setFrontEnd: " + getCatStr("unGeneration","BADINPUTS"),"");
*/
      break;
    default:
      fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","UNKNOWNFUNCTION"), "");
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_checkApplication">
			<header><![CDATA[
/** check the content of the front-end config
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig input, config of the front-end + additional parameters (driver number, boolean archive name, analog archive name)
@param dsDeleteDps input, the list of dp to delete
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_checkApplication(dyn_string dsConfig, dyn_string dsDeleteDps, dyn_string &exceptionInfo)
{
  dyn_string dsConfigLine, dsConfigAdditionalData;
  int i, iRes, iDriverNum;
  string sFeDP, sHostname, sAliasDp, tempDp;
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType "+dsConfig[UN_CONFIG_PLC_NAME], "TAG_prefixConfigTAG_FrontEndType_checkApplication", dsConfig);
  if (dynlen(dsConfig) == (UN_CONFIG_TAG_FRONTENDTYPE_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
    {
    dsConfigLine = dsConfig;
    for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
      {
      dynAppend(dsConfigAdditionalData, dsConfigLine[UN_CONFIG_TAG_FRONTENDTYPE_LENGTH+1]);
      dynRemove(dsConfigLine,UN_CONFIG_TAG_FRONTENDTYPE_LENGTH+1);
      }
    TAG_prefixConfigTAG_FrontEndType_checkData(dsConfigLine, exceptionInfo);
    TAG_prefixConfigTAG_FrontEndType_checkAdditionalData(dsConfigAdditionalData, exceptionInfo);

// first check if the FE name is correct:
    tempDp = dsConfigLine[UN_CONFIG_PLC_NAME];
    if(!unConfigGenericFunctions_nameCheck(tempDp)) {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkApplication: " + dsConfigLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADPLCNAME"),"");
      return;
    }

// check if the FE Application Name is correct:
    tempDp = dsConfigLine[UN_CONFIG_PLC_SUBAPPLICATION];
    if(!unConfigGenericFunctions_nameCheck(tempDp)) {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkApplication: " + dsConfigLine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
      return;
    }

// check if the front-end configurations are correct

// sFEDp is TAG_FrontEndType_feName
    sFeDP = getSystemName() +  c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE + dsConfigLine[UN_CONFIG_PLC_NAME];
//    sAliasDp = dpAliasToName(dsConfigLine[UN_CONFIG_PLC_NAME]);
    sAliasDp = unGenericDpFunctions_dpAliasToName(dsConfigLine[UN_CONFIG_PLC_NAME]);
//DebugN(sAliasDp, dsConfigLine[UN_CONFIG_PLC_NAME]);

    if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".")
    {
      sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);
    }
    if ((sAliasDp != "") && (sAliasDp != sFeDP))
    {
      if (dynContains(dsDeleteDps, sAliasDp) <= 0)    // Alias exists, is not used by the "TAG_FrontEndType" config datapoint and will not be deleted
      {
        fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkApplication: " + dsConfigLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration","BADHOSTNAMEALIAS") + sAliasDp,"");
      }
    }
    if (dpExists(sFeDP) && (dynContains(dsDeleteDps, sFeDP) <= 0))
    {
      sHostname = unGenericDpFunctions_getAlias(sFeDP);
      iRes = dpGet(sFeDP + ".communication.driver_num", iDriverNum);
      if (iRes >= 0)
      {
        if (iDriverNum != dsConfigAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM])
        {
          fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCDRIVER"),"");
        }
        if (sHostname != dsConfigLine[UN_CONFIG_PLC_NAME])
        {
          fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkApplication: " + getCatStr("unGeneration","BADEXISTINGPLCHOSTNAME"),"");
        }
      }
    }
  }
  else
  {
    fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkApplication: " + getCatStr("unGeneration","BADINPUTS"),"");
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_checkData">
			<header><![CDATA[
/** check the front-end config
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig input, config of the front-end + additional parameters (driver number, boolean archive name, analog archive name)
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_checkData(dyn_string dsConfig, dyn_string &exceptionInfo)
{
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType "+dsConfig[UN_CONFIG_PLC_NAME], "TAG_prefixConfigTAG_FrontEndType_checkData", dsConfig);
  if (dynlen(dsConfig) != UN_CONFIG_TAG_FRONTENDTYPE_LENGTH)
  {
    fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkData:" + getCatStr("unGeneration","BADINPUTS"),"");
  }
  else
  {
    unConfigGenericFunctions_checkPLCType(dsConfig[UN_CONFIG_PLC_TYPE], exceptionInfo);
    if (strrtrim(dsConfig[UN_CONFIG_PLC_NAME]) == "")
    {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkData:" + getCatStr("unGeneration","ERRORPLCNAME"),"");
    }
    if (strrtrim(dsConfig[UN_CONFIG_PLC_SUBAPPLICATION]) == "")
    {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkData:" + getCatStr("unGeneration","ERRORSUBAPPLI"),"");
    }      
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_checkAdditionalData">
			<header><![CDATA[
/** check the front-end config
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsAdditionalConfig input, additional parameters (driver number, boolean archive name, analog archive name)
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_checkAdditionalData(dyn_string dsAdditionalConfig, dyn_string &exceptionInfo)
{
  unsigned uDriver;
  
  if (dynlen(dsAdditionalConfig) != UN_CONFIG_PLC_ADDITIONAL_LENGTH)
  {
    fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkAdditionalData:" + getCatStr("unGeneration","BADINPUTS"),"");
  }
  else
  {
    unConfigGenericFunctions_checkUnsigned(dsAdditionalConfig[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], uDriver, exceptionInfo);
    if (dsAdditionalConfig[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "")
    {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkAdditionalData: bool archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
    }
    if (dsAdditionalConfig[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "")
    {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkAdditionalData: analog archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
    }
    if (dsAdditionalConfig[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "")
    {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_checkAdditionalData: event archive" + getCatStr("unGeneration","ERRORARCHIVEEMPTY"),"");
    }
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_setApplication">
			<header><![CDATA[
/** check the front-end config
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigAndAdditionnal input, config of the front-end + additional parameters (driver number, boolean archive name, analog archive name)
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_setApplication(dyn_string dsConfigAndAdditionnal, dyn_string &exceptionInfo)
{
  int iRes, i, position, iDriverNumber;
  string sDpName, addressReference, tempDp, sSection, sDriverLine;
  dyn_string dsConfig, dsAdditionalParameters, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
  dyn_string dsEntry, dsEventArchives, dsConfigSection;
  
  // Check
  
  if (dynlen(dsConfigAndAdditionnal) == (UN_CONFIG_TAG_FRONTENDTYPE_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH))
  {
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType "+dsConfigAndAdditionnal[UN_CONFIG_PLC_NAME], "TAG_prefixConfigTAG_FrontEndType_setApplication", dsConfigAndAdditionnal);
  // 1. Separate config file info and additional parameters
    dsConfig = dsConfigAndAdditionnal;
    dynClear(dsAdditionalParameters);
    for(i=1;i<=UN_CONFIG_PLC_ADDITIONAL_LENGTH;i++)
    {
      dynAppend(dsAdditionalParameters, dsConfig[UN_CONFIG_TAG_FRONTENDTYPE_LENGTH+1]);
      dynRemove(dsConfig,UN_CONFIG_TAG_FRONTENDTYPE_LENGTH+1);
    }
  
  // check if the FE Application Name is correct:
    tempDp = dsConfigAndAdditionnal[UN_CONFIG_PLC_SUBAPPLICATION];
    if(!unConfigGenericFunctions_nameCheck(tempDp)) {
      fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_setApplication: " + dsConfigAndAdditionnal[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration","BADPLCAPPLICATIONNAME"),"");
      return;
    }
  
  // 3. Set FE configuration in TAG_FrontEndType
    // create the TAG_FrontEndType instance
    sDpName = c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE + dsConfig[UN_CONFIG_PLC_NAME];
    tempDp = sDpName;
    unConfigGenericFunctions_createDp(sDpName, TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) <= 0)
    {
      // set the device name (alias) and the appl, archive, time.
      sDpName = getSystemName() + sDpName;
      unConfigGenericFunctions_setAlias(sDpName, dsConfig[UN_CONFIG_PLC_NAME], exceptionInfo);
      iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
             sDpName + ".configuration.importTime", dsImportTimes,
             sDpName + ".configuration.archive_bool", dsBoolArchives,
             sDpName + ".configuration.archive_analog", dsAnalogArchives,
             sDpName + ".configuration.archive_event", dsEventArchives);
      position = dynContains(dsSubApplications, dsConfig[UN_CONFIG_PLC_SUBAPPLICATION]);
      if (position <= 0)
      {
        position = dynlen(dsSubApplications) + 1;
      }
      dsSubApplications[position] = dsConfig[UN_CONFIG_PLC_SUBAPPLICATION];
      dsImportTimes[position] = getCurrentTime();
      dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
      dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
      dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];
      iRes = dpSetWait(sDpName + ".configuration.importTime", dsImportTimes,
               sDpName + ".configuration.archive_bool", dsBoolArchives,
               sDpName + ".configuration.archive_analog", dsAnalogArchives,
               sDpName + ".configuration.archive_event", dsEventArchives,
               sDpName + ".configuration.subApplications",dsSubApplications,
               sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]);
      if (iRes < 0)
      {
        fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_setApplication: " + getCatStr("unPVSS","DPSETFAILED"),"");
      }  
      
      // create any other DP if needed 
      // unConfigGenericFunctions_createDp(...);
      
      // set the FE configuration
      // dpSetWait(sDpName+".configuration..", ...);

      // set the FE DPE
      
    // 4. Create _UnSystemAlarm device
      unSystemIntegrity_createSystemAlarm(tempDp,
                                          DS_pattern,
                                          getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsConfig[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          exceptionInfo);
      
      // set the archive config of the _UnSystemAlarm device
      fwArchive_set(c_unSystemAlarm_dpPattern+DS_pattern+tempDp+".alarm",
              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
              DPATTR_ARCH_PROC_SIMPLESM,
              DPATTR_COMPARE_OLD_NEW,
              0,
              0,
              exceptionInfo);
      
      // create any other _UnSystemAlarm device
      // unSystemIntegrity_createSystemAlarm(tempDp, pattern, ...);
      /* fwArchive_set(c_unSystemAlarm_dpPattern+pattern+tempDp+".alarm",
              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
              DPATTR_ARCH_PROC_SIMPLESM,
              DPATTR_COMPARE_OLD_NEW,
              0,
              0,
              exceptionInfo);*/

    // 5. set config file driver sdection if needed
      iDriverNumber = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM];
      iRes=fwInstallation_getSection("DriverType"+iDriverNumber, dsConfigSection);
      if(dynlen(dsConfigSection) <= 0) {
        // add in the section the data
        // set the section in the config file
        iRes = fwInstallation_setSection("DriverType"+iDriverNumber, dsConfigSection);
        // create any internal DP the driver may need
      }
      
      // create any internal DP the front-end may need
      // do any setting the the front-end may need

      // set address config on counter
      /*fwPeriphAddress_set(sDpName+".communication.counter", 
                                makeDynString(fwPeriphAddress_TYPE_?, 
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], 
                                              dsConfig[...],
                                              ....),
                                exceptionInfo);*/

      // set address config on any other fe DPE. 
      /*fwPeriphAddress_set(sDpName+"....", 
                                makeDynString(fwPeriphAddress_TYPE_?, 
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], 
                                              dsConfig[...],
                                              ....),
                                exceptionInfo);*/
      // set any other config
    }
    else
    {
      dynAppend(exceptionInfo, exceptionInfoTemp);
    }
  }
  else
  {
    fwException_raise(exceptionInfo,"ERROR","TAG_prefixConfigTAG_FrontEndType_setApplication: " + getCatStr("unGeneration","BADINPUTS"),"");
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixConfigTAG_FrontEndType_deleteFrontEnd">
			<header><![CDATA[
/** delete the front-end and its devices
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFeName input, the front-end name
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixConfigTAG_FrontEndType_deleteFrontEnd(string sFeName, dyn_string& exceptionInfo)
{
  string sDpFeName, exceptionText;
  int i, length, iRes;
  dyn_string dsAlarmDps;
  

  sDpFeName = c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE+sFeName;
  if (!dpExists(sDpFeName))
    sDpFeName = "";

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix import TAG_FrontEndType "+sFeName, "TAG_prefixConfigTAG_FrontEndType_deleteFrontEnd", sFeName, sDpFeName);
  if (sDpFeName != "")
  {
    if (unGenericDpFunctions_getSystemName(sDpFeName) == getSystemName())
    {
  // 1. delete any associated front-end DP
      // dpGet(sDpPlcName+"....", sDp);
      // dpDelete(sPlcModPlc);
        
  // 2. Delete TAG_FrontEndType
      iRes = dpDelete(substr(sDpFeName, strpos(sDpFeName, ":") + 1));
      if (iRes < 0)
      {
        fwException_raise(exceptionInfo, "ERROR", "TAG_prefixConfigTAG_FrontEndType_deleteFrontEnd: " + substr(sDpFeName, strpos(sDpFeName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
      }
  // 3. Delete system alarm devices 
      dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpFeName, strpos(sDpFeName, ":") + 1) + "*",c_unSystemAlarm_dpType);
      length = dynlen(dsAlarmDps);
      for(i=1;i<=length;i++)
      {
        // remove them first from the application DP list
        _unSystemIntegrity_modifyApplicationAlertList(dsAlarmDps[i], false, exceptionInfo);
        iRes = dpDelete(dsAlarmDps[i]);
        if (iRes < 0)
        {
          fwException_raise(exceptionInfo, "ERROR", "TAG_prefixConfigTAG_FrontEndType_deleteFrontEnd: " + dsAlarmDps[i] + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
        }
      }
      
    }
    else
    {
      fwException_raise(exceptionInfo, "ERROR", "TAG_prefixConfigTAG_FrontEndType_deleteFrontEnd: " + sDpFeName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
    }
  }
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_checkFESystemAlarm">
			<header><![CDATA[
/** check FE system alarm config line (called by unConfigGenericFunctions_checkFESystemAlarm)
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig input, the FE system alarm config
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_FrontEndType_checkFESystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo)
{
  if(dynlen(dsConfig) == UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_LENGTH) {
    // check here the content of the FE system alarm line
  }
  else
    fwException_raise(exceptionInfo,"ERROR","TAG_FrontEndType_checkFESystemAlarm:" + getCatStr("unGeneration","BADINPUTS"),"");
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_setFESystemAlarm">
			<header><![CDATA[
/** set FE system alarm config line (called by unConfigGenericFunctions_setFESystemAlarm)
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfig input, the FE system alarm config
@param dsAdditionalParameters input, additional parameter (FE systemalarm type, number, driver number, device name, front-end name, archive name)
@param dsAddress output, list of address config
*/]]>
			</header>
			<content><![CDATA[
TAG_FrontEndType_setFESystemAlarm(dyn_string dsConfig, dyn_string dsAdditionalParameters, dyn_string &dsAddress)
{
  int iDriverNum = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DRIVER];
  
  dsAddress = makeDynString(fwPeriphAddress_TYPE_, 
                            iDriverNum, 
                            dsConfig[UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_FIELD1]
//                            ....
  );
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_getFrontEndArchiveDp">
			<header><![CDATA[
/** return the list of front-end and all all other front-end dependant DPEs 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sKeyWord input, the keyword
@param sDp input, front-end DP
@param sFEType input, type of front-end
@param dsArchiveDp output, the list of front-end and all all other front-end dependant DPEs to be archived
*/]]>
			</header>
			<content><![CDATA[
TAG_FrontEndType_getFrontEndArchiveDp(string sKeyWord, string sDp, string sFEType, dyn_string &dsArchiveDp)
{
  switch(sKeyWord) {
    case UN_PLC_COMMAND:
      // return here the list of all the DPE which will have an _archive config during set front-end
      // with UN_PLC_COMMAND keyword
      // front-end DPE, system alarm DPE, etc.
      dsArchiveDp = makeDynString(getSystemName()+c_unSystemAlarm_dpPattern+DS_pattern+sDp+".alarm");
      break;
    case UN_FESYSTEMALARM_COMMAND:
      dsArchiveDp = makeDynString(sDp+".alarm");
      break;
    default:
      dsArchiveDp = makeDynString();
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_getArchiveProxyDPE">
			<header><![CDATA[
/** return a dyn_dyn_string with the list of proxy DPE linked (or that will be linked) 
to the front-end for the three archives: boolean, analog and event

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigLine input, the front-end configuration
@return the list of DPE per archive (3)
*/]]>
			</header>
			<content><![CDATA[
dyn_dyn_string TAG_FrontEndType_getArchiveProxyDPE(dyn_string dsConfigLine)
{
  dyn_dyn_string dds;
  
  //return here for the boolean archive the list of proxy DPEs linked to the front-end
  dds[UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL] = makeDynString();
  //return here for the analog archive the list of proxy DPEs linked to the front-end
  dds[UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG] = makeDynString();
  //return here for the event archive the list of proxy DPEs linked to the front-end
  dds[UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT] = makeDynString();
  
  return dds;
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_getFrontEndManagerConfig">
			<header><![CDATA[
/**
Purpose: return the driver config of TAG_FrontEndType front-end

Parameters:
		iDriverNumber: 		int, 		input, 	the driver number
		sName: 					string, 		output,	the name to display
		sDriverName: 			string, 		output, 	the manager name
		iManType: 	int, output, the manager type
		sCommandLine: 	int, output, the command line to start the manager

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP.
	. distributed system: yes.
*/]]>
			</header>
			<content><![CDATA[

TAG_FrontEndType_getFrontEndManagerConfig(int iDriverNumber, string &sName, string &sDriverName, int &iManType, string &sCommandLine)
{
  string sDriverNumber;
  
  sprintf(sDriverNumber, "%02d", iDriverNumber);
  sName = "TAG_FrontEndType";
  sDriverName = "PVSSDriver";
  iManType = DRIVER_MAN;
  sCommandLine = "-num "+iDriverNumber;
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>

