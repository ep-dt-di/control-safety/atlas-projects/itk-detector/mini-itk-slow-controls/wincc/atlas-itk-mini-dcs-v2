﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixConstantDeclarationsTAG_DeviceType.ctl" concerns="2">
    <header><![CDATA[/**@file

// TAG_prefixConstantDeclarationsTAG_DeviceType.ctl
This library contains the definition of the constants for the device type TAG_DeviceType.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
    </header>
    <elementList>
        <constant name="TAG_PREFIX_CONFIG_TAG_DEVICETYPE_ARCHIVEACTIVE">
            <comment noReplace="true"><![CDATA[
/*
The following is a concrete example of what you should put this file for a device. 
format: Device type;Device number (unique);Device name (Device Identifier);description;Diagnostic panel;HTML page;Default panel;subsystem1 (Domain);subsystem2 (Nature);Widget Name; [FIELDS] ;[[archive bool];[archive analog];[archive event];]
"[FIELDS]" are the non mandatory fields. You can add as many fields as you want before the archive config
This is the import data generated when creating the package :
TAG_DeviceType;1;TAG_FrontEndType_FE_1_TAG_DeviceType_Name;description;diag.pnl;www.site.com;default.pnl;SUBSYS1;SUBSYS2;TAG_WidgetType;A;10;

There are here 2 optional fields : "A" (string, archive active ("Y") or not ("N")) and "10" (random integer), thus length is 2. Declare constant with this value.
*/
]]>
            </comment>
            <content><![CDATA[
// Constant for the check/import of the DeviceType 
// Device config lenght without the mandatory field
const unsigned TAG_PREFIX_CONFIG_TAG_DEVICETYPE_LENGTH = 2;
// If the archive Y/N is configurable for the device, 
// if yes then the import takes the default configuration from the JCOP device definition
// if no then the import does not add the DPEs
// This constant is mandatory
const unsigned TAG_PREFIX_CONFIG_TAG_DEVICETYPE_ARCHIVEACTIVE	= 1; 
const unsigned TAG_PREFIX_CONFIG_TAG_DEVICETYPE_FIELD = 2;
]]>
            </content>
        </constant>
        <constant name="TAG_DEVICETYPE_FACEPLATE_VALUE1">
            <comment><![CDATA[// constant for faceplate]]></comment>
            <content><![CDATA[const int TAG_DEVICETYPE_FACEPLATE_VALUE1 = 10;]]></content>
        </constant>
        <constant name="TAG_DEVICETYPE_WIDGET_VALUE1">
            <comment><![CDATA[// constant for widget]]></comment>
            <content><![CDATA[const int TAG_DEVICETYPE_WIDGET_VALUE1 = 10;]]></content>
        </constant>
        <constant name="">
            <comment><![CDATA[// constant for device action interface]]></comment>
            <content></content>
        </constant>
    </elementList>
</ctrlScript>
            