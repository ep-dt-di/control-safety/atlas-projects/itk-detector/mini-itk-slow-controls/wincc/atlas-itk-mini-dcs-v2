﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixConstantDeclarationsTAG_FrontEndType.ctl" concerns="3" browseSubTypes="true">
    <header><![CDATA[/**@file

// TAG_prefixConstantDeclarationsTAG_FrontEndType.ctl
This library contains the definition of the constants for the front-end type TAG_FrontEndType.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
    </header>
    <elementList>
        <constant name="TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE">
            <comment><![CDATA[// constants for the check/import of the FrontEndType]]></comment>
            <content><![CDATA[const string TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE = "TAG_FrontEndType";]]></content>
        </constant>
        <constant name="TAG_FRONTENDTYPE_FELENGTH">
            <comment noReplace="true"><![CDATA[
/** 
ex: PLCCONFIG;TAG_FRONTENDTYPE;TAG_FRONTENDTYPE_FE1;FE1_APPL;FIELD1;FIEL2;FIELD3;FIELD4
// total length of the PLCONFIG line (dyn_string) withoout PLCONFIG
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_LENGTH = FELENGTH;
// put here below an index for each front-end config (not the 3 mandatory fields: front-end type, front-end instance, application)
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_FIELD1 = FELENGTH-3;
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_FIELD2 = FELENGTH-2;
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_FIELD3 = FELENGTH-1;
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_FIELD4 = FELENGTH;

The following is a concrete example of what you should put this file for a front-end. 
This is the import data generated when creating the package :
PLCCONFIG;TAG_FRONTENDTYPE;TAG_FRONTENDTYPE_FE1;FE1_APPL;FIELD
only one field (named FIELD) => FELENGTH = 3 mandatory fields + FIELD = 4
Declare 2 constant for this information
*/]]>
            </comment>
            <content><![CDATA[
const unsigned TAG_FRONTENDTYPE_FELENGTH = 4;
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_FIELD = 4;
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_LENGTH = 4;
]]>
            </content>
        </constant>
        <constant name="UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_LENGTH">
            <comment><![CDATA[// put here the FE system alarm const for check if needed]]></comment>
            <content><![CDATA[
const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_LENGTH = 1;	// x parameters
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_FRONTEND = 1;	// front-end type
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_NAME = 2;	// name
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_ADDRESS = 3;	// Address in front-end
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_OKALARM = 4;	// true=alarm if value>10, false (default) = alarm if value<=10
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_DESCRIPTION = 5;	// description
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_FIELD1 = 6;	// 
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_FIELD2 = 7;	// 
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_FIELD3 = 8;	// 
//const unsigned UN_TAG_FRONTENDTYPE_CHECKFESYSTEM_ALARM_FIELD4 = 9;	// 
            
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_LENGTH = y;	// y=x+7 parameters

//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_NAME = 1;	// name
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_ADDRESS = 2;	// Address in front-end
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_OKALARM = 3;	// true=alarm if value>10, false (default) = alarm if value<=10
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_DESCRIPTION = 4;	// description
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_FIELD1 = 5;	// 
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_FIELD2 = 6;	// 
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_FIELD3 = 7;	// 
//const unsigned UN_TAG_FRONTENDTYPE_SETFESYSTEM_ALARM_FIELD4 = 8;	// 

//const unsigned UN_TAG_FRONTENDTYPE_FESYSTEMALARM_DEVICENAME = 9;
//const unsigned UN_TAG_FRONTENDTYPE_FESYSTEMALARM_PLCNAME = 10;
//const unsigned UN_TAG_FRONTENDTYPE_FESYSTEMALARM_TYPE = 11;
//const unsigned UN_TAG_FRONTENDTYPE_FESYSTEMALARM_PLCNUMBER = 12;
//const unsigned UN_TAG_FRONTENDTYPE_FESYSTEMALARM_DRIVER = 13;
//const unsigned UN_TAG_FRONTENDTYPE_FESYSTEMALARM_EVENT16 = 14;
//const unsigned UN_TAG_FRONTENDTYPE_FESYSTEMALARM_ARCHIVE = 15;
]]>
            </content>
        </constant>
        <constant name="c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE">
            <comment><![CDATA[// constant for DQGTW system integrity]]></comment>
            <content><![CDATA[
const string c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE = "TAG_FrontEndType_";
const string UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE = "TAG_FrontEndType";
const string UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE_check = "TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheck";
const int c_TAG_prefixSystemIntegrity_defaultTAG_FRONTENDTYPECheckingDelay = 10; // default value for the checking in sec.
]]>
            </content>
        </constant>
        <constant duplicate="true" name="UN_CONFIG_TAG_FRONTENDTYPE_TAG_DEVICETYPE_LENGTH">
            <comment><![CDATA[// constant for the import database panel]]></comment>
            <content><![CDATA[
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_TAG_DEVICETYPE_ARCHIVE_ACTIVE = 1; // mandatory if the archive is parametrizable
const unsigned UN_CONFIG_TAG_FRONTENDTYPE_TAG_DEVICETYPE_LENGTH = 2;
]]>
            </content>
        </constant>
    </elementList>
</ctrlScript>
            