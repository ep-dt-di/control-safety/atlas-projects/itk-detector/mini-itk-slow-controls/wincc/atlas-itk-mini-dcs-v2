﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixConstant_declarations.ctl" concerns="0">
    <header><![CDATA[/**@file

// TAG_prefixConstant_declarations.ctl
This library contains the definition of the common constants.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
    </header>
    <elementList>
        <constant name="c_TAG_PREFIX_FRONTEND_UNSYSTEMALARM">
            <content><![CDATA[const string TAG_PREFIX_DPNAME_PREFIX = "TAG_prefix";]]></content>
        </constant>
        <constant name="c_TAG_PREFIX_FRONTEND_UNSYSTEMALARM">
            <content><![CDATA[const string c_TAG_PREFIX_FRONTEND_UNSYSTEMALARM = "_unSystemAlarm";]]></content>
        </constant>
        <constant name="">
            <comment><![CDATA[// constant for export]]></comment>
            <content></content>
        </constant>
        <constant name="">
            <comment><![CDATA[// any other constants]]></comment>
            <content></content>
        </constant>
        <constant name="">
            <comment><![CDATA[// put here the global variables]]></comment>
            <content></content>
        </constant>
    </elementList>
</ctrlScript>
            