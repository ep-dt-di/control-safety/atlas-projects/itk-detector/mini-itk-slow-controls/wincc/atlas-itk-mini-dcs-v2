﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixExportDevices.ctl" concerns="0">
	<header><![CDATA[/**@file

// TAG_prefixExportDevices.ctl
This library contains the device export functions.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<function name="TAG_prefixExportDevice_writeDeviceTypeLineFormat">
			<header><![CDATA[
/** returns the format of the config line for a device type
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceType input, device type
@param sCommand input, keyword (for front-end only)
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixExportDevice_writeDeviceTypeLineFormat(string sDeviceType, string sCommand)
{
  string sFormat, sMandatory, sDeviceConfig;
  
  sMandatory = sDeviceType+";Device number (unique);Device name (Device Identifier);description;Diagnostic panel;HTML page;Default panel;subsystem1 (Domain);subsystem2 (Nature);Widget Name";
  switch(sDeviceType) 
  {
	// To do : case
	/*
    case TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE:
      switch(sCommand)
      {
        case UN_PLC_COMMAND:
          sFormat = UN_PLC_COMMAND+";"+TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE+";Front-end device name;Front-end application;";
          sDeviceConfig = "";
          fputs("#format: " +sFormat + sDeviceConfig + "\n" , g_fileToExport);
          break;
        case UN_DELETE_COMMAND:
          sFormat =  UN_DELETE_COMMAND+";Front-end device name;Front-end application (or *);[Device type;[Device id;]]";
          fputs("#format: " +sFormat + "\n" , g_fileToExport);
          break;
        default:
          break;
      }
      break;
    */
    case UN_FESYSTEMALARM_COMMAND:
//        sFormat = UN_FESYSTEMALARM_COMMAND+";WorldFip number;Device Name (device Identifier);WorldFip number;alarm type (FALSE=alarm if value<10/TRUE=alarm if value>10);description;CMW Device;CMW property;CMW tag;CMW context filter;";
        fputs("#format: " +sFormat + "\n" , g_fileToExport);
        break;
	// To do : case
    case "TAG_DeviceType":
      sDeviceConfig =   "";
      sFormat = sMandatory+";"+sDeviceConfig;
      fputs("#format: " +sFormat + "\n" , g_fileToExport);
      break;
    default:
      fputs("#no format: \n" , g_fileToExport);
      break;
  }
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>
