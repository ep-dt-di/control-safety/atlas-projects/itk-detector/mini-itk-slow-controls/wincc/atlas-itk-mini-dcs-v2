﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixExportDevicesTAG_FrontEndType.ctl" concerns="3" browseSubTypes="true">
	<header><![CDATA[/**@file

// TAG_prefixExportDevicesTAG_FrontEndType.ctl
This library contains the export functions of the package for the front-end type TAG_FrontEndType.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<function name="TAG_FrontEndType_Com_ExportConfig">
			<header><![CDATA[
/** check the content of the delete command
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsParam  input, parameters from the export panel (FE, application, device type)
@param exceptionInfo output, for errors
*/]]>
			</header>
			<content><![CDATA[
TAG_FrontEndType_Com_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo)
{
  int iRes;
  string sPlcName, sPlcDrv;
  string sType, sDp, sCounterAddress;
  dyn_string dsDpParameters;
  dyn_string dsAnalogArchive, dsEventArchive, dsBoolArchive, dsSubApplications;
  dyn_string exInfo;
  int pos;

  if (dynlen(dsParam)==UN_CONFIG_EXPORT_LENGTH)
  {
    sPlcName = dsParam[UN_CONFIG_EXPORT_PLCNAME];
    sDp = c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE + sPlcName;
    iRes = dpGet(sDp + ".configuration.subApplications", dsSubApplications,
                  sDp + ".configuration.archive_bool", dsBoolArchive,
                  sDp + ".configuration.archive_analog", dsAnalogArchive,
                  sDp + ".configuration.archive_event", dsEventArchive,
                  sDp + ".communication.driver_num", sPlcDrv);
    TextFieldDriver.text = sPlcDrv;
  
    if (dynlen(dsBoolArchive)>0 && dynlen(dsAnalogArchive)>0 && dynlen(dsEventArchive)>0)
    {
      pos = dynContains(dsSubApplications, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
      if(pos >=1 ) 
      {
        TextFieldBoolArchive.text = dsBoolArchive[pos];
        TextFieldAnalogArchive.text = dsAnalogArchive[pos];
        TextFieldEventArchive.text = dsEventArchive[pos];
      }
      else
      {
        TextFieldBoolArchive.text = dsBoolArchive[1];
        TextFieldAnalogArchive.text = dsAnalogArchive[1];
        TextFieldEventArchive.text = dsEventArchive[1];
      }
    }

    //Delete
    dynClear(dsDpParameters);
    TAG_prefixExportDevice_writeDeviceTypeLineFormat(TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE, UN_DELETE_COMMAND);
    dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  
    //PLCCONFIG
    TAG_prefixExportDevice_writeDeviceTypeLineFormat(TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE, UN_PLC_COMMAND);
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, UN_PLC_COMMAND);
    dynAppend(dsDpParameters, TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);

    // get all the TAG_FrontEndType configuration
    // dpGet(...);
    
    // write to file
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

 // get the front-end system alarm
/*    fputs("#" + "\n" , g_fileToExport);
    dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + FE_pattern + c_TAG_prefixSystemIntegrity_TAG_FRONTENDTYPE + dsParam[UN_CONFIG_EXPORT_PLCNAME] + "_*", c_unSystemAlarm_dpType);

    iLen=dynlen(dsAlarmDps);
    if(iLen > 0) {
      TAG_prefixExportDevice_writeDeviceTypeLineFormat(UN_FESYSTEMALARM_COMMAND, "");
    }
    for(i=1; i<=iLen; i++)
    {
      if (dpExists(dsAlarmDps[i]))
      {
        sSplitModPlc = substr(dsAlarmDps[i], strpos(dsAlarmDps[i], dsParam[UN_CONFIG_EXPORT_PLCNAME]) + strlen(dsParam[UN_CONFIG_EXPORT_PLCNAME]) + 1);
        
// get the number (stored in the description)

        sNbr = unGenericDpFunctions_getDescription(dsAlarmDps[i]+".");
        unConfigGenericFunctions_checkUnsigned(sNbr, uNbr, exInfo);
//DebugN(sNbr, uNbr);
        if(uNbr > 0) {
          //Get Alarm Address
          dpGet(dsAlarmDps[i] + ".alarm:_address.._drv_ident" , sAlarmAddress);
          TAG_prefixConfigGenericFunctions_getAddressConfig(dsAlarmDps[i] + ".alarm", dsAddressDpParameters);

          //Get alarm type
          dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl.._type", sAlarmType);
          if(sAlarmType == DPCONFIG_ALERT_NONBINARYSIGNAL)
          {
            fwAlertConfig_getLimits(dsAlarmDps[i] + ".alarm", limitNumbers, alertLimits, exceptionInfo);
            alertRanges = (dynMax(limitNumbers) + 1);
            dsAlertText = makeDynString();
            for(j=1; j<=alertRanges; j++)
            {
              dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl." + j + "._text", sAlertText);
              dynAppend(dsAlertText, sAlertText);
            }
                                        
  //DebugN(dsAlertText);
            if(dynlen(dsAlertText) > 0)
            {
              if (dsAlertText[1]==UN_SYSTEMINTEGRITY_OK)
              {
                sType = "TRUE";
              }
              else
              {
                sType = "FALSE";
              }
            }
            else
              sType = UN_CONFIG_EXPORT_UNDEF;
          }
          else
            sType = UN_CONFIG_EXPORT_UNDEF;  
  
          dynClear(dsDpParameters);
          dynAppend(dsDpParameters, UN_FESYSTEMALARM_COMMAND);
          dynAppend(dsDpParameters, sNbr);                                                              //???? nbr
          dynAppend(dsDpParameters, sSplitModPlc);                                                  //identifier
          dynAppend(dsDpParameters, sNbr);                                                              //???? nbr
          dynAppend(dsDpParameters, sType);                                                          //alarm type
          dynAppend(dsDpParameters, unGenericDpFunctions_getDescription(dsAlarmDps[i] + ".alarm"));  //description
          dynAppend(dsDpParameters, dsAddressDpParameters);                                                  //address
  
          fputs("#For TAG_FrontEndType " + sNbr + "\n" , g_fileToExport);
  // write to file
          unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
        }
      }
    }
    */
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "TAG_FrontEndType_Com_ExportConfig(): Wrong Parameters", "");    
}]]>
			</content>
		</function>
		<function name="TAG_FrontEndType_TAG_DeviceType_ExportConfig" duplicate="true">
			<header><![CDATA[
/** returns the format of the config line for a TAG_DeviceType device type
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDpList input, list of device dp
@param exceptionInfo output, the errors are returned here
*/]]>
			</header>
			<content><![CDATA[
void TAG_FrontEndType_TAG_DeviceType_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  dyn_string dsAllCommonParameters, dsDpParameters, dsArchiveNames, dsAddressDpParameters;
  string sDeviceType, sArchiveActive, sArchiveName;
  float fArchiveTimeFilter;
  int i,iLeni, j, iLenj;
    
  sDeviceType="TAG_DeviceType";
  
  iLeni=dynlen(dsDpList);
  if(iLeni > 0) {
    TAG_prefixExportDevice_writeDeviceTypeLineFormat(sDeviceType, "");
  }
  for(i=1; i<=iLeni; i++)
  {                  
    if (dpExists(dsDpList[i]))
    {
      dynClear(dsAllCommonParameters);
      dynClear(dsDpParameters);
      dynClear(dsArchiveNames);
        
      //Get all common Parameters
      unExportDevice_getAllCommonParameters(dsDpList[i], sDeviceType ,  dsAllCommonParameters);
      dsDpParameters = dsAllCommonParameters;
          
      //Get the device configuration
	  /*
	  - archive config
      unExportDevice_getArchiveParametersDpe(dsDpList[i]+"...", sArchiveActive, fArchiveTimeFilter, "");
	  - address config
	  fwPeriphAddress_get(dsDpList[i] + "...", bCPUExist, daCPUCounterConfig, bIsActive, exceptionInfo);
	  - smoothing config
	  unExportDevice_fwSmoothing_getTemp(dsDpList[i] + "...", bIsSmoothDefined, iSmoothProcedure, fDeadbandTemp, fTimeInterval);
	  unExportDevice_getDeadBand(fDeadbandTemp, bIsSmoothDefined, sMax, sMin, iSmoothProcedure, fDeadband, iDeadBandType);
	  - pv_range
	  fwPvRange_get(dsDpList[i] + "...", bDoesExist , fMinValue , fMaxValue , bNegateRange , bIgnoreOutside , bInclusiveMin , bInclusiveMax , exceptionInfo);
	  - etc.
	  */
	  
      //Get Archive Names for TAG_PREFIX devices
      TAG_prefixConfigGenericFunctions_getArchiveNames(dsDpList[i], sDeviceType, dsArchiveNames);
      iLenj=dynlen(dsArchiveNames);
      for(j=1; j<=iLenj ; j++)
      {
        dynAppend(dsDpParameters, dsArchiveNames[j]);
      }  
      // write the data to the file
      unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }//dp doesn't exist
  }//end for
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>
