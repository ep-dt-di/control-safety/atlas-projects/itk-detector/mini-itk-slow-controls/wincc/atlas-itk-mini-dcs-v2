﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixGenericFunctions.ctl" concerns="0">
	<header><![CDATA[/**@file

// TAG_prefixGenericFunctions.ctl
This library contains the generic functions of the package.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<function name="TAG_prefixGenericFunctions_addDefaultUser">
			<header><![CDATA[
/** add/reset default user, password not change if already set.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/]]>
			</header>
			<content><![CDATA[
TAG_prefixGenericFunctions_addDefaultUser()
{
  string acComponent="fwAccessControl";
  string acVersion="3.2.7", sPassword;
  int acInstalled, iPos;
  dyn_string exceptionInfo, dsUserName, dsPassword;
  bool bIntegratedMode;
  
  fwInstallation_componentInstalled(acComponent, acVersion, acInstalled);
  if (!acInstalled)
  {
    DebugTN("TAG_prefixGenericFunctions_setDefaultUser: Access Control component " + acVersion + " not installed!");
    return;
  }

  bIntegratedMode=_fwAccessControl_integratedMode();
  if(!bIntegratedMode)
  {
    // use here the fwAccessControl functions to create domains, privileges, groups, users
    DebugTN("TAG_prefixGenericFunctions_setDefaultUser: TAG_PREFIX domain, group and user updated");
  }
  else
    DebugTN("TAG_prefixGenericFunctions_setDefaultUser: fwAccessControl mode activated, the changes are automatically populated from the AC server.");
  
}]]>
			</content>
		</function>
		<function name="TAG_prefixGenericFunctions_FaceplateStatusHeader">
			<header><![CDATA[
/** set the text and the colors of the header in the faceplate status tab (state of the FE connection)
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFEDp  input, front-end DP name
@param sFEAlias  input, front-end name (alias)
@param sDeviceAlias  input, device name (alias)
@param iAlarm  input, front-end comm system integrity alarm level
@param bSystemAlarmEnable  input, front-end comm system integrity enable/disable
@param bInvalid  input, device invalid state
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixGenericFunctions_FaceplateStatusHeader(string sFEDp, string sFEAlias, string sDeviceAlias, int iAlarm, bool bSystemAlarmEnable, bool bInvalid)
{
  bool bOk=false, bFrontEnd=false;
  time tLast;
  string sBackColor, sForeColor, sText, sTime, sName;

  sName=sFEAlias;
  if (sName=="")
    sName="?";
  else if (sName!=sFEDp)
    bFrontEnd=true;

  if(bInvalid)
  {
    sBackColor="unDataNotValid";
    sForeColor="_3DText";
    sTime="";
    sText="DATA INVALID";
  }
  else if(!bSystemAlarmEnable)
  {
    //SystemAlarm disabled
    //sBackColor="unAlarmMasked";
    sBackColor="unDataNotValid";  
    sForeColor="_3DText";
    sTime="";
    sText=sName + " DATA Connection DISABLED";
  }
  else
  {
    if(iAlarm > c_unSystemIntegrity_no_alarm_value)
    {
      //Connection stopped
      sBackColor="unDataNotValid";
      sForeColor="_3DText";
      sText=sName + " NO DATA Connection";
   
      if(bFrontEnd)
      {
        if (dpExists(sFEDp))
        {
         dpGet(sFEDp + ".communication.counter:_online.._stime", tLast);
         sTime=" since: " + (string)tLast; 
        }
        else
        {
         sTime="?";
        }  
      }
      else
        sTime="";
    }
    else
    {
      //Connection FE OK
      sBackColor="_3DFace";
      sForeColor="_3DText";
      sText=sName + " DATA Connection OK";
      sTime="";   
      bOk=true;
    }
  }
  sText=sText + sTime;
  if(shapeExists("txtFEstatus"))
  {
    setMultiValue("txtFEstatus", "backCol", sBackColor,
                  "txtFEstatus", "foreCol", sForeColor,
                  "txtFEstatus", "text", sText);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixGenericFunctions_FaceplateStatusDisconnect">
			<header><![CDATA[
/** set the text and the colors of the header in the faceplate status tab (state of the FE connection) in 
  case of disconnection of the remote system
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/]]>
			</header>
			<content><![CDATA[
void TAG_prefixGenericFunctions_FaceplateStatusDisconnect()
{
  string sBackColor, sForeColor, sText;

  sBackColor="_3DFace";
  sForeColor="unDataNoAccess";
  sText="DATA not connected";
    
  if(shapeExists("txtFEstatus"))
  {
    setMultiValue("txtFEstatus", "backCol", sBackColor,
                  "txtFEstatus", "foreCol", sForeColor,
                  "txtFEstatus", "text", sText);
  }
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>
