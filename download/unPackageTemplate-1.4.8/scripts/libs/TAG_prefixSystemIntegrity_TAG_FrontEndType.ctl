﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixSystemIntegrity_TAG_FrontEndType.ctl" concerns="3">
	<header><![CDATA[/**@file

// TAG_prefixSystemIntegrity_TAG_FrontEndType.ctl
This library contains the system integrity functions of the TAG_FrontEndType.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<constant name="g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList">
			<comment>
				<![CDATA[// List of the TAG_FrontEndType-DS dp checked]]>
			</comment>
			<content>
				<![CDATA[global dyn_string g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList;]]>
			</content>
		</constant>
		<constant name="g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId">
			<comment>
				<![CDATA[// List of the TAG_FrontEndType-DS thread Id checking the counter]]>
			</comment>
			<content>
				<![CDATA[global dyn_int g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId;]]>
			</content>
		</constant>
		<constant name="g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay">
			<content>
				<![CDATA[global int g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay;]]>
			</content>
		</constant>
		<function name="TAG_FrontEndType_systemIntegrityInfo">
			<header><![CDATA[
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/]]>
			</header>
			<content><![CDATA[
dyn_string TAG_FrontEndType_systemIntegrityInfo()
{
  return makeDynString(DS_pattern);
}]]>
			</content>
		</function>
		<function name="TAG_prefixSystemIntegrity_TAG_FrontEndType_Initialize">
			<header><![CDATA[
/** Get the list of TAG_FrontEndType checked and enabled by the systemIntegrity. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@param dsResult output, list of enabled front-end systemIntegrity
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixSystemIntegrity_TAG_FrontEndType_Initialize(dyn_string &dsResult)
{
  dyn_string dsList;
  int len, i;
  string dpToCheck;
  bool enabled;

  dpGet(UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
  TAG_prefixSystemIntegrity_TAG_FrontEndType_DataCallback("", dsList);

  dsList = dpNames(c_unSystemAlarm_dpPattern+DS_pattern+"*", c_unSystemAlarm_dpType);
  len = dynlen(dsList);
  for(i = 1; i<=len; i++) {
    dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
    dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+DS_pattern),strlen(dpToCheck));
    if(dpExists(dpToCheck)) {
      if(dpTypeName(dpToCheck) == TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE) {
        dpGet(dsList[i]+".enabled", enabled);
        if(enabled)
          dynAppend(dsResult, dpToCheck);
      }
    }
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixSystemIntegrity_TAG_FrontEndType_DataCallback">
			<header><![CDATA[
/** Callback on the configuration data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@param sDpe1  input, dpe
@param dsConfigData input, configuration data
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixSystemIntegrity_TAG_FrontEndType_DataCallback(string sDpe1, dyn_string dsConfigData)
{
  dyn_string dsTemp=dsConfigData;

  // check the size of the configuration data and add default value if some data are missing
  while(dynlen(dsTemp) <=1)
    dynAppend(dsTemp, "0");

  g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay = dsTemp[1];
  if(g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay <= 0)
    g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay = c_TAG_prefixSystemIntegrity_defaultTAG_FRONTENDTYPECheckingDelay;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix systemIntegrity", "TAG_prefixSystemIntegrity_TAG_FrontEndType_DataCallback", dsConfigData, g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay);
}]]>
			</content>
		</function>
		<function name="TAG_prefixSystemIntegrity_TAG_FrontEndType_HandleCommand">
			<header><![CDATA[
/** Callback on the systemIntegrity command.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@param sDpe1  input, dpe
@param command input, type of command
@param sDpe2  input, dpe
@param parameters input, parameters of the command
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixSystemIntegrity_TAG_FrontEndType_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
  dyn_string exceptionInfo;
  int i, len =dynlen(parameters);
  dyn_string dsResult;
   string sResult;
   string sMessage;
 
  switch(command) {
    case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
      for(i=1; i<=len; i++) {
        TAG_prefixSystemIntegrity_TAG_FrontEndType_checking(parameters[i], true, true, exceptionInfo);
      }
      break;
    case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
      for(i=1; i<=len; i++) {
        TAG_prefixSystemIntegrity_TAG_FrontEndType_checking(parameters[i], false, false, exceptionInfo);
        // remove all _UnSystemAlarm dp from the alert list of applicationDP if it is in
        _unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i], false, exceptionInfo);
        // delete all the _UnSystemAlarm dps
        if(dpExists(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]))
          dpDelete(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]);
      }
      break;
    case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
      for(i=1; i<=len; i++) {
        if(dpExists(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]))
          TAG_prefixSystemIntegrity_TAG_FrontEndType_checking(parameters[i], false, true, exceptionInfo);
      }
      break;
    case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
      for(i=1; i<=len; i++) {
        if(dpExists(c_unSystemAlarm_dpPattern+DS_pattern+parameters[i]))
          TAG_prefixSystemIntegrity_TAG_FrontEndType_checking(parameters[i], false, false, exceptionInfo);
      }
      break;
    case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
      dpSet(UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
          UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList);
      break;
    case UN_SYSTEMINTEGRITY_EXTENDED_DIAGNOSTIC: // extended diagnostic
      dynAppend(dsResult, getCurrentTime());
      dynAppend(dsResult, "g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList:");
      len = dynlen(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList);
      for(i=1;i<=len;i++) {
        sResult = g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList[i];
        dynAppend(dsResult, sResult);
      }
      dynAppend(dsResult, "g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId:");
      len = dynlen(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId);
      for(i=1;i<=len;i++) {
        sResult = g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId[i];
        dynAppend(dsResult, sResult);
      }
      dynAppend(dsResult, "g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay:" + g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay);
      dpSet(UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
          UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", dsResult);
      DebugTN(dsResult);
      break;
    case UN_SYSTEMINTEGRITY_DEBUG:
      if(dynlen(parameters) >= 2)
      {
        g_b32DebugLevel = parameters[1];
        g_sDebugFilter = parameters[2];
      }
      else
      {
        g_b32DebugLevel = 0;
        g_sDebugFilter = "";
      }
      break;
    case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("TAG_package"));
      dpSet(UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
    default:
      break;
  }

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix systemIntegrity", "TAG_prefixSystemIntegrity_TAG_FrontEndType_HandleCommand", command, parameters);
  if(dynlen(exceptionInfo) > 0) {
    if(isFunctionDefined("unMessageText_sendException")) {
      unMessageText_sendException("*", "*", "TAG_prefixSystemIntegrity_TAG_FrontEndType_HandleCommand", "user", "*", exceptionInfo);
    }
// handle any error uin case the send message failed
    if(dynlen(exceptionInfo) > 0) {
      DebugN(getCurrentTime(), exceptionInfo);
    }
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixSystemIntegrity_TAG_FrontEndType_checking">
			<header><![CDATA[
/** This function register/de-register the callback funtion of TAG_FrontEndType manager and creates the _unSystemAlarm_TAG_FrontEndType dp 
with the alarm config but it does not delete it.

  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  CTRL

@param sDp  input, data point name
@param bCreate  input, true create the dp and the alarm config if it does not exist, enable it
@param bRegister  input, true do a dpConnect, false do a dpDisconnect
@param exceptionInfo output, exception are returned here
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixSystemIntegrity_TAG_FrontEndType_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
  string sDpToChek, dp, dpPlc, plcHostname, description;
  int res, thId, pos, drvNum, len,i;
  bool bError = false;
  dyn_string plc_plc_list;

  string alertPanel,alertHelp;
  bool configExists, isActive;
  int alertConfigType;
  dyn_string alertTexts,alertClasses,summaryDpeList,alertPanelParameters;
  dyn_float alertLimits;
  
// remove the system name
  res = strpos(sDp, ":");
  if(res >= 0)
    dp = substr(sDp, res+1, strlen(sDp));
  else
    dp = sDp;
  sDpToChek = dp;

  dp = c_unSystemAlarm_dpPattern+DS_pattern+dp;

// in case the Dp is not of type _UnPlc do nothing. Should not happen, just for security.
  if(dpTypeName(sDpToChek) != TAG_PREFIX_TAG_FRONTENDTYPE_DPTYPE)
    return;
    
  dpGet(sDpToChek+".communication.driver_num", drvNum);

  if(bCreate) {
// get the plc hostname and the driver number
    plcHostname = unGenericDpFunctions_getAlias(sDpToChek);
// create all the _UnSystemIntegrity Dp and its alarm config if it is not existing
    if(!dpExists(dp)) {
      description = getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION")+plcHostname+" -> DS driver "+ drvNum;
      unSystemIntegrity_createSystemAlarm(sDpToChek, DS_pattern, description, exceptionInfo);
    }
  }

  if(bRegister) {
// connect to the callback function if not already done
    pos = _unSystemIntegrity_isInList(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList, dp);
    if(pos <= 0) {
  // add it in the list, because one of the callback function needs it.        
      pos = _unSystemIntegrity_setList(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList, dp, true);

// start the thread for checking the counter
      thId = startThread(UN_SYSTEM_INTEGRITY_TAG_FRONTENDTYPE_check, sDpToChek, drvNum);
      g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId[pos] = thId;
      if(thId<0)
        bError = true;

      if(!bError) {
        // do here any dpConnect to the front-end DPE, or proxy DPE
        // keep the DPs in global list to be able to dpDisconnect it later
      }
      if(bError) {
        pos = _unSystemIntegrity_isInList(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList, dp);
        if(pos > 0) {
    // kill the threads if they were started
          res = stopThread(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId[pos]);
// and then remove it from the list
          pos = _unSystemIntegrity_setList(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList, dp, false);
//DebugN("rmv", res);
          dynRemove(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId, pos);
        }

        fwException_raise(exceptionInfo, "ERROR", 
              "TAG_prefixSystemIntegrity_TAG_FrontEndType_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_CONNECT") +dp,"");
      }
      else {
//DebugN("Enable", g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId, "end");
      // set the enable to true and activate the alarm if it is not activated of all the _UnSystemIntegrity DP.
        dpSet(dp+".enabled", true);
        unAlarmConfig_mask(dp+".alarm", false, exceptionInfo);
      // get all the PLC-PLC dp, and enable them
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++) {
          dpSet(plc_plc_list[i]+".enabled", true);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
        }

      // get all the FESystemAlarm dp, and enable them, check the Ok state and set the correct value
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++) {
          dpSet(plc_plc_list[i]+".enabled", true);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
        }

// set the GQ bit so all data will bypass the smoothing
        dpSet("_Driver" + drvNum + ".SM:_original.._value", 1);
        dpSet("_Driver" + drvNum + ".GQ:_original.._value", 1);
      }
    }
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix systemIntegrity", "TAG_prefixSystemIntegrity_TAG_FrontEndType_checking register", sDp,
                                 g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId, g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId);
  }
  else {
// disconnect the callback function
    pos = _unSystemIntegrity_isInList(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList, dp);
    if(pos > 0) {
// kill the thread
      res = stopThread(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId[pos]);
      if(res<0)
        bError = true;

        // do here any dpDisconnect to the front-end DPE, or proxy DPE
        // use the global list set during the dpConnect 

      if(bError) {
        fwException_raise(exceptionInfo, "ERROR", 
              "TAG_prefixSystemIntegrity_TAG_FrontEndType_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_DISCONNECT") +dp,"");
      }
      else {
      // remove from list
        pos = _unSystemIntegrity_setList(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList, dp, false);
//DebugN("rmv", res);
        dynRemove(g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId, pos);
      // set the enable to false and de-activate the alarm and acknowledge it if necessary of all the _UnSystemIntegrity DP.
        dpSet(dp+".enabled", false, dp+".alarm", c_unSystemIntegrity_no_alarm_value);
        unAlarmConfig_mask(dp+".alarm", true, exceptionInfo);
      // get all the PLC-PLC dp, and disable them
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++) {
// do not reset the alarm value because this DPE should be connected to an address config
          dpSet(plc_plc_list[i]+".enabled", false);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
        }

      // get all the FESystemAlarm dp, and disable them, check the Ok state and set the correct value
        plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
        len = dynlen(plc_plc_list);
        for(i=1;i<=len;i++) {
// do not reset the alarm value because this DPE should be connected to an address config
          dpSet(plc_plc_list[i]+".enabled", false);
          unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
        }
      }
    }
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix systemIntegrity", "TAG_prefixSystemIntegrity_TAG_FrontEndType_checking deregister", sDp, 
                                 g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSList, g_TAG_prefixSystemIntegrity_TAG_FrontEndType_DSThreadId);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheck">
			<header><![CDATA[
/** This function checks if the counter of the TAG_FrontEndType dp is modified periodically. 
A _UnSystemAlarm is set if the counter is not updated periodically.

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  CTRL

@param sFeDp  input, data point name
@param iDrvNum  input, the driver number
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheck(string sFeDp, int iDrvNum)
{
  int waitingTime, oldAlarmValue = -1, oldTimingAlarm = -1;
  int oldValue = -1, newValue, alarmValue, iTime;
  bool bEnabled;
  string sModPlc;
  int res, iPlcNumber;
  
  while(true) {
  // if 0 set it to g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay
    waitingTime = g_TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheckingDelay;
    if(waitingTime <= 0)
      waitingTime = c_TAG_prefixSystemIntegrity_defaultTAG_FRONTENDTYPECheckingDelay;
  // wait
    delay(waitingTime);
  
  // get the value of the counter and the timeout PLCChecking
    dpGet(sFeDp+".communication.counter", newValue);
    if(newValue == oldValue) {
    // the counter was not modified, set an alarm
      alarmValue = c_unSystemIntegrity_alarm_value_level1;
    }
    else {
    // the counter was modified, reset the alarm
      alarmValue = c_unSystemIntegrity_no_alarm_value;
    }

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix systemIntegrity "+sFeDp, "TAG_prefixSystemIntegrity_TAG_FrontEndTypeCheck check", sFeDp, 
                                 waitingTime, oldValue, newValue, oldAlarmValue, alarmValue);
    
    if(oldAlarmValue != alarmValue) {
      dpSet(c_unSystemAlarm_dpPattern+DS_pattern+sFeDp+".alarm", alarmValue);
      // set the GQ bit so all data will bypass the smoothing
      dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
      dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", 1);
    }

    oldValue = newValue;
    oldAlarmValue = alarmValue;
  }
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>
