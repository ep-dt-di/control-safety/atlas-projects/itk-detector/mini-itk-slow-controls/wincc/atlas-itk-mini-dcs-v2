﻿<?xml version="1.0" encoding="UTF-8"?>
<ctrlScript fileName="TAG_prefixTAG_DeviceType.ctl" concerns="2" browseSubTypes="true">
	<header><![CDATA[/**@file

// TAG_prefixTAG_DeviceType.ctl
This library contains the widget, faceplate, etc. functions of TAG_DeviceType.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/]]>
	</header>
	<elementList>
		<function name="TAG_prefixTAG_DeviceType_AcknowledgeAlarm">
			<header><![CDATA[
/** Returns the list of TAG_DeviceType DPE with alarm config that can be acknowledged
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName  input, device name DP name
@param sDpType input, device type
@param dsNeedAck output, the lsit of DPE
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_AcknowledgeAlarm(string sDpName, string sDpType, dyn_string &dsNeedAck)
{
  dsNeedAck = makeDynString("ProcessInput....");
  // if the acknoledge must be sent to the PLC: CPC Front-end: add UN_ACKNOWLEDGE_PLC
  //dsNeedAck = makeDynString("ProcessInput....", UN_ACKNOWLEDGE_PLC);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix AcknowledgeAlarm TAG_DeviceType "+sDpName, "TAG_prefixTAG_DeviceType_AcknowledgeAlarm", sDpName, sDpType, dsNeedAck);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_ObjectListGetValueTime">
			<header><![CDATA[
/** Function called from snapshot utility of the treeDeviceOverview to get the time and value
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName  input, device name
@param sDeviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  string sTime, sValue, sState, sColor;
  bool bOk, bBad;
  
  dpGet(sDeviceName+".ProcessInput.iData", sValue, 
        sDeviceName+".ProcessInput.iData", sState, 
        sDeviceName+".ProcessInput.iData:_online.._stime", sTime, 
        sDeviceName+".ProcessInput.iData:_online.._bad", bBad);

  dsReturnData[1] = sTime;
  dsReturnData[2] = sValue;
  dsReturnData[3] = bBad;
  dsReturnData[4] = sState;
  dsReturnData[5] = sColor;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix snapshot TAG_DeviceType "+sDeviceName, "TAG_prefixTAG_DeviceType_ObjectListGetValueTime", sDeviceName, dsReturnData);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateStatusRegisterCB">
			<header><![CDATA[
/** faceplate DistributedControl callback of the faceplate status panel
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateStatusRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction, iRes;
  dyn_string exceptionInfo;
  bool bRemote;
  
  deviceName = unGenericDpFunctions_getDpName($sDpName);
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;
                                         
  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+sDp, "TAG_prefixTAG_DeviceType_FaceplateStatusRegisterCB", sDp, deviceName, g_bCallbackConnected, iAction);
  switch (iAction)
  {
    case UN_ACTION_DISCONNECT:
      TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_DPCONNECT:
      TAG_prefixTAG_DeviceType_FaceplateConnect(deviceName);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      TAG_prefixTAG_DeviceType_FaceplateDisconnect(deviceName);
      TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection">
			<header><![CDATA[
/** set the faceplate when the device system is disconnected 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection()
{
// 2. animate FE Connection
  TAG_prefixGenericFunctions_FaceplateStatusDisconnect();
  // there is no need to do here execScript because there is one faceplate per device type
  // set all the graphical element to the state data not connnected
  // ex. of functions that can be used
  //unGenericObject_ColorBoxDisconnect, unGenericObject_DisplayValueDisconnect, etc.
  unGenericObject_ColorBoxDisconnect(makeDynString("data_equals"));
  unGenericObject_DisplayValueDisconnect(makeDynString("RangeMax", "RangeMin", "inputData", "outputData"));
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType", "TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection");
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateConnect">
			<header><![CDATA[
/** dpConnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateConnect(string deviceName)
{
  int iAction, iRes;
  dyn_string exceptionInfo;
  string sFrontEnd, sSystemName;
  
  g_sDeviceAlias = unGenericDpFunctions_getAlias(deviceName);

  g_bUnSystemAlarmPlc = false;
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
  g_sFrontEndDp = sSystemName+sFrontEnd;
  g_sFrontEndAlias = unGenericDpFunctions_getAlias(g_sFrontEndDp);
  
  if(sFrontEnd != ""){
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
//DebugN(sSystemName, sFrontEnd);
    if(dpExists(sFrontEnd)) {
      g_bUnSystemAlarmPlc = true;
    }
  }
    
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_FaceplateConnect", deviceName, sFrontEnd, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc) {    
    // set all faceplate global variable usefull for the callback, e.g.: description, format, unit, etc.
    // set graphical element of the faceplate to the init state if needed
	
	bool unitExists;
	dyn_string exceptionInfo;
	fwUnit_get(deviceName + ".ProcessInput.iData", unitExists,g_sUnit, exceptionInfo);
    
    // if there are many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // keep the kind of dpConnect in a variable global to the faceplate and do a swith for the dpConnect
    // and do to the correct dpDisconnect in the TAG_prefixTAG_DeviceType_FaceplateDisconnect
    iRes = dpConnect("TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB",
                      deviceName + ".ProcessInput.iData:_online.._invalid",
                      deviceName + ".ProcessInput.iData",
                      deviceName + ".ProcessOutput.set",
                      sFrontEnd+".alarm",
                      sFrontEnd+".enabled");
    g_bCallbackConnected = (iRes >= 0);
  }
  else
    TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection();
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateDisconnect">
			<header><![CDATA[
/** dpDisconnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateDisconnect(string deviceName)
{
  int iRes;
  string sFrontEnd, sSystemName;
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_FaceplateDisconnect", deviceName, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc) {
// get the systemName
    sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
    sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;

    // if there were many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // re-use the kind of dpConnect kept in a variable global to the faceplate set during TAG_prefixTAG_DeviceType_FaceplateConnect
    // and do to the correct dpDisconnect
    iRes = dpDisconnect("TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB",
                      deviceName + ".ProcessInput.iData:_online.._invalid",
                      deviceName + ".ProcessInput.iData",
                      deviceName + ".ProcessOutput.set",
                      sFrontEnd+".alarm",
                      sFrontEnd+".enabled");
    g_bCallbackConnected = !(iRes >= 0);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB">
			<header><![CDATA[
/** callback function on the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpInvalid input, the invalid DPE 
@param bInvalid input, data validity
@param sDp1 input, the DPE2 
@param iData input, device status data
@param sDp3 input, the DPE3 
@param iSet input, device set data
@param sDpFESystemIntegrityAlarmValue input, front-end device system integrity alarm DPE 
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param sDpFESystemIntegrityAlarmEnabled input, front-end device system integrity enable DPE
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB(string sDpInvalid, bool bInvalid,
                                                    string sDp1, int iData,
                                                    string sDp3, int iSet,
                                                    string sDpFESystemIntegrityAlarmValue, int iFESystemIntegrityAlarmValue, 
                                                    string sDpFESystemIntegrityAlarmEnabled, bool bFESystemIntegrityAlarmEnabled)
{
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+sDp1, "TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB", sDp1, bInvalid, iData, iSet, g_bSystemConnected, 
                               iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);
  if(g_bSystemConnected)
    TAG_prefixGenericFunctions_FaceplateStatusHeader(g_sFrontEndDp, g_sFrontEndAlias, g_sDeviceAlias, iFESystemIntegrityAlarmValue, 
                                              bFESystemIntegrityAlarmEnabled, bInvalid);
  // animate all the graphical element of the faceplate
  // all the variables global to the faceplate can be used
  // encapsulate all the setValue or function call with the following if(g_bSystemConnected)
  /* e.g.:
    if(g_bSystemConnected)
      unGenericObject_DisplayValue(...);
    */
  // ex. of functions that can be used: unGenericObject_DisplayValue, unGenericObject_ColorBoxAnimate, 
  // unGenericObject_ColorBoxAnimateAlarm, unGenericObject_ColoredSquareAnimate, 
  // unGenericObject_DisplayRange, etc.
  
	string sColor;
	switch (iData)
	{
		case TAG_DEVICETYPE_FACEPLATE_VALUE1:
			sColor = "unDisplayValue_Status";
			break;
		default:
			sColor = "red";
			break;
	}
	
	if(g_bSystemConnected)
      unGenericObject_DisplayValue("", g_sUnit, iData, "inputData", sColor, bInvalid);
	  
	if(g_bSystemConnected)
      unGenericObject_DisplayValue("", "", iSet, "outputData", "unDisplayValue_Status", bInvalid);
	  
	if(g_bSystemConnected)
	  unGenericObject_DisplayRange(DPCONFIG_NONE, "", g_sUnit, 0, 0, "RangeMax", "RangeMin", "unDisplayValue_Parameter");
	  
	if(g_bSystemConnected)
	  unGenericObject_ColorBoxAnimate("data_equals", (iData == TAG_DEVICETYPE_FACEPLATE_VALUE1), 1, "unFaceplate_OperationModeActive", 
					bInvalid || (!bFESystemIntegrityAlarmEnabled) || (iFESystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value));
	  
	// if(g_bSystemConnected)
	  // unGenericObject_ColorBoxAnimate(...);
	// if(g_bSystemConnected)
	  // unGenericObject_ColorBoxAnimateAlarm(...);
	// if(g_bSystemConnected)
	  // unGenericObject_ColoredSquareAnimate(...);
	// if(g_bSystemConnected)
	  // unGenericObject_DisplayRange(...);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_ButtonRegisterCB">
			<header><![CDATA[
/** Contextual button DistributedControl callback of the contextual device button panel
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_ButtonRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sDpType, sFunction;
  int iAction, iRes, iManReg01;
  dyn_string exceptionInfo, dsFunctions, dsUserAccess;
  
  deviceName = unGenericDpFunctions_getDpName($sDpName);
  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_ButtonRegisterCB", sDp, deviceName, g_bCallbackConnected, iAction);
  switch (iAction)
  {
    case UN_ACTION_DISCONNECT:
      TAG_prefixTAG_DeviceType_ButtonDisconnection();
      break;
    case UN_ACTION_DPCONNECT:

      sDpType="TAG_DeviceType";
      unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
      sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_BUTTONUSERACCESS];
             
      if (sFunction != "")
      {
        evalScript(dsUserAccess, "dyn_string main(string deviceNameTemp, string sDpTypeTemp) {" + 
               "dyn_string dsUserAccessReturn;" +
               "if (isFunctionDefined(\"" + sFunction  + "\"))" +
               "    {" +
                 sFunction  + "(deviceNameTemp, sDpTypeTemp, dsUserAccessReturn);" +
               "    }" +
               "return dsUserAccessReturn; }", makeDynString(), deviceName, sDpType);
        // keep the list of the device allowed action 
        g_dsUserAccess=dsUserAccess;  
      }
      else
        fwException_raise(exceptionInfo,"ERROR", "TAG_prefixTAG_DeviceType_ButtonRegisterCB:" + getCatStr("unGeneration","UNKNOWNFUNCTION"), "EMPTY");

    // set all contextual panel global variable usefull for the TAG_prefixTAG_DeviceType_ButtonAnimationCB, e.g.: description, format, unit, etc.
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_ButtonRegisterCB Connect", deviceName, sDpType, dsUserAccess);
      iRes = dpConnect("TAG_prefixTAG_DeviceType_ButtonAnimationCB",
               deviceName + ".statusInformation.selectedManager:_lock._original._locked",
               deviceName + ".statusInformation.selectedManager",
               deviceName + ".ProcessInput.iData",
               deviceName + ".ProcessInput.iData:_online.._invalid");
      g_bCallbackConnected = (iRes >= 0);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_ButtonRegisterCB Disconnect", deviceName);
      iRes = dpDisconnect("TAG_prefixTAG_DeviceType_ButtonAnimationCB",
               deviceName + ".statusInformation.selectedManager:_lock._original._locked",
               deviceName + ".statusInformation.selectedManager",
               deviceName + ".ProcessInput.iData",
               deviceName + ".ProcessInput.iData:_online.._invalid");
      g_bCallbackConnected = !(iRes >= 0);
      TAG_prefixTAG_DeviceType_ButtonDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_UserLoginGetButtonState">
			<header><![CDATA[
/** return the device data, this function is called when the user logs in and logs out
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param sType input, the device type
@param dsReturnData output, the device data, [1] = lock state, [2] = lock by, [3] .. [6] device data
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_UserLoginGetButtonState(string deviceName, string sType, dyn_string &dsReturnData)
{
  int iRes, iData;
  string sDpLock, sDpSelectedManager, sSelectedManager, dpData;
  bool bLocked;

  // read the device data (6 string)
  sDpLock = deviceName + ".statusInformation.selectedManager:_lock._original._locked";
  sDpSelectedManager = deviceName + ".statusInformation.selectedManager";
  dpData = deviceName + ".ProcessInput.iData";
  if (dpExists(deviceName))
  {
    iRes = dpGet(sDpLock, bLocked, sDpSelectedManager, sSelectedManager, dpData, iData);
  }
  else
  {
    iRes = -1;
  }
  if (iRes < 0) // Errors during dpget
  {
    bLocked = false;
    sSelectedManager = "";
  }
  dsReturnData=makeDynString(bLocked, sSelectedManager, iData, "", "", "");
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_UserLoginGetButtonState", deviceName, sType, dsReturnData);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_ButtonSetState">
			<header><![CDATA[
/** Set the state of the contextual button of the device
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param sDpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_ButtonSetState(string deviceName, string sDpType, dyn_string dsUserAccess, dyn_string dsData)
{
  bool bSelected;
  dyn_string dsButtons;
  int i, length;
  string localManager;
  bool bLocked, buttonEnabled;
  string sSelectedManager, sDeviceValue1, sDeviceValue2, sDeviceValue3, sDeviceValue4;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_ButtonSetState", deviceName, sDpType, dsUserAccess, dsData);
  bLocked = dsData[1];
  sSelectedManager = dsData[2];
  sDeviceValue1 = dsData[3];
  sDeviceValue2 = dsData[4];
  sDeviceValue3 = dsData[5];
  sDeviceValue4 = dsData[6];

  // dsButtons = list of all the possible actions except Select 
  // use the name of the graphical element without "button"
  // use the name of the graphical elements.
  // put here the list of all the device actions (list of all the buttons)
  dsButtons = makeDynString("Reset", "Increase", "Decrease", "Copy", "Info", "FECnt"); 
  length = dynlen(dsButtons);
// 1. Selection state
  localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedManager);
  bSelected = (localManager == "S");
// 2. for each possible action check if the action is in the list dsUserAccess
  // crosscheck if needed with the state of the device
  for(i=1;i<=length;i++)
  {
    buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0);  // User access
    switch(dsButtons[i])
    {
      case "Reset":
        // calculate the enable state of the button from the device data
        buttonEnabled = buttonEnabled & bSelected & (sDeviceValue1 == "10");
        break;
      case "Increase":
      case "Decrease":
      case "Copy":
        // calculate the enable state of the button from the device data
        buttonEnabled = buttonEnabled & bSelected;
        break;
      case "Info":
      case "FECnt":
        break;
      // add here all the device actions if needed
//      case "..":
//        break;
      default:
        break;
    }
    setValue(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], "enabled", buttonEnabled);
  }
// 3. Button Select state
  unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_ButtonDisconnection">
			<header><![CDATA[
/** set the state of the device action button when the device system is disconnected 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_ButtonDisconnection()
{
  // disable all the device action (button)
//  unGenericObject_ButtonDisconnect(makeDynString(UN_FACEPLATE_BUTTON_SELECT, ".."));
  unGenericObject_ButtonDisconnect(makeDynString(UN_FACEPLATE_BUTTON_SELECT, "Reset", "Increase", "Decrease", "Copy", "Info", "FECnt"));
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType", "TAG_prefixTAG_DeviceType_ButtonDisconnection");
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_ButtonUserAccess">
			<header><![CDATA[
/** returns the list of allowed action on the device for a user logged in
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName input, the device DP name
@param sDpType input, the device type
@param dsAccess input, list of allowed action on the device
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_ButtonUserAccess(string sDpName, string sDpType, dyn_string &dsAccess)
{
  bool operator, expert, admin;
  int i, length;
  dyn_bool dbPermissions;
  dyn_string exceptionInfo;
  string sOperAction, sExpAction, sAdminAction;
  bool bActionDefined = false;

  // get the configured list of authorized action per privilege
  unGenericDpFunctions_getAccessControlPriviledgeRigth(sDpName, g_dsDomainAccessControl, dbPermissions, sOperAction, sExpAction, sAdminAction, exceptionInfo);
  if((sOperAction != UNICOS_ACCESSCONTROL_DOMAINNAME) || (sExpAction != UNICOS_ACCESSCONTROL_DOMAINNAME) ||
     (sAdminAction != UNICOS_ACCESSCONTROL_DOMAINNAME))
    bActionDefined=true;
  operator = dbPermissions[2];
  expert = dbPermissions[3];
  admin = dbPermissions[4];
  dsAccess = makeDynString();
  
  // for each privilege get the list of allowed actions if there are action defined on the device
  // if there are no action defined for the device get the default one
  // in the example, the default behavior is 
  // the FECnt device action is always allowed
  // the Info device action (button) is allowed if one has the operator privilege
  // the Select, Increase and Decrease device actions (buttons) are allowed if one has the expert privilege
  // the Copy and Reset button device actions (buttons) are allowed if one has the admin privilege
  dynAppend(dsAccess, makeDynString("FECnt")); // put here the devcie action that always allowed
  if (operator)
  {
    if(bActionDefined)
      dynAppend(dsAccess, strsplit(sOperAction, UN_ACCESS_CONTROL_SEPARATOR));
    else
      dynAppend(dsAccess, makeDynString("Info")); // put here all the list of device actions (buttons) allowed with the operator privilege
  }
  if (expert)
  {
    if(bActionDefined)
      dynAppend(dsAccess, strsplit(sExpAction, UN_ACCESS_CONTROL_SEPARATOR));
    else 
      dynAppend(dsAccess, makeDynString("Increase", "Decrease", UN_FACEPLATE_BUTTON_SELECT)); // put here all the list of device actions (buttons) allowed with the expert privilege
  }
  if(admin){
    if(bActionDefined)
      dynAppend(dsAccess, strsplit(sAdminAction, UN_ACCESS_CONTROL_SEPARATOR));
    else
      dynAppend(dsAccess, makeDynString("Copy", "Reset")); // put here all the list of device actions (buttons) allowed with the admin privilege
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType "+sDpName, "TAG_prefixTAG_DeviceType_ButtonUserAccess", sDpName, sDpType, dbPermissions, sOperAction, sExpAction, sAdminAction, dsAccess);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_ButtonAnimationCB">
			<header><![CDATA[
/** callback function on the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpLock input, the lock DPE 
@param bLocked input, selected lock state of the device
@param sDpSelect input, the select DPE 
@param sSelectedManager input, user and Ui that selected the device
@param sDp1 input, the DPE1 
@param iData input, device status data
@param sDpInvalid input, the invalid DPE 
@param bInvalid input, data validity
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_ButtonAnimationCB(string sDpLock, bool bLocked, string sDpSelect, string sSelectedManager,
                                           string sDp1, int iData,
                                           string sDpInvalid, bool bInvalid)
{
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix button TAG_DeviceType "+sDp1, "TAG_prefixTAG_DeviceType_ButtonAnimationCB", sDp1, iData, bInvalid, g_dsUserAccess);
  // call the set button state with the device data and the current allowed action on the device
  // which is in g_dsUserAccess
  TAG_prefixTAG_DeviceType_ButtonSetState(unGenericDpFunctions_getDpName(sDp1), "TAG_DeviceType", g_dsUserAccess, 
                  makeDynString(bLocked, sSelectedManager, iData, "", "", ""));
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetRegisterCB">
			<header><![CDATA[
/** widget DistributedControl callback
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction;
  dyn_string exceptionInfo;
  string sFrontEnd;
  bool bRemote;
  
  deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
  if (deviceName == "")  // In case of disconnection
  {
    deviceName = g_sDpName;
  }
  g_bUnSystemAlarmPlc = false;
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);

  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;

  if(sFrontEnd != ""){
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
//DebugN(sSystemName, sFrontEnd);
    if(dpExists(sFrontEnd)) {
      g_bUnSystemAlarmPlc = true;
    }
  }

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_WidgetRegisterCB", sDp, deviceName, g_bCallbackConnected, iAction);
  switch(iAction)
  {
    case UN_ACTION_DISCONNECT:
      TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPCONNECT:
      g_sDpName = deviceName;
      TAG_prefixTAG_DeviceType_WidgetConnect(deviceName, sFrontEnd);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      TAG_prefixTAG_DeviceType_WidgetDisconnect(deviceName, sFrontEnd);
      TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetDisconnection">
			<header><![CDATA[
/** set the widget when the system is disconnected 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sWidgetType input, the type of widget
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetDisconnection(string sWidgetType)
{
  /** use the exec mechanism to allow multiple widget. **/
  string sFunction = "TAG_prefixTAG_DeviceType_"+sWidgetType+"Disconnection";

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType", "TAG_prefixTAG_DeviceType_WidgetDisconnection", sWidgetType);
  if (isFunctionDefined(sFunction) && (sFunction != ""))
  {
    execScript("main(string sWidgetType) {" + sFunction + "(sWidgetType);}", makeDynString(), sWidgetType);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetConnect">
			<header><![CDATA[
/** dpConnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetConnect(string deviceName, string sFrontEnd)
{
  int iRes;
  string sSystemName;
  
//  check if the device name is of type "TAG_DeviceType" if not like no front-end. -> not available.
  if(dpTypeName(deviceName) != "TAG_DeviceType")
    g_bUnSystemAlarmPlc = false;
  
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_WidgetConnect", deviceName, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc)
  {
    // set all widget global variable usefull for the callback, e.g.: description, format, unit, etc.
	bool unitExists;
	dyn_string exceptionInfo;
	fwUnit_get(deviceName + ".ProcessInput.iData", unitExists, g_sUnit, exceptionInfo);
    // if there are many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // keep the kind of dpConnect in a variable global to the widget and do a swith for the dpConnect
    // and do to the correct dpDisconnect in the TAG_prefixTAG_DeviceType_WidgetDisconnect
    iRes = dpConnect("TAG_prefixTAG_DeviceType_WidgetCB", 
                          deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                          deviceName + ".statusInformation.selectedManager",
                          deviceName + ".ProcessInput.iData",
                          deviceName + ".ProcessInput.iData:_online.._invalid",
                          sFrontEnd+".alarm",
                          sFrontEnd+".enabled");
    g_bCallbackConnected = (iRes >= 0);    
  }else{
    TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetDisconnect">
			<header><![CDATA[
/** dpDisconnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name

*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetDisconnect(string deviceName, string sFrontEnd)
{
  int iRes;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_WidgetDisconnect", deviceName, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc)
  {
    // if there were many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // re-use the kind of dpConnect kept in a variable global to the widget set during TAG_prefixTAG_DeviceType_WidgetConnect
    // and do to the correct dpDisconnect
    iRes = dpDisconnect("TAG_prefixTAG_DeviceType_WidgetCB", 
                          deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                          deviceName + ".statusInformation.selectedManager",
                          deviceName + ".ProcessInput.iData",
                          deviceName + ".ProcessInput.iData:_online.._invalid",
                          sFrontEnd+".alarm",
                          sFrontEnd+".enabled");
    g_bCallbackConnected = !(iRes >= 0);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetCB">
			<header><![CDATA[
/** callback function on the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpLocked input, the lock DPE 
@param bLocked input, the lock state 
@param sDpSelectedManager input, the selected manager DPE 
@param sSelectedManager input, the selected manager 
@param sDp1 input, the DPE1 
@param iData input, device data
@param sDpInvalid input, the invalid DPE 
@param bInvalid input, data validity
@param sDpFESystemIntegrityAlarmValue input, front-end device system integrity alarm DPE 
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param sDpFESystemIntegrityAlarmEnabled input, front-end device system integrity enable DPE
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetCB(string sDpLocked, bool bLocked,
                                  string sDpSelectedManager, string sSelectedManager,
                                  string sDp1, int iData, 
                                  string sDpInvalid, bool bInvalid,
                                  string sDpFESystemIntegrityAlarmValue, int iFESystemIntegrityAlarmValue, 
                                  string sDpFESystemIntegrityAlarmEnabled, bool bFESystemIntegrityAlarmEnabled)
{
  /** use the exec mechanism to allow multiple widget. **/
  string sFunction = "TAG_prefixTAG_DeviceType_"+g_sWidgetType+"Animation";
  string selectColor, sWarningLetter, sWarningColor;
  bool bSelectVisible;
  
  // 1. set the widget.
  unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, bSelectVisible);

  // if bInvalid --> N letter and everything blue
  if (bInvalid){
    sWarningLetter = UN_WIDGET_TEXT_INVALID;
    sWarningColor = "unDataNotValid";
  }  
  else {
// if Front-end problem or problems-> O letter
    if((!bFESystemIntegrityAlarmEnabled) || (iFESystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value))
    {
      sWarningLetter = UN_WIDGET_TEXT_OLD_DATA;
      sWarningColor = "unDataNotValid";
    }
  }
  if(g_bSystemConnected)
    setMultiValue("WarningText", "text", sWarningLetter, 
                  "WarningText", "foreCol", sWarningColor,
                  "SelectArea", "foreCol", selectColor,
                  "LockBmp", "visible", bSelectVisible,
                  "WidgetArea", "visible", true);

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+sDp1, "TAG_prefixTAG_DeviceType_WidgetCB", sDp1, iData, g_sWidgetType, 
                               sWarningLetter, sWarningColor, g_bSystemConnected,
                               selectColor, bSelectVisible);
  if (isFunctionDefined(sFunction) && (sFunction != ""))
  {
    // set here all the common graphical element value or
    // give to the function all the variables
    execScript("main(int iData, string sUnit, bool bSystemConnected, string sWidgetType, int iFESystemIntegrityAlarmValue, bool bFESystemIntegrityAlarmEnabled) {" + 
               sFunction + "(iData, sUnit, bSystemConnected, sWidgetType, iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);}", 
               makeDynString(), iData, g_sUnit, g_bSystemConnected, g_sWidgetType, iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_MenuConfiguration">
			<header><![CDATA[
/** pop-up menu
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName input, device DP name
@param sDpType input, device type
@param dsAccessOk input, the access control 
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_MenuConfiguration(string sDpName, string sDpType, dyn_string dsAccessOk, dyn_string &menuList)
{
  dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_TREND_TEXT);
  
  unGenericObject_addSelectToMenu(sDpName, sDpType, dsMenuConfig, dsAccessOk, menuList);
  unGenericObject_addUnicosActionToMenu(sDpName, sDpType, dsMenuConfig, dsAccessOk, menuList);
  unGenericObject_addDefaultUnicosActionToMenu(sDpName, sDpType, dsMenuConfig, dsAccessOk, menuList);
  unGenericObject_addTrendActionToMenu(sDpName, sDpType, dsMenuConfig, dsAccessOk, menuList, true);
  
  // in this example here only the Increase and Reset device actions are added in a sub menu: device action.
  // The same code must be added to have the same behavior as in the ButtonSetState function
  dyn_string dsUserAccess;
  int pos,i, len, iMenu=UN_POPUPMENU_MIN;
  string sMenu, sAccess, sMenuText;
  int iData;
  bool bSelected;
  bool bActionOk;
  dyn_string dsButtons = makeDynString("Increase", "Reset"); // put here all the device actions to add in the menu
  
  pos = dynContains(menuList, "PUSH_BUTTON, Device configuration, 10, 1");
  dynInsertAt(menuList, "SEPARATOR", pos);
  dynInsertAt(menuList, "CASCADE_BUTTON, device action, 1", pos);
  dynInsertAt(menuList, "SEPARATOR", pos);
  dynAppend(menuList, "device action");
  
  bSelected = dynContains(menuList, "PUSH_BUTTON, Deselect, 1, 1");
  dpGet(sDpName+".ProcessInput.iData", iData);
  // get the allowed device actions for the user logged in
  TAG_prefixTAG_DeviceType_ButtonUserAccess(sDpName, "TAG_DeviceType", dsUserAccess);
  len = dynlen(dsButtons);
  for(i=1;i<=len;i++)
  {
    switch(dsButtons[i])
	{
	  case "Increase":
	    if((dynContains(dsUserAccess, dsButtons[i]) > 0) && bSelected)
	      sAccess = "1";
	    else
	      sAccess = "0";
	    sprintf(sMenu, "%d", (iMenu+1));
	    iMenu++;
	    dynAppend(menuList, "PUSH_BUTTON, "+dsButtons[i]+", "+sMenu+", "+sAccess);
	    break;
	  case "Reset":
	    if((dynContains(dsUserAccess, dsButtons[i]) > 0) && bSelected && (iData ==TAG_DEVICETYPE_WIDGET_VALUE1))
	      sAccess = "1";
	    else
	      sAccess = "0";
	    sprintf(sMenu, "%d", (iMenu+1));
	    iMenu++;
	    dynAppend(menuList, "PUSH_BUTTON, "+dsButtons[i]+", "+sMenu+", "+sAccess);
	    break;
	    break;
	}
  }
  
  // to add a device type menu add the menu from UN_POPUPMENU_MIN
  /** e.g.: device menu enable/disabled depending on the device access right
  int pos, iMenu=UN_POPUPMENU_MIN;
  string sAccess, sOperAction, sExpAction, sAdminAction, sMenu;
  dyn_string exceptionInfo, dsArchiveName;
  dyn_bool dbPermissions;
  
  pos = dynContains(menuList, "PUSH_BUTTON, Device configuration, 10, 1");
  if(pos > 0) {
  // get the access rigth on the device
    unGenericDpFunctions_getAccessControlPriviledgeRigth(sDpName, g_dsDomainAccessControl, dbPermissions, sOperAction, sExpAction, sAdminAction, exceptionInfo);
    sAccess = "0";
    if(dbPermissions[4]) {
      sAccess = "1";
    }

    sprintf(sMenu, "%d", (iMenu+1));
    dynInsertAt(menuList, "SEPARATOR", pos);
    dynInsertAt(menuList, "PUSH_BUTTON, device menu, "+sMenu+", "+sAccess, pos);
    dynInsertAt(menuList, "SEPARATOR", pos);
  }
  */
  /** e.g.: add all the device actions 
  dyn_string dsUserAccess;
  int pos,i, len, iMenu=UN_POPUPMENU_MIN;
  string sMenu;
  
  pos = dynContains(menuList, "PUSH_BUTTON, Device configuration, 10, 1");
  if(pos > 0) {
    // get the allowed device actions for the user logged in
    TAG_prefixTAG_DeviceType_ButtonUserAccess(sDpName, "TAG_DeviceType", dsUserAccess);
    // remove Select if it is in the list
    len = dynContains(dsUserAccess, UN_POPUPMENU_SELECT_TEXT);
    if(len > 0)
      dynRemove(dsUserAccess, len);
    len = dynlen(dsUserAccess);
    if(len > 0)
      dynInsertAt(menuList, "SEPARATOR", pos);

    // add all the device actions
    for(i=1;i<=len;i++)
    {
      sprintf(sMenu, "%d", (iMenu+1));
      iMenu++;
      dynInsertAt(menuList, "PUSH_BUTTON, "+dsUserAccess[i]+", "+sMenu+", 1", pos);
    }
    if(len > 0)
      dynInsertAt(menuList, "SEPARATOR", pos);
  }
  */
  /** e.g.: add all the device actions depending on the current device value
  dyn_string dsUserAccess;
  int pos,i, len, iMenu=UN_POPUPMENU_MIN;
  string sMenu, sAccess, sMenuText;
  int iData;
  bool bSelected;
  bool bActionOk;
  dyn_string dsButtons = makeDynString("..", "..", ".."); 
  
  pos = dynContains(menuList, "PUSH_BUTTON, Device configuration, 10, 1");
  if(pos > 0) {
    // get the allowed device actions for the user logged in
    TAG_prefixTAG_DeviceType_ButtonUserAccess(sDpName, "TAG_DeviceType", dsUserAccess);
    bSelected = dynContains(menuList, "PUSH_BUTTON, Deselect, 1, 1");
    dpGet(sDpName+".ProcessInput.iData", iData);
    // remove Select if it is in the list
    len = dynContains(dsUserAccess, UN_POPUPMENU_SELECT_TEXT);
    if(len > 0)
      dynRemove(dsUserAccess, len);
    len = dynlen(dsButtons);
    if(dynlen(dsUserAccess) > 0)
      dynInsertAt(menuList, "SEPARATOR", pos);

    // add the device actions
    for(i=1;i<=len;i++)
    {
      sprintf(sMenu, "%d", (iMenu+1));
      iMenu++;
      sAccess = "0";
      bActionOk = (dynContains(dsUserAccess, dsButtons[i]) > 0);  // User access
      switch(dsButtons[i])
      {
        case "..":
          bActionOk = bActionOk & bSelected & (iData == "..");
          sMenuText = "Action ..";
          break;
        default:
          bActionOk = false;
          sMenuText = "";
          break;
      }
      if(bActionOk)
        sAccess = "1";
      if(sMenuText != "")
        dynInsertAt(menuList, "PUSH_BUTTON, "+sMenuText+", "+sMenu+", "+sAccess, pos);
    }
    if(dynlen(dsUserAccess) > 0)
      dynInsertAt(menuList, "SEPARATOR", pos);
  }
  */
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix rigthClick TAG_DeviceType "+sDpName, "TAG_prefixTAG_DeviceType_MenuConfiguration", sDpName, sDpType, dsAccessOk, menuList);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_HandleMenu">
			<header><![CDATA[
/** handle the answer of the popup menu
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param sDpType input, device type
@param menuList input, the access control 
@param iMenuAnswer input, selected menu value 
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_HandleMenu(string deviceName, string sDpType, dyn_string menuList, int iMenuAnswer)
{
  string sDeviceName = unGenericDpFunctions_getAlias(deviceName);
  int iUN_POPUPMENU_MIN = UN_POPUPMENU_MIN;
  dyn_string exceptionInfo;
  dyn_string dsTemp, dsSplit;
  int len, i, iMenu, iValue;
  
  // handle unicos menu
  if(iMenuAnswer<iUN_POPUPMENU_MIN)
    unGenericObject_HandleUnicosMenu(deviceName, sDpType, menuList, iMenuAnswer, exceptionInfo);
  else { // handle device actions
    // handle the device actions 
    // find the corresponding device action text
    dsTemp = dynPatternMatch("PUSH_BUTTON, *, 1", menuList);
    len = dynlen(dsTemp);
    for(i=1;i<=len;i++)
    {
      dsSplit = strsplit(dsTemp[i], ",");
      sscanf(dsSplit[3], "%d", iMenu);
      if(iMenu == iMenuAnswer) {
        switch(strltrim(dsSplit[2], " "))
        {
          case "Increase":
            dpGet(deviceName+".ProcessOutput.set", iValue);
            dpSet(deviceName+".ProcessOutput.set", ++iValue);
            break;
          case "Reset":
            dpSet(deviceName+".ProcessOutput.set", 0);
            break;
        }
      }
    }
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix rigthClick TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_HandleMenu", deviceName, sDpType, menuList, iMenuAnswer);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_TAG_WidgetTypeAnimation" duplicate="true">
			<header><![CDATA[
/** animate the device TAG_WidgetType widget
!!!!! function trigger by exec call, $-param and variable of the widget cannot be used, all the necessary data 
must be given to the function. The use of global var (global keyword declaration) is allowed
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param iData input, device data
@param bSystemConnected input, connection state of the system, true=system connected/false=system not connected
@param sWidgetType input, the type of widget
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_TAG_WidgetTypeAnimation(int iData, string sUnit, bool bSystemConnected, string sWidgetType, int iFESystemIntegrityAlarmValue, bool bFESystemIntegrityAlarmEnabled)
{
  /*!!!!!!!! all parameters must be given: function launched by exec ***/
  // encapsulate all the setValue or function call with the following if(bSystemConnected)
  string sColor;
  if(bSystemConnected) {
	switch (iData)
	{
		case TAG_DEVICETYPE_WIDGET_VALUE1:
			sColor = "green";
			break;
		default: 
			sColor = "red";
			break;
	}
    setMultiValue("Body1", "backCol", sColor, "PRIMITIVE_TEXT1", "text", iData);
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType TAG_WidgetType", "TAG_prefixTAG_DeviceType_TAG_WidgetTypeAnimation", sWidgetType, iData, bSystemConnected, 
                               iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_TAG_WidgetTypeDisconnection" duplicate="true">
			<header><![CDATA[
/** animate the the device TAG_WidgetType widget when the system is disconnected 
!!!!! function trigger by exec call, $-param and variable of the widget cannot be used, all the necessary data 
must be given to the function. The use of global var (global keyword declaration) is allowed

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sWidgetType input, the type of widget
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_TAG_WidgetTypeDisconnection(string sWidgetType)
{
  // set all the graphical element of the widget
  setMultiValue("Body1", "backCol", "unDataNoAccess",
                "WarningText", "text", "",
                "WidgetArea", "visible", false,
                "SelectArea", "foreCol", "",
                "LockBmp", "visible", false);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType TAG_WidgetType", "TAG_prefixTAG_DeviceType_TAG_WidgetTypeDisconnection", sWidgetType);
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetRegisterAlertCB">
			<header><![CDATA[
/** widget DistributedControl callback
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetRegisterAlertCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction, iRes;
  dyn_string exceptionInfo;
  string sFrontEnd;
  bool bRemote;
  
  deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
  if (deviceName == "")  // In case of disconnection
  {
    deviceName = g_sDpName;
  }
  g_bUnSystemAlarmPlc = false;
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);

  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;

  if(sFrontEnd != ""){
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
//DebugN(sSystemName, sFrontEnd);
    if(dpExists(sFrontEnd)) {
      g_bUnSystemAlarmPlc = true;
    }
  }
  g_sDeviceName = deviceName;
  g_sFrontEnd = sFrontEnd;

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_WidgetRegisterCB", sDp, deviceName, g_bCallbackConnected, g_bDisconnectCB, iAction);
  switch (iAction)
  {
    case UN_ACTION_DISCONNECT:
      TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPCONNECT:
      g_sDpName = deviceName;
      if(g_bUnSystemAlarmPlc) {
        iRes = dpConnect("TAG_prefixTAG_DeviceType_WidgetLockCallBack", deviceName + ".ProcessInput.iData:_lock._alert_hdl._locked");
        g_bCallbackConnected = (iRes >= 0);
      }
      else
        TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      if(g_bUnSystemAlarmPlc) {
        iRes = dpDisconnect("TAG_prefixTAG_DeviceType_WidgetLockCallBack", deviceName + ".ProcessInput.iData:_lock._alert_hdl._locked");
        g_bCallbackConnected = !(iRes >= 0);
      }
      if(g_bDisconnectCB)
        TAG_prefixTAG_DeviceType_WidgetDisconnectAlert(deviceName, sFrontEnd);
      TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetLockCallBack">
			<header><![CDATA[
/** dpConnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpe input, device DPE name
@param bLockState input, state of the alert lock
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetLockCallBack(string sDpe, bool bLockState)
{
  int iRes;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+g_sDeviceName, "TAG_prefixTAG_DeviceType_WidgetLockCallBack", sDpe, bLockState, g_bDisconnectCB, g_sDeviceName, g_sFrontEnd);
  if(bLockState) { // lock set so disconnect if not already disconnected
// no need to check if it was already set before because it is not possible to do 2 dpSet on the lock the Ev reject the message
    if(g_bDisconnectCB) {
      TAG_prefixTAG_DeviceType_WidgetDisconnectAlert(g_sDeviceName, g_sFrontEnd);
    }
    TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
    // set the WidgetArea to true, so that even if there is a problem with the lock, one can do a rigth click and correct the lock state
    setValue("WidgetArea", "visible", true);
  }
  else { // lock not set so connect if not already connected
// no need to check if it was already set before because it is not possible to do 2 dpSet on the lock the Ev reject the message
    if(!g_bDisconnectCB) {
      TAG_prefixTAG_DeviceType_WidgetConnectAlert(g_sDeviceName, g_sFrontEnd);
    }
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetConnectAlert">
			<header><![CDATA[
/** dpConnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetConnectAlert(string deviceName, string sFrontEnd)
{
  int iRes;
  string sSystemName;
  
//  check if the device name is of type "TAG_DeviceType" if not like no front-end. -> not available.
  if(dpTypeName(deviceName) != "TAG_DeviceType")
    g_bUnSystemAlarmPlc = false;
  
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_WidgetConnectAlert", deviceName, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc)
  {
    // set all widget global variable usefull for the callback, e.g.: description, format, unit, etc.
    
    // if there are many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // keep the kind of dpConnect in a variable global to the widget and do a swith for the dpConnect
    // and do to the correct dpDisconnect in the TAG_prefixTAG_DeviceType_WidgetDisconnectAlert
    iRes = dpConnect("TAG_prefixTAG_DeviceType_WidgetCB", 
                          deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                          deviceName + ".statusInformation.selectedManager",
                          deviceName + ".ProcessInput.iData",
                          deviceName + ".ProcessInput.iData:_online.._invalid",
                          sFrontEnd+".alarm",
                          sFrontEnd+".enabled");
    g_bDisconnectCB = (iRes >= 0);    
  }else{
    TAG_prefixTAG_DeviceType_WidgetDisconnection(g_sWidgetType);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_WidgetDisconnectAlert">
			<header><![CDATA[
/** dpDisconnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name

*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_WidgetDisconnectAlert(string deviceName, string sFrontEnd)
{
  int iRes;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix widget TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_WidgetDisconnectAlert", deviceName, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc)
  {
    // if there were many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // re-use the kind of dpConnect kept in a variable global to the widget set during TAG_prefixTAG_DeviceType_WidgetConnectAlert
    // and do to the correct dpDisconnect
    iRes = dpDisconnect("TAG_prefixTAG_DeviceType_WidgetCB", 
                          deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                          deviceName + ".statusInformation.selectedManager",
                          deviceName + ".ProcessInput.iData",
                          deviceName + ".ProcessInput.iData:_online.._invalid",
                          sFrontEnd+".alarm",
                          sFrontEnd+".enabled");
    g_bDisconnectCB = !(iRes >= 0);
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateStatusRegisterAlertCB">
			<header><![CDATA[
/** faceplate DistributedControl callback of the faceplate status panel
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateStatusRegisterAlertCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction, iRes;
  dyn_string exceptionInfo;
  bool bRemote;
  
  deviceName = unGenericDpFunctions_getDpName($sDpName);
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;
  
  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+sDp, "TAG_prefixTAG_DeviceType_FaceplateStatusRegisterAlertCB", sDp, deviceName, g_bCallbackConnected, g_bDisconnectCB, iAction);
  switch (iAction)
  {
    case UN_ACTION_DISCONNECT:
      TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_DPCONNECT:
      TAG_prefixTAG_DeviceType_FaceplateConnectAlert(deviceName);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      TAG_prefixTAG_DeviceType_FaceplateDisconnectAlert(deviceName);
      TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateConnectAlert">
			<header><![CDATA[
/** dpConnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateConnectAlert(string deviceName)
{
  int iAction, iRes;
  dyn_string exceptionInfo;
  string sFrontEnd, sSystemName;
  
  g_sDeviceAlias = unGenericDpFunctions_getAlias(deviceName);

  g_bUnSystemAlarmPlc = false;
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
  g_sFrontEndDp = sSystemName+sFrontEnd;
  g_sFrontEndAlias = unGenericDpFunctions_getAlias(g_sFrontEndDp);
  
  if(sFrontEnd != ""){
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
//DebugN(sSystemName, sFrontEnd);
    if(dpExists(sFrontEnd)) {
      g_bUnSystemAlarmPlc = true;
    }
  }
  g_sDeviceName = deviceName;
  g_sFrontEnd = sFrontEnd;
    
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_FaceplateConnectAlert", deviceName, sFrontEnd, g_bUnSystemAlarmPlc, g_bDisconnectCB);
  if(g_bUnSystemAlarmPlc) { 
    iRes = dpConnect("TAG_prefixTAG_DeviceType_FaceplateLockCallBack", deviceName + ".ProcessInput.iData:_lock._alert_hdl._locked");
    g_bCallbackConnected = (iRes >= 0);
  }
  else
    TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection();
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateDisconnectAlert">
			<header><![CDATA[
/** dpDisconnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateDisconnectAlert(string deviceName)
{
  int iRes;
  string sFrontEnd, sSystemName;
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+deviceName, "TAG_prefixTAG_DeviceType_FaceplateDisconnectAlert", deviceName, g_bUnSystemAlarmPlc, g_bDisconnectCB);
  if(g_bUnSystemAlarmPlc) {
// get the systemName
    sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
    sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;

    iRes = dpDisconnect("TAG_prefixTAG_DeviceType_FaceplateLockCallBack", deviceName + ".ProcessInput.iData:_lock._alert_hdl._locked");
    g_bCallbackConnected = !(iRes >= 0);
    if(g_bDisconnectCB) {
      // if there were many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
      // re-use the kind of dpConnect kept in a variable global to the faceplate set during TAG_prefixTAG_DeviceType_FaceplateConnect
      // and do to the correct dpDisconnect
      iRes = dpDisconnect("TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB",
                        deviceName + ".ProcessInput.iData:_online.._invalid",
                        deviceName + ".ProcessInput.iData",
                        deviceName + ".ProcessOutput.set",
                        sFrontEnd+".alarm",
                        sFrontEnd+".enabled");
      g_bDisconnectCB = !(iRes >= 0);
    }
  }
}]]>
			</content>
		</function>
		<function name="TAG_prefixTAG_DeviceType_FaceplateLockCallBack">
			<header><![CDATA[
/** dpConnect to the device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpe input, device DPE name
@param bLockState input, state of the alert lock
*/]]>
			</header>
			<content><![CDATA[
TAG_prefixTAG_DeviceType_FaceplateLockCallBack(string sDpe, bool bLockState)
{
  int iRes;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TAG_prefix faceplate TAG_DeviceType "+g_sDeviceName, "TAG_prefixTAG_DeviceType_FaceplateLockCallBack", sDpe, bLockState, g_bDisconnectCB, g_sDeviceName, g_sFrontEnd);
  if(bLockState) { // lock set so disconnect if not already disconnected
// no need to check if it was already set before because it is not possible to do 2 dpSet on the lock the Ev reject the message
    if(g_bDisconnectCB) {
      iRes = dpDisconnect("TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB",
                      g_sDeviceName + ".ProcessInput.iData:_online.._invalid",
                      g_sDeviceName + ".ProcessInput.iData",
                      g_sDeviceName + ".ProcessOutput.set",
                      g_sFrontEnd+".alarm",
                      g_sFrontEnd+".enabled");
      g_bDisconnectCB = !(iRes >= 0);
    }
    TAG_prefixTAG_DeviceType_FaceplateStatusDisconnection();
  }
  else { // lock not set so connect if not already connected
// no need to check if it was already set before because it is not possible to do 2 dpSet on the lock the Ev reject the message
    if(!g_bDisconnectCB) {
      // set all faceplate global variable usefull for the callback, e.g.: description, format, unit, etc.
      // set graphical element of the faceplate to the init state if needed
    
      // if there are many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
      // keep the kind of dpConnect in a variable global to the faceplate and do a swith for the dpConnect
      // and do to the correct dpDisconnect in the TAG_prefixTAG_DeviceType_FaceplateDisconnect
      iRes = dpConnect("TAG_prefixTAG_DeviceType_FaceplateStatusAnimationCB",
                        g_sDeviceName + ".ProcessInput.iData:_online.._invalid",
                        g_sDeviceName + ".ProcessInput.iData",
                        g_sDeviceName + ".ProcessOutput.set",
                        g_sFrontEnd+".alarm",
                        g_sFrontEnd+".enabled");
      g_bDisconnectCB = (iRes >= 0);
    }
  }
}]]>
			</content>
		</function>
	</elementList>
</ctrlScript>
