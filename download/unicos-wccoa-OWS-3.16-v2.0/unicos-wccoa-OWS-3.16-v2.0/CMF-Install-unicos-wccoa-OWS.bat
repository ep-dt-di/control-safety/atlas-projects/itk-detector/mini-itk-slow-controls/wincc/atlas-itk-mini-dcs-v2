@echo off

SET OWS_VERSION=3.16.2
SET PROJECTS_TO_INSTALL=OWS_3.16

SET WCCOA_PATH=C:\Siemens\Automation\WinCC_OA\3.16
SET OWS_PATH=C:\WinCC_OA_Proj




:: Create OWS folder
md %OWS_PATH%


:: For each of the four different OWS
for %%x in (%PROJECTS_TO_INSTALL%) do (

  :: Unzip project files
  echo Unzipping %WCCOA_PATH%\bin\unzip.exe -q -o %~dp0unicos-wccoa-OWS\%%x.zip -d %OWS_PATH%\%%x
  %WCCOA_PATH%\bin\unzip.exe -q -o %~dp0unicos-wccoa-OWS\%%x.zip -d %OWS_PATH%\%%x 
  echo ...
  
  :: Register the project
  echo Registering: %WCCOA_PATH%\bin\WCCOActrl -autoreg -config %OWS_PATH%\%%x\config\config
  start /B /wait %WCCOA_PATH%\bin\WCCOActrl -autoreg -config %OWS_PATH%\%%x\config\config
  echo ... )
  
  :: Add OWS_3.16.[VERSION] into Windows registry
  echo Adding OWS_3.16 and version into Windows registry
  echo ...
  reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\OWS_3.16" /f /v DisplayName    /t REG_SZ /d "OWS for WinCC OA 3.16"
  reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\OWS_3.16" /f /v DisplayVersion /t REG_SZ /d "%OWS_VERSION%"
  echo Windows registry updated
  echo ...
  
echo OWS for WinCC 3.16 version %OWS_VERSION% installed
echo ...
echo This window will be closed in 10 seconds

sleep 10
  
  