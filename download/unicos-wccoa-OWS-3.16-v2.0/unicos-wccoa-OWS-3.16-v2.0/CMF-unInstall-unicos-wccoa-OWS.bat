@echo off

set OWS_VERSION=3.16.2
set PROJECTS_TO_INSTALL=OWS_3.16

set INSTALL_OWS_FOLDER=C:\WinCC_OA_Proj
set WINCC_OA_REGISTRY=%programData%\Siemens\WinCC_OA\pvssInst.conf


:: For each of the four different OWS
for %%x in (%PROJECTS_TO_INSTALL%) do (

  :: Unregister project
  echo Unregistering project: %%x 
  echo ...
  cscript.exe //Nologo Unregister_WinCCOA_Project.vbs /project:%%x /registry:%WINCC_OA_REGISTRY%
    
  :: Remove project folder
  echo Removing project folder: %INSTALL_OWS_FOLDER%\%%x
  rmdir /S /Q %INSTALL_OWS_FOLDER%\%%x
  echo ...
  echo Project removal finished: %%x )
  
  echo Removing Windows registry for OWS_3.16
  echo ...
  reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\OWS_3.16" /f
  echo Windows registry updated
  
echo OWS for WinCC 3.16 version %OWS_VERSION% uninstalled
echo ...
echo This window will be closed in 10 seconds

sleep 10