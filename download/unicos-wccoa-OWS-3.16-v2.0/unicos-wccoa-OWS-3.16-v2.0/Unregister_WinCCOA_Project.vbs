Option Explicit


'-----------------------------------------------------------
'------------------------ CONSTANTS ------------------------
'-----------------------------------------------------------
Const iEXIT_SUCCES						= 0
Const iEXIT_MODE_ARGUMENT_EMPTY			= 2000
Const iEXIT_MODE_ARGUMENT_UNDEFINED		= 2001
Const iEXIT_REGISTRY_EMPTY 				= 2010
Const iEXIT_REGISTRY_NOT_EXIST 			= 2011
Const iEXIT_PROJECT_NOT_FOUND 			= 2020

Const iARG_FIXED_NUMBER = 2
Const iFILE_FOR_WRITING = 2


'-----------------------------------------------------------
'------------------------ VARIABLES ------------------------
'-----------------------------------------------------------
Dim iLoop
Dim iNumberOfModifiedLines
Dim iNumberArguments
Dim sArgTemp
Dim sRegistry
Dim sRegistryContent
Dim sProject
Dim objFileText
Dim objFSO
Dim objScriptArguments
Dim objRegistryBlocks
Dim objBlockLine


'-----------------------------------------------------------
'-------------------------- CODE ---------------------------
'-----------------------------------------------------------
WScript.Echo "Variables initialized -> OK"


Set objScriptArguments = WScript.Arguments
Set objFSO   = CreateObject("Scripting.FileSystemObject")


'----------------------- Checking input arguments -----------------------
'	- project
'	- registry
'

iNumberArguments = objScriptArguments.Count


' Print all input arguments
For iLoop = 0 to iNumberArguments-1
	WScript.Echo "Input Arguments -> Arg-" & iLoop & " = " & objScriptArguments(iLoop)
Next

If iNumberArguments <> iARG_FIXED_NUMBER Then
	WScript.Echo "Input Arguments -> Error: script should receive always " & iARG_FIXED_NUMBER & " arguments."
Else
	WScript.Echo "Input Arguments -> Number = " & iNumberArguments
End If


' Check mandatory fields
If objScriptArguments.Named.Exists("project") Then
	WScript.Echo "Input Arguments -> project -> argument defined."
	sArgTemp = objScriptArguments.Named.Item("project")
	If (sArgTemp <> "") Then
		WScript.Echo "Input Arguments -> project -> argument valid."
		sProject = sArgTemp
	Else
		WScript.Echo "Input Arguments -> project -> argument empty."
		WScript.Quit(iEXIT_ACTION_ARGUMENT_EMPTY)
	End If
Else
	WScript.Echo "Input Arguments -> project -> does not exist."
	WScript.Quit(iEXIT_ACTION_ARGUMENT_UNDEFINED)
End If


If objScriptArguments.Named.Exists("registry") Then
	WScript.Echo "Input Arguments -> registry -> argument defined."
	sArgTemp = objScriptArguments.Named.Item("registry")
	If (sArgTemp <> "") Then
		WScript.Echo "Input Arguments -> registry -> argument valid."
		sRegistry = sArgTemp
	Else
		WScript.Echo "Input Arguments -> registry -> argument empty."
		WScript.Quit(iEXIT_ACTION_ARGUMENT_EMPTY)
	End If
Else
	WScript.Echo "Input Arguments -> registry -> does not exist."
	WScript.Quit(iEXIT_ACTION_ARGUMENT_UNDEFINED)
End If


WScript.Echo "Input Arguments -> sProject = " & sProject & ", sRegistry = " & sRegistry & " -> OK"


'----------------------------------------------------------------------------------------------------------------------------------------------------
WScript.Echo "Registry check -> " 

If objFSO.FileExists(sRegistry) Then
	WScript.Echo "Registry check -> Registry = " & sRegistry & " exists"
	If objFSO.GetFile(sRegistry).Size > 0 Then
		WScript.Echo "Registry check -> Registry size(" & objFSO.GetFile(sRegistry).Size & ") > 0"
		Set objFileText = objFSO.OpenTextFile(sRegistry)
		sRegistryContent = objFileText.ReadAll
		WScript.Echo "Registry check -> Registry read"
		objFileText.Close
	Else
		WScript.Echo "Registry check -> Registry size is 0"
		WScript.Quit(iEXIT_REGISTRY_EMPTY)
	End If
Else
	WScript.Echo "Registry check -> registry file does not exist: " & sRegistry
	WScript.Quit(iEXIT_REGISTRY_NOT_EXIST)
End If


'----------------------------------------------------------------------------------------------------------------------------------------------------
WScript.Echo "Removing project in registry -> " 

Set objFileText = objFSO.OpenTextFile(sRegistry, iFILE_FOR_WRITING, TRUE)
WScript.Echo "Removing project in registry -> Opened registry for writting"

objRegistryBlocks = Split(sRegistryContent, "[")
WScript.Echo "Removing project in registry -> Splitted content of the registry"

iNumberOfModifiedLines = 0
For iLoop = 1 to UBound(objRegistryBlocks)

	objBlockLine = Split (objRegistryBlocks(iLoop), vbCrLf)

	If UBound(objBlockLine) > 0 Then
		If objBlockLine(0) = "Software\ETM\PVSS II\Configs\" & sProject & "]" Then
			WScript.Echo "Removing project in registry -> Project block found: " & iLoop & " don't write to registry."
			iNumberOfModifiedLines = iNumberOfModifiedLines + 1
		Else
			objFileText.Write("[" & objRegistryBlocks(iLoop))
		End If
	End If
Next
objFileText.Close
WScript.Echo "Removing project in registry -> Number of project blocks removed " & iNumberOfModifiedLines



If iNumberOfModifiedLines > 0 Then
	WScript.Echo "Removing project in registry -> Project " & sProject & " successfully removed"
	WScript.Quit(iEXIT_SUCCES)
Else
	WScript.Echo "Removing project in registry -> Error project " & sProject & " not found in the registry"
	WScript.Quit(iEXIT_PROJECT_NOT_FOUND)
End If