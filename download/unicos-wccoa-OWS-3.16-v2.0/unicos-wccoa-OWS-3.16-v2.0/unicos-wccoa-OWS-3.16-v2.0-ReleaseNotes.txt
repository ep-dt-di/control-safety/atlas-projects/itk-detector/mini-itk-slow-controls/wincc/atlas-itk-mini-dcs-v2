unicos-wccoa-OWS-3.16 package for WinCC OA 3.16 Version 2.0
=================================================================
!!! ONLY COMPATIBLE WITH WinCC OA 3.16 !!!
=================================================================

+ Description:
==============

This is a generic package for multi UNICOS OWS interface. Only one installation is needed for all UNICOS interfaces 
to one or multiple DS

This OWS package can be used for generic account like "unicryo" or NICE login account (see below). 

	
+ To install unicos-wccoa-OWS
=============================

1. unzip unicos-wccoa-OWS-3.16-vX.Y.zip file
2. execute the CMF-Install-unicos-wccoa-OWS.bat


+ To de-install unicos-wccoa-OWS
================================

1. execute the CMF-unInstall-unicos-wccoa-OWS.bat


+ To start the UNICOS OWS interface with generic and personal account
=====================================================================

Examples are given in the OWS\examples\unicosOWS-HMI folder
. unicosHMI.bat:  start a one screen unicosHMI interface
. 2unicosHMI.bat: start a two screens unicosHMI interface
. 3unicosHMI.bat: start a three screens unicosHMI interface
. WinCCOAui_gedi.bat: start the WinCC OA editor
. LogViewer.bat: start the WinCC OA log

For each DS you need to be connected to:
1. Copy the corresponding bat file for DS you want to be connected to
2. Edit them and set the two following variables
	- DS_HOSTNAME=the hostname of the DS
	- DS_PROJECTPATH=The folder of the WinCC OA project
3. Do shortcuts if needed


+ Versions:
=======================

* 20 Nov 2019   version 2.0:
----------------------------
	[UNHMI-106] Fixed 3.15 reference in the config file


* 28 July 2019	version 1.0:
----------------------------
	[UNHMI-75] Package creation



==============================================
Jonas ARROYO, CERN BE/ICS/FD

Support: icecontrols.support@cern.ch









