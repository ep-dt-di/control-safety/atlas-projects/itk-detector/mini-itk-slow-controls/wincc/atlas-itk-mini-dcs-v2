Set objArgs = WScript.Arguments
If objArgs.Count = 1 Then
	Url = objArgs(0)
	Set objIE = WScript.CreateObject ("InternetExplorer.Application")
	ObjIE.Toolbar = false
	ObjIE.StatusBar = false
	objIE.Navigate Url
	objIE.Visible = true
End If