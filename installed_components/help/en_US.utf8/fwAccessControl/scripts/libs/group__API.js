var group__API =
[
    [ "General Purpose Functions", "group__GeneralPurposeFunctions.html", "group__GeneralPurposeFunctions" ],
    [ "Private Functions", "group__PrivateFunctions.html", "group__PrivateFunctions" ],
    [ "Privilege-checking functions", "group__PermissionFunctions.html", "group__PermissionFunctions" ],
    [ "Domain-manipulation functions", "group__DomainFunctions.html", "group__DomainFunctions" ],
    [ "Group-manipulation functions", "group__GroupFunctions.html", "group__GroupFunctions" ],
    [ "User-manipulation functions", "group__UserFunctions.html", "group__UserFunctions" ],
    [ "Egroups-related functions", "group__EgroupsFunctions.html", "group__EgroupsFunctions" ]
];