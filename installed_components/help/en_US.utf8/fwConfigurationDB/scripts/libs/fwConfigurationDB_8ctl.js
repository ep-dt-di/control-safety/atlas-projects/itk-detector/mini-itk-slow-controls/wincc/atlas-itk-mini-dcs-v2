var fwConfigurationDB_8ctl =
[
    [ "fwConfigurationDB_checkInit", "fwConfigurationDB_8ctl.html#gabfe9731975c38c37302fbfe101a2be58", null ],
    [ "fwConfigurationDB_initialize", "fwConfigurationDB_8ctl.html#ga43d05490668849e2a99e320f93ed38cf", null ],
    [ "fwConfigurationDB_version", "fwConfigurationDB_8ctl.html#ga5d118cf88ccc58f19c3b794f7202cfb8", null ],
    [ "fwConfigurationDB_minimalDBSchemaVersion", "fwConfigurationDB_8ctl.html#gae8b636716b69f47fae54aeffcb09e976", null ],
    [ "fwConfigurationDB_currentSetupName", "fwConfigurationDB_8ctl.html#ga676118bae9020aa17bf8feed33f86ec3", null ],
    [ "fwConfigurationDB_initialized", "fwConfigurationDB_8ctl.html#gaa0cb6d1e9092075e0effa3c1bfb0b7db", null ],
    [ "fwConfigurationDB_allowResolveRemote", "fwConfigurationDB_8ctl.html#ae22458ce57b9540e593ac254b46956d7", null ],
    [ "fwConfigurationDB_DBConfigured", "fwConfigurationDB_8ctl.html#ga202bab768fdd34fa77d6f8c80384dafa", null ],
    [ "GfwConfigurationDB_currentRecipeType", "fwConfigurationDB_8ctl.html#gaa5e66c4b6898765e57f8a19192bc51a5", null ],
    [ "g_fwConfigurationDB_DBConnectionOpen", "fwConfigurationDB_8ctl.html#ga37365fd45a062f3d239acb7d24c1f4ea", null ],
    [ "fwConfigurationDB_hasDBConnectivity", "fwConfigurationDB_8ctl.html#ga28317180a9845b4341429bda0ee03418", null ],
    [ "g_fwConfigurationDB_DBConnection", "fwConfigurationDB_8ctl.html#ga0cd06f5e1ec5329dc08ec91efc01a044", null ],
    [ "fwConfigurationDB_SchemaPrivs", "fwConfigurationDB_8ctl.html#a4c4ce3b827f09954cc7de02c328b4504", null ],
    [ "g_fwConfigurationDB_DebugSQL", "fwConfigurationDB_8ctl.html#a2245ffcee0c654981b15f6e237b81beb", null ],
    [ "g_fwConfigurationDB_Debug", "fwConfigurationDB_8ctl.html#a0889e07ef1c77588c4223fe073243e04", null ],
    [ "g_fwConfigurationDB_stats", "fwConfigurationDB_8ctl.html#af7c01eb3afd0bb7255bcf5cb36f6cd0e", null ],
    [ "fwConfigurationDB_ERROR_GENERAL", "fwConfigurationDB_8ctl.html#ga89b201b02dda2e01ead7670b0bc6aa36", null ],
    [ "fwConfigurationDB_ERROR_DPTNotExist", "fwConfigurationDB_8ctl.html#ga127b56d4f021fe48d6812d3cec9cdf75", null ],
    [ "fwConfigurationDB_ERROR_OperationAborted", "fwConfigurationDB_8ctl.html#gabec9c96c3f0a6303c4611f3b2fe379d0", null ],
    [ "fwConfigurationDB_ERROR_SeparatorCharInStringList", "fwConfigurationDB_8ctl.html#ga08439e37e4ca7841e0943a2c135d1b97", null ]
];