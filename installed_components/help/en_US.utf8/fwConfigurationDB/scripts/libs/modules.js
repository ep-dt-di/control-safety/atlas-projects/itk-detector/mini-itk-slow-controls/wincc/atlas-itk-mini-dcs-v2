var modules =
[
    [ "Private Functions", "group__PrivateFunctions.html", "group__PrivateFunctions" ],
    [ "Database Access Functions", "group__DBAccessFunctions.html", "group__DBAccessFunctions" ],
    [ "Initialization Functions", "group__InitFunctions.html", "group__InitFunctions" ],
    [ "Recipe Functions", "group__RecipeFunctions.html", "group__RecipeFunctions" ],
    [ "Indices used to refer to recipeObject variables", "group__recipeObjectIndices.html", "group__recipeObjectIndices" ],
    [ "Global variables", "group__GlobalVariables.html", "group__GlobalVariables" ],
    [ "Set-up parameters", "group__Constants.html", "group__Constants" ],
    [ "Error Codes", "group__ErrorCodes.html", "group__ErrorCodes" ],
    [ "External access to ConfigurationDB (PL/SQL API)", "group__PLSQLAPI.html", "group__PLSQLAPI" ]
];