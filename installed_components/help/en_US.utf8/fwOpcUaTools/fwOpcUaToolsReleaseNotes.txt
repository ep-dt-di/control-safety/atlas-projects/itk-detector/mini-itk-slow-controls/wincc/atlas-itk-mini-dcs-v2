             JCOP Framework - fwOpcUaTools



15 June 2020: fwOpcUaTools-0.1.0
--------------------------------
- First version of the component

- Includes panel fwOpcUaTools_ConnectionsTool.pnl:
  This panel allows to select a JCOP device and their children to move their OPC UA server connection (driver number) and subscriptions to a different ones automatically.



-------------------------------------------------
Jonas ARROYO, CERN BE/ICS/FD

Support: icecontrols.support@cern.ch

