		UNICOS unCore Component Release Notes


unCore-8.5.1: 7 April 2021
===============================
This version is a part of the UNICOS-8.5.1 release.
Please refer to the release notes of the unicos-framework-8.5.1.

With respect to the previous version of unCore there are the following changes/fixes

* UNCORE-164: Fix for the trend popup panel that did not allow to operate with the original (father) synoptic panel
* UNCORE-167: Fix for unBeep unable to open when double clicked
* UNCORE-172: Support Application Filter in the new HMI
* UNCORE-179: Fixed the multiple acknowledgement button (acknowledge all/acknowledge all visible) in the bottom of the UNICOS Alarm Screen
* UNAESCRN-85: Added missing reference panel overparametrization in the "standard" alarm screen
* UNDEVICE-86: Added BACnet static data visible in the front-end diagnostic panel
* UNHMI-156: Missing screen buttons added for navigation through the new HMI
* UMHMI-157: Device Tree Overview opened in the current active module + some fixes related with error messages that should not appear
* UNHMI-159: Code clean up + fix for small bugs
* UNHMI-161: Fix delay function + fix for the print panel button in the old HMI
* UNHMI-162: Fix for Navigation History Bar and Buttons when startPanel dollar param specified in old HMI
* UNHMI-177: Optimization of global variable g_unHMI_RunsInNewHMI
* UNHMI-178: Fix dependency on unHMI declaration for the unHMI module name
* UNHMI-179: Fix issue of panel name variable in the panelPosition() function for view port zoom and position
* UNHMI-180: Load viewport's zoom and position when navigating through different panels automatically + delays removal + change of PanelName from composed string formed by DpName + PanelName + DollarParms to only PanelName
* UNHMI-181: unicosHMI window cannot be closed if started from GEDI
* UNHMI-182: Fix issue with Window Title for old HMI, and  parameter for Filtered Applications in old and new HMI







unCore-8.5.0: 16 December 2020
===============================

This version is a part of the UNICOS-8.5.0 release.
Please refer to the release notes of the unicos-framework-8.5.0.

With respect to the previous version of unCore there
are the following changes

* ENS-28192: Fix for the export of log history on Windows: 
	we do not foce excel.exe to be used to open the file anymore
	but rather rely on what is configured in the operating system,
	and the file extenstion is set to ".csv" rather than ".xls".
	On Linux the mechanism is unchanged (.txt file extension, and
	the built-in editor is used to open the file)

* UNDEVICE-95: resolved issue with S7 PLC export for CPC projects
	where error was reported and the line corresponding to the
	frontend was missing.

* UNHMI-153: dynamic trends in the new unHMI are not open in the 
	dock module but rather a popup (revert the change in unHMI-1.0.0
	to the original behaviour)

* UNCORE-161: installation of unHMI should not be mandatory:
	remove the mistakenly introduced dependency of unCore on unHMI

* UNCORE-163: fix for wrong $-parameters when unOpenSynopticTrend.pnl
	button is parameterized in GEDI and .xml panel file is used

* Lots of improvements in the Email/SMS notifications
  - UNTOOL-30 Refactoring of unSendMail for clarity and easier maintenance
  - UNTOOL-34 SMS notification: optimize formatting of messages,
	namely SMS: more compact time, value that respects
	the formatting/precision, URL for host diagnostics changed
	from (defunct) "lemon" to (new) COSMOS.
  - UNTOOL-28 respect the SMS_MESSAGE device property if present




unCore-8.4.4: 28 September 2020
===============================

This is a bugfix release necessary namely for the proper
functioning of the unHMI component.

It is supposed to be used on top of unicos-framework-8.4.4, 
as a replacement of the unCore-8.4.3 component included there.
The release contains the same set of subcomponents, with the
same versions, that the unCore-8.4.3.

The following issues have been resolved

* UNHMI-16: it is now possible to use the $startPanel parameter
	for the "old" as well as the "new" HMI panel.
* UNHMI-145: make contextual buttons work correctly when
	a device is selected



---
There is no history of the Release Notes for this component
for older version. Please refer to the Release Notes of
the whole framework instead.
