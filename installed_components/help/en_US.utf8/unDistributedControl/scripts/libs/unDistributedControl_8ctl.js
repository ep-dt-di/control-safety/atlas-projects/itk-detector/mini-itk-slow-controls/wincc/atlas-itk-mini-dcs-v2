var unDistributedControl_8ctl =
[
    [ "unDistributedControl_register", "unDistributedControl_8ctl.html#ad14ada5822442e7e0334b083362df1c6", null ],
    [ "unDistributedControl_deregister", "unDistributedControl_8ctl.html#a22829bf3664fa8aaa7188b34fb3d3fb1", null ],
    [ "unDistributedControl_isRemote", "unDistributedControl_8ctl.html#a03e19914a6717e4bbb1c6d17bed0df0d", null ],
    [ "unDistributedControl_isConnected", "unDistributedControl_8ctl.html#a2b9852ea3b3b16921e087748b54604aa", null ],
    [ "unDistributedControl_getAllDeviceConfig", "unDistributedControl_8ctl.html#a726d7d71877a9c94247870a2808c834d", null ],
    [ "unDistributedControl_setDeviceConfig", "unDistributedControl_8ctl.html#a0b9abe7cdab8f74366f6fca546fdd312", null ],
    [ "unDistributedControl_checkCreateDp", "unDistributedControl_8ctl.html#af513a5b199e0b0c52a6894a9e233b500", null ],
    [ "unDistributedControl_convertHostPort", "unDistributedControl_8ctl.html#ab43eb23391c4a1c9c2c8389ef2b3247e", null ],
    [ "unDistributedControl_getDSHostname", "unDistributedControl_8ctl.html#a40282a3b98d8e121b53331d400afb52a", null ],
    [ "unDistributedControl_saveInConfigFile", "unDistributedControl_8ctl.html#a8f7f6e0a0a0c33faab775098b059f990", null ]
];