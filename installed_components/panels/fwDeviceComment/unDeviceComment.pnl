<?xml version="1.0" encoding="UTF-8"?>
<panel version="14">
 <properties>
  <prop name="Name">
   <prop name="en_US.utf8"></prop>
  </prop>
  <prop name="Size">1167 771</prop>
  <prop name="BackColor">_3DFace</prop>
  <prop name="RefPoint">373.5 177.5</prop>
  <prop name="InitAndTermRef">True</prop>
  <prop name="SendClick">False</prop>
  <prop name="RefFileName"></prop>
  <prop name="DPI">96</prop>
  <prop name="layoutType">None</prop>
 </properties>
 <events>
  <script name="ScopeLib" isEscaped="1"><![CDATA[#uses &quot;fwDeviceComment/fwDeviceComment&quot;
#uses &quot;fwGeneral/fwGeneral&quot;

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// GLOBAL VARIABLES
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

bit32 g_b32CommentPermissions; // bit field encoding permissions for adding, disabling, and deleting comments

#event commentsChanged(dyn_string dsDPList)


// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// PANEL TYPES
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

struct strCommentInfo
{
  string sDeviceAlias; // holds the alias of the device
  dyn_string dsDeviceComments; // holds the comments for the device
};

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// PANEL-GLOBAL FUNCTIONS
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------

/**
  This function is called from the comment edit panel whenever comments are changed
*/
public synchronized void unDeviceCommentsPanel_commentsChangedCB(dyn_string dsDPList)
{
  int iUpperVisibleLine, iLowerVisibleLine;

  // save current table view
  getValue(&quot;fwDeviceComment_commentsTable&quot;, &quot;lineRangeVisible&quot;, iUpperVisibleLine, iLowerVisibleLine);

  // disable table updates
  fwDeviceComment_commentsTable.updatesEnabled = false;

  // refresh table for this DPE only
  unDeviceComment_commentChangedCB(dsDPList);

  // restore the previous table view
  setValue(&quot;fwDeviceComment_commentsTable&quot;, &quot;lineVisible&quot;, iUpperVisibleLine);

  // re-enable table updates
  fwDeviceComment_commentsTable.updatesEnabled = true;

  enableButtons();

}

// ------------------------------------------------------------------------------------------------

/**
  GUI initialization
*/
void initGUI()
{
  unDeviceComment_accessControlSetup(&quot;userChangedCB&quot;, &quot;permissionsChangedCB&quot;);
}

// ------------------------------------------------------------------------------------------------

/** Callback function that reloads the user's rights upon start and when user change occured.
*/
void permissionsChangedCB(string sDpUnused1, dyn_string dsUnused1, string sDpUnused2, dyn_string dsUnused2, string sDpUnused3, dyn_string dsUnused3)
{
  userChangedCB(&quot;&quot;,&quot;&quot;);
}

// ------------------------------------------------------------------------------------------------

/** Callback function that reloads the user's rights upon start and when user change occured.
*/
void userChangedCB(string sDpUnused, string sUserUnused)
{
  dyn_string exceptionInfo;

  unDeviceComment_getCommentPermissions(g_b32CommentPermissions, exceptionInfo);

  if(dynlen(exceptionInfo))
  {
    fwExceptionHandling_display(exceptionInfo);
    g_b32CommentPermissions = UN_DEVICE_COMMENT_NO_PERMISSION; // set permission to least privilege
  }

  bool bCanAddComment = unDeviceComment_hasCommentAddPermission(g_b32CommentPermissions);

  enableButtons();

}

void enableButtons(dyn_int diSelectedRows=makeDynInt())
{
  if ( 0 == dynlen(diSelectedRows) )
  {
    diSelectedRows = fwDeviceComment_commentsTable.getSelectedLines();
    fwDeviceComment_removeHiddenRows(diSelectedRows);
  }

  updateButtonAccess(diSelectedRows, getShape(&quot;activateButton&quot;), &quot;unDeviceComment_hasCommentDeactivateAllPermission&quot;);
  updateButtonAccess(diSelectedRows, getShape(&quot;deactivateButton&quot;), &quot;unDeviceComment_hasCommentDeactivateAllPermission&quot;);
  updateButtonAccess(diSelectedRows, getShape(&quot;deleteButton&quot;), &quot;unDeviceComment_hasCommentDeleteAllPermission&quot;);
}



/**
  Updates the state of the (de)activate/delete button based on the current user's privileges
*/
void updateButtonAccess(dyn_int diSelectedRows, shape shpButton, string sCheckPermisionModifyAllFcn)
{
  string sUser;
  bool bGranted = false;
  bool bCanModifyAllComments;

  if ( isFunctionDefined(sCheckPermisionModifyAllFcn) )
  {
    bCanModifyAllComments = callFunction(sCheckPermisionModifyAllFcn, g_b32CommentPermissions);
  }

  if(dynlen(diSelectedRows) &gt; 0)
  {

    if( bCanModifyAllComments ) //if the role of the user permits to (de)activate/delete all the comments
    {
      bGranted = TRUE;
    }
    else
    {
      //if the role of the user permits to add and to (de)activate/delete own comments
      if ( unDeviceComment_hasCommentAddPermission(g_b32CommentPermissions) )
      {
        bGranted = TRUE;

        // go through each selected row
        for(int index = 1; index &lt;= dynlen(diSelectedRows); index++)
        {
          sUser = fwDeviceComment_commentsTable.cellValueRC(diSelectedRows[index], fwDeviceComment_commentsTable.columnName(FW_DEVICE_COMMENT_TABLE_COLUMN_USER));

          // user has to be the author of every selected comment
          if( unDeviceComment_isNotAuthorOfComment(fwDeviceComment_commentsTable.cellValueRC(diSelectedRows[index], fwDeviceComment_commentsTable.columnName(FW_DEVICE_COMMENT_TABLE_COLUMN_USER))) )
          {
            bGranted = FALSE;
            break;
          }
        }
      }
      //the role of the user does not permit to add or (de)activate any comments
      else
      {
        bGranted = FALSE;
      }
    }

    shpButton.enabled(bGranted);
  }
  else
  {
    shpButton.enabled(FALSE);
  }
}

/**
  Delete or (de)activate the selected rows
*/
void changeSelectedRows(dyn_int diSelectedRows, string sOperation)
{
  int iPos, iResult, iCount;
  string sLogMessage, sCommentData, sDeviceDPE, sModuleName, sFWDCPanelName;
  mapping mapDPComments;
  dyn_string dsStoredComments, dsConfirmationBoxReturnValue, dsModuleSplit, dsChangedDevices, exceptionInfo, dsDecodedComment;
  dyn_anytype daRow;
  dyn_float dfConfirmationBoxReturnValue;
  dyn_errClass deError;

  // confirmation box. Are you sure?
  unGraphicalFrame_ChildPanelOnCentralModalReturn(&quot;vision/MessageInfo&quot;, &quot;Confirm&quot;,
          makeDynString(&quot;$1:&quot; + strtoupper(sOperation[0]) + substr(sOperation, 1, strlen(sOperation) - 1) + &quot; selected rows?&quot;,
          &quot;$2:OK&quot;, &quot;$3:Cancel&quot;),
          dfConfirmationBoxReturnValue, dsConfirmationBoxReturnValue);

  if (dynlen(dfConfirmationBoxReturnValue) &lt; 1 || dfConfirmationBoxReturnValue[1] != 1)
  // if user does not confirm the deletion(s)
  {
    fwDeviceComment_showUserFeedback(&quot;The user has cancelled the action&quot;);
    return;
  }

  // get the current module
  sModuleName = myModuleName();

  // split module name on semicolon
  dsModuleSplit = strsplit(sModuleName, &quot;;&quot;);

  // compose panel name: &quot;module_no - Device Comments&quot; --  see _unGraphicalFrame_getModuleId
  sFWDCPanelName = (char) dsModuleSplit[1][strlen(dsModuleSplit[1])-1] + &quot; - &quot; + FW_DEVICE_COMMENT_FWDC_PANEL_TITLE;

  // connect to a callback in unDeviceComments.pnl that updates the commentsTables
  uiConnect(sModuleName + &quot;.&quot; + sFWDCPanelName + &quot;:&quot;,
            FW_DEVICE_COMMENT_FWDC_PANEL_COMMCHANGE_CB,
            myModuleName() + &quot;.&quot; + myPanelName() + &quot;:&quot;,
            &quot;commentsChanged&quot;);

  // store the comments as values in a mapping whose keys are the device DPs
  for(int index = 1; index &lt;= dynlen(diSelectedRows); index++)
  {
    // get all values for this row from the table
    daRow = fwDeviceComment_commentsTable.getLineN(diSelectedRows[index]);

    sDeviceDPE = daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE];

    sCommentData = unDeviceComment_encodeCommentArray(makeDynString(
        (string) formatTime(UN_DEVICE_COMMENT_TIME_FORMAT, daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_TIMESTAMP]),
        daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_USER],
        daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT],
        daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_EXTENSION],
        daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_SYSTEM],
        daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_HOST],
        (string) daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_ACTIVE]));

    if ( mappingHasKey(mapDPComments, sDeviceDPE) )
    // if we have already seen the DP of this device
    {
      dynAppend(mapDPComments[sDeviceDPE].dsDeviceComments, sCommentData); // append comment to list
    }
    else
    // first time we encounter the DP of this device
    {
      strCommentInfo commentInfo;

      commentInfo.dsDeviceComments = makeDynString(sCommentData); // create comments list
      commentInfo.sDeviceAlias = daRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE];

      mapDPComments[sDeviceDPE] = commentInfo;
    }

  }

  //loop through the mapping keys, i.e. the device DPs, and remove the comments in the mapping values
  for (int i = 1; i &lt;= mappinglen(mapDPComments); i++)
  {
    fwDeviceComment_showProgress(iCount, dynlen(diSelectedRows)-1);
    // get the device DP, i.e. the curent mapping key
    sDeviceDPE = mappingGetKey(mapDPComments, i);

    if ( fwDeviceComment_getUserHasCancelled() )
    {
      DebugN(&quot;User cancelled - skipping the rest&quot;);
      break;
    }

    iResult = unDeviceComment_processCommentsInDP(sDeviceDPE, mapDPComments[sDeviceDPE].dsDeviceComments, sOperation);

    iCount++;

    switch (iResult)
    {
      case UN_DEVICE_COMMENT_NO_ERROR:
        // all is fine, nothing to do here
        break;

      case UN_DEVICE_COMMENT_CANNOT_DPGET_ERROR:
        ; //intentionall fallthrough to the next case, UN_DEVICE_COMMENT_INEXISTENT_DP_ERROR

      case UN_DEVICE_COMMENT_INEXISTENT_DP_ERROR:
        fwDeviceComment_showUserFeedback(&quot;Could not access comments for &quot; + mapDPComments[sDeviceDPE].sDeviceAlias);
        deError = makeError(&quot;&quot;,PRIO_WARNING,ERR_CONTROL,0,&quot;Could not access comments for &quot; + mapDPComments[sDeviceDPE].sDeviceAlias, &quot;no comments have beed &quot; + sOperation + &quot;d for this device.&quot;);
        break;

      case UN_DEVICE_COMMENT_CANNOT_DPSET_ERROR:
        fwDeviceComment_showUserFeedback(&quot;Could not update comments for &quot; + mapDPComments[sDeviceDPE].sDeviceAlias);
        deError = makeError(&quot;&quot;,PRIO_WARNING,ERR_CONTROL,0,&quot;Could not update comments for &quot; + mapDPComments[sDeviceDPE].sDeviceAlias, &quot;no comments have beed &quot; + sOperation + &quot;d for this device.&quot;);
        break;

      case UN_DEVICE_COMMENT_INEXISTENT_ERROR:
        fwDeviceComment_showUserFeedback(&quot;Could not update comments for &quot; + mapDPComments[sDeviceDPE].sDeviceAlias);
        deError = makeError(&quot;&quot;,PRIO_WARNING,ERR_CONTROL,0,&quot;Some comments in the device log of &quot; + mapDPComments[sDeviceDPE].sDeviceAlias + &quot; have been changed remotely&quot;, &quot;no comments have beed &quot; + sOperation + &quot;d for this device. Reloading...&quot;);
        dynAppend(dsChangedDevices, sDeviceDPE); // force reload of the comments for this device
        break;

      default:
        fwDeviceComment_showUserFeedback(&quot;An unknown error occured when processing &quot; + mapDPComments[sDeviceDPE].sDeviceAlias);
        deError = makeError(&quot;&quot;,PRIO_WARNING,ERR_CONTROL,0,&quot;An unknown error occured when processing &quot;  + mapDPComments[sDeviceDPE].sDeviceAlias);
        break;
    }

    if ( dynlen(deError) )
    // something went wrong
    {
      throwError(deError); //Shows the information message
      dynClear(deError);
      continue;
    }

    dynAppend(dsChangedDevices, sDeviceDPE);

    // create a log entry message
    sLogMessage = &quot; &quot; + sOperation + &quot;d &quot; + dynlen(mapDPComments[sDeviceDPE].dsDeviceComments) + &quot; comment(s) on &quot; + mapDPComments[sDeviceDPE].sDeviceAlias + &quot;.&quot;;

    // show feedback to the user
    fwDeviceComment_showUserFeedback(&quot;Successfully&quot; + sLogMessage);

    // make a log entry
    unSendMessage_toAllUsers(unDeviceComment_getFullUserName() + sLogMessage, exceptionInfo);

  }

  fwDeviceComment_showProgress(100, 100);


  // trigger a table delete and reload for the devices whose comments were deleted
  triggerEvent(&quot;commentsChanged&quot;, dsChangedDevices);

  // disconnect the event from the callback
  uiDisconnect(sModuleName + &quot;.&quot; + sFWDCPanelName + &quot;:&quot;,
            FW_DEVICE_COMMENT_FWDC_PANEL_COMMCHANGE_CB,
            myModuleName() + &quot;.&quot; + myPanelName() + &quot;:&quot;,
            &quot;commentsChanged&quot;);
}
]]></script>
  <script name="Close" isEscaped="1"><![CDATA[main()
{
  string sMessage = &quot;Do you want to close the device comments?&quot;;
  dyn_string dsReturn;
  dyn_float dfReturn;
  
  string sPanelName =   &quot;vision/unComment/unDeviceCommentConfirmationClose.pnl&quot;;
  
 
 
    unGraphicalFrame_ChildPanelOnCentralModalReturn(sPanelName, &quot;Confirm&quot;, 
                                          makeDynString(&quot;$1:&quot;+sMessage, &quot;$2:OK&quot;, &quot;$3:Cancel&quot;),
                                          dfReturn, dsReturn);

  bool bClose = true;
  
  if (dynlen(dfReturn) &gt;= 1)
  {
    if(dsReturn[1] == &quot;false&quot;) 
    {
      bClose = false;
    }
  }
  
  if ( bClose)
  {
      {
       PanelOff();        
      }
  } 
}]]></script>
  <script name="Initialize" isEscaped="1"><![CDATA[#uses &quot;fwDeviceComment/unDeviceComment.ctl&quot; //TODO move

const bool g_alertRow = false; //WHY?

main()
{
  initGUI(); // UI related
  unDeviceComment_init(); // business logic
}]]></script>
 </events>
 <shapes>
  <reference parentSerial="-1" Name="PANEL_REF1" referenceId="1">
   <properties>
    <prop name="FileName">fwDeviceComment/fwDeviceComment.pnl</prop>
    <prop name="Location">23 12</prop>
    <prop name="Geometry">1 0 0 1 5 1</prop>
    <prop name="TabOrder">4</prop>
    <prop name="dollarParameters">
     <prop name="dollarParameter">
      <prop name="Dollar">$sDeviceCommentClass</prop>
      <prop name="Value">unDeviceComment</prop>
     </prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <extended>
     <script name="deleteComments" isEscaped="1"><![CDATA[void deleteComments(dyn_int diSelectedCommentRows)
{
  changeSelectedRows(diSelectedCommentRows,&quot;delete&quot;);
}
]]></script>
     <script name="activateComments" isEscaped="1"><![CDATA[void activateComments(dyn_int diSelectedCommentRows)
{
   changeSelectedRows(diSelectedCommentRows,&quot;activate&quot;);
}
]]></script>
     <script name="commentRightClick" isEscaped="1"><![CDATA[void commentRightClick(dyn_string dsRow)
{
  unDeviceComment_rightClick(dsRow);
}
]]></script>
     <script name="commentDoubleClick" isEscaped="1"><![CDATA[void commentDoubleClick(dyn_string dsRow)
{
  unDeviceComment_doubleClick(dsRow);
}
]]></script>
     <script name="commentSelectionChanged" isEscaped="1"><![CDATA[// this function needs to be &quot;synchronized&quot;
synchronized void commentSelectionChanged(dyn_int diSelectedCommentRows)
{
  enableButtons(diSelectedCommentRows);
}
]]></script>
     <script name="deactivateComments" isEscaped="1"><![CDATA[void deactivateComments(dyn_int diSelectedCommentRows)
{
    changeSelectedRows(diSelectedCommentRows,&quot;deactivate&quot;);
}
]]></script>
    </extended>
   </properties>
  </reference>
  <reference parentSerial="-1" Name="fwDeviceComment_filter" referenceId="2">
   <properties>
    <prop name="FileName">fwDeviceComment/objects/fwDeviceFilter.pnl</prop>
    <prop name="Location">43 28</prop>
    <prop name="Geometry">1 0 0 1 -13 -8</prop>
    <prop name="TabOrder">5</prop>
    <prop name="layoutAlignment">AlignCenter</prop>
   </properties>
  </reference>
 </shapes>
</panel>
