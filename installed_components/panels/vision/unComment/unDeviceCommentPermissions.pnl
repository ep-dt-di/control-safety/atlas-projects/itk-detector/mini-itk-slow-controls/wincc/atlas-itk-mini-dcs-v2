<?xml version="1.0" encoding="UTF-8"?>
<panel version="14">
 <properties>
  <prop name="Name">
   <prop name="en_US.utf8">Generic access control for device comments</prop>
  </prop>
  <prop name="Size">691 475</prop>
  <prop name="BackColor">_3DFace</prop>
  <prop name="RefPoint">283.6756756756761 67.29629629629602</prop>
  <prop name="InitAndTermRef">True</prop>
  <prop name="SendClick">False</prop>
  <prop name="RefFileName"></prop>
  <prop name="DPI">96</prop>
  <prop name="layoutType">None</prop>
 </properties>
 <events>
  <script name="ScopeLib" isEscaped="1"><![CDATA[#uses &quot;fwDeviceComment/fwDeviceComment&quot;
#uses &quot;fwAccessControl/fwAccessControl&quot;
#uses &quot;fwGeneral/fwGeneral&quot;
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// GLOBAL VARIABLES
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

const int ERROR_CODE_PRIVILEGES_SAVED = 0;
const int ERROR_CODE_GET_DP_ERROR = -1;
const int ERROR_CODE_SET_DP_ERROR = -2;
const int ERROR_CODE_NO_CONFIRMATION = -3;
const int ERROR_CODE_PRIVILEGES_LOADED = -4;
const int ERROR_CODE_PRIVILEGE_LISTS_EMPTY = -5;


// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// GLOBAL VARIABLES
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

bool g_bCanConfigureCommentAccessRights = false; // used to enable/disable the UI elements
bool g_bCommentPrivilegesHaveChanged = false; // used to enable/disable the apply button

// ------------------------------------------------------------------------------------------------

/** Enables or disables the UI elements based on the access rights of the current user
@par Constraints
  None

@par Usage
	Private to the panel

@par WinCC-OA managers
  VISION

@param bEnabled   boolean, defines the new state of the UI elements
*/
void updatePanelElementsEnabledState(bool bCommPrivChanged=g_bCommentPrivilegesHaveChanged)
{
  addCommentPrivilegesPanel.enabled(g_bCanConfigureCommentAccessRights);
  deactivateCommentPrivilegesPanel.enabled(g_bCanConfigureCommentAccessRights);
  deleteCommentsPrivilegesPanel.enabled(g_bCanConfigureCommentAccessRights);
  configureCommentPrivilegesPanel.enabled(g_bCanConfigureCommentAccessRights);
  hiddenRemoteConfigButton.enabled(g_bCanConfigureCommentAccessRights);

  g_bCommentPrivilegesHaveChanged = bCommPrivChanged; // save new state

  resetButton.enabled(g_bCanConfigureCommentAccessRights &amp;&amp; g_bCommentPrivilegesHaveChanged);
  applyButton.enabled(g_bCanConfigureCommentAccessRights &amp;&amp; g_bCommentPrivilegesHaveChanged);
}

// ------------------------------------------------------------------------------------------------

/** Call-back function for fwAccessControl_setupPanel; called whenever a user logs in or out
@par Constraints
  None

@par Usage
	Private to the panel

@par WinCC-OA managers
  VISION

@param s1   string, imposed by callback function prototype, not used
@param s2   string, imposed by callback function prototype, not used
*/
void applyPanelAccessControl(string s1, string s2)
{
  bit32 b32CommentPermissions = UN_DEVICE_COMMENT_NO_PERMISSION;
  dyn_string exceptionInfo;

  //disable panel elements and setp up for least privilege access
  g_bCanConfigureCommentAccessRights = false;
  updatePanelElementsEnabledState();

  // retrieve all the permissions for device comments
  unDeviceComment_getCommentPermissions(b32CommentPermissions, exceptionInfo);
  if ( dynlen(exceptionInfo) ) //something went wrong
  {
    fwExceptionHandling_display(exceptionInfo);
    return;
  }

  // check if user has privilege to configure the access rights for the device comments
  g_bCanConfigureCommentAccessRights = unDeviceComment_hasConfigureCommentRightsPermission(b32CommentPermissions);

  // enable UI elements, depending on the access rights
  updatePanelElementsEnabledState();

  if ( false == g_bCanConfigureCommentAccessRights ) // if user has no access rights
  {
    loadPrivilegesfromDP(exceptionInfo); // reload privilege lists from DPs

    if ( dynlen(exceptionInfo) ) //something went wrong
    {
      fwExceptionHandling_display(exceptionInfo);
      return;
    }

    g_bCommentPrivilegesHaveChanged = false; // signal that no changes have been applied
  };

}

// ------------------------------------------------------------------------------------------------

/** Shows feedback to the user depending on what error has occured before triggered.

@par Constraints
  None

@par Usage
	Private to the panel

@par WinCC managers
	VISION

@param iErrorCode       int, INPUT, error code that determines feedback to be shown
*/
void showUserFeedback(int iErrorCode)
{
  switch(iErrorCode)
  {
    case ERROR_CODE_SET_DP_ERROR:
      userFeedbackLabel.text = &quot;Cannot access datapoint. Privileges not saved.&quot;;
      break;
    case ERROR_CODE_GET_DP_ERROR:
      userFeedbackLabel.text = &quot;Cannot access datapoint. Privileges not loaded.&quot;;
      break;
    case ERROR_CODE_NO_CONFIRMATION:
      userFeedbackLabel.text = &quot;Action aborted by user.&quot;;
      break;
    case ERROR_CODE_PRIVILEGES_LOADED:
      userFeedbackLabel.text = &quot;Privileges loaded.&quot;;
      break;
    case ERROR_CODE_PRIVILEGES_SAVED:
      userFeedbackLabel.text = &quot;Privileges saved.&quot;;
      break;
    case ERROR_CODE_PRIVILEGE_LISTS_EMPTY:
      userFeedbackLabel.text = &quot;Cannot save. All privilege lists are empty.&quot;;
      break;
    default:
      break;
  }

  userFeedbackLabel.visible(TRUE);
  userFeedbackArrow.visible(TRUE);

  // only display feedback message for 3 seconds
  delay(3);

  userFeedbackLabel.visible(FALSE);
  userFeedbackArrow.visible(FALSE);
}


// ------------------------------------------------------------------------------------------------

/** Ask the user various questions with binary output (OK/Cancel)

@par Constraints
  None

@par Usage
	Called by the 'Clicked' event of the 'Close' and 'Reset' buttons

@par WinCC managers
	VISION

*/
bool promptUser(string sPromptText)
{
  dyn_float  dfResult; // stores float result from dialog
  dyn_string dsResult; // stores string result from dialog
  bool bUserHasAcceptedPrompt = false;

  // prompt the user
  ChildPanelOnCentralModalReturn(&quot;fwGeneral/fwOkCancel.pnl&quot;,
                                 &quot;Warning&quot;,
                                 makeDynString(&quot;$text:&quot; + sPromptText),
                                 dfResult, dsResult);

  bUserHasAcceptedPrompt = (dynlen(dfResult) &gt; 0) &amp;&amp; (dynlen(dsResult) &gt; 0) &amp;&amp; (dfResult[1] &gt; 0.5)  &amp;&amp; (dsResult[1] == &quot;ok&quot;); // user clicked OK

  return bUserHasAcceptedPrompt;
}

// ------------------------------------------------------------------------------------------------

/** Load privileges from DPs

@par Constraints
  None

@par Usage
	Called on panel initialization and when &quot;Reset&quot; is clicked

@par WinCC managers
	VISION

*/
void loadPrivilegesfromDP(dyn_string &amp;exceptionInfo)
{
  dyn_string dsAddCommentPermissions;
  dyn_string dsDeleteAllCommentsPermissions;
  dyn_string dsDeactivateAllCommentsPermissions;
  dyn_string dsConfigureCommentRightsPermissions;
  int iRetrieveSucceeded = ERROR_CODE_PRIVILEGES_LOADED;

  // get all the comment rights as dyn_strings from the DPs
  unDeviceComment_getAllCommentAccessRights(dsAddCommentPermissions,
                                            dsDeleteAllCommentsPermissions,
                                            dsDeactivateAllCommentsPermissions,
                                            dsConfigureCommentRightsPermissions,
                                            exceptionInfo); //function call ends here

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    iRetrieveSucceeded = ERROR_CODE_GET_DP_ERROR; // something went wrong
	}

  if ( ERROR_CODE_PRIVILEGES_LOADED == iRetrieveSucceeded ) // if everything went ok, update the lists
  {
    addCommentPrivilegesPanel.setPrivileges(dsAddCommentPermissions);
    deleteCommentsPrivilegesPanel.setPrivileges(dsDeleteAllCommentsPermissions);
    deactivateCommentPrivilegesPanel.setPrivileges(dsDeactivateAllCommentsPermissions);
    configureCommentPrivilegesPanel.setPrivileges(dsConfigureCommentRightsPermissions);
  }

  startThread(&quot;showUserFeedback&quot;,iRetrieveSucceeded);
}


void initPanel()
{
  dyn_string exceptionInfo;

  //disable panel elements and setp up for least privilege access
  g_bCanConfigureCommentAccessRights = false;
  updatePanelElementsEnabledState();

  //register callback from
  fwAccessControl_setupPanel(&quot;applyPanelAccessControl&quot;,exceptionInfo);

  dpConnect(&quot;applyPanelAccessControl&quot;,
             false,
             UN_DEVICE_COMMENT_GLOBAL_SETTINGS + UN_DEVICE_COMMENT_ACCESS_CONTROL + UN_DEVICE_COMMENT_CONFIGURE_RIGHTS_PRIVILEGES
           );

  // check and handle exceptions that might have been thrown by the fwAccessControl_setupPanel function call
  if ( dynlen(exceptionInfo) )
  {
    fwAccessControl_displayException(exceptionInfo);
    return;
  }

  loadPrivilegesfromDP(exceptionInfo);

  if ( dynlen(exceptionInfo) ) //something went wrong
  {
    fwExceptionHandling_display(exceptionInfo);
    return;
  }

}]]></script>
  <script name="Close" isEscaped="1"><![CDATA[main()
{
  ; // use close button to exit
}]]></script>
  <script name="Terminate" isEscaped="1"><![CDATA[main()
{
  ; // use close button to exit
}]]></script>
  <script name="Initialize" isEscaped="1"><![CDATA[main()
{
  initPanel();
}
]]></script>
 </events>
 <shapes>
  <shape Name="resetButton" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">65</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">265.8131313131313 640</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">29</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Discard any changes made and reload access rights from the DPs.</prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">296.5 444</prop>
    <prop name="Size">99 25</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Reset</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  dyn_string exceptionInfo;

  if ( promptUser(&quot;All the changes that have not been applied will be lost!\nDo you wish to continue?&quot; ) ) // user wants to discard changes  
  {
    loadPrivilegesfromDP(exceptionInfo); // reload from DP

    if ( dynlen(exceptionInfo) ) //something went wrong
    {
      fwExceptionHandling_display(exceptionInfo);
      return;
    }
    
    updatePanelElementsEnabledState(false); // disable the apply button
  }
  else // user has changed his mind
  {
    startThread(&quot;showUserFeedback&quot;,ERROR_CODE_NO_CONFIRMATION);
  }
  
}
]]></script>
   </events>
  </shape>
  <shape Name="cancelButton" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">66</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">621.0858585858587 640</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">30</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Close the panel.</prop>
    </prop>
    <prop name="Hotkey">Esc</prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">580.5 444</prop>
    <prop name="Size">99 25</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Close</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  if ( g_bCommentPrivilegesHaveChanged &amp;&amp; false == promptUser(&quot;All the changes that have not been applied will be lost!\nDo you wish to continue?&quot;) )
  {
    startThread(&quot;showUserFeedback&quot;,ERROR_CODE_NO_CONFIRMATION);
    return;
  }
  PanelOff();
}]]></script>
   </events>
  </shape>
  <reference parentSerial="-1" Name="addCommentPrivilegesPanel" referenceId="8">
   <properties>
    <prop name="FileName">vision/unComment/unDeviceCommentPrivilegeAdd.pnl</prop>
    <prop name="Location">18 51.3</prop>
    <prop name="Geometry">1 0 0 1 -8 -21.3</prop>
    <prop name="TabOrder">35</prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <extended>
     <prop type="CHAR_STRING" name="frameText">Add, (de)activate and delete own comments</prop>
     <script name="privilegesChanged" isEscaped="1"><![CDATA[void privilegesChanged(bool bPrivilegesHaveChanged)
{
  updatePanelElementsEnabledState(bPrivilegesHaveChanged); // enable/disable the apply button
}
]]></script>
    </extended>
   </properties>
  </reference>
  <reference parentSerial="-1" Name="deactivateCommentPrivilegesPanel" referenceId="9">
   <properties>
    <prop name="FileName">vision/unComment/unDeviceCommentPrivilegeAdd.pnl</prop>
    <prop name="Location">8.999999999999773 219.2000000000002</prop>
    <prop name="Geometry">1 0 0 1 2.500000000000185 0.7999999999998124</prop>
    <prop name="TabOrder">36</prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <extended>
     <prop type="CHAR_STRING" name="frameText">Activate / deactivate any comment</prop>
     <script name="privilegesChanged" isEscaped="1"><![CDATA[void privilegesChanged(bool bPrivilegesHaveChanged)
{
  updatePanelElementsEnabledState(bPrivilegesHaveChanged); // enable/disable the apply button
}
]]></script>
    </extended>
   </properties>
  </reference>
  <reference parentSerial="-1" Name="deleteCommentsPrivilegesPanel" referenceId="10">
   <properties>
    <prop name="FileName">vision/unComment/unDeviceCommentPrivilegeAdd.pnl</prop>
    <prop name="Location">9.659218598450309 409.888888888889</prop>
    <prop name="Geometry">1 0 0 1 340.3407814015497 -379.888888888889</prop>
    <prop name="TabOrder">37</prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <extended>
     <prop type="CHAR_STRING" name="frameText">Delete any comment</prop>
     <script name="privilegesChanged" isEscaped="1"><![CDATA[void privilegesChanged(bool bPrivilegesHaveChanged)
{
  updatePanelElementsEnabledState(bPrivilegesHaveChanged); // enable/disable the apply button
}
]]></script>
    </extended>
   </properties>
  </reference>
  <shape Name="applyButton" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">87</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">40 450</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">38</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Save all changes made to the access rights.</prop>
    </prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">12 444</prop>
    <prop name="Size">100 25</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Apply</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  // store privileges to the DPs 
 
  dyn_string dsAddCommentPermissions;
  dyn_string dsDeleteAllCommentsPermissions;
  dyn_string dsDeactivateAllCommentsPermissions;
  dyn_string dsConfigureCommentRightsPermissions;
  dyn_string exceptionInfo;
  bool bOperationSucceeded;
  bool bRetry = true;

  // get add comment privileges
  addCommentPrivilegesPanel.getPrivileges(dsAddCommentPermissions);

  // get delete all comments privileges
  deleteCommentsPrivilegesPanel.getPrivileges(dsDeleteAllCommentsPermissions);
  
  // get deactivate all comments privileges
  deactivateCommentPrivilegesPanel.getPrivileges(dsDeactivateAllCommentsPermissions);

  // get deactivate all comments privileges
  configureCommentPrivilegesPanel.getPrivileges(dsConfigureCommentRightsPermissions);

  // check if the access right privileges for device comments have been properly set up
  // warn user if the lists are empty
  
  if ( (0 == dynlen(dsAddCommentPermissions)) &amp;&amp;
       (0 == dynlen(dsDeactivateAllCommentsPermissions)) &amp;&amp;
       (0 == dynlen(dsDeleteAllCommentsPermissions)) &amp;&amp;
       (0 == dynlen(dsConfigureCommentRightsPermissions)) ) // end if condition
  {
    if ( false == promptUser(&quot;All the privilege lists are empty!\nAre you sure you want to continue?&quot;) )
    {
      return;
    }
  }
  
  while ( true == bRetry )
  {
    bOperationSucceeded = true;  // assume everything went ok


    unDeviceComment_storeAllCommentsRightsInDP(dsAddCommentPermissions,
                                               dsDeleteAllCommentsPermissions,
                                               dsDeactivateAllCommentsPermissions,
                                               dsConfigureCommentRightsPermissions,
                                               exceptionInfo);

  	if (dynlen(exceptionInfo)) // check if everything went smoothly
    {
      fwExceptionHandling_display(exceptionInfo);
      bOperationSucceeded = false;
  	}


    if ( false == bOperationSucceeded ) // something went wrong
    {
      startThread(&quot;showUserFeedback&quot;,ERROR_CODE_SET_DP_ERROR);


      //ask user if we should retry storing the privileges to the DPs
      bRetry = promptUser(&quot;There was an error while trying to store the access rights!\nDo you wish to retry?&quot;);

      if ( false == bRetry )
      {
        startThread(&quot;showUserFeedback&quot;,ERROR_CODE_NO_CONFIRMATION);
      }
      
    }    
    else // if everything was OK
    {
      bRetry = false; // exit while loop
      startThread(&quot;showUserFeedback&quot;,ERROR_CODE_PRIVILEGES_SAVED);      
      updatePanelElementsEnabledState(false); // disable the apply button
    }
    
  } // end while 
  
}
]]></script>
   </events>
  </shape>
  <shape Name="bgRectangle" shapeType="RECTANGLE" layerId="0">
   <properties>
    <prop name="serialId">105</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">20 530</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_Transparent</prop>
    <prop name="BackColor">FwCorporateColor</prop>
    <prop name="TabOrder">42</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="Geometry">0.5563100447663599 0 0 1 -5.401136763744277 -2.842170943040401e-14</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Location">-1 -1</prop>
    <prop name="Size">1270 31</prop>
    <prop name="CornerRadius">0</prop>
    <prop name="Transformable">True</prop>
   </properties>
  </shape>
  <shape Name="title" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">107</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">14.29255319148928 3.999999999999972</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">white</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">43</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">14.29255319148928 3.999999999999972</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,19,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Device comment: access rights</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="userFeedbackLabel" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">124</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">21.00000000000004 420</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">False</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">_Transparent</prop>
    <prop name="TabOrder">44</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinMiter,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">23.00000000000003 422</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">User information</prop>
    </prop>
    <prop name="Distance">0</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="userFeedbackArrow" shapeType="POLYGON" layerId="0">
   <properties>
    <prop name="serialId">125</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 770</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">False</prop>
    <prop name="ForeColor">_Transparent</prop>
    <prop name="BackColor">_3DText</prop>
    <prop name="TabOrder">45</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="Geometry">0.725925925925926 0 0 0.6825000000000001 3.637037037037132 -102.785</prop>
    <prop name="Closed">True</prop>
    <prop name="Points">
     <prop name="Location">10 770</prop>
     <prop name="Location">10 790</prop>
     <prop name="Location">20 780</prop>
     <prop name="Location">10 770</prop>
     <prop name="Location">10 790</prop>
    </prop>
   </properties>
  </shape>
  <reference parentSerial="-1" Name="configureCommentPrivilegesPanel" referenceId="14">
   <properties>
    <prop name="FileName">vision/unComment/unDeviceCommentPrivilegeAdd.pnl</prop>
    <prop name="Location">9.659218598450309 409.888888888889</prop>
    <prop name="Geometry">1 0 0 1 340.3407814015497 -189.888888888889</prop>
    <prop name="TabOrder">46</prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <extended>
     <prop type="CHAR_STRING" name="frameText">Configure comment access rights</prop>
     <script name="privilegesChanged" isEscaped="1"><![CDATA[void privilegesChanged(bool bPrivilegesHaveChanged)
{
  updatePanelElementsEnabledState(bPrivilegesHaveChanged); // enable/disable the apply button
}
]]></script>
    </extended>
   </properties>
  </reference>
  <shape Name="hiddenRemoteConfigButton" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">146</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">147.4115691489362 449.5</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">False</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">46</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="Hotkey">Ctrl+Shift+R</prop>
    <prop name="layoutAlignment">AlignNone</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">147.4115691489362 445.5</prop>
    <prop name="Size">61 22</prop>
    <prop name="BorderStyle">Styled</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Hidden!</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(mapping event)
{
  ChildPanelOnCentral(&quot;vision/unComment/unDeviceCommentRemoteConfig.pnl&quot;, &quot;Remote configuration of FDWC acess rights&quot;, makeDynString());
}]]></script>
   </events>
  </shape>
 </shapes>
 <groups>
  <group parentSerial="-1" Name="userFeedbackGroup" serial="13">
   <properties>
    <prop name="shapeSerial">124</prop>
    <prop name="shapeSerial">125</prop>
    <prop name="layoutAlignment">AlignNone</prop>
   </properties>
  </group>
 </groups>
</panel>
