<?xml version="1.0" encoding="UTF-8"?>
<panel version="14">
 <properties>
  <prop name="Name">
   <prop name="en_US.utf8">(NoName)</prop>
   
  </prop>
  <prop name="Size">704 59</prop>
  <prop name="BackColor">_3DFace</prop>
  <prop name="RefPoint">0 0</prop>
  <prop name="InitAndTermRef">True</prop>
  <prop name="SendClick">False</prop>
  <prop name="RefFileName"></prop>
  <prop name="DPI">96</prop>
  <prop name="layoutType">None</prop>
 </properties>
 <events>
  <script name="ScopeLib" isEscaped="1"><![CDATA[#uses &quot;unRcpDbFunctions.ctl&quot;
#uses &quot;fwGeneral/fwGeneral.ctl&quot;
#uses &quot;unDistributedControl/unDistributedControl.ctl&quot;
#uses &quot;unGenericFunctions_core.ctl&quot;
#uses &quot;unGenericDpFunctions.ctl&quot;
#uses &quot;unGenericObject.ctl&quot;
#uses &quot;unGraphicalFrame.ctl&quot;
#uses &quot;unRcpFunctions.ctl&quot;
#uses &quot;unRcpFunctions_privileges.ctl&quot;
#uses &quot;unRcpFunctions_utilities.ctl&quot;
#uses &quot;unRcpFunctions_instance.ctl&quot;
#uses &quot;unRcpFunctions_class.ctl&quot;
#uses &quot;unicos_declarations.ctl&quot;

dyn_string g_dsUserAccess;	                  // Current user access
string g_sFaceplateButtonType = &quot;UnRcpClass&quot;; // Faceplate type
bool g_bCallbackConnected = false;

//------------------------------------------------------------------------------------------------------------------------

/** 
 * Create a new recipe class.
 */
void createRecipeClass()
{
  string sRcpClassDp, sClassName, sClassDesc, sPcoLink, sApplication;
  dyn_string exceptionInfo, ds;
  dyn_float df;
  
  getMultiValue(&quot;PcoList&quot;, &quot;selectedText&quot;, sPcoLink,
                &quot;ApplicationList&quot;, &quot;selectedText&quot;, sApplication);
  
  // Ask for the recipe class data 
  ChildPanelOnCentralReturn(&quot;vision/unRecipe/UnRcpClass/unUnRcpClass_NewRcpClass.pnl&quot;,
			&quot;Create Recipe Class&quot;,
			makeDynString(&quot;$sTitle:Create Recipe Class &quot;,
				      &quot;$sDescription: Input the data for the new recipe class&quot;,
            &quot;$sPcoLink:&quot; + sPcoLink,
            &quot;$sApplication:&quot; + sApplication),
			df,ds);
  if (!dynlen(df) || df[1]&lt;0) return;
  if (!dynlen(ds) || ds[1]==&quot;&quot;) {
    unRecipeFunctions_writeInRecipeLog(&quot;Error: The recipe class name can't be empty.&quot;);
    return;     
  }  			
  
  sClassName = ds[1];
  sClassDesc = ds[2];
  sPcoLink   = ds[3];
  
  if (!unRecipeFunctions_isLegalRecipeName(sClassName)) {
    unRecipeFunctions_writeInRecipeLog(&quot;Error: Wrong recipe class name: &quot; + sClassName);
    return;  
  }  
  
  if (sPcoLink == &quot;&quot;) {
    unRecipeFunctions_writeInRecipeLog(&quot;Error: The PCO link can't be empty.&quot;);
    return;  
  }

  // Create the new recipe class
  _unRecipeFunctions_createRecipeClass(sClassName, sClassDesc, sPcoLink, sRcpClassDp, exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    unRecipeFunctions_writeInRecipeLog(&quot;Error: &quot; + exceptionInfo[2]);
    DebugTN(exceptionInfo); 
    return; 
  }  

  if (PcoList.text != sPcoLink){
    PcoList.text(sPcoLink);
    unRecipeFunctions_loadRecipeClasses(sPcoLink, exceptionInfo, FALSE);
  }
  unRecipeFunctions_selectRecipeClass(sRcpClassDp, exceptionInfo, FALSE);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Duplicate the current recipe class.
 */
void DuplicateRecipeClass() {
  string sPcoLink, sRcpClassDp, sClassName, sClassDesc, sRcpType, sOrigClassName, sApplication;
  dyn_string exceptionInfo, ds, dsRcpClassElements, dsDeviceOrder;
  dyn_mixed recipeClassInfo;
  dyn_float df;  
  
  // Get the pco link of the current recipe class
  dpGet($sDpName + &quot;.ProcessInput.ClassName&quot;, sOrigClassName,
        $sDpName + &quot;.statusInformation.deviceLink&quot;, sPcoLink,
        $sDpName + &quot;.ProcessInput.DeviceOrder&quot;, dsDeviceOrder);  

  sApplication = unGenericDpFunctions_getApplication($sDpName);

  // Ask for the recipe class data 
  ChildPanelOnCentralReturn(&quot;vision/unRecipe/UnRcpClass/unUnRcpClass_NewRcpClass.pnl&quot;,
			&quot;Create Recipe Class&quot;,
			makeDynString(&quot;$sTitle:Create Recipe Class &quot;,
				      &quot;$sDescription: Input the data for the new recipe class&quot;,
            &quot;$sPcoLink:&quot;+sPcoLink,
            &quot;$sApplication:&quot;+sApplication),
			df,ds);
  
  if (!dynlen(df) || df[1]&lt;0) return;
  if (!dynlen(ds) || ds[1]==&quot;&quot;) {
    unRecipeFunctions_writeInRecipeLog(&quot;Error: The recipe class name can't be empty.&quot;);
    return;     
  }  		 
  
  // Add the recipe class elements
  unRecipeFunctions_getRecipeClassObject($sDpName, recipeClassInfo, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(&quot;Exception getting original recipe class &quot; + sOrigClassName, exceptionInfo);
  } 
  
  //dsRcpClassElements = recipeClassInfo[fwConfigurationDB_RCL_ELEMENTS];
  for (int i=1; i&lt;=dynlen(recipeClassInfo[fwConfigurationDB_RCL_ELEMENTS]); i++) {
    dyn_string deviceSplit = strsplit(recipeClassInfo[fwConfigurationDB_RCL_ELEMENTS][i], &quot;.&quot;);
    string sDeviceElement = deviceSplit[1] + &quot;.&quot; + deviceSplit[dynlen(deviceSplit)];
    dynAppend(dsRcpClassElements, sDeviceElement);
  }  
  
  sRcpType = recipeClassInfo[fwConfigurationDB_RCL_RECIPETYPE];
  
  sClassName = ds[1];
  sClassDesc = ds[2];
  sPcoLink   = ds[3];

  if (!unRecipeFunctions_isLegalRecipeName(sClassName)) {
    unRecipeFunctions_writeInRecipeLog(&quot;Error: Wrong recipe class name: &quot; + sClassName);
    return;  
  }  
    
  // Create the new recipe class
  _unRecipeFunctions_createRecipeClass(sClassName, sClassDesc, sPcoLink, sRcpClassDp, exceptionInfo, dsRcpClassElements);
  if (dynlen(exceptionInfo)) { 
    unRecipeFunctions_writeInRecipeLog(&quot;Error: &quot; + exceptionInfo[2]);
    DebugTN(exceptionInfo); 
    return; 
  }    
  
  dpSet(sRcpClassDp + &quot;.ProcessInput.DeviceOrder&quot;, dsDeviceOrder);
}

//------------------------------------------------------------------------------------------------------------------------
void RemoveRecipeClass() {
  int iRetVal;
  string sClassName, sInitialRcpDp;
  dyn_string ds, exceptionInfo;
  dyn_float df;

  
  // Get the recipe class name
  sClassName = dpGetAlias($sDpName + &quot;.&quot;);
  
  // Get the initial recipe of the recipe class
  iRetVal = _unRecipeFunctions_getInitialRecipeOfClass($sDpName, sInitialRcpDp, exceptionInfo);

  if (iRetVal == RCP_INITIAL_OK) {
    ChildPanelOnCentralReturn(&quot;fwGeneral/fwMessageInfo1.pnl&quot;,
					&quot;Delete Recipe Class&quot;,
         makeDynString(&quot;$1:The recipe class has initial recipes and it can not be deleted.&quot;),
					df,
					ds);
  } else {
     // Asks for user confirmation
    ChildPanelOnCentralReturn(&quot;fwGeneral/fwOkCancel.pnl&quot;,
							&quot;Delete Recipe Class&quot;,
							makeDynString(&quot;$sTitle:Delete recipe class &quot;,
										  &quot;$text:Do you want to delete the recipe class &quot; + sClassName + &quot; \nand all its instances?&quot;  ),
							df,
							ds);
  
    if (df[1]&lt;=0)  {
  	  dpSet($sDpName+&quot;.ProcessInput.State&quot;, &quot;&quot;);
  	  return;
    }
    
    // Delete the recipe class and all its instances
    unRecipeFunctions_deleteRecipeClass($sDpName, exceptionInfo);
  }
}
//------------------------------------------------------------------------------------------------------------------------
void DatabaseValues(string id) {
  time validAt;
  dyn_float df;
  dyn_string ds;
    
  if (id == &quot;Latest&quot;) {
    // Load the latest values of the recipe available in the DB
    validAt = 0;
  } else if (id == &quot;History&quot;) {
    DebugTN(&quot;Not implemented!&quot;);
    return;
/*    ChildPanelOnCentralModalReturn(&quot;vision/unRecipe/UnRcpCommon/unRcp_DatabaseHistory.pnl&quot;,
                                 &quot;Select Recipe&quot;, makeDynString(&quot;$sDpName:&quot;+$sDpName), df, ds);

    if (!dynlen(ds) || !dynlen(df) || df[1]!=1.0) {
      return;
    }    
    
    time t=ds[1];
    */
  }

  // Asks for user confirmation
  ChildPanelOnCentralReturn(&quot;fwGeneral/fwOkCancel.pnl&quot;,
							&quot;Load recipe values from DB&quot;,
							makeDynString(&quot;$sTitle:Load recipe values from DB &quot;,
										  &quot;$text:The recipe values will be loaded from the database for all the \n&quot;
                          + &quot;recipe instances. Do you want to continue?&quot;  ),
							df,
							ds);

  if (!dynlen(ds) || !dynlen(df) || df[1]!=1.0) {
    return;
  }  
    
  unRecipeFunctions_loadRecipeClassInstancesFromDb($sDpName + &quot;.&quot;);  
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Open a new window to edit the DPEs of the devices included in the recipe.
 * @param sRcpClassDp - [IN] The recipe class DP.
 */
void EditRecipeClassDeviceElements(string sRcpClassDp) {
  dyn_float df;
  dyn_string ds;
  string sSelectedDevices = &quot;&quot;, sSelectedTypeName, sDescription, sAlias, sState;
  bool bAutoSelectionTypeError = false;
  
  // Get the recipe class state
  dpGet(sRcpClassDp + &quot;.ProcessInput.State&quot;, sState);
  if (sState == &quot;&quot;) {
    dpSet(sRcpClassDp + &quot;.ProcessInput.State&quot;, UN_RCP_CLASS_STATE_EDIT);
  }
  
  sDescription = dpGetDescription(sRcpClassDp+&quot;.&quot;);
  getSelectedDevices(sSelectedDevices, sSelectedTypeName, bAutoSelectionTypeError);  
  sAlias = dpGetAlias(sRcpClassDp + &quot;.&quot;);

  dyn_string dsParams = makeDynString(
      &quot;$sDpName:&quot; + sRcpClassDp,
      &quot;$sSelectedDevices:&quot; + sSelectedDevices,
      &quot;$sDescription:&quot; + sDescription);
  
  if (!bAutoSelectionTypeError) {
    dynAppend(dsParams, &quot;$sSelectedType:&quot; + sSelectedTypeName);
  } else {
    unRecipeFunctions_writeInRecipeLog(&quot;Warning: Device auto selection is not available when multiple device types are selected.&quot;);
  }
  
  ChildPanelOnCentralModalReturn(
    &quot;vision/unRecipe/UnRcpClass/unUnRcpClass_EditDeviceElements.pnl&quot;,
    sAlias + &quot; - Edit recipe elements&quot;,
    dsParams,
    df, ds);

  // Check if the user pressed the cancel button
  if (dynlen(df)&gt;0 &amp;&amp; df[1]==0) {
    dpSet(sRcpClassDp + &quot;.ProcessInput.State&quot;, sState);
    return;
  }
		
  // If the user clicked the Ok button, reload the recipe class data
  if(dynlen(df)&gt;0 &amp;&amp; df[1]==1) {
    reloadRecipeClassData(ds);
  } else {
    // Restore the recipe class state
    dpSet(sRcpClassDp + &quot;.ProcessInput.State&quot;, sState);	
  }	
}
//------------------------------------------------------------------------------------------------------------------------
private void getSelectedDevices(string &amp;sSelectedDevices, string &amp;sSelectedTypeName, bool &amp;bAutoSelectionTypeError) {
  dyn_int diLines;
  string sDeviceNameCellValue, sDeviceTypeCellValue;
  
  getValue(&quot;RCCElements.RecipeElementsTable&quot;, &quot;getSelectedLines&quot;, diLines);
  int iLen = dynlen(diLines);
  
  for (int i=1; i&lt;=iLen; i++) {
    getMultiValue(&quot;RCCElements.RecipeElementsTable&quot;, &quot;cellValueRC&quot;, diLines[i], &quot;DeviceName&quot;, sDeviceNameCellValue,
                  &quot;RCCElements.RecipeElementsTable&quot;, &quot;cellValueRC&quot;, diLines[i], &quot;DeviceType&quot;, sDeviceTypeCellValue);
    if (i&gt;1) {
      sSelectedDevices += &quot;,&quot;;
      if (sDeviceTypeCellValue != sSelectedTypeName) {
        // The selected devices are of different device types, auto selection is not possible
        bAutoSelectionTypeError = true;
      }
    } else {
      sSelectedTypeName = sDeviceTypeCellValue;
    }
    
    sSelectedDevices += sDeviceNameCellValue;
  }
}
//------------------------------------------------------------------------------------------------------------------------
private void reloadRecipeClassData(dyn_string ds) {
    int iDeviceTypeColumn, iAliasColumn, iElementsColumn, iRows, iCols;
    string sDpes;
    dyn_dyn_string dsData;
    mapping mDeviceAlias;
    
    for (int i=1; i&lt;=dynlen(ds); i++) {
      string sRcpElement = ds[i];
      dyn_string elementSplit = strsplit(sRcpElement, &quot;.&quot;);
      string sDpe  = elementSplit[(dynlen(elementSplit))];  
      unRecipeFunctions_mappingInsertValue(mDeviceAlias, elementSplit[1], sDpe);
    }
    
    // Get the data from the recipe elements table
    getMultiValue(&quot;RCCElements.RecipeElementsTable&quot;, &quot;lineCount&quot;, iRows,
                  &quot;RCCElements.RecipeElementsTable&quot;, &quot;columnCount&quot;, iCols,
                  &quot;RCCElements.RecipeElementsTable&quot;, &quot;nameToColumn&quot;, &quot;DeviceType&quot;, iDeviceTypeColumn,
                  &quot;RCCElements.RecipeElementsTable&quot;, &quot;nameToColumn&quot;, &quot;DeviceName&quot;, iAliasColumn,
                  &quot;RCCElements.RecipeElementsTable&quot;, &quot;nameToColumn&quot;, &quot;DeviceElements&quot;, iElementsColumn);
                  
    for (int i=0; i&lt;iCols; i++) {
      getValue(&quot;RCCElements.RecipeElementsTable&quot;, &quot;getColumnN&quot;, i, dsData[(i+1)]);
    }    

    // Set the DPEs for each device
    for (int i=0; i&lt;iRows; i++) {
      string sAlias = dsData[(iAliasColumn+1)][(i+1)];
      fwGeneral_dynStringToString(mDeviceAlias[sAlias], sDpes, &quot;, &quot;);
      dsData[(iElementsColumn+1)][(i+1)] = sDpes;
    }

    // Fill the data in the table
    setValue(&quot;RCCElements.RecipeElementsTable&quot;, &quot;deleteAllLines&quot;);
    setValue(&quot;RCCElements.RecipeElementsTable&quot;, &quot;appendLines&quot;, iRows, 
				&quot;DeviceType&quot;, dsData[(iDeviceTypeColumn+1)],
				&quot;DeviceName&quot;, dsData[(iAliasColumn+1)],
				&quot;DeviceElements&quot;, dsData[(iElementsColumn+1)]);

    unRecipeFunctions_writeInRecipeLog(&quot;The recipe class elements have been modified. Please apply the changes to the recipe class.&quot;);
    setValue(&quot;applyChangesFrame&quot;, &quot;visible&quot;, true);
}]]></script>
 </events>
 <layers>
  <layer layerId="0">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer1</prop>
  </layer>
  <layer layerId="1">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer2</prop>
  </layer>
  <layer layerId="2">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer3</prop>
  </layer>
  <layer layerId="3">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer4</prop>
  </layer>
  <layer layerId="4">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer5</prop>
  </layer>
  <layer layerId="5">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer6</prop>
  </layer>
  <layer layerId="6">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer7</prop>
  </layer>
  <layer layerId="7">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer8</prop>
  </layer>
 </layers>
 <shapes>
  <shape Name="buttonApplyChanges" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">0</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">485 1.999999999999991</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_3DText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">38</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Apply changes</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">504 2</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Apply Changes</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  unRecipeFunctions_applyRecipeClassModifications($sDpName);
}]]></script>
   </events>
  </shape>
  <shape Name="Text1" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">1</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">607 31</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">False</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">{255,255,255}</prop>
    <prop name="TabOrder">10</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinMiter,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="Location">609 33</prop>
    <prop name="Size">57 17</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,40,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">INIT</prop>
     
    </prop>
    <prop name="Distance">0</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">True</prop>
    <prop name="Fit">False</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
   <events>
    <script name="Initialize" isEscaped="1"><![CDATA[// Parameter: $sDpName, string, device name
// Global variables

main()
{
  dyn_string exceptionInfo;

  unGenericObject_ButtonInit($sDpName, g_sFaceplateButtonType, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonPrivileges" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">2</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">254.666652096643 30</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_3DText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">22</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">300 30</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  Privileges ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  unRecipeFunctions_editRecipeClassPrivileges($sDpName);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonAddDevices" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">3</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">200 1.999999999999999</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">28</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Add new devices to the recipe class</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">200 2</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  Add Devices ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  unRecipeFunctions_addRecipeClassDevices($sDpName);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonNewInstance" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">4</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">100 1.999999999999999</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">29</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Create a new recipe instance</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">100 2</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  New Instance ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  unRecipeFunctions_createRecipeInstance();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonRemoveSelected" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">5</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">200 30</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">36</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Remove the selected recipe class elements</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">200 30</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  Remove Sel...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  string panelName;
  dyn_int diSelLines;
  dyn_string dsReturn;
  dyn_float dfReturn;
  
  // Get the number of devices selected in the table
  getValue(&quot;RCCElements.RecipeElementsTable&quot;, &quot;getSelectedLines&quot;, diSelLines);
  if (dynlen(diSelLines)==0) {
    unRecipeFunctions_writeInRecipeLog(&quot;There are no recipe elements selected.&quot;);
    return;
  }
  
  // Ask confirmation before removing the recipe elements
  unGraphicalFrame_ChildPanelOnCentralModalReturn(
        &quot;vision/MessageInfo&quot;,
        panelName,
        makeDynString(&quot;$1:The selected recipe elements will be removed. Do you want to continue?&quot;, &quot;$2:Ok&quot;,&quot;$3:Cancel&quot;), 
        dfReturn, 
        dsReturn);
  
  if(dynlen(dfReturn)&gt;0 &amp;&amp; dfReturn[1]==1)
  {
    unRecipeFunctions_removeSelectedRecipeClassDevices($sDpName);
  }
}]]></script>
   </events>
  </shape>
  <shape Name="buttonNewClass" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">6</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">0 1.999999999999999</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">40</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Create a new recipe class</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">0 2</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  New Class ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  createRecipeClass();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonCancel" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">7</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">484.9999999999999 30</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">42</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">504 30</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Cancel</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  bool bForceReload = TRUE;
  dyn_string exceptionInfo;
  
  dpSet($sDpName + &quot;.ProcessInput.State&quot;, &quot;&quot;);
  unRecipeFunctions_selectRecipeClass($sDpName, exceptionInfo, bForceReload);    
}]]></script>
   </events>
  </shape>
  <shape Name="buttonEditElements" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">8</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">100 30</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">44</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Edit the recipe elements of the selected devices</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">100 30</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  Edit Elements ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  EditRecipeClassDeviceElements($sDpName);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonRemoveClass" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">9</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">0 30</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">46</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Remove the recipe class</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">0 30</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Remove Class ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  RemoveRecipeClass();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonSelect" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">10</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">559 30</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">47</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">604 30</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Select</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  dyn_string exceptionInfo;
  bool bSelect;
  
  if (dpExists($sDpName)) {
    bSelect = (this.text == UN_FACEPLATE_BUTTON_SELECT_TEXTSELECT);
    unGenericObject_ButtonSelectAction($sDpName, bSelect, exceptionInfo);
  }
}]]></script>
    <script name="Initialize" isEscaped="1"><![CDATA[
bool g_bFirst = false;

//------------------------------------------------------------------------------------------------------------------------

main()
{
  bool bRes, bConnected;
  dyn_string exceptionInfo;
  string deviceSystemName;
  string sDeviceType;

  if ($sDpName == &quot;&quot; || !dpExists($sDpName))
    return;
  
  sDeviceType = dpTypeName($sDpName);
  if (sDeviceType == UN_CONFIG_INTERNAL_UNRCPCLASS_DPT_NAME) {
    return;
  }

  deviceSystemName = unGenericDpFunctions_getSystemName($sDpName);
  unDistributedControl_register(&quot;unRecipeFunctions_registerSelectButtonCB&quot;, bRes, bConnected, deviceSystemName, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
]]></script>
   </events>
  </shape>
  <shape Name="buttonDuplicate" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">11</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">254.666652096643 1.999999999999999</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_3DText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">56</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Duplicate the recipe class</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">300 2</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  Duplicate ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  DuplicateRecipeClass();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonSaveToDb" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">12</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">355 1.999999999999999</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">57</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Save recipe class and all the recipe instances to the database</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">400 2</prop>
    <prop name="Size">102 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Save to DB ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  dyn_string exceptionInfo;
  unRecipeFunctions_saveRecipeToDb($sDpName+&quot;.&quot;, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="PUSH_BUTTON3" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">14</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">559 1.999999999999999</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">58</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">604 2</prop>
    <prop name="Size">98 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8"></prop>
     
    </prop>
   </properties>
  </shape>
  <shape Name="buttonLoadFromDb" shapeType="CASCADE_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">16</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">400 30</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">60</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Load the recipe class and all its instances from the database</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">400 30</prop>
    <prop name="Size">102 26</prop>
    <prop name="PopupItemId"></prop>
    <prop name="Text">
     <prop name="en_US.utf8">Load from DB..</prop>
     
    </prop>
    <prop name="PopupMenu">
     <prop name="PopupItem">
      <prop name="PopupItemId">Latest</prop>
      <prop name="Text">
       <prop name="en_US.utf8">Latest ...</prop>
       
      </prop>
     </prop>
     <prop name="PopupItem">
      <prop name="PopupItemId">History</prop>
      <prop name="Text">
       <prop name="en_US.utf8">History ...</prop>
       
      </prop>
     </prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(string id)
{
  DatabaseValues(id);
}]]></script>
   </events>
  </shape>
  <shape Name="applyChangesFrame" shapeType="RECTANGLE" layerId="1">
   <properties>
    <prop name="serialId">13</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">500 30</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">&lt;{0,0,0},5,_3DFace,5></prop>
    <prop name="BackColor">_Transparent</prop>
    <prop name="TabOrder">54</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,2]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="Geometry">1.041666666666667 0 0 1.166666666666667 93.62499999999986 -0.1666666666666669</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Location">393 1</prop>
    <prop name="Size">97 25</prop>
    <prop name="CornerRadius">0</prop>
    <prop name="Transformable">True</prop>
   </properties>
  </shape>
 </shapes>
</panel>
