<?xml version="1.0" encoding="UTF-8"?>
<panel version="14">
 <properties>
  <prop name="Name">
   <prop name="en_US.utf8">(NoName)</prop>
   
  </prop>
  <prop name="Size">661 58</prop>
  <prop name="BackColor">_3DFace</prop>
  <prop name="RefPoint">0 0</prop>
  <prop name="InitAndTermRef">True</prop>
  <prop name="SendClick">False</prop>
  <prop name="RefFileName"></prop>
  <prop name="DPI">96</prop>
  <prop name="layoutType">None</prop>
 </properties>
 <events>
  <script name="ScopeLib" isEscaped="1"><![CDATA[#uses &quot;unGenericFunctions_core.ctl&quot;
#uses &quot;unGenericObject.ctl&quot;
#uses &quot;unDistributedControl/unDistributedControl.ctl&quot;
#uses &quot;unRcpDbFunctions.ctl&quot;
#uses &quot;unRcpFunctions_activation.ctl&quot;
#uses &quot;unRcpFunctions_utilities.ctl&quot;
#uses &quot;unRcpFunctions_instance.ctl&quot;
#uses &quot;unRcpFunctions_class.ctl&quot;
#uses &quot;unicos_declarations.ctl&quot;

dyn_string g_dsUserAccess;	                  // Current user access
string g_sFaceplateButtonType = &quot;UnRcpInstance&quot;; // Faceplate type
string g_lastDirectory = &quot;&quot;;
bool g_bCallbackConnected = false;

//------------------------------------------------------------------------------------------------------------------------

void openLastActivatedRecipes() {
  string sPcoLink, sPcoLinkDpName;  
  dyn_string recipeDpList, exceptionInfo;  
  
  getPcoLink(sPcoLink, sPcoLinkDpName);
  unRecipeFunctions_getLastActivatedRecipesFromPcoHierarchy(sPcoLinkDpName, recipeDpList, exceptionInfo);
  
  ChildPanelOnCentral(&quot;vision/unRecipe/UnRcpInstance/unUnRcpInstance_ActivateRcpInstanceList.pnl&quot;,
					sPcoLink + &quot; : Last activated recipes&quot;,
					makeDynString(&quot;$sDpName:&quot; + $sDpName,
                      &quot;$sRcpDpList:&quot; + recipeDpList, 
                      &quot;$pco:&quot; + sPcoLink, 
                      &quot;$text:Last activated recipes of PCO: &quot; + sPcoLink,
                      &quot;$panelMode:LAST_ACTIVATED&quot;));
}

//------------------------------------------------------------------------------------------------------------------------

void openInitialRecipes() {
  string sPcoLink, sPcoLinkDpName; 
  dyn_string recipeDpList, exceptionInfo;  
  
  getPcoLink(sPcoLink, sPcoLinkDpName);
  unRecipeFunctions_getInitialRecipesFromPcoHierarchy(sPcoLinkDpName, recipeDpList, exceptionInfo);
  
  ChildPanelOnCentral(&quot;vision/unRecipe/UnRcpInstance/unUnRcpInstance_ActivateRcpInstanceList.pnl&quot;,
       sPcoLink + &quot; : Initial recipes&quot;,
       makeDynString(&quot;$sDpName:&quot; + $sDpName,
                     &quot;$sRcpDpList:&quot; + recipeDpList,
                     &quot;$pco:&quot; + sPcoLink, 
                     &quot;$text:Initial recipes of PCO: &quot; + sPcoLink,
                     &quot;$panelMode:INITIAL&quot;));
}

//------------------------------------------------------------------------------------------------------------------------

void getPcoLink(string &amp; sPcoLink, string &amp;sPcoLinkDpName) {
  string sClassName, sRcpClassDp, sSystemName;
  
  dpGet($sDpName + &quot;.ProcessInput.ClassName&quot;, sClassName);
  sSystemName = unGenericDpFunctions_getSystemName($sDpName);
  
  // Select the PCO in the PcoList combo box
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + sClassName, sRcpClassDp);
  dpGet(sRcpClassDp + &quot;statusInformation.deviceLink:_online.._value&quot;, sPcoLink);
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + sPcoLink, sPcoLinkDpName); 
}

//------------------------------------------------------------------------------------------------------------------------
void saveAs(string id) {
  if (id == &quot;Initial&quot;) {
    saveAsInitialRecipe();
  } else if (id == &quot;DeviceDefault&quot;) {
    saveAsDefaultDeviceValues();      
  }
}
//------------------------------------------------------------------------------------------------------------------------
void saveAsDefaultDeviceValues() {
  dyn_float df;
  dyn_string ds, exceptionInfo;
  
  // Ask for user confirmation
  ChildPanelOnCentralReturn(&quot;fwGeneral/fwOkCancel.pnl&quot;,
							&quot;Save as initial recipe&quot;,
							makeDynString(&quot;$sTitle:Save device default values &quot;,
										  &quot;$text: The recipe values will be saved in the devices as the default values. \n Do you want to continue?&quot;  ),
							df,
							ds);  
  
  
  if (df[1]&lt;=0) return;
  
  unRecipeFunctions_saveAsDeviceDefaultValues($sDpName, exceptionInfo);
}
//------------------------------------------------------------------------------------------------------------------------
void saveAsInitialRecipe() {
  int iRetVal;
  string sClassName, sInitialRcpDp, sRcpClassDp, sPcoLink;
  dyn_float df;
  dyn_string ds, exceptionInfo;
    
  // Get the recipe class data from the recipe instance
  _unRecipeFunctions_getRecipeClassDataFromInstance($sDpName, sClassName, sRcpClassDp, sPcoLink);
  
  // Check if there is an initial recipe for the class
  iRetVal = _unRecipeFunctions_getInitialRecipeOfClass(sRcpClassDp, sInitialRcpDp, exceptionInfo);
  if (iRetVal == RCP_INITIAL_NO_DEFINED) {
    unRecipeFunctions_writeInRecipeLog(
        &quot;Error: The recipe class '&quot; + sClassName + &quot;' doesn't have an initial recipe. &quot; +
        &quot;Please, create the initial recipe of the class.&quot;);
    return;
  }
  
  // Ask for user confirmation
  ChildPanelOnCentralReturn(&quot;fwGeneral/fwOkCancel.pnl&quot;,
							&quot;Save as initial recipe&quot;,
							makeDynString(&quot;$sTitle:Save as initial recipe &quot;,
										  &quot;$text: The recipe values will be saved in the initial recipe. \n Do you want to continue?&quot;  ),
							df,
							ds);  
  
  
  if (df[1]&lt;=0) return;
  
  // Save the recipe values in the initial recipe
  unRecipeFunctions_saveAsInitialRecipe($sDpName, exceptionInfo);
}
//------------------------------------------------------------------------------------------------------------------------
void databaseValues(string id) {
  time validAt;
  dyn_string exceptionInfo;
  
  if (id == &quot;Latest&quot;) {
    // Load the latest values of the recipe available in the DB
    validAt = 0;
  } else if (id == &quot;History&quot;) {
    dyn_float df;
    dyn_string ds;
    string sAlias = dpGetAlias($sDpName + &quot;.&quot;);
    string systemName = unGenericDpFunctions_getSystemName($sDpName);
    ChildPanelOnCentralReturn(&quot;fwConfigurationDB/fwConfigurationDB_RecipeHistory.pnl&quot;,
              &quot;Recipe History in Database&quot;,
              makeDynString(&quot;$recipeName:&quot; + systemName + sAlias),
              df,
              ds);
    
    if (!dynlen(df) || df[1] == 0) {
      return;
    }    
    
    validAt = ds[2];
  }
  
  unRecipeFunctions_displayDbValues($sDpName, validAt, exceptionInfo);
}
//------------------------------------------------------------------------------------------------------------------------]]></script>
 </events>
 <layers>
  <layer layerId="0">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer1</prop>
  </layer>
  <layer layerId="1">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer2</prop>
  </layer>
  <layer layerId="2">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer3</prop>
  </layer>
  <layer layerId="3">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer4</prop>
  </layer>
  <layer layerId="4">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer5</prop>
  </layer>
  <layer layerId="5">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer6</prop>
  </layer>
  <layer layerId="6">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer7</prop>
  </layer>
  <layer layerId="7">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer8</prop>
  </layer>
 </layers>
 <shapes>
  <shape Name="Text1" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">11</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">585 28</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">False</prop>
    <prop name="ForeColor">{0,0,0}</prop>
    <prop name="BackColor">{255,255,255}</prop>
    <prop name="TabOrder">10</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinMiter,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="Location">587 30</prop>
    <prop name="Size">57 17</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,40,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">INIT</prop>
     
    </prop>
    <prop name="Distance">0</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">True</prop>
    <prop name="Fit">False</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
   <events>
    <script name="Initialize" isEscaped="1"><![CDATA[// Parameter: $sDpName, string, device name
// Global variables

main()
{
  dyn_string exceptionInfo;

  unGenericObject_ButtonInit($sDpName, g_sFaceplateButtonType, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonNewInstance" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">29</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">237.1764705882353 0</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">22</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Create a new recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">276 0</prop>
    <prop name="Size">95 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8"> New Instance ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  unRecipeFunctions_createRecipeInstance();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonActivate" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">36</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">0 0</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">28</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Activate the recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">0 0</prop>
    <prop name="Size">91 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Activate</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{

  dyn_string exceptionInfo;

  //dpSet($sDpName+&quot;.ProcessInput.State&quot;, &quot;Activate&quot;);
  
  unRecipeFunctions_activateRecipe($sDpName, exceptionInfo);


}]]></script>
   </events>
  </shape>
  <shape Name="buttonOnlineValues" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">37</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">92 0</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">29</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Get online values of the recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">92 0</prop>
    <prop name="Size">91 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Online Values</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  dyn_string exceptionInfo;
  
  unRecipeFunctions_displayOnlineValues($sDpName, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonEdit" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">44</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">-38 28</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">36</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Edit the recipe values</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">0 28</prop>
    <prop name="Size">91 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Edit</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  dyn_string exceptionInfo;
  
  unRecipeFunctions_editRecipeInstance($sDpName, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonSave" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">45</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">146 0</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">38</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Save recipe values</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">184 0</prop>
    <prop name="Size">91 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">   Save ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{   
  dyn_string exceptionInfo;
  
  unRecipeFunctions_saveRecipeInstance($sDpName, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonDuplicate" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">46</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">350 0</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">40</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Duplicate the current recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">372 0</prop>
    <prop name="Size">95 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">   Duplicate ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  dyn_string exceptionInfo;
  
  unRecipeFunctions_duplicateRecipeInstance($sDpName, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonDelete" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">48</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">536 0</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">44</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Delete the current recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">569 0</prop>
    <prop name="Size">91 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">  Delete ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  dyn_string exceptionInfo;  
  
  unRecipeFunctions_deleteRecipeInstance($sDpName, exceptionInfo);
  if (dynlen(exceptionInfo)) DebugTN(exceptionInfo);

}]]></script>
   </events>
  </shape>
  <shape Name="buttonCancel" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">49</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">243 28</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">46</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Cancel the recipe edition</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">276 28</prop>
    <prop name="Size">95 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Cancel</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  string state;
  
  dpGet($sDpName+&quot;.ProcessInput.State&quot;, state);  
  
  switch(strtolower(state)) {
    case &quot;edit&quot;:     // Cancel the recipe edit
      unRecipeFunctions_cancelEditRecipeInstance($sDpName);
      break;
      
    case &quot;delete&quot;:   // Cancel the recipe delete
      
      break;
  }
  
}]]></script>
   </events>
  </shape>
  <shape Name="buttonInitialRecipes" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">52</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">332 0</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">48</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Open the initial recipes of the PCO hierarchy</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">468 0</prop>
    <prop name="Size">100 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8"> Initial Recipes ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  openInitialRecipes();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonLastActivatedRecipes" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">53</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">421.1111111111111 28</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">49</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Open the last activated recipes of the PCO hierarchy</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">468 28</prop>
    <prop name="Size">100 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8"> Last Activated ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  openLastActivatedRecipes();
}]]></script>
   </events>
  </shape>
  <shape Name="buttonSelect" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">54</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">531 28</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">50</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Select the recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">568.9411764705883 28</prop>
    <prop name="Size">91 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Select</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  dyn_string exceptionInfo;
  bool bSelect;
  
  if (dpExists($sDpName)) {
    bSelect = (this.text == UN_FACEPLATE_BUTTON_SELECT_TEXTSELECT);
    unGenericObject_ButtonSelectAction($sDpName, bSelect, exceptionInfo);
    if (dynlen(exceptionInfo)) { 
      DebugTN(exceptionInfo); 
      return;
    }
  
    if (bSelect) {
      // Selection Progress Bar animation 
      startThread(&quot;unRecipeFunctions_animateSelectionProgressBar&quot;, $sDpName);
    }
  } else {
    unRecipeFunctions_writeInRecipeLog(&quot;Please, select a recipe.&quot;);      
  }
}]]></script>
    <script name="Initialize" isEscaped="1"><![CDATA[
bool g_bFirst = false;

//------------------------------------------------------------------------------------------------------------------------

main()
{
  bool bRes, bConnected;
  dyn_string exceptionInfo;
  string sDeviceSystemName;

  sDeviceSystemName = unGenericDpFunctions_getSystemName($sDpName);
  unDistributedControl_register(&quot;unRecipeFunctions_registerSelectButtonCB&quot;, bRes, bConnected, sDeviceSystemName, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
]]></script>
   </events>
  </shape>
  <shape Name="buttonSaveToDb" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">55</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">146 28</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">51</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Save recipe class and all the recipe instances to the database</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">184 28</prop>
    <prop name="Size">91 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">   Save to DB ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{   
  dyn_string exceptionInfo;
  
  unRecipeFunctions_saveRecipeToDb($sDpName, exceptionInfo);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonDbValues" shapeType="CASCADE_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">56</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">92 28</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">52</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Get database values of the recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">92 28</prop>
    <prop name="Size">91 26</prop>
    <prop name="PopupItemId"></prop>
    <prop name="Text">
     <prop name="en_US.utf8">DB Values</prop>
     
    </prop>
    <prop name="PopupMenu">
     <prop name="PopupItem">
      <prop name="PopupItemId">Latest</prop>
      <prop name="Text">
       <prop name="en_US.utf8">Latest</prop>
       
      </prop>
     </prop>
     <prop name="PopupItem">
      <prop name="PopupItemId">History</prop>
      <prop name="Text">
       <prop name="en_US.utf8">History ...</prop>
       
      </prop>
     </prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main(string id)
{
  databaseValues(id);
}]]></script>
   </events>
  </shape>
  <shape Name="buttonSaveAsInitial" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">58</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">350 28</prop>
    <prop name="Enable">False</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">54</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8">Save values as the initial recipe</prop>
     
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,75,0,0,0,0,0</prop>
     
    </prop>
    <prop name="Location">372 28</prop>
    <prop name="Size">95 26</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Save As Initial ...</prop>
     
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
  saveAsInitialRecipe();
}]]></script>
   </events>
  </shape>
 </shapes>
</panel>
