<?xml version="1.0" encoding="UTF-8"?>
<panel version="14">
 <properties>
  <prop name="Name">
   <prop name="en_US.utf8">Set Limits</prop>
   <prop name="en_US.iso88591">Set Limits</prop>
  </prop>
  <prop name="Size">260 205</prop>
  <prop name="BackColor">_3DFace</prop>
  <prop name="RefPoint">0 0</prop>
  <prop name="InitAndTermRef">True</prop>
  <prop name="SendClick">False</prop>
  <prop name="RefFileName"></prop>
  <prop name="DPI">96</prop>
  <prop name="layoutType">None</prop>
 </properties>
 <events>
  <script name="ScopeLib" isEscaped="1"><![CDATA[// Globals
bool g_bUserAccess;						// User access to Ok and Apply buttons
bool g_bCallbackConnected;				// Is device already connected?
bool g_bSelected;						// True if device is selected

bool firstUse;
int g_iRangeType;
float g_fRangeMin, g_fRangeMax;
string g_sPosSt, g_sPLiOnFormat, g_sPLiOffFormat;

updateStatusLimits(bool &amp;bRes)
{
	float currentLimitOn, currentLimitOff, rangeMin, rangeMax, fPLiOn, fPLiOff;	
	bool dataOk = true, bOpOkOn, bOpOkOff, bRes1;
	int iRes;
	int rangeType;
	string deviceName, alias;
	string formattedValueOn, formattedValueOff;
	dyn_string exceptionInfo, exceptionInfoTemp;
	
	bRes = false;
	deviceName = unGenericDpFunctions_getDpName($sDpName);
	alias = unGenericDpFunctions_getAlias(deviceName);
	// 1. Select the device again
	unGenericObject_NeedSelect(dpTypeName(deviceName), bRes1, exceptionInfo);
	if (bRes1)
		{
		unSelectDeselectHMI_select(deviceName, true, exceptionInfo);
		}
	dynClear(exceptionInfo);
	
	// 2. Check condition : range min &lt;= limit off &lt; limit on &lt;= range max
	unGenericObject_StringToFloat(LimitOnNew.text, currentLimitOn, bOpOkOn);
	unGenericObject_StringToFloat(LimitOffNew.text, currentLimitOff, bOpOkOff);
	if (bOpOkOn &amp;&amp; bOpOkOff)
		{
		if (g_iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)
			{
			rangeMin = g_fRangeMin;
			rangeMax = g_fRangeMax;

			if ((rangeMin &gt; currentLimitOff) || (rangeMax &lt; currentLimitOn))
				{
				dataOk = false;
				unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
										 getCatStr(&quot;unOperator&quot;,&quot;SETSTATUSLIMITSRANGECONDITIONFAILED&quot;),exceptionInfoTemp);
				if(dynlen(exceptionInfoTemp) &gt; 0)
					{
					fwExceptionHandling_display(exceptionInfoTemp);
					}
				}
			}
		if (currentLimitOn &lt;= currentLimitOff)
			{
			dataOk = false;
			unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
									 getCatStr(&quot;unOperator&quot;,&quot;SETSTATUSLIMITSCONDITIONFAILED&quot;),exceptionInfoTemp);
			if(dynlen(exceptionInfoTemp) &gt; 0)
					{
					fwExceptionHandling_display(exceptionInfoTemp);
					}
			}
	
		// 3. Set new status limits
		if (dataOk)
			{
			iRes = dpSetWait(deviceName + &quot;.ProcessOutput.PLiOn&quot;,currentLimitOn,
							 deviceName + &quot;.ProcessOutput.PLiOff&quot;,currentLimitOff);
			if (iRes &lt; 0)	// Error during dpSetWait
				{
				bRes = false;
				unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
										 getCatStr(&quot;unOperator&quot;,&quot;SETSTATUSLIMITSFAILED&quot;),exceptionInfoTemp);
				if(dynlen(exceptionInfoTemp) &gt; 0)
					{
					fwExceptionHandling_display(exceptionInfoTemp);
					}
				unSendMessage_toExpert(&quot;Set Status Limits&quot;,&quot;ERROR&quot;,unSendMessage_getDeviceDescription(deviceName) + 
									   getCatStr(&quot;unPVSS&quot;,&quot;DPSETFAILED&quot;),exceptionInfo);
				}
			else			// Ok
				{
				bRes = true;
				unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
										 getCatStr(&quot;unOperator&quot;,&quot;SETSTATUSLIMITSOK&quot;) + currentLimitOff + &quot;,&quot; +
										 currentLimitOn, exceptionInfoTemp);
				if(dynlen(exceptionInfoTemp) &gt; 0)
					{
					fwExceptionHandling_display(exceptionInfoTemp);
					}
				}	
			}
		else
			{
			bRes = false;
			}
		}
		
	// 4. On error ...
	if (bRes == false)
		{
		beep(400,250);
		iRes = dpGet(deviceName + &quot;.ProcessOutput.PLiOn&quot;, fPLiOn,
					 deviceName + &quot;.ProcessOutput.PLiOff&quot;, fPLiOff);
		if (iRes &gt;=0)
			{
			formattedValueOn = unGenericObject_FormatValue(g_sPLiOnFormat, fPLiOn);
			formattedValueOff = unGenericObject_FormatValue(g_sPLiOffFormat, fPLiOff);
			strreplace(formattedValueOn,&quot; &quot;,&quot;&quot;);
			strreplace(formattedValueOff,&quot; &quot;,&quot;&quot;);
			setValue(&quot;LimitOnNew&quot;, &quot;text&quot;, formattedValueOn);
			setValue(&quot;LimitOffNew&quot;, &quot;text&quot;, formattedValueOff);
			setInputFocus(myModuleName(), myPanelName(), &quot;LimitOnNew&quot;);
			}
		}
}]]></script>
  <script name="Initialize" isEscaped="1"><![CDATA[// Parameters : $sDpName, string, device name
main()
{
	string deviceSystemName;
	int iRes;
	bool bRes, bConnected;
	dyn_string exceptionInfo;
	
	firstUse = true;
	// 1. User callback function
	unGenericDpFunctionsHMI_setCallBack_user(&quot;userCB&quot;,iRes, exceptionInfo);
	
	// 2. For distributed systems
	deviceSystemName = unGenericDpFunctions_getSystemName($sDpName);
	unDistributedControl_register(&quot;registerCB&quot;, bRes, bConnected, deviceSystemName, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
userCB(string sDp, string sUser)
{
	dyn_string exceptionInfo;
	int i, buttonNumber;
	string deviceSystemName, deviceName;
	bool bConnected;

	// 1. Check if the user is authorized to manipulate it and update g_bUserAccess
	deviceName = unGenericDpFunctions_getDpName($sDpName);	
	unGenericButtonFunctionsHMI_isAccessAllowed(deviceName, dpTypeName(deviceName), UN_FACEPLATE_BUTTON_SET_STATUS_LIMITS, g_bUserAccess, exceptionInfo);

	// 2. Check if buttons must be enabled or disabled
	deviceSystemName = unGenericDpFunctions_getSystemName($sDpName);
	unDistributedControl_isConnected(bConnected, deviceSystemName);
	setButtonState(bConnected);
}

//------------------------------------------------------------------------------------------------------------------------
registerCB(string sDp, bool bConnected)
{
	dyn_string exceptionInfo;
	string deviceName, deviceSystemName;
	int iRes, iAction;

	deviceName = unGenericDpFunctions_getDpName($sDpName);
	unGenericObject_Connection(deviceName, g_bCallbackConnected, bConnected, iAction, exceptionInfo);
	switch (iAction)
		{
		case UN_ACTION_DISCONNECT:
			disconnection();
			break;
		case UN_ACTION_DPCONNECT:
			g_iRangeType = DPCONFIG_NONE;
			dpGet(deviceName + &quot;.ProcessInput.PosSt:_pv_range.._type&quot;, g_iRangeType);			// Update range type

			g_sPosSt = dpGetFormat(deviceName + &quot;.ProcessInput.PosSt&quot;);
			g_sPLiOnFormat = dpGetFormat(deviceName + &quot;.ProcessOutput.PLiOn&quot;);
			g_sPLiOffFormat = dpGetFormat(deviceName + &quot;.ProcessOutput.PLiOff&quot;);
			setMultiValue(&quot;LimitOnNew&quot;, &quot;foreCol&quot;, &quot;unDisplayValue_Parameter&quot;, &quot;LimitOnNew&quot;, &quot;enabled&quot;, true);
			setMultiValue(&quot;LimitOffNew&quot;, &quot;foreCol&quot;, &quot;unDisplayValue_Parameter&quot;, &quot;LimitOffNew&quot;, &quot;enabled&quot;, true);

			if (g_iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
				iRes = dpConnect(&quot;animationCB&quot;, deviceName + &quot;.statusInformation.selectedManager:_lock._original._locked&quot;,
	   									        deviceName + &quot;.statusInformation.selectedManager&quot;,
												deviceName + &quot;.ProcessOutput.PLiOn&quot;, deviceName + &quot;.ProcessOutput.PLiOn:_online.._aut_inv&quot;,
												deviceName + &quot;.ProcessOutput.PLiOff&quot;, deviceName + &quot;.ProcessOutput.PLiOff:_online.._aut_inv&quot;,
												deviceName + &quot;.ProcessInput.PosSt:_pv_range.._min&quot;,
						 						deviceName + &quot;.ProcessInput.PosSt:_pv_range.._max&quot;);
			}
			else {
				iRes = dpConnect(&quot;animationCB_noRange&quot;, deviceName + &quot;.statusInformation.selectedManager:_lock._original._locked&quot;,
	   									        deviceName + &quot;.statusInformation.selectedManager&quot;,
												deviceName + &quot;.ProcessOutput.PLiOn&quot;, deviceName + &quot;.ProcessOutput.PLiOn:_online.._aut_inv&quot;,
												deviceName + &quot;.ProcessOutput.PLiOff&quot;, deviceName + &quot;.ProcessOutput.PLiOff:_online.._aut_inv&quot;);
			}
			g_bCallbackConnected = (iRes &gt;= 0);
			break;
		case UN_ACTION_DPDISCONNECT_DISCONNECT:
			if (g_iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
				iRes = dpDisconnect(&quot;animationCB&quot;, deviceName + &quot;.statusInformation.selectedManager:_lock._original._locked&quot;,
	   									        deviceName + &quot;.statusInformation.selectedManager&quot;,
												deviceName + &quot;.ProcessOutput.PLiOn&quot;, deviceName + &quot;.ProcessOutput.PLiOn:_online.._aut_inv&quot;,
												deviceName + &quot;.ProcessOutput.PLiOff&quot;, deviceName + &quot;.ProcessOutput.PLiOff:_online.._aut_inv&quot;,
												deviceName + &quot;.ProcessInput.PosSt:_pv_range.._min&quot;,
						 						deviceName + &quot;.ProcessInput.PosSt:_pv_range.._max&quot;);
			}
			else {
				iRes = dpDisconnect(&quot;animationCB_noRange&quot;, deviceName + &quot;.statusInformation.selectedManager:_lock._original._locked&quot;,
									   		   deviceName + &quot;.statusInformation.selectedManager&quot;, 
											   deviceName + &quot;.ProcessOutput.PLiOn&quot;, deviceName + &quot;.ProcessOutput.PLiOn:_online.._aut_inv&quot;,
											   deviceName + &quot;.ProcessOutput.PLiOff&quot;, deviceName + &quot;.ProcessOutput.PLiOff:_online.._aut_inv&quot;);
			}
			g_bCallbackConnected = !(iRes &gt;= 0);
			disconnection();
			break;
		case UN_ACTION_NOTHING:
		default:
			break;
		}
}
	
//------------------------------------------------------------------------------------------------------------------------
animationCB_noRange(string sDp5, bool bLocked, 
			string sDp6, string sSelectedManager,
			string sDp1, float fPLiOn, string sDp2, bool bPLiOnUnvalid,
			string sDp3, float fPLiOff, string sDp4, bool bPLiOffUnvalid)
{
	animationCB(sDp5, bLocked, sDp6, sSelectedManager, sDp1, fPLiOn, sDp2, bPLiOnUnvalid,
						sDp3, fPLiOff, sDp4, bPLiOffUnvalid, &quot;&quot;, 0, &quot;&quot;, 0);
}
//------------------------------------------------------------------------------------------------------------------------

animationCB(string sDp5, bool bLocked, 
			string sDp6, string sSelectedManager,
			string sDp1, float fPLiOn, string sDp2, bool bPLiOnUnvalid,
			string sDp3, float fPLiOff, string sDp4, bool bPLiOffUnvalid,
			string sDp7, float rangeMin, string sDp8, float rangeMax)
{
	string formattedValueOn, formattedValueOff;
	string formatNew;
	string localManager, sDpNamePosSt;
	string sMin, sMax;
	float fMax, fMin;
	bool bMaxOk, bMinOk;	
	
	sDpNamePosSt = $sDpName;
	// 1. Animate limits
	unGenericObject_DisplayValue(g_sPLiOnFormat, &quot;&quot;, fPLiOn, &quot;LimitOnOld&quot;, &quot;unDisplayValue_Parameter&quot;, bPLiOnUnvalid);
	unGenericObject_DisplayValue(g_sPLiOffFormat, &quot;&quot;, fPLiOff, &quot;LimitOffOld&quot;, &quot;unDisplayValue_Parameter&quot;, bPLiOffUnvalid);
		
	// 3. First use : set new = old values
	if (firstUse == true)
		{
		formattedValueOn = unGenericObject_FormatValue(g_sPLiOnFormat, fPLiOn);
		formattedValueOff = unGenericObject_FormatValue(g_sPLiOffFormat, fPLiOff);
		strreplace(formattedValueOn, &quot; &quot;, &quot;&quot;);
		strreplace(formattedValueOff, &quot; &quot;, &quot;&quot;);
		setValue(&quot;LimitOnNew&quot;, &quot;text&quot;, formattedValueOn);
		setValue(&quot;LimitOffNew&quot;, &quot;text&quot;, formattedValueOff);
		setInputFocus(myModuleName(), myPanelName(), &quot;LimitOnNew&quot;);
		firstUse = false;
		}
	
	// 4. Update selection
	localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedManager);
	if (localManager == &quot;S&quot;)
		g_bSelected = true;							// Selected value
	else
		g_bSelected = false;
		
	// 5. Check if current button must be enabled or disabled
	setButtonState();
	
	// 6. set range min and max
	unGenericObject_DisplayRange(g_iRangeType, g_sPosSt, &quot;&quot;, rangeMin, rangeMax, 
								 &quot;minRangeValue&quot;, &quot;maxRangeValue&quot;, &quot;unDisplayValue_Parameter&quot;);

	sMin = unGenericObject_FormatValue(g_sPosSt, rangeMin);
	sMax = unGenericObject_FormatValue(g_sPosSt, rangeMax);	
	unGenericObject_StringToFloat(sMin, fMin, bMinOk);			
	unGenericObject_StringToFloat(sMax, fMax, bMaxOk);
	
	if (bMinOk&amp;&amp;bMaxOk)
		{
		g_fRangeMin=fMin; 
		g_fRangeMax=fMax;
		}

//	DebugN(g_fRangeMin, g_fRangeMax);
}

//------------------------------------------------------------------------------------------------------------------------
setButtonState(bool connected = true)
{
	bool buttonEnabled;
	
	// Conditions : device must be selected, connected and user has rights to manipulate the button
	buttonEnabled = g_bUserAccess &amp;&amp; g_bSelected &amp;&amp; connected;
	setValue(&quot;StatusLimitsOk&quot;, &quot;enabled&quot;, buttonEnabled);
	setValue(&quot;StatusLimitsApply&quot;, &quot;enabled&quot;, buttonEnabled);
}

//------------------------------------------------------------------------------------------------------------------------

disconnection()
{
	setButtonState(false);
	
	setMultiValue(&quot;LimitOnNew&quot;, &quot;foreCol&quot;, &quot;unDataNoAccess&quot;, &quot;LimitOnNew&quot;, &quot;enabled&quot;, false);
	setMultiValue(&quot;LimitOffNew&quot;, &quot;foreCol&quot;, &quot;unDataNoAccess&quot;, &quot;LimitOffNew&quot;, &quot;enabled&quot;, false);	
	setValue(&quot;LimitOnOld.display&quot;, &quot;foreCol&quot;, &quot;unDataNoAccess&quot;);
	setValue(&quot;LimitOffOld.display&quot;, &quot;foreCol&quot;, &quot;unDataNoAccess&quot;);

	setValue(&quot;minRangeValue.display&quot;, &quot;foreCol&quot;, &quot;unDataNoAccess&quot;);
	setValue(&quot;maxRangeValue.display&quot;, &quot;foreCol&quot;, &quot;unDataNoAccess&quot;);
}]]></script>
 </events>
 <layers>
  <layer layerId="0">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer1</prop>
  </layer>
  <layer layerId="1">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer2</prop>
  </layer>
  <layer layerId="2">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer3</prop>
  </layer>
  <layer layerId="3">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer4</prop>
  </layer>
  <layer layerId="4">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer5</prop>
  </layer>
  <layer layerId="5">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer6</prop>
  </layer>
  <layer layerId="6">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer7</prop>
  </layer>
  <layer layerId="7">
   <prop name="Visible">True</prop>
   <prop name="Name">Layer8</prop>
  </layer>
 </layers>
 <shapes>
  <reference parentSerial="-1" Name="LimitOnOld" referenceId="0">
   <properties>
    <prop name="FileName">objects/UN_INFOS/unFaceplate_DisplayNormalPopupWin.pnl</prop>
    <prop name="Location">95 25</prop>
    <prop name="Geometry">1.1 0 0 1.047619047619048 -20.5 33.80952380952381</prop>
    <prop name="TabOrder">1</prop>
    <prop name="layoutAlignment">AlignCenter</prop>
   </properties>
  </reference>
  <reference parentSerial="-1" Name="LimitOffOld" referenceId="1">
   <properties>
    <prop name="FileName">objects/UN_INFOS/unFaceplate_DisplayNormalPopupWin.pnl</prop>
    <prop name="Location">75 45</prop>
    <prop name="Geometry">1.1 0 0 1.047619047619048 1.499999999999998 38.85714285714286</prop>
    <prop name="TabOrder">2</prop>
    <prop name="layoutAlignment">AlignCenter</prop>
   </properties>
  </reference>
  <shape Name="LimitOnNew" shapeType="TEXT_FIELD" layerId="0">
   <properties>
    <prop name="serialId">22</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">137 68</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">3</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,40,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,-1,11,5,40,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">172 60</prop>
    <prop name="Size">78 23</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Editable">True</prop>
    <prop name="TextFormat">[0s,,,AlignRight]</prop>
   </properties>
   <events>
    <script name="Command" isEscaped="1"><![CDATA[main()
{
	bool bRes=false;
	
	if (StatusLimitsOk.enabled == true)
		{
		updateStatusLimits(bRes);
		if (bRes)
			PanelOff();
		}
}]]></script>
    <script name="KeyboardFocusOut" isEscaped="1"><![CDATA[main()
{
	unGenericObject_DisplayEventLostFocus(&quot;LimitOnNew&quot;, &quot;LimitOnOld.display&quot;, 
									unGenericDpFunctions_getDpName($sDpName) + &quot;.ProcessOutput.PLiOn&quot;, g_sPLiOnFormat);
}]]></script>
   </events>
  </shape>
  <shape Name="LimitOffNew" shapeType="TEXT_FIELD" layerId="0">
   <properties>
    <prop name="serialId">23</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">155 86</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">4</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,11,5,40,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,-1,11,5,40,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">172 86</prop>
    <prop name="Size">78 23</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Editable">True</prop>
    <prop name="TextFormat">[0s,,,AlignRight]</prop>
   </properties>
   <events>
    <script name="Command" isEscaped="1"><![CDATA[main()
{
	bool bRes=false;
	
	if (StatusLimitsOk.enabled == true)
		{
		updateStatusLimits(bRes);
		if (bRes)
			PanelOff();
		}
}]]></script>
    <script name="KeyboardFocusOut" isEscaped="1"><![CDATA[main()
{
	unGenericObject_DisplayEventLostFocus(&quot;LimitOffNew&quot;, &quot;LimitOffOld.display&quot;, 
									unGenericDpFunctions_getDpName($sDpName) + &quot;.ProcessOutput.PLiOff&quot;, g_sPLiOffFormat);
}]]></script>
   </events>
  </shape>
  <shape Name="StatusLimitsOk" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">30</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">-132 96</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">5</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">10 170</prop>
    <prop name="Size">75 25</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Ok</prop>
     <prop name="en_US.iso88591">Ok</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
	bool bRes=false;

	updateStatusLimits(bRes);
	if (bRes)
		PanelOff();
}]]></script>
   </events>
  </shape>
  <shape Name="StatusLimitsApply" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">31</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">-49 96</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">6</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">93 170</prop>
    <prop name="Size">75 25</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Apply</prop>
     <prop name="en_US.iso88591">Apply</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
	bool bRes;
	
	updateStatusLimits(bRes);
}]]></script>
   </events>
  </shape>
  <shape Name="Button3" shapeType="PUSH_BUTTON" layerId="0">
   <properties>
    <prop name="serialId">32</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">33 96</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_ButtonText</prop>
    <prop name="BackColor">_Button</prop>
    <prop name="TabOrder">7</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="Hotkey">Esc</prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Location">175 170</prop>
    <prop name="Size">75 25</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Text">
     <prop name="en_US.utf8">Cancel</prop>
     <prop name="en_US.iso88591">Cancel</prop>
    </prop>
   </properties>
   <events>
    <script name="Clicked" isEscaped="1"><![CDATA[main()
{
	PanelOff();
}]]></script>
   </events>
  </shape>
  <reference parentSerial="-1" Name="maxRangeValue" referenceId="2">
   <properties>
    <prop name="FileName">objects/UN_INFOS/unFaceplate_DisplayNormalPopupWin.pnl</prop>
    <prop name="Location">95 25</prop>
    <prop name="Geometry">1.1 0 0 1.047619047619048 67.50000000000001 111.8095238095238</prop>
    <prop name="TabOrder">13</prop>
    <prop name="layoutAlignment">AlignCenter</prop>
   </properties>
  </reference>
  <reference parentSerial="-1" Name="minRangeValue" referenceId="3">
   <properties>
    <prop name="FileName">objects/UN_INFOS/unFaceplate_DisplayNormalPopupWin.pnl</prop>
    <prop name="Location">95 79</prop>
    <prop name="Geometry">1.1 0 0 1.047619047619048 67.50000000000001 29.23809523809523</prop>
    <prop name="TabOrder">15</prop>
    <prop name="layoutAlignment">AlignCenter</prop>
   </properties>
  </reference>
  <shape Name="limitOnLabel" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">45</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 64</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">18</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">10 64</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Limit on:</prop>
     <prop name="en_US.iso88591">Limit on:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="limitOffLabel" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">46</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 90</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">19</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">10 90</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Limit off:</prop>
     <prop name="en_US.iso88591">Limit off:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="minRangeLabel" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">47</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 116</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">20</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">10 116</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Min. range:</prop>
     <prop name="en_US.iso88591">Min. range:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="maxRangeLabel" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">48</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">10 142</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">21</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">10 142</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Max. range:</prop>
     <prop name="en_US.iso88591">Max. range:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="currentLabel" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">49</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">84 40</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">22</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">84 40</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Current:</prop>
     <prop name="en_US.iso88591">Current:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="newLabel" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">50</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">172 40</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_WindowText</prop>
    <prop name="BackColor">_Window</prop>
    <prop name="TabOrder">23</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">172 40</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,13,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,10,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">New:</prop>
     <prop name="en_US.iso88591">New:</prop>
    </prop>
    <prop name="Distance">2</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
  <shape Name="backgroundRectangle" shapeType="RECTANGLE" layerId="0">
   <properties>
    <prop name="serialId">55</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">920 96</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">_Transparent</prop>
    <prop name="BackColor">unCorporateColor</prop>
    <prop name="TabOrder">24</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinBevel,CapButt,1]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[solid]</prop>
    <prop name="BorderStyle">Normal</prop>
    <prop name="Location">-1 -1</prop>
    <prop name="Size">1062 31</prop>
    <prop name="CornerRadius">0</prop>
    <prop name="Transformable">True</prop>
   </properties>
  </shape>
  <shape Name="title" shapeType="PRIMITIVE_TEXT" layerId="0">
   <properties>
    <prop name="serialId">56</prop>
    <prop name="Type"></prop>
    <prop name="RefPoint">8 2</prop>
    <prop name="Enable">True</prop>
    <prop name="Visible">True</prop>
    <prop name="ForeColor">white</prop>
    <prop name="BackColor">_Transparent</prop>
    <prop name="TabOrder">25</prop>
    <prop name="ToolTipText">
     <prop name="en_US.utf8"></prop>
     <prop name="en_US.iso88591"></prop>
    </prop>
    <prop name="layoutAlignment">AlignCenter</prop>
    <prop name="snapMode">Point</prop>
    <prop name="DashBackColor">_Transparent</prop>
    <prop name="AntiAliased">False</prop>
    <prop name="LineType">[solid,oneColor,JoinMiter,CapButt,2]</prop>
    <prop name="BorderZoomable">False</prop>
    <prop name="FillType">[outline]</prop>
    <prop name="Location">10 4</prop>
    <prop name="Font">
     <prop name="en_US.utf8">Arial,-1,19,5,50,0,0,0,0,0</prop>
     <prop name="en_US.iso88591">Arial,14,-1,5,50,0,0,0,0,0</prop>
    </prop>
    <prop name="Text">
     <prop name="en_US.utf8">Set Limits</prop>
     <prop name="en_US.iso88591">Set Limits</prop>
    </prop>
    <prop name="Distance">0</prop>
    <prop name="BorderOffset">2</prop>
    <prop name="Bordered">False</prop>
    <prop name="Fit">True</prop>
    <prop name="Transformable">True</prop>
    <prop name="TextFormat">[0s,,,AlignLeft]</prop>
   </properties>
  </shape>
 </shapes>
</panel>
