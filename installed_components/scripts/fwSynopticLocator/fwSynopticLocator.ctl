#uses "fwSynopticLocator/fwSynopticLocator.ctl"

const string communicationDPBuild = "fwSynopticLocator_Build";
const string communicationDPClean = "fwSynopticLocator_Clean";
const string communicationDPUpdate = "fwSynopticLocator_Update";
const string communicationDPEBuild = "_fwSynopticLocator_System_Communication.Triggers.Build:_original.._value";
const string communicationDPEClean = "_fwSynopticLocator_System_Communication.Triggers.Clean:_original.._value";
const string communicationDPEUpdate = "_fwSynopticLocator_System_Communication.Triggers.Update:_original.._value";

  /**
    
   Setups the DPs communication environment/system with its needs.
   (DPs: Build, ReBuild, Update)   
   
   @pre The environment is not already initialized.
   @post The environment is initialized.
   @return Int: 0 (OK) or -1 (Something went wrong).
   
   */

void main() {
 
  int replyCode;
  string dpeName;
  
  //connect to all communication DPE
  
  dpeName = communicationDPEBuild;  
  replyCode = dpConnect (communicationDPBuild, false, dpeName);
  if (replyCode != 0) 
  { 
  	DebugTN("Build Synoptic Locator connection failed. Stopping.");
  	return;
  }
  
  dpeName = communicationDPEClean;  
  replyCode = dpConnect (communicationDPClean, false, dpeName);
  if (replyCode != 0) 
  { 
  	DebugTN("Clean Synoptic Locator connection failed. Stopping."); 
  	return;  	
  }
  
  dpeName = communicationDPEUpdate;  
  replyCode = dpConnect (communicationDPUpdate, false, dpeName);
  if (replyCode != 0) 
  { 
  	DebugTN("Update Synoptic Locator connection failed. Stopping.");
  	return;
  }
  
}
