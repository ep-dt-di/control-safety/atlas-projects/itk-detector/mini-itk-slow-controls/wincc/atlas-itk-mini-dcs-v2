/**@file

@brief This library contains unicos CPC constants

@author Jonas Arroro (BE/ICS/FD)
@author Enrique Blanco (EN/ICE)
@author Alexey Merezhin (EN/ICE)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Creation Date
  14/11/2011

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL

*/

const string CPC_UNDEFINED_CONSTANT_VALUE = "undefined"; //!< Undefined value
const unsigned CPC_METAINFO_LENGTH      = 5; //!< Lenght of meta-info in configuration db
const unsigned CPC_CONFIG_MASTER_NAME	  = 1; //!< Position of master in the meta-info block in config db
const unsigned CPC_CONFIG_PARENTS 	   	= 2; //!< Position of parents in the meta-info block in config db
const unsigned CPC_CONFIG_CHILDREN 		  = 3; //!< Position of children in the meta-info block in config db
const unsigned CPC_CONFIG_TYPE			    = 4; //!< Position of type in the meta-info block in config db
const unsigned CPC_CONFIG_SECOND_ALIAS	= 5; //!< Position of second alias in the meta-info block in config db
const string CPC_CONFIG_MASTER_NAME_KEY	 = "MASTER_NAME";  //!< Key for master in parameters
const string CPC_CONFIG_PARENTS_KEY 		 = "PARENTS";      //!< Key for parents in parameters
const string CPC_CONFIG_CHILDREN_KEY 	   = "CHILDREN";     //!< Key for children in parameters
const string CPC_CONFIG_TYPE_KEY			   = "TYPE";         //!< Key for type in parameters
const string CPC_CONFIG_SECOND_ALIAS_KEY = "SECOND_ALIAS"; //!< Key for second alias in parameters
const string CPC_CONFIG_SMS_MESSAGE		   = "SMS_MESSAGE";  //!< Key for sms message in parameters
const string CPC_CONFIG_ALARM_LEVEL      = "ALARM_LEVEL";  //!< Key for alarm level in parameters
const string CPC_CONFIG_MAPPING      = "DESC_";  //!< Key for description field in parameters
const string CPC_CONFIG_FIRST_ORDER_FILTER = "FIRST_ORDER_FILTER";
const string CPC_CONFIG_ALARM_VALUES        = "ALARM_VALUES";
const string CPC_CONFIG_ALARM_VALUES_LOW    = "ALARM_VALUES_LOW";
const string CPC_CONFIG_ALARM_VALUES_MEDIUM = "ALARM_VALUES_MEDIUM";
const string CPC_CONFIG_ALARM_VALUES_HIGH   = "ALARM_VALUES_HIGH";
const string CPC_CONFIG_ALARM_VALUES_SAFETY = "ALARM_VALUES_SAFETY";
const string CPC_CONFIG_ALARM_ACK           = "ALARM_ACK";
const string CPC_CONFIG_ALARM_SMS           = "ALARM_SMS";
const string CPC_CONFIG_ALARM_MESSAGE       = "ALARM_MESSAGE";
const string CPC_CONFIG_ALARM_ACTIVE        = "ALARM_ACTIVE";
const string CPC_CONFIG_CONVERSION          = "MESSAGE_CONVERSION";
const string CPC_CONFIG_LABEL_ON            = "LabelOn";
const string CPC_CONFIG_LABEL_OFF           = "LabelOff";

const string UN_CONFIG_CPC_CONTROLLER_SCALING_METHOD_KEY = "SCALING_METHOD"; //!< Parameter constant
const string CPC_CONFIG_MV_DEVICE        = "MV";  //!< Key for controller mv parameter
const string CPC_CONFIG_OUT_DEVICES      = "OUT";  //!< Key for controller out parameter

const string CPC_PARAMS_DISPLAY_FORMAT = "DISPLAY_FORMAT"; // Name of the display format parameter in import line


//---------
// WIDGETS
//---------
const string CPC_WIDGET_TEXT_CONTROL_AUIHFOMOST = "h";          //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_AUINHFMO = "h";            //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_AUINHMMO = "h";            //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_MANUAL_RESTART = "R";              //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_ALARM_STARTIST = "I";              //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_ALARM_TSTOPIST = "S";              //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_ALARM_FUSTOPIST = "F";             //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_ALARM_ALST = "A";                  //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_BLOCK_TFUSTOP = "T";               //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_WARNING_ALARM_BLOCKED = "B";       //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_MMOST = "M";               //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_LDST = "HL";               //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_FORCED = "F";              //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_MANUAL = "M";              //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_SOFTLDST = "L";            //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_FOMOST = "F";              //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_REMOST = "R";              //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONTROL_TST = "T";                 //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_CONFIGW = "C";                     //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_POSHHW = "W";                      //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_POSHW = "W";                      //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_POSLW = "W";                      //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_POSLLW = "W";                      //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_POSW = "W";                        //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_WARNING_DEFAULT = "W";             //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_WARNING_SIMU = "S";                //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_WARNING_ERROR = "E";               //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_OLD_DATA = "O";                    //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_INVALID = "N";                     //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_ALARM_MASKED = "m";                //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_WARNING_BLOCKED = "B";             //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_ALARM_BLOCKED = "B";               //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_ALARM_DOES_NOT_EXIST = "";         //!< Letter constant for widget animation
const string CPC_WIDGET_TEXT_UNSYNC = "R";					//!< Letter constant for widget animation

// StsReg01 constants
const unsigned CPC_StsReg01_AST =					     0;		       //!< Alarm Status AA (for PosAl DPE; un_POSAL = 9)
const unsigned CPC_StsReg01_SOFTLDST =		     0;		       //!< Controller software local mode
const unsigned CPC_StsReg01_WST =					     1;		       //!< Warning Status AnalogAlarm
const unsigned CPC_StsReg01_AuMoSt =			     2;		       //!< Auto Mode
const unsigned CPC_StsReg01_MaMoSt =			     3;		       //!< Manual Mode
const unsigned CPC_StsReg01_FoMoSt =			     4;		       //!< Forced mode
const unsigned CPC_StsReg01_MONRST = 			     4;		       //!< StsReg bit constant
const unsigned CPC_StsReg01_MOFFRST = 		     5;		       //!< StsReg bit constant
const unsigned CPC_StsReg01_AUEAIST = 		     8;          //!< StsReg bit constant
const unsigned CPC_StsReg01_PREMASKALARM =	   8;	         //!< Premask alarm for DA
const unsigned CPC_StsReg01_REGST = 			     9;		       //!< StsReg bit constant
const unsigned CPC_StsReg01_POSW = 			 	     9;		       //!< StsReg bit constant
const unsigned CPC_StsReg01_MIOERBRST = 	     9;  	       //!< I/O Error blocked
const unsigned CPC_StsReg01_FUSTOPIST =        9;		       //!< Full stop interlock
const unsigned CPC_StsReg01_TRST = 			      10;		       //!< StsReg bit constant
const unsigned CPC_StsReg01_POSALST = 		    10;		       //!< StsReg bit constant
const unsigned CPC_StsReg01_TSTOPIST = 		    11;		       //!< Temporary Stop Interlock status
const unsigned CPC_StsReg01_OUTPST = 		      11;			     //!< StsReg bit constant
const unsigned CPC_StsReg01_AUIHFOMOST = 	    13;		       //!< Forced mode inhibited
const unsigned CPC_StsReg01_AUIHMBST = 		    15;          //!< StsReg bit constant
const unsigned CPC_StsReg01_LLST =			       8;			     //!< ANALOG ALARM -LL
const unsigned CPC_StsReg01_LST =			 	       9;			    //!< ANALOG ALARM -L
const unsigned CPC_StsReg01_HST =			        10;				   //!< ANALOG ALARM -H
const unsigned CPC_StsReg01_HHST =			      11;			     //!< ANALOG ALARM -HH


const unsigned CPC_StsReg01_MAMSKST =	      	15;			     //!< STSREG info alarm setMask
const unsigned CPC_StsReg01_PFSPOSONST_MFC =   5;	         //!< Auto inhibit manual mode (MFC)
const unsigned CPC_StsReg01_POSOV =			      11;			     //!< StsReg bit constant

const unsigned CPC_ManReg01_AMSKST =			    11;		       //!< AlarmMaskOn (Off is when is not bit 11) AA, DA. Output
const unsigned CPC_ManReg01_AMSKRST =		      12;			     //!< AalarmMaskOff (Reset alarm AA, DA) Output.

// AnalogAlarm constants
const unsigned CPC_StsReg01_IST		=               0;       //!< StsReg bit constant
const unsigned CPC_StsReg01_CONFIGW =             5;       //!< StsReg bit constant
const unsigned CPC_StsReg01_ARMRCPST =            3;       //!< StsReg bit constant
const unsigned CPC_StsReg01_PIDARMRCPST         = 5;       //!< StsReg bit constant
const unsigned CPC_StsReg01_ACTHHST	=             4;       //!< StsReg bit constant
const unsigned CPC_StsReg01_ACTLLST	=             5;       //!< StsReg bit constant
const unsigned CPC_StsReg01_POSHHW	=             8;       //!< StsReg bit constant
const unsigned CPC_StsReg01_POSHW	=               9;       //!< StsReg bit constant
const unsigned CPC_StsReg01_POSLW	=              10;       //!< StsReg bit constant
const unsigned CPC_StsReg01_POSLLW	=            11;       //!< StsReg bit constant
const unsigned CPC_StsReg01_MALBRST	=            14;       //!< StsReg bit constant
const unsigned CPC_StsReg01_ALST	=              14;       //!< StsReg bit constant
const unsigned CPC_StsReg01_AUIHMB =             15;       //!< StsReg bit constant

const unsigned CPC_StsReg02_EHHST	  =             0;				//!< StsReg bit constant
const unsigned CPC_StsReg02_EHST		=             1;				//!< StsReg bit constant
const unsigned CPC_StsReg02_ELST		=             2;				//!< StsReg bit constant
const unsigned CPC_StsReg02_ELLST	  =             3;				//!< StsReg bit constant
const unsigned CPC_StsReg02_HHALST	=             4;				//!< StsReg bit constant
const unsigned CPC_StsReg02_HWST		=             5;				//!< StsReg bit constant
const unsigned CPC_StsReg02_LWST		=             6;				//!< StsReg bit constant
const unsigned CPC_StsReg02_LLALST	=             7;				//!< StsReg bit constant
const unsigned CPC_StsReg02_FUSTOPIST =          10;				//!< Full  Stop Interlock status
const unsigned CPC_StsReg02_NEEDRESTART =        11;        //!< StsReg bit constant
const unsigned CPC_StsReg02_SOFTLDST =           12;        //!< StsReg bit constant
const unsigned CPC_StsReg02_IHMHHST	=             8;				//!< StsReg bit constant
const unsigned CPC_StsReg02_IHMHST	=             9;				//!< StsReg bit constant
const unsigned CPC_StsReg02_IHMLST	=            10;				//!< StsReg bit constant
const unsigned CPC_StsReg02_IHMLLST	=            11;				//!< StsReg bit constant
const unsigned CPC_StsReg02_POUTMAIN =           15;        //!< StsReg bit constant
const unsigned CPC_StsReg02_OUTONOVST    =        0;        //!< StsReg bit constant
const unsigned CPC_StsReg02_ANALOGONST   =        8;        //!< StsReg bit constant
const unsigned CPC_StsReg02_ANALOGOFFST  =        9;        //!< StsReg bit constant
const unsigned CPC_StsReg02_OUTOFFOVST   =       14;        //!< StsReg bit constant




// ManReg01 constants --- seems we not use any more
const unsigned CPC_ManReg02_MNEWPOSR		  =       3;				//!< Manual Position request trigger bit in ManReg01
const unsigned CPC_ManReg01_MRESTART			=       9;				//!< ManReg bit constant
const unsigned CPC_ManReg01_MIOERBSETRST 	=      10;				//!< ManReg bit constant
const unsigned CPC_ManReg01_MALBSETRST 		=      10;				//!< ManReg bit constant
const unsigned CPC_ManReg01_MRSETTSASFS	  =       8;        //!< Manual reset temporary as full stop bit position in Manreg01^M
const unsigned CPC_ManReg01_MDALACT		    =      10;        //!< Manual deblock alarm action bit position in ManReg01^M
const unsigned CPC_ManReg01_MNEWHHR		    =       8;				//!< ManReg bit constant
const unsigned CPC_ManReg01_MNEWHR		    =       9;				//!< ManReg bit constant
const unsigned CPC_ManReg01_MNEWLR		    =      12;				//!< ManReg bit constant
const unsigned CPC_ManReg01_MNEWLLR		    =      13;				//!< ManReg bit constant

const unsigned CPC_ManReg02_MREGR 			  =       0;				//!< ManReg bit constant
const unsigned CPC_ManReg02_MOUTPR		  	=       1;				//!< ManReg bit constant
const unsigned CPC_ManReg01_MPSAVE        =       5;                //!< Request SAVE PID parameters (version PID without addressing)
const unsigned CPC_ManReg01_MPREST        =       6;                //!< Request restoring PID parameters


// Copy from unicos core
const unsigned CPC_StsReg01_ONST = 0;                            // On status
const unsigned CPC_StsReg01_POSST = 0;                           // Position status
const unsigned CPC_StsReg01_TST = 0;                             // Tracking status
const unsigned CPC_StsReg01_ACTIVE = 1;                              // Active status
const unsigned CPC_StsReg01_OFFST = 1;                           // Off status
const unsigned CPC_StsReg01_AUMOST = 2;                          // Auto mode
const unsigned CPC_StsReg01_MMOST = 3;                           // Manual mode
const unsigned CPC_StsReg01_FOMOST = 4;                          // Forced mode
const unsigned CPC_StsReg01_LDST = 5;                            // Local drive status
const unsigned CPC_StsReg01_REMOST = 5;                          // Regulation mode status
const unsigned CPC_StsReg01_IOERRORW = 6;                        // IO error warning
const unsigned CPC_StsReg01_IOSIMUW = 7;                         // IO simulated warning
const unsigned CPC_StsReg01_AUREGR = 8;                          // Auto Regulation Request
const unsigned CPC_StsReg01_AUMRW = 8;                           // Auto manual request warning
const unsigned CPC_StsReg01_FODIPRO = 8;                         // Forced value <> process value
const unsigned CPC_StsReg01_FODIAU = 8;                          // Forced value <> auto value
const unsigned CPC_StsReg01_POSALE = 8;                          // Position Alarm Enabled
const unsigned CPC_StsReg01_POSAL = 9;                           // Position alarm
const unsigned CPC_StsReg01_POSALW = 9;                          // Position alarm warning
const unsigned CPC_StsReg01_FUSTOPAL = 9;                        // Full stop alarm
const unsigned CPC_StsReg01_STARTIST = 10;                       // Start interlock status
const unsigned CPC_StsReg01_STARTAL = 10;                        // Start interlock alarm
const unsigned CPC_StsReg01_STOPIST = 11;                        // Stop interlock status
const unsigned CPC_StsReg01_TSTOPAL = 11;                        // Temporary stop alarm
const unsigned CPC_StsReg01_ALMSKST = 11;                        // Alarm masked
const unsigned CPC_StsReg01_ALUNACK = 12;                        // Alarm unacknowledge
const unsigned CPC_StsReg01_AUINHSAVRES = 12;                        // Auto inhibit save/restore (controller)
const unsigned CPC_StsReg01_AUINHFMO = 13;                       // Auto inhibit forced mode
const unsigned CPC_StsReg01_PFSPOSONST = 14;                     // Parameter failsafe position on status
const unsigned CPC_StsReg01_AUESPO = 14;                         // Parameter failsafe position on status (Controller)
const unsigned CPC_StsReg01_HFST = 14;                           // Hardware feedback status
const unsigned CPC_StsReg01_AUPOSRST = 14;                       // Auto position request status
const unsigned CPC_StsReg01_PHFONST = 14;                        // Parameter Feed Back On
const unsigned CPC_StsReg01_AUINHMMO = 15;                       // Auto inhibit manual mode
const unsigned CPC_StsReg01_PHFOFFST = 15;                       // Parameter Feed Back OFF

const unsigned CPC_StsReg02_OUTOVST = 0;                         // Output order value status
const unsigned CPC_StsReg02_RUNOST = 0;                          // Run order status
const unsigned CPC_StsReg02_STRPAREKP = 0;                       // PID Kp parameter
const unsigned CPC_StsReg02_DOUTONO = 0;                         // Digital output on
const unsigned CPC_StsReg02_POSMST = 0;							 // Positioning Mode
const unsigned CPC_StsReg02_REFSMST = 1;						 // Reference Search Mode
const unsigned CPC_StsReg02_AUONRST = 1;                         // Auto on request status
const unsigned CPC_StsReg02_STRPARETI = 1;                       // PID Ti parameter
const unsigned CPC_StsReg02_DOUTOFFO = 1;                        // Digital output off
const unsigned CPC_StsReg02_MONRST = 2;                          // Manual on request status
const unsigned CPC_StsReg02_STRPARETD = 2;                       // PID Td parameter
const unsigned CPC_StsReg02_MOVST = 2;							 // Moving Status
const unsigned CPC_StsReg02_AUOFFRST = 3;                        // Auto off request status
const unsigned CPC_StsReg02_STRPAREKD = 3;                       // PID Kd parameter
const unsigned CPC_StsReg02_UNSYNCST = 3;						 // Reference search not performed
const unsigned CPC_StsReg02_MOFFRST = 4;                         // Manual off request status
const unsigned CPC_StsReg02_BOPKP = 4;                           // By operator Kp parameter
const unsigned CPC_StsReg02_HONRST = 5;                          // Hardware on request status
const unsigned CPC_StsReg02_BOPTI = 5;                           // By operator Ti parameter
const unsigned CPC_StsReg02_AUCSTOP = 5;                         // Auto controlled stop order
const unsigned CPC_StsReg02_CWST = 5;							 // Status of the ClockWise Limit
const unsigned CPC_StsReg02_CCWST =	6;							 // Status of the CounterClockWise Limit
const unsigned CPC_StsReg02_HOFFRST = 6;                         // Hardware off request status
const unsigned CPC_StsReg02_ALACTB = 6;                          // Alarm action blocked
const unsigned CPC_StsReg02_BOPTD = 6;                           // By operator Td parameter
const unsigned CPC_StsReg02_PHFONST = 7;                         // Parameter hardware feedback on status
const unsigned CPC_StsReg02_TFUSTOP = 7;                         // Temporary as full stop
const unsigned CPC_StsReg02_BOPKD = 7;                           // By operator Kd parameter
const unsigned CPC_StsReg02_PHFOFFST = 8;                        // Parameter hardware feedback off status
const unsigned CPC_StsReg02_CSTOPOST = 8;                        // Controlled stop order status
const unsigned CPC_StsReg02_BOPSPOH = 8;                         // By operator setpoint limit high
const unsigned CPC_StsReg02_PPULSEST = 9;                        // Parameter pulse status
const unsigned CPC_StsReg02_BOPSPOL = 9;                         // By operator setpoint limit low
const unsigned CPC_StsReg02_PHLDST = 10;                         // Parameter hardware local drive status
const unsigned CPC_StsReg02_BOPOUTH = 10;                        // By operator output limit high
const unsigned CPC_StsReg02_PHLIST = 11;                         // Parameter hardware local input status
const unsigned CPC_StsReg02_BOPOUTL = 11;                        // By operator output limit low
const unsigned CPC_StsReg02_PHDOUST = 12;                        // Parameter hardware digital output status
const unsigned CPC_StsReg02_ATESPOH = 12;                        // Enable setpoint limit high
const unsigned CPC_StsReg02_ATESPOL = 13;                        // Enable setpoint limit low
const unsigned CPC_StsReg02_ATEOUTH = 14;                        // Enable output limit high
const unsigned CPC_StsReg02_ATEOUTL = 15;                        // Enable output limit low
const unsigned CPC_StsReg02_ALBW = 13;                                                                                                          // Dependent Alarm blocked warning

const unsigned CPC_ManReg01_MAUMOR = 0;                          // Auto mode bit position in ManReg01
const unsigned CPC_ManReg01_MMMOR = 1;                           // Manual mode bit position in ManReg01
const unsigned CPC_ManReg01_MFOMOR = 2;                          // Forced mode bit position in ManReg01
const unsigned CPC_ManReg01_MREMOR = 3;                          // Regulation mode bit position in ManReg01
const unsigned CPC_ManReg01_MONR = 4;                            // On Open request bit position in ManReg01
const unsigned CPC_ManReg01_MDEFPAR = 4;                         // Request restoring PID parameters
const unsigned CPC_ManReg01_MDEFPARSAVE = 5;                         // Request SAVE PID parameters (version PID without addressing)
const unsigned CPC_ManReg01_MOFFR = 5;                           // Off Close request bit position in ManReg01
const unsigned CPC_ManReg01_MSTOPR = 5;
const unsigned CPC_ManReg01_MCSTOPR = 6;                         // Manual controlled stop request bit position in Manreg01
const unsigned CPC_ManReg01_MNEWMR = 6;                          // Manual new manual request bit position in ManReg01
const unsigned CPC_ManReg01_MNEWPOSR = 6;                        // Manual new position request bit position in ManReg01
const unsigned CPC_ManReg01_MSPINR = 7;                          // Increase value
const unsigned CPC_ManReg01_MMASKALR = 7;                        // Mask alarm bit position in ManReg01
const unsigned CPC_ManReg01_MNEWSPR = 7;                         // Manual new setpoint request bit position in ManReg01
const unsigned CPC_ManReg01_MSPDER = 8;                          // Decrease value
const unsigned CPC_ManReg01_MUNMASKALR = 8;                      // Unmask alarm bit position in ManReg01
const unsigned CPC_ManReg01_MSETTSASFS = 8;                      // Manual set temporary as full stop bit position in Manreg01
const unsigned CPC_ManReg01_MNEWSPLHR = 8;                       // Manual new setpoint limit high request bit position in ManReg01
const unsigned CPC_ManReg01_MNEWREFSR = 8;
//const unsigned CPC_ManReg01_MRSETTSASFS = 9;                     // Manual reset temporary as full stop bit position in Manreg01
const unsigned CPC_ManReg01_MNEWSPLLR = 9;                       // Manual new setpoint limit low request bit position in ManReg01
const unsigned CPC_ManReg01_MBALACT = 10;                        // Manual block alarm action bit position in ManReg01
const unsigned CPC_ManReg01_MNEWPOSLHR = 10;                     // Manual new position limit high request bit position in ManReg01
//const unsigned CPC_ManReg01_MDALACT = 11;                        // Manual deblock alarm action bit position in ManReg01
const unsigned CPC_ManReg01_MNEWPOSLLR = 11;                     // Manual new position limit low request bit position in ManReg01
const unsigned CPC_ManReg01_MOPMOR = 12;                         // Manual option mode request bit position in ManReg01
const unsigned CPC_ManReg01_MNEWKPR = 12;                        // Manual new Kp parameter request bit position in ManReg01
const unsigned CPC_ManReg01_MAUDEOR = 13;                        // Manual auto to dependent object request bit position in ManReg01
const unsigned CPC_ManReg01_MNEWTDR = 13;                        // Manual new Td parameter request bit position in ManReg01
const unsigned CPC_ManReg01_MNEWTIR = 14;                        // Manual new Ti parameter request bit position in ManReg01
const unsigned CPC_ManReg01_MNEWKDR = 15;                        // Manual new Kd parameter request bit position in ManReg01
const unsigned CPC_ManReg01_MALACK = 15;                         // Acknowledge alarm bit position in ManReg01

// Button constants
const string CPC_FACEPLATE_BUTTON_OUTPUTPOSITIONING_MODE = "OutputPositioning";     //!< Output positioning button

const string CPC_FACEPLATE_BUTTON_PARAMETER_SET_VALUE	=		"Parameter_SetValue"; 			//!< Set Value for AnalogParameter and WordParameter
const string CPC_FACEPLATE_BUTTON_DIGITALPARAMETER_SET_VALUE	=	"DigitalParameter_SetValue"; 	//!< Set Value DigitalParameter
const string CPC_FACEPLATE_BUTTON_PUMP_SET_FREQUENCY   = 		"SetFrecuency";					//!< Set Frecuency Pump

const string CPC_FACEPLATE_BUTTON_AA_SUBPANEL_SET_ALARM   = "SetAlarm";			//!< Test subpanel Analog Alarm
const string CPC_FACEPLATE_BUTTON_AA_SUBPANEL_RESET_ALARM   = "ResetAlarm";		//!< Test subpanel Analog Alarm
const string CPC_FACEPLATE_BUTTON_AA_SUBPANEL_SET_WARNING   = "SetWarning";		//!< Test subpanel Analog Alarm
const string CPC_FACEPLATE_BUTTON_AA_SUBPANEL_RESET_WARNING   = "ResetWarning";	//!< Test subpanel Analog Alarm
const string CPC_FACEPLATE_BUTTON_SET_AA_LIMITS = "SetAALimits";		//!< Setting limits for AA
const string CPC_FACEPLATE_BUTTON_SET_DA_MAIL_SMS = "SetDAMailSMS";		//!< Setting limits for DA

const string CPC_FACEPLATE_BUTTON_MFC_PARAMS = "MFCSetParameters"; //!< MFC Set Parameters action
const string CPC_FACEPLATE_BUTTON_ALLOW_RESTART = "AllowRestart"; //!< Allow restart action
const string CPC_FACEPLATE_BUTTON_SAVE_VALUE = "SaveValue";       //!< Save value action
const string CPC_FACEPLATE_BUTTON_SET_SPEED = "SetSpeed";
const string CPC_FACEPLATE_BUTTON_REF_SEARCH = "RefRequest";
const string CPC_FACEPLATE_BUTTON_STOP = "Stop";
const string CPC_FACEPLACE_BUTTON_AUTO_TUNE = "AutoTune";

const string CPC_FACEPLACE_BUTTON_BLOCK_IOERROR = "BlockIOError";


//	AA and DA RBMenu
const unsigned CPC_MENU_ALARM_MASK_REPLY		 = 55;			//!< Menu Choice result for Digital Alarm and Analog Alarm
const unsigned CPC_MENU_RECIPES_REPLY		 = 56;			//!< Menu Choice result for Recipes from PCO
const unsigned CPC_MENU_CURRNTPARAMS_REPLY	 = 57;			//!< Menu Choice result for Current Parameters from PCO
const string CPC_POPUPMENU_CURRENTPARAMS 	 = "Current Parameters";	//!< Menu Item for calling Current Parameters from PCO
const string CPC_POPUPMENU_MANUAL_IO_ERROR_BLOCK = "IOErrorBlocking"; //!< IO Error blocking menu item's label

const string CPC_POPUPMENU_RECIPES_TEXT 	  = "Recipes";	//!< Menu Item for calling Recipes from PCO
const string CPC_POPUPMENU_DIAG_INFO_TEXT   = "Info_Diagnostic";	//!<	Unicos "info-diagnostic" menu-text
const string CPC_POPUPMENU_MASK_IST_TEXT    = "MaskISt";   //!< Mask ISt menu item's label
const string CPC_POPUPMENU_MASK_ALST_TEXT   = "MaskAlSt"; //!< Mask ISt menu item's label
const string CPC_POPUPMENU_ALARMS_TEXT      = "Alarms";      //!< Alarms menu item's label

const string CPC_POPUPMENU_HIERARCHY = "HierarchyPopup"; //!< Hierarchu menu item's label
const string CPC_POPUPMENU_ALLOW_TO_RESTART_TEXT = "Allow2RestartPopup"; //!< Allow to restart menu item's label


const int CPC_POPUPMENU_ACK_NUMBER               = 2;    //!< Popup item's number
const int CPC_POPUPMENU_MASK_POSST_NUMBER        = 3;    //!< Popup item's number
const int CPC_POPUPMENU_BLOCK_PLC_NUMBER         = 5;    //!< Popup item's number
const int CPC_POPUPMENU_FACEPLATE_NUMBER         = 6;    //!< Popup item's number
const int CPC_POPUPMENU_DIAGNOSTIC_NUMBER        = 7;    //!< Popup item's number
const int CPC_POPUPMENU_INFO_NUMBER              = 8;    //!< Popup item's number
const int CPC_POPUPMENU_DEVICE_CONFIG_NUMBER     = 10;   //!< Popup item's number
const int CPC_POPUPMENU_UNMASKEVENT_NUMBER       = 11;   //!< Popup item's number
const int CPC_POPUPMENU_MASKEVENT_NUMBER         = 12;   //!< Popup item's number
const int CPC_POPUPMENU_OPENSYNOPTIC_NUMBER      = 15;   //!< Popup item's number
const int CPC_POPUPMENU_MASK_ALST_NUMBER         = 16;   //!< Popup item's number
const int CPC_POPUPMENU_MANUAL_IO_ERROR_NUMBER   = 17;   //!< Popup item's number
const int CPC_POPUPMENU_MASK_IST_NUMBER          = 18;   //!< Popup item's number
const int CPC_POPUPMENU_HIERARCHY_NUMBER         = 19;   //!< Popup item's number
const int CPC_POPUPMENU_DEVICELINK_NUMBER        = 20;   //!< Popup item's number
const int CPC_POPUPMENU_ALARMS_NUMBER            = 401;  //!< Popup item's number - UN_POPUPMENU_MIN + 1
const int CPC_POPUPMENU_RECIPES                  = 402; // cpc
const int CPC_POPUPMENU_ALLOW_TO_RESTART_NUMBER  = 403;


// MFC constants
const unsigned CPC_ManReg01_MSPR        = 6;					//!< Manual Set Point Request trigger
const unsigned CPC_ManReg01_MDRMODE     = 12;					//!< Manual Drive Mode Request Trigger
const unsigned CPC_ManReg01_MTOTMRT     = 13;					//!< Manual Totalizer Mode Request Trigger
const unsigned CPC_ManReg01_MCCRT       = 14;					//!< Manual Calibration Curve Mode Request Trigger

// Config constants
const string CPC_CONFIG_DEFAULT_CALIBRATION_CURVE = "1=default";	  //!< Default string when there is not string for the calibration curve

// Alarm
const string CPC_WIDGET_ALARM_HIGH	  	= "H";						//!< High alarm AnalogAlarm
const string CPC_WIDGET_ALARM_HIGHHIGH	= "HH";						//!< High High alarm AnalogAlarm
const string CPC_WIDGET_ALARM_LOW		    = "L";						//!< Low alarm AnalogAlarm
const string CPC_WIDGET_ALARM_LOWLOW	  = "LL";						//!< Low Low alarm - for AnalogAlarm
const string CPC_WIDGET_ALARM			      = "A";						//!< Alarm for Digital Alarm

// Mail-SMS alert class constants
const string CPC_CONFIG_ALARM_BASE_CLASS   = "_cpcAnalog"; //!< Base alarm class @see cpcConfigGenericFunctions_getAlertClass
const string CPC_CONFIG_ALARM_ACK_POSTFIX  = "_Ack";       //!< Acknowledge postfix for alarm class @see cpcConfigGenericFunctions_getAlertClass
const string CPC_CONFIG_ALARM_MAIL_POSTFIX = "_Mail";      //!< Mail postfix for alarm class @see cpcConfigGenericFunctions_getAlertClass
const string CPC_CONFIG_ALARM_CUSTOM_OK_POSTFIX = " down"; //!< Used as postfix on OK message for custom alarm

const string CPC_CONFIG_ALARM_LOW_BASE_CLASS    = "_cpcP4";   //!< Base alarm class @see cpcConfigGenericFunctions_getAlertClass
const string CPC_CONFIG_ALARM_MEDIUM_BASE_CLASS = "_cpcP3";   //!< Base alarm class @see cpcConfigGenericFunctions_getAlertClass
const string CPC_CONFIG_ALARM_HIGH_BASE_CLASS   = "_cpcP2";   //!< Base alarm class @see cpcConfigGenericFunctions_getAlertClass
const string CPC_CONFIG_ALARM_SAFETY_BASE_CLASS = "_cpcP1";   //!< Base alarm class @see cpcConfigGenericFunctions_getAlertClass

const int CPC_INT16 	= 1;  //!< Virtual datatype @see cpcConfigGenericFunctions_translateDataType
const int CPC_INT32 	= 2;  //!< Virtual datatype @see cpcConfigGenericFunctions_translateDataType
const int CPC_UINT16	= 3;  //!< Virtual datatype @see cpcConfigGenericFunctions_translateDataType
const int CPC_BOOL 	= 4;    //!< Virtual datatype @see cpcConfigGenericFunctions_translateDataType
const int CPC_FLOAT 	= 5;  //!< Virtual datatype @see cpcConfigGenericFunctions_translateDataType

const string CPC_PARAMS_FSPOSON = "PFSPosOn";
const string CPC_PARAMS_HFONST  = "PHFOnSt";
const string CPC_PARAMS_HFOFFST = "PHFOffSt";
const string CPC_PARAMS_PULSEST = "PPulseSt";
const string CPC_PARAMS_HFON    = "PHFOn";
const string CPC_PARAMS_HFOFF   = "PHFOff";
const string CPC_PARAMS_MRESTART = "PEnRstart";
const string CPC_PARAMS_RSTARTFS = "PRstartFS";
const string CPC_PARAMS_OUTOFF  = "POutOff";
const string CPC_PARAMS_OUTMAIN = "POutMain";
const string CPC_PARAMS_PWMMODE = "PPWMMode";
const string CPC_PARAMS_ALDT    = "PAlDt";
const string CPC_PARAMS_HLDRIVE = "PHLDrive";
const string CPC_PARAMS_NOISE_FILTER = "PNoiseFilter";
const string CPC_PARAMS_FEEDBACK_SIM = "PFeedbackSim";
const string CPC_PARAMS_INHIBITMANUALTOTALIZER = "PIhMTot";
const string CPC_MFC_CALIBRATION_UNIT = "CalibrationFlowUnit";
const string CPC_MFC_CALIBRATION_GAS_NAME = "CalibrationGasName";
const string CPC_PARAMS_PCONAME = "PCO_NAME";
const string CPC_PARAMS_AA_ACK_REQUIRED = "ACK_REQUIRED";              //!< Key for AnalogAlarm's autoack property
const string CPC_PARAMS_HH = "HH_SOURCE";
const string CPC_PARAMS_H  = "H_SOURCE";
const string CPC_PARAMS_INPUT = "INPUT_SOURCE";
const string CPC_PARAMS_L  = "L_SOURCE";
const string CPC_PARAMS_LL = "LL_SOURCE";
const string NO_MAIL_POSTFIX = "_NO_MAIL";

const string CPC_CMW_ADDRESS_CONFIG_SEPARATOR = "|"; //!< CMW address separator constant
//TODO: use another approach - IS-1154
const string CPC_CMW_DPTYPE = "CMW"; //!< CMW dp type; cloned from CPC_CMW_DPTYPE
const string CPC_OPCUA_DPTYPE = "OPCUA"; //!< OPCUA dp type; cloned from UNOPCUA_DPTYPE
const string CPC_BACNET_DPTYPE = "BACnet"; //!< BACNET dp type; cloned from UNBACNET_DPTYPE
const string CPC_DIP_DPTYPE = "DIP"; //!< DIP dp type
const string CPC_SNMP_DPTYPE = "SNMP"; //!< DIP dp type
const string CPC_IEC104_DPTYPE = "IEC104";  // IEC104 dp type
