/**@name LIBRARY: S7Config_PLC.ctl

@author: Enrique Blanco (AB/CO)

Creation Date: 19/05/2005

Modification History:

  15/10/2013: Marco Boccioli
  - @jira{IS-1241} modify the config entry for S7 PLC for redundancy: set LimitedTSPPAliveCheck = "No"

  12/06/2012: Marco Boccioli
  - @jira{IS-899} the import device accepts now the PLC type also as a couple PROTOCOL,MODEL.
  For example, instead of ;S7-PLC; now it import ;S7,S7-300; and instead of ;PREMIUM; it does ;MODBUS,PREMIUM;

  30/03/2012: Herve
  - IS-755  unCore - unFrontEnd  Front-end (SIEMENS and SCHNEIDER PLCS UCPC v6) Information updated: baseline and
  application versions added; spec version dropped. Front-end diagnostic panel updated accordingly.

  20/03/2012: Herve
  - IS-735  unCore - unFrontEnd  SIEMENS PLC: new field "resources package employed" used for the CPC 6

  14/10/2011: Herve
  - IS-593  unCore - import/export  stop/restart SIMULATOR/driver from import DB panel

  30/05/2011: Herve
    - IS-519: Remove all dependencies on graphical object from the Import device panel in the core import
    functions, to be able to use the unicos front-end/device check/import functions from a WCCOActrl

  19/02/2009: Herve
    - include unS7_PLC.ctl libs

	18/04/2006: Herve
		- implementation of mask event

	09/09/2005 : Include the versioning feature.
			 				 Functions to check the configuration line and to set the PLC has been modified.

	25/07/2005 : s7Config_setInternalConnection start connection not active if new PLC

version 1#

External Function :
  . unConfigS7_checkDeleteCommand : check the coherency of the delete command
  . unConfigS7_deleteCommand : delete function
  . unConfigS7_checkPLC : check the coherency of the PLC config
  . unConfigS7_setPLC : set PLC config


Internal Functions:
	. s7Config_checkApplication
	. s7Config_checkData
	. s7Config_checkAdditionalData
	. s7Config_setApplication
	. s7Config_deletePLC

	. s7Config_setInternalConnection

	. S7_PLC_checkFESystemAlarm
	. S7_PLC_setFESystemAlarm
	. S7_PLC_getFrontEndArchiveDp



Purpose:
This library contains the functions used to set the S7 PLC.

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . UNICOS-JCOP framework
  . PVSS version: 3.0
  . operating system: XP.
  . distributed system: yes.
*/

//@{

//------------------------------------------------------------------------------------------------------------------------

// unConfigS7_checkDeleteCommand
/**
Purpose: check the coherency of the delete command

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  dsDeleteDps, dyn_string, output, list of dp that will be deleted
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: XP.
	. distributed system: yes.
*/

unConfigS7_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo) {
    string sFeName, sPLCSubApplication = "", sObjectType = "", sNumber = "", sDpPlcName, sPlcModPlc;
    dyn_string dsAlarmDps;

    // DebugN("FUNCTION: unConfigS7_checkDeleteCommand( dsConfig= "+dsConfig+" dsDeleteDps= "+dsDeleteDps);

    if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
// get PLC Name
        sFeName = dsConfig[2];
        if ((sFeName != "") && (dsConfig[1] == UN_DELETE_COMMAND)) {
            if (dynlen(dsConfig) >= 3) {
// get Application
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) {
// get DeviceType
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) {
// get device number
                sNumber = dsConfig[5];
            }
            unGenericDpFunctions_getPlcDevices("", sFeName, sPLCSubApplication, sObjectType, sNumber, dsDeleteDps);
// if only delete key word then delete also modbus and systemalarm
            if ((sFeName != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
// sDpPlc is S7_plcName
                sDpPlcName = c_unSystemIntegrity_S7 + sFeName;
                if (dpExists(sDpPlcName)) {
                    dynAppend(dsDeleteDps, sDpPlcName);
                    dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1), c_unSystemAlarm_dpType);
                    dynAppend(dsDeleteDps, dsAlarmDps);
                }
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigS7_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigS7_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigS7_checkPLC
/**
Purpose: check the coherency of the S7 front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  dsDeleteDps: dyn_string, input, dp to be deleted
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
        . PVSS version: 3.0
        . operating system: Win XP.
        . distributed system: yes.
*/
unConfigS7_checkPLC(string currentObject, dyn_string currentLineSplit, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    // DebugN("FUNCTION: unConfigS7_checkPLC(currentObject= "+currentObject+" currentLineSplit= "+currentLineSplit+" dsDeleteDps="+dsDeleteDps);

    switch (currentObject) {
        case UN_PLC_COMMAND:
            s7Config_checkApplication(currentLineSplit, dsDeleteDps, exceptionInfo);
            break;

        case UN_FESYSTEMALARM_COMMAND:
            unConfigGenericFunctions_checkFESystemAlarm(currentLineSplit, exceptionInfo);
            break;
        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigS7_deleteCommand
/**
Purpose: delete function

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP
	. distributed system: yes.
*/

unConfigS7_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo) {
    string sPLCSubApplication = "", sObjectType = "", sNumber = "";
    dyn_string dsDpeList, exceptionInfoTemp;

    // DebugN("FUNCTION: unConfigS7_deleteCommand(dsConfig ="+dsConfig);

    unConfigS7_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        fwException_raise(exceptionInfo, "ERROR", "unConfigS7_deleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
            if (dynlen(dsConfig) >= 3) {
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) {
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) {
                sNumber = dsConfig[5];
            }

            unGenericDpFunctions_deletePlcDevices(dsConfig[2], sPLCSubApplication, sObjectType, sNumber, "progressBar", exceptionInfo);
            if ((dsConfig[2] != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
                s7Config_deletePLC(dsConfig[2], exceptionInfo);
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigS7_setPLC
/**
Purpose: set S7 PLC config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/
unConfigS7_setPLC(string currentObject, dyn_string currentLineSplit, dyn_string &exceptionInfo) {
    string currentDevice, sPlcName, sPlcType, sAnalogArchive;
    int iPlcNumber, driver;
    bool bEvent16;

// DebugN("FUNCTION: unConfigS7_setPLC(currentObject= ",currentObject," currentLineSplit= ",currentLineSplit,"sPlcType:",sPlcType);

    switch (currentObject) {
        case UN_PLC_COMMAND:
            s7Config_setApplication(currentLineSplit, exceptionInfo);
            break;

        case UN_FESYSTEMALARM_COMMAND:
            if (dynlen(currentLineSplit) == UN_CONFIG_SETFESYSTEM_ALARM_LENGTH) {
                currentDevice = currentLineSplit[UN_CONFIG_FESYSTEMALARM_DEVICENAME];
                sPlcName = currentLineSplit[UN_CONFIG_FESYSTEMALARM_PLCNAME];
                sPlcType = currentLineSplit[UN_CONFIG_FESYSTEMALARM_TYPE];
                iPlcNumber = (int)currentLineSplit[UN_CONFIG_FESYSTEMALARM_PLCNUMBER];
                driver = (int)currentLineSplit[UN_CONFIG_FESYSTEMALARM_DRIVER];
                bEvent16 = (bool)currentLineSplit[UN_CONFIG_FESYSTEMALARM_EVENT16];
                sAnalogArchive = currentLineSplit[UN_CONFIG_FESYSTEMALARM_ARCHIVE];

                unConfigGenericFunctions_setFESystemAlarm(currentLineSplit, makeDynString(currentDevice, sPlcName, sPlcType, iPlcNumber, driver, bEvent16, sAnalogArchive), exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "unConfigS7_setPLC: " + getCatStr("unGeneration", "BADINPUTS"), "");
            }
            break;

        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }

}

//------------------------------------------------------------------------------------------------------------------------

// s7Config_checkApplication
/**
Purpose: check data for a sub application (not comm)

Parameters:
	dsPLC: dyn_string, input, S7 configuration line in file + additional parameters
			dsPLC must contains :

						1	PLC_type (S7-300 or S7-400);
						2	PLC_name ;
						3	APPLICATION;
						4	Local_ID(hex) ;
						5	Local_Rack ;
						6	Local_Slot ;
						7	Local_ConnectionResource(hex) ;
						8	Partner_Rack ;
						9	Partner_Slot ;
						10	Partner_ConnectionResource(hex) ;
						11	Timeout[ms];
						12	PLC_IP;
						13	address_IP;
						14	address_counter ;
						15	address_CommandInterface (Request All, Syncro,...)

						16	Additional PLC info
					  17  PLC UNICOS application version address
						18	PVSS UNICOS application version number (float)

						19. driver number
						20. boolean archive name
						21. analog archive name
						22. False/True Event16,event32
						23. event archive name

	dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
	exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

s7Config_checkApplication(dyn_string dsConfig, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    dyn_string dsPLCLine, dsPLCAdditionalData, dsPLC = dsConfig;
    int i, iRes, iDriverNum;
    string sPLCDp, sHostname, sAliasDp, tempDp;
    bool bOk, bCPC6;
    string sUserTag, sRSC_version, sRSC_majorAddr, sRSC_minorAddr, sRSC_smallAddr, sPLCBaseline, sPLCApplication;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7_PLC " + dsPLC[UN_CONFIG_PLC_NAME], "s7Config_checkApplication", dsPLC);
    // DebugN("FUNCTION: s7Config_checkApplication(dsPLC= "+dsPLC+" dsDeleteDps= "+dsDeleteDps);
    if (dynlen(dsPLC) == (UN_CONFIG_S7_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) { // CPC6
        bOk = true;
        sRSC_version = dsPLC[UN_CONFIG_S7_CPC6_RESPACK_VERSION];
        dynRemove(dsPLC, UN_CONFIG_S7_CPC6_RESPACK_VERSION);
        sRSC_smallAddr = dsPLC[UN_CONFIG_S7_CPC6_RESPACK_SMALL_VERSION_ADDRESS];
        dynRemove(dsPLC, UN_CONFIG_S7_CPC6_RESPACK_SMALL_VERSION_ADDRESS);
        sRSC_minorAddr = dsPLC[UN_CONFIG_S7_CPC6_RESPACK_MINOR_VERSION_ADDRESS];
        dynRemove(dsPLC, UN_CONFIG_S7_CPC6_RESPACK_MINOR_VERSION_ADDRESS);
        sRSC_majorAddr = dsPLC[UN_CONFIG_S7_CPC6_RESPACK_MAJOR_VERSION_ADDRESS];
        dynRemove(dsPLC, UN_CONFIG_S7_CPC6_RESPACK_MAJOR_VERSION_ADDRESS);
        sPLCApplication = dsPLC[UN_CONFIG_S7_CPC6_PLC_APPLICATION_ADDRESS];
        sPLCBaseline = dsPLC[UN_CONFIG_S7_CPC6_PLC_BASELINE_ADDRESS];
        bCPC6 = true;
        sUserTag = "CPC6";
    } else if (dynlen(dsPLC) == (UN_CONFIG_S7_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_S7_FE_VERSION_ADDITIONAL_LENGTH)) {
        bOk = true;
        sUserTag = dsPLC[UN_CONFIG_S7_FE_VERSION];
// remove the version.
        dynRemove(dsPLC, UN_CONFIG_S7_FE_VERSION);
    } else if (dynlen(dsPLC) == (UN_CONFIG_S7_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        bOk = true;
    }

    if (bOk) {
        unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7_PLC " + dsPLC[UN_CONFIG_PLC_NAME], "s7Config_checkApplication, common configuration", dsPLC);
        dsPLCLine = dsPLC;
        for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
            dynAppend(dsPLCAdditionalData, dsPLCLine[UN_CONFIG_S7_LENGTH + 1]);
            dynRemove(dsPLCLine, UN_CONFIG_S7_LENGTH + 1);
        }

        // Check PLC parameters (only the PLC type and the existance of: PLC_name and the application)
        // A DEFINITION OF A NEW CONSTANT IS NEEDED (EQUIVALENT TO: UN_PLC_DPTYPE) USED en unConfigGenericFunctions
        s7Config_checkData(dsPLCLine, exceptionInfo);

        // Check Import Database panel added paramters (archives and driver number)
        s7Config_checkAdditionalData(dsPLCAdditionalData, exceptionInfo);

        // first check if the PLC name is syntactically correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_NAME];		//UN_CONFIG_S7_PLC_NAME
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADPLCNAME"), "");
            return;
        }

// keep the version
        g_mImportFrontEndVersion[tempDp] = sUserTag;

        // check if the PLC Application Name is syntactically correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION];		//UN_CONFIG_S7_APPLICATION
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
            return;
        }

        // sPLCDp is S7_plcName
        sPLCDp = getSystemName() +  c_unSystemIntegrity_S7 + dsPLCLine[UN_CONFIG_PLC_NAME];
        // sAliasDp = dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
        sAliasDp = unGenericDpFunctions_dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
        // DebugN(sAliasDp, dsPLCLine[UN_CONFIG_PLC_NAME]);

        if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".") {
            sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);
        }
        if ((sAliasDp != "") && (sAliasDp != sPLCDp)) {
            if (dynContains(dsDeleteDps, sAliasDp) <= 0) {	// Alias exists, is not used by the "S7" config datapoint and will not be deleted
                fwException_raise(exceptionInfo, "ERROR", "s7Config_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADHOSTNAMEALIAS") + sAliasDp, "");
            }
        }
        if (dpExists(sPLCDp) && (dynContains(dsDeleteDps, sPLCDp) <= 0)) {
            sHostname = unGenericDpFunctions_getAlias(sPLCDp);
            iRes = dpGet(sPLCDp + ".communication.driver_num", iDriverNum);
            if (iRes >= 0) {
                if (iDriverNum != dsPLCAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]) {
                    fwException_raise(exceptionInfo, "ERROR", "s7Config_checkApplication: " + getCatStr("unGeneration", "BADEXISTINGPLCDRIVER") + iDriverNum, "");
                }
                if (sHostname != dsPLCLine[UN_CONFIG_PLC_NAME]) {
                    fwException_raise(exceptionInfo, "ERROR", "s7Config_checkApplication: " + getCatStr("unGeneration", "BADEXISTINGPLCHOSTNAME"), "");
                }
            }
        }
        // Check the addresses for the IPnumber, counter and commands
        bool pollingFE = unConfigGenericFunctions_isPollingOnly(dsPLC[UN_CONFIG_PLC_TYPE]);

        unConfigGenericFunctions_checkAddressS7(dsPLC[UN_CONFIG_S7_ADD_IP], UN_CONFIG_S7_PLC_DATATYPE_DWORD, exceptionInfo, pollingFE);
        unConfigGenericFunctions_checkAddressS7(dsPLC[UN_CONFIG_S7_ADD_COUNTER], UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo);
        unConfigGenericFunctions_checkAddressS7(dsPLC[UN_CONFIG_S7_ADD_COMMANDINTERFACE], UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo, pollingFE);

        // Check the address for: (a) additional info and (b) the PLC version
        // DebugN("unConfigGenericFunctions_checkAddressS7("+dsPLC[UN_CONFIG_S7_ADD_PLCINFO]+","+UN_CONFIG_S7_PLC_DATATYPE_WORD);
        unConfigGenericFunctions_checkAddressS7(dsPLC[UN_CONFIG_S7_ADD_PLCINFO], UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo, pollingFE);
        if (bCPC6) {
            unConfigGenericFunctions_checkAddressS7(sPLCBaseline, UN_CONFIG_S7_PLC_DATATYPE_FLOAT, exceptionInfo, pollingFE);
            unConfigGenericFunctions_checkAddressS7(sPLCApplication, UN_CONFIG_S7_PLC_DATATYPE_FLOAT, exceptionInfo, pollingFE);
            unConfigGenericFunctions_checkAddressS7(sRSC_smallAddr, UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo, pollingFE);
            unConfigGenericFunctions_checkAddressS7(sRSC_minorAddr, UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo, pollingFE);
            unConfigGenericFunctions_checkAddressS7(sRSC_majorAddr, UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo, pollingFE);
        } else {
            unConfigGenericFunctions_checkAddressS7(dsPLC[UN_CONFIG_S7_ADD_PLC_VERSION], UN_CONFIG_S7_PLC_DATATYPE_FLOAT, exceptionInfo, pollingFE);
        }
    } else {	// Bad number of parameters in the import file
        fwException_raise(exceptionInfo, "ERROR", "s7Config_checkApplication: " + getCatStr("unGeneration", "BADINPUTS") + "Bad number of parameters", "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// s7Config_checkData
/**
Purpose: check PLC data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file
			dsPLC must contains :
						1	PLC_type (S7-300 or S7-400);
						2	PLC_name ;
						3	APPLICATION;
						4	Local_ID(hex) ;
						5	Local_Rack ;
						6	Local_Slot ;
						7	Local_ConnectionResource(hex) ;
						8	Partner_Rack ;
						9	Partner_Slot ;
						10	Partner_ConnectionResource(hex) ;
						11	Timeout[ms];
						12	PLC_IP;
						13	address_IP;
						14	address_counter ;
						15	address_CommandInterface (Request All, Syncro,...)

						16	Additional PLC info
					  17  PLC UNICOS application version address
						18	PVSS UNICOS application version number (float)

						exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

s7Config_checkData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCNumber;

    if (dynlen(dsPLC) != UN_CONFIG_S7_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_checkData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        // Check the PLC type (S7-300, S7-400)
        unConfigGenericFunctions_checkPLCType(dsPLC[UN_CONFIG_PLC_TYPE], exceptionInfo);

        if (strrtrim(dsPLC[UN_CONFIG_PLC_NAME]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_checkData:" + getCatStr("unGeneration", "ERRORPLCNAME"), "");
        }
        if (strrtrim(dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_checkData:" + getCatStr("unGeneration", "ERRORSUBAPPLI"), "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// s7Config_checkAdditionalData
/**
Purpose: check additional data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC additional data
			dsPLC must contains :
			1. unsigned, driver number
			2. string, boolean archive
			3. string, analog archive
			4. boolean, event16, event32 treatement
			5. string, event archive
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

s7Config_checkAdditionalData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCDriver;

    if (dynlen(dsPLC) != UN_CONFIG_PLC_ADDITIONAL_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_checkAdditionalData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        unConfigGenericFunctions_checkUnsigned(dsPLC[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], uPLCDriver, exceptionInfo);
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "") {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_checkAdditionalData: bool archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "") {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_checkAdditionalData: analog archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "") {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_checkAdditionalData: event archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------
// s7Config_setApplication
/**
Purpose: set PLC configuration for a sub application (not comm)

Parameters:
			dsConfig: dyn_string, input, S7 configuration line in file + additional parameters
			dsPLC must contains :

						1	PLC_type (S7-300 or S7-400);
						2	PLC_name ;
						3	APPLICATION;
						4	Local_ID(hex) ;
						5	Local_Rack ;
						6	Local_Slot ;
						7	Local_ConnectionResource(hex) ;
						8	Partner_Rack ;
						9	Partner_Slot ;
						10	Partner_ConnectionResource(hex) ;
						11	Timeout[ms];
						12	PLC_IP;
						13	address_IP;
						14	address_counter ;
						15	address_CommandInterface (Request All, Syncro,...)
						16  address_PLCData (New parameters to troubleshoot the PLC, i.e: cycle period)

					  17  PLC UNICOS application version address
						18	PVSS UNICOS application version number (float)

						19. driver number
						20. boolean archive name
						21. analog archive name
						22. False/True Event16,event32
						23. event archive name
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. First line of a generation file must contain the PLC description. Fields are defined in constants
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.

*/

s7Config_setApplication(dyn_string dsConfig, dyn_string &exceptionInfo) {
    dyn_string dsPLCData = dsConfig;
    int iPLCNumber, iRes, i, j, position;
    string sDpName, addressReference, tempDp, sSection, sDpNamePoll;
    dyn_string dsPLC, dsAdditionalParameters, dsPLCExtended, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
    dyn_string dsEntry, dsEventArchives, dsDriverLine;

    dyn_string dsSplitAddress;
    int sAddressFormat, iAddressWithinDB;
    bool bOk, bCPC6, bReduPlc;
    string sUserTag, sRSC_version, sRSC_majorAddr, sRSC_minorAddr, sRSC_smallAddr, sPLCBaseline, sPLCApplication;

    // DebugN("FUNCTION: s7Config_setApplication(dsPLCData= "+dsPLCData);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7_PLC " + dsConfig[UN_CONFIG_PLC_NAME], "s7Config_setApplication", dsConfig);

    // Check just number of parameters
    if (dynlen(dsPLCData) == (UN_CONFIG_S7_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) { // CPC6
        bOk = true;
        sRSC_version = dsPLCData[UN_CONFIG_S7_CPC6_RESPACK_VERSION];
        dynRemove(dsPLCData, UN_CONFIG_S7_CPC6_RESPACK_VERSION);
        sRSC_smallAddr = dsPLCData[UN_CONFIG_S7_CPC6_RESPACK_SMALL_VERSION_ADDRESS];
        dynRemove(dsPLCData, UN_CONFIG_S7_CPC6_RESPACK_SMALL_VERSION_ADDRESS);
        sRSC_minorAddr = dsPLCData[UN_CONFIG_S7_CPC6_RESPACK_MINOR_VERSION_ADDRESS];
        dynRemove(dsPLCData, UN_CONFIG_S7_CPC6_RESPACK_MINOR_VERSION_ADDRESS);
        sRSC_majorAddr = dsPLCData[UN_CONFIG_S7_CPC6_RESPACK_MAJOR_VERSION_ADDRESS];
        dynRemove(dsPLCData, UN_CONFIG_S7_CPC6_RESPACK_MAJOR_VERSION_ADDRESS);
        sPLCApplication = dsPLCData[UN_CONFIG_S7_CPC6_PLC_APPLICATION_ADDRESS];
        sPLCBaseline = dsPLCData[UN_CONFIG_S7_CPC6_PLC_BASELINE_ADDRESS];
        bCPC6 = true;
        sUserTag = "CPC6";
    } else if (dynlen(dsPLCData) == (UN_CONFIG_S7_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_S7_FE_VERSION_ADDITIONAL_LENGTH)) {
        bOk = true;
        sUserTag = dsPLCData[UN_CONFIG_S7_FE_VERSION];
// remove the version.
        dynRemove(dsPLCData, UN_CONFIG_S7_FE_VERSION);
    } else if (dynlen(dsPLCData) == (UN_CONFIG_S7_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        bOk = true;
    }

    if (!bOk) {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: " + getCatStr("unGeneration", "BADINPUTS"), "");
        return;
    }

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "S7_PLC " + dsPLCData[UN_CONFIG_PLC_NAME], "s7Config_setApplication, common configuration", dsPLCData);

    // 1. Separate config file info and additional parameters
    dsPLC = dsPLCData;
    //if double PLC number, then it is a redundant PLC
    bReduPlc = strpos(dsPLC[UN_CONFIG_PLC_NUMBER], UN_PARAMETER_SUBITEM_DELIMITER, 1) > 0;
    dynClear(dsAdditionalParameters);
    for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
        dynAppend(dsAdditionalParameters, dsPLC[UN_CONFIG_S7_LENGTH + 1]);
        dynRemove(dsPLC, UN_CONFIG_S7_LENGTH + 1);
    }

    // DebugN("----> dsPLCData=     					"+dsPLCData);
    // DebugN("----> dsAdditionalParameters= "+dsAdditionalParameters);

    // check if the PLC Application Name is sintactically correct:
    tempDp = dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION];		//UN_CONFIG_S7_APPLICATION
    if (!unConfigGenericFunctions_nameCheck(tempDp)) {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: " + dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
        return;
    }

    // 3. Set PLC configuration in S7
    sDpName = c_unSystemIntegrity_S7 + dsPLC[UN_CONFIG_PLC_NAME];		//S7_PLC_nameOfS7PLC
    tempDp = sDpName;

    // Creates the DPe of type S7_PLC with the name of the PLC
    unConfigGenericFunctions_createDp(sDpName, S7_PLC_DPTYPE, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        return;
    }

    sDpName = getSystemName() + sDpName;
    // Set the alias as the PLC name
    unConfigGenericFunctions_setAlias(sDpName, dsPLC[UN_CONFIG_PLC_NAME], exceptionInfo);

    // Set the dp taking into account the existing ones:
    // (it could be more than one application for PLC !!!
    iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
                 sDpName + ".configuration.importTime", dsImportTimes,
                 sDpName + ".configuration.archive_bool", dsBoolArchives,
                 sDpName + ".configuration.archive_analog", dsAnalogArchives,
                 sDpName + ".configuration.archive_event", dsEventArchives);
    position = dynContains(dsSubApplications, dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]); //UN_CONFIG_S7_APPLICATION
    if (position <= 0) {
        position = dynlen(dsSubApplications) + 1;
    }
    dsSubApplications[position] = dsPLC[UN_CONFIG_PLC_SUBAPPLICATION];
    dsImportTimes[position] = (string)getCurrentTime();
    dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
    dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
    dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];

    iRes = dpSetWait(sDpName + ".configuration.type", unConfigGenericFunctions_extractFeModel(dsPLC[UN_CONFIG_S7_PLC_TYPE]),
                     sDpName + ".configuration.S7_Conn", "_" + dsPLC[UN_CONFIG_PLC_NAME],
                     sDpName + ".configuration.importTime", dsImportTimes,
                     sDpName + ".configuration.archive_bool", dsBoolArchives,
                     sDpName + ".configuration.archive_analog", dsAnalogArchives,
                     sDpName + ".configuration.archive_event", dsEventArchives,
                     sDpName + ".configuration.subApplications", dsSubApplications,
                     sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                     sDpName + ".version.import", sUserTag);
    if (iRes < 0) {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: " + getCatStr("unPVSS", "DPSETFAILED"), "");
    }

    // 4. Create internal S7 PLC datapoints
    bool pollingFE = unConfigGenericFunctions_isPollingOnly(dsPLC[UN_CONFIG_PLC_TYPE]);
    // DebugN("FUNCTION create internal S7 datapoints");
    dyn_string slot = strsplit(dsPLC[UN_CONFIG_S7_LOCAL_SLOT], ",");
    if (dynlen(slot) == 0) { dynAppend(slot, ""); }
    if (dynlen(slot) == 1) { dynAppend(slot, slot[1]); }

    // 4.1 TSPP internal S7 datapoints configuration (_s7_conn, _s7_config)
    dsPLC[UN_CONFIG_S7_LOCAL_SLOT] = slot[1];
    s7Config_setInternalConnection(dsPLC, false, pollingFE, exceptionInfo);

    // 4.2 Polling internal S7 datapoints configuration (_s7_conn, _s7_config)
    dsPLC[UN_CONFIG_S7_LOCAL_SLOT] = slot[2];
    s7Config_setInternalConnection(dsPLC, true, pollingFE, exceptionInfo);

    // 4.3 Create a polling group to get the PLCInfo
    sDpNamePoll = "_poll_" + c_unSystemIntegrity_S7 + dsPLC[UN_CONFIG_PLC_NAME];
// DebugN("s7Config_setApplication - dsPLC[UN_CONFIG_PLC_NUMBER]",dsPLC[UN_CONFIG_PLC_NUMBER], strpos(dsPLC[UN_CONFIG_PLC_NUMBER],UN_PARAMETER_SUBITEM_DELIMITER,1));
    unConfigGenericFunctions_createDp(sDpNamePoll, "_PollGroup" , exceptionInfoTemp);
    iRes = dpSetWait(sDpNamePoll + ".SyncMode", 0,
                     sDpNamePoll + ".Active", true,
                     sDpNamePoll + ".PollInterval", pollingFE ? unConfigGenericFunctions_getPollingInterval(dsPLC[UN_CONFIG_PLC_TYPE]) : S7_POLL_PLCINFO_TIME);
    if (iRes < 0) {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: " + getCatStr("unPVSS", "DPSETFAILED"), "");
    }


    // 5. Create _UnSystemAlarm datapoints
    unSystemIntegrity_createSystemAlarm(tempDp,
                                        DS_pattern,
                                        getCatStr("unSystemIntegrity", "S7_PLC_DS_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                        exceptionInfo);

    fwArchive_set(c_unSystemAlarm_dpPattern + DS_pattern + tempDp + ".alarm",
                  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  0,
                  0,
                  exceptionInfo);

    unSystemIntegrity_createSystemAlarm(tempDp,
                                        DS_Time_pattern,
                                        getCatStr("unSystemIntegrity", "S7_PLC_DS_TIME_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                        exceptionInfo);

    fwArchive_set(c_unSystemAlarm_dpPattern + DS_Time_pattern + tempDp + ".alarm",
                  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  0,
                  0,
                  exceptionInfo);

    if (bReduPlc) {
        unSystemIntegrity_createSystemAlarm(tempDp,
                                            PLCRedu1_DS_Error_pattern,
                                            getCatStr("unSystemIntegrity", "S7_PLC_DS_ERROR_DESCRIPTION") + "1 " + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                            exceptionInfo);

        fwArchive_set(c_unSystemAlarm_dpPattern + PLCRedu1_DS_Error_pattern + tempDp + ".alarm",
                      dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exceptionInfo);

        unSystemIntegrity_createSystemAlarm(tempDp,
                                            PLCRedu2_DS_Error_pattern,
                                            getCatStr("unSystemIntegrity", "S7_PLC_DS_ERROR_DESCRIPTION") + "2 " + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                            exceptionInfo);

        fwArchive_set(c_unSystemAlarm_dpPattern + PLCRedu2_DS_Error_pattern + tempDp + ".alarm",
                      dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exceptionInfo);
    } else {
        unSystemIntegrity_createSystemAlarm(tempDp,
                                            PLC_DS_Error_pattern,
                                            getCatStr("unSystemIntegrity", "S7_PLC_DS_ERROR_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                            exceptionInfo);

        fwArchive_set(c_unSystemAlarm_dpPattern + PLC_DS_Error_pattern + tempDp + ".alarm",
                      dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exceptionInfo);
    }
    // 6. Set PLC configuration: send IP address and counter address
    int ADDR_MODE_INPUT = pollingFE ? DPATTR_ADDR_MODE_INPUT_POLL : DPATTR_ADDR_MODE_INPUT_SPONT;
    string POLL_GROUP = pollingFE ? sDpNamePoll : "";

    // 6.1 IP address
    // addressReference=dsPLC[UN_CONFIG_PLC_NAME]+S7_POLLING_COMMUNICATION+"."+dsPLC[UN_CONFIG_S7_ADD_IP];
    if (!pollingFE) {
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(DPATTR_ADDR_MODE_OUTPUT,
                dsPLC[UN_CONFIG_PLC_NAME],
                dsPLC[UN_CONFIG_S7_ADD_IP]),
                addressReference);

        // DebugN("IP addressReference= "+addressReference);
        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".communication.send_IP",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_OUTPUT,
                                              PVSS_S7_INT32,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              false,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              ""),
                                exceptionInfo);
        } else {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (send IP address)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }    
    }

    // 6.2 Counter address
    unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(ADDR_MODE_INPUT,
            dsPLC[UN_CONFIG_PLC_NAME],
            dsPLC[UN_CONFIG_S7_ADD_COUNTER]),
            addressReference);
    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".communication.counter",
                            makeDynString(fwPeriphAddress_TYPE_S7,
                                          dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          ADDR_MODE_INPUT,
                                          PVSS_S7_UINT16,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          POLL_GROUP),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (Counter)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // 6.3 Command Interface
    unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(DPATTR_ADDR_MODE_OUTPUT,
            dsPLC[UN_CONFIG_PLC_NAME],
            dsPLC[UN_CONFIG_S7_ADD_COMMANDINTERFACE]),
            addressReference);
    if (addressReference != "") {
        // function mapping
        fwPeriphAddress_set(sDpName + ".communication.commandInterface[1]",
                            makeDynString(fwPeriphAddress_TYPE_S7,
                                          dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          DPATTR_ADDR_MODE_OUTPUT,
                                          PVSS_S7_UINT16,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);

        // Parameters mapping (5)
        for (j = 2; j <= 6; j++) {
            // Add a "word" address to the addressReferencebase

            dsSplitAddress = strsplit(addressReference, ".");
            strreplace(dsSplitAddress[3], UN_S7_FORMAT_WORD, "");
            iAddressWithinDB = (int)dsSplitAddress[3];
            iAddressWithinDB = iAddressWithinDB + 2;
            dsSplitAddress[3] = UN_S7_FORMAT_WORD + iAddressWithinDB;
            addressReference = dsSplitAddress[1] + "." + dsSplitAddress[2] + "." + dsSplitAddress[3];

            // function mapping
            fwPeriphAddress_set(sDpName + ".communication.commandInterface[" + j + "]",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_OUTPUT,
                                              PVSS_S7_UINT16,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              false,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              ""),
                                exceptionInfo);
        }
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (CommandInterface)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // 6.4 PLC additional info
    unConfigGenericFunctions_getUnicosAddressReferenceS7(	makeDynString(DPATTR_ADDR_MODE_INPUT_POLL,
            dsPLC[UN_CONFIG_PLC_NAME],
            dsPLC[UN_CONFIG_S7_ADD_PLCINFO]),
            addressReference);
    if (addressReference != "") {
        dsSplitAddress = strsplit(addressReference, ".");
        strreplace(dsSplitAddress[3], UN_S7_FORMAT_WORD, "");
        iAddressWithinDB = (int)dsSplitAddress[3];

        // Parameters mapping (5)
        for (j = 1; j <= 5; j++) {
            dsSplitAddress[3] = UN_S7_FORMAT_WORD + iAddressWithinDB;
            addressReference = dsSplitAddress[1] + "." + dsSplitAddress[2] + "." + dsSplitAddress[3];

            // DebugN("Command addressReference= "+addressReference);
            // function mapping
            fwPeriphAddress_set(sDpName + ".communication.PLCInfo[" + j + "]",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_POLL,
                                              PVSS_S7_UINT16,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_POLLING_VALUES,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              sDpNamePoll),
                                exceptionInfo);

            // Add a "word" address to the addressReferencebase
            iAddressWithinDB = iAddressWithinDB + 2;
        }
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (PLCInfo)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    if (!bCPC6) {
        // 6.5 PLC UNICOS application version address
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(DPATTR_ADDR_MODE_INPUT_POLL,
                dsPLC[UN_CONFIG_PLC_NAME],
                dsPLC[UN_CONFIG_S7_ADD_PLC_VERSION]),
                addressReference);

        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".version.PLC",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_POLL,
                                              PVSS_S7_FLOAT,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_POLLING_VALUES,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              sDpNamePoll),
                                exceptionInfo);
        } else {
            if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (version PLC)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }

        // 6.6 PVSS UNICOS version value setting
        iRes = dpSetWait(sDpName + ".version.PVSS", dsPLC[UN_CONFIG_S7_PVSS_VERSION]);
        if (iRes < 0) {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: ", "Check PVSS version float number");
        }
    }

    // 7. Config line
    sSection = "s7_" + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM];
    dynClear(dsDriverLine);
    dynAppend(dsDriverLine, "maxOutputQueueSize = 1000000");
    //the following section entries are for redundant PLC:
    dynAppend(dsDriverLine, "AliveInterval = 5");
    dynAppend(dsDriverLine, "ReadOpState = 1");
    dynAppend(dsDriverLine, "UseConnections = 2");
    dynAppend(dsDriverLine, "ReadPLCTime = \"r;YES\"");
    dynAppend(dsDriverLine, "TimeSyncUTC = \"YES\"");
    dynAppend(dsDriverLine, "StatCheckInterval = 10");
    dynAppend(dsDriverLine, "reduModeTSPP = 1");
    dynAppend(dsDriverLine, "LimitedTSPPAliveCheck = \"No\"");
    iRes = fwInstallation_getSection(sSection, dsEntry);
    dynAppend(dsEntry, dsDriverLine);
    dynUnique(dsEntry);
    iRes = fwInstallation_setSection(sSection, dsEntry);
    if (iRes != 0) {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: cannot set config file", "");
    }

    if (bCPC6) {
        // set the UN_CONFIG_S7_CPC6_RESPACK_VERSION
        dpSet(sDpName + ".version.resourcePackage", sRSC_version);
        // set the address UN_CONFIG_S7_CPC6_RESPACK_SMALL_VERSION_ADDRESS
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(DPATTR_ADDR_MODE_INPUT_POLL,
                dsPLC[UN_CONFIG_PLC_NAME],
                sRSC_smallAddr),
                addressReference);
        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".version.PLCresourcePackageSmall",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_POLL,
                                              PVSS_S7_UINT16,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_POLLING_VALUES,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              sDpNamePoll),
                                exceptionInfo);
        } else {
            if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (PLCresourcePackageSmall)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
        // set the address UN_CONFIG_S7_CPC6_RESPACK_MINOR_VERSION_ADDRESS
        unConfigGenericFunctions_getUnicosAddressReferenceS7(	makeDynString(DPATTR_ADDR_MODE_INPUT_POLL,
                dsPLC[UN_CONFIG_PLC_NAME],
                sRSC_minorAddr),
                addressReference);
        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".version.PLCresourcePackageMinor",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_POLL,
                                              PVSS_S7_UINT16,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_POLLING_VALUES,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              sDpNamePoll),
                                exceptionInfo);
        } else {
            if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (PLCresourcePackageMinor)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
        // set the address UN_CONFIG_S7_CPC6_RESPACK_MAJOR_VERSION_ADDRESS
        unConfigGenericFunctions_getUnicosAddressReferenceS7(	makeDynString(DPATTR_ADDR_MODE_INPUT_POLL,
                dsPLC[UN_CONFIG_PLC_NAME],
                sRSC_majorAddr),
                addressReference);
        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".version.PLCresourcePackageMajor",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_POLL,
                                              PVSS_S7_UINT16,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_POLLING_VALUES,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              sDpNamePoll),
                                exceptionInfo);
        } else {
            if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (PLCresourcePackageMajor)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
        // set the address UN_CONFIG_S7_CPC6_PLC_BASELINE_ADDRESS
        unConfigGenericFunctions_getUnicosAddressReferenceS7(	makeDynString(DPATTR_ADDR_MODE_INPUT_POLL,
                dsPLC[UN_CONFIG_PLC_NAME],
                sPLCBaseline),
                addressReference);
        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".version.PLCBaseline",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_POLL,
                                              PVSS_S7_FLOAT,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_POLLING_VALUES,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              sDpNamePoll),
                                exceptionInfo);
        } else {
            if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (PLCBaseline)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
        // set the address UN_CONFIG_S7_CPC6_PLC_APPLICATION_ADDRESS
        unConfigGenericFunctions_getUnicosAddressReferenceS7(	makeDynString(DPATTR_ADDR_MODE_INPUT_POLL,
                dsPLC[UN_CONFIG_PLC_NAME],
                sPLCApplication),
                addressReference);
        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".version.PLCApplication",
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_POLL,
                                              PVSS_S7_FLOAT,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_POLLING_VALUES,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              sDpNamePoll),
                                exceptionInfo);
        } else {
            if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "s7Config_setApplication: (PLCApplication)" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------
// s7Config_setInternalConnection
/**
Purpose: set PLC S7 internal datapoints configuration for a TSPP connection

Parameters:
			dsPLC: dyn_string, input, S7 configuration line in file
			dsPLC must contains :

						1	PLC_type (S7-300 or S7-400);
						2	PLC_name ;
						3	APPLICATION;
						4	Local_ID(hex) ;
						5	Local_Rack ;
						6	Local_Slot ;
						7	Local_ConnectionResource(hex) ;
						8	Partner_Rack ;
						9	Partner_Slot ;
						10	Partner_ConnectionResource(hex) ;
						11	Timeout[ms];
						12	PLC_IP;
						13	address_IP;
						14	address_counter ;
						15	address_CommandInterface (Request All, Syncro,...)
						16	address_CommandInterface (Request All, Syncro,...)
						17	address_CommandInterface (Request All, Syncro,...)
						18	address_CommandInterface (Request All, Syncro,...)
      16..50 (spare items)
      --optional: redundant PLC information--
						51	Local_ID(hex) ;
						52	Local_Rack ;
						53	Local_Slot ;
						54	Local_ConnectionResource(hex) ;
						55	Partner_Rack ;
						56	Partner_Slot ;
						57	Partner_ConnectionResource(hex) ;
						58	Timeout[ms];
						59	PLC_IP;


		pollingConn: boolean, input, create a polling connection otherwise TSPP
		exceptionInfo: dyn_string, output, for errors

Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. First line of a generation file must contain the PLC description. Fields are defined in constants
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.

*/

s7Config_setInternalConnection(dyn_string dsPLC, bool pollingConn, bool pollingFE, dyn_string &exceptionInfo) {
    // variables for _S7_Conn
    bool bSetInvalidBit, bRedundant, bUseTSPP, bActive;
    int iS7_300, iResCPId, iResPCId, iLocalId;
    unsigned uDevNr;							// device
    string dp;
    const string REDUNDANT_CONNECTION = "REDUNDANTCONNECTION";

    // Variables for _S7_Config
    dyn_string dsIP;								// IP address
    dyn_string dsTSPPExtras;				// Connection Id, Res. CPId, PC-Rack, PC-Slot, Res. PCId, S7-300
    dyn_uint duRack;								// Rack
    dyn_uint duSlot;								// Slot
    dyn_uint duTimeout;						// Timeout
    dyn_uint duConnectionType;			// Connection Type

    // internal variables
    dyn_string allConnections;
    dyn_int allDevNr, diCurrDevNr;
    int i;
    bool isDefined, createDPE, bReduOk;
    dyn_string dsSplit, dsReduPLC;

    // Check the name of the PLC (it will be the dpe of the type:_s7_Conn)
    if (dsPLC[UN_CONFIG_S7_PLC_NAME] == "") {
        fwException_raise(exceptionInfo, "ERROR", "s7Config_setInternalConnection: " +
                          dsPLC[UN_CONFIG_S7_PLC_NAME] + getCatStr("unGeneration", "BADPLCNAME"), "");
        return;
    }

    // 0. Separate optional redundancy info, if exists
    if (strpos(dsPLC[UN_CONFIG_S7_LOCAL_ID], UN_PARAMETER_SUBITEM_DELIMITER) > 0) {
        bReduOk = true;
        //check that all other redu info is present
        for (i = UN_CONFIG_S7_LOCAL_ID ; i <= UN_CONFIG_S7_PLC_IP ; i++) {
            bReduOk = bReduOk && strpos(dsPLC[i], UN_PARAMETER_SUBITEM_DELIMITER);
            if (bReduOk) {
                dsSplit = strsplit(dsPLC[i], UN_PARAMETER_SUBITEM_DELIMITER);
                dsPLC[i] = dsSplit[1];
                if (dynlen(dsSplit) > 1) {
                    dsReduPLC[i] = dsSplit[2];
                } else {
                    //not all redundancy info is present:
                    //must be like, for example, "2|3", "2" for primary PLC, "|3" optionally for redu PLC
                    fwException_raise(exceptionInfo, "ERROR",
                                      "s7Config_setInternalConnection: " +
                                      getCatStr("unGeneration", "REDUFOUNDFIELDVALUE") + dsPLC[i] +
                                      getCatStr("unGeneration", "REDUBADPARAMETERS") + (i + 1) +
                                      getCatStr("unGeneration", "REDUMISSINGPARAM"), "");
                    return;
                }
            } else {
                //not all redundancy info is present: must be like 2|3
                fwException_raise(exceptionInfo, "ERROR",
                                  "s7Config_setInternalConnection: " +
                                  getCatStr("unGeneration", "REDUFOUNDFIELDVALUE") + dsPLC[i] +
                                  getCatStr("unGeneration", "REDUBADPARAMETERS") + (i + 1) +
                                  getCatStr("unGeneration", "REDUMISSINGDELIMITER") + UN_PARAMETER_SUBITEM_DELIMITER, "");
                return;
            }
        }
        //fill the other fields with fake info
        for (i = 1 ; i < UN_CONFIG_S7_LOCAL_ID ; i++) {
            dsReduPLC[i] = REDUNDANT_CONNECTION;
        }
    }

    // 1.- Create DPe PLC S7 (if already exists get the device number)
    dp = "_" + dsPLC[UN_CONFIG_S7_PLC_NAME];
    if (pollingConn) {
        dp = dp + S7_POLLING_COMMUNICATION;    // DPe name PLC connection (already formatted with prefix "_")
    }


    // Get Device Number
    // If DPE for the connection already exists then take the DevNr already used
    // otherwise find out the first free DevNr starting from 1!
    createDPE = true;
    if (dpExists(dp)) {
        if (dpTypeName(dp) == S7_PLC_INT_DPTYPE_CONN) {
            dpGet(dp + ".DevNr:_online.._value", uDevNr);
            createDPE = false;
        }
    }
    if (createDPE) {
        uDevNr = (unsigned)_unConfigS7_getFirstFreeDeviceNumber();

        // Create new DP for the PLC with the right device number
        // DebugN("Let's create the DPE: "+dp+" type: "+S7_PLC_INT_DPTYPE_CONN);
        dpCreate(dp, S7_PLC_INT_DPTYPE_CONN);
        dpSet(dp + ".DevNr:_original.._value", uDevNr);
    }

    // 2- Create the DPe "_S7_Config" if is no already created
    if (!dpExists(S7_PLC_INT_DPTYPE_CONF)) {
        for (i = 1; i <= 128; i++) {
            dsIP[i] = "";
            duRack[i] = 0u;
            duSlot[i] = 0u;
            duTimeout[i] = 5000u;
            duConnectionType[i] = 0u;
            dsTSPPExtras[i] = "";
        }

        dpCreate(S7_PLC_INT_DPTYPE_CONF, S7_PLC_INT_DPTYPE_CONF);
        dpSetWait(S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value", dsIP,
                  S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value", duRack,
                  S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value", duSlot,
                  S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value", duTimeout,
                  S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", duConnectionType,
                  S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTSPPExtras );

        dpSetWait(S7_PLC_INT_DPTYPE_CONF + ".DoConfigAcconAGLink:_original.._value", true);
    }

    //3 - Assign the data point elements of the type _S7_Conn
    bSetInvalidBit = UN_CONFIG_PLC_INVALID_BIT;		// UNICOS DEFAULTS
    bRedundant = UN_CONFIG_PLC_REDUNDANT;					//

    ////////////
    if (createDPE) {
        bActive = false; //start connection not active if new PLC
        dpSet(dp + ".Active:_original.._value", bActive);
    }
    ////////////
    if (pollingConn) {
        bUseTSPP = false;
    } else {
        bUseTSPP = !pollingFE;
    }

    dpSet(dp + ".SetInvalidBit:_original.._value", bSetInvalidBit,
          dp + ".UseTSPP:_original.._value", bUseTSPP,
          //			dp+".Active:_original.._value", bActive,
          dp + ".DevNr:_original.._value", uDevNr);


    //4 - Assing the data point elements of the type _S7_Config (CASE Connection TSPP)

    // Get the current values
    dpGet(S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", duConnectionType,
          S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value", dsIP,
          S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value", duRack,
          S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value", duSlot,
          S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value", duTimeout,
          S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTSPPExtras);

    if (pollingConn) {
        duConnectionType[uDevNr] = S7_CONN_TYPE_OP;
    } else {
        duConnectionType[uDevNr] = pollingFE ? S7_CONN_TYPE_OP : S7_CONN_TYPE_TSPP;
    }

    dsIP[uDevNr] = dsPLC[UN_CONFIG_S7_PLC_IP];
    duRack[uDevNr] = (unsigned)dsPLC[UN_CONFIG_S7_LOCAL_RACK];
    duSlot[uDevNr] = (unsigned)dsPLC[UN_CONFIG_S7_LOCAL_SLOT];
    duTimeout[uDevNr] = (unsigned)dsPLC[UN_CONFIG_S7_TIMEOUT];


    // TSPPExtras = "0.0.0.0.0.0";  localID, ResCPId and ResPCId must be converted from HEX to INT
    iS7_300 = 0;

    // Originally (PLC Siemens) they are in Hexadecimal
    sscanf(dsPLC[UN_CONFIG_S7_LOCAL_CONNRESOURCE], "%X", iResCPId);
    sscanf(dsPLC[UN_CONFIG_S7_PARTNER_CONNRESOURCE], "%X", iResPCId);
    sscanf(dsPLC[UN_CONFIG_S7_LOCAL_ID], "%X", iLocalId);

    dsTSPPExtras[uDevNr] = (string)iLocalId + "." +
                           (string)iResCPId + "." +
                           (string)dsPLC[UN_CONFIG_S7_PARTNER_RACK] + "." +
                           (string)dsPLC[UN_CONFIG_S7_PARTNER_SLOT] + "." +
                           (string)iResPCId + "." +
                           (string)iS7_300;

    dpSetWait(S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", duConnectionType,
              S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value", dsIP,
              S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value", duRack,
              S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value", duSlot,
              S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value", duTimeout);

    // TSPP extras only in case of TSPP connection
    if (!pollingConn) {
        if (pollingFE) {
            dpSetWait(dp + ".ConnState", 1);
        } else {
            dpSetWait(S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTSPPExtras);
        }
    }

    //5 - Redundancy: If the PLC is redundant, add redundant PLC connection

    //if PLC has redundant PLC info, add it as new PLC into the _S7_conn dp
    if (bReduOk) {
        bool bReduExists;
        int iReduDevNr = 0;


        if (!createDPE) { //if the PLC dpe already existed, check if it was already defined with redu info
            dpGet(dp + ".ReduCP.DevNr:_original.._value", iReduDevNr);
        }
        if (iReduDevNr == 0) {
            iReduDevNr = _unConfigS7_getFirstFreeDeviceNumber();
            bActive = false; //start connection not active if new PLC
            dpSetWait(dp + ".ReduCP.Active:_original.._value", bActive,
                      dp + ".ReduCP.DevNr:_original.._value", iReduDevNr);
        }

        //set the redu PLC internal connection settings

        //5.1 - Assign the data point elements of the type _S7_Config (CASE redu Connection TSPP)

        // Get the current values
        dpGet(S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", duConnectionType,
              S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value", dsIP,
              S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value", duRack,
              S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value", duSlot,
              S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value", duTimeout,
              S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTSPPExtras);

        if (pollingConn) {
            duConnectionType[iReduDevNr] = S7_CONN_TYPE_OP;
        } else {
            duConnectionType[iReduDevNr] = pollingFE ? S7_CONN_TYPE_OP : S7_CONN_TYPE_TSPP;
        }

        dsIP[iReduDevNr] = dsReduPLC[UN_CONFIG_S7_PLC_IP];
        duRack[iReduDevNr] = (unsigned)dsReduPLC[UN_CONFIG_S7_LOCAL_RACK];
        duSlot[iReduDevNr] = (unsigned)dsReduPLC[UN_CONFIG_S7_LOCAL_SLOT];
        duTimeout[iReduDevNr] = (unsigned)dsReduPLC[UN_CONFIG_S7_TIMEOUT];


        // Originally (PLC Siemens) they are in Hexadecimal
        sscanf(dsReduPLC[UN_CONFIG_S7_LOCAL_CONNRESOURCE], "%X", iResCPId);
        sscanf(dsReduPLC[UN_CONFIG_S7_PARTNER_CONNRESOURCE], "%X", iResPCId);
        sscanf(dsReduPLC[UN_CONFIG_S7_LOCAL_ID], "%X", iLocalId);

        dsTSPPExtras[iReduDevNr] = (string)iLocalId + "." +
                                   (string)iResCPId + "." +
                                   (string)dsReduPLC[UN_CONFIG_S7_PARTNER_RACK] + "." +
                                   (string)dsReduPLC[UN_CONFIG_S7_PARTNER_SLOT] + "." +
                                   (string)iResPCId + "." +
                                   (string)1;

        dpSet(S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", duConnectionType,
              S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value", dsIP,
              S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value", duRack,
              S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value", duSlot,
              S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value", duTimeout);

        // TSPP extras only in case of TSPP connection
        if (!pollingConn) {
            if (!pollingFE) dpSet(S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTSPPExtras);
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------


// _unConfigS7_getFirstFreeDeviceNumber
/**
Purpose: get the first available device number for the PLC connection
*/
int _unConfigS7_getFirstFreeDeviceNumber() {
    dyn_int allDevNr, diCurrDevNr;
    // Get all the defined connections and their deviceNumbers
    dyn_string		allConnections = dpNames("*", S7_PLC_INT_DPTYPE_CONN);
    bool isDefined;
    int iDevNr;
    int i;

    if (dynlen(allConnections) <= 0) {
        iDevNr = 1;
    } else {
        for (i = 1; i <= dynlen(allConnections); i++) {
            dpGet(allConnections[i] + ".DevNr:_online.._value", diCurrDevNr[1],
                  allConnections[i] + ".ReduCP.DevNr:_online.._value", diCurrDevNr[2]);
            dynAppend(allDevNr, diCurrDevNr);
        }
        dynUnique(allDevNr);
        // Find out the first free device number (not used)
        iDevNr = 0;
        isDefined = true;
        while (isDefined) {
            iDevNr++;				// first value should be 1
            isDefined = false;
            for (i = 1; i <= dynlen(allDevNr); i++)
                if (iDevNr == allDevNr[i]) {
                    isDefined = true;
                }
        }
    }
    return iDevNr;
}



//---------------------------------------------------------------------------------------------------------------------------------------

// s7Config_deletePLC
/**
Purpose: delete PLC config

Parameters :
	sPlcName, string, input, plc name
	exceptionInfo, dyn_string, output, for errors

Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

s7Config_deletePLC(string sPlcName, dyn_string& exceptionInfo) {
    string sDpPlcName, sPlcModPlc = "", exceptionText;
    int i, length, iRes;
    dyn_string dsAlarmDps;

    // variables for _S7_Conn
    bool bSetInvalidBit, bRedundant, bUseTSPP, bActive;
    int iS7_300, iResCPId, iResPCId, iLocalId;
    unsigned uDevNr = 0, uDevNr2 = 0;							// device number TSPP and polling (2)
    unsigned uReduDevNr = 0, uReduDevNr2 = 0;							// redundant device number TSPP and polling (2)
    string dp, dp_poll;

    // Variables for _S7_Config
    dyn_string dsIP;								// IP address
    dyn_string dsTSPPExtras;				// Connection Id, Res. CPId, PC-Rack, PC-Slot, Res. PCId, S7-300
    dyn_uint duRack;								// Rack
    dyn_uint duSlot;								// Slot
    dyn_uint duTimeout;							// Timeout
    dyn_uint duConnectionType;			// Connection Type

    // DebugN("FUNCTION: s7Config_deletePLC(sPlcName= "+sPlcName);

    sDpPlcName = c_unSystemIntegrity_S7 + sPlcName;
    if (!dpExists(sDpPlcName)) {
        sDpPlcName = "";
    }

    if (sDpPlcName != "") {
        if (unGenericDpFunctions_getSystemName(sDpPlcName) == getSystemName()) {
            // 1. Remove system integrity for this front-end (via RPC call)
            dpSetWait("unicosS7PLC_systemIntegrityInfo.interface.command", UN_SYSTEMINTEGRITY_DELETE
                      , "unicosS7PLC_systemIntegrityInfo.interface.parameters", makeDynString(sDpPlcName));
            delay(5);

            // 2. Delete S7_PlcName
            iRes = dpDelete(substr(sDpPlcName, strpos(sDpPlcName, ":") + 1));
            if (iRes < 0) {
                fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
            }

            // 3.	Delete S7 PLC internal Dpe's
            // 3.1 Delete _S7_Conn DPE (TSPP Connection)
            dp = "_" + sPlcName;
            dp_poll = dp + S7_POLLING_COMMUNICATION;

            // DPe for TSPP
            if (dpExists(dp)) {
                if (dpTypeName(dp) == S7_PLC_INT_DPTYPE_CONN) {
                    // get uDevNr to delete the right element of _S7_Config afterwards
                    dpGet(dp + ".DevNr:_online.._value", uDevNr,
                          dp + ".ReduCP.DevNr:_online.._value", uReduDevNr);
                    iRes = dpDelete(dp);
                    if (iRes < 0) {
                        fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + dp + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                    }
                } else {
                    fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + "Wrong type found: " + dpTypeName(dp) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                    return;
                }
            } else {
                fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + "No DPE to delete" + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                return;
            }

            // DPe for polling
            if (dpExists(dp_poll)) {
                if (dpTypeName(dp_poll) == S7_PLC_INT_DPTYPE_CONN) {
                    // get uDevNr to delete the right element of _S7_Config afterwards
                    dpGet(dp_poll + ".DevNr:_online.._value", uDevNr2,
                          dp_poll + ".ReduCP.DevNr:_online.._value", uReduDevNr2);
                    iRes = dpDelete(dp_poll);
                    if (iRes < 0) {
                        fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + dp_poll + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                    }
                } else {
                    fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + "Wrong type found: " + dpTypeName(dp) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                    return;
                }
            } else {
                fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + "No DPE to delete" + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                return;
            }
            // 3.2 Delete the corresponding part of the _S7_Config DPE

            // _S7_Config exists?
            if (!dpExists(S7_PLC_INT_DPTYPE_CONF)) {
                fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + "_S7_Config does not exists" + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
            } else {
                // Get the current values
                dpGet(S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", duConnectionType,
                      S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value", dsIP,
                      S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value", duRack,
                      S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value", duSlot,
                      S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value", duTimeout,
                      S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTSPPExtras);

                // default values assined to the right position TSPP
                duConnectionType[uDevNr] = 0u;
                dsIP[uDevNr] = "";
                duRack[uDevNr] = 0u;
                duSlot[uDevNr] = 0u;
                duTimeout[uDevNr] = 5000u;
                dsTSPPExtras[uDevNr] = "";

                // if redudnant: default values assined to the right position TSPP
                if (uReduDevNr > 0) {
                    duConnectionType[uReduDevNr] = 0u;
                    dsIP[uReduDevNr] = "";
                    duRack[uReduDevNr] = 0u;
                    duSlot[uReduDevNr] = 0u;
                    duTimeout[uReduDevNr] = 5000u;
                    dsTSPPExtras[uReduDevNr] = "";
                }

                // default values assined to the right position polling
                duConnectionType[uDevNr2] = 0u;
                dsIP[uDevNr2] = "";
                duRack[uDevNr2] = 0u;
                duSlot[uDevNr2] = 0u;
                duTimeout[uDevNr2] = 5000u;
                dsTSPPExtras[uDevNr2] = "";

                // if redudnant: default values assined to the right position TSPP
                if (uReduDevNr2 > 0) {
                    duConnectionType[uReduDevNr2] = 0u;
                    dsIP[uReduDevNr2] = "";
                    duRack[uReduDevNr2] = 0u;
                    duSlot[uReduDevNr2] = 0u;
                    duTimeout[uReduDevNr2] = 5000u;
                    dsTSPPExtras[uReduDevNr2] = "";
                }

                // Clear the right position
                dpSet(S7_PLC_INT_DPTYPE_CONF + ".ConnectionType:_original.._value", duConnectionType,
                      S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value", dsIP,
                      S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value", duRack,
                      S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value", duSlot,
                      S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value", duTimeout,
                      S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTSPPExtras);
            }

            // 5. Delete Polling group
            // DebugN("Delete Polling group (sDpName= "+"_poll_"+sDpPlcName);
            if (dpExists("_poll_" + sDpPlcName)) {
                iRes = dpDelete("_poll_" + sDpPlcName);
                // DebugN("Deleting Polling group (sDpName= "+"_poll_"+sDpPlcName);
                if (iRes < 0) {
                    fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + dp + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                }
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "s7Config_deletePLC: " + sDpPlcName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// S7_PLC_checkFESystemAlarm
/**
Purpose: check FE system alarm config line for DQGTW front-end

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

S7_PLC_checkFESystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo) {

    // DebugN("FUNCTION: S7_PLC_checkFESystemAlarm(dsConfig="+dsConfig+ " for info dynlen(dsConfig)="+dynlen(dsConfig));

    if (dynlen(dsConfig) != UN_CONFIG_CHECKFESYSTEM_ALARM_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "S7_PLC_checkFESystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }

    // Check format
    unConfigGenericFunctions_checkAddressS7(dsConfig[UN_CONFIG_CHECKFESYSTEM_ALARM_ADDRESS], UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo);
}


// ------------------------------------------------------------------------------------------------------------

// S7_PLC_setFESystemAlarm
/**
Purpose: set FEsystem alarm config line form SIEMENS PLC's

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		dsAdditionalParameters, dyn_string, input, additional parameters
			1. dp name
			2. plc name
			3. plc type
			4. plc number
			5. driver num
		dsAddress, dyn_string, output, for address

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP
	. distributed system: yes.
*/

S7_PLC_setFESystemAlarm(dyn_string dsConfig, dyn_string dsAdditionalParameters, dyn_string &dsAddress) {
    string sDpName, sPlcName, sPLCType, addressReference, sArchive;
    int iPLCNumber, driverNum;
    bool bOkRange = false;

    // DebugN("FUNCTION: S7_PLC_setFESystemAlarm(dsConfig= "+dsConfig+" dsAdditionalParameters= "+dsAdditionalParameters);

    if (dynlen(dsConfig) == UN_CONFIG_SETFESYSTEM_ALARM_LENGTH) {
        sPLCType = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_TYPE];
        iPLCNumber = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNUMBER];
        driverNum = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DRIVER];
        sDpName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DEVICENAME];
        sPlcName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNAME];
        sArchive = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_ARCHIVE];

        unConfigGenericFunctions_getUnicosAddressReferenceS7(	makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT,
                dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNAME],
                dsConfig[UN_CONFIG_SETFESYSTEM_ALARM_ADDRESS]),
                addressReference);


        // address reference (case polling)
        // addressReference=sPlcName+S7_POLLING_COMMUNICATION+"."+dsConfig[UN_CONFIG_SETFESYSTEM_ALARM_ADDRESS];

        if (addressReference != "") {
            dsAddress = makeDynString(fwPeriphAddress_TYPE_S7,
                                      driverNum,
                                      addressReference,
                                      DPATTR_ADDR_MODE_INPUT_SPONT,
                                      PVSS_S7_INT16,
                                      UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                      "",
                                      "",
                                      "",
                                      "",
                                      true,
                                      0,
                                      UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                      UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                      "");
        }
    }
}


//------------------------------------------------------------------------------------------------------------------------

// S7_PLC_getFrontEndArchiveDp
/**
Purpose: return the list of Dps that wil be archived during the set of the S7_PLC front-end

Parameters:
		sCommand: 		string, 		input, 	the current entry in the import file
		sDp: 					string, 		input,	the given dp: PLC or SystemAlarm  Dp
		sFEType: 			string, 		input, 	the front-end type
		dsArchiveDp: 	dyn_string, output, list of archivedDp

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

S7_PLC_getFrontEndArchiveDp(string sCommand, string sDp, string sFEType, dyn_string &dsArchiveDp) {

    // DebugN("FUNCTION: S7_PLC_getFrontEndArchiveDp(sCommand= "+sCommand+" sDp= "+sDp+" sFEType= "+sFEType);

    switch (sCommand) {
        case UN_PLC_COMMAND:
            dsArchiveDp = makeDynString(getSystemName() + c_unSystemAlarm_dpPattern + DS_pattern + sDp + ".alarm",
                                        getSystemName() + c_unSystemAlarm_dpPattern + DS_Time_pattern + sDp + ".alarm");
            break;
        case UN_FESYSTEMALARM_COMMAND:
            dsArchiveDp = makeDynString(sDp + ".alarm");
            break;
        default:
            dsArchiveDp = makeDynString();
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// S7_PLC_getFrontEndManagerConfig
/**
Purpose: return the driver config of S7_PLC front-end

Parameters:
		iDriverNumber: 		int, 		input, 	the driver number
		sName: 					string, 		output,	the name to display
		sDriverName: 			string, 		output, 	the manager name
		iManType: 	int, output, the manager type
		sCommandLine: 	int, output, the command line to start the manager

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

S7_PLC_getFrontEndManagerConfig(int iDriverNumber, string &sName, string &sDriverName, int &iManType, string &sCommandLine) {
    sName = "S7";
    sDriverName = "WCCOAs7";
    iManType = DRIVER_MAN;
    sCommandLine = "-num " + iDriverNumber;
}

//------------------------------------------------------------------------------------------------------------------------
#uses "unS7_PLC.ctl"

//@}


