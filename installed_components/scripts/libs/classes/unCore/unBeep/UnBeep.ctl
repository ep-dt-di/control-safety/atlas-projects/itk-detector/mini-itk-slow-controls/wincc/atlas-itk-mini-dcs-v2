#uses "fwGeneral/fwException.ctl"
#uses "fwGeneral/fwExceptionHandling.ctl"

enum UnBeepAlarmState
{
  OK = 0,
  AlarmMasked = 1,
  AlarmNotAck = 2,
  AlarmActive = 3,
  InvalidData = 100,
  DistDisconnected = 200,
  ConfigurationError = 998,
  NotConfigured = 999
};

// instances should not be constructed directly -> use UnBeep::getInstance()
// all initialization should be called explicitely with initialize()
// before termination of the instance, do the cleanup using finalize()

// there is a static instance of exceptionInfo, so that also usable by getInstance();



class UnBeep
{

  protected string m_device;    // string representation of devices, as configured in panels
  protected dyn_string m_deviceList;    // for those that need a list
  protected string m_systemName;        // name of dist system to trace
  protected string m_beepPanel;
  protected UnBeepAlarmState m_alarmState;
  protected bool m_isDistConnected;     // we cache the previous connectivity state to avoid unnecessary callbacks
  protected dyn_string m_connectedDPEs; // effective list of DPEs to which we connect

  protected shape m_refPanel;   // ref-panel which we will animate

  public string getBeepPanel()
  {
    return m_beepPanel;
  }

  public UnBeepAlarmState getAlarmState()
  {
    return m_alarmState;
  }

  public void setAlarmState(UnBeepAlarmState s)
  {
    m_alarmState = s;
    if (m_refPanel != 0) invokeMethod(m_refPanel, "RedrawWidget");
  }

  protected bool m_initialized = false; // set to true where fields are trustable; set to false when going for destruction
  protected static dyn_string excInfo;  // local exceptionInfo

  // returns true if there was an exception or false if there was none
  public static bool checkHandleExceptions()
  {
    if (dynlen(excInfo)) {
      if (myManType() == UI_MAN) fwExceptionHandling_display(excInfo);
      else DebugTN("UnBeep Exception:", exceptionInfo);
      dynClear(excInfo);
      return true;
    }
    return false;
  }

  protected UnBeep() { }

  public shared_ptr <UnBeep> selfptr = nullptr;       // selfptr is initialized in getInstance()

  public void initialize(shape refPnl)
  {
    // until we have dpConnect to object, we need to
    // make sure that the g_unBeepObject is populated
    m_refPanel = refPnl;
    g_unBeepObject = selfptr;
    m_initialized = true;       // mark that we may start to be reliable
    dpConnect("UnBeep_ApplicationReconfigurationCB", true,
              UN_APPLICATION_DPNAME + ".beep.AlarmDevice",
              UN_APPLICATION_DPNAME + ".beep.beepPanel",
              UN_APPLICATION_DPNAME + ".beep.beepText1",
              UN_APPLICATION_DPNAME + ".beep.beepText2");
  }

  public void finalize()
  {
    m_initialized = false;      // at first, mark that we are tearing down!
    if (g_unBeepObject == selfptr) g_unBeepObject = nullptr; // clean the global pointer
    DisconnectDPECallback();
    DisconnectDistCallback();
    shape nullShape = 0;
    m_refPanel = nullShape;
    setAlarmState(UnBeepAlarmState::NotConfigured);
    selfptr = nullptr;  // make it possible to deallocate
  }

  public static shared_ptr < UnBeep > getInstance(string className = "") {

    shared_ptr <UnBeep> instance = nullptr;

    if (className == "") return nullptr;
    if (className == "UnBeep") {
      instance = new UnBeep;
    } else {
      // otherwise dynamically load create an instance from class under libs/classes/unCore/unBeep/
      if (!checkLoadClass(className, excInfo)) return nullptr;
      instance = createInstance(className, excInfo);
    }
    if (instance != nullptr) instance.selfptr = instance;
    return instance;
  }

  protected bool DisconnectDPECallback()
  {
    if (dynlen(m_connectedDPEs)) {
      dpDisconnect("UnBeep_DPEChangedCB", m_connectedDPEs);
      dynClear(m_connectedDPEs);
    }
  }

  protected void ConnectDPECallback()
  {
    if (!dpExists(m_device)) {
      fwException_raise(excInfo, "ERROR",
                        "UnBeep: device" + pDeviceList[i] + "(" + devDP +
                        ") does not exist", "");
    } else {
      dynAppend(m_connectedDPEs, m_device);
      dynAppend(m_connectedDPEs, m_device + ":_online.._invalid");

      int rc = dpConnect("UnBeep_DPEChangedCB", true, m_connectedDPEs);
      if (rc) fwException_raise(excInfo, "ERROR",
                          "UnBeepCPC6 could not dpConnect to devices", "");
    }

    if (dynlen(excInfo)) {
      dynClear (m_connectedDPEs);
      DisconnectDistCallback();
      m_systemName = "";
      setAlarmState(UnBeepAlarmState::ConfigurationError);
    }
  }


  // need to be public for the time being to allow calls from external cb functions
  public void ValuesChangedCB(dyn_string dpes, dyn_mixed values)
  {
    UnBeepAlarmState state = UnBeepAlarmState::OK;

    if (dynlen(dpes) < 2)       state = UnBeepAlarmState::NotConfigured;
    else if (values[2] == TRUE) state = UnBeepAlarmState::InvalidData;
    else if (values[1] != 0)    state = UnBeepAlarmState::AlarmActive;

    setAlarmState(state);
  }


  // nees to be public, as it is a callback
  public void AppConfigChanged(string beepDevices, string beepPanel, string beepText1, string beepText2)
  {
    if (m_initialized) {
      m_beepPanel = beepPanel;
      if (m_refPanel != 0) {
        invokeMethod(m_refPanel, "SetText1", beepText1);
        invokeMethod(m_refPanel, "SetText2", beepText2);
      }
      if (beepDevices != m_device) DeviceConfigurationChanged(beepDevices);
    }
  }

  protected void DeviceConfigurationChanged(string device)
  {
    DisconnectDPECallback();
    DisconnectDistCallback();
    m_device = device;
    m_systemName = "";  // reset;
    dynClear(m_deviceList);     // reset the device list!
    if (device == "") {
      setAlarmState(UnBeepAlarmState::NotConfigured);
      return;
    }

    bool ok = SetupNewDeviceList();

    // to connect unDistributed callback, which will trigger registration of device callbacks in turn
    if (ok) ok = ConnectDistCallback();       // we should check if it succeeded, handle errors, etc

    if (!ok) setAlarmState(UnBeepAlarmState::ConfigurationError);

    checkHandleExceptions();
  }

  /**
    To be implemented in child classes.
    Called from DeviceConfigurationChanged(), it should
    re-populate the m_deviceList and m_systemName
    based on the m_device.

    Other things, such as disconnection/reconnection of callbacks
    are done in DeviceConfigurationChanged()
  */
  protected bool SetupNewDeviceList()
  {
    if (!dpExists(m_device)) {
      fwException_raise(excInfo, "ERROR",
                        "UnBeep configuration: datapoint does not exist: " +
                        m_device, "");
      return false;
    } else {
      m_deviceList = m_device;
      m_systemName = dpSubStr(m_device, DPSUB_SYS);
      return true;
    }
  }


  protected bool ConnectDistCallback()
  {
    // use unDistributed to trace m_SystemName connectivity
    if (m_systemName == "") {
      fwException_raise(excInfo, "ERROR",
                        __FUNCTION__ + " called with empty systemName", "");
      return false;
    }

    bool initOK;
    unDistributedControl_register("UnBeep_DistConnectionCB", initOK,
                                  m_isDistConnected, m_systemName, excInfo);
    if (dynlen(excInfo)) {
      setAlarmState(UnBeepAlarmState::ConfigurationError);
      return false;
    }
    // it might be that the system is not yet connected at this moment;
    // reflect it in the state
    if (!m_isDistConnected)
      setAlarmState(UnBeepAlarmState::DistDisconnected);
    return true;
  }

  protected bool DisconnectDistCallback()
  {
    bool isConnected, initOK;
    if (m_systemName != "") {
      unDistributedControl_deregister("UnBeep_DistConnectionCB", initOK,
                                      isConnected, m_systemName, excInfo);
      setAlarmState(UnBeepAlarmState::DistDisconnected);
    }
    m_isDistConnected = isConnected;
  }

  // needs to be public to be called from external CB function
  public void DistConnectionCB(bool connected)
  {
    if (connected == m_isDistConnected) return;

    m_isDistConnected = connected;
    if (connected) {
      ConnectDPECallback();     // this will trigger value-callbacks anyway
    } else {
      DisconnectDPECallback();
      setAlarmState(UnBeepAlarmState::DistDisconnected);
    }
  }

};








// at the moment in 3.15 dpConnect does not seem to support objects (feature of 3.16)
// hence, poor-man's implementation for callbacks

// for that, we need to have access to the instance of the object.
// here we declare a pointer, which will be a separate instance for every every refpanel
shared_ptr <UnBeep> g_unBeepObject = nullptr;

void UnBeep_ApplicationReconfigurationCB(string dpBeepDevice,
                                         string beepDevice,
                                         string dpBeepPanel, string beepPanel,
                                         string dpBeepText1, string beepText1,
                                         string dpBeepText2, string beepText2)
{
  // avoid nullptr race conditions with finalization mechanism by caching the ptr locally
  shared_ptr <UnBeep> beepObj = g_unBeepObject;
  if (beepObj != nullptr) beepObj.AppConfigChanged(beepDevice, beepPanel, beepText1, beepText2);
}


// we make these callback public so that they are reusable in child classes
public void UnBeep_DistConnectionCB(string dp, bool isConnected)
{
  shared_ptr <UnBeep> beepObj = g_unBeepObject;
  if (beepObj != nullptr) beepObj.DistConnectionCB(isConnected);
}



public void UnBeep_DPEChangedCB(dyn_string dpes, dyn_mixed values)
{
  shared_ptr <UnBeep> beepObj = g_unBeepObject;
  if (beepObj != nullptr) beepObj.ValuesChangedCB(dpes, values);
}


// --------------------------- HELPER FUNCTIONS ----------------------------------------------------------------


// this function should be replaced by a proper class loaded in future
private bool checkLoadClass(string className, dyn_string & exceptionInfo,
                            string pathPrefix = "classes/unCore/unBeep")
{
  if (className == "") {
    fwException_raise(exceptionInfo, "ERROR", "Class name may not be empty.","");
    return false;
  };

  string classFile = pathPrefix + "/" + className + ".ctl";     //should we also handle .ctc, and files with no extensions?

  string classFilePath = getPath(LIBS_REL_PATH, classFile);
  if (classFilePath == "") {
    fwException_raise(exceptionInfo, "ERROR", "Cannot find file to instantiate class " + className, "");
    return false;
  };

  int rc = access(classFilePath, R_OK);
  if (rc != 0) {
    fwException_raise(exceptionInfo, "ERROR", "Class file not accessible:" + classFile, "");
    return false;
  };

  // try to load it with #uses through evalScript
  string script = "#uses \"" + classFile + "\" bool main(){return 1;}";

  // in future we should have classExists(), etc from CtrlOOUtils

  anytype retval;
  rc = evalScript(retval, script, makeDynString());
  if (rc != 0) {
    fwException_raise(exceptionInfo, "ERROR","Class could not be loaded dynamically." + classFile, "");
    return false;
  };

  return true;
}

// creating instances with on-demand class should also be delegated to CtrlOOUtils function
//  -> it should do all the error checking, type-compatibility verif, etc
private shared_ptr <UnBeep> createInstance(string className, dyn_string & exceptionInfo)
{
  string script = "shared_ptr<void> main(){return new " + className + ";}";

  anytype retval;
  int rc = evalScript(retval, script, makeDynString());
  if (rc != 0) {
    fwException_raise(exceptionInfo, "ERROR", "Class could not be instantiated." + className, "");
    return nullptr;
  };

  shared_ptr <UnBeep> obj = nullptr;  //=retval;
  try {
    assignPtr(obj, retval);
  }
  catch {
    dyn_errClass exc = getLastException();
    fwException_raise(exceptionInfo, "ERROR", "Object of class " + className + " is not derived from class UnBeep", getErrorText(exc));
    return nullptr;
  }
  return obj;
}
