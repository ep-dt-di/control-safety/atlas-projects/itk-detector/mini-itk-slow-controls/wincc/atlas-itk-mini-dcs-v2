#uses "classes/unCore/unBeep/UnBeep.ctl"

class UnBeepCPC6:UnBeep
{

  protected bool SetupNewDeviceList()
  {
    // we expect a set of devices of "CPC_AnalogAlarm" or "CPC_DigitalAlarm"
    // in a coma-separated list
    m_deviceList = strsplit(m_device, UN_CONFIG_GROUP_SEPARATOR);
    if (dynlen(m_deviceList) < 1)
    {
      setAlarmState(UnBeepAlarmState::NotConfigured);
      return true;
    }

    // all the DPs need to be from the same system, and we will use
    // this system name in various unicos registration functions
    // we will extract it from the first device, and then check
    // if all others are OK.

    m_systemName = unGenericDpFunctions_getSystemName(m_deviceList[1]);

    // note that at this point the system on which devices are might have not connected yet
    // hence all checking for existence of them should be done in the unDist callback at earliest
    bool ok = true;
    for (int i = 1; i <= dynlen(m_deviceList); i++) {
      string s = unGenericDpFunctions_getSystemName(m_deviceList[i]);
      if (s != m_systemName) {
        setAlarmState(UnBeepAlarmState::ConfigurationError);
        fwException_raise(exceptionInfo, "ERROR", "unBeep: device " + m_deviceList[i] + " is not from system " + m_systemName, "");
        ok = false;
      }
    }
    return ok;
  }


  protected void ConnectDPECallback()
  {
    for (int i = 1; i <= dynlen(m_deviceList); i++) {
      // convert device name to DP
      string devDP = unGenericDpFunctions_getWidgetDpName(m_deviceList[i]);
      if (!dpExists(devDP)) {
        fwException_raise(excInfo, "ERROR", "UnBeepCPC6: device " + m_deviceList[i] + " does not exist", "");
        continue;       // we will handle all errors together
      }

      string devDPT = dpTypeName(devDP);
      switch (devDPT) {
        case "CPC_AnalogAlarm":
        case "CPC_DigitalAlarm":
        case "Alarm":    // bk-compat for CPC5
          dynAppend(m_connectedDPEs, devDP + ".ProcessInput.StsReg01");
          dynAppend(m_connectedDPEs, devDP + ".ProcessInput.StsReg01:_online.._invalid");
          break;
        default:
          fwException_raise(excInfo, "ERROR", "UnBeepCPC6: device " + m_deviceList[i] + 
                            " (" +devDP + ") is of unsupported type " + dpTypeName(devDP)+
                            " . Allowed CPC_AnalogAlarm, CPC_DigitalAlarm, Alarm(CPC5 compat)", "");
          break;
        }
      }

    if (!dynlen(excInfo) && dynlen(m_connectedDPEs)) {
      // we use the callback declared in UnBeep.ctl
      int rc = dpConnect("UnBeep_DPEChangedCB", m_connectedDPEs);
      if (rc)
        fwException_raise(excInfo, "ERROR", "UnBeepCPC6 could not dpConnect to devices", "");
    }

    if (dynlen(excInfo)) {
      DebugTN(excInfo);
      dynClear(m_connectedDPEs);
      DisconnectDistCallback();
      m_systemName = "";
      setAlarmState(UnBeepAlarmState::ConfigurationError);
    }
  }


  // need to be public for the time being to allow calls from external cb functions
  public void ValuesChangedCB(dyn_string dpes, dyn_mixed values)
  {
    // We may have more than one device used, each being in different state
    // hence we need to calculate the effective state of max gravity
    UnBeepAlarmState state = UnBeepAlarmState::OK;

    // As a callback parameters we have pairs of DPE:_online.._value and DPE:_online.._invalid
    if (dynlen(dpes) < 1) {
      state = UnBeepAlarmState::NotConfigured;
    } else {
      // let's see all the :_online.._invalid
      for (int i = 2; i <= dynlen(dpes); i += 2) {
        if (values[i] == TRUE) {
          state = UnBeepAlarmState::InvalidData;
          break;
        }
      }

      for (int i = 1; i <= dynlen(dpes); i++) {
        if      (getBit(values[i], UN_STSREG01_ALMSKST) && (int) state < (int) UnBeepAlarmState::AlarmMasked) state = UnBeepAlarmState::AlarmMasked;
        else if (getBit(values[i], UN_STSREG01_ALUNACK) && (int) state < (int) UnBeepAlarmState::AlarmNotAck) state = UnBeepAlarmState::AlarmNotAck;
        else if (getBit(values[i], UN_STSREG01_POSST)   && (int) state < (int) UnBeepAlarmState::AlarmActive) state = UnBeepAlarmState::AlarmActive;
      }
    }

    setAlarmState(state);       // this already triggers the redraw
  }
};
