#uses "classes/unCore/unBeep/UnBeep.ctl"

class UnBeepCryo: UnBeep {

    protected bool SetupNewDeviceList() {
	fwException_raise(excInfo,"ERROR",__FUNCTION__+" not implemented","");
	return false;
    }

    protected void ConnectDPECallback() {
	fwException_raise(excInfo,"ERROR",__FUNCTION__+" not implemented","");
    }

    public void ValuesChangedCB(dyn_string dpes, dyn_mixed values) {
	DebugTN(__FUNCTION__,"Not implemented",dpes,values);
    }
};
