/**@file

// cpcDeprecated.ctl
This library contains all the deprecated functions from unCPC.

@par Creation Date
  22/06/2018

@author
  Marcin Bes (BE-ICS-FD)
*/

/**Returns the full name of the PLC data point. Checks that PLC name is not empty and that PLC dp exists inside DB.

@deprecated 2018-06-22

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sSystemName The name of the distributed system (can be empty if not set).
@param sPlcName    The name of the plc.
@param sPlcDpName  The full name of the plc dp.
@param exceptionInfo The list of exceptions.
*/
void cpcConfigGenericFunctions_getPlcDpName(string sSystemName, string sPlcName, string &sPlcDpName, dyn_string &exceptionInfo) {
    FWDEPRECATED();
    sPlcDpName = "";

    if ("" == sPlcName) {
        dynAppend(exceptionInfo, "Plc name is not specified");
        return;
    }

    string sTemp = sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName;
    if (!dpExists(sTemp)) {
        dynAppend(exceptionInfo, "Dp for specified Plc does not exist. Plc name: " + sPlcName + ", system name: " + sSystemName);
        return;
    }
    sPlcDpName = sTemp;
}

/**Disable colorbox

@deprecated 2018-06-22

@todo move to the unFaceplate

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape graphical object name
*/
void cpcGenericObject_ColorBoxDisable(string sShape) {
    FWDEPRECATED();
    if (g_bSystemConnected) {
        setMultiValue(sShape + ".colorbox", "enabled", false,
                      sShape + ".colorbox", "backCol", "unFaceplate_Disabled");
    }
}

/**Cloned from UN_INFOS/unWidget_SelectArea.pnl
TODO: move to unCore

@deprecated 2018-06-22

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
cpcGenericObject_selectDevice(string deviceName, string deviceType) {
    FWDEPRECATED();
    dyn_string exceptionInfo;
    string sManager, localManager;
    bool granted, bLocked;
    int iRes;

    if (deviceName != "") {
        if (dpExists(deviceName + ".statusInformation.selectedManager")) {
            unGenericButtonFunctionsHMI_isAccessAllowed(deviceName, deviceType, UN_FACEPLATE_BUTTON_SELECT, granted, exceptionInfo);
            iRes = dpGet(deviceName + ".statusInformation.selectedManager:_lock._original._locked", bLocked,
                         deviceName + ".statusInformation.selectedManager", sManager);
            if (iRes >= 0) {
                localManager = unSelectDeselectHMI_getSelectedState(bLocked, sManager);
                granted = granted && (localManager == "D");
            }
            if (granted) {
                unGenericObject_ButtonSelectAction(deviceName, true, exceptionInfo);
            }
        }
    }
}

/**Animate AC / WC  widget

@deprecated 2018-06-22

@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  UI
*/
void cpcGenericObject_WidgetXC_Animation(bool bLocked, string sSelectedManager, bit32 bit32StsReg01, bool bStsReg01Invalid,
        string sDpPosOV, int iPosOV, bool bInvalidBody, bool bInvalidLetter,
        int iSystemIntegrityAlarmValue, bool bSystemIntegrityAlarmEnabled, string sEvtMask, int iMask, bool lookupForLabel) {
    FWDEPRECATED();
    string sFormattedValue;
    string sWarningLetter, sWarningColor, sControlLetter, sControlColor, sBody1Text, sBody1Color, sSelectColor;
    bool bLockVisible;

    // Control animation
    sBody1Color = "unWidget_ControlStateAuto";					// Auto mode (default)
    sControlLetter = "";
    if ((!bStsReg01Invalid) && (getBit(bit32StsReg01, CPC_StsReg01_FOMOST) == 1)) {
        sBody1Color = "unWidget_ControlStateForced";				// Forced mode
        sControlLetter = CPC_WIDGET_TEXT_CONTROL_FORCED;
        sControlColor = "unWidget_ControlStateForced";
    }
    if ((!bStsReg01Invalid) && (getBit(bit32StsReg01, CPC_StsReg01_MMOST) == 1)) {
        sBody1Color = "unWidget_ControlStateManualLocal";			// Manual mode
        sControlLetter = CPC_WIDGET_TEXT_CONTROL_MANUAL;
        sControlColor = "unWidget_ControlStateManualLocal";
    }
    if (!bStsReg01Invalid) {
        unGenericObject_WidgetWarningAnimation(bit32StsReg01,
                                               makeDynInt(CPC_StsReg01_AUMRW, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW),
                                               makeDynString(CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR),
                                               sWarningLetter, sWarningColor);
        if (bInvalidLetter) {
            sWarningLetter = CPC_WIDGET_TEXT_OLD_DATA;
            sWarningColor = "unDataNotValid";
        }
    }// if

    if ((!bSystemIntegrityAlarmEnabled) || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)) {
        sWarningLetter = CPC_WIDGET_TEXT_OLD_DATA;
        sWarningColor = "unDataNotValid";
    }

    if (lookupForLabel && mappingHasKey(g_mLabels, (string)iPosOV)) {
        sBody1Text = g_mLabels[(string) iPosOV];
    } else {
        sFormattedValue = unGenericObject_FormatValue(g_sPosOvFormat, iPosOV);
        sBody1Text = sFormattedValue + " " + g_sPosOvUnit;
    }

    if (bInvalidBody) {
        sBody1Color = "unDataNotValid";
    }

    if (bInvalidLetter) {
        sWarningLetter = CPC_WIDGET_TEXT_INVALID;
        sWarningColor = "unDataNotValid";
    }

    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sSelectColor, bLockVisible);
    if (g_bSystemConnected) {
        setMultiValue("Body1", "text", sBody1Text, /*"Body1", "fill", "[solid]", */
                      "Body1", "foreCol", sBody1Color, /*"Body1", "backCol", "unWidget_Background",*/
                      "WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "ControlStateText", "text", sControlLetter, "ControlStateText", "foreCol", sControlColor,
                      "SelectArea", "foreCol", sSelectColor, "LockBmp", "visible", bLockVisible,
                      "WidgetArea", "visible", true);
    }

// display event state
    if (g_bSystemConnected) {
        if (iMask == 0) {
            eventState.visible	= true;
        } else {
            eventState.visible	= false;
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// cpcRcpBuffers_StateAnimation
/** animate the device State widget

@deprecated 2018-06-26

!!!!! function trigger by exec call, $-param and variable of the widget cannot be used, all the necessary data
must be given to the function. The use of global var (global keyword declaration) is allowed

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param blobData input, device data
@param bSystemConnected input, connection state of the system, true=system connected/false=system not connected
@param sWidgetType input, the type of widget
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value
*/
cpcRcpBuffers_StateAnimation(blob blobData, bool bSystemConnected, string sWidgetType, int iFESystemIntegrityAlarmValue, bool bFESystemIntegrityAlarmEnabled) {
    /*!!!!!!!! all parameters must be given: function launched by exec ***/
    // encapsulate all the setValue or function call with the following if(bSystemConnected)
    if (bSystemConnected) {
        setMultiValue("Body1", "foreCol", blobData, "Body1", "text", blobData);
    }
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un widget CPC_RcpBuffers State", "cpcRcpBuffers_StateAnimation", sWidgetType, blobData, bSystemConnected,
                                 iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// cpcRcpBuffers_StateDisconnection
/** animate the the device State widget when the system is disconnected

@deprecated 2018-06-26

!!!!! function trigger by exec call, $-param and variable of the widget cannot be used, all the necessary data
must be given to the function. The use of global var (global keyword declaration) is allowed

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sWidgetType input, the type of widget
*/
cpcRcpBuffers_StateDisconnection(string sWidgetType) {
    // set all the graphical element of the widget
    setMultiValue("Body1", "backCol", "unDataNoAccess",
                  "WarningText", "text", "",
                  "WidgetArea", "visible", false,
                  "SelectArea", "foreCol", "",
                  "LockBmp", "visible", false);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un widget CPC_RcpBuffers State", "cpcRcpBuffers_StateDisconnection", sWidgetType);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// cpcRcpBuffers_WidgetCB
/** callback function on the device data

@deprecated 2018-06-26


@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpLocked input, the lock DPE
@param bLocked input, the lock state
@param sDpSelectedManager input, the selected manager DPE
@param sSelectedManager input, the selected manager
@param sDp1 input, the DPE1
@param blobData input, device data
@param sDpInvalid input, the invalid DPE
@param bInvalid input, data validity
@param sDpFESystemIntegrityAlarmValue input, front-end device system integrity alarm DPE
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param sDpFESystemIntegrityAlarmEnabled input, front-end device system integrity enable DPE
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value
*/
cpcRcpBuffers_WidgetCB(string sDpLocked, bool bLocked,
                       string sDpSelectedManager, string sSelectedManager,
                       string sDpFESystemIntegrityAlarmValue, int iFESystemIntegrityAlarmValue,
                       string sDpFESystemIntegrityAlarmEnabled, bool bFESystemIntegrityAlarmEnabled) {
    /** use the exec mechanism to allow multiple widget. **/
    string sFunction = "unUnRcpBuffers_" + g_sWidgetType + "Animation";
    string selectColor, sWarningLetter, sWarningColor;
    bool bSelectVisible;

    // 1. set the widget.
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, bSelectVisible);


    // if Front-end problem or problems-> O letter
    if ((!bFESystemIntegrityAlarmEnabled) || (iFESystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)) {
        sWarningLetter = UN_WIDGET_TEXT_OLD_DATA;
        sWarningColor = "unDataNotValid";
    }

    if (g_bSystemConnected)
        setMultiValue("WarningText", "text", sWarningLetter,
                      "WarningText", "foreCol", sWarningColor,
                      "SelectArea", "foreCol", selectColor,
                      "LockBmp", "visible", bSelectVisible,
                      "WidgetArea", "visible", true);

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un widget CPC_RcpBuffers ", "cpcRcpBuffers_WidgetCB", g_sWidgetType,
                                 sWarningLetter, sWarningColor, g_bSystemConnected,
                                 selectColor, bSelectVisible);
    if (isFunctionDefined(sFunction) && (sFunction != "")) {
        // set here all the common graphical element value or
        // give to the function all the variables
        execScript("main(blob blobData, bool bSystemConnected, string sWidgetType, int iFESystemIntegrityAlarmValue, bool bFESystemIntegrityAlarmEnabled) {" +
                   sFunction + "(blobData, bSystemConnected, sWidgetType, iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);}",
                   makeDynString(), blobData, g_bSystemConnected, g_sWidgetType, iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// cpcRcpBuffers_WidgetConnect
/** dpConnect to the device data

@deprecated 2018-06-26


@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name
*/
cpcRcpBuffers_WidgetConnect(string deviceName, string sFrontEnd) {
    int iRes;
    string sSystemName;

    // check if the device name is of type "CPC_RcpBuffers" if not like no front-end. -> not available.
    if (dpTypeName(deviceName) != "CPC_RcpBuffers") {
        g_bUnSystemAlarmPlc = false;
    }

    sSystemName = unGenericDpFunctions_getSystemName(deviceName);

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un widget CPC_RcpBuffers " + deviceName, "cpcRcpBuffers_WidgetConnect", deviceName, g_bUnSystemAlarmPlc);
    if (g_bUnSystemAlarmPlc) {
        // set all widget global variable usefull for the callback, e.g.: description, format, unit, etc.

        // if there are many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
        // keep the kind of dpConnect in a variable global to the widget and do a swith for the dpConnect
        // and do to the correct dpDisconnect in the unUnRcpBuffers_WidgetDisconnect
        iRes = dpConnect("unUnRcpBuffers_WidgetCB",
                         deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                         deviceName + ".statusInformation.selectedManager",
                         //          deviceName + ".ProcessInput.data",
                         //          deviceName + ".ProcessInput.data:_online.._invalid",
                         sFrontEnd + ".alarm",
                         sFrontEnd + ".enabled");
        g_bCallbackConnected = (iRes >= 0);
    } else {
        cpcRcpBuffers_WidgetDisconnection(g_sWidgetType);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// cpcRcpBuffers_WidgetDisconnect
/** dpDisconnect to the device data

@deprecated 2018-06-26

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name

*/
cpcRcpBuffers_WidgetDisconnect(string deviceName, string sFrontEnd) {
    int iRes;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un widget CPC_RcpBuffers " + deviceName, "cpcRcpBuffers_WidgetDisconnect", deviceName, g_bUnSystemAlarmPlc);
    if (g_bUnSystemAlarmPlc) {
        // if there were many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
        // re-use the kind of dpConnect kept in a variable global to the widget set during cpcRcpBuffers_WidgetConnect
        // and do to the correct dpDisconnect
        iRes = dpDisconnect("unUnRcpBuffers_WidgetCB",
                            deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                            deviceName + ".statusInformation.selectedManager",
                            //             deviceName + ".ProcessInput.data",
                            //             deviceName + ".ProcessInput.data:_online.._invalid",
                            sFrontEnd + ".alarm",
                            sFrontEnd + ".enabled");
        g_bCallbackConnected = !(iRes >= 0);
    }
}

// cpcRcpBuffers_WidgetDisconnection
/** set the widget when the system is disconnected

@deprecated 2018-07-30

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sWidgetType input, the type of widget
*/
cpcRcpBuffers_WidgetDisconnection(string sWidgetType) {
    /** use the exec mechanism to allow multiple widget. **/
    string sFunction = "unUnRcpBuffers_" + sWidgetType + "Disconnection";

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un widget UnRcpBuffers", "cpcRcpBuffers_WidgetDisconnection", sWidgetType);
    if (isFunctionDefined(sFunction) && (sFunction != "")) {
        execScript("main(string sWidgetType) {" + sFunction + "(sWidgetType);}", makeDynString(), sWidgetType);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// cpcRcpBuffers_WidgetRegisterCB
/** widget DistributedControl callback

@deprecated 2018-06-26


@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/
cpcRcpBuffers_WidgetRegisterCB(string sDp, bool bSystemConnected) {
    string deviceName, sSystemName;
    int iAction;
    dyn_string exceptionInfo;
    string sFrontEnd;
    bool bRemote;

    deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
    if (deviceName == "") { // In case of disconnection
        deviceName = g_sDpName;
    }
    g_bUnSystemAlarmPlc = false;
    // get the systemName
    sSystemName = unGenericDpFunctions_getSystemName(deviceName);
    // get the PLC name
    sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);

    unDistributedControl_isRemote(bRemote, sSystemName);
    if (bRemote) {
        g_bSystemConnected = bSystemConnected;
    } else {
        g_bSystemConnected = true;
    }

    if (sFrontEnd != "") {
        sFrontEnd = sSystemName + c_unSystemAlarm_dpPattern + DS_pattern + sFrontEnd;
        //DebugN(sSystemName, sFrontEnd);
        if (dpExists(sFrontEnd)) {
            g_bUnSystemAlarmPlc = true;
        }
    }

    unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un widget CPC_RcpBuffers " + deviceName, "cpcRcpBuffers_WidgetRegisterCB", sDp, deviceName, g_bCallbackConnected, iAction);
    switch (iAction) {
        case UN_ACTION_DISCONNECT:
            unUnRcpBuffers_WidgetDisconnection(g_sWidgetType);
            break;
        case UN_ACTION_DPCONNECT:
            g_sDpName = deviceName;
            unUnRcpBuffers_WidgetConnect(deviceName, sFrontEnd);
            break;
        case UN_ACTION_DPDISCONNECT_DISCONNECT:
            unUnRcpBuffers_WidgetDisconnect(deviceName, sFrontEnd);
            unUnRcpBuffers_WidgetDisconnection(g_sWidgetType);
            break;
        case UN_ACTION_NOTHING:
        default:
            break;
    }
}
