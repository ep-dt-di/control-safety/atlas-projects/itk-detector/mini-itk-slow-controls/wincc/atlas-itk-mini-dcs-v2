/**Add CMW DPE address to the device's output

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDp current dp name
@param deviceType device type
@param dpe dpe name (without path)
@param isInput should be true if it's in ProcessInput and false if it's in ProcessOutput
@param dsDpParameters object's output

@deprecated 2018-07-27

*/
void cpcExportGenericFunctions_processCMWAddress(string sDp, string deviceType, string dpe, bool isInput, dyn_string &dsDpParameters)
{
  FWDEPRECATED();

    string address;
    dyn_string addressConfig;
    string prefix = isInput ? ".ProcessInput." : ".ProcessOutput.";
    unConfigCMW_getAddressConfig(sDp + prefix + dpe, addressConfig);
    if (addressConfig[4] == "") { // no address config
        address = "";
    } else {
        //DebugN("addressConfig", addressConfig);
        address = addressConfig[1] + CPC_CMW_ADDRESS_CONFIG_SEPARATOR +
                  addressConfig[2] + CPC_CMW_ADDRESS_CONFIG_SEPARATOR +
                  addressConfig[3] + CPC_CMW_ADDRESS_CONFIG_SEPARATOR +
                  addressConfig[4] + "$" + addressConfig[5] + "$" + addressConfig[6] + CPC_CMW_ADDRESS_CONFIG_SEPARATOR +
                  addressConfig[7] + CPC_CMW_ADDRESS_CONFIG_SEPARATOR +
                  addressConfig[8] + CPC_CMW_ADDRESS_CONFIG_SEPARATOR +
                  addressConfig[9];
    }
    dynAppend(dsDpParameters, address);
}