/** LHCLoggingDB custom function
Analog Inputs / Analog Parameters:
    Header:  (LHC or Ti)/Collimators/Temperatures  (LHC where Domain[2]=IPx, Tx where Domain[2]=TI2,TI8)
                                         Beam = (Domain[1])
                                            IP = (Domain[2])
                                               family = (Domain[3])

 DO
    Header:  (LHC or Ti)/Collimators/Temperatures  (LHC where Domain[1]=IPx, Tx where Domain[1]=TI2,TI8)
                                         BIC

@reviewed 2018-06-22 @whitelisted{LiteralString}

@author E. Blanco AB/CO IS   [05/08/2008]
@requirements
    Use the existing hiearchy in DB Logging
**/
void cpcExtLHCLoggingDBUserLib_COLLIMATOR(string sDpe, string &sN,  string &sD, string &sH, string &sA, string &sE) {
    dyn_string dsParameters;

    string sActivityCOLLIMATOR = "LHC";      // It can be LHC, TI2, TI8
    string sApplicationCOLLIMATOR = "Collimators";
    string sBICInputs = "BIC";
    string sCatTemps = "Temperatures";

    dyn_string dsNatureCOLLIMATOR;
    dyn_string dsDomaineCOLLIMATOR;

    sH = "ERROR";
    sE = sDpe;

    //Domain / nature
    string deviceName = unGenericDpFunctions_getDpName(sDpe);
    unGenericDpFunctions_getParameters(deviceName, dsParameters);

    //Nature
    if (strpos(dsParameters[UN_PARAMETER_NATURE], ",") >= 0) {      //cas multinature   Analog Input
        dsNatureCOLLIMATOR = strsplit(dsParameters[UN_PARAMETER_NATURE], ",");
    } else {
        dsNatureCOLLIMATOR[1] = dsParameters[UN_PARAMETER_NATURE];
    }
    //Domain
    if (strpos(dsParameters[UN_PARAMETER_DOMAIN], ",") >= 0) { //cas multidomaine
        dsDomaineCOLLIMATOR = strsplit(dsParameters[UN_PARAMETER_DOMAIN], ",");
    } else {
        dsDomaineCOLLIMATOR[1] = dsParameters[UN_PARAMETER_DOMAIN];
    }

    // survey_1:un-CFC_SR8_GT8EF-SURVEY-AnalogInput-00019.ProcessInput.PosSt
    dyn_string dsSplit = strsplit(sDpe, ".");
    if (dynlen(dsSplit) == 3) {    //UNICOS Device

        if (dsSplit[2] == "ProcessInput" && sApplicationCOLLIMATOR != "") {
            string sAlias = dpGetAlias(dsSplit[1] + ".");
            sA = sAlias;
            sE = substr(sDpe, strpos(sDpe, ".") + 1, strlen(sDpe));

            sN = sA + "." + dsSplit[3];

            string sDevType = dpTypeName(sDpe);

            if ((sDevType == "CPC_AnalogInput")  || (sDevType == "CPC_AnalogAlarm") ) {
                // Adapt the field "sActivityCOLLIMATOR"
                if (strpos(dsDomaineCOLLIMATOR[2], "IP") >= 0) {
                    sActivityCOLLIMATOR = "LHC";
                    sH = sActivityCOLLIMATOR + "/" + sApplicationCOLLIMATOR + "/" + sCatTemps + "/" + dsDomaineCOLLIMATOR[1] + "/" + dsDomaineCOLLIMATOR[2] + "/" + dsDomaineCOLLIMATOR[3];     //COLLIMATOR hierarchy
                } else if (strpos(dsDomaineCOLLIMATOR[2], "TI") >= 0) {
                    sActivityCOLLIMATOR = dsDomaineCOLLIMATOR[2];
                    sH = sActivityCOLLIMATOR + "/" + sApplicationCOLLIMATOR + "/" + sCatTemps + "/" + dsDomaineCOLLIMATOR[3];       //COLLIMATOR hierarchy
                } else if (strpos(dsDomaineCOLLIMATOR[4], "IP") >= 0) {
                    sActivityCOLLIMATOR = "LHC";
                    sH = sActivityCOLLIMATOR + "/" + sApplicationCOLLIMATOR + "/" + sCatTemps + "/" + dsDomaineCOLLIMATOR[1] + "/" + dsDomaineCOLLIMATOR[4];
                }
            } else if ((sDevType == "CPC_DigitalOutput") || (sDevType == "CPC_DigitalAlarm") || (sDevType == "CPC_OnOff") ||
                       (sDevType == "CPC_AnalogParameter") || (sDevType == "CPC_DigitalParameter") || (sDevType == "CPC_WordStatus") ||
                       sDevType == "CPC_ProcessControlObject") {
                // Adapt the field "sActivityCOLLIMATOR"
                if (strpos(dsDomaineCOLLIMATOR[1], "IP") >= 0) {
                    sActivityCOLLIMATOR = "LHC";
                    sH = sActivityCOLLIMATOR + "/" + sApplicationCOLLIMATOR + "/" + sCatTemps + "/" + sBICInputs + "/" + dsDomaineCOLLIMATOR[1] + "/" + sDevType;       //COLLIMATOR
                    DebugN("dsDomaineCOLLIMATOR[1]=" + dsDomaineCOLLIMATOR[1]);
                    DebugN("sH=", sH);
                } else if (strpos(dsDomaineCOLLIMATOR[1], "TI") >= 0) {
                    sActivityCOLLIMATOR = dsDomaineCOLLIMATOR[1];
                    sH = sActivityCOLLIMATOR + "/" + sApplicationCOLLIMATOR + "/" + sCatTemps + "/" + sBICInputs + "/" + sDevType;      //COLLIMATOR
                }
            }

            sD = unGenericDpFunctions_getDescription(deviceName);
        } else {
            DebugN("***************** ERROR ****************************");
        }
    } else if (iLen == 2) {     //System Alarm ?
        cpcExtLHCLoggingDBUserLib_COLLIMATOR_SystemAlarmInfo(dsSplit, "Integrity", sE, sD, sA, sN, sH);
    }
}

void cpcExtLHCLoggingDBUserLib_COLLIMATOR_SystemAlarmInfo(const dyn_string dsDpe, const string sLoc, string &sDpe, string &sDescription, string &sAlias, string &sName, string &sHierarchy) {
    string sAlarm;
    string sActivitySURVEY = "LHC" + "/" + "Collimators" + "/" + "Temperatures";

    int iPos = strpos(dsDpe[1], c_unSystemAlarm_dpPattern);

    if (dsDpe[2] == "alarm" && iPos >= 0 && sLoc != "") {
        sAlarm = substr(dsDpe[1], iPos + strlen(c_unSystemAlarm_dpPattern));

        int iPosErr = strpos(sAlarm, PLC_DS_Error_pattern);
        int iPosCom = strpos(sAlarm, DS_pattern);
        int iPosTime = strpos(sAlarm, DS_Time_pattern);

        //for the moment only FE alarms
        if (iPosErr >= 0 || iPosCom >= 0 || iPosTime >= 0) {
            if (iPosErr >= 0) {
                sAlarm = substr(sAlarm, iPosErr + strlen(PLC_DS_Error_pattern)) + ":" + "ERROR";
            } else if (iPosCom >= 0) {
                sAlarm = substr(sAlarm, iPosCom + strlen(DS_pattern)) + ":" + "COMM";
            } else if (iPosTime >= 0) {
                sAlarm = substr(sAlarm, iPosTime + strlen(DS_Time_pattern)) + ":" + "TIME";
            } else {
                sAlarm = "";
            }

            //Type of FE?
            if (sAlarm != "") {
                int iPosunPLC = strpos(sAlarm, c_unSystemIntegrity_UnPlc);
                int iPosS7 = strpos(sAlarm, c_unSystemIntegrity_S7);
                int iPosGTW = strpos(sAlarm, DS_Time_pattern);

                if (iPosunPLC >= 0) {
                    sAlarm = substr(sAlarm, iPosunPLC + strlen(c_unSystemIntegrity_UnPlc));
                } else if (iPosS7 >= 0) {
                    sAlarm = substr(sAlarm, iPosS7 + strlen(c_unSystemIntegrity_S7));
                } else if (iPosGTW >= 0) {
                    sAlarm = substr(sAlarm, iPosGTW + strlen(c_unSystemIntegrity_S7));    //!!!!!!!!! cst has to be replaced
                } else {
                    sAlarm = "";
                }
            }
        } else {
            sAlarm = "";
        }

        if (sAlarm != "") {
            sDpe = dsDpe[1] + "." + dsDpe[2];
            sDescription = unGenericDpFunctions_getDescription(sDpe);
            sName = sAlarm;     //FE NAME:ERROR TYPE
            sAlias = "";
            sHierarchy = sActivitySURVEY + "/" + sLoc ;
        }
    }
}

/** LHCLoggingDB custom function

@reviewed 2018-06-22 @whitelisted{LiteralString}

**/
void cpcExtLHCLoggingDBUserLib_ECR(string sDpe, string &sN,  string &sD, string &sH, string &sA, string &sE) {
    string sLocation, sApplication, sDomaine, sNature, sAppli;

    sH = "ERROR";
    sE = sDpe;

    //Location: ATLAS or CMS
    string sSystemName = getSystemName();
    dyn_string dsSystemName = strsplit(sSystemName, "_");
    if (dynlen(dsSystemName) > 1) {
        if (dsSystemName[1] == "P1") {
            sLocation = "ATLAS";
        } else if (dsSystemName[1] == "P5") {
            sLocation = "CMS";
        }
    }

    string deviceName = unGenericDpFunctions_getDpName(sDpe);
    dyn_string dsParameters;
    unGenericDpFunctions_getParameters(deviceName, dsParameters);

    //Domaine
    if (strpos(dsParameters[UN_PARAMETER_DOMAIN], ",") >= 0) { //multidomains
        dyn_string dsT1 = strsplit(dsParameters[UN_PARAMETER_DOMAIN], ",");
        int i = 1;
        sDomaine = "";
        while (sDomaine == "") {
            sDomaine = dsT1[i];
            i++;
        }
    } else {
        sDomaine = dsParameters[UN_PARAMETER_DOMAIN];
    }

    //Application
    sAppli = unGenericDpFunctions_getApplication(deviceName);
    if (sAppli != "") {
        sApplication = cpcExtLHCLoggingDBUserLib_ECR_Applications(sAppli);
    }

    //Nature
    if (strpos(dsParameters[UN_PARAMETER_NATURE], ",") >= 0) { //multinatures
        dyn_string dsT1 = strsplit(dsParameters[UN_PARAMETER_NATURE], ",");
        int i = 1;
        sNature = "";
        while (sNature == "") {
            sNature = dsT1[i];
            i++;
        }
    } else {
        sNature = dsParameters[UN_PARAMETER_NATURE];
    }

    dyn_string dsDPE = strsplit(sDpe, ".");
    if (dynlen(dsDPE) == 3) {                //Devices
        if (dsDPE[2] == "ProcessInput" && sLocation != "" && sApplication != "" && sNature != "") {
            sA = dpGetAlias(dsDPE[1] + ".");
            sN = sA + "." + dsDPE[3];
            sE = substr(sDpe, strpos(sDpe, ".") + 1, strlen(sDpe));
            sH = c_Activity + "/" + sLocation + "/" + sApplication + "/" + sNature + "/" + sDomaine;
            sD = unGenericDpFunctions_getDescription(deviceName);
        } else {
            DebugN("Error: Domain= " + sDomaine + " Location= " + sLocation + " sAppli=" + sAppli + " Application= " + sApplication + " Nature= "  + sNature);
        }
    } else if (dynlen(dsDPE) == 2) {     // treat as a System Alarm
        unLHCLoggingDBUserLib_getSystemAlarmInfo(dsDPE, "ATLAS", sE, sD, sA, sN, sH);
    }
}

//-----------------------------------------------------------------------------------------------------------

string cpcExtLHCLoggingDBUserLib_ECR_Applications(const string sName) {
    dyn_dyn_string ddsNames;
    dyn_string dsNames, dsAppli;
    int iPos, iLen, i;
    string sAppli;

    dsNames = makeDynString("MRCP", "MRCB", "SRCP", "SRCB", "PCSBT", "PCSSOL", "ANRSCP", "ANRSCB", "LARG", "QS_C", "QU_C", "PCSCL");
    iLen = dynlen(dsNames);

    //1
    iPos = dynContains(dsNames, "MRCP");
    if (iPos > 0) {
        dsAppli = makeDynString("QSC1H_A", "QSC1H", "QS_A");
        ddsNames[iPos] = dsAppli;
    }

    //2
    iPos = dynContains(dsNames, "MRCB");
    if (iPos > 0) {
        dsAppli = makeDynString("QUR1H_A", "QUR1H");
        ddsNames[iPos] = dsAppli;
    }

    //3
    iPos = dynContains(dsNames, "SRCP");
    if (iPos > 0) {
        dsAppli = makeDynString("QSC2H_A", "QSC2H");
        ddsNames[iPos] = dsAppli;
    }

    //4
    iPos = dynContains(dsNames, "SRCB");
    if (iPos > 0) {
        dsAppli = makeDynString("QUR2H_A", "QUI2H_A", "QUR2H");
        ddsNames[iPos] = dsAppli;
    }

    //5
    iPos = dynContains(dsNames, "PCSBT");
    if (iPos > 0) {
        dsAppli = makeDynString("QXPTH_A", "QXPTH", "BTPCS");
        ddsNames[iPos] = dsAppli;
    }

    //6
    iPos = dynContains(dsNames, "PCSSOL");
    if (iPos > 0) {
        dsAppli = makeDynString("QXZSH_A", "QXZSH", "QUZSH_A", "QUZSH", "SOLPCS");
        ddsNames[iPos] = dsAppli;
    }

    //7
    iPos = dynContains(dsNames, "ANRSCP");
    if (iPos > 0) {
        dsAppli = makeDynString("QSC1N_A", "QSCBN");
        ddsNames[iPos] = dsAppli;
    }

    //8
    iPos = dynContains(dsNames, "ANRSCB");
    if (iPos > 0) {
        dsAppli = makeDynString("QUR1N_A", "QUU1N_A", "QPRN");
        ddsNames[iPos] = dsAppli;
    }

    //9
    iPos = dynContains(dsNames, "LARG");
    if (iPos > 0) {
        dsAppli = makeDynString("QXD4N", "QXKAA", "QXKBA", "QXKCA");
        ddsNames[iPos] = dsAppli;
    }

    //10
    iPos = dynContains(dsNames, "QS_C");
    if (iPos > 0) {
        dsAppli = makeDynString("QSC1H_C");
        ddsNames[iPos] = dsAppli;
    }

    //11
    iPos = dynContains(dsNames, "QU_C");
    if (iPos > 0) {
        dsAppli = makeDynString("QUR1H_C", "QUK1H_C", "QUU1H_C", "QUM1H_C");
        ddsNames[iPos] = dsAppli;
    }

    //12
    iPos = dynContains(dsNames, "PCSCL");
    if (iPos > 0) {
        dsAppli = makeDynString("QUIX1A", "QUX1H", "CLPCS");
        ddsNames[iPos] = dsAppli;
    }

    for (i = 1; i <= iLen; i++) {
        if (dynContains(ddsNames[i], sName) > 0) {
            sAppli = dsNames[i];
            i = iLen;
        }
    }

    return sAppli;

}

/** LHCLoggingDB custom function

@reviewed 2018-06-22 @whitelisted{LiteralString}

**/
void cpcExtLHCLoggingDBUserLib_NA48(string sDpe, string &sN,  string &sD, string &sH, string &sA, string &sE) {
    dyn_string dsParameters, dsSplit;
    string sAlias, deviceName, sSystemName, sLocation, sApplication, sNature;
    int iLen;

    sSystemName = getSystemName();
    sH = "ERROR";
    sE = sDpe;

    //Domain - nature
    deviceName = unGenericDpFunctions_getDpName(sDpe);
    unGenericDpFunctions_getParameters(deviceName, dsParameters);

    //Application
    dynClear(dsSplit);
    dsSplit = strsplit(sDpe, "-");
    if (dynlen(dsSplit) > 3) {
        sApplication = dsSplit[3];
        //Special part to define the HIERARCHY for NA48... harcoded
        if (sApplication == "NA48_QX_N") {
            sLocation = "NA62";
            sApplication = "NOT USED";
        }
    } else {
        sApplication = "UNKNOWN";
    }

    //Nature
    if (strpos(dsParameters[UN_PARAMETER_NATURE], ",") >= 0) { //cas multinature
        dyn_string dsT1 = strsplit(dsParameters[UN_PARAMETER_NATURE], ",");
        int i = 1;
        sNature = "";
        while (sNature == "") {
            sNature = dsT1[i];
            i++;
        }
    } else { sNature = dsParameters[UN_PARAMETER_NATURE]; }

    dynClear(dsSplit);
    dsSplit = strsplit(sDpe, ".");
    iLen = dynlen(dsSplit);
    if (iLen == 3) {    //UNICOS Device
        if (dsSplit[2] == "ProcessInput") {     // && sLocation!="" && sApplication!="" && sNature!="")
            sAlias = dpGetAlias(dsSplit[1] + ".");
            sA = sAlias;
            sE = substr(sDpe, strpos(sDpe, ".") + 1, strlen(sDpe));

            sN = sAlias + "." + dsSplit[3];
            sD = unGenericDpFunctions_getDescription(deviceName);

            bool bOK = TRUE;

            if (sLocation == "") {
                bOK = FALSE;
                DebugN("ERROR: Location is empty for " + deviceName);
            } else {
                if (sApplication == "") {
                    bOK = FALSE;
                    DebugN("ERROR: Application is empty for " + deviceName);
                } else {
                    if (sNature == "") {
                        bOK = FALSE;
                        DebugN("ERROR: Nature is empty for " + deviceName);
                    }
                }
            }

            if (bOK) {
                sH = c_Activity + "/" + sLocation + "/" + sNature;    //hierarchy
            }

        } else {
            DebugN("Error: \"ProcessInput\" pattern not found!");
        }
    } else if (iLen == 2) {     //System Alarm ?
        unLHCLoggingDBUserLib_getSystemAlarmInfo(dsSplit, sLocation, sE, sD, sA, sN, sH);
    }
}

