#uses "cpcDeprecated.ctl"
#uses "cpcGenericObjectDeprecated.ctl"

/**@file

@brief This library contains generic functions for CPC objects.

@author Nikolay Kulman (IT-CO-FE)
@author Francisco Mico (IT-CO-FE)
@author Alexey Merezhin (EN-ICE-PLC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Creation Date
  08.07.2003

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI, CTRL
*/

/**Split a string like "key1=value1,key2=value2" into the mapping

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@todo merge with splitCommaSeparatedString

@param data Input string with labels information
@param pairs Result mappint
@return If function finishedf successfully
*/
void cpcGenericObject_StringToMapping(string data, mapping& pairs) {
    dyn_string pairList, pair;

    mappingClear(pairs);

    pairList = strsplit(data, ","); // list of "key=value" strings
    for (int i = 1; i <= dynlen(pairList); i++) {
        if (strpos(pairList[i], "=") < 0) { continue; } //	we have incorrect element
        pair = strsplit(pairList[i], "=");
        if (dynlen(pair) != 2) { continue; } // wrong number of elements - not a "key=value"
        pairs[strltrim(pair[1])] = pair[2];
    }
}

/**Send a binary order to the PLC. Example : auto mode request etc.

Port of unGenericObject_SendBinaryOrder. Extended to support ManReg02

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDpName device name
@param regName ManReg name (e.g. "ManReg01")
@param iManRegBit bit position in ManReg
@param sKey catalogue key
@param exceptionInfo The list of exceptions.
*/
void cpcGenericObject_SendBinaryOrder(string sDpName, string regName, int iManRegBit, string sKey, dyn_string &exceptionInfo) {
    dyn_string exceptionInfoTemp;
    string alias, deviceName;
    bool alreadySent, bRes;

    deviceName = unGenericDpFunctions_getDpName(sDpName);
    // 1. Select device again
    unGenericObject_NeedSelect(dpTypeName(deviceName), bRes, exceptionInfo);
    if (bRes) {
        unSelectDeselectHMI_select(deviceName, true, exceptionInfo);
        dynClear(exceptionInfo);
    }
    // 2. Send pulse
    cpcGenericObject_SendManRegPulse(deviceName, regName, iManRegBit, exceptionInfo, alreadySent);
    // 3. Display result
    if (!alreadySent) {
        cpcGenericObject_notifyUsers(deviceName, sKey, exceptionInfo);
    }
}

/**Send a pulse on a specified bit of ManReg

Port of unGenericObject_SendManRegPulse. Extended to support ManReg02

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDpName device name
@param regName ManReg name (e.g. "ManReg01")
@param bitPosition bit position in ManReg
@param exceptionInfo The list of exceptions.
@param alreadySent is bit already sent
*/
void cpcGenericObject_SendManRegPulse(string sDpName, string regName, unsigned bitPosition, dyn_string &exceptionInfo, bool &alreadySent) {
    unGenericObject_SendPulse(sDpName, ".ProcessOutput." + regName,  bitPosition, exceptionInfo, alreadySent);
}


/**Return front-end validness.

c_unSystemIntegrity_no_alarm_value (0) no alarm PLC is running,
c_unSystemIntegrity_alarm_value_level1 (10): alarm PLC is not correctly running

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param bSystemIntegrityAlarmEnabled '$FRONT_END_DP$.alarm' value
@param iSystemIntegrityAlarmValue '$FRONT_END_DP$.enabled' value
@return true if system invalid, otherwise false
*/
bool cpcGenericObject_SystemInvalid(bool bSystemIntegrityAlarmEnabled, int iSystemIntegrityAlarmValue) {
    return !bSystemIntegrityAlarmEnabled || iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value;
}

/**Standart animation for the widget' letter

If system is invalid, then letter is "O" with "unDataNotValid" color.
If data is invalid, then letter is "N" with "unDataNotValid" color.
Function does not assign any other value/color.

@todo instead of dataInvalid iterate through ':_online.._invalid' dpes to construct this value
@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

*/
void cpcGenericObject_WidgetValidnessAnimation(bool bSystemIntegrityAlarmEnabled, int iSystemIntegrityAlarmValue, bool dataInvalid, string &letter, string &color) {
    if (cpcGenericObject_SystemInvalid(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue)) {
        letter = CPC_WIDGET_TEXT_OLD_DATA;
        color = "unDataNotValid";
    }
    if (dataInvalid) {
        letter = CPC_WIDGET_TEXT_INVALID;
        color = "unDataNotValid";
    }
}

/**Function sets color and value associated with last enabled bit in StsReg

@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param bit32StsReg01 StsReg01 value
@param bStsReg01Invalid is StsReg02 invalid
@param bit32StsReg02 StsReg02 value
@param bStsReg02Invalid is StsReg02 invalid
@param bits list of bits (positions)
@param location where's bit - 1 for StsReg01, 2 for StsReg02
@param letters list of bit's letters (associated by position in the array)
@param colors list of bit's colors (associated by position in the array)
@param letter result letter
@param color result color
*/
void cpcGenericObject_WidgetStsRegBitAnimation(bit32 bit32StsReg01, bool bStsReg01Invalid, bit32 bit32StsReg02, bool bStsReg02Invalid,
        dyn_int bits, dyn_int location, dyn_string letters, dyn_string colors, string &letter, string &color) {
    if (bStsReg01Invalid || bStsReg02Invalid) {
        return;
    }
    for (int i = 1; i <= dynlen(bits); i++) {
        int bit;
        if (location[i] == 1) {
            bit = getBit(bit32StsReg01, bits[i]);
        } else {
            bit = getBit(bit32StsReg02, bits[i]);
        }
        if (bit  == 1) {
            letter = letters[i];
            color = colors[i];
        }
    }
}

/**Animate the alarm part of the widget

diBits and dsLetters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority.
diBits2 and dsLetters2 must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority.

@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param bit32StsReg01 StsReg01 value
@param bit32StsReg02 StsReg02 value
@param diBits bit positions
@param dsLetters associated letters to be displayed
@param diBits2 bit positions
@param dsLetters2 associated letters to be displayed
@param letter the letter to be displayed
@param color the color to be used
*/
void cpcGenericObject_WidgetAlarmTextAnimationDoubleStsReg(bit32 bit32StsReg01, bit32 bit32StsReg02, dyn_int diBits, dyn_string dsLetters, dyn_int diBits2, dyn_string dsLetters2, string &letter, string &color) {
    letter = "";
    color = "unFaceplate_AlarmActive";
    if (dynlen(diBits) == dynlen(dsLetters)) {
        for (int i = 1; i <= dynlen(diBits); i++) {
            if (getBit(bit32StsReg01, diBits[i]) == 1) {
                letter = dsLetters[i];
            }
        }
    }
    if (dynlen(diBits2) == dynlen(dsLetters2)) {
        for (int i = 1; i <= dynlen(diBits2); i++) {
            if (getBit(bit32StsReg02, diBits2[i]) == 1) {
                letter = dsLetters2[i];
            }
        }
    }
}

/**Animate widget

di$...$Bits and ds$...$Letters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority .

@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  UI

@param sWidgetType widget type = dpType + widgetType. ex: AnaDigHeater
@param sDpType object
@param bLocked for select animation
@param sSelectedManager for select animation
@param bit32StsReg01 status register
@param bit32StsReg02 status register (empty in case of not using the stsReg02)
@param diWarningBits warning bit positions in status register.
@param diWarningBits2 warning bit positions in status register. (empty in case of not using the stsReg02)
@param dsWarningLetters associated warning letters to be displayed.
@param dsWarningLetters2 associated warning letters to be displayed. (empty in case of not using the stsReg02)
@param diAlarmBits alarm bit positions in status register (StsReg01).
@param dsAlarmLetters associated alarm letters to be displayed (StsReg01).
@param diAlarmBits2 alarm bit positions in status register (StsReg02).
@param dsAlarmLetters2 associated alarm letters to be displayed (StsReg02).
@param iStartIStState start interlock dpe value
@param iTStopIStState temporal stop interlock dpe value
@param iFuStopIStState full stop interlock dpe value
@param iAlSt alarm dpe value
@param diControlBitNames list of StsReg constants for controls
@param onBitConstant on bit constant name
@param offBitConstant off bit constant name
@param iAlUnackBitPosition alarm unack status bit position in status register
@param bInvalidLetter true if invalid letter as to be shown
@param bInvalidBody invalid animation on body ?
@param iSystemIntegrityAlarmValue the value of the systemIntegrity alarm for the PLC:
@param bSystemIntegrityAlarmEnabled the systemIntegrity alarm is enabled
*/
void cpcGenericObject_WidgetAnimationDoubleStsReg(string sWidgetType, string sDpType,
        bool bLocked, string sSelectedManager, bit32 bit32StsReg01, bit32 bit32StsReg02,
        dyn_int diWarningBits, dyn_int diWarningBits2,
        dyn_string dsWarningLetters, dyn_string dsWarningLetters2,
        dyn_int diAlarmBits, dyn_string dsAlarmLetters,
        dyn_int diAlarmBits2, dyn_string dsAlarmLetters2,
        int iStartIStState, int iTStopIStState, int iFuStopIStState, int iAlSt,
        dyn_string diControlBitNames,
        string onBitConstant, string offBitConstant, int iAlUnackBitPosition,
        bool bInvalidLetter, bool bInvalidBody,
        int iSystemIntegrityAlarmValue,
        bool bSystemIntegrityAlarmEnabled) {
    string sColorSelect, sWarningLetter, sWarningColor, sAlarmLetter, sAlarmColor, sControlLetter, sControlColor, sBodyColor, sFunction;
    bool bLockVisible, isRestartNeeded = false;
    dyn_string dsFunctions, exceptionInfo;

    if (onBitConstant != "" && offBitConstant != "") {
        isRestartNeeded = g_bMRestart && getBit(bit32StsReg02, CPC_StsReg02_NEEDRESTART) == 0;
    }
    // 1. Select
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sColorSelect, bLockVisible);
    // 2. Warning (if the stsreg02 exists then the treatment is different)
    cpcGenericObject_WidgetWarningAnimationDoubleStsReg(bit32StsReg01, bit32StsReg02, diWarningBits, diWarningBits2, dsWarningLetters, dsWarningLetters2, sWarningLetter, sWarningColor);

    // 2.1 if the device is not correclty running (the iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)
    // or the checking is not enabled then set the letter to O and the color to not valid data
    if ((!bSystemIntegrityAlarmEnabled) || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)) {
        sWarningLetter = CPC_WIDGET_TEXT_OLD_DATA;
        sWarningColor = "unDataNotValid";
    }

    if (bInvalidLetter) {
        sWarningLetter = CPC_WIDGET_TEXT_INVALID;
        sWarningColor = "unDataNotValid";
    }
    // 3. Alarm
    if (!bInvalidBody) {
        cpcGenericObject_WidgetAlarmTextAnimationDoubleStsReg(bit32StsReg01, bit32StsReg02, diAlarmBits, dsAlarmLetters, diAlarmBits2, dsAlarmLetters2, sAlarmLetter, sAlarmColor);
        if (onBitConstant != "" && offBitConstant != "") {
            if (sAlarmLetter == "" && isRestartNeeded) {
                sAlarmLetter = CPC_WIDGET_TEXT_MANUAL_RESTART;
                sAlarmColor = "cpcColor_Widget_Warning";
            }
        }
    }

    // 4. Control state
    if (!bInvalidBody) {
        cpcGenericObject_WidgetControlStateAnimation(bit32StsReg01, bit32StsReg02, diControlBitNames, sControlLetter, sControlColor);
    }
    // 5. Animate all except body
    if (g_bSystemConnected)
        setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "AlarmText", "text", sAlarmLetter, "AlarmText", "foreCol", sAlarmColor,
                      "ControlStateText", "text", sControlLetter, "ControlStateText", "foreCol", sControlColor,
                      "SelectArea", "foreCol", sColorSelect, "LockBmp", "visible", bLockVisible,
                      "WidgetArea", "visible", true);
    // 6. Animate body
    sBodyColor = "cpcColor_Widget_Status";					// Auto mode (default)
    if (bInvalidBody) {
        sBodyColor = "unDataNotValid";							// Invalid data
    } else {
        if (sControlLetter == CPC_WIDGET_TEXT_CONTROL_FORCED) {
            sBodyColor = "unWidget_ControlStateForced";			// Forced mode
        }
        if (isRestartNeeded) {
            sBodyColor = "cpcColor_Widget_Warning";
        }
        if (sAlarmLetter == CPC_WIDGET_TEXT_ALARM_TSTOPIST || sAlarmLetter == CPC_WIDGET_TEXT_ALARM_FUSTOPIST) {
            sBodyColor = "unFaceplate_AlarmActive";
        }
        if (iAlUnackBitPosition >= 0 && getBit(bit32StsReg01, iAlUnackBitPosition) == 1 && (iTStopIStState == 1 || iTStopIStState == 3 || iFuStopIStState == 1 || iFuStopIStState == 3)) {
            sBodyColor = "unWidget_AlarmNotAck";
        }
    }

    sFunction = cpcGenericObject_GetAnimationFunction(sWidgetType);

    int iOnBit = cpcGenericObject_StsRegConstantNameToValue(onBitConstant, bit32StsReg01, bit32StsReg02);
    int iOffBit = cpcGenericObject_StsRegConstantNameToValue(offBitConstant, bit32StsReg01, bit32StsReg02);
    if (isFunctionDefined(sFunction) && sFunction != "") {
        if (g_bSystemConnected) {
            execScript("main(int iOn, int iOff, string sBodyColor) {" +
                       sFunction + "(iOn, iOff, sBodyColor);}",
                       makeDynString(), iOnBit, iOffBit, sBodyColor);
        }
    }
}

/**Animate widget

di$...$Bits and ds$...$Letters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority .

@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  UI

@param sWidgetType widget type = dpType + widgetType. ex: AnaDigHeater
@param sDpType object
@param bLocked for select animation
@param sSelectedManager for select animation
@param bit32StsReg01 status register
@param diWarningBits warning bit positions in status register.
@param dsWarningLetters associated warning letters to be displayed.
@param diAlarmBits alarm bit positions in status register.
@param dsAlarmLetters associated alarm letters to be displayed.
@param diControlBitNames list of StsReg constants for controls
@param onBitConstant on bit constant name
@param offBitConstant off bit constant name
@param iAlUnackBitPosition alarm unack status bit position in status register
@param bInvalidLetter true if invalid letter as to be shown
@param bInvalidBody invalid animation on body ?
@param iSystemIntegrityAlarmValue the value of the systemIntegrity alarm for the PLC:
@param bSystemIntegrityAlarmEnabled the systemIntegrity alarm is enabled
*/
void cpcGenericObject_WidgetAnimation(string sWidgetType, string sDpType,
                                      bool bLocked, string sSelectedManager, bit32 bit32StsReg01,
                                      dyn_int diWarningBits, dyn_string dsWarningLetters,
                                      dyn_int diAlarmBits, dyn_string dsAlarmLetters,
                                      dyn_string diControlBitNames,
                                      string onBitConstant, string offBitConstant, int iAlUnackBitPosition,
                                      bool bInvalidLetter, bool bInvalidBody,
                                      int iSystemIntegrityAlarmValue,
                                      bool bSystemIntegrityAlarmEnabled) {
    dyn_int diWarningBits_EMPTY, diAlarmBits_EMPTY;
    dyn_string dsWarningLetters_EMPTY, dsAlarmLetters_EMPTY;
    bit32 bit32StsReg02_EMPTY;

    diWarningBits_EMPTY = makeDynInt();
    diAlarmBits_EMPTY = makeDynInt();
    dsWarningLetters_EMPTY = makeDynString();
    dsAlarmLetters_EMPTY = makeDynString();

    cpcGenericObject_WidgetAnimationDoubleStsReg(sWidgetType, sDpType, bLocked, sSelectedManager,
            bit32StsReg01, bit32StsReg02_EMPTY,
            diWarningBits, diWarningBits_EMPTY, dsWarningLetters, dsWarningLetters_EMPTY,
            diAlarmBits, dsAlarmLetters, diAlarmBits_EMPTY, dsAlarmLetters_EMPTY,
            0, 0, 0, 0,
            diControlBitNames, onBitConstant, offBitConstant, iAlUnackBitPosition,
            bInvalidLetter, bInvalidBody, iSystemIntegrityAlarmValue, bSystemIntegrityAlarmEnabled);
}

/**Returns formatted value concatenated with unit

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  UI

@param value original value
@param format dpe format
@param unit dpe unit
@return formatted value with unit
*/
string cpcGenericObject_FormatValueWithUnit(float value, string format, string unit) {
    return unGenericObject_FormatValue(format, value) + " " + unit;
}

/**Animation of widget display values

@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  UI

@param sShape display values shapes
@param sUnit dpe unit
@param sFormat dpe format
@param fValue dpe value
@param sColor animation color
@param bInvalid is dpe invalid
*/
void cpcGenericObject_WidgetDisplayValueAnimation(string sShape, string sUnit, string sFormat, float fValue, string sColor, bool bInvalid) {
    string displayColor, formattedValue;

    displayColor = sColor;
    formattedValue = unGenericObject_FormatValue(sFormat, fValue);
    if (bInvalid) {
        displayColor = "unDataNotValid";
    }
    if (sUnit != "") {
        formattedValue = formattedValue + " " + sUnit;
    }
    if (g_bSystemConnected) {
        setMultiValue(sShape, "text", formattedValue, sShape, "foreCol", displayColor);
    }
}

/**Handle the result of the pop-up menu for the UNICOS device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName the device dp name
@param sDpType the device dp type
@param menuList list of requested action
@param menuAnswer selected action from the menuList
@param exceptionInfo the error is returned here
*/
void cpcGenericObject_HandleUnicosMenu(string deviceName, string sDpType, dyn_string menuList, int menuAnswer, dyn_string &exceptionInfo) {
    bool bRes;
    string sCatKey;
    dyn_string exceptionInfoTemp1, exceptionInfoTemp2;

    switch (menuAnswer) {
        case CPC_POPUPMENU_BLOCK_PLC_NUMBER:
            bRes = dynContains(menuList, "PUSH_BUTTON, Block PLC Alarm, " + CPC_POPUPMENU_BLOCK_PLC_NUMBER + ", 1");
            sCatKey = (bRes > 0) ? "MASKPLC" : "UNMASKPLC";
            unGenericObject_SendBinaryOrder(deviceName, CPC_ManReg01_MALBSETRST, sCatKey, exceptionInfo);
            break;
        case CPC_POPUPMENU_MANUAL_IO_ERROR_NUMBER:
            cpcGenericObject_SendBinaryOrder(deviceName, "ManReg01", CPC_ManReg01_MIOERBSETRST, "IOERRORBLOCKING", exceptionInfo);
            break;
        case CPC_POPUPMENU_HIERARCHY_NUMBER:
            cpcGenericObject_OpenHierarchy(deviceName);
            break;
        case CPC_POPUPMENU_ALARMS_NUMBER:
            cpcGenericObject_OpenAlarmsPanel(deviceName);
            break;
        case CPC_POPUPMENU_MASK_ALST_NUMBER:
            dpGet(deviceName + ".ProcessInput.HHAlSt:_alert_hdl.._active", bRes);
            unAlarmConfig_mask(deviceName + ".ProcessInput.HHAlSt", bRes, exceptionInfoTemp1);
            unAlarmConfig_mask(deviceName + ".ProcessInput.HWSt", bRes, exceptionInfoTemp1);
            unAlarmConfig_mask(deviceName + ".ProcessInput.LWSt", bRes, exceptionInfoTemp1);
            unAlarmConfig_mask(deviceName + ".ProcessInput.LLAlSt", bRes, exceptionInfoTemp2);
            sCatKey = bRes ? "MASKPVSS" : "UNMASKPVSS";
            cpcGenericObject_notifyUsers(deviceName, sCatKey, exceptionInfoTemp1);
            if (dynlen(exceptionInfoTemp1) > 0) {
                dynAppend(exceptionInfo, exceptionInfoTemp1);
            }
            if (dynlen(exceptionInfoTemp2) > 0) {
                dynAppend(exceptionInfo, exceptionInfoTemp2);
            }
            break;
        case CPC_POPUPMENU_MASK_IST_NUMBER:
            dpGet(deviceName + ".ProcessInput.ISt:_alert_hdl.._active", bRes);
            unAlarmConfig_mask(deviceName + ".ProcessInput.ISt", bRes, exceptionInfoTemp1);
            sCatKey = bRes ? "MASKPVSS" : "UNMASKPVSS";
            cpcGenericObject_notifyUsers(deviceName, sCatKey, exceptionInfoTemp1);
            if (dynlen(exceptionInfoTemp1) > 0) {
                dynAppend(exceptionInfo, exceptionInfoTemp1);
            }
            break;
        case CPC_POPUPMENU_ALLOW_TO_RESTART_NUMBER:
            cpcGenericObject_SendAllowToRestart(deviceName, exceptionInfo);
            break;
        case CPC_POPUPMENU_UNMASKEVENT_NUMBER:				// unmask event
            cpcConfigGenericFunctions_setMaskEvent(sDpType, deviceName, true, exceptionInfo);
            cpcGenericObject_notifyUsers(deviceName, "UNMASKEVENT", exceptionInfo);
            break;
        case CPC_POPUPMENU_MASKEVENT_NUMBER:				// mask event
            cpcConfigGenericFunctions_setMaskEvent(sDpType, deviceName, false, exceptionInfo);
            cpcGenericObject_notifyUsers(deviceName, "MASKEVENT", exceptionInfo);
            break;
        case CPC_POPUPMENU_RECIPES:
            cpcGenericObject_OpenRecipes(deviceName);
            break;
        default:
            unGenericObject_HandleUnicosMenu(deviceName, sDpType, menuList, menuAnswer, exceptionInfo);
            break;
    }
}

void cpcGenericObject_SendAllowToRestart(string deviceName, dyn_string &exceptionInfo)
{
    unGenericObject_Acknowledge(deviceName, exceptionInfo);
    cpcGenericObject_SendBinaryOrder(deviceName, "ManReg01", CPC_ManReg01_MRESTART, "MANUALRESTART", exceptionInfo);
}

/**Add the UNICOS action to the UNICOS device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName the device dp name
@param sDpType the device dp type
@param dsMenuConfig list of requested action
@param dsAccessOk list of authorized action
@param menuList output menu
*/
void cpcGenericObject_addUnicosActionToMenu(string deviceName, string sDpType, dyn_string dsMenuConfig, dyn_string dsAccessOk, dyn_string &menuList) {
    bool isAlertActive;
    bit32 bit32StsReg01, bit32StsReg02;
    bool bStsReg01Bad, bStsReg02Bad;
    string menuText, currentAlertClass;
    int iRes, alertType = DPCONFIG_NONE, iManReg01;

    unGenericObject_addSelectToMenu(deviceName, sDpType, dsMenuConfig, dsAccessOk, menuList);
    if (unGenericDpFunctions_isMaskEvent(deviceName)) {
        int iMask;
        unGenericDpFunctions_getMaskEvent(deviceName, iMask);

        if (iMask == 0) {
            unGenericObject_addItemToPopupMenu(menuList, "Unmask Event", CPC_POPUPMENU_UNMASKEVENT_NUMBER, dynContains(dsAccessOk, UN_FACEPLATE_MASKEVENT) > 0);
        } else {
            unGenericObject_addItemToPopupMenu(menuList, "Mask Event",   CPC_POPUPMENU_MASKEVENT_NUMBER, dynContains(dsAccessOk, UN_FACEPLATE_MASKEVENT) > 0);
        }
    }

    if (dynlen(menuList) > 0) {
        if (menuList[dynlen(menuList)] != "SEPARATOR") {
            dynAppend(menuList, "SEPARATOR");
        }
    }

    // 2. Ack Alarm
    if (dynContains(dsMenuConfig, UN_POPUPMENU_ACK_TEXT) > 0) {
        if (dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_ACK_ALARM) > 0) {
            // reset manreg01: to be able to ack alarm afterwards. Workaround due to a PVSS bug: right click event cancelled if another right click is executed before the end of the previous one.
            if (dpExists(deviceName + ".ProcessOutput.ManReg01")) {
                iRes = dpGet(deviceName + ".ProcessOutput.ManReg01", iManReg01);
                //if ((iRes < 0) || (iManReg01 != 0)) { // causes problems when there is no ManReg01
                if ((iRes == 0) && (iManReg01 != 0)) {
                    dpSet(deviceName + ".ProcessOutput.ManReg01", 0);
                }
            }
        }
        bool isAckEnabled = true;
        if (sDpType == "CPC_AnaDO" || sDpType == "CPC_AnaDig" || sDpType == "CPC_Analog" || sDpType == "CPC_MassFlowController" || sDpType == "CPC_OnOff" || sDpType == "CPC_ProcessControlObject") {
            dyn_string exceptionInfo;
            dpGet(deviceName + ".ProcessInput.StsReg01", bit32StsReg01, deviceName + ".ProcessInput.StsReg01:_online.._invalid", bStsReg01Bad,
                  deviceName + ".ProcessInput.StsReg02", bit32StsReg02, deviceName + ".ProcessInput.StsReg02:_online.._invalid", bStsReg02Bad);
            isAckEnabled = cpcButton_isAckEnabled(deviceName, sDpType, bit32StsReg01, bStsReg01Bad, bit32StsReg02, bStsReg02Bad);
        }

        unGenericObject_addItemToPopupMenu(menuList, "Ack. Alarm", CPC_POPUPMENU_ACK_NUMBER, dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_ACK_ALARM) > 0 && isAckEnabled);
    }

    // Allow Restart
    if (dynContains(dsMenuConfig, CPC_POPUPMENU_ALLOW_TO_RESTART_TEXT) > 0) {
        dyn_string exceptionInfo;
        bool bMRestart = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
        if (bMRestart) {
            bool bRstartFS = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_RSTARTFS, exceptionInfo);
            dpGet(deviceName + ".ProcessInput.StsReg01", bit32StsReg01, deviceName + ".ProcessInput.StsReg01:_online.._invalid", bStsReg01Bad,
                  deviceName + ".ProcessInput.StsReg02", bit32StsReg02, deviceName + ".ProcessInput.StsReg02:_online.._invalid", bStsReg02Bad);
            bool allowToRestartEnabled = cpcButton_IsAllowRestartEnabled(deviceName, sDpType, bit32StsReg01, bStsReg01Bad, bit32StsReg02, bStsReg02Bad, bRstartFS);
            unGenericObject_addItemToPopupMenu(menuList, "Allow Restart", CPC_POPUPMENU_ALLOW_TO_RESTART_NUMBER, dynContains(dsAccessOk, CPC_FACEPLATE_BUTTON_ALLOW_RESTART) > 0 && allowToRestartEnabled);
        }
    }

    // 3. Mask PosSt
    if (dynContains(dsMenuConfig, UN_POPUPMENU_MASK_POSST_TEXT) > 0) {
        dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl.._type", alertType);
        if (alertType != DPCONFIG_NONE) {
            iRes = dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl.._active", isAlertActive);
            if (iRes >= 0) {
                unGenericObject_addItemToPopupMenu(menuList, isAlertActive ? "Mask PVSS Alarm" : "Unmask PVSS Alarm", CPC_POPUPMENU_MASK_POSST_NUMBER, dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_MASK_ALARM) > 0);
            }
        }
    }

    // 4. Mask ISt
    if (dynContains(dsMenuConfig, CPC_POPUPMENU_MASK_IST_TEXT) > 0) {
        dpGet(deviceName + ".ProcessInput.ISt:_alert_hdl.._type", alertType);
        if (alertType != DPCONFIG_NONE) {
            iRes = dpGet(deviceName + ".ProcessInput.ISt:_alert_hdl.._active", isAlertActive);
            if (iRes >= 0) {
                unGenericObject_addItemToPopupMenu(menuList, isAlertActive ? "Mask PVSS Alarm" : "Unmask PVSS Alarm", CPC_POPUPMENU_MASK_IST_NUMBER, dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_MASK_ALARM) > 0);
            }
        }
    }
    if (dynContains(dsMenuConfig, CPC_POPUPMENU_MASK_ALST_TEXT) > 0) {
        dpGet(deviceName + ".ProcessInput.HHAlSt:_alert_hdl.._type", alertType);
        if (alertType != DPCONFIG_NONE) {
            iRes = dpGet(deviceName + ".ProcessInput.HHAlSt:_alert_hdl.._active", isAlertActive);
            if (iRes >= 0) {
                unGenericObject_addItemToPopupMenu(menuList, isAlertActive ? "Mask PVSS Alarm" : "Unmask PVSS Alarm", CPC_POPUPMENU_MASK_ALST_NUMBER, dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_MASK_ALARM) > 0);
            }
        }
    }

    // 5. Block Plc
    if (dynContains(dsMenuConfig, UN_POPUPMENU_BLOCK_PLC_TEXT) > 0) {
        if (dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION) > 0) {
            iRes = dpGet(deviceName + ".ProcessOutput.ManReg01", iManReg01);
            if ((iRes < 0) || (iManReg01 != 0)) {
                dpSet(deviceName + ".ProcessOutput.ManReg01", 0);
            }
        }
        dpGet(deviceName + ".ProcessInput.StsReg01", bit32StsReg01);
        bool allowed = (!getBit(bit32StsReg01, CPC_StsReg01_AUIHMBST)) && (dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION) > 0);
        unGenericObject_addItemToPopupMenu(menuList, getBit(bit32StsReg01, CPC_StsReg01_MALBRST) ? "Deblock PLC Alarm" : "Block PLC Alarm", CPC_POPUPMENU_BLOCK_PLC_NUMBER, allowed);
    }

    // 7. Manual IO Error Block
    if (dynContains(dsMenuConfig, CPC_POPUPMENU_MANUAL_IO_ERROR_BLOCK) > 0) {
        iRes = dpGet(deviceName + ".ProcessInput.StsReg01", bit32StsReg01);
        unGenericObject_addItemToPopupMenu(menuList, getBit(bit32StsReg01, CPC_StsReg01_MIOERBRST) ? "Manual IO Error Unblock" : "Manual IO Error Block", CPC_POPUPMENU_MANUAL_IO_ERROR_NUMBER, dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_FORCED_MODE) > 0);
    }
}

/**Add the UNICOS default action to the UNICOS device: faceplate, diagnostic and Info

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName the device dp name
@param sDpType the device dp type
@param dsMenuConfig list of requested action
@param dsAccessOk list of authorized action
@param menuList output menu
*/
void cpcGenericObject_addDefaultUnicosActionToMenu(string deviceName, string sDpType, dyn_string dsMenuConfig, dyn_string dsAccessOk, dyn_string &menuList) {
    dyn_string exceptionInfo, dsHtmlLinks;
    string sLink, sEna, sSystemName = unGenericDpFunctions_getSystemName(deviceName), sDeviceLink;
    int i, len, iCste, pos;
    dyn_string dsSplit, dsParameters;
    bool bConnected;
    string sDeviceDpLink, sSystemLink, strMenu;
    bit32 b32Sts;

    unGenericDpFunctions_getParameters(deviceName, dsParameters);

    if (dynlen(menuList) > 0) {
        if (menuList[dynlen(menuList)] != "SEPARATOR") {
            dynAppend(menuList, "SEPARATOR");
        }
    }
    // 1. faceplate
    if (dynContains(dsMenuConfig, UN_POPUPMENU_FACEPLATE_TEXT) > 0) {
        unGenericObject_addItemToPopupMenu(menuList, "Faceplate", CPC_POPUPMENU_FACEPLATE_NUMBER, true);
    }
    unGenericObject_addItemToPopupMenu(menuList, "Hierarchy", CPC_POPUPMENU_HIERARCHY_NUMBER, true);
    if (dynContains(dsMenuConfig, CPC_POPUPMENU_ALARMS_TEXT) > 0) {
        unGenericObject_addItemToPopupMenu(menuList, "Alarms", CPC_POPUPMENU_ALARMS_NUMBER, dynlen(cpcGenericObject_getLinkedAlarms(deviceName, exceptionInfo)) > 0);
    }
    if (dynContains(dsMenuConfig, CPC_POPUPMENU_RECIPES_TEXT) > 0) {
        unGenericObject_addItemToPopupMenu(menuList, CPC_POPUPMENU_RECIPES_TEXT, CPC_POPUPMENU_RECIPES, true);
    }

    unGenericObject_addItemToPopupMenu(menuList, UN_POPUPMENU_DIAGNOSTIC_LABEL, UN_POPUPMENU_DIAGNOSTIC_ID, dsParameters[UN_PARAMETER_DIAGNOSTIC] != "");
	if (dsParameters[UN_PARAMETER_HTML] != "") {
		dynAppend(menuList, "CASCADE_BUTTON, Info, 1");
	} else {
		unGenericObject_addItemToPopupMenu(menuList, UN_POPUPMENU_INFO_LABEL, UN_POPUPMENU_INFO_ID, dsParameters[UN_PARAMETER_HTML] != "");
	}

    unGenericObject_addItemToPopupMenu(menuList, UN_POPUPMENU_SYNOPTIC_LABEL, UN_POPUPMENU_SYNOPTIC_ID, dsParameters[UN_PARAMETER_PANEL] != "");
    unGenericObject_addItemToPopupMenu(menuList, UN_POPUPMENU_COMMENTS_LABEL, UN_POPUPMENU_COMMENTS_ID, TRUE);
    unGenericObject_addItemToPopupMenu(menuList, UN_POPUPMENU_DEVICECONFIG_LABEL, UN_POPUPMENU_DEVICECONFIG_ID, TRUE);
    unGenericObject_addItemToPopupMenu(menuList, UN_POPUPMENU_SYNOPTICLOC_LABEL, UN_POPUPMENU_SYNOPTICLOC_ID, TRUE);

    unGenericDpFunctions_getDeviceLink(deviceName, sLink);
    if (sLink != "") {
        dynAppend(menuList, "CASCADE_BUTTON, Device link(s), 1");
        dynAppend(menuList, "Device link(s)");
        dsSplit = strsplit(sLink, UN_CONFIG_GROUP_SEPARATOR);
        len = dynlen(dsSplit);
        iCste = CPC_POPUPMENU_DEVICELINK_NUMBER;
        for (i = 1; i <= len; i++) {
            iCste++;
            if (dsSplit[i] == "SEPARATOR") {
                dynAppend(menuList, "SEPARATOR");
            } else {
                bool enabled = true;
                pos = strpos(dsSplit[i], ":");
                if (pos <= 0) { // no system name add device system name
                    sDeviceLink = sSystemName + dsSplit[i];
                } else {
                    sDeviceLink = dsSplit[i];
                }

                sSystemLink = unGenericDpFunctions_getSystemName(sDeviceLink);
                unDistributedControl_isConnected(bConnected, sSystemLink);
                if (!bConnected) {
                    enabled = false;
                } else {
                    sDeviceDpLink = unGenericDpFunctions_getWidgetDpName(sDeviceLink);
                    if (!dpExists(sDeviceDpLink)) {
                        enabled = false;
                    }
                }
                string itemName = dsSplit[i];
                if (sDpType == UN_CONFIG_CPC_ANADIG_DPT_NAME || sDpType == UN_CONFIG_CPC_ANALOG_DPT_NAME || sDpType == UN_CONFIG_CPC_ANADO_DPT_NAME) {
                    if (enabled) { // dev existing
                        if (sSystemLink == unGenericDpFunctions_getSystemName(deviceName)) {
                            if (dpTypeName(sDeviceDpLink) == UN_CONFIG_CPC_CONTROLLER_DPT_NAME) { // controller
                                dpGet(sDeviceDpLink + ".ProcessInput.StsReg01", b32Sts);
                                if (getBit(b32Sts, CPC_StsReg01_ACTIVE)) {
                                    itemName = "* " + itemName;
                                }
                            }
                        }
                    }
                }
                unGenericObject_addItemToPopupMenu(menuList, itemName, iCste, enabled);
            }
        }
    }

	if (dsParameters[UN_PARAMETER_HTML] != "") {
		dynAppend(menuList, "Info");
		dsHtmlLinks = strsplit(dsParameters[UN_PARAMETER_HTML],UN_PARAMETER_SUBITEM_DELIMITER);
		if(dynlen(dsHtmlLinks) == 1) {
			unGenericObject_addItemToPopupMenu(menuList, "Label", UN_POPUPMENU_INFO_ID, 1);
		} else {
			for (int i=1 ; i <=dynlen(dsHtmlLinks); i+=2) {
				unGenericObject_addItemToPopupMenu(menuList, dsHtmlLinks[i], UN_POPUPMENU_INFO_ID + (i - 1)/2, 1);
			}
		}
	}
}

/**Notify users about success or failed operation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param sKey message key. unCPCOperator.cat msg catalog should contain $KEY$OK and $KEY$FAILED messages
@param exceptionInfo 'OK' message if it's emtpy.
*/
void cpcGenericObject_notifyUsers(string deviceName, string sKey, dyn_string exceptionInfo) {
    dyn_string exceptionInfoTemp;
    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + ":" + getCatStr("unCPCOperator", sKey + "FAILED"), exceptionInfoTemp);
        if (dynlen(exceptionInfoTemp) > 0) {
            fwExceptionHandling_display(exceptionInfoTemp);
        }
        unSendMessage_toExpertException(this.name(), exceptionInfo);
    } else {
        unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + ":" + getCatStr("unCPCOperator", sKey + "OK"), exceptionInfoTemp);
        if (dynlen(exceptionInfoTemp) > 0) {
            fwExceptionHandling_display(exceptionInfoTemp);
        }
    }
}

/**Function called before the button set state when a user logs in. Common function for all the UNICOS device types.

@par Constraints
  This library works with an unicos button panel and need global variables declared in this panel
		- g_dsUserAccess, dyn_string, button identifier is added to this variable if user access
		- $sDpName, string, the device DP name
		- g_sFaceplateButtonType, string, faceplate type

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
@param sType device type
@param dsReturnData data
*/
void cpcGenericObject_UserCBGetButtonState(string deviceName, string sType, dyn_string &dsReturnData) {
    int iRes;
    string sDpLock, sDpSelectedManager, sSelectedManager, sStsReg01, sStsReg01Bad;
    string sStsReg02, sStsReg02Bad;
    bool bLocked, stsReg01Bad, stsReg02Bad;
    bit32 stsReg01Value, stsReg02Value;

// 2. Get parameters to call the set button function
    sDpLock = deviceName + ".statusInformation.selectedManager:_lock._original._locked";
    sDpSelectedManager = deviceName + ".statusInformation.selectedManager";
    sStsReg01 = deviceName + ".ProcessInput.StsReg01";
    sStsReg01Bad = deviceName + ".ProcessInput.StsReg01:_online.._aut_inv";
    sStsReg02 = deviceName + ".ProcessInput.StsReg02";
    sStsReg02Bad = deviceName + ".ProcessInput.StsReg02:_online.._aut_inv";
    if (dpExists(deviceName)) {
        iRes = dpGet(sDpLock, bLocked, sDpSelectedManager, sSelectedManager);
        if (dpExists(sStsReg01)) {
            iRes = iRes + dpGet(sStsReg01, stsReg01Value, sStsReg01Bad, stsReg01Bad);
        } else {
            sStsReg01 = "";
            sStsReg01Bad = "";
            stsReg01Value = 0;
            stsReg01Bad = 0;
        }
        if (dpExists(sStsReg02)) {
            iRes = iRes + dpGet(sStsReg02, stsReg02Value, sStsReg02Bad, stsReg02Bad);
        } else {
            sStsReg02 = "";
            sStsReg02Bad = "";
            stsReg02Value = 0;
            stsReg02Bad = 0;
        }
    } else {
        iRes = -1;
    }
    if (iRes < 0) { // Errors during dpget
        sDpLock = "";
        bLocked = false;
        sDpSelectedManager = "";
        sSelectedManager = "";
        sStsReg01 = "";
        stsReg01Value = 0;
        sStsReg01Bad = "";
        stsReg01Bad = 0;
        sStsReg02 = "";
        stsReg02Value = 0;
        sStsReg02Bad = "";
        stsReg02Bad = 0;
    }
    dsReturnData = makeDynString(bLocked, sSelectedManager, stsReg01Value, stsReg01Bad, stsReg02Value, stsReg02Bad);
}

/**Get the list of Controller

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sSystemName system name
@param dsDeviceName list of devices
*/
dyn_string cpcGenericObject_getCtrlLink(string sSystemName, dyn_string dsDeviceName) {
    dyn_string dsCtrl;
    string sDp;
    int i, len, iPos;

    len = dynlen(dsDeviceName);
    for (i = 1; i <= len; i++) {
        iPos = strpos(dsDeviceName[i], ":");
        if (iPos <= 0) {
            dsDeviceName[i] = sSystemName + dsDeviceName[i];
        }
        sDp = unGenericDpFunctions_getWidgetDpName(dsDeviceName[i]);
        if (sSystemName == unGenericDpFunctions_getSystemName(sDp)) {
            if (dpExists(sDp)) {
                if (dpTypeName(sDp) == UN_CONFIG_CPC_CONTROLLER_DPT_NAME) {
                    dynAppend(dsCtrl, sDp + ".ProcessInput.StsReg01");
                }
            }
        }
    }

    return dsCtrl;
}

/**Animate value translated

@todo move to the unFaceplate
@see unFaceplate_animateTranslatedValue

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape graphical object name
@param dict dictionary mapping
@param unit value's unit
@param format value's format
@param value value to translate/animate
@param color animation color
@param invalid is dpe invalid
*/
void cpcGenericObject_animateTranslatedValue(string sShape, mapping dict, string unit, string format, anytype value, string color, bool invalid) {
    if (mappingHasKey(dict, (string)value)) { // display label
        if (g_bSystemConnected) {
            setMultiValue(sShape + ".display", "foreCol", invalid ? "unDataNotValid" : color, sShape + ".display", "text", dict[(string)value]);
        }
    } else { // display original value
        if (g_bSystemConnected) {
            unGenericObject_DisplayValue(format, unit, (float) value, sShape, color, invalid);
        }
    }
}

/**Animate text value

@todo move to the unFaceplate
@todo unify params order

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param value value to translate/animate
@param sShape graphical object name
@param sForeCol animation color
@param bInvalid is dpe invalid
*/
void cpcGenericObject_DisplayText(string value, string sShape, string sForeCol, bool bInvalid) {
    if (g_bSystemConnected) {
        setMultiValue(sShape + ".display", "foreCol", bInvalid ? "unDataNotValid" : sForeCol,
                      sShape + ".display", "text", value);
    }
}



/**Animate boolean (bit) value

@todo move to the unFaceplate

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape graphical object name
@param iCommandbit bit value
@param iEnableBit is bit enabled
@param sOneBackCol active color
@param bInvalid is dpe invalid
@param sZeroBackCol inactive color
*/
void cpcGenericObject_ColorBoxAnimate(string sShape, int iCommandbit, int iEnableBit, string sOneBackCol, bool bInvalid, string sZeroBackCol = "unFaceplate_Unactive") {
    if (g_bSystemConnected) {
        unGenericObject_ColorBoxAnimate(sShape, iCommandbit, iEnableBit, sOneBackCol, bInvalid, sZeroBackCol);
    }
}

/**Animate checkbox

@todo move to the unFaceplate

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape graphical object name
@param state boolean state
@param enabled is it enabled
*/
void cpcGenericObject_CheckboxAnimate(string sShape, bool state, bool enabled = true) {
    if (g_bSystemConnected) {
        setValue(sShape + ".checkbox", "state", 0, state);
    }
    if (shapeExists(sShape + "Text")) {
        if (g_bSystemConnected) {
            setValue(sShape + "Text", "enabled", enabled);
        }
    }
}

/**Animate faceplate buttons disconnection

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsShapes buttons (graphical objects) to disconnect
*/
cpcGenericObject_ButtonDisconnect(dyn_string dsShapes) {
    int length = dynlen(dsShapes);
    for (int i = 1; i <= length; i++) {
        string button = UN_FACEPLATE_BUTTON_PREFIX + dsShapes[i];
        if (shapeExists(button)) {
            setValue(button, "enabled", false);
        }
    }
    if (shapeExists(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_SELECT)) {
        setValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_SELECT, "text", UN_FACEPLATE_BUTTON_SELECT_TEXTSELECT);
    }
}

/**Control state animation

Based on constant's name convention, function find an active bit and set according letter (CPC_WIDGET_TEXT_CONTROL_$BIT_NAME$) and color (unWidget_ControlStateManualLocal or unWidget_ControlStateForced if letter == CPC_WIDGET_TEXT_CONTROL_FORCED)

@todo move to cpcWidget

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param bit32StsReg01 StsReg01 value
@param bit32StsReg02 StsReg02 value
@param d_sBits list of bit constants
@param letter result letter
@param color result color
*/
void cpcGenericObject_WidgetControlStateAnimation(bit32 bit32StsReg01, bit32 bit32StsReg02, dyn_string d_sBits, string &letter, string &color)
{
  string sKey, sConstant;
  dyn_string exceptionInfo;


    letter = "";
    color = "unWidget_ControlStateManualLocal";


    for (int i = 1; i <= dynlen(d_sBits); i++) {
        string bit = d_sBits[i];
        dyn_string names = strsplit(bit, "_");
        if (dynlen(names) != 3) {
            fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_WidgetControlStateAnimation: wrong bit name - " + bit, "");
            break;
        }

  // Split the constant to query the cache: key + constant
  if( substr(bit, 0, 4) == "CPC_" )
  {
    sKey = "CPC";
    sConstant = substr(bit, 4);
  }
  else
  {
    fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_WidgetControlStateAnimation() -> constant doesn't belong to CPC: " + bit, "");
    fwExceptionHandling_display(exceptionInfo);
    return;
  }



        int pos = cpcConfigGenericFunctions_getConstantByName(sKey, sConstant);

        bool enabled = false;
        if (names[2] == "StsReg01") {
            enabled = getBit(bit32StsReg01, pos) == 1;
        } else if (names[2] == "StsReg02") {
            enabled = getBit(bit32StsReg02, pos) == 1;
        } else {
            fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_WidgetControlStateAnimation: wrong register in " + bit, "");
        }
        if (enabled) {
            letter = cpcConfigGenericFunctions_getStringConstantByName("CPC_WIDGET_TEXT_CONTROL_" + names[3]);
            break;
        }
    }
    if (letter == CPC_WIDGET_TEXT_CONTROL_FORCED) {
        color = "unWidget_ControlStateForced";
    }
}

/**Retrieve an online value of a bit

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param name bit name (position constant)
@param bit32StsReg01 StsReg01 value
@param bit32StsReg02 StsReg02 value
@return a bit value from StsReg
*/
int cpcGenericObject_StsRegConstantNameToValue(string name, bit32 bit32StsReg01, bit32 bit32StsReg02)
{
  string sKey, sConstant;
  dyn_string exceptionInfo;

    if (name == "") {
        return -1;
    }

  // Split the constant to query the cache: key + constant
  if( substr(name, 0, 4) == "CPC_" )
  {
    sKey = "CPC";
    sConstant = substr(name, 4);
  }
  else
  {
    fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_StsRegConstantNameToValue() -> constant doesn't belong to CPC: " + name, "");
    fwExceptionHandling_display(exceptionInfo);
    return -1;
  }

    int pos = cpcConfigGenericFunctions_getConstantByName(sKey, sConstant);
    if (pos < 0) {
        fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_StsRegConstantNameToValue: negative constant value for " + name, "");
        return -1;
    }

    dyn_string names = strsplit(name, "_");
    if (dynlen(names) != 3) {
        fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_StsRegConstantNameToValue: wrong constant name - " + name, "");
        return -1;
    }

    bool value = false;
    if (names[2] == "StsReg01") {
        return getBit(bit32StsReg01, pos);
    } else if (names[2] == "StsReg02") {
        return getBit(bit32StsReg02, pos);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_StsRegConstantNameToValue: wrong register in " + name, "");
        return -1;
    }
}

/**Send a pulse with a specified number

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDpName object name
@param sDpe Dpe name
@param value the pulse value of ManReg
@param exceptionInfo details of any exceptions are returned here
*/
void cpcGenericObject_SendBigPulse(string sDpName, string sDpe, bit32 value, dyn_string &exceptionInfo) {
    string regName, dpType, alias;
    bit32 regValue;
    dyn_string exceptionInfoTemp;

    regName = unGenericDpFunctions_getDpName(sDpName) + sDpe;
    if (dpGet(regName, regValue) == -1) {
        fwException_raise(exceptionInfo, "ERROR", "unGenericObject_sendPulse : " + regName + getCatStr("unPVSS", "DPGETFAILED"), "");
    } else {
        if (dpSet(regName, value) == -1) {
            fwException_raise(exceptionInfo, "ERROR", "unGenericObject_sendPulse : " + regName + getCatStr("unPVSS", "DPSETFAILED"), "");
        }
    }

    unGenericObject_WaitPulse();

    if (dpSet(regName, regValue) == -1) {
        fwException_raise(exceptionInfo, "ERROR", "unFaceplateButtons_sendPulse : " + regName + getCatStr("unPVSS", "DPSETFAILED"), "");
    }
}

/**Translate interlock acronyms

@todo move to unFaceplate

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param acronyms types of linked interlocks
@return human representation for linked interlocks
*/
string cpcGenericObject_translateAlarmAcronym(dyn_string acronyms) {
    if (dynlen(acronyms) == 1) {
        switch (substr(acronyms[1], 0, 2)) {
            case "AL":
                return "No Interlock (AL)";
            case "FS":
                return "Full Stop Interlock (FS)";
            case "TS":
                return "Temp Stop Interlock (TS)";
            case "SI":
                return "Start Interlock (SI)";
            default:
                return "Unknown (" + acronyms + ")";
        }
    } else if (dynlen(acronyms) > 1) {
        dyn_string clearTypes;
        for (int i = 1; i <= dynlen(acronyms); i++) {
            dynAppend(clearTypes, substr(acronyms[i], 0, 2));
        }
        return "Multiple (" + dynJoin(clearTypes, ",") + ")";
    } else {
    }
    return "No type";
}

/**Translate interlock acronyms attaching master object

@param dsAcronyms,   input, dyn_string, Types of linked interlocks
@param dsObjects,    input, dyn_string, Master of each alarm

@return dyn_string,  List of type of alarm and its master

*/
dyn_string cpcGenericObject_translateMultiAlarmAcronym(dyn_string dsAcronyms, dyn_string dsObjects)
{
  int iLoop, iLen;
  dyn_string dsReturnedString;


  iLen = dynlen(dsObjects);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynAppend(dsReturnedString, dsObjects[iLoop] + " - " + cpcGenericObject_translateAlarmAcronym(makeDynString(dsAcronyms[iLoop])));
  }

  return dsReturnedString;
}




/**Animate warning colorBoxes

Color :
- if alert hdl does not exist, backcolor = "unAlarmDoesNotExist"
- else if bInvalid = true, backcolor = "unDataNotValid"
- else if alarm unack., backcolor = "unFaceplate_AlarmNotAck"
- else if alarm active and ack., backcolor = "unFaceplate_AlarmActive"
- else backcolor = "unFaceplate_Unactive"

@todo move to unFaceplate

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape colorbox shape name
@param iAlarmState act state alarm
@param isAlertExists is alert exist
@param isAlertActive is alert active
@param isAlertInvalid is alert invalid
*/
void cpcGenericObject_ColorBoxAnimateWarning(string sShape, int iAlarmState, bool isAlertExists, bool isAlertActive, bool isAlertInvalid) {
    cpcGenericObject_ColorBoxAnimateAlarm(sShape, iAlarmState, isAlertExists, isAlertActive, isAlertInvalid, "cpcColor_Widget_Warning", "cpcColor_Widget_Warning");
}

/**Animate alarm colorBoxes

Color :
- if alert hdl does not exist, backcolor = "unAlarmDoesNotExist"
- else if bInvalid = true, backcolor = "unDataNotValid"
-	else if alarm unack., backcolor = "unFaceplate_AlarmNotAck"
- else if alarm active and ack., backcolor = "unFaceplate_AlarmActive"
- else backcolor = "unFaceplate_Unactive"

@todo move to unFaceplate

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape colorbox shape name
@param iAlarmState act state alarm
@param isAlertExists is alert exist
@param isAlertActive is alert active
@param isAlertInvalid is alert invalid
@param activeColor active color, default is "unFaceplate_AlarmActive"
@param notAckColor not acknowledge color, default is "unFaceplate_AlarmNotAck"
*/
void cpcGenericObject_ColorBoxAnimateAlarm(string sShape, int iAlarmState, bool isAlertExists, bool isAlertActive, bool isAlertInvalid, string activeColor = "unFaceplate_AlarmActive", string notAckColor = "unFaceplate_AlarmNotAck") {
    string sColor;
    bool textEnabled = true;

    if (!isAlertExists) {
        sColor = "_3DFace";
        textEnabled = false;
    } else if (isAlertInvalid) {
        sColor = "unDataNotValid";
    } else if (!isAlertActive) {
        sColor = "unFaceplate_Disabled";
        textEnabled = false;
    } else {
        switch (iAlarmState) {
            case DPATTR_ALERTSTATE_NONE:
                sColor = "unFaceplate_Unactive";
                break;
            case DPATTR_ALERTSTATE_APP_ACK:
                sColor = activeColor;
                break;
            case DPATTR_ALERTSTATE_APP_NOT_ACK:
            case DPATTR_ALERTSTATE_DISAPP_NOT_ACK:
            case DPATTR_ALERTSTATE_APP_DISAPP_NOT_ACK:
                sColor = notAckColor;
                break;
            default:
                sColor = "unFaceplate_Unactive";
                break;
        }
    }

    if (g_bSystemConnected) {
        setValue(sShape + ".colorbox", "backCol", sColor);
    }
    if (shapeExists(sShape + "Text")) {
        if (g_bSystemConnected) {
            setValue(sShape + "Text", "enabled", textEnabled);
        }
    }
}

/**Retrieve 5 ranges alarm type

Return in iAlertType:
-# DPCONFIG_ALERT_NONBINARYSIGNAL if Unicos 5 ranges alert_hdl is correct (and if sDpe is analogic)
-# DPCONFIG_ALERT_BINARYSIGNAL for boolean sDpe
-# DPCONFIG_NONE if no alert_hdl is defined or if alert_hdl is a bad analogic alarm

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sDpe dpe
@param iAlertType alert type
@param exceptionInfo for errors
*/
dyn_bool cpcGenericObject_GetAnalogAlarmType(string sDpe, int &iAlertType, dyn_string &exceptionInfo) {
    int alertDPType = DPCONFIG_NONE, iRes, rangesNumber = -1, rangeOk;
    iAlertType = DPCONFIG_NONE;
    dyn_bool ranges = makeDynBool(false, false, false, false);
    bit32 alertRange = 0;

    if (!dpExists(sDpe)) {
        fwException_raise(exceptionInfo, "ERROR", "cpcGenericObject_GetAnalogAlarmType: " + sDpe + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
    } else {
        dpGet(sDpe + ":_alert_hdl.._type", alertDPType);
        switch (alertDPType) {
            case DPCONFIG_ALERT_BINARYSIGNAL:
                iAlertType = DPCONFIG_ALERT_BINARYSIGNAL;
                break;
            case DPCONFIG_ALERT_NONBINARYSIGNAL:
                iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL; //@TODO false when invalid alarms
                dpGet(sDpe + ":_alert_hdl.._num_ranges", rangesNumber);
                //@TODO: optimize it!
                //@TODO : check if rangeOK is valid! - rangesNumber <= 5 && rangeOK in the middle...
                for (int i = 1; i <= rangesNumber; i++) {
                    string rangeClass = "#";
                    dpGet(sDpe + ":_alert_hdl." + i + "._class", rangeClass);
                    if (rangeClass == "") {
                        rangeOk = i; // Range Ok
                    }
                }
                for (int i = 1; i < rangeOk; i++) {
                    string rangeClass = "#";
                    dpGet(sDpe + ":_alert_hdl." + i + "._class", rangeClass);
                    if (strpos(rangeClass, "cpcAnalogLL") >= 0) {
                        setBit(alertRange, 0, true);
                        ranges[1] = true;
                    } else if (strpos(rangeClass, "cpcAnalogL") >= 0) {
                        setBit(alertRange, 1, true);
                        ranges[2] = true;
                    } else {
                    }
                }
                for (int i = rangeOk + 1; i <= rangesNumber; i++) {
                    string rangeClass = "#";
                    dpGet(sDpe + ":_alert_hdl." + i + "._class", rangeClass);
                    if (strpos(rangeClass, "cpcAnalogHH") >= 0) {
                        setBit(alertRange, 3, true);
                        ranges[4] = true;
                    } else if (strpos(rangeClass, "cpcAnalogH") >= 0) {
                        setBit(alertRange, 2, true);
                        ranges[3] = true;
                    } else {
                    }
                }
                break;
            case DPCONFIG_NONE:
            default:
                iAlertType = DPCONFIG_NONE;
                break;
        }
    }
    return ranges;
}

/**Return is alarm configured with Mail/SMS

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpName dpe
@param alertType alert type

@reviewed 2018-08-01 @whitelisted{FalsePositive}

*/
bool cpcGenericObject_isAlarmWithMail(string dpName, int alertType) {
    string alertClass;
    int rangesNumber;
    dyn_bool isConfigured;
    if (alertType == DPCONFIG_ALERT_NONBINARYSIGNAL) {
        dpGet(dpName + ":_alert_hdl.._num_ranges", rangesNumber);
        for (int i = 1; i <= rangesNumber; i++) {
            dpGet(dpName + ":_alert_hdl." + i + "._class", alertClass);
            if (alertClass != "") { // skip OK class
                dynAppend(isConfigured, patternMatch(UN_PROCESSALARM_PATTERN, alertClass));
            }
        }
        dynUnique(isConfigured);
        if (dynlen(isConfigured) == 1) {
            return isConfigured[1];
        } else {
            return false;
        }
    } else {
        dpGet(dpName + ":_alert_hdl.1._class", alertClass);
        return patternMatch(UN_PROCESSALARM_PATTERN, alertClass);
    }
}

/** Function returns true if dpe is in any Mail/SMS category
*/
bool cpcGenericObject_isInMailSMS(string dpName) {
    dyn_string dsSettings;
    dpGet(unGenericDpFunctions_getSystemName(dpName) + "_unSendMail.config.report_settings:_original.._value", dsSettings);
    for (int i = 1; i <= dynlen(dsSettings); i++) {
        if (unProcessAlarm_isInDPCategory(dpName, dsSettings[i])) return true;
    }
    return false;
}



/**Animate widget disconnection

@todo move to the cpcWidget

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sWidgetType widget type
@param sDpType object
*/
void cpcGenericObject_WidgetDisconnection(string sWidgetType, string sDpType) {
    dyn_string dsFunctions, exceptionInfo;
    string sFunction;

    // 1. Disconnection of widget body
    sFunction = "cpcWidget_Widget" + sWidgetType + "Disconnection";
    if (isFunctionDefined(sFunction)) {
        execScript("main() {" + sFunction + "();}", makeDynString());
    }
    // 2. Disconnection of alarm, warning, mode letter & select animation & right click
    setMultiValue("WarningText", "text", "", "AlarmText", "text", "", "ControlStateText", "text", "",
                  "SelectArea", "foreCol", "", "LockBmp", "visible", false, "WidgetArea", "visible", false);
}

/**Return widget animation function for a given widget type

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI
*/
string cpcGenericObject_GetAnimationFunction(string sWidgetType) {
    return "cpcWidget_Widget" + sWidgetType + "Animation";
}

/**Open hierarchy window for a device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
*/
void cpcGenericObject_OpenHierarchy(string deviceName) {
    dyn_string exceptionInfo;
    int xPos, yPos;
    dyn_int diPanelSize;
    bool bRes;
    float width, heigth;

    getValue("", "position", xPos, yPos);
    getValue("", "size", width, heigth);
    xPos = xPos + 2 * width;
    string panelPath = "vision/cpcObjects/General/cpcHierarchy.pnl";
    diPanelSize = getPanelSize(panelPath);
    if (dynlen(diPanelSize) == 2) {
        xPos = xPos - diPanelSize[1];
        yPos = yPos - diPanelSize[2] - 15;
    }

    string sPanelName = "Hierarchy of " + unGenericDpFunctions_getAlias(deviceName);

    if (this.name == "WidgetArea") {
        unGraphicalFrame_ChildPanelOn(panelPath, sPanelName, makeDynString("$sDpName:" + deviceName), xPos, yPos);
    } else {
        unGraphicalFrame_ChildPanelOnCentral(panelPath, sPanelName, makeDynString("$sDpName:" + deviceName));
    }
}

/**Open alarm window for a device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
*/
void cpcGenericObject_OpenAlarmsPanel(string deviceName) {
    dyn_string exceptionInfo;
    int xPos, yPos;
    dyn_int diPanelSize;
    bool bRes;
    float width, heigth;

    getValue("", "position", xPos, yPos);
    getValue("", "size", width, heigth);
    xPos = xPos + 2 * width;
    diPanelSize = getPanelSize("vision/cpcObjects/General/cpcLinkedAlarmList.pnl");
    if (dynlen(diPanelSize) == 2) {
        xPos = xPos - diPanelSize[1];
        yPos = yPos - diPanelSize[2] - 15;
    }
    unGraphicalFrame_ChildPanelOn("vision/cpcObjects/General/cpcLinkedAlarmList.pnl",
                                  unGenericDpFunctions_getAlias(deviceName) + " " + unGenericDpFunctions_getDescription(deviceName) + " Configured alarms",
                                  makeDynString("$sDpName:" + deviceName),
                                  xPos, yPos);
}

/**Open recipe window

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param deviceName device name
*/
cpcGenericObject_OpenRecipes(string deviceName) {
    dyn_string exceptionInfo;
    int xPos, yPos;
    dyn_int diPanelSize;
    bool bRes;
    float width, heigth;

    getValue("", "position", xPos, yPos);
    getValue("", "size", width, heigth);
    xPos = xPos + 2 * width;
    diPanelSize = getPanelSize("vision/unRecipe/UnRcpInstance/unFaceplateUnRcpInstance.pnl");
    if (dynlen(diPanelSize) == 2) {
        xPos = xPos - diPanelSize[1];
        yPos = yPos - diPanelSize[2] - 15;
    }
    unGraphicalFrame_ChildPanelOn("vision/unRecipe/UnRcpInstance/unFaceplateUnRcpInstance.pnl",
                                  "RcpInstanceFaceplate",
                                  makeDynString("$sDpName:" + deviceName),
                                  xPos, yPos);
}


/**Returns max width and max height withing objects widgets

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param objects list of objects
*/
dyn_int cpcGenericObject_getMaxWidgetSize(dyn_string objects) {
    dyn_int maxDim = makeDynInt(1, 1);
    for (int i = 1; i <= dynlen(objects); i++) {
        string widget = cpcGenericObject_getDeviceWidget(objects[i]);
        if (widget != "") {
            dyn_int widgetDim = getPanelSize(widget);
            if (widgetDim[1] > maxDim[1]) {
                maxDim[1] = widgetDim[1];
            }
            if (widgetDim[2] > maxDim[2]) {
                maxDim[2] = widgetDim[2];
            }
        }
    }
    return maxDim;
}

/**Returns a widget associated with device.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param device device name
*/
string cpcGenericObject_getDeviceWidget(string device) {
    string sWidget;
    dyn_string dsWidgets;

    string objectDP = unGenericDpFunctions_getWidgetDpName(device);
    string objectType = dpTypeName(objectDP);

    unGenericDpFunctions_getDeviceWidgetList(objectType, dsWidgets);
    unGenericDpFunctions_getDeviceWidget(objectDP, sWidget, dsWidgets);

    return sWidget;
}

/**Return a list of alarms linked to object

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param device device name
@param exceptionInfo for errors
*/
dyn_string cpcGenericObject_getLinkedAlarms(string device, dyn_string& exceptionInfo) {
    dyn_string children, alarmList;
    string deviceSystemName = unGenericDpFunctions_getSystemName(device);
    unGenericDpFunctions_getKeyDeviceConfiguration(device, CPC_CONFIG_CHILDREN_KEY, children, exceptionInfo);
    dynUnique(children);
    for (int i = 1; i <= dynlen(children); i++) {
        string child = unGenericDpFunctions_getWidgetDpName(deviceSystemName + children[i]);
        if (child != "" && strpos(dpTypeName(child), "Alarm") > 0) {
            dynAppend(alarmList, child);
        }
    }
    return alarmList;
}

/**Returns device's childrens filtered by type

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param device device name
@param childrenType dp type to fileter children with
@param exceptionInfo for errors
*/
dyn_string cpcGenericObject_getLinkedObjects(string device, string childrenType, dyn_string& exceptionInfo) {
    dyn_string children, childrenList;
    string deviceSystemName = unGenericDpFunctions_getSystemName(device);
    unGenericDpFunctions_getKeyDeviceConfiguration(device, CPC_CONFIG_CHILDREN_KEY, children, exceptionInfo);
    dynUnique(children);
    for (int i = 1; i <= dynlen(children); i++) {
        string child = unGenericDpFunctions_getWidgetDpName(deviceSystemName + children[i]);
        if (child != "" && dpTypeName(child) == childrenType) {
            dynAppend(childrenList, child);
        }
    }
    return childrenList;
}

cpcGenericObject_WidgetWarningAnimationDoubleStsReg(bit32 bit32StsReg01, bit32 bit32StsReg02, dyn_int diBits, dyn_int diBits2, dyn_string dsLetters, dyn_string dsLetters2, string &letter, string &color) {
    // Init
    letter = "";
    color = "cpcColor_Widget_Warning";

    // Low Priority for the first stsReg: stsReg01
    if (dynlen(diBits) == dynlen(dsLetters)) {
        // For each defined warning
        for (int i = 1; i <= dynlen(diBits); i++) {
            if (getBit(bit32StsReg01, diBits[i]) == 1) {
                letter = dsLetters[i];
            }
        }
    }
    // CASE double stsReg to be taken into account: Analog, AnaDig, OnOff, PCO.
    if (dynlen(diBits2) > 0) {
        // First treatement of the stsreg02 bits
        if (dynlen(diBits2) == dynlen(dsLetters2)) {
            // For each defined warning
            for (int i = 1; i <= dynlen(diBits2); i++) {
                if (getBit(bit32StsReg02, diBits2[i]) == 1) {
                    letter = dsLetters2[i];
                }
            }
        }
    }
}

/**Animate 'allow restart needed' bit.

Faceplate should contain unFaceplate_Colorbox.pnl graphical object named "NEEDRESTART".

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
*/
void cpcGenericObject_animateNeedRestart(dyn_string dpes, dyn_string values) {
    bool needRestartInvalid, needRestart;
    if (dynlen(dpes) > 0) { // online animation
        unFaceplate_fetchStatusBitAndValidness(dpes, values, "CPC_StsReg02_NEEDRESTART", needRestart, needRestartInvalid);
        cpcGenericObject_ColorBoxAnimate("NEEDRESTART", g_params["MRestart"] && needRestart == 0, 1, "cpcColor_Faceplate_Warning",
                                         needRestartInvalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
    } else {
        unGenericObject_ColorBoxDisconnect("NEEDRESTART");
    }
}

/**Animate mask event.

Faceplate should contain unFaceplate_Colorbox.pnl graphical object named "eventState".

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
*/
void cpcGenericObject_animateMaskEvent(dyn_string dpes, dyn_string values) {
    bool maskEvent, invalid;
    if (dynlen(dpes) > 0) { // online animation
        unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "eventMask", maskEvent, invalid);
        if (g_bSystemConnected)
            unGenericObject_ColorBoxAnimate("eventState", !maskEvent, 1, "cpcColor_Faceplate_Warning",
                                            invalid || unFaceplate_connectionValid(values));
    } else {
        unGenericObject_ColorBoxDisconnect("eventState");
    }
}

/** Check if given value is in the range of specified dpe

Note: only Min-Max range is supported; if there is no config or config of another nature, value is accepted as in range
@par Constraints
  None

@par Usage
  Public

@param key name of the value; for logging purpose
@param value value to be checked
@param dpe Data Point Element holding the pvRange configuration
@param exceptionInfo for exception handling
*/
bool valueInsideRange(anytype key, anytype value, string dpe, dyn_string &exceptionInfo) {

    bool configExists;
    int pvRangeType;
    dyn_anytype configData;
    dyn_string exceptionInfoTemp;

    fwPvRange_getObject(dpe, configExists, pvRangeType, configData, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        return false;
    }
    if(configExists && pvRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK) {

        anytype minValue = configData[fwPvRange_MINIMUM_VALUE];
        anytype maxValue = configData[fwPvRange_MAXIMUM_VALUE];

        anytype minInclusive = configData[fwPvRange_INCLUSIVE_MINIMUM];
        anytype maxInclusive = configData[fwPvRange_INCLUSIVE_MAXIMUM];

        if (( minInclusive && value <  minValue) ||
            (!minInclusive && value <= minValue) ||
            ( maxInclusive && value >  maxValue) ||
            (!maxInclusive && value >= maxValue)) {
                dynAppend(exceptionInfo, "Value " + value + " for " + key + " is outside the allowed range defined for " + dpe);
                return false;
            } else {
                return true;
            }
    } else {
        return true;
    }
}

/** For given device set its default values specified in defDpeValues mapping (DefDPEname:value)

Function checks if all Def DPEs exist for specified deviceName (e.g. deviceName.ProcessOutput.DefDPEName) and if provided values are in correct ranges (if applicable)
If any of the DefDpes does not exist or any value is outside valid range, no changes will be applied to the system

@par Constraints
  None

@par Usage
  Public

@param deviceName name of the device
@param defDpeValues Mapping between Def DPE names and values to be set on them
@param exceptionInfo for exception handling

@return 0 if everything went successfully; -1 if any of DefDPEs does not exist or any value is invalid; n if n dpSets failed
*/
int cpcGenericObject_setDefaultValue(string deviceName, mapping defDpeValues, dyn_string &exceptionInfo) {
    bool allDpesExist = true, allValuesValid = true;

    for(int i=1; i<= mappinglen(defDpeValues); i++) {
        anytype key = mappingGetKey(defDpeValues, i);
        anytype value = mappingGetValue(defDpeValues, i);
        string dpeName = deviceName + ".ProcessOutput." + key;
        if (!dpExists(dpeName)) { // verify if dpes exist and values are correct
            dynAppend(exceptionInfo, "DPE " + dpeName + " does not exist.");
            allDpesExist = false;
        } else { // check if value is valid in given context
            string dpTypeName = dpTypeName(deviceName);
            if (dpTypeName == "CPC_WordParameter" || dpTypeName == "CPC_AnalogParameter") {
                allValuesValid &= valueInsideRange(key, value, deviceName + ".ProcessInput.PosSt", exceptionInfo);
            } else if (dpTypeName == "CPC_Controller") {
                if(key == "DefKc" && value <= 0){
                    allValuesValid = false;
                    dynAppend(exceptionInfo, "Value " + value + " for " + dpeName + " must be positive.");
                }
                if((key == "DefTi" || key == "DefTd" || key == "DefTds") && value < 0) {
                    allValuesValid = false;
                    dynAppend(exceptionInfo, "Value " + value + " for " + dpeName + " cannot be negative.");
                }
                if(key == "DefSp" || key == "DefSpH" || key == "DefSpL"){
                    allValuesValid &= valueInsideRange(key, value, deviceName + ".ProcessInput.ActSP", exceptionInfo);
                }
                if(key == "DefOutH" || key == "DefOutL") {
                    allValuesValid &= valueInsideRange(key, value, deviceName + ".ProcessInput.OutOVSt", exceptionInfo);
                }
            }
        }
    }

    // if there is anything wrong - do not appy any changes
    if(!allDpesExist || !allValuesValid) {
        return -1;
}
    int failedDpSets = 0;
    for(int i=1; i<= mappinglen(defDpeValues); i++) {
        string dpeName = deviceName + ".ProcessOutput." + mappingGetKey(defDpeValues, i);
        anytype value = mappingGetValue(defDpeValues, i);
        if(dpSet(dpeName, value) != 0) {
            failedDpSets++;
            dynAppend(exceptionInfo, "Problem with dpSet on " + dpeName);
        }
    }
    return failedDpSets;
}
