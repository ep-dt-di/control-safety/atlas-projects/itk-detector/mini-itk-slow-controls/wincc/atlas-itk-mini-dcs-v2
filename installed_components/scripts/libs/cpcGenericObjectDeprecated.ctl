/**Animate interlock

@todo move to the unFaceplate

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape graphical object name
@param pvssAlarmState state of pvss alert (value of ":_alert_hdl.._act_state" property of dpe)
@param alarmState state in StsReg
@param alarmAck is alarm acknowledged (normally it's a CPC_StsReg01_ALUNACK value)
@param bInvalid is dpe invalid

@reviewed 2018-08-01

*/
void cpcGenericObject_ColorBoxAnimateInterlock(string sShape, int pvssAlarmState, bool alarmState, bool alarmAck, bool bInvalid) 
{

  FWDEPRECATED();

    if (alarmState) {
        if (g_bSystemConnected) {
            unGenericObject_ColoredSquareAnimate(sShape, alarmState, alarmAck && pvssAlarmState == 1 || pvssAlarmState == 3, "unFaceplate_AlarmNotAck", "unFaceplate_AlarmActive", bInvalid);
        }
    } else {
        if (g_bSystemConnected) {
            unGenericObject_ColoredSquareAnimate(sShape, !(alarmAck && (pvssAlarmState == 1 || pvssAlarmState == 3)), true, "unFaceplate_Unactive", "unFaceplate_AlarmNotAck", bInvalid);
        }
    }
}






/**Is bit set in StsReg value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param value (StsReg) value
@param bitPosition bit position

@reviewed 2018-08-01

*/
bool cpcGenericObject_getBit(int value, int bitPosition) 
{

  FWDEPRECATED();

    int pattern = pow(2, bitPosition);
    return value & pattern;
}