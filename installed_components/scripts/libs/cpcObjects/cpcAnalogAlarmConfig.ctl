/**@file

// cpcAnalogAlarmConfig.ctl
This library contains the import and export function of the CPC_AnalogAlarm.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_ANALOGALARM_DPT_NAME = "CPC_AnalogAlarm";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_ANALOGALARM_LENGTH = 24;

const unsigned UN_CONFIG_CPC_ANALOGALARM_UNIT                   = 1;
const unsigned UN_CONFIG_CPC_ANALOGALARM_FORMAT                 = 2;
const unsigned UN_CONFIG_CPC_ANALOGALARM_HHALST_ARCHIVE_ACTIVE  = 3;
const unsigned UN_CONFIG_CPC_ANALOGALARM_HWST_ARCHIVE_ACTIVE    = 3;
const unsigned UN_CONFIG_CPC_ANALOGALARM_LWST_ARCHIVE_ACTIVE    = 3;
const unsigned UN_CONFIG_CPC_ANALOGALARM_LLALST_ARCHIVE_ACTIVE  = 3;
const unsigned UN_CONFIG_CPC_ANALOGALARM_IST_ARCHIVE_ACTIVE     = 3;
const unsigned UN_CONFIG_CPC_ANALOGALARM_WST_ARCHIVE_ACTIVE     = 3;
const unsigned UN_CONFIG_CPC_ANALOGALARM_POSST_ARCHIVE_ACTIVE       = 4;
const unsigned UN_CONFIG_CPC_ANALOGALARM_POSST_ARCHIVE_TIME_FILTER  = 5;
const unsigned UN_CONFIG_CPC_ANALOGALARM_SMS_CATEGORIES             = 6;
const unsigned UN_CONFIG_CPC_ANALOGALARM_SMS_MESSAGE                = 7;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ACK_ALARM                  = 8;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ALARM_LEVEL                = 9;
const unsigned UN_CONFIG_CPC_ANALOGALARM_NORMAL_POSITION            = 10;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_STSREG01           = 11;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_STSREG02           = 12;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_EVSTSREG01     = 13;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_EVSTSREG02     = 14;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_POSST              = 15;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_HHST               = 16;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_HST                = 17;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_LST                = 18;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_LLST               = 19;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_MANREG01           = 20;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_HH             = 21;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_H                  = 22;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_L                  = 23;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDRESS_LL             = 24;

const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_SECOND_ALIAS    = 7;

const string UN_CONFIG_CPC_ANALOGALARM_HHALST_ALERT_TEXT   = "HH";
const string UN_CONFIG_CPC_ANALOGALARM_HWST_ALERT_TEXT     = "H";
const string UN_CONFIG_CPC_ANALOGALARM_LWST_ALERT_TEXT     = "L";
const string UN_CONFIG_CPC_ANALOGALARM_LLALST_ALERT_TEXT   = "LL";

const string UN_CONFIG_CPC_ANALOGALARM_HHALST_ALERT_CLASS_POSTFIX   = "HH";
const string UN_CONFIG_CPC_ANALOGALARM_HWST_ALERT_CLASS_POSTFIX     = "H";
const string UN_CONFIG_CPC_ANALOGALARM_LWST_ALERT_CLASS_POSTFIX     = "L";
const string UN_CONFIG_CPC_ANALOGALARM_LLALST_ALERT_CLASS_POSTFIX   = "LL";
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_AnalogAlarmConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"]        = ".ProcessInput.StsReg01";
    props["dataType"]       = CPC_UINT16;
    config["StsReg01"]      = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.StsReg02";
    props["dataType"]       = CPC_UINT16;
    config["StsReg02"]      = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.evStsReg01";
    props["dataType"]       = CPC_INT32;
    config["evStsReg01"]    = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.evStsReg02";
    props["dataType"]       = CPC_INT32;
    config["evStsReg02"]    = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.PosSt";
    props["dataType"]       = CPC_FLOAT;
    config["PosSt"]         = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessInput.HHSt";
    props["dataType"]       = CPC_FLOAT;
    config["HHSt"]          = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessInput.HSt";
    props["dataType"]       = CPC_FLOAT;
    config["HSt"]           = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessInput.LSt";
    props["dataType"]       = CPC_FLOAT;
    config["LSt"]           = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessInput.LLSt";
    props["dataType"]       = CPC_FLOAT;
    config["LLSt"]          = props;

    mappingClear(props);
    props["hasAcknowledge"] = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.HHAlSt";
    props["dpe"]            = "StsReg02";
    props["bitPosition"]    = CPC_StsReg02_HHALST;
    props["isAlarm"]        = true;
    config["HHAlSt"]        = props;

    mappingClear(props);
    props["hasAcknowledge"] = false;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.HWSt";
    props["dpe"]            = "StsReg02";
    props["bitPosition"]    = CPC_StsReg02_HWST;
    props["isAlarm"]        = true;
    config["HWSt"]          = props;

    mappingClear(props);
    props["hasAcknowledge"] = false;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.LWSt";
    props["dpe"]            = "StsReg02";
    props["bitPosition"]    = CPC_StsReg02_LWST;
    props["isAlarm"]        = true;
    config["LWSt"]          = props;

    mappingClear(props);
    props["hasAcknowledge"] = true;
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.LLAlSt";
    props["dpe"]            = "StsReg02";
    props["bitPosition"]    = CPC_StsReg02_LLALST;
    props["isAlarm"]        = true;
    config["LLAlSt"]        = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.ISt";
    props["dpe"]            = "StsReg01";
    props["bitPosition"]    = CPC_StsReg01_IST;
    props["dataType"]       = CPC_BOOL;
    config["ISt"]           = props;

    mappingClear(props);
    props["hasArchive"]     = true;
    props["address"]        = ".ProcessInput.WSt";
    props["dpe"]            = "StsReg01";
    props["bitPosition"]    = CPC_StsReg01_WST;
    props["dataType"]       = CPC_BOOL;
    config["WSt"]           = props;

    mappingClear(props);
    props["address"]        = ".ProcessInput.AlUnAck";
    props["dpe"]            = "StsReg01";
    props["bitPosition"]    = CPC_StsReg01_ALUNACK;
    config["AlUnAck"]       = props;

    mappingClear(props);
    props["address"]        = ".ProcessOutput.ManReg01";
    props["dataType"]       = CPC_UINT16;
    config["ManReg01"]      = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessOutput.HH";
    props["dataType"]       = CPC_FLOAT;
    config["HH"]            = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessOutput.H";
    props["dataType"]       = CPC_FLOAT;
    config["H"]             = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessOutput.L";
    props["dataType"]       = CPC_FLOAT;
    config["L"]             = props;

    mappingClear(props);
    props["hasUnit"]        = true;
    props["hasFormat"]      = true;
    props["address"]        = ".ProcessOutput.LL";
    props["dataType"]       = CPC_FLOAT;
    config["LL"]            = props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_AnalogAlarmConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                   = UN_CONFIG_CPC_ANALOGALARM_DPT_NAME;
  mTemp["LENGTH"]                     = UN_CONFIG_CPC_ANALOGALARM_LENGTH;
  mTemp["UNIT"]                       = UN_CONFIG_CPC_ANALOGALARM_UNIT;
  mTemp["FORMAT"]                     = UN_CONFIG_CPC_ANALOGALARM_FORMAT;
  mTemp["HHALST_ARCHIVE_ACTIVE"]      = UN_CONFIG_CPC_ANALOGALARM_HHALST_ARCHIVE_ACTIVE;
  mTemp["HWST_ARCHIVE_ACTIVE"]        = UN_CONFIG_CPC_ANALOGALARM_HWST_ARCHIVE_ACTIVE;
  mTemp["LWST_ARCHIVE_ACTIVE"]        = UN_CONFIG_CPC_ANALOGALARM_LWST_ARCHIVE_ACTIVE;
  mTemp["LLALST_ARCHIVE_ACTIVE"]      = UN_CONFIG_CPC_ANALOGALARM_LLALST_ARCHIVE_ACTIVE;
  mTemp["IST_ARCHIVE_ACTIVE"]         = UN_CONFIG_CPC_ANALOGALARM_IST_ARCHIVE_ACTIVE;
  mTemp["WST_ARCHIVE_ACTIVE"]         = UN_CONFIG_CPC_ANALOGALARM_WST_ARCHIVE_ACTIVE;
  mTemp["POSST_ARCHIVE_ACTIVE"]       = UN_CONFIG_CPC_ANALOGALARM_POSST_ARCHIVE_ACTIVE;
  mTemp["POSST_ARCHIVE_TIME_FILTER"]  = UN_CONFIG_CPC_ANALOGALARM_POSST_ARCHIVE_TIME_FILTER;
  mTemp["SMS_CATEGORIES"]             = UN_CONFIG_CPC_ANALOGALARM_SMS_CATEGORIES;
  mTemp["SMS_MESSAGE"]                = UN_CONFIG_CPC_ANALOGALARM_SMS_MESSAGE;
  mTemp["ACK_ALARM"]                  = UN_CONFIG_CPC_ANALOGALARM_ACK_ALARM;
  mTemp["ALARM_LEVEL"]                = UN_CONFIG_CPC_ANALOGALARM_ALARM_LEVEL;
  mTemp["NORMAL_POSITION"]            = UN_CONFIG_CPC_ANALOGALARM_NORMAL_POSITION;
  mTemp["ADDRESS_STSREG01"]           = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_STSREG01;
  mTemp["ADDRESS_STSREG02"]           = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_STSREG02;
  mTemp["ADDRESS_EVSTSREG01"]         = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_EVSTSREG02"]         = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_EVSTSREG02;
  mTemp["ADDRESS_POSST"]              = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_POSST;
  mTemp["ADDRESS_HHST"]               = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_HHST;
  mTemp["ADDRESS_HST"]                = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_HST;
  mTemp["ADDRESS_LST"]                = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_LST;
  mTemp["ADDRESS_LLST"]               = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_LLST;
  mTemp["ADDRESS_MANREG01"]           = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_MANREG01;
  mTemp["ADDRESS_HH"]                 = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_HH;
  mTemp["ADDRESS_H"]                  = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_H;
  mTemp["ADDRESS_L"]                  = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_L;
  mTemp["ADDRESS_LL"]                 = UN_CONFIG_CPC_ANALOGALARM_ADDRESS_LL;
  mTemp["ADDITIONAL_LENGTH"]          = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]       = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]      = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]     = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]         = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]        = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]            = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"]    = UN_CONFIG_CPC_ANALOGALARM_ADDITIONAL_SECOND_ALIAS;
  mTemp["HHALST_ALERT_TEXT"]          = UN_CONFIG_CPC_ANALOGALARM_HHALST_ALERT_TEXT;
  mTemp["HWST_ALERT_TEXT"]            = UN_CONFIG_CPC_ANALOGALARM_HWST_ALERT_TEXT;
  mTemp["LWST_ALERT_TEXT"]            = UN_CONFIG_CPC_ANALOGALARM_LWST_ALERT_TEXT;
  mTemp["LLALST_ALERT_TEXT"]          = UN_CONFIG_CPC_ANALOGALARM_LLALST_ALERT_TEXT;
  mTemp["HHALST_ALERT_CLASS_POSTFIX"] = UN_CONFIG_CPC_ANALOGALARM_HHALST_ALERT_CLASS_POSTFIX;
  mTemp["HWST_ALERT_CLASS_POSTFIX"]   = UN_CONFIG_CPC_ANALOGALARM_HWST_ALERT_CLASS_POSTFIX;
  mTemp["LWST_ALERT_CLASS_POSTFIX"]   = UN_CONFIG_CPC_ANALOGALARM_LWST_ALERT_CLASS_POSTFIX;
  mTemp["LLALST_ALERT_CLASS_POSTFIX"] = UN_CONFIG_CPC_ANALOGALARM_LLALST_ALERT_CLASS_POSTFIX;

  g_mCpcConst["UN_CONFIG_CPC_ANALOGALARM"] = mTemp;
}









/**
Returns the list of parameter names that passed via config line.
*/
dyn_string CPC_AnalogAlarmConfig_getParamNames() {
//begin_TAG_SCRIPT_DEVICE_TYPE_getParamNames
    return makeDynString(CPC_PARAMS_ALDT);
//end_TAG_SCRIPT_DEVICE_TYPE_getParamNames
}

/** Check custom configuration
*/
CPC_AnalogAlarmConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    int alarmLevel;
    unConfigGenericFunctions_checkInt(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_ANALOGALARM_ALARM_LEVEL], alarmLevel, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) { return; }
    if (alarmLevel < 0 || alarmLevel > 3) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogAlarmConfig_checkCustomConfig: wrong alarm level: '" + configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_ANALOGALARM_ALARM_LEVEL] + "'", "");
        return;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_AnalogAlarmConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_ALARM_LEVEL, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_ANALOGALARM_ALARM_LEVEL]), exceptionInfo);
    unGenericDpFunctions_setKeyDeviceConfiguration(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_PARAMS_AA_ACK_REQUIRED, makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_ANALOGALARM_ACK_ALARM]), exceptionInfo);

    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.HHSt", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);
    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.HSt",  dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);
    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.LSt",  dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);
    fwArchive_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.LLSt", dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG], DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0, 0, exceptionInfo);

    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.HHAlSt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.LLAlSt");
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_AnalogAlarm Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_AnalogAlarmConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters, dsTemp;


  sObject = UN_CONFIG_CPC_ANALOGALARM_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.HHAlSt");
    dynRemove(dsDpParameters, dynlen(dsDpParameters));

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");

    cpcExportGenericFunctions_getCategories(sCurrentDp,
                                            makeDynString(".ProcessInput.HHAlSt",
                                                          ".ProcessInput.HWSt",
                                                          ".ProcessInput.LWSt",
                                                          ".ProcessInput.LLAlSt"),
                                            dsDpParameters,
                                            exceptionInfo);

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_CONFIG_SMS_MESSAGE, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogAlarmConfig_ExportConfig() -> Error exporting SMS key / values, due to: " + exceptionInfo[2], "");
      return;
    }

    unGenericDpFunctions_getKeyDeviceConfiguration(sCurrentDp, CPC_PARAMS_AA_ACK_REQUIRED, dsTemp, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogAlarmConfig_ExportConfig() -> Error exporting ACK required key / values, due to: " + exceptionInfo[2], "");
      return;
    }
    else
    {
      if( dynlen(dsTemp) == 0 )
      {
        cpcExportGenericFunctions_getAcknowledgeAlarm(sCurrentDp, ".ProcessInput.HHAlSt", dsDpParameters, exceptionInfo);
        if( dynlen(exceptionInfo) > 0 )
        {
          fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogAlarmConfig_ExportConfig() -> Error exporting ACK alarm, due to: " + exceptionInfo[2], "");
          return;
        }
      }
      else
      {
        dynAppend(dsDpParameters, dsTemp[1]);
      }
    }

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_CONFIG_ALARM_LEVEL, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogAlarmConfig_ExportConfig() -> Error exporting ALARM level, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getDigitalNormalPosition(sCurrentDp, sObject, makeDynString(".ProcessInput.HHAlSt"), dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg02",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg02", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "HHSt",       TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "HSt",        TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "LSt",        TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "LLSt",       TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "HH",         FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "H",          FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "L",          FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "LL",         FALSE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    makeDynString("ProcessInput.PosSt",
                                                                  "ProcessInput.HHSt",
                                                                  "ProcessInput.HSt",
                                                                  "ProcessInput.LSt",
                                                                  "ProcessInput.LLSt"),
                                                    dsDpParameters); // handling special archiving config
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogAlarmConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogAlarmConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

  }

}//  CPC_AnalogAlarmConfig_ExportConfig()





/**
Purpose: Export CPC_AnalogAlarm Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_AnalogAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_AnalogAlarm Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_AnalogAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_AnalogAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_AnalogAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
}


void IEC104_CPC_AnalogAlarm_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_AnalogAlarmConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_AnalogAlarm_ExportConfig() -> Error exporting CPC_AnalogAlarm device, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_AnalogAlarm_ExportConfig()

//@}
