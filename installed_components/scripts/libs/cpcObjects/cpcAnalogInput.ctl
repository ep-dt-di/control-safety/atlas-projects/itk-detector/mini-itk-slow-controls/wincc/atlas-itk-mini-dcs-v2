/**@file

// cpcAnalogInput.ctl
This library contains the widget, faceplate, etc. functions of AnalogInput.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of AnalogInput DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_AnalogInput_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_AnalogInput_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.PosSt", value,
          deviceName + ".ProcessInput.PosSt:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.PosSt"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.PosSt"); // formated value with unit
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_AnalogInput_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_MASK_POSST_TEXT, CPC_POPUPMENU_MANUAL_IO_ERROR_BLOCK,
                                            UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_AnalogInput_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_AnalogInput_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    dyn_string exceptionInfo;

    cpcGenericObject_GetAnalogAlarmType(deviceName + ".ProcessInput.PosSt", g_iAlertType, exceptionInfo);
    g_sPosStFormat = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_sPosStUnit = dpGetUnit(deviceName + ".ProcessInput.PosSt");
    g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_DISPLAY_FORMAT, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      exceptionInfo[2] = "Error in CPC_AnalogInput_WidgetInitStatics() with device: " + deviceName + " getting the display parameters properties: " + exceptionInfo[2];
      fwExceptionHandling_display(exceptionInfo);
      return;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of AnalogInput alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogInput_WidgetLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
}

/** Returns the list of AnalogInput DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogInput_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.HFSt:_online.._invalid");

    if (g_iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_text");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogInput_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    //TODO: work on me
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    float fPosSt = values[9];
    bool bPosStInvalid = values[10];
    bool bHFStInvalid = values[11];

    bool isAlarmActive, isUnAck;
    string actText;
    if (g_iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL) {
        isAlarmActive = values[12];
        actText = values[13];
        isUnAck = values[14];
    }

    string formattedValue;
    string warningLetter, warningColor, alarmLetter, alarmColor, controlLetter, controlColor, body1Text, body1Color, selectColor;
    bool lockVisible;

    body1Color = "cpcColor_Widget_Status";					// Auto mode (default)
    controlLetter = "";
    if (!bStsReg01Invalid) {
        if (getBit(bit32StsReg01, CPC_StsReg01_FOMOST) == 1) {
            body1Color = "unWidget_ControlStateForced";				// Forced mode
            controlLetter = CPC_WIDGET_TEXT_CONTROL_FORCED;
            controlColor = "unWidget_ControlStateForced";
        } else if (getBit(bit32StsReg01, CPC_StsReg01_AUIHFOMOST) == 1) {	// Forced mode inhibited
            controlLetter = CPC_WIDGET_TEXT_CONTROL_AUIHFOMOST;
        }
    }

    if (g_iAlertType != DPCONFIG_ALERT_NONBINARYSIGNAL) {
        alarmLetter = CPC_WIDGET_TEXT_ALARM_DOES_NOT_EXIST;
        alarmColor = "unAlarmDoesNotExist";
    } else if (!isAlarmActive) {
        alarmLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
        alarmColor = "unAlarmMasked";
    } else {
        if (isUnAck) {
            if (actText == "HH" || actText == "LL") { alarmLetter = actText; }
            alarmColor = "unWidget_AlarmNotAck";
        } else {
            if (actText == "H" || actText == "L") {
                alarmLetter = actText;
                alarmColor = "cpcColor_Widget_Warning";
            } else if (actText == "HH" || actText == "LL") {
                alarmLetter = actText;
                alarmColor = "unFaceplate_AlarmActive";
            }
        }
        if (alarmColor != "") {
            body1Color = alarmColor;
        }
    }

    if (bPosStInvalid) {
        body1Color = "unDataNotValid";
    }

    if (!bStsReg01Invalid) {
        unGenericObject_WidgetWarningAnimation(bit32StsReg01,
                                               makeDynInt(CPC_StsReg01_FODIPRO, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW, CPC_StsReg01_MIOERBRST),
                                               makeDynString(CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR, CPC_WIDGET_TEXT_WARNING_ALARM_BLOCKED),
                                               warningLetter, warningColor);
    }

    formattedValue = unGenericObject_FormatValue(g_sPosStFormat, fPosSt);
    if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "HEX") {
        sprintf(formattedValue, "%X", (int)formattedValue);
        formattedValue = "0x" + formattedValue;
    } else if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "hex") {
        sprintf(formattedValue, "%x", (int)formattedValue);
        formattedValue = "0x" + formattedValue;
    }
    body1Text = formattedValue + " " + g_sPosStUnit;

    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, lockVisible);
    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue, bStsReg01Invalid || bPosStInvalid || bHFStInvalid, warningLetter, warningColor);

    if (g_bSystemConnected)
        setMultiValue("Body1", "text", body1Text, "Body1", "foreCol", body1Color,
                      "WarningText", "text", warningLetter, "WarningText", "foreCol", warningColor,
                      "AlarmText", "text", alarmLetter, "AlarmText", "foreCol", alarmColor,
                      "ControlStateText", "text", controlLetter, "ControlStateText", "foreCol", controlColor,
                      "SelectArea", "foreCol", selectColor, "LockBmp", "visible", lockVisible,
                      "WidgetArea", "visible", true, "eventState", "visible", iMask == 0,
                      "commentsButton", "visible", isCommentsActive);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

*/
CPC_AnalogInput_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    setMultiValue("Body1", "foreCol", "unDataNoAccess",
                  "WarningText", "text", "", "AlarmText", "text", "", "ControlStateText", "text", "",
                  "SelectArea", "foreCol", "", "LockBmp", "visible", false, "WidgetArea", "visible", false,
                  "eventState", "visible", false);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_AnalogInput_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    dyn_string exceptionInfo;

    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_AUTO_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_SET_VALUE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MASK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_FORCED_MODE] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[CPC_FACEPLACE_BUTTON_BLOCK_IOERROR] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_AnalogInput_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_AnalogInput_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];

    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_FORCED_MODE:
                bit1 = !(getBit(stsReg01Value, CPC_StsReg01_FOMOST) == 0) && !bStsReg01Bad;
                bit2 = !(getBit(stsReg01Value, CPC_StsReg01_AUINHFMO) == 0) && !bStsReg01Bad;
                buttonEnabled = buttonEnabled && bSelected && !(bit1 || bit2);
                break;
            case CPC_FACEPLACE_BUTTON_BLOCK_IOERROR:
                buttonEnabled = (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_FORCED_MODE) > 0) && bSelected && !bStsReg01Bad; // same access rights as "Forced mode" button
                break;
            case UN_FACEPLATE_BUTTON_AUTO_MODE:
            case UN_FACEPLATE_BUTTON_SET_VALUE:
                bit1 = !(getBit(stsReg01Value, CPC_StsReg01_AUMOST) == 0) && !bStsReg01Bad;
                buttonEnabled = buttonEnabled && bSelected && !(bit1);
                break;
            case UN_FACEPLATE_BUTTON_ACK_ALARM:
                buttonEnabled = buttonEnabled && bSelected && cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(deviceName, exceptionInfo) == "TRUE";
                break;
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_AnalogInput_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    int iAlertType, iRangeType;
    dyn_string exceptionInfo;

    g_mParameters["AlarmRanges"] = cpcGenericObject_GetAnalogAlarmType(deviceName + ".ProcessInput.PosSt", iAlertType, exceptionInfo);
    g_mParameters["BoolAlert"] = (iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL);
    g_mParameters["rangeClasses"] = makeDynString();

    if (g_mParameters["BoolAlert"]) {
        int rangesNumber;
        dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl.._num_ranges", rangesNumber);
        for (int i = 1; i <= rangesNumber; i++) {
            string rangeClass = "#";
            dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._class", rangeClass);
            dynAppend(g_mParameters["rangeClasses"], rangeClass);
        }
    }

    int iRes = dpGet(deviceName + ".ProcessInput.PosSt:_pv_range.._type", iRangeType);
    if ((iRes >= 0) && (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)) {
        g_mParameters["RangeType"] = DPCONFIG_MINMAX_PVSS_RANGECHECK;
    } else {
        g_mParameters["RangeType"] = DPCONFIG_NONE;
    }

    g_mParameters["PosStFormat"] = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_mParameters["PosStUnit"] = dpGetUnit(deviceName + ".ProcessInput.PosSt");
    g_mParameters["HFStFormat"] = dpGetFormat(deviceName + ".ProcessInput.HFSt");
    g_mParameters["HFStUnit"] = dpGetUnit(deviceName + ".ProcessInput.HFSt");
    g_mParameters["AlAck"] = cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(deviceName, exceptionInfo) == "TRUE";
    g_mParameters["MailSMS"] = cpcGenericObject_isInMailSMS(deviceName + ".ProcessInput.PosSt");
    g_mParameters["FirstOrderFilter"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_CONFIG_FIRST_ORDER_FILTER, exceptionInfo, "0");
	g_mParameters["MessageConversion"] = "none";
	bool configExists;
    int conversionType, order;
    dyn_float arguments;
	fwConfigConversion_get(deviceName + ".ProcessInput.PosSt", configExists, DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN, conversionType, order, arguments, exceptionInfo);
	if(configExists) {
		if(order==1) {

			g_mParameters["MessageConversion"] = "y = " + arguments[2] + "x";
			if(arguments[1]!=0) {
				g_mParameters["MessageConversion"] += " + " + arguments[1];
			}
        }
	}
    dynClear(exceptionInfo);
    g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_DISPLAY_FORMAT, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      exceptionInfo[2] = "Error in CPC_AnalogInput_FaceplateInitStatics() with device: " + deviceName + " getting the display parameters properties: " + exceptionInfo[2];
      fwExceptionHandling_display(exceptionInfo);
      return;
    }

//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of AnalogInput alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogInput_FaceplateLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
}

/** Returns the list of AnalogInput DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogInput_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.HFSt");
    dynAppend(dpes, deviceName + ".ProcessInput.HFSt:_online.._invalid");

    if (g_mParameters["RangeType"] == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_pv_range.._min");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_pv_range.._max");
    }

    if (g_mParameters["BoolAlert"]) {
        mappingRemove(g_mParameters, "alertPositions_HH");
        mappingRemove(g_mParameters, "alertPositions_H");
        mappingRemove(g_mParameters, "alertPositions_L");
        mappingRemove(g_mParameters, "alertPositions_LL");

        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_range");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
        for (int i = 1; i <= dynlen(g_mParameters["rangeClasses"]); i++) {
            string rangeClass = g_mParameters["rangeClasses"][i];
            if (strpos(rangeClass, "_cpcAnalogHH") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._l_limit");
                g_mParameters["alertPositions_HH"] = i;
            } else if (strpos(rangeClass, "_cpcAnalogH") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._l_limit");
                g_mParameters["alertPositions_H"] = i;
            } else if (strpos(rangeClass, "_cpcAnalogLL") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._u_limit");
                g_mParameters["alertPositions_LL"] = i;
            } else if (strpos(rangeClass, "_cpcAnalogL") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._u_limit");
                g_mParameters["alertPositions_L"] = i;
            }
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogInput_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    string sPosSt;
    if (mappingHasKey(g_mParameters, "PosStUnit") && mappingHasKey(g_mParameters, "PosStFormat")) {
    unFaceplate_animateOnlineValue(dpes, values, "PosSt", g_mParameters["PosStUnit"], g_mParameters["PosStFormat"], "cpcColor_Faceplate_Status");
        if (mappingHasKey(g_mParameters, "RangeType")) {
            unFaceplate_animateRange(dpes, values, "PosSt", g_mParameters["RangeType"], g_mParameters["PosStUnit"], g_mParameters["PosStFormat"]);
        }
    }
    getValue("PosSt.display", "text", sPosSt);
    if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "HEX") {
        sprintf(sPosSt, "%X", (int)sPosSt);
        sPosSt = "0x" + sPosSt;
        setValue("PosSt.display", "text", sPosSt);
    } else if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "hex") {
        sprintf(sPosSt, "%x", (int)sPosSt);
        sPosSt = "0x" + sPosSt;
        setValue("PosSt.display", "text", sPosSt);
    }

    if (unGenericObject_shapeExists("FirstOrderFilter") && mappingHasKey(g_mParameters, "FirstOrderFilter")) {
        setValue("FirstOrderFilter.text", "text", g_mParameters["FirstOrderFilter"] == "0" ? "disabled" : g_mParameters["FirstOrderFilter"] + " s.");
    }
	if (unGenericObject_shapeExists("MessageConversion") && mappingHasKey(g_mParameters, "MessageConversion")) {
        setValue("MessageConversion.display", "text", g_mParameters["MessageConversion"]);
    }
    if (mappingHasKey(g_mParameters, "HFStUnit") && mappingHasKey(g_mParameters, "HFStFormat")) {
    unFaceplate_animateOnlineValue(dpes, values, "HFSt", g_mParameters["HFStUnit"], g_mParameters["HFStFormat"], "cpcColor_Faceplate_Status");
    }

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUIHFOMOST", true, "cpcColor_Faceplate_Status");

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FODIPRO", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MIOERBRST", true, "cpcColor_Faceplate_Warning");
    cpcGenericObject_animateMaskEvent(dpes, values);

    if (dynlen(dpes) == 0) {
        unGenericObject_ColorBoxDisconnect(makeDynString("Alarm_HH", "Alarm_H", "Alarm_UnAck", "Alarm_LL", "Alarm_L"));
        unGenericObject_DisplayValueDisconnect(makeDynString("ValueHH", "ValueH", "ValueL", "ValueLL"));
    } else if (mappingHasKey(g_mParameters, "BoolAlert") && !g_mParameters["BoolAlert"]) { // if does not exist
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_HH", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateWarning("Alarm_H", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateWarning("Alarm_L", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_LL", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_UnAck", 0, false, false, false);
        cpcGenericObject_CheckboxAnimate("MailSMS", false, false);
        if (g_bSystemConnected)
            setMultiValue("ValueHH.display", "text", "", "ValueHH.display", "foreCol", "unAlarmDoesNotExist",
                          "ValueH.display", "text", "", "ValueH.display", "foreCol", "unAlarmDoesNotExist",
                          "ValueL.display", "text", "", "ValueL.display", "foreCol", "unAlarmDoesNotExist",
                          "ValueLL.display", "text", "", "ValueLL.display", "foreCol", "unAlarmDoesNotExist");
    } else { // animate alarms
        bool ackPossible = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._ack_possible");
        bool alarmActive = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._active");

        cpcGenericObject_CheckboxAnimate("MailSMS", mappingHasKey(g_mParameters, "MailSMS") && g_mParameters["MailSMS"], alarmActive);
        cpcGenericObject_ColorBoxAnimate("Alarm_UnAck", ackPossible, alarmActive && mappingHasKey(g_mParameters, "AlAck") && g_mParameters["AlAck"], "cpcColor_Alarm_Bad", false);

        float alarmRange;
        int alarmState;
        if (mappingHasKey(g_mParameters, "alertPositions_HH")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_HH"] + "._l_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_HH"] + "._act_state");
            if(mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
            unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueHH", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_HH", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueHH.display", "text", "", "ValueHH.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_HH", 0, false, false, false);
        }
        if (mappingHasKey(g_mParameters, "alertPositions_LL")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_LL"] + "._u_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_LL"] + "._act_state");
            if(mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
            unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueLL", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_LL", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueLL.display", "text", "", "ValueLL.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_LL", 0, false, false, false);
        }
        if (mappingHasKey(g_mParameters, "alertPositions_H")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_H"] + "._l_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_H"] + "._act_state");
            if(mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
            unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueH", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_H", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueH.display", "text", "", "ValueH.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_H", 0, false, false, false);
        }
        if (mappingHasKey(g_mParameters, "alertPositions_L")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_L"] + "._u_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_L"] + "._act_state");
            if(mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
            unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueL", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_L", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueL.display", "text", "", "ValueL.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_L", 0, false, false, false);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}
