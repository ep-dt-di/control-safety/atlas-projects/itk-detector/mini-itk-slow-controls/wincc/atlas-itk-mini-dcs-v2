/**@file

// cpcAnalogInputConfig.ctl
This library contains the import and export function of the CPC_AnalogInput.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_ANALOGINPUT_DPT_NAME = "CPC_AnalogInput";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_ANALOGINPUT_LENGTH = 22;

const unsigned UN_CONFIG_CPC_ANALOGINPUT_UNIT                       = 1;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_FORMAT                     = 2;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_RANGEMAX                   = 3;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_RANGEMIN                   = 4;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_HHLIMIT                    = 5;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_HLIMIT                     = 6;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_LLIMIT                     = 7;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_LLLIMIT                    = 8;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ALARM_ACTIVE               = 9;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_DEADBAND                   = 10;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_DEADBAND_TYPE              = 11;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ARCHIVE_ACTIVE             = 12;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ARCHIVE_TIME_FILTER        = 13;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_SMS_CATEGORIES             = 14;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_SMS_MESSAGE                = 15;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ACK_ALARM                  = 16;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_STSREG01           = 17;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_EVSTSREG01         = 18;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_POSST              = 19;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_HFST               = 20;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_MANREG01           = 21;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_MPOSR              = 22;

const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_SECOND_ALIAS    = 7;

// SOFT_FE constants
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_UNIT                  = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_FORMAT                = 2;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_RANGEMAX              = 3;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_RANGEMIN              = 4;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_HHLIMIT               = 5;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_HLIMIT                = 6;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_LLIMIT                = 7;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_LLLIMIT               = 8;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ALARM_ACTIVE          = 9;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ARCHIVE_ACTIVE        = 10;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ARCHIVE_TIME_FILTER   = 11;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_SMS_CATEGORIES        = 12;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_SMS_MESSAGE           = 13;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ACK_ALARM             = 14;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_LENGTH                = 14;

const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ADDITIONAL_LENGTH     = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ADDITIONAL_MASKEVENT  = 1;

//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_AnalogInputConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"]            = ".ProcessInput.StsReg01";
    props["dataType"]           = CPC_UINT16;
    config["StsReg01"]          = props;

    mappingClear(props);
    props["hasArchive"]         = true;
    props["address"]            = ".ProcessInput.evStsReg01";
    props["dataType"]           = CPC_INT32;
    config["evStsReg01"]        = props;

    mappingClear(props);
    props["5RangesAlerts"]      = true;
    props["hasUnit"]            = true;
    props["hasFormat"]          = true;
    props["hasArchive"]         = true;
    props["hasPvRange"]         = true;
    props["hasSmooth"]          = true;
    props["address"]            = ".ProcessInput.PosSt";
    props["dataType"]           = CPC_FLOAT;
    config["PosSt"]             = props;

    mappingClear(props);
    props["hasUnit"]            = true;
    props["hasFormat"]          = true;
    props["hasSmooth"]          = true;
    props["address"]            = ".ProcessInput.HFSt";
    props["dataType"]           = CPC_FLOAT;
    config["HFSt"]    = props;

    mappingClear(props);
    props["address"]            = ".ProcessOutput.ManReg01";
    props["dataType"]           = CPC_UINT16;
    config["ManReg01"]          = props;

    mappingClear(props);
    props["address"]            = ".ProcessOutput.MPosR";
    props["dataType"]           = CPC_FLOAT;
    config["MPosR"]             = props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_AnalogInputConfig_initializeConstants()
{
  mapping mTemp;


 mTemp["DPT_NAME"]                = UN_CONFIG_CPC_ANALOGINPUT_DPT_NAME;
 mTemp["LENGTH"]                  = UN_CONFIG_CPC_ANALOGINPUT_LENGTH;
 mTemp["UNIT"]                    = UN_CONFIG_CPC_ANALOGINPUT_UNIT;
 mTemp["FORMAT"]                  = UN_CONFIG_CPC_ANALOGINPUT_FORMAT;
 mTemp["RANGEMAX"]                = UN_CONFIG_CPC_ANALOGINPUT_RANGEMAX;
 mTemp["RANGEMIN"]                = UN_CONFIG_CPC_ANALOGINPUT_RANGEMIN;
 mTemp["HHLIMIT"]                 = UN_CONFIG_CPC_ANALOGINPUT_HHLIMIT;
 mTemp["HLIMIT"]                  = UN_CONFIG_CPC_ANALOGINPUT_HLIMIT;
 mTemp["LLIMIT"]                  = UN_CONFIG_CPC_ANALOGINPUT_LLIMIT;
 mTemp["LLLIMIT"]                 = UN_CONFIG_CPC_ANALOGINPUT_LLLIMIT;
 mTemp["ALARM_ACTIVE"]            = UN_CONFIG_CPC_ANALOGINPUT_ALARM_ACTIVE;
 mTemp["DEADBAND"]                = UN_CONFIG_CPC_ANALOGINPUT_DEADBAND;
 mTemp["DEADBAND_TYPE"]           = UN_CONFIG_CPC_ANALOGINPUT_DEADBAND_TYPE;
 mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_ANALOGINPUT_ARCHIVE_ACTIVE;
 mTemp["ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_ANALOGINPUT_ARCHIVE_TIME_FILTER;
 mTemp["SMS_CATEGORIES"]          = UN_CONFIG_CPC_ANALOGINPUT_SMS_CATEGORIES;
 mTemp["SMS_MESSAGE"]             = UN_CONFIG_CPC_ANALOGINPUT_SMS_MESSAGE;
 mTemp["ACK_ALARM"]               = UN_CONFIG_CPC_ANALOGINPUT_ACK_ALARM;
 mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_STSREG01;
 mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_EVSTSREG01;
 mTemp["ADDRESS_POSST"]           = UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_POSST;
 mTemp["ADDRESS_HFST"]            = UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_HFST;
 mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_MANREG01;
 mTemp["ADDRESS_MPOSR"]           = UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_MPOSR;
 mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_LENGTH;
 mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_MASKEVENT;
 mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_PARAMETERS;
 mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_MASTER_NAME;
 mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_PARENTS;
 mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_CHILDREN;
 mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_TYPE;
 mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_ANALOGINPUT_ADDITIONAL_SECOND_ALIAS;

 g_mCpcConst["UN_CONFIG_CPC_ANALOGINPUT"] = mTemp;


 mappingClear(mTemp);
  mTemp["UNIT"]                 = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_UNIT;
  mTemp["FORMAT"]               = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_FORMAT;
  mTemp["RANGEMAX"]             = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_RANGEMAX;
  mTemp["RANGEMIN"]             = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_RANGEMIN;
  mTemp["HHLIMIT"]              = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_HHLIMIT;
  mTemp["HLIMIT"]               = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_HLIMIT;
  mTemp["LLIMIT"]               = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_LLIMIT;
  mTemp["LLLIMIT"]              = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_LLLIMIT;
  mTemp["ALARM_ACTIVE"]         = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ALARM_ACTIVE;
  mTemp["ARCHIVE_ACTIVE"]       = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]  = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ARCHIVE_TIME_FILTER;
  mTemp["SMS_CATEGORIES"]       = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_SMS_CATEGORIES;
  mTemp["SMS_MESSAGE"]          = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_SMS_MESSAGE;
  mTemp["ACK_ALARM"]            = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ACK_ALARM;
  mTemp["LENGTH"]               = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_LENGTH;
  mTemp["ADDITIONAL_LENGTH"]    = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"] = UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT_ADDITIONAL_MASKEVENT;

  g_mCpcConst["UN_CONFIG_SOFT_FE_CPC_ANALOGINPUT"] = mTemp;
}




/**
Purpose: Export CPC_AnalogInput Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_AnalogInputConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_ANALOGINPUT_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp,   dsDpParameters, ".ProcessInput.PosSt");
    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");
    cpcExportGenericFunctions_getRange(sCurrentDp,  dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInputConfig_ExportConfig() -> Error exporting ranges, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_get5AlertLimits(sCurrentDp, dsDpParameters);
    cpcExportGenericFunctions_getAlarmActive(sCurrentDp, dsDpParameters);
    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInputConfig_ExportConfig() -> Error exporting deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);
    cpcExportGenericFunctions_getCategories(sCurrentDp, makeDynString(".ProcessInput.PosSt"), dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInputConfig_ExportConfig() -> Error exporting PosSt categories, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getKeyDeviceConfiguration(sCurrentDp, dsDpParameters, CPC_CONFIG_SMS_MESSAGE, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInputConfig_ExportConfig() -> Error exporting SMS key / value, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_get5AlertAcknowledgeAlarm(sCurrentDp, ".ProcessInput.PosSt", dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInputConfig_ExportConfig() -> Error exporting 5 alert acknowledge alarms, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "HFSt",       TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosR",      FALSE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInputConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInputConfig_ExportConfig() -> Error exporting metainformation, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_AnalogInputConfig_ExportConfig()





/**
Purpose: Export CPC_AnalogInput Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_AnalogInput Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

CMW_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

SNMP_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
}

DIP_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
}





void IEC104_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_AnalogInputConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_AnalogInput_ExportConfig() -> Error exporting CPC_AnalogInput devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_AnalogInput_ExportConfig()





/** Check custom configuration
*/
CPC_AnalogInputConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    mapping mParameters = cpcConfigGenericFunctions_getParametersMapping(configLine, UN_CONFIG_CPC_ANALOGINPUT_DPT_NAME, hasArchive);
    string sConversion = mappingHasKey(mParameters, CPC_CONFIG_CONVERSION) ? mParameters[CPC_CONFIG_CONVERSION] : "";

    cpcConfigGenericFunctions_checkMsgConv(sConversion, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
CPC_AnalogInputConfig_setCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
    string sDPName = configLine[UN_CONFIG_OBJECTGENERAL_DPNAME];
    if (configLine[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_ANALOGINPUT_ADDRESS_STSREG01] == "") {
        if(dpSetWait(sDPName + ".ProcessInput.StsReg01", pow(2, CPC_StsReg01_AUMOST) + pow(2, CPC_StsReg01_AUINHFMO)) !=0 ) {
            fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogInput_setCustomConfig: error in StsReg01 dpSet " + getCatStr("unGeneration", "BADINPUTS"), "");
        }
    }

    // message conversion
    string sConversion = cpcGenericDpFunctions_getDeviceProperty(configLine[UN_CONFIG_OBJECTGENERAL_DPNAME], CPC_CONFIG_CONVERSION, exceptionInfo, "");

    cpcConfigGenericFunctions_setMsgConv(sConversion, makeDynString(sDPName + ".ProcessInput.PosSt", sDPName + ".ProcessInput.HFSt"), exceptionInfo);

//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

void SOFT_FE_CPC_AnalogInput_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    string sObject = UN_CONFIG_CPC_ANALOGINPUT_DPT_NAME;
    int amount = dynlen(dsDpList);
    for (int i = 1; i <= amount; i++) {
        dyn_string dsDpParameters;
        string currentDP = dsDpList[i];

        if (!dpExists(currentDP)) { continue; }

        unExportDevice_getAllCommonParameters(currentDP, sObject, dsDpParameters);
        cpcExportGenericFunctions_getUnit(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getFormat(currentDP, dsDpParameters, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_getRange(currentDP, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
        cpcExportGenericFunctions_get5AlertLimits(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getAlarmActive(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getArchive(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getCategories(currentDP, makeDynString(".ProcessInput.PosSt"), dsDpParameters, exceptionInfo);
        cpcExportGenericFunctions_getKeyDeviceConfiguration(currentDP, dsDpParameters, CPC_CONFIG_SMS_MESSAGE, exceptionInfo);
        cpcExportGenericFunctions_get5AlertAcknowledgeAlarm(currentDP, ".ProcessInput.PosSt", dsDpParameters, exceptionInfo);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_1, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),   dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_2, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_3, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),  dsDpParameters);
        cpcExportGenericFunctions_getMaskEvent(currentDP, dsDpParameters);

        // update msg_conv parameter before export
        cpcExportGenericFunctions_updateConversionParameters(currentDP, ".ProcessInput.PosSt", exceptionInfo);

        cpcExportGenericFunctions_getParameters(currentDP, dsDpParameters, exceptionInfo);
        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions


//@}
