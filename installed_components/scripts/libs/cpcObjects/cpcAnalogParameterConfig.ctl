/**@file

// cpcAnalogParameterConfig.ctl
This library contains the import and export function of the CPC_AnalogParameter.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_ANALOGPARAMETER_DPT_NAME = "CPC_AnalogParameter";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_LENGTH					= 15;

const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_UNIT					= 1;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_FORMAT					= 2;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_DEADBAND				= 3;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_DEADBAND_TYPE			= 4;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_RANGEMAX				= 5;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_RANGEMIN				= 6;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ARCHIVE_ACTIVE			= 7;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ARCHIVE_TIME_FILTER	= 8;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_MPOSRST		= 9;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_POSST			= 10;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_MPOSR			= 11;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_STSREG01		= 12;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_EVSTSREG01		= 13;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_MANREG01		= 14;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_DEFAULT_VALUE			= 15;

const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_SECOND_ALIAS    = 7;

// SOFT_FE constants
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_UNIT                  = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_FORMAT                = 2;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_RANGEMAX              = 3;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_RANGEMIN              = 4;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ARCHIVE_ACTIVE        = 5;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ARCHIVE_TIME_FILTER   = 6;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_DEFAULT_VALUE         = 7;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_AUTONOMOUS            = 8;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_LENGTH                = 8;

const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ADDITIONAL_LENGTH     = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ADDITIONAL_MASKEVENT  = 1;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_AnalogParameterConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg01";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg01"] 		= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg01";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg01"] 	= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["address"] 		= ".ProcessInput.MPosRSt";
    props["dataType"] 		= CPC_FLOAT;
    config["MPosRSt"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dataType"] 		= CPC_FLOAT;
    config["PosSt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg01";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg01"] 		= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessOutput.MPosR";
    props["dataType"] 		= CPC_FLOAT;
    config["MPosR"] 		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_AnalogParameterConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_ANALOGPARAMETER_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_ANALOGPARAMETER_LENGTH;
  mTemp["UNIT"]                    = UN_CONFIG_CPC_ANALOGPARAMETER_UNIT;
  mTemp["FORMAT"]                  = UN_CONFIG_CPC_ANALOGPARAMETER_FORMAT;
  mTemp["DEADBAND"]                = UN_CONFIG_CPC_ANALOGPARAMETER_DEADBAND;
  mTemp["DEADBAND_TYPE"]           = UN_CONFIG_CPC_ANALOGPARAMETER_DEADBAND_TYPE;
  mTemp["RANGEMAX"]                = UN_CONFIG_CPC_ANALOGPARAMETER_RANGEMAX;
  mTemp["RANGEMIN"]                = UN_CONFIG_CPC_ANALOGPARAMETER_RANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_ANALOGPARAMETER_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_ANALOGPARAMETER_ARCHIVE_TIME_FILTER;
  mTemp["ADDRESS_MPOSRST"]         = UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_MPOSRST;
  mTemp["ADDRESS_POSST"]           = UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_POSST;
  mTemp["ADDRESS_MPOSR"]           = UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_MPOSR;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_STSREG01;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_ANALOGPARAMETER_ADDRESS_MANREG01;
  mTemp["DEFAULT_VALUE"]           = UN_CONFIG_CPC_ANALOGPARAMETER_DEFAULT_VALUE;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_ANALOGPARAMETER_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_ANALOGPARAMETER"] = mTemp;


  mappingClear(mTemp);
  mTemp["UNIT"]                 = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_UNIT;
  mTemp["FORMAT"]               = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_FORMAT;
  mTemp["RANGEMAX"]             = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_RANGEMAX;
  mTemp["RANGEMIN"]             = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_RANGEMIN;
  mTemp["ARCHIVE_ACTIVE"]       = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]  = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ARCHIVE_TIME_FILTER;
  mTemp["DEFAULT_VALUE"]        = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_DEFAULT_VALUE;
  mTemp["AUTONOMOUS"]           = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_AUTONOMOUS;
  mTemp["LENGTH"]               = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_LENGTH;
  mTemp["ADDITIONAL_LENGTH"]    = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"] = UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_ADDITIONAL_MASKEVENT;

  g_mCpcConst["UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER"] = mTemp;
}




/** Check custom configuration
*/
CPC_AnalogParameterConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    float fValue, fMax, fMin;
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_ANALOGPARAMETER_DEFAULT_VALUE], fValue, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_ANALOGPARAMETER_RANGEMIN], fMin, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_ANALOGPARAMETER_RANGEMAX], fMax, exceptionInfo);
    if ((fValue < fMin) || (fValue > fMax)) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogParameterConfig_checkCommonConfig: " + getCatStr("unCPCGeneration", "BADRANGE"), "");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_AnalogParameterConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    unConfigGenericFunctions_setUnit(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_ANALOGPARAMETER_UNIT], exceptionInfo);
    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_ANALOGPARAMETER_FORMAT], exceptionInfo);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_ANALOGPARAMETER_DEFAULT_VALUE]);
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_AnalogParameter Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_AnalogParameterConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo, bool bExportOnlineValuesAsDefault = FALSE)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_ANALOGPARAMETER_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters);
    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters);
    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogParameterConfig_ExportConfig() -> Error exporting deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogParameterConfig_ExportConfig() -> Error exporting ranges, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosR",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);

    if( bExportOnlineValuesAsDefault )
    {
      // Replace default values with current online values
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.PosSt", dsDpParameters);
    }
    else
    {
      // Use the regular default values
      cpcExportGenericFunctions_getDefaultValue(sCurrentDp, dsDpParameters);
    }

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogParameterConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogParameterConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_AnalogParameterConfig_ExportConfig()





/**
Purpose: Export CPC_AnalogParameter Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_AnalogParameter Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

DIP_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

SNMP_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}


void IEC104_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_AnalogParameter_ExportConfig() -> Error exporting CPC_AnalogParameter devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_AnalogParameter_ExportConfig()





/**
Purpose: Export CPC_AnalogParameter Devices and override the default parameters with the online values
         These are just simple wrappers to indicate that the device supports the feature

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_AnalogParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

_UnPlc_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_AnalogParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

OPCUA_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_AnalogParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
   CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

BACnet_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_AnalogParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
   CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

DIP_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_AnalogParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

SNMP_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_AnalogParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

void IEC104_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  // Call CPC_AnalogParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = TRUE
  CPC_AnalogParameterConfig_ExportConfig(dsDpList, exceptionInfo, TRUE);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_AnalogParameter_ExportConfigOnlineValues() -> Error exporting AnalogParameter, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_AnalogParameter_ExportConfigOnlineValues()






/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
CPC_SOFT_FE_AnalogParameterConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
    float fValue, fMax, fMin;
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_DEFAULT_VALUE], fValue, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_RANGEMIN], fMin, exceptionInfo);
    unConfigGenericFunctions_checkFloat(configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_RANGEMAX], fMax, exceptionInfo);
    if ((fValue < fMin) || (fValue > fMax)) {
        fwException_raise(exceptionInfo, "ERROR", "CPC_AnalogParameterConfig_checkCommonConfig: " + getCatStr("unCPCGeneration", "BADRANGE"), "");
        return;
    }
    bool val;
    cpcConfigGenericFunctions_checkBool("AUTONOMOUS", configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_AUTONOMOUS], val, exceptionInfo);
}

CPC_SOFT_FE_AnalogParameterConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
    unConfigGenericFunctions_setUnit(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_UNIT], exceptionInfo);
    unConfigGenericFunctions_setFormat(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_FORMAT], exceptionInfo);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_DEFAULT_VALUE]);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt",    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_DEFAULT_VALUE]);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.MPosRSt",  dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_DEFAULT_VALUE]);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.MPosR",    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_DEFAULT_VALUE]);

    bool autonomous;
    cpcConfigGenericFunctions_checkBool("AUTONOMOUS", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_ANALOGPARAMETER_AUTONOMOUS], autonomous, exceptionInfo);
    if (autonomous) {
        unGenericDpFunctions_createDpeDpFunction(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt");
        unGenericDpFunctions_createDpeDpFunction(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.MPosRSt");
        unGenericDpFunctions_addDpeinDpFunction(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt", dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.MPosR:_original.._value");
        unGenericDpFunctions_addDpeinDpFunction(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.MPosRSt", dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.MPosR:_original.._value");
    }
}

/**
Purpose: Wrapper for SOFT_FE_CPC_AnalogParameter_ExportConfig to override default values with online values
*/
void SOFT_FE_CPC_AnalogParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  // Call SOFT_FE_CPC_AnalogParameter_ExportConfig with bExportOnlineValuesAsDefault = true
  SOFT_FE_CPC_AnalogParameter_ExportConfig(dsDpList, exceptionInfo, true);
}

void SOFT_FE_CPC_AnalogParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo, bool bExportOnlineValuesAsDefault = false) {
    string sObject = UN_CONFIG_CPC_ANALOGPARAMETER_DPT_NAME;
    int amount = dynlen(dsDpList);
    for (int i = 1; i <= amount; i++) {
        dyn_string dsDpParameters;
        string currentDP = dsDpList[i];

        if (!dpExists(currentDP)) { continue; }

        unExportDevice_getAllCommonParameters(currentDP, sObject, dsDpParameters);
        cpcExportGenericFunctions_getUnit(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getFormat(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getRange(currentDP, dsDpParameters, exceptionInfo);
        cpcExportGenericFunctions_getArchive(currentDP, dsDpParameters);

        if(bExportOnlineValuesAsDefault) {// replace default values with current online values
          cpcExportGenericFunctions_getDPEValue(currentDP, ".ProcessInput.PosSt", dsDpParameters);
        }
        else { // use the regular default values
          cpcExportGenericFunctions_getDefaultValue(currentDP, dsDpParameters);
        }
        cpcExportGenericFunctions_getAutonomous(currentDP, dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_1, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_2, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_3, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT), dsDpParameters);
        cpcExportGenericFunctions_getMaskEvent(currentDP, dsDpParameters);

        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions


//@}
