/**@file

// cpcAnalogStatus.ctl
This library contains the widget, faceplate, etc. functions of AnalogStatus.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_AnalogStatus_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;
    bool bBad;

    dpGet(deviceName + ".ProcessInput.PosSt", value,
          deviceName + ".ProcessInput.PosSt:_online.._stime", sTime,
          deviceName + ".ProcessInput.PosSt:_online.._bad", bBad);

    dsReturnData[1] = sTime;
    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.PosSt"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.PosSt"); // formated value with unit
    dsReturnData[3] = bBad;
    dsReturnData[4] = "OK";
    dsReturnData[5] = "unEventList_ForeColor";
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_AnalogStatus_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_MASK_POSST_TEXT, UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, CPC_POPUPMENU_DIAG_INFO_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

CPC_AnalogStatus_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_AnalogStatus_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_AnalogStatus_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    dyn_string exceptionInfo;
    string sDp = deviceName + ".ProcessInput.PosSt";

    cpcGenericObject_GetAnalogAlarmType(sDp, g_iAlertType, exceptionInfo);
    g_sPosStFormat = dpGetFormat(sDp);
    g_sPosStUnit = dpGetUnit(sDp);
    g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_DISPLAY_FORMAT, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      exceptionInfo[2] = "Error in CPC_AnalogStatus_WidgetInitStatics() with device: " + deviceName + " getting the display parameters properties: " + exceptionInfo[2];
      fwExceptionHandling_display(exceptionInfo);
      return;
    }

//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of AnalogStatus alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogStatus_WidgetLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
}

/** Returns the list of AnalogStatus DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogStatus_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    if (g_iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_text");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogStatus_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];

    float fPosSt = values[6];
    bool bPosStInvalid = values[7];

    bool isAlarmActive, isUnAck;
    string actText;
    if (g_iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL) {
        isAlarmActive = values[8];
        actText = values[9];
        isUnAck = values[10];
    }

    string sBody1Text, sBody1Color;
    string sWarningLetter, sWarningColor;

    sBody1Text = cpcGenericObject_FormatValueWithUnit(fPosSt, g_sPosStFormat, g_sPosStUnit);
    if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "HEX") {
        sprintf(sBody1Text, "%X", (int)sBody1Text);
        sBody1Text = "0x" + sBody1Text;
    } else if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "hex") {
        sprintf(sBody1Text, "%x", (int)sBody1Text);
        sBody1Text = "0x" + sBody1Text;
    }
    sBody1Color = "cpcColor_Widget_Status";
    string alarmLetter = CPC_WIDGET_TEXT_ALARM_DOES_NOT_EXIST, alarmColor, selectColor;

    if (g_iAlertType != DPCONFIG_ALERT_NONBINARYSIGNAL) {
        alarmLetter = CPC_WIDGET_TEXT_ALARM_DOES_NOT_EXIST;
        alarmColor = "unAlarmDoesNotExist";
    } else if (!isAlarmActive) {
        alarmLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
        alarmColor = "unAlarmMasked";
    } else {
        if (isUnAck) {
            if (actText == "HH" || actText == "LL") { alarmLetter = actText; }
            alarmColor = "unWidget_AlarmNotAck";
        } else {
            if (actText == "H" || actText == "L") {
                alarmLetter = actText;
                alarmColor = "cpcColor_Widget_Warning";
            } else if (actText == "HH" || actText == "LL") {
                alarmLetter = actText;
                alarmColor = "unFaceplate_AlarmActive";
            }
        }
        if (alarmColor != "") {
            sBody1Color = alarmColor;
        }
    }

    if(bPosStInvalid){
        sBody1Color = "unDataNotValid";
    }

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue,
            bPosStInvalid,
            sWarningLetter, sWarningColor);

    bool lockVisible;
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, lockVisible);

    if (g_bSystemConnected)
        setMultiValue("Body1", "text", sBody1Text, "Body1", "foreCol", sBody1Color,
                      "WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "WidgetArea", "visible", true,
                      "commentsButton", "visible", isCommentsActive,
                      "AlarmText",  "text",     alarmLetter,
                      "AlarmText",  "foreCol",  alarmColor,
                      "SelectArea", "foreCol", selectColor,
                      "LockBmp", "visible", lockVisible
                      );

//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

mapping CPC_AnalogStatus_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    dyn_string exceptionInfo;

    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MASK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS] = UN_ACCESS_RIGHTS_EXPERT;

//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_AnalogStatus_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_ACK_ALARM:
                buttonEnabled = buttonEnabled && bSelected && cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(deviceName, exceptionInfo) == "TRUE";
                break;
            case UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS:
                buttonEnabled = buttonEnabled && bSelected;
                break;
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_AnalogStatus_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    setMultiValue("Body1", "foreCol", "unDataNoAccess",
				  "Body1", "backCol", "unWidget_Background",
                  "WarningText", "text", "",
				  "AlarmText", "text", "",
				  "SelectArea", "foreCol", "",
				  "LockBmp", "visible", false,
				  "WidgetArea", "visible", false);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_AnalogStatus_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    int iAlertType, iRangeType;
    dyn_string exceptionInfo;

    g_mParameters["AlarmRanges"] = cpcGenericObject_GetAnalogAlarmType(deviceName + ".ProcessInput.PosSt", iAlertType, exceptionInfo);
    g_mParameters["BoolAlert"] = (iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL);
    g_mParameters["rangeClasses"] = makeDynString();

    if (g_mParameters["BoolAlert"]) {
        int rangesNumber;
        dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl.._num_ranges", rangesNumber);
        for (int i = 1; i <= rangesNumber; i++) {
            string rangeClass = "#";
            dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._class", rangeClass);
            dynAppend(g_mParameters["rangeClasses"], rangeClass);
        }
    }

    int iRes = dpGet(deviceName + ".ProcessInput.PosSt:_pv_range.._type", iRangeType);
    if ((iRes >= 0) && (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)) {
        g_mParameters["RangeType"] = DPCONFIG_MINMAX_PVSS_RANGECHECK;
    } else {
        g_mParameters["RangeType"] = DPCONFIG_NONE;
    }

    g_mParameters["PosStFormat"] = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_mParameters["PosStUnit"] = dpGetUnit(deviceName + ".ProcessInput.PosSt");
    g_mParameters["AlAck"] = cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(deviceName, exceptionInfo) == "TRUE";
    g_mParameters["MailSMS"] = cpcGenericObject_isInMailSMS(deviceName + ".ProcessInput.PosSt");
    g_mParameters["FirstOrderFilter"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_CONFIG_FIRST_ORDER_FILTER, exceptionInfo, "0");
	g_mParameters["MessageConversion"] = "none";
	bool configExists;
    int conversionType, order;
    dyn_float arguments;
	fwConfigConversion_get(deviceName + ".ProcessInput.PosSt", configExists, DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN, conversionType, order, arguments, exceptionInfo);
	if(configExists) {
		if(order==1) {

			g_mParameters["MessageConversion"] = "y = " + arguments[2] + "x";
			if(arguments[1]!=0) {
				g_mParameters["MessageConversion"] += " + " + arguments[1];
			}
        }
	}
    dynClear(exceptionInfo);
    g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_DISPLAY_FORMAT, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      exceptionInfo[2] = "Error in CPC_AnalogStatus_FaceplateInitStatics() with device: " + deviceName + " getting the display parameters properties: " + exceptionInfo[2];
      fwExceptionHandling_display(exceptionInfo);
      return;
    }

//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of AnalogStatus alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogStatus_FaceplateLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
}

/** Returns the list of AnalogStatus DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_AnalogStatus_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");

    if (g_mParameters["RangeType"] == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_pv_range.._min");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_pv_range.._max");
    }

    if (g_mParameters["BoolAlert"]) {
        mappingRemove(g_mParameters, "alertPositions_HH");
        mappingRemove(g_mParameters, "alertPositions_H");
        mappingRemove(g_mParameters, "alertPositions_L");
        mappingRemove(g_mParameters, "alertPositions_LL");

        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_range");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
        for (int i = 1; i <= dynlen(g_mParameters["rangeClasses"]); i++) {
            string rangeClass = g_mParameters["rangeClasses"][i];
            if (strpos(rangeClass, "_cpcAnalogHH") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._l_limit");
                g_mParameters["alertPositions_HH"] = i;
            } else if (strpos(rangeClass, "_cpcAnalogH") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._l_limit");
                g_mParameters["alertPositions_H"] = i;
            } else if (strpos(rangeClass, "_cpcAnalogLL") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._u_limit");
                g_mParameters["alertPositions_LL"] = i;
            } else if (strpos(rangeClass, "_cpcAnalogL") > 0) {
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._act_state");
                dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl." + i + "._u_limit");
                g_mParameters["alertPositions_L"] = i;
            }
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_AnalogStatus_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    string sPosSt;
    if(mappingHasKey(g_mParameters, "PosStUnit") && mappingHasKey(g_mParameters, "PosStFormat")) {
        unFaceplate_animateOnlineValue(dpes, values, "PosSt", g_mParameters["PosStUnit"], g_mParameters["PosStFormat"], "cpcColor_Faceplate_Status");
        if (mappingHasKey(g_mParameters, "RangeType")) {
            unFaceplate_animateRange(dpes, values, "PosSt", g_mParameters["RangeType"], g_mParameters["PosStUnit"], g_mParameters["PosStFormat"]);
        }
    }
    getValue("PosSt.display", "text", sPosSt);
    if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "HEX") {
        sprintf(sPosSt, "%X", (int)sPosSt);
        sPosSt = "0x" + sPosSt;
        setValue("PosSt.display", "text", sPosSt);
    } else if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "hex") {
        sprintf(sPosSt, "%x", (int)sPosSt);
        sPosSt = "0x" + sPosSt;
        setValue("PosSt.display", "text", sPosSt);
    }

    if (unGenericObject_shapeExists("FirstOrderFilter") && mappingHasKey(g_mParameters, "FirstOrderFilter")) {
        setValue("FirstOrderFilter.text", "text", g_mParameters["FirstOrderFilter"] == "0" ? "disabled" : g_mParameters["FirstOrderFilter"] + " s.");
    }
    if (unGenericObject_shapeExists("MessageConversion") && mappingHasKey(g_mParameters, "MessageConversion")) {
        setValue("MessageConversion.display", "text", g_mParameters["MessageConversion"]);
    }

    if (dynlen(dpes) == 0) {
        unGenericObject_ColorBoxDisconnect(makeDynString("Alarm_HH", "Alarm_H", "Alarm_UnAck", "Alarm_LL", "Alarm_L"));
        unGenericObject_DisplayValueDisconnect(makeDynString("ValueHH", "ValueH", "ValueL", "ValueLL"));
    } else if (mappingHasKey(g_mParameters, "BoolAlert") && !g_mParameters["BoolAlert"]) { // if does not exist
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_HH", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateWarning("Alarm_H", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateWarning("Alarm_L", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_LL", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_UnAck", 0, false, false, false);
        cpcGenericObject_CheckboxAnimate("MailSMS", false, false);
        if (g_bSystemConnected)
            setMultiValue("ValueHH.display", "text", "", "ValueHH.display", "foreCol", "unAlarmDoesNotExist",
                          "ValueH.display", "text", "", "ValueH.display", "foreCol", "unAlarmDoesNotExist",
                          "ValueL.display", "text", "", "ValueL.display", "foreCol", "unAlarmDoesNotExist",
                          "ValueLL.display", "text", "", "ValueLL.display", "foreCol", "unAlarmDoesNotExist");
    } else { // animate alarms
        bool ackPossible = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._ack_possible");
        bool alarmActive = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._active");

        cpcGenericObject_CheckboxAnimate("MailSMS", mappingHasKey(g_mParameters, "MailSMS") && g_mParameters["MailSMS"], alarmActive);
        cpcGenericObject_ColorBoxAnimate("Alarm_UnAck", ackPossible, alarmActive && mappingHasKey(g_mParameters, "AlAck") && g_mParameters["AlAck"], "cpcColor_Alarm_Bad", false);

        float alarmRange;
        int alarmState;
        if (mappingHasKey(g_mParameters, "alertPositions_HH")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_HH"] + "._l_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_HH"] + "._act_state");
            if (mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
                unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueHH", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_HH", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueHH.display", "text", "", "ValueHH.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_HH", 0, false, false, false);
        }
        if (mappingHasKey(g_mParameters, "alertPositions_LL")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_LL"] + "._u_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_LL"] + "._act_state");
            if (mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
                unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueLL", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_LL", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueLL.display", "text", "", "ValueLL.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateAlarm("Alarm_LL", 0, false, false, false);
        }
        if (mappingHasKey(g_mParameters, "alertPositions_H")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_H"] + "._l_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_H"] + "._act_state");
            if (mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
                unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueH", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_H", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueH.display", "text", "", "ValueH.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_H", 0, false, false, false);
        }
        if (mappingHasKey(g_mParameters, "alertPositions_L")) {
            alarmRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_L"] + "._u_limit");
            alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl." + g_mParameters["alertPositions_L"] + "._act_state");
            if (mappingHasKey(g_mParameters, "PosStFormat") && mappingHasKey(g_mParameters, "PosStUnit")) {
                unGenericObject_DisplayValue(g_mParameters["PosStFormat"], g_mParameters["PosStUnit"], alarmRange, "ValueL", "unDisplayValue_Parameter", false);
            }
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_L", alarmState, mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"], alarmActive, false);
        } else {
            setMultiValue("ValueL.display", "text", "", "ValueL.display", "foreCol", "unAlarmDoesNotExist");
            cpcGenericObject_ColorBoxAnimateWarning("Alarm_L", 0, false, false, false);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}
