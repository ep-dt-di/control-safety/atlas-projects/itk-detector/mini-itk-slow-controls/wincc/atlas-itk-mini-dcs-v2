

// $License: NOLICENSE
/**@file
 *
 * This library contains algorithms for auto-tuning
 *
 * @author Ruben Marti (EN-ICE)
 *         Laura de Frutos (UVa)
 * @date   June 2015
 * @version
 */

/*---------------------------------------------------------------------------------------------------------------------*/

/***********************************************************************************************************************/
/****************************************************** Matrix Functions************************************************/
/***********************************************************************************************************************/

// Calculate the matrix
dyn_dyn_float cpcController_AutoTuning_MatrixInversion(dyn_dyn_float A, int order) {
    // get the determinant of a
    dyn_dyn_float Y;
    double det = 1.0 / cpcController_AutoTuning_CalcDeterminant(A, order);

    // memory allocation
    dyn_float temp;
    dyn_dyn_float minor;

    for (int j = 1; j <= order; j++) {
        for (int i = 1; i <= order; i++) {
            // get the co-factor (matrix) of A(j,i)
            minor = cpcController_AutoTuning_GetMinor( A, j, i, order);
            Y[i][j] = det * cpcController_AutoTuning_CalcDeterminant(minor, order - 1);
            if ( (i + j) % 2 == 1) { Y[i][j] = -Y[i][j]; }
        }
    }

    return Y;

    // release memory
    dynClear(temp);
    dynClear(minor);
}

/*---------------------------------------------------------------------------------------------------------------------*/

// Calculate the minors
dyn_dyn_float cpcController_AutoTuning_GetMinor(dyn_dyn_float src, int row, int col, int order) {
    // indicate which col and row is being copied to dest
    int colCount = 1, rowCount = 1;
    dyn_dyn_float dest;

    for (int i = 1; i <= order; i++ ) {
        if ( i != row ) {
            colCount = 1;
            for (int j = 1; j <= order; j++ ) {
                // when j is not the element
                if ( j != col ) {
                    dest[rowCount][colCount] = src[i][j];
                    colCount++;
                }
            }
            rowCount++;
        }
    }

    return dest;
}

/*---------------------------------------------------------------------------------------------------------------------*/

// Calculate the determinant recursively.
double cpcController_AutoTuning_CalcDeterminant( dyn_dyn_float mat, int order) {
    // order must be >= 0
    // stop the recursion when matrix is a single element
    if ( order == 1 ) { return mat[1][1]; }

    // the determinant value
    float det = 0;

    // allocate the cofactor matrix
    dyn_dyn_float minor;

    for (int i = 1; i <= order; i++ ) {
        // get minor of element (0,i)
        minor = cpcController_AutoTuning_GetMinor( mat,  1, i , order);
        // the recusion is here!
        det += pow( -1.0, (i - 1) ) * mat[1][i] * cpcController_AutoTuning_CalcDeterminant( minor, order - 1 );
    }

    // release memory
    dynClear(minor);

    return det;
}

/*---------------------------------------------------------------------------------------------------------------------*/

/***********************************************************************************************************************/
/****************************************************** Filter *********************************************************/
/***********************************************************************************************************************/

dyn_float cpcController_AutoTuning_Filter (dyn_float Num_Filter, dyn_float Den_Filter, dyn_float vector_list) {
    dyn_float x, y, yd;
    float sum_x = 0, sum_y = 0, a1 = 1;
    int Or_Num = dynlen(Num_Filter);
    int Or_Den = dynlen(Den_Filter);
    int i, j;

    if (Or_Den > 1) { a1 = Den_Filter[1]; }

    for (i = 1; i <= dynlen(vector_list); i++) {
        for (j = 1; j <= Or_Num; j++) {
            if   ((i - j) >= 0)   { x[j] = Num_Filter[j] * vector_list[i - j + 1]; }
            else              { x[j] = 0.0; }
        }
        for (j = 2; j <= Or_Den; j++) {
            if   ((i - j) >= 0) { y[j] = Den_Filter[j] * yd[i - j + 1]; }
            else            { y[j] = 0.0; }
        }

        sum_x = dynSum(x);
        if (Or_Den > 1) { sum_y = dynSum(y); }

        yd[i] = (sum_x - sum_y) / a1;
    }

    return (yd);

    // Release memory
    dynClear(x);
    dynClear(y);
    dynClear(yd);
}

/***********************************************************************************************************************/
/********************************************************* IFT *********************************************************/
/***********************************************************************************************************************/

cpcController_AutoTuning_IFT(string deviceName, int N, float RStep, float Delay, float alpha, int nCycles, float Samp_Time, float Lambda, string NumParam_Controller) {
    dyn_string exceptionInfo;
    bool alreadySent;
    int Disc_Delay;  //System delay (samples)

// Initial variables
    bool enable_IFT;
    float SP_Init, SP_max, SP_min;
    float Out, Out_max, Out_min;
    dyn_float yExp0, uExp0;
    float ySteady, ySteady_In;
    float uSteady, uSteady_In;
    float max_Noise, min_Noise;
    float Ny;

// Experiments
    float y;
    dyn_float yExp1, yExp2;
    float u;
    dyn_float uExp1, uExp2;
    float Uo, SP, R;
    float Gamma = 1;
    dyn_float Cy, Cy_Ant, Cy_Init, Inc_Cy;  // PID parameters
    float J, J_ant;                         // Cost
    bool Exp_OK = TRUE;
    bool End_Opt = FALSE;
    bool ERROR = FALSE;


// Program variables
    int iter = 0;
    int i = 1, j = 1, k = 1;

// Calculation variables
    dyn_float R_vector; // R Vector (Discrete step input)
    dyn_float yd;       // Desired output (R vector filtered)
    dyn_float yhat;     // Error

    dyn_float numK, denK, numTi, denTi, numTd, denTd;
    dyn_dyn_float dy, du;
//   float T=Samp_Time, Te2=pow(T,2);
    float T, Te2;
    dyn_float Cye2;
    dyn_float dJ;
    dyn_dyn_float Hessian;
    dyn_dyn_float Inv_Hessian;
    float det_Hessian;

// Object to modify the Setpoint
    string StringSP;
    string AnalogParameter;
    string AnalogParameterAux;
    bool EnableSP;

    dpGet(deviceName + ".AutoTunning.IFT.EnableSP", EnableSP);

    //Check what type of object is used to indicate the SP if it is a AIR or AI , set forced mode
    dpGet(deviceName + ".AutoTunning.IFT.AnalogSP", AnalogParameterAux);
    float SP_InitAux;
    if(AnalogParameterAux != "")
    {
        dyn_string resul = strsplit(AnalogParameterAux,".");
        dpGet(resul[1] + ".ProcessInput.PosSt",SP_InitAux);
        string typeDp =  dpTypeName(resul[1]);
        if(typeDp == "CPC_AnalogInput" || typeDp == "CPC_AnalogInputReal")
        {
              unGenericObject_SendPulse(resul[1], ".ProcessOutput.ManReg01", CPC_ManReg01_MFOMOR, exceptionInfo, alreadySent);
        }
    }

    /***********************************************************************************************************************/
    /*************************************************** Auxiliar Vectors **************************************************/
    /***********************************************************************************************************************/

    dyn_float Num_Filter, Den_Filter;

    Disc_Delay = floor(Delay / Samp_Time);
    if (Disc_Delay == 0) {
        Disc_Delay = 1;
    }

    dynClear(Num_Filter);
    for (i = 1; i <= (Disc_Delay + 1); i++) { // Filter numerator
        if (i <= (Disc_Delay))  { Num_Filter[i] = 0; }
        else                  { Num_Filter[i] = 1 - alpha; }
    }

    dynClear(Den_Filter);
    for (i = 1; i <= 2; i++) {      // Filter denominator
        if (i == 1)  { Den_Filter[i] = 1; }
        else       { Den_Filter[i] = (-1) * alpha; }
    }


    dyn_dyn_float Ident;  // Identity matrix [Ny,Ny]
    dynClear(Ident);
    for (i = 1; i <= Ny; i++) {
        for (j = 1; j <= Ny; j++) {
            if (i == j)  { Ident[i][j] = 1; }
            else       { Ident[i][j] = 0; }
        }
    }
    /***********************************************************************************************************************/
    /************************************************** Experiment 0 *******************************************************/
    /***********************************************************************************************************************/

    if (NumParam_Controller == "PI")       { Ny = 2; }
    else if (NumParam_Controller == "PID") { Ny = 3; }
    if (Samp_Time < 2.0) {
        i = 2;
        while (Samp_Time < 2.0) {
            Samp_Time = Samp_Time * i;
            i++;
        }
    }
    T = Samp_Time;
    Te2 = pow(T, 2);


    dynClear(Cy);
    dpGet (deviceName + ".ProcessInput.ActKc", Cy[1]);
    dpGet (deviceName + ".ProcessInput.ActTi", Cy[2]);
    if (Ny == 3) {
        dpGet (deviceName + ".ProcessInput.ActTd",  Cy[3]);
        dpGet (deviceName + ".ProcessInput.ActTds", Cy[4]);
    } else {
        Cy[3] = 0;
        Cy[4] = 0;
    }
    Cy_Init = Cy;       // PID parameters -> Iter(0)
    Cy_Ant = Cy_Init;   // PID parameters -> Iter(i-1)


// Set Point Limits
    dpGet (deviceName + ".AutoTunning.Limits.SP_Max", SP_max);
    dpGet (deviceName + ".AutoTunning.Limits.SP_Min", SP_min);
    dpGet (deviceName + ".ProcessInput.ActSP", SP_Init);
    dpSet (deviceName + ".AutoTunning.IFT.SP_Init", SP_Init);

// Output Limits
    dpGet (deviceName + ".AutoTunning.Limits.Out_Max", Out_max);
    dpGet (deviceName + ".AutoTunning.Limits.Out_Min", Out_min);
    dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
// Average value of controlled variable
    dynClear(yExp0);
    dynClear(uExp0);
    for (i = 1; i <= 10; i++) {
        dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
        if (enable_IFT == FALSE) { break; }
        dpGet (deviceName + ".ProcessInput.MV", yExp0[i]);
        dpGet (deviceName + ".ProcessInput.OutOVSt", uExp0[i]);
        delay(Samp_Time);
    }

    if (enable_IFT) {
        ySteady_In = dynAvg(yExp0);
        ySteady = ySteady_In;
        uSteady_In = dynAvg(uExp0);
        uSteady = uSteady_In;
        // Discrete step input
        dynClear(R_vector);
        for (i = 1; i <= N; i++) { R_vector[i] = RStep; }

        // Desired output -> Filtered step input
        dynClear(yd);
        yd = cpcController_AutoTuning_Filter(Num_Filter, Den_Filter, R_vector);

        // Desired output (%)
        for (i = 1; i <= N; i++) { yd[i] = 100 * yd[i] / ySteady; }


        if ((SP_Init + RStep) > SP_max || (SP_Init + RStep) < SP_min) {
            Exp_OK == FALSE;
        } else {
            //Activate Manual Mode
            unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MMMOR, exceptionInfo, alreadySent);

            //Activate Regulation
            unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MREGR, exceptionInfo, alreadySent);
        }
    }

    /***********************************************************************************************************************/

    dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
    while (enable_IFT == TRUE) {

        /***********************************************************************************************************************/
        /************************************************** Experiment 1 *******************************************************/
        /***********************************************************************************************************************/

//   t1 = getCurrentTime();
        if (SP_Init + RStep >= SP_min && SP_Init + RStep <= SP_max) {
            if (EnableSP) {
                StringSP = deviceName + ".ProcessOutput.MSP";
                //Write and send desired SP value
                dpSet(StringSP, SP_Init + RStep);
                unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWSPR, exceptionInfo, alreadySent);
            } else {
                dpGet(deviceName + ".AutoTunning.IFT.AnalogSP", AnalogParameter);
                StringSP = AnalogParameter + "ProcessOutput.MPosR";
                //Write and send desired SP value
                dpSet(StringSP, SP_Init + RStep);
                unGenericObject_SendPulse(AnalogParameter, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWMR, exceptionInfo, alreadySent);
            }
        } else {
            Exp_OK = FALSE;
        }

        dynClear(yExp1);
        for (i = 1; i <= N; i++) {
            dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
            if (enable_IFT == FALSE) { break; }
            dpGet (deviceName + ".ProcessInput.MV", y);
            dpGet (deviceName + ".ProcessInput.OutOVSt", u);
            yExp1[i] = 100 * (y - ySteady_In) / ySteady_In;
            uExp1[i] = 100 * (u - uSteady_In) / uSteady_In;

            if (u > Out_max || u < Out_min) {


            dyn_string exceptionInfo;

            unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + ":" + "Autotuning Failed: Exceeded Limits",exceptionInfo);

            //Pop-up
			dyn_float df;
            dyn_string ds;
    		ChildPanelOnCentralModalReturn("vision/MessageWarning","AutoTuning Failure","Limits Exceeds",df,ds);

            dpSet(deviceName + ".AutoTunning.IFT.Active", FALSE);
            enable_IFT = FALSE;
            break;
        }

            delay(Samp_Time);
        }

        if  (i > N) {
            Exp_OK = TRUE;

            /***********************************************************************************************************************/
            /**************************************************** Cost function ****************************************************/
            /***********************************************************************************************************************/

            dynClear(yhat);
            for (i = 1; i <= N; i++) { // yHat vector -> error
                yhat[i] = (yExp1[i] - yd[i]);
            }

            J = 0;
            for (i = 1; i <= N; i++) {
                J = J + (pow(yhat[i], 2) + Lambda * pow(uExp1[i], 2)) / (2 * N);
            }
            // If J(i)>J(i-1) -> END IFT
            if (J < J_ant || iter == 0 ) { J_ant = J; }
            else                     { End_Opt = TRUE; }

        } else {
            Exp_OK = FALSE;
        }

        /***********************************************************************************************************************/
        iter++;

        if (iter >= nCycles + 1) {
            dpSet(deviceName + ".AutoTunning.IFT.Active", FALSE);
            //dpGet(deviceName + ".AutoTunning.IFT.Active",enable_IFT);
            enable_IFT = FALSE;
        }
        /***********************************************************************************************************************/
        /************************************************** Experiment 2 *******************************************************/
        /***********************************************************************************************************************/


        dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
        if (enable_IFT == TRUE && End_Opt == FALSE && Exp_OK == TRUE) {

            dynClear(yExp2);
            for (i = 1; i <= N; i++) {
                // Write SP value
                R = RStep - ySteady_In * yExp1[i] / 100;

                if ((SP_Init + R) > SP_max || (SP_Init + R) < SP_min) {

                    dpSet(deviceName + ".AutoTunning.IFT.Active", FALSE);
                    Exp_OK == FALSE;
                } else {
                    // SP from controller
                    if (EnableSP) {
                        StringSP = deviceName + ".ProcessOutput.MSP";
                        //Write and send desired SP value
                        dpSet(StringSP, SP_Init + R);
                        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWSPR, exceptionInfo, alreadySent);
                    }
                    // SP from analog parameter
                    else {
                        dpGet(deviceName + ".AutoTunning.IFT.AnalogSP", AnalogParameter);
                        StringSP = AnalogParameter + "ProcessOutput.MPosR";
                        //Write and send desired SP value
                        dpSet(StringSP, SP_Init + R);
                        unGenericObject_SendPulse(AnalogParameter, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWMR, exceptionInfo, alreadySent);
                    }

                    // Data adquisition
                    dpGet (deviceName + ".ProcessInput.MV", y);
                    yExp2[i] = 100 * (y - ySteady_In) / ySteady_In;
                    dpGet (deviceName + ".ProcessInput.OutOVSt", u);
                    uExp2[i] = 100 * (u) / uSteady_In;
                    if (Samp_Time > 2) {delay(Samp_Time - 2);}

                    if (u > Out_max || u < Out_min) {
                         dyn_string exceptionInfo;

                         unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + ":" + "Autotuning Failed: Exceeded Limits",exceptionInfo);

                         //Pop-up
						dyn_float df;
						dyn_string ds;
						ChildPanelOnCentralModalReturn("vision/MessageWarning","AutoTuning Failure","Limits Exceeds",df,ds);

                        dpSet(deviceName + ".AutoTunning.Close.myModuleName", myModuleName());
                        dpSet(deviceName + ".AutoTunning.Close.myPanelName", myPanelName());
                        dpSet(deviceName + ".AutoTunning.IFT.Active", FALSE);
                        dpSet(deviceName + ".AutoTunning.IFT.Active", FALSE);
                        enable_IFT = FALSE;
                        break;
                    }

                }

                dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
                if (enable_IFT == FALSE) { break; }
            }
            if (i > N) {
                Exp_OK = TRUE;
            } else {
                Exp_OK = FALSE;
            }
        } // If enable_IFT==TRUE


        /***********************************************************************************************************************/
        /**************************************************** Calculations *****************************************************/
        /***********************************************************************************************************************/

        dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
        if (enable_IFT == FALSE || Exp_OK == FALSE || End_Opt == TRUE) {
            dpSet(deviceName + ".AutoTunning.IFT.Active", FALSE);
            dpGet(deviceName + ".AutoTunning.IFT.Active", enable_IFT);
        } else {


            /***********************************************************************************************************************/
            /********************************** Calculate the partial derivative of PID function   *********************************/
            /***********************************************************************************************************************/

            dynClear(Cye2);
            Cye2 = makeDynFloat(pow(Cy[1], 2), pow(Cy[2], 2), pow(Cy[3], 2));

            dynClear(numK);
            numK  = makeDynFloat(1.0);
            dynClear(denK);
            denK  = makeDynFloat(Cy[1], 0);
            dynClear(numTi);
            numTi = makeDynFloat(-Te2, -2 * Te2, -Te2);
            dynClear(denTi);
            denTi = makeDynFloat(Te2 * Cy[2] + 2 * T * Cye2[2] + 4 * Cy[3] * Cye2[2], 2 * Te2 * Cy[2] - 8 * Cy[3] * Cye2[2], -2 * T * Cye2[2] + Te2 * Cy[2] + 4 * Cy[3] * Cye2[2]);

            if (Ny == 3) {
                dynClear(numTd);
                numTd = makeDynFloat(4 * Cy[2], -8 * Cy[2], 4 * Cy[2]);
                dynClear(denTd);
                denTd = makeDynFloat(Te2 + 2 * Cy[2] * T + 4 * Cy[3] * Cy[2], -8 * Cy[3] * Cy[2] + 2 * Te2, Te2 + 4 * Cy[3] * Cy[2] - 2 * T * Cy[2]);
            }

            dynClear(dy);
            dy[1] = cpcController_AutoTuning_Filter(numK,  denK,  yExp2);
            dy[2] = cpcController_AutoTuning_Filter(numTi, denTi, yExp2);

            if (Ny == 3) {
                dy[3] = cpcController_AutoTuning_Filter(numTd, denTd, yExp2);
            }

            dynClear(du);
            du[1] = cpcController_AutoTuning_Filter(numK,  denK,  uExp2);
            du[2] = cpcController_AutoTuning_Filter(numTi, denTi, uExp2);
            if (Ny == 3) {
                du[3] = cpcController_AutoTuning_Filter(numTd, denTd, uExp2);
            }


            /***********************************************************************************************************************/
            /************************************** Calculate the derivative of cost function **************************************/
            /***********************************************************************************************************************/

            // dJ vector
            dynClear(dJ);
            dJ = makeDynFloat(0, 0, 0);
            for (i = 1; i <= Ny; i++) {
                for (j = 1; j <= N; j++) {
                    dJ[i] = dJ[i] + (yhat[j] * dy[i][j] + Lambda * uExp1[j] * du[i][j]) / N ;
                }
            }

            // Hessian matrix
            dynClear(Hessian);
            for (i = 1; i <= Ny; i++) {
                for (j = 1; j <= Ny; j++) {
                    for (k = 1; k <= N; k++) {
                        Hessian[i][j] = Hessian[i][j] + (dy[i][k] * dy[j][k] + Lambda * du[i][k] * du[j][k]) / N;
                    }
                }
            }



            dynClear(Inv_Hessian);
            det_Hessian = cpcController_AutoTuning_CalcDeterminant(Hessian, Ny);

            if (det_Hessian == 0) {
                Inv_Hessian = Ident;
            } else {
                Inv_Hessian = cpcController_AutoTuning_MatrixInversion (Hessian, Ny);
            }

            /***********************************************************************************************************************/
            /****************************************** Calculate the new PID parameters *******************************************/
            /***********************************************************************************************************************/

            Cy_Ant = Cy;
            k = 1;

            do {
                Cy = Cy_Ant;

                for (i = 1; i <= Ny; i++) {
                    for (j = 1; j <= Ny; j++) {
                        Cy[i] = Cy[i] - Gamma * Inv_Hessian[i][j] * dJ[j];
                    }
                }

                if (Ny == 3) {
                    if (Cy[3] < 0) { Cy[3] = 0; }
                    Cy[4] = Cy[3] / 10;
                }
                if (Cy_Ant[1] != 0)  { Inc_Cy[1] = fabs(Cy[1] - Cy_Ant[1]); }
                else               { Inc_Cy[1] = 0; }
                if (Cy_Ant[2] != 0)  { Inc_Cy[2] = fabs(Cy[2] - Cy_Ant[2]); }
                else               { Inc_Cy[2] = 0; }
                if (Cy_Ant[3] != 0)  { Inc_Cy[3] = fabs(Cy[3] - Cy_Ant[3]); }
                else               { Inc_Cy[3] = 0; }

                k++;
                Gamma = Gamma / 2;

//     } while (k<=10 && (Cy[1]<0 || Cy[2]<0 || Inc_Cy[1]>25 || Inc_Cy[2]>25 || Inc_Cy[3]>10));
            } while (k <= 10 && (Cy[1] < 0 || Cy[2] < 0));

            if (k > 10) {
                ERROR = TRUE;
                dpSet(deviceName + ".AutoTunning.IFT.Active", FALSE);
            } else {
                dpSet(deviceName + ".ProcessOutput.MKc", Cy[1]);
                unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKPR, exceptionInfo, alreadySent);
                dpSet(deviceName + ".ProcessOutput.MTi", Cy[2]);
                unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTIR, exceptionInfo, alreadySent);
                dpSet(deviceName + ".ProcessOutput.MTd", Cy[3]);
                unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTDR, exceptionInfo, alreadySent);
                dpSet(deviceName + ".ProcessOutput.MTds", Cy[4]);
                unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKDR, exceptionInfo, alreadySent);
            }

        } // if Exp_OK && IFT_ON...
    } // while enable_IFT...

    /***********************************************************************************************************************/

    if (Exp_OK == FALSE || End_Opt == TRUE) {
        if (Exp_OK == FALSE) {DebugTN("Failed experiments: END OF AUTOTUNING");}
        else               {DebugTN("End of optimization: END OF AUTOTUNING");}

        dpSet(deviceName + ".ProcessOutput.MKc", Cy_Ant[1]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKPR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTi", Cy_Ant[2]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTIR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTd", Cy_Ant[3]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTDR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTds", Cy_Ant[4]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKDR, exceptionInfo, alreadySent);
    }

    if (ERROR == TRUE) {
        dpSet(deviceName + ".ProcessOutput.MKc", Cy_Init[1]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKPR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTi", Cy_Init[2]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTIR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTd", Cy_Init[3]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTDR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTds", Cy_Init[4]);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKDR, exceptionInfo, alreadySent);
    }

    //Activate Auto Mode
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MAUMOR, exceptionInfo, alreadySent);

    //Activate Regulation
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MREGR, exceptionInfo, alreadySent);


    //Write and send initial SP
    if (EnableSP) {
        StringSP = deviceName + ".ProcessOutput.MSP";
        //Write and send desired SP value
        dpSet(StringSP, SP_Init);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWSPR, exceptionInfo, alreadySent);
    } else {

        dpGet(deviceName + ".AutoTunning.IFT.AnalogSP", AnalogParameterAux);
        dyn_string resul = strsplit(AnalogParameterAux,".");
        string typeDp =  dpTypeName(resul[1]);
        //Write and send desired SP value
        dpSet(resul[1] + ".ProcessOutput.MPosR", SP_InitAux);
        unGenericObject_SendPulse(resul[1], ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWMR, exceptionInfo, alreadySent);
        if(typeDp == "CPC_AnalogInput" || typeDp == "CPC_AnalogInputReal")
        {
              unGenericObject_SendPulse(resul[1], ".ProcessOutput.ManReg01", CPC_ManReg01_MAUMOR, exceptionInfo, alreadySent);
        }
    }
} // function


/*---------------------------------------------------------------------------------------------------------------------*/


/***********************************************************************************************************************/
/******************************************************** RELAY ********************************************************/
/***********************************************************************************************************************/


cpcController_AutoTuning_RELAY(string deviceName, float dmax, float dmin, int nCycles, bool signe, string NumParam_Controller) {

    dyn_string exceptionInfo;
    bool alreadySent;

    bool x1 = TRUE;
    bool x2 = FALSE;
    float epsilon = 0;        //Output amplitude
    float SP = 0, MV = 0, error = 0;
    float Uo, Ux, max = 0, min = 1000;

    bool EnTim = FALSE;
    time t1, t2;
    int Time_1, Time_2, ms1, ms2;
    float Period;

    bool EnCount = TRUE;
    int Count = 0;

    float P_K, P_Ti, P_Td, P_Tds;

    dyn_float Y;

    float MVMax, MVMin;

    float Ny; //Structure Controller (PID - PI)

    bool Active_Method;

	/***********************************************************************************************************************/
	/************************************************ Scaling **************************************************************/
	/***********************************************************************************************************************/
	dyn_string properties, resul;
	string aux, aux1, Scaling;
	int pos1;


	dpGet(deviceName+".statusInformation.configuration.deviceConfiguration",properties);

	for(int j=1;j<=dynlen(properties);j++)
	 {
		aux = properties[j];
		pos1 = strpos(aux,"SCALING_METHOD");

		if(pos1 >= 0)
		{
		  aux1 = j;
		}
	 }

	if(aux1 >= 0)
	{
		resul = strsplit(properties[aux1],":");
		if(dynlen(resul)>=2)
		{
		  Scaling = resul[2];
		}
	}

	float DefOutH,DefOutL;
	float DefMvH, DefMvL;
	if(Scaling == "1") // input scaling
	{
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._max",DefMvH);
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._min",DefMvL);
	}
	else if(Scaling == "2")  // full scaling
	{
		dpGet(deviceName+".ProcessInput.OutOVSt:_pv_range.._max",DefOutH);
		dpGet(deviceName+".ProcessInput.OutOVSt:_pv_range.._min",DefOutL);
		// need ActSP scaling limits too
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._max",DefMvH);
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._min",DefMvL);
	}


    /***********************************************************************************************************************/

    //Initializing variables
    dpGet (deviceName + ".ProcessInput.OutOVSt", Uo);
    dpGet (deviceName + ".ProcessInput.ActSP", SP);
    dpGet (deviceName + ".AutoTunning.Limits.SP_Max", MVMax);
    dpGet (deviceName + ".AutoTunning.Limits.SP_Min", MVMin);

    /***********************************************************************************************************************/

    //Activate Manual Mode
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MMMOR, exceptionInfo, alreadySent);

    //Write desired output value
    dpSet(deviceName + ".ProcessOutput.MPosR", Uo);
    //Activate writting
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);

    //Activate Out Positioning
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MOUTPR, exceptionInfo, alreadySent);


    /***********************************************************************************************************************/
    /************************************************ Detecting Steady State ***********************************************/
    /***********************************************************************************************************************/

    //SSID FILTER Parameters
    float l1 = 0.2;
    float l2 = 0.1;
    float l3 = 0.1;

    float cl1 = 1 - l1;
    float cl2 = 1 - l2;
    float cl3 = 1 - l3;


    float yf = 0;
    float nu2f = 0;
    float delta2f = 0;
    float y_old = 0;
    float ID_Filter = 0.5;
    float R_Filter,R_Filter_Old;
    bool SteadyState;
    float y;
    dyn_float yExp1, YExp;
    float Samp_Time;
	R_Filter = 0;

	dpGet(deviceName+".AutoTunning.fSampTime",Samp_Time);

    /***********************************************************************************************************************/

    //Indentify the steady state to change U
    SteadyState = FALSE;
    dpGet(deviceName + ".AutoTunning.Relay.Active", Active_Method);
    int i = 1;
    float y_Steady;
    dpGet (deviceName + ".ProcessInput.MV", y_Steady);

    while (!SteadyState & Active_Method) {
        dpGet(deviceName + ".AutoTunning.Relay.Active", Active_Method);
        dpGet (deviceName + ".ProcessInput.MV", y);
        YExp[i] = y;
        delay(Samp_Time);

        if (i > 5) {
            SteadyState = TRUE;
        }
        i++;
    }

    /***********************************************************************************************************************/

    // Calculate hysteresis parameters
    dpGet(deviceName + ".AutoTunning.Relay.Active", Active_Method);
    if (Active_Method) {
        int j = i;
        float max_noise = YExp[i - 1], min_noise = YExp[i - 1];
        while (i >= 2 && i >= (j - 10)) {
            i--;
            if (YExp[i] > max_noise) {
                max_noise = YExp[i];
            }
            if (YExp[i] < min_noise) {
                min_noise = YExp[i];
            }
        }
        epsilon = 3 * (max_noise - min_noise);
        }

    /***********************************************************************************************************************/
    /******************************************************** RELAY ********************************************************/
    /***********************************************************************************************************************/

    int aux = 1;
    dyn_float Y;
    do {

        dpGet(deviceName + ".AutoTunning.Relay.Active", Active_Method);
        dpGet (deviceName + ".ProcessInput.MV", MV);
        Y[aux] = MV;
        error = MV - SP;
        aux = aux + 1;
        //Actual oputput value
        dpGet (deviceName + ".ProcessInput.OutOVSt", Ux);

        //Rising Event
        if (Ux <= (dmin)) {
            EnCount = TRUE;
        }

        /******************************** Hysteresis Section ********************************/
        //Positive zone
        if  (error >= epsilon) {
            //Write desired output value
            if (signe) {
                dpSet(deviceName + ".ProcessOutput.MPosR", dmax);
            } else {
                dpSet(deviceName + ".ProcessOutput.MPosR", dmin);
            }
            //Activate writting
            unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);
            x1 = 1;
            x2 = 0;
        }

        //Negative zone
        else if  (error <= (-epsilon)) {
            //Write desired output value
            if (signe) {
                dpSet(deviceName + ".ProcessOutput.MPosR", dmin);
            } else {
                dpSet(deviceName + ".ProcessOutput.MPosR", dmax);
            }
            //Activate writting
            unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);
            x1 = 0;
            x2 = 1;
        }

        //Intermediate zone
        else if (error > ((-1)*epsilon) && error < epsilon) {
            if (x1 == 1) {
                //Write desired output value
                if (signe) {
                    dpSet(deviceName + ".ProcessOutput.MPosR", dmax);
                } else {
                    dpSet(deviceName + ".ProcessOutput.MPosR", dmin);
                }
                //Activate writting
                unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);
            } else if (x2 == 1) {
                //Write desired output value
                if (signe) {
                    dpSet(deviceName + ".ProcessOutput.MPosR", dmin);
                } else {
                    dpSet(deviceName + ".ProcessOutput.MPosR", dmax);
                }
                //Activate writting
                unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);
            }
        }

        dpGet (deviceName + ".ProcessInput.OutOVSt", Ux);

        if (Ux >= Uo && EnCount == TRUE) {
            EnCount = FALSE;
            Count ++;
        }

        if (Count == nCycles && EnTim == FALSE) {
            EnTim = TRUE;
            //Countdown
            t1 = getCurrentTime();
        }

        //Maximun and Minimum Alarm
        if (MV > MVMax | MV < MVMin) {
    			string aliasName;
    			dyn_string exceptionInfo;
    			bool bRes;
    			aliasName = unGenericDpFunctions_getAlias($sDpName);
    			unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + ":" + "Autotuning Failed: Exceeded Limits",exceptionInfo);
				Active_Method = FALSE;

    			//Pop-up
    			dyn_float df;
    			dyn_string ds;
    			ChildPanelOnCentralModalReturn("vision/MessageWarning","AutoTuning Failure","Limits Exceeds",df,ds);
				dpSet(deviceName + ".AutoTunning.Relay.Active", FALSE);
				break;
        }

    } while (Count <= nCycles & Active_Method);


    //Finish Countdown
    t2 = getCurrentTime();

    //Write desired output value
    dpSet(deviceName + ".ProcessOutput.MPosR", Uo);
    //Activate writting
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);


    /************************************************************************************/
    dpGet(deviceName + ".AutoTunning.Relay.Active", Active_Method);

    if(Active_Method)
    {
      Time_1 = period(t1);
      Time_2 = period(t2);

      ms1 = milliSecond(t1);
      ms2 = milliSecond(t2);

      Period = Time_2 - Time_1 + 0.001 * (ms2 - ms1);

      float a;
      a = dynMax(Y) - dynMin(Y);  // TODO improve the robustness of the amplitude estimation!

      if (NumParam_Controller == "PI")       { Ny = 2; }
      else if (NumParam_Controller == "PID") { Ny = 3; }
      if (Ny == 2) {
          // PID parameters Calculatation
          P_Ti = Period / 1.2;
		  if(Scaling == "1") // input scaling
		  {
			P_K = (DefMvH-DefMvL)/100*0.45 * (4 * (dmax - dmin)) / (3.1416 * a);
		  }
		  else if(Scaling == "2") // full scaling
		  {
			P_K = (DefMvH-DefMvL)/(DefOutH-DefOutL)*0.45 * (4 * (dmax - dmin)) / (3.1416 * a);
		  }
		  else // no scaling
		  {
			P_K = 0.45 * (4 * (dmax - dmin)) / (3.1416 * a);
		  }

          dpSet(deviceName + ".AutoTunning.KNew", P_K);
          dpSet(deviceName + ".AutoTunning.TiNew", P_Ti);
          dpSet(deviceName + ".AutoTunning.TdNew", 0);
          dpSet(deviceName + ".AutoTunning.TdsNew", 0);

      } else if (Ny == 3) {
          // PID parameters Calculatation
          P_Ti = Period / 2.0;
          P_Td = Period / 8;
          P_Tds = P_Td / 10;
          if(Scaling == "1") // input scaling
		    {
			  P_K = (DefMvH-DefMvL)/100*0.6 * (4 * (dmax - dmin)) / (3.1416 * a);
		    }
		    else if(Scaling == "2") // full scaling
		    {
			  P_K = (DefMvH-DefMvL)/(DefOutH-DefOutL)*0.6 * (4 * (dmax - dmin)) / (3.1416 * a);
		    }
		  else  // no scaling
		  {
			P_K = 0.6 * (4 * (dmax - dmin)) / (3.1416 * a);    // 0.6 is original ZN.  0.2 recommended for no overshoot
		  }


          dpSet(deviceName + ".AutoTunning.KNew", P_K);
          dpSet(deviceName + ".AutoTunning.TiNew", P_Ti);
          dpSet(deviceName + ".AutoTunning.TdNew", P_Td);
          dpSet(deviceName + ".AutoTunning.TdsNew", P_Tds);
      }
    }
    else
    {
		//Error during AutoTuning
		float Kp, Ti, Td, Tds;
		dpGet (deviceName + ".ProcessInput.ActKc", Kp);
		dpGet (deviceName + ".ProcessInput.ActTi", Ti);
		dpGet (deviceName + ".ProcessInput.ActTd", Td);
		dpGet (deviceName + +".ProcessInput.ActTds", Tds);

		dpSet(deviceName + ".AutoTunning.KNew", Kp);
		dpSet(deviceName + ".AutoTunning.TiNew", Ti);
		dpSet(deviceName + ".AutoTunning.TdNew", Td);
		dpSet(deviceName + ".AutoTunning.TdsNew", Tds);
    }


    //Activate Auto Mode
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MAUMOR, exceptionInfo, alreadySent);

    //Activate Regulation
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MREGR, exceptionInfo, alreadySent);


}//end function


/*---------------------------------------------------------------------------------------------------------------------*/


/***********************************************************************************************************************/
/******************************************************** S-IMC ********************************************************/
/***********************************************************************************************************************/


dyn_float cpcController_AutoTuning_Lagrange_interp_Inv(dyn_float x, dyn_float y, float y0) {
    int N_Sampling = dynlen(x);

//Initialize outputs
    dyn_float x0;
    dyn_float IDEXACT;
    dyn_float IDINTERP;
    int i;

//Subtract target level to simplify subsequent code
    dyn_float Y;
    for (int i = 1; i <= N_Sampling; i++) {
        Y[i] = y[i] - y0;
    }
//Find exact values
    int counter1 = 0;
    for (i = 1; i <= N_Sampling; i++) {
        if (Y[i] == 0) {
            counter1++;
            IDEXACT[counter1] = i;
        }
    }
//Find crossing
    int counter2 = 0;
    for (i = 1; i <= N_Sampling - 1; i++) {
        if (Y[i]*Y[i + 1] < 0) {
            counter2++;
            IDINTERP[counter2] = i;
        }
    }

//Compusing the different solution
    if (dynlen(IDEXACT) > 0) {
        for (i = 1; i <= counter1; i++) {
            x0[i] = x[IDEXACT[i]];
       }
    }
    if (dynlen(IDINTERP) > 0) {
        for (i = 1; i <= counter2; i++) {
            x0[counter1 + i] = x[IDINTERP[i]] + (x[IDINTERP[i] + 1] - x[IDINTERP[i]]) * Y[IDINTERP[i]] / (Y[IDINTERP[i]] - Y[IDINTERP[i] + 1]);
        }
    }

    return x0;

}

/*---------------------------------------------------------------------------------------------------------------------*/


/***********************************************************************************************************************/
/**************************************************** IDENTIFICATION ***************************************************/
/***********************************************************************************************************************/

cpcController_AutoTuning_IDENTIFICATION(string deviceName, float DeltaU, float Samp_Time, int timeout) {
    dyn_string exceptionInfo;
    bool alreadySent;
    dyn_float yExp1, yExp2, yExp, TimeExp2;
    float y, u, Uo, Time0;
    int i, j; //Auxiliar variables
    bool SteadyState;
    time TimeAux;

    // timing
    time tEndCalc, tDiff, tDelay;
    int iDiff_sec, iDiff_milli;
    float fElapsedTime;
	// timeout handling
	time tStart, tCurrent;
	int iTstart, iTcurrent, iTelapsed;
	bool bTimeout = false;

    float MVMax, MVMin;

    bool Active_Method;

    //SSID FILTER Parameters
    float l1 = 0.2;
    float l2 = 0.1;
    float l3 = 0.1;

    float cl1 = 1 - l1;
    float cl2 = 1 - l2;
    float cl3 = 1 - l3;


    float yf = 0;
    float nu2f = 0;
    float delta2f = 0;
    float y_old = 0;
    float ID_Filter = 0.5;
    float R_Filter,R_Filter_Old;

    Uo = 0;
    //User data - Change on U
    float Ustep = DeltaU;

    //Indentified Pant Parameters
    dyn_float Kp;
    dyn_float Theta;
    dyn_float Tau;

	/***********************************************************************************************************************/
	/************************************************ Scaling **************************************************************/
	/***********************************************************************************************************************/
	dyn_string properties, resul;
	string aux, aux1, Scaling;
	int pos1;

	dpGet(deviceName+".statusInformation.configuration.deviceConfiguration",properties);

	for(int j=1;j<=dynlen(properties);j++)
	 {
		aux = properties[j];
		pos1 = strpos(aux,"SCALING_METHOD");

		if(pos1 >= 0)
		{
		  aux1 = j;
		}
	 }

	if(aux1 >= 0)
	{
		resul = strsplit(properties[aux1],":");
		if(dynlen(resul)>=2)
		{
		  Scaling = resul[2];
		}
	}

	float DefOutH,DefOutL;
	float DefMvH, DefMvL;
	if(Scaling == "1")
	{
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._max",DefMvH);
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._min",DefMvL);
	}
	else if(Scaling == "2")
	{
		dpGet(deviceName+".ProcessInput.OutOVSt:_pv_range.._max",DefOutH);
		dpGet(deviceName+".ProcessInput.OutOVSt:_pv_range.._min",DefOutL);
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._max",DefMvH);
		dpGet(deviceName+".ProcessInput.ActSP:_pv_range.._min",DefMvL);
	}

    /************************************************************************************/

    //Initializing variables
    dpGet (deviceName + ".ProcessInput.OutOVSt", Uo);
    dpGet (deviceName + ".AutoTunning.Limits.SP_Max", MVMax);
    dpGet (deviceName + ".AutoTunning.Limits.SP_Min", MVMin);

    /************************************************************************************/
    //Activate Manual Mode
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MMMOR, exceptionInfo, alreadySent);

    //Activate Out Positioning
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MOUTPR, exceptionInfo, alreadySent);
    /************************************************************************************/


    //Write desired output value
    dpSet(deviceName + ".ProcessOutput.MPosR", Uo);
    //Activate writting
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);

    //Recollect Data until the new Steady State
    //Indentify the steady state to change U
    yf = 0;
    nu2f = 0;
    delta2f = 0;
    y_old = 0;
    ID_Filter = 0.5;
    SteadyState = FALSE;
    i = 1;
	  R_Filter = 0.0;

    //Change on Manipulated Variable
    //Write desired output value
    dpSet(deviceName + ".ProcessOutput.MPosR", Uo + Ustep);
    //Activate writting
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MNEWPOSR, exceptionInfo, alreadySent);
    dpGet(deviceName + ".AutoTunning.SIMC.Active", Active_Method);

	// Get start time of experiment, for evaluation of timeout
	tStart = getCurrentTime();
	iTstart = period(tStart) + 0.0001 * milliSecond(tStart);
	// DebugTN("cpcController_AutoTuning_IDENTIFICATION: Start time is " + iTstart);
	
    while (!SteadyState & Active_Method & !bTimeout) {
        dpGet(deviceName + ".AutoTunning.SIMC.Active", Active_Method);
        dpGet (deviceName + ".ProcessInput.MV", y);
        yExp2[i] = y ;
        TimeAux = getCurrentTime();
        TimeExp2[i] = period(TimeAux) + 0.001 * milliSecond(TimeAux);
        if (i == 1) {
            Time0 = TimeExp2[i];
        }
        TimeExp2[i] = TimeExp2[i] - Time0;

		// Timeout handling
		tCurrent = getCurrentTime();
		iTcurrent = period(tCurrent) + 0.001 * milliSecond(tCurrent);
		iTelapsed = iTcurrent - iTstart;
		// DebugTN ("cpcController_AutoTuning_IDENTIFICATION: Elapsed time is " + iTelapsed);
		if (iTelapsed > timeout) 
		{
			bTimeout = true; // quit after this loop
			DebugTN ("cpcController_AutoTuning_IDENTIFICATION: Identification experiment terminated by timeout after " + iTelapsed + " seconds. Please check the step response before relying on the identified parameters.");
		}
		
        //delay(Samp_Time);  // this doesn't work for sample time as a float

        //SSID FILTER
		      R_Filter_Old = R_Filter;
        nu2f = l2 * pow(( yExp2[i] - yf), 2) + cl2 * nu2f;
        yf = l1 * yExp2[i] + cl1 * yf;
        delta2f = l3 * pow((yExp2[i] - y_old), 2) + cl3 * delta2f;
        y_old = yExp2[i];
        R_Filter = (2 - l1) * nu2f / delta2f;

        if (R_Filter > 2.5) {
            ID_Filter = 0;
        } else if (R_Filter < 1)
			     {
            ID_Filter = 1;
        } else {
            if (i > 1) {
                ID_Filter = ID_Filter;
            }
        }
        if (ID_Filter == 1 || fabs(R_Filter-R_Filter_Old)<1e-4) {
            SteadyState = TRUE;
        }
        i++;

        //Maximun and Minimum Alarm
		      dpGet(deviceName + ".AutoTunning.SIMC.Active", Active_Method);  // There is already a dpGet for this on line 1219!
        if (y > MVMax | y < MVMin && Active_Method == TRUE)
      		{
          string descripName;
          dyn_string exceptionInfo;           	 

          unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + ":" + "Autotuning Failed: Exceeded Limits",exceptionInfo); 
       
          //Pop-up      
  			     dyn_float df;
          dyn_string ds;
      		  ChildPanelOnCentralModalReturn("vision/MessageWarning","AutoTuning Failure","Limits Exceeds",df,ds);

          dpSet(deviceName + ".AutoTunning.SIMC.Active", FALSE);
  			     break;
  			     SteadyState = TRUE;
        }

        // Sleep for the rest of the sample time
        tEndCalc = getCurrentTime();
        tDiff = tEndCalc - TimeAux;
        iDiff_sec = period(tDiff);
        iDiff_milli = milliSecond(tDiff);
        fElapsedTime = iDiff_milli / 1000.0 + iDiff_sec;
        if ( fElapsedTime < Samp_Time)
        {
          tDelay = (Samp_Time - fElapsedTime); // Cast to time type from float
          delay(period(tDelay), milliSecond(tDelay));
        }
    }
    dpGet(deviceName + ".AutoTunning.SIMC.Active", Active_Method);

  	//*******************************
  	//Interpolation
  	//*******************************
  	//Time at 28.3% Final Steady State
    int N_Sampling = dynlen(yExp2);
    if(N_Sampling >=5)
    {
    	float YSteady = yExp2[N_Sampling - 1];

    	float YTau1 = 0.283 * (YSteady - yExp2[1]) + yExp2[1];
    	float YTau2 = 0.632 * (YSteady - yExp2[1]) + yExp2[1];

    	dyn_float TimeTau1, TimeTau2;
		TimeTau1 = cpcController_AutoTuning_Lagrange_interp_Inv(TimeExp2, yExp2, YTau1);
    	TimeTau2 = cpcController_AutoTuning_Lagrange_interp_Inv(TimeExp2, yExp2, YTau2);

    	//MODEL
    	int Num_Sol_1 = dynlen(TimeTau1);
    	int Num_Sol_2 = dynlen(TimeTau2);
    	if(Scaling == "1")
    	{
    		Kp[1] = 100/(DefMvH-DefMvL)*(yExp2[N_Sampling - 1] - yExp2[1]) / (Ustep);
    	}
    	else if(Scaling == "2")
    	{
      // Kp calculation for full scaling
        Kp[1] = (DefOutH-DefOutL)/(DefMvH-DefMvL)*(yExp2[N_Sampling - 1] - yExp2[1]) / (Ustep);  // TODO div by zero if no step defined
    	}
    	else
    	{
    		Kp[1] = (yExp2[N_Sampling - 1] - yExp2[1]) / (Ustep);
    	}
    	Tau[1] = 1.5 * (TimeTau2[Num_Sol_2] - TimeTau1[1]); // Seconds
    	if (TimeTau2[Num_Sol_2] - Tau[1] > 0 && TimeTau2[Num_Sol_2] - Tau[1] > Samp_Time) {
    		Theta[1] = (TimeTau2[Num_Sol_2] - Tau[1]); // Seconds
    	} else {
    		Theta[1] = Samp_Time;
    	}
		
	DebugTN("cpcController_AutoTuning_IDENTIFICATION: Identification Complete. Identified parameters are: Process Gain " + Kp[1] + ", Time Delay " + Theta[1] + ", Time Constant " + Tau[1]);

    	dpSet(deviceName + ".AutoTunning.Kp", fabs(Kp[1]));

    	dpSet(deviceName + ".AutoTunning.Theta", Theta[1]);

    	dpSet(deviceName + ".AutoTunning.Tau", Tau[1]);
    }


    //Activate Auto Mode
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MAUMOR, exceptionInfo, alreadySent);

    //Activate Regulation
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MREGR, exceptionInfo, alreadySent);
}

void cpcController_AutoTuning_stopAutoTuning(string deviceName, string algSelected) {
    DebugTN("STOPPING");
    dyn_string exceptionInfo;
    bool alreadySent;

    bool Active_Relay;
    bool Active_Identification;
    bool Active_IFT;

    float Kp, Ti, Td, Tds;

    if (algSelected == "IFT") {
        DebugTN("Stopping IFT");
        Active_IFT = FALSE;
        dpSet (deviceName + ".AutoTunning.IFT.Active", Active_IFT);

        dpGet(deviceName + ".AutoTunning.IFT.IFT_Kp_In", Kp);
        dpGet(deviceName + ".AutoTunning.IFT.IFT_Ti_In", Ti);
        dpGet(deviceName + ".AutoTunning.IFT.IFT_Td_In", Td);
        dpGet(deviceName + ".AutoTunning.IFT.IFT_Tds_In", Tds);

        dpSet(deviceName + ".ProcessOutput.MKc", Kp);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKPR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTi", Ti);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTIR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTd", Td);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWTDR, exceptionInfo, alreadySent);
        dpSet(deviceName + ".ProcessOutput.MTds", Tds);
        unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWKDR, exceptionInfo, alreadySent);

		//Write and send initial SP value
		string AnalogParameter;
		float SP_Init;

		dpGet(deviceName + ".AutoTunning.IFT.AnalogSP", AnalogParameter);
		dpGet (deviceName + ".AutoTunning.IFT.SP_Init", SP_Init);
		if (AnalogParameter == "") {
			string StringSP = deviceName + ".ProcessOutput.MSP";
			//Write and send desired SP value

			dpSet(StringSP, SP_Init);
			unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWSPR, exceptionInfo, alreadySent);
		} else {
			dpGet(deviceName + ".AutoTunning.IFT.AnalogSP", AnalogParameter);
			string StringSP = AnalogParameter + "ProcessOutput.MPosR";
			//Write and send desired SP value

			dpSet(StringSP, SP_Init);
			unGenericObject_SendPulse(AnalogParameter, ".ProcessOutput.ManReg01", CPC_ManReg01_MNEWMR, exceptionInfo, alreadySent);
		}
    } else

        if (algSelected == "RELAY") {
            DebugTN("Stopping RELAY");
            Active_Relay = FALSE;
            dpSet(deviceName + ".AutoTunning.Relay.Active", Active_Relay);
        } else

            if (algSelected == "SIMC") {
                DebugTN("Stopping SIMC");
                Active_Identification = FALSE;
                dpSet(deviceName + ".AutoTunning.SIMC.Active", Active_Identification);
            }

    //Activate Auto Mode
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg01", CPC_ManReg01_MAUMOR, exceptionInfo, alreadySent);

    //Activate Regulation
    unGenericObject_SendPulse(deviceName, ".ProcessOutput.ManReg02", CPC_ManReg02_MREGR, exceptionInfo, alreadySent);
}

cpcController_AutoTuning_SIMC(string deviceName) {
    float K, Tau, Theta;

    dpGet(deviceName + ".AutoTunning.Kp", K);
    dpGet(deviceName + ".AutoTunning.Theta", Theta);
    dpGet(deviceName + ".AutoTunning.Tau", Tau);

    float TuneParameter = 0;
    float TuneParameter_User = 0;
    float TuneParameter_min = 0;
    float TuneParameter_max = 0;

    TuneParameter_min = -Theta;
    TuneParameter_max = 5 * Tau;  // Used to be theta! this was not correct.

    dpGet(deviceName + ".AutoTunning.SIMC.TuneParameters", TuneParameter_User);
    TuneParameter = TuneParameter_User * (TuneParameter_max - TuneParameter_min) / 100;

    float Kc = 0;
    float Ti = 0;
    float Ti_aux1 = 0;
    float Ti_aux2 = 0;

    if (K > 0) {

		Kc = (1 / K) * (Tau / (TuneParameter + Theta));


        setValue("KNew", "text", Kc);

        Ti_aux1 = Tau;
        Ti_aux2 = 4 * (TuneParameter + Theta);
        if (Ti_aux1 > Ti_aux2) {
            Ti = Ti_aux2;
        } else {
            Ti = Ti_aux1;
        }
        setValue("TiNew", "text", Ti);
    } else {
        DebugTN("cpcController_AutoTuning_SIMC: K is not > 0, cannot calculate controller parameters!");
        setValue("KNew", "text", "--");
        setValue("TiNew", "text", "--");
    }
}
