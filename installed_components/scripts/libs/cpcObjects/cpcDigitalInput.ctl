/**@file

// cpcDigitalInput.ctl
This library contains the widget, faceplate, etc. functions of DigitalInput.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of DigitalInput DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_DigitalInput_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_DigitalInput_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.PosSt", value,
          deviceName + ".ProcessInput.PosSt:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = unGenericObject_FormatValue(dpGetFormat(deviceName + ".ProcessInput.PosSt"), value) + " " + dpGetUnit(deviceName + ".ProcessInput.PosSt"); // formated value with unit
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_DigitalInput_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_MASK_POSST_TEXT, CPC_POPUPMENU_MANUAL_IO_ERROR_BLOCK,
                                            UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_DigitalInput_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_DigitalInput_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    int iAlertType1;
    int iRes = dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl.._type", iAlertType1);
    g_bBoolAlert = ((iRes >= 0) && (iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL));
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of DigitalInput alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalInput_WidgetLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
}

/** Returns the list of DigitalInput DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalInput_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_DigitalInput_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bool posSt = values[9];
    bool posStInvalid = values[10];

    bool isAlarmActive, isUnAck;
    int iActState;
    if (g_bBoolAlert) {
        isAlarmActive = values[11];
        iActState = values[12];
        isUnAck = values[13];
    }

    string warningLetter, warningColor, alarmLetter, alarmColor, controlLetter, controlColor;
    string body1Color, body1BackColor, selectColor;
    bool lockVisible;

    if (bStsReg01Invalid || posStInvalid) {
        warningLetter = CPC_WIDGET_TEXT_INVALID;
        warningColor = "unDataNotValid";
        body1Color = "unDataNotValid";
        body1BackColor = "unWidget_Background";
    } else {
        body1Color = "unWidget_ControlStateAuto";       // Auto mode
        if (getBit(bit32StsReg01, CPC_StsReg01_FOMOST) == 1) {  // Forced mode
            body1Color = "unWidget_ControlStateForced";
            controlLetter = CPC_WIDGET_TEXT_CONTROL_FORCED;
            controlColor = "unWidget_ControlStateForced";
        } else if (getBit(bit32StsReg01, CPC_StsReg01_AUIHFOMOST) == 1) { // Forced mode inhibited
            controlLetter = CPC_WIDGET_TEXT_CONTROL_AUIHFOMOST;
        }
        if (g_bBoolAlert) {                 // Alarm config exists
            if (isAlarmActive) {               // Alarm active
                if ((iActState == DPATTR_ALERTSTATE_APP_NOT_ACK) || (iActState == DPATTR_ALERTSTATE_APP_ACK)) { // Alarm ?
                    body1Color = "unFaceplate_AlarmActive";
                }
                if (isUnAck) {             // Alarm not ack ?
                    body1Color = "unWidget_AlarmNotAck";
                }
            } else {                      // Alarm masked
                alarmLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
                alarmColor = "unAlarmMasked";
            }
        }
        body1BackColor = "unWidget_Background";
        if (posSt) {
            body1BackColor = body1Color;
        }
        unGenericObject_WidgetWarningAnimation(bit32StsReg01,
                                               makeDynInt(CPC_StsReg01_FODIPRO, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW, CPC_StsReg01_MIOERBRST),
                                               makeDynString(CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR, CPC_WIDGET_TEXT_WARNING_ALARM_BLOCKED),
                                               warningLetter, warningColor);

// 6.1 if the device is not correclty running (the iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)
// or the checking is not enabled then set the letter to O and the color to not valid data

        if ((!bSystemIntegrityAlarmEnabled) || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)) {
            warningLetter = CPC_WIDGET_TEXT_OLD_DATA;
            warningColor = "unDataNotValid";
        }

    }
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, lockVisible);
    if (g_bSystemConnected) {
        setMultiValue("Body1", "foreCol", body1Color, "Body1", "fill", "[solid]", "Body1", "backCol", body1BackColor,
                      "WarningText", "text", warningLetter, "WarningText", "foreCol", warningColor,
                      "AlarmText", "text", alarmLetter, "AlarmText", "foreCol", alarmColor,
                      "ControlStateText", "text", controlLetter, "ControlStateText", "foreCol", controlColor,
                      "SelectArea", "foreCol", selectColor, "LockBmp", "visible", lockVisible,
                      "WidgetArea", "visible", true);
        if(g_sWidgetType == "DIPump"){
            setMultiValue("Body2", "foreCol", posSt?unWidget_Background:body1Color);
        }
    }

// display event state
    if (g_bSystemConnected) {
        eventState.visible = iMask == 0;
        commentsButton.visible = isCommentsActive;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

*/
CPC_DigitalInput_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    //todo : redo me
    cpcGenericObject_WidgetDisconnection(sWidgetType, UN_CONFIG_CPC_DIGITALINPUT_DPT_NAME);
    // set event state
    eventState.visible  = false;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_DigitalInput_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_AUTO_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MASK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ONOPEN_REQUEST] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_FORCED_MODE] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[CPC_FACEPLACE_BUTTON_BLOCK_IOERROR] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_DigitalInput_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_DigitalInput_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];

    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_FORCED_MODE:
                bit1 = !(getBit(stsReg01Value, CPC_StsReg01_FOMOST) == 0) && !bStsReg01Bad;
                bit2 = !(getBit(stsReg01Value, CPC_StsReg01_AUINHFMO) == 0) && !bStsReg01Bad;
                buttonEnabled = buttonEnabled && bSelected && !(bit1 || bit2);
                break;
            case CPC_FACEPLACE_BUTTON_BLOCK_IOERROR:
                buttonEnabled = (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_FORCED_MODE) > 0) && bSelected && !bStsReg01Bad; // same access rights as "Forced mode" button
                break;
            case UN_FACEPLATE_BUTTON_ONOPEN_REQUEST:
            case UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST:
            case UN_FACEPLATE_BUTTON_AUTO_MODE:
                bit1 = !(getBit(stsReg01Value, CPC_StsReg01_AUMOST) == 0) && !bStsReg01Bad;
                buttonEnabled = buttonEnabled && bSelected && !bit1;
                break;
            case UN_FACEPLATE_BUTTON_ACK_ALARM:
                buttonEnabled = buttonEnabled && bSelected && cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.PosSt") == "Y";
                break;
            case UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS:
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_DigitalInput_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    int iAlertType1;
    int iRes = dpGet(deviceName + ".ProcessInput.PosSt:_alert_hdl.._type", iAlertType1);
    g_params["BoolAlert"] = ((iRes >= 0) && (iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL));
    g_params["AlAck"] = cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.PosSt") == "Y";
    g_params["MailSMS"] = cpcGenericObject_isInMailSMS(deviceName + ".ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of DigitalInput alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalInput_FaceplateLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateLockDPEs
}

/** Returns the list of DigitalInput DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_DigitalInput_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");

    if (g_params["BoolAlert"]) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ok_range");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_DigitalInput_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUIHFOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_HFST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStsRegNotBit(dpes, values, "CPC_StsReg01_HFST", true, "cpcColor_Faceplate_Status");

    if(g_bSystemConnected && dynlen(values) >= 2) {
        bool posSt, posStInvalid;
        unFaceplate_fetchAnimationCBOnlineValue(dpes, values, "PosSt", posSt, posStInvalid);
        unGenericObject_ColorBoxAnimate("POSST", posSt, true, "cpcColor_Faceplate_Status",
                    posStInvalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
        unGenericObject_ColorBoxAnimate("NOT_POSST", !posSt, true, "cpcColor_Faceplate_Status",
                    posStInvalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
    } else { // disconnection animation
        unGenericObject_ColorBoxDisconnect(makeDynString("POSST", "NOT_POSST"));
    }

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FODIPRO", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MIOERBRST", true, "cpcColor_Faceplate_Warning");
    cpcGenericObject_animateMaskEvent(dpes, values);

    // Animate alarm colorboxes
    anytype alarmActiveValue = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._active");
    if (dynlen(dpes) == 0) {
        unGenericObject_ColorBoxDisconnect(makeDynString("Alarm_Position", "Alarm_UnAck"));
    } else if (mappingHasKey(g_params, "BoolAlert") && g_params["BoolAlert"] && alarmActiveValue != UN_FACEPLATE_MISSED_VALUE) {
        int alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._act_state");
        bool ackPossible = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._ack_possible");
        bool alarmActive = alarmActiveValue;
        bool stsRegInvalid = unFaceplate_fetchAnimationCBValue(dpes, values, ".ProcessInput.StsReg01:_online.._invalid");
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_Position", alarmState, mappingHasKey(g_params, "BoolAlert") && g_params["BoolAlert"], alarmActive, stsRegInvalid || unFaceplate_connectionValid(values));
        cpcGenericObject_ColorBoxAnimate("Alarm_UnAck", ackPossible, alarmActive && mappingHasKey(g_params, "AlAck") && g_params["AlAck"], "cpcColor_Alarm_Bad", stsRegInvalid || unFaceplate_connectionValid(values));
        cpcGenericObject_CheckboxAnimate("MailSMS", mappingHasKey(g_params, "MailSMS") && g_params["MailSMS"], alarmActive);
    } else {
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_Position", 0, false, false, false);
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_UnAck", 0, false, false, false);
        cpcGenericObject_CheckboxAnimate("MailSMS", false, false);
    }

    if (mappingHasKey(g_params, "BoolAlert") && g_params["BoolAlert"]) {
        bool okRange = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosSt:_alert_hdl.._ok_range");
        if (okRange) { normalPosition.number(0); }
        else { normalPosition.number(1); }
    } else {
        normalPosition.number(2);
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}