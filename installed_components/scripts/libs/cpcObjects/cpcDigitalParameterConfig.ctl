/**@file

// cpcDigitalParameterConfig.ctl
This library contains the import and export function of the CPC_DigitalParameter.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_DIGITALPARAMETER_DPT_NAME = "CPC_DigitalParameter";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_LENGTH = 5;

const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ARCHIVE_ACTIVE 		= 1;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDRESS_STSREG01 		= 2;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDRESS_EVSTSREG01	= 3;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDRESS_MANREG01		= 4;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_DEFAULT_VALUE 		= 5;

const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_SECOND_ALIAS    = 7;

// SOFT_FE constants
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_ARCHIVE_ACTIVE       = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_DEFAULT_VALUE        = 2;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_AUTONOMOUS           = 3;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_LENGTH               = 3;

const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_ADDITIONAL_LENGTH    = 1;
const unsigned UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_ADDITIONAL_MASKEVENT = 1;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_DigitalParameterConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg01";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg01"] 		= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg01";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg01"] 	= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["isAlarm"]		= true;
    props["stubAlarm"]		= true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dpe"]			= "StsReg01";
    props["bitPosition"]	= CPC_StsReg01_POSST;
    config["PosSt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg01";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg01"] 		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}





/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_DigitalParameterConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_DIGITALPARAMETER_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_DIGITALPARAMETER_LENGTH;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_DIGITALPARAMETER_ARCHIVE_ACTIVE;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_DIGITALPARAMETER_ADDRESS_STSREG01;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_DIGITALPARAMETER_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_DIGITALPARAMETER_ADDRESS_MANREG01;
  mTemp["DEFAULT_VALUE"]           = UN_CONFIG_CPC_DIGITALPARAMETER_DEFAULT_VALUE;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_DIGITALPARAMETER_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_DIGITALPARAMETER"] = mTemp;


  mappingClear(mTemp);
  mTemp["ARCHIVE_ACTIVE"]       = UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_ARCHIVE_ACTIVE;
  mTemp["DEFAULT_VALUE"]        = UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_DEFAULT_VALUE;
  mTemp["AUTONOMOUS"]           = UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_AUTONOMOUS;
  mTemp["LENGTH"]               = UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_LENGTH;
  mTemp["ADDITIONAL_LENGTH"]    = UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"] = UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_ADDITIONAL_MASKEVENT;

  g_mCpcConst["UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER"] = mTemp;
}




/** Check custom configuration
*/
CPC_DigitalParameterConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    bool bValue;
    cpcConfigGenericFunctions_checkBool("DEFAULT_VALUE", configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_DIGITALPARAMETER_DEFAULT_VALUE], bValue, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_DigitalParameterConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    bool bDefaultValue;
    string sDefaultValueConfigured = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_DIGITALPARAMETER_DEFAULT_VALUE];
    cpcConfigGenericFunctions_checkBool("DEFAULT_VALUE", sDefaultValueConfigured, bDefaultValue, exceptionInfo);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", bDefaultValue);
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_DigitalParameter Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_DigitalParameterConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo, bool bExportOnlineValuesAsDefault = FALSE)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_DIGITALPARAMETER_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");
    dynRemove(dsDpParameters, dynlen(dsDpParameters));

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);

    if( bExportOnlineValuesAsDefault )
    {
      // Replace default values with current online values
      cpcExportGenericFunctions_getDPEValue(sCurrentDp, ".ProcessInput.PosSt", dsDpParameters);
    }
    else
    {
      // Use the regular default values
      cpcExportGenericFunctions_getBoolDefaultValue(sCurrentDp, dsDpParameters);
    }

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalParameterConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalParameterConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_DigitalParameterConfig_ExportConfig()





/**
Purpose: Export CPC_DigitalParameter Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_DigitalParameter Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

DIP_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}

SNMP_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo);
}


void IEC104_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalParameterConfig_ExportConfig() -> Error exporting CPC_DigitalParameter devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_DigitalParameter_ExportConfig()



/**
Purpose: Export CPC_DigitalParameter Devices and override the default parameters with the online values
         These are just simple wrappers to indicate that the device supports the feature

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

_UnPlc_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

OPCUA_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

BACnet_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

DIP_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}

SNMP_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo) {
    // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
    CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo, true);
}


void IEC104_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = TRUE
  CPC_DigitalParameterConfig_ExportConfig(dsDpList, exceptionInfo, TRUE);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "CPC_DigitalParameterConfig_ExportConfig() -> Error exporting CPC_DigitalParameter devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_DigitalParameter_ExportConfigOnlineValues()





/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.
*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
CPC_SOFT_FE_DigitalParameterConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
    bool val;
    cpcConfigGenericFunctions_checkBool("AUTONOMOUS", configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_AUTONOMOUS], val, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) { return; }
    cpcConfigGenericFunctions_checkBool("DEFAULT_VALUE", configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_DEFAULT_VALUE], val, exceptionInfo);
}

CPC_SOFT_FE_DigitalParameterConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
    bool bDefaultValue;
    cpcConfigGenericFunctions_checkBool("DEFAULT_VALUE", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_DEFAULT_VALUE], bDefaultValue, exceptionInfo);
    dpSetWait(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.DfltVal", bDefaultValue);

    bool autonomous;
    cpcConfigGenericFunctions_checkBool("AUTONOMOUS", dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_SOFT_FE_CPC_DIGITALPARAMETER_AUTONOMOUS], autonomous, exceptionInfo);
    if (autonomous) {
        string sDp = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.StsReg01";
        unGenericDpFunctions_createDpeDpFunction(sDp);
        dyn_string params = makeDynString(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessOutput.ManReg01:_original.._value");
        dpSetWait(sDp + ":_dp_fct.._param", params, sDp + ":_dp_fct.._fct", "cpcDigitalParameterDPFunc_getStsReg01Value(\"" + sDp + "\", p1)");

        sDp = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.PosSt";
        unGenericDpFunctions_createDpeDpFunction(sDp);
        params = makeDynString(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.StsReg01:_original.._value");
        dpSetWait(sDp + ":_dp_fct.._param", params, sDp + ":_dp_fct.._fct", "p1");
    }
}

/**
Purpose: Wrapper for SOFT_FE_CPC_DigitalParameter_ExportConfig to override default values with online values
*/
void SOFT_FE_CPC_DigitalParameter_ExportConfigOnlineValues(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  // Call CPC_DigitalParameterConfig_ExportConfig with bExportOnlineValuesAsDefault = true
  SOFT_FE_CPC_DigitalParameter_ExportConfig(dsDpList, exceptionInfo, true);
}


void SOFT_FE_CPC_DigitalParameter_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo, bool bExportOnlineValuesAsDefault = false) {
    string sObject = UN_CONFIG_CPC_DIGITALPARAMETER_DPT_NAME;
    int amount = dynlen(dsDpList);
    for (int i = 1; i <= amount; i++) {
        dyn_string dsDpParameters;
        string currentDP = dsDpList[i];

        if (!dpExists(currentDP)) { continue; }

        unExportDevice_getAllCommonParameters(currentDP, sObject, dsDpParameters);

        cpcExportGenericFunctions_getArchive(currentDP, dsDpParameters);
        dynRemove(dsDpParameters, dynlen(dsDpParameters));

        if(bExportOnlineValuesAsDefault) {// replace default values with current online values
          cpcExportGenericFunctions_getDPEValue(currentDP, ".ProcessInput.PosSt", dsDpParameters);
        }
        else { // use the regular default values
          cpcExportGenericFunctions_getBoolDefaultValue(currentDP, dsDpParameters);
        }
        cpcExportGenericFunctions_getAutonomous(currentDP, dsDpParameters, ".ProcessInput.StsReg01");
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_1, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_2, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG), dsDpParameters);
        cpcExportGenericFunctions_getArchiveNameForDpes(currentDP, UN_CONFIG_EXPORT_ARCHIVE_3, cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT), dsDpParameters);
        cpcExportGenericFunctions_getMaskEvent(currentDP, dsDpParameters);

        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions


//@}
