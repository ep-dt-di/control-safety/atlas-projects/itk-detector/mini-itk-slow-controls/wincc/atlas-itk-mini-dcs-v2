
/**@file

// cpcLocal.ctl
This library contains the widget, faceplate, etc. functions of Local.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of Local DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_Local_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.PosAl");
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_Local_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    float value;
    string sTime;

    dpGet(deviceName + ".ProcessInput.PosAl", value,
          deviceName + ".ProcessInput.PosAl:_online.._stime", sTime);

    dsReturnData[1] = sTime;
    dsReturnData[2] = value;
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_Local_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_Local_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_Local_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    int iAlertType1;
    int iRes = dpGet(deviceName + ".ProcessInput.PosAl:_alert_hdl.._type", iAlertType1);
    g_bBoolAlert = iRes >= 0 && iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of Local DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_Local_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosAl:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosAl:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.PosAl:_alert_hdl.._ack_possible");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_Local_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bool bIsAlarmActive, bAckPossible;
    int iActState;
    if (g_bBoolAlert) {
        bIsAlarmActive = values[9];
        iActState = values[10];
        bAckPossible = values[11];
    }

    string selectColor, warningLetter, warningColor, alarmLetter, alarmColor, sFunction;
    bool bLockVisible;
    dyn_string dsFunctions, exceptionInfo;
    string sBodyColor = "cpcColor_Widget_Status";

    if (bStsReg01Invalid) {
        sBodyColor = "unDataNotValid";
    } else {
        if (bIsAlarmActive) {
            if ((iActState == DPATTR_ALERTSTATE_APP_NOT_ACK) || (iActState == DPATTR_ALERTSTATE_APP_ACK)) {
                sBodyColor = "unFaceplate_AlarmActive";
            }
            if (bAckPossible) {
                sBodyColor = "unWidget_AlarmNotAck";
            }
        } else {
            alarmLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
            alarmColor = "unAlarmMasked";
        }
    }
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, bLockVisible);
    unGenericObject_WidgetWarningAnimation(bit32StsReg01, makeDynInt(CPC_StsReg01_POSALW, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW), makeDynString(CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR), warningLetter, warningColor);
    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue,
            bStsReg01Invalid,
            warningLetter, warningColor);
    if (g_bSystemConnected)
        setMultiValue("WarningText", "text", warningLetter, "WarningText", "foreCol", warningColor,
                      "AlarmText", "text", alarmLetter, "AlarmText", "foreCol", alarmColor,
                      "SelectArea", "foreCol", selectColor, "LockBmp", "visible", bLockVisible,
                      "WidgetArea", "visible", true, "eventState", "visible", iMask == 0,
                      "commentsButton", "visible", isCommentsActive);

    sFunction = cpcGenericObject_GetAnimationFunction(g_sWidgetType);
    if (isFunctionDefined(sFunction) && sFunction != "") {
        if (g_bSystemConnected) {
            execScript("main(int iOn, int iOff, string sBodyColor) {" +
                       sFunction + "(iOn, iOff, sBodyColor);}",
                       makeDynString(), getBit(bit32StsReg01, CPC_StsReg01_ONST), getBit(bit32StsReg01, CPC_StsReg01_OFFST), sBodyColor);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_Local_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    cpcGenericObject_WidgetDisconnection(sWidgetType, UN_CONFIG_CPC_LOCAL_DPT_NAME);
    eventState.visible = false;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_Local_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    if (cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.PosAl") == "Y") {
        buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    } else {
        buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_NOONE;
    }
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_Local_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_Local_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];

    buttonEnabled = (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_ACK_ALARM) > 0);	// User access
    setValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_ACK_ALARM, "enabled", buttonEnabled);
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_Local_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    dyn_string exceptionInfo;
    int iAlertType1;
    int iRes = dpGet(deviceName + ".ProcessInput.PosAl:_alert_hdl.._type", iAlertType1);
    g_params["BoolAlert"] = iRes >= 0 && iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL;
    g_params["AlAck"] = cpcExportGenericFunctions_getAcknowledgeAlarmValue(deviceName, ".ProcessInput.PosAl") != "N";
    g_params["PHFOn"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_HFON, exceptionInfo);
    g_params["PHFOff"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_HFOFF, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of Local DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_Local_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    if (g_params["BoolAlert"]) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosAl:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosAl:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.PosAl:_alert_hdl.._ack_possible");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_Local_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    if (mappingHasKey(g_params, "PHFOn")) {
        unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ONST", g_params["PHFOn"], "cpcColor_Faceplate_Status");
    }
    if (mappingHasKey(g_params, "PHFOff")) {
        unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_OFFST", g_params["PHFOff"], "cpcColor_Faceplate_Status");
    }

    if (dynlen(dpes) == 0) {
        unGenericObject_ColorBoxDisconnect(makeDynString("Alarm_Position", "Alarm_UnAck"));
    } else {
        bool ackPossible = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosAl:_alert_hdl.._ack_possible");
        bool alarmActive = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosAl:_alert_hdl.._active");
        int alarmState = unFaceplate_fetchAnimationCBValue(dpes, values, "ProcessInput.PosAl:_alert_hdl.._act_state");
        bool stsRegInvalid = unFaceplate_fetchAnimationCBValue(dpes, values, ".ProcessInput.StsReg01:_online.._invalid");
        cpcGenericObject_ColorBoxAnimate("Alarm_UnAck", ackPossible, alarmActive && mappingHasKey(g_params, "AlAck") && g_params["AlAck"], "cpcColor_Alarm_Bad", stsRegInvalid || unFaceplate_connectionValid(values));
        cpcGenericObject_ColorBoxAnimateAlarm("Alarm_Position", alarmState, mappingHasKey(g_params, "BoolAlert") && g_params["BoolAlert"], alarmActive, stsRegInvalid || unFaceplate_connectionValid(values));
    }

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_POSW", true, "cpcColor_Faceplate_Warning");
    cpcGenericObject_animateMaskEvent(dpes, values);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}

//@}