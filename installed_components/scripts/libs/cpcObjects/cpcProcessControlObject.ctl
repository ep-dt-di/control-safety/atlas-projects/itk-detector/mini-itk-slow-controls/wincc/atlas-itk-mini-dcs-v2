/**@file

// cpcProcessControlObject.ctl
This library contains the widget, faceplate, etc. functions of ProcessControlObject.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of ProcessControlObject DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name DP name
@param dpType input, device type
@param dsNeedAck output, the lsit of DPE
*/
CPC_ProcessControlObject_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.FuStopISt", "ProcessInput.StartISt", "ProcessInput.TStopISt", "ProcessInput.AlSt", UN_ACKNOWLEDGE_PLC);
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_ProcessControlObject_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    int value;
    string sTime, result;

    dpGet(deviceName + ".ProcessInput.OpMoSt", value,
          deviceName + ".ProcessInput.OpMoSt:_online.._stime", sTime);

    CPC_ProcessControlObject_getOptionModesLabel(deviceName, value, result);
    dsReturnData[1] = sTime;
    dsReturnData[2] = result;
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_ProcessControlObject_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, CPC_POPUPMENU_ALARMS_TEXT, CPC_POPUPMENU_RECIPES_TEXT, CPC_POPUPMENU_ALLOW_TO_RESTART_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_ProcessControlObject_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_ProcessControlObject_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    dyn_string exceptionInfo;
    int iAlertType1, iAlertType2, iAlertType3, iAlertType4;
    int iRes = dpGet(deviceName + ".ProcessInput.StartISt:_alert_hdl.._type", iAlertType1,
                     deviceName + ".ProcessInput.TStopISt:_alert_hdl.._type", iAlertType2,
                     deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._type", iAlertType3,
                     deviceName + ".ProcessInput.AlSt:_alert_hdl.._type", iAlertType4);
    g_bBoolAlert = ((iRes >= 0) && (iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType2 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType3 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType4 == DPCONFIG_ALERT_BINARYSIGNAL));
    g_bMRestart = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);

    string pcoName;
    CPC_ProcessControlObject_getName(deviceName, pcoName);
    if (pcoName == "") { pcoName = "PCO"; }
    setMultiValue("PCOName", "text", pcoName);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of ProcessControlObject DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_ProcessControlObject_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.OpMoSt");
    dynAppend(dpes, deviceName + ".ProcessInput.OpMoSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuOpMo:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.StartISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.TStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.AlSt:_alert_hdl.._act_state");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_ProcessControlObject_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];
    int iMask = values[6];

    bit32 bit32StsReg01 = values[7];
    bool bStsReg01Invalid = values[8];
    bit32 bit32StsReg02 = values[9];
    bool bStsReg02Invalid = values[10];
    int iOpMoSt = values[11];
    bool bOpMoStInvalid = values[12];
    bool bAuOpMoInvalid = values[13];
    int iStartIStState, iTStopIStState, iFuStopIStState, iAlSt;
    if (g_bBoolAlert) {
        iStartIStState = values[14];
        iTStopIStState = values[15];
        iFuStopIStState = values[16];
        iAlSt = values[17];
    }

    string deviceName, sFunction, blockColor, blockLetter, pcoColor, sOpMo, displayColor;
    string sColorSelect, sWarningLetter, sWarningColor, sAlarmLetter, sAlarmColor, sControlLetter, sControlColor, sBodyColor;
    bool bLockVisible, isRestartNeeded;
    dyn_string dsFunctions, exceptionInfo;

    deviceName = unGenericDpFunctions_getDpName(dpes[7]);
    isRestartNeeded = g_bMRestart && getBit(bit32StsReg02, CPC_StsReg02_NEEDRESTART) == 0;
// 1. Select
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sColorSelect, bLockVisible);
// 2. Warning (Include the second stsReg: stsReg02 to check the warning letter)
    cpcGenericObject_WidgetWarningAnimationDoubleStsReg(bit32StsReg01, bit32StsReg02,
            makeDynInt(CPC_StsReg01_AUMRW, CPC_StsReg01_IOSIMUW, CPC_StsReg01_IOERRORW),
            makeDynInt(CPC_StsReg02_ALBW),
            makeDynString(CPC_WIDGET_TEXT_WARNING_DEFAULT, CPC_WIDGET_TEXT_WARNING_SIMU, CPC_WIDGET_TEXT_WARNING_ERROR),
            makeDynString(CPC_WIDGET_TEXT_WARNING_BLOCKED),
            sWarningLetter, sWarningColor);
// 2.1 if the device is not correclty running (the iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)
// or the checking is not enabled then set the letter to O and the color to not valid data

    if ((!bSystemIntegrityAlarmEnabled) || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value)) {
        sWarningLetter = CPC_WIDGET_TEXT_OLD_DATA;
        sWarningColor = "unDataNotValid";
    }

    if (bStsReg01Invalid || bStsReg02Invalid || bOpMoStInvalid || bAuOpMoInvalid) {
        sWarningLetter = CPC_WIDGET_TEXT_INVALID;
        sWarningColor = "unDataNotValid";
    }
    if (!bStsReg01Invalid && !bStsReg02Invalid) {
        // 3. Alarm
        unGenericObject_WidgetAlarmTextAnimation(bit32StsReg01,
                makeDynInt(CPC_StsReg01_ALST, CPC_StsReg01_STARTIST, CPC_StsReg01_TSTOPIST, CPC_StsReg01_FUSTOPIST),
                makeDynString(CPC_WIDGET_TEXT_ALARM_ALST, CPC_WIDGET_TEXT_ALARM_STARTIST, CPC_WIDGET_TEXT_ALARM_TSTOPIST, CPC_WIDGET_TEXT_ALARM_FUSTOPIST),
                sAlarmLetter, sAlarmColor);
        if (sAlarmLetter == "" && isRestartNeeded) {
            sAlarmLetter = CPC_WIDGET_TEXT_MANUAL_RESTART;
            sAlarmColor = "cpcColor_Widget_Warning";
        }
        // 4. Control state
        cpcGenericObject_WidgetControlStateAnimation(bit32StsReg01, bit32StsReg02, makeDynString("CPC_StsReg01_FOMOST", "CPC_StsReg01_MMOST", "CPC_StsReg02_SOFTLDST", "CPC_StsReg01_AUIHFOMOST", "CPC_StsReg01_AUINHMMO"), sControlLetter, sControlColor);
        // 5. Block text
        if (getBit(bit32StsReg02, CPC_StsReg02_TFUSTOP) == 1) {
            blockLetter = CPC_WIDGET_TEXT_BLOCK_TFUSTOP;
            blockColor = "unFaceplate_StatusAlarmActive";
        }
        if (getBit(bit32StsReg02, CPC_StsReg02_ALACTB) == 1) {
            blockLetter = CPC_WIDGET_TEXT_ALARM_BLOCKED;
            blockColor = "unFaceplate_StatusAlarmActive";
        }
    }
// 6. Animate letters and select
    if (g_bSystemConnected) {
        setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "AlarmText", "text", sAlarmLetter, "AlarmText", "foreCol", sAlarmColor,
                      "ControlStateText", "text", sControlLetter, "ControlStateText", "foreCol", sControlColor,
                      "SelectArea", "foreCol", sColorSelect, "LockBmp", "visible", bLockVisible,
                      "BlockText", "text", blockLetter, "BlockText", "foreCol", blockColor,
                      "WidgetArea", "visible", true);
    }
// 7. Animate body
    sBodyColor = "unWidget_ControlStateAuto";
    if (bStsReg01Invalid || bStsReg02Invalid) {
        sBodyColor = "unDataNotValid";
    } else {
        if (sControlLetter == CPC_WIDGET_TEXT_CONTROL_FORCED) {
            sBodyColor = "unWidget_ControlStateForced";
        }
        if (isRestartNeeded) {
            sBodyColor = "cpcColor_Widget_Warning";
        }
        if (sAlarmLetter == CPC_WIDGET_TEXT_ALARM_TSTOPIST || sAlarmLetter == CPC_WIDGET_TEXT_ALARM_FUSTOPIST) {
            sBodyColor = "unFaceplate_AlarmActive";
        }
        if (getBit(bit32StsReg01, CPC_StsReg01_ALUNACK) == 1 && (iTStopIStState == 1 || iTStopIStState == 3 || iFuStopIStState == 1 || iFuStopIStState == 3)) {
            sBodyColor = "unWidget_AlarmNotAck";
        }

        if (blockLetter == CPC_WIDGET_TEXT_ALARM_BLOCKED) {
            sBodyColor = "unFaceplate_StatusAlarmActive";
        }
    }
    sFunction = cpcGenericObject_GetAnimationFunction(g_sWidgetType);
    if (isFunctionDefined(sFunction) && (sFunction != "")) {
        if (g_bSystemConnected)
            execScript("main(int iOn, int iOff, string sBodyColor) {" +
                       sFunction + "(iOn, iOff, sBodyColor);}",
                       makeDynString(), getBit(bit32StsReg01, CPC_StsReg01_ONST), getBit(bit32StsReg01, CPC_StsReg01_OFFST), sBodyColor);
    }
// 8. PCO name
    pcoColor = sBodyColor;
    if (getBit(bit32StsReg01, CPC_StsReg01_OFFST) == 0 && g_sWidgetType != "Air") {
        pcoColor = "unWidget_AlternativeColor";
    }
    if (bStsReg01Invalid || bStsReg02Invalid) {
        pcoColor = "unDataNotValid";
    }
    if (g_bSystemConnected) {
        setMultiValue("PCOName", "foreCol", pcoColor);
    }
// 9. Option mode
    CPC_ProcessControlObject_getOptionModesLabel(deviceName, iOpMoSt, sOpMo);
    displayColor = "cpcColor_Widget_Request";
    if (bOpMoStInvalid) {
        displayColor = "unDataNotValid";
    }
    if (g_bSystemConnected) {
        setMultiValue("OptionMode", "text", sOpMo, "OptionMode", "foreCol", displayColor);
    }

    // display event state
    if (g_bSystemConnected) {
        eventState.visible = iMask == 0;
        commentsButton.visible = isCommentsActive;
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_ProcessControlObject_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    cpcGenericObject_WidgetDisconnection(sWidgetType, UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME);
    unGenericObject_WidgetDisplayValueDisconnect(makeDynString("OptionMode", "PCOName"));
    setValue("BlockText", "text", "");

    // set event state
    eventState.visible = false;
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_ProcessControlObject_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_AUTO_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MANUAL_MODE] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ONOPEN_REQUEST] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_CONTROLLED_STOP_REQUEST] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_OPTION_MODES] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MANUAL_AUTO_TO_DEPENDENT_OBJECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[CPC_FACEPLATE_BUTTON_ALLOW_RESTART] = UN_ACCESS_RIGHTS_OPERATOR;

    buttons[UN_FACEPLATE_BUTTON_FORCED_MODE] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_DEBLOCK_ALARM_ACTION] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_SET_TEMPORARY_AS_FULL_STOP] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_BUTTON_RESET_TEMPORARY_AS_FULL_STOP] = UN_ACCESS_RIGHTS_EXPERT;
    buttons[UN_FACEPLATE_MASKEVENT] = UN_ACCESS_RIGHTS_EXPERT;
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the name of device
@param dpes input, the list of dpes to connect
*/
CPC_ProcessControlObject_ButtonDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonDPEs
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_ProcessControlObject_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    mixed stsReg01Value = dsData[3];
    bool bStsReg01Bad = dsData[4];
    mixed stsReg02Value = dsData[5];
    bool bStsReg02Bad = dsData[6];
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_SET_TEMPORARY_AS_FULL_STOP:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg02Value, CPC_StsReg02_TFUSTOP) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_RESET_TEMPORARY_AS_FULL_STOP:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                getBit(stsReg02Value, CPC_StsReg02_TFUSTOP) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg02Value, CPC_StsReg02_ALACTB) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_DEBLOCK_ALARM_ACTION:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                getBit(stsReg02Value, CPC_StsReg02_ALACTB) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_FORCED_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_FOMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHFMO) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_MANUAL_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_MMOST) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUINHMMO) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_AUTO_MODE:
                buttonEnabled = buttonEnabled && bSelected &&
                                !CPC_ProcessControlObject_isTopPCO(deviceName) &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_ONOPEN_REQUEST:
            case UN_FACEPLATE_BUTTON_OFFCLOSE_REQUEST:
            case UN_FACEPLATE_BUTTON_CONTROLLED_STOP_REQUEST:
            case UN_FACEPLATE_BUTTON_OPTION_MODES:
            case UN_FACEPLATE_BUTTON_MANUAL_AUTO_TO_DEPENDENT_OBJECT:
                buttonEnabled = buttonEnabled && bSelected &&
                                !getBit(stsReg01Value, CPC_StsReg01_AUMOST) &&
                                !getBit(stsReg02Value, CPC_StsReg02_SOFTLDST) &&
                                !bStsReg01Bad && !bStsReg02Bad;
                break;
            case UN_FACEPLATE_BUTTON_NEXT:
            case UN_FACEPLATE_BUTTON_BACK:
            default:
                break;
        }

        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else if (dsButtons[i] == CPC_FACEPLATE_BUTTON_ALLOW_RESTART || dsButtons[i] == UN_FACEPLATE_BUTTON_ACK_ALARM) {
            cpcButton_ButtonSetState(makeDynString(CPC_FACEPLATE_BUTTON_ALLOW_RESTART, UN_FACEPLATE_BUTTON_ACK_ALARM), dsUserAccess, deviceName, dpType, dsData, exceptionInfo);
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_ProcessControlObject_FaceplateInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
    int iRes, iAlertType1, iAlertType2, iAlertType3, iAlertType4, iRangeType;
    dyn_string exceptionInfo;
    iRes = dpGet(deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._type", iAlertType1,
                 deviceName + ".ProcessInput.StartISt:_alert_hdl.._type", iAlertType2,
                 deviceName + ".ProcessInput.TStopISt:_alert_hdl.._type", iAlertType3,
                 deviceName + ".ProcessInput.AlSt:_alert_hdl.._type", iAlertType4);
    g_params["BoolAlert"] = ((iRes >= 0) && (iAlertType1 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType2 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType3 == DPCONFIG_ALERT_BINARYSIGNAL) && (iAlertType4 == DPCONFIG_ALERT_BINARYSIGNAL));

    dyn_string exceptionInfo;
    g_params["MRestart"] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MRESTART, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateInitStatics
}

/** Returns the list of ProcessControlObject DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_ProcessControlObject_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".eventMask");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg01:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02");
    dynAppend(dpes, deviceName + ".ProcessInput.StsReg02:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.OpMoSt");
    dynAppend(dpes, deviceName + ".ProcessInput.OpMoSt:_online.._invalid");
    dynAppend(dpes, deviceName + ".ProcessInput.AuOpMo");
    dynAppend(dpes, deviceName + ".ProcessInput.AuOpMo:_online.._invalid");
    if (g_params["BoolAlert"]) {
        dynAppend(dpes, deviceName + ".ProcessInput.StartISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.TStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.FuStopISt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.AlSt:_alert_hdl.._act_state");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_ProcessControlObject_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ONST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_OFFST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_ALACTB", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_TFUSTOP", true, "cpcColor_Faceplate_Status");

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_RUNOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_CSTOPOST", true, "cpcColor_Faceplate_Status");
    CPC_ProcessControlObject_animateOptionMode(dpes, values, "OpMoSt", "cpcColor_Faceplate_Request");

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_MMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUINHMMO", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_FOMOST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUINHFMO", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_SOFTLDST", true, "cpcColor_Faceplate_Status");

    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_AUONRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_MONRST", true, "cpcColor_Faceplate_Status");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_AUCSTOP", true, "cpcColor_Faceplate_Status");
    CPC_ProcessControlObject_animateOptionMode(dpes, values, "AuOpMo", "cpcColor_Faceplate_Request");

    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_STARTIST",   "StartISt",  "CPC_StsReg01_ALUNACK");
    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_TSTOPIST",  "TStopISt",  "CPC_StsReg01_ALUNACK");
    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_FUSTOPIST", "FuStopISt", "CPC_StsReg01_ALUNACK");
    unFaceplate_animateInterlock(dpes, values, "CPC_StsReg01_ALST",      "AlSt",      "CPC_StsReg01_ALUNACK");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_ALUNACK", true, "cpcColor_Alarm_Bad");

    cpcGenericObject_animateMaskEvent(dpes, values);
    cpcGenericObject_animateNeedRestart(dpes, values);
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOERRORW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_IOSIMUW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg02_ALBW", true, "cpcColor_Faceplate_Warning");
    unFaceplate_animateStatusBit(dpes, values, "CPC_StsReg01_AUMRW", true, "cpcColor_Faceplate_Warning");
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
}


//begin_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions
/**Animate option mode in faceplate.

Function is alike unFaceplate functions.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of parameter to animate
@param color field color on valid state
*/
CPC_ProcessControlObject_animateOptionMode(dyn_string dpes, dyn_string values, string dpeName, string color) {
    string convertedValue;
    int value;
    bool invalid;
    if (dynlen(dpes) > 0) { // online animation
        unFaceplate_fetchAnimationCBOnlineValue(dpes, values, dpeName, value, invalid);
        CPC_ProcessControlObject_getOptionModesLabel(unGenericDpFunctions_getDpName(dpes[6]), value, convertedValue);
        cpcGenericObject_DisplayText(convertedValue, dpeName, color,
                                     invalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
    } else {
        unGenericObject_DisplayValueDisconnect(dpeName);
    }
}

/**
Purpose: this function is called by a PCO object to get an option mode label

Parameter:
	-> sDpName, string input, device name
	-> uLabel, unsigned input, label number
	-> result, string output, result of the function

Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_ProcessControlObject_getOptionModesLabel(string sDpName, unsigned uLabel, string &result) {
    string sOptionModes;
    dyn_string dsOptionModes;

    result = "";
    //todo: description should be fetched in advance, not in animation function!
    sOptionModes = unGenericDpFunctions_getDescription(sDpName + UN_PCO_OPTION_MODE_PATH);
    dsOptionModes = strsplit(sOptionModes, UN_PCO_OPTION_MODE_DELIMITER);
    if ((dynlen(dsOptionModes) >= uLabel) && (uLabel > 0) && (UN_PCO_OPTION_MODE_NUMBER >= uLabel)) {
        result = dsOptionModes[uLabel];
    }
    if (result == "") {
        //result = "Op Mode " + uLabel;
    }
}

/**
Purpose: this function is called by a PCO object to get an option mode allowance

Parameter:
	-> sDpName, string input, device name
	-> uLabel, unsigned input, label number
	-> result, bit32 output, result of the function

Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_ProcessControlObject_getOptionModesAllowance(string sDpName, unsigned uLabel, bit32 &result) {
    string sOptionModes;
    dyn_string dsOptionModes;

    result = 0;
    //todo: description should be fetched in advance, not in animation function!
    sOptionModes = unGenericDpFunctions_getDescription(sDpName + UN_PCO_OPTION_MODE_PATH);
    dsOptionModes = strsplit(sOptionModes, UN_PCO_OPTION_MODE_DELIMITER);
    if ((dynlen(dsOptionModes) >= (UN_PCO_OPTION_MODE_NUMBER + uLabel)) && (uLabel > 0)) {
        result = dsOptionModes[UN_PCO_OPTION_MODE_NUMBER + uLabel];
    }
}

/**
Purpose: this function is called by a PCO object to set the option mode labels & allowance

Parameter:
	-> sDpName, string input, device name
	-> dsLabels, dyn_string input, labels
	-> dsAllowances, dyn_string input, allowances (must be bit32 variables converted to string)
	-> iRes, int output, result of the function, < 0 if an error occured

Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_ProcessControlObject_setOptionModes(string sDpName, dyn_string dsLabels, dyn_string dsAllowances, int &iRes) {
    int i, length;
    string description;

    iRes = -1;
    length = dynlen(dsLabels);
    if ((length != dynlen(dsAllowances)) || (length == 0)) {
        //error
    } else {
        for (i = 1; i <= UN_PCO_OPTION_MODE_NUMBER; i++) {		// Labels
            if (i <= length) {
                description = description + UN_PCO_OPTION_MODE_DELIMITER + dsLabels[i];
            } else {
                description = description + UN_PCO_OPTION_MODE_DELIMITER;
            }
        }
        for (i = 1; i <= UN_PCO_OPTION_MODE_NUMBER; i++) {		// Allowances
            if (i <= length) {
                description = description + UN_PCO_OPTION_MODE_DELIMITER + dsAllowances[i];
            } else {
                description = description + UN_PCO_OPTION_MODE_DELIMITER;
            }
        }
        description = substr(description, 1);
        iRes = dpSetDescription(sDpName + UN_PCO_OPTION_MODE_PATH, description);
    }
}

/**
Purpose: this function is called by a PCO object to get PCO name

Parameter:
	-> sDpName, string input, device name
	-> result, string output, result of the function

Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_ProcessControlObject_getName(string sDpName, string &result) {
    dyn_string exceptionInfo;
    result = cpcGenericDpFunctions_getDeviceProperty(sDpName, CPC_PARAMS_PCONAME, exceptionInfo);
}

/**
Purpose: this function is called by a PCO object to set the PCO name

Parameter:
	-> sDpName, string input, device name
	-> name, string input, name to be set
	-> exceptionInfo, dyn_string output, for errors

Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_ProcessControlObject_setName(string sDpName, string name, dyn_string &exceptionInfo) {
    cpcGenericDpFunctions_setDeviceProperty(sDpName, CPC_PARAMS_PCONAME, name, exceptionInfo);
}

unPCO_getNextAllowedModes(string sDpName, int iCurrentMode, bool bRunning, dyn_string &dsAllowedModes, dyn_string &exceptionInfo) {
    string parameters;
    dyn_string dsParameters;
    bit32 allowance;
    int i;

    dynClear(dsAllowedModes);
    /*	if ((iCurrentMode <= 0) || (iCurrentMode >  UN_PCO_OPTION_MODE_NUMBER))
    		{
    		fwException_raise(exceptionInfo,"ERROR","unPCO_getNextAllowedModes : " + getCatStr("unFunctions","BADINPUTS"),"");
    		return;
    		}
    */
    // 1. Get parameters
    parameters = unGenericDpFunctions_getDescription(sDpName + UN_PCO_OPTION_MODE_PATH);
    dsParameters = strsplit(parameters, UN_PCO_OPTION_MODE_DELIMITER);
    while (dynlen(dsParameters) < (2 * UN_PCO_OPTION_MODE_NUMBER)) {
        dynAppend(dsParameters, "");
    }

    // 2. Not running -> each defined mode is allowed
    if (bRunning == false) {
        for (i = 1; i <= UN_PCO_OPTION_MODE_NUMBER; i++) {
            if ((strltrim(dsParameters[i]) != "") && (i != iCurrentMode)) {
                dynAppend(dsAllowedModes, dsParameters[i]);
            }
        }
    } else {

        // 3. While running -> allowed modes are coded in the allowance table
        allowance = dsParameters[iCurrentMode + UN_PCO_OPTION_MODE_NUMBER];
        for (i = 1; i <= UN_PCO_OPTION_MODE_NUMBER; i++) {
            if ((strltrim(dsParameters[i]) != "") && (getBit(allowance, UN_PCO_OPTION_MODE_NUMBER - i) == 1) && (i != iCurrentMode)) {
                dynAppend(dsAllowedModes, dsParameters[i]);
            }
        }
    }
}
unPCO_getOptionModesAllowance(string sDpName, unsigned uLabel, bit32 &result) {
    CPC_ProcessControlObject_getOptionModesAllowance(sDpName, uLabel, result);
}
unPCO_setOptionModes(string sDpName, dyn_string dsLabels, dyn_string dsAllowances, int &iRes) {
    CPC_ProcessControlObject_setOptionModes(sDpName, dsLabels, dsAllowances, iRes);
}



/**
Purpose: to get the modes you can access from current mode (using allowance tables)

Parameter:
	-> sDpName, string input, device name
	-> iCurrentMode, int input, current mode
	-> bRunning, bool input, true while running
	-> dsAllowedModes, dyn_string output, result of the function
	-> exceptionInfo, dyn_string output, for errors

Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
CPC_ProcessControlObject_getNextAllowedModes(string sDpName, int iCurrentMode, bool bRunning, dyn_string &dsAllowedModes, dyn_string &dsUnAllowedModes, dyn_string &exceptionInfo) {
    string parameters;
    dyn_string dsParameters;
    bit32 allowance;

    dynClear(dsAllowedModes);
    dynClear(dsUnAllowedModes);

    // 1. Get parameters
    parameters = unGenericDpFunctions_getDescription(sDpName + UN_PCO_OPTION_MODE_PATH);
    dsParameters = strsplit(parameters, UN_PCO_OPTION_MODE_DELIMITER);
    while (dynlen(dsParameters) < (2 * UN_PCO_OPTION_MODE_NUMBER)) {
        dynAppend(dsParameters, "");
    }
    if (bRunning) {
        // 3. While running -> allowed modes are coded in the allowance table
        allowance = dsParameters[iCurrentMode + UN_PCO_OPTION_MODE_NUMBER];
        for (int i = 1; i <= UN_PCO_OPTION_MODE_NUMBER; i++) {
            if (strltrim(dsParameters[i]) != "") {
                if ((getBit(allowance, UN_PCO_OPTION_MODE_NUMBER - i) == 1) && (i != iCurrentMode)) {
                    dynAppend(dsAllowedModes, dsParameters[i]);
                } else {
                    dynAppend(dsUnAllowedModes, dsParameters[i]);
                }
            }
        }
    } else {
        // 2. Not running -> each defined mode is allowed
        for (int i = 1; i <= UN_PCO_OPTION_MODE_NUMBER; i++) {
            if (strltrim(dsParameters[i]) != "") {
                if (i != iCurrentMode) {
                    dynAppend(dsAllowedModes, dsParameters[i]);
                } else {
                    dynAppend(dsUnAllowedModes, dsParameters[i]);
                }
            }
        }
    }
}

bool CPC_ProcessControlObject_isTopPCO(string dpName)
{
    string master;
    dyn_string exceptionInfo;
    unGenericDpFunctions_getKeyDeviceConfiguration(dpName, CPC_CONFIG_MASTER_NAME_KEY, master, exceptionInfo);
    return master == "";
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions


//@}