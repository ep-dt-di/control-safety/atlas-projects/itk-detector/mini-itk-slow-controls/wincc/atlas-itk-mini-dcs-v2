/**@file

// cpcProcessControlObjectConfig.ctl
This library contains the import and export function of the CPC_ProcessControlObject.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME = "CPC_ProcessControlObject";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_LENGTH = 26;

const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_PCONAME = 1;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_NORMAL_POSITION = 2;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_STSREG01 = 3;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_STSREG02 = 4;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_EVSTSREG01 = 5;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_EVSTSREG02 = 6;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_AUOPMO = 7;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_OPMOST = 8;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_MANREG01 = 9;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_MOPMOR = 10;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_1 = 11;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_2 = 12;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_3 = 13;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_4 = 14;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_5 = 15;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_6 = 16;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_7 = 17;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_8 = 18;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_1 = 19;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_2 = 20;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_3 = 21;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_4 = 22;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_5 = 23;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_6 = 24;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_7 = 25;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_8 = 26;

const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_SECOND_ALIAS    = 7;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_ProcessControlObjectConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg01";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg01"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.StsReg02";
    props["dataType"] 		= CPC_UINT16;
    config["StsReg02"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.OpMoSt";
    props["dataType"] 		= CPC_FLOAT;
    config["OpMoSt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.AuOpMo";
    props["dataType"] 		= CPC_FLOAT;
    config["AuOpMo"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.StartISt";
    props["dpe"]			= "StsReg01";
    props["bitPosition"]	= CPC_StsReg01_STARTIST;
    props["isAlarm"]		= true;
    config["StartISt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.TStopISt";
    props["dpe"]			= "StsReg01";
    props["bitPosition"]	= CPC_StsReg01_TSTOPIST;
    props["isAlarm"]		= true;
    config["TStopISt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.FuStopISt";
    props["dpe"]			= "StsReg01";
    props["bitPosition"]	= CPC_StsReg01_FUSTOPIST;
    props["isAlarm"]		= true;
    config["FuStopISt"] 	= props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlSt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALST;
    props["isAlarm"]      = true;
    config["AlSt"]        = props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg01";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg01"] 	= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg02";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg02"] 	= props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlUnAck";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALUNACK;
    config["AlUnAck"]     = props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg01";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg01"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MOpMoR";
    props["dataType"] 		= CPC_FLOAT;
    config["MOpMoR"] 		= props;
//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_ProcessControlObjectConfig()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_LENGTH;
  mTemp["PCONAME"]                 = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_PCONAME;
  mTemp["NORMAL_POSITION"]         = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_NORMAL_POSITION;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_STSREG01;
  mTemp["ADDRESS_STSREG02"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_STSREG02;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_EVSTSREG02"]      = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_EVSTSREG02;
  mTemp["ADDRESS_AUOPMO"]          = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_AUOPMO;
  mTemp["ADDRESS_OPMOST"]          = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_OPMOST;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_MANREG01;
  mTemp["ADDRESS_MOPMOR"]          = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDRESS_MOPMOR;
  mTemp["MODE_NAME_1"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_1;
  mTemp["MODE_NAME_2"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_2;
  mTemp["MODE_NAME_3"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_3;
  mTemp["MODE_NAME_4"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_4;
  mTemp["MODE_NAME_5"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_5;
  mTemp["MODE_NAME_6"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_6;
  mTemp["MODE_NAME_7"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_7;
  mTemp["MODE_NAME_8"]             = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_8;
  mTemp["MODE_ALLOWANCE_1"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_1;
  mTemp["MODE_ALLOWANCE_2"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_2;
  mTemp["MODE_ALLOWANCE_3"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_3;
  mTemp["MODE_ALLOWANCE_4"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_4;
  mTemp["MODE_ALLOWANCE_5"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_5;
  mTemp["MODE_ALLOWANCE_6"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_6;
  mTemp["MODE_ALLOWANCE_7"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_7;
  mTemp["MODE_ALLOWANCE_8"]        = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_8;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_PROCESSCONTROLOBJECT"] = mTemp;
}




/**
Returns the list of parameter names that passed via config line.
*/
dyn_string CPC_ProcessControlObjectConfig_getParamNames() {
//begin_TAG_SCRIPT_DEVICE_TYPE_getParamNames
    return makeDynString(CPC_PARAMS_MRESTART, CPC_PARAMS_RSTARTFS);
//end_TAG_SCRIPT_DEVICE_TYPE_getParamNames
}

/** Check custom configuration
*/
CPC_ProcessControlObjectConfig_checkCustomConfig(dyn_string configLine, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
    for (int i = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_1; i <= UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_8; i++) {
        string allowance = configLine[UN_CONFIG_COMMON_LENGTH + i];
        if ((strlen(allowance) != 0) && (strlen(allowance) != 8)) {
            fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_ProcessControlObject_checkConfig: " + getCatStr("unGeneration", "BADALLOWANCE"), "");
        } else {
            strreplace(allowance, "1", "");
            strreplace(allowance, "0", "");
            if (allowance != "") {
                fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_ProcessControlObject_checkConfig: " + getCatStr("unGeneration", "BADALLOWANCE"), "");
            }
        }
    }

    if (configLine[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_PCONAME] != "") {
        DebugTN("PCO_NAME field is depricated and to be removed in the future. Please use PCO_NAME parameter instead or update to the latest CPC package.");
        int parametersPos = cpcConfigGenericFunctions_getConstantDPEProperty(UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME, "", hasArchive, "PARAMETERS");
        if (strpos(configLine[UN_CONFIG_COMMON_LENGTH + parametersPos], CPC_PARAMS_PCONAME) >= 0) {
            fwException_raise(exceptionInfo, "ERROR", "PCO NAME is defined in two places. Use PARAMETERS field to define PCO_NAME.", "");
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_checkCustomConfig
}

/** Set custom configuration
*/
CPC_ProcessControlObjectConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    int iRes;
    if (dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_PCONAME] != "") {
        CPC_ProcessControlObject_setName(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_PCONAME], exceptionInfo);
    }
    CPC_ProcessControlObject_setOptionModes(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME],
                                            makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_1],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_2],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_3],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_4],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_5],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_6],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_7],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_NAME_8]),
                                            makeDynString(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_1],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_2],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_3],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_4],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_5],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_6],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_7],
                                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + UN_CONFIG_CPC_PROCESSCONTROLOBJECT_MODE_ALLOWANCE_8]),
                                            iRes);
    if (iRes < 0) {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_ProcessControlObject_setConfig: set option modes failed", "");
    }

    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.TStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.FuStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.StartISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.AlSt");
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_ProcessControlObject Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_ProcessControlObjectConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    dynAppend(dsDpParameters, ""); // deprecated field for PCO object

    cpcExportGenericFunctions_getDigitalNormalPosition(sCurrentDp,
                                                       sObject,
                                                       makeDynString(".ProcessInput.FuStopISt",
                                                                     ".ProcessInput.TStopISt",
                                                                     ".ProcessInput.StartISt"),
                                                       dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg02",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg02", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuOpMo",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "OpMoSt",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MOpMoR",     FALSE, dsDpParameters);

    cpcExportGenericFunctions_getPCOParameters(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ProcessControlObjectConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_ProcessControlObjectConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_ProcessControlObjectConfig_ExportConfig()





/**
Purpose: Export CPC_ProcessControlObject Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_ProcessControlObject_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_ProcessControlObjectConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_ProcessControlObject Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_ProcessControlObject_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_ProcessControlObjectConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_ProcessControlObject_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_ProcessControlObjectConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_ProcessControlObject_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_ProcessControlObjectConfig_ExportConfig(dsDpList, exceptionInfo);
}





void IEC104_CPC_ProcessControlObject_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_ProcessControlObjectConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_ProcessControlObject_ExportConfig() -> Error exporting CPC_ProcessControl devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_ProcessControlObject_ExportConfig()





//@}
