/**@file

// cpcSteppingMotorConfig.ctl
This library contains the import and export function of the CPC_SteppingMotor.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{



// Constants
const string UN_CONFIG_CPC_STEPPINGMOTOR_DPT_NAME = "CPC_SteppingMotor";
//begin_TAG_SCRIPT_DEVICE_TYPE_constants
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_LENGTH = 26;

const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_UNIT = 1;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_FORMAT = 2;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_RANGEMAX = 3;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_RANGEMIN = 4;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_SPDST_RANGEMAX = 5;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_SPDST_RANGEMIN = 6;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_DEADBAND = 7;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_DEADBAND_TYPE = 8;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ARCHIVE_ACTIVE = 9;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ARCHIVE_TIME_FILTER = 10;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_NORMAL_POSITION = 11;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_STSREG01 = 12;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_STSREG02 = 13;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_EVSTSREG01 = 14;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_EVSTSREG02 = 15;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_POSST = 16;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_SPDST = 17;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_POSRST = 18;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_SPDRST = 19;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_AUPOSRST = 20;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_AUSPDRST = 21;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MPOSRST = 22;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MSPDRST = 23;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MANREG01 = 24;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MPOSR = 25;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MSPDR = 26;

const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_LENGTH          = 7;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_MASKEVENT       = 1;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_PARAMETERS      = 2;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_MASTER_NAME     = 3;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_PARENTS         = 4;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_CHILDREN        = 5;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_TYPE            = 6;
const unsigned UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_SECOND_ALIAS    = 7;
//end_TAG_SCRIPT_DEVICE_TYPE_constants


/**
DPE configuration
TODO: use fw info instead
*/
mapping CPC_SteppingMotorConfig_getConfig() {
    mapping config;
    mapping props;

//begin_TAG_SCRIPT_DEVICE_TYPE_deviceConfig
    mappingClear(props);
    props["address"]      = ".ProcessInput.StsReg01";
    props["dataType"]     = CPC_UINT16;
    config["StsReg01"]    = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.StsReg02";
    props["dataType"]     = CPC_UINT16;
    config["StsReg02"]    = props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg01";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg01"] 	= props;

    mappingClear(props);
    props["hasArchive"] 	= true;
    props["address"] 		= ".ProcessInput.evStsReg02";
    props["dataType"] 		= CPC_INT32;
    config["evStsReg02"] 	= props;

    mappingClear(props);
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["address"] 		= ".ProcessInput.PosSt";
    props["dataType"] 		= CPC_FLOAT;
    config["PosSt"] 		   = props;

    mappingClear(props);
    props["hasFormat"]		= false;
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasPvRange"] 	= true;
    props["address"] 		= ".ProcessInput.SpdSt";
    props["dataType"] 		= CPC_UINT16;
    config["SpdSt"] 		  = props;

    mappingClear(props);
    props["hasSmooth"] 		= true;
    props["hasArchive"] 	= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.PosRSt";
    props["dataType"] 		= CPC_FLOAT;
    config["PosRSt"] 		= props;

    mappingClear(props);
    props["hasSmooth"] 		= false;
    props["hasArchive"] 	= true;
    props["hasUnit"] 		= false;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.SpdRSt";
    props["dataType"] 		= CPC_UINT16;
    config["SpdRSt"]       = props;

    mappingClear(props);
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.AuPosRSt";
    props["dataType"] 		= CPC_FLOAT;
    config["AuPosRSt"] 		= props;

    mappingClear(props);
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= false;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.AuSpdRSt";
    props["dataType"] 		= CPC_UINT16;
    config["AuSpdRSt"] 		= props;

    mappingClear(props);
    props["hasFormat"]		= true;
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= true;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.MPosRSt";
    props["dataType"] 		= CPC_FLOAT;
    config["MPosRSt"] 		= props;

    mappingClear(props);
    props["hasSmooth"] 		= true;
    props["hasUnit"] 		= false;
    props["hasFormat"]		= true;
    props["address"] 		= ".ProcessInput.MSpdRSt";
    props["dataType"] 		= CPC_UINT16;
    config["MSpdRSt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.StartISt";
    props["dpe"]			   = "StsReg01";
    props["bitPosition"]	= CPC_StsReg01_STARTIST;
    props["isAlarm"]		   = true;
    config["StartISt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.TStopISt";
    props["dpe"]			   = "StsReg01";
    props["bitPosition"]	= CPC_StsReg01_TSTOPIST;
    props["isAlarm"]		   = true;
    config["TStopISt"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessInput.FuStopISt";
    props["dpe"]		    	= "StsReg02";
    props["bitPosition"]	= CPC_StsReg02_FUSTOPIST;
    props["isAlarm"]		   = true;
    config["FuStopISt"] 	= props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlSt";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALST;
    props["isAlarm"]      = true;
    config["AlSt"]        = props;

    mappingClear(props);
    props["address"]      = ".ProcessInput.AlUnAck";
    props["dpe"]          = "StsReg01";
    props["bitPosition"]  = CPC_StsReg01_ALUNACK;
    config["AlUnAck"]     = props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.ManReg01";
    props["dataType"] 		= CPC_UINT16;
    config["ManReg01"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MPosR";
    props["dataType"] 		= CPC_FLOAT;
    config["MPosR"] 		= props;

    mappingClear(props);
    props["address"] 		= ".ProcessOutput.MSpdR";
    props["dataType"] 		= CPC_UINT16;
    config["MSpdR"] 		= props;

//end_TAG_SCRIPT_DEVICE_TYPE_deviceConfig

    return config;
}




/**
  Initialize the constants required for the import process to improve the performance
*/
void CPC_SteppingMotorConfig_initializeConstants()
{
  mapping mTemp;


  mTemp["DPT_NAME"]                = UN_CONFIG_CPC_STEPPINGMOTOR_DPT_NAME;
  mTemp["LENGTH"]                  = UN_CONFIG_CPC_STEPPINGMOTOR_LENGTH;
  mTemp["UNIT"]                    = UN_CONFIG_CPC_STEPPINGMOTOR_UNIT;
  mTemp["FORMAT"]                  = UN_CONFIG_CPC_STEPPINGMOTOR_FORMAT;
  mTemp["RANGEMAX"]                = UN_CONFIG_CPC_STEPPINGMOTOR_RANGEMAX;
  mTemp["RANGEMIN"]                = UN_CONFIG_CPC_STEPPINGMOTOR_RANGEMIN;
  mTemp["SPDST_RANGEMAX"]          = UN_CONFIG_CPC_STEPPINGMOTOR_SPDST_RANGEMAX;
  mTemp["SPDST_RANGEMIN"]          = UN_CONFIG_CPC_STEPPINGMOTOR_SPDST_RANGEMIN;
  mTemp["DEADBAND"]                = UN_CONFIG_CPC_STEPPINGMOTOR_DEADBAND;
  mTemp["DEADBAND_TYPE"]           = UN_CONFIG_CPC_STEPPINGMOTOR_DEADBAND_TYPE;
  mTemp["ARCHIVE_ACTIVE"]          = UN_CONFIG_CPC_STEPPINGMOTOR_ARCHIVE_ACTIVE;
  mTemp["ARCHIVE_TIME_FILTER"]     = UN_CONFIG_CPC_STEPPINGMOTOR_ARCHIVE_TIME_FILTER;
  mTemp["NORMAL_POSITION"]         = UN_CONFIG_CPC_STEPPINGMOTOR_NORMAL_POSITION;
  mTemp["ADDRESS_STSREG01"]        = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_STSREG01;
  mTemp["ADDRESS_STSREG02"]        = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_STSREG02;
  mTemp["ADDRESS_EVSTSREG01"]      = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_EVSTSREG01;
  mTemp["ADDRESS_EVSTSREG02"]      = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_EVSTSREG02;
  mTemp["ADDRESS_POSST"]           = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_POSST;
  mTemp["ADDRESS_SPDST"]           = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_SPDST;
  mTemp["ADDRESS_POSRST"]          = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_POSRST;
  mTemp["ADDRESS_SPDRST"]          = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_SPDRST;
  mTemp["ADDRESS_AUPOSRST"]        = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_AUPOSRST;
  mTemp["ADDRESS_AUSPDRST"]        = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_AUSPDRST;
  mTemp["ADDRESS_MPOSRST"]         = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MPOSRST;
  mTemp["ADDRESS_MSPDRST"]         = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MSPDRST;
  mTemp["ADDRESS_MANREG01"]        = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MANREG01;
  mTemp["ADDRESS_MPOSR"]           = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MPOSR;
  mTemp["ADDRESS_MSPDR"]           = UN_CONFIG_CPC_STEPPINGMOTOR_ADDRESS_MSPDR;
  mTemp["ADDITIONAL_LENGTH"]       = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_LENGTH;
  mTemp["ADDITIONAL_MASKEVENT"]    = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_MASKEVENT;
  mTemp["ADDITIONAL_PARAMETERS"]   = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_PARAMETERS;
  mTemp["ADDITIONAL_MASTER_NAME"]  = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_MASTER_NAME;
  mTemp["ADDITIONAL_PARENTS"]      = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_PARENTS;
  mTemp["ADDITIONAL_CHILDREN"]     = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_CHILDREN;
  mTemp["ADDITIONAL_TYPE"]         = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_TYPE;
  mTemp["ADDITIONAL_SECOND_ALIAS"] = UN_CONFIG_CPC_STEPPINGMOTOR_ADDITIONAL_SECOND_ALIAS;

  g_mCpcConst["UN_CONFIG_CPC_STEPPINGMOTOR"] = mTemp;
}




/**
Returns the list of parameter names that passed via config line.
*/
dyn_string CPC_SteppingMotorConfig_getParamNames() {
//begin_TAG_SCRIPT_DEVICE_TYPE_getParamNames
    return makeDynString(CPC_PARAMS_MRESTART, CPC_PARAMS_RSTARTFS);
//end_TAG_SCRIPT_DEVICE_TYPE_getParamNames
}

/** Set custom configuration
*/
CPC_SteppingMotorConfig_setCustomConfig(dyn_string dsConfigs, bool hasArchive, dyn_string &exceptionInfo) {
//begin_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.TStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.FuStopISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.StartISt");
    cpcDpFunc_fixAckPropogation(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME], ".ProcessInput.AlSt");
//end_TAG_SCRIPT_DEVICE_TYPE_setCustomConfig
}

/**
Purpose: Export CPC_SteppingMotor Devices

Usage: External function

PVSS manager usage: NG, NV
*/
void CPC_SteppingMotorConfig_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{
  int iLoop, iLen;
  string sObject, sCurrentDp;
  dyn_string dsDpParameters;


  sObject = UN_CONFIG_CPC_STEPPINGMOTOR_DPT_NAME;
  iLen    = dynlen(dsDpList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    dynClear(dsDpParameters);
    sCurrentDp = dsDpList[iLoop];
    if( !dpExists(sCurrentDp) )
    {
      continue;
    }

    unExportDevice_getAllCommonParameters(sCurrentDp, sObject, dsDpParameters);

    cpcExportGenericFunctions_getUnit(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");
    cpcExportGenericFunctions_getFormat(sCurrentDp, dsDpParameters, ".ProcessInput.PosSt");
    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_SteppingMotorConfig_ExportConfig() -> Error exporting PosSt ranges, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getRange(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.SpdSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_SteppingMotorConfig_ExportConfig() -> Error exporting SpdSt ranges, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getDeadband(sCurrentDp, dsDpParameters, exceptionInfo, ".ProcessInput.PosSt");
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_SteppingMotorConfig_ExportConfig() -> Error exporting PosSt deadband, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getArchive(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getDigitalNormalPosition(sCurrentDp,
                                                       sObject,
                                                       makeDynString(".ProcessInput.StartISt",
                                                                     ".ProcessInput.TStopISt",
                                                                     ".ProcessInput.FuStopISt"),
                                                       dsDpParameters);

    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg01",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "StsReg02",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg01", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "evStsReg02", TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "SpdSt",      TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "PosRSt",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "SpdRSt",     TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuPosRSt",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "AuSpdRSt",   TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MSpdRSt",    TRUE,  dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "ManReg01",   FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MPosR",      FALSE, dsDpParameters);
    cpcExportGenericFunctions_processAddress(sCurrentDp, sObject, "MSpdR",      FALSE, dsDpParameters);

    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_1,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_BOOL),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_2,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_ANALOG),
                                                    dsDpParameters);
    cpcExportGenericFunctions_getArchiveNameForDpes(sCurrentDp,
                                                    UN_CONFIG_EXPORT_ARCHIVE_3,
                                                    cpcExportGenericFunctions_getArchivedDpes(sObject, UN_CONFIG_ADDITIONAL_ARCHIVE_EVENT),
                                                    dsDpParameters);

    cpcExportGenericFunctions_getMaskEvent(sCurrentDp, dsDpParameters);

    cpcExportGenericFunctions_getParameters(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_SteppingMotorConfig_ExportConfig() -> Error exporting parameters, due to: " + exceptionInfo[2], "");
      return;
    }

    cpcExportGenericFunctions_getMetainfo(sCurrentDp, dsDpParameters, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "CPC_SteppingMotorConfig_ExportConfig() -> Error exporting metainfo, due to: " + exceptionInfo[2], "");
      return;
    }

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  }

}//  CPC_SteppingMotorConfig_ExportConfig()





/**
Purpose: Export CPC_SteppingMotor Devices for S7_PLC front-end

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC_SteppingMotor_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_SteppingMotorConfig_ExportConfig(dsDpList, exceptionInfo);
}

/**
Purpose: Export CPC_SteppingMotor Devices for _UnPlc front-end

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC_SteppingMotor_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_SteppingMotorConfig_ExportConfig(dsDpList, exceptionInfo);
}

OPCUA_CPC_SteppingMotor_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_SteppingMotorConfig_ExportConfig(dsDpList, exceptionInfo);
}

BACnet_CPC_SteppingMotor_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    CPC_SteppingMotorConfig_ExportConfig(dsDpList, exceptionInfo);
}




void IEC104_CPC_SteppingMotor_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo)
{

  CPC_SteppingMotorConfig_ExportConfig(dsDpList, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "IEC104_CPC_SteppingMotor_ExportConfig() -> Error exporting CPC_SteppingMotor devices, due to: " + exceptionInfo[2], "");
    return;
  }

}//  IEC104_CPC_SteppingMotor_ExportConfig()




//@}
