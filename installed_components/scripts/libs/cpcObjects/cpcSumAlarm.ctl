/**@file

// cpcSumAlarm.ctl
This library contains the widget, faceplate, etc. functions of SummaryAlarm.

@par Creation Date
  16/11/2020

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)
  Jonas Arroyo (BE-ICS)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Returns the list of Summary Alarm DPE with alarm config that can be acknowledged

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName, string,     input, device name DP name
@param sDpType,     string,     input, device type
@param dsNeedAck,   dyn_string, output, the list of DPE
*/
void CPC_SumAlarm_AcknowledgeAlarm(string sDeviceName, string sDpType, dyn_string &dsNeedAck)
{
  dsNeedAck = makeDynString("ProcessInput.SumAlarm");
}





/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName,  string,     input,  device name
@param sDeviceType,  string,     input,  device type
@param dsReturnData, dyn_string, output, return data, array of 5 strings
*/
void CPC_SumAlarm_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  int iLoop, iLen;
  string sTime, sText, sAlias, sState;
  dyn_string dsDpe;

  dpGet(sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._text",   sText,
        sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._alerts", sTime);

  dsReturnData[1] = sTime;
  dsReturnData[2] = sText;
}



/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_SumAlarm_MenuConfiguration(string sDeviceName, string sDpType, dyn_string dsAccessOk, dyn_string &dsMenuList)
{
  dyn_string dsMenuConfig;


  dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT,
                               UN_POPUPMENU_ACK_TEXT,
                               CPC_POPUPMENU_DIAG_INFO_TEXT,
                               //CPC_POPUPMENU_MASK_IST_TEXT,
                               UN_POPUPMENU_TREND_TEXT,
                               //UN_POPUPMENU_BLOCK_PLC_TEXT,
                               UN_POPUPMENU_FACEPLATE_TEXT);

  cpcGenericObject_addUnicosActionToMenu       (sDeviceName, sDpType, dsMenuConfig, dsAccessOk, dsMenuList);
  cpcGenericObject_addDefaultUnicosActionToMenu(sDeviceName, sDpType, dsMenuConfig, dsAccessOk, dsMenuList);
  unGenericObject_addTrendActionToMenu         (sDeviceName, sDpType, dsMenuConfig, dsAccessOk, dsMenuList);

}




/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName,    string,     input, device DP name
@param sDpType,        string,     input, device type
@param dsMenuList,     dyn_string, input, the access control
@param iMenuAnswer,    int,        input, selected menu value
*/
CPC_SumAlarm_HandleMenu(string sDeviceName, string sDpType, dyn_string dsMenuList, int iMenuAnswer)
{
  dyn_string exceptionInfo;


  cpcGenericObject_HandleUnicosMenu(sDeviceName, sDpType, dsMenuList, iMenuAnswer, exceptionInfo);
  if( dynlen(exceptionInfo) > 0 )
  {
    unSendMessage_toExpertException("Right Click", exceptionInfo);
  }
}





/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName input, the device name

*/
CPC_SumAlarm_WidgetInitStatics(string sDeviceName)
{
  int iAlertType, iRes;
  string sDescription;


  iRes = dpGet(sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._type", iAlertType);

  if( (iRes == 0) && (iAlertType == DPCONFIG_SUM_ALERT) )
  {
    g_bBoolAlert = TRUE;
  }
  else
  {
    g_bBoolAlert = FALSE;
  }

}





/** Returns the list of DigitalAlarm DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
void CPC_SumAlarm_WidgetDPEs(string sDeviceName, dyn_string &dsDpes)
{
  dynAppend(dsDpes, sDeviceName + ".eventMask");


  if( g_bBoolAlert )
  {
    dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._active");
    dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._act_state");
    dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_possible");
    dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._act_state_text");
    dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._dp_list");
    dynAppend(dsDpes, sDeviceName + ".ProcessInput.ForceAlarm");
    dynAppend(dsDpes, sDeviceName + ".ProcessInput.ForceAlarmEnable");
  }
}



/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_SumAlarm_WidgetAnimation(dyn_string dsDpes, dyn_anytype daValues, string sWidgetType)
{
  bool bSystemIntegrityAlarmEnabled, bLocked, bIsCommentsActive, bAlarmActive, bAlarmAcknowledable, bIsAlarmActive, bLockVisible, bFrontEndSystemIntegrityStatus, bForceAlarm, bForceAlarmEnabled, bInvalid;
  int iSystemIntegrityAlarmValue, iMask, iAlarmState, iLen, iLoop;
  string sSelectedManager, sAlarmText, sBody1Color, sBody1BColor, sColorSelect, sWarningColor, sWarningLetter, sAlarmTypeLetter, sAlarmTypeColor, sAlarmTextLetter, sAlarmTextColor, sDpeList;
  dyn_string dsDpeList;


  // Initialize variables
  bIsAlarmActive   = FALSE;
  bInvalid         = FALSE;
  sBody1Color      = "unDataNotValid";
  sBody1BColor     = "";
  sColorSelect     = "";
  sWarningColor    = "";
  sWarningLetter   = "";
  sAlarmTypeLetter = "";
  sAlarmTypeColor  = "";
  sAlarmTextLetter = "";
  sAlarmTextColor  = "";


  // System Integrity
  iSystemIntegrityAlarmValue   = daValues[1];
  bSystemIntegrityAlarmEnabled = daValues[2];

  // Device Selection
  bLocked            = daValues[3];
  sSelectedManager   = daValues[4];
  bIsCommentsActive  = daValues[5];
  iMask              = daValues[6];

  // Alert Values
  bAlarmActive        = daValues[7];
  iAlarmState         = daValues[8];
  bAlarmAcknowledable = daValues[9];
  sAlarmText          = daValues[10];
  sDpeList            = daValues[11];
  bForceAlarm         = daValues[12];
  bForceAlarmEnabled  = daValues[13];


  // Check if summary alarm is invalid, checking if the DPE list exists
  fwGeneral_dynStringToString(dsDpeList, sDpeList, ",");
  iLen = dynlen(dsDpeList);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( dpExists(dsDpeList[iLoop]) == FALSE )
    {
      bInvalid = TRUE;
      return;
    }
  }


  // Get if alarm is defined and activated
  if( g_bBoolAlert )
  {
    bIsAlarmActive = bAlarmActive;
  }
  if( !bIsAlarmActive )
  {
    sAlarmTextLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
    sAlarmTextColor  = "unAlarmMasked";
  }


  bFrontEndSystemIntegrityStatus = unFaceplate_connectionValid(dsDpes);
  if( bFrontEndSystemIntegrityStatus )
  {
    if( iAlarmState != 0 )
    {
      // Alarm triggered
      sBody1Color = "cpcColor_Alarm_Bad";
    }
    else
    {
      // Alarm OK
      sBody1Color = "unAlarm_Ok";
    }

    if( bAlarmAcknowledable )
    {
      sBody1Color = "cpcColor_Alarm_AlarmNotAck";
    }

    if( iAlarmState != 0 )
    {
      sAlarmTypeLetter = CPC_WIDGET_ALARM;
      sAlarmTypeColor  = "cpcColor_Alarm_Bad";
      sBody1BColor     = sBody1Color;
    }

    // Force alarm enabled, and force alarm == TRUE
    if( bForceAlarmEnabled )
    {
        // Alarm forced and triggered
        sAlarmTextLetter = CPC_WIDGET_TEXT_ALARM_BLOCKED;
        sAlarmTextColor  = "unAlarmMasked";
        sBody1Color      = "unAlarmMasked";
        sBody1BColor     = "";
    }


    // Warning animation
    unGenericObject_WidgetWarningAnimation(bForceAlarmEnabled,
                                           makeDynInt(0),
                                           makeDynString(CPC_WIDGET_TEXT_POSW),
                                           sWarningLetter,
                                           sWarningColor);
  }


  cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled,
                                            iSystemIntegrityAlarmValue,
                                            bInvalid,
                                            sWarningLetter,
                                            sWarningColor);

  unGenericObject_WidgetSelectAnimation(bLocked,
                                        sSelectedManager,
                                        sColorSelect,
                                        bLockVisible);

  if( g_bSystemConnected )
  {
    setMultiValue("WarningText",      "text",    sWarningLetter,
                  "WarningText",      "foreCol", sWarningColor,

                  "AlarmType",        "text",    sAlarmTypeLetter,
                  "AlarmType",        "foreCol", sAlarmTypeColor,	//	"A"

                  "AlarmText",        "text",    sAlarmTextLetter,
                  "AlarmText",        "foreCol", sAlarmTextColor,	//	"B"

                  "ControlStateText", "text",    "",
                  "SelectArea",       "foreCol", sColorSelect,
                  "LockBmp",          "visible", bLockVisible,

                  "Body1",            "fill",   "[solid]",
                  "Body1",            "foreCol", sBody1Color,
                  "Body1",            "backCol", sBody1BColor,

                  "WidgetArea",       "visible", TRUE,
                  "eventState",       "visible", iMask == 0,
                  "commentsButton",   "visible", bIsCommentsActive);
  }
}





/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
void CPC_SumAlarm_WidgetDisconnection(string sWidgetType)
{
  setMultiValue("Body1",       "foreCol", "unDataNoAccess",
                "Body1",       "backCol", "unWidget_Background",
                "Body1",       "fill",    "[solid]",
                "WarningText", "text",    "",
                "WidgetArea",  "visible", FALSE,
                "AlarmType",   "text",    "",
                "AlarmText",   "text",    "",
                "eventState",  "visible", FALSE);
}





/** Return button configuration including access level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
mapping CPC_SumAlarm_ButtonConfig(string sDeviceName)
{
  bool bAcknowleable;
  int iAnswer;
  mapping mButtons;
  dyn_string exceptionInfo;
  dyn_errClass deError;


  mButtons[UN_FACEPLATE_BUTTON_SELECT]      = UN_ACCESS_RIGHTS_OPERATOR;
  mButtons[UN_FACEPLATE_BUTTON_FORCED_MODE] = UN_ACCESS_RIGHTS_EXPERT;

  // Check if alarm is acknowledgeable
  iAnswer = dpGet( sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_possible", bAcknowleable);
  if( iAnswer != 0 )
  {
    fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarm_ButtonConfig() getting if alarm is acknowleable: " + sDeviceName + ".ProcessInput.SumAlarm", "");
    fwExceptionHandling_display(exceptionInfo);
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarm_ButtonConfig() getting if alarm is acknowleable: " + sDeviceName + ".ProcessInput.SumAlarm, due to: " + getErrorText(deError), "");
      fwExceptionHandling_display(exceptionInfo);
    }
    else
    {
      if( bAcknowleable )
      {
        mButtons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
      }
      else
      {
        mButtons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_NOONE;
      }
    }
  }

  //mButtons[UN_FACEPLATE_MASKEVENT]                 = UN_ACCESS_RIGHTS_EXPERT;
  //mButtons[UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION] = UN_ACCESS_RIGHTS_EXPERT;

  return mButtons;
}





/** Configure the list of dpes that needs for buttons animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName,  string,     input, the name of device
@param dsDpes,       dyn_string, input, the list of dpes to connect
*/
void CPC_SumAlarm_ButtonDPEs(string sDeviceName, dyn_string &dsDpes)
{
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_possible");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._active");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.ForceAlarmEnable");
}




/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName,   string,     input, the device DP name
@param sDpType,       string,     input, the device type
@param dsUserAccess,  dyn_string, input, list of allowed action on the device
@param dsData,        dyn_string, input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
void CPC_SumAlarm_ButtonSetState(string sDeviceName, string sDpType, dyn_string dsUserAccess, dyn_string dsData)
{
  bool bLocked, sLocalManager, bSelected, bButtonEnabled, bActive, bAcknowledgeable, bForced;
  int iLoop, iLen;
  string sSelectedUiManager, sSelectedState, sButtonText;
  dyn_string dsButtons;


  bLocked            = dsData[1];
  sSelectedUiManager = dsData[2];
  bAcknowledgeable   = dsData[3];
  bActive            = dsData[4];
  bForced            = dsData[5];


  // Selection state of the device
  sSelectedState     = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUiManager);
  if( sSelectedState == "S" )
  {
    bSelected = TRUE;
  }
  else
  {
    bSelected = FALSE;
  }


  dsButtons = unSimpleAnimation_ButtonList(sDeviceName);
  iLen      = dynlen(dsButtons);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    if( dynContains(dsUserAccess, dsButtons[iLoop]) > 0 )
    {
      bButtonEnabled = TRUE;
    }
    else
    {
      bButtonEnabled = FALSE;
    }


    switch( dsButtons[iLoop] )
    {
      case UN_FACEPLATE_BUTTON_ACK_ALARM:
        bButtonEnabled = bButtonEnabled && bSelected && bActive && bAcknowledgeable;
        break;

      case UN_FACEPLATE_BUTTON_FORCED_MODE:
        bButtonEnabled = bButtonEnabled && bSelected;
        if( bForced )
        {
          sButtonText = "Remove forced";
        }
        else
        {
          sButtonText = "Forced Mode";
        }

        setValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_FORCED_MODE, "text", sButtonText);
        break;

      default:
        break;
    }

    if( dsButtons[iLoop] == UN_FACEPLATE_BUTTON_SELECT )
    {
      unGenericObject_ButtonAnimateSelect(sSelectedState, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
    }
    else
    {
      cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[iLoop], bButtonEnabled);
    }
  }
}





/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName string, input, the device name

*/
void CPC_SumAlarm_FaceplateInitStatics(string sDeviceName)
{
  bool bSystemIntegrityEnabled;
  int iSystemIntegrityAlarm, iLoop, iLen;
  string sSystemName, sFrontEnd, sSystemIntegrity, sAlias, sBackColor;
  dyn_string exceptionInfo, dsAlarmTypes, dsParents, dsDpeListFiltered;
  dyn_errClass deError;


  sSystemName      = unGenericDpFunctions_getSystemName(sDeviceName);
  sFrontEnd        = unGenericObject_GetPLCNameFromDpName(sDeviceName);
  sSystemIntegrity = sSystemName + c_unSystemAlarm_dpPattern + DS_pattern + sFrontEnd;

  if( dpGet(sSystemIntegrity + ".enabled", bSystemIntegrityEnabled,
            sSystemIntegrity + ".alarm",   iSystemIntegrityAlarm,
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._active",               g_mParameters["Alarm_Active"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_possible",         g_mParameters["Alarm_Acknowledgeable"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._act_state_text",       g_mParameters["Alarm_Message"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._act_state_color",      g_mParameters["Alarm_Message_BackColor"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_has_prio",         g_mParameters["Alarm_Option_Priority"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._order",                g_mParameters["Alarm_Option_Order"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_deletes",          g_mParameters["Alarm_Option_AckDelete"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._non_ack",              g_mParameters["Alarm_Option_NonAck"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._came_ack",             g_mParameters["Alarm_Option_CameAck"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._pair_ack",             g_mParameters["Alarm_Option_PairAck"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._both_ack",             g_mParameters["Alarm_Option_BothAck"],
            sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._dp_list",              g_mParameters["Alarm_DpeList"],
            sDeviceName + ".ProcessInput.ForceAlarmEnable",                           g_mParameters["Force_Enable"],
            sDeviceName + ".ProcessInput.ForceAlarm",                                 g_mParameters["Force_Setting"])        != 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarm_FaceplateInitStatics() animating faceplace for device: " + sDeviceName, "");
    fwExceptionHandling_display(exceptionInfo);
    return;
  }
  else
  {
    deError = getLastError();
    if( dynlen(deError) > 0 )
    {
      fwException_raise(exceptionInfo, "ERROR", "Error in CPC_SumAlarm_FaceplateInitStatics() animating faceplace for device: " + sDeviceName + ", due to: " + getErrorText(deError) , "");
      fwExceptionHandling_display(exceptionInfo);
      return;
    }
  }


  // Translate to aliases
  iLen = dynlen(g_mParameters["Alarm_DpeList"]);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    sAlias = dpGetAlias(g_mParameters["Alarm_DpeList"][iLoop]);
    if( sAlias == "" )
    {
      dynAppend(dsDpeListFiltered, dpSubStr(g_mParameters["Alarm_DpeList"][iLoop], DPSUB_DP_EL));
    }
    else
    {
      dynAppend(dsDpeListFiltered, sAlias);
    }
  }


  if(g_bSystemConnected)
  {
    setMultiValue("ALARM_MESSAGE.display", "text",        g_mParameters["Alarm_Message"],
                  "ALARM_MESSAGE.display", "backCol",     g_mParameters["Alarm_Message_BackColor"],
                  "COMBO_BOX_ORDER",       "selectedPos", g_mParameters["Alarm_Option_Priority"]+1,
                  "COMBO_BOX_ORDER",       "backCol",     "_3DFace",
                  "COMBO_BOX_PRIORITY",    "selectedPos", g_mParameters["Alarm_Option_Order"]+1,
                  "COMBO_BOX_PRIORITY",    "backCol",     "_3DFace",
                  "TABLE_DPE",             "appendLines", dynlen(dsDpeListFiltered), "NAME", dsDpeListFiltered);

    // Animate table colot
    if( !bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value) )
    {
      sBackColor = "unDataNotValid";
    }
    else
    {
      sBackColor = "_3DFace";
    }
    setMultiValue("TABLE_DPE", "adjustColumn", 0,
                  "TABLE_DPE", "backCol",      sBackColor);


    // Animate box state
    unGenericObject_ColorBoxAnimate("ALARM_ACTIVE",          g_mParameters["Alarm_Active"],           TRUE, "cpcColor_Faceplate_Status", (!bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("ALARM_ACKNOWLEDGEABLE", g_mParameters["Alarm_Acknowledgeable"],  TRUE, "unFaceplate_AlarmNotAck",   (!bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_DELETES",        g_mParameters["Alarm_Option_AckDelete"], TRUE, "cpcColor_Faceplate_Status", (!bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_NOT",            g_mParameters["Alarm_Option_NonAck"],    TRUE, "cpcColor_Faceplate_Status", (!bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_CAME",           g_mParameters["Alarm_Option_CameAck"],   TRUE, "cpcColor_Faceplate_Status", (!bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_COUPLE",         g_mParameters["Alarm_Option_PairAck"],   TRUE, "cpcColor_Faceplate_Status", (!bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_CAME_WENT",      g_mParameters["Alarm_Option_BothAck"],   TRUE, "cpcColor_Faceplate_Status", (!bSystemIntegrityEnabled || (iSystemIntegrityAlarm > c_unSystemIntegrity_no_alarm_value)) );
  }
  else
  {
    // disconnection animation
    unGenericObject_ColorBoxDisconnect(makeDynString("ALARM_ACTIVE"));
    unGenericObject_ColorBoxDisconnect(makeDynString("ALARM_ACKNOWLEDGEABLE"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_DELETES"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_NOT"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_CAME"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_COUPLE"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_CAME_WENT"));
    setMultiValue("ALARM_MESSAGE.display", "backCol", "unDataNoAccess",
                  "COMBO_BOX_ORDER",       "backCol", "unDataNoAccess",
                  "COMBO_BOX_PRIORITY",    "backCol", "unDataNoAccess",
                  "TABLE_DPE",             "backCol", "unDataNoAccess");
  }

}





/** Returns the list of DigitalAlarm DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName    string,     input, the device name
@param dsDpes,        dyn_string, output, dpe list

*/
CPC_SumAlarm_FaceplateDPEs(string sDeviceName, dyn_string &dsDpes)
{
  string sSystemName, sFrontEnd, sSystemIntegrity;


  sSystemName      = unGenericDpFunctions_getSystemName(sDeviceName);
  sFrontEnd        = unGenericObject_GetPLCNameFromDpName(sDeviceName);
  sSystemIntegrity = sSystemName + c_unSystemAlarm_dpPattern + DS_pattern + sFrontEnd;

  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._active");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_possible");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._act_state_text");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._act_state_color");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_has_prio");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._order");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._ack_deletes");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._non_ack");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._came_ack");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._pair_ack");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._both_ack");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_alert_hdl.._dp_list");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.ForceAlarmEnable");
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.ForceAlarm");
}




/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDpes    input, dyn_string,  The dpe names
@param daValues  input, dyn_anytype, The dpe values

*/
CPC_SumAlarm_FaceplateStatusAnimationCB(dyn_string dsDpes, dyn_anytype daValues)
{
  const int iSYS_INT_ALARM       = 1;
  const int iSYS_INT_ENABLE      = 2;
  const int iALARM_ACTIVE        = 3;
  const int iALARM_ACK           = 4;
  const int iALARM_MESSAGE       = 5;
  const int iALARM_MESSAGE_COLOR = 6;
  const int iALARM_PRIORITY      = 7;
  const int iALARM_ORDER         = 8;
  const int iOPT_ACK_DELETE      = 9;
  const int iOPT_NONT_ACK        = 10;
  const int iOPT_CAME_ACK        = 11;
  const int iOPT_PAIR_ACK        = 12;
  const int iOPT_BOTH_ACK        = 13;
  const int iDPE_LIST            = 14;
  const int iFORCE_ENABLE        = 15;
  const int iFORCE_VALUE         = 16;

  int iLen, iLoop;
  string sAlias, sBackColor;
  dyn_string dsDpeListFiltered;

  // Translate to aliases
  iLen = dynlen(daValues[iDPE_LIST]);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    sAlias = dpGetAlias(daValues[iDPE_LIST][iLoop]);
    if( sAlias == "" )
    {
      dynAppend(dsDpeListFiltered, dpSubStr(daValues[iDPE_LIST][iLoop], DPSUB_DP_EL));
    }
    else
    {
      dynAppend(dsDpeListFiltered, sAlias);
    }
  }


  if(g_bSystemConnected)
  {
    setValue("TABLE_DPE", "deleteAllLines");
    setMultiValue("ALARM_MESSAGE.display", "text",        daValues[iALARM_MESSAGE],
                  "ALARM_MESSAGE.display", "backCol",     daValues[iALARM_MESSAGE_COLOR],
                  "COMBO_BOX_ORDER",       "selectedPos", daValues[iALARM_PRIORITY]+1,
                  "COMBO_BOX_ORDER",       "backCol",     "_3DFace",
                  "COMBO_BOX_PRIORITY",    "selectedPos", daValues[iALARM_ORDER]+1,
                  "COMBO_BOX_PRIORITY",    "backCol",     "_3DFace",
                  "TABLE_DPE",             "appendLines", dynlen(dsDpeListFiltered), "NAME", dsDpeListFiltered);

    // Animate table colot
    if( !daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value) )
    {
      sBackColor = "unDataNotValid";
    }
    else
    {
      sBackColor = "_3DFace";
    }
    setMultiValue("TABLE_DPE", "adjustColumn", 0,
                  "TABLE_DPE", "backCol",      sBackColor);


    // Animate box state
    unGenericObject_ColorBoxAnimate("ALARM_ACTIVE",          daValues[iALARM_ACTIVE],   TRUE, "cpcColor_Faceplate_Status", (!daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("ALARM_ACKNOWLEDGEABLE", daValues[iALARM_ACK],      TRUE, "unFaceplate_AlarmNotAck",   (!daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_DELETES",        daValues[iOPT_ACK_DELETE], TRUE, "cpcColor_Faceplate_Status", (!daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_NOT",            daValues[iOPT_NONT_ACK],   TRUE, "cpcColor_Faceplate_Status", (!daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_CAME",           daValues[iOPT_CAME_ACK],   TRUE, "cpcColor_Faceplate_Status", (!daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_COUPLE",         daValues[iOPT_PAIR_ACK],   TRUE, "cpcColor_Faceplate_Status", (!daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value)) );
    unGenericObject_ColorBoxAnimate("OPTION_CAME_WENT",      daValues[iOPT_BOTH_ACK],   TRUE, "cpcColor_Faceplate_Status", (!daValues[iSYS_INT_ENABLE] || (daValues[iSYS_INT_ALARM] > c_unSystemIntegrity_no_alarm_value)) );
  }
  else
  {
    // disconnection animation
    unGenericObject_ColorBoxDisconnect(makeDynString("ALARM_ACTIVE"));
    unGenericObject_ColorBoxDisconnect(makeDynString("ALARM_ACKNOWLEDGEABLE"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_DELETES"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_NOT"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_CAME"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_COUPLE"));
    unGenericObject_ColorBoxDisconnect(makeDynString("OPTION_CAME_WENT"));
    setMultiValue("ALARM_MESSAGE.display", "backCol", "unDataNoAccess",
                  "COMBO_BOX_ORDER",       "backCol", "unDataNoAccess",
                  "COMBO_BOX_PRIORITY",    "backCol", "unDataNoAccess",
                  "TABLE_DPE",             "backCol", "unDataNoAccess");
  }

}



/** Returns the list of Summary Alarm alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName,  string,     input,  the device name
@param sDpes,        dyn_string, output, dpe list

*/
void CPC_SumAlarm_WidgetLockDPEs(string sDeviceName, dyn_string &dsDpes)
{
  dynAppend(dsDpes, sDeviceName + ".ProcessInput.SumAlarm:_lock._alert_hdl._locked");
}





//@}
