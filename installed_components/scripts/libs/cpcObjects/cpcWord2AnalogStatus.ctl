/**@file

// cpcWord2AnalogStatus.ctl
This library contains the widget, faceplate, etc. functions of Word2AnalogStatus.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_Word2AnalogStatus_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    CPC_AnalogStatus_ObjectListGetValueTime(deviceName, deviceType, dsReturnData);
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_Word2AnalogStatus_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(CPC_POPUPMENU_DIAG_INFO_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_Word2AnalogStatus_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_Word2AnalogStatus_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    g_sPosStFormat = dpGetFormat(deviceName + ".ProcessInput.PosSt");
    g_sPosStUnit = dpGetUnit(deviceName + ".ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of Word2AnalogStatus DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_Word2AnalogStatus_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_Word2AnalogStatus_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];

    float fPosSt = values[6];
    bool bPosStInvalid = values[7];

    string sBody1Text, sBody1Color;
    string sWarningLetter, sWarningColor;

    sBody1Text = cpcGenericObject_FormatValueWithUnit(fPosSt, g_sPosStFormat, g_sPosStUnit);
    sBody1Color = bPosStInvalid ? "unDataNotValid" : "cpcColor_Widget_Status";

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue,
            bPosStInvalid,
            sWarningLetter, sWarningColor);

    if (g_bSystemConnected)
        setMultiValue("Body1", "text", sBody1Text, "Body1", "foreCol", sBody1Color,
                      "WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
                      "WidgetArea", "visible", true,
                      "commentsButton", "visible", isCommentsActive);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_Word2AnalogStatus_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background",
                  "WarningText", "text", "", "WidgetArea", "visible", false);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

//@}