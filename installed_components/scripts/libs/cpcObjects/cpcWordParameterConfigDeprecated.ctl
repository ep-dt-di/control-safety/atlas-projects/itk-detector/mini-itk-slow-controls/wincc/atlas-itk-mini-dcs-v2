/*
  Use this place to define any unexisting constants or functions than you need for the device implementation.

@Deprecated 2018-08-01

*/
//begin_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions
bool CPC_WordParameterConfig_isWithText(string posStDescription) {

  FWDEPRECATED();


    posStDescription = strrtrim(posStDescription);
    return posStDescription != "" && posStDescription != "-"; // as legacy empty description were not an empty string but "-"
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomConfigFunctions