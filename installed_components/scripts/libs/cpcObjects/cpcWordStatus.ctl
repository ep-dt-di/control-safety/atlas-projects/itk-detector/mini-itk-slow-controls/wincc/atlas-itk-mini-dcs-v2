/**@file

// cpcWordStatus.ctl
This library contains the widget, faceplate, etc. functions of WordStatus.

@par Creation Date
  dd/mm/yyyy

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  the author (DEP-GROUP)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved
*/
//@{


/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName  input, device name
@param deviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
CPC_WordStatus_ObjectListGetValueTime(string deviceName, string deviceType, dyn_string &dsReturnData) {
//begin_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
    CPC_AnalogStatus_ObjectListGetValueTime(deviceName, deviceType, dsReturnData);
//end_TAG_SCRIPT_DEVICE_TYPE_ObjectListGetValueTime
}

/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
CPC_WordStatus_MenuConfiguration(string deviceName, string dpType, dyn_string dsAccessOk, dyn_string &menuList) {
//begin_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
    dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_MASK_POSST_TEXT, UN_POPUPMENU_ACK_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, CPC_POPUPMENU_DIAG_INFO_TEXT, UN_POPUPMENU_TREND_TEXT);

    cpcGenericObject_addUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    cpcGenericObject_addDefaultUnicosActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
    unGenericObject_addTrendActionToMenu(deviceName, dpType, dsMenuConfig, dsAccessOk, menuList);
//end_TAG_SCRIPT_DEVICE_TYPE_MenuConfiguration
}

CPC_WordStatus_AcknowledgeAlarm(string deviceName, string dpType, dyn_string &dsNeedAck) {
//begin_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
    dsNeedAck = makeDynString("ProcessInput.PosSt");
//end_TAG_SCRIPT_DEVICE_TYPE_AcknowledgeAlarm
}

/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param dpType input, device type
@param menuList input, the access control
@param menuAnswer input, selected menu value
*/
CPC_WordStatus_HandleMenu(string deviceName, string dpType, dyn_string menuList, int menuAnswer) {
    dyn_string exceptionInfo;

//begin_TAG_SCRIPT_DEVICE_TYPE_HandleMenu
    cpcGenericObject_HandleUnicosMenu(deviceName, dpType, menuList, menuAnswer, exceptionInfo);
//end_TAG_SCRIPT_DEVICE_TYPE_HandleMenu

    if (dynlen(exceptionInfo) > 0) {
        unSendMessage_toExpertException("Right Click", exceptionInfo);
    }
}

/** Init static values which are used in widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name

*/
CPC_WordStatus_WidgetInitStatics(string deviceName) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
    dyn_string exceptionInfo;
    string sDp = deviceName + ".ProcessInput.PosSt";
    g_sPosStFormat = dpGetFormat(sDp);
    g_sPosStUnit = dpGetUnit(sDp);
    cpcGenericObject_StringToMapping(cpcGenericDpFunctions_getMapping(deviceName, ".ProcessInput.PosSt"), g_mLabels);
    if (g_sWidgetType == "WordStatusBit" || g_sWidgetType == "WordStatusBitBig") { // fill with defaults
        for (int i = 0; i < 16; i++) {
            if (!mappingHasKey(g_mLabels, (string)i)) {
                g_mLabels[(string)i] = "N/A";
            }
        }

        g_mLabels[CPC_PARAMS_NO_BIT]    = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_NO_BIT, exceptionInfo, "No Status");
        g_mLabels[CPC_PARAMS_MULTI_BIT] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_MULTI_BIT, exceptionInfo, "Multiple Status");
    }

    int iAlertType;
    int iRes = dpGet(sDp + ":_alert_hdl.._type", iAlertType);
    g_bBoolAlert = ((iRes >= 0) && (iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL));
    g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] = cpcGenericDpFunctions_getDeviceProperty(deviceName, CPC_PARAMS_DISPLAY_FORMAT, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
      exceptionInfo[2] = "Error in CPC_WordStatus_WidgetInitStatics() with device: " + deviceName + " getting the display parameters properties: " + exceptionInfo[2];
      fwExceptionHandling_display(exceptionInfo);
      return;
    }

//end_TAG_SCRIPT_DEVICE_TYPE_WidgetInitStatics
}

/** Returns the list of WordStatus alert handler's locker dpes

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_WordStatus_WidgetLockDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_lock._alert_hdl._locked");
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetLockDPEs
}

/** Returns the list of WordStatus DPEs which should be connected on widget animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_WordStatus_WidgetDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    if (g_bBoolAlert) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_text");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_WordStatus_WidgetAnimation(dyn_string dpes, dyn_anytype values, string widgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4];
    bool isCommentsActive = values[5];

    int iPosSt = values[6];
    bool bPosStInvalid = values[7];

    string sWarningLetter, sWarningColor;
    int iIndex, iLength, iBitCounter, iBitPos;
    string sBody1Text, sBody1Color = "cpcColor_Widget_Status";
    string alarmLetter = CPC_WIDGET_TEXT_ALARM_DOES_NOT_EXIST, alarmColor = "unAlarmDoesNotExist", selectColor, actText;

    bool isAlarmActive, isUnAck;
    int iActState;
    if (g_bBoolAlert) {
        isAlarmActive = values[8];
        iActState = values[9];
        isUnAck = values[10];
        actText = values[11];
    }

    if (widgetType == "WordStatusBit") {
        for (int j = 0; j < 16; j++) {
            if (getBit(iPosSt, j) == 1) {
                if (iBitCounter == 0) {
                    iBitPos = j;
                }
                iBitCounter = iBitCounter + 1;
            }
        }
        if (iBitCounter == 0) {
            sBody1Text = g_mLabels[CPC_PARAMS_NO_BIT];
        } else if (iBitCounter == 1) {
            sBody1Text = CPC_WordStatus_lookupLabel(g_mLabels, iBitPos);
        } else {
            sBody1Text = g_mLabels[CPC_PARAMS_MULTI_BIT];
        }
    } else {
        sBody1Text = mappingHasKey(g_mLabels, (string)iPosSt) ? g_mLabels[(string)iPosSt] : iPosSt + " " + g_sPosStUnit;
    }

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue, bPosStInvalid, sWarningLetter, sWarningColor);

    if (g_bSystemConnected) {
        setMultiValue("WarningText",    "text",     sWarningLetter,
                      "WarningText",    "foreCol",  sWarningColor,
                      "commentsButton", "visible",  isCommentsActive);
        if (shapeExists("WidgetArea")) {
            setValue("WidgetArea", "visible", true);
        }
    }

    bool lockVisible;
    unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, lockVisible);

    if (g_bBoolAlert) {      // Alarm config exists
        if (isAlarmActive) { // Alarm active
            if ((iActState == DPATTR_ALERTSTATE_APP_NOT_ACK) || (iActState == DPATTR_ALERTSTATE_APP_ACK)) {
                if(strpos(actText, "Warning") == 0 || strpos(actText, "Low Warning") == 0) { // is warning
                    sBody1Color = "cpcColor_Widget_Warning";
                    alarmColor = "cpcColor_Widget_Warning";
                } else {
                    sBody1Color = "unFaceplate_AlarmActive";
                    alarmColor = "unFaceplate_AlarmActive";
                }
                alarmLetter = CPC_WIDGET_TEXT_ALARM_ALST;
            }
            if(isUnAck) {   // in case alarm not acknowledged, override color
                sBody1Color = "unWidget_AlarmNotAck";
                alarmColor = "unWidget_AlarmNotAck";
            }
        } else {             // Alarm masked
            alarmLetter = CPC_WIDGET_TEXT_ALARM_MASKED;
            alarmColor = "unAlarmMasked";
        }
    }

    if (bPosStInvalid) {
        sBody1Color = "unDataNotValid";
    }

    if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "HEX") {
        sprintf(sBody1Text, "%X", (int)sBody1Text);
        sBody1Text = "0x" + sBody1Text;
    } else if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "hex") {
        sprintf(sBody1Text, "%x", (int)sBody1Text);
        sBody1Text = "0x" + sBody1Text;
    }

    if (widgetType == "WordStatusBitBig") {
        for (int j = 0; j < 16; j++) {
            string sTempBodyText = CPC_WordStatus_lookupLabel(g_mLabels, j);
            string bodyRef = "Body" + j;
            setMultiValue(bodyRef, "text",    sTempBodyText,
                          bodyRef, "fill",    "[solid]",
                          bodyRef, "foreCol", sBody1Color,
                          bodyRef, "backCol", "unWidget_Background");
            unGenericObject_ColoredSquareAnimate("bit" + j, getBit(iPosSt, j), true, "cpcColor_Widget_Status", "cpcColor_Widget_Status", false);
        }
    } else if (widgetType == "WSStep") {

        // set displayed label - not displaying current WS value, but "active value"!!
        if(g_iLabelMode == 2) {
            sBody1Text = g_iValue;
        } else {
            sBody1Text = mappingHasKey(g_mLabels, (string)g_iValue) ? g_mLabels[(string)g_iValue] : g_iValue + " " + g_sPosStUnit;
            if(g_iLabelMode == 3) {
                sBody1Text = g_iValue + ":" + sBody1Text;
            }
        }

        // set sBodyColor depending on WS status
        if(!bPosStInvalid) {
            sBody1Color = (iPosSt == g_iValue) ? g_sActiveColor : g_sPassiveColor;
        }

        setMultiValue("init",     "visible",  g_xInitStep,
                      "Body1",    "text",     sBody1Text,
                      "Body1",    "foreCol",  "unWidget_Background",
                      "Body2",    "backCol",  sBody1Color);

    } else if (widgetType == "WSTransition") {
        // set sBodyColor depending on WS status
        if(!bPosStInvalid) {
            bool iBitOk = g_iBit >= 0 && g_iBit <= 31;
            if(iBitOk) {
                sBody1Color = getBit(iPosSt, g_iBit) ? g_sActiveColor : g_sPassiveColor;
            } else if (!iBitOk) {
                sBody1Color = "{200,200,200}";
            }
        }

        setMultiValue("Body1",  "backCol",  sBody1Color,
                      "Body1",  "foreCol",  sBody1Color);

    } else {
        setMultiValue("Body1",  "text",     sBody1Text,
                      "Body1",  "fill",     "[solid]",
                      "Body1",  "foreCol",  sBody1Color,
                      "Body1",  "backCol",  "unWidget_Background");
    }

    setMultiValue("AlarmText",  "text",     alarmLetter,
                  "AlarmText",  "foreCol",  alarmColor,
                  "SelectArea", "foreCol", selectColor,
                  "LockBmp", "visible", lockVisible);
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetAnimation
}

mapping CPC_WordStatus_ButtonConfig(string deviceName) {
    mapping buttons;
//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    dyn_string exceptionInfo;

    buttons[UN_FACEPLATE_BUTTON_SELECT] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_MASK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_ACK_ALARM] = UN_ACCESS_RIGHTS_OPERATOR;
    buttons[UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS] = UN_ACCESS_RIGHTS_EXPERT;

//end_TAG_SCRIPT_DEVICE_TYPE_ButtonConfig
    return buttons;
}

/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param dpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
CPC_WordStatus_ButtonSetState(string deviceName, string dpType, dyn_string dsUserAccess, dyn_string dsData) {
    bool bSelected, buttonEnabled, bit1, bit2, bit3;
    string localManager;
    dyn_string exceptionInfo;

    bool bLocked = dsData[1];
    string sSelectedUIManager = dsData[2];

    localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedUIManager);
    bSelected = (localManager == "S"); // Selection state
    dyn_string dsButtons = unSimpleAnimation_ButtonList(deviceName);

//begin_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
    for (int i = 1; i <= dynlen(dsButtons); i++) {
        buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0); // User access
        switch (dsButtons[i]) {
            case UN_FACEPLATE_BUTTON_ACK_ALARM:
                buttonEnabled = buttonEnabled && bSelected && cpcExportGenericFunctions_get5AlertAcknowledgeAlarmValue(deviceName, exceptionInfo) == "TRUE";
                break;
            case UN_FACEPLATE_BUTTON_SET_ALARM_LIMITS:
                buttonEnabled = buttonEnabled && bSelected;
                break;
            default:
                break;
        }
        if (dsButtons[i] == UN_FACEPLATE_BUTTON_SELECT) {
            unGenericObject_ButtonAnimateSelect(localManager, (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0));
        } else {
            cpcButton_setButtonState(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], buttonEnabled);
        }
    }
//end_TAG_SCRIPT_DEVICE_TYPE_ButtonSetState
}

/** Disconnect function for the widget data

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables defined in OnOff faceplate
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-24 @whitelisted{Callback}

*/
CPC_WordStatus_WidgetDisconnection(string sWidgetType) {
//begin_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
    dyn_string dsSquareName = makeDynString("");
    setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "backCol", "unWidget_Background", "WarningText", "text", "", "AlarmText", "text", "");
    if (shapeExists("WidgetArea")) {
        setValue("WidgetArea", "visible", false);
    }
    if (sWidgetType == "WordStatusBitBig") {
        for (int j = 0; j < 16; j++) {
            dynAppend(dsSquareName, "bit" + j);
            setValue("bit" + j + ".border", "backCol", "unDataNoAccess");
            setValue("bit" + j + ".body", "backCol", "unWidget_Background");
        }
        setValue("Body1",    "fill",     "[solid]");
    } else if (sWidgetType == "WSStep") {
        setMultiValue(
            "init",     "visible",  false,
            "Body1",    "foreCol",  "unWidget_Background",
            "Body1",    "backCol",  "unDataNoAccess",
            "Body1",    "fill",     "[solid]",
            "Body2",    "backCol",  "unDataNoAccess");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_WidgetDisconnection
}

/** Init static values which are used in faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName input, the device name

*/
CPC_WordStatus_FaceplateInitStatics(string sDeviceName)
{
  bool bConfigExists;
  int iAlertType, iRes, iConversionType, iOrder;
  string sDp;
  dyn_float dfArguments;
  dyn_string exceptionInfo;


  sDp  = sDeviceName + ".ProcessInput.PosSt";
  iRes = dpGet(sDp + ":_alert_hdl.._type", iAlertType);

  g_mParameters["BoolAlert"]   = (iAlertType == DPCONFIG_ALERT_NONBINARYSIGNAL);
  g_mParameters["PosStFormat"] = dpGetFormat(sDp);
  g_mParameters["PosStUnit"]   = dpGetUnit(sDp);

  cpcGenericObject_StringToMapping(cpcGenericDpFunctions_getMapping(sDeviceName, ".ProcessInput.PosSt"), g_mLabels);

  // Add a message conversion for the WordStatus value
  g_mParameters["MessageConversion"] = "none";
  fwConfigConversion_get(sDeviceName + ".ProcessInput.PosSt",
                         bConfigExists,
                         DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN,
                         iConversionType,
                         iOrder,
                         dfArguments,
                         exceptionInfo);
  if( bConfigExists )
  {
    if( iOrder == 1 )
    {
      g_mParameters["MessageConversion"] = "y = " + dfArguments[2] + "x";
      if( dfArguments[1] != 0 )
      {
        g_mParameters["MessageConversion"] += " + " + dfArguments[1];
      }
    }
  }

  dynClear(exceptionInfo);
  g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] = cpcGenericDpFunctions_getDeviceProperty(sDeviceName, CPC_PARAMS_DISPLAY_FORMAT, exceptionInfo);
  if (dynlen(exceptionInfo) > 0) {
    exceptionInfo[2] = "Error in CPC_WordStatus_FaceplateInitStatics() with device: " + sDeviceName + " getting the display parameters properties: " + exceptionInfo[2];
    fwExceptionHandling_display(exceptionInfo);
    return;
  }

}




/** Returns the list of WordStatus DPEs which should be connected on faceplate animation

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device name
@param dpes input/output, dpe list

*/
CPC_WordStatus_FaceplateDPEs(string deviceName, dyn_string &dpes) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
    dynAppend(dpes, deviceName + ".statusInformation.selectedManager:_lock._original._locked");
    dynAppend(dpes, deviceName + ".statusInformation.selectedManager");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt");
    dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_online.._invalid");
    if (g_mParameters["BoolAlert"]) {
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._active");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_state");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._ack_possible");
        dynAppend(dpes, deviceName + ".ProcessInput.PosSt:_alert_hdl.._act_text");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateDPEs
}

/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpes input, the dpe names
@param values input, the dpe values

*/
CPC_WordStatus_FaceplateStatusAnimationCB(dyn_string dpes, dyn_anytype values) {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusAnimationCB
    int iSystemIntegrityAlarmValue = values[1];
    bool bSystemIntegrityAlarmEnabled = values[2];
    bool bLocked = values[3];
    string sSelectedManager = values[4], actText;
    int iPosSt = values[5];
    bool bPosStInvalid = values[6];
    bool isUnAck, isAlarmActive;
    int iActState;
    if (mappingHasKey(g_mParameters, "BoolAlert") && g_mParameters["BoolAlert"]) {
        isAlarmActive = values[7];
        iActState = values[8];
        isUnAck = values[9];
        actText = values[10];
    }

    string sWarningLetter, sWarningColor;
    string body1Color = "unWidget_ControlStateAuto";

    cpcGenericObject_WidgetValidnessAnimation(bSystemIntegrityAlarmEnabled, iSystemIntegrityAlarmValue, bPosStInvalid, sWarningLetter, sWarningColor);

    if (bPosStInvalid) {
        body1Color = "unDataNotValid";
    } else if (isAlarmActive) {
        if(isUnAck) {
            body1Color = "unWidget_AlarmNotAck";
        }
        else if ((iActState == DPATTR_ALERTSTATE_APP_NOT_ACK) || (iActState == DPATTR_ALERTSTATE_APP_ACK)) {
            if(strpos(actText, "Warning") == 0 || strpos(actText, "Low Warning") == 0) { // is warning
                body1Color = "cpcColor_Widget_Warning";
            } else {
                body1Color = "unFaceplate_AlarmActive";
            }
        }
    }

    if (mappingHasKey(g_mParameters, "WidgetType") && (g_mParameters["WidgetType"] == "WordStatusBit")) {
        for (int j = 0; j < 16; j++) {
            string sTempBodyText = CPC_WordStatus_lookupLabel(g_mLabels, j);
            string bodyRef = "Body" + j;
            if (g_bSystemConnected) {
                setMultiValue(bodyRef, "text",    sTempBodyText,
                              bodyRef, "fill",   "[solid]",
                              bodyRef, "foreCol", body1Color,
                              bodyRef, "backCol", "unWidget_Background");
                unGenericObject_ColoredSquareAnimate("bit" + j, getBit(iPosSt, j), true, "unFaceplate_StatusActive", "unFaceplate_StatusActive", false);
            }
        }
        if (g_bSystemConnected) {
            setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor);
        }
    } else {
        string uniqueMessageText = mappingHasKey(g_mLabels, (string)iPosSt) ? g_mLabels[(string)iPosSt] : "No Description";
        string decimalValueText = iPosSt;

        if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "HEX") {
            sprintf(decimalValueText, "%X", iPosSt);
            decimalValueText = "0x" + decimalValueText;
        } else if (g_mParameters[CPC_PARAMS_DISPLAY_FORMAT] == "hex") {
            sprintf(decimalValueText, "%x", iPosSt);
            decimalValueText = "0x" + decimalValueText;
        }
        if (g_bSystemConnected) {
            setMultiValue("UniqueMessageText", "text", uniqueMessageText,
                          "UniqueMessageText", "fill", "[solid]",
                          "UniqueMessageText", "foreCol", body1Color,
                          "UniqueMessageText", "backCol", "unWidget_Background",
                          "DecimalValue", "text", decimalValueText,
                          "DecimalValue", "fill", "[solid]",
                          "DecimalValue", "foreCol", body1Color,
                          "DecimalValue", "backCol", "unWidget_Background",
                          "WarningText2", "text", sWarningLetter,
                          "WarningText2", "foreCol", sWarningColor);
        }
    }

  if( unGenericObject_shapeExists("MessageConversion") &&
      mappingHasKey(g_mParameters, "MessageConversion")       )
  {
    setValue("MessageConversion.display", "text", g_mParameters["MessageConversion"]);
  }

}




/**Faceplate disconnection function.

Normally you should not use this function.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
CPC_WordStatus_FaceplateStatusDisconnection() {
//begin_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusDisconnection
    if (g_mParameters["WidgetType"] == "WordStatusBit") {
        setMultiValue("Body1", "foreCol", "unDataNoAccess", "WarningText", "foreCol", "unDataNoAccess");
        for (int j = 0; j < 16; j++) {
            setMultiValue("bit" + j + ".border", "backCol", "unDataNoAccess",
                          "bit" + j + ".body",   "backCol", "unWidget_Background");
        }
    } else {
        setMultiValue("UniqueMessageText", "foreCol", "unDataNoAccess",
                      "DecimalValue",      "foreCol", "unDataNoAccess",
                      "WarningText2",      "foreCol", "unDataNoAccess");
    }
//end_TAG_SCRIPT_DEVICE_TYPE_FaceplateStatusDisconnection
}


//begin_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions
string CPC_WordStatus_lookupLabel(mapping labels, string idx) {
    if (mappingHasKey(labels, idx)) {
        return labels[idx];
    } else {
        return "No message attributed";
    }
}
//end_TAG_SCRIPT_DEVICE_TYPE_CustomFunctions


//@}
