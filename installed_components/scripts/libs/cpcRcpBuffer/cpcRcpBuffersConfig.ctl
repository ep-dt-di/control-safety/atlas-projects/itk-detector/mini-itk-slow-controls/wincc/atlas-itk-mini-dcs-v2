/**@file

// cpcRcpBuffersConfig.ctl
This library contains the function to configure the CPC_RcpBuffers.

@par Creation Date
  07/06/2011

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  Ivan Prieto Barreiro (EN-ICE)
*/

// Constants
const string CPC_CONFIG_RECIPEBUFFERS_DPT_NAME = "CPC_RcpBuffers";
// constant for the check/import of the DeviceType
const unsigned CPC_CONFIG_RECIPEBUFFERS_LENGTH 		  	 		       = 9;  // Device config lenght without the mandatory field
const unsigned CPC_CONFIG_RECIPEBUFFERS_HEADER_ADDR 	  	 		   = 1;  // Recipe header buffer address
const unsigned CPC_CONFIG_RECIPEBUFFERS_STATUS_ADDR 	  	 		   = 2;  // Recipe status buffer address
const unsigned CPC_CONFIG_RECIPEBUFFERS_BUFFERS_LENGTH  	 		   = 3;	 // Recipe buffers length
const unsigned CPC_CONFIG_RECIPEBUFFERS_MANREGADDR_ADDR 	 		   = 4;	 // Start address of ManRegAddr buffer
const unsigned CPC_CONFIG_RECIPEBUFFERS_MANREGVAL_ADDR  	 		   = 5;	 // Start address of ManRegVal buffer
const unsigned CPC_CONFIG_RECIPEBUFFERS_REQADDR_ADDR 	  	 		   = 6;	 // Start address of ReqAddr buffer
const unsigned CPC_CONFIG_RECIPEBUFFERS_REQVAL_ADDR 	  	 		   = 7;	 // Start address of ReqVal buffer
const unsigned CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_FUNC 	 		   = 8;	 // Recipe activation function
const unsigned CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_TIMEOUT 		       = 9;	 // Recipe activation timeout

const unsigned CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH		 		   = 1000; // Maximum size for the recipe buffers
const unsigned CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE  = 60;   // Maximum number of elements in a rcp buffer word structure
const unsigned CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE = 30;   // Maximum number of elements in a rcp buffer float structure
const unsigned CPC_CONFIG_RECIPEBUFFERS_BUFFER_SHIFT_MODBUS  = 60;    // structure's address shift for ModBus
const unsigned CPC_CONFIG_RECIPEBUFFERS_BUFFER_SHIFT_SIEMENS = 120;   // structure's address shift for S7




/**
  Initialize the constants required for the import process to improve the performance
*/
void cpcRcpBuffersConfig_initializeConstants()
{
  mapping mTemp;

  mTemp["DPT_NAME"]                     = CPC_CONFIG_RECIPEBUFFERS_DPT_NAME;
  mTemp["LENGTH"]                       = CPC_CONFIG_RECIPEBUFFERS_LENGTH;
  mTemp["HEADER_ADDR"]                  = CPC_CONFIG_RECIPEBUFFERS_HEADER_ADDR;
  mTemp["STATUS_ADDR"]                  = CPC_CONFIG_RECIPEBUFFERS_STATUS_ADDR;
  mTemp["BUFFERS_LENGTH"]               = CPC_CONFIG_RECIPEBUFFERS_BUFFERS_LENGTH;
  mTemp["MANREGADDR_ADDR"]              = CPC_CONFIG_RECIPEBUFFERS_MANREGADDR_ADDR;
  mTemp["MANREGVAL_ADDR"]               = CPC_CONFIG_RECIPEBUFFERS_MANREGVAL_ADDR;
  mTemp["REQADDR_ADDR"]                 = CPC_CONFIG_RECIPEBUFFERS_REQADDR_ADDR;
  mTemp["REQVAL_ADDR"]                  = CPC_CONFIG_RECIPEBUFFERS_REQVAL_ADDR;
  mTemp["ACTIVATION_FUNC"]              = CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_FUNC;
  mTemp["ACTIVATION_TIMEOUT"]           = CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_TIMEOUT;
  mTemp["MAX_LENGTH"]                   = CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH;
  mTemp["MAX_ELEMENTS_WORD_STRUCTURE"]  = CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE;
  mTemp["MAX_ELEMENTS_FLOAT_STRUCTURE"] = CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE;
  mTemp["BUFFER_SHIFT_MODBUS"]          = CPC_CONFIG_RECIPEBUFFERS_BUFFER_SHIFT_MODBUS;
  mTemp["BUFFER_SHIFT_SIEMENS"]         = CPC_CONFIG_RECIPEBUFFERS_BUFFER_SHIFT_SIEMENS;

  g_mCpcConst["CPC_CONFIG_RECIPEBUFFERS"] = mTemp;
}




//------------------------------------------------------------------------------------------------------------------------
// cpcRcpBuffersConfig_checkConfig
/** check the device configuration

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}
*/
cpcRcpBuffersConfig_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo) {
    if (dynlen(dsConfigs) >= UN_CONFIG_COMMON_LENGTH) {
        // 1. Common
        unConfigGenericFunctions_checkParameters(dsConfigs, "CPC_RcpBuffers", exceptionInfo);
        // 2. call the device check function
        unConfigGenericFunctions_checkDeviceConfig(dsConfigs, "CPC_RcpBuffers", exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "cpcRcpBuffersConfig_checkConfig: " + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------
// cpcRcpBuffersConfig_setConfig
/** set the device configuration

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs  input, config line as dyn_string
@param exceptionInfo output, for errors

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}
*/
cpcRcpBuffersConfig_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo) {
    string sDpName;

    // 1. Initialization : checking + dpCreate
    unConfigGenericFunctions_checkAll(dsConfigs, "CPC_RcpBuffers", exceptionInfo);
    if (dynlen(exceptionInfo) <= 0) {  // Good inputs
        sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
        unConfigGenericFunctions_createDp(sDpName, "CPC_RcpBuffers", exceptionInfo);
        if (dynlen(exceptionInfo) <= 0) { // Creation ok or dp already exists
            // 2. Generate device
            // 2.1. Parameters
            unConfigGenericFunctions_setCommon(dsConfigs, exceptionInfo);
            // 2.2 call the device check function
            unConfigGenericFunctions_setDeviceConfig(dsConfigs, "CPC_RcpBuffers", exceptionInfo);
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------
// _UnPlc_CPC_RcpBuffers_checkConfig
/** check the CPC_RcpBuffers device configuration for the _UnPlc front-end

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}
*/
_UnPlc_CPC_RcpBuffers_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo) {

    // Check the config line length
    if (dynlen(dsConfigs) != (CPC_CONFIG_RECIPEBUFFERS_LENGTH + UN_CONFIG_COMMON_LENGTH)) {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_checkConfig: " + getCatStr("unGeneration", "BADINPUTS"), "");
        return;
    }

    // check all the device configuration
    string sHeaderBufferAddr 	   = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_HEADER_ADDR];
    string sStatusBufferAddr     = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_STATUS_ADDR];
    string sManRegAddrBufferAddr = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGADDR_ADDR];
    string sManRegValBufferAddr  = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGVAL_ADDR];
    string sReqAddrBufferAddr    = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQADDR_ADDR];
    string sReqValBufferAddr     = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQVAL_ADDR];
    string sActivationFunction   = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_FUNC];
    int iBuffersLength  	   	   = (int)dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_BUFFERS_LENGTH];
    int iActivationTimeout   	   = (int)dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_TIMEOUT];

    // Check the addresses
    unConfigGenericFunctions_checkAddresses(makeDynString(sHeaderBufferAddr, sStatusBufferAddr, sManRegAddrBufferAddr, sManRegValBufferAddr, sReqValBufferAddr, sReqAddrBufferAddr), exceptionInfo);
    if (dynlen(exceptionInfo)) { return; }

    // Check the buffers length
    if (iBuffersLength <= 0 || iBuffersLength > CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_checkConfig: The buffers length must be >0 and <=1000.", "");
    }

    // Check the activation timeout value
    if (iActivationTimeout <= 0) {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_checkConfig: The recipe activation timeout must be > 0.", "");
    }
}
//------------------------------------------------------------------------------------------------------------------------
// S7_PLC_CPC_RcpBuffers_checkConfig
/** check the CPC_RcpBuffers device configuration for the S7_PLC front-end

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
S7_PLC_CPC_RcpBuffers_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo) {
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unCPC import TAG_FrontEndType_CPC_RcpBuffers", "S7_PLC_CPC_RcpBuffers_checkConfig", dsConfigs);

    // Check the config line length
    if (dynlen(dsConfigs) != (CPC_CONFIG_RECIPEBUFFERS_LENGTH + UN_CONFIG_COMMON_LENGTH)) {
        fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_RcpBuffers_checkConfig: " + getCatStr("unGeneration", "BADINPUTS"), "");
        return;
    }

    // check all the device configuration
    string sHeaderBufferAddr 	   = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_HEADER_ADDR];
    string sStatusBufferAddr     = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_STATUS_ADDR];
    string sManRegAddrBufferAddr = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGADDR_ADDR];
    string sManRegValBufferAddr  = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGVAL_ADDR];
    string sReqAddrBufferAddr    = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQADDR_ADDR];
    string sReqValBufferAddr     = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQVAL_ADDR];
    string sActivationFunction   = dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_FUNC];
    int iBuffersLength  	   	   = (int)dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_BUFFERS_LENGTH];
    int iActivationTimeout   	   = (int)dsConfigs[UN_CONFIG_COMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_TIMEOUT];

    // Check the S7 addresses
    unConfigGenericFunctions_checkAddressS7(makeDynString(sHeaderBufferAddr,
                                            sStatusBufferAddr,
                                            sManRegAddrBufferAddr,
                                            sManRegValBufferAddr,
                                            sReqAddrBufferAddr),
                                            UN_CONFIG_S7_PLC_DATATYPE_WORD,
                                            exceptionInfo);

    unConfigGenericFunctions_checkAddressS7(makeDynString(sReqValBufferAddr),
                                            UN_CONFIG_S7_PLC_DATATYPE_FLOAT,
                                            exceptionInfo);

    if (dynlen(exceptionInfo)) { return; }

    // Check the buffers length
    if (iBuffersLength <= 0 || iBuffersLength > CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_RcpBuffers_checkConfig: The buffers length must be >0 and <=1000.", "");
    }

    // Check the activation timeout value
    if (iActivationTimeout <= 0) {
        fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_RcpBuffers_checkConfig: The recipe activation timeout must be > 0.", "");
    }
}

//------------------------------------------------------------------------------------------------------------------------
// S7_PLC_CPC_RcpBuffers_setConfig
/** set the CPC_RcpBuffers device configuration for the S7_PLC front-end

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
S7_PLC_CPC_RcpBuffers_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo) {
    string sDpName 			   = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    string sHeaderBufferAddr 	   = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_HEADER_ADDR];
    string sStatusBufferAddr     = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_STATUS_ADDR];
    string sManRegAddrBufferAddr = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGADDR_ADDR];
    string sManRegValBufferAddr  = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGVAL_ADDR];
    string sReqAddrBufferAddr    = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQADDR_ADDR];
    string sReqValBufferAddr     = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQVAL_ADDR];
    string sActivationFunction   = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_FUNC];
    int iBuffersLength  	   	   = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_BUFFERS_LENGTH];
    int iActivationTimeout	   = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_TIMEOUT];

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un import S7_PLC_CPC_RcpBuffers " + sDpName, "S7_PLC_CPC_RcpBuffers_setConfig", dsConfigs);

    // Set the buffers length
    int rc = dpSet(sDpName + ".ProcessInput.BuffersLength", iBuffersLength,
                   sDpName + ".ProcessInput.ActivationTimeout", iActivationTimeout,
                   sDpName + ".ProcessInput.ActivationFunction", sActivationFunction);
    if (rc) {
        fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_RcpBuffers_checkConfig: Error setting the recipe buffers data in " + sDpName, "");
        return;
    }

    string plcName = unGenericObject_GetPLCNameFromDpName(sDpName);
    plcName = substr(plcName, strlen(S7_PLC_DPTYPE) + 1);

    dyn_anytype params;
    params[fwPeriphAddress_TYPE] = fwPeriphAddress_TYPE_S7;
    params[fwPeriphAddress_DRIVER_NUMBER] = dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER];
    params[fwPeriphAddress_DATATYPE] = PVSS_S7_INT16; // INT16 conversion
    params[fwPeriphAddress_ACTIVE] = UN_CONFIG_DEFAULT_ADDRESS_ACTIVE;
    params[fwPeriphAddress_S7_LOWLEVEL] = true; //or true if you want timestamp to be updated only on value change
    params[fwPeriphAddress_S7_POLL_GROUP] = ""; //poll group name
    params[fwPeriphAddress_S7_INTERVAL] = UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME;

    // Sets the Status buffer address
    params[fwPeriphAddress_REFERENCE] = plcName + "." + sStatusBufferAddr;
    params[fwPeriphAddress_DIRECTION] = DPATTR_ADDR_MODE_INPUT_SPONT; // write mode
    fwPeriphAddress_set(sDpName + ".ProcessInput.Status", params,  exceptionInfo);


    // Sets the Header buffer address
    params[fwPeriphAddress_REFERENCE] = plcName + "_POLL." + sHeaderBufferAddr;
    params[fwPeriphAddress_DIRECTION] = DPATTR_ADDR_MODE_OUTPUT; // write mode
    fwPeriphAddress_set(sDpName + ".ProcessOutput.Header", params,  exceptionInfo);
    if (dynlen(exceptionInfo)) { DebugTN(exceptionInfo); return; }


    // Removes the address config of the word buffer structures
    int numStructures = ((CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE) + 1;
    for (int i = 1; i <= numStructures; i++) {
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ManRegAddr.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ManRegVal.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ReqAddr.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
    }

    // Removes the address config of the float buffer structures
    numStructures = ((CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE) + 1;
    for (int i = 1; i <= numStructures; i++) {
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ReqVal.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
    }

    // Gets the db number of the buffers
    int pos = strpos(sHeaderBufferAddr, ".");
    string db = substr(sHeaderBufferAddr, 0, pos + 1);

    // Gets the start offset of each buffer
    string tmp = substr(sManRegAddrBufferAddr, pos + 4);
    int iManRegAddrStart = tmp;
    tmp = substr(sManRegValBufferAddr, pos + 4);
    int iManRegValStart = tmp;
    tmp = substr(sReqAddrBufferAddr, pos + 4);
    int iReqAddrStart = tmp;
    tmp = substr(sReqValBufferAddr, pos + 4, strlen(sReqValBufferAddr) - 1 - (pos + 4));
    int iReqValStart = tmp;

    // Sets the ManRegAddr, ManRegVal and ReqAddr buffer addresses
    int iOffset = 0;
    int iWordStructures = ((iBuffersLength - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE) + 1;
    for (int i = 1; i <= iWordStructures; i++) {
        // ManRegAddr buffer address
        params[fwPeriphAddress_REFERENCE] = plcName + "_POLL." + db + "DBW" + (iManRegAddrStart + iOffset);
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ManRegAddr.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo)) { DebugTN(exceptionInfo); return; }

        // ManRegVal buffer address
        params[fwPeriphAddress_REFERENCE] = plcName + "_POLL." + db + "DBW" + (iManRegValStart + iOffset);
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ManRegVal.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo)) { DebugTN(exceptionInfo); return; }

        // ReqAddr buffer address
        params[fwPeriphAddress_REFERENCE] = plcName + "_POLL." + db + "DBW" + (iReqAddrStart + iOffset);
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ReqAddr.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo)) { DebugTN(exceptionInfo); return; }

        iOffset += 2 * CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE;
    }


    // Sets the ReqVal buffer addresses
    int iOffset = 0;
    int iFloatStructures = ((iBuffersLength - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE) + 1;
    params[fwPeriphAddress_DATATYPE] = PVSS_S7_FLOAT;
    for (int i = 1; i <= iFloatStructures; i++) {
        // ReqAddr buffer address
        params[fwPeriphAddress_REFERENCE] = plcName + "_POLL." + db + "DBD" + (iReqValStart + iOffset) + "F";
        params[fwPeriphAddress_DIRECTION] = DPATTR_ADDR_MODE_OUTPUT; // write mode
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ReqVal.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo)) { DebugTN(exceptionInfo); return; }

        iOffset += 4 * CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE;
    }

}

//------------------------------------------------------------------------------------------------------------------------
// _UnPlc_CPC_RcpBuffers_setConfig
/** set the CPC_RcpBuffers device configuration for the _UnPlc front-end

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}
*/
_UnPlc_CPC_RcpBuffers_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo) {
    string sDpName 			   = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    int iHeaderBufferAddr 	   = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_HEADER_ADDR];
    int iStatusBufferAddr        = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_STATUS_ADDR];
    int iBuffersLength  	   	   = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_BUFFERS_LENGTH];
    int iManRegAddrBufferAddr    = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGADDR_ADDR];
    int iManRegValBufferAddr     = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_MANREGVAL_ADDR];
    int iReqAddrBufferAddr       = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQADDR_ADDR];
    int iReqValBufferAddr        = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_REQVAL_ADDR];
    int iActivationTimeout	   = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_TIMEOUT];
    string sActivationFunction   = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + CPC_CONFIG_RECIPEBUFFERS_ACTIVATION_FUNC];

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "un import _UnPlc_CPC_RcpBuffers " + sDpName, "_UnPlc_CPC_RcpBuffers_setConfig", dsConfigs);

    // Set the buffers length
    int rc = dpSet(sDpName + ".ProcessInput.BuffersLength", iBuffersLength,
                   sDpName + ".ProcessInput.ActivationTimeout", iActivationTimeout,
                   sDpName + ".ProcessInput.ActivationFunction", sActivationFunction);
    if (rc) {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_setConfig: Error setting the recipe buffers data in " + sDpName, "");
        return;
    }

    string plcName = unGenericObject_GetPLCNameFromDpName(sDpName);
    plcName = substr(plcName, strlen(UN_PLC_DPTYPE) + 1);

    string plcNumber;
    int rc = dpGet(c_unSystemIntegrity_UnPlc + plcName + ".configuration.mod_PLC", plcNumber);
    if (rc) {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_setConfig: Unable to read the MODBUS PLC number from" +
                          c_unSystemIntegrity_UnPlc + plcName + ".configuration.mod_PLC", "");
        return;
    }
    plcNumber = substr(plcNumber, strlen(c_unSystemIntegrity_ModPlc));

    // Common address parameters
    dyn_anytype params;
    params[fwPeriphAddress_TYPE] = fwPeriphAddress_TYPE_MODBUS;
    params[fwPeriphAddress_DRIVER_NUMBER] = dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER];
    params[fwPeriphAddress_ACTIVE] = UN_CONFIG_DEFAULT_ADDRESS_ACTIVE;
    params[fwPeriphAddress_MODBUS_SUBINDEX] = 0;
    params[fwPeriphAddress_MODBUS_START] = UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME;
    params[fwPeriphAddress_MODBUS_INTERVAL] = UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME;
    params[fwPeriphAddress_MODBUS_OBJECT_SIZE] = "";

    // Sets the Status buffer address
    params[fwPeriphAddress_DATATYPE] = PVSS_MODBUS_INT16;
    params[fwPeriphAddress_REFERENCE] = "U." + plcNumber + ".16." + iStatusBufferAddr;
    params[fwPeriphAddress_DIRECTION] = DPATTR_ADDR_MODE_INPUT_SPONT;
    params[fwPeriphAddress_MODBUS_LOWLEVEL] = true;
    fwPeriphAddress_checkModbusParameters(params, exceptionInfo);
    if (dynlen(exceptionInfo) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_setConfig: wrong modbus params", "");
        return;
    }


    string addressReference;
    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT,
            dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE],
            dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER],
            false,
            iStatusBufferAddr,
            dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]), addressReference);




    if (addressReference == "") {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_setConfig: can't create addressReference", "");
        return;
    }

    fwPeriphAddress_set(dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME] + ".ProcessInput.Status",
                        makeDynString(fwPeriphAddress_TYPE_MODBUS,
                                      dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER],
                                      addressReference,
                                      DPATTR_ADDR_MODE_INPUT_SPONT,
                                      PVSS_MODBUS_INT16,
                                      UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                      "", "", "", "",
                                      UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_INPUT,
                                      0,
                                      UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME, UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME, ""),
                        exceptionInfo);
    //fwPeriphAddress_set(sDpName + ".ProcessInput.Status", params,  exceptionInfo);
    if (addressReference == "") {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_CPC_RcpBuffers_setConfig: can't create periph address", "");
        return;
    }

    dpSet(sDpName + ".ProcessInput.Status:_address.._lowlevel", true);

    // Sets the Header buffer address
    params[fwPeriphAddress_REFERENCE] = "M." + plcNumber + ".16." + iHeaderBufferAddr;
    params[fwPeriphAddress_DIRECTION] = DPATTR_ADDR_MODE_OUTPUT;
    params[fwPeriphAddress_MODBUS_LOWLEVEL] = UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_OUTPUT;
    fwPeriphAddress_checkModbusParameters(params, exceptionInfo);
    if (dynlen(exceptionInfo))	{ return; }
    fwPeriphAddress_set(sDpName + ".ProcessOutput.Header", params,  exceptionInfo);
    if (dynlen(exceptionInfo))	{ return; }


    // Removes the address config of the word buffer structures
    int numStructures = ((CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE) + 1;
    for (int i = 1; i <= numStructures; i++) {
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ManRegAddr.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ManRegVal.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ReqAddr.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
    }

    // Removes the address config of the float buffer structures
    numStructures = ((CPC_CONFIG_RECIPEBUFFERS_MAX_LENGTH - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE) + 1;
    for (int i = 1; i <= numStructures; i++) {
        fwPeriphAddress_delete(sDpName + ".ProcessOutput.ReqVal.Structure" + i, exceptionInfo);
        if (dynlen(exceptionInfo))	{ DebugTN(exceptionInfo); return; }
    }

    // Sets the ManRegAddr, ManRegVal and ReqAddr buffer addresses
    params[fwPeriphAddress_DATATYPE] = PVSS_MODBUS_UINT16;
    int iOffset = 0;
    int iWordStructures = ((iBuffersLength - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE) + 1;
    for (int i = 1; i <= iWordStructures; i++) {
        // ManRegAddr buffer address
        params[fwPeriphAddress_REFERENCE] = "M." + plcNumber + ".16." + (iManRegAddrBufferAddr + iOffset);
        fwPeriphAddress_checkModbusParameters(params, exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ManRegAddr.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }

        // ManRegVal buffer address
        params[fwPeriphAddress_REFERENCE] = "M." + plcNumber + ".16." + (iManRegValBufferAddr + iOffset);
        fwPeriphAddress_checkModbusParameters(params, exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ManRegVal.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }

        // ReqAddr buffer address
        params[fwPeriphAddress_REFERENCE] = "M." + plcNumber + ".16." + (iReqAddrBufferAddr + iOffset);
        fwPeriphAddress_checkModbusParameters(params, exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ReqAddr.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }

        iOffset += CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_WORD_STRUCTURE;
    }

    // Sets the ReqVal buffer addresses
    int iOffset = 0;
    int iFloatStructures = ((iBuffersLength - 1) / CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE) + 1;
    params[fwPeriphAddress_DATATYPE] = PVSS_MODBUS_FLOAT;
    for (int i = 1; i <= iFloatStructures; i++) {
        // ReqAddr buffer address
        params[fwPeriphAddress_REFERENCE] = "M." + plcNumber + ".16." + (iReqValBufferAddr + iOffset);
        fwPeriphAddress_checkModbusParameters(params, exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }
        fwPeriphAddress_set(sDpName + ".ProcessOutput.ReqVal.Structure" + i, params,  exceptionInfo);
        if (dynlen(exceptionInfo))	{ return; }

        iOffset += 2 * CPC_CONFIG_RECIPEBUFFERS_MAX_ELEMENTS_FLOAT_STRUCTURE;
    }

}

//------------------------------------------------------------------------------------------------------------------------
// cpcRcpBufferExportDevice_writeDeviceTypeLineFormat
/** returns the format of the config line for a device type

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceType input, device type
@param sCommand input, keyword (for front-end only)
*/
cpcRcpBufferExportDevice_writeDeviceTypeLineFormat(string sDeviceType, string sCommand) {
    string sFormat, sMandatory, sDeviceConfig;

    sMandatory = sDeviceType + ";Device number (unique);Device name (Device Identifier);description;Diagnostic panel;HTML page;Default panel;subsystem1 (Domain);subsystem2 (Nature);Widget Name";
    sDeviceConfig = "addr_HeaderBuffer;addr_StatusBuffer;buffers_length;addr_ManRegAddrBuffer;addr_ManRegValBuffer;addr_ReqAddrBuffer;addr_ReqValBuffer;activationFunction;activationTimeout;";
    sFormat = sMandatory + ";" + sDeviceConfig;
    fputs("#format: " + sFormat + "\n" , g_fileToExport);
}

//------------------------------------------------------------------------------------------------------------------------
// S7_PLC_CPC_RcpBuffers_ExportConfig
/** returns the format of the config line for a device type

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}

@param dsDpList input, list of device dp
@param exceptionInfo output, the errors are returned here
*/
void S7_PLC_CPC_RcpBuffers_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    dyn_string dsAllCommonParameters, dsDpParameters, dsArchiveNames, dsAddressDpParameters;
    string sDeviceType, sStatusBufferAddr, sHeaderBufferAddr, sManRegAddrBufferAddr, sManRegValBufferAddr,
           sReqAddrBufferAddr, sReqValBufferAddr, sActivationFunction;
    string plcName;
    float fArchiveTimeFilter;
    int i, iLeni, j, iLenj, rc, iBuffersLength, iActivationTimeout;
    dyn_string dpes, address;
    dyn_bool configExists, isActive;
    dyn_dyn_anytype params;

    sDeviceType = "CPC_RcpBuffers";

    iLeni = dynlen(dsDpList);
    if (iLeni > 0) {
        cpcRcpBufferExportDevice_writeDeviceTypeLineFormat(sDeviceType, "");
    }

    for (i = 1; i <= iLeni; i++) {
        if (dpExists(dsDpList[i])) {
            dynClear(dsAllCommonParameters);
            dynClear(dsDpParameters);
            dynClear(dsArchiveNames);

            //Get all common Parameters
            unExportDevice_getAllCommonParameters(dsDpList[i], sDeviceType, dsAllCommonParameters);
            dsDpParameters = dsAllCommonParameters;

            // Get the buffers data
            rc = dpGet(dsDpList[i] + ".ProcessInput.BuffersLength", iBuffersLength,
                       dsDpList[i] + ".ProcessInput.ActivationTimeout", iActivationTimeout,
                       dsDpList[i] + ".ProcessInput.ActivationFunction", sActivationFunction);

            if (rc) {
                fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_RcpBuffers_ExportConfig: Error getting the recipe buffers data from " + dsDpList[i], "");
                continue;
            }

            // Get the buffers addresses
            dpes = makeDynString(dsDpList[i] + ".ProcessOutput.Header",
                                 dsDpList[i] + ".ProcessInput.Status",
                                 dsDpList[i] + ".ProcessOutput.ManRegAddr.Structure1",
                                 dsDpList[i] + ".ProcessOutput.ManRegVal.Structure1",
                                 dsDpList[i] + ".ProcessOutput.ReqAddr.Structure1",
                                 dsDpList[i] + ".ProcessOutput.ReqVal.Structure1");
            fwPeriphAddress_getMany(dpes,	configExists, params, isActive, exceptionInfo);
            if (dynlen(exceptionInfo)) {
                fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_RcpBuffers_ExportConfig: Error getting the periphery address of the recipe buffers from " + dsDpList[i], "");
                continue;
            }

            for (int j = 1; j <= 6; j++) {
                if (configExists[j]) {
                    address[j] = params[j][fwPeriphAddress_REFERENCE];
                    address[j] = substr(address[j], strpos(address[j], ".") + 1);
                }
            }

            dynAppend(dsDpParameters, address[1]);
            dynAppend(dsDpParameters, address[2]);
            dynAppend(dsDpParameters, iBuffersLength);
            dynAppend(dsDpParameters, address[3]);
            dynAppend(dsDpParameters, address[4]);
            dynAppend(dsDpParameters, address[5]);
            dynAppend(dsDpParameters, address[6]);
            dynAppend(dsDpParameters, sActivationFunction);
            dynAppend(dsDpParameters, iActivationTimeout);

            // write the data to the file
            unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
        }//dp doesn't exist
    }//end for
}

//------------------------------------------------------------------------------------------------------------------------
// _UnPlc_CPC_RcpBuffers_ExportConfig
/** returns the format of the config line for a device type

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDpList input, list of device dp
@param exceptionInfo output, the errors are returned here

@reviewed 2018-07-24 @whitelisted{UNICOSImportExport}
*/
void _UnPlc_CPC_RcpBuffers_ExportConfig(dyn_string dsDpList, dyn_string &exceptionInfo) {
    dyn_string dsAllCommonParameters, dsDpParameters, dsArchiveNames, dsAddressDpParameters;
    string sDeviceType, sStatusBufferAddr, sHeaderBufferAddr, sManRegAddrBufferAddr, sManRegValBufferAddr,
           sReqAddrBufferAddr, sReqValBufferAddr, sActivationFunction;
    string plcName;
    float fArchiveTimeFilter;
    int i, iLeni, j, iLenj, rc, iBuffersLength, iActivationTimeout;
    dyn_string dpes, address;
    dyn_bool configExists, isActive;
    dyn_dyn_anytype params;

    sDeviceType = "CPC_RcpBuffers";

    iLeni = dynlen(dsDpList);
    if (iLeni > 0) {
        cpcRcpBufferExportDevice_writeDeviceTypeLineFormat(sDeviceType, "");
    }

    for (i = 1; i <= iLeni; i++) {
        if (dpExists(dsDpList[i])) {
            dynClear(dsAllCommonParameters);
            dynClear(dsDpParameters);
            dynClear(dsArchiveNames);

            //Get all common Parameters
            unExportDevice_getAllCommonParameters(dsDpList[i], sDeviceType, dsAllCommonParameters);
            dsDpParameters = dsAllCommonParameters;

            // Get the buffers data
            rc = dpGet(dsDpList[i] + ".ProcessInput.BuffersLength", iBuffersLength,
                       dsDpList[i] + ".ProcessInput.ActivationTimeout", iActivationTimeout,
                       dsDpList[i] + ".ProcessInput.ActivationFunction", sActivationFunction);
            if (rc) {
                fwException_raise(exceptionInfo, "ERROR", "S7_PLC_CPC_RcpBuffers_ExportConfig: Error getting the buffers data from " + dsDpList[i], "");
                continue;
            }

            cpcExportGenericFunctions_processAddress(dsDpList[i], CPC_CONFIG_RECIPEBUFFERS_DPT_NAME, "Header", false, dsDpParameters);
            cpcExportGenericFunctions_processAddress(dsDpList[i], CPC_CONFIG_RECIPEBUFFERS_DPT_NAME, "Status", true, dsDpParameters);
            dynAppend(dsDpParameters, iBuffersLength);
            cpcExportGenericFunctions_processAddress(dsDpList[i], CPC_CONFIG_RECIPEBUFFERS_DPT_NAME, "ManRegAddr.Structure1", false, dsDpParameters);
            cpcExportGenericFunctions_processAddress(dsDpList[i], CPC_CONFIG_RECIPEBUFFERS_DPT_NAME, "ManRegVal.Structure1", false, dsDpParameters);
            cpcExportGenericFunctions_processAddress(dsDpList[i], CPC_CONFIG_RECIPEBUFFERS_DPT_NAME, "ReqAddr.Structure1", false, dsDpParameters);
            cpcExportGenericFunctions_processAddress(dsDpList[i], CPC_CONFIG_RECIPEBUFFERS_DPT_NAME, "ReqVal.Structure1", false, dsDpParameters);
            dynAppend(dsDpParameters, sActivationFunction);
            dynAppend(dsDpParameters, iActivationTimeout);

            // write the data to the file
            unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
        }//dp doesn't exist
    }//end for
}

//------------------------------------------------------------------------------------------------------------------------
