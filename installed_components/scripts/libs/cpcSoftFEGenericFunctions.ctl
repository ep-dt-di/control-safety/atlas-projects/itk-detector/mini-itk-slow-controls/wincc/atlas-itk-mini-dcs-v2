//// GET DEVICE VALUE
/** Function sets up an AnalogInput value
@reviewed 2018-06-22 @whitelisted{API}
*/
int cpcSoftFEGenericFunctions_setAnalogInputValue(string alias, mixed value) {
    string dpName = unGenericDpFunctions_dpAliasToName(alias);
    return dpSet(dpName + "ProcessInput.PosSt", value,
                 dpName + "ProcessInput.StsReg01", 8196,
                 dpName + "ProcessInput.HFSt", value);
}

/** Function sets up an AnalogParameter value

@reviewed 2018-07-26 @whitelisted{Callback}

*/
int cpcSoftFEGenericFunctions_setAnalogParameterValue(string alias, mixed value) {
    string dpName = unGenericDpFunctions_dpAliasToName(alias);
    return dpSet(dpName + "ProcessOutput.MPosR", value);
}

/** Function sets up an AnalogStatus value

@reviewed 2018-07-26 @whitelisted{Callback}

*/
int cpcSoftFEGenericFunctions_setAnalogStatusValue(string alias, mixed value) {
    string dpName = unGenericDpFunctions_dpAliasToName(alias);
    return dpSet(dpName + "ProcessInput.PosSt", value);
}

//// SET DEVICE VALUE
/** Function returns an analog value, can be used for AnalogInput, AnalogStatus, AnalogParameter
*/
mixed cpcSoftFEGenericFunctions_getAnalogValue(string alias, string dpeName = "ProcessInput.PosSt") {
    mixed value;
    string dpName = unGenericDpFunctions_dpAliasToName(alias);
    int iRes = dpGet(dpName + dpeName, value);
    if (iRes < 0) {
        DebugN("iRes : " + iRes + " Error getting " + alias );
    }

    return value;
}

/** Function returns the last time stamp analog value, can be used for AnalogInput, AnalogStatus, AnalogParameter, others
*/
int cpcSoftFEGenericFunctions_getTimeStamp(string alias, string dpeName = "ProcessInput.PosSt") {
    int iTime;
    string dpName = unGenericDpFunctions_dpAliasToName(alias);
    int iRes = dpGet(dpName + dpeName + ":_original.._stime", iTime);
    if (iRes < 0) {
        DebugN("iRes : " + iRes + " Error getting " + alias );
    }

    return iTime;
}

/** Function returns the value of a status bit, can be used for StsReg01 or others.
*/
bool cpcSoftFEGenericFunctions_getStatusBit(string alias, int bit, string statusDPE = "ProcessInput.StsReg01") {
    int status;
    string dpName = unGenericDpFunctions_dpAliasToName(alias);
    int iRes = dpGet(dpName + statusDPE, status);
    if (iRes < 0) {
        DebugN("iRes : " + iRes + " Error getting " + alias + "." + statusDPE);
    }

    return getBit(status, bit);
}

/** Function recovering the IOError of a stsreg01
@reviewed 2018-06-22 @whitelisted{API}
*/
bool cpcSoftFEGenericFunctions_getIOError(string alias) {
    return cpcSoftFEGenericFunctions_getStatusBit(alias, CPC_StsReg01_IOERRORW);
}

//// MANUPULATE SOFT FRONT END
/** Function intiaties the watchdog to zero.

@reviewed 2018-07-26 @whitelisted{Callback}

*/
int cpcSoftFEGenericFunctions_initiateWatchdogFrontEnd(string frontNameAlias) {
    string dpName = unGenericDpFunctions_dpAliasToName(frontNameAlias);
    return dpSet(dpName + "communication.counter", 0);
}

/** Function increase the watchdog.
@reviewed 2018-06-22 @whitelisted{API}
*/
int cpcSoftFEGenericFunctions_increaseWatchdogFrontEnd(string frontNameAlias) {
    int iCounter;
    string dpName = unGenericDpFunctions_dpAliasToName(frontNameAlias);
    dpGet(dpName + "communication.counter", iCounter);
    return dpSet(dpName + "communication.counter", iCounter + 1);
}

/** Function return true if front end is enabled.
@reviewed 2018-06-22 @whitelisted{API}
*/
bool cpcSoftFEGenericFunctions_getStatusFrontEnd(string frontNameAlias) {
    bool bState;
    string sSystemName = getSystemName();
    dpGet(sSystemName + c_unSystemAlarm_dpPattern + DS_pattern + c_unSystemIntegrity_SOFT_FE + frontNameAlias + ".enabled", bState);
    return bState;
}
