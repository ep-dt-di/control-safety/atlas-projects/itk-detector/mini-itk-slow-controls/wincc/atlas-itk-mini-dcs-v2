/**@file

This library contains utility functions for the import of fwDIP config.

@par Creation Date
	19/12/2002

@par Modification History
  09/04/2015: Herve
  ENS-11382: DIP Import blocks HMI
  
  27/08/2013: Herve Milcent
  DIP-120 - DIP and fwDipImport, accept "single" keyword in the import file and use "single" in the export instead of "simple"
  
  27/08/2013: Herve Milcent
  first version
  
@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@author
	Herve Milcent
*/

//@{

#uses "CtrlXml"
#uses "fwDIP/fwDIP.ctl"
/*****
  constants declaration
**********/

const int FWDIPIMPORT_CONST_FWDIPIMPORT_CONST_WARNING_MESSAGE = 1;
const int FWDIPIMPORT_CONST_ERROR_FEEDBACK = 2;
const int FWDIPIMPORT_CONST_OK_FEEDBACK = 3;
const int FWDIPIMPORT_CONST_WARNING_FEEDBACK = 4;
const string FWDIPIMPORT_CONST_ERROR_MESSAGE = "ERROR";
const string FWDIPIMPORT_CONST_WARNING_MESSAGE = "WARNING";
const string FWDIPIMPORT_CONST_VARIABLE_NOT_SET = "";

const int FWDIPIMPORT_CONST_DRIVER_NOTINCONSOLE = 1;
const int FWDIPIMPORT_CONST_DRIVER_NOTRUNNING = 2;
const int FWDIPIMPORT_CONST_DRIVER_RUNNING = 3;

const int FWDIPIMPORT_CONST_MAX_ITERATION_MANAGER_CHECK = 6;
const int FWDIPIMPORT_CONST_WAIT_MANAGER_CHECK = 10;
const int FWDIPIMPORT_CONST_QUERY_TIMEOUT = 15;
const int FWDIPIMPORT_CONST_BUFFER_DEFAULT = 1000;
const bool FWDIPIMPORT_CONST_OVERWRITE_DEFAULT = true;

const string FWDIPIMPORT_CONST_XSD_BOOL = "xsd:bool";
const string FWDIPIMPORT_CONST_XSD_FLOAT = "xsd:float";
const string FWDIPIMPORT_CONST_XSD_STRING = "xsd:string";
const string FWDIPIMPORT_CONST_XSD_INT = "xsd:int";

//@}

//@{
//------------------------------------------------------------------------------------------------------------------------
/** Get the value of a xml node

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param docNum		input, the xml document number
@param node			input, the node id
@return					The value of the xml node or "" in case of an error is returned
*/
string fwDIPImport_getNodeValue(int docNum, int node)
{
  int childeNode = xmlFirstChild(docNum, node);
  if(childeNode != -1) {
//    DebugN("fwDIPImport_getNodeValue", xmlNodeName(docNum, node), xmlNodeName(docNum, childeNode), xmlNodeValue(docNum, childeNode));
    return  xmlNodeValue(docNum, childeNode);
  }
  else {
//    DebugN("fwDIPImport_getNodeValue", xmlNodeName(docNum, node));
    return "";
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Check if the DIP configuration is correct

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param mTemp		input, the current DIP config read from the file
@param m_dsAllPublication			the list of all the publications except the ones of the current DIP config used
@param exceptionInfo	details of any exceptions are returned here
@param fFile		input, the log file name id
@return					true=OK/false=error
*/
bool _fwDIP_checkDIPManager(mapping mTemp, mapping &m_dsAllPublication, dyn_string &exceptionInfo, file fFile)
{
  bool bReturn=false;
	dyn_string dsDIPConfig = dpNames("*","_FwDipConfig");
  int iNumber = -1, iRes, iNumberConfig, iPos;
  string sDIPConfig="--";
  dyn_string dsConfigFileDIPConfig;
  dyn_int diNumber;
  
  // if the DIP config is used by another DIP manager, raise ERROR and skip the configuration
  // if the corresponding DIP manager has already another config, raise an ERROR and skip the configuration
  if(mappingHasKey(mTemp, "managerNumber"))
    iNumber = mTemp["managerNumber"];
  if(mappingHasKey(mTemp, "name"))
    sDIPConfig = mTemp["name"];
  if((sDIPConfig != "--") && (iNumber != -1))
  {
  	iRes = _fwDIP_getAllDIPConfigFromConfigFile(diNumber, dsConfigFileDIPConfig);
  	if(iRes < 0) 
    {
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: cannot read the project config file\n", fFile);
      }
      fwException_raise(exceptionInfo, "ERROR", "cannot read the project config file","");	
    }
    else
    {
      if(dynContains(dsDIPConfig, getSystemName()+sDIPConfig) > 0)
      {
        iPos = dynContains(dsConfigFileDIPConfig, sDIPConfig);
        if(iPos > 0)
        {
          if(iNumber == diNumber[iPos])
          {
            if(fFile != 0)
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: DIP config "+sDIPConfig+" already defined\n", fFile);
            bReturn = true;
          }
          else
          {
            if(fFile != 0)
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: DIP config "+sDIPConfig+" used by manager "+diNumber[iPos]+"\n", fFile);
            fwException_raise(exceptionInfo, "ERROR", "DIP config "+sDIPConfig+" used by manager "+diNumber[iPos], "");
          }
        }
        else
        {
          if(fFile != 0)
            _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: DIP config "+sDIPConfig+" already defined but not in config file\n", fFile);
          bReturn = true;
        }
      }
      else
      {
        iPos = dynContains(diNumber, iNumber);
        if(iPos > 0)
        {
          if(fFile != 0)
            _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: DIP manager "+iNumber+" using already "+dsConfigFileDIPConfig[iPos]+"\n", fFile);
          fwException_raise(exceptionInfo, "ERROR", "DIP manager "+iNumber+" using already "+dsConfigFileDIPConfig[iPos], "");
        }
        else
        {
          if(fFile != 0){
            _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: DIP config "+sDIPConfig+" not used\n", fFile);
          }
          bReturn = true;
        }
      }
    }
  }
  else
  {
    if(fFile != 0){
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: wrong DIP configuration in xml file\n", fFile);
    }
    fwException_raise(exceptionInfo, "ERROR", "wrong DIP configuration in xml file", "");
  }
  
  // if no error get here all the publications except the ones of the DIP config currently used
  if(bReturn)
    _fwDIPImport_getAllOtherDIPPublication(dsDIPConfig, sDIPConfig, m_dsAllPublication, exceptionInfo);

  return bReturn;
}
//------------------------------------------------------------------------------------------------------------------------
/** Get all the publications except the ones of the DIP config currently used
the list of publications is returned in a mapping variable:
- key: the DIP config name
- value: the list of publication

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param dsDIPAllConfig		input, list of all the existing DIP config
@param sDIPConfig		input, the DIP config to skip
@param m_dsAllPublication			the list of all the publication except the ones of the current DIP config used
@param exceptionInfo	details of any exceptions are returned here
*/
_fwDIPImport_getAllOtherDIPPublication(dyn_string dsDIPAllConfig, string sDIPConfig, mapping &m_dsAllPublication, dyn_string &exceptionInfo)
{
  dyn_dyn_string ddsCurrentPublicationInfo, ddsCurrentSubscriptionInfo;
  dyn_string dsDIPConfig = dsDIPAllConfig, dsAllPublication;
  int iPos, i, len, iConfig, lenConfig;
  
  // skip the sDIPConfig. 
  iPos = dynContains(dsDIPConfig, getSystemName()+sDIPConfig);
  if(iPos > 0)
    dynRemove(dsDIPConfig, iPos);
  len = dynlen(dsDIPConfig);
  for(i=1;i<=len;i++)
  {
    dynClear(dsAllPublication);
    dynClear(ddsCurrentPublicationInfo);
    // get all the publications
    fwDIP_getAllPublications(dsDIPConfig[i], ddsCurrentPublicationInfo, exceptionInfo);
//    DebugN(iPos, dsDIPConfig[i], ddsCurrentPublicationInfo);
    if(dynlen(ddsCurrentPublicationInfo) >= fwDIP_OBJECT_ITEM)
      lenConfig = dynlen(ddsCurrentPublicationInfo[fwDIP_OBJECT_ITEM]);
    else
      lenConfig = 0;
    for(iConfig=1;iConfig<=lenConfig;iConfig++)
    {
      dynAppend(dsAllPublication, ddsCurrentPublicationInfo[fwDIP_OBJECT_ITEM][iConfig]);
    }
    dynUnique(dsAllPublication);
    m_dsAllPublication[dsDIPConfig[i]] = dsAllPublication;
  }
//  DebugN(m_dsAllPublication);
}
//------------------------------------------------------------------------------------------------------------------------
/** Check if the publication is already defined and return the DIP config name

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param sPublicationName		input, the DIP publication
@param m_dsAllPublication			the list of all the publication except the ones of the current DIP config used
@param sDIPConfig		if the publication is already defined, the DIP config DP name is returned here
@return					true=publication already defined/false=publication not defined
*/
bool _fwDIPImport_isPublicationUsed(string sPublicationName, mapping &m_dsAllPublication, string &sDIPConfig)
{
  bool bResult=false;
  int iPos, i, len;  
  dyn_string dsTemp;
  
  len = mappinglen(m_dsAllPublication);
  sDIPConfig = "";
  for(i=1;i<=len;i++)
  {
    dsTemp = mappingGetValue(m_dsAllPublication, i);
    if(dynContains(dsTemp, sPublicationName) > 0)
    {
      sDIPConfig =  mappingGetKey(m_dsAllPublication, i);
      i = len+1;
      bResult = true;
    }
  }
  return bResult;
}
//------------------------------------------------------------------------------------------------------------------------
/** Get all the DIP Config from the wincc oa config file.

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param diNumber		the list of DIP manager number
@param dsConfigFileDIPConfig			the list of DIP config DP
@return					0=OK/any other value=error
*/
int _fwDIP_getAllDIPConfigFromConfigFile(dyn_int &diNumber, dyn_string &dsConfigFileDIPConfig)
{
  int iReturn;
  dyn_string configLines;
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";
  dyn_string dsTemp;
  int iTemp;
  string sTemp;
  int len;
  
  /*****
 get all the lines of the config file, and lok for all [dip_x] section.
   - the DIPmanager number is x
   -the DIP config DP is the one from the line DipConfig

Example:   
[dip_6]
DipConfig = "CTRLLHCbDIPConfig_1"

   *******/
  iReturn = _fwInstallation_getConfigFile(configLines);
	if(iReturn == 0)
  {
    while(dynlen(configLines)>0)
    {
      sTemp = configLines[1];
      dynRemove(configLines, 1);
      if(strpos(sTemp, "[dip_")==0)
      {
        // dip section, get the manager number
        dsTemp = strsplit(sTemp, "_");
        if(dynlen(dsTemp)>1)
        {
          iTemp = (int)dsTemp[2];
        }
        else
          iTemp = 0;
        if(iTemp > 0)
        {
          // look for DipConfig and get the string after the =
          len = dynlen(configLines);
          while(len>0)
          {
            sTemp = configLines[1];
            if(strpos(sTemp, "[dip_")!=0)
            {
              dynRemove(configLines, 1);
              len = dynlen(configLines);
              if(strpos(sTemp, "DipConfig")==0)
              {
                dsTemp = strsplit(sTemp, "=");
                if(dynlen(dsTemp) > 1)
                {
                  sTemp = strltrim(strrtrim(dsTemp[2]));
                  sTemp = strltrim(strrtrim(sTemp,"\""),"\"");
                  sTemp = strltrim(strrtrim(sTemp,"\\"),"\\");
                  dynAppend(diNumber, iTemp);
                  dynAppend(dsConfigFileDIPConfig, sTemp);
                  len = -1;
                }
              }
            }
            else
              len = -1;
          }
        }
      }
    }
  }
  return iReturn;
}
//------------------------------------------------------------------------------------------------------------------------
/** Check if the SIM driver 13 is started and of type SIM, if not, it is added in the console and started

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param exceptionInfo	details of any exceptions are returned here
@param fFile		input, the log file name id
*/
fwDIPImport_checkDriver(dyn_string &exceptionInfo, file fFile)
{
  // check if the SIM driver 13 is started and of type SIM
  // if not, adds it in the console and starts it
  // check again
  int iState = _fwDIPImport_managerState(DRIVER_MAN, "WCCILsim", "-num 13", 13);
  
  switch(iState)
  {
    case FWDIPIMPORT_CONST_DRIVER_NOTINCONSOLE:
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: driver 13 not in console\n", fFile);
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: adding driver 13 in console\n", fFile);
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: starting driver 13\n", fFile);
      }
      _fwDIPImport_addStartManager(DRIVER_MAN, "WCCILsim", "-num 13", "ManAdd");
      _fwDIPImport_addStartManager(DRIVER_MAN, "WCCILsim", "-num 13", "ManStart");
      iState = _fwDIPImport_managerState(DRIVER_MAN, "WCCILsim", "-num 13", 13);
      if(iState != FWDIPIMPORT_CONST_DRIVER_RUNNING)
      {
        if(fFile != 0){
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: driver 13 not started\n", fFile);
        }
        fwException_raise(exceptionInfo, "ERROR", "driver 13 not started", "");
      }
      else
      {
        if(fFile != 0){
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: driver 13 started\n", fFile);
        }
      }
      break;
    case FWDIPIMPORT_CONST_DRIVER_NOTRUNNING:
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: starting driver 13\n", fFile);
      }
      _fwDIPImport_addStartManager(DRIVER_MAN, "WCCILsim", "-num 13", "ManStart");
      iState = _fwDIPImport_managerState(DRIVER_MAN, "WCCILsim", "-num 13", 13);
      if(iState != FWDIPIMPORT_CONST_DRIVER_RUNNING)
      {
        if(fFile != 0){
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: driver 13 not started\n", fFile);
        }
        fwException_raise(exceptionInfo, "ERROR", "driver 13 not started", "");
      }
      else
      {
        if(fFile != 0){
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: driver 13 started\n", fFile);
        }
      }
      break;
    case FWDIPIMPORT_CONST_DRIVER_RUNNING:
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: driver 13 started\n", fFile);
      }
      break;
    default:
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: checking driver 13\n", fFile);
      }
      fwException_raise(exceptionInfo, "ERROR", "checking driver 13", "");
      break;
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Add a manager in the console, start a manager in the console

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param iManagerType		input, the type of manager: DRIVER_MAN, API_MAN
@param sManager		input, the manager name
@param sCommandLine		input, command line of the manager
@param sAction		input, type of action: "ManStart"=start a manager, "ManAdd"=add a manager
*/
_fwDIPImport_addStartManager(int iManagerType, string sManager, string sCommandLine, string sAction)
{
  dyn_string exceptionInfo;
  string sNum,sNewStatus;
  int i,iNum,iIndex,iState,iRes;
  string sState;
  int iPort;
  string sHost, sIP, sSystem;
  
  sSystem=getSystemName();
	// retrieve the hostname  and portnum of the pmon on the server
  if(isFunctionDefined("unGenericDpFunctions_getHostName"))
  {
    unGenericDpFunctions_getHostName(sSystem, sHost, sIP);	
    unGenericDpFunctions_getpmonPort(sSystem, iPort);
  }
		
  // use default if needed
  if (sHost=="" || iPort==0)
  {
    sHost=getHostname();
    iPort=pmonPort();
  }
  switch(sAction)
  {
    case "ManStart":
      fwInstallationManager_command("START", sManager, sCommandLine, sHost, iPort);
      // wait until started or max time.
      for(i=1;i <= FWDIPIMPORT_CONST_MAX_ITERATION_MANAGER_CHECK;i++)
      {
        delay(FWDIPIMPORT_CONST_WAIT_MANAGER_CHECK);
        iState = _fwDIPImport_managerState(DRIVER_MAN, "WCCILsim", "-num 13", 13);
        if(iState == FWDIPIMPORT_CONST_DRIVER_RUNNING)
        {
          i = FWDIPIMPORT_CONST_MAX_ITERATION_MANAGER_CHECK+1;
        }
      }
      break;
    case "ManAdd":
      // create the internal driver DP if not existing
      if(iManagerType == DRIVER_MAN)
      {
    		if(!dpExists("_Driver13"))
    			dpCreate("_Driver13", "_DriverCommon");
    		if(!dpExists("_Stat_Configs_driver_13"))
    			dpCreate("_Stat_Configs_driver_13", "_Statistics_DriverConfigs");
      }
      iRes = _fwDIPImport_add(sManager, "manual", 30, 2, 2, sCommandLine, sHost, iPort);
      break;
  }
//  DebugTN(iRes, sManager, sManagerCommand, exceptionInfo, sSystem);
}
//------------------------------------------------------------------------------------------------------------------------
/** Check the state of a manager

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param iManagerType		input, the type of manager: DRIVER_MAN, API_MAN
@param sManager		input, the manager name
@param sCommandLine		input, command line of the manager
@param iManagerNumber		input, the manager number
@return  the state of the manager: FWDIPIMPORT_CONST_DRIVER_NOTINCONSOLE, FWDIPIMPORT_CONST_DRIVER_RUNNING, FWDIPIMPORT_CONST_DRIVER_NOTRUNNING
*/
int _fwDIPImport_managerState(int iManagerType, string sManager, string sCommandLine, int iManagerNumber)
{
  int iReturn = FWDIPIMPORT_CONST_DRIVER_NOTRUNNING;
  int iIndex, iState, iResult;
  dyn_string dsResult;
  dyn_int diMan;
  string sType;
  dyn_mixed properties;
  string sHost, sIP, sSystem, user, pwd;
  int iPort;
  
  sSystem=getSystemName();
	
  if(isFunctionDefined("unGenericDpFunctions_getHostName"))
  {
    unGenericDpFunctions_getHostName(sSystem, sHost, sIP);	
    unGenericDpFunctions_getpmonPort(sSystem, iPort);
  }
		
  if (sHost=="" || iPort==0)
  {
    sHost=getHostname();
    iPort=pmonPort();
  }
  
  if(fwInstallation_getPmonInfo(user, pwd) != 0)
  {
    return -1;
  }
  
  iResult = fwInstallationManager_getProperties(sManager, sCommandLine, properties, sHost, iPort, user, pwd);
  
  if(iResult == 0)
  {
    if(properties[FW_INSTALLATION_MANAGER_PMON_IDX] == -1)
      iResult = -1;
    else
    { // bug ENS-8682
      if(strpos(properties[FW_INSTALLATION_MANAGER_OPTIONS], sCommandLine+ " ") < 0)
      {
        if(properties[FW_INSTALLATION_MANAGER_OPTIONS] != sCommandLine)
          iResult = -1;
      }
    }
  }
//  DebugN(iResult, sManager, sCommandLine, properties);
  if(iResult == 0)
  {
    iReturn = iResult;
    if(iManagerType == DRIVER_MAN)
    {
      dpGet("_Connections.Driver.ManNums", diMan);
      if(dynContains(diMan, 13)>0)
      {
        //check driver type;
        dpGet("_Driver13.DT", sType);
        if(sType == "SIM")
          iReturn = FWDIPIMPORT_CONST_DRIVER_RUNNING;
        else
        {
          // driver 13 started, not SIM.
          iReturn = -1;
        }
      }
      else
        iReturn = FWDIPIMPORT_CONST_DRIVER_NOTRUNNING;
    }
    else
    {
      dpGet("_Connections.Api.ManNums", diMan);
      if(dynContains(diMan, iManagerNumber)>0)
      {
        iReturn = FWDIPIMPORT_CONST_DRIVER_RUNNING;
      }
      else
        iReturn = FWDIPIMPORT_CONST_DRIVER_NOTRUNNING;
    }
  }
  else 
    iReturn = FWDIPIMPORT_CONST_DRIVER_NOTINCONSOLE;
  return iReturn;
}
//------------------------------------------------------------------------------------------------------------------------
/** Load a XML file
  All the DIP configuration of the file are kept in two variables:
- the type DIP configuration (configuration, publication, subscription) are kept in a dyn_mapping variable: dmDIPtype
- the index of the configuration in dmDIPtype is the key of the DPE list in mDIPdpeList.
- the mDIPdpeList is a mapping composed of dyn_dyn_string, each index of the dyn_dyn_string of a given key is 
the list of DPE and its configuration, the configuration is a dyn_string
- in a mapping of the list dmDIPtype one can have:
  - the DIP configuration (usually the first instance), there is no DPE list associated in mDIPdpeList
  - the publication, one mapping per publication, there could be a list of DPE in mDIPdpeList for this publication
example:
  dmDIPtype[1]: configuration, DIP config
  dmDIPtype[1]: publication, type of publication, etc.
  dmDIPtype[2]: publication, type of publication, etc.
  ....
  dmDIPtype[n]: subscription, type of publication, etc.
  
  mDIPdpeList[1]: dyn_string[1] first DPE of the publication and it configuration 
                  dyn_string[2] second DPE of the publication and it configuration
                  ...
  mDIPdpeList[2]: dyn_string[1] first DPE of the publication and it configuration
                  
  ....
  mDIPdpeList[n]: dyn_string[1] first DPE of the subscription and it configuration
  
  The DPE configuration is a dyn_string:
[1] name
[2] alias
[3] element
[4] tag
[5] buffer
[6] overwrite: true or false
[7] type: simple, single or multiple
[8] description
[9] unit

To retreive all the DIP configuration, one loops:
- over the length of dmDIPtype
- get the DPE list from mDIPdpeList with the position in the dmDIPtype

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param iDipMgrNumber		the DIP manager number to used
@param sFile			input, the XML file to read
@param dmDIPtype		list of DIP configuration
@param mDIPdpeList		DPE list
@param m_dsAllPublication		all the DIP publication
@param exceptionInfo	details of any exceptions are returned here
@param sLogFile			input, the log file name
*/
fwDIPImport_loadFile(int &iDipMgrNumber, string sFile, dyn_mapping &dmDIPtype, mapping &mDIPdpeList, mapping &m_dsAllPublication, dyn_string &exceptionInfo, string sLogFile="")
{
  file fFile;
  string sErrorMessage;
  int iErrorLine;
  int iErrorColumn;
  
  if(sLogFile != "")
  {
    fFile = fopen(sLogFile, "a");
  }
  
  // We know the xml exists and is valid so no need to check anything
  unsigned xmlConfigFile = (unsigned) xmlDocumentFromFile(sFile, sErrorMessage, iErrorLine, iErrorColumn);
  if(sLogFile != "")
  {
    if(xmlConfigFile < 0)
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " Error with XML file: "+sFile+", errorMessage: "+sErrorMessage + ", line:" +iErrorLine+ ", column: "+iErrorColumn+"\n", fFile);
    else
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " file: "+sFile+" opened\n", fFile);
  }
  if(xmlConfigFile < 0)
  {
    fwException_raise(
        exceptionInfo,
        "ERROR",
        "Error with XML file: "+sFile+", errorMessage: "+sErrorMessage+ ", line:" +iErrorLine+ ", column: "+iErrorColumn,
        ""
    );
    fclose(fFile);
    return;
  }
  // check the state of the SIM driver  
  fwDIPImport_checkDriver(exceptionInfo, fFile);
  // load the XML file in memory
  fwDIPImport_loadConfigInMemory(iDipMgrNumber, xmlConfigFile, xmlFirstChild(xmlConfigFile), "", dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
  // dump the DIP data of the XML file into the log file
  _fwDIPImport_dumpInLogFile(fFile, dmDIPtype, mDIPdpeList);
  if(sLogFile != "")
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " file: "+sFile+" closed\n", fFile);
  fclose(fFile);
}
//------------------------------------------------------------------------------------------------------------------------
/** Create the DIP config, publication and subscription
  All the DIP configuration of the file are kept in two variables:
- the type DIP configuration (configuration, publication, subscription) are kept in a dyn_mapping variable: dmDIPtype
- the index of the configuration in dmDIPtype is the key of the DPE list in mDIPdpeList.
- the mDIPdpeList is a mapping composed of dyn_dyn_string, each index of the dyn_dyn_string of a given key is 
the list of DPE and its configuration, the configuration is a dyn_string
- in a mapping of the list dmDIPtype one can have:
  - the DIP configuration (usually the first instance), there is no DPE list associated in mDIPdpeList
  - the publication, one mapping per publication, there could be a list of DPE in mDIPdpeList for this publication
example:
  dmDIPtype[1]: configuration, DIP config
  dmDIPtype[1]: publication, type of publication, etc.
  dmDIPtype[2]: publication, type of publication, etc.
  ....
  dmDIPtype[n]: subscription, type of publication, etc.
  
  mDIPdpeList[1]: dyn_string[1] first DPE of the publication and it configuration 
                  dyn_string[2] second DPE of the publication and it configuration
                  ...
  mDIPdpeList[2]: dyn_string[1] first DPE of the publication and it configuration
                  
  ....
  mDIPdpeList[n]: dyn_string[1] first DPE of the subscription and it configuration
  
  The DPE configuration is a dyn_string:
[1] name
[2] alias
[3] element
[4] tag
[5] buffer
[6] overwrite: true or false
[7] type: simple, single or multiple
[8] description
[9] unit

To retreive all the DIP configuration, one loops:
- over the length of dmDIPtype
- get the DPE list from mDIPdpeList with the position in the dmDIPtype

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param dmDIPtype		list of DIP configuration
@param mDIPdpeList		DPE list
@param exceptionInfo	details of any exceptions are returned here
@param sLogFile			input, the log file name
*/
fwDIPImport_createDIPConfig(dyn_mapping &dmDIPtype, mapping &mDIPdpeList, dyn_string &exceptionInfo, string sLogFile="")
{
  file fFile;
  int iPub, lenPub;
  string sDIPConfigDP;
  int iElapsedTime, iStartTime=(int)getCurrentTime();
  
  if(sLogFile != "")
  {
    fFile = fopen(sLogFile, "a");
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " importing DIP config starting\n", fFile);
  }
  /***
    loop over all the DIP config, if the index is a key of mDIPdpeList: create the publication or subscription
    if not it is the configuration of the DIP manager
    ****/
  lenPub = dynlen(dmDIPtype);
  _fwDIP_setProgressBar(0, lenPub);
  for(iPub=1;iPub<=lenPub;iPub++){
    _fwDIP_setProgressBar(iPub);
    iElapsedTime = (int)(getCurrentTime()-iStartTime);
    _fwDIP_setRemainingTime(iElapsedTime, iPub, lenPub, true);
    if(!mappingHasKey(mDIPdpeList, iPub))
      _fwDIPImport_createDIPManagerConfig(fFile, dmDIPtype[iPub], sDIPConfigDP, exceptionInfo);
    else
      _fwDIPImport_createDIPPublicationSubscriptionConfig(fFile, sDIPConfigDP, dmDIPtype[iPub], mDIPdpeList[iPub], exceptionInfo);
  }
  _fwDIP_setProgressBar(0);
  _fwDIP_setRemainingTime(iElapsedTime, iPub, lenPub, false);
  if(sLogFile != "")
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " importing DIP config done\n", fFile);
  fclose(fFile);
}
//------------------------------------------------------------------------------------------------------------------------
/** Configure the DIP config

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param fFile		input, the log file name id
@param mDIPtype		input, a DIP configuration
@param sDIPConfigDP		the DIP config DP
@param exceptionInfo	details of any exceptions are returned here
*/
_fwDIPImport_createDIPManagerConfig(file fFile, mapping mDIPtype, string &sDIPConfigDP, dyn_string &exceptionInfo)
{
  if(mDIPtype["DIPType"] =="configuration")
  {
  // if needed create/update the DIP config, add it in the config file, add the DIP manager
  // if needed update the DIP config
    sDIPConfigDP = mDIPtype["name"];
    if(fFile != 0)
    {
      _fwDIP_fputs("configuring DIP config, name: "+mDIPtype["name"]+" ,publisherName: "+mDIPtype["publisherName"]+", "+
            "managerNumber: "+mDIPtype["managerNumber"]+", queryTimeout: "+mDIPtype["queryTimeout"]+
            ", dns: "+(mDIPtype["dns"]==""? "(default dns)":mDIPtype["dns"])+
            ", parameters: "+(mDIPtype["parameters"]==""? "(no parameters)":mDIPtype["parameters"])+"\n", fFile);
    }
    fwDIPImport_DIPApiManager(mDIPtype["managerNumber"], mDIPtype["dns"], mDIPtype["parameters"], exceptionInfo, fFile);
    fwDIPImport_DIPApiConfigProjectFile(mDIPtype["managerNumber"], mDIPtype["name"], exceptionInfo, fFile);
    fwDIPImport_DIPConfigDP(mDIPtype["name"], mDIPtype["publisherName"], mDIPtype["queryTimeout"], exceptionInfo, fFile);
  }
  else
  {
    sDIPConfigDP = FWDIPIMPORT_CONST_VARIABLE_NOT_SET;
    if(fFile != 0) {
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING unknown type "+mDIPtype["DIPType"]+", skipping\n", fFile);
    }
    fwException_raise(
        exceptionInfo,
        "WARNING",
        "WARNING unknown type "+mDIPtype["DIPType"]+", skipping",
        ""
    );
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Check if the DIP API manager is started and add it in the console if needed

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param iManagerNumber		input, the DIP manager number
@param sDns		input, the DIP dns
@param sParameters		input, extra parameters for the DIP manager
@param exceptionInfo	details of any exceptions are returned here
@param fFile		input, the log file name id
*/
fwDIPImport_DIPApiManager(int iManagerNumber, string sDns, string sParameters, dyn_string &exceptionInfo, file fFile)
{
  string sManArgs;
  int iState;
  
  // check if the DIP API manager is started
  // if not, add it in the console
  
  sManArgs = "-num "+iManagerNumber;
  if(sDns != "")
    sManArgs += " -dns "+sDns;
  if(sParameters != "")
    sManArgs += " "+sParameters;
  iState = _fwDIPImport_managerState(API_MAN, "WCCOAdip", sManArgs, iManagerNumber);
  
  switch(iState)
  {
    case FWDIPIMPORT_CONST_DRIVER_NOTINCONSOLE:
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: WCCOAdip "+sManArgs+" not in console\n", fFile);
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: adding WCCOAdip "+sManArgs+" in console\n", fFile);
      }
      _fwDIPImport_addStartManager(API_MAN, "WCCOAdip", sManArgs, "ManAdd");
      // check if the API manager was added
      iState = _fwDIPImport_managerState(API_MAN, "WCCOAdip", sManArgs, iManagerNumber);
      if(iState == FWDIPIMPORT_CONST_DRIVER_NOTINCONSOLE)
      {
        if(fFile != 0){
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: WCCOAdip "+sManArgs+" not added\n", fFile);
        }
        fwException_raise(exceptionInfo, "ERROR", "WCCOAdip "+sManArgs+" not added", "");
      }
      else
      {
        if(fFile != 0){
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: WCCOAdip "+sManArgs+" added\n", fFile);
        }
      }
      break;
    case FWDIPIMPORT_CONST_DRIVER_NOTRUNNING:
    case FWDIPIMPORT_CONST_DRIVER_RUNNING:
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: WCCOAdip "+sManArgs+" in console\n", fFile);
      }
      break;
    default:
      if(fFile != 0){
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR: getting state WCCOAdip "+sManArgs+" in console\n", fFile);
      }
      fwException_raise(exceptionInfo, "ERROR", "getting state WCCOAdip "+sManArgs+" in console", "");
      break;
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Set the entry for the DIP API manager in the winccoa project config file

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param iManagerNumber		input, the DIP manager number
@param sDIPConfigDP		input, the DIP config DP name
@param exceptionInfo	details of any exceptions are returned here
@param fFile		input, the log file name id
*/
fwDIPImport_DIPApiConfigProjectFile(int iManagerNumber, string sDIPConfigDP, dyn_string &exceptionInfo, file fFile)
{
  int iRes;
  dyn_string dsSection=makeDynString("#DIP config set by fwDIPImport",
                                     "DipConfig = \""+sDIPConfigDP+"\"");

  // add in the config file the data
	iRes = fwInstallation_setSection( "dip_"+iManagerNumber, dsSection);
  if(iRes < 0){
    if(fFile != 0)
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR setting [dip_"+iManagerNumber+"] with "+sDIPConfigDP+"\n", fFile);
		fwException_raise(exceptionInfo, "ERROR", "ERROR setting [dip_"+iManagerNumber+"] with "+sDIPConfigDP,"");	
  }
  else
  {
    if(fFile != 0)
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO setting [dip_"+iManagerNumber+"] with "+sDIPConfigDP+"\n", fFile);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Create the DIP config DP if needed and set it.

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param sDIPConfigDP		input, the DIP config DP name
@param sPublisherName		input, the DIP config publisher name
@param iQueryTimeOut		input, the query timeout of for the DIP API manager
@param exceptionInfo	details of any exceptions are returned here
@param fFile		input, the log file name id
*/
fwDIPImport_DIPConfigDP(string sDIPConfigDP, string sPublisherName, int iQueryTimeOut, dyn_string &exceptionInfo, file fFile)
{
  // if the DIPConfigDP does not exists, creates it and check it is created
  // set the DIPConfigDP
  if(!dpExists(sDIPConfigDP))
  {
    if(dpIsLegalName(sDIPConfigDP))
    {
      if(fFile != 0)
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO creating "+sDIPConfigDP+"\n", fFile);
      dpCreate(sDIPConfigDP,"_FwDipConfig");
    }
  }
  if(dpExists(sDIPConfigDP))
  {
    if(fFile != 0)
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO setting "+sDIPConfigDP+",publisherName: "+sPublisherName+", "+
            ", queryTimeout: "+iQueryTimeOut+"\n", fFile);
  	dpSet(sDIPConfigDP+".queryTimeout", iQueryTimeOut, sDIPConfigDP+".publishName", sPublisherName);
  }
  else
  {
    if(fFile != 0)
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR "+sDIPConfigDP+" not existing\n", fFile);
		fwException_raise(exceptionInfo, "ERROR", "ERROR "+sDIPConfigDP+" not existing","");	
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Create the DIP publication or subscription.
  All the DIP publication or subscription is stored in two variables:
- mDIPtype: containing the configuration of the publication or subscription
- ddsDPE dyn_dyn_string: the list of DPE and its configuration
  The DPE configuration is a dyn_string:
[1] name
[2] alias
[3] element
[4] tag
[5] buffer
[6] overwrite: true or false
[7] type: simple, single or multiple
[8] description
[9] unit

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param fFile		input, the log file name id
@param sDIPConfigDP		input, the DIP config DP name
@param mDIPtype		input, the DIP config publisher name
@param ddsDPE		input, the query timeout of for the DIP API manager
@param exceptionInfo	details of any exceptions are returned here
*/
_fwDIPImport_createDIPPublicationSubscriptionConfig(file fFile, string sDIPConfigDP, mapping mDIPtype, dyn_dyn_string ddsDPE, dyn_string &exceptionInfo)
{
  if(mDIPtype["DIPType"] =="publication")
  {
    _fwDIPImport_createDIPPublication(fFile, sDIPConfigDP, mDIPtype, ddsDPE, exceptionInfo);
  }
  else if(mDIPtype["DIPType"] =="subscription")
  {
    _fwDIPImport_createDIPSubscription(fFile, sDIPConfigDP, mDIPtype, ddsDPE, exceptionInfo);
  }
  else
  {
    if(fFile != 0) {
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING unknown type "+mDIPtype["DIPType"]+", skipping\n", fFile);
    }
    fwException_raise(
        exceptionInfo,
        "WARNING",
        "WARNING unknown type "+mDIPtype["DIPType"]+", skipping",
        ""
    );
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Create the DIP publication.
  All the DIP publication or subscription is stored in two variables:
- mDIPtype: containing the configuration of the publication or subscription
- ddsDPE dyn_dyn_string: the list of DPE and its configuration
  The DPE configuration is a dyn_string:
[1] name
[2] alias
[3] element
[4] tag
[5] buffer
[6] overwrite: true or false
[7] type: simple, single or multiple
[8] description: not used
[9] unit: not used

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param fFile		input, the log file name id
@param sDIPConfigDP		input, the DIP config DP name
@param mDIPtype		input, the DIP config publisher name
@param ddsDPE		input, the query timeout of for the DIP API manager
@param exceptionInfo	details of any exceptions are returned here
*/
_fwDIPImport_createDIPPublication(file fFile, string sDIPConfigDP, mapping mDIPtype, dyn_dyn_string ddsDPE, dyn_string &exceptionInfo)
{
  int iDPE, lenDPE=0, lenTag;
  dyn_string dsTag, dsDPE, dsName, dsElements, dsAlias;
  dyn_int diBuffer;
  dyn_bool dbOverwrite;
  dyn_string exceptionInfoTemp;
  
  if(sDIPConfigDP != FWDIPIMPORT_CONST_VARIABLE_NOT_SET)
  {
  // create all the publications
    // for multiple publication a tag is expected for each DPE
    // combination of dpe, elements and alias must exits
    // for simple or single publication the first DPE will be used, the others will be skipped
    // publication already existing are overwritten
    lenDPE = dynlen(ddsDPE);
    if(lenDPE <1)
    {
      // no DPE, skip the publication
      if(fFile != 0) {
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING "+mDIPtype["name"]+" no DPE, skipping publication\n", fFile);
      }
      fwException_raise(
          exceptionInfo,
          "WARNING",
          "WARNING "+mDIPtype["name"]+" no DPE, skipping publication",
          ""
      );
    }
    else
    {
      switch(mDIPtype["type"])
      {
        case "simple":
        case "single":
          if(fFile != 0)
            _fwDIP_fputs("DIP Config: "+sDIPConfigDP+", configuring publication, name: "+mDIPtype["name"]+" ,type: "+mDIPtype["type"]+", "+lenDPE+" DPE(s)\n", fFile);
          for(iDPE=1;iDPE<=lenDPE;iDPE++)
          {
            dynAppend(dsName, ddsDPE[iDPE][1]);
            dynAppend(dsElements, ddsDPE[iDPE][3]);
            dynAppend(dsAlias, ddsDPE[iDPE][2]);
            dynAppend(dsTag, ddsDPE[iDPE][4]);
            dynAppend(diBuffer, ddsDPE[iDPE][5]);
            dynAppend(dbOverwrite, ddsDPE[iDPE][6]);
            if(fFile != 0)
              _fwDIP_fputs("    publication DPE name: "+ddsDPE[iDPE][1]+", alias: "+ddsDPE[iDPE][2]+", element: "+ddsDPE[iDPE][3]+ 
                     ", tag: "+ddsDPE[iDPE][4]+", buffer: "+ddsDPE[iDPE][5]+", overwrite: "+ddsDPE[iDPE][6]+
                     ", type: "+ddsDPE[iDPE][7]+", description: "+ddsDPE[iDPE][8]+", unit: "+ddsDPE[iDPE][9]+"\n", fFile);
          }
          // simple or single and more than 1 DPE use only first one
          if(lenDPE != 1)
          {
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING "+mDIPtype["name"]+" configured as simple/single with "+lenDPE+" DPE, use only first one\n", fFile);
            }
            fwException_raise(
                exceptionInfo,
                "WARNING",
                "WARNING "+mDIPtype["name"]+" configured as simple/single with "+lenDPE+" DPE, use only first one",
                ""
            );
          }
          if(_fwDIPImport_checkPublicationDPE(dsDPE, dsName[1], dsElements[1], dsAlias[1], exceptionInfo))
          {
            // publish with the first DPE
            fwDIP_publishStructure(mDIPtype["name"], dsDPE[1], dsTag[1], diBuffer[1], sDIPConfigDP, exceptionInfoTemp, dbOverwrite[1]);
            _fwDIPImport_dumpException("publication (simple/single)", fFile, exceptionInfoTemp, exceptionInfo, mDIPtype["name"], sDIPConfigDP);
          }
          else
          {
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING publication "+mDIPtype["name"]+" DPE not existing\n", fFile);
            }
            fwException_raise(
                exceptionInfo,
                "WARNING",
                "WARNING publication "+mDIPtype["name"]+" DPE not existing",
                ""
            );
          }
          break;
        case "multiple":
          // multiple, expect tag unique!
          if(fFile != 0)
            _fwDIP_fputs("DIP Config: "+sDIPConfigDP+", configuring publication, name: "+mDIPtype["name"]+" ,type: "+mDIPtype["type"]+", "+lenDPE+" DPE(s)\n", fFile);
          for(iDPE=1;iDPE<=lenDPE;iDPE++)
          {
            dynAppend(dsName, ddsDPE[iDPE][1]);
            dynAppend(dsElements, ddsDPE[iDPE][3]);
            dynAppend(dsAlias, ddsDPE[iDPE][2]);
            dynAppend(dsTag, ddsDPE[iDPE][4]);
            dynAppend(diBuffer, ddsDPE[iDPE][5]);
            dynAppend(dbOverwrite, ddsDPE[iDPE][6]);
            if(fFile != 0)
              _fwDIP_fputs("    publication DPE, name: "+ddsDPE[iDPE][1]+", alias: "+ddsDPE[iDPE][2]+", element: "+ddsDPE[iDPE][3]+ 
                     ", tag: "+ddsDPE[iDPE][4]+", buffer: "+ddsDPE[iDPE][5]+", overwrite: "+ddsDPE[iDPE][6]+
                     ", type: "+ddsDPE[iDPE][7]+", description: "+ddsDPE[iDPE][8]+", unit: "+ddsDPE[iDPE][9]+"\n", fFile);
          }
          dynUnique(dsTag);
          lenTag = dynContains(dsTag, "");
          if(lenTag>0)
            dynRemove(dsTag, lenTag);
          lenTag = dynlen(dsTag);
          if(lenTag != lenDPE)
          {
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING "+mDIPtype["name"]+" configured as multiple with "+lenTag+" instead of "+lenDPE+" tag, skipping publication\n", fFile);
            }
            fwException_raise(
                exceptionInfo,
                "WARNING",
                "WARNING "+mDIPtype["name"]+" configured as multiple with "+lenTag+" instead of "+lenDPE+" tag, skipping publication",
                ""
            );
          }
          else
          {
            // correct configuration, publish
            dynUnique(diBuffer);
            dynUnique(dbOverwrite);
            if((dynlen(diBuffer) >1) || (dynlen(dbOverwrite)>1))
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING "+mDIPtype["name"]+" not the same value for overwrite or buffer, use the first definition\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING "+mDIPtype["name"]+" not the same value for overwrite or buffer, use the fisrt definition",
                  ""
              );
            }
            if(_fwDIPImport_checkPublicationDPE(dsDPE, dsName, dsElements, dsAlias, exceptionInfo))
            {
              // publish with the first DPE
              fwDIP_publishStructure(mDIPtype["name"], dsDPE, dsTag, diBuffer[1], sDIPConfigDP, exceptionInfoTemp, dbOverwrite[1]);
            //dump error!!
              _fwDIPImport_dumpException("publication (multiple)", fFile, exceptionInfoTemp, exceptionInfo, mDIPtype["name"], sDIPConfigDP);
            }
            else
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING publication "+mDIPtype["name"]+" DPE not existing\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING publication "+mDIPtype["name"]+" DPE not existing",
                  ""
              );
            }
          }
          break;
        default:
          if(fFile != 0) {
            _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING "+mDIPtype["name"]+" unknown type "+mDIPtype["type"]+", skipping publication\n", fFile);
          }
          fwException_raise(
              exceptionInfo,
              "WARNING",
              "WARNING "+mDIPtype["name"]+" unknown type "+mDIPtype["type"]+", skipping publication",
              ""
          );
        break;
      }
    }
  }
  else
  {
    if(fFile != 0) {
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING DIPConfig not set, skipping publication "+mDIPtype["name"]+"\n", fFile);
    }
    fwException_raise(
        exceptionInfo,
        "WARNING",
        "WARNING DIPConfig not set, skipping publication "+mDIPtype["name"],
        ""
    );
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Dump exception in the log file.

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param sType		input, type of configuration
@param fFile		input, the log file name id
@param exceptionInfoTemp	input, the exception to write to the log file
@param exceptionInfo	details of any exceptions are returned here
@param sPubName		input, the name of the configuration
@param sDIPConfigDP		input, the DIP config DP name
*/
_fwDIPImport_dumpException(string sType, file fFile, dyn_string exceptionInfoTemp, dyn_string &exceptionInfo, string sPubName, string sDIPConfigDP)
{
  int i,len=dynlen(exceptionInfoTemp);
  if(len<=0)
  {
    if(fFile != 0)
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO: creating/setting "+sType+": "+sPubName+" in "+sDIPConfigDP+"\n", fFile);
  }
  else
  {
    for(i=1;i<=len;i++)
      _fwDIP_fputs(exceptionInfoTemp[i]+"\n", fFile);
    dynAppend(exceptionInfo, exceptionInfoTemp);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Check and return the publication DPE.

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param dsDPE		the list of DPE
@param dsName		input, the list of name xml node value
@param dsElements		input, the list of elements xml node value
@param dsAlias		input, the list of alias xml node value
@param exceptionInfo	details of any exceptions are returned here
*/
bool _fwDIPImport_checkPublicationDPE(dyn_string &dsDPE, dyn_string dsName, dyn_string dsElements, dyn_string dsAlias, dyn_string &exceptionInfo)
{
  bool bOk = true;
  int i, len = dynlen(dsName);
  string sDPE, sDP;
  
  for(i=1;i<=len;i++)
  {
    if(dsName[i] != "")
      sDPE = dsName[i]+dsElements[i];
    else
    {
      sDP = dpAliasToName(dsAlias[i]);
      if(dpExists(sDP))
        sDP = dpSubStr(sDP, DPSUB_DP);
      sDPE =  sDP+dsElements[i];
    }
//    DebugN(sDPE);
    if(dpExists(sDPE))
      dynAppend(dsDPE, sDPE);
    else
    {
      bOk = false;
      i=len+1;
    }
  }
  return bOk;
}
//------------------------------------------------------------------------------------------------------------------------
/** Create the DIP subscription.
  All the DIP publication or subscription is stored in two variables:
- mDIPtype: containing the configuration of the publication or subscription
- ddsDPE dyn_dyn_string: the list of DPE and its configuration
  The DPE configuration is a dyn_string:
[1] name
[2] alias
[3] element
[4] tag
[5] buffer
[6] overwrite: true or false
[7] type: simple, single or multiple
[8] description
[9] unit

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param fFile		input, the log file name id
@param sDIPConfigDP		input, the DIP config DP name
@param mDIPtype		input, the DIP config publisher name
@param ddsDPE		input, the query timeout of for the DIP API manager
@param exceptionInfo	details of any exceptions are returned here
*/
_fwDIPImport_createDIPSubscription(file fFile, string sDIPConfigDP, mapping mDIPtype, dyn_dyn_string ddsDPE, dyn_string &exceptionInfo)
{
  int iDPE, lenDPE=0;
  dyn_string dsTag, dsDPE, dsName, dsElements, dsAlias, dsType, dsUnit, dsDescription;
  dyn_int diBuffer;
  dyn_bool dbOverwrite;
  dyn_string exceptionInfoTemp;
  
  if(sDIPConfigDP != FWDIPIMPORT_CONST_VARIABLE_NOT_SET)
  {
  // create all the subscriptions
    
    // type is the DPType and must exist
    // combination of dpe, elements and alias not existing it is created
    // xsdType must be of sametype as the DPE
    // allowed xsdType are bool, int, unsigned, float, char, string
    lenDPE = dynlen(ddsDPE);
    if(lenDPE <1)
    {
      // no DPE, skip the publication
      if(fFile != 0) {
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING "+mDIPtype["name"]+" no DPE, skipping subscription\n", fFile);
      }
      fwException_raise(
          exceptionInfo,
          "WARNING",
          "WARNING "+mDIPtype["name"]+" no DPE, skipping subscription",
          ""
      );
    }
    else
    {
      switch(mDIPtype["type"])
      {
        case "multiple":
        case "simple":
        default:
          if(fFile != 0)
            _fwDIP_fputs("DIP Config: "+sDIPConfigDP+", configuring subscription, name: "+mDIPtype["name"]+" ,type: "+mDIPtype["type"]+ " ,xsdType: "+mDIPtype["xsdType"]+", "+lenDPE+" DPE(s)\n", fFile);
          for(iDPE=1;iDPE<=lenDPE;iDPE++)
          {
            dynAppend(dsName, ddsDPE[iDPE][1]);
            dynAppend(dsElements, ddsDPE[iDPE][3]);
            dynAppend(dsAlias, ddsDPE[iDPE][2]);
            dynAppend(dsTag, ddsDPE[iDPE][4]);
            dynAppend(diBuffer, ddsDPE[iDPE][5]);
            dynAppend(dbOverwrite, ddsDPE[iDPE][6]);
            dynAppend(dsType, ddsDPE[iDPE][7]);
            dynAppend(dsDescription, ddsDPE[iDPE][8]);
            dynAppend(dsUnit, ddsDPE[iDPE][9]);
            if(fFile != 0)
              _fwDIP_fputs("    subscription DPE, name: "+ddsDPE[iDPE][1]+", alias: "+ddsDPE[iDPE][2]+", element: "+ddsDPE[iDPE][3]+ 
                     ", tag: "+ddsDPE[iDPE][4]+", buffer: "+ddsDPE[iDPE][5]+", overwrite: "+ddsDPE[iDPE][6]+
                     ", type: "+ddsDPE[iDPE][7]+", description: "+ddsDPE[iDPE][8]+", unit: "+ddsDPE[iDPE][9]+"\n", fFile);
          }
          dynUnique(dbOverwrite);
          if(dynlen(dbOverwrite)>1)
          {
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING "+mDIPtype["name"]+" not the same value for overwrite, use the first definition\n", fFile);
            }
            fwException_raise(
                exceptionInfo,
                "WARNING",
                "WARNING "+mDIPtype["name"]+" not the same value for overwrite, use the fisrt definition",
                ""
            );
          }
          if(_fwDIPImport_checkAndCreateSubscriptionDPE(mDIPtype["xsdType"], dsDPE, dsType, dsName, dsElements, dsAlias, exceptionInfo))
          {
//            DebugN(dsDPE);
            // subscribe
            fwDIP_subscribeStructure(mDIPtype["name"], dsDPE, dsTag,sDIPConfigDP, exceptionInfoTemp, dbOverwrite[1]);
            _fwDIPImport_dumpException("subscription", fFile, exceptionInfoTemp, exceptionInfo, mDIPtype["name"], sDIPConfigDP);
            // if no error set unit and description
            lenDPE = dynlen(dsDPE);
            for(iDPE=1;iDPE<=lenDPE;iDPE++)
            {
              dpSetUnit(dsDPE[iDPE], dsUnit[iDPE]);
              dpSetDescription(dsDPE[iDPE], dsDescription[iDPE]);
            }
          }
          else
          {
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING subscription "+mDIPtype["name"]+" wrong DPE definition/setting\n", fFile);
            }
            fwException_raise(
                exceptionInfo,
                "WARNING",
                "WARNING subscription "+mDIPtype["name"]+" wrong DPE definition/setting",
                ""
            );
          }
          break;
      }
    }
  }
  else
  {
    if(fFile != 0) {
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING DIPConfig not set, skipping subscription "+mDIPtype["name"]+"\n", fFile);
    }
    fwException_raise(
        exceptionInfo,
        "WARNING",
        "WARNING DIPConfig not set, skipping subscription "+mDIPtype["name"],
        ""
    );
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Check and create the subscription DPE

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param sSubscriptionType		input, type of subscription read from the file, xsd:bool, xsd:float, xsd:int, xsd:string
@param dsDPE		list of DPE
@param dsType		input, DP type of the given DPE
@param dsName		input, the list of name xml node value
@param dsElements		input, the list of elements xml node value
@param dsAlias		input, the list of alias xml node value
@param exceptionInfo	details of any exceptions are returned here
*/
bool _fwDIPImport_checkAndCreateSubscriptionDPE(string sSubscriptionType, dyn_string &dsDPE, dyn_string dsType, dyn_string dsName, 
                                           dyn_string dsElements, dyn_string dsAlias, dyn_string &exceptionInfo)
{
  bool bOk = true;
  int i, len = dynlen(dsName);
  string sDPE, sDP;
  dyn_string dsDpel;
  dyn_int diDpelType;
  dyn_string dsAllDPType;
  
  // get all the DP type
  dsAllDPType = dpTypes();
  for(i=1;i<=len;i++)
  {
    dynClear(dsDpel);
    dynClear(diDpelType);
    if(dynContains(dsAllDPType, dsType[i]) >0) {
      _fwDIPImport_getDPE(dsType[i], dsDpel, diDpelType);
      if(dynlen(dsDpel) == 1) 
      {
        if(dsDpel[1] == dsType[i])
          dsDpel[1] = dsType[i]+".";
      }
    }
//    DebugN(dsType[i], dsDpel, diDpelType);
    // type is the DPType and must exist
    if(dynlen(dsDpel) <= 0)
    {
      bOk = false;
      i=len+1;
    }
    else
    {
      // xsdType must be of sametype as the DPE.
      // allowed xsdType are bool, int, unsigned, float, char, string
      if(!_fwDIPImport_checkDPEType(sSubscriptionType, dsType[i], dsElements[i],dsDpel, diDpelType))
      {
        bOk = false;
        i=len+1;
      }
      else
      {
        // combination of dpe, elements and alias not existing it is created
        // dpe not existing --> skip subscription
        if(dsName[i] != "")
          sDPE = dsName[i]+dsElements[i];
        else
        {
          sDP = dpAliasToName(dsAlias[i]);
          if(dpExists(sDP))
            sDP = dpSubStr(sDP, DPSUB_DP);
          sDPE =  sDP+dsElements[i];
        }
    //    DebugN(sDPE);
        if(dpExists(sDPE))
          dynAppend(dsDPE, sDPE);
        else
        {
          // create DP
          if(dsName[i] != "")
            dpCreate(dsName[i], dsType[i]);
          if(dpExists(sDPE))
            dynAppend(dsDPE, sDPE);
          else
          {
            bOk = false;
            i=len+1;
          }
        }
      }
    }
  }
  return bOk;
}
//------------------------------------------------------------------------------------------------------------------------
/** Check the type of DPE can accept the DIP subscription

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param sSubscriptionType		input, type of subscription read from the file, xsd:bool, xsd:float, xsd:int, xsd:string
@param sDPType		input, DP type
@param sElements		input, the elements xml node value=DPE
@param dsDpel		input, the list of list of DPE of the sDPType
@param diDpelType	input, the list of list of DPE type of the sDPType
*/
bool _fwDIPImport_checkDPEType(string sSubscriptionType, string sDPType, string sElements, dyn_string dsDpel, dyn_int diDpelType)
{
  bool bOk = false;
  int iPos = dynContains(dsDpel, sDPType+sElements);
  // xsdType must be of sametype as the DPE.
  // allowed xsdType are bool, int, unsigned, float, char, string
//  DebugN(dsDpel, sElements, diDpelType, DPEL_FLOAT, sSubscriptionType);
  if(iPos > 0)
  {
    switch(sSubscriptionType)
    {
      case FWDIPIMPORT_CONST_XSD_BOOL:
        if(diDpelType[iPos] == DPEL_BOOL)
          bOk = true;
        break;
      case FWDIPIMPORT_CONST_XSD_FLOAT:
        if(diDpelType[iPos] == DPEL_FLOAT)
          bOk = true;
        break;
      case FWDIPIMPORT_CONST_XSD_STRING:
        if(diDpelType[iPos] == DPEL_STRING)
          bOk = true;
        break;
      case FWDIPIMPORT_CONST_XSD_INT:
        if(diDpelType[iPos] == DPEL_INT)
          bOk = true;
        break;
      default:
        break;
    }
  }
  return bOk;
}
//------------------------------------------------------------------------------------------------------------------------
/** dump the DIP config in the log file
  All the DIP configuration of the file are kept in two variables:
- the type DIP configuration (configuration, publication, subscription) are kept in a dyn_mapping variable: dmDIPtype
- the index of the configuration in dmDIPtype is the key of the DPE list in mDIPdpeList.
- the mDIPdpeList is a mapping composed of dyn_dyn_string, each index of the dyn_dyn_string of a given key is 
the list of DPE and its configuration, the configuration is a dyn_string
- in a mapping of the list dmDIPtype one can have:
  - the DIP configuration (usually the first instance), there is no DPE list associated in mDIPdpeList
  - the publication, one mapping per publication, there could be a list of DPE in mDIPdpeList for this publication
example:
  dmDIPtype[1]: configuration, DIP config
  dmDIPtype[1]: publication, type of publication, etc.
  dmDIPtype[2]: publication, type of publication, etc.
  ....
  dmDIPtype[n]: subscription, type of publication, etc.
  
  mDIPdpeList[1]: dyn_string[1] first DPE of the publication and it configuration 
                  dyn_string[2] second DPE of the publication and it configuration
                  ...
  mDIPdpeList[2]: dyn_string[1] first DPE of the publication and it configuration
                  
  ....
  mDIPdpeList[n]: dyn_string[1] first DPE of the subscription and it configuration
  
  The DPE configuration is a dyn_string:
[1] name
[2] alias
[3] element
[4] tag
[5] buffer
[6] overwrite: true or false
[7] type: simple, single or multiple
[8] description
[9] unit

To retreive all the DIP configuration, one loops:
- over the length of dmDIPtype
- get the DPE list from mDIPdpeList with the position in the dmDIPtype

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param fFile		input, the log file name id
@param dmDIPtype		list of DIP configuration
@param mDIPdpeList		DPE list
*/
_fwDIPImport_dumpInLogFile(file fFile, dyn_mapping dmDIPtype, mapping mDIPdpeList)
{
  int iPub, lenPub, iDPE, lenDPE;
  dyn_dyn_string ddsTemp;
  lenPub = dynlen(dmDIPtype);
  int iElapsedTime, iStartTime=(int)getCurrentTime();
  
  if(fFile != 0)
  {
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO start dumping DIP config ... "+lenPub+" DIP config(s)\n", fFile, 0, lenPub);
    for(iPub=1;iPub<=lenPub;iPub++){
      iElapsedTime = (int)(getCurrentTime()-iStartTime);
      _fwDIP_setRemainingTime(iElapsedTime, iPub, lenPub, true);
      lenDPE = 0;
      if(dmDIPtype[iPub]["DIPType"] =="configuration")
      {
        _fwDIP_fputs(dmDIPtype[iPub]["DIPType"]+", name: "+dmDIPtype[iPub]["name"]+" ,publisherName: "+dmDIPtype[iPub]["publisherName"]+", "+
              "managerNumber: "+dmDIPtype[iPub]["managerNumber"]+", queryTimeout: "+dmDIPtype[iPub]["queryTimeout"]+
              ", dns: "+(dmDIPtype[iPub]["dns"]==""? "(default dns)":dmDIPtype[iPub]["dns"])+
              ", parameters: "+(dmDIPtype[iPub]["parameters"]==""? "(no parameters)":dmDIPtype[iPub]["parameters"])+"\n", fFile, iPub);
      }
      else if(dmDIPtype[iPub]["DIPType"] =="publication")
      {
        ddsTemp = mDIPdpeList[iPub];
        lenDPE = dynlen(ddsTemp);
        _fwDIP_fputs(dmDIPtype[iPub]["DIPType"]+", name: "+dmDIPtype[iPub]["name"]+" ,type: "+dmDIPtype[iPub]["type"]+", "+lenDPE+" DPE(s)\n", fFile, iPub);
      }
      else
      {
        ddsTemp = mDIPdpeList[iPub];
        lenDPE = dynlen(ddsTemp);
        _fwDIP_fputs(dmDIPtype[iPub]["DIPType"]+", name: "+dmDIPtype[iPub]["name"]+", type: "+dmDIPtype[iPub]["type"]+ 
               ", xsdType: "+dmDIPtype[iPub]["xsdType"]+", "+lenDPE+" DPE(s)\n", fFile, iPub);
      }
      for(iDPE=1;iDPE<=lenDPE;iDPE++)
      {
        _fwDIP_fputs("    name: "+ddsTemp[iDPE][1]+", alias: "+ddsTemp[iDPE][2]+", element: "+ddsTemp[iDPE][3]+ 
               ", tag: "+ddsTemp[iDPE][4]+", buffer: "+ddsTemp[iDPE][5]+", overwrite: "+ddsTemp[iDPE][6]+
               ", type: "+ddsTemp[iDPE][7]+", description: "+ddsTemp[iDPE][8]+", unit: "+ddsTemp[iDPE][9]+"\n", fFile, iPub);
      }
    }
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO end dumping DIP config ...\n", fFile, 0);
    _fwDIP_setRemainingTime(iElapsedTime, iPub, lenPub, false);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** returns the file name to be use for log file

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@return					the file name is returned
*/
string fwDIPImport_makeLogFileName()
{
  return getPath(LOG_REL_PATH) + "/fwDIPImport" + formatTime("_%Y_%m_%d_%H_%M_%S", getCurrentTime()) + ".txt";
}
//------------------------------------------------------------------------------------------------------------------------
/** Load the DIP config into memory
- the type DIP configuration (configuration, publication, subscription) are kept in a dyn_mapping variable: dmDIPtype
- the index of the configuration in dmDIPtype is the key fo the DPE list in mDIPdpeList

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param iDipMgrNumber		the DIP manager number to used
@param docNum			input, the XML doc number
@param node			input, the node number
@param type			input, type of config read from the xml file: configuration, publication, etc.
@param dmDIPtype		list of DIP configuration
@param mDIPdpeList		DPE list
@param m_dsAllPublication		all the DIP publication
@param fFile		input, the log file name id
@param exceptionInfo	details of any exceptions are returned here
*/
void fwDIPImport_loadConfigInMemory(int &iDipMgrNumber, int docNum, int node, string type, dyn_mapping &dmDIPtype, mapping &mDIPdpeList, mapping &m_dsAllPublication, file fFile, dyn_string &exceptionInfo) 
{ 
  dyn_string dsTemp;
  dyn_dyn_string ddsTemp;
  mapping mTemp;
  string sDIPConfig;
  
//DebugTN("start", node);
  if ( node != -1 ) 
  { 
    if( xmlNodeType(docNum, node) == XML_ELEMENT_NODE)
    {
      switch(xmlNodeName(docNum, node))
      {
        case "publication":
//          DebugN(type, "publication "+node, xmlElementAttributes(docNum, node));
          mTemp = xmlElementAttributes(docNum, node);
          mTemp["DIPType"] = "publication";
          if(dynlen(dmDIPtype) > 0)
          {
            if(_fwDIPImport_isPublicationUsed(mTemp["name"], m_dsAllPublication, sDIPConfig))
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING DIP publication "+mTemp["name"]+" alreay used by "+sDIPConfig+", skipping publication\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING DIP publication "+mTemp["name"]+" alreay used by "+sDIPConfig+", skipping publication",
                  ""
              );
            }
            else
            {
              dynAppend(dmDIPtype, mTemp);
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
              fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), "publication "+node, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
            }
          }
          else
          {
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing DIP configuration for publication, skipping publication\n", fFile);
            }
            fwException_raise(
                exceptionInfo,
                "WARNING",
                "WARNING missing DIP configuration for publication, skipping publication",
                ""
            );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "subscription":
//          DebugN(type, "subscription "+node, xmlElementAttributes(docNum, node));
          mTemp = xmlElementAttributes(docNum, node);
          mTemp["DIPType"] = "subscription";
          if(!mappingHasKey(mTemp, "xsdType")) 
          {
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing xsdType for "+mTemp["name"]+", skipping config\n", fFile);
            }
            fwException_raise(
                exceptionInfo,
                "WARNING",
                "WARNING missing xsdType for "+mTemp["name"],
                ""
            );
            mTemp["xsdType"] = FWDIPIMPORT_CONST_VARIABLE_NOT_SET;
          }
          else
          {
            if(dynlen(dmDIPtype) > 0)
            {
              switch(mTemp["xsdType"])
              {
                case FWDIPIMPORT_CONST_XSD_BOOL:
                case FWDIPIMPORT_CONST_XSD_FLOAT:
                case FWDIPIMPORT_CONST_XSD_STRING:
                case FWDIPIMPORT_CONST_XSD_INT:
                  dynAppend(dmDIPtype, mTemp);
                  mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
                  fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), "subscription "+node, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
                  break;
                default:
                  if(fFile != 0) {
                    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING unknown xsdType "+mTemp["xsdType"]+" for "+mTemp["name"]+", skipping subscription\n", fFile);
                  }
                  fwException_raise(
                      exceptionInfo,
                      "WARNING",
                      "WARNING unknown xsdType "+mTemp["xsdType"]+" for "+mTemp["name"]+", skipping subscription",
                      ""
                  );
                  break;
              }
            }
            else
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing DIP configuration for subscription, skipping subscription\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing DIP configuration for subscription, skipping subscription",
                  ""
              );
            }
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "configuration":
//          DebugN("configuration", xmlElementAttributes(docNum, node));
          mTemp = xmlElementAttributes(docNum, node);
          mTemp["DIPType"] = "configuration";
          if(!mappingHasKey(mTemp, "managerNumber")) 
          {
            mTemp["managerNumber"] = iDipMgrNumber;
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO using "+iDipMgrNumber+" for manager number\n", fFile);
            }
          }
          else
          {
            iDipMgrNumber = mTemp["managerNumber"];
            if(fFile != 0) {
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO using "+iDipMgrNumber+" for manager number\n", fFile);
            }
          }
          if(!mappingHasKey(mTemp, "publisherName"))
          {
            mTemp["publisherName"] = getHostname()+"_"+rand();
            if(fFile != 0) 
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO no publisherName defined, using "+mTemp["publisherName"]+" as default\n", fFile);
          }
          if(!mappingHasKey(mTemp, "queryTimeout"))
          {
            mTemp["queryTimeout"] = FWDIPIMPORT_CONST_QUERY_TIMEOUT;
            if(fFile != 0) 
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO no queryTimeout defined, using "+mTemp["queryTimeout"]+" as default\n", fFile);
          }
          if(!mappingHasKey(mTemp, "dns"))
          {
            mTemp["dns"] = "";
            if(fFile != 0) 
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO no dns defined, default dns will be used\n", fFile);
          }
          if(!mappingHasKey(mTemp, "parameters"))
          {
            mTemp["parameters"] = "";
            if(fFile != 0) 
              _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO no parameters defined\n", fFile);
          }
          // if the DIP config is used by another DIP manager, raise ERROR and skip the configuration
          // the corresponding DIP manager has already another config, raise an ERROR and skip the configuration
          if(_fwDIP_checkDIPManager(mTemp, m_dsAllPublication, exceptionInfo, fFile))
          {
            dynAppend(dmDIPtype, mTemp);
            fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), "configuration "+node, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), "", dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "dpe":
//          DebugN(type, "dpe "+node);
          ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
          dynAppend(ddsTemp, makeDynString(FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                           FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                           FWDIPIMPORT_CONST_BUFFER_DEFAULT, FWDIPIMPORT_CONST_OVERWRITE_DEFAULT, 
                                           FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                           FWDIPIMPORT_CONST_VARIABLE_NOT_SET));
          mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "name":
//          DebugN(dynlen(dmDIPtype), dmDIPtype, mappinglen(mDIPdpeList), type, "name "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[1] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "alias":
//          DebugN(type, "alias "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[2] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "element":
//          DebugN(type, "element "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[3] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "tag":
//          DebugN(type, "tag "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[4] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "buffer":
//          DebugN(type, "buffer "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[5] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "overwrite":
//          DebugN(type, "overwrite "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[6] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "type":
//          DebugN(type, "overwrite "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[7] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "description":
//          DebugN(type, "overwrite "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[8] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        case "unit":
//          DebugN(type, "overwrite "+node, fwDIPImport_getNodeValue(docNum, node));
          if((dynlen(dmDIPtype) >0) && (mappinglen(mDIPdpeList) >0))
          {
            ddsTemp = mDIPdpeList[dynlen(dmDIPtype)];
            if(dynlen(ddsTemp) <=0)
            {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing dpe node for "+xmlNodeName(docNum, node),
                  ""
              );
            }
            else
            {
              dsTemp = ddsTemp[dynlen(ddsTemp)];
              dsTemp[9] = fwDIPImport_getNodeValue(docNum, node);
              ddsTemp[dynlen(ddsTemp)] = dsTemp;
              mDIPdpeList[dynlen(dmDIPtype)] = ddsTemp;
            }
          }
          else
          {
              if(fFile != 0) {
                _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
              }
              fwException_raise(
                  exceptionInfo,
                  "WARNING",
                  "WARNING missing corresponfing publication/subscription node for "+xmlNodeName(docNum, node),
                  ""
              );
          }
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo);
          break;
        default:
//          DebugN("unknwn", type, xmlNodeName(docNum, node), node);
          if(fFile != 0) {
            _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING unknown node name "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
          }
          fwException_raise(
              exceptionInfo,
              "WARNING",
              "WARNING unknown node name "+xmlNodeName(docNum, node),
              ""
          );
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
          fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
          break;
      }
    }
    else if(xmlNodeType(docNum, node) == XML_PROCESSING_INSTRUCTION_NODE)
    {
      fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
      fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
    }
    else if(xmlNodeType(docNum, node) == XML_COMMENT_NODE)
    {
      fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
      fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
    }
    else
    {
      if(fFile != 0) {
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING unknown type "+xmlNodeType(docNum, node)+", "+xmlNodeName(docNum, node)+", skipping config\n", fFile);
      }
      fwException_raise(
          exceptionInfo,
          "WARNING",
          "WARNING unknown type "+xmlNodeType(docNum, node)+", "+xmlNodeName(docNum, node),
          ""
      );
      fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlFirstChild(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
      fwDIPImport_loadConfigInMemory(iDipMgrNumber, docNum, xmlNextSibling(docNum, node), type, dmDIPtype, mDIPdpeList, m_dsAllPublication, fFile, exceptionInfo); 
    }
  }
//  else
//   DebugTN("end", node);
}

/***************
ENS-8696
 function to remove when the ENS-8696 will be implemented
****************/
_fwDIPImport_getDPE(string sDpType, dyn_string &dsDpel, dyn_int &diDpelType)
{
  dyn_dyn_string ddsElements, ddsElements1;
  dyn_dyn_int ddiTypes, ddiTypes1;
  int i, j, lenI, lenJ, iType, iBis, lenIbis;
  string sTemp, sTempBis, sTypeRef;
  dyn_string dsTemp;
  dyn_int diTemp;
  dyn_string ds;
  dyn_int di;
  
  dpTypeGet(sDpType, ddsElements1, ddiTypes1);
  lenI = dynlen(ddsElements1);
//DebugN("**** before", lenI, sDpType, ddsElements1, ddiTypes1);
// remove all the empty dyn_string, dyn_int: case of dpref.
  for(i=1;i<=lenI;i++) {
    if(dynlen(ddsElements1[i]) > 0) {
      ddsElements[dynlen(ddsElements)+1] = ddsElements1[i];
      ddiTypes[dynlen(ddiTypes)+1] = ddiTypes1[i];
    }
  }
  lenI = dynlen(ddsElements);
//DebugN("**** result", sDpType, lenI, ddsElements, ddiTypes);

  for(i=2;i<=lenI;i++) {
    lenJ = dynlen(ddsElements[i]);
    for(j=1; j<=lenJ; j++) {
      if(ddsElements[i][j] == "") {
//DebugN(i, j, lenJ, ddsElements[i][j], ddsElements[i-1][j]);
        ddsElements[i][j] = ddsElements[i-1][j];
      }
    }
  }
//  DebugN("******* after", ddsElements, ddiTypes);
  for(i=1;i<=lenI;i++) {
    lenJ=dynlen(ddiTypes[i]);
    sTemp = "";
    sTypeRef = "";
    ds = makeDynString();
    di = makeDynInt();
    for(j=1;j<=lenJ;j++) {
      switch(ddiTypes[i][j]) {
        case DPEL_TYPEREF:
//DebugN(i, j, ddsElements[i][j], "DPEL_TYPEREF");  
          iType = ddiTypes[i][j];
          sTempBis = ddsElements[i][j];
          sTypeRef = ddsElements[i][j+1];
          _fwDIPImport_getDPE(sTypeRef, ds, di);
          break;
        default:
//DebugN(i, j, ddsElements[i][j], "default", ddiTypes[i][j]);  
          iType = ddiTypes[i][j];
          sTempBis = ddsElements[i][j];
          break;
      }
      if(sTemp == "") 
        sTemp = sTempBis;
      else
        sTemp = sTemp+"."+sTempBis;
//DebugN("sTempBis="+sTempBis, "stemp="+sTemp);
    }
    if(sTemp != "") {
      if(iType == DPEL_TYPEREF) {
        lenIbis = dynlen(ds);
        for(iBis = 1; iBis<=lenIbis;iBis++) {
          dynAppend(dsTemp, sTemp+substr(ds[iBis], strlen(sTypeRef), strlen(ds[iBis])));
          dynAppend(diTemp, di[iBis]);
        }
      }
      else {
        dynAppend(dsTemp, sTemp);
        dynAppend(diTemp, iType);
      }
    }
  }
//  DebugN(dsTemp);
  dsDpel = dsTemp;
  diDpelType = diTemp;
}

/***************
** ENS-8684, ENS-8683, ENS-8682
 function to remove when the fwInstallation function will:
- remove the contraint on the block Ui and do not add manager when callin the fwInstallationManager_add
- the fwInstallation will work fine in scaterred UI.
****************/


/** This function allows to insert a manager into a project. It is checked before, if the 
manager already exists.

@param manager				name of the manager
@param startMode			{manual, once, always}
@param secKill				seconds to kill after stop
@param restartCount		number of restarts
@param resetMin				restart counter reset time (minutes)
@param commandLine		commandline for the manager
@param host	hostname
@param port	pmon port
@param user	pmon user
@param pwd		pmon password
@return 1 - manager added, 2 - manager already existing, 3 - manager addition disabled, 0 - manager addition failed
@author F. Varela (original idea by S. Schmeling)
*/
int _fwDIPImport_add(string manager, 
                              string startMode, 
                              int secKill, 
                              int restartCount, 
                              int resetMin,
                              string commandLine, 
                              string host = "", 
                              int port = 4999, 
                              string user = "", 
                              string pwd = "")
{
	bool failed, disabled;
	string str;
  dyn_mixed managerInfo;
  
  if(host == "")
  {
    host = fwInstallation_getHostname(); 
    port = pmonPort();
  }
  
  if(user == "")
  {
    if(fwInstallation_getPmonInfo(user, pwd) != 0)
    {
      fwInstallation_throw("_fwDIPImport_add: Could not resolve pmon username and password. Action aborted", "error", 1);
      return -1;
    }
  }
  bool blockUis = false;
  string dp = fwInstallation_getInstallationDp();
  dpGet(dp + ".addManagersDisabled", disabled,
        dp + ".blockUis", blockUis);
        
  fwInstallationManager_getProperties(manager, commandLine, managerInfo, host, port, user, pwd);
//  if(managerInfo[FW_INSTALLATION_MANAGER_PMON_IDX] != -1)
//  {
//    DebugN("BUG", str, host, port);
//    return 2; //Manager already in the PVSS console
//  }
  //!!!!! 
  str = user + "#" + pwd + "#SINGLE_MGR:INS "+_fwDIPImport_pmonGetCount(host, port)+" "+
        manager+" "+startMode+" "+secKill+" "+restartCount+" "+
        resetMin+" "+commandLine;
  
  if(pmon_command(str, host, port, FALSE, TRUE))
  {
//    DebugN("EROR", str, host, port);
    fwInstallation_throw("Failed to insert manager: "  + manager + " " + commandLine + " " + host + " " + port + " "+user);
    return 0;
  } 
  
//    DebugN("OK", str, host, port);
  return 1;//unfortunately this is not coherent with the convetion followed in the libraries (legacy);
}
// _fwDIPImport_pmonGetCount
/** Equivalent to pmonGetCount(). This function returns the number of manager in the console.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sHost  input, pmon hostname
@param iPort input, pmon port number
@return the number of manager in the console
*/
int _fwDIPImport_pmonGetCount(string sHost, int iPort)
{
  bool bFailed;
  int iPmonCount = -1, i, len;
  dyn_dyn_string ddsResult;
  
  bFailed = pmon_query("#"+"#MGRLIST:LIST", sHost, iPort, ddsResult, false, true);
  if(!bFailed)
  {			
    len = dynlen(ddsResult);
    for(i=1;i<=len;i++)
    {
      if(dynlen(ddsResult[i]) == 5) // case no argument
        dynAppend(ddsResult[i], "");
      if (dynlen(ddsResult[i])>=6)
      {			
        iPmonCount = i;
      }
    }
  }
//  DebugTN(pmonGetCount(), iPmonCount);
  return iPmonCount;
}
// _fwDIP_fputs
/** dump string to a file and the screen log.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sString  input, string to write to the log and the screen log
@param fFile input, file descriptor
@param iPos input, progress bar value
@param iMax input, progress bar max
*/
_fwDIP_fputs(string sString, file fFile, int iPos=-1, int iMax = -1)
{
  int pos;
  string sStringToScreen=sString;
  int iCurrentTime=(int)getCurrentTime();
  fputs(sString, fFile);
  if(myManType() == UI_MAN)
  {
    if(shapeExists("messageLog"))
    {
      pos = strpos(sStringToScreen, "\n", strlen(sStringToScreen)-1);
      if(pos>0)
        sStringToScreen = substr(sStringToScreen, 0, pos);
      messageLog.appendItem(sStringToScreen);
      messageLog.bottomPos(messageLog.itemCount);
      if((iCurrentTime - g_iLastTime) > 2)
      {
  //      DebugTN("delay");
        delay(0,5);
        g_iLastTime = iCurrentTime;
      }
    }
	_fwDIP_setProgressBar(iPos, iMax);
  }
}
// _fwDIP_setProgressBar
/** set the progress bar.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param iPos input, progress bar value
@param iMax input, progress bar max
*/
public _fwDIP_setProgressBar(int iPos=-1, int iMax = -1)
{
  int iCurrentTime=(int)getCurrentTime();
  if(myManType() == UI_MAN)
  {
    if(shapeExists("progressBar"))
    {
       if((iCurrentTime - g_iLastTime) > 2)
       {
         delay(0,5);
         g_iLastTime = iCurrentTime;
       }
      if (iMax >0) {
        setValue("progressBar", "maximum", iMax);
      }
      if(iPos >=0) {
        setValue("progressBar", "progress", iPos);
      }
    }
  }
}
// _fwDIP_setRemainingTime
/** set the remaining time.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param iElapsedTime input, elapsed time since the beginning
@param iDone input, number of element processed
@param iTotal input, total number of element
@param bVisible input, visibility of the text
*/
_fwDIP_setRemainingTime(int iElapsedTime, int iDone, int iTotal, bool bVisible)
{
  int iCurrentTime=(int)getCurrentTime();
  float fTime;
  string sTime;
  if(myManType() == UI_MAN)
  {
    if(shapeExists("remainingText"))
    {
       fTime = (float)iElapsedTime/(float)iDone;
       if(fTime <=0.1)
         fTime = 0.1;
       fTime *= (float)(iTotal-iDone);
       remainingText.text = "Remaining time: "+(int)fTime +" sec.";
       remainingText.visible = bVisible;
       sTime = (time)((int)getCurrentTime()+(int)fTime);
       endTime.text = "Estimated end time: "+sTime;
       endTime.visible = bVisible;
       if((iCurrentTime - g_iLastTime) > 2)
       {
//         DebugTN("delay", fTime, iDone, iTotal, iElapsedTime);
         delay(0,5);
         g_iLastTime = iCurrentTime;
       }
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
//@}
