/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwDeviceComment.ctl

@author
  Dr. Marc Bengulescu (BE-ICS-FD)
*/



#uses "fwDeviceComment/fwDeviceCommentDeprecated"
#uses "fwDeviceComment/fwDeviceCommentImportExport"
#uses "fwDeviceComment/fwDeviceCommentTest"
#uses "fwDeviceComment/unDeviceComment"
#uses "fwDeviceComment/fwDeviceFilter"

global string FW_DEVICE_COMMENT_CLASS; //Set to $sDeviceCommentClass in the panel init function

const string FW_DEVICE_COMMENT_DELIMITER = "~";

const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_DPE         = 0;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_APPLICATION = 1;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE      = 2;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_TIMESTAMP   = 3;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_USER        = 4;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT     = 5;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_EXTENSION   = 6;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_SYSTEM      = 7;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_HOST        = 8;
const unsigned FW_DEVICE_COMMENT_TABLE_COLUMN_ACTIVE      = 9;

const string FW_DEVICE_COMMENT_TABLE_REF = "fwDeviceComment_commentsTable";
const string FW_DEVICE_COMMENT_FWDC_PANEL_REF = "PANEL_REF1";
const string FW_DEVICE_COMMENT_FWDC_PANEL_TITLE = "Device Comments";
const string FW_DEVICE_COMMENT_FWDC_PANEL_COMMCHANGE_CB = "unDeviceCommentsPanel_commentsChangedCB";


//global string     gFwDeviceComment_getCommentsFunction;
global string     gFwDeviceComment_populateTableFunction;
global dyn_string gFwDeviceComment_tableChangedCallbackFunction;
global dyn_string gFwDeviceComment_filterAppliedCallbackFunction;
global unsigned   gFwDeviceComment_totalShown = 0;
private global dyn_dyn_anytype gFwDeviceComment_filteredComments;
private global bool gFwDeviceComment_bUserHasCancelled;

public void fwDeviceComment_init()
{
  fwDeviceComment_commentsTable.updatesEnabled = false;

  // set default sorting order to
  fwDeviceComment_commentsTable.sortOrig(makeDynBool(true, true, false),
                                         fwDeviceComment_commentsTable.columnToName(FW_DEVICE_COMMENT_TABLE_COLUMN_APPLICATION),
                                         fwDeviceComment_commentsTable.columnToName(FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE),
                                         fwDeviceComment_commentsTable.columnToName(FW_DEVICE_COMMENT_TABLE_COLUMN_TIMESTAMP));

  fwDeviceComment_commentsTable.updatesEnabled = true;
  
  fwDeviceComment_addTableChangedCallback("fwDeviceComment_sortTableAdjustColumns");

}

/**
* @reviewed 2018-06-22 @whitelisted{Callback}
*/
private void fwDeviceComment_sortTableAdjustColumns()
{
  gFwDeviceComment_totalShown = fwDeviceComment_commentsTable.lineCount(); 
  fwDeviceComment_commentsTable.sortOrig(0);
  fwDeviceComment_commentsTable.adjustColumn(FW_DEVICE_COMMENT_TABLE_COLUMN_APPLICATION);
  fwDeviceComment_commentsTable.adjustColumn(FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE);
}

public void fwDeviceComment_addRows(dyn_string dpe, dyn_string application, dyn_string device, dyn_time timestamp, dyn_string user, dyn_string comment, 
                                      dyn_string commentExt, dyn_string sys, dyn_string host, dyn_bool active) {  
  fwDeviceComment_commentsTable.appendLines(
       dynlen(device),
       "dpe", dpe,
       "application", application,
       "device", device,
       "timestamp", timestamp,
       "user", user, 
       "comment", comment, 
       "extension", commentExt, 
       "system", sys, 
       "host", host,
       "active", active
   );
}

public void fwDeviceComment_deleteAllRows() {
  fwDeviceComment_commentsTable.deleteAllLines();
}

/**
  Deletes all the comments for the specified device
*/
public void fwDeviceComment_deleteDeviceRows(string sDeviceDPE)
{
  dyn_dyn_string ddsListOfValues; //The values for key1 that are searched in the table
  shape shpCommentsTable;

  ddsListOfValues[1]= makeDynString(sDeviceDPE);
  
  fwDeviceComment_commentsTable.deleteLines(1,
                                            fwDeviceComment_commentsTable.columnToName(FW_DEVICE_COMMENT_TABLE_COLUMN_DPE),
                                            ddsListOfValues); // delete matching lines
}


/**
* Apply a filter to the table.
*  
* dpFilterList is the patterns of DPs to match for populating the table. 
* The filter is then applied after the table has been populated.
* The table will only be populated if hasDpFilterListChanged = true
* and it will only be filtered if hasChanged = true.
*  
* filterValues is a mapping { columnN => dyn_string(valuesToMatch) } or { columnN => "*" }
*
* @reviewed 2018-06-21 @whitelisted{Callback}
*
*/
public void fwDeviceComment_applyFilter(unsigned maxRows, bool hasDpFilterListChanged, bool hasChanged, dyn_string dpFilterList, 
                                          time start, time end, mapping filterValues) { 
   //firstly, repopulate the table if required
   if (hasDpFilterListChanged) {     
     fwDeviceComment_populateTable(maxRows, dpFilterList);      
   }    
   
   //then filter the table  
   if (hasChanged) {    
     gFwDeviceComment_totalShown = 0; 
     unsigned tableMax = fwDeviceComment_commentsTable.lineCount();
    
     //hide every row first
     for (unsigned rowN = 0; rowN < tableMax; rowN++) {
       fwDeviceComment_commentsTable.hideRow(rowN);
     }
      
     if (filterValues[(string)FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT] != "*") {
       //wrap comment filter in *s
       unsigned commentLength = dynlen(filterValues[(string)FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT]);
       for (unsigned i = 1; i <= commentLength; i++) {
         filterValues[(string)FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT][i]
             = "*" + filterValues[(string)FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT][i] + "*";
       }
     }
   
     unsigned filterLength = mappinglen(filterValues);     

     for (unsigned rowN = 0; rowN < tableMax; rowN++) {
       bool show = true;
       dyn_anytype row = fwDeviceComment_commentsTable.getLineN(rowN);
        
       if (!(row[FW_DEVICE_COMMENT_TABLE_COLUMN_TIMESTAMP + 1] >= start 
               && row[FW_DEVICE_COMMENT_TABLE_COLUMN_TIMESTAMP + 1] <= end)) {
         show = false;
       } else {             
         for (unsigned filterN = 1; filterN <= filterLength; filterN++) {
           unsigned columnN = mappingGetKey(filterValues, filterN);
           anytype  pattern = mappingGetValue(filterValues, (string)filterN);
         
           bool lShow = false;
           if (pattern == "*") {
             lShow = true;
           } else {
             unsigned dynLen = dynlen(pattern);
             string rowValue = row[columnN+1];
             
             //if it's the comment field then combine it with the comment extension field
             if (columnN == FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT) {
               rowValue = rowValue + row[FW_DEVICE_COMMENT_TABLE_COLUMN_EXTENSION + 1];
             }
             //check any of the filter values against the row value
             for (unsigned x = 1; x <= dynLen; x++) {
               if (patternMatch(pattern[x], rowValue)) {
                 lShow = true;
                 break;
               }
             }
           }
           show = show & lShow;
         }   
       }
       if (show) {
         gFwDeviceComment_totalShown++;
         fwDeviceComment_commentsTable.showRow(rowN);
       }
     }
   }
   
  unsigned callbackFunctionCount = dynlen(gFwDeviceComment_filterAppliedCallbackFunction);
  
  if (callbackFunctionCount > 0) {
    unsigned totalResults = fwDeviceComment_commentsTable.lineCount();  
    
    for (unsigned i = 1; i <= callbackFunctionCount; i++) {
      if (isFunctionDefined(gFwDeviceComment_filterAppliedCallbackFunction[i])) {
          execScript(
        			"main(unsigned totalResults, unsigned totalShown)" +
        			"{" +
        			"  " + gFwDeviceComment_filterAppliedCallbackFunction[i] + "(totalResults, totalShown);" +
        			"}",
        			makeDynString(),
           totalResults,
           gFwDeviceComment_totalShown
        		);        
      }
    }
  }  
}

public void fwDeviceComment_setPopulateTableFunction(string functionName) {
  if (!isFunctionDefined(functionName)) {
    //throw exception
  }
  gFwDeviceComment_populateTableFunction = functionName;
}

public void fwDeviceComment_populateTable(unsigned maxRows, dyn_string dpFilter = makeDynString(), bool bClearTable = true)
{    
  if (isFunctionDefined(gFwDeviceComment_populateTableFunction))
  {
    callFunction(gFwDeviceComment_populateTableFunction, maxRows, dpFilter, bClearTable);
  }

  unsigned callbackFunctionCount = dynlen(gFwDeviceComment_tableChangedCallbackFunction);

  if (callbackFunctionCount > 0)
  {
    for (unsigned i = 1; i <= callbackFunctionCount; i++)
    {
      if (isFunctionDefined(gFwDeviceComment_tableChangedCallbackFunction[i]))
      {
        callFunction(gFwDeviceComment_tableChangedCallbackFunction[i]);
      }
    }
  } 

}

public void fwDeviceComment_enable(const bool enable) {
  setValue("fwDeviceComment_commentsTable", "enabled", enable);
  setValue("importExportBtn", "enabled", enable);  
}


/**
* Store to a global variable the DPs of all the visible rows in the table 
* Used for: exporting comments
*/
public bool fwDeviceComment_setFilteredComments(dyn_dyn_anytype ddaFilteredComments)
{
  gFwDeviceComment_filteredComments = ddaFilteredComments;
}


/**
* Retrive from a global variable the DPs of all the visible rows in the table 
* Used for: exporting comments
*/
public bool fwDeviceComment_getFilteredComments(dyn_dyn_anytype &ddaFilteredComments)
{
  ddaFilteredComments = gFwDeviceComment_filteredComments;
}

/**
* Add a table change callback - it will be executed when the table is repopulated.
*/
public void fwDeviceComment_addTableChangedCallback(string cbFunctionName) {
  if (!isFunctionDefined(cbFunctionName)) {
    //thow exception
  }
  dynAppend(gFwDeviceComment_tableChangedCallbackFunction, cbFunctionName);
}

/**
* Add a table change callback - it will be executed when the table is repopulated.
*/
public void fwDeviceComment_addFilterAppliedCallback(string cbFunctionName) {
  if (!isFunctionDefined(cbFunctionName)) {
    //thow exception
  }
  dynAppend(gFwDeviceComment_filterAppliedCallbackFunction, cbFunctionName);
}

public void fwDeviceComment_clearCallbacks() {
  dynClear(gFwDeviceComment_filterAppliedCallbackFunction);
  dynClear(gFwDeviceComment_tableChangedCallbackFunction);
  gFwDeviceComment_populateTableFunction = "";
}

// ------------------------------------------------------------------------------------------------

/**
* Displays a progressbar when searching for comments
*/
public void fwDeviceComment_showProgress(int iProgress, int iTotal)
{
  if ( 0 == iProgress )
  // do some initializations here
  {
    rectangleBusyBar.totalSteps(iTotal);
    rectangleBusyBar.maximum(iTotal);
    rectangleBusyBar.visible = true;
    cancelButton.visible = true;
    cancelButton.enabled = true;
    fwDeviceComment_setUserHasCancelled(false);
  } else if ( iTotal == iProgress)
  // all done, hide the progress bar
  {
    rectangleBusyBar.visible = false;
    cancelButton.visible = false;
    cancelButton.enabled = false;
  }

  // set the actual progress value
  rectangleBusyBar.progress(iProgress);
  
}

// ------------------------------------------------------------------------------------------------

/** 
  Returns the panel shape object for fwDeviceComment.pnl
*/
private shape fwDeviceComment_getPanelShape()
{

  shape shpPanel; // shape placeholder
  
  // get the current module
  string sModuleName = myModuleName(); 

  // split module name on semicolon
  dyn_string dsModuleSplit = strsplit(sModuleName, ";"); 
  
  // copose panel name: "module_no - Device Comments"
  string sPanelName = (char) dsModuleSplit[1][strlen(dsModuleSplit[1])-1] + " - " + FW_DEVICE_COMMENT_FWDC_PANEL_TITLE; // see _unGraphicalFrame_getModuleId

  // alternative: unGraphicalFrame_isPanelOpen(FW_DEVICE_COMMENT_FWDC_PANEL_TITLE)
  if ( isPanelOpen(sPanelName, sModuleName) )
  // panel is displayed
  {
    // get the panel shape  
    shpPanel = getShape(sModuleName + "." + sPanelName + ":" + FW_DEVICE_COMMENT_FWDC_PANEL_REF);
  }

  return shpPanel;
}
    

// ------------------------------------------------------------------------------------------------

/** Shows feedback to the user 
*   This function should be called in a seperate thread, i.e. startThread("showUserFeedback","Message to the user");*
*/
public void fwDeviceComment_showUserFeedback(string sMessage)
{
  shape shpPanel = fwDeviceComment_getPanelShape();
  
  if ( shpPanel )  
  {
    shpPanel.fwDeviceCommentPanel_showUserFeedback(sMessage);
  }
}

// ------------------------------------------------------------------------------------------------

/**
* Retrieves from a global variable the *userHasCancelled* state 
* Used for: cancelling search/filter
*/
public bool fwDeviceComment_getUserHasCancelled()
{
  return gFwDeviceComment_bUserHasCancelled;
}

// ------------------------------------------------------------------------------------------------

/**
* Stores to a global variable the *userHasCancelled* state 
* Used for: cancelling search/filter
*/
public void fwDeviceComment_setUserHasCancelled(bool bUserHasCancelled)
{
  gFwDeviceComment_bUserHasCancelled = bUserHasCancelled;
}

// ------------------------------------------------------------------------------------------------

/**
  Returns the indexes of only the *visible* rows from the provied row index list.
  
  *visible = actually displayed in the table
  
  The WinCC-OA function getSelectedLines() also includes any
  filtered out rows that are not displayed in the table.
*/
void fwDeviceComment_removeHiddenRows(dyn_int &diRowIndexes)
{
  dyn_int diTemp = diRowIndexes;

  dynClear(diRowIndexes);

  for (int i = 1; i <= dynlen(diTemp); i++)
  {
    if ( false == fwDeviceComment_commentsTable.isRowHidden(diTemp[i]) )
    {
      dynAppend(diRowIndexes, diTemp[i]);
    }
  }
}
