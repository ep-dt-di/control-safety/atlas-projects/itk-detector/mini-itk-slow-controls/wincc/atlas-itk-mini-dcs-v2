/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwDeviceFilter.ctl

@author
  Dr. Marc Bengulescu (BE-ICS-FD)
*/



#uses "fwGeneral/fwGeneral.ctl"
#uses "fwDeviceComment/fwDeviceComment.ctl"
#uses "fwAlarmHandling/fwAlarmHandling.ctl"
#uses "unDistributedControl/unDistributedControl.ctl"

const string FW_DEVICE_FILTER_LIST_APPLICATION_REF = "fwDeviceFilter_applicationSelection";
const string FW_DEVICE_FILTER_LIST_DEVICE_TYPE_REF = "fwDeviceFilter_deviceTypeSelection";
const string FW_DEVICE_FILTER_LIST_NATURE_REF      = "fwDeviceFilter_natureSelection";
const string FW_DEVICE_FILTER_LIST_DOMAIN_REF      = "fwDeviceFilter_domainSelection";
const string FW_DEVICE_FILTER_TIME_PERIOD_REF      = "fwDeviceFilter_timePeriod";
const string FW_DEVICE_FILTER_PLACEHOLDER_REF      = "fwDeviceFilter_filterPlaceHolder";
const string FW_DEVICE_FILTER_REF                  = "fwDeviceFilter";

const string FW_DEVICE_FILTER_CUSTOM_COMBO_FIELD_NAME = "fwAlarmScreenGeneric_combo_filterFieldEdit";

const string FW_DEVICE_FILTER_MAX_ROWS_REF         = "fwDeviceFilter_maxRows";

const signed FW_DEVICE_FILTER_LIST_APPLICATION = -1;
const signed FW_DEVICE_FILTER_LIST_DEVICE_TYPE = -2;
const signed FW_DEVICE_FILTER_LIST_NATURE      = -3;
const signed FW_DEVICE_FILTER_LIST_DOMAIN      = -4;
const signed FW_DEVICE_FILTER_TIME_PERIOD      = -5;

const unsigned FW_DEVICE_FILTER_LIST_1 = 1;
const unsigned FW_DEVICE_FILTER_LIST_2 = 2;
const unsigned FW_DEVICE_FILTER_LIST_3 = 3;
const unsigned FW_DEVICE_FILTER_LIST_4 = 4;
const unsigned FW_DEVICE_FILTER_LIST_5 = 5;
const unsigned FW_DEVICE_FILTER_LIST_6 = 6;


const string FW_DEVICE_FILTER_OPTION_ALL  = "*";
const string FW_DEVICE_FILTER_OPTION_LIST = "List...";

const unsigned FW_DEVICE_FILTER_TYPE_COMBO_CUSTOM = 0;
const unsigned FW_DEVICE_FILTER_TYPE_COMBO        = 1;
const unsigned FW_DEVICE_FILTER_TYPE_TEXT         = 2;
const unsigned FW_DEVICE_FILTER_TYPE_TIME_PERIOD  = 3;
const unsigned FW_DEVICE_FILTER_TYPE_UNDEFINED    = 255;

const unsigned FW_DEVICE_FILTER_MAX_FILTER = FW_DEVICE_FILTER_LIST_6;
const unsigned FW_DEVICE_FILTER_MIN_FILTER = FW_DEVICE_FILTER_TIME_PERIOD;

const unsigned FW_DEVICE_FILTER_DEFAULT_MAX_ROWS = 1500;

const unsigned FW_DEVICE_FILTER_DEFAULT_TIME_FILTER_START = 2592000; //1 Month

global dyn_string gFwDeviceFilter_applyCBFunction = "";
global string gFwDeviceFilter_initialiseDpListFunction = "";
global string gFwDeviceFilter_groupQueryFunction = "";
global string gFwDeviceFilter_filterStartCB = "";
global string gFwDeviceFilter_filterEndCB = "";

global unsigned gFwDeviceFilter_previousMaxRows = FW_DEVICE_FILTER_DEFAULT_MAX_ROWS;
global mapping gFwDeviceFilter_previousValues;

global mapping gFwDeviceFilter_defaults;

public void fwDeviceFilter_init() {      
  //reset previous values
  mappingClear(gFwDeviceFilter_previousValues);
  gFwDeviceFilter_previousMaxRows = FW_DEVICE_FILTER_DEFAULT_MAX_ROWS;
  
  if (!shapeExists("fwAlarmScreenGeneric_systemInfoLabel")) {
    //TODO this is required but it depends on fwAlarmScreen now. generalise??
    addSymbol(
        myModuleName(), 
        myPanelName(), 
        "fwAlarmHandling/fwAlarmScreenGenericSystemInfo.pnl",
        "fwAlarmScreen_systemInfo",
        makeDynString(),
        0, 0, 0, 1, 1
    );
    fwAlarmScreenGeneric_initSystemInfo();
  }  
  
  // 1. set global
  synchronized(g_dsTreeApplication) {    
  
   // Init device type list 
    dyn_string dsDeviceTypeList;
    unGenericDpFunctions_getUnicosObjects(dsDeviceTypeList);
    dynUnique(dsDeviceTypeList);    
    
    //fwAlarmScreenGeneric_combocheckbox* should be moved to a general library
    fwAlarmScreenGeneric_combocheckbox_enableWidget(FW_DEVICE_FILTER_LIST_DEVICE_TYPE_REF, false);
    fwAlarmScreenGeneric_combocheckbox_setItems(FW_DEVICE_FILTER_LIST_DEVICE_TYPE_REF, dsDeviceTypeList);
  
    dyn_string dsSystemNames;    
    dyn_uint duSystemIds;
    dyn_string dsHost;
    dyn_int diPort;
  
    unDistributedControl_getAllDeviceConfig(dsSystemNames, duSystemIds, dsHost, diPort);          // System names        
    
    dynInsertAt(dsSystemNames, FW_DEVICE_FILTER_OPTION_ALL, 1);
    //dynAppend(dsSystemNames, UNALARMSCREEN_LIST_NAME); //why?
    dynUnique(dsSystemNames); 

    // Init domain list
    // Remove "*" and "List"
    dyn_string dsDomainList = g_dsTreeSubsystem1;
    
    int iAllPos = dynContains(dsDomainList, FW_DEVICE_FILTER_OPTION_ALL);
    if (iAllPos > 0)
    {
      dynRemove(dsDomainList, iAllPos);
    }      
    int iListPos = dynContains(dsDomainList, FW_DEVICE_FILTER_OPTION_LIST);
    if (iListPos > 0)
    {
      dynRemove(dsDomainList, iListPos);
    }    
      
    fwAlarmScreenGeneric_combocheckbox_enableWidget(FW_DEVICE_FILTER_LIST_DOMAIN_REF, false);   
    fwAlarmScreenGeneric_combocheckbox_setItems(FW_DEVICE_FILTER_LIST_DOMAIN_REF, dsDomainList);

     // Init nature list 
    // Remove "*" and "List"
    dyn_string dsNatureList = g_dsTreeSubsystem2;
    iAllPos = dynContains(dsNatureList, FW_DEVICE_FILTER_OPTION_ALL);

    if (iAllPos > 0)
    {
      dynRemove(dsNatureList, iAllPos);
    }      
    iListPos = dynContains(dsNatureList, FW_DEVICE_FILTER_OPTION_LIST);
    if (iListPos > 0)
    {
      dynRemove(dsNatureList, iListPos);
    }    
    fwAlarmScreenGeneric_combocheckbox_enableWidget(FW_DEVICE_FILTER_LIST_NATURE_REF, false);   
    fwAlarmScreenGeneric_combocheckbox_setItems(FW_DEVICE_FILTER_LIST_NATURE_REF, dsNatureList);
    
    // Init application list
    dyn_string dsApplication = g_dsTreeApplication;
      
    // Load application filter
    if (g_unicosHMI_bApplicationFilterInUse) {
      synchronized(g_unicosHMI_dsApplicationList) {
        // The "*" disappears, which is necessary to ignore unwanted applications
        dsApplication = dynIntersect(dsApplication, g_unicosHMI_dsApplicationList);    
                
        // If only not visible (not connected) applications are filtered display the information
        if (dynlen(dsApplication) < 1)
        {
          dynAppend(dsApplication,"--Empty filter--");
        }
        else
        {        
          dynInsertAt(dsApplication, FW_DEVICE_FILTER_OPTION_ALL, 1);
          dynAppend(dsApplication, FW_DEVICE_FILTER_OPTION_LIST);
        }
      }
    }
      
    iAllPos = dynContains(dsApplication, FW_DEVICE_FILTER_OPTION_ALL);
    if (iAllPos > 0) {
      dynRemove(dsApplication, iAllPos);
    }      
    iListPos = dynContains(dsApplication, FW_DEVICE_FILTER_OPTION_LIST);
    if (iListPos > 0) {
      dynRemove(dsApplication, iListPos);
    }         
      
    fwAlarmScreenGeneric_combocheckbox_setItems(FW_DEVICE_FILTER_LIST_APPLICATION_REF, dsApplication);
    fwAlarmScreenGeneric_combocheckbox_enableWidget(FW_DEVICE_FILTER_LIST_APPLICATION_REF, false);    
  }

    int iXPos, iYPos;
    // Display date time picker
    if (!shapeExists(FW_DEVICE_FILTER_TIME_PERIOD_REF)) {
      getValue("dateTimeWidgetPlaceholder", "position", iXPos, iYPos);
      addSymbol(
            myModuleName(), 
            myPanelName(), 
            "fwGeneral/fwGeneralDateTimeWidget.pnl",
            FW_DEVICE_FILTER_TIME_PERIOD_REF, 
            makeDynString("$sStartDate:", "$sEndDate:",
                         "$sStartTime:", "$sEndTime:", 
                         "$bDateAndTime:TRUE", "$bShowTimeZone:TRUE", "$sTimeZone:", 
                         "$iTimePeriod:600", "$bEnabled:FALSE"), 
            iXPos,
            iYPos, 
            0, 
            1, 
            1
          );
      while(!shapeExists(FW_DEVICE_FILTER_TIME_PERIOD_REF)) {
        delay(0,50);//wait for datetimewidget to be added
      }      
    }   
      
    delay(0,50);//wait for datetimewidget to be ready

    fwDeviceFilter_enable(true);
    fwDeviceFilter_resetFilter(false);
}

public fwDeviceFilter_clearCallbacks() {
  dynClear(gFwDeviceFilter_applyCBFunction);
  gFwDeviceFilter_filterStartCB = "";
  gFwDeviceFilter_filterEndCB = "";
}

public void fwDeviceFilter_addApplyFilterCallbackFunction(const string cbFunctionName) {
  dynAppend(gFwDeviceFilter_applyCBFunction, cbFunctionName);
}

public void fwDeviceFilter_setFilterStartCallbackFunction(const string cbFunctionName) {
  gFwDeviceFilter_filterStartCB = cbFunctionName;
}

public void fwDeviceFilter_setFilterEndCallbackFunction(const string cbFunctionName) {
  gFwDeviceFilter_filterEndCB = cbFunctionName;
}

public void fwDeviceFilter_resetFilter(bool apply = true) {
  //enable the dynamic lists
  for (unsigned n = 1; n <= FW_DEVICE_FILTER_MAX_FILTER; n++) {
     int type = _fwDeviceFilter_getFilterType(n);
     if (type == FW_DEVICE_FILTER_TYPE_COMBO) {
       if (mappingHasKey(gFwDeviceFilter_defaults, (string)n) && strlen(gFwDeviceFilter_defaults[(string)n]) > 0) {
         dyn_string values;
         string filterRef = _filterNumberToRef(n);
         getValue(filterRef + ".combo", "items", values);
         for (int i = 1; i <= dynlen(values); i++) {
            if (gFwDeviceFilter_defaults[(string)n] == values[i]) {
              setValue(filterRef + ".combo", "selectedPos", i);
              break;
            }
          }
       } else {
         setValue(_filterNumberToRef(n) + ".combo", "selectedPos", 1);
       }
     } else if (type == FW_DEVICE_FILTER_TYPE_TEXT) {
       setValue(_filterNumberToRef(n) + ".text", "text", "*"); 
     }
  }
  //enable the static ones
  fwAlarmScreenGeneric_combocheckbox_setSelectedItems(FW_DEVICE_FILTER_LIST_APPLICATION_REF, makeDynString("*"), true);
  fwAlarmScreenGeneric_combocheckbox_setSelectedItems(FW_DEVICE_FILTER_LIST_DEVICE_TYPE_REF, makeDynString("*"), true);
  fwAlarmScreenGeneric_combocheckbox_setSelectedItems(FW_DEVICE_FILTER_LIST_NATURE_REF, makeDynString("*"), true);
  fwAlarmScreenGeneric_combocheckbox_setSelectedItems(FW_DEVICE_FILTER_LIST_DOMAIN_REF, makeDynString("*"), true);
  time now = getCurrentTime();
    _fwDeviceFilter_setFilterValues(
        FW_DEVICE_FILTER_TIME_PERIOD, 
        makeDynString(now - FW_DEVICE_FILTER_DEFAULT_TIME_FILTER_START, now)
  );    
  fwDeviceFilter_enableFilter(FW_DEVICE_FILTER_TIME_PERIOD, false);

  if (apply) {
    fwDeviceFilter_applyFilter();
  } else {
    _fwDeviceFilter_enableApplyButton(true);
  }
}

/**
* Apply the filter, by taking all the filter values and 
* calling any applyFilter callback functions, with those values.
*/
public void fwDeviceFilter_applyFilter() {  
  if (isFunctionDefined(gFwDeviceFilter_filterStartCB)) {      
      execScript(
      			"main() {" + gFwDeviceFilter_filterStartCB + "(); }",
      			makeDynString()
      );
  }
  mapping filterEnableState = _fwDeviceFilter_getEnableState();  
  unsigned numberOfFunctions = dynlen(gFwDeviceFilter_applyCBFunction);      
  fwDeviceFilter_enable(false);    
  
  if (numberOfFunctions > 0) {  
    unsigned previousMaxRows = gFwDeviceFilter_previousMaxRows;    
    
    // put all the values of the dynamic filters into a mapping
    // { filterName => dyn_string(values) } or { filterName => "*" }
    mapping values;
    bool hasChanged = false;
    for (unsigned n = 1; n <= FW_DEVICE_FILTER_MAX_FILTER; n++) {
      if (_fwDeviceFilter_filterExists(n)) {
        bool areAllSelected, hasSelectionChanged;       
        dyn_string v = _fwDeviceFilter_getValues(n, areAllSelected, hasSelectionChanged, filterEnableState);
        hasChanged = hasChanged | hasSelectionChanged;
        if (areAllSelected) {
          values[fwDeviceFilter_getFilterName(n)] = FW_DEVICE_FILTER_OPTION_ALL;
        } else {
          values[fwDeviceFilter_getFilterName(n)] = v;
        }
      }
    }

    time start, end;
    bool startOk, endOk;
    dyn_string exceptionInfo;
    bool hasTimeStartChanged, hasTimeEndChanged, areAllSelected1;

    dyn_string timePeriod = _fwDeviceFilter_getValues(FW_DEVICE_FILTER_TIME_PERIOD, areAllSelected1, hasTimeStartChanged, filterEnableState);
    start = (time)timePeriod[1];
    end   = (time)timePeriod[2];

    
    bool hasDpFilterChanged = false;
    
    dyn_string dpList = fwDeviceFilter_dpListFilter(hasDpFilterChanged);
    
    hasDpFilterChanged = hasDpFilterChanged | (fwDeviceFilter_getMaxRows() != previousMaxRows);
    hasChanged = hasChanged | hasTimeStartChanged | hasTimeEndChanged | hasDpFilterChanged;
    
    for (unsigned i = 1; i <= numberOfFunctions; i++) {
      string cbFunction = gFwDeviceFilter_applyCBFunction[i];
      if (isFunctionDefined(cbFunction)) {      
        execScript(
        			"main(unsigned maxRows, bool hasDeviceListChanged, bool hasChanged, dyn_string dpFilterList, time start, time end, mapping filterValues)" +
        			"{" +
        			"  " + cbFunction + "(maxRows, hasDeviceListChanged, hasChanged, dpFilterList, start, end, filterValues);" +
        			"}",
        			makeDynString(),
           fwDeviceFilter_getMaxRows(),
           hasDpFilterChanged,
           hasChanged,
           dpList,
           start,
           end,
           values
        );
      }
    }
  }

  _fwDeviceFilter_setEnableState(filterEnableState);
  _fwDeviceFilter_enableApplyButton(true);
  if (isFunctionDefined(gFwDeviceFilter_filterEndCB)) {      
      execScript(
      			"main() {" + gFwDeviceFilter_filterEndCB + "(); }",
      			makeDynString()
      );
  }
}

public unsigned fwDeviceFilter_getMaxRows() {
  unsigned ret;
  getValue(FW_DEVICE_FILTER_MAX_ROWS_REF, "text", ret);
  return (unsigned)ret;
}

private dyn_string _fwDeviceFilter_getValues(const signed n, bool &areAllSelected, bool &hasSelectionChanged, const mapping filterEnableState, const bool returnStarForAll = false) {
  string filterRef = _filterNumberToRef(n);
  dyn_string result;

  if (n < 0) {
    if (n == FW_DEVICE_FILTER_TIME_PERIOD) {
      dyn_string exceptionInfo;
      bool ok;
      time start, end;
      if (!filterEnableState[FW_DEVICE_FILTER_TIME_PERIOD]) {
         start = "0";
         end = "now"; 
      } else {
         start = fwGeneral_dateTimeWidget_getStartDateTime(ok, exceptionInfo);
         end = fwGeneral_dateTimeWidget_getEndDateTime(ok, exceptionInfo);
      }
      result = makeDynString((string)start, (string)end);
    } else {
      result = fwAlarmScreenGeneric_combocheckbox_getSelectedItems(filterRef, areAllSelected);
    }              
  } else {
     string value;
     if (shapeExists(filterRef + ".combo")) {
       getValue(filterRef + ".combo", "selectedText", value);
     } else if (shapeExists(filterRef + ".text")) {
       getValue(filterRef + ".text", "text", value);
     } else {
      //throw exception   
     }
     areAllSelected = value == FW_DEVICE_FILTER_OPTION_ALL;
     result = makeDynString(value);
  }
  if (areAllSelected && returnStarForAll) {
     result = makeDynString(FW_DEVICE_FILTER_OPTION_ALL);
  }  

  string str;
  fwGeneral_dynStringToString(result, str, UN_DEFAULT_DELIMITER);
  string hash = cryptoHash(str);
  
  if (!mappingHasKey(gFwDeviceFilter_previousValues, n) 
        || gFwDeviceFilter_previousValues[n] != hash 
        || fwDeviceFilter_getMaxRows() != gFwDeviceFilter_previousMaxRows) {
    hasSelectionChanged = true;    
  } else {
    hasSelectionChanged = false;
  }
  
  gFwDeviceFilter_previousValues[n] = hash;
  gFwDeviceFilter_previousMaxRows = fwDeviceFilter_getMaxRows();

  return result;
}

public string fwDeviceFilter_getFilterName(const signed filterN) {
  switch (filterN) {
    //fixed names
    case FW_DEVICE_FILTER_LIST_APPLICATION: return "application";
    case FW_DEVICE_FILTER_LIST_DEVICE_TYPE: return "deviceType";
    case FW_DEVICE_FILTER_LIST_NATURE: return "nature";
    case FW_DEVICE_FILTER_LIST_DOMAIN: return "domain"; 
    case FW_DEVICE_FILTER_TIME_PERIOD: return "timePeriod";
    default:
      //dynamic filter names
      if (filterN > 0 && filterN <= FW_DEVICE_FILTER_MAX_FILTER) {
        if (shapeExists(FW_DEVICE_FILTER_REF + filterN + ".name")) {
          string name;
          getValue(FW_DEVICE_FILTER_REF + filterN + ".name", "text", name);
          return name;
        } else {
            //throw exception  
        }
      } else {
        //throw exception  
      }
  }  
}

/**
* Link the column of a table to a given filter.
* 
* The values will be taken from the column to populate the filter.
*/
public void fwDeviceFilter_linkTableColumn(const string tableRef, const unsigned columnN, const unsigned filterN, const unsigned type = FW_DEVICE_FILTER_TYPE_COMBO, const string sDefault = "") {
  if (filterN < 1 || filterN > FW_DEVICE_FILTER_MAX_FILTER) {
    //throw exception
  }
  if (!shapeExists(tableRef)) {
    //throw exception
  }
  gFwDeviceFilter_defaults[(string)filterN] = sDefault;
  string name;
  getValue(tableRef, "columnName", columnN, name);
  fwDeviceFilter_addFilter(filterN, (string)columnN, strtoupper(substr(name, 0, 1)) + substr(name, 1), type); 
  _fwDeviceFilter_setTableRef(filterN, tableRef, columnN);
  _fwDeviceFilter_updateFilter(filterN);
}

/**
* @reviewed 2018-06-22 @whitelisted{Callback}
*/
public void fwDeviceFilter_updateFilters() {
  for (unsigned n = 1; n <= FW_DEVICE_FILTER_MAX_FILTER; n++) {
    _fwDeviceFilter_updateFilter(n);
  }
}

private void _fwDeviceFilter_updateFilter(const unsigned filterN) {
  if (_fwDeviceFilter_filterExists(filterN) && _fwDeviceFilter_getFilterType(filterN) == FW_DEVICE_FILTER_TYPE_COMBO) {
    string tableRef;
    unsigned columnN;
    if (_fwDeviceFilter_getTableRef(filterN, tableRef, columnN)) {
      dyn_string column;
      getValue(tableRef, "getColumnN", columnN, column);
      unsigned max = dynUnique(column);
      dynSort(column, true /*ascending*/);
      if (max == -1) {
        //TODO throw exception
      }
  
      dyn_string values = makeDynString();

      for (unsigned i = 1; i <= dynlen(column); i++) {
        dynAppend(values, column[i]);        
      }  
      if (mappingHasKey(gFwDeviceFilter_defaults, (string)filterN)) {
        _fwDeviceFilter_setFilterValues(filterN, values, gFwDeviceFilter_defaults[(string)filterN]);
      } else {
         _fwDeviceFilter_setFilterValues(filterN, values);
      }
    }
  }
}

private unsigned _fwDeviceFilter_getFilterType(const unsigned filterN) {
  switch (filterN) {
    case FW_DEVICE_FILTER_TIME_PERIOD: return FW_DEVICE_FILTER_TYPE_TIME_PERIOD;
    case FW_DEVICE_FILTER_LIST_APPLICATION: 
    case FW_DEVICE_FILTER_LIST_DEVICE_TYPE:
    case FW_DEVICE_FILTER_LIST_NATURE:
    case FW_DEVICE_FILTER_LIST_DOMAIN: return FW_DEVICE_FILTER_TYPE_COMBO_CUSTOM;    
    default:
      if (filterN > 0 && filterN <= FW_DEVICE_FILTER_MAX_FILTER) {
        if (shapeExists(FW_DEVICE_FILTER_REF + filterN + ".combo")) {
          return FW_DEVICE_FILTER_TYPE_COMBO;
        } else if (shapeExists(FW_DEVICE_FILTER_REF + filterN + ".text")) {
          return FW_DEVICE_FILTER_TYPE_TEXT;
        } else {
            return FW_DEVICE_FILTER_TYPE_UNDEFINED;//throw exception  
        }
      } else {
        return FW_DEVICE_FILTER_TYPE_UNDEFINED;//throw exception  
      }
  }  
}

private bool _fwDeviceFilter_filterExists(const unsigned filterN) {
  return shapeExists(FW_DEVICE_FILTER_REF + filterN + ".name");
}

private bool _fwDeviceFilter_setTableRef(const unsigned filterN, const string tableRef, const unsigned columnN) {
  if (!shapeExists(FW_DEVICE_FILTER_REF + filterN + ".tableRef")) {
    //throw exception
  }
  
  return setValue(FW_DEVICE_FILTER_REF + filterN + ".tableRef", "text", tableRef + UN_DEFAULT_DELIMITER + columnN) == 0;
}

private bool _fwDeviceFilter_getTableRef(const unsigned filterN, string &tableRef, unsigned &columnN) {
  if (!shapeExists(FW_DEVICE_FILTER_REF + filterN + ".tableRef")) {
    //throw exception
  }
  string value;
  bool ok = getValue(FW_DEVICE_FILTER_REF + filterN + ".tableRef", "text", value);
  if (ok == 0) {
    dyn_string values = strsplit(value, UN_DEFAULT_DELIMITER);
    if (dynlen(values) == 2) {
      tableRef = values[1];
      columnN  = (unsigned)values[2];      
      return true;
    } else {
        tableRef = "";
        columnN = 0;
        return false;
    }
  } else {
    return false;
  }
}


private mapping _fwDeviceFilter_getEnableState() {
   mapping state;
   //enable the dynamic lists
   for (unsigned n = 1; n <= FW_DEVICE_FILTER_MAX_FILTER; n++) {
       state[n] = fwDeviceFilter_isFilterEnabled(n);
   }
   //enable the static ones
   for (signed n = -1; n >= FW_DEVICE_FILTER_MIN_FILTER; n--) {
       state[n] = fwDeviceFilter_isFilterEnabled(n);
   }   
   return state;
}

private void _fwDeviceFilter_setEnableState(mapping state) {
   for (unsigned n = 1; n <= FW_DEVICE_FILTER_MAX_FILTER; n++) {
       fwDeviceFilter_enableFilter(n, state[n]);
   }
   //enable the static ones
   for (signed n = -1; n >= FW_DEVICE_FILTER_MIN_FILTER; n--) {
       fwDeviceFilter_enableFilter(n, state[n]);
   }   
}


public void fwDeviceFilter_enable(const bool enable) {
   //enable the dynamic lists
   for (unsigned n = 1; n <= FW_DEVICE_FILTER_MAX_FILTER; n++) {
       fwDeviceFilter_enableFilter(n, enable);
   }
   //enable the static ones
   for (signed n = -1; n >= FW_DEVICE_FILTER_MIN_FILTER; n--) {
       fwDeviceFilter_enableFilter(n, enable);
   }   
   _fwDeviceFilter_enableApplyButton(enable);
}

private void _fwDeviceFilter_enableApplyButton(const bool enable) {
  setValue("fwDeviceComment_applyFilterButton", "enabled", enable);
}

public void fwDeviceFilter_enableFilter(const signed n, const bool enable) {  
  string ref = _filterNumberToRef(n);
  if (n < 0) {
    if (n == FW_DEVICE_FILTER_TIME_PERIOD) {
      fwGeneral_dateTimeWidget_setEnabled(enable);
      setValue("chkEnableDateTimeWidget", "state", 0, enable);
    } else {
      fwAlarmScreenGeneric_combocheckbox_enableWidget(ref, enable);   
    }
  } else {
    int type = _fwDeviceFilter_getFilterType(n);
    if (type == FW_DEVICE_FILTER_TYPE_COMBO) {
      setValue(ref + ".combo", "enabled", enable);   
    } else if (type == FW_DEVICE_FILTER_TYPE_TEXT) {
      setValue(ref + ".text", "enabled", enable);    
    }
    setValue(ref + ".tableRef", "visible", false); //WHY?
    setValue(ref + ".name", "visible", false);
  }
  setValue("displayedResults", "enabled", enable);
  setValue("totalResults", "enabled", enable);
  setValue("fwDeviceFilter_maxRows", "enabled", enable);
}

public bool fwDeviceFilter_isFilterEnabled(const signed n) {
  string ref = _filterNumberToRef(n);
  if (n < 0) {
    if (n == FW_DEVICE_FILTER_TIME_PERIOD) {
      return fwGeneral_dateTimeWidget_isEnabled(enable);
    } else {
      bool enable;
      getValue(ref + "." + FW_ALARM_GENERIC_COMBOCHECKBOX_TEXT_FIELD, "enabled", enable);
      return enable;
    }
  } else {
    int type = _fwDeviceFilter_getFilterType(n);
    if (type == FW_DEVICE_FILTER_TYPE_COMBO) {
      bool enable;
      getValue(ref + ".combo", "enabled", enable);   
      return enable;
    } else if (type == FW_DEVICE_FILTER_TYPE_TEXT) {
      bool enable;
      getValue(ref + ".text", "enabled", enable);    
      return enable;
    }
  }
}

/**
* @reviewed 2018-06-22 @whitelisted{Callback}
*/
public void fwDeviceFilter_setResultsTotal(const unsigned total, const unsigned displayed) {
  totalResults.text     = total;
  displayedResults.text = displayed;
}

public void fwDeviceFilter_addFilter(const unsigned n, const string name, const string label, const unsigned type, const dyn_string values = makeDynString(), const string sDefault = "") {
  if (shapeExists(FW_DEVICE_FILTER_REF + n)) {
    //already exists, so do nothing
    return;
  }
  if (!shapeExists(FW_DEVICE_FILTER_PLACEHOLDER_REF + n)) {
    //throw exception
  }

  unsigned x, y, w, h;
  getValue(FW_DEVICE_FILTER_PLACEHOLDER_REF + n, "position", x, y);
  getValue(FW_DEVICE_FILTER_PLACEHOLDER_REF + n, "size", w, h);
  y = y + h;
  
  string filterRef = _filterNumberToRef(n);
  
  switch (type) {
    case FW_DEVICE_FILTER_TYPE_COMBO:
      addSymbol(
          myModuleName(), 
          myPanelName(), 
          "fwDeviceComment/objects/fwDeviceFilter_combo.pnl", 
          filterRef, 
          makeDynString(), 
          x, y, 
          0, 
          1, 1
      );
      while (!shapeExists(filterRef + ".combo")) {
        delay(0, 20);
      }
      break;
    case FW_DEVICE_FILTER_TYPE_TEXT:
      addSymbol(
          myModuleName(), 
          myPanelName(), 
          "fwDeviceComment/objects/fwDeviceFilter_text.pnl", 
          filterRef, 
          makeDynString(), 
          x, y, 
          0, 
          1, 1
      );
      while (!shapeExists(filterRef + ".text")) {
        delay(0, 20);
      }
      break;
    default:
      DebugN("ERROR - unknown type");
      //throw exception
  }

  string str;  
  
  _fwDeviceFilter_setFilterValues(n, values, sDefault);
  
  setValue(FW_DEVICE_FILTER_PLACEHOLDER_REF + n + "_Label", "text", label);
  setValue(FW_DEVICE_FILTER_PLACEHOLDER_REF + n + "_Label", "visible", true);
  setValue(filterRef + ".name", "text", name);
  setValue(filterRef, "enabled", false);  
  setValue(filterRef, "visible", true);   
}

private void _fwDeviceFilter_setFilterValues(const signed filterN, const dyn_string values, const string sDefault = "") {  
  string filterRef = _filterNumberToRef(filterN);
  if (filterN < 0) {
    if (filterN == FW_DEVICE_FILTER_TIME_PERIOD) {
      if (dynlen(values) != 2) {
        //throw exception
      }
      fwGeneral_dateTimeWidget_setStartDateTime((time)values[1]);
      fwGeneral_dateTimeWidget_setEndDateTime((time)values[2]);
    } else {
      fwAlarmScreenGeneric_combocheckbox_setItems(filterRef, values);
    }
  } else {
    if (shapeExists(filterRef + ".combo")) {
        dyn_string valuesWithAll = values;
        dynInsertAt(valuesWithAll, FW_DEVICE_FILTER_OPTION_ALL, 1);
        setValue(filterRef + ".combo", "items", valuesWithAll);
        gFwDeviceFilter_defaults[(string)filterN] = sDefault;
        if (strlen(sDefault) > 0) {         
          for (int i = 1; i <= dynlen(valuesWithAll); i++) {
            if (sDefault == valuesWithAll[i]) {
              setValue(filterRef + ".combo", "selectedPos", i);
              break;
            }
          }
        }
    } else if (shapeExists(filterRef + ".text")) {
      string str;
      if (dynlen(values) == 0) {
        str = FW_DEVICE_FILTER_OPTION_ALL;
      } else {
        fwGeneral_dynStringToString(values, str, ", ");
      }      
      setValue(filterRef + ".text", "text", str);
    } else {
       //throw exception 
    }
  } 
}

private string _filterNumberToRef(const signed n) {
  switch (n) {
    case FW_DEVICE_FILTER_LIST_APPLICATION: return FW_DEVICE_FILTER_LIST_APPLICATION_REF;
    case FW_DEVICE_FILTER_LIST_DEVICE_TYPE: return FW_DEVICE_FILTER_LIST_DEVICE_TYPE_REF;
    case FW_DEVICE_FILTER_LIST_NATURE: return FW_DEVICE_FILTER_LIST_NATURE_REF;
    case FW_DEVICE_FILTER_LIST_DOMAIN: return FW_DEVICE_FILTER_LIST_DOMAIN_REF;
    case FW_DEVICE_FILTER_TIME_PERIOD: return FW_DEVICE_FILTER_TIME_PERIOD_REF;
    default:
      if (n > 0 && n <= FW_DEVICE_FILTER_MAX_FILTER) {
        return FW_DEVICE_FILTER_REF + n;
      } else {
        //throw exception  
      }
  }
}

/**
  Derived from fwAlarmScreenGeneric_dpListFilter.
  
  reload = unAlarmScreen_g_iAlarmScreenType != UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY;
  
  @par Description:
  Get a list of DP matching the given filter.
  
  //TODO make system names a filter list
*/
public dyn_string fwDeviceFilter_dpListFilter(bool &hasSelectionChanged, const bool reload = true, const dyn_string dsFilterSystemNames = fwAlarmScreenGeneric_getConnectedSystems())
{  
  //the following code is derived from _unAlarmScreen_dpListFilter
  dyn_string dsFilterDps;
  dyn_string dsDpList = _fwDeviceFilter_initialiseDpList();

  if (reload) {
    string sQueryWHERE;
    string sSystem;
    bool areAllDomainsSelected, areAllNaturesSelected, areAllTypesSelected, areAllApplicationsSelected;
    bool hasDomainsSelectionChanged, hasNaturesSelectionChanged, hasTypesSelectionChanged, hasApplicationSelectionChanged;
    mapping filterEnableState = _fwDeviceFilter_getEnableState();
    
    dyn_string dsFilterDomain      = _fwDeviceFilter_getValues(FW_DEVICE_FILTER_LIST_DOMAIN, areAllDomainsSelected, hasDomainsSelectionChanged, filterEnableState, true);     
    dyn_string dsFilterNature      = _fwDeviceFilter_getValues(FW_DEVICE_FILTER_LIST_NATURE, areAllNaturesSelected, hasNaturesSelectionChanged, filterEnableState, true);
    dyn_string dsFilterType        = _fwDeviceFilter_getValues(FW_DEVICE_FILTER_LIST_DEVICE_TYPE, areAllTypesSelected, hasTypesSelectionChanged, filterEnableState, true);
    dyn_string dsFilterApplication = _fwDeviceFilter_getValues(FW_DEVICE_FILTER_LIST_APPLICATION, areAllApplicationsSelected, hasApplicationSelectionChanged, filterEnableState, true);
    
    hasSelectionChanged = hasDomainsSelectionChanged | hasNaturesSelectionChanged | hasTypesSelectionChanged | hasApplicationSelectionChanged;
    
    //Is there at least one but not all selected?
    bool bMultiDomain = !areAllDomainsSelected && dynlen(dsFilterDomain) > 0;
    bool bMultiNature = !areAllNaturesSelected && dynlen(dsFilterNature) > 0;
        
    dyn_string dsFilterDpsDomain;
    dyn_string dsFilterDpsNature;
    dyn_string dsFilterDpsType;
    

    if (dynlen(dsFilterType) > 0 && !areAllTypesSelected) {
      sQueryWHERE = " AND (";
      for(int i = 1; i <= dynlen(dsFilterType) ; i++) {
        sQueryWHERE = sQueryWHERE + "_DPT = \"" + dsFilterType[i] + "\" OR ";
      }
      sQueryWHERE = substr(sQueryWHERE, 0, strlen(sQueryWHERE) - 3) + ") ";  // Delete "OR "
    } else {
      sQueryWHERE = "";
    }

    // For each system, get dpName according to Domain and Nature filters    
    for (int i = 1 ; i <= dynlen(dsFilterSystemNames) ; i++) {
      if (strpos(dsFilterSystemNames[i], ":")>=0) {
        sSystem = dsFilterSystemNames[i];
      } else {
        sSystem = dsFilterSystemNames[i] + ":";
      }
      if (dynlen(dsFilterSystemNames) == 1 && bMultiDomain && !bMultiNature && sQueryWHERE == "") {  
        dyn_string dsTemp;
        dyn_string dsSplit = dsFilterDomain;
        for (int j = 1; j <= dynlen(dsSplit) ; j++) {
          dyn_string dps;
          dpGet(unGenericDpFunctions_groupNameToDpName("Domain:" + dsSplit[j], sSystem) + ".Dps", dps);   
          dynAppend(dsFilterDpsDomain, dps);
        }     
      } else if (dynlen(dsFilterSystemNames) == 1 && !bMultiDomain && bMultiNature && sQueryWHERE=="") {
        dyn_string dsTemp;
        dyn_string dsSplit = dsFilterNature;
        for (int j = 1; j <= dynlen(dsSplit) ; j++) {
          dyn_string dps;
          dpGet(unGenericDpFunctions_groupNameToDpName("Nature:" + dsSplit[j], sSystem) + ".Dps", dps);   
          dynAppend(dsFilterDpsNature, dps);
          
        }        
      } else if (bMultiDomain || bMultiNature) {
          // Used dpN list
        dyn_string dsTemp = _fwDeviceFilter_getList(dsFilterDomain, sQueryWHERE, UN_DOMAIN_PREFIX, sSystem);
        dynAppend(dsFilterDpsDomain, dsTemp);        
        dsTemp = _fwDeviceFilter_getList(dsFilterNature, sQueryWHERE, UN_NATURE_PREFIX, sSystem);
        dynAppend(dsFilterDpsNature, dsTemp);              
      }
    }

    // Merge the results  
    dynUnique(dsFilterDpsDomain);
    dynUnique(dsFilterDpsNature);
    
    int iLenDomain = dynlen(dsFilterDpsDomain);
    int iLenNature = dynlen(dsFilterDpsNature);
     
    if (iLenDomain>0 && iLenNature>0) { // If a query was done on Domains and Natures
      dsFilterDps = dynIntersect(dsFilterDpsDomain, dsFilterDpsNature);
    } else if (iLenDomain>0 && iLenNature==0 && !bMultiNature) { // If a query was done for Domains and Natures
      dsFilterDps = dsFilterDpsDomain;
    } else if (iLenDomain>0 && iLenNature==0 && bMultiNature) { // If a query was done only for Domains
      dynClear(dsFilterDps);
    } else if (iLenDomain==0 && iLenNature>0 && !bMultiDomain) { // If a query was done only for Natures
      dsFilterDps = dsFilterDpsNature;
    } else if (iLenDomain == 0 && iLenNature > 0 && bMultiDomain) { // If a query was done for Domains and Natures
      dynClear(dsFilterDps);
    } else if ((iLenDomain == 0) && (iLenNature == 0) && (bMultiDomain || bMultiNature)) {
      dynClear(dsFilterDps);
    }    

    if (dynlen(dsFilterDps) == 0 && !bMultiDomain && !bMultiNature) { // No filter for the moment  
      if (!areAllTypesSelected && dynlen(dsFilterType) > 0) {  //if filter on Object          
          //pattern on DpName (5 fields)
          dyn_string dsPattern;
          for(int i = 1 ; i <= UN_DPNAME_FIELD_LENGTH ; i++) {
            dsPattern[i] = UNALARMSCREEN_FILTER_ALL;
          }
    
          //built object pattern

          for(int i = 1 ; i<= dynlen(dsFilterType) ; i++) {
            dsPattern[UN_DPNAME_FIELD_OBJECT] = dsFilterType[i];
            string sObjectPattern = "";
            for (int j = 1 ; j <= UN_DPNAME_FIELD_LENGTH ; j++)
            {
              sObjectPattern = sObjectPattern + dsPattern[j] + UN_DPNAME_SEPARATOR;
            }
          
            sObjectPattern = substr(sObjectPattern, 0, strlen(sObjectPattern) - strlen(UN_DPNAME_SEPARATOR));
          
            //update existing list
            for(int j = 1; j <= dynlen(dsDpList) ; j++) {
              string sys;
              for (int i = 1 ; i <= dynlen(dsFilterSystemNames) ; i++) {
                if (strpos(dsFilterSystemNames[i], ":")>=0) {
                  sys = dsFilterSystemNames[i];
                } else {
                  sys = dsFilterSystemNames[i] + ":";
                }
                dynAppend(dsFilterDps, sys + sObjectPattern);
              }
            }
          }
        } else  { // no new filter => set filter from memory
        dsFilterDps = dsDpList;
      }
    } else if (dynlen(dsFilterDps) == 0) {
      dsFilterDps = makeDynString("NOT-VALID-DP1");
    } else {
      dsFilterDps = _fwDeviceFilter_checkDeviceType(dsFilterDps, dsFilterType);
    }
    
    if (!areAllApplicationsSelected || g_unicosHMI_bApplicationFilterInUse) {
      dsFilterDps = _fwDeviceFilter_checkApplication(dsFilterDps, dsFilterApplication);
    }
    
    if (dynlen(dsFilterDps) == 0) {
      dsFilterDps = makeDynString("NOT-VALID-DP2");
    }
  } else {
    // Update existing list
    for(int i = 1; i <= dynlen(dsDpList) ; i++) {
      dynAppend(dsFilterDps, dsDpList[i]);
    }
  }
  
  dynUnique(dsFilterDps);

  return dsFilterDps;
}


public void fwDeviceFilter_setInitialiseDpListFunction(const string functionName) {
  if (!isFunctionDefined(functionName)) {
    //throw exception
  }
  gFwDeviceFilter_initialiseDpListFunction = functionName;
}

private dyn_string _fwDeviceFilter_initialiseDpList() {
  dyn_string dpList;
  if (isFunctionDefined(gFwDeviceFilter_initialiseDpListFunction)) {      
      evalScript(
         dpList,
      			"dyn_string main()" +
      			"{" +
      			" return " + gFwDeviceFilter_initialiseDpListFunction + "();" +
      			"}",
      			makeDynString()
      );
  }
  return dpList;
}

public void fwDeviceFilter_setGroupQueryFunction(const string functionName) {
  gFwDeviceFilter_groupQueryFunction = functionName;
}


private dyn_string  _fwDeviceFilter_getList(const dyn_string dsGroup, const string sQueryWHERE, const string sPrefix, const string sSystem) {  
  dyn_string dsFilterDps; 
  
  if (isFunctionDefined(gFwDeviceFilter_groupQueryFunction)) {   
    string sQueryFROM = _fwDeviceFilter_QueryGroup(dsGroup, sPrefix, sSystem);
    if (sQueryFROM != "") {
       string sQueryGroup;   
       sQueryGroup = callFunction(gFwDeviceFilter_groupQueryFunction, sSystem, sQueryFROM, sQueryWHERE); // fixed "void function can not return a value" syntax error
       
      dyn_dyn_anytype ddDps;

      dpQuery(sQueryGroup, ddDps);
  
      for(int i = 2 ; i <= dynlen(ddDps) ; i++) {
        dynAppend(dsFilterDps, dpSubStr(ddDps[i][1],DPSUB_SYS_DP));
      }
    }
  }
    
  return dsFilterDps;  
}

/**
  @par Description:
  Transform a DP group list to a dpQuery 'FROM' clause with a list of DPs.
  
  @par Usage:
  Internal.
  
  @param[in]  dsFilter  dyn_string, The list of DP groups.
  @param[in]  sPrefix   string,     Nature/domain indicator.
  @param[in]  sSystem   string,     System on which to look for.
*/
private string  _fwDeviceFilter_QueryGroup(const dyn_string dsFilter, const string sPrefix, const string sSystem) {
  //from _unAlarmScreen_QueryGroup
  string sFROM;

  // Get DP group
  dyn_string dsDpGroup;  
  int iFilterLength = dynlen(dsFilter);
  for(int i = 1 ; i <= iFilterLength ; i++) {
    dsDpGroup[i] = unGenericDpFunctions_groupNameToDpName(sPrefix + dsFilter[i], sSystem);
  }

  // Delete groups that doesn't have valid DPE -> they make the dpQuery crash
  int iIndex = 1;
  while (iIndex <= iFilterLength) {
    if (dsDpGroup[iIndex] != "") {
      if (dpExists(dsDpGroup[iIndex])) {
        dyn_string dsCurrentDpGroupDpList;
        int iRes = dpGet(dsDpGroup[iIndex] + ".Dps", dsCurrentDpGroupDpList); 
        int iDpGroupListLength = dynlen(dsCurrentDpGroupDpList);
        
        if (iRes < 0 || iDpGroupListLength == 0) {
          dynRemove(dsDpGroup, iIndex);
        } else {
          int iIndex2 = 1;
          bool bDpExists = false;
          while ((iIndex2 <= iDpGroupListLength) && (!bDpExists)) {
            bDpExists = dpExists(dsCurrentDpGroupDpList[iIndex2]);
            iIndex2++;
          }
          
          if (!bDpExists) {
            dynRemove(dsDpGroup, iIndex);
          } else {
            iIndex++;
          }  
        }
      } else {
        dynClear(dsDpGroup);
        iIndex = iFilterLength + 1; // Exit
      }
    } else {
      dynRemove(dsDpGroup, iIndex);
    }
      
    iFilterLength = dynlen(dsDpGroup);
  }

  // Now Dps from Groups can be use for FROM in query
  if (dynlen(dsDpGroup) > 0) {
    sFROM = " FROM \'{";
    for(int i = 1 ; i <= dynlen(dsDpGroup) ; i++) {
      sFROM = sFROM + "DPGROUP(" + substr(dsDpGroup[i],strpos(dsDpGroup[i],":")+1) + "),";
    }
    
    sFROM = substr(sFROM, 0, strlen(sFROM) - 1) + "}\' ";  // Delete last ","
    if (dynlen(dsDpGroup) == 1) {
      strreplace(sFROM, "{", "");
      strreplace(sFROM, "}", "");
    }
  } else {
    sFROM = "";
  }

  return sFROM;
} 


/**
  @par Description:
  Check which of the given DPs is of any of the given applications.
  
  @par Usage:
  Internal.
  
  @param[in]  dsDp        dyn_string, The list of DP in which to check.
  @param[in]  dsApplParam dyn_string, The list of applications.
  
  @return The list of DP from the input that are of any of the given applications.
*/
private dyn_string  _fwDeviceFilter_checkApplication(const dyn_string dsDp, const dyn_string dsApplParam) {
  dyn_string dsResult;  
  
  dyn_string dsAppl = dsApplParam;
  if (g_unicosHMI_bApplicationFilterInUse) {
    synchronized(g_unicosHMI_dsApplicationList) {
      if (dynContains(dsApplParam, FW_DEVICE_FILTER_OPTION_ALL) > 0) {
        dsAppl = g_unicosHMI_dsApplicationList;
      } else {
        dsAppl = dynIntersect(dsAppl, g_unicosHMI_dsApplicationList);
      }
    }
  }
  
  // For each DP in the filter
  for(int i = 1 ; i <= dynlen(dsDp) ; i++) {
    // Current dp
    dyn_string dsTemp = strsplit(dsDp[i], UN_DPNAME_SEPARATOR);
      
      
    // For each application in the list
    for (int j = 1 ; j <= dynlen(dsAppl) ; j++) {
      // Current application      
      string sAppl = dsAppl[j];
    
      if(dynlen(dsTemp) == UN_DPNAME_FIELD_LENGTH) {
        if(dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION] == sAppl) {
          dynAppend(dsResult, dsDp[i]);
        } else if(dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION] == "*") {
          dynAppend(
              dsResult, 
              dsTemp[UN_DPNAME_FIELD_PREFIX]  + UN_DPNAME_SEPARATOR + 
              dsTemp[UN_DPNAME_FIELD_PLCNAME] + UN_DPNAME_SEPARATOR+
              sAppl + UN_DPNAME_SEPARATOR +
              dsTemp[UN_DPNAME_FIELD_OBJECT] + UN_DPNAME_SEPARATOR +
              dsTemp[UN_DPNAME_FIELD_NUMBER]);
        }
    } else {// case no standard UNICOS DP format --> rebuilt it!

        int iPos = strpos(dsDp[i], ".");
        string sTemp = substr(dsDp[i], iPos, strlen(dsDp[i]) - iPos);
        dynAppend(
            dsResult, 
            "*" + UN_DPNAME_SEPARATOR +
            "*" + UN_DPNAME_SEPARATOR +
            sAppl + UN_DPNAME_SEPARATOR +
            "*" + UN_DPNAME_SEPARATOR +
            "*"+sTemp
          );
      }
    } // End application loop
  } // End DP loop
  dynUnique(dsResult);
  
  return dsResult;
}

/**
  @par Description:
  Check which of the given DPs is of any of the given device types.
  
  @par Usage:
  Internal.
  
  @param[in]  dsDp      dyn_string, The list of DP in which to check.
  @param[in]  dsDevType dyn_string, The list of device types.
  
  @return The list of DP from the input that are of any of the given device types.
*/
private dyn_string _fwDeviceFilter_checkDeviceType(const dyn_string dsDp, const dyn_string dsDevType) {
  dyn_string dsRes;
  
  if(dynlen(dsDevType) == 1) {
    if(dsDevType[1] == FW_DEVICE_FILTER_OPTION_ALL) {
      return dsDp;
    }
  }

  for(int i = 1 ; i <= dynlen(dsDp) ; i++) {
    if(dynContains(dsDevType, dpTypeName(dpSubStr(dsDp[i], DPSUB_SYS_DP)) > 0)) {
     dynAppend(dsRes, dsDp[i]);
    }
  }
  
  return dsRes;
}

/**
  This function updates the textFieldOut event script of the filters that contain a textedit field
  in order to apply the filter immediately upon pressing the ENTER key while editing the filed
*/
fwDeviceFilter_updateCommandEvents()
{
  unsigned uFilterType;
  string sFilterRef;
  bool bFilterTypeOK;
  string sEventScript;
  dyn_string dsScriptLines;


  // scan all filters
  for ( int i = FW_DEVICE_FILTER_MIN_FILTER; i <= FW_DEVICE_FILTER_MAX_FILTER; i++)
  {
    uFilterType = _fwDeviceFilter_getFilterType(i);

    switch ( uFilterType )
    {
      case FW_DEVICE_FILTER_TYPE_COMBO_CUSTOM:
        bFilterTypeOK = true;
      
        // compose the reference for the text field of the filter
        sFilterRef = _filterNumberToRef(i) + "." + FW_DEVICE_FILTER_CUSTOM_COMBO_FIELD_NAME;

        break;
       
      case FW_DEVICE_FILTER_TYPE_TEXT:
        bFilterTypeOK = true;

        // compose the reference for the text field of the filter
        sFilterRef = _filterNumberToRef(i) + ".text";

        break;

      default:
        bFilterTypeOK = false;

    } // end switch ( uFilterType )

    // keep searching if current filter does not have a text field
    if ( false == bFilterTypeOK )
    {
      continue;
    }
    
    // get the script for the callback of the textFieldOut (a.k.a. Command) event 
    // this event occurs when the user presses ENTER while editing the textfield
    getValue(sFilterRef, "script", "textFieldOut", sEventScript); 
    
    if ( "" == sEventScript )
    // script is empty / not defined
    {
      sEventScript = "main(){\nfwDeviceFilter_applyFilter();\n}"; // call applyFilter
    }
    else
    // script already contains some code --> call applyFilter after the last statement
    {
      // split the script into pseudo-statements, on each semicolon ";"
      dsScriptLines = strsplit(sEventScript, ';');
      
      // insert the call to applyFilter as the penultimate statement
      // the last pseudo-statement is the main() closing bracket "}"
      dynInsertAt(dsScriptLines, "\nfwDeviceFilter_applyFilter()", dynlen(dsScriptLines) );
      
      // re-join all the pseudo-statements with the ';' character
      sEventScript = strjoin(dsScriptLines, ';');
    } // end if ("" == script )

    
    // save the new script as the callback for the textFieldOut event
    setValue(sFilterRef, "script", "textFieldOut", sEventScript); 
    

  } // end for( int i...
 
}
