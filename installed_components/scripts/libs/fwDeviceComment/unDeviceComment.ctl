/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: unDeviceComment.ctl

@author
  Dr. Marc Bengulescu (BE-ICS-FD)
*/



#uses "fwAccessControl/fwAccessControl"
#uses "fwDeviceComment/fwDeviceComment"
#uses "fwGeneral/fwGeneral"


// comment permissions
const uint UN_DEVICE_COMMENT_NO_PERMISSION = 0u;
const uint UN_DEVICE_COMMENT_CAN_ADD = 1u;
const uint UN_DEVICE_COMMENT_CAN_DEACTIVATE_ALL = 2u;
const uint UN_DEVICE_COMMENT_CAN_DELETE_ALL = 4u;
const uint UN_DEVICE_COMMENT_CAN_CONFIGURE_RIGHTS = 8u;

// indices of comment string components
const uint UN_DEVICE_COMMENT_TIMESTAMP_INDEX = 1;
const uint UN_DEVICE_COMMENT_USER_INDEX = 2;
const uint UN_DEVICE_COMMENT_COMMENT_INDEX = 3;
const uint UN_DEVICE_COMMENT_EXTENSION_INDEX = 4;
const uint UN_DEVICE_COMMENT_SYSTEM_INDEX = 5;
const uint UN_DEVICE_COMMENT_HOST_INDEX = 6;
const uint UN_DEVICE_COMMENT_ACTIVE_STATE_INDEX = 7;

const string UN_DEVICE_COMMENT_TIME_FORMAT = "%Y.%m.%d %H:%M:%S";
const string UN_DEVICE_COMMENT_POSTINSTALL_FLAG = "FWDC_POSTINST";
const string UN_DEVICE_COMMENT_DEVICELOG = ".statusInformation.deviceLog";
const string UN_DEVICE_COMMENT_GLOBAL_SETTINGS = "_unDeviceComment";
const string UN_DEVICE_COMMENT_ACCESS_CONTROL = ".accessControl";
const string UN_DEVICE_COMMENT_ADD_PRIVILEGES = ".commentAddPrivileges";
const string UN_DEVICE_COMMENT_DELETE_ALL_PRIVILEGES = ".commentDeleteAllPrivileges";
const string UN_DEVICE_COMMENT_DEACTIVATE_ALL_PRIVILEGES = ".commentDeactivateAllPrivileges";
const string UN_DEVICE_COMMENT_CONFIGURE_RIGHTS_PRIVILEGES = ".commentConfigureRightsPrivileges";

const string UN_DEVICE_COMMENT_NO_ERROR = 0;
const string UN_DEVICE_COMMENT_CANNOT_DPGET_ERROR = -1;
const string UN_DEVICE_COMMENT_CANNOT_DPSET_ERROR = -2;
const string UN_DEVICE_COMMENT_INEXISTENT_DP_ERROR = -3; // DP of comment does not exist (remote system disconnected)
const string UN_DEVICE_COMMENT_INEXISTENT_ERROR = -4; // DP of comment does exists, but comment has been changed/deleted

const string UN_DEVICE_COMMENT_DELIMITER = "~";

const string UN_DEVICE_COMMENT_CHANGE_CALLBACK = "unDeviceComment_commentChangedCB";

// comment search timeout thread
const uint UN_DEVICE_COMMENT_COMMENT_SEARCH_TIMEOUT = 180; //seconds
private global int gUnDeviceComment_searchCountdownTimer;
private global string gUnDeviceComment_searchMessage;

void unDeviceComment_init() {//pass in table ref here
  fwDeviceComment_clearCallbacks();
  fwDeviceFilter_clearCallbacks();
  
  fwDeviceFilter_addApplyFilterCallbackFunction("fwDeviceComment_applyFilter");       
  fwDeviceFilter_setInitialiseDpListFunction("unDeviceComment_initialiseDpList");
  fwDeviceComment_addTableChangedCallback("fwDeviceFilter_updateFilters");
  fwDeviceComment_addFilterAppliedCallback("fwDeviceFilter_setResultsTotal");  

  fwDeviceFilter_linkTableColumn(FW_DEVICE_COMMENT_TABLE_REF, FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE, FW_DEVICE_FILTER_LIST_1);
  fwDeviceFilter_linkTableColumn(FW_DEVICE_COMMENT_TABLE_REF, FW_DEVICE_COMMENT_TABLE_COLUMN_USER, FW_DEVICE_FILTER_LIST_2);
  fwDeviceFilter_linkTableColumn(FW_DEVICE_COMMENT_TABLE_REF, FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT, FW_DEVICE_FILTER_LIST_3, FW_DEVICE_FILTER_TYPE_TEXT);
  fwDeviceFilter_linkTableColumn(FW_DEVICE_COMMENT_TABLE_REF, FW_DEVICE_COMMENT_TABLE_COLUMN_SYSTEM, FW_DEVICE_FILTER_LIST_4);
  fwDeviceFilter_linkTableColumn(FW_DEVICE_COMMENT_TABLE_REF, FW_DEVICE_COMMENT_TABLE_COLUMN_HOST, FW_DEVICE_FILTER_LIST_5);
  //hardcode the active filter instead of linking to the table so that we can set the default of TRUE.
  fwDeviceFilter_addFilter(FW_DEVICE_FILTER_LIST_6, FW_DEVICE_COMMENT_TABLE_COLUMN_ACTIVE, "Active", FW_DEVICE_FILTER_TYPE_COMBO, makeDynString(FALSE, TRUE), TRUE);

  fwDeviceComment_setPopulateTableFunction("unDeviceComment_populateTable");
  
  fwDeviceComment_setImportFunction("unDeviceComment_import");
  fwDeviceComment_setExportFunction("unDeviceComment_export");
  fwDeviceFilter_setGroupQueryFunction("unDeviceFilter_setGroupQueryFunction");
  fwDeviceFilter_setFilterStartCallbackFunction("unDeviceComment_filterStartCB");
  fwDeviceFilter_setFilterEndCallbackFunction("unDeviceComment_filterEndCB");

  fwDeviceFilter_init(); 
  fwDeviceFilter_enableFilter(FW_DEVICE_FILTER_TIME_PERIOD, false);
  fwDeviceComment_init();  
  
  fwDeviceFilter_updateCommandEvents();
  fwDeviceFilter_applyFilter();
}

/**
* @reviewed 2018-06-22 @whitelisted{Callback}
*/
void unDeviceComment_filterStartCB()
{
  fwDeviceComment_showProgress(0,0);
  fwDeviceComment_enable(false);
  gUnDeviceComment_searchMessage = "Search/filter complete";
}

/**
* @reviewed 2018-06-22 @whitelisted{Callback}
*/
void unDeviceComment_filterEndCB()
{
  fwDeviceComment_enable(true);
  fwDeviceComment_showProgress(100,100);
  fwDeviceComment_showUserFeedback(gUnDeviceComment_searchMessage);
}


/**
*  This function counts down for a specified number of seconds,
*  then sets a flag to indicate that the timeout has expired
*
* @reviewed 2018-06-22 @whitelisted{Thread}
*/
void unDeviceComment_commentSearchTimeoutThread(int iTimeout)
{
  // initialize global counter with value from function argument
  gUnDeviceComment_searchCountdownTimer = iTimeout;
  
  while ( gUnDeviceComment_searchCountdownTimer > 0 )
  {
    delay(1);
    gUnDeviceComment_searchCountdownTimer--;
  }
}

/**
  Strip UN_DEVICE_COMMENT_DEVICELOG from a DPE
*/
string unDeviceComment_stripDeviceLogSuffix(string sDPE)
{
  string sOutput;
  
  int sDeviceLogSuffixPos = strpos(sDPE, UN_DEVICE_COMMENT_DEVICELOG);

  if ( sDeviceLogSuffixPos > 0)
  // DPE contains the suffix
  {
    // remove the suffix
    sOutput = substr(sDPE, 0, sDeviceLogSuffixPos );
  }
  else
  // DPE does not contain the suffix
  {
    sOutput = sDPE;
  }
  
  return sOutput;
}


/**
  Handles the change of a DP, after the user has double clicked on a row
  The name of this function should be stored in the UN_DEVICE_COMMENT_CHANGE_CALLBACK constant
*/
void unDeviceComment_commentChangedCB(dyn_string dsDPList)
{
  for ( int i = 1; i <= dynlen(dsDPList); i++)
  {
    // standardize DP name
    dsDPList[i] = unDeviceComment_stripDeviceLogSuffix(dsDPList[i]);
    
    // remove DP from the table
    fwDeviceComment_deleteDeviceRows(dsDPList[i]);
  }

  fwDeviceComment_populateTable(fwDeviceFilter_getMaxRows(), dsDPList, false);
  fwDeviceFilter_applyFilter();
}


/**
*  Processes the double-click event of a comment (i.e. table row)
*
* @reviewed 2018-06-22 @whitelisted{Callback}
*/
void unDeviceComment_doubleClick(dyn_string line) {
  string dpe        = line[FW_DEVICE_COMMENT_TABLE_COLUMN_DPE + 1];
  string deviceName = line[FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE + 1];

  ChildPanelOnCentral(
    "vision/unComment/unDeviceComment.pnl",
    "Comments for " + deviceName, 
    makeDynString(
      "$sDeviceDpe:" + dpe,
      "$sDeviceName:" + deviceName,
      "$sDpeCallback:" + true
    )
  );

}


void unDeviceComment_rightClick(dyn_string dsRow)
{
  string sDPName, sDevice;
  dyn_string exceptionInfo;
  
  sDPName = dsRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE]; // get the full DPE
  sDevice = dpSubStr(sDPName,DPSUB_SYS) + dsRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE]; // get device as system + alias
  
  unGenericObject_WidgetRightClick(sDevice, dpTypeName(sDPName), exceptionInfo);

  if ( dynlen(exceptionInfo) )
  {
    fwExceptionHandling_log(exceptionInfo);
    return;
  }
  
}

void unDeviceComment_populateTable(unsigned max, const dyn_string dpList = makeDynString(), bool bClearTable = true)
{
  if ( true == bClearTable )
  {
    fwDeviceComment_deleteAllRows();
  }

  dyn_string dpe, application, device;
  dyn_time timestamp;
  dyn_string user, comment;
  dyn_string commentExt, sys, host;
  dyn_bool active;
  
  // start the countdown thread
  int  iThreadID = startThread("unDeviceComment_commentSearchTimeoutThread", UN_DEVICE_COMMENT_COMMENT_SEARCH_TIMEOUT);
  
  unsigned total = 0;
  unsigned len = dynlen(dpList);
  for (unsigned i = 1; i <= len; i++) {
    string dp = dpList[i];
    if (strpos(dp, ":") < 0) {
      dp = "*:" + dp;
    }
    dyn_string dpes = dpNames(dp + UN_DEVICE_COMMENT_DEVICELOG);
    unsigned dpesLen = dynlen(dpes);
    for (unsigned i = 1; i <= dpesLen; i++) {
      if (dpExists(dpes[i] + "Active")) {
        dyn_string deviceLog;
        bool deviceLogActive;
        dpGet(dpes[i], deviceLog);
          
        if (dynlen(deviceLog) > 0) {
            string deviceName = dpSubStr(dpes[i], DPSUB_SYS_DP);
            string sApplicationName    = unGenericDpFunctions_getApplication(dpSubStr(dpes[i], DPSUB_DP));
            string alias      = unGenericDpFunctions_getAlias(deviceName);
            string sUserFeedbackMessage;
            unsigned length   = dynlen(deviceLog);
            
            fwDeviceComment_showProgress(i, dpesLen-1);

            sprintf(sUserFeedbackMessage,
                   "Time remaining: %3u s   Querying %s -- %s",
                   gUnDeviceComment_searchCountdownTimer,
                   sApplicationName,
                   alias);
            
            fwDeviceComment_showUserFeedback(sUserFeedbackMessage);

            for (unsigned i = 1; i <= length; i++) {
                  dyn_string commentRow = unDeviceComment_decodeCommentString(deviceLog[i]);
                	bool bUserHasCancelled = fwDeviceComment_getUserHasCancelled();

                  if (dynlen(commentRow) != 7) { //TODO better validation
                    continue;
                  }
                  
                  // stop if one of the folowing conditions is fulfilled:
                  // - the number of rows has reached the maximum allowed
                  // - the countdown timer has expired
                  // - the user has pressed the cancel button
                  
                  if (total++ >= max ||
                      0 == gUnDeviceComment_searchCountdownTimer ||
                      true == bUserHasCancelled) // end if condition
                  {
                    fwDeviceComment_addRows(dpe, application, device, timestamp, user, comment, commentExt, sys, host, active);
                    if ( 0 == gUnDeviceComment_searchCountdownTimer )
                    {
                      gUnDeviceComment_searchMessage = "Search incomplete: timeout of " + UN_DEVICE_COMMENT_COMMENT_SEARCH_TIMEOUT + " s expired";
                    }
                    else if ( total >= max )
                    {
                      stopThread(iThreadID);
                      gUnDeviceComment_searchMessage = "Search incomplete: reach maximum number of comments";
                    }
                    else if (true == bUserHasCancelled)
                    {
                      stopThread(iThreadID);
                      // user can't see this message anymore, since the progress bar dialog has been closed
                      gUnDeviceComment_searchMessage = "Search incomplete: user has cancelled";
                    }
                    
                    return;
                  }
                  dynAppend(dpe, deviceName);
                  dynAppend(application, sApplicationName); 
                  dynAppend(device, alias);
                  dynAppend(timestamp, (time) commentRow[1]);
                  dynAppend(user, commentRow[2]);
                  dynAppend(comment, commentRow[3]);
                  dynAppend(commentExt, commentRow[4]);
                  dynAppend(sys, commentRow[5]);
                  dynAppend(host, commentRow[6]);
                  dynAppend(active, (bool)commentRow[7]);
            }            
        }
      }
    }   
  }

  fwDeviceComment_addRows(dpe, application, device, timestamp, user, comment, commentExt, sys, host, active);
  stopThread(iThreadID);
}

/**
*
* @reviewed 2018-06-22 @whitelisted{EvalScript}
*/
dyn_string unDeviceComment_initialiseDpList() {
  return makeDynString("*:*");
}

/**
*
* @reviewed 2018-06-22 @whitelisted{EvalScript}
*/
string unDeviceFilter_setGroupQueryFunction(string sys, string queryFrom, string queryWhere) {
    string query = "SELECT \'_original.._value\' ";
    
    query = query + queryFrom;
    
    query = query + "REMOTE \'" + sys + "\' ";
          
    query = query + " WHERE _EL = \"statusInformation.deviceLog\" ";
    
    if (queryWhere != "") {
      query = query + queryWhere;
    }   
      
    return query;
}

/**
*
* @reviewed 2018-06-22 @whitelisted{EvalScript}
*/
bool unDeviceComment_import(const string filename) {
    file inputFile = fopen(filename, "r");
    while (feof(inputFile) == 0) {
        string currentLine;
        fgets(currentLine, 100000, inputFile);
        if (currentLine == "") continue;
        int idx;
        idx = strpos(currentLine, ";");
        string alias = substr(currentLine, 0, idx);
        currentLine = substr(currentLine, idx+1);
        
        idx = strpos(currentLine, ";");
        string deviceLogActive_s = substr(currentLine, 0, idx);
        bool deviceLogActive = strtolower(strrtrim(strltrim(deviceLogActive_s))) == "true";
        currentLine = substr(currentLine, idx+1);
        currentLine = strrtrim(currentLine, "\r\n");
        dyn_string deviceLog = strsplit(currentLine, ";");

        string deviceName = unGenericDpFunctions_dpAliasToName(alias);

        dpSetWait(deviceName + "statusInformation.deviceLogActive", deviceLogActive,
                  deviceName + "statusInformation.deviceLog", deviceLog);

    }
    fclose(inputFile);
    return true;
}

/**
*
* @reviewed 2018-06-22 @whitelisted{EvalScript}
*/
bool unDeviceComment_export(const string filename) {

  bool bDeviceFirstSeen;
  bool bCanReadDPE;
  bool deviceLogActive;
  string sComment;
  string sDPEToSkip;

  dyn_errClass err; 
  dyn_anytype daCommentRow;
  dyn_dyn_anytype ddaFilteredComments;


  // open file for writing  
  file outputFile = fopen(filename, "w");

  
  // retrieve the **sorted** filtered table rows containing the device comments
  fwDeviceComment_getFilteredComments(ddaFilteredComments);
  
  for ( int i = 1; i <=  dynlen(ddaFilteredComments); i++ )
  // go through each comment row
  {
    // a device can have multiple comments, so we mark the first time we see a device...
    // since the table is sorted, we see a device for the first time its DPE differs...
    // from the DPE of the previous row
    bDeviceFirstSeen = ( i == 1) ? true : (ddaFilteredComments[i][1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE] != ddaFilteredComments[i-1][1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE]);

    // temp variable to facilitate readability;
    daCommentRow = ddaFilteredComments[i];
    
    if ( bDeviceFirstSeen )
    // this is the first time we have seen the device
    {
      // create the '.statusInformation.deviceLogActive' DPE string of the device
      string sDeviceLogActiveDPE = daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE] + ".statusInformation.deviceLogActive";

      // test if deviceLogActive DPE exists
      bCanReadDPE = dpExists(sDeviceLogActiveDPE);     

      // test if dpGet on the deviceLogActive DPE succeeds
      bCanReadDPE &= (0 == dpGet(sDeviceLogActiveDPE, deviceLogActive));
      
      // test whether an error occurred when doing dpGet
      err = getLastError();

      bCanReadDPE &= ( dynlen(err) == 0 );
       
      if ( bCanReadDPE )
      // if the deviceLogActive DPE can be read
      {
        // build the header string
        sComment = ( i > 1  ? "\n" : "") +
                   dpSubStr(daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE], DPSUB_SYS) +
                   daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DEVICE] +
                   ";" +
                   deviceLogActive;

        // write header string to output file
        fputs(sComment, outputFile);

        // no need to skip subsequent comments for this device
        sDPEToSkip = "";
      }
      else
      // if the deviceLogActive cannot be read (e.g. remote system has disconnected)...
      // raise an exception and skip the rest of the comments for this device
      {
        string sErrorText;
        dyn_string exceptionInfo;

        if(dynlen(err) > 0) 
        { 
          sErrorText = "\n\n" + getErrorText(err);
        }         
        
        fwException_raise(exceptionInfo,
                          "ERROR",
                          "Could not read " + sDeviceLogActiveDPE + "\nThis device will NOT be included in the export file." + sErrorText,
                          "fwDeviceComment_readDeviceLogActiveDPE");
        
        fwExceptionHandling_display(exceptionInfo);
        
        // store the DPE of the device and skip subsequent comments for it        
        sDPEToSkip = daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE];

      } // end if ( bCanReadDPE )
      
    }  // end if ( bDeviceFirstSeen )

    if ( sDPEToSkip == daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_DPE] )    
    // if the DPE of the device could not be read on the first time we have encountered it
    {
      continue; // skip all subsequent comments for this device
    }
  
    // build the comment string  
    sComment = ";" + escape(unDeviceComment_encodeCommentArray(makeDynString(
                        formatTime(UN_DEVICE_COMMENT_TIME_FORMAT, daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_TIMESTAMP]),
                        daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_USER],
                        daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_COMMENT],
                        daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_EXTENSION],
                        daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_SYSTEM],
                        daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_HOST],
                        (string) daCommentRow[1 + FW_DEVICE_COMMENT_TABLE_COLUMN_ACTIVE]))); 

    // write the comment string to the file
    fputs(sComment, outputFile);

  } // end for ( int i = 1;
  
  fclose(outputFile);

  return true;  
}


private string escape(string text) {
    strreplace(text, ";", ",");
    return text;
}

public dyn_string unDeviceComment_decodeCommentString(string s) {
  dyn_string result = strsplit(s, UN_DEVICE_COMMENT_DELIMITER);

  for (int i = 1; i <= dynlen(result); i++) {
      strreplace(result[i], ":71LD3:", UN_DEVICE_COMMENT_DELIMITER);
  }
  return result;
}

public string unDeviceComment_encodeCommentArray(dyn_string r) {
  string s = "";
  int len = dynlen(r);
  for (int i = 1; i <= len; i++) {
    string str = r[i];
    strreplace(str, UN_DEVICE_COMMENT_DELIMITER, ":71LD3:");
    s = s + str;
    if (i != len) {
      s = s + UN_DEVICE_COMMENT_DELIMITER;
    }
  }
  return s;
}


// ------------------------------------------------------------------------------------------------

// Getter: Query and return permission to add comments
bool unDeviceComment_hasCommentAddPermission(bit32 b32CommentPermissions)
{
  return (b32CommentPermissions & UN_DEVICE_COMMENT_CAN_ADD);
}


// ------------------------------------------------------------------------------------------------

// Getter: Query and return permission to (de)activate all comments
bool unDeviceComment_hasCommentDeactivateAllPermission(bit32 b32CommentPermissions)
{
  return (b32CommentPermissions & UN_DEVICE_COMMENT_CAN_DEACTIVATE_ALL);
}


// ------------------------------------------------------------------------------------------------

// Getter: Query and return permission to delete all comments
bool unDeviceComment_hasCommentDeleteAllPermission(bit32 b32CommentPermissions)
{
  return (b32CommentPermissions & UN_DEVICE_COMMENT_CAN_DELETE_ALL);
}


// ------------------------------------------------------------------------------------------------

// Getter: Query and return permission to configure comments access rights
bool unDeviceComment_hasConfigureCommentRightsPermission(bit32 b32CommentPermissions)
{
  bool canConfigureCommentAccessRights;
  dyn_string dsUnusedPlaceholder;

  canConfigureCommentAccessRights = fwAccessControl_HasGroupAdminPrivilege(dsUnusedPlaceholder, "", false);
  
  return canConfigureCommentAccessRights || (b32CommentPermissions & UN_DEVICE_COMMENT_CAN_CONFIGURE_RIGHTS);
}


// ------------------------------------------------------------------------------------------------

// Store all comment privileges lists to DPs
void unDeviceComment_storeAllCommentsRightsInDP(dyn_string dsAddCommentPermissions,
                                                dyn_string dsDeleteAllCommentsPermissions,
                                                dyn_string dsDeactivateAllCommentsPermissions,
                                                dyn_string dsConfigureCommentRightsPermissions,
                                                dyn_string &exceptionInfo,
                                                string sSystemName = "")
{
  // consistency check on system name, must be empty string or end with a colon
  if ( ("" != sSystemName) && (':' != sSystemName[strlen(sSystemName)-1]) )
  {
    sSystemName += ":";
  }


  //set access rights  for adding comment 
  unDeviceComment_storeCommentsAccessRightsInDP(sSystemName +
                                                UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                UN_DEVICE_COMMENT_ADD_PRIVILEGES,
                                                dsAddCommentPermissions,
                                                exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}


  //set access rights  for deleting any comment (not just own's comments)
  unDeviceComment_storeCommentsAccessRightsInDP(sSystemName +
                                                UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                UN_DEVICE_COMMENT_DELETE_ALL_PRIVILEGES,
                                                dsDeleteAllCommentsPermissions,
                                                exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

  
  //set access rights  for deactivating any comment (not just own's comments)
  unDeviceComment_storeCommentsAccessRightsInDP(sSystemName +
                                                UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                UN_DEVICE_COMMENT_DEACTIVATE_ALL_PRIVILEGES,
                                                dsDeactivateAllCommentsPermissions,
                                                exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}


  //set access rights for configuring comment permissions 
  unDeviceComment_storeCommentsAccessRightsInDP(sSystemName +
                                                UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                UN_DEVICE_COMMENT_CONFIGURE_RIGHTS_PRIVILEGES,
                                                dsConfigureCommentRightsPermissions,
                                                exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

}

  
// ------------------------------------------------------------------------------------------------

// Store the list of privileges indicating the comment access rights to DPs
void unDeviceComment_storeCommentsAccessRightsInDP(string sDPName, dyn_string dsCommentPermissions, dyn_string &exceptionInfo)
{
  dyn_errClass err;

  if ( dpExists(sDPName) )
  {
    dpSetWait(sDPName, dsCommentPermissions);
    err = getLastError(); //test whether an error occurred 
    if(dynlen(err) > 0) 
    { 
      throwError(err);      
      fwException_raise(exceptionInfo,"ERROR", getErrorText(err),"");
    }     
  }
  else // DP does not exist
  {
    fwException_raise(exceptionInfo,"ERROR", "Could not store device comment access rights. DP " + sDPName + " does not exist!","");
  }
  
}


// ------------------------------------------------------------------------------------------------

// Query the DP and return the list of privileges indicating the comment access rights
unDeviceComment_retreiveCommentsAccessRightsFromDP(string sDPName, dyn_string &dsCommentPermissions, dyn_string &exceptionInfo)
{
  dyn_errClass err;

  dynClear(dsCommentPermissions);

  if ( dpExists(sDPName) )
  {
    dpGet(sDPName, dsCommentPermissions);
    err = getLastError(); //test whether an error occurred 
    if(dynlen(err) > 0) 
    { 
      throwError(err);      
      fwException_raise(exceptionInfo,"ERROR", getErrorText(err),"");
    }     
  }
  else // DP does not exist
  {
    fwException_raise(exceptionInfo,"ERROR", "Could not retrieve device comment access rights. DP " + sDPName + " does not exist!","");
  }

}


// ------------------------------------------------------------------------------------------------

// Check if the user has any of the privileges in the comment access rights list
bool unDeviceComment_parseCommentsAccessRights(dyn_string dsCommentPermissions, dyn_string &exceptionInfo)
{  
  bool bGranted = false; //assume user has no privileges

  if ( dynlen(dsCommentPermissions) > 0 )
  {
    for ( int i = 1; i <= dynlen(dsCommentPermissions); i++ ) // check if user has any of these privileges
    {
      fwAccessControl_isGranted(dsCommentPermissions[i],	bGranted, exceptionInfo);

    	if (dynlen(exceptionInfo)) // check if everything went smoothly
      {
    		return false;
    	}

      if (bGranted) // if user has at least on privilege we can stop, since he already has the access right
      {
        return true;
      }
      
    } // end for
    
  } // end if ( dynlen(dsCommentPermissions) > 0 )

  return bGranted;
}

// Return all commment access rights from the DPs
void unDeviceComment_getAllCommentAccessRights(dyn_string &dsAddCommentPermissions,
                                               dyn_string &dsDeleteAllCommentsPermissions,
                                               dyn_string &dsDeactivateAllCommentsPermissions,
                                               dyn_string &dsConfigureCommentRightsPermissions,
                                               dyn_string &exceptionInfo,
                                               string sSystemName = "") // end of function prototype
{
  // consistency check on system name, must be empty string or end with a colon
  if ( ("" != sSystemName) && (':' != sSystemName[strlen(sSystemName)-1]) )
  {
    sSystemName += ":";
  }
  
  // add comments  
  unDeviceComment_retreiveCommentsAccessRightsFromDP(sSystemName +
                                                     UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                     UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                     UN_DEVICE_COMMENT_ADD_PRIVILEGES,
                                                     dsAddCommentPermissions,
                                                     exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

 
  // delete all comments
  unDeviceComment_retreiveCommentsAccessRightsFromDP(sSystemName +
                                                     UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                     UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                     UN_DEVICE_COMMENT_DELETE_ALL_PRIVILEGES,
                                                     dsDeleteAllCommentsPermissions,
                                                     exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

  // deactivate comments
  unDeviceComment_retreiveCommentsAccessRightsFromDP(sSystemName +
                                                     UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                     UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                     UN_DEVICE_COMMENT_DEACTIVATE_ALL_PRIVILEGES,
                                                     dsDeactivateAllCommentsPermissions,
                                                     exceptionInfo);
  
	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}


  // configure comment rights
  unDeviceComment_retreiveCommentsAccessRightsFromDP(sSystemName +
                                                     UN_DEVICE_COMMENT_GLOBAL_SETTINGS +
                                                     UN_DEVICE_COMMENT_ACCESS_CONTROL +
                                                     UN_DEVICE_COMMENT_CONFIGURE_RIGHTS_PRIVILEGES,
                                                     dsConfigureCommentRightsPermissions,
                                                     exceptionInfo);
  
	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}
  
}
// ------------------------------------------------------------------------------------------------

// Set up comment access rights if on a freshly installed system or reset if the DPs are somehow empty
// By default everyone holding any privilege in the UNICOS domain has the permission for all comment actions
// If the UNICOS domain does not exist, everyone holding any privilege in the SYSTEM domain will get them
void unDeviceComment_initCommentAccessRights(dyn_string &exceptionInfo)
{
  dyn_string dsAddCommentPermissions;
  dyn_string dsDeleteAllCommentsPermissions;
  dyn_string dsDeactivateAllCommentsPermissions;
  dyn_string dsConfigureCommentRightsPermissions;

  dyn_string dsMatchingDomains;  
  
  string sUnusedTemp;
  string sDomainName;
  dyn_int diUnusedTemp;
  int iUnusedTemp;
  dyn_string dsUnusedTemp;
  dyn_string dsPrivilegeNames;
  dyn_string dsDomainNames;

  
  // get all the comment rights as dyn_strings from the DPs
  unDeviceComment_getAllCommentAccessRights(dsAddCommentPermissions,
                                            dsDeleteAllCommentsPermissions,
                                            dsDeactivateAllCommentsPermissions,
                                            dsConfigureCommentRightsPermissions,
                                            exceptionInfo); //function call ends here
  
	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}
  
  // if the comment access rights have already been initialized on this system
  if ( (dynlen(dsAddCommentPermissions) > 0) ||
       (dynlen(dsDeleteAllCommentsPermissions) > 0) ||
       (dynlen(dsDeactivateAllCommentsPermissions) > 0) ||
       (dynlen(dsConfigureCommentRightsPermissions) > 0) ) // end of if condition
  {
    DebugN("fwDeviceComment: access rights are already configured.");
    return; // comment access rights are already set up, nothing to do
  }

  DebugN("fwDeviceComment: configuring access rights...");

  
  // get the domains on the system
  fwAccessControl_getAllDomains(dsDomainNames, dsUnusedTemp, exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

  dsMatchingDomains = dynPatternMatch("*DEVICECOMMENT",dsDomainNames);
  
  // loop through matching domains
  for ( int iDomain = 1; iDomain <= dynlen(dsMatchingDomains); iDomain++ )
  {
    // get list of privileges from the previusly selected domain
    fwAccessControl_getDomain(dsMatchingDomains[iDomain], sUnusedTemp, sUnusedTemp, iUnusedTemp, dsPrivilegeNames, diUnusedTemp, exceptionInfo);
  	if (dynlen(exceptionInfo)) // check if everything went smoothly
    {
      return;
  	}
      
    for (int iPrivilege = 1; iPrivilege <= dynlen(dsPrivilegeNames); iPrivilege++ )
    {
      switch ( dsPrivilegeNames[iPrivilege] )
      {
        case "canAddMine":
          dynAppend(dsAddCommentPermissions, dsMatchingDomains[iDomain] + ":" + dsPrivilegeNames[iPrivilege]);
          break;
          
        case "canDeleteAll":
          dynAppend(dsDeleteAllCommentsPermissions, dsMatchingDomains[iDomain] + ":" + dsPrivilegeNames[iPrivilege]);
          break;
          
        case "canDeactivateAll":
          dynAppend(dsDeactivateAllCommentsPermissions, dsMatchingDomains[iDomain] + ":" + dsPrivilegeNames[iPrivilege]);
          break;
          
        case "canEditCommentRights":
          dynAppend(dsConfigureCommentRightsPermissions, dsMatchingDomains[iDomain] + ":" + dsPrivilegeNames[iPrivilege]);
          break;

      } // end switch

    } // end for (int iPrivilege
    
  } // end for ( int iDomain...
 
  unDeviceComment_storeAllCommentsRightsInDP(dsAddCommentPermissions,
                                             dsDeleteAllCommentsPermissions,
                                             dsDeactivateAllCommentsPermissions,
                                             dsConfigureCommentRightsPermissions,
                                             exceptionInfo);

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}
  
}

// ------------------------------------------------------------------------------------------------

// Get the access right privileges for device comments from the DPs
void unDeviceComment_getCommentPermissions(bit32 &b32CommentPermissions, dyn_string &exceptionInfo)
{
  dyn_string dsAddCommentPermissions;
  dyn_string dsDeleteAllCommentsPermissions;
  dyn_string dsDeactivateAllCommentsPermissions;
  dyn_string dsConfigureCommentRightsPermissions;
  bit32 b32TempCommentPermissions = UN_DEVICE_COMMENT_NO_PERMISSION; // initial state is the state of least permission

  b32CommentPermissions = UN_DEVICE_COMMENT_NO_PERMISSION; // reset comment permission to least privileges

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

  // get all the comment rights as dyn_strings from the DPs
  unDeviceComment_getAllCommentAccessRights(dsAddCommentPermissions,
                                            dsDeleteAllCommentsPermissions,
                                            dsDeactivateAllCommentsPermissions,
                                            dsConfigureCommentRightsPermissions,
                                            exceptionInfo); //function call ends here
	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}
  
  // add comments permission
  b32TempCommentPermissions |= ( unDeviceComment_parseCommentsAccessRights(dsAddCommentPermissions, exceptionInfo) ? UN_DEVICE_COMMENT_CAN_ADD : UN_DEVICE_COMMENT_NO_PERMISSION );
 
	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}
  
  // delete all comments permission 
  b32TempCommentPermissions |= ( unDeviceComment_parseCommentsAccessRights(dsDeleteAllCommentsPermissions, exceptionInfo) ? UN_DEVICE_COMMENT_CAN_DELETE_ALL : UN_DEVICE_COMMENT_NO_PERMISSION );

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

  // deactivate comments permission
  b32TempCommentPermissions |= ( unDeviceComment_parseCommentsAccessRights(dsDeactivateAllCommentsPermissions, exceptionInfo) ? UN_DEVICE_COMMENT_CAN_DEACTIVATE_ALL : UN_DEVICE_COMMENT_NO_PERMISSION );

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

  // configure comment rights permission
  b32TempCommentPermissions |= ( unDeviceComment_parseCommentsAccessRights(dsConfigureCommentRightsPermissions, exceptionInfo) ? UN_DEVICE_COMMENT_CAN_CONFIGURE_RIGHTS : UN_DEVICE_COMMENT_NO_PERMISSION );

	if (dynlen(exceptionInfo)) // check if everything went smoothly
  {
    return;
	}

  // if we have reached this point, then everything was OK
  b32CommentPermissions = b32TempCommentPermissions; // set the comment permission
    
} 

// ------------------------------------------------------------------------------------------------

// Set up the access control and the device comment permissions for the panel
void unDeviceComment_accessControlSetup(string sUserChangedCB, string sPermissionsChangedCB)
{
  int iUserCallbackResult; //unused, just a placeholder
  dyn_string exceptionInfo;
  dyn_errClass err;  
  
  // register UNICOS access control
  unGenericDpFunctionsHMI_setCallBack_user(sUserChangedCB, iUserCallbackResult, exceptionInfo);

  if( dynlen(exceptionInfo) ) 
  // something went wrong
  { 
    // display a message
    fwExceptionHandling_display(exceptionInfo);
    
    // quit
    PanelOff();
  }     

  // monitor the dpS
  dpConnect(sPermissionsChangedCB,
            true,
            UN_DEVICE_COMMENT_GLOBAL_SETTINGS + UN_DEVICE_COMMENT_ACCESS_CONTROL + UN_DEVICE_COMMENT_ADD_PRIVILEGES,
            UN_DEVICE_COMMENT_GLOBAL_SETTINGS + UN_DEVICE_COMMENT_ACCESS_CONTROL + UN_DEVICE_COMMENT_DELETE_ALL_PRIVILEGES,
            UN_DEVICE_COMMENT_GLOBAL_SETTINGS + UN_DEVICE_COMMENT_ACCESS_CONTROL + UN_DEVICE_COMMENT_DEACTIVATE_ALL_PRIVILEGES
            );
    
  err = getLastError(); //test whether an error occurred 

  if(dynlen(err) > 0) 
  // something went wrong  
  { 
    // display a message
    fwException_raise(exceptionInfo,"ERROR", getErrorText(err),"");
    fwExceptionHandling_display(exceptionInfo);

    // quit
    PanelOff();
  }     
}


// ------------------------------------------------------------------------------------------------

//  Checks if the current user is NOT the author of the comment (provided as param)
bool unDeviceComment_isNotAuthorOfComment(string sAuthor)
{
  string sUserName;

  fwAccessControl_getUserName(sUserName);

  return (sAuthor != sUserName);
}


// ------------------------------------------------------------------------------------------------

// Query the DP and return the list of device comments
int unDeviceComment_retreiveCommentsFromDP(string sDPName, dyn_string &dsDeviceComments)
{
  dyn_errClass err;
  int iResult;

  dynClear(dsDeviceComments);
  
  // compose full DPE name to where comments are stored
  sDPName = unDeviceComment_stripDeviceLogSuffix(sDPName) + UN_DEVICE_COMMENT_DEVICELOG;  

  if ( dpExists(sDPName) )
  {
    iResult = dpGet(sDPName, dsDeviceComments);
    err = getLastError(); //test whether an error occurred 
    if(dynlen(err) > 0) 
    { 
      iResult = UN_DEVICE_COMMENT_CANNOT_DPGET_ERROR;
    }     
  }
  else // DP does not exist
  {
    iResult = UN_DEVICE_COMMENT_INEXISTENT_DP_ERROR;
  }
  
  return iResult;
}


// ------------------------------------------------------------------------------------------------

// Store the list of device comments to the device DP
int unDeviceComment_storeCommentsInDP(string sDPName, dyn_string dsDeviceComments)
{
  dyn_errClass err;
  int iResult;

  // compose full DPE name to where comments are stored
  sDPName = unDeviceComment_stripDeviceLogSuffix(sDPName) + UN_DEVICE_COMMENT_DEVICELOG;  

  if ( dpExists(sDPName) && dpExists(sDPName + "Active") )
  {
    iResult = dpSetWait(sDPName, dsDeviceComments,
                        sDPName + "Active", unDeviceComment_hasActiveComments(dsDeviceComments));
    err = getLastError(); //test whether an error occurred 
    if(dynlen(err) > 0) 
    { 
      iResult = UN_DEVICE_COMMENT_CANNOT_DPSET_ERROR;
    }     
  }
  else // DP does not exist
  {
    iResult = UN_DEVICE_COMMENT_INEXISTENT_DP_ERROR;
  }

  return iResult;
}

// ------------------------------------------------------------------------------------------------

// Checks whether list of comments has at least one active comment
bool unDeviceComment_hasActiveComments(dyn_string dsStoredComments)
{
  dyn_string dsCommentSections;

  for(int i = 1; i <= dynlen(dsStoredComments); i++) 
  {
      dsCommentSections = unDeviceComment_decodeCommentString(dsStoredComments[i]);
      if( true == dsCommentSections[UN_DEVICE_COMMENT_ACTIVE_STATE_INDEX] )
      {
          return true;
      }
  }
  return false;
}


// ------------------------------------------------------------------------------------------------

// Get the full name from the provided user name.
string unDeviceComment_getFullUserName(string sUserName = "")
{
  int iUserId;
  bool bEnabled;
  string sComment, sLogMessage, sFullUserName, sDescription;
  dyn_string dsGroupNames, exceptionInfo;

  if("" == sUserName)
  {
    fwAccessControl_getUserName(sUserName);
  }
  
  fwAccessControl_getUser(sUserName, sFullUserName, sDescription, iUserId, bEnabled, dsGroupNames, exceptionInfo);
  
  return sFullUserName;
}

// ------------------------------------------------------------------------------------------------
/** Getter: Creates the representation of one comment necessary to be stored in the datapoint.

@par Constraints
  None
  
@par Usage
	Private to the panel

@par WinCC managers
	VISION
  
@param dsCommentSections       dyn_string, INPUT, the individual components of one comment
  
@return string, one prepared comment
*/
string unDeviceComment_getPreparedComment(dyn_string dsCommentSections)
{
  string sComment;
  
  if(dynlen(dsCommentSections) == 7)
  {
    sComment = unDeviceComment_encodeCommentArray(dsCommentSections);

  }
  
  return sComment;
}


// ------------------------------------------------------------------------------------------------
/** Process, i.e. (de)activate or delete, a list of comments for a device
  *
  * @param sDeviceDPE                   the DP of the device with ot without UN_DEVICE_COMMENT_DEVICELOG suffix
  * @param dsSelectedDeviceComments     the list of comments to be processed for this device
  * @param sOperation                   the operation to be performed on the comment list: ( activate | deactivate | delete )
  * 
  * @return iResult                     the result of the operation (zero if success)
  *
  */
int unDeviceComment_processCommentsInDP(string sDeviceDPE, dyn_string dsSelectedDeviceComments, string sOperation)
{
  int iResult, iPos;  
  string sComment;
  dyn_string dsStoredComments, dsDecodedComment;

  // get the comments stored in the DP
  iResult = unDeviceComment_retreiveCommentsFromDP(sDeviceDPE, dsStoredComments);

  if ( iResult < 0 )
  // something went wrong
  {
    return iResult;
  }

  // loop through the selected comments and apply "sOperation" on them
  for (int i = 1; i <= dynlen(dsSelectedDeviceComments); i++)
  {
    iPos = dynContains(dsStoredComments, dsSelectedDeviceComments[i]);
    
    if (iPos > 0)
    {
      dsDecodedComment = unDeviceComment_decodeCommentString(dsStoredComments[iPos]);

      switch (sOperation)
      {
        case "delete":
          dynRemove(dsStoredComments, iPos);
          break;

        case "activate":
          dsDecodedComment[UN_DEVICE_COMMENT_ACTIVE_STATE_INDEX] = true;
          dsDecodedComment[UN_DEVICE_COMMENT_TIMESTAMP_INDEX] = formatTime(UN_DEVICE_COMMENT_TIME_FORMAT, getCurrentTime());
          sComment = unDeviceComment_getPreparedComment(dsDecodedComment);
          dynRemove(dsStoredComments, iPos);
          dynInsertAt(dsStoredComments, sComment, 1);
          break;
          
        case "deactivate":
          dsDecodedComment[UN_DEVICE_COMMENT_ACTIVE_STATE_INDEX] = false;
          sComment = unDeviceComment_getPreparedComment(dsDecodedComment);
          dsStoredComments[iPos] = sComment;
          break;
          
        default:
          fwException_raise(exceptionInfo,"ERROR", "Unsupported operation","");
          fwExceptionHandling_display(exceptionInfo);
          break;

      } // end of switch (sOperation)

    } 
    else
    // comment may have been changed or deleted remotely, notify user to to reload comments table
    {
      return UN_DEVICE_COMMENT_INEXISTENT_ERROR;
    } // end of if (iPos > 0)

  } // end of for (int i = 1...

  // store the updated comment list to the device DP 
  iResult = unDeviceComment_storeCommentsInDP(sDeviceDPE, dsStoredComments);
  
  return iResult;
}




//TODO
/*

void unDeviceComment_addTestComments(const dyn_string dpList) {
  for (unsigned i = 1; i <= dynlen(dpList); i++) {
      dyn_string dpes = dpNames(dpList[i]);

      for (unsigned i = 1; i <= dynlen(dpes); i++) {
          int n = rand() % 10;
          Debug("creating comment " + n + " comments for " + dpes[i]);
          for (int j = 0; j < n; j++) {
            unDeviceComment_saveNewComment(dpes[i], "test comment " + j, "extended test comment " + j, true);
          }
      }      
  }
}


const string TIME_FORMAT = "%Y.%m.%d %H:%M:%S";
const string DEFAULT_COMMENT_TEXT = "Type your comment ...";
const string DEFAULT_EXTENSION_TEXT = "Add additional information ...";

const int ERROR_CODE_NO_ERROR = 0;
const int ERROR_CODE_GET_DP_ERROR = -1;
const int ERROR_CODE_SET_DP_ERROR = -2;
const int ERROR_CODE_NO_CONFIRMATION = -3;
const int ERROR_CODE_NO_COMMENT = -4;

// Add 1 to make it an index
const int USER_NAME_COLUMN = 3;
const int TIMESTAMP_COLUMN = 0;
const int ACTIVE_STATE_COLUMN = 6;

const int IS_ACTIVE = 1;

const int MAX = 10;


string getFullUserName(string sUserName = "")
{
  int iUserId;
  bool bEnabled;
  string sComment, sLogMessage, sFullUserName, sDescription;
  dyn_string dsGroupNames, exceptionInfo;

  if("" == sUserName)
  {
    fwAccessControl_getUserName(sUserName);
  }
  
  fwAccessControl_getUser(sUserName, sFullUserName, sDescription, iUserId, bEnabled, dsGroupNames, exceptionInfo);
  
  return sFullUserName;
}




private void unDeviceComment_saveNewComment(string dpe, string sCommentText, string sExtensionText, bool bActiveState = true)
{
  //dpSetWait(dpe, makeDynString(), dpe + "Active", false);
  //return;
  string sCurrentTime;
  string sSystemName, sHostName, sIPAddress, sUserName, sLogMessage, sComment;
  dyn_string dsStoredComments;

  sCurrentTime = formatTime(TIME_FORMAT, getCurrentTime());
  sSystemName = getSystemName();
  unGenericDpFunctions_getHostName(sSystemName, sHostName, sIPAddress);
  fwAccessControl_getUserName(sUserName);

  sLogMessage = getFullUserName(sUserName) + " saved a new comment on " + dpe + ".";
   
  if(sExtensionText == DEFAULT_EXTENSION_TEXT)
  {
    sExtensionText = "";
  }
  
  sComment = (string) sCurrentTime + UN_DEVICE_COMMENT_DELIMITER
             + sUserName + UN_DEVICE_COMMENT_DELIMITER
             + sCommentText + UN_DEVICE_COMMENT_DELIMITER
             + sExtensionText + UN_DEVICE_COMMENT_DELIMITER
             + sSystemName + UN_DEVICE_COMMENT_DELIMITER
             + sHostName + UN_DEVICE_COMMENT_DELIMITER                          
             + (string) bActiveState;
  
  dsStoredComments = getCommentsFromDp(dpe);
  
  dynInsertAt(dsStoredComments, sComment, 1);
  
  // we only store as many comments as defined in UN_DEVICE_MAXIMUM_COMMENTS
  while(dynlen(dsStoredComments) > MAX)
  {
    dynRemove(dsStoredComments, dynlen(dsStoredComments));
  }
  
  dpSetWait(dpe, dsStoredComments, dpe + "Active", hasActiveComment(dsStoredComments));
}
*/
/** Checks whether list of comments has at least one active comment

@par Constraints
  None
  
@par Usage
	Private to the panel

@par WinCC managers
	VISION
  
@param dsStoredComments       dyn_string, INPUT, the prepared comments to be written

@return bool, active or not
*//*
private bool hasActiveComment(dyn_string dsStoredComments)
{
    dyn_string dsCommentSections;

    for(int index = 1; index <= dynlen(dsStoredComments); index++) 
    {
        dsCommentSections = strsplit(dsStoredComments[index], UN_DEVICE_COMMENT_DELIMITER);
        if(dsCommentSections[ACTIVE_STATE_COLUMN+1] == TRUE)
        {
            return TRUE;
        }
    }
    return FALSE;
}


*/
/** Getter: Retrieves the comments from the datapoint.

@par Constraints
  None
  
@par Usage
	Private to the panel

@par WinCC managers
	VISION
  
@return dyn_string, the prepared comments as array
*//*
dyn_string getCommentsFromDp(string dpe)
{
  int iReturnCode;
  dyn_string dsStoredComments;
  
  iReturnCode = dpGet(dpe, dsStoredComments);
  
  return dsStoredComments;
}
*/


