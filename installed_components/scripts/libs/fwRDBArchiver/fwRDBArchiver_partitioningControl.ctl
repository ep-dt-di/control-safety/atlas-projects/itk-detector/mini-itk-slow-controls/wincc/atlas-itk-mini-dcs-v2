dyn_string QUERYPARAMS = makeDynString( "ELEMENT_ID           NUMBER(25) NOT NULL",
                                        "TS                   TIMESTAMP(9) NOT NULL",
                                        "VALUE_NUMBER         BINARY_DOUBLE",
                                        "STATUS               NUMBER(20)",
                                        "MANAGER              NUMBER(20)",
                                        "USER_                NUMBER(5)",  
                                        "SYS_ID               NUMBER(20)",
                                        "BASE                 NUMBER(1) default 0", 
                                        "TEXT                 VARCHAR2(4000)",
                                        "VALUE_STRING         VARCHAR2(4000)",
                                        "VALUE_TIMESTAMP      TIMESTAMP(9)",
                                        "CORRVALUE_NUMBER     BINARY_DOUBLE",
                                        "OLVALUE_NUMBER       BINARY_DOUBLE",
                                        "CORRVALUE_STRING     VARCHAR2(4000)",
                                        "OLVALUE_STRING       VARCHAR2(4000)",
                                        "CORRVALUE_TIMESTAMP  TIMESTAMP(9)",
                                        "OLVALUE_TIMESTAMP    TIMESTAMP(9)");     


/**
 * Function returning the list of columns for the given arc group name
 * @param pisGroupName arc group name to run query for
 * @param podsColumns output parameter to return column list
 *
 * @return 0 if everything was fine and we have any values, -1 otherwise 
 */
public int fwRDBArchiver_getArcGroupColumns(string pisGroupName, dyn_string &podsColumns){
  string lSQL;
  dyn_dyn_anytype lValues;
  
//   lSQL = " select utc.column_name " +
//          " from arc_statement ast, arc_archive aa, user_tab_columns utc " +
//          " where ast.group_name = aa.group_name and aa.status = 'CURRENT' and ast.dynamic = 'Y' " + 
//          " and ast.group_name = '" + pisGroupName + "' " +
//          " and ast.object_name not like '%HISTORYVALUES_' " +
//          " and utc.table_name = ast.object_name ||  to_char(aa.archive#, 'FM00000000')";
  lSQL = "select utc.column_name " +
         " from arc_archive aa, user_tab_columns utc " +
         " where aa.status = 'CURRENT' " +
         " and aa.group_name = '" + pisGroupName + "' " +
         " and utc.table_name = aa.group_name || 'HISTORY_' ||  to_char(aa.archive#, 'FM00000000')";
  
  int ret = runRealSQLQuery(lSQL, lValues); 

  if(dynlen(lValues) > 0){
    podsColumns = getDynString(lValues, 1);
    return 0;
  }  
  
  return -1;
}

/**
 * Return list of the arc groups with the information if they are custom (editable strucutre)
 * @param poddaValues output parameter with list of arc groups
 *
 * @return 0 if everything was fine and we have any values, -1 otherwise 
 */
public int fwRDBArchiver_getArcGroups(dyn_dyn_anytype& poddaValues)
{
  string lSQL;
  dyn_dyn_anytype lValues;
 
  lSQL = "select group_name, custom_structure from arc_group where template_name not like 'ALERT' order by 1";
  int ret = runRealSQLQuery(lSQL, lValues);
 
  if(dynlen(lValues) > 0){
    poddaValues = lValues;
    return 0;
  }  
  
  return -1;
  
}

/**
 * Creates new custom arc group with given name and column set
 * @param pisArcname new arc group name
 * @param pidiColumnsIdxs column set for the new arc
 *
 * @return 0 if everything was fine, -1 otherwise 
 */
public int fwRDBArchiver_createNewArc(string pisArcName, dyn_int pidiColumnsIdxs)
{
  string lSQL;
  dyn_dyn_anytype lValues;
  dyn_string dsTmp;

  string sTemplateSQLSufix;  
  string sTemplateSQLPrefix;
  string sTemplateSQL;
  
  DebugN(pidiColumnsIdxs);
  
  lSQL = "select command from arc_template where template_name = 'CUSTOM' and suf_object_name like 'HISTORY_'"; 
  int ret = runRealSQLQuery(lSQL, lValues);
  if(dynlen(lValues) == 1)
  {
    sTemplateSQLSufix = lValues[1][1];
  }
  
  int iTmp = strpos(sTemplateSQLSufix, ",CONSTRAINT");
  sTemplateSQLSufix = substr(sTemplateSQLSufix, iTmp);
  sTemplateSQLSufix += "' where template_name = 'CUSTOM' and suf_object_name like 'HISTORY_'";
  
  sTemplateSQLPrefix = "update arc_template set command = 'CREATE TABLE $name$ (  ";
  for(int i = 1; i <= dynlen(pidiColumnsIdxs); ++i)
  {
    if(i != 1)
    {
      sTemplateSQL += " , ";
    }
    sTemplateSQL += QUERYPARAMS[pidiColumnsIdxs[i]];
  }

  DebugN(  sTemplateSQLPrefix + sTemplateSQL + sTemplateSQLSufix);
  
  ret = runRealSQL(sTemplateSQLPrefix + sTemplateSQL + sTemplateSQLSufix);
  if(ret < 0)
  {
    DebugN("Error update template: " + ret);
    return -1;
  }
  
  ret = runRealSQL("commit");
  if(ret < 0)
  {
    DebugN("Error commiting: " + ret);
    return -1;
  }
  
  string sPath = _fwRDBArchiver_getArcFilePath();
  
  if(sPath == "")
  {
    DebugN("The dbfile_path is empty");
    return -1;
  }

  string sTSPart = "FALSE";
  anytype aTmp = _fwRDBArchiver_getArcConfigValue("ts_partitioning_enabled");
  DebugN("ts_partitioning_enabled: " + aTmp);
  if( aTmp == 1)
  {
    sTSPart = "TRUE";
  }
  
  lSQL = "declare ret integer; begin ret := ArchiveControl.CreateNewGroup('CUSTOM', '" + pisArcName;
  lSQL +=  "', '" + sPath + "',to_timestamp('1970.01.01 01:00:00.000', 'YYYY.MM.DD HH24:MI:SS.FF3'),1, true, " + sTSPart + "); end;";
  
  DebugN(lSQL);
  
  ret = runRealSQL(lSQL);
  if(ret < 0)
  {
    DebugN("Error creating arc group: " + ret);
    return -1;
  }
  
  return 0;
}

/**
 * Updates the given custom arc group with given name and column set
 * @param pisArcname arc group name
 * @param pidiColumnsIdxs column set for the arc
 *
 * @return 0 if everything was fine, -1 otherwise 
 */
public int fwRDBArchiver_updateArc(string pisArcName, dyn_int pidiColumnsIdxs)
{
  string lSQL;
  dyn_dyn_anytype lValues;
  dyn_string dsTmp;

  string sTemplateSQLSufix;  
  string sTemplateSQLPrefix;
  string sTemplateSQL;
  
  DebugN(pidiColumnsIdxs);
  
  lSQL = "select command from arc_template where template_name = 'CUSTOM' and suf_object_name like 'HISTORY_'"; 
  int ret = runRealSQLQuery(lSQL, lValues);
  if(dynlen(lValues) == 1)
  {
    sTemplateSQLSufix = lValues[1][1];
  }
  
  int iTmp = strpos(sTemplateSQLSufix, ",CONSTRAINT");
  sTemplateSQLSufix = substr(sTemplateSQLSufix, iTmp);
  sTemplateSQLSufix += "' where group_name = '" + pisArcName + "' and object_name like '%HISTORY_'";
  
  sTemplateSQLPrefix = "update arc_statement set command = 'CREATE TABLE $name$ (  ";
  for(int i = 1; i <= dynlen(pidiColumnsIdxs); ++i)
  {
    if(i != 1)
    {
      sTemplateSQL += " , ";
    }
    sTemplateSQL += QUERYPARAMS[pidiColumnsIdxs[i]];
  }

  DebugN(  sTemplateSQLPrefix + sTemplateSQL + sTemplateSQLSufix);
  
  ret = runRealSQL(sTemplateSQLPrefix + sTemplateSQL + sTemplateSQLSufix);
  if(ret < 0)
  {
    DebugN("Error update command: " + ret);
    return -1;
  }
  
  ret = runRealSQL("commit");
  if(ret < 0)
  {
    DebugN("Error commiting: " + ret);
    return -1;
  }
  
  
  lSQL = "declare ret integer; begin ret := ArchiveControl.CreateArcAndSwitch('" + pisArcName +"'); end;";
  
  DebugN(lSQL);
  
  ret = runRealSQL(lSQL);
  if(ret < 0)
  {
    DebugN("Error updating arc group: " + ret);
    return -1;
  }
  
  return 0;
}
  
/**
 * Private function to read dbfile_path for the given arc group from DB
 * @param pisArcGrp arc group for which data will be read
 *
 * @return value of the dbfile_path
 */
private string _fwRDBArchiver_getArcFilePath(string pisArcGrp = "EVENT")
{
  string lSQL;
  dyn_dyn_anytype lValues;
  string sRet = "";
  
  lSQL = "select dbfile_path from arc_group where group_name = '" + pisArcGrp + "'";

  DebugN(lSQL);  
  
  int ret = runRealSQLQuery(lSQL, lValues);
  if(dynlen(lValues) == 1)
  {
    sRet = lValues[1][1];
  }
  
  return sRet;

}

/**
 * Private function to read arc_config value for the given key
 * @param pisFiled arc config key for which data will be read
 *
 * @return value for the given key
 */
private anytype _fwRDBArchiver_getArcConfigValue(string pisFiled)
{
  string lSQL;
  dyn_dyn_anytype lValues;
  anytype aRet = "";
  
  lSQL = "select value from arc_config where name = '" + pisFiled + "'";
  
  DebugN(lSQL);  
  
  int ret = runRealSQLQuery(lSQL, lValues);
  if(dynlen(lValues) == 1)
  {
    aRet = lValues[1][1];
  }
  
  return aRet;

}
