//================================================
//          HISTORY OF MODIFICATIONS
//
//          ORIGINAL AUTHOR: Bobby
//
//          G.M. 21-4-2009 added fwDevice_modifyDeviceType
//
//          18-3-2013 G.M. cosmetic changes
//================================================
//
//          2016-07-25 saved as 3.14 G.M.
//

/*
fwDevice_checkDefinition(string dptype,bool &result, dyn_string &exc_info)
fwDevice_registerDeviceType(string fwDevice_type,string nickname,bool &result,dyn_string &exc_info)
fwDevice_modifyDeviceType(string fwDevice_type,string nickname,bool &result,dyn_string &exc_info)
*/
//
//========================================================================
// check if datapoint type matches fwDevice definition
//
#uses "fwRack/fwRackDeprecated.ctl"

#uses "fwGeneral/fwGeneral.ctl"

fwDevice_checkDefinition(string dptype,bool &result, dyn_string &exc_info)
{
  string dp;
  dyn_string dpes_in_dp,dpes_types_in_dp,dpes_in_device;
  dyn_string exception_info;
  int i;
  
  dp = dptype+fwDevice_DEFINITION_SUFIX;
  dynClear(exc_info);
  result = TRUE;
  dpGet(dp+".properties.dpes",dpes_in_device);
  fwGeneral_getDpElements("",dptype,dpes_in_dp,dpes_types_in_dp,exception_info,makeDynInt(DPEL_STRUCT));
  
  if (dynlen(dpes_in_dp) != dynlen(dpes_in_device)) {result=FALSE;return;}
  for(i=1;i<=dynlen(dpes_in_dp);i++) if (dpes_in_dp[i] != dpes_in_device[i]) {
                                   result=FALSE;
                                   dynAppend(exc_info,"i="+i+", dp="+dpes_in_dp[i]+", device="+dpes_in_device[i]);
  }
  return;
}

//========================================================================
// registers a new fw device type 
//
fwDevice_registerDeviceType(string fwDevice_type,string nickname,bool &result,dyn_string &exc_info)
{
  string dp;
  dyn_string dpElements, dpeElementsTypes, emptyList, falseList, exceptionInfo; 
  
  dynClear(exc_info);
  result = FALSE;
  
  if (fwDevice_type == "") {dynAppend(exc_info,"fwDevice_type should not be empty");result = FALSE; return;}
  if (nickname == "") nickname = fwDevice_type;
  
  dp = fwDevice_type+fwDevice_DEFINITION_SUFIX;
  if (dpExists(dp)) {dynAppend(exc_info,"fwDeviceType "+fwDevice_type+" is already defined");result = FALSE; return;}
  
  dpCreate(dp,"_FwDeviceDefinition");
  
  
  // set defaults for definition
  dpSet(dp + ".type", nickname,
 	dp + ".dpType", fwDevice_type,
 	dp + ".panels.navigator.hardware", makeDynString("fwDevice/fwDeviceManage"),
	 dp + ".panels.navigator.logical", makeDynString("fwDevice/fwDeviceManage"),
	 dp + ".panels.editor.hardware", makeDynString("fwDevice/fwDeviceManage", "fwDevice/fwDeviceExpertConfiguration"),
	 dp + ".panels.editor.logical", makeDynString("fwDevice/fwDeviceManage"),
	 dp + ".panels.editor.expert", makeDynString("fwDevice/fwDeviceConfigurationExpert"));
	
  // create dummy dp, necessary for some operations
  if (!dpExists("dummy_" + fwDevice_type)) dpCreate("dummy_" + fwDevice_type, fwDevice_type);
	
  fwGeneral_getDpElements("", fwDevice_type, dpElements, dpeElementsTypes, exceptionInfo, makeDynInt(DPEL_STRUCT));
  //DebugTN(dpElements, dpeElementsTypes);
	
  fwGeneral_fillDynString(emptyList, dynlen(dpElements), exceptionInfo, "EMPTY");
  fwGeneral_fillDynString(falseList, dynlen(dpElements), exceptionInfo, FALSE);
		
  // default values properties
  dpSet(dp + ".properties.dpes", dpElements,
 	dp + ".properties.names", dpElements,
 	dp + ".properties.description", emptyList,
	 dp + ".properties.defaultValues", emptyList,
	 dp + ".properties.types", emptyList,
 	dp + ".properties.userData", emptyList);
	
  // default values for configs		
  dpSet(dp + ".configuration.address.general.defaultType", fwDevice_ADDRESS_NONE,
	 dp + ".configuration.address.OPC.general.canHave", FALSE,
 	dp + ".configuration.address.DIM.general.canHave", FALSE,
 	dp + ".configuration.address.canHave", falseList,
 	dp + ".configuration.dpFunction.canHave", falseList,
	 dp + ".configuration.alert.canHave", falseList,
 	dp + ".configuration.archive.canHave", falseList,
	 dp + ".configuration.smoothing.canHave", falseList,		
 	dp + ".configuration.pvRange.canHave", falseList,			
	 dp + ".configuration.format.canHave", falseList,
	 dp + ".configuration.unit.canHave", falseList,		
	 dp + ".configuration.conversion.canHave", falseList);


  result = TRUE;
  return;

}

//========================================================================
// modify a fw device type
//
fwDevice_modifyDeviceType(string fwDevice_type,string nickname,bool &result,dyn_string &exc_info)
{
  string dp;
  dyn_string dpElements, dpeElementsTypes, emptyList, falseList, exceptionInfo; 
  
  dynClear(exc_info);
  result = FALSE;
  
  if (fwDevice_type == "") {dynAppend(exc_info,"fwDevice_type should not be empty");result = FALSE; return;}
  if (nickname == "") nickname = fwDevice_type;
  
  dp = fwDevice_type+fwDevice_DEFINITION_SUFIX;
  DebugN("fwDevice_modifyDeviceType ",fwDevice_type,nickname,"dp="+dp);
  
  if (dpExists(dp)==FALSE) dpCreate(dp,"_FwDeviceDefinition");  // only recreate if it does not exist

  
  // set defaults for definition
  dpSet(dp + ".type", nickname,
 	dp + ".dpType", fwDevice_type,
 	dp + ".panels.navigator.hardware", makeDynString("fwDevice/fwDeviceManage"),
 	dp + ".panels.navigator.logical", makeDynString("fwDevice/fwDeviceManage"),
 	dp + ".panels.editor.hardware", makeDynString("fwDevice/fwDeviceManage", "fwDevice/fwDeviceExpertConfiguration"),
 	dp + ".panels.editor.logical", makeDynString("fwDevice/fwDeviceManage"),
	 dp + ".panels.editor.expert", makeDynString("fwDevice/fwDeviceConfigurationExpert"));

	
  // create dummy dp, necessary for some operations
  if (!dpExists("dummy_" + fwDevice_type)) dpCreate("dummy_" + fwDevice_type, fwDevice_type);

  fwGeneral_getDpElements("", fwDevice_type, dpElements, dpeElementsTypes, exceptionInfo, makeDynInt(DPEL_STRUCT));

  //DebugTN(dpElements, dpeElementsTypes);
	
  fwGeneral_fillDynString(emptyList, dynlen(dpElements), exceptionInfo, "EMPTY");
  fwGeneral_fillDynString(falseList, dynlen(dpElements), exceptionInfo, FALSE);
		
  // default values properties
  dpSet(dp + ".properties.dpes", dpElements,
 	dp + ".properties.names", dpElements,
	 dp + ".properties.description", emptyList,
 	dp + ".properties.defaultValues", emptyList,
	 dp + ".properties.types", emptyList,
	 dp + ".properties.userData", emptyList);
	
  // default values for configs		
  dpSet(dp + ".configuration.address.general.defaultType", fwDevice_ADDRESS_NONE,
	 dp + ".configuration.address.OPC.general.canHave", FALSE,
	 dp + ".configuration.address.DIM.general.canHave", FALSE,
 	dp + ".configuration.address.canHave", falseList,
 	dp + ".configuration.dpFunction.canHave", falseList,
	 dp + ".configuration.alert.canHave", falseList,
	 dp + ".configuration.archive.canHave", falseList,
	 dp + ".configuration.smoothing.canHave", falseList,		
	 dp + ".configuration.pvRange.canHave", falseList,			
	 dp + ".configuration.format.canHave", falseList,
 	dp + ".configuration.unit.canHave", falseList,		
	 dp + ".configuration.conversion.canHave", falseList);


  result = TRUE;
  return;

}

