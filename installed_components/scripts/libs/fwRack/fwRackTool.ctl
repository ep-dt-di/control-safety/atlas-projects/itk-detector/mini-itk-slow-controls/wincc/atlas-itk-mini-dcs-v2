//=================================================================
//          HISTORY OF MODIFICATIONS
//
//          ORIGINAL AUTHOR: Giulio
//
//          18-3-2013 G.M. Cosmetic changes
//
//          2016-07-25 saved as 3.14 G.M.
//

#uses "fwRack/fwAuxil.ctl"
#uses "fwRack/fwDevice2.ctl"
#uses "fwRack/fwRack.ctl"
#uses "fwRack/fwRackDeprecated.ctl"

#uses "fwNode/fwNode.ctl"

//=================================================================================================
// fwRackTool contains functions to read and process the "Table Configuration", "Type Description",
// "Equipment Description" and "Equipment Status" tables
//

// fw version
const string L_Version = "5.5.0"; // constant deprecated - version string is now retrieved using fwInstallation function

//=========================================================================
// 28-1-2009
// -check with dpExists before calling dpDelete (function DeconfigurePLC)
//=========================================================================
// 21-4-2009
// - do not delete existing devices and types (unless needed) when reconfiguring
// - added function fwRackTool_Version
//=========================================================================
// 28-5-2014   (version 5.4.1)
//  - further checks on duplication of equip names in PLC configuration
//  - introduction of a specific flag to allow/disallow device type modifications
//=========================================================================
// 25-7-2016
//  - improvements in fwRackTool_ReadEventTable 
//
global int _RT_DebugLevel = 1;

// names of the "blob" dpes containing the tables collected from the PLC
const string fwRackConfigurationTables_blob =".Tables.Configuration";
const string fwRackDescriptionTypes_blob = ".Tables.EquipmentConfiguration";
const string fwRackDescriptionEquipments_blob = ".Tables.EquipmentDescription";
const string fwRackEquipmentStatus_blob = ".Tables.EquipmentStatus";
const string fwRackDescriptionCommands_blob = ".Tables.CommandDescription";

//const int fwRackExcelColumns = 7; // code, description, value, dpe, affectchild, has_alert, alert
const int fwRackExcelColumns = 10; // G.M. 29-4-2008 added alert_text and has_archive 25-6-2008 add alert_value
//----------position of information inside "Table Configuration Table"
const int _TableConfig_LENGTH  = 1;
const int _TableConfig_NEQUIP  = 2;
const int _TableConfig_PLC_ADR = 3;
const int _TableConfig_PLC_LEN = 4;   
const int _TableConfig_CMD_ADR = 5;
const int _TableConfig_CMD_LEN = 6;
const int _TableConfig_CMD_RESULT_ADR = 7;
const int _TableConfig_CMD_RESULT_LEN = 8;   
const int _TableConfig_SYS_STATUS_ADR = 9;
const int _TableConfig_SYS_STATUS_LEN = 10;
const int _TableConfig_CLOCK_ADR = 11;
const int _TableConfig_CLOCK_LEN = 12;   
const int _TableConfig_MEASURE_ADR = 13;
const int _TableConfig_MEASURE_LEN = 14;   
const int _TableConfig_EQU_STATUS_ADR = 15;
const int _TableConfig_EQU_STATUS_LEN = 16;
const int _TableConfig_EQU_DESC_ADR = 17;
const int _TableConfig_EQU_DESC_LEN = 18;
const int _TableConfig_TXTCMD_ADR = 19;
const int _TableConfig_TXTCMD_LEN = 20;
const int _TableConfig_TYPE_DESC_ADR = 21;
const int _TableConfig_TYPE_DESC_LEN = 22;
const int _TableConfig_CONF_MODIF = 23;
     

//----------definition of error codes
const int _OK = 0;
const int _ERROR_TABLE_WRONG_LENGTH = 1;

//----------definitions for archive config (G.M 29-4-2008)
const string L_archive_class = "RDB-99) EVENT";
const string L_archive_type = 15;
//------------------------------------------------

const string FwRackPowerDistributionPLC_DevDef = "FwRackPowerDistributionPLCInfo";
global dyn_string EXCEL_code_list,
                  EXCEL_text_list,
                  EXCEL_dpe_list,
                  EXCEL_alert_list,
                  EXCEL_has_alert_list,
                  EXCEL_value_list,
                  EXCEL_alert_value_list,  // G.M. 25-6-2008
                  EXCEL_affectchild_list,
                  EXCEL_alert_text_list,
                  EXCEL_has_archive_list;  //G.M 29-4-2008 added the last 2

// dyn_string New_DeviceList,New_DeviceTypeList; // G.M. 21-4-2009
global dyn_string fwRack_New_DeviceList, fwRack_New_DeviceTypeList; // 5.4.1 rename and made them global
dyn_string New_TypeList;                     // G.M. 21-4-2009

int allow_types_modif;            // -1: does not allow. >= 0 allow modifications in the structure of the 
                                  // device datatypes
int only_use_good_configuration;  // -1 : process also if there are errors. 
                                  //  0 or >0 only process if no errors 

int preserve_old_configuration;   //G.M. 21-4-2009 -1: delete the old configuration. Otherwise keep it

global bool         fwRack_Duplicated_Equip_Name_Flag;
global dyn_string   fwRack_Duplicated_Equip_Name_List;
//===================================================================================
// functions in this library
// ---test & debug functions
//void fwRackTool_DebugLevel(int value)
//void fwRackTool_DumpGlobal(string PLC)
//
// ---read the Excel table
//void fwRackTool_ReadEventTable(string filename, int &nevents, dyn_string &exc_info) 
//void fwRackTool_ReadExcelGlobals(dyn_string &exc_info) 
//
// ---configure datatypes and datapoints getting info from the PLC tables
//void fwRackTool_ReadDescriptionTypes(string PLC,dyn_string &exc_info)
//void fwRackTool_ReadDescriptionEquipments(string PLC, dyn_string &exc_info)
//void fwRackTool_ReconfigurePLC(string PLC,dyn_string &exc_info)
//
//---read the Equipment Status from the blob and dispatches this info to the equipment datapoints
//void fwRackTool_ReadEquipmentStatus(string PLC, dyn_string &exc_info)
//
//---set/reset the invalid bit
//void fwRackTool_SetInvalidBit(string PLC,bool flag, dyn_string &exc_info)
//
//===================================================================================
//        variables used when reading the Equipment Status
//
// The status bits (boolean) of all the "Properties" of all defined "Equipments" is contained 
// in the "Equipment Status" table, implemented as a number of consecutive memory words in the PLC.
// Every word may encode up to 16 status bits, and all the bits contained in a word belong to the 
// same equipment. All the bits belonging to a given equipment are packed in CONSECUTIVE WORDS. The
// first status bit of a given equipment will be found at bit 0 of the first word dedicated to that
// equipment, and the other bits will follow WITHOUT HOLES. If more than 16 bits are needed, the next 
// word will be used, and so on.
// Another table, the "Equipment Description", specifies  the index of the first word (in Equipment 
// Status) containing the status bits for each equipment. The number of bits per equipment depend on 
// the equipment type, and specific lists of the properties for each type are contained in yet another 
//table, the "Equipment Type Description".
//
// The Equipment Status Table is read from the PLC memory and copied into a "blob" datapoint every 
// time at least one of the status bits changes. When this happens, the PVSS code must decode the blob 
// to either -write all the status bit values into the corresponding dpes of the equipment datapoints 
// (if this is the first time after a reconfiguration), or -only write the values that have changed. 
// To be able to perform this task quickly, we need a way of finding the dpe associated with a given 
// position (Word, bit) in the Equipment Status table. We will use the following datastructures
//
// _RT_status_dpe, _RT_status_address_word, _RT_status_address_bit : these arrays will contain, for every 
// property of every equipment, the name of the dpe in the equipment data point, and the position of the
// status bit in the Equipment Status table
//
// _RT_first_index_in_status : this integer array has as many elements as words in the Equipment Status table.
// The Nth element of the array contains either -1 if the corresponding word (in E.S. table) is not used, 
// or the <index> of the first dpe in _RT_status_dpe whose status bit is contained in this word of the E.S. 
// table (this means that _RT_status_address_word[<index>] == N, and L_statue_address_bit[<index>] == 0)
// NOTICE that we count the words in E.S. starting from zero, so _RT_first_index_in_status[1] contains the
// index of the property dpe (if any) coded in Word 0, Bit 0 of the E.S. Table
//
// _RT_current_status is a boolean array containing the status bits in the same order used in _RT_status_dpe.
// 
//
// NEW : BECAUSE OF THE MULTIPLE PLC ENVIRONMENT, THESE VARIABLES WILL BE DEFINED INSIDE EVERY ROUTINE
// NEEDING THEM, AND WILL BE KEPT IN DATAPOINTS (tagged with the PLC name)
//
// dyn_string _RT_status_dpe;          // list of the equipment dpes which should receive the status 
                                       // values coming from the PLC
// dyn_int _RT_status_address_word;    // These two arrays contain the list of the positions where the values
// dyn_int _RT_status_address_bit;     // will be found in the "Equipment_Status" blob. word and bit start from 0

// dyn_int _RT_first_index_in_status;  // to each used word in the EquipmentStatus blob, associates a position  
                                       // in the _RT_status_dpe array. The position contains the name of the dpe
                                       // whose status is contained in bit 0 of the word in the blob

// global dyn_bool _RT_current_status; // this array will contain the current values of all the status bits; 
                                       // it will be filled by doing dpGet(_RT_status_dpe,_RT_current_status);

// dyn_string _RT_status_dpe_invalid;
// dyn_bool _RT_status_invalid;

// int _RT_status_dpe_len;       // length of _RT_status_dpe
//
//


//================================================================================
// fwRackTool_Version
//================================================================================
string fwRackTool_Version()
{
  return fwInstallation_getComponentVersion("fwRack");
}

//================================================================================
// fwRackTool_DumpGlobal
//================================================================================
void fwRackTool_DumpGlobal(string PLC)
{
int i;
string buf;

dyn_string _RT_status_dpe;
dyn_int _RT_status_address_word,_RT_status_address_bit,_RT_first_index_in_status;

if (PLC == "") return;

dpGet("RCA/"+PLC+"/Rack_StatusAddressWord.iarr",_RT_status_address_word);
dpGet("RCA/"+PLC+"/Rack_StatusAddressBit.iarr",_RT_status_address_bit);
dpGet("RCA/"+PLC+"/Rack_StatusDpe.list",_RT_status_dpe);
dpGet("RCA/"+PLC+"/Rack_FirstIndexInStatus.iarr",_RT_first_index_in_status);

DebugTN("fwRackTool_DumpGlobal");
DebugN("  i:  word bit    dpe");
for (i=1;i<=dynlen(_RT_status_dpe);i++) {
    sprintf(buf,"%4d: %4d %2d     %s",i,_RT_status_address_word[i],_RT_status_address_bit[i],_RT_status_dpe[i]);
    DebugN(buf);
}
DebugN("------------------------------------------");
DebugN("first index in status");
for (i=1; i<=dynlen(_RT_first_index_in_status);i++) DebugN(i,_RT_first_index_in_status[i]);
DebugTN("------------------------------------------");
}


//================================================================================
// fwRackTool_ReadEventTable reads the comma separated file derived from the Excel table,
// and produces a set of datapoints of type FwList
//=================================================================================
void fwRackTool_ReadEventTable(dyn_string &exc_info)
{
file f;
string buf;
  
dyn_string split;
int i,j;
dyn_dyn_string table;
  
string filename;
  
  //filename = getPath(DATA_REL_PATH)+"ExcelFile.csv";
  //filename = fwInstallation_getComponentPath("FwRack")+"ExcelFile.csv";
  
dyn_string projPaths;

  //--- new: create a dp to save last selection
  if (dpExists("fwRack_LastExelFileUsed")==FALSE) dpCreate("fwRack_LastExelFileUsed","FwParam");
  
  DebugTN(L_Version);
  fwInstallation_getProjPaths(projPaths);
  DebugN(projPaths);
  
  //-------first try to find customized file
  for (i = 1; i <= dynlen(projPaths); i++){
    filename = projPaths[i] + "/data/ExcelFileCustom.csv"; DebugN(filename);
    j = access(filename, F_OK); 
    if (j == -1) {
        filename = "";
        continue;
    }
    DebugN("trying to open "+filename);
    f = fopen(filename,"r");
    if (f != 0) { // file exists and can be opened
      fclose(f);
      break;
    } else filename = "";
  }
  
  if (filename == "") {  // no good custom file found: try with default file
    for (i = 1; i <= dynlen(projPaths); i++){
      filename = projPaths[i] + "/data/ExcelFile.csv"; DebugN(filename);
      j = access(filename, F_OK); 
      if (j == -1) {
        filename = "";
        continue;
      }
      DebugN("trying to open "+filename);
      f = fopen(filename,"r");
      if (f != 0) {
        fclose(f);
        break;
      } else filename = "";
    }
  }    
      
//===new G.M. 19-6-2009 do not call fileSelector if this runs under a ctrl manager
DebugN("BEFORE CHECK ON MANAGER TYPE");
if (myManType() != CTRL_MAN) {
  string dat,datapath;
  dat = filename;
  datapath = filename;
  for (i = strlen(datapath); i > 1; i--) {
    if (datapath[i]=='/') {
        datapath = substr(datapath,0,i);
        break;
     }
  }

  DebugN("BEFORE FILE SELECTOR: filename=",filename);
  fileSelector(dat,datapath,FALSE,"ExcelFile*.csv");
  DebugTN(dat);
  if (dat == "") return;
  filename = dat;
}
//===end new

  if (filename == "") {
      DebugTN("cannot find Excel file");
      return;
  }

  ///fwGeneral_getProjectPath
  f = fopen(filename,"r");
  if (f == 0) {
      dynAppend(exc_info,"Unable to open file "+filename);
      DebugTN("===============>  Cannot find "+ filename +" file!");
      //nevents = -1;
      return;
  }
  for(;;) {
     if (feof(f)) break;
     fgets(buf,500,f);                // read a line from the file
     buf = substr(buf,0,strlen(buf)-1);
     if (buf[0]=="#") continue;
     if (strlen(buf)<5) continue;
     split = strsplit(buf,",");       // cut the line into the different fields
     if (dynlen(split) < fwRackExcelColumns) for(i=dynlen(split)+1;i<=fwRackExcelColumns;i++) dynAppend(split,"");
     for (i = 1; i <= dynlen(split); i++) { // remove additional spaces 
          split[i]=strrtrim(split[i]);
          split[i]=strltrim(split[i]);
          dynAppend(table[i],split[i]);
     }
  }
  fclose(f);
  DebugTN("....The Excel file contains "+dynlen(table[1])+" valid lines");
  if (_RT_DebugLevel >= 2) DebugTN(table);
  if (dpExists("RCA_EXCEL_code")==FALSE)          dpCreate("RCA_EXCEL_code","FwList");
  if (dpExists("RCA_EXCEL_text")==FALSE)          dpCreate("RCA_EXCEL_text","FwList");
  if (dpExists("RCA_EXCEL_value")==FALSE)         dpCreate("RCA_EXCEL_value","FwList");
  if (dpExists("RCA_EXCEL_dpe")==FALSE)           dpCreate("RCA_EXCEL_dpe","FwList");
  if (dpExists("RCA_EXCEL_has_alert")==FALSE)     dpCreate("RCA_EXCEL_has_alert","FwList");
  if (dpExists("RCA_EXCEL_alert")==FALSE)         dpCreate("RCA_EXCEL_alert","FwList");
  if (dpExists("RCA_EXCEL_affectchild")==FALSE)   dpCreate("RCA_EXCEL_affectchild","FwList");
  if (dpExists("RCA_EXCEL_alert_text")==FALSE)    dpCreate("RCA_EXCEL_alert_text","FwList"); //G.M 29-4-2008
  if (dpExists("RCA_EXCEL_has_archive")==FALSE)   dpCreate("RCA_EXCEL_has_archive","FwList"); //G.M 29-4-2008
  if (dpExists("RCA_EXCEL_alert_value")==FALSE)   dpCreate("RCA_EXCEL_alert_value","FwList"); //G.M 25-6-2008
  
  dpSetWait("RCA_EXCEL_code.list",table[1]);
  dpSetWait("RCA_EXCEL_text.list",table[2]);
  dpSetWait("RCA_EXCEL_value.list",table[3]);
  dpSetWait("RCA_EXCEL_dpe.list",table[4]);
  dpSetWait("RCA_EXCEL_affectchild.list",table[5]);
  dpSetWait("RCA_EXCEL_has_alert.list",table[6]);
  dpSetWait("RCA_EXCEL_alert.list",table[7]);
  dpSetWait("RCA_EXCEL_alert_text.list",table[8]);  //G.M 29-4-2008
  dpSetWait("RCA_EXCEL_has_archive.list",table[9]); //G.M 29-4-2008
  dpSetWait("RCA_EXCEL_alert_value.list",table[10]); //G.M 29-4-2008
}   


//=========================================================================== 
//void fwRackTool_ReadExcelGlobals(dyn_string &exc_info) 
//
// reads from the RCA_EXCEL... datapoints to the EXCEL... global list
//===========================================================================
void fwRackTool_ReadExcelGlobals(dyn_string &exc_info) 
{
int i;
  
dpGet("RCA_EXCEL_code.list",EXCEL_code_list);
dpGet("RCA_EXCEL_text.list",EXCEL_text_list);
dpGet("RCA_EXCEL_dpe.list",EXCEL_dpe_list);
dpGet("RCA_EXCEL_has_alert.list",EXCEL_has_alert_list);
dpGet("RCA_EXCEL_alert_value.list",EXCEL_alert_value_list); //G.M. 25-06-2008
dpGet("RCA_EXCEL_affectchild.list",EXCEL_affectchild_list);
dpGet("RCA_EXCEL_value.list",EXCEL_value_list);
dpGet("RCA_EXCEL_alert.list",EXCEL_alert_list);
dpGet("RCA_EXCEL_alert_text.list",EXCEL_alert_text_list);   //G.M 29-4-2008
dpGet("RCA_EXCEL_has_archive.list",EXCEL_has_archive_list); //G.M 29-4-2008

//===new G.M. 25-06-2008 
for (i = 1; i <= dynlen(EXCEL_value_list); i++) 
   if (EXCEL_alert_value_list[i] == "") EXCEL_alert_value_list[i] = EXCEL_value_list[i];
}


//===========================================================================
// fwRackTool_CheckDescriptionTypes looks for errors in the table containing the description of all 
// the equipment types used by this Rack System.
//
// Format of the table inside the blob dpe:
//
//    -----------------------------------------
//    |   Equipment Type Number               |
//    |   Number of commands for this type    |
//    |   Code of first command               |
//    |       ............                    |
//    |   Code of last command                |
//    |   Number of events for this type      |
//    |   Code of first event                 |
//    |       ............                    |
//    |   Code of last event                  |
//    -----------------------------------------
//    |   Equipment Type Number               |
//    |       .............                   |
//    |_______________________________________|
//
// The table must match the above described format
// The Equipment Type Number must be a positive integer
// The same Equipment Type Number cannot be reused
// The number of commands for a type must be non-negative
// The same command code cannot appear twice in the same Equipment Type
// The number of events for a type must be non-negative
// Any event with code > 0 defines a "property" of the equipment type, 
// associated with a bit in the status table
// The relative position of a property in the table determines the position 
// of the corresponding bit in the status table
// Events with code == 0 do not define properties, and correspond to unused bits in the status table
// The same property code cannot appear twice in the same Equipment Type

//===========================================================================
void fwRackTool_CheckDescriptionTypes(string PLC,dyn_string &exc_info,dyn_string &deviceTypeModify_info)
{
blob Desc_types;
int blob_len;
int pointer;
int n_types;
int i,j,n;
int jj;

string sysname; //G.M. 21-4-2009
dyn_string dpnames; // G.M. 21-4-2009
bool recreate_existing_types;  // G.M 21-4-2009

int n_events,event,n_commands,command,type_num;
dyn_int command_list,event_list,type_nums;
dyn_int event_list_pos, old_event_list, old_event_list_pos;  // NEW G.M. 21-4-2009

dpGet("RCA/"+PLC+fwRackDescriptionTypes_blob, Desc_types);
blob_len = bloblen(Desc_types);
if (_RT_DebugLevel > 0) DebugTN("CHECK TYPES: blob_len = "+blob_len);
if (_RT_DebugLevel > 1) DebugTN(Desc_types);

//====NEW G.M. 21-4-2009
sysname = getSystemName();
dynClear(New_TypeList);
//====

for(;;) {   // read and process the entire blob
  if (pointer >= blob_len) break;
  ++n_types;
  blobGetValue(Desc_types,pointer,type_num,2,TRUE); 
  DebugTN("type_num = "+type_num,"pointer = "+pointer);
  pointer = pointer+2; //read type_num
  if (type_num == 0) {
     dynAppend(exc_info,"CHECK_TYPES: illegal type 0 defined at "+n_types+" : empty space at the end of table?");
  }
  j = dynContains(type_nums,type_num);
  if (j  > 0) {
     dynAppend(exc_info,"CHECK_TYPES: type "+type_num+ " defined at "+n_types+" already defined at position "+j);
  }
  dynAppend(New_TypeList,"FwRackDevicePDType_"+type_num); //G.M. 21-4-2009
  
  //----read the Command codes for this type
  //DebugTN("type_num = "+type_num);
  if (pointer >= blob_len) {dynAppend(exc_info,"CHECK_TYPES: last type not completed");break;}
  blobGetValue(Desc_types,pointer,n_commands,2,TRUE); 
  DebugTN("n_commands = "+n_commands,"pointer = "+pointer);
  pointer = pointer+2; //read number of commands
  //DebugTN("n_commands = "+ n_commands);
  dynClear(command_list);

  if (n_commands > 0) {
    for(i=1;i<= n_commands; i++) {
        if (pointer >= blob_len) {dynAppend(exc_info,"CHECK_TYPES: last type not completed");break;}
        blobGetValue(Desc_types,pointer,command,2,TRUE);
        if (_RT_DebugLevel > 1) DebugTN("command = "+command,"pointer = "+pointer);        
        pointer = pointer+2;
        j=dynContains(command_list,command);
        if (j > 0) {
          dynAppend(exc_info,"CHECK_TYPES: type "+type_num+ " same command ("+command+" twice: pos "+j+" and "+i);
        }
        dynAppend(command_list, command);
        //if (type_num == 6) DebugN("COMMAND = "+command," Command_list = "+command_list);
    }
  }
  
  if (pointer >= blob_len) {dynAppend(exc_info,"CHECK_TYPES: last type not completed");break;}
  blobGetValue(Desc_types,pointer,n_events,2,TRUE); 
  DebugTN("n_events = "+n_events,"pointer = "+pointer);
  pointer = pointer+2; //read number of events
  //DebugTN("n_events = "+ n_events);
  dynClear(event_list);
  dynClear(event_list_pos); // NEW G.M. 21-4-2009
  if (n_events > 0) {
    for(i=1;i<=n_events;i++) {
      if (pointer >= blob_len) {dynAppend(exc_info,"CHECK_TYPES: last type not completed");break;}
      blobGetValue(Desc_types,pointer,event,2,TRUE);
      if (_RT_DebugLevel > 1) DebugTN("event = "+event,"pointer = "+pointer);        
      pointer = pointer+2;
      if (event == 0) continue;
      j=dynContains(event_list,event);
      if (j > 0) {
          dynAppend(exc_info,"CHECK_TYPES: type "+type_num+ "same property ("+event+" twice: pos "+j+" and "+i);
      }
      dynAppend(event_list, event);
      dynAppend(event_list_pos,i-1); // NEW G.M. 21-4-2009
    }
  }
  //==== NEW. G.M. 21-4-2009    if this type already existed, check that commands and events are the same
    if (fwAuxil_doesDpTypeExist("FwRackDevicePDType_"+type_num)) {
        //----check if this type has datapoints belonging to other PLCs (in which case we cannot recreate it)
        dpnames = dpNames("RCA*","FwRackDevicePDType_"+type_num);
        for(i=1;i<=dynlen(dpnames);i++) {
            dpnames[i] = substr(dpnames[i],strlen(sysname)+4);          // skip system name + "RCA\";
            if (PLC+"\\" != substr(dpnames[i],0,strlen(PLC)+1)) {       // type is used in another PLC
                           recreate_existing_types = FALSE;             // and it cannot be recreated
                           break;
            }
        }
        if (recreate_existing_types == TRUE) continue; // NO NEED TO CHECK
        
        //-----now check if the description of this type is still the same as the existing one
        // check command list
        // check event list
        dpGet("FwRackDevicePDType_"+type_num+"_Events.list",old_event_list);
        dpGet("FwRackDevicePDType_"+type_num+"_EventPos.list",old_event_list_pos);
        if (dynlen(event_list) != dynlen(old_event_list)) {
           if (allow_types_modif) dynAppend(deviceTypeModify_info,"CHECK_TYPES: type "+type_num+ "number of events differs from existing one");
           else dynAppend(exc_info,"CHECK_TYPES: type "+type_num+ "number of events differs from existing one");
           continue;
        }
        for(i=1;i<=dynlen(event_list);i++) {
           if (event_list[i] != old_event_list[i]) {
               if (allow_types_modif) dynAppend(deviceTypeModify_info,"CHECK_TYPES: type "+type_num+ "event code differs from existing one at pos. "+i);
               else dynAppend(exc_info,"CHECK_TYPES: type "+type_num+ "event code differs from existing one at pos. "+i);
           }
           if (event_list_pos[i] != old_event_list_pos[i]) {
               if (allow_types_modif) dynAppend(deviceTypeModify_info,"CHECK_TYPES: type "+type_num+ "event pos differs from existing one at pos. "+i);
               else dynAppend(exc_info,"CHECK_TYPES: type "+type_num+ "event pos differs from existing one at pos. "+i);
           }
        }
   }

  //====
}

}

//===========================================================================
// fwRackTool_ReadDescriptionTypes reads the table containing the description of all 
// the equipment types used by this Rack System. It creates the corresponding datapoint 
// types, and register them as fwDevices
//===========================================================================
void fwRackTool_ReadDescriptionTypes(string PLC,dyn_string &exc_info)
{
blob Desc_types;
blob Desc_commands;
int blob_len;
int pointer;
int n_types;
int i,j;
int jj;

string type_name; 
dyn_string type_names;
int type_num;
int n_events, n_commands;
dyn_int event_list, command_list, clean_event_list, clean_event_pos;
dyn_string event_name, clean_eventnames_list, command_name;

dyn_anytype type_desc;

bool result;

dyn_string children;

string command;
dyn_string commands;
string buf;

dyn_string exc_info2;

string PropertyOrMask;
bool HasMask;
int nProperties,nMaskBits;
int k;
string dpename,dpename2;

//DebugTN("------------------------------------------------------>------->>---->>" + PLC);
if (PLC == "") return;

//---- we don't yet have the event and command names
//dpGet("fwRack_EventNameList.List",event_name);
//dpGet("fwRack_CommandNameList.List",command_name);

fwAuxil_createListDptype();      // creates dpType FwList if it does not exist
fwAuxil_createIntParamDptype();  // creates dpType FwIntParam if it does not exist
fwAuxil_createParamDptype();     // creates dpType FwParam if it does not exist
//------------------------------------------------------------------------------------
// Read the Excel File if this was not already done
if (dpExists("RCA_EXCEL_code")==FALSE) fwRackTool_ReadEventTable(exc_info2);
fwRackTool_ReadExcelGlobals(exc_info2);

//------------------------------------------------------------------------------------
// Reads the text corresponding to the different commands (from a PLC table) and saves
// them into a datapoint. This datapoint (or a dyn_string loaded with its contents) 
// shall be later used to go from a command code to the corresponding text.
// The table (CommandDescription) should be the same on every PLC. Ideally there should
// be only one datapoint (and not one dp per PLC) containing the command texts
//
dpGet("RCA/"+PLC+fwRackDescriptionCommands_blob, Desc_commands);
if (_RT_DebugLevel > 1) DebugTN("COMMAND DESCRIPTION BLOB:",Desc_commands);


for(i=1;i<=bloblen(Desc_commands)/8;i++) {
     blobGetValue(Desc_commands,(i-1)*8,buf,8);  sscanf(buf,"%s",command);
     dynAppend(commands,command);
}
if (dpExists("RCA_"+PLC+"_"+"CommandDescription")==FALSE) dpCreate("RCA_"+PLC+"_"+"CommandDescription","FwList");
dpSetWait("RCA_"+PLC+"_"+"CommandDescription.list",commands);
//if (dynlen(commands)>0) dpSetWait("RCA_"+PLC+"_"+"CommandDescription.list",commands);
//
// end of CommandDescription processing
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
// Reads the table containing the description of the types used by this PLC
// for every type it reads the possible commands and events, saves them into FwList datapoints,
// create a datapoint type to be used to instanciate devices of this type,
// register it as a fwDeviceType
//
// A type is identified by an integer code. Not all the possible types will be present on every PLC.
// The definition of a type with a given code will be identical on every PLC using that type
//
dpGet("RCA/"+PLC+fwRackDescriptionTypes_blob, Desc_types);
blob_len = bloblen(Desc_types);
//if (_RT_DebugLevel > 0) {DebugTN("blob_len = "+blob_len);DebugTN(Desc_types);}

for(;;) {   // read and process the entire blob
  if (pointer >= blob_len) break;
  ++n_types;   //if (n_types>1) break;
  if (_RT_DebugLevel > 0) DebugTN("n_types = "+n_types);
  //DebugTN("pointer=",pointer);
  //=== READ THE TYPE CODE
  blobGetValue(Desc_types,pointer,type_num,2,TRUE); pointer = pointer+2;
  type_name = "FwRackDevicePDType_"+type_num;
  
  //----READ THE NUMBER OF COMMANDS FOR THIS TYPE AND THEIR CODES
  //DebugTN("type_num = "+type_num);
  blobGetValue(Desc_types,pointer,n_commands,2,TRUE); pointer = pointer+2; //read number of commands
  //DebugTN("n_commands = "+ n_commands);
  dynClear(command_list);
  HasMask = FALSE;
  if (n_commands > 0) {
      command_list = fwAuxil_blobGetValue_int(Desc_types,n_commands,pointer); 
      pointer = pointer + 2*n_commands; //read the commands
  }
  for(i=1;i<=dynlen(command_list);i++) if (command_list[i] == 9) HasMask=TRUE;

  //----READ THE NUMBER OF EVENTS FOR THIS TYPE AND THEIR CODES
  blobGetValue(Desc_types,pointer,n_events,2,TRUE); pointer = pointer+2; //read number of events
  //DebugTN("n_events = "+ n_events);
  dynClear(event_list);
  if (n_events > 0) {
      event_list = fwAuxil_blobGetValue_int(Desc_types,n_events,pointer); 
      pointer = pointer + 2*n_events; //read the events
  }
  //
  //---create and set the auxiliary datapoints of type FwList
  // we have to remove unwanted "zero" properties from the saved event list
  // ("zero" properties are used as spaceholders in the PLC table, and provide a way of specifying
  // unused bits in the status datablock)
  dynClear(clean_event_list);
  dynClear(clean_eventnames_list);
  dynClear(clean_event_pos);
  for(j=1;j<=dynlen(event_list);j++) {
      if (event_list[j] != 0) {
            dynAppend(clean_event_list,event_list[j]);
            dynAppend(clean_event_pos,j-1);
      }
  }
  if (dpExists(type_name+"_Events")==FALSE) dpCreate(type_name+"_Events","FwList");     
  dpSetWait(type_name+"_Events.list",clean_event_list);
  if (dpExists(type_name+"_EventPos")==FALSE) dpCreate(type_name+"_EventPos","FwList");   
  dpSetWait(type_name+"_EventPos.list",clean_event_pos);
  if (dpExists(type_name+"_Commands")==FALSE) dpCreate(type_name+"_Commands","FwList");   
  dpSetWait(type_name+"_Commands.list",command_list);

  //==== FORMAT THE DATA STRUCTURE TO CREATE THE DATAPOINT TYPE FOR THIS KIND OF DEVICES
  //---now fill the type_desc array, used to create the datapoint type (by dpTypeCreate or dpTypeChange)
  dynAppend(type_desc,type_name);
  dynAppend(type_desc,1); //level 1
  dynAppend(type_desc,1); //structure
  nProperties = 0;
  nMaskBits = 0;
  for(j=1;j<=dynlen(event_list);j++) {
     if (event_list[j] == 0) continue; // SKIP ALL PROPERTIES = 0
     k=dynContains(EXCEL_code_list,event_list[j]);
     if (HasMask && (j+16)>dynlen(event_list)) ++nMaskBits;
     else ++nProperties;
     //---if we find event code in Excel table, we use Excel name...
     if (k > 0) {
       dpename = EXCEL_dpe_list[k];
     } else {  
     //---else we use event code directly
        PropertyOrMask = "Property";
        if (HasMask && (j+16)>dynlen(event_list)) PropertyOrMask="MaskBit";
        dpename=PropertyOrMask+"_"+event_list[j];
     }
     if (event_list[j] >= 5000) dpename = "INV_"+dpename;  // codes above 5000 represent inverted signals
     //---write the info to create the dpe
     //----NEW G.M. 26-6-2008 detect duplications in dpe names
     jj=dynContains(type_desc,dpename); 
     if (jj > 0) DebugTN("BEWARE----->IN TYPE "+type_name+": duplicated dpe "+
                         dpename+" at line "+j+ "(first at line "+(j-3)/3);
     //----
     dynAppend(type_desc,dpename);
     dynAppend(type_desc,2);  // level 2
     dynAppend(type_desc,23); //boolean
     //---
     dynAppend(clean_eventnames_list,dpename);
  }
  dynAppend(type_desc,"NameInPlc");dynAppend(type_desc,2);dynAppend(type_desc,25); //level 2, string
  dynAppend(type_desc,"Command");dynAppend(type_desc,2);dynAppend(type_desc,25);  //level 2, string
  dynAppend(type_desc,"Response");dynAppend(type_desc,2);dynAppend(type_desc,25); //level 2, string
  dynAppend(type_desc,"ParentID");dynAppend(type_desc,2);dynAppend(type_desc,25); //level 2, string
  dynAppend(type_desc,"HasPower");dynAppend(type_desc,2);dynAppend(type_desc,23); //level 2, string
  dynAppend(type_desc,"Mask");dynAppend(type_desc,2);dynAppend(type_desc,1); //level 2, structure 
  dynAppend(type_desc,"rMask");dynAppend(type_desc,3);dynAppend(type_desc,46); //level 3, blob  
  dynAppend(type_desc,"wMask");dynAppend(type_desc,3);dynAppend(type_desc,46); //level 3, blob  
  dynAppend(type_desc,"StatusAddress");dynAppend(type_desc,2);dynAppend(type_desc,21); //level 2, int  
  
  //dynAppend(type_desc,"nProperties");dynAppend(type_desc,2);dynAppend(type_desc,21); //level 2, int  
  //dynAppend(type_desc,"nMaskBits");dynAppend(type_desc,2);dynAppend(type_desc,21); //level 2, int 

  //DebugTN("type_desc=",type_desc);
  
  //==== CREATION OR MODIFICATION OF THE (NEW) DATAPOINT TYPE 
  //======NEW G.M. 21-4-2009 if the type already exist, you may have to modify it
  if (fwAuxil_doesDpTypeExist(type_name)==TRUE) fwAuxil_modifyDatapointType(type_desc);
    //======
  if (fwAuxil_doesDpTypeExist(type_name)==FALSE) fwAuxil_createDatapointType(type_desc);
  dynClear(type_desc);
  
  //=== REGISTER THIS TYPE AS A FW DEVICE
  if (_RT_DebugLevel > 1) DebugTN("Before fwDevice_modifyDeviceType");
  //fwDevice_registerDeviceType(type_name,type_name,result,exc_info2);  OLD
  fwDevice_modifyDeviceType(type_name,type_name,result,exc_info2);  // NEW G.M. 21-4-2009
  DebugTN("Registering "+type_name+": "+result); if(result == FALSE) DebugTN(exc_info2);
  
  //=== WRITE SOME AUXILIARY INFO
  if (dpExists(type_name+"_nProperties")==FALSE) dpCreate(type_name+"_nProperties","FwIntParam");
  if (dpExists(type_name+"_nMaskBits")==FALSE) dpCreate(type_name+"_nMaskBits","FwIntParam");
  if (dpExists(type_name+"_StatusAddressLen")==FALSE) dpCreate(type_name+"_StatusAddressLen","FwIntParam");
  dpSetWait(type_name+"_nProperties.count",nProperties);
  dpSetWait(type_name+"_nMaskBits.count",nMaskBits);
  dpSetWait(type_name+"_StatusAddressLen.count",(dynlen(event_list)+15)/16);

  if (dpExists(type_name+"_EventNames")==FALSE) dpCreate(type_name+"_EventNames","FwList");   
  dpSetWait(type_name+"_EventNames.list",clean_eventnames_list);
  //--- append type_name to list of datapoint types
  dynAppend(type_names,type_name);
  //Setting Navigation panel for device type
  dpSetWait(type_name +"Info.panels.navigator.hardware","fwRack/fwRackObjectParamRef");
}  // end of loop on the blob

//=== end of TypesDescription processing
dpSetWait(PLC+"_ConfigurationDpTypeList.list",type_names);

//-----------------------------------------------------------------------------
// Add all device types as possible children of FwRackPowerDistributionPLC_DevDef
if (dpExists(FwRackPowerDistributionPLC_DevDef)) {
   dpGet(FwRackPowerDistributionPLC_DevDef+".general.childrenDPTypes",children);
   for(j=1;j<=dynlen(type_names);j++) {
           if (dynContains(children,type_names[j]) < 1) dynAppend(children,type_names[j]);
   }
   dpSetWait(FwRackPowerDistributionPLC_DevDef+".general.childrenDPTypes",children);
}
}
//==========================================================================================
// fwRackTool_CheckDescriptionEquipments is called before trying to actually create the devices.
// The function checks that thereare no names duplications, that the types are existing, that the
// configuration table has the correct length 
//
// Format of the table inside the blob dpe: one block of 26 bytes per equipment
//
//    ------------------------------------------
//    |   Equipment Name             14 bytes  |
//    |   Parent address        (int) 2 bytes  |
//    |   Reserved                    2 bytes  |
//    |   Equipment type code   (int) 2 bytes  |
//    |   Code of last command                 |
//    |   Equipment number      (int) 2 bytes  |
//    |   Equip. status address (int) 2 bytes  |
//    |   Equip. status length        1 byte   |
//    |   Equip. "travee" number      1 byte   |
//    -----------------------------------------
//    |   Equipment Name                       |
//    |       .............                    |
//    |________________________________________|
//
// The equipment name cannot be an empty string
// The same equipment name cannot appear twice


void fwRackTool_CheckDescriptionEquipments(string PLC, dyn_string &exc_info)
{
blob Desc_Equip;
int blob_len;
int pointer;
int parent_address;
int status_address;
int equip_number;
int equip_type;
int status_length,travee;
int dummy;
string buf, equip_name,parent_name;
string plc_equip_name; 
dyn_string equip_names;
dyn_int status_addresses;

int j,endlessloop;
string Prefix;

dyn_int _RT_ConfigTable;
int _RT_start_equdes_table; // start address (in PLC memory) of the "Equipment Description" table
int _RT_TableDescriptionEquipments_Fault;

Prefix = "RCA/"+PLC+"/";

dpGet("RCA/"+PLC+fwRackDescriptionEquipments_blob, Desc_Equip);
blob_len = bloblen(Desc_Equip);

//====NEW G.M. 21-4-2009
dynClear(fwRack_New_DeviceList);
dynClear(fwRack_New_DeviceTypeList);
//====

if ((blob_len % 26) != 0) {
  _RT_TableDescriptionEquipments_Fault = _ERROR_TABLE_WRONG_LENGTH;
  dynAppend(exc_info,"CHECK_EQUIP: Equipment description table has a wrong length");
  return;
}


//----------------------------------------------------------------------------------
// reads the Configuration table and the offset of the status info in the PLC memory
//
_RT_ConfigTable = fwAuxil_dpBlobGetValue_int("RCA/"+PLC+fwRackConfigurationTables_blob);
_RT_start_equdes_table = _RT_ConfigTable[_TableConfig_EQU_DESC_ADR];
DebugN("_RT_ConfigTable = "+_RT_ConfigTable);
DebugN("_RT_start_equdes_table = "+_RT_start_equdes_table);

fwRack_Duplicated_Equip_Name_Flag = FALSE;      //5.4.1 G.M.
dynClear(fwRack_Duplicated_Equip_Name_List);    //5.4.1 G.M.

for(;;) {
  ++endlessloop; if (endlessloop > 2000) {dynAppend(exc_info,"ENDLESSLOOP");return;}

  if (pointer >= blob_len) break;
  //======reads data for one equipment
  equip_name = "";
  blobGetValue(Desc_Equip,pointer,buf,14);   pointer = pointer+ 14; sscanf(buf,"%s",equip_name);
  //DebugTN("EQUIP_NAME="+equip_name);
  if (equip_name == "") {
     dynAppend(exc_info, "CHECK_EQUIP: Empty Name for Equipment "+endlessloop);
     pointer = pointer+12;
     continue;
  }
  j=dynContains(equip_names, equip_name);
  if (j > 0) {
     dynAppend(exc_info,"CHECK_EQUIP: Equipment "+endlessloop+" : name "+
               equip_name+" already used at position "+j);
     fwRack_Duplicated_Equip_Name_Flag = TRUE;                    //5.4.1 G.M.
     dynAppend(fwRack_Duplicated_Equip_Name_List,equip_name);     //5.4.1 G.M.
     pointer = pointer+12;                                        //5.4.1 G.M.
     continue;
  }

  dynAppend(equip_names,equip_name);
  
  blobGetValue(Desc_Equip,pointer,parent_address,2,TRUE);  pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,dummy,2,TRUE);           pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,equip_type,2,TRUE);      pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,equip_number,2,TRUE);    pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,status_address,2,TRUE);  pointer = pointer+ 2; 
  blobGetValue(Desc_Equip,pointer,status_length,1);        pointer = pointer+ 1;
  blobGetValue(Desc_Equip,pointer,travee,1);               pointer = pointer+ 1;
  if ((parent_address < _RT_start_equdes_table) || (parent_address >= _RT_start_equdes_table+blob_len)) {
    if (parent_address != 0)
       dynAppend(exc_info,"CHECK_EQUIP: Equipment "+endlessloop+" : parent address "+
                 parent_address+" is outside table");
  }

  if ((parent_address - _RT_start_equdes_table)% 13 != 0) {
    if (parent_address != 0)
     dynAppend(exc_info,"CHECK_EQUIP: Equipment "+endlessloop+" : parent address "+
               parent_address+" does not point to an equipment name");
  }
  j = dynContains(status_addresses,status_address);
  if (j > 0) {
     dynAppend(exc_info,"CHECK_EQUIP: Equipment "+endlessloop+" : status address "+
               status_address+" already used at position "+j);
     continue;     
  }
  dynAppend(status_addresses,status_address);

  equip_name = fwRack_formatString(equip_name);   // REPLACE ILLEGAL CHARACTERS as defined in _fwRackSettings
  dynAppend(fwRack_New_DeviceList,Prefix+equip_name);         // G.M. 21-4-2009
  dynAppend(fwRack_New_DeviceTypeList,"FwRackDevicePDType_"+equip_type);     // G.M. 21-4-2009
}
}
//==========================================================================================
// fwRackTool_ReadDescriptionEquipments reads the Equipment Description table, creates all 
// the device's datapoints, prepare data structures to link the data in the Equipment Status table 
// to the corresponding datapoint elements in the device's datapoints
//
void fwRackTool_ReadDescriptionEquipments(string PLC, dyn_string &exc_info)
{
blob Desc_Equip;
int blob_len;
int pointer;
int parent_address;
int status_address;
int equip_number;
int equip_type;
int status_length,travee;
int dummy;
string buf, equip_name,parent_name;
string plc_equip_name; 
dyn_string equip_names;

dyn_string event_list;
dyn_string event_names_list;
dyn_string event_pos;
int pos;

int i;
int offset;
int max_add;

string alert_text;

int endlessloop;
string Prefix, Pre_fix, Prefix_minus1;

dyn_int _RT_ConfigTable;
int _RT_start_status_table; // start address (in PLC memory) of the "Equipment Status" table
int _RT_start_equdes_table; // start address (in PLC memory) of the "Equipment Description" table

dyn_string  _RT_status_dpe, _RT_status_dpe_invalid;
dyn_int     _RT_status_address_word, _RT_status_address_bit, _RT_first_index_in_status;
dyn_bool    _RT_current_status, _RT_status_invalid;

int _RT_TableDescriptionEquipments_Fault;  // TO DO : should correspond to a datapoint

//------------------------------------
// VARIABLES FOR LOGICAL TREE CREATION
dyn_string node_names,node_parents;
dyn_bool is_node_created;
dyn_string created_nodes;
int n_created;

dyn_string exc_infonode, exc_info2;
int status;

int k,n;

      string dpealert,sysnam;
      int al_i;


if (PLC == "") return;

fwAuxil_createDynIntDptype();  // creates dpType FyDynInt if it does not exist

fwRackTool_ReadExcelGlobals(exc_info2);  // for when it is not called together with ReadDescriptionTypes

Prefix = "RCA/"+PLC+"/";
Pre_fix = "RCA_"+PLC+"_";
Prefix_minus1 = "RCA/"+PLC;

//-------------------------------------------------------------------------
// Reads the table describing the equipments defined on this PLC. For every equipment,
// it creates a datapoint of the corresponding type
//
dpGet("RCA/"+PLC+fwRackDescriptionEquipments_blob, Desc_Equip);
blob_len = bloblen(Desc_Equip);

if ((blob_len % 26) != 0) {
  _RT_TableDescriptionEquipments_Fault = _ERROR_TABLE_WRONG_LENGTH;
  dynAppend(exc_info,"Equipment description table has a wrong length");
  return;
}

//----------------------------------------------------------------------------------
// reads the Configuration table and the offset of the status info in the PLC memory
//
_RT_ConfigTable = fwAuxil_dpBlobGetValue_int("RCA/"+PLC+fwRackConfigurationTables_blob);
_RT_start_status_table = _RT_ConfigTable[_TableConfig_EQU_STATUS_ADR];
_RT_start_equdes_table = _RT_ConfigTable[_TableConfig_EQU_DESC_ADR];

//----------------------------------
// read and process the entire blob
for(;;) {
  ++endlessloop; if (endlessloop > 2000) {dynAppend(exc_info,"ENDLESSLOOP");return;}

  if (pointer >= blob_len) break;
  //======reads data for one equipment
  equip_name = ""; //NEW G.M. 13-5-2008
  blobGetValue(Desc_Equip,pointer,buf,14);   pointer = pointer+ 14; sscanf(buf,"%s",equip_name);
  //DebugTN("EQUIP_NAME="+equip_name);
  if (equip_name == "") {DebugTN("empty name");pointer = pointer+ 12;continue;} //NEW G.M. 13-5-2008
  plc_equip_name = equip_name;  // NEW G.M. 16-5-2008 save name with dots, 
                                //to be used when sending commands to the PLC

  equip_name = fwRack_formatString(equip_name);  // REPLACE ILLEGAL CHARACTERS as defined in _fwRackSettings

  blobGetValue(Desc_Equip,pointer,parent_address,2,TRUE);  pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,dummy,2,TRUE);           pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,equip_type,2,TRUE);      pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,equip_number,2,TRUE);    pointer = pointer+ 2;
  blobGetValue(Desc_Equip,pointer,status_address,2,TRUE);  pointer = pointer+ 2; 
  blobGetValue(Desc_Equip,pointer,status_length,1);        pointer = pointer+ 1;
  blobGetValue(Desc_Equip,pointer,travee,1);               pointer = pointer+ 1;
  
  //========= read features of this equipment's type
  dpGet("FwRackDevicePDType_"+equip_type+"_Events.list",event_list);
  dpGet("FwRackDevicePDType_"+equip_type+"_EventNames.list",event_names_list);
  dpGet("FwRackDevicePDType_"+equip_type+"_EventPos.list",event_pos);
  
  //======creates datapoint
  equip_name = Prefix+equip_name;
  dynAppend(equip_names,equip_name);
  if (dpExists(equip_name)==FALSE) dpCreate(equip_name,"FwRackDevicePDType_"+equip_type);
  else if (dpTypeName(equip_name) != "FwRackDevicePDType_"+equip_type) {
           dynAppend(exc_info,equip_name + "already defined but with another dptype");
           return; 
           
  }
  status=dpSetAlias(equip_name+".",equip_name);if (status == -1) DebugTN("faulty setAlias for "+equip_name);
  //======sets dp elements
  dpSetWait(equip_name+".NameInPlc",plc_equip_name); //NEW G.M. 16-5-2008
  //=== parent
  parent_name = "";
  if (parent_address >=_RT_start_equdes_table) {
     blobGetValue(Desc_Equip,2*(parent_address-_RT_start_equdes_table),buf,14); 
     sscanf(buf,"%s",parent_name);

     parent_name = fwRack_formatString(parent_name);   // REPLACE ILLEGAL CHARACTERS as defined in _fwRackSettings
     parent_name = Prefix+parent_name;
  }
  dpSetWait(equip_name+".ParentID",parent_name);
  //=== property descriptions and alerts
  for(k=1;k<=dynlen(event_list);k++) {
    n=dynContains(EXCEL_code_list,event_list[k]);
    //DebugTN(equip_name,"k="+k,"event_list[k]="+event_list[k],"n="+n);
    if (n < 1) continue;
    //----set dpe comment
    if(dpExists(equip_name+"."+EXCEL_dpe_list[n]))
      dpSetDescription(equip_name+"."+EXCEL_dpe_list[n],EXCEL_dpe_list[n]+":"+EXCEL_text_list[n]);
    // set alert if defined
    
    
    if (EXCEL_has_alert_list[n] == "1") {

      if (_RT_DebugLevel > 1) DebugTN("-----CREATE ALERT FOR "+equip_name+"."+EXCEL_dpe_list[n]);
      alert_text = EXCEL_alert_text_list[n]; 
      if (alert_text == "") alert_text = "Not Ok"; //G.M. 29-4-2008 5-6-2008
      if (EXCEL_alert_value_list[n] == "1") {  //G.M. 25-6-2008
          fwAlertConfig_set(equip_name+"."+EXCEL_dpe_list[n],
                            DPCONFIG_ALERT_BINARYSIGNAL,
                            makeDynString("Ok",alert_text),              // text
                            makeDynFloat(0),                             // limits, not used
                            makeDynString("",EXCEL_alert_list[n]+"."),   // alert class
                            makeDynString(""),                           // summaryDpeList
                            "",                                          // alert panel
                            makeDynString(""),                           // alert panel params
                            "alertHelp",
                            exc_info2);
      if (dynlen(exc_info2)>0) DebugTN(EXCEL_alert_list[n]);
      } else {
          fwAlertConfig_set(equip_name+"."+EXCEL_dpe_list[n],
                            DPCONFIG_ALERT_BINARYSIGNAL,
                            makeDynString(alert_text,"Ok"),         // text
                            makeDynFloat(0),                      // limits, not used
                            makeDynString(EXCEL_alert_list[n]+".",""),   // alert class
                            makeDynString(""),                    // summaryDpeList
                            "",                                   // alert panel
                            makeDynString(""),                    // alert panel params
                            "alertHelp",
                            exc_info2);
      if (dynlen(exc_info2)>0) DebugTN(EXCEL_alert_list[n]);
      }
    }
    //----------create archive config if required  (G.M. 29-4-2008)
    
    if (EXCEL_has_archive_list[n] == "1") {
                fwArchive_set(equip_name+"."+EXCEL_dpe_list[n],
                              L_archive_class, 
                              L_archive_type,
                              0,0,0,exc_info2);
                if (dynlen(exc_info2)>0) DebugTN(EXCEL_alert_list[n]);
    }
    
  }
  
  
  //-------------------------------------------
  // FILL LISTS TO LATER BUILD LOGICAL TREE
  //
  dynAppend(node_names,equip_name);
  if (parent_name == "") parent_name = Prefix_minus1; 
    dynAppend(node_parents,parent_name);
    
  dynAppend(is_node_created, FALSE);
  
  //======updates "status_dpe..." and "status_address..." arrays
  offset = status_address - _RT_start_status_table;
  dpSetWait(equip_name+".StatusAddress",offset);
  //DebugTN("equip_name="+equip_name,"status_offset = "+offset,"dynlen(event_list)="+dynlen(event_list));
  for(i=1;i<=dynlen(event_list);i++) {
     pos = event_pos[i];
     dynAppend(_RT_status_dpe,equip_name+"."+event_names_list[i]);
     dynAppend(_RT_status_address_word,(offset+(pos)/16) );  // see doc. at the beginning of this file (was i-1)
     dynAppend(_RT_status_address_bit, (pos)%16 );  // see doc. at the beginning of this file (was i-1)
     //DebugTN(equip_name+"."+event_names_list[i],(offset+(pos)/16),(pos)%16);
     dynAppend(_RT_status_dpe_invalid,equip_name+"."+event_names_list[i]+":_original.._exp_inv");
  }
  
} // end of blob table processing

dpSetWait(PLC+"_ConfigurationDpList.list",equip_names);
//--------------------------------------------------------------------------------
// builds the array _RT_first_index_in_status, to link the position of a word in the 
// equipment status blob with the position of a dpe name in the _RT_status_dpe array
//
dynClear(_RT_first_index_in_status);
max_add = dynMax(_RT_status_address_word);

//--------NEW 14-5 G.M. find type of last device, and reserve space for undefined trailing properties 
i=dynContains(_RT_status_address_word,max_add);
string max_add_dp = dpSubStr(_RT_status_dpe[i],DPSUB_DP);
string max_add_type = dpTypeName(max_add_dp);
int max_add_status_address_len;
dpGet(max_add_type+"_StatusAddressLen.count",max_add_status_address_len);
//DebugTN("---max_add_type="+max_add_type, "max_add_dp="+max_add_dp, 
//        "max_add="+max_add, "i="+i,"len="+max_add_status_address_len);
//DebugTN(_RT_status_address_word);
for(i=0;i<=max_add +(max_add_status_address_len-1);i++) dynAppend(_RT_first_index_in_status,-1);
//---END OF NEW
_RT_first_index_in_status[(_RT_status_address_word[1])+1] = 1; // introduced because of the modif in the loop
for(i=2;i<=dynlen(_RT_status_address_word);i++) {
    //if (_RT_status_address_bit[i] == 0) _RT_first_index_in_status[(_RT_status_address_word[i])+1] = i;
    if (_RT_status_address_word[i] != _RT_status_address_word[i-1])
                                  _RT_first_index_in_status[(_RT_status_address_word[i])+1] = i;
}
//
//-------------------------------------------------------------------------
// writes everything to datapoints, to store it permanently
//
if (dpExists(Pre_fix+"FWRack_StatusDpe")==FALSE) dpCreate(Pre_fix+"FWRack_StatusDpe","FwList");
if (dpExists(Pre_fix+"FWRack_StatusDpeInvalid")==FALSE) dpCreate(Pre_fix+"FWRack_StatusDpeInvalid","FwList");
dpSetWait(Pre_fix+"FWRack_StatusDpe.list",_RT_status_dpe);
dpSetWait(Pre_fix+"FWRack_StatusDpeInvalid.list",_RT_status_dpe_invalid);

if (dpExists(Pre_fix+"FWRack_StatusAddressWord")==FALSE) dpCreate(Pre_fix+"FWRack_StatusAddressWord","FwDynInt");
if (dpExists(Pre_fix+"FWRack_StatusAddressBit")==FALSE) dpCreate(Pre_fix+"FWRack_StatusAddressBit","FwDynInt");
if (dpExists(Pre_fix+"FWRack_FirstIndexInStatus")==FALSE) dpCreate(Pre_fix+"FWRack_FirstIndexInStatus","FwDynInt");
dpSetWait(Pre_fix+"FWRack_StatusAddressWord.iarr",_RT_status_address_word);
dpSetWait(Pre_fix+"FWRack_StatusAddressBit.iarr",_RT_status_address_bit);
dpSetWait(Pre_fix+"FWRack_FirstIndexInStatus.iarr",_RT_first_index_in_status);

_RT_TableDescriptionEquipments_Fault = _OK;  //TO_DO : write somewhere that the reconfiguration is complete




//------------------------------------------------------------------
// CREATION OF THE LOGICAL TREE
//
fwNode_initialize();
if (_RT_DebugLevel > 1) {
   DebugTN("----------------------------lists to build logical tree------");
   for(i=1;i<=dynlen(node_names);i++) DebugTN(node_names[i], node_parents[i]);
   DebugTN("-------------------------------------------------------------");
}

// create root logical node node=Prefix,parent=""  REPLACE BY FW FUNCTION
//dpSetAlias(Prefix_minus1+".",Prefix_minus1 ); already created before ?


if(dpAliasToName("RCA")=="")  fwNode_createLogical("RCA","","","",exc_infonode);

dpSetAlias(Prefix_minus1 + ".",Prefix_minus1);
if (dynlen(exc_infonode) > 0) DebugTN("Creating node RCA:",exc_infonode);
dynAppend(created_nodes,Prefix_minus1);

//-----------------------------------------------
// loop until all nodes are created. Several steps might be needed, as a parent could
// be defined after a child in the DescriptionsEquipments table
//
n_created = 0;
endlessloop = 0;  // watchdog

for(;;) {  
  
    ++ endlessloop; if (endlessloop > 100000) {dynAppend(exc_info,"LOGICAL TREE ENDLESSLOOP");return;}
    
    if (n_created == dynlen(node_names)) break;
    for(i=1;i<=dynlen(node_names);i++) {
        if (is_node_created[i]) continue; // node already created
        if (dynContains(created_nodes,node_parents[i]) < 1) continue; // parent not yet created

        //DebugTN("will create "+node_names[i]+"  with parent "+node_parents[i]);
        dynClear(exc_infonode);
        //fwNode_createLogical(node_names[i],node_parents[i],"","",exc_infonode);
        //DebugTN("---------------------------------New logical View---------------------------");
        string name;
        fwDevice_getName(node_names[i],name,exc_infonode);        
        dpSetAlias(node_names[i]+".",dpGetAlias(node_parents[i]+".") +"/" + name );
        //DebugTN("New logical node alias: " + dpGetAlias(node_parents[i]+".") +"/" + name );
        //DebugTN("-------------------------------------------------------------");
        if (dynlen(exc_infonode) > 0) DebugTN(exc_infonode);
        ++n_created;
        is_node_created[i] = TRUE;
        dynAppend(created_nodes,node_names[i]);
    }
   
}
//DebugTN("----------------------------created nodes -------------------");
//for(i=1;i<=dynlen(created_nodes);i++) DebugTN(created_nodes[i]);
//DebugTN("-------------------------------------------------------------");

}


//======================================================================================
// this function calls the two previous functions to perform a complete reconfiguration
//======================================================================================
bool fwRackTool_ReconfigurePLC(string PLC,dyn_string &exc_info)
{
int check_flag;

dyn_string check_exc_info;
dyn_string check_deviceTypeModify_info;
dyn_string dpid_list;  // G.M. 21-4-2009
int i,elid;            // G.M. 21-4-2009
unsigned dpid;         // G.M. 21-4-2009
dyn_string exc_info2;

     DebugTN(L_Version);  
     
//------------------------------------------------------------------
// SOME PRELIMINARY OPERATIONS, NOT DEPENDING ON THE PLC
//
//   create some useful datapoint types if they do not exist
//
     fwAuxil_createIntParamDptype();
     fwAuxil_createParamDptype();
     fwAuxil_createDynIntDptype();
     fwAuxil_createListDptype();
     fwAuxil_createBoolDptype();
//
//   read the values of some options from datapoints (create them if they do not exist yet)
//     
//=== new G.M. 4-7-2008 flag to let the user configure even if configuration has errors
     if (dpExists("FwRack_OnlyUseCorrectConfig")==FALSE) dpCreate("FwRack_OnlyUseCorrectConfig","FwIntParam");
     dpGet("FwRack_OnlyUseCorrectConfig.count",only_use_good_configuration);
//
//=== new G.M. 28-5-2014 specific flag to allow for modifications in datatypes structure
     if (dpExists("FwRack_AllowTypesModif")==FALSE) dpCreate("FwRack_AllowTypesModif","FwIntParam");  //5.4.1
     dpGet("FwRack_AllowTypesModif.count",allow_types_modif);                                        // 5.4.1
//
//=== new G.M. 21-4-2009 flag to let the system delete the old configuration (if -1)
//  this option is especially useful in building 4, while it should not be used in operation 
     if (dpExists("FwRack_PreserveOldConfig")==FALSE) dpCreate("FwRack_PreserveOldConfig","FwIntParam");
     dpGet("FwRack_PreserveOldConfig.count",preserve_old_configuration);
//
// END OF PRELIMINARY OPERATIONS
//--------------------------------------------------------------------
// BEGINNING OF PART DEPENDING ON THE PLC
//
     DebugTN("------------------------------------>>>>>>>>>" + PLC);
     if(dpExists("RCA_"+ PLC+"_EquipmentStatus_Old")) dpDelete("RCA_"+ PLC+"_EquipmentStatus_Old");
     
     if (dpExists(PLC+"_ConfigurationDpList")==FALSE) dpCreate(PLC+"_ConfigurationDpList","FwList");
     if (dpExists(PLC+"_ConfigurationDpTypeList")==FALSE) dpCreate(PLC+"_ConfigurationDpTypeList","FwList");

     
//=== copy previous list of device names and dpids to _Old datapoint (for diagnostic)
     if (dpExists(PLC+"_DpId")) {
            dpGet(PLC+"_DpId.list",dpid_list);
            if (dpExists(PLC+"_DpId_Old")==FALSE) dpCreate(PLC+"_DpId_Old","FwList");
            dpSetWait(PLC+"_DpId_Old.list",dpid_list);
     }


//=== create dp to store status after configuration (0=OK, 1=BAD);
     if (dpExists( "RCA_"+ PLC+"_ConfigError" )==FALSE) {
                dpCreate("RCA_"+ PLC+"_ConfigError" ,"FwBoolValue");
                          fwAlertConfig_set("RCA_"+ PLC+"_ConfigError.Value",
                            DPCONFIG_ALERT_BINARYSIGNAL,
                            makeDynString("Ok","RCA_"+PLC+" ConfigurationError"),         // text
                            makeDynFloat(0),                      // limits, not used
                            makeDynString("_fwErrorNack"+".",""),   // alert class
                            makeDynString(""),                    // summaryDpeList
                            "",                                   // alert panel
                            makeDynString(""),                    // alert panel params
                            "alertHelp",
                            exc_info2);
     }


//=== preprocess new configuration (types and devices), to find errors G.M. 30-6-2008
//=== this step checks the consistency of the tables loaded in the PLC, 
//=== without performing the new configuration
     fwRackTool_CheckDescriptionTypes(PLC,check_exc_info,check_deviceTypeModify_info);
     if (dynlen(check_exc_info)> 0) {
           DebugTN(check_exc_info);check_flag = 1; 
           dpSetWait( "RCA_"+ PLC+"_ConfigError.Value" ,TRUE); 
     }
     dynClear(check_exc_info);
     fwRackTool_CheckDescriptionEquipments(PLC,check_exc_info);
     if (dynlen(check_exc_info)> 0) {
           DebugTN(check_exc_info);check_flag = 1; 
           dpSetWait( "RCA_"+ PLC+"_ConfigError.Value" ,TRUE); 
     }          
//=== in case of errors, process configuration only if only_use_good_configuration flag is negative
     if (check_flag > 0) {
       if (only_use_good_configuration >= 0) { // G.M. 21-4-2009  change from >0 to >=0 (default behaviour)
         DebugTN("---> CORRECT PLC CONFIGURATION and try again"); return false; 
       } else {
         DebugTN("---> BEWARE: the PLC CONFIGURATION CONTAINS ERRORS");
         DebugTN("---> I configure anyway, but you should fix the errors");
       }
     }

     
//=== here we start the configuration process. First we clean the old configuration, then we create the new one
     
//=== clean old configuration
     fwRackTool_DeconfigurePLC(PLC,exc_info); //G.M. 21-4-2009 moved it here, after the new tables were checked
     
//=== create new configuration (types and devices)
//=== create types
     fwRackTool_ReadDescriptionTypes(PLC,exc_info);
     if (dynlen(exc_info)>0) {
         DebugTN(exc_info);
         dpSetWait( "RCA_"+ PLC+"_ConfigError.Value" ,TRUE);
         return false;
     }

//=== create devices
     fwRackTool_ReadDescriptionEquipments(PLC,exc_info);
     if (dynlen(exc_info)>0) {
         DebugTN(exc_info);
         dpSetWait( "RCA_"+ PLC+"_ConfigError.Value" ,TRUE);
         return false;
     }
     
//=== NEW G.M. 21-4-2009 produce and save the new dpid list (device name + dpid)
     if (dpExists(PLC+"_ConfigurationDpList")) {  // if this file does not exist, configuration did not work
         dpGet(PLC+"_ConfigurationDpList.list",dpid_list);
         for(i=1;i<=dynlen(dpid_list);i++) {
           dpGetId(dpid_list[i],dpid,elid);
           dpid_list[i] = dpid_list[i]+"  "+dpid;
         }
         if (dpExists(PLC+"_DpId")==FALSE) dpCreate(PLC+"_DpId","FwList");
         dpSetWait(PLC+"_DpId.list",dpid_list);
     }
     
     
//===
// TO_DO : write somewhere that reconfiguration is complete
     DebugTN("Reconfigure is finished"); //G.M. 21-4-2009
     if (check_flag == 0){
       dpSetWait( "RCA_"+ PLC+"_ConfigError.Value" ,FALSE); // G.M. 1-8-2011
     }
     fwRackTool_SaveTables("RCA/"+PLC); //G.M. 2019-05
     return true;
}


//======================================================================================
// this function deletes all the datapoints related to the configuration of a PLC
//======================================================================================
void fwRackTool_DeconfigurePLC(string PLC,dyn_string &exc_info)
{
  dyn_string dp_list;
  dyn_string dptype_list;
  dyn_string names;
  int i,j;
  string dummy; //NEW G.M. 13-5-2008
  dyn_string types;
  
  DebugTN("Deconfigure "+PLC);
  //types = dpTypes("fwRack*");   WRONG G.M. 2-4-2009
  types = dpTypes("FwRack*");    // RIGHT G.M. 2-4-2009
  if (dpExists(PLC+"_ConfigurationDpList.list")) dpGet(PLC+"_ConfigurationDpList.list",dp_list);
  if (dpExists(PLC+"_ConfigurationDpTypeList.list")) dpGet(PLC+"_ConfigurationDpTypeList.list",dptype_list);
  //DebugTN("dp_list="+dp_list);
  //DebugTN("dptype_list="+dptype_list);
  
  //====By default it will not delete devices unless they are of a different type.    
  //DebugTN("preserve old configuration = "+preserve_old_configuration);    
  for(i=1;i<=dynlen(dp_list);i++) {
    if (dpExists(dp_list[i])) {  // if device already existed,
        if (preserve_old_configuration == -1) {      // delete it if you do not want to keep it
                         dpDelete(dp_list[i]);
                         continue;
        } 
        j=dynContains(fwRack_New_DeviceList,dp_list[i]);    // find its position in the new list
        if (j <=0) {dpDelete(dp_list[i]);continue;}  // not in the new list? delete it
        if (fwRack_New_DeviceTypeList[j] != dpTypeName(dp_list[i])) { // if type is different, delete it
                         dpDelete(dp_list[i]);
                         continue;
        } 
    }
  }
  //==== if a type is now empty you can delete it; it will be recreated if required                                
  for(i=1;i<=dynlen(dptype_list);i++) {
             dummy = "dummy_"+dptype_list[i];  //NEW G.M. 13-5-2008
             if (dpExists(dummy)) dpDelete(dummy);  //NEW G.M. 13-5-2008
             names = dpNames("*",dptype_list[i]);                       
             //for(j=1;j<=dynlen(names);j++) dpDelete(names[j]); //NO: otherwise it deletes dp from other PLCs
             if (dynlen(names)==0) { //NEW G.M. 13-5-2008
                  if (dynContains(types,dptype_list[i]) > 0) dpTypeDelete(dptype_list[i]);
                  if (dpExists(dptype_list[i]+"Info")) dpDelete(dptype_list[i]+"Info"); //G.M. 30-6-2008
             }
  }
  if (dpExists("RCA/"+PLC+"/EquipmentStatus_Old") ) dpDelete("RCA/"+PLC+"/EquipmentStatus_Old");
}
    
//======================================================================================
// this function reads the table containing all the equipment status bits, and dispatches
// these boolean values to the Properties dpes of the different equipments
//======================================================================================
void fwRackTool_ReadEquipmentStatus(string PLCdp, dyn_string &exc_info)
{
  dyn_int Status_New_From_Blob;
  dyn_int Status_Old;
  bool compare;
  int i,j,first_index;
  dyn_bool L_new_status;
  bool val;
  int k;
  dyn_string dpe_list;
  dyn_bool   val_list;
  string Prefix, Pre_fix;
  dyn_string  _RT_status_dpe, _RT_status_dpe_invalid;
  dyn_int     _RT_status_address_word, _RT_status_address_bit, _RT_first_index_in_status;
  dyn_bool    _RT_current_status, _RT_status_invalid;
  int         _RT_status_dpe_len;
  string PLC;
  dyn_string dsExceptions;  
  
  
  //DebugTN("-------->" + PLCdp);  
  PLCdp = dpSubStr(PLCdp,DPSUB_DP);
  //DebugTN("-------->" + PLCdp);  
  fwDevice_getName(PLCdp,PLC,dsExceptions);

  DebugTN("read Equipment Status: PLC="+PLC);
  if (PLC == "") return;

  //DebugTN("Read Equip Status of "+PLC);
  Prefix = "RCA/"+PLC+"/";
  Pre_fix = "RCA_"+PLC+"_";
  if (dpExists(Pre_fix+"EquipmentStatus_Old")==FALSE) dpCreate(Pre_fix+"EquipmentStatus_Old","FwDynInt");
  dpGet(Pre_fix+"FWRack_StatusDpe.list",_RT_status_dpe); 
  _RT_status_dpe_len = dynlen(_RT_status_dpe);
  dpGet(_RT_status_dpe,_RT_current_status); 

  dpGet(Pre_fix+"FWRack_StatusAddressWord.iarr",_RT_status_address_word);
  dpGet(Pre_fix+"FWRack_StatusAddressBit.iarr",_RT_status_address_bit);
  dpGet(Pre_fix+"FWRack_FirstIndexInStatus.iarr",_RT_first_index_in_status);
 
  L_new_status = _RT_current_status; // only to give the right dimension to L_new_status
  Status_New_From_Blob = fwAuxil_dpBlobGetValue_int("RCA/"+PLC+fwRackEquipmentStatus_blob);
  dpGet(Pre_fix+"EquipmentStatus_Old.iarr",Status_Old);
  
  if (dynlen(Status_Old) == dynlen(Status_New_From_Blob)) compare=TRUE;  //first time, or config change
  for(i=1;i<=dynlen(Status_New_From_Blob);i++) {
    if (compare) if (Status_New_From_Blob[i]==Status_Old[i])  continue;
    
    first_index = _RT_first_index_in_status[i]; 
    //if (first_index > 0) DebugTN("i="+i, "first_index="+first_index,
    //                             "RT_status_dpe[first_index]="+_RT_status_dpe[first_index]);
    //else DebugTN("i="+i, "first_index="+first_index);
    if (first_index == -1) continue; // this word is not used
    for(j=first_index;;j++) {
        if (j > _RT_status_dpe_len) break;                // end of array
        if (_RT_status_address_word[j] != i-1) break;     // status for index j is in another word           
        if (compare) {      //only update changed values
           k = 1<<_RT_status_address_bit[j];
           if ((Status_New_From_Blob[i] & k) != (Status_Old[i] & k)) {
              val = (Status_New_From_Blob[i] & k) >> _RT_status_address_bit[j];
              dynAppend(dpe_list,_RT_status_dpe[j]);
              dynAppend(val_list,val);
           }
        } else {     //update all dpes
           k = 1<<_RT_status_address_bit[j];          
           L_new_status[j]= (Status_New_From_Blob[i] & k) >> _RT_status_address_bit[j];
        }
    }
  }
  if (compare) {
         dpSetWait(dpe_list,val_list);  // only update modified values
         if (_RT_DebugLevel > 0) {
             DebugTN("dpe_list, val list");
             for(i=1;i<=dynlen(dpe_list);i++) DebugTN(dpe_list[i]+"   "+val_list[i]);
         }

  } else {
         dpSetWait(_RT_status_dpe,L_new_status);  // update all values
         if (_RT_DebugLevel > 0) {
              //DebugTN("_RT_status_dpe,L_new_status");
              //for(i=1;i<=dynlen(_RT_status_dpe);i++) DebugTN(_RT_status_dpe[i]+"   "+L_new_status[i]);
         }
  }
  
  dpSetWait(Pre_fix+"EquipmentStatus_Old.iarr",Status_New_From_Blob); // previous status = new status
}


//=========================================================================
// set/reset the invalid bit for all the properties of all equipment of a given PLC
// (for example while configuration is changing)
//
void fwRackTool_SetInvalidBit(string PLC,bool flag, dyn_string &exc_info)
{
  dyn_bool values;
  dyn_string dpe_list;
  string dp;
  int i,j;

  dynAppend(exc_info,"Set Invalid Bit: PLC="+PLC+"  flag="+flag);
  dp = "RCA/"+PLC+"/FWRack_StatusDpeInvalid";
  if (dpExists(dp) == FALSE) {dynAppend(exc_info,dp+" does not exist");return;}
  dpGet(dp+".list",dpe_list);  
  if (dynlen(dpe_list) == 0) {dynAppend(exc_info,"invalid_dpe list empty");return;}
  
//dpGet(dpe_list,values);DebugTN(values);
//dynClear(values);

  for(i=1;i<=dynlen(dpe_list);i++) dynAppend(values,flag);
  j=dpSetWait(dpe_list,values);
  if (j==0) dynClear(exc_info);
  else {dynAppend(exc_info,"dpSetWait failed ("+j+")");DebugTN(dpe_list,values);}
  return;
}

//========================== NEW G.M. 2019-05 =============================
void fwRackTool_SaveTables(string dp)
{
  blob myblob;

  dpGet(dp + ".Tables.EquipmentDescription",myblob);
  dpSetWait(dp + ".SavedTables.EquipmentDescription",myblob);
  dpGet(dp + ".Tables.EquipmentConfiguration",myblob);
  dpSetWait(dp + ".SavedTables.EquipmentConfiguration",myblob);
  dpGet(dp + ".Tables.CommandDescription",myblob);
  dpSetWait(dp + ".SavedTables.CommandDescription",myblob);
  dpGet(dp + ".Tables.Configuration",myblob);
  dpSetWait(dp + ".SavedTables.Configuration",myblob);
  dpGet(dp + ".Tables.Identification",myblob);
  dpSetWait(dp + ".SavedTables.Identification",myblob);
  fwRackTool_CompareTablesWithLastUsed(dp);
}

//========================== NEW G.M. 2019-05 =============================
bool fwRackTool_CompareTablesWithLastUsed(string dp)
{
  bool ok = TRUE;

  ok = ok && fwRackTool_CompareTwoBlobDpes(dp + ".Tables.Configuration",dp + ".SavedTables.Configuration");
  ok = ok && fwRackTool_CompareTwoBlobDpes(dp + ".Tables.EquipmentDescription",dp + ".SavedTables.EquipmentDescription");
  ok = ok && fwRackTool_CompareTwoBlobDpes(dp + ".Tables.EquipmentConfiguration",dp + ".SavedTables.EquipmentConfiguration");
  ok = ok && fwRackTool_CompareTwoBlobDpes(dp + ".Tables.Identification",dp + ".SavedTables.Identification");
  ok = ok && fwRackTool_CompareTwoBlobDpes(dp + ".Tables.CommandDescription",dp + ".SavedTables.CommandDescription");

  dpSetWait(dp+".Status.ConfigurationTablesMismatch",!ok);
  return(ok);
}

bool fwRackTool_CompareConfigurationTable(string dp)
{
  bool ok = fwRackTool_CompareTwoBlobDpes(dp + ".Tables.Configuration",dp + ".SavedTables.Configuration");

  dpSetWait(dp+".Status.ConfigurationTablesMismatch",!ok);
  return(ok);
}

//======================== NEW G.M., M.S. 2019-05 =========================
bool fwRackTool_CompareTwoBlobDpes(string dpe1, string dpe2, bool skipIfSecondBlobEmpty = true) {
  blob blob1, blob2;

  dpGet(dpe1, blob1);
  dpGet(dpe2, blob2);
  if(blob1 == blob2 || (skipIfSecondBlobEmpty && bloblen(blob2) == 0)){
    return(TRUE);
  }
  DebugN("mismatch between "+dpe1+" and "+dpe2);
  return(FALSE);
}
