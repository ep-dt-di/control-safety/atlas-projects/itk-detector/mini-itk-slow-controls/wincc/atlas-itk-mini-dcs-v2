#uses "fwRack/fwRackDeprecated.ctl"

/*
This library is used by the PREMIUM Application.
*/
//=========================================================
//          HISTORY OF MODIFICATIONS
//
//          ORIGINAL AUTHOR: Roland Gierlich   PH / CMI     October 2004
//
//          18-3-2013 G.M. cosmetic changes (is it still used?)
//
//
//          2016-07-25 saved as 3.14 G.M.
//

//==========================================================
int BlobToInt(blob &Input, int Position, int Length)
{
int Output;

blobGetValue(Input, Position*2, Output, Length*2, TRUE);
return Output;
}

//==========================================================
/*
Returns TRUE if the hexadecimal variables are identical.
*/
bool CompareBlobs(blob &a, blob &b)
{
string sa, sb;
sa = a;
sb = b;

if (sa == sb) return TRUE;
return FALSE;
}

//==========================================================
unsigned BlobToUnsigned(blob &Input, int Position, int Length)
{
unsigned Output;

blobGetValue(Input, Position*2, Output, Length*2, TRUE);
return Output;
}

//==========================================================
string BlobToString(blob &Input, int Position, int Length)
{
string Answer;
//DebugTN(Input, Position*2, Answer, Length*2, TRUE);
blobGetValue(Input, Position*2, Answer, Length*2, TRUE);
return Answer;
}



//==========================================================
string BlobToPattern(blob &Input, int Position)
{
unsigned 	Intermed;
bit32 		Pattern;
string 		a;
string 		Answer;
int i;

//DebugTN("Problem..." + Input);

if (Input != "")  blobGetValue(Input, Position*2 , Intermed, 2, TRUE);
Pattern = Intermed;
a = Pattern;

for (i = 16; i<32; i++) Answer += a[i];
return Answer;
}


//==========================================================
blob PatternToBlob(string &Pattern)
{
bit32 		a = Pattern;
unsigned 	b = a;
blob 			c;

blobZero(c, 2);
blobSetValue(c, 0, b, 2, TRUE);
return c;
}

//==========================================================
void SetBitInPattern(string &Pattern, bool Bit, int Position)
{
string temp;
int i;

for (i = 0; i < Position; i++) temp += Pattern[i];

if (Bit) temp += "1";
else     temp += "0";
	
for (i = (Position+1); i < 16; i++) temp+=Pattern[i];
Pattern = temp;
}
