#uses "fwSynopticLocator/fwSynopticLocator.ctl"
#uses "fwSynopticLocator/fwSynopticLocatorDeprecated.ctl"

const string DEVICES_DP_PREFIX = "_fwSynopticLocator_Device_";
const string DEVICES_DPT_NAME = "_FwSynLoc_Devices";

const string PANELS_DP_PREFIX = "_fwSynopticLocator_Panel_";
const string PANELS_DPT_NAME = "_FwSynLoc_Panels";



  /**
    
   Creates a Device Representation in the system.
   
   @param deviceName The name of the device that wants to be represented in the system.
   @param panelNames The list of the names of the panels that contains the device named <i>deviceName</i>.
   @pre The representation of the device named <i>deviceName</i> does not exists.
   @post The representation of the device named <i>deviceName</i> exists and has <i>panelNames</i> as assigned panels.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Device_Create(string deviceName, const dyn_string & panelNames) {

    /* Precondition Checking Start */
    string sDeviceNameOrig = deviceName; //need to do this as nameCheck modify the parameters
    if ( nameCheck(sDeviceNameOrig, NAMETYPE_DP) != 0)
    {
    	DebugTN(__FUNCTION__, "The device "+deviceName+" has an invalid name for a DP and will not be listed.");
    	return -1;
    }

    if (dpExists(DEVICES_DP_PREFIX + deviceName)) {
      
      DebugTN("fwSynopticLocator_Data_Device_Create: Precondition violation (The representation of the device named " + deviceName + " should not exist, but it does.");
      
    }
    
    /*  Precondition Checking End  */  
    
    int replyCode = dpCreate(DEVICES_DP_PREFIX + deviceName, DEVICES_DPT_NAME);
    
    replyCode = replyCode || fwSynopticLocator_Data_Device_SetPanels(deviceName, panelNames);
    
    return replyCode;
  
  }

  int fwSynopticLocator_Data_Devices_Create(dyn_string deviceName) {

    int replyCode = 0; 
    /* Precondition Checking Start */
    int nCheckDevices = 0;
    int nDevices = dynlen(deviceName);
    dyn_string deviceCorrect;
    for(int i=1; i<= nDevices; i++)
    {
      string sDeviceNameOrig = deviceName[i]; //need to do this as nameCheck modify the parameters
      if ( nameCheck(sDeviceNameOrig, NAMETYPE_DP) != 0)
      {
        DebugTN(__FUNCTION__, "The device "+deviceName[i]+" has an invalid name for a DP and will not be listed.");
        //return -1;
        replyCode = -1;
      }
      else
      {
        dynAppend(deviceCorrect, deviceName[i]);
        nCheckDevices++;
      }
    }

    if(nCheckDevices != nDevices)
    {
      DebugTN("fwSynopticLocator_Data_Devices_Create: Precondition violation (Some devices have an invalid name for a DP and will not be listed)");
    }

    nCheckDevices = 0;
    int nDeviceCorrect = dynlen(deviceCorrect);
    for(int i=1; i<= nDeviceCorrect; i++)
    {
      if (dpExists(DEVICES_DP_PREFIX + deviceCorrect[i]))
      {
        DebugTN("fwSynopticLocator_Data_Device_Create: Precondition violation (The representation of the device named " + deviceName[i] + " should not exist, but it does)");
      }
      else
      {
        nCheckDevices++;
      }
    }

    if(nCheckDevices != nDeviceCorrect)
    {
      DebugTN("fwSynopticLocator_Data_Devices_Create: Precondition violation (Some devices should not exist, but they do)");
    }
    
    /*  Precondition Checking End  */  

    dyn_dyn_string panelNames;
    
    for(int i=1; i<= nDeviceCorrect; i++)
    {
      replyCode = replyCode || dpCreate(DEVICES_DP_PREFIX + deviceCorrect[i], DEVICES_DPT_NAME);
      panelNames[i] = makeDynString();
    }

    
    replyCode = replyCode || fwSynopticLocator_Data_Devices_SetPanels(deviceCorrect, panelNames);
    
    return replyCode;
  
  }
  
  /**
    
   Obtains the name of the panels in which the device <i>deviceName</i> is contained.
   
   @param deviceName The name of the device.
   @param panelNames It will contain the name of the panels in which the device <i>deviceName</i> is contained.
   @pre The representation of the device named <i>deviceName</i> exists.
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Device_GetPanels(string deviceName, dyn_string & panelNames){

    /* Precondition Checking Start */

    if (!dpExists(DEVICES_DP_PREFIX + deviceName)) {
      
      DebugTN("fwSynopticLocator_Data_Device_GetPanels: Precondition violation (The representation of the device named " + deviceName + " should exist, but it does not.");
      
    }
    
    /*  Precondition Checking End  */    
    
    return dpGet(DEVICES_DP_PREFIX + deviceName + ".:_original.._value", panelNames);
  
  }  


  int fwSynopticLocator_Data_Devices_GetPanels(dyn_string deviceName, dyn_dyn_string & panelNames){

    /* Precondition Checking Start */

    dyn_string deviceNameComplete;
    dyn_string deviceNameExist;
    dyn_dyn_string panelNamesExist;
    int nDevice = dynlen(deviceName);
    for(int i=1; i<= nDevice; i++)
    {
      if (!dpExists(DEVICES_DP_PREFIX + deviceName[i]))
      {
        DebugTN("fwSynopticLocator_Data_Device_GetPanels: Precondition violation (The representation of the device named " + deviceName[i] + " should exist, but it doesn't)");
      }
      else
      {
        dynAppend(deviceNameComplete, DEVICES_DP_PREFIX + deviceName[i] + ".:_original.._value");
        dynAppend(deviceNameExist, deviceName[i]);
        dynAppend(panelNamesExist, makeDynString());  //panelNames in fwSynopticLocator_Data_Devices_GetPanels is empty as INPUT.
      }
    }

    if(dynlen(deviceNameComplete) != nDevice)
    {
      DebugTN("fwSynopticLocator_Data_Device_GetPanels: Precondition violation (Some devices should exist, but they don't)");
      dynClear(deviceName);
      deviceName = deviceNameExist; //update the list of existint devices
      dynClear(panelNames);
      panelNames = panelNamesExist; //update the list of list (we want return the real list of list of panels, not from non existing panels)
    }
    
    /*  Precondition Checking End  */    
    
    return dpGet(deviceNameComplete, panelNames);
  
  }  
  
  /**
    
   Returns the name of every device represented in the system.
   
   @pre --
   @post --
   @return Dyn_string: Name of the devices represented in the system.
   
   */
  dyn_string fwSynopticLocator_Data_Device_GetAll() {

    dyn_string deviceNames = dpNames(DEVICES_DP_PREFIX + "*", DEVICES_DPT_NAME);
    int cutPos;    
    
    int nDevices = dynlen(deviceNames);
    for (int i = 1; i <= nDevices; i++) {
 
      //SYSTEM_NAME + ":" + DEVICES_DP_PREFIX + DEVICE_NAME
 
      cutPos = strpos(deviceNames[i],DEVICES_DP_PREFIX)+strlen(DEVICES_DP_PREFIX);
      
      //cutPos !'!
      //SYSTEM_NAME + ":" + DEVICES_DP_PREFIX !'! DEVICE_NAME
      
      deviceNames[i] = substr(deviceNames[i], cutPos, strlen(deviceNames[i]));

      //DEVICE_NAME
      
    }  
    
    return deviceNames;
  
  }  

  /**
    
   Returns the name of the non existent devices:
   Get first all the panel names, get the devices info for each panel,
   and match for each device, if this devices exists really.
   Looking for an Alias in all DPEs which match with all the devices
   from each panel.
   If some devices doesn't match, append to the list.
   
   @pre --
   @post --
   @return Dyn_string: Name of the devices non existent
   
   */
  dyn_string fwSynopticLocator_Data_Device_GetAllNonExistent() {

    dyn_string deviceNames;

    dyn_string devices = fwSynopticLocator_Data_Device_GetAll();  
    int nDevices = dynlen(devices);
    for(int j=1; j<=nDevices;j++)
    {
      //special instruction to search the label _tagalias_ due we need the pipe, |
      string sDeviceAlias = devices[j];
      strreplace( sDeviceAlias, "_tagalias_", "|");
      if(dpExists(dpAliasToName(sDeviceAlias)) == false)
      {
        //But to append again, we want the _tagalias_ structure.
        dynAppend(deviceNames, devices[j]);
      }
    }
    
    return deviceNames;
  
  } 





  
  
  /**
    
   Deletes (clear) every device representation in the system.
   
   @pre --
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Device_DeleteAll(string progressFunction) {

    dyn_string devices = dpNames(DEVICES_DP_PREFIX + "*", DEVICES_DPT_NAME);
    int replyCode = 0;    
    
    int nDevices = dynlen(devices);
    float progressValue;
    for (int i = 1; i <= nDevices; i++) {
      
      progressValue = 50 + (i*50/nDevices);
      execScript(progressFunction, makeDynString(), progressValue);
      
      replyCode = replyCode || dpDelete(devices[i]);
      
    }  
    
    execScript(progressFunction, makeDynString(), 100); // In case it does not enter the for
    
    return replyCode;
  
  }  
  
  
   /**
    
   Sets the panels of the device with name <i>deviceName</i> to <i>panelNames</i>
   
   @param deviceName The name of the device representation which panels want to be setted.
   @param panelNames The list of the names of the panels that contains the device <i>deviceName</i>.
   @pre The representation of the device named <i>deviceName</i> does exists.
   @post The representation of the device named <i>deviceName</i> now has <i>panelNames</i> as its list of containing panels.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Device_SetPanels(string deviceName, const dyn_string & panelNames){
    
    /* Precondition Checking Start */
    
    if (!dpExists(DEVICES_DP_PREFIX + deviceName)) {
      
      DebugTN("fwSynopticLocator_Data_Device_SetPanels: Precondition violation (The representation of the device named " + deviceName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */
    
    return dpSetWait(DEVICES_DP_PREFIX + deviceName + ".:_original.._value", panelNames);
  
  }


  int fwSynopticLocator_Data_Devices_SetPanels(dyn_string &deviceName, dyn_dyn_string & panelNames){
    
    /* Precondition Checking Start */

    dyn_string deviceNameComplete;
    dyn_string deviceNameExist;
    dyn_dyn_string panelNamesExist;
    int nDevice = dynlen(deviceName);
    for(int i=1; i<= nDevice; i++)
    {
      if (!dpExists(DEVICES_DP_PREFIX + deviceName[i]))
      {
        DebugTN("fwSynopticLocator_Data_Device_SetPanels: Precondition violation (The representation of the device named " + deviceName[i] + " should exist, but it doesn't)");
      }
      else
      {
        dynAppend(deviceNameComplete, DEVICES_DP_PREFIX + deviceName[i] + ".:_original.._value");
        dynAppend(deviceNameExist, deviceName[i]);
        dynAppend(panelNamesExist, panelNames[i]);  //here, panelNames[i] is not empty (And we can access to the element.)
      }
    }

    if(dynlen(deviceNameComplete) != nDevice)
    {
      DebugTN("fwSynopticLocator_Data_Device_SetPanels: Precondition violation (Some devices should exist, but they don't)");

      dynClear(deviceName);
      deviceName = deviceNameExist; //update the list of existint devices
      dynClear(panelNames);
      panelNames = panelNamesExist; //update the list of list of panels
    }
    
    /*  Precondition Checking End  */
    
    return dpSetWait(deviceNameComplete, panelNames);
  
  }
  
  /**
    
   Deletes a Device Representation in the system.
   
   @param deviceName The name of the device representation that wants to be removed from the system.
   @pre The representation of the device named <i>deviceName</i> does exists.
   @post The representation of the device named <i>deviceName</i> does not exists.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Device_Delete(string deviceName){
    
    /* Precondition Checking Start */
    
    if (!dpExists(DEVICES_DP_PREFIX + deviceName)) {
      
      DebugTN("fwSynopticLocator_Data_Device_Delete: Precondition violation (The representation of the device named " + deviceName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */
    
    return dpDelete(DEVICES_DP_PREFIX + deviceName);
  
  }
  
  /**
    
   Device representation existance checker.
   
   @param deviceName The name of the device.
   @pre --
   @post --
   @return Bool: True (Device <i>deviceName</i> does exist) or False (Device <i>deviceName</i> does not exist).
   
   */
  bool fwSynopticLocator_Data_Device_Exists(string deviceName) {

    return dpExists(DEVICES_DP_PREFIX + deviceName);
  
  }
  
  /**
    
   Panel representation existance checker.
   
   @param panelName The name of the panel.
   @pre --
   @post --
   @return Bool: True (Panel <i>panelName</i> does exist) or False (Panel <i>panelName</i> does not exist).
   
   */
  
  bool fwSynopticLocator_Data_Panel_Exists(string panelName) {

    return dpExists(PANELS_DP_PREFIX + panelName);
  
  }
  

  
  /**
    
   Returns the number of panels represented in the system.
   
   @pre --
   @post --
   @return Int: Number of panels represented in the system.
   
   */  
  
  int fwSynopticLocator_Data_Panel_NumberOf() {

    return dynlen(dpNames(PANELS_DP_PREFIX + "*", PANELS_DPT_NAME));
    
  }
  
  /**
    
   Creates a Panel Representation in the system.
   It uses the panel setters and the device setter.   
   
   @param panelName The name of the panel that wants to be represented in the system.
   @param panelTitle The title of the panel <i>panelName</i>.
   @param panelPath The path of the panel <i>panelName</i>.
   @param panelDate The last modification date of the panel <i>panelName</i>.
   @param devices The devices that the panel <i>panelName</i> contains.
   @pre The representation of the panel named <i>panelName</i> does not exists.
   @post The representation of the panel named <i>panelName</i> does exists and its parameters has been correctly assigned.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_Create(string panelName, string panelTitle, string panelPath, time panelDate, const dyn_string & deviceNames) {
    
    /* Precondition Checking Start */
    
    if (dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_Create: Precondition violation (The representation of the panel named " + panelName + " should not exist, but it does.");
      
    }
    
    /*  Precondition Checking End  */
    
    int replyCode = dpCreate(PANELS_DP_PREFIX + panelName, PANELS_DPT_NAME);    
    
    replyCode = replyCode || fwSynopticLocator_Data_Panel_SetTitle(panelName, panelTitle);
    replyCode = replyCode || fwSynopticLocator_Data_Panel_SetPath(panelName, panelPath);
    replyCode = replyCode || fwSynopticLocator_Data_Panel_SetDate(panelName, panelDate);
    replyCode = replyCode || fwSynopticLocator_Data_Panel_SetDevices(panelName, deviceNames);
    
    return replyCode;

  }  

  /**
    
   Panel title getter.
   
   @param panelName The name of the panel.
   @param panelTitle It will be assigned to the title of the panel <i>panelName</i>.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_GetTitle(string panelName, string & panelTitle) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_GetTitle: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */ 
   
    return dpGet(PANELS_DP_PREFIX + panelName + ".info.title:_original.._value", panelTitle);
    
  }
  
  /**
    
   Panel last modification date getter.
   
   @param panelName The name of the panel.
   @param panelDate It will be assigned to the last modification date of the panel <i>panelName</i>.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_GetDate(string panelName, time & panelDate) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_GetDate: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */ 

    return dpGet(PANELS_DP_PREFIX + panelName + ".info.date:_original.._value", panelDate);
    
  }
  
  /**
    
   Panel full path getter.
   
   @param panelName The name of the panel.
   @param panelPath It will be assigned to the panel path of the panel <i>panelName</i>.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_GetPath(string panelName, string & panelPath) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_GetPath: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */ 

    return dpGet(PANELS_DP_PREFIX + panelName + ".info.path:_original.._value", panelPath);
    
  }
  
  /**
    
   Gets the names of the devices contained in the panel <i>panelName</i>.
   
   @param panelName The name of the panel.
   @param deviceNames It will be assigned to the devices contained in the panel <i>panelName</i>.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_GetDevices(string panelName, dyn_string & deviceNames) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_GetDevices: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */ 
    
    return dpGet(PANELS_DP_PREFIX + panelName + ".devices:_original.._value", deviceNames);
    
  }

  int fwSynopticLocator_Data_Panel_GetDevicesNonExistent(string panelName, dyn_string & deviceNames)
  {
    //Get all the devices from this panel
    dyn_string devicesFromOnePanel;
    fwSynopticLocator_Data_Panel_GetDevices(panelName, devicesFromOnePanel);

    //Compare this list of devices, with the list of the nonexisting devices
    //And choose the devices from devicesFromOnePanel, which ARE in the nonexistingDeviceList (allDevicesNonExistent)
    dyn_string allDevicesNonExistent = fwSynopticLocator_Data_Device_GetAllNonExistent();


    const int old = -1;
    const int stay = 0;
    const int add = 1;    
    
    mapping devicesMapping;
    
    int nAllDevices = dynlen(allDevicesNonExistent);
    for (int i = 1; i <= nAllDevices; i++)
    {
       devicesMapping[allDevicesNonExistent[i]] = old; 
    }
    
    int nDevicesFromPanel = dynlen(devicesFromOnePanel);
    for (int i = 1; i <= nDevicesFromPanel; i++)
    {
      //if already exists, the device has the old value (-1) and add (1) to  "stay" state
      //if doesn't exists, only add (1) to "add" state
      devicesMapping[devicesFromOnePanel[i]] = devicesMapping[devicesFromOnePanel[i]] + 1;
    }

    string device;
    int valueDevice;
    int nMapping = mappinglen(devicesMapping);
    for (int i = 1; i <= nMapping; i++)
    {
      
      device = mappingGetKey(devicesMapping,i);
      valueDevice = mappingGetValue(devicesMapping, i);
      
      switch(valueDevice)
      {
        
        case stay:
        {
          dynAppend(deviceNames, device);
          break;
        }

        case old:
        {
          break;
        }
      }

    }

    // This mapping does the same as this double loop (Check the performance!)
    // int nDevicesFromOnePanel = dynlen(devicesFromOnePanel);
    // for(int i=1; i<=nDevicesFromOnePanel; i++)
    // {
    //   int nDevicesNonExistent= dynlen(allDevicesNonExistent);
    //   for(int j=1; j<=nDevicesNonExistent; j++)
    //   {
    //     if(devicesFromOnePanel[i] == allDevicesNonExistent[j])
    //     {
    //       //the devices from the panel is in the list of the nonexistint devices. Add to the list
    //       dynAppend(deviceNames, devicesFromOnePanel[i]);
    //     }
    //   }
    // }

    return 0;

  }

  
  /**
    
   Returns the name of every panel represented in the system.
   
   @pre --
   @post --
   @return Dyn_string: Name of the panels represented in the system.
   
   */
  dyn_string fwSynopticLocator_Data_Panel_GetAll() {

    dyn_string panelNames = dpNames(PANELS_DP_PREFIX + "*", PANELS_DPT_NAME);
    int cutPos;    
    
    int nPanels = dynlen(panelNames);
    for (int i = 1; i <= nPanels; i++) {
 
      cutPos = strpos(panelNames[i],PANELS_DP_PREFIX)+strlen(PANELS_DP_PREFIX);
      panelNames[i] = substr(panelNames[i], cutPos, strlen(panelNames[i]));
      
    }
  
    //DebugTN("fwSynopticLocator_Data_Panel_GetAll: Number of Panels " + dynlen(panelNames));  
    
    return panelNames;
  
  }  


  dyn_string fwSynopticLocator_Data_Panel_GetAllNonExistent()
  {
    //Get all non existing devices
    dyn_string devices = fwSynopticLocator_Data_Device_GetAllNonExistent();
    dyn_string panels;
    string sDevice;
    for(int j=1; j<= dynlen(devices); j++)
    {
      sDevice =  devices[j];
      mapping map;
      //Get all the panel associated to this nonexistint device
      fwSynopticLocator_getDeviceMapping(sDevice, map);
      dynAppend(panels, map[sDevice]);
    }
    //panels is the list of devices which doesn't existis. (but the panel has the information of all devices!)
    //remove repetition    
    fwSynopticLocator_removeRepetitions(panels);

    return panels;
  }
  
  /**
    
   Deletes (clears) every panel representation in the system.
   
   @pre --
   @post --
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_DeleteAll(string progressFuntion) {

    dyn_string panels = dpNames(PANELS_DP_PREFIX + "*", PANELS_DPT_NAME);
    int replyCode = 0;    
    
    int nPanels = dynlen(panels);
    
    float progressValue;
    for (int i = 1; i <= nPanels; i++) {

      progressValue = i*50/nPanels;
      execScript(progressFunction, makeDynString(), progressValue);
      
      replyCode = replyCode || dpDelete(panels[i]);
      
    } 
   
    execScript(progressFunction, makeDynString(), 50); // In case it does not enter the for 
    
    return replyCode;
  
  }  
  
  /**
    
   Sets the title of the panel named <i>panelName<i>.
   
   @param panelName The name of the panel.
   @param panelTitle The new title of the panel.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post The representation of the panel <i>panelName</i> contains <i>panelTitle</i> as its panel title.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_SetTitle(string panelName, string panelTitle) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_SetTitle: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      
    }
    
    /*  Precondition Checking End  */ 
    return dpSetWait(PANELS_DP_PREFIX + panelName + ".info.title:_original.._value", panelTitle);
    
  }
  
  
  /**
    
   Sets the last modification date of the panel named <i>panelName<i>.
   
   @param panelName The name of the panel.
   @param panelDate The new date of the panel.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post The representation of the panel <i>panelName</i> has <i>panelDate</i> as its panel las modification date.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_SetDate(string panelName, time panelDate) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_SetDate: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */     
    
    return dpSetWait(PANELS_DP_PREFIX + panelName + ".info.date:_original.._value", panelDate);
    
  }
  
  /**
    
   Sets the full path of the panel named <i>panelName<i>.
   
   @param panelName The name of the panel.
   @param panelPath The new path of the panel.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post The representation of the panel <i>panelName</i> has <i>panelPath</i> as its panel path.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_SetPath(string panelName, string panelPath) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_SetPath: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1;
    }
    
    /*  Precondition Checking End  */
    
    //store only the relative panel path, not the absolute one as the function to open a child panel is always using a relative path to the panels
    string sRelativePanelPath = fwSynopticLocator_File_GetPathRelative(panelPath);    
    
    return dpSetWait(PANELS_DP_PREFIX + panelName + ".info.path:_original.._value", sRelativePanelPath);
    
  }
  
  
  /**
    
   Sets the names of the devices contained in the panel <i>panelName</i>.
   
   @param panelName The name of the panel.
   @param deviceNames The new set of devices contained by the panel <i>panelName</i>.
   @pre The representation of the panel named <i>panelName</i> exists.
   @post The representation of the panel <i>panelName</i> has <i>deviceNames</i> as its contained-devices set.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_SetDevices(string panelName, const dyn_string & deviceNames) {
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_SetDevices: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1; 
    }
    
    /*  Precondition Checking End  */
    
    return dpSetWait(PANELS_DP_PREFIX + panelName + ".devices:_original.._value", deviceNames);
    
  }

  /**
    
   Deletes a Panel Representation in the system.
   
   @param panelName The name of the panel representation that wants to be deleted of the system.
   @pre The representation of the panel named <i>panelName</i> does exists.
   @post The representation of the panel named <i>panelName</i> does not exists.
   @return Int: 0 (OK) or !0 (Something went wrong).
   
   */
  int fwSynopticLocator_Data_Panel_Delete(string panelName){
    
    /* Precondition Checking Start */
    
    if (!dpExists(PANELS_DP_PREFIX + panelName)) {
      
      DebugTN("fwSynopticLocator_Data_Panel_Delete: Precondition violation (The representation of the panel named " + panelName + " should exist, but it does not.");
      return -1; 
    }
    
    /*  Precondition Checking End  */
    
    return dpDelete(PANELS_DP_PREFIX + panelName);
  
  }
