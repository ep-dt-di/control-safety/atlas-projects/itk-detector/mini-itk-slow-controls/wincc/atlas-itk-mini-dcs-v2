/**
 * JCOP
 * Copyright (C) CERN 2018 All rights reserved
 */
/**@file

@name LIBRARY: fwSynopticLocatorFile.ctl

*/

#uses "fwSynopticLocator/fwSynopticLocatorDeprecated.ctl"

  /**
    
   Returns the folder names contained by the folder represented by <i>path</i>.
   This function filters (excluding) the "." and ".." linux default folders.
   
   @param path It represents a folder in the system, using the full path (from root).
   @pre --
   @post --
   @return Dyn_string: Names of the folders contained by folder identyfied by <i>path</i>.
   
   */
  public dyn_string fwSynopticLocator_File_GetFolderNames(string path) {
  
    dyn_string folders = getFileNames(path, "*", FILTER_DIRS); 
    dyn_string filteredFolders; 
    
    int nFolders = dynlen(folders);
    for (int i = 1; i <= nFolders; i++) {  

       if ((folders[i] != ".") && (folders[i] != "..")) {
         
         dynAppend(filteredFolders, folders[i]);  
         
       }
       
    }
    
    return filteredFolders;
  
  }
  
  /**
    
   Returns a dyn_string that contains the devices identified by <i>deviceIdentifiers</i> contained by the panel <i>panelPath</i>.
   
   @param panelPath The directory of the panel
   @param deviceIdentifiers The identifiers of the devices
   @pre --
   @post --
   @return Dyn_string: Contains the device names of the panel <i>panelPath</i>.
   
   */ 
  
  public dyn_string fwSynopticLocator_File_GetDeviceNamesFromPanel(string panelPath, const dyn_string & deviceIdentifiers) {
  
    string fileContent = fwSynopticLocator_File_Read(panelPath);
  
    dyn_string devices;
    int nIdentifiers = dynlen(deviceIdentifiers);
    
    for (int i = 1; i <= nIdentifiers; i++) {

      dynAppend(devices, fwSynopticLocator_File_GetDevicesFromText(fileContent, deviceIdentifiers[i]));
  
    }

    return devices;
  
  }

  public dyn_string fwSynopticLocator_File_devicesToStandardName(dyn_string devices)
  {
    int nDevices = dynlen(devices);
    for (int i = 1; i <= nDevices; i++)
    {
      devices[i] = fwSynopticLocator_File_toStandardName(devices[i]);
    }

    return devices;
  }

  public dyn_string fwSynopticLocator_File_devicesFromStandardName(dyn_string devices)
  {
    int nDevices = dynlen(devices);
    for (int i = 1; i <= nDevices; i++)
    {
      devices[i] = fwSynopticLocator_File_fromStandardName(devices[i]);
    }

    return devices;
  }

  string fwSynopticLocator_File_toStandardName( string name)
  {    
    string tmp=name;
    
    dyn_dyn_string map = fwSynopticLocator_File_getMappingStandardName();
    
    for(int i=1; i<=dynlen( map); i++)
    {
      strreplace( tmp, map[i][1], map[i][2]);
    }
    
    return tmp;
  }

string fwSynopticLocator_File_fromStandardName( string name)
{
  string tmp=name;
  
  dyn_dyn_string map = fwSynopticLocator_File_getMappingStandardName();
  
  for(int i=1; i<=dynlen( map); i++)
  {
    strreplace( tmp, map[i][2], map[i][1]);
  }
  
  return tmp;
}

dyn_dyn_string fwSynopticLocator_File_getMappingStandardName()
{
  dyn_dyn_string tmp;
  
  tmp[1][1] = " "; tmp[1][2] = "_space_";
  tmp[2][1] = "/"; tmp[2][2] = "_slash_";
  tmp[3][1] = "*"; tmp[3][2] = "_star_";
  tmp[4][1] = ","; tmp[4][2] = "_comma_";
  tmp[5][1] = ";"; tmp[5][2] = "_semicolum_";
  tmp[6][1] = "."; tmp[6][2] = "_dot_";
  tmp[7][1] = ":"; tmp[7][2] = "_dotvert_";
  tmp[8][1] = "-"; tmp[8][2] = "_dash_";
  tmp[9][1] = "="; tmp[9][2] = "_equals_";

  tmp[10][1] = "|"; tmp[10][2] = "_tagalias_";   //Extra tag to create DPE in fwSynopticLocator (devices with alias)s
  
  return tmp;
}

  
  /**
    
   Returns a dyn_string that contains the full path to every panel file in the <i>path</i> directory (including sub directories).
   
   @param path The directory in which the search will start.
   @pre --
   @post --
   @return Dyn_string: Contains the path of every panel file in the <i>path</i> directory.
   
   */      
  
  public dyn_string fwSynopticLocator_File_GetPanels(string path) {
  
    dyn_string panelFileNames = fwSynopticLocator_File_GetPNLandXMLfileNames(path);
    int nPanelFileNames = dynlen(panelFileNames);
    
    for (int i = 1; i <= nPanelFileNames; i++) {  
    
      panelFileNames[i] = path + "/" + panelFileNames[i];
       
    }  
  
    dyn_string folders = fwSynopticLocator_File_GetFolderNames(path);

    int nFolders = dynlen(folders);
    for (int i = 1; i <= nFolders; i++) {  

       dynAppend(panelFileNames,fwSynopticLocator_File_GetPanels(path + "/" + folders[i]));
       
     }
   
    return panelFileNames; 
  
  }
  
  /**
    
   Returns the panel file names contained by the folder represented by the <i>path</i> parameter.
   
   @param path It represents a folder in the system, using the full path (from root)
   @pre --
   @post --
   @return Dyn_string: Names of the .pnl and .xml files contained by folder identyfied by <i>path</i>.
   
   */
  
  public dyn_string fwSynopticLocator_File_GetPNLandXMLfileNames(const dyn_string & path) {

    dyn_string fileNames;
    int nPaths = dynlen(path);    
    
    for (int i = 1; i <= nPaths; i++) {
      
      dynAppend(fileNames, getFileNames(path[i],"*.pnl"));
      dynAppend(fileNames, getFileNames(path[i],"*.xml"));

    }  

    return fileNames;    
    
  }
  
  /**
    
   Returns the title of the panel, identified by one of the identifiers in the titleIdentifiers dyn_string.
   
   @param path It represents a panel (.pnl or .xml) file in the system, using the full path (from root).
   @param titleIdentifiers It contains the title identifiers.
   @pre --
   @post --
   @return String: The title of the panel file represented by <i>path</i>.
   
   */
  
  public string fwSynopticLocator_File_GetPanelTitle(string path, const dyn_string & titleIdentifiers) {
    
    if (strpos(path, ".pnl") > 0) return fwSynopticLocator_File_GetPanelTitleFromPNL(path, titleIdentifiers);
    else if (strpos(path, ".xml") > 0) return fwSynopticLocator_File_GetPanelTitleFromXML(path, titleIdentifiers);
    else return "-1";
    
  }  
  
  /**
    
   Returns the title of the panel (just .pnl), identified by one of the identifiers in the titleIdentifiers dyn_string.
   
   @param path It represents a panel (.pnl) file in the system, using the full path (from root).
   @param titleIdentifiers It contains the title identifiers.
   @pre --
   @post --
   @return String: The title of the panel file represented by <i>path</i>.
   
   */  
  
  private string fwSynopticLocator_File_GetPanelTitleFromPNL(string path, const dyn_string & titleIdentifiers) {
  
    string textedPanel = fwSynopticLocator_File_Read(path);

    string title = "*No title found*";
    
    int nTitleIdentifiers = dynlen(titleIdentifiers);
    for (int i = 1; i <= nTitleIdentifiers; i++)
    {
      int pos = strpos(textedPanel, titleIdentifiers[i]);
      
      // Found the first (the first one should be the correct one) identifier for nTitleIdentifiers[i]
      if (pos > 0)
      { 
        
        //"$headerTitle""______________" // ____ is the Title Name
        //or
        //"$headerTitle"" "____________" " // "____" is the Title Name (between quotation marks)


        /*

        //Title with Quotes

        "$title1\"\"\\\"BEQ2\\\"\""
        title1\"\"\\\"BEQ2\\\"\"    
        \"\\\"BEQ2\\\"\"    
        \\\"BEQ2\\\"    
        \"BEQ2\"      
        "BEQ2"

        //No quotes in the panel name

        "$title1\"\"\\BEQ2\\\""
        title1\"\"\\BEQ2\\\""
        \"\\BEQ2\\\""
        \\BEQ2\\  
        BEQ2

        */

        //Check if the titleIdentifiers[i] has $ before,
        string sDollarTitleId = "";
        sDollarTitleId = textedPanel[pos-1];
        if(sDollarTitleId != "$")
          return title; 
        
        //Identify the line in the string, where the title name is.
        //len 200 as maximum to find a name (instead of a while loop)
        int iLenMaxCompleteLine = 200;
        string sHeaderTitleLineComplete = "";
        for(int j=0; j<=iLenMaxCompleteLine; j++)
        {
          if(textedPanel[pos + j] == 10)
          {
            //10 => break line in ASCII
            break;
          }
          else
          {
            sHeaderTitleLineComplete = sHeaderTitleLineComplete + textedPanel[pos + j];
          }
          
          if(j == iLenMaxCompleteLine - 1)
          {
            //not found break line
            //finish for but without value
            sHeaderTitleLineComplete = "";
          }
        };
                
        string sHeaderTitle = "";        
        //Remove 'title1\"'
        sHeaderTitle = strltrim(sHeaderTitleLineComplete, titleIdentifiers[i]);  //trim the title
        sHeaderTitle = substr(sHeaderTitle, 1);  //+1 because we want also remove \" (and '\"' counts as one character)
        
        //now, remove the quotes at the start and the end of the panel name
        sHeaderTitle = substr(sHeaderTitle, 1);
        sHeaderTitle = substr(sHeaderTitle, 0, strlen(sHeaderTitle)-1);
        
        //remove double backslash (occurs when we read a panel .pnl)
        dyn_string sHeaderTitle_Split = strsplit(sHeaderTitle, "\\");
        if(dynlen(sHeaderTitle_Split) == 3)
          sHeaderTitle = sHeaderTitle_Split[2] + sHeaderTitle_Split[3];    //there is more characters after the second backslash (it means a title with quotes)
        else if(dynlen(sHeaderTitle_Split) == 1)
          sHeaderTitle = sHeaderTitle_Split[1];  //there is no more characters after backslash (it is the name directly)
        
        //translate if exist, '\"' as a simple '"'.
        //For this case, Remove the quotes but dont put another.
        strreplace(sHeaderTitle, "\"", "");
        
        //if all is right, return the title. otherwise, return empty. (Check the string functions!)
        title = sHeaderTitle;
        
        //DebugTN(title);
        return title; 
        
        //If found, return title. Title will cointain the title if a way of identifying a Title Name was meet, otherwise, "*No title*".
        
      }

    }

    return title;
    
  }
  
  /**
    
   Returns the title of the panel (just .xml), identified by one of the identifiers in the titleIdentifiers dyn_string.
   
   @param path It represents a panel (.xml) file in the system, using the full path (from root).
   @param titleIdentifiers It contains the title identifiers.
   @pre --
   @post --
   @return String: The title of the panel file represented by <i>path</i>.
   
   */    
  
  private string fwSynopticLocator_File_GetPanelTitleFromXML(string path, const dyn_string & titleIdentifiers) {
  
    string textedPanel = fwSynopticLocator_File_Read(path);
    string title = "*No title*";       
    
    int nTitleIdentifiers = dynlen(titleIdentifiers);
    for (int i = 1; i <= nTitleIdentifiers; i++) {
     
      int pos = strpos(textedPanel, titleIdentifiers[i]);
      
      if (pos >= 0) { // Found the first (the first one should be the correct one) identifier for nTitleIdentifiers[i]
      
        pos = strpos(textedPanel, "\">", pos) + 2; // "Value">Title</ Goes to T of Title
        
        int lastPos = strpos(textedPanel, "<", pos); // lue">Title</prop> Goes after e of Title
        
        title = substr(textedPanel, pos, lastPos-pos); // Takes title 

        return title;
      
      }  

    }
    
    return title;
    
  }
  
  /**
    
   Returns a string which contains the text of the file <i>path</i>.
   
   @param path It represents a file in the system, using the full path (from root).
   @pre --
   @post --
   @return String: Contains the texted <i>path</i> file. In an error case, it returns a string with "-1".
   
   */  
  
  public string fwSynopticLocator_File_Read(string path) {
  
    string result;
  
    if (fileToString(path, result)) {
    
      return result;    
    
    }
  
    return "-1";  
  
  }  
  
  /**
    
   Returns a string ("pnl" or "xml") depending on the type of panel file that <i>panelText</i> was.
   
   @param panelText It is a string which represents a file that was converted into text.
   @pre --
   @post --
   @return String: "pnl" (panelText was extracted from a .pnl file) / "xml" (.xml). In case of error, it returns "-1".
   
   */    
  
  public string panelType(string & panelText) {  
    
    if ((strpos(panelText, "<?xml") > 0 )|| (strpos(panelText, "<panel>") > 0)) return "xml"; //Change to global variable?
    else if (strpos(panelText, "LANG:")) return "pnl"; //Change to global variable?
    else return -1;
    
  }
  
  /**
    
   Returns the devices contained in <i>panelText</i> identified by, at least, one of the identifiers in <i>identifier</i>.
   If a device name is "", it is not taken into account.
   <i>panelText</i> is the extracted text of a .pnl file or .xml file, representing a panel.
   
   @param panelText It is a panel file texted (.pml or .xml).
   @param identifier The identifier with which the devices will be looked for.
   @pre --
   @post --
   @return Dyn_string: The devices contained in panelText, identified by <i>identifier</i>.
   
   */

  public dyn_string fwSynopticLocator_File_GetDevicesFromText(string & panelText, string identifier) {  

    if (panelType(panelText) == "xml") return fwSynopticLocator_File_GetDevicesFromXMLText(panelText, identifier);
    else if (panelType(panelText) == "pnl") return fwSynopticLocator_File_GetDevicesFromPNLText(panelText, identifier);
    else return -1;
    
  }
  
  /**
    
   Returns the devices contained in <i>panelText</i> identified by, at least, one of the identifiers in <i>identifier</i>.
   If a device name is "", it is not taken into account.
   <i>panelText</i> is the extracted text of .xml file, representing a panel.
   
   @param panelText It is a panel file texted (.xml).
   @param identifier The identifier with which the devices will be looked for.
   @pre --
   @post --
   @return Dyn_string: The devices contained in panelText, identified by <i>identifier</i>.
   
   */  
  
  private dyn_string fwSynopticLocator_File_GetDevicesFromXMLText(string panelText, string identifierOrig) {

  string identifier = identifierOrig+"</prop>"; //to make sure it only catches the identifier of the $-param note another text in e.g. a script
    int nextStart = 0;
    int nextEnd = 0;
    string device;
    dyn_string devices;
    string deviceToExtract;
  
    nextStart = strpos(panelText, identifier, nextStart);  
  
    while (nextStart >= 0 ) {
    
      nextStart = strpos(panelText, "Value", nextStart); //Because of document structure        
      nextStart = nextStart + 7; // Value"> number of chars.
      nextEnd = strpos(panelText, "<", nextStart);
      device = substr(panelText, nextStart, nextEnd-nextStart);
    
      if(strpos(device,":") > 0) {
        
        //SYSTEM_NAME:DEVICE_NAME, [1] = SYSTEM_NAME, [2] = DEVICE_NAME        
        
        dyn_string separatedDevice = strsplit(device, ":"); //To ensure variable cleaning.
        
        if (dynlen(separatedDevice) == 2) device = separatedDevice[2];     
        else if (dynlen(separatedDevice) == 1) device = "";
        
      }
    
      if (device != "") {

        //Before to add a devices, check if there is a semicolon as separator.
        //If it is the case, separe and add to the list of all devices.
        int iFlagErrorEmpty = 0;
        dyn_string dsDevicesInIdentifier;
        dsDevicesInIdentifier=strsplit(device, ";");
        int iNDevicesInIdentifier = dynlen(dsDevicesInIdentifier);
        for(int i=1; i<=iNDevicesInIdentifier;i++)
        {
          //check if it is empty or not between semicolon. if it is empty, not append.
          if(dsDevicesInIdentifier[i] != "")
          {
            dynAppend(devices, dsDevicesInIdentifier[i]); 
          }
          else
          {
            //error
            iFlagErrorEmpty = -1;
          }
        }

        if(iFlagErrorEmpty == -1)
        {
          DebugTN("fwSynopticLocator_File_GetDevicesFromPNLText: There is an empty device before the semicolon (separator), device " + device);
        }
        
      }
      
      nextStart = strpos(panelText, identifier, nextEnd+1);
    }
  
    return devices;
  
  }
  
    
  /**
    
   Returns the devices contained in <i>panelText</i> identified by, at least, one of the identifiers in <i>identifier</i>.
   If a device name is "", it is not taken into account.
   <i>panelText</i> is the extracted text of .pnl file, representing a panel.
   
   @param panelText It is a panel file texted (.pnl).
   @param identifier The identifier with which the devices will be looked for.
   @pre --
   @post --
   @return Dyn_string: The devices contained in panelText, identified by <i>identifier</i>.
   
   */  
  
  private dyn_string fwSynopticLocator_File_GetDevicesFromPNLText(string panelText, string identifier) {

    int nextStart = 0;
    int nextEnd = 0;
    string device;
    dyn_string devices;
    string deviceToExtract;
  
    nextStart = strpos(panelText, identifier, nextStart);  
  
    while (nextStart >= 0) {
      
      if (substr(panelText, nextStart, 2 + strlen(identifier)) == (identifier + "\"" + "\"")) {
          
        // 1st way of identifying a Device Name from a Device Identifier
        //"$identifier""______________" // ____ is the Devie Name 
        
        nextStart = nextStart + strlen(identifier) + 2; // "" (2 chars) always after the identifier
        nextEnd = strpos(panelText, "\"", nextStart);
        device = substr(panelText, nextStart, nextEnd-nextStart);
    
        if(strpos(device,":") > 0) {
        
          //SYSTEM_NAME:DEVICE_NAME, [1] = SYSTEM_NAME, [2] = DEVICE_NAME        
        
          dyn_string separatedDevice = strsplit(device, ":"); //To ensure variable cleaning.
        
          if (dynlen(separatedDevice) == 2) device = separatedDevice[2];     
          else if (dynlen(separatedDevice) == 1) device = "";
        
        }
    
        if (device != "" && strpos(device, "$") < 0) 
        {
          //Before to add a devices, check if there is a semicolon as separator.
          //If it is the case, separe and add to the list of all devices.
          int iFlagErrorEmpty = 0;
          dyn_string dsDevicesInIdentifier;
          dsDevicesInIdentifier=strsplit(device, ";");
          int iNDevicesInIdentifier = dynlen(dsDevicesInIdentifier);
          for(int i=1; i<=iNDevicesInIdentifier;i++)
          {
            //check if it is empty or not between semicolon. if it is empty, not append.
            if(dsDevicesInIdentifier[i] != "")
            {
              dynAppend(devices, dsDevicesInIdentifier[i]); 
            }
            else
            {
              //error
              iFlagErrorEmpty = -1;
            }
          }

          if(iFlagErrorEmpty == -1)
          {
            DebugTN("fwSynopticLocator_File_GetDevicesFromPNLText: There is an empty device before the semicolon (separator), device " + device);
          }
        
        }
        
        nextStart = strpos(panelText, identifier, nextEnd+1);        
        
      }
      
      else { nextStart = strpos(panelText, identifier, nextStart+1); }
    
    }
  
    return devices;
  
  }
  
  /**
    
   Returns the name (without .format) of the file identified by <i>path</i>.
   
   @param path It represents a file in the system, using the full path (from root).
   @pre --
   @post --
   @return String: The name of the file represented by <i>path</i>.
   
   */  
  
  public string fwSynopticLocator_File_GetName(string path) {
  
    dyn_string folders = strsplit(path, "/");
  
    string name = folders[dynlen(folders)];
  
    name = strsplit(name, ".")[1]+"_"+strtoupper(strsplit(name, ".")[2]);
    
    return name;
  
  }
  
  
  /**
    From the absolute path of a file, this function removes the part of the 
    path which is the WinCC OA project path. If the project has only one 
    project path this function is a equivalent to simple call to getPath. It
    the project has multiple paths (like TIP), it needs to search for all the
    paths
    
    @param sFileAbsolutePath: absolute path of a file
    @return String the relative path of a file or empty if eeror
    
    */
  public string fwSynopticLocator_File_GetPathRelative( string sFileAbsolutePath)
  {
    
    
    //iterate over all project paths and exit when we find the one that matches the absolute path 
    for( int i=1; i<=SEARCH_PATH_LEN; i++)
    {
      string sProjPath = getPath( PANELS_REL_PATH, "", 1, i);
      if ( strpos( sFileAbsolutePath, sProjPath) > -1)
      {
        string sRelativePath = sFileAbsolutePath;
        strreplace( sRelativePath, sProjPath, "");
        return sRelativePath;
      }
    }    
    
    return ""; //if we reach this point, no match has been found - error
  }
  
  
  /**
    
   Returns the last modification time of the file identified by <i>path</i>.
   
   @param path It represents a file in the system, using the full path (from root).
   @pre --
   @post --
   @return Time: The last modification time of the file identified by <i>path</i>.
   
   */ 
  
  public time fwSynopticLocator_File_GetDate(string path) {
  
    return getFileModificationTime(path);
    
  }

  
