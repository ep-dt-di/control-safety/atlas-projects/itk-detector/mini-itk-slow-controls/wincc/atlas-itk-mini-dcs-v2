class fwViewer_loginClass{
  string loginPanelPath;
  string userName;
  string key;

  public fwViewer_loginClass (string loginPanelPath_= "fwViewer/fwLoginWindow.pnl") {
    loginPanelPath = loginPanelPath_;
  }
  public bool login() {
    bool status = false;
    const string empty ="";
    string password, user;

    mapping loginData = getLoginData();
    password = loginData["password"];
    user = loginData["userName"];

    if ((password != empty)  & (user != empty)) {
       userName = user;
       key = generateKey(userName, password);
       status = true;
       return status;
     }
     return status;
  }

  public string getKey() {
    return key;
  }
  
  public string getActiveUser() {
    return userName;
  }

  public bool isUserActive() {
    bool userActive = true;
    if(userName ==""){
      userActive = false;
    }
    return userActive;
  }
  public bool logout() {
	userName = "";
	key = "";
	return true;
	
  }

  private mapping getLoginData () {
    string panelName = "Login window";
    dyn_string parameters = makeDynString("");
    dyn_float resultFlota;
    dyn_string resultText;
    string password, user;
    const int expectedNumberOfField = 2;

    ChildPanelOnCentralModalReturn(loginPanelPath,panelName, parameters,resultFlota,resultText);

    if(dynlen(resultText) == expectedNumberOfField) {
      user = resultText[1];
      password = resultText[2];
    }

    return makeMapping("userName", user, "password", password);
  }

   private string generateKey(string user, string password) {
     return base64Encode(user + ":" + password);
  }

};
