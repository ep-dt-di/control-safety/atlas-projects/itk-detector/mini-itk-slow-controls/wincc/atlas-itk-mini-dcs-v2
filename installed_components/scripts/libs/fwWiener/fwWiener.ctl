/**@file

   @par Creation Date
        25/04/2017

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @author Oliver Holme
   @author Fernando Varela
   @author Jonas Arroyo Garcia

   @copyright CERN copyright


*/


//@{


// Library dependencies
#uses "fwDevice/fwDevice.ctl"
#uses "fwDevice/fwDeviceFrontEndConfigFile.ctl"


// Constants
/** @var string FW_WIENER_PL512_DEVICE_MODEL

 Constant used to identify the PL512 TCP crate JCOP model
*/
const string FW_WIENER_PL512_DEVICE_MODEL = "PL512 Crate (TCP/IP)";




/** Function to add newer properties exposed by latest firmware relase in PL512 crates.

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param[out]  exceptionInfo  dyn_string: returns details of any errors

   @return bool: TRUE = Success, FALSE = Error: some JCOP call returned an error
*/
bool fwWiener_applyDefaultOpcAddressing2Pl512(dyn_string &exceptionInfo)
{
  int iLoop, iLen, iAddressType, iLenError, iLoopChildren, iLenChildren;
  string sName, sModel, sError;
  dyn_string dsDps, dsDevice, dsAddress, dsChannelsDps, dsChannelAddress;
  dyn_errClass deError;


  DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Checking OPC Addresses for Marathon PL512 Power Supplies");

  // Initialization
  sError = "";
  fwDevice_initialize(); //initialization of the fwDevice library.


  dsDps   = dpNames(getSystemName() + "Wiener/*", "FwWienerMarathon");
  deError = getLastError();
  if( dynlen(deError) > 0 )
  {
    DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Getting all FwWienerMarathon crates, due to: " + getErrorText(deError));
    return FALSE;
  }


  iLen = dynlen(dsDps);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    // Initialize variables
    dynClear(dsDevice);
    dynClear(dsAddress);
    dynClear(dsChannelsDps);
    dsDevice[fwDevice_DP_NAME] = dsDps[iLoop];
    sModel                     = "";
    sName                      = "";
    iAddressType               = -1;


    // Get device name
    fwDevice_getName(dsDps[iLoop], sName, exceptionInfo);
    iLenError = dynlen(exceptionInfo);
    if( iLenError > 0 )
    {
      // fwDevice_getName doesn't return nothing in the exceptionInfo variable
      if( iLenError >= 2 )
      {
        sError = exceptionInfo[2];
      }
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Failed to read name for device: " + dsDps[iLoop] + ". Error details: " + sError);
      return FALSE;
    }


    // Get model name
    fwDevice_getModel(dsDevice, sModel, exceptionInfo);
    iLenError = dynlen(exceptionInfo);
    if( iLenError > 0 )
    {
      if( iLenError >=  2 )
      {
        sError = exceptionInfo[2];
      }
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Failed to read model for device: " + sName + ". Error details: " + sError);
      return FALSE;
    }
    else
    {
      if( sModel != FW_WIENER_PL512_DEVICE_MODEL )
      {
        //Not a PL512 Marathon. Nothing to be done
        continue;
      }
    }


    DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Checking if addressing OPC addressing of Marathon PL512 Power Supply: " + sName + " is up-to-date");
    // Check if the address is up-to-date. otherwise, reapply default config
    if( dpGet(dsDps[iLoop] + ".NetworkAddress.NetworkAddress:_address.._type", iAddressType) != 0 )
    {
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: getting address from Maraton PL512 power supply: " + dsDps[iLoop] + ".NetworkAddress.NetworkAddress:_address.._type");
      return FALSE;
    }
    else
    {
      deError = getLastError();
      if( dynlen(deError) > 0 )
      {
        DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: getting address from Maraton PL512 power supply: " + dsDps[iLoop] + ".NetworkAddress.NetworkAddress:_address.._type due to: " + getErrorText(deError));
        return FALSE;
      }
    }

    if( iAddressType == DPCONFIG_PERIPH_ADDR_MAIN )
    {
      // The address already exists, nothing to be done
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> OPC addressing of Marathon PL512 Power Supply: " + sName + " is up-to-date");
      continue;
    }


    DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> OPC Addressing out-dated. Re-applying default addressing to Marathon PL512 Power Supply: " + sName);

    fwDevice_getAddressDefaultParams("FwWienerMarathon", dsAddress, exceptionInfo, FW_WIENER_PL512_DEVICE_MODEL, dsDps[iLoop], fwPeriphAddress_TYPE_OPC);
    if( dynlen(exceptionInfo) > 0 )
    {
      if( dynlen(exceptionInfo) >= 2 )
      {
        sError = exceptionInfo[2];
      }
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Failed to re-apply default addressing to device: " + sName + ". Error details:" + sError);
      return FALSE;
    }

    // Set main crate addresses
    fwDevice_setAddress(dsDps[iLoop], dsAddress, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      if( dynlen(exceptionInfo) >= 2 )
      {
        sError = exceptionInfo[2];
      }
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Failed to re-apply default addressing to device: " + sName + ". Error details:" + sError);
      return FALSE;
    }

    // Set all children addresses
    fwDevice_getChildren(dsDevice, fwDevice_HARDWARE, dsChannelsDps, exceptionInfo);
    if( dynlen(exceptionInfo) > 0 )
    {
      if( dynlen(exceptionInfo) >= 2 )
      {
        sError = exceptionInfo[2];
      }
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Failed to re-apply default addressing to device: " + sName + ". Error details:" + sError);
      return FALSE;
    }
    DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512()-> Default OPC addressing successfully applied to Marathon PL512 Power Supply: " + sName);


    iLenChildren = dynlen(dsChannelsDps);
    for( iLoopChildren = 1 ; iLoopChildren <= iLenChildren ; iLoopChildren++ )
    {
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Re-applying default addressing to Marathon PL512 Power Supply Channel: " + dsChannelsDps[iLoopChildren]);

      // Get default channel configuration parameters
      fwDevice_getAddressDefaultParams("FwWienerMarathonChannel", dsChannelAddress, exceptionInfo, FW_WIENER_PL512_DEVICE_MODEL, "", fwPeriphAddress_TYPE_OPC);
      if( dynlen(exceptionInfo) > 0 )
      {
        if( dynlen(exceptionInfo) >= 2 )
        {
          sError = exceptionInfo[2];
        }
        DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Failed to re-apply default addressing to device: " + dsChannelsDps[iLoopChildren] + ". Error details:" + sError);
        return FALSE;
      }

      // Apply default channel configuration parameters
      fwDevice_setAddress(dsChannelsDps[iLoopChildren], dsChannelAddress, exceptionInfo);
      if( dynlen(exceptionInfo) > 0 )
      {
        if( dynlen(exceptionInfo) >= 2 )
        {
          sError = exceptionInfo[2];
        }
        DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Error: Failed to re-apply default addressing to device: " + dsChannelsDps[iLoopChildren] + ". Error details:" + sError);
        return FALSE;
      }
      DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> Default OPC addressing successfully applied to Marathon PL512 Power Supply Channel: " + dsChannelsDps[iLoopChildren]);
    }
  }

  DebugTN("fwWiener_applyDefaultOpcAddressing2Pl512() -> All Marathon PL512 Power Supplies' Addresses are up-to-date");

  return TRUE;
}






/** Function to create the Wiener OPC DA config file based on the WinCC OA instances

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @par Usage
        Public

   @par PVSS managers
        VISION


   @param[in]   canBuses       dyn_string: Array with the list of canBuses devices to create the config file
   @param[in]   fileName       string: Wiener OPC DA config file
   @param[out]  exceptionInfo  dyn_string: returns details of any errors
   @param[in]   driverNumber   int: by default is 14. In case you would like to specify a different driver number

   @return Nothing
*/
fwWiener_createOpcConfigFile(dyn_string canBuses, string fileName, dyn_string &exceptionInfo, int driverNumber = 14)
{

  file outputFile;
  string header, fileContents, openingTag, closingTag;

  fwDeviceFrontEndConfigFile_GetDpTypeTag(dpTypeName("WIENER_" + driverNumber), fwDeviceFrontEndConfigFile_OPENING_TAG, openingTag, exceptionInfo);
  fwDeviceFrontEndConfigFile_GetDpTypeTag(dpTypeName("WIENER_" + driverNumber), fwDeviceFrontEndConfigFile_CLOSING_TAG, closingTag, exceptionInfo);

  //DebugN(canBuses);
  int length = dynlen(canBuses);
  for(int i=1; i<=length; i++)
  {
    string model, tempOpen, tempClose, tempStore;
    dyn_int childPositions;
    dyn_string canBusChildren;
    mapping sortedChildren;

    fwDevice_getModel(makeDynString(canBuses[i]), model, exceptionInfo);
    if(model == "CAN Bus")
    {
      fwDeviceFrontEndConfigFile_GetDeviceTag(canBuses[i], fwDeviceFrontEndConfigFile_OPENING_TAG, tempOpen, exceptionInfo);
      fwDeviceFrontEndConfigFile_GetDeviceTag(canBuses[i], fwDeviceFrontEndConfigFile_CLOSING_TAG, tempClose, exceptionInfo);
      header += tempOpen + tempClose;
      fwDeviceFrontEndConfigFile_ProcessEntry(canBuses[i], header, header, exceptionInfo);

      fwDevice_getChildren(canBuses[i], fwDevice_HARDWARE, canBusChildren, exceptionInfo);
      for(int j=1; j<=dynlen(canBusChildren); j++)
      {
        int position;
        string name;
        fwDevice_getPosition(canBusChildren[j], name, position, exceptionInfo);
        sortedChildren[position] = canBusChildren[j];
      }

      childPositions = mappingKeys(sortedChildren);
      dynSortAsc(childPositions);

      for(int j=1; j<=dynlen(childPositions); j++)
      {
        fwDeviceFrontEndConfigFile_CreateContents(sortedChildren[childPositions[j]], tempStore, exceptionInfo);
        fileContents += tempStore;
      }
    }
    else
    {
      fwDeviceFrontEndConfigFile_CreateContents(canBuses[i], tempStore, exceptionInfo);
      //DebugN(tempStore);
      fileContents += tempStore;
    }
  }

  strreplace(openingTag, "<!-- CAN details -->", "<!-- CAN details -->\n" + header);
  fileContents = openingTag + fileContents + closingTag;

  outputFile = fopen(fileName, "w");
  fprintf(outputFile, fileContents);
  fclose(outputFile);
}




/** Function to update the WinCC OA settings DPE based in the hardware settings

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param[in]   dpName         string: Device DP to retrieve its settings from the hardware
   @param[out]  exceptionInfo  dyn_string: returns details of any errors

   @return Nothing
*/
fwWiener_loadSettingsFromHw(string dpName, dyn_string &exceptionInfo)
{
  int i;
  string dpType;
  dyn_string readbackElements, readbackManagers, settingElements;
  dyn_int managers;
  dyn_anytype values;
  dyn_errClass errors;

  dpType = dpTypeName(dpName);
  fwWiener_getReadbackElements(dpType, dpName, readbackElements, exceptionInfo);

  settingElements = readbackElements;
  for(i=1; i<=dynlen(readbackElements); i++)
  {
    strreplace(settingElements[i], "ReadBackSettings", "Settings");
    strreplace(settingElements[i], "ReadbackSettings", "Settings");

    readbackManagers[i] = readbackElements[i] + ":_online.._manager";
  }

  if(dynlen(readbackElements) > 0)
  {
    dpGet(readbackElements, values,
          readbackManagers, managers);
  }

  //go through lists backwards to ensure that deleted entries do not change indexes to be used later
  for(i=dynlen(managers); i>0; i--)
  {
    char manType, manNum;
    getManIdFromInt(managers[i], manType, manNum);
    //DebugN(readbackManagers[i], manType, manNum, (char)DRIVER_MAN);
    if(manType != (char)DRIVER_MAN)
    {
      //DebugN("removing because not set by driver", settingElements[i], values[i]);
      dynRemove(settingElements, i);
      dynRemove(values, i);
    }
  }

  if(dynlen(values) > 0)
  {
    dpSetWait(settingElements, values);
    errors = getLastError();
    if(dynlen(errors) > 0)
    {
      throwError(errors);
      fwException_raise(exceptionInfo, "ERROR", "Could not load all the settings from hardware.", "");
    }
  }
}




/** Function to get the readback elements for certain model

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param[in]   dpType            string: Device DPT to identify its JCOP Device definition and model
   @param[in]   prefixDpName      string: prefix in the dpName to access the settings fields in the device dp name
   @param[out]  readbackElements  dyn_string: dpes with readback settings
   @param[out]  exceptionInfo     dyn_string: returns details of any errors

   @return Nothing
*/
fwWiener_getReadbackElements(string dpType, string prefixDpName, dyn_string &readbackElements, dyn_string &exceptionInfo)
{
  int i, j, k, level, found = -1;
  dyn_string path;
  dyn_dyn_string elementNames, refMap;
  dyn_dyn_int elementTypes;
  string stringPath, refType;

  dpTypeGet(dpType, elementNames, elementTypes);
  refMap = dpGetDpTypeRefs(dpType);
  //DebugN(refMap);

  for(i=1; i<=dynlen(elementNames); i++)
  {
    for(j=1; j<=dynlen(elementNames[i]); j++)
    {
      if(elementTypes[i][j] != 0)
      {
        level = j;

        if(level > 1)
          path[level] = elementNames[i][j];
        else
          path[level] = prefixDpName;

        while(dynlen(path) > level)
          dynRemove(path, level + 1);

        if(found == (level-1))
        {
          fwGeneral_dynStringToString(path, stringPath, ".");
          dynAppend(readbackElements, stringPath);
        }
        else
          found = -1;

        if(elementTypes[i][j] == DPEL_STRUCT)
        {
          if(strtolower(elementNames[i][j]) == "readbacksettings")
            found = level;
        }
        else if(elementTypes[i][j] == DPEL_TYPEREF)
        {
          for(k=1; k<=dynlen(refMap); k++)
          {//DebugN((path[level-1] + "." + elementNames[i][j]));
            if((refMap[k][2] == elementNames[i][j]) || (refMap[k][2] == (path[level-1] + "." + elementNames[i][j])))
            {
              fwGeneral_dynStringToString(path, stringPath, ".");
              fwWiener_getReadbackElements(refMap[k][1], stringPath, readbackElements, exceptionInfo);
              break;
            }
          }
        }
        break;
      }
    }
  }
//  DebugN(readbackElements);
}






/** Function to set the Krakow Marathon customizations

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param[in]   dpName            string: crate device dpname to set customizations
   @param[out]  exceptionInfo     dyn_string: returns details of any errors

   @return Nothing
*/
fwWiener_setKrakowMarathonCustomisation(string dpName, dyn_string &exceptionInfo)
{
  int i, length, pos;
  string name;
  dyn_string children;

  fwConfigConversion_set(dpName + ".Status.OutputFailure", DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN, DPDETAIL_CONV_INVERT, 0, makeDynFloat(), exceptionInfo);

  fwDevice_getChildren(dpName, fwDevice_HARDWARE, children, exceptionInfo);

  length = dynlen(children);
  for(i=1; i<=length; i++)
  {
    fwDevice_getPosition(children[i], name, pos, exceptionInfo);

    dpSetWait(children[i] + ".Status.FailureMaxCurrent:_address.._datatype", 491,
              children[i] + ".Status.FailureMaxCurrent:_address.._subindex", pos);
    dpSetWait(children[i] + ".Status.FailureMaxSenseVoltage:_address.._datatype", 491,
              children[i] + ".Status.FailureMaxSenseVoltage:_address.._subindex", pos);
    dpSetWait(children[i] + ".Status.FailureMaxTerminalVoltage:_address.._datatype", 491,
              children[i] + ".Status.FailureMaxTerminalVoltage:_address.._subindex", pos);
  }
}




/** Function to remove the Krakow Marathon customizations

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param[in]   dpName            string: crate device dpname to set customizations
   @param[out]  exceptionInfo     dyn_string: returns details of any errors

   @return Nothing
*/
fwWiener_deleteKrakowMarathonCustomisation(string dpName, dyn_string &exceptionInfo)
{
  fwConfigConversion_delete(dpName + ".Status.OutputFailure", DPCONFIG_CONVERSION_RAW_TO_ENG_MAIN, exceptionInfo);
}






/** Function to change the Supervision Behaviour Types

   @par Constraints
       fwDevice/fwDevice.ctl, fwDevice/fwDeviceFrontEndConfigFile.ctl libraries

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param[in]   dpName            string: crate device dpname to set customizations
   @param[out]  exceptionInfo     dyn_string: returns details of any errors

   @return Nothing
*/
fwWiener_setSupervisionBehaviourOpcTypes(string dpName, dyn_string &exceptionInfo)
{
  const int OPC_INT16_DATATYPE = 481;

  dpSetWait(dpName + ".SupervisionBehavior.Settings.MaxSenseVoltage:_address.._datatype",    OPC_INT16_DATATYPE,
            dpName + ".SupervisionBehavior.Settings.MaxTerminalVoltage:_address.._datatype", OPC_INT16_DATATYPE,
            dpName + ".SupervisionBehavior.Settings.MaxCurrent:_address.._datatype",         OPC_INT16_DATATYPE,
            dpName + ".SupervisionBehavior.Settings.MaxTemperature:_address.._datatype",     OPC_INT16_DATATYPE,
            dpName + ".SupervisionBehavior.Settings.MinSenseVoltage:_address.._datatype",    OPC_INT16_DATATYPE,
            dpName + ".SupervisionBehavior.Settings.MaxPower:_address.._datatype",           OPC_INT16_DATATYPE,
            dpName + ".SupervisionBehavior.Settings.Timeout:_address.._datatype",            OPC_INT16_DATATYPE);
}

//@}
