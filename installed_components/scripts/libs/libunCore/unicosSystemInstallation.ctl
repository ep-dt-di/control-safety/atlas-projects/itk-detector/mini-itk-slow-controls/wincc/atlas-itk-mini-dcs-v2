// $License: NOLICENSE
/**@file

// unicosSystemInstallation.ctl
This library contains the function used to add or reset the user configuration.

@par Creation Date
  08/06/2010

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  H. Milcent (EN-ICE)
*/

//@{

//------------------------------------------------------------------------------------------------------------------------
// unicosSystemInstallation_addDefaultUser
/** add/reset default user, password set to "".
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param exceptionText  output, the error is returned here
*/
unicosSystemInstallation_addDefaultUser(string &exceptionText)
{
  _unicosSystemInstallation_addDefaultUser("_eFrJ/JQbSFE", "_eFrJ/JQbSFE", "_eFrJ/JQbSFE", "_eFrJ/JQbSFE", exceptionText);  //re-define the user with EMPTY password
}

//------------------------------------------------------------------------------------------------------------------------
// unicosSystemInstallation_setAccessControlSetup
/** configure the access control setup
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param exceptionInfo  output, the error is returned here
*/
unicosSystemInstallation_setAccessControlSetup(dyn_string &exceptionInfo)
{
  dyn_mixed dmAcConfiguration;
  
  fwAccessControl_getConfiguration(dmAcConfiguration,exceptionInfo);
  if(dynlen(exceptionInfo) <=0)
  {
    DebugTN("unCore.postInstall: reading access control configuration");
    dmAcConfiguration[fwAccessControl_CONFIG_AccessRight_DomainAdmin] = "SYSTEM:FwAccessCtrl";
    dmAcConfiguration[fwAccessControl_CONFIG_AccessRight_GroupAdmin]  = "SYSTEM:FwAccessCtrl";
    dmAcConfiguration[fwAccessControl_CONFIG_AccessRight_UserAdmin]   = "SYSTEM:FwAccessCtrl";
    DebugTN("unCore.postInstall: setting access control configuration");
    fwAccessControl_setConfiguration(dmAcConfiguration,exceptionInfo);
  }
  
}

//------------------------------------------------------------------------------------------------------------------------
// unicosSystemInstallation_upgrateToJCOPAccessControl
/** deprecated function: used when upgrading a unicos project with a very old access control.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param ex  output, the error is returned here
*/
unicosSystemInstallation_upgrateToJCOPAccessControl(string &ex)
{
  dyn_string dsUserName, dsPassword;
  int iPos;
  //maybe better to use _fwAccessControl_generateRandomPassword() to avoid empty passwords - need then Root action to set the correct passwords?  
  string sOper = "_eFrJ/JQbSFE", sMonitor = "_eFrJ/JQbSFE", sExpert = "_eFrJ/JQbSFE", sAdmin = "_eFrJ/JQbSFE";
  dyn_string exceptionInfo;
  
    DebugTN("unCore.postInstall: Updating access control in progress please wait");
    dpGet("_Users.UserName:_online.._value", dsUserName, "_Users.Password:_online.._value", dsPassword);
    iPos = dynContains(dsUserName, "operator");
    if(iPos > 0) 
      sOper = "";
    iPos = dynContains(dsUserName, "monitor");
    if(iPos > 0) 
      sMonitor = "";
    iPos = dynContains(dsUserName, "expert");
    if(iPos > 0) 
      sExpert = "";
    iPos = dynContains(dsUserName, "admin");
    if(iPos > 0) 
      sAdmin = "";
    _unicosSystemInstallation_addDefaultUser(sMonitor, sOper, sExpert, sAdmin, ex);
}

//------------------------------------------------------------------------------------------------------------------------
// _unicosSystemInstallation_addDefaultUser
/** add/reset default user, with password.
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sMonitor  input, the password for the monitor user
@param sOper  input, the password for the operator user
@param sExpert  input, the password for the expert user
@param sAdmin  input, the password for the admin user
@param ex  output, the error is returned here
*/
_unicosSystemInstallation_addDefaultUser(string sMonitor, string sOper, string sExpert, string sAdmin, string &ex)
{
string acComponent="fwAccessControl";
string acVersion="3.2.9";
int acInstalled;
dyn_string exceptionInfo;
bool bIntegratedMode;

    fwInstallation_componentInstalled(acComponent, acVersion, acInstalled);
    if (!acInstalled)
      {
      ex="ERROR: Access Control component " + acVersion + " not installed. Default UNICOS users not set!";
      DebugTN("UNICOS addDefaultUser: " + ex);
      return;
      }

    bIntegratedMode=_fwAccessControl_integratedMode();
    if(bIntegratedMode)
      {
      DebugTN("unCore.postInstall UNICOS users: fwAccessControl mode activated, the changes are automatically populated from the AC server");    
      return;
      }
          
    DebugTN("unCore.postInstall: Installing default UNICOS users, domain, group, please wait...");
        
    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddDomain("UNICOS",
    		 makeDynString("monitor","operator","expert","admin"),
    		 exceptionInfo,
    		 "UNICOS",
    		 "UNICOS domain");

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddGroup("operator",
    		 makeDynString("SYSTEM:Visualize","SYSTEM:Normal user level","SYSTEM:Extended user level","SYSTEM:Administration","SYSTEM:Acknowledge","UNICOS:monitor","UNICOS:operator"),
    		 exceptionInfo,
    		 "operator",
    		 "UNICOS operator group");

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddGroup("monitor",
    		 makeDynString("SYSTEM:Visualize","SYSTEM:Normal user level","SYSTEM:Extended user level","SYSTEM:Administration","SYSTEM:Acknowledge","UNICOS:monitor"),
    		 exceptionInfo,
    		 "monitor",
    		 "UNICOS monitor group");

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddGroup("expert",
    		 makeDynString("SYSTEM:Visualize","SYSTEM:Normal user level","SYSTEM:Extended user level","SYSTEM:Administration","SYSTEM:Acknowledge","UNICOS:monitor","UNICOS:operator","UNICOS:expert"),
    		 exceptionInfo,
    		 "expert",
    		 "UNICOS expert group");

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddGroup("admin",
    		 makeDynString("SYSTEM:Visualize","SYSTEM:Normal user level","SYSTEM:Extended user level","SYSTEM:Administration","SYSTEM:Acknowledge","UNICOS:monitor","UNICOS:operator","UNICOS:expert","UNICOS:admin"),
    		 exceptionInfo,
    		 "admin",
    		 "UNICOS admin group");

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddUser("operator",
    		 makeDynString("operator"),
    		 exceptionInfo,
    		 "operator",
    		 "UNICOS operator user name",
    		 sOper,
    		 TRUE,
    		 3072,
    		 TRUE,
         TRUE);

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddUser("monitor",
    		 makeDynString("monitor"),
    		 exceptionInfo,
    		 "monitor",
    		 "UNICOS monitor user name",
    		 sMonitor,
    		 TRUE,
    		 2,
    		 TRUE,
         TRUE);

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddUser("expert",
    		 makeDynString("expert"),
    		 exceptionInfo,
    		 "expert",
    		 "UNICOS expert user name",
    		 sExpert,
    		 TRUE,
    		 3,
    		 TRUE,
         TRUE);

    if (!dynlen(exceptionInfo)) fwAccessControl_checkAddUser("admin",
    		 makeDynString("admin"),
    		 exceptionInfo,
    		 "admin",
    		 "UNICOS admin user name",
    		 sAdmin,
    		 TRUE,
    		 4,
    		 TRUE,
         TRUE);
  
    if(dynlen(exceptionInfo) > 0)
      {
      DebugTN("unCore.postInstall: ERROR UNICOS addDefaultUser: " + exceptionInfo);
      ex = "ERROR: JCOP access control setup failed, please contact UNICOS support!";
      }
}

//------------------------------------------------------------------------------------------------------------------------

//@}
