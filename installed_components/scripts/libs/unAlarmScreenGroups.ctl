/**
  @file unAlarmScreenGroups.ctl
  
  @par Description:
  This library contains the function to manage groups in the UNICOS alarm screen. It is a distinct file to make the code clearer.
  A group can be created when saving a filter by checking the box "Add to quick list". A group is a dyn_anytype whose content can be accessed with the UNALARMSCREEN_GROUPS_INDEX_xxx variables.
   - UNALARMSCREEN_GROUPS_INDEX_ID          : integer         - The ID of the group. It is used as a name for the group DP.
   - UNALARMSCREEN_GROUPS_INDEX_CHILDREN    : dyn_dyn_anytype - The list of all sub-groups. See "Group hierarchy" below. Each element is access with the same UNALARMSCREEN_GROUPS_INDEX_xxx variables.
   - UNALARMSCREEN_GROUPS_INDEX_IS_CHILD    : bool            - Indicates whether or not the group is a sub-group. See "Group hierarchy" below. 
   - UNALARMSCREEN_GROUPS_INDEX_FILTER_REF  : string          - The filter to which this group is linked. A group doesn't define any filter, it only knows to which UNICOS alarm filter it is linked.
   - UNALARMSCREEN_GROUPS_INDEX_TITLE       : string          - The name of this group. It will be displayed in the widget.
   - UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME   : time            - The time this group has been declared as "seen". It is some kind of acknowledging outside of PVSS internals. Its behaviour could be changed at the developer's will.
  
  ------------------------
  --- Group hierarchy: ---
  ------------------------
  Groups can be sorted hierarchically: when creating a group it is possible to select a parent-group. 
  The result of this is that by default a group that has a parent (called sub-group or child-group) will not be displayed in the alarm screen. Instead, a button will appear on the parent group to show all sub-groups.
  There is only one level of hierarchy, which means a child can not have children.
  
  ---------------------
  --- Group widget: ---
  ---------------------
  If you want to create your own group widget, here is what you need:
    - Variables in the scopelib (it is important to use this name and type):
      - const bool g_alertRow = false;
      - int iAlarmCount;
      - bool bSubGroupsVisible;
      - int iMutexSync;
      - bool bDpQueryConnected;
      - mapping g_mAlertStateInfo;
    - Internal widgets: you can use any of the widgets defined by the constants UNALARMSCREEN_GROUPS_WIDGET_xxx. What they do is explained in the comments.
      Only two are mandatory: 
        - UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE: This rectangle has to represent the size of the widget to calculate how much space to leave between groups. 
          Note: it would have been possible to directly get the size of the panel but it is not always to its minimal size (for development purpose) so not correct.
        - UNALARMSCREEN_GROUPS_WIDGET_SUBGROUP_LIST: This list contains all the identifiers of sub-groups to interact with them.
      The others are not technically mandatory, but it wouldn't make sense to remove the group name or the alarm count.
  It is also possible to drag&drop a widget to reorder the groups. For that, you can use the functions _unAlarmScreen_groups_groupDragXXX.
  
  What is foreseen as future development, besides the open JIRA cases:
   1) Change the system connection management. Use the fwAlarmScreenGeneric system connected/disconnected functions instead of disconnecting/reconnecting all the queries. 
   2) Move all the management to a separate control manager, like in PSEN. A separate manager would do all the dpQueryConnect once at project start-up and signal the information to the group widgets. 
    On big systems the dpQueryConnect at start-up blocks the event manager, with a separate script it would do it only when starting the project, which is not a problem.
    The alarms callback function doesn't need a big update, the only difference is that the mapping will be common to all groups, unlike now where it is declared in each group.
    A bigger mapping would be needed, where each element would be the mapping of each group. A mutex should synchronize the different callbacks.
   3) Move everything to fwAlarmScreenGeneric. A few things would have to change, besides variable and graphical elements renaming:
     - Getting the dpQuery will have to be generic with an evalScript, since the structure of the filter is different and the filter DP doesn't have the same name on every alarm panel.
     - Applying a filter from the group would have to be generic as well.
     - Saving a group would have to be generic to retrieve the correct list of filters for the current alarm panel.
    
  Once this has been done, the groups could be adapted to be used in JCOP, UNICOS, PSEN, and any future alarm panel, by just creating a group widget for the needs of the panel.
  
  
  Why should this be used instead of JCOP groups?
   - The code is much cleaner and easier to maintain.
   - More advanced features (hierarchy, easy reordering, easy to create a group, ...).
   - The groups are managed internally. 
    In JCOP, to monitor the groups an infinite loop checks the content of each line. If for some reason (filtering) not all lines are visible, the groups are not the reflection of the real state of the system.
    Here it doesn't matter, there could be no alarm in the list and the groups would still be up to date.
    Moreover, with JCOP groups when more than lots of alarms are active the UI uses 100% of the CPU core.
    It would actually even be possible to adapt the code to allow the use of groups independently in a separate panel. It would require a few changes, easily doable with generic (exec/eval script) function calls.
    Only the application of a filter would not be possible. One could think of adapt that to fetch the alarms of that group and show them manually.
  
  
  @par Creation Date:
	August 2013

  @par Modification History:
  
  31/08/2017    | Jean-Charles Tournier | IS-1953 Prevent click and right click actions when groups are placed on process panels 
                                                  and provide hooks to handle click and right click action in the future
  23/03/2015    | Jean-Charles Tournier | IS-1410 fixing problem with alarm group calculation 
  02/12/2013    | Cyril Caillaba        | Fully working and complete version. Release candidate.
  20/08/2013    | Cyril Caillaba        | First operational beta-version.

  @author 
	Cyril Caillaba (EN-ICE-SCD)

  UNICOS
   Copyright CERN 2013 all rights reserved
*/

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// ----------------------------- CONSTANTS ------------------------------
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
#uses "fwAlarmHandling/fwAlarmScreenGeneric.ctl"

// Datapoints.
const string  UNALARMSCREEN_GROUPS_DP_PREFIX                    = "_unAlertPanel_FilterGroup_";                                 // The prefix to name a group DP. PrefixXXXX where XXXX is the group ID on 4 digits.
const string  UNALARMSCREEN_GROUPS_DPTYPE                       = "_UnAlertPanel_FilterGroup";                                  // The DP type of this DP.
const string  UNALARMSCREEN_GROUP_SETUP_DP                      = "_unAlertPanel_FilterGroupSetup";                             // The DP with settings for all groups.
const string  UNALARMSCREEN_GROUP_SETUP_DPTYPE                  = "_UnAlertPanel_FilterGroupSetup";                             // The DP type for this DP.
const string  UNALARMSCREEN_GROUP_SETUP_SORTING_DP              = "_unAlertPanel_FilterGroupSetup.sorting";                     // The list of groups in the order they are supposed to be displayed.
const string  UNALARMSCREEN_GROUP_SETUP_DIST_CB_DP              = "_unAlertPanel_FilterGroupSetup.distributedCallback";         // The DP that will trigger the callback for system state change.
const string  UNALARMSCREEN_GROUP_SETUP_GROUP_WIDGET            = "_unAlertPanel_FilterGroupSetup.groupWidget";                 // The widget that has to be used for a group.
const string  UNALARMSCREEN_GROUP_SETUP_SHOW_GROUPS             = "_unAlertPanel_FilterGroupSetup.showGroups";                  // Indicates whether or not the groups should be visible.
const string  UNALARMSCREEN_GROUPS_DPE_TITLE                    = ".title";                                                     // DPE group title            - string
const string  UNALARMSCREEN_GROUPS_DPE_CHILDREN                 = ".children";                                                  // DPE group children ID list - dyn_int
const string  UNALARMSCREEN_GROUPS_DPE_IS_CHILD                 = ".isChild";                                                   // DPE group is a child       - bool
const string  UNALARMSCREEN_GROUPS_DPE_FILTER_REF               = ".filterRef";                                                 // DPE group reference filter - string
const string  UNALARMSCREEN_GROUPS_DPE_SEEN_TIME                = ".seenTime";                                                  // DPE group seen time        - time
const string  UNALARMSCREEN_GROUPS_DPE_DIST_SYSTEM_IDS          = "_DistConnections.Dist.ManNums";                              // DPE with the list of connected system IDs - dyn_int
  
// Group structure.   
const int     UNALARMSCREEN_GROUPS_INDEX_ID                     = 1;                                                            // Group ID               - int
const int     UNALARMSCREEN_GROUPS_INDEX_CHILDREN               = 2;                                                            // Group children list    - dyn_dyn_anytype (recursive)
const int     UNALARMSCREEN_GROUPS_INDEX_IS_CHILD               = 3;                                                            // Group is child         - bool
const int     UNALARMSCREEN_GROUPS_INDEX_FILTER_REF             = 4;                                                            // Group reference filter - string
const int     UNALARMSCREEN_GROUPS_INDEX_TITLE                  = 5;                                                            // Group title            - string
const int     UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME              = 6;                                                            // Group seen time        - string
  
// Individual widgets inside the group widget.    
const string  UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE            = "unAlarmScreen_groups_groupSize";                             // Rectangle to dynamically get the size of the group.
const string  UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE           = "unAlarmScreen_groups_groupTitle";                            // Text to display the name of the group.
const string  UNALARMSCREEN_GROUPS_WIDGET_ALARM_COUNT           = "unAlarmScreen_groups_alarmCount";                            // Text to display the number of active alarm for this group.
const string  UNALARMSCREEN_GROUPS_WIDGET_UNACK_ALARM_COUNT     = "unAlarmScreen_groups_unackAlarmCount";                       // Text to display the number of unacked active alarms for this group.
const string  UNALARMSCREEN_GROUPS_WIDGET_NEW_ALARM_COUNT       = "unAlarmScreen_groups_newAlarmCount";                         // Text to display the number of new alarms.
const string  UNALARMSCREEN_GROUPS_WIDGET_LAST_ALARM_DATE       = "unAlarmScreen_groups_lastAlarmDate";                         // Text to display the date of the most recent alarm of this group.
const string  UNALARMSCREEN_GROUPS_WIDGET_INFORMATION_TEXT      = "unAlarmScreen_groups_infoText";                              // Text to display any additional information.
const string  UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS       = "unAlarmScreen_groups_showSubGroups";                         // Button to show/hide the sub-groups.
const string  UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS_BG    = "unAlarmScreen_groups_showSubGroups_bg";                      // Background of button to show/hide the sub-groups.
const string  UNALARMSCREEN_GROUPS_WIDGET_ALARM_BACKGROUND      = "unAlarmScreen_groups_alarmColourBg";                         // Background with the colour of the alarm.
const string  UNALARMSCREEN_GROUPS_WIDGET_SUBGROUP_LIST         = "unAlarmScreen_groups_subGroupsList";                         // List of the sub-groups.
  
// Global widgets for group display.  
const string  UNALARMSCREEN_GROUPS_GROUP_WIDGET_CURRENT_PANEL   = "unAlarmScreen_groups_groupWidget";                           // Widget to store the panel file to use for a widget.
const string  UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET             = "rectangleGroupPosition";                                     // Widget to indicate the position where to add the next group.
const string  UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET         = "rectangleSubGroupPosition";                                  // Widget to indicate the position where to add the next sub-group.
const string  UNALARMSCREEN_GROUPS_GROUP_POS_ORIG               = "rectangleGroupOriginalPos";                                  // Widget to indicate the original coordinates of the groups.
const string  UNALARMSCREEN_GROUPS_GROUP_HIGHLIGHT              = "unAlarmScreen_groups_rectangleGroupHighlight";               // Widget to indicate the original which group is currently selected.
const string  UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT          = "unAlarmScreen_groups_rectangleSubGroupHighlight";            // Widget to indicate the original which sub-group is currently selected.
const string  UNALARMSCREEN_GROUPS_NO_GROUP_TEXT                = "unAlarmScreen_groups_noGroupInfoText";                       // Widget to signal that there is no group available now.
const string  UNALARMSCREEN_GROUPS_GROUPS_DISABLED_TEXT         = "unAlarmScreen_groups_groupsDisabledInfoText";                // Widget to signal that groups are currently disabled.

// dpQuery array constants.
const int     UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_DPE       = 1;                                                            // Group dpQuery result index: alarm DPE          - string.
const int     UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_TIME      = 2;                                                            // Group dpQuery result index: alarm time         - atime.
const int     UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_STATE     = 3;                                                            // Group dpQuery result index: alarm state        - int.
const int     UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_ABBR      = 4;                                                            // Group dpQuery result index: alarm abbreviation - string.
const int     UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_PRIO      = 5;                                                            // Group dpQuery result index: alarm priority     - int.
const int     UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_ACK       = 6;                                                            // Group dpQuery result index: alarm ack state    - int.

// Content of each element of the mapping storing information on alarms.
const int     UNALARMSCREEN_ALARM_STATE_INDEX_TIME              = 1;                                                            // Alarm time           - time.
const int     UNALARMSCREEN_ALARM_STATE_INDEX_PRIORITY          = 2;                                                            // Alarm priority       - int.
const int     UNALARMSCREEN_ALARM_STATE_INDEX_ACTIVE            = 3;                                                            // Alarm active or not  - bool.
const int     UNALARMSCREEN_ALARM_STATE_INDEX_ACK               = 4;                                                            // Alarm acked          - bool.
const int     UNALARMSCREEN_ALARM_STATE_INDEX_COLOUR            = 5;                                                            // Alarm colour         - string.

// Configuration constants.
const string  UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_FOLDER     = "vision/unAlertPanel/";                                       // The folder where group widget panel files are stored.
const string  UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_PREFIX     = "unAlertScreenGroups_group_";                                 // The name of a group widget panel file.
const string  UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_EXTENSION  = ".pnl";                                                       // The extension of a group widget panel file.

// Other constants.
const string  UNALARMSCREEN_GROUPS_GROUP_WIDGET_PANEL_DEFAULT   = "vision/unAlertPanel/unAlertScreenGroups_group_Default.pnl";  // The panel to display for a group widget.
const string  UNALARMSCREEN_GROUPS_CONFIG_PANEL                 = "vision/unAlertPanel/unAlertScreenGroups_config.pnl";         // The panel to configure the groups.
const string  UNALARMSCREEN_GROUPS_LABEL_INIT                   = "Initializing...";                                            // Label to show when initializing the group, before knowing the exact count.
const string  UNALARMSCREEN_GROUPS_LABEL_NO_ALARM               = "No active alarm";                                            // Label to show when no alarm is active.
const string  UNALARMSCREEN_GROUPS_DATE_STRING_FORMAT           = "%Y.%m.%d - %H:%M:%S";                                        // Format of the last alarm date.
const string  UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE              = "_________ROW_BREAK_________";                                // Filter title to indicate a row break.
const string  UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF             = "UNALARMGROUP_REF_";                                          // Prefix to use for a reference to a (sub-)group widget. RefXXX where XXXX is the group ID on 4 digits.
const string  UNALARMSCREEN_GROUPS_DPE_FORMAT                   = "%04d";                                                       // Format of the group ID when used as a string (4 digits with zeros).
const string  UNALARMSCREEN_GROUPS_NO_ALARM_COLOUR              = "unAlarm_Ok";                                                 // Colour of a group widget when there is no alarm.
const string  UNALARMSCREEN_GROUPS_NON_INIT_COLOUR              = "grey";                                                       // Colour of a group widget before it is initialized.
const int     UNALARMSCREEN_GROUPS_CALLBACK_TIMEOUT             = 3000;                                                         // Time-out for the dpQueryConnect.
const string  UNALARMSCREEN_GROUPS_SHOW_SUB_GROUPS_ICON_FILL    = "[pattern,[tile,any,arrow_single_right_16.png]]";             // Icon to show when sub-groups are hidden.
const string  UNALARMSCREEN_GROUPS_HIDE_SUB_GROUPS_ICON_FILL    = "[pattern,[tile,any,arrow_single_left_16.png]]";              // Icon to show when sub-groups are visible.
const string  UNALARMSCREEN_GROUPS_DRAG_ICON_START              = "StandardIcons/Insert_before_32.png";                         // Icon to show when dragging a group.
const string  UNALARMSCREEN_GROUPS_DOLLAR_PARAM_GROUP_ID        = "$iGroupId";                                                  // Dollar parameter for the group ID.
const float   UNALARMSCREEN_GROUPS_SUB_GROUP_SCALE              = 0.8;                                                          // The scale with which to display the sub-groups.
const float   UNALARMSCREEN_GROUPS_EMPTY_GROUP_LIST_HEIGHT      = 56;                                                           // Height of the area in which the groups are displayed when there is no group.
const string  UNALARMSCREEN_GROUPS_GROUP_ACTIVE_LABEL           = "Displayed group: %s";                                        // Label to indicate a group is applied to the AES.
const int     UNALARMSCREEN_GROUPS_LOOP_SLEEP_CONDITION         = 300;                                                          // Number of loop after which to sleep for one millisecond to let the UI refresh.



//global variable storing the alert state filter of the group
//it is stored as a global variable in order to haev it directly
//during the callback and thus to avoid having to do a dpGet in the CB
//key->groupId, value->alert state filter value
mapping gUnAlarmScreenFilterAlertState; 

//buffer time for the dpQueryConnectSingle when registering the alarm groups
const int UNALARM_SCREEN_GROUP_DPQUERY_BUFFERTIME = 500;

// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------- Public functions -------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------
// ---------------------------- GUI functions ---------------------------
// ----------------------------------------------------------------------

/**
  @par Description:
  Show the given group.
  
  @par Usage:
  Public.
  
  @param  daGroup dyn_anytype input,  The group to show.
*/
void unAlarmScreen_groups_showGroup(const dyn_anytype daGroup)
{
  int iPosX;
  int iPosY;  
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "position", iPosX, iPosY); 
  
  int iSpaceX;
  int iSpaceY;
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "size", iSpaceX, iSpaceY); 
  
  int iSubGroupSpaceX;
  int iSubGroupSpaceY;
  getValue(UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET, "size", iSubGroupSpaceX, iSubGroupSpaceY);   
  
  int iPosXSource;
  int iPosYSource;
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_ORIG, "position", iPosXSource, iPosYSource);   
    
  // Max X position before switching to new line:
  // The frame "frameTableFilter" marks the limit. Its size is not fixed, it can be extended or reduced.
  int iFrameX;
  int iFrameY;  
  getValue("frameTableFilter", "position", iFrameX, iFrameY);
  
  int iFrameWidth;
  int iFrameHeight;
  getValue("frameTableFilter", "size", iFrameWidth, iFrameHeight);
  
  const int iMaxX = iFrameX + iFrameWidth - iSpaceX - 5; // -5 to leave a bit of space.
  const int iMaxXSubGroup = iFrameX + iFrameWidth - iSubGroupSpaceX - 5;
    
  if (daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE] == UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE)
  {
    iPosX = iPosXSource;
    iPosY += iSpaceY;
  }
  else if (!daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
  {    
    if (iPosX > iMaxX) // New line
    {
      iPosX = iPosXSource;
      iPosY += iSpaceY;
    }
    
    string sGroupId;
    sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
    
    bool bHasChildren = dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) > 0;
    
    // Show "show sub-groups" button if needed.
    _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS,    "visible",  bHasChildren);
    _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS_BG, "visible",  bHasChildren);
      
    // If the group has children it is alone on its line (jump line before and after)
    if (bHasChildren)
    {
      if (iPosX != iPosXSource) // Additional line unless it is the first element
      {
        iPosX = iPosXSource;
        iPosY += iSpaceY;
      }
      
      // If the widget is already here just move it to the right position.
      // Otherwise add it.
      bool bAlreadyExists;
      if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId))
      {
        setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId, "position", iPosX, iPosY);
        bAlreadyExists = true;
      }
      else
      {      
        addSymbol(
            myModuleName(), 
            myPanelName(), 
            _unAlarmScreen_groups_getCurrentPanel(), 
            UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId, 
            makeDynString(UNALARMSCREEN_GROUPS_DOLLAR_PARAM_GROUP_ID + ":" + daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]), 
            iPosX, 
            iPosY, 
            0, 
            1, 
            1
          );
        
        bAlreadyExists = false;
      }
      
      
      // Add sub-groups but invisible at first.
      int iSubGroupPosX = iPosX + iSpaceX;
      int iSubGroupPosY = iPosY;
      setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET, "position", iSubGroupPosX, iSubGroupPosY);
      for (int i = 1 ; i <= dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) ; i++)
      {
        getValue(UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET, "position", iSubGroupPosX, iSubGroupPosY);   
      
        if (iSubGroupPosX > iMaxXSubGroup) // Cannot be more than one line of sub-groups: display an error message
        {
          dyn_string dsExceptions;
          fwException_raise(dsExceptions, "", "Too many sub-groups to display. Expand the window to increase the maximum displayable items.","");	
          fwExceptionHandling_display(dsExceptions);
          break;
        }
          
        dyn_anytype daSubGroup = daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN][i];
        
        unAlarmScreen_groups_showSubGroup(daSubGroup, daGroup, !bAlreadyExists);
      }
      
      iPosY += iSpaceY;
    }
    else
    {
      // If the widget is already here just move it to the right position.
      // Otherwise add it.
      if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId))
      {
        setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId, "position", iPosX, iPosY);
      }
      else
      {
        addSymbol(
            myModuleName(), 
            myPanelName(), 
            _unAlarmScreen_groups_getCurrentPanel(), 
            UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId, 
            makeDynString(UNALARMSCREEN_GROUPS_DOLLAR_PARAM_GROUP_ID + ":" + daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]), 
            iPosX, 
            iPosY, 
            0, 
            1, 
            1
          );
      }
        
      iPosX += iSpaceX;
    }
    
    listVisibleGroups.appendItem(sGroupId);    
  }
    
  setValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "position", iPosX, iPosY); 
}

/**
  @par Description:
  Show a sub-group.
  
  @par Usage:
  Public.
  
  @param  daGroup       dyn_anytype input,  The sub-group to show.
  @param  daParentGroup dyn_anytype input,  The parent of this group.
  @param  bHide         bool input,         Indicates whether or not to hide the widget after added.
*/
void unAlarmScreen_groups_showSubGroup(const dyn_anytype daSubGroup, const dyn_anytype daParentGroup, bool bHide = true)
{
  string sSubGroupId;
  sprintf(sSubGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, daSubGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
  
  string sGroupId;
  sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, daParentGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);    
  
  int iSubGroupPosX;
  int iSubGroupPosY;
  getValue(UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET, "position", iSubGroupPosX, iSubGroupPosY);    

  // To identify if sub-groups of the same parent-groups are already visible or not, we look at the pattern of the button.
  bool bSubGroupsVisibleLocal;
  if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS))
  {
    string sFill;
    getValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS, "fill", sFill);
    bSubGroupsVisibleLocal = UNALARMSCREEN_GROUPS_HIDE_SUB_GROUPS_ICON_FILL == sFill;
  }
  
  
  // If the widget is already here just move it to the right position.
  // Otherwise add it.
  if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sSubGroupId))
  {
    setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sSubGroupId, "position", iSubGroupPosX, iSubGroupPosY);
  }
  else
  {     
    addSymbol(
        myModuleName(), 
        myPanelName(), 
        _unAlarmScreen_groups_getCurrentPanel(), 
        UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sSubGroupId, 
        makeDynString(UNALARMSCREEN_GROUPS_DOLLAR_PARAM_GROUP_ID + ":" + daSubGroup[UNALARMSCREEN_GROUPS_INDEX_ID]), 
        iSubGroupPosX, 
        iSubGroupPosY, 
        0, 
        UNALARMSCREEN_GROUPS_SUB_GROUP_SCALE, 
        UNALARMSCREEN_GROUPS_SUB_GROUP_SCALE
      );
  }
          
  if (bHide || !bSubGroupsVisibleLocal)
  {
    setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sSubGroupId, "visible", false);    
  }
    
  setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SUBGROUP_LIST, "appendItem", UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sSubGroupId);
  
  listVisibleGroups.appendItem(sSubGroupId);
  
  int iSubGroupSpaceX;
  int iSubGroupSpaceY;
  getValue(UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET, "size", iSubGroupSpaceX, iSubGroupSpaceY);     
  setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET, "position", iSubGroupPosX + iSubGroupSpaceX, iSubGroupPosY);  
}

/**
  @par Description:
  Show all the groups.
  
  @par Usage:
  Public.
*/
void unAlarmScreen_groups_showGroups()
{
  // Ignore if table filter is reduced (can happen when reloading user config on start-up).
  if (fwAlarmScreenGeneric_isTableFilterReduced())
  {
    return;
  }
  
  if (!unAlarmScreen_groups_config_getShowGroups())
  {
    fwAlarmScreenGeneric_showTableFilter(false);
    setValue(UNALARMSCREEN_GROUPS_NO_GROUP_TEXT, "visible", false);  
    _fwAlarmScreenGeneric_rearrangeScreen();  
    return;
  }
  else
  {    
    setValue(UNALARMSCREEN_GROUPS_GROUPS_DISABLED_TEXT, "visible", false);
  }
  
  int iPosXSource;
  int iPosYSource;
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_ORIG, "position", iPosXSource, iPosYSource);     
  setValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "position", iPosXSource, iPosYSource); 
  
  dyn_dyn_anytype ddaAllGroups = unAlarmScreen_groups_getAllGroups(true);
  listVisibleGroups.deleteAllItems();
  // Remove all row break at the end of the list.
  // Ignore sub-groups.
  int i = dynlen(ddaAllGroups);
  while ((i > 0) && ((UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE == ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_TITLE]) || (ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])))
  {
    if (!ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
    {
      unAlarmScreen_groups_deleteGroup(ddaAllGroups[i]);
      dynRemove(ddaAllGroups, i);
    }
    
    i--;
  } 
   
  // Show groups one by one
  for (int i = 1 ; i <= dynlen(ddaAllGroups) ; i++)
  {
    unAlarmScreen_groups_showGroup(ddaAllGroups[i]);
  }
  
  if (dynlen(listVisibleGroups.items()) > 0)
  {
    setValue(UNALARMSCREEN_GROUPS_NO_GROUP_TEXT, "visible", false);
  }
  else
  {
    setValue(UNALARMSCREEN_GROUPS_NO_GROUP_TEXT, "visible", true);  
  }
    
  
  // Re-highlight the previously selected group if any.
  if ("" != selectedGroup.text())
  {
    unAlarmScreen_groups_highlightGroup(selectedGroup.text());
  }  
  
  unAlarmScreen_groups_adjustFrameSize();
}

/**
  @par Description:
  Remove all the group widgets.
  
  @par Usage:
  Public.
*/
void unAlarmScreen_groups_removeGroups()
{
  dyn_string dsGroupWidgets = listVisibleGroups.items();
  
  for (int i = 1 ; i <= dynlen(dsGroupWidgets) ; i++)
  {
    if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + dsGroupWidgets[i]))
    {      
      removeSymbol(myModuleName(), myPanelName(), UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + dsGroupWidgets[i]);
    }
  }  
  
  listVisibleGroups.deleteAllItems();
  
  int iPosXSource;
  int iPosYSource;
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_ORIG, "position", iPosXSource, iPosYSource);     
  setValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "position", iPosXSource, iPosYSource); 
  
  unAlarmScreen_groups_adjustFrameSize();
}

/**
  @par Description:
  Highlight the group (when right-click->Show alarms or widget is clicked).
  
  @par Usage:
  Public.
  
  @param iGroupId integer input,  The ID of the group clicked.
*/
void unAlarmScreen_groups_highlightGroup(const int iGroupId)
{
  string sGroupId;
  sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, iGroupId);
  
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup(iGroupId);
  
  
  if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId) && (dynlen(daGroup) > 0))
  {  
    int iPosX;
    int iPosY;
    getValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId, "position", iPosX, iPosY);
    
    bool bVisible;    
    getValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE, "visible", bVisible);
    
    setValue(UNALARMSCREEN_GROUPS_GROUP_HIGHLIGHT, "position", iPosX, iPosY);  
    setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT, "position", iPosX, iPosY);  
    
    setValue(UNALARMSCREEN_GROUPS_GROUP_HIGHLIGHT, "visible", bVisible && !daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD]);
    setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT, "visible", bVisible && daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD]);
    
    selectedGroup.text(iGroupId);
  }
  else
  {
    unAlarmScreen_groups_clearHighlight();
  }
}

/**
  @par Description:
  Clear the group highlighting (when manually changing the filter after selecting a group or cancelling selection).
  
  @par Usage:
  Public.
*/
void unAlarmScreen_groups_clearHighlight()
{
  setValue(UNALARMSCREEN_GROUPS_GROUP_HIGHLIGHT, "visible", false);
  setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT, "visible", false);
  selectedGroup.text("");
}

/**
  @par Description:
  Adjust the size of the quick list frame. To use when adding or removing a group can change the size of the frame.
  
  @par Usage:
  Internal.
*/
void unAlarmScreen_groups_adjustFrameSize()
{
  int iPosX;
  int iPosY;  
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "position", iPosX, iPosY); 
  
  int iSpaceX;
  int iSpaceY;
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "size", iSpaceX, iSpaceY);   
  
  int iPosXSource;
  int iPosYSource;
  getValue(UNALARMSCREEN_GROUPS_GROUP_POS_ORIG, "position", iPosXSource, iPosYSource);   
    
  int iFrameWidth;
  int iFrameHeight;
  getValue("frameTableFilter", "size", iFrameWidth, iFrameHeight);
    
  int iFramePosX;
  int iFramePosY;
  getValue("frameTableFilter", "position", iFramePosX, iFramePosY);
  
  int iYBottom;
  if (iPosX == iPosXSource) // Next group is on a new line, line is empty for now.
  {
    iYBottom = iPosY;
  }
  else
  {
    iYBottom = iPosY + iSpaceY;
  }  
  int iNewHeight = iYBottom - iFramePosY;
  
  // Set minimum height for quick filter menu
  if(iNewHeight < UNALARMSCREEN_TABLEFILTER_HEIGHT)
  {
    iNewHeight = UNALARMSCREEN_TABLEFILTER_HEIGHT;
  }
  
  
  if (0 == dynlen(listVisibleGroups.items()))
  {
    bool bNoGroupTextVisible;
    bool bGroupsDisabledTextVisible;
    getMultiValue(
        UNALARMSCREEN_GROUPS_NO_GROUP_TEXT, "visible", bNoGroupTextVisible,
        UNALARMSCREEN_GROUPS_GROUPS_DISABLED_TEXT, "visible", bGroupsDisabledTextVisible
      );

    if (bNoGroupTextVisible || bGroupsDisabledTextVisible)
    {
      iNewHeight = UNALARMSCREEN_GROUPS_EMPTY_GROUP_LIST_HEIGHT;
    }
    else
    {
      iNewHeight = FW_ALARM_GENERIC_FRAME_REDUCED_HEIGHT;
    }
  }
  
  setValue("frameTableFilter", "size", iFrameWidth, iNewHeight);
  _fwAlarmScreenGeneric_rearrangeScreen();  
}


// ----------------------------------------------------------------------
// --------------------- Group management functions ---------------------
// ----------------------------------------------------------------------

/**
  @par Description:
  Remove the widget of the given group.
  
  @par Usage:
  Public.
  
  @par daGroup  dyn_anytype input,  The group to remove.
*/
void unAlarmScreen_groups_removeGroup(const dyn_anytype daGroup)
{ 
  // WARNING: the removeSymbol will destroy the widget and stop the script. It must be the last action.
  
  string sGroupId;
  sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
  
  dyn_string dsGroupWidgets = listVisibleGroups.items();  
  
  int iPos = dynContains(dsGroupWidgets, sGroupId);
  
  if (iPos > 0)
  {
    dynRemove(dsGroupWidgets, iPos);
  }
  
  listVisibleGroups.deleteAllItems();
  listVisibleGroups.items(dsGroupWidgets);
 
  // Remove sub groups
  dyn_string dsSubGroupList;
  getValue(UNALARMSCREEN_GROUPS_WIDGET_SUBGROUP_LIST, "items", dsSubGroupList);
  for (int i = 1 ; i <= dynlen(dsSubGroupList) ; i++)
  {
    if (shapeExists(dsSubGroupList[i]))
    {
      removeSymbol(myModuleName(), myPanelName(), dsSubGroupList[i]);
    }
  } 

  if (dynlen(listVisibleGroups.items()) <= 0)
  {
    setValue(UNALARMSCREEN_GROUPS_NO_GROUP_TEXT, "visible", true);  
  }
  
  if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId))
  {
    removeSymbol(myModuleName(), myPanelName(), UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId);
  }
}

/**
  @par Description:
  Create a group with the given information.
  
  @par Usage:
  Public.
  
  @param  sReferenceFilter  string input, The filter linked to this group. It will also be the group name.
  @param  sParentGroup      string input, The parent filter. None by default.
  
  @return The created group.
*/
dyn_anytype unAlarmScreen_groups_createGroup(const string sReferenceFilter, string sParentGroup = "")
{ 
  dyn_anytype daGroup;

  // "None" means no parent selected imho ""
  if(sParentGroup == "None")
  {
    sParentGroup = "";
  }
  
  // Try to set a filter as its own child: error!
  // Should never get here, unless a developer changed a panel.
  if (sReferenceFilter == sParentGroup)
  {
    return makeDynAnytype();
  }

  // Find first available DP name (or DP with the same reference filter if exists)
  int iGroupId = 0;
  string sDpName;
  
  bool bDpOk = false;
  do
  {
    iGroupId++;
    
    string sGroupId;
    sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, iGroupId);
    
    sDpName = UNALARMSCREEN_GROUPS_DP_PREFIX + sGroupId;
    
    if (dpExists(sDpName))
    {
      string sFilterRef;
      dpGet(sDpName + UNALARMSCREEN_GROUPS_DPE_FILTER_REF, sFilterRef);
      
      if ((sReferenceFilter == sFilterRef) && (UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE != sReferenceFilter))
      {
        bDpOk = true;
      }    
    }
    else
    {
      bDpOk = true;
    }
    
  }
  while (!bDpOk);
  
  // Seen time
  time tSeenTime; // Default 1970.01.01
  
  daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]          = iGroupId;
  daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]    = makeDynInt();
  daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD]    = "" != sParentGroup;
  daGroup[UNALARMSCREEN_GROUPS_INDEX_FILTER_REF]  = sReferenceFilter;
  daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]       = sReferenceFilter;
  daGroup[UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME]   = tSeenTime;
  
  unAlarmScreen_groups_saveGroup(daGroup); 
  
  dyn_int diGroupOrder;
  dpGet(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);
  dynAppend(diGroupOrder, iGroupId);
  dynUnique(diGroupOrder);
  dpSetWait(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);

  // Update the children list in the parent group if needed.
  if ("" != sParentGroup) 
  {
    dyn_string dsDps = dpNames(UNALARMSCREEN_GROUPS_DP_PREFIX + "*", UNALARMSCREEN_GROUPS_DPTYPE);
    
    for (int i = 1 ; i <= dynlen(dsDps) ; i++)
    {
      string sDp = dsDps[i];
      
      string sFilterRef;
      string sTitle;
      dpGet(sDp + UNALARMSCREEN_GROUPS_DPE_FILTER_REF, sFilterRef);
      dpGet(sDp + UNALARMSCREEN_GROUPS_DPE_TITLE, sTitle);
      
      if (sParentGroup == sTitle)
      {  
        dyn_int diChildren;
        dpGet(sDp + UNALARMSCREEN_GROUPS_DPE_CHILDREN, diChildren);
        dynAppend(diChildren, iGroupId);
        dynUnique(diChildren);
        dynSort(diChildren);
        dpSet(sDp + UNALARMSCREEN_GROUPS_DPE_CHILDREN, diChildren);
      }
    }
  }

  return daGroup;
}

/**
  @par Description:
  Save the given group. Warning, everything is overwritten (if another group has the same ID it will be erased).
  
  @par Usage:
  Public.
  
  @param  daGroup dyn_anytype input,  The group to save.
*/
void unAlarmScreen_groups_saveGroup(const dyn_anytype daGroup)
{
  string sGroupId;
  sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
  
  
  string sGroupDp = UNALARMSCREEN_GROUPS_DP_PREFIX + sGroupId;
  if (!dpExists(sGroupDp))
  {
    dpCreate(sGroupDp, UNALARMSCREEN_GROUPS_DPTYPE);
  }
  
  dyn_int diChildren;
  for (int i = 1 ; i <= dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) ; i++)
  {
    dynAppend(diChildren, daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN][i][UNALARMSCREEN_GROUPS_INDEX_ID]);
  }
  
  dpSet(
      sGroupDp + UNALARMSCREEN_GROUPS_DPE_TITLE,      daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE],
      sGroupDp + UNALARMSCREEN_GROUPS_DPE_CHILDREN,   diChildren,
      sGroupDp + UNALARMSCREEN_GROUPS_DPE_IS_CHILD,   daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD],
      sGroupDp + UNALARMSCREEN_GROUPS_DPE_FILTER_REF, daGroup[UNALARMSCREEN_GROUPS_INDEX_FILTER_REF],
      sGroupDp + UNALARMSCREEN_GROUPS_DPE_SEEN_TIME,  daGroup[UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME]
    );
}

/**
  @par Description:
  Delete the given group. It doesn't delete the related saved filter.
  
  @par Usage:
  Public.
  
  @param iGroupId int input,  The ID of the group to delete.
*/
void unAlarmScreen_groups_deleteGroup(const dyn_anytype daGroup, bool bDeleteWidget = true)
{
  const int iGroupId = daGroup[UNALARMSCREEN_GROUPS_INDEX_ID];
  string sGroupId;
  sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, iGroupId);
  const string sGroupDp = UNALARMSCREEN_GROUPS_DP_PREFIX + sGroupId;
  
  // Delete the group DP.
  if (dpExists(sGroupDp))
  {
    string sRef;
    dpGet(sGroupDp + UNALARMSCREEN_GROUPS_DPE_FILTER_REF, sRef);
    if (dpExists(UNALARMSCREEN_DP_FILTER_PREFIX + sRef))
    {
      dpDelete(UNALARMSCREEN_DP_FILTER_PREFIX + sRef);
    }
    
    dpDelete(sGroupDp);
  }
  
  // Update parent group if necessary
  if (daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
  {
    dyn_string dsDps = dpNames(UNALARMSCREEN_GROUPS_DP_PREFIX + "*", UNALARMSCREEN_GROUPS_DPTYPE);
    
    for (int i = 1 ; i <= dynlen(dsDps) ; i++)
    {
      string sDp = dsDps[i];      
      dyn_int diChildren;    
      dpGet(sDp + UNALARMSCREEN_GROUPS_DPE_CHILDREN, diChildren);
      
      int iPos = dynContains(diChildren, iGroupId);
      if (iPos > 0)
      {
        dynRemove(diChildren, iPos);
        dpSetWait(sDp + UNALARMSCREEN_GROUPS_DPE_CHILDREN, diChildren);
      }
    }
  }
  
  
  
  // Update the group sorting DP.
  dyn_int diGroupOrder;
  dpGet(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);
  int iGroupPos = dynContains(diGroupOrder, iGroupId);
  if (iGroupPos > 0)
  {
    dynRemove(diGroupOrder, iGroupPos);
    dynUnique(diGroupOrder);
    dpSetWait(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);    
  }
  
  // Delete the widget
  if (bDeleteWidget)
  {
    unAlarmScreen_groups_removeGroup(daGroup);
  }
}

/**
  @par Description:
  Get a group from its ID.
  
  @par Usage:
  Public.
  
  @param  iGroupId  int input,  The ID of the group to fetch.
  
  @return The group as a dyn_anytype.
*/
dyn_anytype unAlarmScreen_groups_getGroup(const int iGroupId)
{
  dyn_anytype daGroup;
  string sGroupId;
  sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, iGroupId);
  const string sGroupDp = UNALARMSCREEN_GROUPS_DP_PREFIX + sGroupId;
  
  if (dpExists(sGroupDp))
  {
    dyn_int diChildren;
    bool bIsChild;
    string sFilterRef;
    string sTitle;
    time tSeenTime;
    
    dpGet(
        sGroupDp + UNALARMSCREEN_GROUPS_DPE_CHILDREN    , diChildren,
        sGroupDp + UNALARMSCREEN_GROUPS_DPE_IS_CHILD    , bIsChild,
        sGroupDp + UNALARMSCREEN_GROUPS_DPE_FILTER_REF  , sFilterRef,
        sGroupDp + UNALARMSCREEN_GROUPS_DPE_TITLE       , sTitle,
        sGroupDp + UNALARMSCREEN_GROUPS_DPE_SEEN_TIME   , tSeenTime
      );
      
    // Build the list of sub-groups.
    dyn_dyn_anytype ddaSubGroups;
    for (int i = 1 ; i <= dynlen(diChildren) ; i++)
    {
      dyn_anytype daSubGroup = unAlarmScreen_groups_getGroup(diChildren[i]);
      if (dynlen(daSubGroup) > 0)
      {
        dynAppend(ddaSubGroups, daSubGroup);
      }
    }
    
    daGroup[UNALARMSCREEN_GROUPS_INDEX_ID         ] = iGroupId;
    daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN   ] = ddaSubGroups;
    daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD   ] = bIsChild;
    daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE      ] = sTitle;    
    daGroup[UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME  ] = tSeenTime;  
    daGroup[UNALARMSCREEN_GROUPS_INDEX_FILTER_REF ] = sFilterRef;  
    
    
    // Related filter doesn't exist, remove the group (unless it is a row break).
    if (!dpExists(UNALARMSCREEN_DP_FILTER_PREFIX + sFilterRef) && (UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE != sTitle))
    {
      unAlarmScreen_groups_deleteGroup(daGroup);   
      dynClear(daGroup);
      return daGroup; 
    }
  }
  
  return daGroup;
}

/**
  @par Description:
  Get all the existing groups.
  
  @par Usage:
  Public.
  
  @param  bSorted bool input, Whether or not the group list should be sorted according to the system settings. Necessary for any GUI action. Default: false.
  
  @return dyn_dyn_anytype The list of all groups.
*/
dyn_dyn_anytype unAlarmScreen_groups_getAllGroups(bool bSorted = false)
{
  dyn_string dsDps = dpNames(UNALARMSCREEN_GROUPS_DP_PREFIX + "*", UNALARMSCREEN_GROUPS_DPTYPE);
  dyn_dyn_anytype ddaAllGroups;
  
  for (int i = 1 ; i <= dynlen(dsDps) ; i++)
  {
    string sDp = dsDps[i];
    string sId = substr(sDp, strpos(sDp, UNALARMSCREEN_GROUPS_DP_PREFIX) + strlen(UNALARMSCREEN_GROUPS_DP_PREFIX));   
    
    dyn_anytype daGroup = unAlarmScreen_groups_getGroup(sId);
    if (dynlen(daGroup) > 0)
    {
      dynAppend(ddaAllGroups, daGroup);
    }
  }
  
  if (bSorted)
  {
    dyn_int diGroupOrder;
    dpGet(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);
    
    dyn_dyn_anytype ddaAllGroupsSorted;
    
    // Sort children in each group
    for (int i = 1 ; i <= dynlen(ddaAllGroups) ; i++)
    {
      dyn_dyn_anytype ddaSortedChildren;
      
      for (int j = 1 ; j <= dynlen(diGroupOrder) ; j++)
      {
        for (int k = 1 ; k <= dynlen(ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) ; k++)
        {
          if (ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_CHILDREN][k][UNALARMSCREEN_GROUPS_INDEX_ID] == diGroupOrder[j])
          {
            dynAppend(ddaSortedChildren, ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_CHILDREN][k]);
            break;
          }
        }
      }
      
      ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_CHILDREN] = ddaSortedChildren;
    }
    
    // Sort groups
    for (int i = 1 ; i <= dynlen(diGroupOrder) ; i++)
    {
      for (int j = 1 ; j <= dynlen(ddaAllGroups) ; j++)
      {
        if (ddaAllGroups[j][UNALARMSCREEN_GROUPS_INDEX_ID] == diGroupOrder[i])
        {
          dynAppend(ddaAllGroupsSorted, ddaAllGroups[j]);
          break;
        }
      }
    }
    
    ddaAllGroups = ddaAllGroupsSorted;
  }
  
  return ddaAllGroups;
}


// ----------------------------------------------------------------------
// ------------------------ Group widget actions ------------------------
// ----------------------------------------------------------------------
// All those functions are synchronized so they can never be called at the same time.

/**
  @par Description:
  Initialize a group widget.
  
  @par Usage:
  Public.
  
  @param iGroupId integer input,  The ID of the group to initialize.
*/
void unAlarmScreen_groups_initGroupWidget(const int iGroupId) synchronized (iMutexSync)
{
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup(iGroupId);
  if (dynlen(daGroup) < 1)
  {
    return;
  }
  
  // Init variables
  bSubGroupsVisible = false;
  iAlarmCount = 0;
  mappingClear(g_mAlertStateInfo);
  
  // Init GUI elements.
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE, "text",         daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE, "toolTipText",  daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]); // Because sometimes the title is too long
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_ALARM_COUNT, "text",         iAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_UNACK_ALARM_COUNT, "text",   iAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_NEW_ALARM_COUNT, "text",     iAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_LAST_ALARM_DATE, "text",     "");
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_INFORMATION_TEXT, "text",    UNALARMSCREEN_GROUPS_LABEL_INIT);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_ALARM_BACKGROUND, "backCol", UNALARMSCREEN_GROUPS_NON_INIT_COLOUR);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS, "visible",  dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) > 0);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS_BG, "visible",  dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) > 0);

  // Connect the callback for seem time change.
  string sGroupId;
  sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);  
  string sGroupDp = UNALARMSCREEN_GROUPS_DP_PREFIX + sGroupId;
  dpConnect("unAlarmScreen_groups_seenTimeUpdateCallBack", false, sGroupDp + UNALARMSCREEN_GROUPS_DPE_SEEN_TIME);
  
  // Connect the callback for system state change.
  // The callback will be triggered once, starting the monitoring of the group.
  dpConnect("unAlarmScreen_groups_distributedCallBack", true, UNALARMSCREEN_GROUPS_DPE_DIST_SYSTEM_IDS);
}

/**
  @par Description:
  Show all the sub-groups of the clicked group.
  
  @par Usage:
  Public.
  
  @param  iGroupId  int input,  The clicked parent-group.
*/
void unAlarmScreen_groups_showSubGroups(const int iGroupId)
{
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup(iGroupId);
  
  for (int i = 1 ; i <= dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) ; i++)
  {
    dyn_anytype daSubGroup = daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN][i];    
    
    string sGroupId;
    sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, daSubGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
    if (shapeExists(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId))
    {
      setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId, "visible", !bSubGroupsVisible);
      _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS,    "visible", false);  // Hide the "show sub groups" button.  
      _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS_BG, "visible", false);  // Hide the "show sub groups" button.   
      _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_SUBGROUP_LIST,      "visible", false);  // Hide the sub-group list.
      _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId + "." + UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE,         "visible", false);  // Hide the highlight template.
      
      if (daSubGroup[UNALARMSCREEN_GROUPS_INDEX_ID] == selectedGroup.text())
      {
        unAlarmScreen_groups_highlightGroup(daSubGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
      }
    }
  }  
  
  if (bSubGroupsVisible)
  {
    bSubGroupsVisible = false;
    setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT, "visible", false);
    _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS, "fill", UNALARMSCREEN_GROUPS_SHOW_SUB_GROUPS_ICON_FILL);
  }
  else
  {
    bSubGroupsVisible = true;
    _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS, "fill", UNALARMSCREEN_GROUPS_HIDE_SUB_GROUPS_ICON_FILL);
  }
}

/**
  @par Description:
  Show the menu when clicking on the group widget.
  
  @par Usage:
  Public.
  
  @param iGroupId integer input,  The ID of the group clicked.
*/
void unAlarmScreen_groups_groupPopUpMenu(const int iGroupId) synchronized (iMutexSync)
{

  //IS-1953 - We are not in the alarm screen, do something else
  if (unAlarmScreen_groups_isDisplayedInAlarmScreen() == false)
  {
    //@TODO: create function to handle right click menu in process panel
    return;
  }
  
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup(iGroupId);
  if (dynlen(daGroup) < 1)
  {
    return;
  }
  
  dyn_dyn_anytype ddaAllGroups = unAlarmScreen_groups_getAllGroups(true);
  dyn_int diGroupIds = getDynInt(ddaAllGroups, UNALARMSCREEN_GROUPS_INDEX_ID);
  int iGroupPos = dynContains(diGroupIds, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
  
  if (iGroupPos < 1) // Should never happen but who knows, god might be angry...
  {
    return;
  }
    
  const int MENU_SHOW_ALARMS          = 1;
  const int MENU_ACK_ALARMS           = 2;
  const int MENU_DELETE_GROUP         = 3;
  const int MENU_MOVE_GROUP_UP        = 4;
  const int MENU_MOVE_GROUP_DOWN      = 5;
  const int MENU_ADD_NEW_LINE_BEFORE  = 6;
  const int MENU_RENAME               = 7;
  const int MENU_REMOVE_LINE_BEFORE   = 8;
  const int MENU_RESET_ALARMS         = 9;
  const int MENU_CONFIRM_SEEN         = 10;
  
  
                           
  // Always clickable if there are alarms.         
  int iAccess_MENU_SHOW_ALARMS          = iAlarmCount > 0 ? 1 : 0;     
  // Always clickable. Showed only in particular case where the group is selected.
  int iAccess_MENU_RESET_ALARMS         = 1;                               
  // Always clickable if there are alarms.
  int iAccess_MENU_CONFIRM_SEEN         = iAlarmCount > 0 ? 1 : 0;
  // Always clickable if there are alarms, each individual alarm has to check if ack is authorized. 
  int iAccess_MENU_ACK_ALARMS           = (iAlarmCount > 0) && unAlarmScreen_g_bState & unAlarmScreen_g_bGranted & unAlarmScreen_g_bAck ? 1 : 0;                               
  // The top button in the cascade will manage this access.
  int iAccess_MENU_DELETE_GROUP         = 1;                                        
  // The top button in the cascade will manage this access.
  int iAccess_MENU_MOVE_GROUP_UP        = 1;                                 
  // The top button in the cascade will manage this access.       
  int iAccess_MENU_MOVE_GROUP_DOWN      = 1;                                                    
  // Clickable if this is not a child.      
  int iAccess_MENU_ADD_NEW_LINE_BEFORE  = daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD] ? 0 : 1; 
  // The top button in the cascade will manage this access.
  int iAccess_MENU_RENAME               = 1;                                              
  // Clickable if this is not a child.
  int iAccess_MENU_REMOVE_LINE_BEFORE   = daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD] ? 0 : 1; 
  
  int iAdminRights = (unAlarmScreen_g_bState & unAlarmScreen_g_bGranted) ? 1 : 0;
  
  dyn_string dsMenu;
    
  if (iGroupId == selectedGroup.text())
  {
    dynAppend(dsMenu,
        "PUSH_BUTTON, Reset alarm list, "     + MENU_RESET_ALARMS         + ", "  + iAccess_MENU_RESET_ALARMS
      );
  }
  else
  {
    dynAppend(dsMenu,
        "PUSH_BUTTON, Show alarms, "          + MENU_SHOW_ALARMS          + ", "  + iAccess_MENU_SHOW_ALARMS
      );
  }
  
  dynAppend(dsMenu,
      makeDynString(
        // UPDATE: It was requested to prevent users from acknowledging from the group widget. The code is kept in case the decision changes later.
        // "PUSH_BUTTON, Acknowledge alarms, "   + MENU_ACK_ALARMS           + ", "  + iAccess_MENU_ACK_ALARMS, 
        "PUSH_BUTTON, Mark as seen, "         + MENU_CONFIRM_SEEN         + ", "  + iAccess_MENU_CONFIRM_SEEN, 
        "SEPARATOR", 
        "CASCADE_BUTTON, Options, "                                               + iAdminRights, 
        "Options",
        "PUSH_BUTTON, Rename group, "         + MENU_RENAME               + ", "  + iAccess_MENU_RENAME, 
        "PUSH_BUTTON, Delete group, "         + MENU_DELETE_GROUP         + ", "  + iAccess_MENU_DELETE_GROUP/*, 
        "PUSH_BUTTON, Move group up, "        + MENU_MOVE_GROUP_UP        + ", "  + iAccess_MENU_MOVE_GROUP_UP,
        "PUSH_BUTTON, Move group down, "      + MENU_MOVE_GROUP_DOWN      + ", "  + iAccess_MENU_MOVE_GROUP_DOWN*/ // Move group/up and down not needed because of drag&drop implementation. The code stays in case...
      )
    );
  
  if (!daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
  {
    dynAppend(dsMenu,
      "PUSH_BUTTON, Add new line before, "  + MENU_ADD_NEW_LINE_BEFORE  + ", "  + iAccess_MENU_ADD_NEW_LINE_BEFORE
    );
  }
  
  if ((iGroupPos > 1) && (ddaAllGroups[iGroupPos - 1][UNALARMSCREEN_GROUPS_INDEX_TITLE] == UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE))
  {
    dynAppend(dsMenu,
        "PUSH_BUTTON, Delete line jump, "  + MENU_REMOVE_LINE_BEFORE    + ", "  + iAccess_MENU_REMOVE_LINE_BEFORE    
      );  
  }
    
  int iAnswer;
  
  popupMenu(dsMenu, iAnswer); 
  
  switch(iAnswer)
  {
    case MENU_SHOW_ALARMS         :
    {
      _unAlarmScreen_groups_rightClick_showAlarms(daGroup);
      break;
    }
    case MENU_RESET_ALARMS        :
    {
      unAlarmScreen_groups_clearHighlight();
      unAlarmScreen_displayFilter(UNALARMSCREEN_FILTER_DEFAULT);
      unAlarmScreen_applyFilter();
      break;
    }
    case MENU_ACK_ALARMS          :
    {
      _unAlarmScreen_groups_rightClick_ackAlarms(daGroup);
      break;
    }
    case MENU_CONFIRM_SEEN        :
    {
      _unAlarmScreen_groups_rightClick_markAsSeen(daGroup);
      break;
    }
    case MENU_RENAME              :
    {
      _unAlarmScreen_groups_rightClick_renameGroup(daGroup);
      break;
    }
    case MENU_DELETE_GROUP        :
    {
      _unAlarmScreen_groups_rightClick_deleteGroup(daGroup);
      break;
    }
    case MENU_MOVE_GROUP_UP       :
    {
      _unAlarmScreen_groups_rightClick_moveGroupUp(daGroup);
      break;
    }
    case MENU_MOVE_GROUP_DOWN     :
    {
      _unAlarmScreen_groups_rightClick_moveGroupDown(daGroup);
      break;
    }
    case MENU_ADD_NEW_LINE_BEFORE :
    {
      _unAlarmScreen_groups_rightClick_addNewLine(daGroup);
      break;
    }
    case MENU_REMOVE_LINE_BEFORE :
    {
      _unAlarmScreen_groups_rightClick_removeNewLine(daGroup);
      break;
    }
    default:    
    {
      // Not possible / ignore
      break;
    }
  }
}

/**
  @par Description:
  Action on click on the title or the background rectangle of a group widget.
  
  @par Usage:
  Public.
  
  @param iGroupId integer input,  The ID of the group clicked.
*/
void unAlarmScreen_groups_clickAction(const int iGroupId) synchronized (iMutexSync)
{
  //IS-1953 - We are not in the alarm screen, do something else
  if (unAlarmScreen_groups_isDisplayedInAlarmScreen() == false)
  {
    //@TODO: create function to handle click action in process panel
    return;
  }
  
  // Ignore if no alarm
  if (0 == iAlarmCount)
  {
    return;
  }
  
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup(iGroupId);
  if (dynlen(daGroup) < 1)
  {
    return;
  }
  
  // If the group is already selected: unselect
  // Otherwise: show this group's alarms.
  if (iGroupId == selectedGroup.text())
  {
    fwAlarmScreenGeneric_setActiveGroup("");
    unAlarmScreen_groups_clearHighlight();
    unAlarmScreen_displayFilter(UNALARMSCREEN_FILTER_DEFAULT);
    unAlarmScreen_applyFilter();
  }
  else
  {
    _unAlarmScreen_groups_rightClick_showAlarms(daGroup);
  }
}


// ----------------------------------------------------------------------
// ------------------ Group reordering with drag & drop -----------------
// ----------------------------------------------------------------------

/**
  @par Description:
  Action on widget drag start.
  
  @param iKeys  int input,  The keys pressed with the drag (SHIFT/CTRL/ALT/MIX).
  
  @par Usage:
  Internal.
*/
void _unAlarmScreen_groups_groupDragStart(int iKeys, int iGroupId)
{
  if (unAlarmScreen_g_bState & unAlarmScreen_g_bGranted)
  {
    string sGroupId;
    sprintf(sGroupId, UNALARMSCREEN_GROUPS_DPE_FORMAT, iGroupId);
  
    dragStart(sGroupId, UNALARMSCREEN_GROUPS_DRAG_ICON_START, DRAG_COPY);
  }
}

/**
  @par Description:
  Action on widget drag enter.
  
  @param  sInformation, string input, Text in "text or plain" format retrieved from the drag operation.
  @param  iDragType,    int input,    One of the drag constants DRAG_COPY, DRAG_MOVE or DRAG_LINK (see _unAlarmScreen_groups_groupDragStart()).
  
  @par Usage:
  Internal.
*/
void _unAlarmScreen_groups_groupDragEnter(string sInformation, int iDragType, int iGroupId)
{
  int iGroupToMoveId = sInformation;
  int iGroupDestinationId = iGroupId;
  
  // Cannot move group to its own position.
  if (iGroupToMoveId == iGroupDestinationId)
  {
    dropAccept(false);
    return;
  }
  
  dyn_anytype daGroupToMove = unAlarmScreen_groups_getGroup(iGroupToMoveId);
  dyn_anytype daGroupDestination = unAlarmScreen_groups_getGroup(iGroupDestinationId);
  
  // If the two groups are sub-groups, check if they are from the same parent.
  // If one of them only is a sub-group, drag impossible.
  // If none is a sub-group, drag ok.
  if (daGroupToMove[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD] && daGroupDestination[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
  {
    dyn_dyn_anytype ddaAllGroups = unAlarmScreen_groups_getAllGroups();
    for (int i = 1 ; i <= dynlen(ddaAllGroups) ; i++)
    {
      dyn_int diChildren = getDynInt(ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_CHILDREN], UNALARMSCREEN_GROUPS_INDEX_ID);
      if ((dynContains(diChildren, iGroupToMoveId) > 0) && (dynContains(diChildren, iGroupDestinationId) > 0))
      {
        dropAccept(true);
        return;
      }
    }
  }
  else if (!daGroupToMove[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD] && !daGroupDestination[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
  {
    dropAccept(true);
  }
  else
  {
    dropAccept(false);
  }
} 

/**
  @par Description:
  Action on widget drag drop.
  
  @param  sInformation, string input, Text in "text or plain" format retrieved from the drag operation.
  @param  iDragType,    int input,    One of the drag constants DRAG_COPY, DRAG_MOVE or DRAG_LINK (see _unAlarmScreen_groups_groupDragStart()).
  
  @par Usage:
  Internal.
*/
void _unAlarmScreen_groups_groupDragDrop(string sInformation, int iDragType, int iGroupId)
{
  int iGroupToMoveId = sInformation;
  int iGroupDestinationId = iGroupId;
  
  dyn_anytype daGroupToMove = unAlarmScreen_groups_getGroup(iGroupToMoveId);
  dyn_anytype daGroupDestination = unAlarmScreen_groups_getGroup(iGroupDestinationId);

  dyn_int diGroupOrder;
  dpGet(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);
  
  int iPosOld = dynContains(diGroupOrder, iGroupToMoveId);
  dynRemove(diGroupOrder, iPosOld);
  int iPosNew = dynContains(diGroupOrder, iGroupDestinationId);
  dynInsertAt(diGroupOrder, iGroupToMoveId, iPosNew);
  
  dynUnique(diGroupOrder);
  dpSetWait(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);
  
  unAlarmScreen_groups_showGroups();
}


// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------ Callback functions ------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

/**
  @par Description:
  Callback triggered when the list of alarms for a given group changes. Displays the new alarm count on the widget.
  
  @par Usage:
  Internal.
  
  @param  iGroupId        anytype input,          The ID of the group that for which the callback was triggered.
  @param  ddaQueryResult  dyn_dyn_anytype input,  The elements that have changed state for the given group.
*/
void unAlarmScreen_groups_alarmCallBack(anytype iGroupId, dyn_dyn_anytype ddaQueryResult) synchronized (iMutexSync)
{		
  // Get rid of the useless first element
  dynRemove(ddaQueryResult, 1);
  
  int iQueryResultLength = dynlen(ddaQueryResult);
  
  // Get the group.
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup((int) iGroupId);
  
  // Ignore if it doesn't exist any more.
  if (dynlen(daGroup) < 1)
  {
    return;
  }
  
  // Happens at initialization when there are no alarms for this group.
  // Warning, there can be no alarm on one of the many systems so we must not reset the colour and info, just update with what is already known.
  if (iQueryResultLength < 1)
  {
    _unAlarmScreen_groups_updateView(daGroup);
    return;
  }
  
  //Now get the color of the alarm as _alert_hdl.._act_state_color is not
  //retrieved correctly by dpQueryConnect and a dpGet is requiered
  dyn_string dsDpeActStateColor, dsDpeActStateColorValue;
  for(int i = 1 ; i <= iQueryResultLength ; i++)
  {
  	dsDpeActStateColor[i] = ddaQueryResult[i][1] + ":_alert_hdl.._act_state_color";
  }
  dpGet(dsDpeActStateColor, dsDpeActStateColorValue); 
  
  // Evaluate the state of the group.
  //  - Count the active alarms  
  //  - Count unack alarms
  //  - Get higher priority
  
  for (int i = 1 ; i <= iQueryResultLength ; i++)
  {
    atime atAlertTtime = ddaQueryResult[i][UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_TIME];    
    string sKey = dpSubStr(getAIdentifier(atAlertTtime), DPSUB_SYS_DP_EL_CONF_DET);  // Key in the alarm state mapping: DPE with config index (dpe:__alert_hdl.X).
    int iAckState = ddaQueryResult[i][UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_ACK];
    int iAlertState = ddaQueryResult[i][UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_STATE];   
    
    
    // Warning: if the alarm is already known, we have to update the info.
    if (mappingHasKey(g_mAlertStateInfo, sKey))
    {
      // Problem when acknowledging a WENT alarm.
      // The time in the ACK callback is the same as in the WENT one, so we need a strict '=' comparison to be sure to update.
      if (((time) atAlertTtime) < g_mAlertStateInfo[sKey][UNALARMSCREEN_ALARM_STATE_INDEX_TIME])
      {
        continue;
      }
     
    } else
    {
    	// Reset alarm info
    	g_mAlertStateInfo[sKey] = makeDynAnytype();
    }
    
    //now see if the alarm is in the list depending on the filter defined
    int iFilterState = gUnAlarmScreenFilterAlertState[iGroupId];     
  	g_mAlertStateInfo[sKey][UNALARMSCREEN_ALARM_STATE_INDEX_ACTIVE] = _unAlarmScreen_groups_numAlarmInList( iFilterState,  iAlertState, 	iAckState);
    
    // Save alert time
    g_mAlertStateInfo[sKey][UNALARMSCREEN_ALARM_STATE_INDEX_TIME] = (time) atAlertTtime; 
    
    // Save alert priority
    g_mAlertStateInfo[sKey][UNALARMSCREEN_ALARM_STATE_INDEX_PRIORITY] = ddaQueryResult[i][UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_PRIO];    
    
    // Save alert ack state - Use to compute the number of unack alerts
    if ( (DPATTR_ALERTSTATE_APP_NOT_ACK == iAlertState 
    	 || DPATTR_ALERTSTATE_DISAPP_NOT_ACK == iAlertState
    	 || DPATTR_ALERTSTATE_APP_DISAPP_NOT_ACK == iAlertState)
    	 && (DPATTR_ACKTYPE_NOT == iAckState)
    )
    {
    	g_mAlertStateInfo[sKey][UNALARMSCREEN_ALARM_STATE_INDEX_ACK] = false;
    } else
    {
    	g_mAlertStateInfo[sKey][UNALARMSCREEN_ALARM_STATE_INDEX_ACK] = true;
    }
  	
    
    // Save colour if the alarm is present in the list
	g_mAlertStateInfo[sKey][UNALARMSCREEN_ALARM_STATE_INDEX_COLOUR] = dsDpeActStateColorValue[i];	
    
  }  
  
  _unAlarmScreen_groups_updateView(daGroup);
}

/** 
 *
 * Helper function for the dpQueryConnect callback to 
 * evaluate if a given alarm range is present in the list due to
 * the filter "Alert State" and if yes how alarms for this range
 *  
 * @param iFilerAlertState	(int)	IN	: possible values PANEL_FILTER_STATE_*
 * @param iActState	(int)	IN	: possible values are DPATTR_ALERTSTATE_*
 * @param iAckState	(int)	IN	: possible values are DPATTR_ACKTYPE_*
 * @return value of type 'int': number of alarms present in the list for this range
 */
private int _unAlarmScreen_groups_numAlarmInList( int iFilerAlertState, int iActState, int iAckState)
{
	int iNumAlarm = 0;
	
	//DebugTN(__FUNCTION__, iFilerAlertState, iActState, iAckState);
	
	// State (ack/non ack/ ...)  
  	const int PANEL_FILTER_STATE_ALL        = 1;
  	const int PANEL_FILTER_STATE_UNACK      = 2;
  	const int PANEL_FILTER_STATE_PENDING       = 3;
  	const int PANEL_FILTER_STATE_PENDING_UNACK = 4;
  	
  	switch (iFilerAlertState)
  	{
    	case PANEL_FILTER_STATE_ALL         : // Show all alarms
    	{
    		if ((DPATTR_ALERTSTATE_NONE != iActState ))
    		{
    			//if the alarm is WENT/UNACK there are two alarm lines
    			//otherwise just one as unicos always have two lines for WENT and CAME when not ack
    			if (    (DPATTR_ALERTSTATE_DISAPP_NOT_ACK == iActState
      					  || DPATTR_ALERTSTATE_APP_DISAPP_NOT_ACK == iActState
      					) 
      					&& 
      					(DPATTR_ACKTYPE_NOT == iAckState)
      				)
    			{
    				iNumAlarm = 2;	
    			} 
    			//if the alarm is WENT/ACK that means it is not displayed anymore
    			else if ( (DPATTR_ALERTSTATE_DISAPP_NOT_ACK == iActState
      					  || DPATTR_ALERTSTATE_APP_DISAPP_NOT_ACK == iActState
      					) 
      					&& 
      					(DPATTR_ACKTYPE_NOT != iAckState))
    			{
    				iNumAlarm = 0;
    			} else
    			{
    				iNumAlarm = 1;	
    			}
    			
    		}
      			
      		break;
    	}
    	case PANEL_FILTER_STATE_UNACK       : // Show alarms CAME/UNACK or WENT/UNACK.
    	{
      		if (((		   DPATTR_ALERTSTATE_APP_NOT_ACK == iActState 
      					|| DPATTR_ALERTSTATE_DISAPP_NOT_ACK == iActState
      					|| DPATTR_ALERTSTATE_APP_DISAPP_NOT_ACK == iActState
      		) 
      				&& (DPATTR_ACKTYPE_NOT == iAckState)
      		))
    		{
    			iNumAlarm = 1;
    		}
      		break;
    	}
    	case PANEL_FILTER_STATE_PENDING        : // Show alarms CAME/UNACK or CAME/ACK
    	{
      		if ((DPATTR_ALERTSTATE_APP_NOT_ACK == iActState || DPATTR_ALERTSTATE_APP_ACK == iActState))
    		{
    			iNumAlarm = 1;
    		}
      		break;
    	}
    	case PANEL_FILTER_STATE_PENDING_UNACK  : // Show alarms CAME/UNACK
    	{
      		if (
      			//if UNACK
      			((DPATTR_ALERTSTATE_APP_NOT_ACK == iActState       					
      				) 
      				&& (DPATTR_ACKTYPE_NOT == iAckState)
      			)      			
      		)
    		{
    			iNumAlarm = 1;
    		}
      		break;
    	}
  	} 
  	
  	return iNumAlarm;
}

/**
  @par Description:
  Callback triggered when a group is set as "Seen". Update the info and show it.
  
  @par Usage:
  Internal.
  
  @param  sDpe        string input, Irrelevant.
  @param  tSeenTime   time input,   Irrelevant.
*/
void unAlarmScreen_groups_seenTimeUpdateCallBack(string sDpe, time tSeenTime) synchronized (iMutexSync)
{
  int iGroupId = $iGroupId;
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup(iGroupId);

  _unAlarmScreen_groups_updateView(daGroup);
}

/**
  @par Description:
  Callback triggered when a system connects or disconnects. Individual connection for each group widget. Disconnects/reconnects the alarm count callback.
  
  @par Usage:
  Internal.
  
  @param[in]  sDpe                string,     Irrelevant.
  @param[in]  dsRemoteSystemList  dyn_string, The list of remote systems.
*/
void unAlarmScreen_groups_distributedCallBack(string sDpe, dyn_string dsRemoteSystemList)  synchronized (iMutexSync)
{
  int iGroupId = $iGroupId;
  if (bDpQueryConnected) // For the first time we get to the callback.
  {
    // Disconnect the current callback connection.
    dpQueryDisconnect("unAlarmScreen_groups_alarmCallBack", iGroupId);  
  }
  
  
  dyn_anytype daGroup = unAlarmScreen_groups_getGroup(iGroupId);
  if (dynlen(daGroup) < 1)
  {
    return;
  }
  
  
  // Reset group widgets and variables.
  iAlarmCount = 0;
  mappingClear(g_mAlertStateInfo);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE, "text",         daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE, "toolTipText",  daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_ALARM_COUNT, "text",         iAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_UNACK_ALARM_COUNT, "text",   iAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_NEW_ALARM_COUNT, "text",     iAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_LAST_ALARM_DATE, "text",     "");
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_INFORMATION_TEXT, "text",    UNALARMSCREEN_GROUPS_LABEL_INIT);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_ALARM_BACKGROUND, "backCol", UNALARMSCREEN_GROUPS_NON_INIT_COLOUR);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS, "visible",  dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) > 0);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_SHOW_SUB_GROUPS_BG, "visible",  dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) > 0);
    
  // Get the filter
  string sFilterRefDp = getSystemName() + UNALARMSCREEN_DP_FILTER_PREFIX + daGroup[UNALARMSCREEN_GROUPS_INDEX_FILTER_REF];
  string sFilter;
  dpGet(sFilterRefDp + ".filter", sFilter);
  
  // Get the list of all connected system names (the callback contains only the IDs)
  dyn_string dsSystemNames;
  dynAppend(dsSystemNames, getSystemName()); // Local system
  for (int i = 1 ; i <= dynlen(dsRemoteSystemList) ; i++)
  {
    string sSystemName = getSystemName(dsRemoteSystemList[i]);
    if ("" != sSystemName)
    {
      dynAppend(dsSystemNames, sSystemName);
    }
  } 
    
  // Set the new callback for each system
  dynUnique(dsSystemNames);
  dyn_string dsQuery = unAlarmScreen_groups_buildQuery(sFilter, dsSystemNames, iGroupId);
  for (int i = 1 ; i <= dynlen(dsQuery) ; i++)
  {
   // DebugTN(__FUNCTION__, "GroupId", iGroupId, "query", dsQuery[i]);
    dpQueryConnectSingle("unAlarmScreen_groups_alarmCallBack", true, iGroupId, dsQuery[i], UNALARM_SCREEN_GROUP_DPQUERY_BUFFERTIME);
  }
  
  bDpQueryConnected = true;
}

// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------- Configuration functions ----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

/**
  @par Description:
  Init the group configuration:
    - Panel to use for a group (read from settings datapoint).
    - Height of this panel.
    - Width of this panel.
  
  This information is stored in the appropriate widgets.
    
  @par Usage:
  Internal.  
*/
void _unAlarmScreen_groups_config_init()
{  
  string sPanelFile;
  if (dpExists(UNALARMSCREEN_GROUP_SETUP_GROUP_WIDGET))
  {
    dpGet(UNALARMSCREEN_GROUP_SETUP_GROUP_WIDGET, sPanelFile);
  }
  
  // File doesn't exist? Use the default one.  
  if (!isfile(getPath(PANELS_REL_PATH, sPanelFile)))
  {
    sPanelFile = UNALARMSCREEN_GROUPS_GROUP_WIDGET_PANEL_DEFAULT;
  }
    
  setValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_CURRENT_PANEL, "text", sPanelFile);
  
  // The widget is shown outside of the panel to avoid bothering the user.
  int iX = -500;
  int iY = -500;
  
  // Dummy group id that doesn't exist for sure. Like that the monitoring won't start.
  string sGroupId = 0;
  string sSymbolReference = UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId;
  
  addSymbol(
    myModuleName(), 
    myPanelName(), 
    sPanelFile, 
    sSymbolReference, 
    makeDynString(UNALARMSCREEN_GROUPS_DOLLAR_PARAM_GROUP_ID + ":" + sGroupId), 
    iX, 
    iY, 
    0, 
    1, 
    1
  );
  
  // It sometimes takes time before the widget is shown.
  while(!shapeExists(sSymbolReference + "." + UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE))
  {
    delay(0, 10);
  }
  
  int iWidth;
  int iHeight;
  string sBackColour;
  
  getValue(sSymbolReference + "." + UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE, "size", iWidth, iHeight);
  getValue(sSymbolReference + "." + UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE, "backCol", sBackColour);
  
  // Set the size of the rectangles for position and highlight.
  setValue(UNALARMSCREEN_GROUPS_GROUP_POS_WIDGET, "size", iWidth, iHeight);
  setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_POS_WIDGET,  "size", iWidth * UNALARMSCREEN_GROUPS_SUB_GROUP_SCALE, iHeight * UNALARMSCREEN_GROUPS_SUB_GROUP_SCALE);
  
  setValue(UNALARMSCREEN_GROUPS_GROUP_HIGHLIGHT,  "size", iWidth, iHeight);
  setValue(UNALARMSCREEN_GROUPS_GROUP_HIGHLIGHT,  "backCol", sBackColour);
  
  setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT,  "size", iWidth * UNALARMSCREEN_GROUPS_SUB_GROUP_SCALE, iHeight * UNALARMSCREEN_GROUPS_SUB_GROUP_SCALE);
  setValue(UNALARMSCREEN_GROUPS_SUB_GROUP_HIGHLIGHT,  "backCol", sBackColour);
}

/**
  @par Description:
  Load the current settings in the configuration panel.
  
  @par Usage:
  Internal.
*/
void _unAlarmScreen_groups_config_loadSettings()
{
  // 1) Show group visibility settings.
  checkBoxHideGroups.state(0, unAlarmScreen_groups_config_getShowGroups());

  // 2) Show group panel settings.
  string sDefaultPanelFullPath = getPath(PANELS_REL_PATH, UNALARMSCREEN_GROUPS_GROUP_WIDGET_PANEL_DEFAULT);
  string sDefaultPanelFolder = dirName(sDefaultPanelFullPath);
  dyn_string dsAllGroupWidgets = getFileNames(sDefaultPanelFolder, UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_PREFIX + "*" + UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_EXTENSION);
  
  
  for (int i = 1 ; i <= dynlen(dsAllGroupWidgets) ; i++)
  {
    string sPanelName = baseName(dsAllGroupWidgets[i]);
    sPanelName = substr(sPanelName, strlen(UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_PREFIX));
    sPanelName = substr(sPanelName, 0, strpos(sPanelName, UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_EXTENSION));
    
    if ("" != sPanelName)
    {
      listPanels.appendItem(sPanelName);
    }
  }


  dpGet(UNALARMSCREEN_GROUP_SETUP_GROUP_WIDGET, sFormerPanel); // sFormerPanel is defined in the scopelib.
  
  string sFormerPanelShort = baseName(getPath(PANELS_REL_PATH, sFormerPanel));
  sFormerPanelShort = substr(sFormerPanelShort, strlen(UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_PREFIX));
  sFormerPanelShort = substr(sFormerPanelShort, 0, strpos(sFormerPanelShort, UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_EXTENSION));

  listPanels.selectedText(sFormerPanelShort);
  _unAlarmScreen_groups_config_showWidget(sFormerPanelShort);
}

/**
  @par Description:
  In the configuration panel, show the selected widget panel.
  
  @par Usage:
  Internal.
  
  @param[in]  sPanelFile string, The widget panel to show.
*/
void _unAlarmScreen_groups_config_showWidget(string sPanelFileShort)
{
  int iX;
  int iY;
  getValue("rectanglePosition", "position", iX, iY);
    
  // Dummy group id that doesn't exist for sure. Like that the monitoring won't start.
  string sGroupId = 0;
  string sSymbolReference = UNALARMSCREEN_GROUPS_GROUP_WIDGET_REF + sGroupId;
  if (shapeExists(sSymbolReference))
  {
    removeSymbol(myModuleName(), myPanelName(), sSymbolReference);
  }
    
  string sPanelFile = UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_FOLDER + UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_PREFIX + sPanelFileShort + UNALARMSCREEN_GROUPS_CONFIG_PANEL_FILE_EXTENSION;
    
  // File doesn't exist? Use the default one.  
  if (!isfile(getPath(PANELS_REL_PATH, sPanelFile)))
  {
    groupWidgetProblemText.text = "File does not exist.";
    groupWidgetProblemText.visible(true);
    groupWidgetProblemBackground.visible(true);
    return;
  }
  else
  {
    groupWidgetProblemText.visible(false);
    groupWidgetProblemBackground.visible(false);
  }
  
  addSymbol(
    myModuleName(), 
    myPanelName(), 
    sPanelFile, 
    sSymbolReference, 
    makeDynString(UNALARMSCREEN_GROUPS_DOLLAR_PARAM_GROUP_ID + ":" + sGroupId), 
    iX, 
    iY, 
    0, 
    1, 
    1
  );
  
  // Check if the panel is valid or not.
  // As explained in the file description, a valid panel has two mandatory widgets UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE and UNALARMSCREEN_GROUPS_WIDGET_SUBGROUP_LIST
  if (!shapeExists(UNALARMSCREEN_GROUPS_WIDGET_GROUP_SIZE) || !shapeExists(UNALARMSCREEN_GROUPS_WIDGET_SUBGROUP_LIST))
  {
    groupWidgetProblemText.visible(true);
    groupWidgetProblemBackground.visible(true);
  }
  else
  {
    groupWidgetProblemText.visible(false);
    groupWidgetProblemBackground.visible(false);
    dpSetWait(UNALARMSCREEN_GROUP_SETUP_GROUP_WIDGET, sPanelFile);
    textWarningChange.visible(sFormerPanel != sPanelFile);
    groupWidgetChangeInfoArrow.visible(sFormerPanel != sPanelFile);
  }  
}

/**
  @par Description:
  Set the group monitoring widget to use in the application.
  To be called from the configuration panel.
  
  @par Usage:
  Internal.
*/
void _unAlarmScreen_groups_config_saveGroupWidgetPanel()
{
  string sPanel = listPanels.selectedText();
}

/**
  @par Description:
  Get which panel is used to show groups now.
  
  @par Usage:
  Internal.
  
  @return The panel to use.
*/
string _unAlarmScreen_groups_getCurrentPanel()
{
  string sPanel;
  getValue(UNALARMSCREEN_GROUPS_GROUP_WIDGET_CURRENT_PANEL, "text", sPanel);
  
  return sPanel;
}

/**
  @par Description:
  Get if the groups have to be shown or not.
  
  @par Usage:
  Public.
  
  @return True if the groups can be shown, false otherwise.
*/
bool unAlarmScreen_groups_config_getShowGroups()
{
  bool bShow;
  dpGet(UNALARMSCREEN_GROUP_SETUP_SHOW_GROUPS, bShow);
  
  return bShow;
}

/**
  @par Description:
  Set if the groups have to be shown or not.
  
  @par Usage:
  Public.
  
  @param[in]  bShow bool, True if the groups can be shown, false otherwise.
*/
void unAlarmScreen_groups_config_setShowGroups(const bool bShow)
{
  dpSetWait(UNALARMSCREEN_GROUP_SETUP_SHOW_GROUPS, bShow);
}

// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------ Internal functions ------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

/**
  @par Description:
  To avoid repeating the if (shapeExists(XXX)) setValue(XXX, yyy, zzz), this function can be used instead.
  
  @par Usage:
  Internal.
  
  @param[in]  sWidget     sting,    The widget to change.
  @param[in]  sAttribute  string,   The attribute of this widget to change. 
  @param[in]  aValue      anytype,  The value to set to this attribute.
*/
void _unAlarmScreen_groups_setValue(const string sWidget, const string sAttribute, const anytype aValue)
{
  if (shapeExists(sWidget))
  {
    setValue(sWidget, sAttribute, aValue);
  }
}

/**
  @par Description:
  Build a list of DP queries to get the alarms matching the given filter. One for each system.
  
  @par Usage:
  Internal.
  
  @param[in]  sFilter           string,     The filter to build a query for.
  @param[in]  dsAllSystemNames  dyn_string, The list of all the systems connected to this project (included the local one). Can be altered if filtering on specific systems.
  @param[in]  iGroupId           int,       The group id currently treated. 
  
  
  @return A list of queries, one for each system.
*/
dyn_string unAlarmScreen_groups_buildQuery(const string sFilter, dyn_string dsAllSystemNames, const int iGroupId)
{  
  dyn_string dsFilter = strsplit(sFilter, UNALARMSCREEN_FILTER_DELIMITER);
  
  dyn_string dsQueryList;
  
  // Build DP comment
  string sFilterDpComment;  
  dyn_string dsDomains = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN], UNALARMSCREEN_FILTER_COMBO_DELIMITER);
  bool bMultiDomain = dynlen(dsDomains) > 1;
  if (bMultiDomain)
  {
    sFilterDpComment = UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
  }
  else
  {
    if (dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN] == "")
    {
      sFilterDpComment = UNALARMSCREEN_FILTER_DELIMITER;
    }
    else if (dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN] == UNALARMSCREEN_FILTER_ALL)
    {
      sFilterDpComment = dsFilter[UNALARMSCREEN_FILTER_FIELD_DOMAIN] + UNALARMSCREEN_FILTER_DELIMITER;    
    }
    else
    {
      // At leat one domain => filter with dpList
      bMultiDomain = true;
      sFilterDpComment = UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
    }
  }
  
  // Nature
  dyn_string dsNatures = strsplit(dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE], UNALARMSCREEN_FILTER_COMBO_DELIMITER);
  bool bMultiNature = dynlen(dsNatures) > 1;
  
  if (bMultiNature)
  {
    sFilterDpComment = sFilterDpComment + UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
  }
  else
  {
    if (dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE] == "")
    {
      sFilterDpComment = sFilterDpComment + UNALARMSCREEN_FILTER_DELIMITER;
    }
    else if (dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE] == UNALARMSCREEN_FILTER_ALL)
    {
      sFilterDpComment += dsFilter[UNALARMSCREEN_FILTER_FIELD_NATURE] + UNALARMSCREEN_FILTER_DELIMITER;        
    }
    else    // At leat one nature => filter with dpList
    {
      bMultiNature = true;
      sFilterDpComment += UNALARMSCREEN_FILTER_ALL + UNALARMSCREEN_FILTER_DELIMITER;
    }
  }  
  
  switch (unAlarmScreen_g_iAlarmScreenType)
  {
    case UNALARMSCREEN_PANEL_ALL_ALARMS:
    {
      sFilterDpComment =   
          sFilterDpComment + 
          dsFilter[UNALARMSCREEN_FILTER_FIELD_ALIAS]       + UNALARMSCREEN_FILTER_DELIMITER +
          dsFilter[UNALARMSCREEN_FILTER_FIELD_DESCRIPTION] + UNALARMSCREEN_FILTER_DELIMITER + 
          dsFilter[UNALARMSCREEN_FILTER_FIELD_NAME]        + UNALARMSCREEN_FILTER_DELIMITER + 
          UNALARMSCREEN_FILTER_ALL;
      break;
    }
    case UNALARMSCREEN_PANEL_SYSTEM_INTEGRITY:
    {
      sFilterDpComment = dsFilter[UNALARMSCREEN_FILTER_FIELD_DESCRIPTION];
      break;
    }
    default:
    {
      sFilterDpComment = UNALARMSCREEN_FILTER_ALL;
      break;
    }
  }
  
  //save the alert state filter to the global mapping for faster retrieval in the call back
  gUnAlarmScreenFilterAlertState[iGroupId] = dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTSTATE];
  
  // Short (bad/...)
  string    sFilterAlertText = dsFilter[UNALARMSCREEN_FILTER_FIELD_ALERTTEXT];
  
  // Filter DPs
  dyn_string dsFilterDps = _unAlarmScreen_dpListFilter(dsFilter, bMultiDomain, bMultiNature, dsAllSystemNames);
  
  // Convert "group:::" and list to query-ready string
  string sFilterDps = unAlarmScreen_groups_getQueryGroupClause(dsFilterDps);
  
  // Problem: a query can be done only on one system.
  // If there are several, we have to connect the same query for each system so several queries are returned.  
  if ((1 == dynlen(dsAllSystemNames)) && (getSystemName() == dsAllSystemNames[1])) // Only local system alarms?
  {  
    // Query
    string sQuery = 
        "SELECT ALERT " +       
            "'_alert_hdl.._act_state', "        +
            "'_alert_hdl.._abbr', "             +            
            "'_alert_hdl.._prior', "            + 
            "'_alert_hdl.._ack_state' "         + // Necessary otherwise the callback is not triggered when an alarm goes from CAME/unack to CAME/ack.
        "FROM " + sFilterDps + " " + 
        "WHERE " +
            "_COMMENT LIKE \""                + sFilterDpComment  + "\" " +
            "AND _DP LIKE \""                 + "*"               + "\" " +
            "AND '_alert_hdl.._abbr' LIKE \"" + sFilterAlertText  + "\" " 
      ;
    
    dynAppend(dsQueryList, sQuery);
  }
  else
  {
    for (int i = 1 ; i <= dynlen(dsAllSystemNames) ; i++)
    {
      string sQuery = 
          "SELECT ALERT " +       
              "'_alert_hdl.._act_state', "        +
              "'_alert_hdl.._abbr', "             +              
              "'_alert_hdl.._prior', "             + 
              "'_alert_hdl.._ack_state' "             + 
          "FROM " + sFilterDps + " REMOTE'" + dsAllSystemNames[i] + "' " + 
          "WHERE " +
              "_COMMENT LIKE \""                + sFilterDpComment  + "\" " +
              "AND _DP LIKE \""                 + "*"               + "\" " +
              "AND '_alert_hdl.._abbr' LIKE \"" + sFilterAlertText  + "\" " 
        ;
      
      dynAppend(dsQueryList, sQuery);
    }
  }
  
  
  return dsQueryList;
}

/**
  @par Description:
  Convert a DP list containing "group:::" to a proper format for dp query.
  Code taken from aes.ctl and lightened.
  
  @par Usage:
  Internal.
  
  @param  dsDpList  dyn_string input, The list of dp to convert.
  
  @return A string ready to be used in a "FROM" statement of a dp query.
*/
string unAlarmScreen_groups_getQueryGroupClause(dyn_string dsDpList)
{
  int iLength;
  
  bool bGroup = false;
  
  dyn_string dsGroup;
  dyn_string dsDpe;
  
  string sQueryFrom;
  string sRdbRes;
  string sGroupResolved;  
  
  paCfgReadValue(PROJ_PATH + CONFIG_REL_PATH + "config", "ui", "queryRDBdirect", sRdbRes);

  iLength = dynlen(dsDpList);
  if(iLength <= 0)
  {
    // Retrieves better queryperformance than "'{}'".
    sQueryFrom = "'{*}'";
    return sQueryFrom;
  }

  // Separate groups and dpes.
  for(int n = 1 ; n <= iLength ; n++ )
  {
    if(strpos(dsDpList[n], AES_GROUPIDF ) >= 0)
    {
      // Append group item.
      dynAppend(dsGroup, dsDpList[n]);
    }
    else
    {
      // Append dpe item.
      dynAppend(dsDpe, dsDpList[n]);
    }
  }

  if( (dynlen(dsGroup) == 0) && (dynlen(dsDpe)==0 ))
  {
    sQueryFrom="'{*}'";
    return sQueryFrom;
  }

  sQueryFrom = "'{";

  // Groups.
  iLength =dynlen(dsGroup);
  if(iLength > 0)
  {
    bGroup = true;
    for(int n = 1 ; n <= iLength ; n++ )
    {
      string sGroupName = dsGroup[n];

      strreplace(sGroupName, AES_GROUPIDF, "" );
      sGroupName = groupNameToDpName(sGroupName);
      sGroupName = dpSubStr(sGroupName, DPSUB_DP_EL );
      
      if (sRdbRes == 1)
      {
        dyn_string dsDpsList;
        dyn_string dsGroupNameDps;
        dyn_string dsGroupNameTypes;
        groupGetFilterItems(sGroupName, dsGroupNameTypes, dsGroupNameDps);
        
        for (int k = 1 ; k <= dynlen(dsGroupNameDps) ; k++ )
        {
          dyn_string dsSplitDpsList = strsplit(dsGroupNameDps[k], ":");
          if (dynlen(dsSplitDpsList) > 1)
          {
            dsGroupNameDps[k] = dsSplitDpsList[2];
          }
        }
        
        for (int i = 1 ; i <= dynlen(dsGroupNameTypes) ; i++ )
        {
          // Reading DP(E)-Items
          dyn_string dsItemsInFilter;
          int iOverFlow;
          groupGetFilteredDps(dsGroupNameTypes[i], dsGroupNameDps[i], dsItemsInFilter, iOverFlow);
    
          // Examine each filter item
          for (int j = 1 ; j <= dynlen(dsItemsInFilter) ; j++ )
          {
            // Add each item in this filter to list of all dps in this dpgroup
            dynAppend(dsDpsList, dsItemsInFilter[j]);
          }
        }
        
        for (int m = 1 ; m <= dynlen(dsDpsList) ; m++ )
        {
          dsDpsList[m] = dpSubStr(dsDpsList[m], DPSUB_DP);
        }
        
        string sGroupResolved = dsDpsList;
        strreplace(sGroupResolved, " | ", ",");        
        sQueryFrom += ((n == 1) ? "" : ",") + sGroupResolved;
      }
      else
        sQueryFrom += ((n == 1) ? "" : "," ) + "DPGROUP(" + sGroupName + ")";
    }
  }

  // Dpes
  iLength = dynlen(dsDpe);
  if(iLength > 0)
  {
    for(int n = 1; n <= iLength ; n++ )
    {
      sQueryFrom += (((n == 1) && (!bGroup)) ? "" : ",") + dsDpe[n];
    }
  }

  sQueryFrom += "}'";
  
  return sQueryFrom;
}

/**
  @par Description:
  Action on right click menu "Show alarms".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
*/
void _unAlarmScreen_groups_rightClick_showAlarms(const dyn_anytype daGroup)
{
  string sFilterRefDp = UNALARMSCREEN_DP_FILTER_PREFIX + daGroup[UNALARMSCREEN_GROUPS_INDEX_FILTER_REF];
  
  if (dpExists(sFilterRefDp))
  {
    string sFilter;
    dpGet(sFilterRefDp + ".filter", sFilter);
    unAlarmScreen_groups_highlightGroup(daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
    unAlarmScreen_displayFilter(sFilter);
    unAlarmScreen_applyFilter();
    
    string sActiveGroupLabel;
    sprintf(sActiveGroupLabel, UNALARMSCREEN_GROUPS_GROUP_ACTIVE_LABEL, daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]);
    fwAlarmScreenGeneric_setActiveGroup(sActiveGroupLabel);
  }
}

/**
  @par Description:
  Action on right click menu "Acknowledge alarms".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
*/
void _unAlarmScreen_groups_rightClick_ackAlarms(const dyn_anytype daGroup)
{
  dyn_string dsExceptions;
  
  // Get all alarms for this group.  
  string sFilterRefDp = UNALARMSCREEN_DP_FILTER_PREFIX + daGroup[UNALARMSCREEN_GROUPS_INDEX_FILTER_REF];
  if (dpExists(sFilterRefDp))
  {
    string sFilter;
    dpGet(sFilterRefDp + ".filter", sFilter);   
    
    dyn_string dsQuery = unAlarmScreen_groups_buildQuery(sFilter);
    
    dyn_dyn_anytype ddaResults;
    dpQuery(dsQuery, ddaResults);
    dynRemove(ddaResults, 1);
    
    fwOpenProgressBar("Acknowledging alarms", "...", 2);
    int iProgressCount = 1;
    int iAlarmCount = dynlen(ddaResults);
    for (int i = 1 ; i <= iAlarmCount ; i++)
    {
      fwShowProgressBar("Acknowledging ...", ((float) iProgressCount)/((float) iAlarmCount) * 100);
      iProgressCount++;
      
      // Check if acknowledging is authorized.
      bool bGranted;
      
      string sDpe = ddaResults[i][UNALARMSCREEN_GROUPS_QUERY_RESULT_INDEX_DPE];
      string sDeviceName = unGenericDpFunctions_getDpName(sDpe);
      string sType = dpTypeName(sDeviceName);
      dyn_string dsUnicosObjects;
      unGenericDpFunctions_getUnicosObjects(dsUnicosObjects);
      
      if (dynContains(dsUnicosObjects, sType) > 0)
      {						
        unGenericButtonFunctionsHMI_isAccessAllowed(sDeviceName, sType, UN_FACEPLATE_BUTTON_ACK_ALARM, bGranted, dsExceptions);
        
        if (bGranted)
        {
          unGenericObject_Acknowledge(dpSubStr(sDpe, DPSUB_SYS_DP_EL), dsExceptions);
        }
      }
      else
      {
        unGenericButtonFunctionsHMI_isAccessAllowed(sDeviceName, UN_GENERIC_USER_ACCESS, UN_FACEPLATE_BUTTON_ACK_ALARM, bGranted, dsExceptions);
        if (bGranted)
        {
          unAlarmConfig_acknowledge(dpSubStr(sDpe, DPSUB_SYS_DP_EL), dsExceptions);
        }
      }
    }
    fwCloseProgressBar("Done");
  }
  else
  {
    fwException_raise(dsExceptions, "ERROR", "Filter " + sFilterRefDp + " does not exist","");	
  }
  
  if (dynlen(dsExceptions) > 0)
  {
    fwExceptionHandling_display(dsExceptions);
    return;
  }
}

/**
  @par Description:
  Action on right click menu "Mark as seen". Sets the seen date of the group to the current date.
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
*/
void _unAlarmScreen_groups_rightClick_markAsSeen(dyn_anytype daGroup)
{
  time tNow = getCurrentTime();
  
  daGroup[UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME] = tNow;
  unAlarmScreen_groups_saveGroup(daGroup);
}

/**
  @par Description:
  Action on right click menu "Delete group".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
*/
void _unAlarmScreen_groups_rightClick_deleteGroup(const dyn_anytype daGroup)
{
  unAlarmScreen_groups_deleteGroup(daGroup, false);
  
  for (int i = 1 ; i <= dynlen(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN]) ; i++)
  {
    unAlarmScreen_groups_deleteGroup(daGroup[UNALARMSCREEN_GROUPS_INDEX_CHILDREN][i]);
  }  
  
  unAlarmScreen_groups_showGroups();
  unAlarmScreen_groups_removeGroup(daGroup);
}

/**
  @par Description:
  Rename the given group.
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group to rename.
*/
void _unAlarmScreen_groups_rightClick_renameGroup(dyn_anytype daGroup)
{
  dyn_float dfReturn;
  dyn_string dsReturn;
  
  ChildPanelOnCentralModalReturn(
      "vision/MessageInput", 
      "New name", 
      makeDynString(
        "$1:Enter the new name of the group.",
        "$2:%s",
        "$3:" + daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]
        ), 
      dfReturn, 
      dsReturn
    );
    
  if ((dynlen(dsReturn) > 0) && ("" != dsReturn[1]))
  {
    daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE] = dsReturn[1];
    unAlarmScreen_groups_saveGroup(daGroup);
    
    _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE, "text", daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]);
    _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_GROUP_TITLE, "toolTipText", daGroup[UNALARMSCREEN_GROUPS_INDEX_TITLE]);
  }
}

/**
  @par Description:
  Action on right click menu "Move group up".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
*/
void _unAlarmScreen_groups_rightClick_moveGroupUp(const dyn_anytype daGroup)
{
  _unAlarmScreen_groups_rightClick_move(daGroup, true);
}

/**
  @par Description:
  Action on right click menu "Move group down".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
*/
void _unAlarmScreen_groups_rightClick_moveGroupDown(const dyn_anytype daGroup)
{
  _unAlarmScreen_groups_rightClick_move(daGroup, false);
}

/**
  @par Description:
  Action on right click menu "Move group up or down".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
  @param  bUp     bool input,         True to move the group up, false to move it down (default up).
*/
void _unAlarmScreen_groups_rightClick_move(const dyn_anytype daGroup, bool bUp = true)
{
  dyn_dyn_anytype ddaAllGroups = unAlarmScreen_groups_getAllGroups(true);
  int iGroupCount = dynlen(ddaAllGroups);
  
  dyn_int diGroupIds = getDynInt(ddaAllGroups, UNALARMSCREEN_GROUPS_INDEX_ID);

  int iGroupPos = dynContains(diGroupIds, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
  
  if (bUp && (iGroupPos <= 1)) // Cannot move the top group up.
  {
    return;
  }
  
  if (!bUp && (iGroupPos == iGroupCount)) // Cannot move bottom group down.
  {
    return;
  }
  
  int iGroupNewPos = 0;
  
  
  // If it's a child is must be moved straight to the previous/next child, not simply the previous/next group in the DP order.
  if (daGroup[UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
  {
    // Find the parent and the other children.
    for (int i = 1 ; i <= iGroupCount ; i++)
    {
      dyn_int diSubGroupIds = getDynInt(ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_CHILDREN], UNALARMSCREEN_GROUPS_INDEX_ID);
      int iSubGroupPos = dynContains(diSubGroupIds, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
      if (bUp && (iSubGroupPos > 1)) // Cannot move the top group up.
      {
        iGroupNewPos = dynContains(diGroupIds, diSubGroupIds[iSubGroupPos -1]);
        break;
      }
      
      if (!bUp && (dynlen(diSubGroupIds) > iSubGroupPos)) // Cannot move bottom group down.
      {
        iGroupNewPos = dynContains(diGroupIds, diSubGroupIds[iSubGroupPos + 1]);
        break;
      }
    }
    
    if (iGroupNewPos < 1)
    {
      return;
    }    
  }
  else
  {
    if (bUp)
    {
      for (int i = iGroupPos - 1 ; i >= 1 ; i--)
      {
        if (!ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
        {
          iGroupNewPos = i;
          break;
        }
      }
    }
    else
    {
      for (int i = iGroupPos + 1 ; i <= iGroupCount ; i++)
      {
        if (!ddaAllGroups[i][UNALARMSCREEN_GROUPS_INDEX_IS_CHILD])
        {
          iGroupNewPos = i;
          break;
        }
      }
    }
  }
  
  if (((bUp && (iGroupNewPos < iGroupPos)) || (!bUp && (iGroupNewPos > iGroupPos))) && (iGroupNewPos > 0))
  {
    // Update order dp
    dyn_int diGroupOrder;
    dpGet(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);  
    dynRemove(diGroupOrder, iGroupPos);
    dynInsertAt(diGroupOrder, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID], iGroupNewPos);
    dynUnique(diGroupOrder);
    dpSetWait(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);
    
    unAlarmScreen_groups_showGroups();
  }
}

/**
  @par Description:
  Action on right click menu "Add new line before".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to show the alarms.  
*/
void _unAlarmScreen_groups_rightClick_addNewLine(const dyn_anytype daGroup)
{
  dyn_dyn_anytype ddaAllGroups = unAlarmScreen_groups_getAllGroups(true);
  dyn_int diGroupOrder = getDynInt(ddaAllGroups, UNALARMSCREEN_GROUPS_INDEX_ID); 
  
  dyn_anytype daNewLineGroup = unAlarmScreen_groups_createGroup(UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE);  
  
  // In the list diGroupOrder, the new group just created is not present. We add it at its expected position.  
  dynInsertAt(diGroupOrder, daNewLineGroup[UNALARMSCREEN_GROUPS_INDEX_ID], dynContains(diGroupOrder, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]));  
  
  dynUnique(diGroupOrder);
  dpSetWait(UNALARMSCREEN_GROUP_SETUP_SORTING_DP, diGroupOrder);
  unAlarmScreen_groups_showGroups();
}

/**
  @par Description:
  Action on right click menu "Delete line jump".
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group before which to remove the line break.
*/
void _unAlarmScreen_groups_rightClick_removeNewLine(const dyn_anytype daGroup)
{
  dyn_dyn_anytype ddaAllGroups = unAlarmScreen_groups_getAllGroups(true);
  dyn_int diGroupIds = getDynInt(ddaAllGroups, UNALARMSCREEN_GROUPS_INDEX_ID);
  int iGroupPos = dynContains(diGroupIds, daGroup[UNALARMSCREEN_GROUPS_INDEX_ID]);
  
  if ((iGroupPos > 1) && (UNALARMSCREEN_GROUPS_ROW_BREAK_TITLE == ddaAllGroups[iGroupPos - 1][UNALARMSCREEN_GROUPS_INDEX_TITLE]))
  {
    unAlarmScreen_groups_deleteGroup(ddaAllGroups[iGroupPos - 1]);
  }
  
  
  unAlarmScreen_groups_showGroups();
}

/**
  @par Description:
  Update the elements of the current group widget.
  
  @par Usage:
  Internal.
  
  @param  daGroup dyn_anytype input,  The group for which to update the information.  
*/
void _unAlarmScreen_groups_updateView(const dyn_anytype daGroup)
{
  int iActiveAlarmCount = 0;
  int iUnackAlarmCount = 0;
  int iNewAlarmCount = 0;
  string sDate = "";
  string sInfoText = "";
  string sBackColour;
											 
  //evaluate all the counters
  _unAlarmScreen_groups_calculateCounters(daGroup, iActiveAlarmCount, iUnackAlarmCount, iNewAlarmCount,
  											sDate, sInfoText, sBackColour);
  
  //and now update the counter display
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_ALARM_COUNT,       "text",     iActiveAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_UNACK_ALARM_COUNT, "text",     iUnackAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_NEW_ALARM_COUNT,   "text",     iNewAlarmCount);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_LAST_ALARM_DATE,   "text",     sDate);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_INFORMATION_TEXT,  "text",     sInfoText);
  _unAlarmScreen_groups_setValue(UNALARMSCREEN_GROUPS_WIDGET_ALARM_BACKGROUND,  "backCol",  sBackColour);
  
  iAlarmCount = iActiveAlarmCount;
}

/** 
 *  @par Description:
 * 	Calculate the counter displayed by the group as well as the color/text/etc.
 * 
 * @param daGroup	(dyn_anytype)	IN, The group for which to update the information	
 * @param iActiveAlarmCount	(int)	OUT	
 * @param iUnackAlarmCount	(int)	OUT	
 * @param iNewAlarmCount	(int)	OUT	
 * @param sDate	(string)	OUT	
 * @param sInfoText	(string)	OUT	
 * @param sBackColour	(string)	OUT	
 * @return value of type 'void'
 */
void _unAlarmScreen_groups_calculateCounters(const dyn_anytype daGroup, 
											  int &iActiveAlarmCount,
											  int &iUnackAlarmCount,
											  int &iNewAlarmCount,
											  string &sDate,
											  string &sInfoText,
											  string &sBackColour)
{
  int iHighestPrio = 0;
  string sHighestPrioColour;
  time tHighestPrioTime;
  time tLatestAlarm;
  
  
  dyn_string dsMappingKeys = mappingKeys(g_mAlertStateInfo);
  int iMappingKeysLength = dynlen(dsMappingKeys);
  for (int i = 1 ; i <= iMappingKeysLength ; i++)
  {
    if ((i % UNALARMSCREEN_GROUPS_LOOP_SLEEP_CONDITION) == 0)
    {
      delay(0, 1);
    }
    
    dyn_anytype daAlertState = g_mAlertStateInfo[dsMappingKeys[i]];
    
    if (daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_ACTIVE] > 0)
    {
      // Active alarms.
      iActiveAlarmCount += daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_ACTIVE];
    
      // Unack alarms.
      if (!daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_ACK])
      {
        iUnackAlarmCount++;
      }
      
      // Priority
      // If priority is the highest, take the colour.
      // If it is equal, take the colour of the most recent.
      if (daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_PRIORITY] > iHighestPrio)
      {
        iHighestPrio = daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_PRIORITY];
        sHighestPrioColour = daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_COLOUR];
        tHighestPrioTime = daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_TIME];
      }  
      else if (daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_PRIORITY] == iHighestPrio)
      {
        if (daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_TIME] > tHighestPrioTime)
        {
          iHighestPrio = daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_PRIORITY];
          sHighestPrioColour = daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_COLOUR];
          tHighestPrioTime = daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_TIME];
        }
      }
      
      // Time
      if (daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_TIME] > tLatestAlarm)
      {
        tLatestAlarm = daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_TIME];
      }
      
      // New alarms
      if (daAlertState[UNALARMSCREEN_ALARM_STATE_INDEX_TIME] > daGroup[UNALARMSCREEN_GROUPS_INDEX_SEEN_TIME])
      {
        iNewAlarmCount++;
      }
    }
    else
    {
      // Remove to avoid keeping too much data in memory.
      mappingRemove(g_mAlertStateInfo, dsMappingKeys[i]);
    }
  }
  
  // Finally, compute the color and date to display  
  if (0 == iActiveAlarmCount)
  {
    sBackColour = UNALARMSCREEN_GROUPS_NO_ALARM_COLOUR;
  }
  else
  {
    sBackColour = sHighestPrioColour;
    sDate = formatTime(UNALARMSCREEN_GROUPS_DATE_STRING_FORMAT, tLatestAlarm);    
  }
  
}


/** 
 *  @par Description:
 * 	Since IS-1953, it is possible to place the alarm group widget on a normal process panel,
 *  i.e. not in the alarm screen itself. Depending on where it is place, different actions
 *  need to be taken when clicking or right clicking. This function simply determines if the
 *  alarm group is displayed in the alarm screen or not. To do so, it just checks if a table
 *  with the name table_top is present.
 * 
 * @return value of type 'bool': TRUE is displayed in the group is selected in the alarm screen
 *                               FALSE otherwise
 */
bool unAlarmScreen_groups_isDisplayedInAlarmScreen()
{
  dyn_string dsTables = getShapes(myModuleName(), myPanelName(), "shapeType", "TABLE");

  int iNumTables = dynlen(dsTables);

  for( int i=1; i<=iNumTables; i++)
  {
    //if in the current panel there is table named table_top, we are in the alarm screen
    if ( strpos( dsTables[i], AES_TABLENAME_TOP) >= 0)
    {
      return true;
    }
  }  
  
  return false;
}
