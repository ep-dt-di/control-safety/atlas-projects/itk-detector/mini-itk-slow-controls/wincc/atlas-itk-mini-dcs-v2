/**@name LIBRARY: unBackup.ctl

@author: Frederic BERNARD (EN-ICE)

Creation Date: 30 09 2009

Modification History: 
	06/11/2009: Frederic
		- update unBackup_checkNFS(): replace "_unApplication.unBackupStatus" by "_DataManager.Backup.Status:_online.._stime"

External Function : 

Internal Functions :

Purpose: This library contains the functions to check the BACKUP of the whole application

Usage: Public

PVSS manager usage: Ctrl + UI

Constraints:
	. PVSS version: 3.6 
	. operating system: XP + Linux
*/

#uses "libunCore/unCoreDeprecated.ctl"

//@{

long unBackup_getTime(string sLine)
{
dyn_string dsSplit, dsSplitTime;
string sMonth, sHour, sMinute;
int iMonth, iHour, iMinute, iYear, iRes, iLen, i;
time tNow, t;
long q;

  dsSplit=strsplit(sLine," ");
    
  iLen=dynlen(dsSplit);  
  for(i=iLen;i>=1;i--)
    {
    if(dsSplit[i]=="")
      dynRemove(dsSplit, i);
    }
     
  tNow=getCurrentTime();  
  if(dynlen(dsSplit)==9)
    {  
    sMonth=dsSplit[6];
    switch (sMonth)
      {  
      case "Jan":
        iMonth=1;
        break;
      case "Feb":
        iMonth=2;
        break;
      case "Mar":
        iMonth=3;
        break;        
      case "Apr":
        iMonth=4;
        break;
      case "May":
        iMonth=5;
        break;
      case "Jun":
        iMonth=6;
        break;       
      case "Jul":
        iMonth=7;
        break;       
      case "Aug":
        iMonth=8;
        break;        
      case "Sep":
        iMonth=9;
        break;        
      case "Oct":
        iMonth=10;
        break;
      case "Nov":
        iMonth=11;
        break;                       
      case "Dec":
        iMonth=12;
        break;
      default:
        DebugN("unBackup_getTime() - Unknown month: " + sMonth);        
      }
    
    dsSplitTime=strsplit(dsSplit[8],":");
    if(dynlen(dsSplitTime)==2)
      {
      sHour=dsSplitTime[1];
      sMinute=dsSplitTime[2];
      sscanf(sHour,"%d", iHour);
      sscanf(sMinute,"%d", iMinute);
      iYear=year(tNow);      
      }
    else  //if date is not this year, the hours and minutes are replaced by the year
      {
      sscanf(dsSplit[8],"%d", iYear);
      iHour=iMinute=0;      
      }
                 
    t=makeTime(iYear,iMonth,dsSplit[7],iHour,iMinute,0);
    q=(long)t;
    }
  else
    DebugN("unBackup_getTime() - wrong parameter: dynlen=" + dynlen(dsSplit) + " - " + dsSplit);  
      
return q;  
}
  
//----------------------------

void unBackup_checkNFS(const string sSystemName, const string sPath, bool &bRes)
{
string sCommand, sLog, sOutput, sProjName;
dyn_string dsOutput;  
int iRes, i, iLen, iPeriod;
long qBackup, qLastBackup;
time tTimeLastBackup;
bool bOk;   
   
  sLog=getPath(LOG_REL_PATH);
  sProjName=unBackup_getProjName();  
  sLog=sLog+UNBACKUP_LOG_OUTPUT;
  sCommand="ls -l " + sPath + " > " + sLog;
  iRes = system(sCommand);
  if(iRes>=0)
    {
    bOk=fileToString(sLog, sOutput);
    if(bOk && sOutput!="")
      {          
      dsOutput=strsplit(sOutput, "\n");
      iLen=dynlen(dsOutput);
      for(i=1; i<=iLen; i++)
        {
        if(patternMatch("*" + sProjName, dsOutput[i]))
          {
          qBackup=unBackup_getTime(dsOutput[i]);
          dpGet(sSystemName + "_DataManager.Backup.Status:_online.._stime", tTimeLastBackup);
          qLastBackup=(long)tTimeLastBackup;
          iPeriod=600;//max time granted for the unBackup.ctl script which localy manages the backup folders... (PVSSToolMedia)
          qLastBackup=qLastBackup-iPeriod;
          //DebugN("unBackup_checkNFS",qBackup,qLastBackup,(qBackup>=qLastBackup));          
          if(qBackup>=qLastBackup)  bRes=TRUE;
          else
            {
            time t1, t2;
            string s1, s2;
            t1=qBackup;
            t2=qLastBackup;
            s1=(string)t1;
            s2=(string)t2;
            DebugTN("unBackup_checkNFS() - Backup time is not correct! " + dsOutput[i] + " on NFS= " + s1 + " - Local Backup (dpe)= " + s2);
            }
          } 
        }
      }
    else
      DebugN("unBackup_checkNFS() - No result found!");        
    }
  else
    DebugN("unBackup_checkNFS() - 'system' command returned an error!");            
      
}
    
//----------------------------
/** unBackup_checkDistantBackup
  *
  * @reviewed 2018-06-22 @whitelisted{FalsePositive}
  */
bool unBackup_checkDistantBackup(const string sSystemName, dyn_string &exceptionInfo)
{
string sPath, sFunction;
int iRes;
bool bOk=FALSE;
 
  sPath=unBackup_getFinalpath(sSystemName, exceptionInfo);
  if(sPath!="")
    {  
    sFunction=unGenericDpFunctions_getDescription(sSystemName + UNBACKUP_SCHEDULER_CAT);
   if(isFunctionDefined(sFunction))
      {
      iRes=evalScript(bOk, "bool main(const string sSystemName, const string sPath)" +
      "{" +
      "bool bTest=FALSE;" +
      sFunction + "(sSystemName, sPath, bTest);" +
      "return bTest;" +
      "}", makeDynString(), sSystemName, sPath);
      }
    else
      fwException_raise(exceptionInfo, "ERROR", "Invalid check Function: " + sFunction, "");
    }
  else
    fwException_raise(exceptionInfo, "ERROR", "Invalid backup parameters - Path: " + sPath, "");
  
return bOk;                   
}

//----------------------------

string unBackup_getFinalpath(const string sSystemName, dyn_string &exceptionInfo)
{
string sPath, sDpN, sHost;

  sDpN=sSystemName + UNBACKUP_SCHEDULER_CAT + ".actions.script.path";
  if(dpExists(sDpN))
    {
    sPath=unGenericDpFunctions_getDescription(sDpN);
    if(sPath=="")
      {
      //set default UNICOS EN-ICE/BE-CO path
      dpGet(sSystemName + "_unApplication.hostname", sHost);    
      sPath="/nfs/cs-ccr-backup2/backup/PVSS_BACKUP/"+sHost;
      unConfigGenericFunctions_setDescription(sDpN, sPath, exceptionInfo);      
      }
    }
  else
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","DPDOESNOTEXIST") + " - " + sDpN, "");         
    
  return sPath;  
}

//----------------------------
/** unBackup_checkLocalBackup
  *
  * @reviewed 2018-06-22 @whitelisted{FalsePositive}
  */
bool unBackup_checkLocalBackup(const string sSystemName, dyn_string &exceptionInfo)
{
string sLastBackup, sTimeLastBackup, sDpN;
time tTimeLastBackup;
bool bLocalBackup;
int iPeriod;
long q1, q2;

  sDpN=sSystemName + "_unApplication.unBackupStatus";
  if(dpExists(sDpN))
    {			
    dpGet(sDpN, sLastBackup,
          sDpN + ":_online.._stime", tTimeLastBackup,
          sSystemName + "_OnlineBackup.interval", iPeriod);
    
  	q1=(long)tTimeLastBackup;
    q2=(long)getCurrentTime();        	
    if(iPeriod>0)
      {      
    	if((q2-q1)>= iPeriod)
        bLocalBackup=FALSE;
    	else
    	  {
    		if (sLastBackup!="") bLocalBackup=TRUE;
        else bLocalBackup=FALSE;      
    		}
      }
    else
      bLocalBackup=TRUE;      	
    }
  else
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration","DPDOESNOTEXIST") + " - " + sDpN, "");         

return bLocalBackup;
}

//----------------------------

bool unBackup_checkCtrl(const string sSystemName, dyn_string &exceptionInfo)
{
bool bOk;
dyn_string dsResult;

  dsResult=unBackup_getManagerState(sSystemName, exceptionInfo);

	if(dynlen(exceptionInfo)==0)
    {          
  	if (dynlen(dsResult)==5)
  		{
  		if(dsResult[1]=="0")  bOk=FALSE;
  		else  bOk=TRUE;
  		}   
    }  
  
  return bOk;  
}

//----------------------------

dyn_string unBackup_getManagerState(const string sSystemName, dyn_string &exceptionInfo)
{
dyn_string dsResult;
int iIndex, iState;  

  unSystemIntergity_manageManager("WCCOActrl", "unBackup.ctl", "", iIndex, exceptionInfo, sSystemName);	//to get iIndex (sCommand is empty)
	if(dynlen(exceptionInfo)==0)
    dsResult=unSystemIntergity_getManagerState(iIndex, iState, exceptionInfo, sSystemName);

return dsResult;    
} 

//----------------------------

unBackup_startStopKillCtrl(const string sSystemName, string sPmonCommand, dyn_string &exceptionInfo)
{
dyn_float dfReturn;
dyn_string dsReturn;
int iIndex=SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE;
string sText;
	
	if(sPmonCommand == SYSTEMINTEGRITY_START_COMMAND)
		sText = "$1:You are going to start WCCOActrl unBackup.ctl";
	else if	(sPmonCommand == SYSTEMINTEGRITY_STOP_COMMAND)
		sText = "$1:You are going to stop WCCOActrl unBackup.ctl";
	else if	(sPmonCommand == SYSTEMINTEGRITY_KILL_COMMAND)
		sText = "$1:You are going to kill WCCOActrl unBackup.ctl";	
	else if	(sPmonCommand == SYSTEMINTEGRITY_SETMANUAL_COMMAND || sPmonCommand == SYSTEMINTEGRITY_SETALWAYS_COMMAND)
		sText = "$1:You are going to change the Properties for WCCOActrl unBackup.ctl";	
	
	//confirmation to do
	unGraphicalFrame_ChildPanelOnCentralModalReturn(	"vision/MessageInfo", "Warning", 
							   									makeDynString(sText,
											   				 "$2:Yes", "$3:No"), dfReturn, dsReturn);
			
	if ((dynlen(dfReturn) == 1) && (dfReturn[1] == 1))
		{
		unSystemIntergity_manageManager("WCCOActrl", "unBackup.ctl", sPmonCommand, iIndex, exceptionInfo, sSystemName);
		}
}

//----------------------------

string unBackup_getProjName()
{
string sLog, sProjName;
dyn_string dsSplit;
int iLen;

  sLog=getPath(LOG_REL_PATH);  
  strreplace(sLog, "\\","//");
  dsSplit=strsplit(sLog, "//");			
  iLen=dynlen(dsSplit);
  if(iLen>1)
    sProjName=dsSplit[(iLen-1)];
    
	return sProjName;
}

//@}
