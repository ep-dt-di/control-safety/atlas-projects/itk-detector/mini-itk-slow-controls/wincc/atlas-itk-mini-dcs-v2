#uses "fwConfigs/fwPeriphAddressBACnet.ctl"
/**@name LIBRARY: unConfigBACnet.ctl

@author: Alexey Merezhin (EN/ICE)

Creation Date: 25/09/2015

Purpose:
This library contains the function used to import the BACnet front end.

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . unGeneration.cat
  . UNICOS-JCOP framework
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

// constant for BACnet system integrity
const string c_unSystemIntegrity_BACNET = "BACnet_";
// constant for import of BACnet
const string UNBACNET_DPTYPE = "BACnet";
const string BACNET_POLL_PREFIX = "_BACNET_POLL_";

const string c_unSystemIntegrity_UNBACnet = "BACnet_";

/*importation line example: 
PLCCONFIG;BACnet;fe_name;app_name;594246;1:137.138.205.209:47900;1000;AnalogInput.0.Units
*/
const int UN_CONFIG_BACNET_LENGTH = 7;

const int UN_CONFIG_BACNET_DEVICE_ID = 4;
const int UN_CONFIG_BACNET_CONN_INFO = 5;
const int UN_CONFIG_BACNET_POLL_INTERVAL = 6;
const int UN_CONFIG_BACNET_COUNTER = 7;

/**
Purpose: check the coherency of the delete command

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  dsDeleteDps, dyn_string, output, list of dp that will be deleted
  exceptionInfo, dyn_string, output, for errors

*/
unConfigBACnet_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo) {
    string sFeName, sPLCSubApplication = "", sObjectType = "", sNumber = "", sDpPlcName, sPlcModPlc;
    dyn_string dsAlarmDps;

    if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
        // get PLC Name
        sFeName = dsConfig[2];
        if ((sFeName != "") && (dsConfig[1] == UN_DELETE_COMMAND)) {
            if (dynlen(dsConfig) >= 3) { // get Application
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) { // get DeviceType
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) { // get device number
                sNumber = dsConfig[5];
            }
            unGenericDpFunctions_getPlcDevices("", sFeName, sPLCSubApplication, sObjectType, sNumber, dsDeleteDps);
            // if only delete key word then delete also modbus and systemalarm
            if ((sFeName != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
                // sDpPlc is BACnet_plcName
                sDpPlcName = c_unSystemIntegrity_BACNET + sFeName;
                if (dpExists(sDpPlcName)) {
                    dynAppend(dsDeleteDps, sDpPlcName);
                    dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1), c_unSystemAlarm_dpType);
                    dynAppend(dsDeleteDps, dsAlarmDps);
                }
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

/**
Purpose: check the coherency of the front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  dsDeleteDps: dyn_string, input, dp to be deleted
  exceptionInfo: dyn_string, output, for errors

*/
unConfigBACnet_check(string currentObject, dyn_string currentLineSplit, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    switch (currentObject) {
        case UN_PLC_COMMAND:
            unConfigBACnet_checkApplication(currentLineSplit, dsDeleteDps, exceptionInfo);
            break;
        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }
}

/**
Purpose: delete function

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  exceptionInfo, dyn_string, output, for errors
*/
unConfigBACnet_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo) {
    string sPLCSubApplication = "", sObjectType = "", sNumber = "";
    dyn_string dsDpeList, exceptionInfoTemp;

    unConfigBACnet_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_deleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
            if (dynlen(dsConfig) >= 3) {
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) {
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) {
                sNumber = dsConfig[5];
            }
            unGenericDpFunctions_deletePlcDevices(dsConfig[2], sPLCSubApplication, sObjectType, sNumber, "progressBar", exceptionInfo);
            if ((dsConfig[2] != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
                unConfigBACnet_deleteFrontEnd(dsConfig[2], exceptionInfo);
            }
            if (dynlen(dsConfig) == 2) {
                // todo :check me, find good driver number
                string backnet_device = "_" + dsConfig[2];
                dpDelete(backnet_device, "_BacnetDevice");
                string backnet_pool_group = BACNET_POLL_PREFIX + dsConfig[2];
                dpDelete(backnet_pool_group, "_BacnetDevice");
            }
        }
    }
}

/**
Purpose: set front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  exceptionInfo: dyn_string, output, for errors
*/
unConfigBACnet_set(string currentObject, dyn_string currentLineSplit, dyn_string &exceptionInfo) {
    string currentDevice, sPlcName, sPlcType, sAnalogArchive;
    int iPlcNumber, driver;
    bool bEvent16;

    switch (currentObject) {
        case UN_PLC_COMMAND:
            unConfigBACnet_setApplication(currentLineSplit, exceptionInfo);
            // TODO
            break;
        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }
}

/**
Purpose: check data for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, BACnet configuration line in file + additional parameters
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
		dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
		exceptionInfo: dyn_string, output, for errors
*/
unConfigBACnet_checkApplication(dyn_string dsPLC, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    dyn_string dsPLCLine, dsPLCAdditionalData;
    int i, iRes, iDriverNum;
    string sPLCDp, sHostname, sAliasDp, tempDp;

    if (dynlen(dsPLC) == (UN_CONFIG_BACNET_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        dsPLCLine = dsPLC;
        for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
            dynAppend(dsPLCAdditionalData, dsPLCLine[UN_CONFIG_BACNET_LENGTH + 1]);
            dynRemove(dsPLCLine, UN_CONFIG_BACNET_LENGTH + 1);
        }
        unConfigBACnet_checkData(dsPLCLine, exceptionInfo);
        unConfigBACnet_checkAdditionalData(dsPLCAdditionalData, exceptionInfo);

        // first check if the PLC name is correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_NAME];
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADPLCNAME"), "");
            return;
        }

        // check if the PLC Application Name is correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION];
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
            return;
        }

        // sPLCDp is UNBACnet_plcName
        sPLCDp = getSystemName() +  c_unSystemIntegrity_UNBACnet + dsPLCLine[UN_CONFIG_PLC_NAME];
        sAliasDp = unGenericDpFunctions_dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);

        if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".") {
            sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);
        }
        if ((sAliasDp != "") && (sAliasDp != sPLCDp)) {
            if (dynContains(dsDeleteDps, sAliasDp) <= 0) {	// Alias exists, is not used by the "BACnet" config datapoint and will not be deleted
                fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADHOSTNAMEALIAS") + sAliasDp, "");
            }
        }
        if (dpExists(sPLCDp) && (dynContains(dsDeleteDps, sPLCDp) <= 0)) {
            sHostname = unGenericDpFunctions_getAlias(sPLCDp);
            iRes = dpGet(sPLCDp + ".communication.driver_num", iDriverNum);
            if (iRes >= 0) {
                if (iDriverNum != dsPLCAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkApplication: " + getCatStr("unGeneration", "BADEXISTINGPLCDRIVER") + iDriverNum, "");
                }
                if (sHostname != dsPLCLine[UN_CONFIG_PLC_NAME]) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkApplication: " + getCatStr("unGeneration", "BADEXISTINGPLCHOSTNAME"), "");
                }
            }
        }
        // TODO check extra params
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkApplication: " + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

/**
Purpose: check PLC data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, name
			3. string, subApplication
			4. int, PLC address = PLC number
		exceptionInfo, dyn_string, output, for errors
*/
unConfigBACnet_checkData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCNumber;

    if (dynlen(dsPLC) != UN_CONFIG_BACNET_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        unConfigGenericFunctions_checkPLCType(dsPLC[UN_CONFIG_PLC_TYPE], exceptionInfo);

        if (strrtrim(dsPLC[UN_CONFIG_PLC_NAME]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkData:" + getCatStr("unGeneration", "ERRORPLCNAME"), "");
        }

        if (strrtrim(dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkData:" + getCatStr("unGeneration", "ERRORSUBAPPLI"), "");
        }

        // TODO check live counter address
    }
}

/**
Purpose: check additional data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC additional data
			dsPLC must contains :
			1. unsigned, driver number
			2. string, boolean archive
			3. string, analog archive
		exceptionInfo, dyn_string, output, for errors
*/
unConfigBACnet_checkAdditionalData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCDriver;

    if (dynlen(dsPLC) != UN_CONFIG_PLC_ADDITIONAL_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkAdditionalData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkAdditionalData: bool archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkAdditionalData: analog archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_checkAdditionalData: event archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
    }
}

/**
Purpose: set PLC configuration for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file + additional parameters
			dsPLC must contains :
			(Plc config line)
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
		exceptionInfo: dyn_string, output, for errors
*/
unConfigBACnet_setApplication(dyn_string dsPLCData, dyn_string &exceptionInfo) {
    int iPLCNumber, iRes, i, position;
    string sDpName, addressReference, tempDp;
    dyn_string dsPLC, dsAdditionalParameters, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
    dyn_string dsEventArchives;
    // Check

    if (dynlen(dsPLCData) != (UN_CONFIG_BACNET_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_setApplication: " + getCatStr("unGeneration", "BADINPUTS"), "");
        return;
    }

    // 1. Separate config file info and additional parameters
    dsPLC = dsPLCData;
    dynClear(dsAdditionalParameters);
    for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
        dynAppend(dsAdditionalParameters, dsPLC[UN_CONFIG_BACNET_LENGTH + 1]);
        dynRemove(dsPLC, UN_CONFIG_BACNET_LENGTH + 1);
    }

    // check if the PLC Application Name is correct:
    tempDp = dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION];
    if (!unConfigGenericFunctions_nameCheck(tempDp)) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_setApplication: " + dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
        return;
    }

    // add BACnet driver if don't exist
    string driverDP = getSystemName() + "_Bacnet_" + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM];
    if (!dpExists(driverDP)) {
        unConfigGenericFunctions_createDp(driverDP, "_Bacnet", exceptionInfoTemp);
        if (dynlen(exceptionInfoTemp) > 0) {
            dynAppend(exceptionInfo, exceptionInfoTemp);
            return;
        }
    }
    // Set PLC configuration in UnBACnet
    sDpName = c_unSystemIntegrity_UNBACnet + dsPLC[UN_CONFIG_PLC_NAME];
    tempDp = sDpName;
    unConfigGenericFunctions_createDp(sDpName, UNBACNET_DPTYPE, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        return;
    }

    sDpName = getSystemName() + sDpName;
    unConfigGenericFunctions_setAlias(sDpName, dsPLC[UN_CONFIG_PLC_NAME], exceptionInfo);
    iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
                 sDpName + ".configuration.importTime", dsImportTimes,
                 sDpName + ".configuration.archive_bool", dsBoolArchives,
                 sDpName + ".configuration.archive_analog", dsAnalogArchives,
                 sDpName + ".configuration.archive_event", dsEventArchives);
    position = dynContains(dsSubApplications, dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]);
    if (position <= 0) {
        position = dynlen(dsSubApplications) + 1;
    }
    // if the event archive is empty, set it to bool archive
    if (dynlen(dsEventArchives) <= 0) {
        dsEventArchives = dsBoolArchives;
    }

    dsSubApplications[position] = dsPLC[UN_CONFIG_PLC_SUBAPPLICATION];
    dsImportTimes[position] = (string)getCurrentTime();
    dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
    dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
    dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];
    iRes = dpSetWait(sDpName + ".configuration.importTime", dsImportTimes,
                     sDpName + ".configuration.archive_bool", dsBoolArchives,
                     sDpName + ".configuration.archive_analog", dsAnalogArchives,
                     sDpName + ".configuration.archive_event", dsEventArchives,
                     sDpName + ".configuration.subApplications", dsSubApplications,
                     sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]);

    if (iRes < 0) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_setApplication: " + getCatStr("unPVSS", "DPSETFAILED"), "");
    }

    // 4. Create _UnSystemAlarm datapoints
    unSystemIntegrity_createSystemAlarm(tempDp,
                                        DS_pattern,
                                        getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                        exceptionInfo);

    // Create a BACnet device
    string backnet_device = "_" + dsPLC[UN_CONFIG_PLC_NAME];
    dpCreate(backnet_device, "_BacnetDevice");
    dpSetWait(backnet_device + ".DeviceId", dsPLC[UN_CONFIG_BACNET_DEVICE_ID],
        backnet_device + ".ConnInfo", dsPLC[UN_CONFIG_BACNET_CONN_INFO]);
    dpSetWait(backnet_device + ".Active", false);

    string backnet_pool_group = BACNET_POLL_PREFIX + dsPLC[UN_CONFIG_PLC_NAME];
    dpCreate(backnet_pool_group, "_PollGroup");
    dpSetWait(backnet_pool_group + ".PollInterval", dsPLC[UN_CONFIG_BACNET_POLL_INTERVAL],
              backnet_pool_group + ".Active", false);

    unConfigBACnet_setAddress(sDpName + ".communication.counter", dsPLC[UN_CONFIG_PLC_NAME], dsPLC[UN_CONFIG_BACNET_COUNTER], 
        DPATTR_ADDR_MODE_INPUT_POLL, dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], 800, exceptionInfo);
}


/** Function checks whether addressReference follows the BACnet address format.

*/
unConfigBACnet_checkAddress(string addressReference, dyn_string &exceptionInfo)
{
    // TODO : implement check bacnet address function 
    if (addressReference != "") {
        // how to check it?
        // fwPeriphAddress_check(dpe, configParameters, exceptionInfo);
    }
}

unConfigBACnet_setAddress(string dpe, string frontEndName, string addressReference, int mode, mixed driverNum, int defaultConvertionDataType, dyn_string& exceptionInfo)
{
    if (addressReference == "") {
        fwPeriphAddress_delete(dpe, exceptionInfo);
    } else {
        dyn_string configParameters = makeDynString();
        configParameters[FW_PARAMETER_FIELD_COMMUNICATION] = fwPeriphAddress_TYPE_BACNET;
        configParameters[FW_PARAMETER_FIELD_DRIVER] = driverNum;
        configParameters[FW_PARAMETER_FIELD_ADDRESS] = frontEndName + "." + addressReference;
        configParameters[FW_PARAMETER_FIELD_MODE] = mode;
        configParameters[FW_PARAMETER_FIELD_DATATYPE] = defaultConvertionDataType;
        configParameters[FW_PARAMETER_FIELD_ACTIVE] = true;
        configParameters[FW_PARAMETER_FIELD_LOWLEVEL] = 0;
        configParameters[fwPeriphAddress_BACNET_POLL_GROUP] = BACNET_POLL_PREFIX + frontEndName;

        fwPeriphAddress_set(dpe, configParameters, exceptionInfo);
        if (dynlen(exceptionInfo) > 0) {
            DebugTN(exceptionInfo);
        }
    }
}


string DRV_BACNET_convertToUnicosAddress(string sDeviceDpeName, string sDeviceType)
{
  int iConfigType;
  string sFrontEnd;
  mixed mAddreessReference;

  dpGet(sDeviceDpeName + ":_address.._type",       iConfigType,
        sDeviceDpeName + ":_address.._reference" , mAddreessReference);

  if( iConfigType == DPCONFIG_NONE )
  {
    // if addr is not defined - return an empty string
    return "";
  }

  sFrontEnd = unGenericObject_GetPLCNameFromDpName(sDeviceDpeName);

  // sFrontEnd = BACnet_FrontEndName -> Remove "BACnet_"
  if( substr(sFrontEnd, 0, strlen("BACnet_")) == "BACnet_" )
  {
    sFrontEnd = substr(sFrontEnd, strlen("BACnet_"));
  }

  // Remove FrontEnd from the address reference -> FrontEnd.Address1.Address2.Address3
  if( substr(mAddreessReference, 0, strlen(sFrontEnd + ".")) == sFrontEnd + "." )
  {
    mAddreessReference = substr(mAddreessReference, strlen(sFrontEnd + "."));
  }

  return mAddreessReference;

}




/**
Purpose: delete Front-end config

Parameters :
	sPlcName, string, input, plc name
	exceptionInfo, dyn_string, output, for errors
*/
unConfigBACnet_deleteFrontEnd(string sPlcName, dyn_string& exceptionInfo) {
    string sDpPlcName, sPlcModPlc = "", exceptionText;
    int i, length, iRes;
    dyn_string dsAlarmDps;


    sDpPlcName = c_unSystemIntegrity_UNBACnet + sPlcName;
    if (!dpExists(sDpPlcName)) {
        sDpPlcName = "";
    }

    if (sDpPlcName != "") {
        if (unGenericDpFunctions_getSystemName(sDpPlcName) == getSystemName()) {
            string driver;
            dpGet(sDpPlcName + ".communication.driver_num", driver);

            // 2. Delete _Un_Plc
            iRes = dpDelete(substr(sDpPlcName, strpos(sDpPlcName, ":") + 1));
            if (iRes < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_deleteFrontEnd: " + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
            }
            // 3. Delete alarm system
            dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1), c_unSystemAlarm_dpType);
            length = dynlen(dsAlarmDps);
            for (i = 1; i <= length; i++) {
                _unSystemIntegrity_modifyApplicationAlertList(dsAlarmDps[i], false, exceptionInfo);
                iRes = dpDelete(dsAlarmDps[i]);
                if (iRes < 0) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_deleteFrontEnd: " + dsAlarmDps[i] + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                }
            }
            // 4. delete BACnet server / subs
            DebugTN("TODO : delete BACnet server?");
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigBACnet_deleteFrontEnd: " + sDpPlcName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
        }
    }
}

/**
Purpose: return the list of Dps that wil be archived during the set of the front-end

Parameters:
		sCommand: string, input, the current entry in the import file
		sDp: string, input, the given dp: PLC or SystemAlarm  Dp
		sFEType: string, input, the front-end type
		dsArchiveDp: dyn_string, output, list of archivedDp
*/
BACnet_getFrontEndArchiveDp(string sCommand, string sDp, string sFEType, dyn_string &dsArchiveDp) {
    switch (sCommand) {
        case UN_PLC_COMMAND:
            dsArchiveDp = makeDynString();
            break;
        default:
            dsArchiveDp = makeDynString();
            break;
    }
}


BACnet_Com_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo)
{
  unsigned uBacnetPollInterval;
  string sFrontEndName, sFrontEndDriverNumber, sBacnetDevice, sBacnetId, sBacnetConnectionInfo, sBacnetCounterAddress, sBacnetPoll;
  dyn_string dsDpParameters, dsBoolArchive, dsAnalogArchive, dsEventArchive, dsTemp;


  if( dynlen(dsParam) == UN_CONFIG_EXPORT_LENGTH )
  {
    sFrontEndName = dsParam[UN_CONFIG_EXPORT_PLCNAME];
    dpGet(c_unSystemIntegrity_UNBACnet + sFrontEndName + ".configuration.archive_bool",   dsBoolArchive,
          c_unSystemIntegrity_UNBACnet + sFrontEndName + ".configuration.archive_analog", dsAnalogArchive,
          c_unSystemIntegrity_UNBACnet + sFrontEndName + ".configuration.archive_event",  dsEventArchive,
          c_unSystemIntegrity_UNBACnet + sFrontEndName + ".communication.driver_num",     sFrontEndDriverNumber);


    // Delete header
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
    dynAppend(dsDpParameters, "FRONT_END");
    dynAppend(dsDpParameters, "[APPLICATION]");
    dynAppend(dsDpParameters, "[DEVICE_TYPE]");
    dynAppend(dsDpParameters, "[DEVICE_NUMBER]");
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

    // Delete statement
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
    dynAppend(dsDpParameters, "*");
    dynAppend(dsDpParameters, "*");
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    unExportDevice_writeDsStringToFile(makeDynString(), "");

    // PLCCONFIG header
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, "# Config Line : " + UN_PLC_COMMAND);
    dynAppend(dsDpParameters, "BACnet");
    dynAppend(dsDpParameters, "FRONT_END");
    dynAppend(dsDpParameters, "APPLICATION");
    dynAppend(dsDpParameters, "DEVICE_ID");
    dynAppend(dsDpParameters, "BACNET_CONN_INFO");
    dynAppend(dsDpParameters, "POLLING_SAMPLING_TIME");
    dynAppend(dsDpParameters, "COUNTER_ADDRESS");
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

    // PLCCONFIG statement
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, UN_PLC_COMMAND);
    dynAppend(dsDpParameters, "BACnet");
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);

    // Create a BACnet device
    sBacnetDevice = "_"             + dsParam[UN_CONFIG_EXPORT_PLCNAME];
    sBacnetPoll   = "_BACNET_POLL_" + dsParam[UN_CONFIG_EXPORT_PLCNAME];
    dpGet(sBacnetDevice + ".DeviceId",     sBacnetId,
          sBacnetDevice + ".ConnInfo",     sBacnetConnectionInfo,
          sBacnetPoll   + ".PollInterval", uBacnetPollInterval);

    dynAppend(dsDpParameters, sBacnetId);
    dynAppend(dsDpParameters, sBacnetConnectionInfo);
    dynAppend(dsDpParameters, uBacnetPollInterval);

    sBacnetCounterAddress = DRV_BACNET_convertToUnicosAddress(c_unSystemIntegrity_UNBACnet + sFrontEndName + ".communication.counter", UNBACNET_DPTYPE);
    dynAppend(dsDpParameters, sBacnetCounterAddress);

    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    unExportDevice_writeDsStringToFile(makeDynString(), "");
  }
  else
  {
    fwException_raise(exceptionInfo, "ERROR", "BACnet_Com_ExportConfig(): Wrong Parameters", "");
  }
}





/**
Purpose: return the driver config of BACnet front-end

Parameters:
        iDriverNumber:      int,        input,  the driver number
        sName:                  string,         output, the name to display
        sDriverName:            string,         output,     the manager name
        iManType:   int, output, the manager type
        sCommandLine:   int, output, the command line to start the manager
*/
BACnet_getFrontEndManagerConfig(int iDriverNumber, string &sName, string &sDriverName, int &iManType, string &sCommandLine)
{
  sName = "BACnet";
  sDriverName = "WCCOAbacnet";
  iManType = DRIVER_MAN;
  sCommandLine = "-num "+iDriverNumber;
}

