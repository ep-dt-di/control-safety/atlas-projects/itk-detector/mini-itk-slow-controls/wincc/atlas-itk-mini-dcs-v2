#uses "unFrontEnd/unFrontEndDeprecated.ctl"

/**@file unConfigGenericFunctions.ctl

This library contains the function used to set Unicos database configuration.

@author
	Vincent Forest (LHC-IAS)

@par Creation Date
	31/10/2002

@par Usage:
	Public.

@par Managers
	PVSS/WinCCOA manager usage: Ctrl, NV, NG

@par Constraints:
	. unGeneration.cat & unPVSS.cat
	. JCOP framework
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

//@{

const string UN_FE_POLL_DP_PREFIX = "_poll_PLC_";

//------------------------------------------------------------------------------------------------------------------------
//unConfigGenericFunctions_checkUnicosPLC
/**
Purpose: check the config of the UNICOS PLC

@par Usage:
        External function

@par manager usage:
        Ctrl, NG, NV

@par Constraints:
        . PVSS version: 3.0
        . operating system: NT and W2000, but tested only under W2000.
        . distributed system: yes.

@param          currentObject: string, input, type of config
@param          currentLineSplit: dyn_string, input, config
@param          dsDeleteDps: dyn_string, input, dp to be deleted
@param          exceptionInfo: dyn_string, output, for errors

@reviewed 2018-11-13 @whitelisted{Callback} Used by device import
*/
unConfigGenericFunctions_checkUnicosPLC(string currentObject, dyn_string currentLineSplit, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkUnicosPLC", currentObject, currentLineSplit, dsDeleteDps, "-");
    switch (currentObject) {
        case UN_PLC_COMMAND:
            unConfigGenericFunctions_checkPLCApplication(currentLineSplit, dsDeleteDps, exceptionInfo);
            break;
        case UN_PLC_COMMAND_EXTENDED:
            unConfigGenericFunctions_checkPLCCommunication(currentLineSplit, dsDeleteDps, exceptionInfo);
            break;
        case c_unSystemAlarm_dpType:
            unConfigGenericFunctions_checkSystemAlarm(currentLineSplit, exceptionInfo);
            break;
        case UN_FESYSTEMALARM_COMMAND:
            unConfigGenericFunctions_checkFESystemAlarm(currentLineSplit, exceptionInfo);
            break;
        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------
//unConfigGenericFunctions_setUnicosPLC
/**
Purpose: set the config of the UNICOS PLC


@param		currentObject string, input, type of config. Possibilities:
		- UN_PLC_COMMAND
		- UN_PLC_COMMAND_EXTENDED
		- c_unSystemAlarm_dpType
		- UN_FESYSTEMALARM_COMMAND
@param		currentLineSplit dyn_string, input, config
@param		exceptionInfo dyn_string, output, for errors

@par Usage:
External function
PVSS manager usage: Ctrl, NG, NV

@par Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_setUnicosPLC(string currentObject, dyn_string currentLineSplit, dyn_string &exceptionInfo) {
    string currentDevice, sPlcName, sPlcType, sAnalogArchive;
    int iPlcNumber, driver;
    bool bEvent16;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_setUnicosPLC", currentObject, currentLineSplit, "-");
    switch (currentObject) {
        case UN_PLC_COMMAND:
            unConfigGenericFunctions_setPLCApplication(currentLineSplit, exceptionInfo);
            break;
        case UN_PLC_COMMAND_EXTENDED:
            unConfigGenericFunctions_setPLCCommunication(currentLineSplit, exceptionInfo);
            break;
        case c_unSystemAlarm_dpType:
            if (dynlen(currentLineSplit) == UN_CONFIG_SETSYSTEM_ALARM_LENGTH) {
                currentDevice = currentLineSplit[UN_CONFIG_SYSTEMALARM_DEVICENAME];
                sPlcName = currentLineSplit[UN_CONFIG_SYSTEMALARM_PLCNAME];
                sPlcType = currentLineSplit[UN_CONFIG_SYSTEMALARM_TYPE];
                iPlcNumber = (int)currentLineSplit[UN_CONFIG_SYSTEMALARM_PLCNUMBER];
                driver = (int)currentLineSplit[UN_CONFIG_SYSTEMALARM_DRIVER];
                bEvent16 = (bool)currentLineSplit[UN_CONFIG_SYSTEMALARM_EVENT16];
                sAnalogArchive = currentLineSplit[UN_CONFIG_SYSTEMALARM_ARCHIVE];
//DebugN("SA", currentLineSplit, currentDevice, sPlcName, sPlcType, iPlcNumber, driver, bEvent16, sAnalogArchive);
                unConfigGenericFunctions_setSystemAlarm(currentLineSplit, makeDynString(currentDevice, sPlcName, sPlcType, iPlcNumber, driver, bEvent16, sAnalogArchive), exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setUnicosPLC: " + getCatStr("unGeneration", "BADINPUTS"), "");
            }
            break;
        case UN_FESYSTEMALARM_COMMAND:
            if (dynlen(currentLineSplit) == UN_CONFIG_SETFESYSTEM_ALARM_LENGTH) {
                currentDevice = currentLineSplit[UN_CONFIG_FESYSTEMALARM_DEVICENAME];
                sPlcName = currentLineSplit[UN_CONFIG_FESYSTEMALARM_PLCNAME];
                sPlcType = currentLineSplit[UN_CONFIG_FESYSTEMALARM_TYPE];
                iPlcNumber = (int)currentLineSplit[UN_CONFIG_FESYSTEMALARM_PLCNUMBER];
                driver = (int)currentLineSplit[UN_CONFIG_FESYSTEMALARM_DRIVER];
                bEvent16 = (bool)currentLineSplit[UN_CONFIG_FESYSTEMALARM_EVENT16];
                sAnalogArchive = currentLineSplit[UN_CONFIG_FESYSTEMALARM_ARCHIVE];
//DebugN("FESA", currentLineSplit, currentDevice, sPlcName, sPlcType, iPlcNumber, driver, bEvent16, sAnalogArchive);
                unConfigGenericFunctions_setFESystemAlarm(currentLineSplit, makeDynString(currentDevice, sPlcName, sPlcType, iPlcNumber, driver, bEvent16, sAnalogArchive), exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setUnicosPLC: " + getCatStr("unGeneration", "BADINPUTS"), "");
            }
            break;
        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setAnalogAlert
/**
Purpose: set analog alert of given datapoint with 5 ranges
:
@param		sDpName string, input, datapoint name
@param		dsdescription dyn_string, input, datapoint descriptions
@param		dfLimits dyn_float, input, limits (the limits will be sorted before setting the alert config)
@param		active bool, input, true if alert must be set to active
@param		exceptionInfo dyn_string, output, for errors

@par Usage
External function.
PVSS manager usage: Ctrl, NG, NV

@par Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setAnalogAlert(string sDpName, dyn_string dsdescription, dyn_float dfLimits, dyn_string dsAlertTexts, dyn_string dsAlertClasses, bool active, dyn_string &exceptionInfo) {
    int alertType;
    bool isAlarmActive;

    unConfigGenericFunctions_setAlarmDescription(sDpName, dsdescription, exceptionInfo);	// Set description for alert screen
    dpGet(sDpName + ":_alert_hdl.._type", alertType);
    switch (alertType) {
        case DPCONFIG_NONE:
            fwAlertConfig_createAnalog(sDpName,
                                       dsAlertTexts,
                                       dfLimits,
                                       dsAlertClasses,
                                       UN_CONFIG_DEFAULT_ALERT_PANEL,
                                       UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS,
                                       UN_CONFIG_DEFAULT_ALERT_HELP,
                                       exceptionInfo);
            if (active) {
                dpSetWait(sDpName + ":_alert_hdl.._active", active);
            }
            break;

        case DPCONFIG_ALERT_NONBINARYSIGNAL:
            fwAlertConfig_modifyAnalog(sDpName,
                                       dsAlertTexts,
                                       dfLimits,
                                       dsAlertClasses,
                                       UN_CONFIG_DEFAULT_ALERT_PANEL,
                                       UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS,
                                       UN_CONFIG_DEFAULT_ALERT_HELP,
                                       exceptionInfo);
            dpGet(sDpName + ":_alert_hdl.._active", isAlarmActive);
            if ((isAlarmActive != active) || (isAlarmActive == -1)) {
                dpSetWait(sDpName + ":_alert_hdl.._active", active);
            }
            break;

        case DPCONFIG_ALERT_BINARYSIGNAL:
        default:
            fwException_raise(exceptionInfo, "ERROR:", "unConfigGenericFunctions_setAnalogAlert :" + getCatStr("unGeneration", "BADALERT"),"");
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setDigitalAlert
/**
Purpose: set digital alert of given datapoint

Parameters:
		sDpName: string, input, datapoint name
		dsdescription: dyn_string, input, datapoint description
		bOkRange: bool, input, false if ok range = false, true if Ok range = true
		active: bool, input, true if alert must be set to active
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setDigitalAlert(string sDpName, dyn_string dsdescription, bool bOkRange, bool active, bool bSMS, dyn_string &exceptionInfo) {
    int i, length, alertType;
    dyn_float dfLimitsGood;
    dyn_string dsAlertTexts, dsAlertClasses;
    int isAlarmActive = -1;

    if (bOkRange) {	// Ok range is 1
        dsAlertTexts = makeDynString(UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_BAD,
                                     UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_OK);
        if (bSMS) {
            dsAlertClasses = makeDynString(UN_PROCESSALARM_DIGITAL_ALERT_CLASS, "");
            unProcessAlarm_addDpToList(sDpName, UN_PROCESSALARM_CATEGORY, exceptionInfo);
        } else {
            dsAlertClasses = makeDynString(UN_CONFIG_DEFAULT_ALERT_DIGITAL_ALERT_CLASS, "");
            unProcessAlarm_removeDpFromList(sDpName, exceptionInfo);
        }
    } else {		// Ok range is 0
        dsAlertTexts = makeDynString(UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_OK,
                                     UN_CONFIG_DEFAULT_ALERT_DIGITAL_TEXT_BAD);
        if (bSMS) {
            dsAlertClasses = makeDynString("", UN_PROCESSALARM_DIGITAL_ALERT_CLASS);
            unProcessAlarm_addDpToList(sDpName, UN_PROCESSALARM_CATEGORY, exceptionInfo);
        } else {
            dsAlertClasses = makeDynString("", UN_CONFIG_DEFAULT_ALERT_DIGITAL_ALERT_CLASS);
            unProcessAlarm_removeDpFromList(sDpName, exceptionInfo);
        }
    }
    unConfigGenericFunctions_setAlarmDescription(sDpName, dsdescription, exceptionInfo);	// Set description for alert screen
    dpGet(sDpName + ":_alert_hdl.._type", alertType);
    switch (alertType) {
        case DPCONFIG_NONE:
            fwAlertConfig_createDigital(sDpName,
                                        dsAlertTexts,
                                        dsAlertClasses,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS,
                                        UN_CONFIG_DEFAULT_ALERT_HELP,
                                        exceptionInfo);
            if (active) {
                dpSetWait(sDpName + ":_alert_hdl.._active", active);
            }
            break;

        case DPCONFIG_ALERT_BINARYSIGNAL:
            fwAlertConfig_modifyDigital(sDpName,
                                        dsAlertTexts,
                                        dsAlertClasses,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL,
                                        UN_CONFIG_DEFAULT_ALERT_PANEL_PARAMETERS,
                                        UN_CONFIG_DEFAULT_ALERT_HELP,
                                        exceptionInfo);
            dpGet(sDpName + ":_alert_hdl.._active", isAlarmActive);
            if ((isAlarmActive != active) || (isAlarmActive == -1)) {
                dpSetWait(sDpName + ":_alert_hdl.._active", active);
            }
            break;

        case DPCONFIG_ALERT_NONBINARYSIGNAL:
        default:
            fwException_raise(exceptionInfo, "ERROR:", "unConfigGenericFunctions_setDigitalAlert :" + getCatStr("unGeneration", "BADALERT"),"");
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getAliasLink
/**
Purpose: get alias of given datapoint and its device link

Parameters:
		sAlias: string, input, composite alias deviceAlias,deviceLink
		sReturnDeviceAlias: string, output, alias
		sReturnDeviceLink: string, output, device link

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_getAliasLink(string sAlias, string &sReturnDeviceAlias, string &sReturnDeviceLink) {
    string sDeviceAlias, sDeviceLinks, sTemp;
    int pos;

    pos = strpos(sAlias, UN_CONFIG_GROUP_SEPARATOR);
    if (pos > 0) {
        sDeviceAlias = substr(sAlias, 0, pos);
        sDeviceLinks = substr(sAlias, pos + 1, strlen(sAlias));
    } else {
        sDeviceAlias = sAlias;
    }

    sTemp = substr(sDeviceLinks, strlen(sDeviceLinks) - 1, strlen(sDeviceLinks));
    if (sTemp == UN_CONFIG_GROUP_SEPARATOR) {
        sDeviceLinks = substr(sDeviceLinks, 0, strlen(sDeviceLinks) - 1 );
    }

    sReturnDeviceAlias = sDeviceAlias;
    sReturnDeviceLink = sDeviceLinks;

//DebugN("unConfigGenericFunctions_getAliasLink", sDeviceAlias, sDeviceLinks, sReturnDeviceLink);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setAlias
/**
Purpose: set alias of given datapoint

Parameters:
		sDpName: string, input, datapoint name
		sAlias: string, input, alias
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setAlias(string sDpName, string sAlias, dyn_string &exceptionInfo) {
    int position;
    dyn_string existingAliases, existingDps;
    string sDpNameTemp, sDpAlias;
    dyn_errClass err;

    if (dpExists(sDpName)) {
        sDpAlias = dpAliasToName(sAlias);
        if (sDpAlias != "") { // Alias dp exists
            if (substr(sDpAlias, strlen(sDpAlias) - 1) == ".") {	// If last character is a point, delete it
                sDpAlias = substr(sDpAlias, 0, strlen(sDpAlias) - 1);
            }
            if (sDpAlias != sDpName) { // Error only if Alias is not currently used by DP sDpName
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setAlias: " + sAlias + getCatStr("unGeneration", "ALIASALREADYUSED"), "");
            }
        } else {
            sDpNameTemp = sDpName;
            if (strpos(sDpName, ".") < 0) {
                sDpNameTemp = sDpName + ".";
            }
            dpSetAlias(sDpNameTemp, sAlias);
            err = getLastError();
            if (dynlen(err) > 0) {
                throwError(err);
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setAlias: " + getCatStr("unGeneration", "ERRORSETTINGALIAS"), "");
            }
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setAlias: " + sDpName + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setDeviceLink
/**
Purpose: save the device links

Parameters:
		sDpName: string, input, datapoint name
		sDeviceLink: string, input, list of device links

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setDeviceLink(string sDpName, string sDeviceLink) {
//DebugN("unConfigGenericFunctions_setDeviceLink", sDeviceLink);
    dpSet(sDpName + ".statusInformation.deviceLink", sDeviceLink);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setDescription
/**
Purpose: set description of given datapoint

Parameters:
		sDpName: string, input, datapoint name
		sDescription: string, input, description
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setDescription(string sDpName, string sDescription, dyn_string &exceptionInfo) {
    string sDpNameTemp;
    int iRes;

    if (dpExists(sDpName)) {
        sDpNameTemp = sDpName;
        if (strpos(sDpName, ".") < 0) {
            sDpNameTemp = sDpName + ".";
        }
        iRes = dpSetDescription(sDpNameTemp, sDescription);
        if (iRes < 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setDescription: " + sDpName + getCatStr("unPVSS", "DPSETDESCRIPTIONFAILED"), "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setDescription: " + sDpName + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setAlarmDescription
/**
Purpose: set Alarm description of given datapoint element

Parameters:
		sDpe: string, input, datapoint element
		dsDescription: dyn_string, input, descriptions
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: W2000 and XP
	. distributed system: yes.
*/

unConfigGenericFunctions_setAlarmDescription(const string sDpe, dyn_string dsDescription, dyn_string &exceptionInfo) {
    if (dpExists(sDpe)) {
        string sDescription;
        int iRes, len, i;

        len = dynlen(dsDescription);
        if (len < UN_CONFIG_ALARM_DESCRIPTION_LENGTH) {
            for (i = len + 1; i <= UN_CONFIG_ALARM_DESCRIPTION_LENGTH; i++) {
                dsDescription[i] = "";
            }
        }

        sDescription = 	unConfigGenericFunctions_FormatDomainNature(dsDescription[UN_CONFIG_ALARM_DOMAIN]) + UN_ALERT_DESCRIPTION_DELIMITER +
                        unConfigGenericFunctions_FormatDomainNature(dsDescription[UN_CONFIG_ALARM_NATURE]) + UN_ALERT_DESCRIPTION_DELIMITER +
                        dsDescription[UN_CONFIG_ALARM_ALIAS] + UN_ALERT_DESCRIPTION_DELIMITER +
                        dsDescription[UN_CONFIG_ALARM_DESCRIPTION] + UN_ALERT_DESCRIPTION_DELIMITER +
                        dsDescription[UN_CONFIG_ALARM_NAME] + UN_ALERT_DESCRIPTION_DELIMITER +
                        dsDescription[UN_CONFIG_ALARM_DEFAULTPANEL];

        iRes = dpSetDescription(sDpe, sDescription);
        if (iRes < 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setAlarmDescription: " + sDpe + getCatStr("unPVSS", "DPSETDESCRIPTIONFAILED"), "");
        }

    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setAlarmDescription: " + sDpe + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
    }

}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setUnit
/**
Purpose: set unit of given datapoint

Parameters:
		sDpName: string, input, datapoint name
		sUnit: string, input, unit
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. Change unit only for int, float, unsigned and boolean element
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setUnit(string sDpName, string sUnit, dyn_string &exceptionInfo) {
    int iRes;
    int dpeType;
    string sDpNameTemp;

    if (dpExists(sDpName)) {
        sDpNameTemp = sDpName;
        if (strpos(sDpName, ".") < 0) {
            sDpNameTemp = sDpName + ".";
        }
        dpeType = dpElementType(sDpNameTemp);
        switch (dpeType) {
            case DPEL_INT:
            case DPEL_FLOAT:
            case DPEL_UINT:
            case DPEL_BOOL:
                iRes = dpSetUnit(sDpName, sUnit);
                if (iRes != 0) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setUnit: " + sDpName + getCatStr("unPVSS", "DPSETUNITFAILED"), "");
                }
                break;

            default:
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setUnit: " + sDpName + getCatStr("unGeneration", "DPDOESNOTHAVEUNIT"), "");
                break;
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setUnit: " + sDpName + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setFormat
/**
Purpose: set format to given datapoint

@par Usage
Public

@par PVSS managers
Ctrl, Vision

@par Constraints
	. Change format only for int, float, unsigned element
	. Input sDpFormat must be changed to fit with PVSS format.
		In Unicos, format string is set with : "#" (digit), "." floating point and "EXP" exponential format
		"EXP" is equal to UN_FACEPLATE_DEFAULT_EXP_FORMAT constant in PVSS ("%2.3e").
		"###.#" is equal to "%5.1f" in PVSS


@param 		sDpName: string, input, datapoint name
@param 		sFormat: string, input, format pattern. Example: #.##
                          Optionally, it can be separated by | with a second field LOG_SCALE. Example: #.##|LOG_SCALE
                          The parameter LOG_SCALE indicates that the trending will be shown in logarithmic scale
                          See UN_DEVICE_LOG_SCALE_CONFIGURATION for the exact parameter name.
@param 		exceptionInfo: dyn_string, output, for errors
*/

unConfigGenericFunctions_setFormat(string sDpName, string sDpFormat, dyn_string &exceptionInfo) {
    bool bRes, bSet = true;
    int dpeType, length, afterPoint, iFixedFormat;
    string sDpNameTemp;
    string pvssFormat, sFixedFormat;
    string sFormat = strtoupper(sDpFormat);
    string sOriginalFormat = sFormat;
    int iLogScale = -1;
    dyn_string dsUnicosConfig;
    dyn_string dsFormatSplit;
    //check if the optional logarithmic scale flag is present. If so, extract it
//  DebugN("unConfigGenericFunctions_setFormat - sFormat before" ,sOriginalFormat);
    dsFormatSplit = _unConfigGenericFunctions_extractFormatSubitems(sFormat);
    sFormat = dsFormatSplit[1];
    if (dynlen(dsFormatSplit) > 1) {
        switch (dsFormatSplit[2]) {
            case UN_DEVICE_LOG_SCALE_CONFIGURATION:
                iLogScale = 1;
                break;
        }
    } else if (strpos(sOriginalFormat, UN_PARAMETER_SUBITEM_DELIMITER) >= 0) { //the format is like "#.##|" it means log scale forced to 0
        iLogScale = 0;
    }

//  DebugN("unConfigGenericFunctions_setFormat - dsFormatSplit" ,dsFormatSplit);
//  DebugN("unConfigGenericFunctions_setFormat - sDpForma after" ,sFormat);
//  DebugN("unConfigGenericFunctions_setFormat - iLogScale" ,iLogScale);
//  DebugN("################################################################################");
    if (dpExists(sDpName)) {
        sDpNameTemp = sDpName;
        if (strpos(sDpName, ".") < 0) {
            sDpNameTemp = sDpName + ".";
        }
        dpeType = dpElementType(sDpNameTemp);
        switch (dpeType) {
            case DPEL_INT:
            case DPEL_FLOAT:
            case DPEL_UINT:
                if (sFormat == UN_FACEPLATE_EXP) {
                    pvssFormat = UN_FACEPLATE_DEFAULT_EXP_FORMAT;
                } else {
                    length = strlen(sFormat);
                    if (length > 0) {
                        if (strpos(sFormat, UN_DISPLAY_VALUE_DIGIT_TYPE) > 0) { // fixed digit format = D
                            sFixedFormat = substr(sFormat, 0, strpos(sFormat, UN_DISPLAY_VALUE_DIGIT_TYPE));
                            sscanf(sFixedFormat, "%d", iFixedFormat);
                            if ((iFixedFormat > 0) && (iFixedFormat < UN_DISPLAY_VALUE_DIGIT_MAX_DIGIT)) {
                                iFixedFormat++; // add "."
                                pvssFormat = "%" + iFixedFormat + UN_DISPLAY_VALUE_DIGIT_FLOAT_FORMAT + UN_DISPLAY_VALUE_DIGIT_FLOAT_TYPE; // is x.20f
                            } else {
                                bSet = false;
                                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFormat: " + sDpName + getCatStr("unGeneration", "DEBUGBADFORMAT"), "");
                            }
                        } else { //normal ##.# format or xEXP
                            if (strpos(sFormat, UN_FACEPLATE_EXP) > 0) { // xEXP
                                strreplace(sFormat, UN_FACEPLATE_EXP, "");
                                pvssFormat = UN_FACEPLATE_DEFAULT_EXP_FORMAT;
                                sFormat = "." + sFormat + "e";
                                strreplace(pvssFormat, ".3e", sFormat);
                            } else { //normal ##.# format
                                if (strpos(sFormat, ".") >= 0) {
                                    afterPoint = length - strpos(sFormat, ".") - 1;
                                } else {
                                    afterPoint = 0;
                                }
                                pvssFormat = "%" + length + "." + afterPoint + "f";
                            }
                        }
                    } else {
                        bSet = false;
//						fwException_raise(exceptionInfo,"ERROR","unConfigGenericFunctions_setFormat: " + sDpName + getCatStr("unGeneration","DEBUGBADFORMAT"),"");
                    }
                }
                if (bSet) {
//DebugN("setFormat", sDpName, sDpFormat, pvssFormat, sFormat, sFixedFormat, iFixedFormat);
                    bRes = dpSetFormat(sDpName, pvssFormat);
                    if (bRes != 0) {
                        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFormat: " + sDpName + getCatStr("unPVSS", "DPSETFORMATFAILED"), "");
                    }
                }
                break;

            default:
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFormat: " + sDpName + getCatStr("unGeneration", "DPDOESNOTHAVEFORMAT"), "");
                break;
        }

        //set the logarithmic scale in the UNICOS configuration dpe
        if (iLogScale >= 0) {
// DebugN("unConfigGenericFunctions_setFormat - iLogScale", iLogScale, dpSubStr(sDpName, DPSUB_SYS_DP));
            unGenericDpFunctions_setKeyUnicosConfiguration(dpSubStr(sDpName, DPSUB_SYS_DP), UN_DEVICE_LOG_SCALE_CONFIGURATION, makeDynString(iLogScale), exceptionInfo);
// DebugN("unConfigGenericFunctions_setFormat - UN_DEVICE_LOG_SCALE_CONFIGURATION", UN_DEVICE_LOG_SCALE_CONFIGURATION);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFormat: " + sDpName + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setGroups
/**
Purpose: associate DP with its group

Parameters:
		sDpName: string, input, device name
		dsGroupNames: dyn_string, input, group names
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_setGroups(string sDpName, dyn_string dsGroupNames, dyn_string &exceptionInfo) {
    int i, length, iError, iRes;
    dyn_string existingGroups;
    dyn_string groupDpNames, groupDpTypes;
    string groupDP, sSystemName;

    if (dpExists(sDpName)) {
        sSystemName = unGenericDpFunctions_getSystemName(sDpName);
        existingGroups = unGenericDpFunctions_groupGetNames(sSystemName);
//DebugN("unConfigGenericFunctions_setGroups, groupGetNames", sDpName, dsGroupNames, existingGroups, sSystemName);
        length = dynlen(dsGroupNames);
        for (i = 1; i <= length; i++) {
            if (dsGroupNames[i] != "") {
                if (dynContains(existingGroups, dsGroupNames[i]) <= 0) {		// Group doesn't exist
                    iError = 0;
                    if (sSystemName != getSystemName()) {
                        iError = -1;
                    } else {
                        groupCreate(dsGroupNames[i], UN_CONFIG_GROUP_PRIVATE, groupDP, UN_CONFIG_GROUP_MAIN, iError);    // PVSS lib dpGroups.ctl function
                    }
                } else {
                    groupDP = unGenericDpFunctions_groupNameToDpName(dsGroupNames[i], sSystemName);
//DebugN("unConfigGenericFunctions_setGroups, groupNameToDpName", dsGroupNames[i], sSystemName);
                    iError = 0;
                    if (groupDP == "") {
                        iError = -1;
                    }
                }
                if (iError < 0) {								// Error
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setGroup : " + dsGroupNames[i]  + getCatStr("unGeneration", "GROUPCREATEERRROR") + " " + sSystemName + " " + getSystemName(), "");
                } else {										// Group exists
                    iRes = dpGet(groupDP + ".Dps", groupDpNames,
                                 groupDP + ".Types", groupDpTypes); // Get DP associated to current group
                    if (iRes >= 0) {
                        if (dynContains(groupDpNames, sDpName) <= 0) {	// DP doesn't already belong to the group
                            dynAppend(groupDpNames, sDpName);
                            dynAppend(groupDpTypes, "");
                            iRes = dpSetWait(groupDP + ".Dps", groupDpNames,
                                             groupDP + ".Types", groupDpTypes);
                            if (iRes < 0) {
                                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setGroup : " + groupDP + ".Dps" + getCatStr("unPVSS", "DPSETFAILED"), "");
                            }
                        }
                    } else {
                        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setGroup : " + groupDP + ".Dps" + getCatStr("unPVSS", "DPGETFAILED"), "");
                    }
                }
            }//empty group?
        }//end for
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setGroup : " + sDpName + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_deleteDpInGroup
/**
Purpose: delete datapoint of specified group

Parameters:
		sDpName: string, input, device name
		sGroup: string, input, group name
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. This library uses PVSS lib dpGroups.ctl. Include it in the config file.
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_deleteDpInGroup(string sDpName, string sGroup, dyn_string &exceptionInfo) {
    int iRes, position;
    string groupDP, sSystemName;
    dyn_string groupDpNames, groupDpTypes;

    sSystemName = unGenericDpFunctions_getSystemName(sDpName);
    groupDP = unGenericDpFunctions_groupNameToDpName(sGroup, sSystemName);
//DebugN("unConfigGenericFunctions_deleteDpInGroup, groupNameToDpName", sDpName, sGroup, sSystemName, groupDP);
    if (groupDP != "") {						// Group exists
        iRes = dpGet(groupDP + ".Dps", groupDpNames,
                     groupDP + ".Types", groupDpTypes);
        if (iRes < 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_deleteDpInGroup : " + groupDP + getCatStr("unPVSS", "DPGETFAILED"), "");
        } else {
            position = dynContains(groupDpNames, sDpName);
            while (position > 0) {			// Group contains datapoint
                dynRemove(groupDpNames, position);
                dynRemove(groupDpTypes, position);
                position = dynContains(groupDpNames, sDpName);
            }
            iRes = dpSetWait(groupDP + ".Dps", groupDpNames,
                             groupDP + ".Types", groupDpTypes);
            if (iRes < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_deleteDpInGroup : " + groupDP + getCatStr("unPVSS", "DPSETFAILED"), "");
            }
            if (dynlen(groupDpNames) <= 0) { // no more dps --> delete group
                dpDelete(groupDP);
//DebugN("deleting group: ", groupDP, sGroup);
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_deleteDpInMultiGroup
/**
Purpose: remove datapoint of specified multi group

Parameters:
		sDpName: string, input, device name
		sGroupPrefix: string, input, group prefix name
		sMultiGroup: string, input, group name list
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. This library uses PVSS lib dpGroups.ctl. Include it in the config file.
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_deleteDpInMultiGroup(string sDpName, string sGroupPrefix, string sMultiGroup, dyn_string &exceptionInfo) {
    dyn_string dsGroups;
    int iLen, i;

    dsGroups = unConfigGenericFunctions_getMultiGroup(sMultiGroup);
//DebugN("unConfigGenericFunctions_deleteDpInMultiGroup", dsGroups);
    iLen = dynlen(dsGroups);
    for (i = 1; i <= iLen; i++) {
        if (dsGroups[i] != "") {
            unConfigGenericFunctions_deleteDpInGroup(sDpName, sGroupPrefix + dsGroups[i], exceptionInfo);
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getMultiGroup
/**
Purpose: get all the groups

Parameters:
		sGroup: string, input, group name list "," separated
		return: dyn_string, output, the group list

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. This library uses PVSS lib dpGroups.ctl. Include it in the config file.
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
dyn_string unConfigGenericFunctions_getMultiGroup(string sGroup) {
//DebugN("unConfigGenericFunctions_getMultiGroup", strsplit(sGroup, UN_CONFIG_GROUP_SEPARATOR));
    return strsplit(sGroup, UN_CONFIG_GROUP_SEPARATOR);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setPLCApplication
/**
Purpose: set PLC configuration for a sub application (not comm)

Parameters:
		dsConfig: dyn_string, input, PLC configuration line in file + additional parameters
			dsConfig must contains :
			(Plc config line)
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. First line of a generation file must contain the PLC description. Fields are defined in constants at this library beginning
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_setPLCApplication(dyn_string dsConfig, dyn_string &exceptionInfo) {
    dyn_string dsPLCData = dsConfig;
    int iPLCNumber, iRes, i, sIPAddress, position;
    string sDpName, addressReference, tempDp;
    dyn_string dsPLC, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
    dyn_string dsEventArchives;
    bool bOk, bCPC6;
    string sUserTag;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UNPLC " + dsPLCData[UN_CONFIG_PLC_NAME], "unConfigGenericFunctions_setPLCApplication", dsPLCData);
//DebugN("PLCAP", dsPLCData);
    // Check
    if (dynlen(dsPLCData) == (UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        bOk = true;
        bCPC6 = true;
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_RESPACK_VERSION);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_RESPACK_SMALL_VERSION_ADDRESS);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_RESPACK_MINOR_VERSION_ADDRESS);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_RESPACK_MAJOR_VERSION_ADDRESS);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_PLC_APPLICATION_ADDRESS);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_PLC_BASELINE_ADDRESS);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_ADDRESS_CMD);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_ADDRESS_COUNTER);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_ADDRESS_SEND_IP);
        dynRemove(dsPLCData, UN_CONFIG_UNPLC_CPC6_IP);
        sUserTag = "CPC6";

    } else if (dynlen(dsPLCData) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_FE_VERSION_ADDITIONAL_LENGTH)) {
        bOk = true;
        sUserTag = dsPLCData[UN_CONFIG_FE_VERSION];
// remove the version.
        dynRemove(dsPLCData, UN_CONFIG_FE_VERSION);
    } else if (dynlen(dsPLCData) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        bOk = true;
    }

    if (bOk) {
        unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UNPLC " + dsPLCData[UN_CONFIG_PLC_NAME], "unConfigGenericFunctions_setPLCApplication, common configuration", dsPLCData);
        // 1. Separate config file info and additional parameters
        dsPLC = dsPLCData;
        string sPlcModel = unConfigGenericFunctions_extractFeModel(dsPLC[UN_CONFIG_PLC_TYPE]);
        dyn_string dsAdditionalParameters;
        for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
            dynAppend(dsAdditionalParameters, dsPLC[UN_CONFIG_PLC_LENGTH + 1]);
            dynRemove(dsPLC, UN_CONFIG_PLC_LENGTH + 1);
        }

        // check if the PLC Application Name is correct:
        tempDp = dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION];
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCApplication: " + dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
            return;
        }

        // 2. Create associated _Mod_Plc dpe if it doesn't exist
        string modPlcDP = "_Mod_Plc_" + dsPLC[UN_CONFIG_PLC_NUMBER];
        unConfigGenericFunctions_createDp(modPlcDP, "_Mod_Plc", exceptionInfo);
        if (dynlen(exceptionInfo) > 0) return;

        // 3. Set PLC configuration in _UnPlc
        sDpName = c_unSystemIntegrity_UnPlc + dsPLC[UN_CONFIG_PLC_NAME];
        unConfigGenericFunctions_createDp(sDpName, UN_PLC_DPTYPE, exceptionInfo);
        if (dynlen(exceptionInfo) > 0) return;

        bool pollingFE = unConfigGenericFunctions_isPollingOnly(dsPLC[UN_CONFIG_PLC_TYPE]);
        if (pollingFE) {
            // create polling group
            string pollDp = UN_FE_POLL_DP_PREFIX + dsPLC[UN_CONFIG_PLC_NAME];
            unConfigGenericFunctions_createDp(pollDp, "_PollGroup", exceptionInfo);
            if (dynlen(exceptionInfo) > 0) return;
            dpSetWait(pollDp + ".Active", true,
                      pollDp + ".PollInterval", unConfigGenericFunctions_getPollingInterval(dsPLC[UN_CONFIG_PLC_TYPE]));
            // TODO allow custom poll interval
        }

        // other configs
        sDpName = getSystemName() + sDpName;
        unConfigGenericFunctions_setAlias(sDpName, dsPLC[UN_CONFIG_PLC_NAME], exceptionInfo);
        iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
                     sDpName + ".configuration.importTime", dsImportTimes,
                     sDpName + ".configuration.archive_bool", dsBoolArchives,
                     sDpName + ".configuration.archive_analog", dsAnalogArchives,
                     sDpName + ".configuration.archive_event", dsEventArchives);
        position = dynContains(dsSubApplications, dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]);
        if (position <= 0) {
            position = dynlen(dsSubApplications) + 1;
        }

        // if the event archive is empty, set it to bool archive
        if (dynlen(dsEventArchives) <= 0) {
            dsEventArchives = dsBoolArchives;
        }

        dsSubApplications[position] = dsPLC[UN_CONFIG_PLC_SUBAPPLICATION];
        dsImportTimes[position] = (string)getCurrentTime();
        dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
        dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
        dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];
        iRes = dpSetWait(sDpName + ".configuration.mod_PLC", "_Mod_Plc_" + dsPLC[UN_CONFIG_PLC_NUMBER],
                         sDpName + ".configuration.importTime", dsImportTimes,
                         sDpName + ".configuration.archive_bool", dsBoolArchives,
                         sDpName + ".configuration.archive_analog", dsAnalogArchives,
                         sDpName + ".configuration.archive_event", dsEventArchives,
                         sDpName + ".configuration.type", sPlcModel,
                         sDpName + ".configuration.subApplications", dsSubApplications,
                         sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                         sDpName + ".version.import", sUserTag);
        if (iRes < 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCApplication: " + getCatStr("unPVSS", "DPSETFAILED"), "");
        }

        if (bCPC6) {
            _unConfigGenericFunctions_setCPC6PLCCom(dsConfig, pollingFE, exceptionInfo);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCApplication: " + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}


//------------------------------------------------------------------------------------------------------------------------
//_unConfigGenericFunctions_splitRedundancyConfig
/** check if the PLC config contains redundancy information.
If yes, split primary and secondary PLC info

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, config line as dyn_string
@param iCPCConfigVersion input, the CPC version (5 or 6. Default: 5).
@param dsPrimaryConfig  output, config line for the primary PLC as dyn_string
@param dsSecondaryConfig  output, config line for the primary PLC as dyn_string
@param exceptionInfo output, for errors
@return true if redundancy exists
*/
bool _unConfigGenericFunctions_splitRedundancyConfig(dyn_string dsConfig,
        int iCPCConfigVersion,
        dyn_string &dsPrimaryConfig,
        dyn_mixed &dsSecondaryConfig,
        dyn_string &exceptionInfo) {
    dyn_string dsSplit, dsPLC = dsConfig;
    bool bReduOk;
    int iPlcIpIndex;

    if (iCPCConfigVersion == 6) {
        iPlcIpIndex = UN_CONFIG_UNPLC_CPC6_IP;
    } else {
        iPlcIpIndex = UN_CONFIG_PLC_EXTENDED_IP;
    }

//check that all other redu info is present
    bReduOk = (strpos(dsPLC[iPlcIpIndex], UN_PARAMETER_SUBITEM_DELIMITER) > 0);
    if (bReduOk) {
        //Redundant PLC
        //split IP addresses, primary and secondary
        dsSplit = strsplit(dsPLC[iPlcIpIndex], UN_PARAMETER_SUBITEM_DELIMITER);
        if (dynlen(dsSplit) > 1) {
            dsPLC[iPlcIpIndex] = dsSplit[1];
            dsSecondaryConfig[iPlcIpIndex] = makeDynString(dsSplit[2] + UN_CONFIG_PLC_PORT);
        } else {
            dsSecondaryConfig[iPlcIpIndex] = UN_CONFIG_PLC_REDUNDANT_HOSTNAMES;
            bReduOk = false;
        }
        if (!bReduOk) {
            //not all redundancy info is present:
            //must be like, for example, "1.2.3.4|1.2.3.5", "1.2.3.4" for primary PLC, "|1.2.3.5" optionally for redu PLC
            fwException_raise(exceptionInfo, "ERROR",
                              "_unConfigGenericFunctions_splitRedundancyConfig: " +
                              getCatStr("unGeneration", "REDUFOUNDFIELDVALUE") + dsConfig[UN_CONFIG_PLC_NUMBER] +
                              ", " + dsConfig[iPlcIpIndex] +
                              getCatStr("unGeneration", "REDUMISSINGPARAM"), "");
            return false;
        }
    } else {
        dsSecondaryConfig[iPlcIpIndex] = UN_CONFIG_PLC_REDUNDANT_HOSTNAMES;
        bReduOk = false;
    }
    dsPrimaryConfig = dsPLC;
    return bReduOk;
}

//------------------------------------------------------------------------------------------------------------------------
//_unConfigGenericFunctions_setCPC6PLCCom
/** execute the set: set the CPC6 PLC com

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfig  input, config line as dyn_string
@param exceptionInfo output, for errors
*/
_unConfigGenericFunctions_setCPC6PLCCom(dyn_string dsConfig, bool pollingFE, dyn_string &exceptionInfo) {
    int iPLCNumber, iRes, i, sIPAddress, position, iPosInFile, iPosNextSection, length, iPVSS_Modbus_Uint;
    string sDpName, addressReference, exceptionText, configFile, fileInString, sSection, sPlcLine, sUnicosLine, sDriverLine, sAlarmDp, sServerPort;
    dyn_string dsPLC;
    dyn_string dynFileLines;
    bool bAlreadySet, bReduOk;
    int iReduUnitNr;
    dyn_string dsReduIpAddress, dsSplit;
    dyn_mixed dsPLC2;
    string sPlcProtocol;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UNPLC " + dsConfig[UN_CONFIG_PLC_NAME], "_unConfigGenericFunctions_setCPC6PLCCom", dsConfig);
    int ADDR_MODE_INPUT = pollingFE ? DPATTR_ADDR_MODE_INPUT_POLL : DPATTR_ADDR_MODE_INPUT_SPONT;
    string POLL_GROUP = pollingFE ? UN_FE_POLL_DP_PREFIX + dsConfig[UN_CONFIG_PLC_NAME] : "";
    if (!dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16] && sPlcProtocol == UN_CONFIG_QUANTUM) {
        iPVSS_Modbus_Uint = PVSS_MODBUS_INT32;
    } else {
        iPVSS_Modbus_Uint = PVSS_MODBUS_UINT16;
    }

    // 1. Separate config file info and additional parameters and extended parameters and set _UnPlc config
    dsPLC = dsConfig;

    // 1.1 Check if the optional redundancy information is also present
    bReduOk = _unConfigGenericFunctions_splitRedundancyConfig(dsConfig, 6, dsPLC, dsPLC2, exceptionInfo);

    // 1.2 get the PLC protocol (S7 or MODBUS or legacy ones - PREMIUM QUANTUM)
    sPlcProtocol = unConfigGenericFunctions_extractFeProtocol(dsConfig[UN_CONFIG_PLC_TYPE]);

    // 2. _Mod_Plc dpe
    sDpName = "_Mod_Plc_" + dsPLC[UN_CONFIG_PLC_NUMBER];
    sDpName = getSystemName() + sDpName;
    iRes = dpSetWait(sDpName + ".PlcNumber", dsPLC[UN_CONFIG_PLC_NUMBER],
                     sDpName + ".HostsAndPorts", makeDynString(dsPLC[UN_CONFIG_UNPLC_CPC6_IP] + UN_CONFIG_PLC_PORT),
                     sDpName + ".UnitAddress", dsPLC[UN_CONFIG_PLC_NUMBER],
                     sDpName + ".TransactionTimeout", UN_CONFIG_PLC_TIMEOUT,
                     sDpName + ".Coding", UN_CONFIG_PLC_FRAME_CODING,
                     sDpName + ".Endianity", UN_CONFIG_PLC_FRAME_ENDIANITY,
                     sDpName + ".SetInvalidBit", UN_CONFIG_PLC_INVALID_BIT,
                     sDpName + ".RedundantPlc", bReduOk,
                     sDpName + ".ReduHostsAndPorts", dsPLC2[UN_CONFIG_UNPLC_CPC6_IP],
                     sDpName + ".ReduUnitAddress", dsPLC[UN_CONFIG_PLC_NUMBER],
                     sDpName + ".Error", UN_CONFIG_PLC_ERROR,
                     sDpName + ".SentFrames", UN_CONFIG_PLC_SENTFRAMES,
                     sDpName + ".RcvFrames", UN_CONFIG_PLC_RCVFRAMES,
                     sDpName + ".RejFrames", UN_CONFIG_PLC_REJFRAMES,
                     sDpName + ".DrvNumber", dsPLC[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]);

    if (iRes < 0) {
        fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom: " + getCatStr("unPVSS", "DPSETFAILED"), "");
    }


    // 3. Set PLC configuration in _UnPlc -> send IP address and counter address
    sDpName = c_unSystemIntegrity_UnPlc + dsConfig[UN_CONFIG_PLC_NAME];
    sAlarmDp = sDpName + ".alarm";
    sDpName = getSystemName() + sDpName;
    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_OUTPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], false, dsConfig[UN_CONFIG_UNPLC_CPC6_ADDRESS_SEND_IP],
            dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]), addressReference);

    if (!pollingFE && addressReference != "") {
        fwPeriphAddress_set(sDpName + ".communication.send_IP",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          DPATTR_ADDR_MODE_OUTPUT,
                                          PVSS_MODBUS_INT32,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(ADDR_MODE_INPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], (sPlcProtocol == UN_CONFIG_QUANTUM),
            dsConfig[UN_CONFIG_UNPLC_CPC6_ADDRESS_COUNTER], dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]),
            addressReference);

    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".communication.counter",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          ADDR_MODE_INPUT,
                                          iPVSS_Modbus_Uint,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          POLL_GROUP
                                         ),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_OUTPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], false, dsConfig[UN_CONFIG_UNPLC_CPC6_ADDRESS_CMD],
            dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]), addressReference);

    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".communication.commandInterface",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          DPATTR_ADDR_MODE_OUTPUT,
                                          PVSS_MODBUS_UINT16,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""
                                         ),
                            exceptionInfo);
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // 4. Create _UnSystemAlarm datapoints
    unSystemIntegrity_createSystemAlarm(sDpName,
                                        PLC_DS_pattern,
                                        getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsConfig[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                        exceptionInfo);
    fwArchive_set(c_unSystemAlarm_dpPattern + PLC_DS_pattern + sAlarmDp,
                  dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  0,
                  0,
                  exceptionInfo);

    unSystemIntegrity_createSystemAlarm(sDpName,
                                        PLC_DS_Time_pattern,
                                        getCatStr("unSystemIntegrity", "PLC_DS_TIME_DESCRIPTION") + dsConfig[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                        exceptionInfo);
    fwArchive_set(c_unSystemAlarm_dpPattern + PLC_DS_Time_pattern + sAlarmDp,
                  dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  0,
                  0,
                  exceptionInfo);

    unSystemIntegrity_createSystemAlarm(sDpName,
                                        PLC_DS_Error_pattern,
                                        getCatStr("unSystemIntegrity", "PLC_DS_ERROR_DESCRIPTION") + dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM] + " -> " + dsPLC[UN_CONFIG_PLC_NAME],
                                        exceptionInfo);
    fwArchive_set(c_unSystemAlarm_dpPattern + PLC_DS_Error_pattern + sAlarmDp,
                  dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                  DPATTR_ARCH_PROC_SIMPLESM,
                  DPATTR_COMPARE_OLD_NEW,
                  0,
                  0,
                  exceptionInfo);

    // PLC baseline version (float)
    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(ADDR_MODE_INPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], (sPlcProtocol == UN_CONFIG_QUANTUM),
            dsConfig[UN_CONFIG_UNPLC_CPC6_PLC_BASELINE_ADDRESS], dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]),
            addressReference);

    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".version.PLCBaseline",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          ADDR_MODE_INPUT,
                                          PVSS_MODBUS_FLOAT,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          POLL_GROUP
                                         ),
                            exceptionInfo);
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // PLC application version (float)
    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(ADDR_MODE_INPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], (sPlcProtocol == UN_CONFIG_QUANTUM),
            dsConfig[UN_CONFIG_UNPLC_CPC6_PLC_APPLICATION_ADDRESS], dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]),
            addressReference);

    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".version.PLCApplication",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          ADDR_MODE_INPUT,
                                          PVSS_MODBUS_FLOAT,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          POLL_GROUP
                                         ),
                            exceptionInfo);
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // SPECS, RESPACK version
    iRes = dpSetWait(sDpName + ".version.resourcePackage", dsConfig[UN_CONFIG_UNPLC_CPC6_RESPACK_VERSION]);
    if (iRes < 0) {
        fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom: " + getCatStr("unPVSS", "DPSETFAILED"), "");
    }

    // RESPACK MAJOR ADDRESS
    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(ADDR_MODE_INPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], (sPlcProtocol == UN_CONFIG_QUANTUM),
            dsConfig[UN_CONFIG_UNPLC_CPC6_RESPACK_MAJOR_VERSION_ADDRESS], dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]),
            addressReference);

    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".version.PLCresourcePackageMajor",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          ADDR_MODE_INPUT,
                                          iPVSS_Modbus_Uint,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          POLL_GROUP
                                         ),
                            exceptionInfo);
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // RESPACK MINOR ADDRESS
    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(ADDR_MODE_INPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], (sPlcProtocol == UN_CONFIG_QUANTUM),
            dsConfig[UN_CONFIG_UNPLC_CPC6_RESPACK_MINOR_VERSION_ADDRESS], dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]),
            addressReference);

    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".version.PLCresourcePackageMinor",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          ADDR_MODE_INPUT,
                                          iPVSS_Modbus_Uint,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          POLL_GROUP
                                         ),
                            exceptionInfo);
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // RESPACK SMALL ADDRESS
    unConfigGenericFunctions_getUnicosAddressReference(makeDynString(ADDR_MODE_INPUT, sPlcProtocol,
            dsConfig[UN_CONFIG_PLC_NUMBER], (sPlcProtocol == UN_CONFIG_QUANTUM),
            dsConfig[UN_CONFIG_UNPLC_CPC6_RESPACK_SMALL_VERSION_ADDRESS], dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_EVENT16]),
            addressReference);

    if (addressReference != "") {
        fwPeriphAddress_set(sDpName + ".version.PLCresourcePackageSmall",
                            makeDynString("MODBUS",
                                          dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                          addressReference,
                                          ADDR_MODE_INPUT,
                                          iPVSS_Modbus_Uint,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          false,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          POLL_GROUP
                                         ),
                            exceptionInfo);
    } else {
        if (!pollingFE) fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // 5. Config line
    configFile = getPath(CONFIG_REL_PATH, "config");
    sSection = "[mod_" + dsConfig[UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM] + "]";
// IS-443    sPlcLine = "plc = \"_Mod_Plc_" + dsConfig[UN_CONFIG_PLC_NUMBER] + "\"";
    sUnicosLine = "addUnicosMarker = 65534";
    sDriverLine = "maxOutputQueueSize = 600000";
    sServerPort = "tcpServerPort=502";

    if (access(configFile, F_OK) == 0) {
        if (fileToString(configFile, fileInString)) {
            dynFileLines = strsplit(fileInString, "\n");
            length = dynlen(dynFileLines);
            i = 1;
            iPosInFile = 0;
            while ((i <= length) && (iPosInFile == 0)) {
                if (strpos(dynFileLines[i], sSection) >= 0) {
                    iPosInFile = i;
                }
                i++;
            }
            if (iPosInFile > 0) {      // Section already exists
                i = iPosInFile + 1;
                iPosNextSection = length;    // If next section is not found, next section index = end of file
                while ((i <= length) && (iPosNextSection == length)) {
                    if (strpos(dynFileLines[i], "[") >= 0) {
                        iPosNextSection = i;  // Set index of next section
                    }
                    i++;
                }
                bAlreadySet = false;      // Add plc line if it doesn't already exist
                for (i = iPosInFile; i <= iPosNextSection; i++) {
                    if (strpos(dynFileLines[i], sPlcLine) >= 0) {
                        bAlreadySet = true;
                    }
                }
                if (!bAlreadySet) {
                    dynInsertAt(dynFileLines, sPlcLine, iPosInFile + 1);
                }
                bAlreadySet = false;      // Add unicos line if it doesn't already exist
                for (i = iPosInFile; i <= iPosNextSection; i++) {
                    if (strpos(dynFileLines[i], sUnicosLine) >= 0) {
                        bAlreadySet = true;
                    }
                }
                if (!bAlreadySet) {
                    dynInsertAt(dynFileLines, sUnicosLine, iPosInFile + 1);
                }

                bAlreadySet = false;      // Add driver line if it doesn't already exist
                for (i = iPosInFile; i <= iPosNextSection; i++) {
                    if (strpos(dynFileLines[i], sDriverLine) >= 0) {
                        bAlreadySet = true;
                    }
                }
                if (!bAlreadySet) {
                    dynInsertAt(dynFileLines, sDriverLine, iPosInFile + 1);
                }
                 bAlreadySet = false;			// Add tcpServer line if it doesn't already exist
                for (i = iPosInFile; i <= iPosNextSection; i++) {
                    if (strpos(dynFileLines[i], sServerPort) >= 0) {
                        bAlreadySet = true;
                    }
                }
                if (!bAlreadySet) {
                    dynInsertAt(dynFileLines, sServerPort, iPosInFile + 1);
                }

            } else {          // Section doesn't exist, create it before section [ui]
                i = 1;
                iPosInFile = 0;
                while ((i <= length) && (iPosInFile == 0)) {
                    if (strpos(dynFileLines[i], "[ui]") >= 0) {
                        iPosInFile = i;
                    }
                    i++;
                }
                if (iPosInFile == 0) {  // Section [ui] doesn't exist, create section at the end of file
                    dynAppend(dynFileLines, "");
                    dynAppend(dynFileLines, sSection);
                    dynAppend(dynFileLines, sUnicosLine);
                    dynAppend(dynFileLines, sDriverLine);
                    dynAppend(dynFileLines, sServerPort);
                    dynAppend(dynFileLines, sPlcLine);
                    dynAppend(dynFileLines, "");
                } else {        // Section [ui] exists
                    dynInsertAt(dynFileLines, "", iPosInFile);
                    dynInsertAt(dynFileLines, sPlcLine, iPosInFile);
                    dynInsertAt(dynFileLines, sUnicosLine, iPosInFile);
                    dynInsertAt(dynFileLines, sDriverLine, iPosInFile);
                    dynInsertAt(dynFileLines, sServerPort, iPosInFile);
                    dynInsertAt(dynFileLines, sSection, iPosInFile);
                    dynInsertAt(dynFileLines, "", iPosInFile);
                }
            }
            unGenericDpFunctions_fwInstallation_saveFile(dynFileLines, configFile, exceptionText);
            if (strlen(exceptionText) > 0) {
                fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom: " + exceptionText, "");
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom: cannot set config file" , "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "_unConfigGenericFunctions_setCPC6PLCCom: cannot set config file", "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setPLCCommunication
/**
Purpose: set PLC configuration for communication

Parameters:
		dsConfig: dyn_string, input, PLC configuration line in file + additional parameters
			dsConfig must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
			(Plc extended config line)
			8. IP Plc
			9. address for send IP
			10. address for counter
			11. address for command interface
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. First line of a generation file must contain the PLC description. Fields are defined in constants at this library beginning
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_setPLCCommunication(dyn_string dsConfig, dyn_string &exceptionInfo) {
    dyn_string dsPLCData = dsConfig;
    int iPLCNumber, iRes, i, sIPAddress, position, iPosInFile, iPosNextSection, length, iPVSS_Modbus_Uint;
    string sDpName, addressReference, exceptionText, configFile, fileInString, sSection, sPlcLine, sUnicosLine, sDriverLine, sAlarmDp, sServerPort;
    dyn_string dsPLC, dsAdditionalParameters, exceptionInfoTemp;
    dyn_string dsPLCExtended, dynFileLines;
    bool bAlreadySet, bOk, bVersion, bReduOk;
    string sVersion;
    int iReduUnitNr;
    dyn_string dsReduIpAddress;
    dyn_mixed dsPLC2;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UNPLC " + dsPLCData[UN_CONFIG_PLC_NAME], "unConfigGenericFunctions_setPLCCommunication", dsPLCData);
    // Check
    if (dynlen(dsPLCData) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_PLC_EXTENDED_LENGTH + UN_CONFIG_FE_VERSION_ADDITIONAL_LENGTH)) {
        bOk = true;
        bVersion = true;
        sVersion = dsPLCData[UN_CONFIG_FE_VERSION];
// remove the version.
        dynRemove(dsPLCData, UN_CONFIG_FE_VERSION);
    } else if (dynlen(dsPLCData) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_PLC_EXTENDED_LENGTH)) {
        bOk = true;
    }
    if (bOk) {
        // 1. Separate config file info and additional parameters and extended parameters and set _UnPlc config
        dsPLC = dsPLCData;

        for (i = 1; i <= UN_CONFIG_PLC_EXTENDED_LENGTH; i++) {
            dynAppend(dsPLCExtended, dsPLC[UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + 1]);
            dynRemove(dsPLC, UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + 1);
        }
        if (bVersion) {
            dynInsertAt(dsPLC, sVersion, UN_CONFIG_FE_VERSION);
        }
        unConfigGenericFunctions_setPLCApplication(dsPLC, exceptionInfo);
        if (bVersion) {
            dynRemove(dsPLC, UN_CONFIG_FE_VERSION);
        }
        for (i = 1; i <= (UN_CONFIG_PLC_ADDITIONAL_LENGTH); i++) {
            dynAppend(dsAdditionalParameters, dsPLC[UN_CONFIG_PLC_LENGTH + 1]);
            dynRemove(dsPLC, UN_CONFIG_PLC_LENGTH + 1);
        }

        // 1.1 Check if the optional redundancy information is also present
        bReduOk = _unConfigGenericFunctions_splitRedundancyConfig(dsPLCExtended, 5, dsPLCExtended, dsPLC2, exceptionInfo);

        // 2. Create associated _Mod_Plc dpe if it doesn't exist and set configs
        sDpName = "_Mod_Plc_" + dsPLC[UN_CONFIG_PLC_NUMBER];
        unConfigGenericFunctions_createDp(sDpName, "_Mod_Plc", exceptionInfoTemp);
        if (dynlen(exceptionInfoTemp) <= 0) {
            sDpName = getSystemName() + sDpName;
            iRes = dpSetWait(sDpName + ".PlcNumber", dsPLC[UN_CONFIG_PLC_NUMBER],
                             sDpName + ".HostsAndPorts", makeDynString(dsPLCExtended[UN_CONFIG_PLC_EXTENDED_IP] + UN_CONFIG_PLC_PORT),
                             sDpName + ".UnitAddress", dsPLC[UN_CONFIG_PLC_NUMBER],
                             sDpName + ".TransactionTimeout", UN_CONFIG_PLC_TIMEOUT,
                             sDpName + ".Coding", UN_CONFIG_PLC_FRAME_CODING,
                             sDpName + ".Endianity", UN_CONFIG_PLC_FRAME_ENDIANITY,
                             sDpName + ".SetInvalidBit", UN_CONFIG_PLC_INVALID_BIT,
                             sDpName + ".RedundantPlc", bReduOk,
                             sDpName + ".ReduHostsAndPorts", dsPLC2[UN_CONFIG_PLC_EXTENDED_IP],
                             sDpName + ".ReduUnitAddress", dsPLC[UN_CONFIG_PLC_NUMBER],
                             sDpName + ".ConnState", UN_CONFIG_PLC_CONNSTATE,
                             sDpName + ".Error", UN_CONFIG_PLC_ERROR,
                             sDpName + ".SentFrames", UN_CONFIG_PLC_SENTFRAMES,
                             sDpName + ".RcvFrames", UN_CONFIG_PLC_RCVFRAMES,
                             sDpName + ".RejFrames", UN_CONFIG_PLC_REJFRAMES,
                             sDpName + ".DrvNumber", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]);

            if (iRes < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication: " + getCatStr("unPVSS", "DPSETFAILED"), "");
            }

        } else {
            dynAppend(exceptionInfo, exceptionInfoTemp);
            dynClear(exceptionInfoTemp);
        }

        // 3. Set PLC configuration in _UnPlc -> send IP address and counter address
        sDpName = c_unSystemIntegrity_UnPlc + dsPLC[UN_CONFIG_PLC_NAME];
        sAlarmDp = sDpName + ".alarm";
        unConfigGenericFunctions_createDp(sDpName, UN_PLC_DPTYPE, exceptionInfoTemp);
        if (dynlen(exceptionInfoTemp) <= 0) {
            sDpName = getSystemName() + sDpName;
            unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_OUTPUT, dsPLC[UN_CONFIG_PLC_TYPE],
                    dsPLC[UN_CONFIG_PLC_NUMBER], false, dsPLCExtended[UN_CONFIG_PLC_EXTENDED_ADDRESS_SEND_IP], dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_EVENT16]), addressReference);

            if (addressReference != "") {

                fwPeriphAddress_set(sDpName + ".communication.send_IP",
                                    makeDynString("MODBUS",
                                                  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                                  addressReference,
                                                  DPATTR_ADDR_MODE_OUTPUT,
                                                  PVSS_MODBUS_INT32,
                                                  UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                                  "",
                                                  "",
                                                  "",
                                                  "",
                                                  false,
                                                  0,
                                                  UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                                  UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                                  ""),
                                    exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
            }
            unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT, dsPLC[UN_CONFIG_PLC_TYPE],
                    dsPLC[UN_CONFIG_PLC_NUMBER], (dsPLC[UN_CONFIG_PLC_TYPE] == UN_CONFIG_QUANTUM), dsPLCExtended[UN_CONFIG_PLC_EXTENDED_ADDRESS_COUNTER], dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_EVENT16]), addressReference);

            if (addressReference != "") {
                if (!dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_EVENT16] && dsPLC[UN_CONFIG_PLC_TYPE] == UN_CONFIG_QUANTUM) {
                    iPVSS_Modbus_Uint = PVSS_MODBUS_INT32;
                } else {
                    iPVSS_Modbus_Uint = PVSS_MODBUS_UINT16;
                }

                fwPeriphAddress_set(sDpName + ".communication.counter",
                                    makeDynString("MODBUS",
                                                  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                                  addressReference,
                                                  DPATTR_ADDR_MODE_INPUT_SPONT,
                                                  iPVSS_Modbus_Uint,
                                                  UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                                  "",
                                                  "",
                                                  "",
                                                  "",
                                                  false,
                                                  0,
                                                  UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                                  UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                                  ""
                                                 ),
                                    exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
            }

            unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_OUTPUT, dsPLC[UN_CONFIG_PLC_TYPE],
                    dsPLC[UN_CONFIG_PLC_NUMBER], false, dsPLCExtended[UN_CONFIG_PLC_EXTENDED_ADDRESS_CMD], dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_EVENT16 ]), addressReference);

            if (addressReference != "") {

                fwPeriphAddress_set(sDpName + ".communication.commandInterface",
                                    makeDynString("MODBUS",
                                                  dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                                  addressReference,
                                                  DPATTR_ADDR_MODE_OUTPUT,
                                                  PVSS_MODBUS_UINT16,
                                                  UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                                  "",
                                                  "",
                                                  "",
                                                  "",
                                                  false,
                                                  0,
                                                  UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                                  UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                                  ""
                                                 ),
                                    exceptionInfo);
            } else {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication:" + getCatStr("unGeneration", "ADDRESSFAILED"), "");
            }
        } else {
            dynAppend(exceptionInfo, exceptionInfoTemp);
            dynClear(exceptionInfoTemp);
        }

        // 4. Create _UnSystemAlarm datapoints
        unSystemIntegrity_createSystemAlarm(sDpName,
                                            PLC_DS_pattern,
                                            getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                            exceptionInfo);
        fwArchive_set(c_unSystemAlarm_dpPattern + PLC_DS_pattern + sAlarmDp,
                      dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exceptionInfo);

        unSystemIntegrity_createSystemAlarm(sDpName,
                                            PLC_DS_Time_pattern,
                                            getCatStr("unSystemIntegrity", "PLC_DS_TIME_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                            exceptionInfo);
        fwArchive_set(c_unSystemAlarm_dpPattern + PLC_DS_Time_pattern + sAlarmDp,
                      dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exceptionInfo);

        unSystemIntegrity_createSystemAlarm(sDpName,
                                            PLC_DS_Error_pattern,
                                            getCatStr("unSystemIntegrity", "PLC_DS_ERROR_DESCRIPTION") + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM] + " -> " + dsPLC[UN_CONFIG_PLC_NAME],
                                            exceptionInfo);
        fwArchive_set(c_unSystemAlarm_dpPattern + PLC_DS_Error_pattern + sAlarmDp,
                      dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL],
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exceptionInfo);

        // 5. Config line
        configFile = getPath(CONFIG_REL_PATH, "config");
        sSection = "[mod_" + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM] + "]";
// IS-443		sPlcLine = "plc = \"_Mod_Plc_" + dsPLC[UN_CONFIG_PLC_NUMBER] + "\"";
        sUnicosLine = "addUnicosMarker = 65534";
        sDriverLine = "maxOutputQueueSize = 600000";
		sServerPort = "tcpServerPort=502";
        if (access(configFile, F_OK) == 0) {
            if (fileToString(configFile, fileInString)) {
                dynFileLines = strsplit(fileInString, "\n");
                length = dynlen(dynFileLines);
                i = 1;
                iPosInFile = 0;
                while ((i <= length) && (iPosInFile == 0)) {
                    if (strpos(dynFileLines[i], sSection) >= 0) {
                        iPosInFile = i;
                    }
                    i++;
                }
                if (iPosInFile > 0) {			// Section already exists
                    i = iPosInFile + 1;
                    iPosNextSection = length;		// If next section is not found, next section index = end of file
                    while ((i <= length) && (iPosNextSection == length)) {
                        if (strpos(dynFileLines[i], "[") >= 0) {
                            iPosNextSection = i;	// Set index of next section
                        }
                        i++;
                    }
                    bAlreadySet = false;			// Add plc line if it doesn't already exist
                    for (i = iPosInFile; i <= iPosNextSection; i++) {
                        if (strpos(dynFileLines[i], sPlcLine) >= 0) {
                            bAlreadySet = true;
                        }
                    }
                    if (!bAlreadySet) {
                        dynInsertAt(dynFileLines, sPlcLine, iPosInFile + 1);
                    }
                    bAlreadySet = false;			// Add unicos line if it doesn't already exist
                    for (i = iPosInFile; i <= iPosNextSection; i++) {
                        if (strpos(dynFileLines[i], sUnicosLine) >= 0) {
                            bAlreadySet = true;
                        }
                    }
                    if (!bAlreadySet) {
                        dynInsertAt(dynFileLines, sUnicosLine, iPosInFile + 1);
                    }

                    bAlreadySet = false;			// Add driver line if it doesn't already exist
                    for (i = iPosInFile; i <= iPosNextSection; i++) {
                        if (strpos(dynFileLines[i], sDriverLine) >= 0) {
                            bAlreadySet = true;
                        }
                    }
                    if (!bAlreadySet) {
                        dynInsertAt(dynFileLines, sDriverLine, iPosInFile + 1);
                    }
                    bAlreadySet = false;			// Add tcpServer line if it doesn't already exist
                    for (i = iPosInFile; i <= iPosNextSection; i++) {
                        if (strpos(dynFileLines[i], sServerPort) >= 0) {
                            bAlreadySet = true;
                        }
                    }
                    if (!bAlreadySet) {
                        dynInsertAt(dynFileLines, sServerPort, iPosInFile + 1);
                    }

                } else {					// Section doesn't exist, create it before section [ui]
                    i = 1;
                    iPosInFile = 0;
                    while ((i <= length) && (iPosInFile == 0)) {
                        if (strpos(dynFileLines[i], "[ui]") >= 0) {
                            iPosInFile = i;
                        }
                        i++;
                    }
                    if (iPosInFile == 0) {	// Section [ui] doesn't exist, create section at the end of file
                        dynAppend(dynFileLines, "");
                        dynAppend(dynFileLines, sSection);
                        dynAppend(dynFileLines, sUnicosLine);
                        dynAppend(dynFileLines, sDriverLine);
                        dynAppend(dynFileLines, sServerPort);
                        dynAppend(dynFileLines, sPlcLine);
                        dynAppend(dynFileLines, "");
                    } else {			// Section [ui] exists
                        dynInsertAt(dynFileLines, "", iPosInFile);
                        dynInsertAt(dynFileLines, sPlcLine, iPosInFile);
                        dynInsertAt(dynFileLines, sUnicosLine, iPosInFile);
                        dynInsertAt(dynFileLines, sDriverLine, iPosInFile);
			dynInsertAt(dynFileLines, sServerPort, iPosInFile);
                        dynInsertAt(dynFileLines, sSection, iPosInFile);
                        dynInsertAt(dynFileLines, "", iPosInFile);
                    }
                }
                unGenericDpFunctions_fwInstallation_saveFile(dynFileLines, configFile, exceptionText);
                if (strlen(exceptionText) > 0) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication: " + exceptionText, "");
                }
            } else {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication: cannot set config file" , "");
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication: cannot set config file", "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPLCCommunication: " + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkPLCApplication
/**
Purpose: check PLC data for a sub application (not comm)

Parameters:
		dsConfig: dyn_string, input, PLC configuration line in file + additional parameters
			dsConfig must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
		dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkPLCApplication(dyn_string dsConfig, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    dyn_string dsPLCLine, dsPLCAdditionalData, dsPLC = dsConfig, dsPLC2;
    int i, iRes, iDriverNum;
    string sPLCDp, sModPLC, sPLCType, sHostname, sAliasDp, tempDp;
    bool bOk;
    string sUserTag;
    float fFloat;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UNPLC " + dsPLC[UN_CONFIG_PLC_NAME], "unConfigGenericFunctions_checkPLCApplication", dsPLC);
//DebugN("start", dsPLC);
    if (dynlen(dsPLC) == (UN_CONFIG_UNPLC_LENGTH_CPC6 + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        bOk = true;
        unConfigGenericFunctions_checkAddresses(makeDynString(
                dsPLC[UN_CONFIG_UNPLC_CPC6_ADDRESS_SEND_IP],
                dsPLC[UN_CONFIG_UNPLC_CPC6_ADDRESS_COUNTER],
                dsPLC[UN_CONFIG_UNPLC_CPC6_ADDRESS_CMD],
                dsPLC[UN_CONFIG_UNPLC_CPC6_PLC_BASELINE_ADDRESS],
                dsPLC[UN_CONFIG_UNPLC_CPC6_RESPACK_MAJOR_VERSION_ADDRESS],
                dsPLC[UN_CONFIG_UNPLC_CPC6_RESPACK_MINOR_VERSION_ADDRESS],
                dsPLC[UN_CONFIG_UNPLC_CPC6_RESPACK_SMALL_VERSION_ADDRESS]),
                                                dsPLC[UN_CONFIG_UNPLC_CPC6_PLC_APPLICATION_ADDRESS],
                                                exceptionInfo);
        unConfigGenericFunctions_checkIP(dsPLC[UN_CONFIG_UNPLC_CPC6_IP], exceptionInfo);

        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_RESPACK_VERSION);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_RESPACK_SMALL_VERSION_ADDRESS);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_RESPACK_MINOR_VERSION_ADDRESS);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_RESPACK_MAJOR_VERSION_ADDRESS);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_PLC_APPLICATION_ADDRESS);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_PLC_BASELINE_ADDRESS);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_ADDRESS_CMD);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_ADDRESS_COUNTER);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_ADDRESS_SEND_IP);
        dynRemove(dsPLC, UN_CONFIG_UNPLC_CPC6_IP);
        sUserTag = "CPC6";
    } else if (dynlen(dsPLC) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_FE_VERSION_ADDITIONAL_LENGTH)) {
        bOk = true;
        sUserTag = dsPLC[UN_CONFIG_FE_VERSION];
// remove the version.
        dynRemove(dsPLC, UN_CONFIG_FE_VERSION);
    } else if (dynlen(dsPLC) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        bOk = true;
    }

    if (bOk) {
        unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UNPLC " + dsPLC[UN_CONFIG_PLC_NAME], "unConfigGenericFunctions_checkPLCApplication, common configuration", dsPLC);
//DebugN("ok for check", dsPLC);
        dsPLCLine = dsPLC;
        for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
            dynAppend(dsPLCAdditionalData, dsPLCLine[UN_CONFIG_PLC_LENGTH + 1]);
            dynRemove(dsPLCLine, UN_CONFIG_PLC_LENGTH + 1);
        }
        unConfigGenericFunctions_checkPLCData(dsPLCLine, exceptionInfo);
        unConfigGenericFunctions_checkPLCAdditionalData(dsPLCAdditionalData, exceptionInfo);

// first check if the PLC name is correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_NAME];
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADPLCNAME"), "");
            return;
        }

// keep the version
        g_mImportFrontEndVersion[tempDp] = sUserTag;

// check if the PLC Application Name is correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION];
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: " + dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
            return;
        }

// sPLCDp is _unPlc_plcName
        sPLCDp = getSystemName() +  c_unSystemIntegrity_UnPlc + dsPLCLine[UN_CONFIG_PLC_NAME];
//		sAliasDp = dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
        sAliasDp = unGenericDpFunctions_dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);
//DebugN(sAliasDp, dsPLCLine[UN_CONFIG_PLC_NAME]);

        if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".") {
            sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);
        }
        if ((sAliasDp != "") && (sAliasDp != sPLCDp)) {
            if (dynContains(dsDeleteDps, sAliasDp) <= 0) {	// Alias exists, is not used by the "_UnPlc" config datapoint and will not be deleted
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADHOSTNAMEALIAS") + sAliasDp, "");
            }
        }
        if (dpExists(sPLCDp) && (dynContains(dsDeleteDps, sPLCDp) <= 0)) {
            sHostname = unGenericDpFunctions_getAlias(sPLCDp);
            iRes = dpGet(sPLCDp + ".configuration.mod_PLC", sModPLC,
                         sPLCDp + ".configuration.type", sPLCType,
                         sPLCDp + ".communication.driver_num", iDriverNum);
            if (iRes >= 0) {
                if (sModPLC != ("_Mod_Plc_" + dsPLCLine[UN_CONFIG_PLC_NUMBER])) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: PLC already exists but its _ModPlc datapoint is different.", "");
                }
                if (sPLCType != unConfigGenericFunctions_extractFeModel(dsPLCLine[UN_CONFIG_PLC_TYPE])) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: Front-End or PLC already exists but its type is different: existing: " + sPLCType + " new: " + unConfigGenericFunctions_extractFeModel(dsPLCLine[UN_CONFIG_PLC_TYPE]), "");
                }
                if (iDriverNum != dsPLCAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: Front-End or PLC already exists but its driver number is different: existing: " + iDriverNum + " new: " + dsPLCAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], "");
                }
                if (sHostname != dsPLCLine[UN_CONFIG_PLC_NAME]) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: Front-End or PLC already exists but its Front-End or PLC Name is different. existing: " + sHostname + " new : " + dsPLCLine[UN_CONFIG_PLC_NAME], "");
                }
            }
        }
        if (unConfigGenericFunctions_checkMODBUSUnitNumber(sPLCDp, "_Mod_Plc_" + dsPLCLine[UN_CONFIG_PLC_NUMBER])) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: MODBUS Unit number already used by another MODBUS PLC", "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCApplication: " + getCatStr("unGeneration", "BADINPUTS"), "");
    }
//DebugN("unConfigGenericFunctions_checkPLCApplication", g_mImportFrontEndVersion);
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigGenericFunctions_checkMODBUSUnitNumber
/** check if the MODBUS UNIT number (_ModPlc DP) is already assigned to another PLC

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sPLCDp input, the PLC DP
@param sModbusPlc input, the _ModPlc DP (equivalent to the MODBUS UNIT Number in UNICOS)
@return true if the MODBUS UNIT number (_ModPlc DP) is already assigned to another PLC, false if not
*/
bool unConfigGenericFunctions_checkMODBUSUnitNumber(string sPLCDp, string sModbusPlc) {
    bool bResult;
    dyn_string dsPLC, dsModbusPlc;
    int iPos;

    dsPLC = dpNames("*.configuration.mod_PLC", "_UnPlc");
    iPos = dynContains(dsPLC, sPLCDp + ".configuration.mod_PLC");
    if (iPos > 0) {
        dynRemove(dsPLC, iPos);
    }
    if (dynlen(dsPLC) > 0) {
        dpGet(dsPLC, dsModbusPlc);
    }
    if (dynContains(dsModbusPlc, sModbusPlc) > 0) {
        bResult = true;
    }

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "_UnPlc " + sPLCDp, "unConfigGenericFunctions_checkMODBUSUnitNumber", sPLCDp, sModbusPlc, dsPLC, dsModbusPlc, bResult, "-");
    return bResult;
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkPLCCommunication
/**
Purpose: check PLC data for communication

Parameters:
		dsConfig: dyn_string, input, PLC configuration line in file + additional parameters
			dsConfig must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication MUST BE "COMMUNICATION"
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
			(Plc extended config line)
			8. IP Plc
			9. address for send IP
			10. address for counter
			11. address for command interface
		dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkPLCCommunication(dyn_string dsConfig, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    dyn_string dsPLC = dsConfig;
    dyn_string dsPLCLine, dsPLCExtended;
    int i;
    bool bOk, bVersion;
    string sVersion;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UNPLC " + dsPLC[UN_CONFIG_PLC_NAME], "unConfigGenericFunctions_checkPLCCommunication", dsPLC);
    if (dynlen(dsPLC) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_PLC_EXTENDED_LENGTH + UN_CONFIG_FE_VERSION_ADDITIONAL_LENGTH)) {
        bOk = true;
        sVersion = dsPLC[UN_CONFIG_FE_VERSION];
// remove the version.
        dynRemove(dsPLC, UN_CONFIG_FE_VERSION);
        bVersion = true;
    } else if (dynlen(dsPLC) == (UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + UN_CONFIG_PLC_EXTENDED_LENGTH)) {
        bOk = true;
    }
    if (bOk) {
        dsPLCLine = dsPLC;
        for (i = 1; i <= UN_CONFIG_PLC_EXTENDED_LENGTH; i++) {
            dynAppend(dsPLCExtended, dsPLCLine[UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + 1]);
            dynRemove(dsPLCLine, UN_CONFIG_PLC_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH + 1);
        }
        if (bVersion) {
            dynInsertAt(dsPLCLine, sVersion, UN_CONFIG_FE_VERSION);
        }
//DebugN("aft", dsPLCExtended, dsPLCLine);
// 1. Check Plc config line + additional parameters
        unConfigGenericFunctions_checkPLCApplication(dsPLCLine, dsDeleteDps, exceptionInfo);
        if (dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION] != "COMMUNICATION") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCCommunication: " + getCatStr("unGeneration", "SUBAPPLIISNOTCOMM"), "");
        }
// 2. Check Plc extended parameters
        unConfigGenericFunctions_checkIP(dsPLCExtended[UN_CONFIG_PLC_EXTENDED_IP], exceptionInfo);
        unConfigGenericFunctions_checkAddresses(makeDynString(dsPLCExtended[UN_CONFIG_PLC_EXTENDED_ADDRESS_SEND_IP],
                                                dsPLCExtended[UN_CONFIG_PLC_EXTENDED_ADDRESS_COUNTER],
                                                dsPLCExtended[UN_CONFIG_PLC_EXTENDED_ADDRESS_CMD]),
                                                exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCCommunication: " + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkPLCData
/**
Purpose: check PLC data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, name
			3. string, subApplication
			4. int, PLC address = PLC number
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkPLCData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCNumber;

    if (dynlen(dsPLC) != UN_CONFIG_PLC_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        unConfigGenericFunctions_checkPLCType(dsPLC[UN_CONFIG_PLC_TYPE], exceptionInfo);
        if (strrtrim(dsPLC[UN_CONFIG_PLC_NAME]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCData:" + getCatStr("unGeneration", "ERRORPLCNAME"), "");
        }
        if (strrtrim(dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCData:" + getCatStr("unGeneration", "ERRORSUBAPPLI"), "");
        }
        unConfigGenericFunctions_checkUnsigned(dsPLC[UN_CONFIG_PLC_NUMBER], uPLCNumber, exceptionInfo);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkPLCAdditionalData
/**
Purpose: check PLC additional data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC additional data
			dsPLC must contains :
			1. unsigned, driver number
			2. string, boolean archive
			3. string, analog archive
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkPLCAdditionalData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCDriver;

    if (dynlen(dsPLC) != UN_CONFIG_PLC_ADDITIONAL_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCAdditionalData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        unConfigGenericFunctions_checkUnsigned(dsPLC[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], uPLCDriver, exceptionInfo);
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCAdditionalData: bool archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCAdditionalData: analog archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCAdditionalData: event archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkPLCType
/**
Purpose: check PLC type

Parameters:
		sPLCType: string, input, PLC type
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkPLCType(string sPLCType, dyn_string &exceptionInfo) {
    string sFontEnd;
    bool bDollar = true;
    sPLCType = unConfigGenericFunctions_extractFeProtocol(sPLCType);
// DebugN("FUNCTION UNICOS: unConfigGenericFunctions_checkPLCType(sPLCType= "+sPLCType);

    if (isFunctionDefined("shapeExists")) {
        if (shapeExists("frontEndType")) {
            sFontEnd = unConfigGenericFunctions_convertPlcTypeToLegacyType(frontEndType.text);
            bDollar = false;
        }
    }
    if (bDollar) {
        sFontEnd = unConfigGenericFunctions_convertPlcTypeToLegacyType($sFrontEndDeviceType);
    }

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkPLCType", bDollar, sFontEnd, "-");
    switch (sFontEnd) {
        // case SIEMENS S7
        case S7_PLC_DPTYPE:
            if ((sPLCType != UN_CONFIG_S7_400) && (sPLCType != UN_CONFIG_S7_300) && //back compatibility
                    (sPLCType != UN_CONFIG_PROTOCOL_S7)) { //now this is the only type for S7
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCType (PLC type SIEMENS):" + getCatStr("unGeneration", "BADINPUTS"), "");
            }
            break;
        // case SCHNEIDER
        case UN_PLC_DPTYPE:
            if ((sPLCType != UN_CONFIG_QUANTUM) && (sPLCType != UN_CONFIG_PREMIUM) && (sPLCType != UN_CONFIG_UNITY) && //back-compatibility
                    (sPLCType != UN_CONFIG_PROTOCOL_MODBUS)) { //now this is the only type for modbus PLC
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCType (PLC type SCHNEIDER):" + getCatStr("unGeneration", "BADINPUTS"), "");
            }
            break;
        default:
            if (sPLCType != sFontEnd) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPLCType:" + getCatStr("unGeneration", "BADINPUTS"), "");
            }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setParameters
/**
Purpose: set the parameters Alias, Description, Domain, Nature, Diagnostic etc. for each device

Parameters:
		sDpName, string, input, device name
		sAliasLink, string, input, device alias and device link
		sDescription, string, input, device description
		sDomainConfig, string, input, domain
		sNatureConfig, string, input, nature
		sDiagnostic, string, input, diagnostic panel
		sHtml, string, input, html help
		sDefaultPanel, string, input, default panel
		sWidget, string, input, widget

		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setParameters(string sDpName, string sAliasLink, string sDescription, string sDomainConfig, string sNatureConfig,
                                       string sDiagnostic, string sHtml, string sDefaultPanel,
                                       string sWidget, dyn_string &exceptionInfo) {
    string deviceName, sAlias, sLink;
    string sOldDomain, sOldNature;
    dyn_string dsParameters, dsTemp;
    int i, len;
    string sDomain, sNature, sAccDomain, sOper, sExpert, sAdmin;
    dyn_string ds;


    ds = strsplit(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
    if (dynlen(ds) == UN_CONFIG_UNICOS_DOMAIN_AND_ACCESS_CONTROL_LEN) { // access control + domain present
        sAccDomain = ds[1];
        sDomain = ds[2];
    } else if (strpos(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR) >= 0) { // if UN_ACCESS_CONTROL_DOMAIN_SEPARATOR in sDomain config --> domain = ""
        sAccDomain = ds[1];
        sDomain = "";
    } else {
        sDomain = sDomainConfig;
    }
    ds = strsplit(sNatureConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
    // if no ACC priv assume list of nature
    if (dynlen(ds) >= (UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN - 1)) {
        sOper = ds[1];
        sExpert = ds[2];
        sAdmin = ds[3];
        if (dynlen(ds) == UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN) {
            sNature = ds[4];
        } else { // dynlen = UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN -1 --> consider nature = ""
            sNature = "";
        }
    } else {
        sNature = sNatureConfig;
    }

    deviceName = unGenericDpFunctions_getDpName(sDpName);
    unConfigGenericFunctions_getAliasLink(sAliasLink, sAlias, sLink);
    unConfigGenericFunctions_setAlias(deviceName, sAlias, exceptionInfo);
    unConfigGenericFunctions_setDeviceLink(deviceName, sLink);

//DebugN("unConfigGenericFunctions_setParameters", sDpName, deviceName, sAlias, sDescription, sDomainConfig, sDomain, sAccDomain, sNatureConfig, sOper, sExpert, sAdmin, sNature, sDiagnostic, sHtml, sDefaultPanel, sWidget);

    unConfigGenericFunctions_setDescription(deviceName, sDescription, exceptionInfo);
    unGenericDpFunctions_getParameters(deviceName, dsParameters);	// Delete old domain / nature of current device if necessary
    sOldDomain = dsParameters[UN_PARAMETER_DOMAIN];
    sOldNature = dsParameters[UN_PARAMETER_NATURE];

//	if ((sOldDomain != "") && (sOldDomain != sDomain))
    if (sOldDomain != sDomain) {
        unConfigGenericFunctions_deleteDpInMultiGroup(deviceName, UN_DOMAIN_PREFIX, sOldDomain, exceptionInfo);
    }

//	if ((sOldNature != "") && (sOldNature != sNature))
    if (sOldNature != sNature) {
        unConfigGenericFunctions_deleteDpInMultiGroup(deviceName, UN_NATURE_PREFIX, sOldNature, exceptionInfo);
    }

    dsParameters = makeDynString(sDiagnostic, sHtml, sDefaultPanel, sDomain, sNature, "", "", sWidget);
    unGenericDpFunctions_setParameters(deviceName, dsParameters, exceptionInfo);
    dynClear(dsParameters);

//	if ((sDomain != "") && (sOldDomain != sDomain)) {
    if (sOldDomain != sDomain) {
        dsTemp = unConfigGenericFunctions_getMultiGroup(sDomain);
        len = dynlen(dsTemp);
        for (i = 1; i <= len; i++) {
            if (dsTemp[i] != "") {
                dynAppend(dsParameters, UN_DOMAIN_PREFIX + dsTemp[i]);
            }
        }
    }

//	if ((sNature != "") && (sOldNature != sNature)) {
    if (sOldNature != sNature) {
        dsTemp = unConfigGenericFunctions_getMultiGroup(sNature);
        len = dynlen(dsTemp);
        for (i = 1; i <= len; i++) {
            if (dsTemp[i] != "") {
                dynAppend(dsParameters, UN_NATURE_PREFIX + dsTemp[i]);
            }
        }
    }

//DebugN("unConfigGenericFunctions_setParameters", dsParameters);
    if (dynlen(dsParameters) > 0) {
        unConfigGenericFunctions_setGroups(deviceName, dsParameters, exceptionInfo);
    }

    unConfigGenericFunctions_setDeviceAccessControl(deviceName, sAccDomain, sOper, sExpert, sAdmin);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkDeviceAccessControl
/**
Purpose: check the list of domain access control

Parameters:
  sAccessControlDomainList, string, input, access control domain list
  exceptionInfo, dyn_string, output, for errors


Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_checkDeviceAccessControl(string sAccessControlDomainList, dyn_string &exceptionInfo) {
    if (patternMatch("*[:""'@`#$%^&*?!;=+~(){}<>|]*", sAccessControlDomainList)) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDeviceAccessControl:" + getCatStr("unGeneration", "BADDATA"), sAccessControlDomainList);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setDeviceAccessControl
/**
Purpose: set the list of access control domain and priviledge action of the device

Parameters:
  sDpName, string, input, device name
  sAccessControlDomainList, string, input, access control domain list
  sOperator, string, input, action list for operator priviledge
  sExpert, string, input, action list for expert priviledge
  sAdmin, string, input, action list for admin priviledge


Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_setDeviceAccessControl(string sDpName, string sAccessControlDomainList, string sOperator, string sExpert, string sAdmin) {
    if (dpExists(sDpName + ".statusInformation.accessControl.accessControlDomain"))
        dpSet(sDpName + ".statusInformation.accessControl.accessControlDomain", sAccessControlDomainList,
              sDpName + ".statusInformation.accessControl.operator", sOperator,
              sDpName + ".statusInformation.accessControl.expert", sExpert,
              sDpName + ".statusInformation.accessControl.admin", sAdmin);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setDeviceAccessControl
/**
Purpose: get the list of access control domain and priviledge action of the device

Parameters:
  sDpName, string, input, device name
  sAccessControlDomainList, string, input, access control domain list
  sOperator, string, input, action list for operator priviledge
  sExpert, string, input, action list for expert priviledge
  sAdmin, string, input, action list for admin priviledge


Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_getDeviceAccessControl(string sDpName, string &sAccessControlDomainList, string &sOperator, string &sExpert, string &sAdmin) {
    if (dpExists(sDpName + ".statusInformation.accessControl.accessControlDomain"))
        dpGet(sDpName + ".statusInformation.accessControl.accessControlDomain", sAccessControlDomainList,
              sDpName + ".statusInformation.accessControl.operator", sOperator,
              sDpName + ".statusInformation.accessControl.expert", sExpert,
              sDpName + ".statusInformation.accessControl.admin", sAdmin);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setStsRegAddress
/**
Purpose: set the StsReg config

Parameters:
		sDpNameStsReg, string, input, StsReg name
		driverNum, int, input, driver number
		addressReference, string, input, reference address for _address config
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setStsRegAddress(string sDpNameStsReg, int driverNum, string addressReference, dyn_string &exceptionInfo) {
    if (addressReference != "") {
        fwPeriphAddress_set(sDpNameStsReg,
                            makeDynString("MODBUS",
                                          driverNum,
                                          addressReference,
                                          DPATTR_ADDR_MODE_INPUT_SPONT,
                                          PVSS_MODBUS_UINT16,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_STSREG,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setStsRegAddress: " + sDpNameStsReg + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setStsRegAlarmBitAddress
/**
Purpose: set config for an alarm bit from StsReg

Parameters:
		sDpNameBit, string, input, dpe
		driverNum, int, input, driver number
		addressReference, string, input, reference address for _address config
		iBitNumber, int, input, bit position in StsReg

		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setStsRegAlarmBitAddress(string sDpNameBit, int driverNum, string addressReference, int iBitNumber,
        dyn_string &exceptionInfo) {
    if (addressReference != "") {
        fwPeriphAddress_set(sDpNameBit,
                            makeDynString("MODBUS",
                                          driverNum,
                                          addressReference,
                                          DPATTR_ADDR_MODE_INPUT_SPONT,
                                          PVSS_MODBUS_BOOL,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_EVSTSREG,
                                          iBitNumber,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setStsRegAlarmBitAddress: " + sDpNameBit + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setevStsRegAddress
/**
Purpose: set evStsReg address config

Parameters:
		sDpNameevStsReg, string, input, evStsReg name
		driverNum, int, input, driver number
		addressReference, string, input, reference address for _address config
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setevStsRegAddress(string sDpNameevStsReg, int driverNum, string addressReference, bool bEvent16, dyn_string &exceptionInfo) {
    int iPVSS_Modbus_Uint;

    if (addressReference != "") {
        if (bEvent16) {
            iPVSS_Modbus_Uint = PVSS_MODBUS_UINT16;
        } else {
            iPVSS_Modbus_Uint = PVSS_MODBUS_INT32;
        }

        fwPeriphAddress_set(sDpNameevStsReg,
                            makeDynString("MODBUS",
                                          driverNum,
                                          addressReference,
                                          DPATTR_ADDR_MODE_INPUT_SPONT,
                                          iPVSS_Modbus_Uint,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_EVSTSREG,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setevStsRegAddress: " + sDpNameevStsReg + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setManReg
/**
Purpose: set ManReg config

Parameters:
		sDpNameManReg, string, input, ManReg name
		driverNum, int, input, driver number
		addressReference, string, input, reference address for _address config
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setManReg(string sDpNameManReg, int driverNum, string addressReference, dyn_string &exceptionInfo) {
    if (addressReference != "") {
        fwPeriphAddress_set(sDpNameManReg,
                            makeDynString("MODBUS",
                                          driverNum,
                                          addressReference,
                                          DPATTR_ADDR_MODE_OUTPUT,
                                          PVSS_MODBUS_UINT16,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_MANREG,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setManReg: " + sDpNameManReg + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setCommon
/**
Purpose : set common config

Parameters:
		dsConfigs, dyn_string, input, full config line
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setCommon(dyn_string dsConfigs, dyn_string &exceptionInfo) {
// alias is deviceName,deviceLink
    unConfigGenericFunctions_setParameters(	dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DIAGNOSTIC],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_HTML],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL],
                                            dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_WIDGET],
                                            exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setFloatIntBlock
/**
Purpose : set a block of float or int dpe config

Parameters:
		dsConfigs, dyn_string, input, full config line
		dataType, int, input, float or int
		dsInputDps, dyn_string, input, names of available Dps (in ProcessInput)
		diInputAddresses, dyn_int, input, index of addresses in dsConfigs
		dsOutputDps, dyn_string, input, names of available Dps (in ProcessOutput)
		diOutputAddresses, dyn_int, input, index of addresses in dsConfigs
		diCommonData, dyn_int, input, index of common data (unit format deadband ...)
			4 parameters : 1-unit, 2-format 3-deadband 4-daedband type
		diRange, dyn_int, string, index of range min max, empty dyn_int = no range (pv_range config is set on first dpname of dsInputDps)
			2 parameters : 1-range min 2-range max
		dsArchivedDps, dyn_string, input, list of archived names
		diArchiveConfig, dyn_int, input, index of archive config (archive active and time filter)
			2 parameters : 1-active 2-time filter

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setFloatIntBlock(dyn_string dsConfigs, int dataType, dyn_string dsInputDps, dyn_int diInputAddresses,
        dyn_string dsOutputDps, dyn_int diOutputAddresses, dyn_int diCommonData, dyn_int diRange,
        dyn_string dsArchivedDps, dyn_int diArchiveConfig, dyn_string &exceptionInfo) {
    int i, length, iPLCNumber, driverNum, Type;
    string addressReference, sDpName, sPLCType;
    dyn_string exceptionInfoTemp;

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_setFloatIntBlock(dsConfigs= "+dsConfigs+
    																													" dataType= "+dataType+
    																													" dsInputDps= "+dsInputDps+
    																													" diInputAddresses= "+diInputAddresses+
    																													" dsOutputDps= "+dsOutputDps+
    																													" diOutputAddresses= "+diOutputAddresses+
    																													" diCommonData= "+diCommonData+
    																													" diRange= "+diRange+
    																													" dsArchivedDps= "+dsArchivedDps+
    																													" diArchiveConfig= "+diArchiveConfig+" )");
    */

    // Initialization
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    driverNum = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER];
    iPLCNumber = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER];
    sPLCType = dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE];

    // 1. Input address configs
    length = dynlen(dsInputDps);
    for (i = 1; i <= length; i++) {
        unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT, sPLCType, iPLCNumber, false,
                dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diInputAddresses[i]], dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]), addressReference);

        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".ProcessInput." + dsInputDps[i],
                                makeDynString("MODBUS",
                                              driverNum,
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_SPONT,
                                              dataType,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_INPUT,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              ""),
                                exceptionInfoTemp);
        } else {
            fwException_raise(exceptionInfoTemp, "ERROR", "unConfigGenericFunctions_setFloatIntBlock: " + sDpName + ".ProcessInput." + dsInputDps[i] + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
    }

    // 2. Output addess config
    dynAppend(exceptionInfo, exceptionInfoTemp);
    dynClear(exceptionInfoTemp);

    length = dynlen(dsOutputDps);
    for (i = 1; i <= length; i++) {
        unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_OUTPUT, sPLCType, iPLCNumber, false,
                dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diOutputAddresses[i]], dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]), addressReference);
        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".ProcessOutput." + dsOutputDps[i],
                                makeDynString("MODBUS",
                                              driverNum,
                                              addressReference,
                                              DPATTR_ADDR_MODE_OUTPUT,
                                              dataType,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_OUTPUT,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              ""),
                                exceptionInfoTemp);
        } else {
            fwException_raise(exceptionInfoTemp, "ERROR", "unConfigGenericFunctions_setFloatIntBlock: " + sDpName + ".ProcessInput." + dsOutputDps[i] + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
    }

    dynClear(exceptionInfoTemp);
    // 3. set comon config: archive, smoothing, pv_range, etc.
    unConfigGenericFunctions_setCommonFloatIntBlock(dsConfigs, dataType, dsInputDps,
            diCommonData, diRange,
            dsArchivedDps, diArchiveConfig, exceptionInfoTemp);

    dynAppend(exceptionInfo, exceptionInfoTemp);
}

//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_set5RangesAlert
/**
Purpose : set 5 ranges _alert_hdl on given dpe

Parameters:
		dsConfigs, dyn_string, input, full config line
		sDp, string, input, dpe
		description, string, input, description
		iNormalPosition, int, input, index of alarm active, exist parameter
		diLimits, dyn_int, input, index of limits in dsConfig
				4 parameters : index of 1.LL 2.L 3.H 4.HH
		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. convertIndex must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_set5RangesAlert(dyn_string dsConfigs, string sDp, string description, int iNormalPosition, dyn_int diLimits, dyn_string &exceptionInfo) {
    bool alarmExists, alarmActive, bOkRange, bSMS;
    dyn_string dsDescription;
    dyn_float dfLimits;
    dyn_string dsAlarms, dsAlertClass;
    int iAlertType, iLevel;
    string sDeviceAlias, sDeviceLinks;
    string sDomain, sDomainConfig, sNature, sNatureConfig;
    dyn_string ds;
    /*
    	DebugN("FUNCTION: unConfigGenericFunctions_set5RangesAlert(dsConfigs= "+dsConfigs+
    																														" sDp= " +sDp+
    																														" description= "+description+
    																														" iNormalPosition= "+iNormalPosition+
    																														" diLimits= "+diLimits+" )");
    */

    dynClear(dsDescription);
    sDomainConfig = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN];
    ds = strsplit(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
    if (dynlen(ds) == UN_CONFIG_UNICOS_DOMAIN_AND_ACCESS_CONTROL_LEN) { // access control + domain present
        sDomain = ds[2];
    } else if (strpos(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR) >= 0) { // if UN_ACCESS_CONTROL_DOMAIN_SEPARATOR in sDomain config --> domain = ""
        sDomain = "";
    } else {
        sDomain = sDomainConfig;
    }

    sNatureConfig = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE];
    ds = strsplit(sNatureConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
    // if no ACC priv assume list of nature
    if (dynlen(ds) >= (UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN - 1)) {
        if (dynlen(ds) == UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN) {
            sNature = ds[4];
        } else { // dynlen = UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN -1 --> consider nature = ""
            sNature = "";
        }
    } else {
        sNature = sNatureConfig;
    }

    dsDescription [UN_CONFIG_ALARM_DOMAIN] = sDomain;
    dsDescription [UN_CONFIG_ALARM_NATURE] = sNature;
// alias is the deviceName only
    unConfigGenericFunctions_getAliasLink(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS],
                                          sDeviceAlias, sDeviceLinks);
    dsDescription [UN_CONFIG_ALARM_ALIAS] = sDeviceAlias;
    dsDescription [UN_CONFIG_ALARM_DESCRIPTION] = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION];
    dsDescription [UN_CONFIG_ALARM_NAME] = description;
    dsDescription [UN_CONFIG_ALARM_DEFAULTPANEL] = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL];

    if (dynlen(diLimits) == 4) {
        unConfigGenericFunctions_getNormalPosition(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + iNormalPosition], alarmExists, alarmActive, bOkRange, iLevel, bSMS);
        if (alarmExists) {
            dfLimits = makeDynFloat(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[1]],
                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[2]],
                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[3]],
                                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diLimits[4]]);


            dpGet(sDp + ":_alert_hdl.._type", iAlertType);
            if (iAlertType != DPCONFIG_NONE) {
                fwAlertConfig_deleteAnalog(sDp, exceptionInfo);
            }

            unConfigGenericFunctions_set4LevelsAlert(iLevel, bSMS, dsAlarms, dsAlertClass, dfLimits);
            if (bSMS) {
                unProcessAlarm_addDpToList(sDp, UN_PROCESSALARM_CATEGORY, exceptionInfo);
            } else {
                unProcessAlarm_removeDpFromList(sDp, exceptionInfo);
            }


            unConfigGenericFunctions_setAnalogAlert(	sDp, dsDescription, dfLimits,
                    dsAlarms, dsAlertClass,
                    alarmActive, exceptionInfo);
        } else {
            fwAlertConfig_deleteAnalog(sDp, exceptionInfo);
// even if the alarm is deleted set the description
            unConfigGenericFunctions_setAlarmDescription(sDp, dsDescription, exceptionInfo);	// Set description for alert screen
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_check5RangesAlert
/**
Purpose : check 5 ranges _alert_hdl data

Parameters:
		dsConfigs, dyn_string, input, full config line
		iNormalPosition, int, input, index of normal position parameter
		diLimits, dyn_int, input, index of limits in dsConfig
				4 parameters : index of 1.LL 2.L 3.H 4.HH
		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. convertIndex must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_check5RangesAlert(dyn_string dsConfigs, int iNormalPosition, dyn_int diLimits, dyn_string &exceptionInfo) {
    float fLL, fL, fH, fHH;
    int iLevel;
    dyn_string exceptionInfoTemp;
    bool alarmExists, alarmActive, bOkRange, bError, bSMS;

    unConfigGenericFunctions_checkNormalPosition(dsConfigs[UN_CONFIG_COMMON_LENGTH + iNormalPosition], exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
    } else {
        unConfigGenericFunctions_getNormalPosition(dsConfigs[UN_CONFIG_COMMON_LENGTH + iNormalPosition], alarmExists, alarmActive, bOkRange, iLevel, bSMS);
        if (alarmExists) {
            if (dynlen(diLimits) == 4) {
                unConfigGenericFunctions_checkFloat(dsConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[1]], fLL, exceptionInfoTemp);
                unConfigGenericFunctions_checkFloat(dsConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[2]], fL, exceptionInfoTemp);
                unConfigGenericFunctions_checkFloat(dsConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[3]], fH, exceptionInfoTemp);
                unConfigGenericFunctions_checkFloat(dsConfigs[UN_CONFIG_COMMON_LENGTH + diLimits[4]], fHH, exceptionInfoTemp);
                if (dynlen(exceptionInfoTemp) <= 0) {	// Check Ok
                    unConfigGenericFunctions_check4LevelsAlert(fHH, fH, fL, fLL, iLevel, bError);
                    if (bError) {
                        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_check5RangesAlert:" + getCatStr("unGeneration", "BADDATA"), "");
                    }
                } else {
                    dynAppend(exceptionInfo, exceptionInfoTemp);
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_check5RangesAlert:" + getCatStr("unGeneration", "BADDATA"), "");
                }
            } else {
                dynAppend(exceptionInfo, exceptionInfoTemp);
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_check5RangesAlert:" + getCatStr("unGeneration", "BADDATA"), "");
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getArchiving
/**
Purpose: is archiving active

Parameters:
		sParameter, string, input, archiving field in config file
		archiveActive, bool , output, archiving active

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_getArchiving(string sParameter, bool &archiveActive, int iArchiveTimeFilter = 0,
                                      int &iArchiveType, int &iSmoothProcedure) {
    /*
    			use archive=Y/N/O/A
    			N=No,
    			Y=Yes if archiveTimeFilter=0 ==> no smoothing else time smoothing
    			O=Yes if archiveTimeFilter=0 => only old/new else old/new or time smoothing
    			A=Yes if archiveTimeFilter=0 => only old/new else old/new and time smoothing
      VA,xxx.xx,A/O/N= absolute value smoothing with A: and time smoothing/O: or time smoothing/N or anything else no time smoothing
      VR,xxx.xx, A/O/N= relative value smoothing based on min/max with A: and time smoothing/O: or time smoothing/N or anything else no time smoothing
    */
    dyn_string dsSplit = strsplit(sParameter, UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR);

    while (dynlen(dsSplit) < UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_LENGTH) {
        dynAppend(dsSplit, "N");
    }

    switch (dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TYPE]) {
        case UN_CONFIG_ARCHIVE_ACTIVE:
            archiveActive = true;
            if (iArchiveTimeFilter > 0) { // time smoothing
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_TIME_SMOOTH;
            } else { // no smoothing
                iArchiveType = DPATTR_ARCH_PROC_VALARCH;
                iSmoothProcedure = 0;
            }
            break;
        case UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_OR_TIME:
            archiveActive = true;
            if (iArchiveTimeFilter > 0) { // time smoothing or old/new
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_OLD_NEW_OR_TIME_SMOOTH;
            } else { // old/new
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_COMPARE_OLD_NEW;
            }
            break;
        case UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_AND_TIME:
            archiveActive = true;
            if (iArchiveTimeFilter > 0) { // time smoothing and old/new
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_OLD_NEW_AND_TIME_SMOOTH;
            } else { // old/new
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_COMPARE_OLD_NEW;
            }
            break;
        case UN_CONFIG_ARCHIVE_ACTIVE_VALUE_ABSOLUTE:
            archiveActive = true;
            if ((dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TIME] == UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_AND_TIME) &&
                    (iArchiveTimeFilter > 0)) { // value absolute and time smoothing
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_TIME_AND_VALUE_SMOOTH;
            } else if ((dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TIME] == UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_OR_TIME) &&
                       (iArchiveTimeFilter > 0)) { // value absolute or time smoothing
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_TIME_OR_VALUE_SMOOTH;
            } else { // value absolute
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_VALUE_SMOOTH;
            }
            break;
        case UN_CONFIG_ARCHIVE_ACTIVE_VALUE_RELATIVE:
            archiveActive = true;
            if ((dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TIME] == UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_AND_TIME) &&
                    (iArchiveTimeFilter > 0)) { // value relative and time smoothing
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_TIME_AND_VALUE_REL_SMOOTH;
            } else if ((dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TIME] == UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_OR_TIME) &&
                       (iArchiveTimeFilter > 0)) { // value relative or time smoothing
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_TIME_OR_VALUE_REL_SMOOTH;
            } else { // value relative
                iArchiveType = DPATTR_ARCH_PROC_SIMPLESM;
                iSmoothProcedure = DPATTR_VALUE_REL_SMOOTH;
            }
            break;
        default:
            archiveActive = false;
            iArchiveType = DPATTR_ARCH_PROC_VALARCH;
            iSmoothProcedure = 0;
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getArchivingDeadband
/**
Purpose: is archiving active

Parameters:
		sParameter, string, input, archiving field in config file
		fReturnDeadBand, float, output, the DeadBand Value to set

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_getArchivingDeadband(string sParameter, float &fReturnDeadBand) {
    dyn_string dsSplit = strsplit(sParameter, UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR), exceptionInfo;
    float fDeadBand = -1;

    while (dynlen(dsSplit) < UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_LENGTH) {
        dynAppend(dsSplit, "N");
    }

    switch (dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_TYPE]) {
        case UN_CONFIG_ARCHIVE_ACTIVE_VALUE_ABSOLUTE:
            unConfigGenericFunctions_checkFloat(dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_VALUE], fDeadBand, exceptionInfo);
            if (fDeadBand < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_getArchivingDeadband: deadband < 0", "");
            }
            break;
        case UN_CONFIG_ARCHIVE_ACTIVE_VALUE_RELATIVE:
            unConfigGenericFunctions_checkFloat(dsSplit[UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_VALUE], fDeadBand, exceptionInfo);
            if (fDeadBand < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_getArchivingDeadband: deadband < 0", "");
            }
            break;
        default:
            break;
    }
    if (dynlen(exceptionInfo) > 0) {
        fDeadBand = 0;
    }
    fReturnDeadBand = fDeadBand;
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getNormalPosition
/**
Purpose: get normal position

Parameters:
		sParameter, string, input, alarm state field in config file
		alarmExists, bool , output, does alarm exist ?
		alarmActive, bool, output, is alarm active ?
		bOkRange, bool, output, valid Range (DI/DO devices)?
		iMode, int, output, active level (for AI/AO devices)?

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_getNormalPosition(string sParameter, bool &alarmExists, bool &alarmActive, bool &bOkRange, int &iMode, bool &bSMS) {
    switch (sParameter) {		// Normal Position
        case "0":				// Normal position is off
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 15;			//HH H L LL
            bSMS = false;
            break;
        case "1":				// Normal position is on
            alarmExists = true;
            alarmActive = true;
            bOkRange = true;
            iMode = 15;			//HH H L LL
            bSMS = false;
            break;
        case "3":				// Normal position is off but alarm is masked
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 15;			//HH H L LL
            bSMS = false;
            break;
        case "4":				// Normal position is on but alarm is masked
            alarmExists = true;
            alarmActive = false;
            bOkRange = true;
            iMode = 15;			//HH H L LL
            bSMS = false;
            break;
        case "5":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 14;			//H L LL
            bSMS = false;
            break;
        case "6":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 7;			//HH H L
            bSMS = false;
            break;
        case "7":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 6;			//H L
            bSMS = false;
            break;
        case "8":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 3;			//HH H
            bSMS = false;
            break;
        case "9":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 12;			//L LL
            bSMS = false;
            break;
        case "10":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 2;			//H
            bSMS = false;
            break;
        case "11":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 4;			//L
            bSMS = false;
            break;
        case "12":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 14;			//H L LL
            bSMS = false;
            break;
        case "13":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 7;			//HH H L
            bSMS = false;
            break;
        case "14":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 6;			//H L
            bSMS = false;
            break;
        case "15":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 3;			//HH H
            bSMS = false;
            break;
        case "16":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 12;			//L LL
            bSMS = false;
            break;
        case "17":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 2;			//H
            bSMS = false;
            break;
        case "18":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 4;			//L
            bSMS = false;
            break;
        case "2":				// No normal position
        default:
            alarmExists = false;
            alarmActive = false;
            bSMS = false;
            break;
        case "100":				// Normal position is off
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 15;			//HH H L LL
            bSMS = true;
            break;
        case "101":				// Normal position is on
            alarmExists = true;
            alarmActive = true;
            bOkRange = true;
            iMode = 15;			//HH H L LL
            bSMS = true;
            break;
        case "103":				// Normal position is off but alarm is masked
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 15;			//HH H L LL
            bSMS = true;
            break;
        case "104":				// Normal position is on but alarm is masked
            alarmExists = true;
            alarmActive = false;
            bOkRange = true;
            iMode = 15;			//HH H L LL
            bSMS = true;
            break;
        case "105":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 14;			//H L LL
            bSMS = true;
            break;
        case "106":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 7;			//HH H L
            bSMS = true;
            break;
        case "107":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 6;			//H L
            bSMS = true;
            break;
        case "108":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 3;			//HH H
            bSMS = true;
            break;
        case "109":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 12;			//L LL
            bSMS = true;
            break;
        case "110":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 2;			//H
            bSMS = true;
            break;
        case "111":
            alarmExists = true;
            alarmActive = true;
            bOkRange = false;
            iMode = 4;			//L
            bSMS = true;
            break;
        case "112":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 14;			//H L LL
            bSMS = true;
            break;
        case "113":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 7;			//HH H L
            bSMS = true;
            break;
        case "114":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 6;			//H L
            bSMS = true;
            break;
        case "115":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 3;			//HH H
            bSMS = true;
            break;
        case "116":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 12;			//L LL
            bSMS = true;
            break;
        case "117":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 2;			//H
            bSMS = true;
            break;
        case "118":
            alarmExists = true;
            alarmActive = false;
            bOkRange = false;
            iMode = 4;			//L
            bSMS = true;
            break;
        case "102":				// No normal position
        default:
            alarmExists = false;
            alarmActive = false;
            bSMS = true;
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_createDp
/**
Purpose: create datapoint

Parameters:
		sDpName, string, input, datapoint name
		sDpType, string , input, datapoint type
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_createDp(string sDpName, string sDpType, dyn_string &exceptionInfo) {
    string deviceName;
    bool nameOk = false;

    deviceName = substr(sDpName, strpos(sDpName, ":") + 1);
    if (dpExists(deviceName) == false) {
        nameOk = dpIsLegalName(deviceName);
        if (nameOk) {
            dpCreate(deviceName, sDpType);
            if (dpExists(deviceName) == false) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_createDp: " + deviceName + getCatStr("unGeneration", "DPCREATEFAILED"), "");
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_createDp: " + deviceName + getCatStr("unGeneration", "BADDPNAME"), "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkFloat
/**
Purpose: check if given string could be read as a float

Parameters:
		toBeChecked, input, string, string to be formatted
		formatted, output, float, float from string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkFloat(string toBeChecked, float &formatted, dyn_string &exceptionInfo) {
    if (strltrim(strrtrim(toBeChecked)) != "") {
        if (sscanf(toBeChecked, "%f", formatted) <= 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkFloat:" + getCatStr("unGeneration", "BADDATA"), toBeChecked);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkFloat:" + getCatStr("unGeneration", "EMPTYFIELD"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkInt
/**
Purpose: check if given string could be read as an integer

Parameters:
		toBeChecked, input, string, string to be formatted
		formatted, output, int, float from string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkInt(string toBeChecked, int &formatted, dyn_string &exceptionInfo) {
    if (strltrim(strrtrim(toBeChecked)) != "") {
        if (sscanf(toBeChecked, "%d", formatted) <= 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkInt:" + getCatStr("unGeneration", "BADDATA"), toBeChecked);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkInt:" + getCatStr("unGeneration", "EMPTYFIELD"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkUnsigned
/**
Purpose: check if given string could be read as an unsigned

Parameters:
		toBeChecked, input, string, string to be formatted
		formatted, output, unsigned, float from string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkUnsigned(string toBeChecked, unsigned &formatted, dyn_string &exceptionInfo) {
    int temp;

    if (strltrim(strrtrim(toBeChecked)) != "") {
        if (sscanf(toBeChecked, "%d", temp) > 0) {
            if (temp >= 0) {
                formatted = (unsigned)temp;
            } else {
                formatted = 0u;
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkUnsigned:" + getCatStr("unGeneration", "BADDATA"), toBeChecked);
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkUnsigned:" + getCatStr("unGeneration", "BADDATA"), toBeChecked);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkUnsigned:" + getCatStr("unGeneration", "EMPTYFIELD"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkAlias
/**
Purpose: check Alias string

Parameters:
		alias, input, string, string to be checked
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkAlias(string alias, dyn_string &exceptionInfo) {
    int i, len, pos;
    dyn_string dsSplit, dsTemp;
    string sLink, sAlias;

    dsSplit = strsplit(alias, UN_CONFIG_GROUP_SEPARATOR);
//DebugN("unConfigGenericFunctions_checkAlias", alias, dsSplit);
    len = dynlen(dsSplit);
// check the alias of the device
    if (len >= 1) {
        _unConfigGenericFunctions_checkAlias(dsSplit[1], exceptionInfo);
        sAlias = dsSplit[1];
        dynRemove(dsSplit, 1);
        if ((dynContains(dsSplit, sAlias) > 0) || (dynContains(dsSplit, getSystemName() + sAlias) > 0)) {
            fwException_raise(exceptionInfo, "ERROR", "The device name is not allowed in the links", "");
        } else {
            // check the device link, : is allowed so remove it because a link can be in another system
            dsTemp = dsSplit;
            for (i = 1; i < len; i++) {
                pos = strpos(dsSplit[i], ":");
                sLink = substr(dsSplit[i], pos + 1, strlen(dsSplit[i]));
                _unConfigGenericFunctions_checkAlias(sLink, exceptionInfo);
//DebugN(dsSplit, sLink, dsTemp);
                dynRemove(dsTemp, 1);
                if ((dynContains(dsTemp, dsSplit[i]) > 0) || (dynContains(dsTemp, sLink) > 0) || (dynContains(dsTemp, getSystemName() + sLink) > 0)) {
                    fwException_raise(exceptionInfo, "ERROR", sLink + " is not more than one time in the links", "");
                }
            }
        }
    } else {
        _unConfigGenericFunctions_checkAlias(alias, exceptionInfo);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// _unConfigGenericFunctions_checkAlias
/**
Purpose: check Alias string

Parameters:
		alias, input, string, string to be checked
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

_unConfigGenericFunctions_checkAlias(string alias, dyn_string &exceptionInfo) {
    if (strltrim(strrtrim(alias)) != "") {
        if (patternMatch("*[: ""'@`#$%^&*?!,;=+~(){}<>|]*", alias)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAlias:" + getCatStr("unGeneration", "BADDATA"), alias);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAlias:" + getCatStr("unGeneration", "EMPTYFIELD"), "");
    }
//DebugN("_unConfigGenericFunctions_checkAlias", alias);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkPanel
/**
Purpose: check if given string could be read as a reference to a panel

Parameters:
		panel, input, string, string to be checked
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkPanel(string panel, dyn_string &exceptionInfo) {
    string sPanel;
    int iPos;
    bool bCheck = true;

    sPanel = panel;
    if (strpos(sPanel, unTreeWidget_treeNodeDpPrefix) == 0) {
        bCheck = false;
    } else {
        iPos = strpos(sPanel, ".pnl");
        if (iPos <= 0) {
            iPos = strpos(sPanel, ".xml");
            if (iPos <= 0) {
                iPos = strlen(sPanel);
            }
        }
        sPanel = substr(sPanel, 0, iPos);
    }
    if (bCheck) {
        if (patternMatch("*[: ""'@`#$%^&*?!.,;=+-~(){}<>|]*", sPanel)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkPanel:" + getCatStr("unGeneration", "BADDATA"), panel);
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkGroup
/**
Purpose: check if given string could be read as a group name

Parameters:
		group, input, string, string to be checked
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkGroup(string sText, dyn_string &exceptionInfo) {
// in case of one domain and nature -->	if (patternMatch("*[:""~`!@#$%^&*()+={},;'?|<>/\\]*", sText) || (sText==" "))
    if (patternMatch("*[:""~`!@#$%^&*()+={};'?|<>/\\]*", sText) || (strpos(sText, " ") >= 0)) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkGroup: " + getCatStr("unGeneration", "BADDOMAIN"), sText);
    }

}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkRange
/**
Purpose: check given range

Parameters:
		group, input, string, string to be checked
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkRange(string rangeMin, string rangeMax, dyn_string &exceptionInfo) {
    dyn_string exceptionInfoTemp;
    float fMin, fMax;

    unConfigGenericFunctions_ScientificFormat(rangeMin);
    unConfigGenericFunctions_ScientificFormat(rangeMax);
    unConfigGenericFunctions_checkFloat(rangeMin, fMin, exceptionInfoTemp);
    unConfigGenericFunctions_checkFloat(rangeMax, fMax, exceptionInfoTemp);

    if (dynlen(exceptionInfoTemp) <= 0) {
        if (fMin != fMax) {
            if (fMin >= fMax) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkRange:" + getCatStr("unGeneration", "BADDATA"), "");
            }
        } else if (fMin != 0.0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkRange:" + getCatStr("unGeneration", "BADDATA"), "");
        }
    } else {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkRange:" + getCatStr("unGeneration", "BADDATA"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkAddresses
/**
Purpose: check addresses

Parameters:
		dsAddresses, input, dyn_string, dyn_string to be checked
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkAddresses(dyn_string dsAddresses, dyn_string &exceptionInfo) {
    int i, length;
    unsigned currentAddress;

    length = dynlen(dsAddresses);
    for (i = 1; i <= length; i++) {
        unConfigGenericFunctions_checkUnsigned(dsAddresses[i], currentAddress, exceptionInfo);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkWidget
/**
Purpose: check widget field

Parameters:
		sWidget, string, input, widget name
		deviceType, string, input, unicos object
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkWidget(string sWidget, string deviceType, dyn_string &exceptionInfo) {
    dyn_string dsWidgets;
    int i, len;
    bool found = false;

    unGenericDpFunctions_getDeviceWidgetList(deviceType, dsWidgets);

    len = dynlen(dsWidgets);
    for (i = 1; (i <= len) && (!found); i++) {
        if (sWidget == unGenericDpFunctions_extractDeviceWidgetFromWidgetFileName(dsWidgets[i])) {
            found = true;
        }
    }
    if (!found) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkWidget: " + sWidget + getCatStr("unGeneration", "BADWIDGET"), sWidget);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkDeadBand
/**
Purpose: check dead bande value & type

Parameters:
		sDeadBand, string, input, deadband value
		sDeadBandType, string, input, deadband type
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkDeadBand(string sDeadBand, string sDeadBandType, dyn_string &exceptionInfo) {
    float ff;

    if (sDeadBandType != UN_CONFIG_DEADBAND_NONE) {
        unConfigGenericFunctions_checkFloat(sDeadBand, ff, exceptionInfo);
    }

    if (dynlen(exceptionInfo) == 0) {
        if (sDeadBandType != UN_CONFIG_DEADBAND_RELATIF && sDeadBandType != UN_CONFIG_DEADBAND_VALUE &&
                sDeadBandType != UN_CONFIG_DEADBAND_OLD_NEW && sDeadBandType != UN_CONFIG_DEADBAND_NONE) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDeadBand:" + getCatStr("unGeneration", "DEBUGDEADBANDTYPEBAD"), "");
        } else {
            if (sDeadBandType == UN_CONFIG_DEADBAND_RELATIF && (ff < 0 || ff > 100)) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDeadBand:" + getCatStr("unGeneration", "DEBUGDEADBANDRELATIF"), "");
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkArchiveParameters
/**
Purpose: check archive time filter & active

Parameters:
		sTimeFilter, string, input, archive time filter
		sActive, string, input, archive active
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkArchiveParameters(string sTimeFilter, string sActive, dyn_string &exceptionInfo) {
    float ff;

    if (sTimeFilter != "") {
        switch (sActive) {
            case UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_AND_TIME:
            case UN_CONFIG_ARCHIVE_ACTIVE:
            case UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_OR_TIME:
                unConfigGenericFunctions_checkFloat(sTimeFilter, ff, exceptionInfo);
                break;
            default:
                break;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkUnitFormat
/**
@Deprecated

Purpose: check archive time filter & active

Parameters:
		sUnit, string, input, unit
		sFormat, string, input, format
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkUnitFormat(string sUnit, string sFormat, dyn_string &exceptionInfo) {
    dyn_string tmpExceptionInfo;
    fwException_raise(tmpExceptionInfo, "WARNING", "unConfigGenericFunctions_checkUnitFormat: this function is deprecated. Please use unConfigGenericFunctions_checkFormat()", "");
    unConfigGenericFunctions_checkFormat(sFormat, exceptionInfo);
}


dyn_string _unConfigGenericFunctions_extractFormatSubitems(string sFormat) {
    dyn_string dsFormatSplit;
    if (strpos(sFormat, UN_PARAMETER_SUBITEM_DELIMITER) >= 0) {
        dsFormatSplit = strsplit(sFormat, UN_PARAMETER_SUBITEM_DELIMITER);
    } else {
        dsFormatSplit[1] = sFormat;
    }
    return dsFormatSplit;
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkFormat
/**
Purpose: check the value format

@par Constraints

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param sFormat the format as string (for example, #.##) Plus the Logarithmic Scale flag for the trending. example:
          #.##|LOG_SCALE if logarithmic,
          #.##| if not logarithmic.
@param exceptionInfo	Details of any exceptions are returned here
*/
unConfigGenericFunctions_checkFormat(string sFormat, dyn_string &exceptionInfo) {
    string sTempFormat = strtoupper(sFormat);
    int pos, iFormat;
    dyn_string dsFormatSplit;

//extract the Logarithmic scale flag
    dsFormatSplit = _unConfigGenericFunctions_extractFormatSubitems(sTempFormat);
// DebugN("unConfigGenericFunctions_checkFormat - dsFormatSplit",dsFormatSplit);
    sTempFormat = dsFormatSplit[1];
// DebugN("unConfigGenericFunctions_checkFormat - sTempFormat",sTempFormat);
    if (dynlen(dsFormatSplit) > 1) {
        switch (dsFormatSplit[2]) {
            case UN_DEVICE_LOG_SCALE_CONFIGURATION:
            case "":
                break;
            default:
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkFormat: " + dsFormatSplit[2] + getCatStr("unGeneration", "DEBUGBADFORMAT"), "");
                break;
        }
    }

// if EXP->OK
    if (sTempFormat != UN_FACEPLATE_EXP) {
// check if fixed display
        pos = strpos(sTempFormat, UN_DISPLAY_VALUE_DIGIT_TYPE);
        if (pos > 0) { // fixed display
            sTempFormat = substr(sTempFormat, 0, pos);
            pos = (int)sTempFormat;
            if (pos <= 0) { // error
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkFormat: " + sFormat + getCatStr("unGeneration", "DEBUGBADFORMAT"), "");
            }
        } else { // check ##.# format of xEXP
            pos = strpos(sTempFormat, UN_FACEPLATE_EXP);
            if (pos > 0) { // xEXP format
                strreplace(sTempFormat, UN_FACEPLATE_EXP, "");
                unConfigGenericFunctions_checkInt(sTempFormat, iFormat, exceptionInfo);
            } else { // ##.# format
                strreplace(sTempFormat, "#", "");
                if ((sTempFormat != "") && (sTempFormat != ".")) { // error
                    fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkFormat: " + sFormat + getCatStr("unGeneration", "DEBUGBADFORMAT"), "");
                }
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkNormalPosition
/**
Purpose: check normal position parameter

Parameters:
		sNormalPosition, string, input, parameter
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkNormalPosition(string sNormalPosition, dyn_string &exceptionInfo) {
    int iNormalPosition;

    sscanf(sNormalPosition, "%d", iNormalPosition);
    if ((iNormalPosition < 0) || (iNormalPosition > 18 && iNormalPosition < 100) || (iNormalPosition > 118)) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkNormalPosition:" + getCatStr("unGeneration", "BADDATA"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkDpName
/**
Purpose: check dp name

Parameters:
		sDpName, string, input, dp name
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkDpName(string sDpName, dyn_string &exceptionInfo) {
    string deviceName, systemName;
    dyn_string dsNames, dsNameContent;
    dyn_uint duIds;
    bool bContentOk;

    deviceName = substr(sDpName, strpos(sDpName, ":") + 1);
    systemName = unGenericDpFunctions_getSystemName(sDpName);
    if (strltrim(strrtrim(deviceName)) != "") {
        if (!dpIsLegalName(deviceName)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDpName:" + getCatStr("unGeneration", "BADDPNAME"), "");
        } else {
            bContentOk = true;
            dsNameContent = strsplit(deviceName, UN_DPNAME_SEPARATOR);
            if (dynlen(dsNameContent) != UN_DPNAME_FIELD_LENGTH) {
                bContentOk = false;
            }
            if (!bContentOk) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDpName:" + getCatStr("unGeneration", "BADUNICOSDPNAME"), "");
            }
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDpName:" + getCatStr("unGeneration", "EMPTYFIELD"), "");
    }
    if (systemName != "") {
        if (systemName != getSystemName()) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDpName:" + getCatStr("unGeneration", "BADSYSTEMNAME"), "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkParameters
/**
Purpose: check common parameters

Parameters:
		dsCommonParameters, dyn_string, input, config dyn_string -> text line without additional parameters
		sObject, string, input, device type
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkParameters(dyn_string dsCommonParameters, string sObject, dyn_string &exceptionInfo) {
    dyn_string ds;

    // alias here is deviceName,deviceLink
    unConfigGenericFunctions_checkAlias(dsCommonParameters[UN_CONFIG_COMMON_ALIAS], exceptionInfo);
    // Description UN_CONFIG_COMMON_DESCRIPTION
    ds = strsplit(dsCommonParameters[UN_CONFIG_COMMON_DOMAIN], UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
    // if no ACC domain assume list of domain
    if (dynlen(ds) == UN_CONFIG_UNICOS_DOMAIN_AND_ACCESS_CONTROL_LEN) {
        unConfigGenericFunctions_checkDeviceAccessControl(ds[1], exceptionInfo);
        unConfigGenericFunctions_checkGroup(ds[2], exceptionInfo);
    } else if (strpos(dsCommonParameters[UN_CONFIG_COMMON_DOMAIN], UN_ACCESS_CONTROL_DOMAIN_SEPARATOR) >= 0) { // if UN_ACCESS_CONTROL_DOMAIN_SEPARATOR in sDomain config --> domain = ""
        unConfigGenericFunctions_checkDeviceAccessControl(ds[1], exceptionInfo);
    } else {
        unConfigGenericFunctions_checkGroup(dsCommonParameters[UN_CONFIG_COMMON_DOMAIN], exceptionInfo);
    }

    ds = strsplit(dsCommonParameters[UN_CONFIG_COMMON_NATURE], UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
    // if no ACC priv assume list of nature
    if (dynlen(ds) >= (UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN - 1)) {
        unConfigGenericFunctions_checkDeviceAccessControl(ds[1], exceptionInfo);
        unConfigGenericFunctions_checkDeviceAccessControl(ds[2], exceptionInfo);
        unConfigGenericFunctions_checkDeviceAccessControl(ds[3], exceptionInfo);
        if (dynlen(ds) == UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN) {
            unConfigGenericFunctions_checkGroup(ds[4], exceptionInfo);
        }
    } else {
        unConfigGenericFunctions_checkGroup(dsCommonParameters[UN_CONFIG_COMMON_NATURE], exceptionInfo);
    }

    unConfigGenericFunctions_checkPanel(dsCommonParameters[UN_CONFIG_COMMON_DIAGNOSTIC], exceptionInfo);
    // Html UN_CONFIG_COMMON_HTML
    unConfigGenericFunctions_checkPanel(dsCommonParameters[UN_CONFIG_COMMON_DEFAULTPANEL], exceptionInfo);
    unConfigGenericFunctions_checkWidget(dsCommonParameters[UN_CONFIG_COMMON_WIDGET], sObject, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkAll
/**
Purpose: check the global config syn_string

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		sObject, string, input, device type
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkAll(dyn_string dsConfig, string sObject, dyn_string &exceptionInfo) {
    unsigned uPLCDriver, uPLCNumber;
    dyn_string dsConfigLine, checkLine, dsFunctions;
    string sFunction;
    int i;
    bool bCheck, bDollar = true;
    dyn_string dsDollar;

    // DebugN("FUNCTION: unConfigGenericFunctions_checkAll(dsConfig= "+dsConfig+ " sObject= "+sObject+ " )");

    if (isFunctionDefined("shapeExists")) {
        if (shapeExists("checkData")) {
            bCheck = checkData.state(0);
            bDollar = false;
        }
    }
    if (bDollar) {
        bCheck = (bool)$bCheckBeforeImport;
        dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType, "$sDeviceType:" + $sDeviceType);
    }
    // 1. Check general fields
    if (dynlen(dsConfig) >= UN_CONFIG_OBJECTGENERAL_LENGTH) {
        unConfigGenericFunctions_checkUnsigned(dsConfig[UN_CONFIG_OBJECTGENERAL_DRIVER], uPLCDriver, exceptionInfo);
        unConfigGenericFunctions_checkPLCType(dsConfig[UN_CONFIG_OBJECTGENERAL_PLC_TYPE], exceptionInfo);
        unConfigGenericFunctions_checkUnsigned(dsConfig[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER], uPLCNumber, exceptionInfo);
        if (dsConfig[UN_CONFIG_OBJECTGENERAL_ARCHIVE_BOOL] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAll: bool " + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsConfig[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAll: analog " + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        // check UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT
        if (dsConfig[UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAll: event " + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        // Object DpType UN_CONFIG_OBJECTGENERAL_OBJECT
        unConfigGenericFunctions_checkDpName(dsConfig[UN_CONFIG_OBJECTGENERAL_DPNAME], exceptionInfo);

        // 2. Check object config
        dsConfigLine = dsConfig;
        for (i = 1; i <= UN_CONFIG_OBJECTGENERAL_LENGTH; i++) {
            dynRemove(dsConfigLine, 1);
        }


// Init
        unGenericObject_GetFunctions(sObject, dsFunctions, exceptionInfo);
        sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_GENERATIONCHECK];

        // DebugN("--------> sFunction= "+sFunction+ " ----------------");
        unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkAll", bCheck, sFunction, dsDollar, dsConfigLine, "-");

        if (sFunction != "") {
            if (bCheck) {
                evalScript(checkLine, "dyn_string main(dyn_string configLine) {" +
                           "dyn_string exceptionInfo;" +
                           "if (isFunctionDefined(\"" + sFunction  + "\"))" +
                           "    {" + sFunction  + "(configLine, exceptionInfo);" +
                           "    }" +
                           "else " +
                           "    {" +
                           "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction  + "\" + getCatStr(\"unGeneration\",\"UNKNOWNFUNCTION\"),\"\");" +
                           "    }" +
                           "return exceptionInfo; }", dsDollar, dsConfigLine);
                if (dynlen(checkLine) > 0) {
                    dynAppend(exceptionInfo, checkLine);
                }
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAll:" + getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAll:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkDeleteCommand
/**
Purpose: check delete command line

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		dsDeleteDps, dyn_string, output, list of dp that will be deleted
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo) {
    string sPlcName, sPLCSubApplication = "", sObjectType = "", sNumber = "", sDpPlcName, sPlcModPlc;
    dyn_string dsAlarmDps;

    if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
// get PLC Name
        sPlcName = dsConfig[2];
        if ((sPlcName != "") && (dsConfig[1] == UN_DELETE_COMMAND)) {
            if (dynlen(dsConfig) >= 3) {
// get Application
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) {
// get DeviceType
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) {
// get device number
                sNumber = dsConfig[5];
            }
            unGenericDpFunctions_getPlcDevices("", sPlcName, sPLCSubApplication, sObjectType, sNumber, dsDeleteDps);
// if only delete key word then delete also modbus and systemalarm
//DebugN("del", sPlcName, dsDeleteDps, sPLCSubApplication, sObjectType, sNumber, c_unSystemIntegrity_UnPlc+sPlcName, dpExists(c_unSystemIntegrity_UnPlc+sPlcName));
            if ((sPlcName != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
// if sDpPlcName exist get _Mod_Plc and add it.
// sDpPlc is _unPlc_plcName
                sDpPlcName = c_unSystemIntegrity_UnPlc + sPlcName;
                if (dpExists(sDpPlcName)) {
                    dynAppend(dsDeleteDps, sDpPlcName);
                    dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1), c_unSystemAlarm_dpType);
                    dynAppend(dsDeleteDps, dsAlarmDps);
                    dpGet(sDpPlcName + ".configuration.mod_PLC", sPlcModPlc);
                    if (dpExists(sPlcModPlc)) {
                        dynAppend(dsDeleteDps, sPlcModPlc);
                    }
                }
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
//DebugN("end", dsDeleteDps);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkDeleteCommand", dsConfig, dsDeleteDps, "-");
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_deleteCommand
/**
Purpose: execute delete command line

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo) {
    string sPLCSubApplication = "", sObjectType = "", sNumber = "", sh = "";
    dyn_string dsDpeList, exceptionInfoTemp;

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_deleteCommand", dsConfig, "-");
    unConfigGenericFunctions_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_deleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
            if (dynlen(dsConfig) >= 3) {
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) {
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) {
                sNumber = dsConfig[5];
            }
            if (isFunctionDefined("shapeExists")) {
                if (shapeExists("progressBar")) {
                    sh = "progressBar";
                }
            }

            unGenericDpFunctions_deletePlcDevices(dsConfig[2], sPLCSubApplication, sObjectType, sNumber, "progressBar", exceptionInfo);

            if ((dsConfig[2] != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
                unGenericDpFunctions_deletePlc(dsConfig[2], exceptionInfo);
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkSystemAlarm
/**
Purpose: check system alarm config line

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkSystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo) {
    string sName, sFEType, sFunction;
    dyn_string exceptionInfoTemp;
    dyn_string dsDollar;
    bool bDollar = true;

    if (dynlen(dsConfig) >= UN_CONFIG_CHECKSYSTEM_ALARM_PLC_NAME) {
        sName = dsConfig[UN_CONFIG_CHECKSYSTEM_ALARM_PLC_NAME];
//DebugN(dsConfig);
        if (sName == "" || !unConfigGenericFunctions_nameCheck(sName)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkSystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), dsConfig[UN_CONFIG_CHECKSYSTEM_ALARM_PLC_NAME]);
        } else {
            sFEType = dsConfig[UN_CONFIG_CHECKSYSTEM_ALARM_FRONTEND];
            if ((sFEType == UN_CONFIG_PREMIUM) || (sFEType == UN_CONFIG_QUANTUM) || (sFEType == UN_CONFIG_UNITY)) {
                sFEType = UN_PLC_DPTYPE;
            }

            sFunction = sFEType + UN_CONFIG_SYSTEM_ALARM_CHECKFUNCTION;
            if (isFunctionDefined("shapeExists")) {
                if (shapeExists("TextFieldPlcType")) {
                    bDollar = false;
                }
            }
            if (bDollar) {
                dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
            }
            unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkSystemAlarm", sFunction, dsDollar, dsConfig, "-");
            evalScript(exceptionInfoTemp, "dyn_string main(dyn_string configLine) {" +
                       "dyn_string exceptionInfo;" +
                       "if (isFunctionDefined(\"" + sFunction + "\"))" +
                       "    {" +
                       "    " + sFunction + "(configLine, exceptionInfo);" +
                       "    }" +
                       "else " +
                       "    {" +
                       "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNCHECK\"),\"\");" +
                       "    }" +
                       "return exceptionInfo; }", dsDollar,
                       dsConfig);
            dynAppend(exceptionInfo, exceptionInfoTemp);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkSystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_checkSystemAlarm
/**
Purpose: check system alarm config line for QUANTUM and PREMIUM PLC

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-25 @whitelisted{Callback}

*/

_UnPlc_checkSystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo) {
    if (dynlen(dsConfig) == UN_CONFIG_CHECKSYSTEM_ALARM_LENGTH) {
        unConfigGenericFunctions_checkAddresses(makeDynString(dsConfig[UN_CONFIG_CHECKSYSTEM_ALARM_ADDRESS]), exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_checkSystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkFESystemAlarm
/**
Purpose: check FESystemAlarm type config line

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkFESystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo) {
    string sName, sFEType, sFunction;
    dyn_string exceptionInfoTemp;
    dyn_string dsDollar;
    bool bDollar = true;

    if (dynlen(dsConfig) >= UN_CONFIG_CHECKFESYSTEM_ALARM_NAME) {
        sName = dsConfig[UN_CONFIG_CHECKFESYSTEM_ALARM_NAME];
        if (sName == "" || !unConfigGenericFunctions_nameCheck(sName)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkFESystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), dsConfig[UN_CONFIG_CHECKFESYSTEM_ALARM_NAME]);
        } else {
            sFEType = dsConfig[UN_CONFIG_CHECKFESYSTEM_ALARM_FRONTEND];
            if ((sFEType == UN_CONFIG_PREMIUM) || (sFEType == UN_CONFIG_QUANTUM) || (sFEType == UN_CONFIG_UNITY)) {
                sFEType = UN_PLC_DPTYPE;
            } else if ((sFEType == UN_CONFIG_S7_400) || (sFEType == UN_CONFIG_S7_300)) {
                sFEType = S7_PLC_DPTYPE;
            }

            sFunction = sFEType + UN_CONFIG_FESYSTEM_ALARM_CHECKFUNCTION;
            if (isFunctionDefined("shapeExists")) {
                if (shapeExists("TextFieldPlcType")) {
                    bDollar = false;
                }
            }
            if (bDollar) {
                dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
            }
            unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkFESystemAlarm", sFunction, dsDollar, dsConfig, "-");
            evalScript(exceptionInfoTemp, "dyn_string main(dyn_string configLine) {" +
                       "dyn_string exceptionInfo;" +
                       "if (isFunctionDefined(\"" + sFunction + "\"))" +
                       "    {" +
                       "    " + sFunction + "(configLine, exceptionInfo);" +
                       "    }" +
                       "else " +
                       "    {" +
                       "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNCHECK\"),\"\");" +
                       "    }" +
                       "return exceptionInfo; }", dsDollar,
                       dsConfig);
            dynAppend(exceptionInfo, exceptionInfoTemp);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkFESystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_checkFESystemAlarm
/**
Purpose: check FE system alarm config line for QUANTUM and PREMIUM PLC

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-25 @whitelisted{Callback}

*/

_UnPlc_checkFESystemAlarm(dyn_string dsConfig, dyn_string &exceptionInfo) {
    if (dynlen(dsConfig) == UN_CONFIG_CHECKFESYSTEM_ALARM_LENGTH) {
        unConfigGenericFunctions_checkAddresses(makeDynString(dsConfig[UN_CONFIG_CHECKFESYSTEM_ALARM_ADDRESS]), exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "_UnPlc_checkFESystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setSystemAlarm
/**
Purpose: set system alarm config line

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		dsAdditionalParameters, dyn_string, input, additional parameters
			1. dp name
			2. plc name
			3. plc type
			4. plc number
			5. driver num
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setSystemAlarm(dyn_string dsConfig, dyn_string dsAdditionalParameters, dyn_string &exceptionInfo) {
    string sDpName, sPlcName, sPLCType, sArchive, sFunction;
    int iPLCNumber, driverNum;
    dyn_string dsAddress, exInfo;
    dyn_string dsDollar;
    bool bDollar = true;

    if ((dynlen(dsAdditionalParameters) == UN_CONFIG_ADDITIONAL_SYSTEMALARM_LENGTH) && (dynlen(dsConfig) >= UN_CONFIG_SETSYSTEM_ALARM_PLC_NAME)) {
        sPLCType = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_TYPE];
        iPLCNumber = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_PLCNUMBER];
        driverNum = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_DRIVER];
        sDpName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_DEVICENAME];
        sPlcName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_PLCNAME];
        sArchive = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_ARCHIVE];
//DebugN("SA", dsConfig, dsAdditionalParameters, sPLCType, iPLCNumber, driverNum, sDpName, sPlcName, sArchive);
        unSystemIntegrity_createSystemAlarm(sDpName, PLC_PLC_pattern, "Communication " + dsConfig[UN_CONFIG_SETSYSTEM_ALARM_PLC_NAME] + " -> " + sPlcName, exceptionInfo);
        sDpName = c_unSystemAlarm_dpPattern + PLC_PLC_pattern + sDpName + ".alarm";

        if ((sPLCType == UN_CONFIG_PREMIUM) || (sPLCType == UN_CONFIG_QUANTUM) || (sPLCType == UN_CONFIG_UNITY)) {
            sPLCType = UN_PLC_DPTYPE;
        }
        sFunction = sPLCType + UN_CONFIG_SYSTEM_ALARM_SETFUNCTION;

        if (isFunctionDefined(sFunction)) {
            if (isFunctionDefined("shapeExists")) {
                if (shapeExists("TextFieldPlcType")) {
                    bDollar = false;
                }
            }
            if (bDollar) {
                dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
            }
            unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_setSystemAlarm", sFunction, dsDollar, dsConfig, "-");
            evalScript(dsAddress, "dyn_string main(dyn_string configLine, dyn_string dsAdd) {" +
                       "dyn_string dsAddress;" +
                       sFunction + "(configLine, dsAdd, dsAddress);" +
                       "return dsAddress; }", dsDollar,
                       dsConfig, dsAdditionalParameters);
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setSystemAlarm: " + sFunction + " " + getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
        }

        if (dynlen(dsAddress) != 0) {
            fwPeriphAddress_set(sDpName, dsAddress, exceptionInfo);
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setSystemAlarm: " + sDpName + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
        fwArchive_set(sDpName,
                      sArchive,
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exInfo);
        dynAppend(exceptionInfo, exInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setSystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_setSystemAlarm
/**
Purpose: set system alarm config line form QUANTUM and PREMIUM PLC

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		dsAdditionalParameters, dyn_string, input, additional parameters
			1. dp name
			2. plc name
			3. plc type
			4. plc number
			5. driver num
		dsAddress, dyn_string, output, for address

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-08-01 @whitelisted{Callback}

*/

_UnPlc_setSystemAlarm(dyn_string dsConfig, dyn_string dsAdditionalParameters, dyn_string &dsAddress) {
    string sDpName, sPlcName, sPLCType, addressReference, sArchive;
    int iPLCNumber, driverNum;

    if (dynlen(dsConfig) == UN_CONFIG_SETSYSTEM_ALARM_LENGTH) {
        sPLCType = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_TYPE];
        iPLCNumber = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_PLCNUMBER];
        driverNum = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_DRIVER];
        sDpName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_DEVICENAME];
        sPlcName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_PLCNAME];
        sArchive = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_SYSTEMALARM_ARCHIVE];

        unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT, sPLCType, iPLCNumber, false,
                dsConfig[UN_CONFIG_SETSYSTEM_ALARM_ADDRESS], dsAdditionalParameters[6]), addressReference);
        if (addressReference != "") {
            dsAddress = makeDynString("MODBUS",
                                      driverNum,
                                      addressReference,
                                      DPATTR_ADDR_MODE_INPUT_SPONT,
                                      PVSS_MODBUS_INT16,
                                      UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                      "",
                                      "",
                                      "",
                                      "",
                                      true,
                                      0,
                                      UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                      UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                      "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setFESystemAlarm
/**
Purpose: set system alarm config line

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		dsAdditionalParameters, dyn_string, input, additional parameters
makeDynString(currentDevice, sPlcName, sPlcType, iPlcNumber, driver, bEvent16)
			1. dp name
			2. PLCname
			3. PLC type
			4. PLC number
			5. driver num
		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setFESystemAlarm(dyn_string dsConfig, dyn_string dsAdditionalParameters, dyn_string &exceptionInfo) {
    string sDpName, sPlcName, sPLCType, sArchive, sFunction;
    int iPLCNumber, driverNum;
    bool bOkRange = false;
    dyn_string dsAddress, exInfo;
    dyn_string dsDollar;
    bool bDollar = true;

    if ((dynlen(dsAdditionalParameters) == UN_CONFIG_ADDITIONAL_FESYSTEMALARM_LENGTH) && (dynlen(dsConfig) >= UN_CONFIG_SETFESYSTEM_ALARM_NAME)) {
        sPLCType = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_TYPE];
        iPLCNumber = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNUMBER];
        driverNum = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DRIVER];
        sDpName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DEVICENAME];
        sPlcName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNAME];
        sArchive = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_ARCHIVE];

//DebugN("FESA", dsConfig, dsAdditionalParameters, sPLCType, iPLCNumber, driverNum, sDpName, sPlcName, sArchive);
        if (strtolower(dsConfig[UN_CONFIG_SETFESYSTEM_ALARM_OKALARM]) == "true") {
            bOkRange = true;
        }
        unSystemIntegrity_createFrontEndSystemAlarm(sDpName, FE_pattern, dsConfig[UN_CONFIG_SETFESYSTEM_ALARM_DESCRIPTION], bOkRange, exceptionInfo);
        sDpName = c_unSystemAlarm_dpPattern + FE_pattern + sDpName + ".alarm";

        if ((sPLCType == UN_CONFIG_PREMIUM) || (sPLCType == UN_CONFIG_QUANTUM) || (sPLCType == UN_CONFIG_UNITY)) {
            sPLCType = UN_PLC_DPTYPE;
        } else if ((sPLCType == UN_CONFIG_S7_400) || (sPLCType == UN_CONFIG_S7_300)) {
            sPLCType = S7_PLC_DPTYPE;
        }
        sFunction = sPLCType + UN_CONFIG_FESYSTEM_ALARM_SETFUNCTION;

        if (isFunctionDefined(sFunction)) {
            if (isFunctionDefined("shapeExists")) {
                if (shapeExists("TextFieldPlcType")) {
                    bDollar = false;
                }
            }
            if (bDollar) {
                dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
            }
            unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_setFESystemAlarm", sFunction, dsDollar, dsConfig, "-");
            evalScript(dsAddress, "dyn_string main(dyn_string configLine, dyn_string dsAdd) {" +
                       "dyn_string dsAddress;" +
                       sFunction + "(configLine, dsAdd, dsAddress);" +
                       "return dsAddress; }", dsDollar,
                       dsConfig, dsAdditionalParameters);
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFESystemAlarm: " + sFunction + " " + getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
        }

        if (dynlen(dsAddress) != 0) {
            fwPeriphAddress_set(sDpName, dsAddress,	exceptionInfo);
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFESystemAlarm: " + sDpName + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
        fwArchive_set(sDpName,
                      sArchive,
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exInfo);
        dynAppend(exceptionInfo, exInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFESystemAlarm:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_setFESystemAlarm
/**
Purpose: set FEsystem alarm config line form QUANTUM and PREMIUM PLC

Parameters:
		dsConfig, dyn_string, input, config dyn_string
		dsAdditionalParameters, dyn_string, input, additional parameters
			1. dp name
			2. plc name
			3. plc type
			4. plc number
			5. driver num
		dsAddress, dyn_string, output, for address

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-08-01 @whitelisted{Callback}

*/

_UnPlc_setFESystemAlarm(dyn_string dsConfig, dyn_string dsAdditionalParameters, dyn_string &dsAddress) {
    string sDpName, sPlcName, sPLCType, addressReference, sArchive;
    int iPLCNumber, driverNum;
    bool bOkRange = false;

    if (dynlen(dsConfig) == UN_CONFIG_SETFESYSTEM_ALARM_LENGTH) {
        sPLCType = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_TYPE];
        iPLCNumber = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNUMBER];
        driverNum = (int)dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DRIVER];
        sDpName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_DEVICENAME];
        sPlcName = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_PLCNAME];
        sArchive = dsAdditionalParameters[UN_CONFIG_ADDITIONAL_FESYSTEMALARM_ARCHIVE];

        unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT, sPLCType, iPLCNumber, false,
                dsConfig[UN_CONFIG_SETFESYSTEM_ALARM_ADDRESS], dsAdditionalParameters[6]), addressReference);
        if (addressReference != "") {
            dsAddress = makeDynString("MODBUS",
                                      driverNum,
                                      addressReference,
                                      DPATTR_ADDR_MODE_INPUT_SPONT,
                                      PVSS_MODBUS_INT16,
                                      UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                      "",
                                      "",
                                      "",
                                      "",
                                      true,
                                      0,
                                      UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                      UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                      "");
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_convertIP
/**
Purpose: IP address format to be send to the PLC

Parameters: string, input, sIP, computer address IP

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
int unConfigGenericFunctions_convertIP(string sIP) {
    dyn_string dsIP;
    string sIPhexa, currentHexa;
    int i, iResult, currentIP, iLen;
    bool bIPOk = true;

    dsIP = strsplit(sIP, ".");
    if (dynlen(dsIP) == 4) {
        for (i = 1; i <= 4; i++) {
            sscanf(dsIP[i], "%d", currentIP);
            iLen = sprintf(currentHexa, "%x", currentIP);
            if (iLen == 1) {
                currentHexa = "0" + currentHexa;
            }
            if ((iLen == 2) || (iLen == 1)) {
                sIPhexa = sIPhexa + currentHexa;
            } else {
                bIPOk = false;
            }
        }
        if (bIPOk) {
            if (sscanf(sIPhexa, "%x", iResult) <= 0) {
                bIPOk = false;
            }
        }
    } else {
        bIPOk = false;
    }
    if (!bIPOk) {
        iResult = 0;
    }
    return iResult;
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkIP
/**
Purpose: check IP format

Parameters: string, input, sIP, IP string to be checked
		    dyn_string, output, exceptionInfo, for errors
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_checkIP(string sIP, dyn_string &exceptionInfo) {
    dyn_string dsIP, dsIPRedu;
    bool bIPOk = false;
    int i1, i2, i3, i4, plcNr;

//if redundant PLC, the IP are separated by a separator UN_PARAMETER_SUBITEM_DELIMITER.
// Split them and theck them one by one
    dsIPRedu = strsplit(sIP, UN_PARAMETER_SUBITEM_DELIMITER);
// DebugN("unConfigGenericFunctions_checkIP - dsIPRedu:",dsIPRedu);
    for (plcNr = 1 ; plcNr < dynlen(dsIPRedu) ; plcNr++) {
        dsIP = strsplit(dsIPRedu[plcNr], ".");
        if (dynlen(dsIP) == 4) {
            if ((sscanf(dsIP[1], "%d", i1) > 0) &&
                    (sscanf(dsIP[2], "%d", i2) > 0) &&
                    (sscanf(dsIP[3], "%d", i3) > 0) &&
                    (sscanf(dsIP[4], "%d", i4) > 0)) {
                if ((i1 >= 0) && (i1 <= 255) && (i2 >= 0) && (i2 <= 255) && (i3 >= 0) && (i3 <= 255) && (i4 >= 0) && (i4 <= 255)) {
                    bIPOk = true;
                }
            }
        }
        if (!bIPOk) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkIP:" + getCatStr("unGeneration", "ERRORIP") + ": " + dsIP, "");
            return;
        }
    }
}





//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_ScientificFormat
/**
Purpose: Format scientific value to float

Parameters:
		sValue: string, input, value to be format

Usage: Internal function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_ScientificFormat(string &sValue) {
    string sVal, sExp, sTemp_1, sTemp_2, sFormat;
    int iPos, iNb, iExp;
    float fVal;

    sVal = strtolower(sValue);

    iPos = strpos(sVal, "e");
    if (iPos >= 1) {
        sExp = substr(sVal, iPos + 1, strlen(sVal));
        sExp = strrtrim(sExp);
        sTemp_1 = substr(sExp, 0, 1);
        if (sTemp_1 == "+") {
            sExp = substr(sExp, 1, strlen(sExp)); //delete +
        }

        sTemp_1 = substr(sVal, 0 , iPos);
        sVal = sTemp_1 + "e" + sExp; //Format

        //%.?f
        sFormat = "%.2f";
        iExp = (int)sExp;
        iPos = strpos(sTemp_1, ".");
        if (iPos >= 1) {
            sTemp_2 = substr(sTemp_1, iPos + 1, strlen(sTemp_1));
            iNb = strlen(sTemp_2) - iExp;
            if (iNb > 0) {
                sFormat = "%." + iNb + "f";
            }
        } else {
            if (iExp < 0) {
                sFormat = "%." + substr(sExp, 1 , strlen(sExp)) + "f";
            }
        }
        sscanf(sVal, "%e", fVal); //Format to Float
        sprintf(sVal, sFormat, fVal); //Format to String
    }
    sValue = strltrim(strrtrim(sVal));
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_GetDeadBandValue
/**
Purpose : get DeadBand Value from DeadBandType

Parameters:
		sDeadBand, string, input, Deadband Value set in the file
		fDeadBand, float, output, new DeadBand Value to set
		iDeadBandType, int, input, 0 = None , 1 = Static Value , 2 = Relatif Value set in the file
		iType, int, output, Type to set
		fRangeMax, float, input, Range Max for the Dp

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unConfigGenericFunctions_GetDeadBandValue(string sDeadBand, float &fDeadBand, int iDeadBandType, int &iType, float fRangeMax, float fRangeMin) {
    dyn_string exceptionInfo;
    float fRange;

    unConfigGenericFunctions_checkFloat(sDeadBand, fDeadBand, exceptionInfo);

    if (dynlen(exceptionInfo) > 0) {
        fDeadBand = -1;
    } else {
        switch (iDeadBandType) {
            case UN_CONFIG_DEADBAND_VALUE:
                fRange = fRangeMax - fRangeMin;
                if (fRange != 0) {
                    fDeadBand = (fDeadBand * fRange) / 100;
                    iType = DPATTR_VALUE_SMOOTH;		//= 0
                } else {
                    fDeadBand = -1;
                }
                break;

            case UN_CONFIG_DEADBAND_RELATIF:
                iType = DPATTR_VALUE_REL_SMOOTH;		//= 7
                break;

            case UN_CONFIG_DEADBAND_NONE:
                fDeadBand = - 1;
                break;
            case UN_CONFIG_DEADBAND_OLD_NEW:
                fDeadBand = - 1;
                iType = DPATTR_COMPARE_OLD_NEW;  //=4
                break;
            default:
                fDeadBand = -1;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_check4LevelsAlert
/**
Purpose : check levels for AI/AO alarms according to the Normal Position

Parameters:
		fHH, float, input, HHL level
		fH, float, input, HL level
		fL, float, input, L level
		fLL, float, input, LL level
		iMode, int, input, valid levels ?
		bEr, bool, output, result

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_check4LevelsAlert(float fHH, float fH, float fL, float fLL, int iMode, bool &bEr) {
    int iMask_L, iMask_LL, iMask_H, iMask_HH;

    iMask_LL = 8;	//1000
    iMask_L = 4;	//100
    iMask_H = 2;	//10
    iMask_HH = 1; //1

    if ((iMode & iMask_LL) == 8) {
        if (fL <= fLL) {
            bEr = true;
            return;
        }
    }

    if ((iMode & iMask_HH) == 1) {
        if (fH >= fHH) {
            bEr = true;
            return;
        }
    }

    if ((iMode & iMask_L) == 4 && (iMode & iMask_H) == 2) {
        if (fL >= fH) {
            bEr = true;
            return;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_set4LevelsAlert
/**
Purpose : set levels for AI/AO alarms according to the Normal Position

Parameters:
		iMode, int, input, valid levels ?
		dsLevelsAlarm, dyn_string, output, Levels to set
		dsLevelsAlertsClass, dyn_string, output, Alert Class to set

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_set4LevelsAlert(int iMode, bool bSMS, dyn_string &dsLevelsAlarm, dyn_string & dsLevelsAlertsClass, dyn_float &dfLevelLimits) {
    int iMask_L, iMask_LL, iMask_H, iMask_HH;
    string sLL, sL, sH, sHH, sOk, sLLClass, sLClass, sHClass, sHHClass;
    dyn_float dfTemp;

    iMask_LL = 8;	//1000
    iMask_L = 4;	//100
    iMask_H = 2;	//10
    iMask_HH = 1; //1

    sLL = "LL";
    sL = "L";
    sH = "H";
    sHH = "HH";
    sOk = "Ok";

    if (bSMS) {
        sLLClass = UN_PROCESSALARM_ANALOG_ALERT_CLASS_LL;
        sLClass = UN_PROCESSALARM_ANALOG_ALERT_CLASS_L;
        sHClass = UN_PROCESSALARM_ANALOG_ALERT_CLASS_H;
        sHHClass = UN_PROCESSALARM_ANALOG_ALERT_CLASS_HH;
    } else {
        sLLClass = "_unAnalogLL.";
        sLClass = "_unAnalogL.";
        sHClass = "_unAnalogH.";
        sHHClass = "_unAnalogHH.";
    }

    dfTemp = dfLevelLimits;
    dynClear(dfLevelLimits);
    dynClear(dsLevelsAlarm);
    dynClear(dsLevelsAlertsClass);

    if ((iMode & iMask_LL) == 8) {
        dynAppend (dsLevelsAlarm, sLL);
        dynAppend (dsLevelsAlertsClass, sLLClass);
        dynAppend (dfLevelLimits, dfTemp[1]);
    }

    if ((iMode & iMask_L) == 4) {
        dynAppend (dsLevelsAlarm, sL);
        dynAppend (dsLevelsAlertsClass, sLClass);
        dynAppend (dfLevelLimits, dfTemp[2]);
    }

    dynAppend (dsLevelsAlarm, sOk);
    dynAppend (dsLevelsAlertsClass, "");

    if ((iMode & iMask_H) == 2) {
        dynAppend (dsLevelsAlarm, sH);
        dynAppend (dsLevelsAlertsClass, sHClass);
        dynAppend (dfLevelLimits, dfTemp[3]);
    }

    if ((iMode & iMask_HH) == 1) {
        dynAppend (dsLevelsAlarm, sHH);
        dynAppend (dsLevelsAlertsClass, sHHClass);
        dynAppend (dfLevelLimits, dfTemp[4]);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getUnicosAddressReference
/**

@author: Vincent Forest, Herve Milcent (LHC-IAS)
Creation Date: 20/11/2002
Modification History: None

Purpose: get formatted address for Unicos using given parameters

Parameters :
	- dsParameters, dyn_string, input, parameters used to build the address reference (see constants definition)
	- addressReference, string, output, address reference (empty string in case of error)

Usage: Public

PVSS manager usage: CTRL, NV, NG

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unConfigGenericFunctions_getUnicosAddressReference(dyn_string dsParameters, string& addressReference) {
    int mode, iPLCNumber, address, iNbEvent;
    string sPLCType;
    bool event;
    bool dataOk = true;
    string temp;

    // 1. Check input data
    if (dynlen(dsParameters) == 6) {	//UN_ADDRESS_PARAMETER_FIELD_NUMBER=5 ds fwPeriphAddress ???
        if (sscanf(dsParameters[UN_ADDRESS_PARAMETER_FIELD_MODE], "%d", mode) <= 0) {
            dataOk = false;
        }
        sPLCType = unConfigGenericFunctions_extractFeProtocol(dsParameters[UN_ADDRESS_PARAMETER_FIELD_PLCTYPE]);
        if ((sPLCType != UN_CONFIG_QUANTUM) && (sPLCType != UN_CONFIG_PREMIUM) && (sPLCType != UN_CONFIG_UNITY) && //legacy support
                (sPLCType != UN_CONFIG_PROTOCOL_MODBUS)) { //new type: the protocol
            dataOk = false;
        }
        if (sscanf(dsParameters[UN_ADDRESS_PARAMETER_FIELD_PLCNUMBER], "%d", iPLCNumber) <= 0) {
            dataOk = false;
        }
        temp = strtolower(dsParameters[UN_ADDRESS_PARAMETER_FIELD_EVENT]);
        if ((temp != "false") && (temp != "true") && (temp != "0") && (temp != "1")) {
            dataOk = false;
        }
        event = (bool)dsParameters[UN_ADDRESS_PARAMETER_FIELD_EVENT];
        if (sscanf(dsParameters[UN_ADDRESS_PARAMETER_FIELD_ADDRESS], "%d", address) <= 0) {
            dataOk = false;
        }
    } else {
        dataOk = false;
    }

    // 2. Build formatted address
    addressReference = "";
    if (dataOk) {
        switch	(mode) {
            // Input
            case DPATTR_ADDR_MODE_INPUT_SPONT:
            case DPATTR_ADDR_MODE_INPUT_SQUERY:
            case DPATTR_ADDR_MODE_INPUT_POLL:
                switch (sPLCType) {
                    case UN_CONFIG_PREMIUM:
                    case UN_CONFIG_UNITY:
                    case UN_CONFIG_PROTOCOL_MODBUS:
                        if (event) {
                            if (strtoupper(dsParameters[UN_ADDRESS_PARAMETER_FIELD_EVENT16]) == "TRUE") {
                                iNbEvent = UN_PREMIUM_INPUT_NB_EVENT;    //16 bits
                            } else {
                                iNbEvent = UN_PREMIUM_INPUT_NB_EVENT32;    //32 bits
                            }
                        } else {
                            if (mode == DPATTR_ADDR_MODE_INPUT_POLL) {
                                iNbEvent = 4;
                            } else {
                                iNbEvent = UN_PREMIUM_INPUT_NB_MISC;
                            }
                        }
                        string letter = mode == DPATTR_ADDR_MODE_INPUT_POLL ? "M" : UN_PREMIUM_INPUT_LETTER_EVENT;
                        addressReference = letter + "." + iPLCNumber + "." + iNbEvent + "." + address;
                        break;

                    case UN_CONFIG_QUANTUM:
                        if (event) {
                            if (strtoupper(dsParameters[UN_ADDRESS_PARAMETER_FIELD_EVENT16]) == "TRUE") {
                                iNbEvent = UN_QUANTUM_INPUT_NB_EVENT;    //16 bits
                            } else {
                                iNbEvent = UN_QUANTUM_INPUT_NB_EVENT32;    //32 bits
                            }
                        } else {
                            if (mode == DPATTR_ADDR_MODE_INPUT_POLL) {
                                iNbEvent = 4;
                            } else {
                                iNbEvent = UN_QUANTUM_INPUT_NB_MISC;
                            }
                        }
                        string letter = mode == DPATTR_ADDR_MODE_INPUT_POLL ? "M" : UN_QUANTUM_INPUT_LETTER_EVENT;
                        addressReference = letter + "." + iPLCNumber + "." + iNbEvent + "." + address;
                        break;

                    default:
                        break;
                }
                break;

            // Output
            case DPATTR_ADDR_MODE_OUTPUT:
            case DPATTR_ADDR_MODE_OUTPUT_SINGLE:
                addressReference = UN_PREMIUM_QUANTUM_OUTPUT_LETTER_ALL + "." +
                                   iPLCNumber + "." +
                                   UN_PREMIUM_QUANTUM_OUTPUT_NB_ALL + "." +
                                   address;
                break;

            // Unknown
            case DPATTR_ADDR_MODE_UNDEFINED:
            default:
                break;
        }
    }
}




//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setMaskEvent
/**
Purpose: mask/unmask the event

Parameters:
		sDpName: string, input, the device dpname
		iMask: integer, input, the event mask value
		exceptionInfo: dyn_string, output, the error is returned here

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setMaskEvent(string sDpName, int iMask, dyn_string dsDpe, dyn_string &exceptionInfo) {
    bool configExists, isActive;
    string archiveClass, sDpPLC;
    int i, len, archiveType, smoothProcedure;
    float deadband, timeInterval;
    string sApplication, sSystem, sDp, sArchive;
    dyn_string dsTemp, dsAppli, dsArchive;
    int iPos;

    //DebugN("unConfigGenericFunctions_setMaskEvent", sDpName, iMask);
    len = dynlen(dsDpe);
    for (i = 1; i <= len; i++) {
        // get the archive class and archive active if configured
        fwArchive_get(sDpName + dsDpe[i], configExists, archiveClass, archiveType, smoothProcedure, deadband, timeInterval, isActive, exceptionInfo);
        if (configExists) {
            if ((iMask == 0) && (isActive)) {
                // mask event && archive active
                dpSet(sDpName + dsDpe[i] + ":_archive.._archive" , false);
            } else if ((iMask == 1) && (!isActive)) {
                // unmask event && archive not active
                fwArchive_set(sDpName + dsDpe[i],
                              archiveClass,
                              DPATTR_ARCH_PROC_SIMPLESM,
                              DPATTR_COMPARE_OLD_NEW,
                              0,
                              0,
                              exceptionInfo);
            }
        } else {
            //create Archive Config only for evStsReg
            if (strpos(dsDpe[i], ".ProcessInput.evStsReg") >= 0) {
                //get Archive
                dsTemp = strsplit(sDpName, UN_DPNAME_SEPARATOR);
                if (dynlen(dsTemp) >= UN_DPNAME_FIELD_LENGTH) {
                    sSystem = unGenericDpFunctions_getSystemName(sDpName);
                    sApplication = dsTemp[UN_DPNAME_FIELD_PLCSUBAPPLICATION];
                    sDpPLC = unGenericObject_GetPLCNameFromDpName(sDpName);

                    if (sDpPLC != "") {
                        sDp = sSystem + sDpPLC + ".configuration.subApplications";
                        dpGet(sDp, dsAppli);
                        iPos = dynContains(dsAppli, sApplication);
                        if (iPos > 0) {
                            sDp = sSystem + sDpPLC + ".configuration.archive_event";
                            dpGet(sDp, dsArchive);
                            if (dynlen(dsArchive) >= iPos) {
                                sArchive = dsArchive[iPos];
                            }
                        }
                    }
                }

                if (sArchive != "") {
                    fwArchive_set(sDpName + dsDpe[i],
                                  sArchive,
                                  DPATTR_ARCH_PROC_SIMPLESM,
                                  DPATTR_COMPARE_OLD_NEW,
                                  0,
                                  0,
                                  exceptionInfo);

                    configExists = false;
                    fwArchive_get(sDpName + dsDpe[i], configExists, archiveClass, archiveType, smoothProcedure, deadband, timeInterval, isActive, exceptionInfo);

                    if (configExists) {
                        unConfigGenericFunctions_setMaskEvent(sDpName, iMask, dsDpe, exceptionInfo);
                    } else {
                        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setMaskEvent: archive config not created", "");
                    }
                }
            }
        }
    }

    dpSet(sDpName + ".eventMask", iMask);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkDeviceConfig
/**
Purpose: check data before configuration

Parameters:
		configLine: dyn_string, input, configs (see dyn_string indexes in constants defined)
		sDeviceType: string, input, the device type
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkDeviceConfig(dyn_string configLine, string sDeviceType, dyn_string &exceptionInfo) {
    string sFrontEndType, sFunction;
    dyn_string exceptionInfoTemp;
    dyn_string dsDollar;
    bool bDollar = true;

    // Takes into account Schneider and Siemens PLC's. (_unPLC, S7_PLC)
    if (isFunctionDefined("shapeExists")) {
        if (shapeExists("TextFieldPlcType")) {
            sFrontEndType = TextFieldPlcType.text;
            bDollar = false;
        }
    }
    if (bDollar) {
        sFrontEndType = $sFrontEndType;
        dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
    }
    sFrontEndType = unConfigGenericFunctions_convertPlcTypeToLegacyType(unConfigGenericFunctions_extractFeProtocol(sFrontEndType));

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkDeviceConfig", sFrontEndType, sDeviceType, dsDollar, "-");
    if ((sFrontEndType == UN_CONFIG_PREMIUM) || (sFrontEndType == UN_CONFIG_QUANTUM) || (sFrontEndType == UN_CONFIG_UNITY)) {
        sFrontEndType = UN_PLC_DPTYPE;
    } else if ((sFrontEndType == UN_CONFIG_S7_400) || (sFrontEndType == UN_CONFIG_S7_300)) {
        sFrontEndType = S7_PLC_DPTYPE;
    }

    sFunction = sFrontEndType + "_" + sDeviceType + UN_CONFIG_DEVICE_CHECKFUNCTIONSUFFIX;
    // DebugN("unConfigGenericFunctions_checkDeviceConfig - sFunction:",sFunction, "configLine",  configLine);
    evalScript(exceptionInfoTemp, "dyn_string main(dyn_string configLine) {" +
               "dyn_string exceptionInfo;" +
               "if (isFunctionDefined(\"" + sFunction + "\"))" +
               "    {" +
               "    " + sFunction + "(configLine, exceptionInfo);" +
               "    }" +
               "else " +
               "    {" +
               "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNCHECK\"),\"\");" +
               "    }" +
               "return exceptionInfo; }", dsDollar,
               configLine);
    dynAppend(exceptionInfo, exceptionInfoTemp);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setDeviceConfig
/**
Purpose: set data configuration

Parameters:
		configLine: dyn_string, input, configs (see dyn_string indexes in constants defined)
		sDeviceType: string, input, the device type
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setDeviceConfig(dyn_string configLine, string sDeviceType, dyn_string &exceptionInfo) {
    string sFrontEndType, sFunction;
    dyn_string exceptionInfoTemp;
    dyn_string dsDollar;
    bool bDollar = true;

    // Takes into account Schneider and Siemens PLC's. (_unPLC, S7_PLC)
    if (isFunctionDefined("shapeExists")) {
        if (shapeExists("TextFieldPlcType")) {
            sFrontEndType = unConfigGenericFunctions_extractFeProtocol(TextFieldPlcType.text);
            bDollar = false;
        }
    }
    if (bDollar) {
        sFrontEndType = unConfigGenericFunctions_extractFeProtocol($sFrontEndType);
        dsDollar = makeDynString("$sFrontEndType:" + $sFrontEndType, "$dsArchiveList:" + $dsArchiveList, "$sFrontEndVersion:" + $sFrontEndVersion, "$bCheckBeforeImport:" + $bCheckBeforeImport, "$sFrontEndDeviceType:" + $sFrontEndDeviceType);
    }
    if (isDollarDefined("$bIgnoreDriverRelatedConfigs")) {
        dynAppend(dsDollar, "$bIgnoreDriverRelatedConfigs:" + $bIgnoreDriverRelatedConfigs);
    }

    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_setDeviceConfig", sFrontEndType, sDeviceType, dsDollar, "-");
    if ((sFrontEndType == UN_CONFIG_PREMIUM) || (sFrontEndType == UN_CONFIG_QUANTUM) || (sFrontEndType == UN_CONFIG_UNITY) ||
            sFrontEndType == UN_CONFIG_PROTOCOL_MODBUS) {
        sFrontEndType = UN_PLC_DPTYPE;
    } else if ((sFrontEndType == UN_CONFIG_S7_400) || (sFrontEndType == UN_CONFIG_S7_300) ||
               sFrontEndType == UN_CONFIG_PROTOCOL_S7) {
        sFrontEndType = S7_PLC_DPTYPE;
    }

    sFunction = sFrontEndType + "_" + sDeviceType + UN_CONFIG_DEVICE_SETFUNCTIONSUFFIX;
// DebugN("unConfigGenericFunctions_setDeviceConfig - sFunction:",	sFunction);
    evalScript(exceptionInfoTemp, "dyn_string main(dyn_string configLine) {" +
               "dyn_string exceptionInfo;" +
               "if (isFunctionDefined(\"" + sFunction + "\"))" +
               "    {" +
               "    " + sFunction + "(configLine, exceptionInfo);" +
               "    }" +
               "else " +
               "    {" +
               "    fwException_raise(exceptionInfo,\"ERROR\",\"" + sFunction + "\" + getCatStr(\"unGeneration\",\"UNKNOWNSET\"),\"\");" +
               "    }" +
               "return exceptionInfo; }", dsDollar,
               configLine);
    dynAppend(exceptionInfo, exceptionInfoTemp);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setCommonAllRegisters
/**
Purpose : set evstsreg, alarm bit common config

Parameters:
		dsConfigs, dyn_string, input, full config line
		dsStsRegNames, dyn_string, input, names of available StsReg (1 StsReg => 1 evStsReg)
		dsAlarmBitNames, dyn_string, input, names of available alarm bit
											like this : makeDynString("bit1StsReg1;bit2StsReg1", "bit1StsReg2;bit2StsReg2;bit3StsReg2");
		dsAlarmBitPositions, dyn_string, input, bit positions in StsReg (same format than dsAlarmBitNames)
		iNormalPosition, int, input, normal position of alarm bits (index in dsConfigs)

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setCommonAllRegisters(dyn_string dsConfigs,
        dyn_string dsStsRegNames,
        dyn_string dsAlarmBitNames, dyn_string dsAlarmBitPositions, int iNormalPosition,
        dyn_string &exceptionInfo) {
    string sDpName, bitDescription, sDeviceAlias, sDeviceLinks;
    int i, j, length, length2;
    dyn_string currentAlarmBitNames, exceptionInfoTemp, dsbitDescription;
    dyn_int currentAlarmBitPositions;
    bool bOkRange, alarmExists, alarmActive, bSMS;
    int iLevel;
    string sDomain, sDomainConfig, sNature, sNatureConfig;
    dyn_string ds;
    /*
    	DebugN("FUNCTION: unConfigGenericFunctions_setCommonAllRegisters(dsConfigs= "+dsConfigs+
    																																	" dsStsRegNames= "+dsStsRegNames+
    																																	" dsAlarmBitNames= "+dsAlarmBitNames+
    																																	" dsAlarmBitPositions= "+dsAlarmBitPositions+
    																																	" iNormalPosition= "+iNormalPosition+" )");
    */

    // Check

    // 1. General parameters
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];

    // 2. Input
    length = dynlen(dsStsRegNames);
    for (i = 1; i <= length; i++) {
        // 2.1. evStsReg

// set archive:
        fwArchive_set(sDpName + ".ProcessInput.ev" + dsStsRegNames[i],
                      dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_EVENT],
                      DPATTR_ARCH_PROC_SIMPLESM,
                      DPATTR_COMPARE_OLD_NEW,
                      0,
                      0,
                      exceptionInfo);

        // 2.3. Alarm bit on StsReg
        if ((i <= dynlen(dsAlarmBitNames)) && (i <= dynlen(dsAlarmBitPositions))) {
            currentAlarmBitNames = strsplit(dsAlarmBitNames[i], ";");
            currentAlarmBitPositions = strsplit(dsAlarmBitPositions[i], ";");
            length2 = dynlen(currentAlarmBitNames);
            if (dynlen(currentAlarmBitPositions) < length2) {
                length2 = dynlen(currentAlarmBitPositions);
            }
            for (j = 1; j <= length2; j++) {
                unGenericDpFunctions_getBitDescription(dsConfigs[UN_CONFIG_OBJECTGENERAL_OBJECT], dsStsRegNames[i], currentAlarmBitPositions[j], bitDescription);
                if (bitDescription == "") {		// Bit description was not found
                    bitDescription = currentAlarmBitNames[j];
                }

                dynClear(dsbitDescription);

                sDomainConfig = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DOMAIN];
                ds = strsplit(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
                if (dynlen(ds) == UN_CONFIG_UNICOS_DOMAIN_AND_ACCESS_CONTROL_LEN) { // access control + domain present
                    sDomain = ds[2];
                } else if (strpos(sDomainConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR) >= 0) { // if UN_ACCESS_CONTROL_DOMAIN_SEPARATOR in sDomain config --> domain = ""
                    sDomain = "";
                } else {
                    sDomain = sDomainConfig;
                }

                sNatureConfig = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_NATURE];
                ds = strsplit(sNatureConfig, UN_ACCESS_CONTROL_DOMAIN_SEPARATOR);
                // if no ACC priv assume list of nature
                if (dynlen(ds) >= (UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN - 1)) {
                    if (dynlen(ds) == UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN) {
                        sNature = ds[4];
                    } else { // dynlen = UN_CONFIG_UNICOS_NATURE_AND_PRIV_LEN -1 --> consider nature = ""
                        sNature = "";
                    }
                } else {
                    sNature = sNatureConfig;
                }

                dsbitDescription [UN_CONFIG_ALARM_DOMAIN] = sDomain;
                dsbitDescription [UN_CONFIG_ALARM_NATURE] = sNature;
// alias is deviceName only
                unConfigGenericFunctions_getAliasLink(dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS],
                                                      sDeviceAlias, sDeviceLinks);
                dsbitDescription [UN_CONFIG_ALARM_ALIAS] = sDeviceAlias;
                dsbitDescription [UN_CONFIG_ALARM_DESCRIPTION] = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION];
                dsbitDescription [UN_CONFIG_ALARM_NAME] = bitDescription;
                dsbitDescription [UN_CONFIG_ALARM_DEFAULTPANEL] = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DEFAULTPANEL];

// set alarm
                unConfigGenericFunctions_getNormalPosition(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + iNormalPosition], alarmExists, alarmActive, bOkRange, iLevel, bSMS);
                if (alarmExists) {
                    unConfigGenericFunctions_setDigitalAlert(sDpName + ".ProcessInput." + currentAlarmBitNames[j], dsbitDescription, bOkRange, alarmActive,  bSMS, exceptionInfo);
                } else {
                    fwAlertConfig_deleteDigital(sDpName + ".ProcessInput." + currentAlarmBitNames[j], exceptionInfo);
                    // even if the alarm is deleted set the description
                    unConfigGenericFunctions_setAlarmDescription(sDpName + ".ProcessInput." + currentAlarmBitNames[j],
                            dsbitDescription, exceptionInfo);	// Set description for alert screen
                }
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setCommonFloatIntBlock
/**
Purpose : set a block of float or int dpe common config

Parameters:
		dsConfigs, dyn_string, input, full config line
		dataType, int, input, float or int
		dsInputDps, dyn_string, input, names of available Dps (in ProcessInput)
		diInputAddresses, dyn_int, input, index of addresses in dsConfigs
		dsOutputDps, dyn_string, input, names of available Dps (in ProcessOutput)
		diOutputAddresses, dyn_int, input, index of addresses in dsConfigs
		diCommonData, dyn_int, input, index of common data (unit format deadband ...)
			4 parameters : 1-unit, 2-format 3-deadband 4-daedband type
		diRange, dyn_int, string, index of range min max, empty dyn_int = no range (pv_range config is set on first dpname of dsInputDps)
			2 parameters : 1-range min 2-range max
		dsArchivedDps, dyn_string, input, list of archived names
		diArchiveConfig, dyn_int, input, index of archive config (archive active and time filter)
			2 parameters : 1-active 2-time filter

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setCommonFloatIntBlock(dyn_string dsConfigs, int dataType, dyn_string dsInputDps,
        dyn_int diCommonData, dyn_int diRange,
        dyn_string dsArchivedDps, dyn_int diArchiveConfig, dyn_string &exceptionInfo) {
    int i, length, deadbandType, Type;
    string unit, format, sDpName, sArchive, sRangeMin, sRangeMax;
    bool archiveActive;
    float archiveTimeFilter, deadband, fDeadBand, fMin, fMax, fReturnDeadBand;
    dyn_string exceptionInfoTemp;
    int iArchiveType, iSmoothProcedure;

    // Check

    // Initialization
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    if (dynlen(diCommonData) >= 4) {
        unit = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diCommonData[1]];
        format = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diCommonData[2]];
        deadband = (float)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diCommonData[3]];
        deadbandType = (int)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diCommonData[4]];
    } else {
        unit = "";
        format = "";
        deadband = 0;
        deadbandType = 0;
    }

    // 1. Input common configs
    length = dynlen(dsInputDps);
    for (i = 1; i <= length; i++) {
        unConfigGenericFunctions_setUnit(sDpName + ".ProcessInput." + dsInputDps[i], unit, exceptionInfoTemp);
        unConfigGenericFunctions_setFormat(sDpName + ".ProcessInput." + dsInputDps[i], format, exceptionInfoTemp);
    }

    // 2. Output common config
    dynAppend(exceptionInfo, exceptionInfoTemp);
    dynClear(exceptionInfoTemp);

    // 3. _pv_range
    dynAppend(exceptionInfo, exceptionInfoTemp);
    dynClear(exceptionInfoTemp);

    if (dynlen(dsInputDps) > 0) {
        if (dynlen(diRange) >= 2) {	// Range
            //Format values
            sRangeMin = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diRange[1]];
            sRangeMax = dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diRange[2]];

            fMin = (float)sRangeMin;
            fMax = (float)sRangeMax;

            if (fMin != fMax) { // if min!= max --> create pv_range config
                unConfigGenericFunctions_ScientificFormat(sRangeMin);
                unConfigGenericFunctions_ScientificFormat(sRangeMax);

                fwPvRange_set(sDpName + ".ProcessInput." + dsInputDps[1],
                              sRangeMin,
                              sRangeMax,
                              UN_CONFIG_DEFAULT_RANGE_NEGATE_RANGE,
                              UN_CONFIG_DEFAULT_RANGE_IGNORE_OUTSIDE,
                              UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MIN,
                              UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MAX,
                              exceptionInfoTemp);
            } else { // if min == max --> delete pv_range
                fwPvRange_delete(sDpName + ".ProcessInput." + dsInputDps[1], exceptionInfoTemp);
            }
        } else {						// No Range
            fwPvRange_delete(sDpName + ".ProcessInput." + dsInputDps[1], exceptionInfoTemp);
        }
    }

    // 4. _archive . For the moment, datapoints in dsArchivedDps are supposed to be in "ProcessInput" part
    dynAppend(exceptionInfo, exceptionInfoTemp);
    dynClear(exceptionInfoTemp);

    if (dynlen(diArchiveConfig) >= 2) {
        archiveTimeFilter = (float)dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diArchiveConfig[2]];
        sArchive = dsConfigs[UN_CONFIG_OBJECTGENERAL_ARCHIVE_ANALOG];
        unConfigGenericFunctions_getArchiving(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diArchiveConfig[1]], archiveActive,
                                              archiveTimeFilter, iArchiveType, iSmoothProcedure);

        unConfigGenericFunctions_getArchivingDeadband(dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diArchiveConfig[1]], fReturnDeadBand);
    } else {
        archiveActive = false;
    }
    length = dynlen(dsArchivedDps);
    for (i = 1; i <= length; i++) {
//DebugN("unConfigGenericFunctions_setCommonFloatIntBlock", archiveActive, sDpName + ".ProcessInput." + dsArchivedDps[i]);
        if (archiveActive) {		// Archive
            fwArchive_set(sDpName + ".ProcessInput." + dsArchivedDps[i],
                          sArchive,
                          iArchiveType,
                          iSmoothProcedure,
                          fReturnDeadBand,
                          archiveTimeFilter,
                          exceptionInfoTemp);
        } else {					// No Archive
            fwArchive_delete(sDpName + ".ProcessInput." + dsArchivedDps[i], exceptionInfoTemp);
        }
    }

    // 5. Smoothing
    dynAppend(exceptionInfo, exceptionInfoTemp);
    dynClear(exceptionInfoTemp);

    length = dynlen(dsInputDps);
    for (i = 1; i <= length; i++) {
        unConfigGenericFunctions_GetDeadBandValue(deadband, fDeadBand, deadbandType, Type, sRangeMax, sRangeMin);
        if (fDeadBand > 0 && (deadbandType == "1" || deadbandType == "2")) {
            fwSmoothing_set(sDpName + ".ProcessInput." + dsInputDps[i], Type, fDeadBand, UN_CONFIG_DEFAULT_SMOOTHING_TIME_FILTER, exceptionInfoTemp);
        } else if (deadbandType == UN_CONFIG_DEADBAND_OLD_NEW) {
            fwSmoothing_set(sDpName + ".ProcessInput." + dsInputDps[i], Type, fDeadBand, UN_CONFIG_DEFAULT_SMOOTHING_TIME_FILTER, exceptionInfoTemp);
        } else {
            fwSmoothing_delete(sDpName + ".ProcessInput." + dsInputDps[i], exceptionInfoTemp);
        }
    }

    dynAppend(exceptionInfo, exceptionInfoTemp);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkCommonFloatIntBlock
/**
Purpose : check a block of float or int dpe common configs

Parameters:
		dsConfigs, dyn_string, input, full config line
		convertIndex, int, input, constant to add to index to obtain an absolute index
		diInputAddresses, dyn_int, input, index of addresses in dsConfigs
		diOutputAddresses, dyn_int, input, index of addresses in dsConfigs
		diCommonData, dyn_int, input, index of common data (unit format deadband ...)
			4 parameters : 1-unit, 2-format 3-deadband 4-daedband type
		diRange, dyn_int, string, index of range min max, empty dyn_int = no range (pv_range config is set on first dpname of dsInputDps)
			2 parameters : 1-range min 2-range max
		diArchiveConfig, dyn_int, input, index of archive config (archive active and time filter)
			2 parameters : 1-active 2-time filter

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. convertIndex must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkCommonFloatIntBlock(dyn_string dsConfigs, int convertIndex,
        dyn_int diCommonData, dyn_int diRange, dyn_int diArchiveConfig, dyn_string &exceptionInfo) {
    int i, length;
    dyn_string dsAddresses;

    // 2. Common data
    if (dynlen(diCommonData) >= 4) {
        unConfigGenericFunctions_checkUnitFormat(dsConfigs[convertIndex + diCommonData[1]],
                dsConfigs[convertIndex + diCommonData[2]], exceptionInfo);
        unConfigGenericFunctions_checkDeadBand(dsConfigs[convertIndex + diCommonData[3]],
                                               dsConfigs[convertIndex +  diCommonData[4]], exceptionInfo);
    }
    // 3. Range
    if (dynlen(diRange) >= 2) {
        unConfigGenericFunctions_checkRange(dsConfigs[convertIndex + diRange[1]], dsConfigs[convertIndex + diRange[2]], exceptionInfo);
    }
    // 4. Archive
    if (dynlen(diArchiveConfig) >= 2) {
        unConfigGenericFunctions_checkArchiveParameters(dsConfigs[convertIndex + diArchiveConfig[2]],
                dsConfigs[convertIndex + diArchiveConfig[1]], exceptionInfo);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkFloatIntBlockAddress
/**
Purpose : check a block of float or int dpe common configs

Parameters:
		dsConfigs, dyn_string, input, full config line
		convertIndex, int, input, constant to add to index to obtain an absolute index
		diInputAddresses, dyn_int, input, index of addresses in dsConfigs
		diOutputAddresses, dyn_int, input, index of addresses in dsConfigs

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. convertIndex must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkFloatIntBlockAddress(dyn_string dsConfigs, int convertIndex, dyn_int diInputAddresses,
        dyn_int diOutputAddresses, dyn_string &exceptionInfo) {
    int i, length;
    dyn_string dsAddresses;

    // 1. Addresses
    length = dynlen(diInputAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diInputAddresses[i]]);
    }
    length = dynlen(diOutputAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diOutputAddresses[i]]);
    }
    unConfigGenericFunctions_checkAddresses(dsAddresses, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkArchive
/**
Purpose : checks if the archive name are correct

Parameters:
		sBoolArchive, string, input, boolean archive
		sAnalogArchive, string, input, boolean archive
		sEventArchive, string, input, boolean archive

		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkArchive(string sBoolArchive, string sAnalogArchive, string sEventArchive, dyn_string &exceptionInfo) {
    dyn_string dsArchive;
    bool bOkBool = true, bOkAnalog = true, bOkEvent = true, bDollar = true;

    if (isFunctionDefined("shapeExists")) {
        if (shapeExists("boolArchive")) {
            dsArchive = boolArchive.items;
            bDollar = false;
        }
    }
    if (bDollar) {
        dsArchive = strsplit($dsArchiveList, ";");
    }

    if (sBoolArchive != "") {
        if (dynContains(dsArchive, sBoolArchive) > 0) {
            bOkBool = true;
        } else {
            bOkBool = false;
        }
    }

    if (sAnalogArchive != "") {
        if (dynContains(dsArchive, sAnalogArchive) > 0) {
            bOkAnalog = true;
        } else {
            bOkAnalog = false;
        }
    }
    if (sEventArchive != "") {
        if (dynContains(dsArchive, sEventArchive) > 0) {
            bOkEvent = true;
        } else {
            bOkEvent = false;
        }
    }

    if (!bOkBool || !bOkAnalog || !bOkEvent) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkArchive: wrong archive name (" + sBoolArchive + "=" + bOkBool + "), (" + sAnalogArchive + "=" + bOkAnalog + "), (" + sEventArchive + "=" + bOkEvent + ")", "");
    }
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_checkArchive", sBoolArchive, sAnalogArchive, sEventArchive, dsArchive, "-");
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_getFrontEndArchiveDp
/**
Purpose: return the list of Dps that wil be archived during the set of the _UnPlc front-end

Parameters:
		sCommand: string, input, the current entry in the import file
		sDp: string, input, the given dp: PLC or SystemAlarm  Dp
		sFEType: string, input, the front-end type
		dsArchiveDp: dyn_string, output, list of archivedDp

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-07-25 @whitelisted{Callback}

*/

_UnPlc_getFrontEndArchiveDp(string sCommand, string sDp, string sFEType, dyn_string &dsArchiveDp) {
//DebugN(sCommand, sDp, sFEType);
    switch (sCommand) {
        case UN_PLC_COMMAND:
            dsArchiveDp = makeDynString();
            break;
        case UN_PLC_COMMAND_EXTENDED:
            dsArchiveDp = makeDynString(getSystemName() + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sDp + ".alarm",
                                        getSystemName() + c_unSystemAlarm_dpPattern + PLC_DS_Time_pattern + sDp + ".alarm",
                                        getSystemName() + c_unSystemAlarm_dpPattern + PLC_DS_Error_pattern + sDp + ".alarm");
            break;
        case c_unSystemAlarm_dpType:
            dsArchiveDp = makeDynString(sDp + ".alarm");
            break;
        case UN_FESYSTEMALARM_COMMAND:
            dsArchiveDp = makeDynString(sDp + ".alarm");
            break;
        default:
            dsArchiveDp = makeDynString();
            break;
    }
}

//------------------------------------------------------------------------------------------------------------------------
//	SIEMENS S7 PLC UNICOS
//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkAddressS7
/**
Purpose: check addresses

Parameters:
		dsAddresses, 		input, 	dyn_string, dyn_string to be checked
		dataType, 			input,	int, dataType of address (defined in S7Config_PLC.ctl)
		exceptionInfo, 	output, dyn_string, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: win XP.
	. distributed system: yes.
*/

unConfigGenericFunctions_checkAddressS7(dyn_string dsAddresses, int dataType, dyn_string &exceptionInfo, bool allowEmptyAddress = false) {
    if (allowEmptyAddress) {
        if (dynlen(dsAddresses) == 1 && dsAddresses[1] == "") return;
    }
    int i, length;

    // Support variables for scanning the format
    string s1, s2, s3;
    string s1_ok, s2_ok, s3_ok;
    int i1, i2, i3;

    // DebugN("FUNCTION: unConfigGenericFunctions_checkAddressS7(dsAddresses= "+dsAddresses+" dataType= "+dataType+")");

    s1_ok = "DB";
    switch (dataType) {
        case UN_CONFIG_S7_PLC_DATATYPE_WORD:		// 16 bits
            s2_ok = "." + UN_S7_FORMAT_WORD;
            s3_ok = "";
            break;
        case UN_CONFIG_S7_PLC_DATATYPE_DWORD:		// 32 bits
            s2_ok = "." + UN_S7_FORMAT_DOUBLE;
            s3_ok = "";
            break;
        case UN_CONFIG_S7_PLC_DATATYPE_FLOAT:		// 32 bits float number
            s2_ok = "." + UN_S7_FORMAT_DOUBLE;
            s3_ok = "F";
            break;
        case UN_CONFIG_S7_PLC_DATATYPE_BOOL:		// 1 bit
            s2_ok = "." + UN_S7_FORMAT_BIT;
            s3_ok = ".";
            break;
        case UN_CONFIG_S7_PLC_DATATYPE_BYTE:		// 8 bits
            s2_ok = "." + UN_S7_FORMAT_BYTE;
            s3_ok = "";
            break;
    }

    length = dynlen(dsAddresses);
    // For all addresses (max. 2)
    for (i = 1; i <= length; i++) {
        s1 = "";
        s2 = "";
        s3 = "";
        sscanf(dsAddresses[i], "%[^0-9]%d%[^0-9]%d%[^0-9]%d", s1, i1, s2, i2, s3, i3);
        if ((s1 != s1_ok) || (i1 < 1 || i1 > 32767) || (s2 != s2_ok) || (i2 < 0 || i2 > 65535) || (s3 != s3_ok) || (i3 < 0 || i3 > 7)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_checkAddressS7:" + dsAddresses[i] + getCatStr("unGeneration", "BADDATA"), "");

            // DebugN("check: s1= "+s1+ " i1= "+i1+" s2= "+s2+" i2= "+i2+" s3= "+s3+" i3= "+i3);
            // DebugN("check: s1_ok= "+s1_ok+" s2_ok= "+s2_ok+" s3_ok= "+s3_ok);
            return;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setFloatIntBlockS7
/**
Purpose : set a block of float or int dpe config

Parameters:
		dsConfigs, 				dyn_string, input, full config line
		dataType, 				int, 				input, float or int
		dsInputDps, 			dyn_string, input, names of available Dps (in ProcessInput)
		diInputAddresses, dyn_int, 		input, index of addresses in dsConfigs
		dsOutputDps, 			dyn_string, input, names of available Dps (in ProcessOutput)
		diOutputAddresses, dyn_int, 	input, index of addresses in dsConfigs
		diCommonData, 		dyn_int, 		input, index of common data (unit format deadband ...)
			4 parameters : 1-unit, 2-format 3-deadband 4-daedband type
		diRange, 					dyn_int, 	string, index of range min max, empty dyn_int = no range (pv_range config is set on first dpname of dsInputDps)
			2 parameters : 1-range min 2-range max
		dsArchivedDps, 		dyn_string, input, list of archived names
		diArchiveConfig, 	dyn_int, 		input, index of archive config (archive active and time filter)
			2 parameters : 1-active 2-time filter

		exceptionInfo, dyn_string, 		output, for errors

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setFloatIntBlockS7(dyn_string dsConfigs, int dataType, dyn_string dsInputDps, dyn_int diInputAddresses,
        dyn_string dsOutputDps, dyn_int diOutputAddresses, dyn_int diCommonData, dyn_int diRange,
        dyn_string dsArchivedDps, dyn_int diArchiveConfig, dyn_string &exceptionInfo) {
    int i, length, iPLCNumber, driverNum, Type;
    string addressReference, sDpName, sPLCType;
    dyn_string exceptionInfoTemp;

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_setFloatIntBlockS7(dsConfigs= "+dsConfigs+
    																															" dataType= "+dataType+
    																															" dsInputDps= "+dsInputDps+
    																															" diInputAddresses= "+diInputAddresses+
    																															" dsOutputDps= "+dsOutputDps+
    																															" diOutputAddresses= "+diOutputAddresses+
    																															" diCommonData= "+diCommonData+
    																															" diRange= "+diRange+
    																															" dsArchivedDps= "+dsArchivedDps+
    																															" diArchiveConfig= "+diArchiveConfig+" )");
    */

    // Initialization
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    driverNum = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER];
    iPLCNumber = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER];
    sPLCType = dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE];

    // 1. Input address configs
    length = dynlen(dsInputDps);
    for (i = 1; i <= length; i++) {
        // PostSt and HFStOrAuPosRSt address
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(
                    DPATTR_ADDR_MODE_INPUT_SPONT,
                    dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diInputAddresses[i]]),
                addressReference);

        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".ProcessInput." + dsInputDps[i],
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              driverNum,
                                              addressReference,
                                              DPATTR_ADDR_MODE_INPUT_SPONT,
                                              dataType,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_INPUT,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              ""),
                                exceptionInfoTemp);
        } else {
            fwException_raise(exceptionInfoTemp, "ERROR", "unConfigGenericFunctions_setFloatIntBlock: " + sDpName + ".ProcessInput." + dsInputDps[i] + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
    }

    // 2. Output addess config
    dynAppend(exceptionInfo, exceptionInfoTemp);
    dynClear(exceptionInfoTemp);

    length = dynlen(dsOutputDps);
    for (i = 1; i <= length; i++) {
        // MPosR address
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(
                    DPATTR_ADDR_MODE_OUTPUT,
                    dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diOutputAddresses[i]]),
                addressReference);

        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".ProcessOutput." + dsOutputDps[i],
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              driverNum,
                                              addressReference,
                                              DPATTR_ADDR_MODE_OUTPUT,
                                              dataType,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_OUTPUT,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              ""),
                                exceptionInfoTemp);
        } else {
            fwException_raise(exceptionInfoTemp, "ERROR", "unConfigGenericFunctions_setFloatIntBlockS7: " + sDpName + ".ProcessInput." + dsOutputDps[i] + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
    }

    dynClear(exceptionInfoTemp);

    // 3. set comon config: archive, smoothing, pv_range, etc.
    unConfigGenericFunctions_setCommonFloatIntBlock(dsConfigs, dataType, dsInputDps,
            diCommonData, diRange,
            dsArchivedDps, diArchiveConfig, exceptionInfoTemp);

    dynAppend(exceptionInfo, exceptionInfoTemp);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getUnicosAddressReferenceS7
/**

@author: Enrique Blanco
Creation Date: 05/06/05
Modification History: None

Purpose: get formatted address for Unicos using given parameters

Parameters :
	- dsParameters, dyn_string, input, Address_mode (TSPP, Polling), PLCname, config address (i.e.: DB10.DBD12)
	- addressReference, string, output, address reference (empty string in case of error)

Usage: Public

PVSS manager usage: CTRL, NV, NG

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP
	. distributed system: yes but not tested.
*/

unConfigGenericFunctions_getUnicosAddressReferenceS7(dyn_string dsParameters, string& addressReference) {
    int mode;
    string sConnectionName;
    string sPartialAddress;

    bool dataOk = true;
    dyn_string temp;

    // DebugN("FUNCTION: unConfigGenericFunctions_getUnicosAddressReferenceS7(dsParameters= "+dsParameters+" )");

    // 1. Check input data
    if (dynlen(dsParameters) != 3) {	//UN_ADDRESS_PARAMETER_FIELD_NUMBER=5 ds fwPeriphAddress ???
        // error
        DebugN("Error in function: unConfigGenericFunctions_getUnicosAddressReferenceS7 (bad parameter number=" + dynlen(dsParameters));
        return;
    }

    // Parameters
    mode = (int)dsParameters[1];						// i.e.: DPATTR_ADDR_MODE_INPUT_SPONT
    sConnectionName = dsParameters[2];	// i.e.: PLC_XX
    sPartialAddress = dsParameters[3];	// i.e.: DB10.DBW78
    if (sPartialAddress == "") {
        addressReference = "";
        return;
    }

    // Connection name (i.e.: S7_plcname or S7_plcname_POLL
    if (mode == DPATTR_ADDR_MODE_INPUT_POLL || mode == DPATTR_ADDR_MODE_OUTPUT) {
        sConnectionName = sConnectionName + S7_POLLING_COMMUNICATION;
    }

    // Absolute address reference
    addressReference = sConnectionName + "." + sPartialAddress;
}

//-----------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setevStsRegAddressS7
/**
Purpose: set evStsReg address config

Parameters:
		sDpNameevStsReg, 	string, 		input, 	evStsReg name
		driverNum, 				int, 				input, 	driver number
		addressReference, string, 		input, 	reference address for _address config
		exceptionInfo, 		dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

unConfigGenericFunctions_setevStsRegAddressS7(string sDpNameevStsReg, int driverNum, string addressReference, bool bEvent16, dyn_string &exceptionInfo) {
    int iPVSS_S7_Uint;

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_setevStsRegAddressS7(sDpNameevStsReg="+sDpNameevStsReg+" driverNum="+driverNum+
    																																" addressReference="+addressReference+" bEvent16="+bEvent16);
    */

    if (addressReference != "") {
        if (bEvent16) {
            iPVSS_S7_Uint = PVSS_S7_UINT16;
        } else {
            iPVSS_S7_Uint = PVSS_S7_INT32;
        }

        fwPeriphAddress_set(sDpNameevStsReg,
                            makeDynString(fwPeriphAddress_TYPE_S7,
                                          driverNum,
                                          addressReference, 	// PLC_name +"."+ address_specs (if polling -> PLC_name=PLC_name+Sufix_polling)
                                          DPATTR_ADDR_MODE_INPUT_SPONT,
                                          iPVSS_S7_Uint,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_EVSTSREG,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setevStsRegAddressS7: " + sDpNameevStsReg + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setStsRegAddressS7
/**
Purpose: set the StsReg config

Parameters:
		sDpNameStsReg, 		string, 		input, 	StsReg name
		driverNum, 				int, 				input, 	driver number
		addressReference, string, 		input, 	reference address for _address config
		exceptionInfo, 		dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

unConfigGenericFunctions_setStsRegAddressS7(string sDpNameStsReg, int driverNum, string addressReference, dyn_string &exceptionInfo) {

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_setStsRegAddressS7(sDpNameStsReg="+sDpNameStsReg+" driverNum="+driverNum+
    																																" addressReference="+addressReference);

    */

    if (addressReference != "") {
        fwPeriphAddress_set(sDpNameStsReg,
                            makeDynString(fwPeriphAddress_TYPE_S7,
                                          driverNum,
                                          addressReference,
                                          DPATTR_ADDR_MODE_INPUT_SPONT,
                                          PVSS_S7_UINT16,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_STSREG,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setStsRegAddressS7: " + sDpNameStsReg + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setStsRegAlarmBitAddressS7
/**
Purpose: set config for an alarm bit from StsReg
				 In the case of S7 the bit must be concatenated to the addressreference and change the way it's called.
				 i.e.:  StsReg01 address reference = DB10.DBW20
				 				bit position within the stsReg01 = 2
				 				NEW addressreference:  DB10.DBX20.2

Parameters:
		sDpNameBit, 			string, input, dpe
		driverNum, 				int, 		input, driver number
		addressReference, string, input, reference address for _address config
		iBitNumber, 			int, 		input, bit position in StsReg

		exceptionInfo, dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

unConfigGenericFunctions_setStsRegAlarmBitAddressS7(string sDpNameBit, int driverNum, string addressReference, int iBitNumber,
        dyn_string &exceptionInfo) {
    string s1, s2;
    string s2_ok = ".DBW";
    int i1, i2;

    string s1_int2stringConvert;
    string s2_int2stringConvert;
    string s3_int2stringConvert;

    dyn_string totalAddress;
    string partialAddress;
    string sPlcName, sDbName, sAddressing;


    /*	DebugN("FUNCTION: unConfigGenericFunctions_setStsRegAlarmBitAddressS7(sDpNameBit="+sDpNameBit+" driverNum="+driverNum+
    																								" addressReference="+addressReference+" iBitNumber="+iBitNumber);
    	*/

    // Separate PLCname.DBx.DBWy
    totalAddress = strsplit(addressReference, ".");
    sPlcName = totalAddress[1];
    sDbName = totalAddress[2];
    sAddressing = totalAddress[3];
    partialAddress = sDbName + "." + sAddressing;

    sscanf (partialAddress, "%[^0-9]%d%[^0-9]%d", s1, i1, s2, i2);
    //	DebugN("check: s1= "+s1+ " i1= "+i1+" s2= "+s2+" i2= "+i2);
    if (s2 == s2_ok) {
        s2 = ".DBX";
        if ((iBitNumber >= 0) && (iBitNumber <= 7)) {
            // Change DBW by DBX and adds the bit number
            s1_int2stringConvert = i1;
            i2 = i2 + 1;
            s2_int2stringConvert = i2;
            s3_int2stringConvert = iBitNumber;
        } else if ((iBitNumber >= 8) && (iBitNumber <= 15)) {
            // Case of second byte addressing DBX(WordNumber+1).(bitNumber-8)
            s1_int2stringConvert = i1;
//					i2=i2+1;
            s2_int2stringConvert = i2;
            iBitNumber = iBitNumber - 8;
            s3_int2stringConvert = iBitNumber;
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setStsRegAlarmBitAddressS7: " + sDpNameBit + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
        // format addressReference
        addressReference = sPlcName + "." + s1 + s1_int2stringConvert + s2 + s2_int2stringConvert + "." + s3_int2stringConvert;
        //			 DebugN(" ---> case bit conversion, addressReference= "+addressReference);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setStsRegAlarmBitAddressS7: " + sDpNameBit + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }

    // addressReference is formatted according to Siemens referencing case BOOL (PVSS_S7_BOOL)
    if (addressReference != "") {
        fwPeriphAddress_set(sDpNameBit,
                            makeDynString(fwPeriphAddress_TYPE_S7,
                                          driverNum,
                                          addressReference,
                                          DPATTR_ADDR_MODE_INPUT_SPONT,
                                          PVSS_S7_BOOL,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_EVSTSREG,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setStsRegAlarmBitAddressS7: " + sDpNameBit + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setManRegS7
/**
Purpose: set ManReg config

Parameters:
		sDpNameManReg, 		string, 		input, 	ManReg name
		driverNum, 				int, 				input, 	driver number
		addressReference, string, 		input, 	reference address for _address config
		exceptionInfo, 		dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

unConfigGenericFunctions_setManRegS7(string sDpNameManReg, int driverNum, string addressReference, dyn_string &exceptionInfo) {

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_setManRegS7(sDpNameManReg="+sDpNameManReg+" driverNum="+driverNum+
    																								" addressReference="+addressReference);

    */

    if (addressReference != "") {
        fwPeriphAddress_set(sDpNameManReg,
                            makeDynString(fwPeriphAddress_TYPE_S7,
                                          driverNum,
                                          addressReference,
                                          DPATTR_ADDR_MODE_OUTPUT,
                                          PVSS_S7_UINT16,
                                          UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                          "",
                                          "",
                                          "",
                                          "",
                                          UN_CONFIG_LOWLEVEL_MANREG,
                                          0,
                                          UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                          UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                          ""),
                            exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setManRegS7: " + sDpNameManReg + getCatStr("unGeneration", "ADDRESSFAILED"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setArchiving
/**
Purpose: get the archive config from fwDefinition and set it to the DP.
WARNING: from the time being the smoothing is set to old/new --> it should be read from the fwDeviceDefinition

Parameters:
		sDeviceType, 			string, 		input, 	device type
		sDeviceDpName, 		string,			input, 	device DP name
		bArchive,					bool,				input,	true=set archive config, false=delete archive config
		sBoolArchive,			string,			input, 	bool archive name
		sAnalogArchive,		string,			input, 	Analog archive name
		sEventArchive,		string,			input, 	event archive name
		exceptionInfo, 		dyn_string, output, for errors
		dsDPENoArchive,		dyn_string, input, 	list of DPE on which the archive must not be set.

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

unConfigGenericFunctions_setArchiving(string sDeviceType, string sDeviceDpName, bool bArchive, string sBoolArchive, string sAnalogArchive, string sEventArchive, dyn_string &exceptionInfo,
                                      dyn_string dsDPENoArchive = makeDynString()) {
    dyn_string dsDPE;
    int i, len, len2, pos;
    string sArchive;
    dyn_string dsSplit;

// get archive DPE from fwDeviceDefinition
    fwDevice_getArchiveElements(sDeviceType, dsDPE, exceptionInfo);
//DebugN("**** bef", sDeviceType, dsDPE, bArchive, dsDPENoArchive);
    if (dynlen(exceptionInfo) <= 0) {
// remove the DPE that do not need archive
        len = dynlen(dsDPENoArchive);
        for (i = 1; i <= len; i++) {
            pos = dynContains(dsDPE, dsDPENoArchive[i]);
            if (pos > 0) {
                dynRemove(dsDPE, pos);
            }
        }
//DebugN("after", dsDPE, bArchive);
        len = dynlen(dsDPE);
        if (bArchive) {
// set the archive config with old/new smoothing
            for (i = 1; i <= len; i++) {
// if dpe=evStsReg01 or evStsReg02 --> sEventArchive
// if dpElementType = bool --> sBoolArchive
// else --> sAnalogArchive
                sArchive = "";
                dsSplit = strsplit(dsDPE[i], ".");
                len2 = dynlen(dsSplit);
                if (len2 > 0) {
                    if ((dsSplit[len2] == "evStsReg01") || (dsSplit[len2] == "evStsReg02")) {
                        sArchive = sEventArchive;
                    } else {
                        switch (dpElementType(sDeviceDpName + dsDPE[i])) {
                            case DPEL_BOOL:
                                sArchive = sBoolArchive;
                                break;
                            default:
                                sArchive = sAnalogArchive;
                                break;
                        }
                    }
                }
                if (sArchive != "") {
                    fwArchive_set(sDeviceDpName + dsDPE[i],
                                  sArchive,
                                  DPATTR_ARCH_PROC_SIMPLESM,
                                  DPATTR_COMPARE_OLD_NEW,
                                  0,
                                  0,
                                  exceptionInfo);
                }
//DebugN(i, dsDPE[i], sArchive, exceptionInfo);
            }
        } else {
// delete the archive config
            for (i = 1; i <= len; i++) {
                fwArchive_delete(sDeviceDpName + dsDPE[i], exceptionInfo);
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_FormatDomainNature
/**
Purpose: remove empty Domain/Nature in multiple Domain/Nature list

Parameters:
		sDeviceType, 			string, 		input, 	Domain or Nature

Usage: external function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

string unConfigGenericFunctions_FormatDomainNature(string sField) {
    dyn_string dsSplit;
    int iPos, iLen, j;
    string sReturn, sTemp;

    dsSplit = strsplit(sField, UN_CONFIG_GROUP_SEPARATOR);
    dynUnique(dsSplit);
    iPos = dynContains(dsSplit, "");
    if (iPos > 0) {
        dynRemove(dsSplit, iPos);
    }

    iLen = dynlen(dsSplit);
    for (j = 1; j <= iLen; j++) {
        sTemp = sTemp + UN_CONFIG_GROUP_SEPARATOR + dsSplit[j];
    }

    sReturn = substr(sTemp, 1);

    return sReturn;
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_reconvertIP
/**
Purpose: get real IP address format

Parameters: int, input, iIP, computer address IP converted

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: XP
	. distributed system: yes.
*/
string unConfigGenericFunctions_reconvertIP(int iIP) {
    dyn_string dsIP;
    string sResult, currentHexa, sByte;
    int i, iLen, iStart, iByte, iLength;

    iLen = sprintf(currentHexa, "%x", iIP);

    if (iLen == 8) {
        i = 0;
        iStart = 0;
        iLength = 2;

        while (i < 4) {
            i++;
            iByte = 0;
            sByte = "";

            sByte = substr(currentHexa, iStart, iLength);
            sscanf(sByte, "%x", iByte);
            dsIP[i] = iByte;
            iStart += iLength;
        }

        if (dynlen(dsIP) == 4) {
            sResult = dsIP[1] + "." + dsIP[2] + "."  + dsIP[3] + "."  + dsIP[4];
        }
    }

    return sResult;
}

/** Parses specified config files and returns the [general] section

@param configLines : list of lines read from the config file
@param lineNumbers : contains the list of line numbers that belong to [general] section;
	    note that more than one [general] section may exist (they are all concatenated),
	    and the list will not contain the comment lines, empty lines and the lines with "[general]" header
@param keywords : parsed keyword part of the line,
@param values   : parsed value of the line, with double-quote characters stripped out.

@ returns 0 on success, -1: if file cannot be opened or other error
*/
int _unConfigGenericFunctions_FindGeneralSection(dyn_string configLines, dyn_int &lineNumbers, dyn_string &keywords, dyn_string &values) {
    dynClear(lineNumbers);
    dynClear(keywords);
    dynClear(values);


    int maxLines = dynlen(configLines);
    int numLines = 0;

    bool isInGeneralSection = false;
    for (int i = 1; i <= maxLines; i++) {

        string line = strltrim(strrtrim(configLines[i]));
        strreplace(line, " \"", "");
        if (strlen(line) < 1) { continue; }
        if (substr(line, 0, 1) == "#") { continue; }
        if (!isInGeneralSection) {
            if (line == "[general]") {
                isInGeneralSection = true;
                continue;
            }
        } else { // we are in the [general] section
            if (substr(line, 0, 1) == "[") {
                isInGeneralSection = false;
                continue;
            }

            string keyword = "";
            string value = "";
            dyn_string ds = strsplit(line, "=");
            if (dynlen(ds) > 1) {
                keyword = strltrim(strrtrim(ds[1]));
                value = strrtrim(strltrim(ds[2], "\""), "\"");
            }

            numLines++;
            lineNumbers[numLines] = i;
            keywords[numLines] = keyword;
            values[numLines] = value;
        }
    }

    return 0;
}

//-------------------------------------------------------------------------
// unConfigGenericFunctions_getConfigFile
/**
Purpose: to get a specified config file as a dyn_string

Parameters:
    configLines,		dyn_string, 	output, lines from a specified config file
    sFile,			string,		input, config file name/pathf to read data from

Returns 0 on success, -1 on error

Usage: external function

*/

int unConfigGenericFunctions_getConfigFile(dyn_string &configLines, string sFile) {

    string fileInString;
    bool fileLoaded = fileToString(sFile, fileInString);
    if (! fileLoaded ) {
        DebugN("unConfigGenericFunctions_getConfigFile: Cannot load " + sFile);
        return -1;
    }

    configLines = strsplit(fileInString, "\n");
    return 0;
}

//-------------------------------------------------------------------------
// unConfigGenericFunctions_Create_Config_ows
/**
Purpose: to set the OWS config file for Linux and Windows console

Parameters:
		sConfig_ows,		string, 		 input, name of the config file for OWS

Usage: external function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. !!! works with the standard PVSS AB/CO instalation !!! - eg: /opt/unicryo/PVSS_project/unicos_dev/.... or C:\dev_disk\PVSS_projects\unicos_dev\...
	. PVSS version: 3.0
	. operating system: Win XP, Linux.
	. distributed system: yes.
*/

void unConfigGenericFunctions_Create_Config_ows(const string sConfig_ows, string dummyShareFolder = "", string dummyInstalledComponent = "") {

    bool ok=true;

    int rc = _unConfigGenericFunctions_generate_Config_ows(sConfig_ows, false, false);
    if (rc < 0) ok=false;

    rc = _unConfigGenericFunctions_generate_Config_ows(sConfig_ows, false, true);
    if (rc < 0) ok=false;

    rc = _unConfigGenericFunctions_generate_Config_ows(sConfig_ows, true, false);
    if (rc < 0) ok=false;

    rc = _unConfigGenericFunctions_generate_Config_ows(sConfig_ows, true, true);
    if (rc < 0) ok=false;

    if (!ok) { DebugTN("Warning: config file(s) for OWS might not be created; OWS may not work properly"); }
}


int _unConfigGenericFunctions_generate_Config_ows(string targetConfigFile, bool isLinux, bool isConfig_PROJ) {
    string sHost = getHostname();
    if (sHost == "") {
        DebugTN("ERROR: Hostname is unknown! OWS will not work!!!");
        return -1;
    }

    // find the main proj_path, and extract the service account username  (eg. unicryo)
    string sPath = getPath(CONFIG_REL_PATH);

    dyn_string dsPath = strsplit(sPath, "/");
    int iPos = dynContains(dsPath, "opt");
    if (iPos <= 0) return -1;

    string sUser = dsPath[iPos + 1];

    // start off by reading the main config file and parsing its [general] section

    dyn_string configLines;
    int rc = unConfigGenericFunctions_getConfigFile(configLines, sPath + "/config");
    if (rc != 0) { return -1; }


    dyn_int lineNumbers, linesToRemove;
    dyn_string keywords, values, linesToAppend;
    rc = _unConfigGenericFunctions_FindGeneralSection(configLines, lineNumbers, keywords, values);
    if (rc != 0) { return -1; }

    if (dynlen(lineNumbers) < 1) {
        DebugN("ERROR: Could not identify the [general] section in the config file");
        return -1;
    }

    // mark for removal the lines that should not go into config_ows;
    dyn_string keywordsToRemove;
    if (isConfig_PROJ) {
        // Remove only pvss_path, as it will be re-added later (no duplicates)
        keywordsToRemove = makeDynString("pvss_path");
    } else {
        // Remove these, as they're already a part of OWS project config file
        keywordsToRemove = makeDynString("proj_version", "pvss_path", "langs", "userName", "password");
    }

    for (int i = 1; i <= dynlen(keywordsToRemove); i++) {
        int idx = dynContains(keywords, keywordsToRemove[i]);
        if (idx) { dynAppend(linesToRemove, lineNumbers[idx]); }
    }

    // treat the proj_paths

    for (int i = 1; i <= dynlen(keywords); i++) {
        if (keywords[i] != "proj_path") { continue; }
        string proj_path = values[i];
        // tokenize/process the proj_path
        string svc_account = "";
        string host = "";
        string sharedFolder = "";
        string proj_path_without_prefix = "";
        string remotePath = "";

        dyn_string ds = strsplit(proj_path, "/"); // note! as we have paths starting with "/", the first element is empty!

        if (ds[2] == "opt") { // process local paths: "/opt/dcsna62/PVSS_projects/NA62DCS01/NA62DCS01"
            host = sHost;
            svc_account = ds[3];
            sharedFolder = ds[4];
            for (int j = 5; j <= dynlen(ds); j++) { proj_path_without_prefix += "/" + ds[j]; }

        } else if (ds[2] == "nfs") { // processs remote folders for portal-like projects,
            // eg. "/nfs/cs-ccr-na62ds3.cern.ch/local/dcsna62/PVSS_projects/NA62DCS13/NA62DCS13"
            host = ds[3];
            svc_account = ds[5];
            sharedFolder = ds[6];
            for (int j = 7; j <= dynlen(ds); j++) { proj_path_without_prefix += "/" + ds[j]; }

        } else {
            DebugN("WARNING: Cannot transform proj_path " + proj_path + " : OWS may not work");
            continue;
        }

        proj_path_without_prefix = strltrim(proj_path_without_prefix, "/"); // remove leading slash


        string new_proj_path;
        if (isLinux) {
            new_proj_path = "/nfs/" + host + "/local/" + svc_account + "/" + sharedFolder + "/" + proj_path_without_prefix;
        } else {
            new_proj_path = "//"    + host + "/" +  sharedFolder + "/" + proj_path_without_prefix;
        }

        configLines[lineNumbers[i]] = "proj_path = \"" + new_proj_path + "\"";
    }

    // treat the PVSS paths, add only in case "standalone" configs
    if (isConfig_PROJ) {
        if (isLinux) {
            dynAppend(linesToAppend, "pvss_path = \"/opt/WinCC_OA/" + VERSION + "\"");
        } else {
            dynAppend(linesToAppend, "pvss_path = \"c:/Siemens/Automation/WinCC_OA/" + VERSION + "\"");
        }
    }
    // treat the "event=" and "data=", if not present yet
    if (dynContains(keywords, "event") == 0) {
        dynAppend(linesToAppend, "event = \"" + sHost +  "\"");
    }

    if (dynContains(keywords, "data") == 0) {
        dynAppend(linesToAppend, "data = \"" + sHost +  "\"");
    }

    // append the lines that need to be appended
    int insertIdx = dynMax(lineNumbers) + 1;
    dynInsertAt(configLines, linesToAppend, insertIdx);

    // and then remove the lines that need to be removed
    // note: reverse order to remove lines from the end
    dynSort(linesToRemove, false);
    for (int i = 1; i <= dynlen(linesToRemove); i++) {
        dynRemove(configLines, linesToRemove[i]);
    }

    string fileName = sPath + targetConfigFile;
    if (isLinux) { fileName += "_linux"; }
    if (isConfig_PROJ) { fileName += "_" + PROJ; }

    string exceptionText;
    DebugTN("INFO: Generating config OWS file", fileName);
    unGenericDpFunctions_fwInstallation_saveFile(configLines, fileName, exceptionText);
    if (exceptionText != "") {
        DebugN("Could not write OWS config file " + fileName, exceptionText);
        return -1;
    }

    return 0;
}

//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setDescriptionDPE
/**
Purpose: Set the alias on all the DPE of the device DP, remove first the DPE pattern

Parameters:
		sDeviceDpName: string, input, the device DP name
		dsDpe: dyn_string, input, list of DPE
		dsDescription: dyn_string, input, list of description
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setDescriptionDPE(string sDeviceDpName, dyn_string dsDpe, dyn_string dsDescription, dyn_string &exceptionInfo) {
    int i, len;

    len = dynlen(dsDpe);

    if (len == dynlen(dsDescription)) {
        for (i = 1; i <= len; i++) {
            unConfigGenericFunctions_setDescription(sDeviceDpName + dsDpe[i], dsDescription[i], exceptionInfo);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setDescriptionDPE: wrong parameter size", "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setPvRangeDPE
/**
Purpose: Set the the pv_range config

Parameters:
		sDeviceDpName: string, input, the device DP name
		dsDpe: dyn_string, input, list of DPE
		dfMin: dyn_float, input, min value
		dfMax: dyn_float, input, max value
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setPvRangeDPE(string sDeviceDpName, dyn_string dsDpe, dyn_float dfMin, dyn_float dfMax,
                                       dyn_string &exceptionInfo) {
    int i, len;
    int iDeadBandType, iType;
    float fDeadBand;

    len = dynlen(dsDpe);
    if ((len == dynlen(dfMin)) && (len == dynlen(dfMax))) {
        for (i = 1; i <= len; i++) {
            fwPvRange_set(sDeviceDpName + dsDpe[i], dfMin[i], dfMax[i],
                          UN_CONFIG_DEFAULT_RANGE_NEGATE_RANGE, UN_CONFIG_DEFAULT_RANGE_IGNORE_OUTSIDE,
                          UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MIN, UN_CONFIG_DEFAULT_RANGE_INCLUSIVE_MAX,
                          exceptionInfo);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setPvRangeDPE: wrong parameter size", "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setUnitDPE
/**
Purpose: Set the unit

Parameters:
		sDeviceDpName: string, input, the device DP name
		dsDpe: dyn_string, input, list of DPE
		dsUnit: dyn_string, input, unit
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setUnitDPE(string sDeviceDpName, dyn_string dsDpe, dyn_string dsUnit, dyn_string &exceptionInfo) {
    int i, len;

    len = dynlen(dsDpe);
    if (len == dynlen(dsUnit)) {
        for (i = 1; i <= len; i++) {
            unConfigGenericFunctions_setUnit(sDeviceDpName + dsDpe[i], dsUnit[i], exceptionInfo);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setUnitDPE: wrong parameter size", "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setFormatDPE
/**
Purpose: Set the the pv_range config

Parameters:
		sDeviceDpName: string, input, the device DP name
		dsDpe: dyn_string, input, list of DPE
		dsFormat: dyn_string, input, unit
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setFormatDPE(string sDeviceDpName, dyn_string dsDpe, dyn_string dsFormat, dyn_string &exceptionInfo) {
    int i, len;

    len = dynlen(dsDpe);
    if (len == dynlen(dsFormat)) {
        for (i = 1; i <= len; i++) {
            unConfigGenericFunctions_setFormat(sDeviceDpName + dsDpe[i], dsFormat[i], exceptionInfo);
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigGenericFunctions_setFormatDPE: wrong parameter size", "");
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setSmoothingDPE
/**
Purpose: Set the the smooting config

Parameters:
		sDeviceDpName: string, input, the device DP name
		dsDpe: dyn_string, input, list of DPE
		dsDeadbandType: dyn_string, input, list of of deadband type ordered like dsDpe
		dsDeadband: dyn_string, input, list of deadband values ordered like dsDpe (this is the real deadband value not a % of a min-max)
		dfMin: dyn_float, input, min value
		dfMax: dyn_float, input, max value
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unConfigGenericFunctions_setSmoothingDPE(string sDeviceDpName, dyn_string dsDpe, dyn_string dsDeadbandType,
        dyn_string dsDeadband, dyn_string &exceptionInfo) {
    int i, len;
    int iDeadBandType, iType;
    float fDeadBand;

    len = dynlen(dsDpe);
    while (dynlen(dsDeadbandType) < len) {
        dynAppend(dsDeadbandType, "0");
    }
    while (dynlen(dsDeadband) < len) {
        dynAppend(dsDeadband, "0");
    }
    for (i = 1; i <= len; i++) {
//DEADBAND SETTING
        iDeadBandType = (int)dsDeadbandType[i];
        unConfigGenericFunctions_GetDeadBandValue(dsDeadband[i], fDeadBand, iDeadBandType, iType, 100, 0);
//DebugN(dsDeadband[i], dsDeadbandType[i], iType, fDeadBand, iDeadBandType, iDeadBandType);
        if (fDeadBand > 0 && (iDeadBandType == 1 || iDeadBandType == 2)) {
            fwSmoothing_set(sDeviceDpName + dsDpe[i], iType, fDeadBand, UN_CONFIG_DEFAULT_SMOOTHING_TIME_FILTER, exceptionInfo);
        } else if (iDeadBandType == UN_CONFIG_DEADBAND_OLD_NEW) {
            fwSmoothing_set(sDeviceDpName + dsDpe[i], iType, fDeadBand, UN_CONFIG_DEFAULT_SMOOTHING_TIME_FILTER, exceptionInfo);
        } else {
            fwSmoothing_delete(sDeviceDpName + dsDpe[i], exceptionInfo);
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setArchiveDPE
/**
Purpose: get the archive config from fwDefinition and set it to the DP.
WARNING: from the time being the smoothing is set to old/new for DPE except if it is in the list of DPE.
Boolean DPE are in sBoolArchive, all other type are in sAnalogArchive

Algorithm:
	1. if dsConfigArchive[1] == "N" there will be no archive on the device
	2. else for each DPE from dsDeviceDpe set the archive according to dsConfigArchive
	i.e.: dsDeviceDpe[i] --> dsArchiveConfig[1+i]

Parameters:
		sDeviceType, 			string, 		input, 	device type
		sDeviceDpName, 		string,			input, 	device DP name
		dsDeviceDpe,				dyn_string,	input,	list of DPE
		dsConfigArchive,	dyn_string,	input,	list of archive config for dsDeviceDpe ordered like dsDeviceDpe
		iArchiveTimeFilter, int, input, the archive smoothing time filter
		sBoolArchive,			string,			input, 	bool archive name
		sAnalogArchive,		string,			input, 	Analog archive name
		sEventArchive,		string,			input, 	event archive name
		exceptionInfo, 		dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

unConfigGenericFunctions_setArchiveDPE(string sDeviceType, string sDeviceDpName, dyn_string dsDeviceDpe, dyn_string dsConfigArchive,
                                       int iArchiveTimeFilter,
                                       string sBoolArchive, string sAnalogArchive, string sEventArchive,
                                       dyn_string &exceptionInfo) {
    dyn_string dsDPE;
    int i, len, len2, pos, iSmoothType, iArchiveType;
    float fDeadband;
    string sArchive;
    dyn_string dsSplit;
    bool bSetArchive;
    bool bArchive;
    dyn_string dsArchive = dsConfigArchive;

    if (dynlen(dsArchive) < 1) {
        dsArchive = makeDynString("N");
    }

    unConfigGenericFunctions_getArchiving(dsArchive[1], bArchive, 0,	iArchiveType, iSmoothType);
    dynRemove(dsArchive, 1);
    while (dynlen(dsArchive) < dynlen(dsDeviceDpe)) {
        dynAppend(dsArchive, "N");
    }

//DebugN(dsDeviceDpe, dsArchive, dsConfigArchive);
// get archive DPE from fwDeviceDefinition
    fwDevice_getArchiveElements(sDeviceType, dsDPE, exceptionInfo);
//DebugN("**** bef", sDeviceType, dsDPE, bArchive);
    if (dynlen(exceptionInfo) <= 0) {
//DebugN("after", dsDPE, bArchive);
        len = dynlen(dsDPE);
// set the archive config with old/new smoothing
        for (i = 1; i <= len; i++) {
            bSetArchive = bArchive;
            if (bSetArchive) {
// if dpElementType = bool --> sBoolArchive
// else --> sAnalogArchive
                sArchive = "";
                switch (dpElementType(sDeviceDpName + dsDPE[i])) {
                    case DPEL_BOOL:
                        sArchive = sBoolArchive;
                        pos = dynContains(dsDeviceDpe, dsDPE[i]);
                        if (pos > 0) {
                            unConfigGenericFunctions_getArchiving(dsArchive[pos], bSetArchive, iArchiveTimeFilter, iArchiveType, iSmoothType);
                        } else {
                            bSetArchive = false;
                        }
                        fDeadband = 0;
                        break;
                    default:
                        sArchive = sAnalogArchive;
                        pos = dynContains(dsDeviceDpe, dsDPE[i]);
                        if (pos > 0) {
                            unConfigGenericFunctions_getArchiving(dsArchive[pos], bSetArchive, iArchiveTimeFilter, iArchiveType, iSmoothType);
                            unConfigGenericFunctions_getArchivingDeadband(dsArchive[pos], fDeadband);
//DebugN(pos, dsDeviceDpe[pos], dsArchive[pos], fDeadband);
                            if (fDeadband == -1) {
                                fDeadband = 0;
                            }
                        } else {
                            bSetArchive = false;
                        }
                        break;
                }
//DebugN(i, dsDPE[i], sArchive, exceptionInfo);
            }
            if (bSetArchive) {
                fwArchive_set(sDeviceDpName + dsDPE[i],
                              sArchive,
                              iArchiveType,
                              iSmoothType,
                              fDeadband,
                              0,
                              exceptionInfo);
//DebugN("set arch", sDeviceDpName + dsDPE[i], sArchive, iArchiveType, iSmoothType, fDeadband);
            } else {
                // delete the archive config
                fwArchive_delete(sDeviceDpName + dsDPE[i], exceptionInfo);
            }
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_nameCheck
/**
Purpose: check if the name is a correct dpName


Parameters:
		sName: string, input, the name to check
		return value: bool, output, true = name OK/false = name bad

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
*/

bool unConfigGenericFunctions_nameCheck(string sName) {
    bool bResult = true;
    string sTemp;
    int iPos, i, len = strlen(sName);
    dyn_string dsNameToCheck;

    if (len < UN_CONFIG_NAMECHECK_MAXLEN) {
        dsNameToCheck[1] = sName;
    } else {
        iPos = 0;
        i = 1;
        sTemp = sName;
        len = strlen(sTemp);
        while (len >= UN_CONFIG_NAMECHECK_MAXLEN) {
            dsNameToCheck[i++] = substr(sTemp, 0, UN_CONFIG_NAMECHECK_MAXLEN - 1);
            sTemp = substr(sTemp, UN_CONFIG_NAMECHECK_MAXLEN - 1, len - (UN_CONFIG_NAMECHECK_MAXLEN - 1));
            len = strlen(sTemp);
//DebugN(i-1, dsNameToCheck[i-1], sTemp);
        }
        dynAppend(dsNameToCheck, sTemp);
    }
//DebugN(dsNameToCheck);
    len = dynlen(dsNameToCheck);
    for (i = 1; (i <= len) & bResult; i++) {
        if (nameCheck(dsNameToCheck[i]) < 0) {
            bResult = false;
        }
    }
    return bResult;
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_getFrontEndManagerConfig

/**
Purpose: return the driver config of _UnPlc (MODBUS) front-end

Parameters:
		iDriverNumber: 		int, 		input, 	the driver number
		sName: 					string, 		output,	the name to display
		sDriverName: 			string, 		output, 	the manager name
		iManType: 	int, output, the manager type
		sCommandLine: 	int, output, the command line to start the manager

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.

@reviewed 2018-08-01 @whitelisted{Callback}
*/

_UnPlc_getFrontEndManagerConfig(int iDriverNumber, string &sName, string &sDriverName, int &iManType, string &sCommandLine) {
    sName = "MODBUS";
    sDriverName = "WCCOAmod";
    iManType = DRIVER_MAN;
    sCommandLine = "-num " + iDriverNumber;
}

//------------------------------------------------------------------------------------------------------------------------


/**Get the legacy type name of a plc (i.e. S7_PLC or _unPLC) and convert to the new names (i.e. S7 or Modbus)
Possible input constants:   \n
S7_PLC_DPTYPE   \n
UN_PLC_DPTYPE   \n
Possible output constants:   \n
UN_CONFIG_PROTOCOL_S7   \n
UN_CONFIG_PROTOCOL_MODBUS   \n

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param legacyName, string, the PLC legacy type name (i.e. S7_PLC or _unPLC)
@return string the PLC new type name (i.e. S7 or Modbus)
*/
string unConfigGenericFunctions_convertPlcTypeToNewType(string legacyTypeName) {
    switch (legacyTypeName) {
        case S7_PLC_DPTYPE:
            return UN_CONFIG_PROTOCOL_S7;
        case UN_PLC_DPTYPE:
            return UN_CONFIG_PROTOCOL_MODBUS;
        default:
            return legacyTypeName;
    }
}
//------------------------------------------------------------------------------------------------------------------------


/**Get the new type name of a plc (i.e. S7 or Modbus)  and convert to the legacy names (i.e. S7_PLC or _unPLC)
Possible input constants:   \n
UN_CONFIG_PROTOCOL_S7   \n
UN_CONFIG_PROTOCOL_MODBUS   \n
Possible output constants:   \n
S7_PLC_DPTYPE   \n
UN_PLC_DPTYPE   \n

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param legacyName, string, the PLC new type name (i.e. S7 or Modbus)
@return string, the PLC legacy type name (i.e. S7_PLC or _unPLC)
*/
string unConfigGenericFunctions_convertPlcTypeToLegacyType(string newTypeName) {
    switch (newTypeName) {
        case UN_CONFIG_PROTOCOL_S7:
            return S7_PLC_DPTYPE;
        case UN_CONFIG_PROTOCOL_MODBUS:
            return UN_PLC_DPTYPE;
        default:
            return newTypeName;
    }
}

//------------------------------------------------------------------------------------------------------------------------


/**Get the new type name of a plc (from the line of the import file) and returns the PLC type (i.e. S7 or MODBUS).

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param legacyName, string, the PLC type and model. Examples of input string:  "MODBUS,PREMIUM", "S7,S7-300"
@return string, the PLC type name Examples of output strings:  "MODBUS", "S7"
*/
string unConfigGenericFunctions_extractFeProtocol(string sPlcTypeConfig) {
    //old version support
    if (strpos(sPlcTypeConfig, ",") < 0) { return sPlcTypeConfig; }

    //new version: the plc type in the config line contains the info: TYPE,MODEL
    dyn_string dsSplit = strsplit(sPlcTypeConfig, ",");
    if (dynlen(dsSplit)) {
        return dsSplit[1];
    }
    return sPlcTypeConfig;
}


//------------------------------------------------------------------------------------------------------------------------


/**Get the new type name of a plc (from the line of the import file) and returns the PLC type (i.e. S7-300 or PREMIUM).

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param legacyName, string, the PLC type and model. Examples of input string:  "MODBUS,PREMIUM", "S7,S7-300"
@return string, the PLC model name (free label). Examples of output strings:  "PREMIUM", "S7-300"
*/
string unConfigGenericFunctions_extractFeModel(string sPlcTypeConfig) {
    //old version support
    if (strpos(sPlcTypeConfig, ",") < 0) { return sPlcTypeConfig; }

    //new version: the plc type in the config line contains the info: TYPE,MODEL
    dyn_string dsSplit = strsplit(sPlcTypeConfig, ",");
    if (dynlen(dsSplit) > 1) {
        return dsSplit[2];//model is the second item
    }
    return sPlcTypeConfig;
}

/** Function returns whether it is a polling only front end based on plc type
@param plcType - plc type label from importation line
*/
bool unConfigGenericFunctions_isPollingOnly(string plcType) {
    string plcModel = unConfigGenericFunctions_extractFeModel(plcType);
    return plcModel == "polling";
}

int unConfigGenericFunctions_getPollingInterval(string plcType) {
    dyn_string dsSplit = strsplit(plcType, ",");
    if (dynlen(dsSplit) > 2) {
        return dsSplit[3];
    }
    return 5000;
}

//------------------------------------------------------------------------------------------------------------------------
#uses "un_UnPlc.ctl"
//@}
