/**@name LIBRARY: unConfigOPCUA.ctl

@author: Alexey Merezhin (EN/ICE)

Creation Date: 25/09/2015

Purpose:
This library contains the function used to import the OPC UA front end.

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . unGeneration.cat
  . UNICOS-JCOP framework
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

// Data type OPC constant
const int OPCUA_DEFAULT = 750;
const int OPCUA_INT16 = 754;
const int OPCUA_INT32 = 756;
const int OPCUA_UINT16 = 755;
const int OPCUA_UINT32 = 757;
const int OPCUA_BOOL = 751;
const int OPCUA_FLOAT = 760;
const int OPCUA_DOUBLE = 761;

const string OPCUA_SERVER_PREFIX        = "OPCUA_";
const string OPCUA_SUBSCRIPTION_PREFIX = "OPCUA_Sub_";

// constant for UNOPC system integrity
const string c_unSystemIntegrity_UNOPCUA = "OPCUA_";
// constant for import of UNOPC
const string UNOPCUA_DPTYPE = "OPCUA";
/*importation line example: 
PLCCONFIG;OPCUA;fe_name;app_name;cvl-plc-tspp:4841;10;100;0;0;0;2|s|WATCHDOG.counter.value|750;

OPCUA address should be defined in NodeID format as following:
NAMESPACE_INDEX|IDENTIFIER_TYPE|IDENTIFIER|CONVERTION_DATATYPE
* namespace index is a number >= 0
* identifier type defines the type of identifier, could be 
    ** i  -> numeric (UInteger)
    ** s  -> string (String)
    ** g  -> guid (Guid)
    ** b  -> opaque (ByteString)
* identifier as it is
* convertion datatype is optional, values available:
    ** DEFAULT         750     Default
    ** BOOLEAN         751     Boolean
    ** SBYTE           752     Short Byte
    ** BYTE            753     Byte
    ** INT16           754     16-Bit Integer signed
    ** UINT16          755     16-Bit Integer unsigned
    ** INT32           756     32-Bit Integer signed
    ** UINT32          757     32-Bit Integer unsigned
    ** INT64           758     64-Bit Integer signed
    ** UINT64          759     64-Bit Integer unsigned
    ** FLOAT           760     Floating-point value
    ** DOUBLE          761     Floating-point value, double precision
    ** STRING          762     String
    ** DATETIME        763     Date & Time
    ** GUID            764     Unique Identifier
    ** BYTESTRING      765     Byte String
    ** XMLELEMENT      766     XML Element
    ** NODEID          767     Node ID
    ** LOCALIZEDTEXT   768     Localized Text

*/
const int UN_CONFIG_OPCUA_LENGTH = 8;
const int UN_CONFIG_UNOPCUA_SERVERNAME = 4; // opcua server name and port, for example cvl-plc-tspp:4841 or host:51234/UA/UA_Server
const int UN_CONFIG_UNOPCUA_RECONNECT_TIMER = 5; // This value gives the length in seconds of the interval after which the client tries a reconnect to the OPCUA server.
const int UN_CONFIG_UNOPCUA_QUEUE_SIZE = 6; // amount of queue slots in the server, all the queue is send alltogether on the single subscription event
const int UN_CONFIG_UNOPCUA_PUBLISHING_INTERVAL = 7; // how often we recieve data from the server, in ms
const int UN_CONFIG_UNOPCUA_COUNTER = 8; // OPCUA address of the counter

/**
Purpose: check the coherency of the delete command

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  dsDeleteDps, dyn_string, output, list of dp that will be deleted
  exceptionInfo, dyn_string, output, for errors
*/
unConfigOPCUA_checkDeleteCommand(dyn_string dsConfig, dyn_string &dsDeleteDps, dyn_string &exceptionInfo) {
    string sFeName, sPLCSubApplication = "", sObjectType = "", sNumber = "", sDpPlcName, sPlcModPlc;
    dyn_string dsAlarmDps;

    if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
        // get PLC Name
        sFeName = dsConfig[2];
        if ((sFeName != "") && (dsConfig[1] == UN_DELETE_COMMAND)) {
            if (dynlen(dsConfig) >= 3) { // get Application
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) { // get DeviceType
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) { // get device number
                sNumber = dsConfig[5];
            }
            unGenericDpFunctions_getPlcDevices("", sFeName, sPLCSubApplication, sObjectType, sNumber, dsDeleteDps);
            // if only delete key word then delete also modbus and systemalarm
            if ((sFeName != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
                // sDpPlc is UNOPC_plcName
                sDpPlcName = c_unSystemIntegrity_UNOPCUA + sFeName;
                if (dpExists(sDpPlcName)) {
                    dynAppend(dsDeleteDps, sDpPlcName);
                    dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1), c_unSystemAlarm_dpType);
                    dynAppend(dsDeleteDps, dsAlarmDps);
                }
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkDeleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

/**
Purpose: check the coherency of the OPC front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  dsDeleteDps: dyn_string, input, dp to be deleted
  exceptionInfo: dyn_string, output, for errors
*/
unConfigOPCUA_check(string currentObject, dyn_string currentLineSplit, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    switch (currentObject) {
        case UN_PLC_COMMAND:
            unConfigOPCUA_checkApplication(currentLineSplit, dsDeleteDps, exceptionInfo);
            break;
        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }
}

/**
Purpose: delete function

Parameters:
  dsConfig, dyn_string, input, config dyn_string
  exceptionInfo, dyn_string, output, for errors
*/
unConfigOPCUA_deleteCommand(dyn_string dsConfig, dyn_string &exceptionInfo) {
    string sPLCSubApplication = "", sObjectType = "", sNumber = "";
    dyn_string dsDpeList, exceptionInfoTemp;

    unConfigOPCUA_checkDeleteCommand(dsConfig, dsDpeList, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_deleteCommand:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        if (dynlen(dsConfig) >= 2) {			// At least, delete instruction and plc name
            if (dynlen(dsConfig) >= 3) {
                sPLCSubApplication = dsConfig[3];
            }
            if (dynlen(dsConfig) >= 4) {
                sObjectType = dsConfig[4];
            }
            if (dynlen(dsConfig) >= 5) {
                sNumber = dsConfig[5];
            }
            unGenericDpFunctions_deletePlcDevices(dsConfig[2], sPLCSubApplication, sObjectType, sNumber, "progressBar", exceptionInfo);
            if ((dsConfig[2] != "") && (sPLCSubApplication == "") && (sObjectType == "") && (sNumber == "")) {
                unConfigOPCUA_deleteFrontEnd(dsConfig[2], exceptionInfo);
            }
        }
    }
}

/**
Purpose: set OPC front-end config

Parameters:
  currentObject: string, input, type of config
  currentLineSplit: dyn_string, input, config
  exceptionInfo: dyn_string, output, for errors
*/
unConfigOPCUA_set(string currentObject, dyn_string currentLineSplit, dyn_string &exceptionInfo) {
    string currentDevice, sPlcName, sPlcType, sAnalogArchive;
    int iPlcNumber, driver;
    bool bEvent16;

    switch (currentObject) {
        case UN_PLC_COMMAND:
            g_dsImport_UnOPC_config = makeDynString();
            unConfigOPCUA_setApplication(currentLineSplit, exceptionInfo);
            // keep the PVSS OPC Server name for future use in import of the device.
            if (dynlen(exceptionInfo) <= 0) {
                g_dsImport_UnOPC_config[UNOPC_SERVERNAME] = currentLineSplit[UN_CONFIG_UNOPC_SERVERNAME];
                g_dsImport_UnOPC_config[UNOPC_GROUPINNAME] = currentLineSplit[UN_CONFIG_UNOPC_GROUPINNAME];
                g_dsImport_UnOPC_config[UNOPC_GROUPOUTNAME] = currentLineSplit[UN_CONFIG_UNOPC_GROUPOUTNAME];
            }
            break;
        default:
            fwException_raise(exceptionInfo, "ERROR", getCatStr("unGeneration", "UNKNOWNFUNCTION"), "");
            break;
    }
}

/**
Purpose: check data for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, OPC configuration line in file + additional parameters
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
		dsDeleteDps, dyn_string, input, datapoints that will be deleted before importation
		exceptionInfo: dyn_string, output, for errors
*/
unConfigOPCUA_checkApplication(dyn_string dsPLC, dyn_string dsDeleteDps, dyn_string &exceptionInfo) {
    dyn_string dsPLCLine, dsPLCAdditionalData;
    int i, iRes, iDriverNum;
    string sPLCDp, sHostname, sAliasDp, tempDp;

    if (dynlen(dsPLC) == (UN_CONFIG_OPCUA_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        dsPLCLine = dsPLC;
        for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
            dynAppend(dsPLCAdditionalData, dsPLCLine[UN_CONFIG_OPCUA_LENGTH + 1]);
            dynRemove(dsPLCLine, UN_CONFIG_OPCUA_LENGTH + 1);
        }
        unConfigOPCUA_checkData(dsPLCLine, exceptionInfo);
        unConfigOPCUA_checkAdditionalData(dsPLCAdditionalData, exceptionInfo);

        // first check if the PLC name is correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_NAME];
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADPLCNAME"), "");
            return;
        }

        // check if the PLC Application Name is correct:
        tempDp = dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION];
        if (!unConfigGenericFunctions_nameCheck(tempDp)) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
            return;
        }

        // sPLCDp is UNOPC_plcName
        sPLCDp = getSystemName() +  c_unSystemIntegrity_UNOPCUA + dsPLCLine[UN_CONFIG_PLC_NAME];
        sAliasDp = unGenericDpFunctions_dpAliasToName(dsPLCLine[UN_CONFIG_PLC_NAME]);

        if (substr(sAliasDp, strlen(sAliasDp) - 1) == ".") {
            sAliasDp = substr(sAliasDp, 0, strlen(sAliasDp) - 1);
        }
        if ((sAliasDp != "") && (sAliasDp != sPLCDp)) {
            if (dynContains(dsDeleteDps, sAliasDp) <= 0) {	// Alias exists, is not used by the "OPC" config datapoint and will not be deleted
                fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: " + dsPLCLine[UN_CONFIG_PLC_NAME] + getCatStr("unGeneration", "BADHOSTNAMEALIAS") + sAliasDp, "");
            }
        }
        if (dpExists(sPLCDp) && (dynContains(dsDeleteDps, sPLCDp) <= 0)) {
            sHostname = unGenericDpFunctions_getAlias(sPLCDp);
            iRes = dpGet(sPLCDp + ".communication.driver_num", iDriverNum);
            if (iRes >= 0) {
                if (iDriverNum != dsPLCAdditionalData[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: " + getCatStr("unGeneration", "BADEXISTINGPLCDRIVER") + iDriverNum, "");
                }
                if (sHostname != dsPLCLine[UN_CONFIG_PLC_NAME]) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: " + getCatStr("unGeneration", "BADEXISTINGPLCHOSTNAME"), "");
                }
            }
        }

        if (dsPLCLine[UN_CONFIG_UNOPCUA_SERVERNAME] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: OPCUA server name is empty. Example of the server name value: host:4148/UA_Server", "");
        }

        int iCorrectValue;
        dyn_string exceptionInfoTmp;
        unConfigGenericFunctions_checkInt(dsPLCLine[UN_CONFIG_UNOPCUA_RECONNECT_TIMER], iCorrectValue, exceptionInfoTmp);
        if (dynlen(exceptionInfoTmp) > 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: ReconnectTimer value should be an integer, value: " + dsPLCLine[UN_CONFIG_UNOPCUA_RECONNECT_TIMER], "");
            dynClear(exceptionInfoTmp);
        }
        unConfigGenericFunctions_checkInt(dsPLCLine[UN_CONFIG_UNOPCUA_QUEUE_SIZE], iCorrectValue, exceptionInfoTmp);
        if (dynlen(exceptionInfoTmp) > 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: QueueSize value should be an integer, value: " + dsPLCLine[UN_CONFIG_UNOPCUA_QUEUE_SIZE], "");
            dynClear(exceptionInfoTmp);
        }
        unConfigGenericFunctions_checkInt(dsPLCLine[UN_CONFIG_UNOPCUA_PUBLISHING_INTERVAL], iCorrectValue, exceptionInfoTmp);
        if (dynlen(exceptionInfoTmp) > 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: PublishingInterval value should be an integer, value: " + dsPLCLine[UN_CONFIG_UNOPCUA_PUBLISHING_INTERVAL], "");
            dynClear(exceptionInfoTmp);
        }

        unConfigOPCUA_checkAddress(dsPLCLine[UN_CONFIG_UNOPCUA_COUNTER], exceptionInfo);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkApplication: " + getCatStr("unGeneration", "BADINPUTS"), "");
    }
}

/**
Purpose: check PLC data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file
			dsPLC must contains :
			1. string, "QUANTUM" or "PREMIUM"
			2. string, name
			3. string, subApplication
			4. int, PLC address = PLC number
		exceptionInfo, dyn_string, output, for errors
*/
unConfigOPCUA_checkData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCNumber;

    if (dynlen(dsPLC) != UN_CONFIG_OPCUA_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        unConfigGenericFunctions_checkPLCType(dsPLC[UN_CONFIG_PLC_TYPE], exceptionInfo);

        if (strrtrim(dsPLC[UN_CONFIG_PLC_NAME]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkData:" + getCatStr("unGeneration", "ERRORPLCNAME"), "");
        }

        if (strrtrim(dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]) == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkData:" + getCatStr("unGeneration", "ERRORSUBAPPLI"), "");
        }

        // TODO check live counter address
    }
}

/**
Purpose: check additional data for configuration

Parameters:
		dsPLC: dyn_string, input, PLC additional data
			dsPLC must contains :
			1. unsigned, driver number
			2. string, boolean archive
			3. string, analog archive
		exceptionInfo, dyn_string, output, for errors
*/
unConfigOPCUA_checkAdditionalData(dyn_string dsPLC, dyn_string &exceptionInfo) {
    unsigned uPLCDriver;

    if (dynlen(dsPLC) != UN_CONFIG_PLC_ADDITIONAL_LENGTH) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAdditionalData:" + getCatStr("unGeneration", "BADINPUTS"), "");
    } else {
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAdditionalData: bool archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAdditionalData: analog archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
        if (dsPLC[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT] == "") {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAdditionalData: event archive" + getCatStr("unGeneration", "ERRORARCHIVEEMPTY"), "");
        }
    }
}

/**
Purpose: set PLC configuration for a sub application (not comm)

Parameters:
		dsPLC: dyn_string, input, PLC configuration line in file + additional parameters
			dsPLC must contains :
			(Plc config line)
			1. string, "QUANTUM" or "PREMIUM"
			2. string, PLC name
			3. string, PLC subapplication
			4. int, PLC address = PLC number
			(additional parameters)
			5. driver number
			6. boolean archive name
			7. analog archive name
		exceptionInfo: dyn_string, output, for errors
*/
unConfigOPCUA_setApplication(dyn_string dsPLCData, dyn_string &exceptionInfo) {
    int iPLCNumber, iRes, i, position;
    string sDpName, addressReference, tempDp;
    dyn_string dsPLC, dsAdditionalParameters, dsSubApplications, dsImportTimes, dsBoolArchives, dsAnalogArchives, exceptionInfoTemp;
    dyn_string dsEventArchives;
    // Check

    if (dynlen(dsPLCData) != (UN_CONFIG_OPCUA_LENGTH + UN_CONFIG_PLC_ADDITIONAL_LENGTH)) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_setApplication: " + getCatStr("unGeneration", "BADINPUTS"), "");
        return;
    }

    // 1. Separate config file info and additional parameters
    dsPLC = dsPLCData;
    dynClear(dsAdditionalParameters);
    for (i = 1; i <= UN_CONFIG_PLC_ADDITIONAL_LENGTH; i++) {
        dynAppend(dsAdditionalParameters, dsPLC[UN_CONFIG_OPCUA_LENGTH + 1]);
        dynRemove(dsPLC, UN_CONFIG_OPCUA_LENGTH + 1);
    }

    // check if the PLC Application Name is correct:
    tempDp = dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION];
    if (!unConfigGenericFunctions_nameCheck(tempDp)) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_setApplication: " + dsPLCData[UN_CONFIG_PLC_SUBAPPLICATION] + getCatStr("unGeneration", "BADPLCAPPLICATIONNAME"), "");
        return;
    }

    // add OPCUA driver if don't exist
    string driverDP = getSystemName() + "_OPCUA" + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM];
    if (!dpExists(driverDP)) {
        unConfigGenericFunctions_createDp(driverDP, "_OPCUA", exceptionInfoTemp);
        if (dynlen(exceptionInfoTemp) > 0) {
            dynAppend(exceptionInfo, exceptionInfoTemp);
            return;
        }
    }
    // Set PLC configuration in UnOPC
    sDpName = c_unSystemIntegrity_UNOPCUA + dsPLC[UN_CONFIG_PLC_NAME];
    tempDp = sDpName;
    unConfigGenericFunctions_createDp(sDpName, UNOPCUA_DPTYPE, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        return;
    }

    sDpName = getSystemName() + sDpName;
    unConfigGenericFunctions_setAlias(sDpName, dsPLC[UN_CONFIG_PLC_NAME], exceptionInfo);
    iRes = dpGet(sDpName + ".configuration.subApplications", dsSubApplications,
                 sDpName + ".configuration.importTime", dsImportTimes,
                 sDpName + ".configuration.archive_bool", dsBoolArchives,
                 sDpName + ".configuration.archive_analog", dsAnalogArchives,
                 sDpName + ".configuration.archive_event", dsEventArchives);
    position = dynContains(dsSubApplications, dsPLC[UN_CONFIG_PLC_SUBAPPLICATION]);
    if (position <= 0) {
        position = dynlen(dsSubApplications) + 1;
    }
    // if the event archive is empty, set it to bool archive
    if (dynlen(dsEventArchives) <= 0) {
        dsEventArchives = dsBoolArchives;
    }

    dsSubApplications[position] = dsPLC[UN_CONFIG_PLC_SUBAPPLICATION];
    dsImportTimes[position] = (string)getCurrentTime();
    dsBoolArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_BOOL];
    dsAnalogArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_ANALOG];
    dsEventArchives[position] = dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_ARCHIVE_EVENT];
    iRes = dpSetWait(sDpName + ".configuration.importTime", dsImportTimes,
                     sDpName + ".configuration.archive_bool", dsBoolArchives,
                     sDpName + ".configuration.archive_analog", dsAnalogArchives,
                     sDpName + ".configuration.archive_event", dsEventArchives,
                     sDpName + ".configuration.subApplications", dsSubApplications,
                     sDpName + ".communication.driver_num", dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM]);

    if (iRes < 0) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_setApplication: " + getCatStr("unPVSS", "DPSETFAILED"), "");
    }

    // 4. Create _UnSystemAlarm datapoints
    unSystemIntegrity_createSystemAlarm(tempDp,
                                        DS_pattern,
                                        getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION") + dsPLC[UN_CONFIG_PLC_NAME] + " -> DS driver " + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM],
                                        exceptionInfo);

    // 5. create OPCUAServer
    string opcuaServer = "_" + OPCUA_SERVER_PREFIX + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM];
    unConfigGenericFunctions_createDp(opcuaServer, "_OPCUAServer", exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        return;
    }
    dpSetWait(opcuaServer + ".Config.ConnInfo", "opc.tcp://" + dsPLC[UN_CONFIG_UNOPCUA_SERVERNAME],
              opcuaServer + ".Config.ReconnectTimer", dsPLC[UN_CONFIG_UNOPCUA_RECONNECT_TIMER],
              opcuaServer + ".Config.Separator", ".");

    string opcuaSub = "_" + OPCUA_SUBSCRIPTION_PREFIX + dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM];
    unConfigGenericFunctions_createDp(opcuaSub, "_OPCUASubscription", exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0) {
        dynAppend(exceptionInfo, exceptionInfoTemp);
        return;
    }
    dpSetWait(opcuaSub + ".Config.MonitoredItems.DataChangeFilter.Trigger", "1",
              opcuaSub + ".Config.MonitoredItems.DiscardOldest", "TRUE",
              opcuaSub + ".Config.MonitoredItems.QueueSize", dsPLC[UN_CONFIG_UNOPCUA_QUEUE_SIZE], // amount of queue slots in the server
              opcuaSub + ".Config.MonitoredItems.SamplingInterval", 0, // suggested "best effort" cycle's time of the server in ms // works inconsistent as server can ignore it
              opcuaSub + ".Config.RequestedPublishingInterval", dsPLC[UN_CONFIG_UNOPCUA_PUBLISHING_INTERVAL], // how often we recieve data from the server, in ms
              opcuaSub + ".Config.MonitoredItems.TimestampsToReturn", 0, // 0 - use "source" timestamp
              opcuaSub + ".Config.Priority", 0,
              opcuaSub + ".Config.PublishingEnabled", "TRUE",
              opcuaSub + ".Config.RequestedLifetimeCount", "100",  // amount of attemps to subscribe to the item before delete it
              opcuaSub + ".Config.RequestedMaxKeepAliveCount", "10",  // amount of cycles without data when data is not changed in the server
              opcuaSub + ".Config.SubscriptionType", "1" // locked to 1 = "data" in WinCCOA. WinCCOA driver doesn't support  data, 2 - "events" or 3 "alarms" subscription types
             );

    dpSetWait(opcuaServer + ".Config.Subscriptions", makeDynString(opcuaSub));
    dpSetWait(opcuaServer + ".Config.Active", true);

    unConfigOPCUA_setAddress(sDpName + ".communication.counter", dsPLC[UN_CONFIG_UNOPCUA_COUNTER], DPATTR_ADDR_MODE_INPUT_SPONT, dsAdditionalParameters[UN_CONFIG_PLC_ADDITIONAL_DRIVER_NUM], OPCUA_DEFAULT, exceptionInfo);
}


/** Function checks whether addressReference follows the OPCUA address format.
*/
unConfigOPCUA_checkAddress(string addressReference, dyn_string &exceptionInfo)
{
    if (addressReference == "") return;
    dyn_string addressParts = strsplit(addressReference, "|");
    if (dynlen(addressParts) != 3 && dynlen(addressParts) != 4) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAddress: address '" + addressReference + "' does not follow format NAMESPACE_INDEX|IDENTIFIER_TYPE|IDENTIFIER[|CONVERTION_DATATYPE]", "");
        return;
    }

    int iCorrectValue;
    dyn_string exceptionInfoTmp;
    unConfigGenericFunctions_checkInt(addressParts[1], iCorrectValue, exceptionInfoTmp);
    if (dynlen(exceptionInfoTmp) > 0) {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAddress: namespace is not an integer, value = " + addressParts[1] + ", address: " + addressReference, "");
        return;
    }
    if (addressParts[2] != "i" && addressParts[2] != "s" && addressParts[2] != "g" && addressParts[2] != "b") {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAddress: indentifier type should be one of {i,s,g,b}, value = " + addressParts[2] + ", address: " + addressReference, "");
        return;
    }
    if (addressParts[3] == "") {
        fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAddress: indentifier should not be empty, address: " + addressReference, "");
        return;
    }

    if (dynlen(addressParts) == 4) {
        unConfigGenericFunctions_checkInt(addressParts[4], iCorrectValue, exceptionInfoTmp);
        if (dynlen(exceptionInfoTmp) > 0) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAddress: convertion datatype is not an integer, value = " + addressParts[4] + ", address: " + addressReference, "");
            return;
        }
        if (iCorrectValue < 750 || iCorrectValue > 768) {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_checkAddress: convertion datatype should be in [750, 768] interval, value = " + addressParts[4] + ", address: " + addressReference, "");
            return;
        }
    }
}

unConfigOPCUA_setAddress(string dpe, string addressReference, int mode, mixed driverNum, int defaultConvertionDataType, dyn_string& exceptionInfo)
{
    if (addressReference == "") {
        fwPeriphAddress_delete(dpe, exceptionInfo);
    } else {
        string opcServerName = OPCUA_SERVER_PREFIX + driverNum;
        string opcSubscription = OPCUA_SUBSCRIPTION_PREFIX + driverNum;
        string opcKind = "1";
        string opcVariant = "1";
        string pollGroup = "";

        dyn_string addressParts = strsplit(addressReference, "|");
        string namespaceIndex = addressParts[1];
        string identifierType = addressParts[2];
        string identifier = addressParts[3];
        int convertionType = defaultConvertionDataType;
        if (dynlen(addressParts) == 4) {
            convertionType = (int)addressParts[4];
        }
        
        string opcItemName = "ns=" + namespaceIndex + ";" + identifierType + "=" + identifier;

        fwPeriphAddress_setOPCUA(dpe, opcServerName, driverNum,
                                 opcItemName, opcSubscription, opcKind, opcVariant, convertionType, mode, pollGroup, exceptionInfo);
    }
}


string DRV_OPCUA_convertToUnicosAddress(string sDeviceDpeName, string sDeviceType)
{
    int configType, convertionDatatype;
    mixed addr_ref;
    dpGet(sDeviceDpeName + ":_address.._type", configType,
        sDeviceDpeName + ":_address.._reference" , addr_ref,
        sDeviceDpeName + ":_address.._datatype", convertionDatatype);
    if (configType == DPCONFIG_NONE) { // if addr is not defined - return an empty string
        return "";
    }

    dyn_string referenceParts = strsplit(addr_ref, "$");
    string addr = referenceParts[5];
    // convert addr="ns=2;s=xxx" into result="2|s|xxx"
    dyn_string addrParts = strsplit(addr, ";");
    strreplace(addrParts[1], "ns=", "");
    dyn_string identifierParts = strsplit(addrParts[2], "=");
    string result = addrParts[1] + "|" + identifierParts[1] + "|" + identifierParts[2];

    // add convertion type
    result += "|" + convertionDatatype;

    return result;
}

/**
Purpose: delete Front-end config

Parameters :
	sPlcName, string, input, plc name
	exceptionInfo, dyn_string, output, for errors
*/
unConfigOPCUA_deleteFrontEnd(string sPlcName, dyn_string& exceptionInfo) {
    string sDpPlcName, sPlcModPlc = "", exceptionText;
    int i, length, iRes;
    dyn_string dsAlarmDps;


    sDpPlcName = c_unSystemIntegrity_UNOPCUA + sPlcName;
    if (!dpExists(sDpPlcName)) {
        sDpPlcName = "";
    }

    if (sDpPlcName != "") {
        if (unGenericDpFunctions_getSystemName(sDpPlcName) == getSystemName()) {
            string driver;
            dpGet(sDpPlcName + ".communication.driver_num", driver);

            // 2. Delete _Un_Plc
            iRes = dpDelete(substr(sDpPlcName, strpos(sDpPlcName, ":") + 1));
            if (iRes < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_deleteFrontEnd: " + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1) + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
            }
            // 3. Delete alarm system
            dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + "*" + substr(sDpPlcName, strpos(sDpPlcName, ":") + 1), c_unSystemAlarm_dpType);
            length = dynlen(dsAlarmDps);
            for (i = 1; i <= length; i++) {
                _unSystemIntegrity_modifyApplicationAlertList(dsAlarmDps[i], false, exceptionInfo);
                iRes = dpDelete(dsAlarmDps[i]);
                if (iRes < 0) {
                    fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_deleteFrontEnd: " + dsAlarmDps[i] + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
                }
            }
            // 4. delete OPCUA server / subs
            iRes = dpDelete("_" + OPCUA_SERVER_PREFIX + driver);
            if (iRes < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_deleteFrontEnd: " + OPCUA_SERVER_PREFIX + driver + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
            }
            iRes = dpDelete("_" + OPCUA_SUBSCRIPTION_PREFIX + driver);
            if (iRes < 0) {
                fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_deleteFrontEnd: " + OPCUA_SUBSCRIPTION_PREFIX + driver + getCatStr("unGeneral", "UNOBJECTNOTDELETED"), "");
            }
        } else {
            fwException_raise(exceptionInfo, "ERROR", "unConfigOPCUA_deleteFrontEnd: " + sDpPlcName + getCatStr("unGeneral", "UNOBJECTNOTDELETEDBADSYSTEM"), "");
        }
    }
}

/**
Purpose: return the list of Dps that wil be archived during the set of the OPC front-end

Parameters:
		sCommand: string, input, the current entry in the import file
		sDp: string, input, the given dp: PLC or SystemAlarm  Dp
		sFEType: string, input, the front-end type
		dsArchiveDp: dyn_string, output, list of archivedDp
*/
OPCUA_getFrontEndArchiveDp(string sCommand, string sDp, string sFEType, dyn_string &dsArchiveDp) {
    switch (sCommand) {
        case UN_PLC_COMMAND:
            dsArchiveDp = makeDynString();
            break;
        default:
            dsArchiveDp = makeDynString();
            break;
    }
}


OPCUA_Com_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo) {
    dyn_string dsDpParameters;
    dyn_string dsBoolArchive, dsAnalogArchive, dsEventArchive;
    string sPlcName, sPlcDrv;
    dyn_string dsTemp;

    if (dynlen(dsParam) == UN_CONFIG_EXPORT_LENGTH) {
        sPlcName = dsParam[UN_CONFIG_EXPORT_PLCNAME];
        dpGet(c_unSystemIntegrity_UNOPCUA + sPlcName + ".configuration.archive_bool", dsBoolArchive,
              c_unSystemIntegrity_UNOPCUA + sPlcName + ".configuration.archive_analog", dsAnalogArchive,
              c_unSystemIntegrity_UNOPCUA + sPlcName + ".configuration.archive_event", dsEventArchive,
              c_unSystemIntegrity_UNOPCUA + sPlcName + ".communication.driver_num", sPlcDrv);

        //Delete
        dynClear(dsDpParameters);
        dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
        dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
        dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

        //PLCCONFIG
        dynClear(dsDpParameters);
        dynAppend(dsDpParameters, UN_PLC_COMMAND);
        dynAppend(dsDpParameters, "OPCUA");
        dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
        dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);

        string opcuaServer = "_" + OPCUA_SERVER_PREFIX + sPlcDrv;
        string connInfo, reconnectTimer;
        dpGet(opcuaServer + ".Config.ConnInfo", connInfo, opcuaServer + ".Config.ReconnectTimer", reconnectTimer);
        strreplace(connInfo, "opc.tcp://", "");
        dynAppend(dsDpParameters, connInfo);
        dynAppend(dsDpParameters, reconnectTimer);

        string queueSize, samplingInterval, timestampsToReturn, priority;
        string opcuaSub = "_" + OPCUA_SUBSCRIPTION_PREFIX + sPlcDrv;
        dpGet(opcuaSub + ".Config.MonitoredItems.QueueSize", queueSize,
              opcuaSub + ".Config.MonitoredItems.SamplingInterval", samplingInterval,
              opcuaSub + ".Config.MonitoredItems.TimestampsToReturn", timestampsToReturn,
              opcuaSub + ".Config.Priority", priority);
        dynAppend(dsDpParameters, queueSize);
        dynAppend(dsDpParameters, samplingInterval);
        dynAppend(dsDpParameters, timestampsToReturn);
        dynAppend(dsDpParameters, priority);

        string counterAddr = DRV_OPCUA_convertToUnicosAddress(c_unSystemIntegrity_UNOPCUA + sPlcName + ".communication.counter", UNOPCUA_DPTYPE);
        dynAppend(dsDpParameters, counterAddr);

        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    } else {
        fwException_raise(exceptionInfo, "ERROR", "OPC_Com_ExportConfig(): Wrong Parameters", "");
    }

}

/**
Purpose: return the driver config of OPCUA front-end

Parameters:
        iDriverNumber:      int,        input,  the driver number
        sName:                  string,         output, the name to display
        sDriverName:            string,         output,     the manager name
        iManType:   int, output, the manager type
        sCommandLine:   int, output, the command line to start the manager
*/
OPCUA_getFrontEndManagerConfig(int iDriverNumber, string &sName, string &sDriverName, int &iManType, string &sCommandLine)
{
  sName = "OPCUA";
  sDriverName = "WCCOAopcua";
  iManType = DRIVER_MAN;
  sCommandLine = "-num "+iDriverNumber;
}

