/**@file

// moniDIM.ctl
This library contains the widget, faceplate, etc. functions of DIM.

@par Creation Date
  dd/mm/yyyy

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  the author (DEP-GROUP)
*/

private const bool unDIM_fwDIMLibLoaded = fwGeneral_loadCtrlLib("fwDIM/fwDIM.ctl",false);

//@{

//------------------------------------------------------------------------------------------------------------------------
// DIM_ObjectListGetValueTime
/** Function called from snapshot utility of the treeDeviceOverview to get the time and value
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName  input, device name
@param sDeviceType input, device type
@param dsReturnData output, return data, array of 5 strings
*/
DIM_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  string sTime="0", sSystemName, sFrontEnd;
  int iCom;
  bool bEnable;
  string sWarningLetter, sWarningColor, sBodyColor="unDataNoAccess", sText = "???", sCompleteText;
  dyn_string dsColor;
  
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(sDeviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(sDeviceName);
  if(dpExists(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm")) {
    dpGet(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm", iCom,
          sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".enabled", bEnable,
          sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm:_online.._stime", sTime);
    unGenericObject_getFrontEndColorState(makeDynString(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm"), dsColor);
    unGenericObject_getFrontState(makeDynString("COM ERR"),
                                  makeDynString(dsColor[1]), 
                                  makeDynInt(iCom), 
                                  iCom, bEnable, sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
    
    /*** if there are more than one systemIntegrity alarm
    unGenericObject_getFrontEndColorState(makeDynString(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm",
                                                        sSystemName+c_unSystemAlarm_dpPattern+PM_STATE_pattern+sFrontEnd+".alarm",
                                                        sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm"), dsColor);
    // give as a dyn_string the list of possible text, the selected one and the color will be the one with the highest alarm value
    unGenericObject_getFrontState(makeDynString("COM ERR", "PM ERR", "TIME ERR"),
                                  makeDynString(dsColor[1], dsColor[2], dsColor[3]), 
                                  makeDynInt(3*iCom, 2*iPM, iTime), 
                                  iCom, bEnable, sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
      
      */
  }
  
  dsReturnData[1] = sTime;
  dsReturnData[2] = sText;
  dsReturnData[3] = "FALSE";
  dsReturnData[4] = sCompleteText;
  dsReturnData[5] = sBodyColor;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "moni snapshot DIM "+sDeviceName, "DIM_ObjectListGetValueTime", sDeviceName, dsReturnData);
}

//---------------------------------------------------------------------------------------------------------------------------------------
// DIM_MenuConfiguration
/** pop-up menu
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName input, device DP name
@param sDpType input, device type
@param dsAccessOk input, the access control 
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function
*/
DIM_MenuConfiguration(string sDpName, string sDpType, dyn_string dsAccessOk, dyn_string &menuList)
{
  // return the common menu list
  _UnPlc_MenuConfiguration(sDpName, sDpType, dsAccessOk, menuList);
  // add any other menu choice
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_HandleMenu
/** handle the answer of the popup menu
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName input, device DP name
@param sDpType input, device type
@param menuList input, the access control 
@param menuAnswer input, selected menu value 
*/
DIM_HandleMenu(string sDpName, string sDpType, dyn_string menuList, int menuAnswer)
{
  // handle the action of common menu list
  _UnPlc_HandleMenu(sDpName, sDpType, menuList, menuAnswer);
  // handle all the other action of menu list
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_WidgetRegisterCB
/** widget DistributedControl callback
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/
DIM_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction;
  dyn_string exceptionInfo;
  string sFrontEnd, sSystInt;
  bool bRemote;
  
  // get the device DP name from the $-param $sIdentifier of the widget
  deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
  if (deviceName == "")  // In case of disconnection
  {
    deviceName = g_sDpName;
  }
  g_bUnSystemAlarmPlc = false;
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);

  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;
    
  if(sFrontEnd != ""){
    sSystInt = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
//DebugN(sSystemName, sFrontEnd);
    if(dpExists(sSystInt)) {
      g_bUnSystemAlarmPlc = true;
    }
  }

  // check the state of the system
  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "moni widget DIM "+deviceName, "DIM_WidgetRegisterCB", sDp, deviceName, g_bCallbackConnected, iAction);
  switch(iAction)
    {
    case UN_ACTION_DISCONNECT:
      DIM_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPCONNECT:
      g_sDpName = deviceName;
      DIM_WidgetConnect(deviceName, sFrontEnd, sSystInt);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      DIM_WidgetDisconnect(deviceName, sFrontEnd, sSystInt);
      DIM_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_WidgetDisconnection
/** set the widget when the system considered disconnected 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sWidgetType input, the type of widget
*/
DIM_WidgetDisconnection(string sWidgetType)
{
  /** multiple widget is not allowed for front-end **/
  _UnPlc_WidgetDisconnection(sWidgetType);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "moni widget DIM", "DIM_WidgetDisconnection", sWidgetType);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_WidgetConnect
/** dpConnect to the front-end device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name
@param sSystInt input, front-end device system integrity DP
*/
DIM_WidgetConnect(string deviceName, string sFrontEnd, string sSystInt)
{
  int iRes;
  string sSystemName;
  dyn_string dsColor;
  
//  check if the device name is of type "DIM" if not like of no front-end. -> not available.
  if(dpTypeName(deviceName) != "DIM")
    g_bUnSystemAlarmPlc = false;
  
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "moni widget DIM "+deviceName, "DIM_WidgetConnect", deviceName, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc)
  {
    // set all widget global variable usefull for the callback, e.g.: description, format, unit, etc.
    unGenericObject_getFrontEndColorState(makeDynString(sSystInt+".alarm"), dsColor);
    g_sCommColor = dsColor[1];
    iRes = dpConnect("DIM_WidgetCB", sSystInt+".alarm", sSystInt+".enabled");
    g_bCallbackConnected = (iRes >= 0);    
  }else{
    DIM_WidgetDisconnection(g_sWidgetType);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_WidgetDisconnect
/** dpDisconnect to the front-end device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name
@param sSystInt input, front-end device system integrity DP
*/
DIM_WidgetDisconnect(string deviceName, string sFrontEnd, string sSystInt)
{
  int iRes;
  string sSystemName;

  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "moni widget DIM "+deviceName, "DIM_WidgetDisconnect", deviceName, g_bUnSystemAlarmPlc);
  if(g_bUnSystemAlarmPlc)
  {
    iRes = dpDisconnect("DIM_WidgetCB", sSystInt+".alarm", sSystInt+".enabled");
    g_bCallbackConnected = !(iRes >= 0);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_WidgetCB
/** callback function on the front-end device data
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@reviewed 2018-06-22 @whitelisted{Callback}

@param sDpSystemIntegrityAlarmValue input, front-end device system integrity alarm DPE 
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param sDpSystemIntegrityAlarmEnabled input, front-end device system integrity enable DPE
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value
*/
DIM_WidgetCB(string sDpSystemIntegrityAlarmValue, int iFESystemIntegrityAlarmValue, 
                    string sDpSystemIntegrityAlarmEnabled, bool bFESystemIntegrityAlarmEnabled)
{
  /** multiple widget is not allowed for front-end **/
  // encapsulate all the setValue or function call with the following if(g_bSystemConnected)
  string sWarningLetter, sWarningColor, sBodyColor="white", sText = "???", sCompleteText;
  
  if(g_bSystemConnected) {
    unGenericObject_getFrontState(makeDynString("COM ERR"),
                                  makeDynString(g_sCommColor),
                                  makeDynInt(iFESystemIntegrityAlarmValue), 
                                  iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled,
                                  sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
    /*** if there are more than one systemIntegrity alarm
    unGenericObject_getFrontState(makeDynString("COM ERR", "PM ERR", "TIME ERR"),
                                  makeDynString(g_sCommColor, g_sPMColor, g_sTimeColor),
                                  makeDynInt(3*iFESystemIntegrityAlarmValue, 2*iAlarmPM, iAlarmTime), 
                                  iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled,
                                  sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
    */
    setMultiValue("WidgetArea", "visible", true, 
                  "WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor, 
                  "Body1", "foreCol", sBodyColor, "Body1", "text", sText);
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "moni widget DIM "+sDpSystemIntegrityAlarmValue, "DIM_WidgetCB", sDpSystemIntegrityAlarmValue, 
                               iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled, g_bSystemConnected,
                               sWarningLetter, sText);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_FaceplateRegisterCB
/** faceplate DistributedControl callback
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name
*/
DIM_FaceplateRegisterCB(string sDp, bool bSystemConnected)
{
  _UnPlc_FaceplateRegisterCB(sDp, bSystemConnected);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// DIM_FaceplateDisconnection
/** set the faceplate when the system considered disconnected 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
DIM_FaceplateDisconnection()
{
  _UnPlc_FaceplateDisconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------

//@}
