/**@name LIBRARY: unTreeDeviceOverview.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 11/09/2007

Modification History: 
  24/11/2011: Herve
  - IS-675: UNICOS scripts: system name added in the MessageText log 
 
	18/12/2007: Herve
		- add function to write subsystem1 (domai), subsystem2 (nature), FE characterstics.
		
	08/10/2007: Herve
		- new parameter to unDeviceListUpdate_writeFile

	26/09/2007: Herve
		- file name configurable when writing file
		
	22/09/2007: Herve
		- add MessageText

version 1.1

External Functions: 

Internal Functions: 

Purpose: 
This library contains the functions to be get and write the device list data to file.

Usage: Public

PVSS manager usage: WCCOActrl, WCCOAui

Constraints:
	. global variable used in the include files
	. PVSS version: 3.0 
	. operating system: WXP and Linux
	. distributed system: yes
*/

// global variable

dyn_string g_dsDeviceType;
mapping g_m_bRegistered;
string 	g_sFunctionTriggerUpdate;

//@{

//--------------------------------------------------------------------------------------------------------------------------------
//unDeviceListUpdate_register
/**
Purpose: register each defined system to create the device list

Parameters:
	@param sFunction: string, input, the CB function name

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_register(string sFunction)
{
	int i, len;
	dyn_string dsSystemNames, dsHost, exInfo;
	dyn_int diSystemId, diPort;
	bool bRes, bConnected;
	dyn_dyn_string ddsTemp;

/*-------------------------------------------------------------------------------------------*/
// get the list of the systems connected to the system  
/*-------------------------------------------------------------------------------------------*/
	unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemId, dsHost, diPort);
	len = dynlen(dsSystemNames);

	for(i=1;i<=len;i++) { if(strpos(dsSystemNames[i], ":") < 0) dsSystemNames[i] = dsSystemNames[i]+":"; }

	
	// UNCORE-30; handle reconfigurations of unDistributed
	// firstly process those that were removed
	for (i=1;i<=dynlen(g_dsTreeDeviceOverviewRegisteredSystem);i++) {
	    string sysName=g_dsTreeDeviceOverviewRegisteredSystem[i];
	    if (!dynContains(dsSystemNames,sysName)) {
		// remove it...
		unDistributedControl_deregister(sFunction, bRes, bConnected, sysName, exInfo);
		dynRemove(g_dsTreeDeviceOverviewRegisteredSystem,i);
		i--; // we removed from the list on which we iterate, hence readjust the iterator variable
		mappingRemove(g_m_bTreeDeviceOverviewRegisteredSystem,sysName);
		mappingRemove(g_m_bTreeDeviceOverviewSystemConnected,sysName);
		mappingRemove(g_m_ddsTreeDeviceOverviewDeviceList,sysName);
		mappingRemove(g_m_bTreeDeviceOverviewRegisteredSystemInitialized,sysName);
		mappingRemove(g_m_bTreeDeviceOverview_gettingDeviceRunning,sysName);
	    }
	}
	// ADD NON-EXISTING ONES
	dyn_string dsToAdd;
	for(i=1;i<=len;i++) {
		// skip already existing ones
		string sysName=dsSystemNames[i]; 
		if (dynContains(g_dsTreeDeviceOverviewRegisteredSystem,sysName)) continue;
		dynAppend(dsToAdd,                                 sysName);
		dynAppend(g_dsTreeDeviceOverviewRegisteredSystem,  sysName);
		g_m_bTreeDeviceOverviewRegisteredSystem           [sysName] = false;
		g_m_bTreeDeviceOverviewSystemConnected            [sysName] = false;
		g_m_ddsTreeDeviceOverviewDeviceList               [sysName] = ddsTemp;
		g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sysName] = false;
		g_m_bTreeDeviceOverview_gettingDeviceRunning      [sysName] = false;
	}
	for(i=1;i<=dynlen(dsToAdd);i++) {
		unDistributedControl_register(sFunction, bRes, bConnected, dsToAdd[i], exInfo);
//DebugN(bRes, bConnected, dsSystemNames[i]);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
//unDeviceListUpdate_registerTriggerUpdate
/**
Purpose: register each defined system for the triggerUpdate DPE in order to create the device list if needed

Parameters:
	@param sFunction: string, input, the CB function name for triggerUpdate

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_registerTriggerUpdate(string sFunction)
{
	int i, len;
	dyn_string dsSystemNames, dsHost, exInfo;
	dyn_int diSystemId, diPort;
	bool bRes, bConnected;

/*-------------------------------------------------------------------------------------------*/
// get the list of the systems connected to the system  
/*-------------------------------------------------------------------------------------------*/
	g_sFunctionTriggerUpdate = sFunction;
	unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemId, dsHost, diPort);
	len = dynlen(dsSystemNames);
	for(i=1;i<=len;i++) {
		if(strpos(dsSystemNames[i], ":") < 0)
			dsSystemNames[i] = dsSystemNames[i]+":";
		g_m_bRegistered[dsSystemNames[i]] = false; 
	}
	for(i=1;i<=len;i++) {
		unDistributedControl_register("unDeviceListUpdate_registerTriggerUpdateCB", bRes, bConnected, dsSystemNames[i], exInfo);
//DebugN(bRes, bConnected, dsSystemNames[i]);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_registerTriggerUpdateCB
/**
Purpose:
Distributed control register callback function dpConnect/dpDisconnect the triggerUpdate

Parameters:
	@param sDpe: string, input, the distributed control DPE of a PVSS system
	@param bSystemConnected: bool, input, state of the system

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
  
  @reviewed 2018-06-25 @whitelisted{Callback}
*/
unDeviceListUpdate_registerTriggerUpdateCB(string sDpe, bool bSystemConnected)
{
	string sSystemName;
	string sDp = dpSubStr(sDpe, DPSUB_DP);
	bool bRemote, localConnected;
	int iRes;
	
// get the system name and check if it is local or remote
	sSystemName = substr(sDp, strlen(c_unDistributedControl_dpName), strlen(sDp))+":";
	unDistributedControl_isRemote(bRemote, sSystemName);
//DebugN("unTreeDeviceOverview_register", g_m_bTreeDeviceOverviewRegisteredSystem, sDp, bSystemConnected, bRemote, sSystemName, mappingHasKey(g_m_bTreeDeviceOverviewRegisteredSystem, sSystemName));
	if(mappingHasKey(g_m_bRegistered, sSystemName)) {
//DebugN("has key");
		if(bRemote)
			localConnected = bSystemConnected;
		else
			localConnected = true;
		if(localConnected) { //the system is connected
	// if the system is not already connected add it: trigger the display cb
	// else do nothing
			if(!g_m_bRegistered[sSystemName]) {
				iRes = dpConnect(g_sFunctionTriggerUpdate, false, sSystemName+"_unApplication.triggerUpdate");
				g_m_bRegistered[sSystemName] = (iRes >=0);
			}
		}
		else { // the system is disconnected
	// if the system is connected delete it: trigger the display cb
	// else do nothing
			if(g_m_bRegistered[sSystemName]) {
				iRes = dpDisconnect(g_sFunctionTriggerUpdate, sSystemName+"_unApplication.triggerUpdate");
				g_m_bRegistered[sSystemName] = false;
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_writeFile
/**
Purpose:
Write the device list into a file

Parameters:
	@param sSystemName: string, input, the system name
	@param ddsDevice: dyn_dyn_string, input, device data
	@param sFileNameSuffix: string, input, file name suffix def="_TreeDeviceOverview_deviceList.dat"

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_writeFile(string sSystemName, dyn_dyn_string ddsDevice, string sSender, string sDescription, string sFileNameSuffix = "_TreeDeviceOverview_deviceList.dat")
{
	int i, len, len2, i2;
	file fFile;
	string sFileName;
	string sBuffer;
	string sFileSystemName = sSystemName;
	dyn_string exceptionInfo;
	
	strreplace(sFileSystemName, ":", "");
	unMessageText_send("*", "*", getSystemName()+sSender, "user", "*", "EXPERTINFO", 
				sDescription +sFileSystemName, exceptionInfo);
	sFileName = getPath(DATA_REL_PATH)+"_"+sFileSystemName+sFileNameSuffix;
//DebugTN("*** start unDeviceListUpdate_writeFile", sSystemName, sFileName);
	fFile = fopen(sFileName, "w");
//DebugN(fFile, PROJ_PATH);
	len = dynlen(ddsDevice);
	if((len > 0) && (fFile != 0))
	{
		for(i=1;i<=len;i++) {
			len2 = dynlen(ddsDevice[i]);
			sBuffer = "";
			for(i2=1;i2<=len2;i2++) {
				sBuffer += strrtrim(ddsDevice[i][i2])+";";
			}
			fprintf(fFile, "%s\n", sBuffer);
		}
	}
	fclose(fFile);
//DebugTN("*** end unDeviceListUpdate_writeFile", sSystemName);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_triggerHMI
/**
Purpose:
Trigger the HMI interface to update their internal list of device data

Parameters:
	@param iCommand: int, input, the command to execute
	@param sSystemName: string, input, the system name

Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_triggerHMI(int iCommand, string sSystemName)
{
//DebugN("!!!!!!!!! unDeviceListUpdate_triggerHMI", iCommand, sSystemName);
	dpSet("_unApplication.deviceList.command", iCommand, "_unApplication.deviceList.systemName", sSystemName);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_compareDeviceList
/**
Purpose:
Compare two device data

Parameters:
	@param dds1: dyn_dyn_string, input, device data 1
	@param dds2: dyn_dyn_string, input, device data 2
	@param returnValue: false=the device data are identical/false=the device data are different
	
Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
bool unDeviceListUpdate_compareDeviceList(dyn_dyn_string dds1, dyn_dyn_string dds2)
{
	int i, len=dynlen(dds1);
	bool bResult = true;
	
//DebugTN("*** start unDeviceListUpdate_compareDeviceList", len, dynlen(dds2));

	if(len == dynlen(dds2)) {
		for(i=1;(i<=len) && (bResult);i++) {
			if(dds1[i]!=dds2[i]) 
				bResult = false;
		}
	}
	else
		bResult = false;
		
//DebugTN("*** end unDeviceListUpdate_compareDeviceList", bResult);
	return bResult;
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_writeSubsystemToFile()
/**
Purpose:
Write subsystem1 and subsystem2 list to file

Parameters:
	@param sSystemName: string, input, the system name
	@param sSender: string, input, the sender
	@param sDescription: string, input, description
	
Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_writeSubsystemToFile(string sSystemName, string sSender, string sDescription)
{
	dyn_string dsDomain, dsNature;
	dyn_dyn_string dds;
	
	// get subsystem1 and subsystem2 for sSystem
	unGenericDpFunctions_getDomainNatureListFromSystem(sSystemName, dsDomain, dsNature);
	dds[1]=dsDomain;
	dds[2]=dsNature;
	unDeviceListUpdate_writeFile(sSystemName, dds, sSender, sDescription, TREE_DEVICE_FILE_SUBSYSTEM);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_writeFECharacteristicsToFile()
/**
Purpose:
Write FE characteritics (FE, FE DP, FE type and FE application) to file

Parameters:
	@param sSystemName: string, input, the system name
	@param sSender: string, input, the sender
	@param sDescription: string, input, description
	
Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_writeFECharacteristicsToFile(string sSystemName, string sSender, string sDescription)
{
	dyn_dyn_string ddsTemp;
	dyn_string dsConfiguredFrontEndType;
	dyn_string dsFrontEnd, dsFrontEndDp, dsFrontEndType, dsTempApplication;
	int lenApp, iApp;
	
	unGenericDpFunctions_getFrontEndDeviceType(dsConfiguredFrontEndType);
	_unTreeDeviceOverview_getFrontEndName(dsConfiguredFrontEndType, sSystemName, dsFrontEnd, dsFrontEndDp, dsFrontEndType);
//DebugN("!!!!", dynlen(dsFrontEnd), dynlen(dsFrontEndDp), dynlen(dsFrontEndType));
	lenApp = dynlen(dsFrontEnd);
	for(iApp=1;iApp<=lenApp;iApp++) {
		dsTempApplication = makeDynString();
		unTreeDeviceOverview_getApplication(sSystemName, dsFrontEnd[iApp], dsTempApplication);
		dynInsertAt(dsTempApplication, dsFrontEndType[iApp],1);
		dynInsertAt(dsTempApplication, dsFrontEndDp[iApp],1);
		dynInsertAt(dsTempApplication, dsFrontEnd[iApp],1);
		ddsTemp[iApp] = dsTempApplication;
	}
//DebugTN("write write FE", sSystemName);
	unDeviceListUpdate_writeFile(sSystemName, ddsTemp, sSender, sDescription, TREE_DEVICE_FILE_FEAPPLICATION);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unDeviceListUpdate_writeSubsystemAndFECharacteristicsToFile()
/**
Purpose:
Write FE characteritics, subsystem1 and subsystem2 list to file

Parameters:
	@param sSystemName: string, input, the system name
	@param sSender: string, input, the sender
	@param sDescriptionSubystem: string, input, description for subsystem write
	@param sDescriptionFE: string, input, description for FE characteristics write
	
Usage: Public

Constraints:
	. PVSS version: 3.0 
	. PVSS manager type: WCCOActrl, WCCOAui
	. operating system: WXP and Linux
	. distributed system: yes
*/
unDeviceListUpdate_writeSubsystemAndFECharacteristicsToFile(string sSystemName, string sSender, string sDescriptionSubystem, string sDescriptionFE)
{
	unDeviceListUpdate_writeSubsystemToFile(sSystemName, sSender, sDescriptionSubystem);
	unDeviceListUpdate_writeFECharacteristicsToFile(sSystemName, sSender, sDescriptionFE);
}

//-------------------------------------------------------------------------------------------------------------------------

//@}

