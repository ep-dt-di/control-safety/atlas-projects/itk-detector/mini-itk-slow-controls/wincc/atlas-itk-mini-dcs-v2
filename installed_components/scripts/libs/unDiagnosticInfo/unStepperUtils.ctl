/**
 * @file
 * @brief Utilities for Stepper Diagnostics panels
 * @author rafael.j.f.santos@cern.ch
 */

const char CSV_FILE_DELIMITER = "@";


/**
 * @brief Get diagnostics information for given alias
 * @param[in] sAlias Alias name
 * @param[in] sApplicationName Name of the application
 * @param[in] expectedLength Expected number of columns
 * @return (see unStepperInfo_getFileInfo)
 */
dyn_string unStepperInfo_getDiagnosticsInfo(const string &sAlias, 
                                            const string &sApplicationName, 
                                            const int iExpectedLength){
    string sFileName = unStepperInfo_getFileName(sAlias);
    return unStepperInfo_getFileInfo(sFileName, sApplicationName, iExpectedLength);
}


/**
 * @brief Reads stepper information data from files in "panels/vision/diagnostic/appname/*"
 * @param[in] sFileName Name of the file
 * @param[in] sApplicationName Name of the application
 * @param[in] expectedLength Expected length of each row
 * @return Data from file as a dynamic string. Each element is a row
 */
dyn_string unStepperInfo_getFileInfo(const string &sFileName, 
                                     const string &sApplicationName, 
                                     const int iExpectedLength){
    string sFileString;    
    dyn_string dsLines;
    // Read file
    string sPath = getPath(PANELS_REL_PATH, "vision/diagnostic/" + sApplicationName + "/");
    if (!fileToString(sPath + sFileName, sFileString)){
        unStepperInfo_raiseAndDisplayError("Error reading file");
        return dsLines;
    };
    // Split lines
    dsLines = strsplit(sFileString, "\n");
    // Check that number of columns is ok in every row
    unStepperInfo_validateColumnsNumber(dsLines, iExpectedLength);

    return dsLines;
}


/**
 * @brief Checks if each line has expected number of columns and raises fwException in case
 *        this doesn't happen
 * @param[in] dsLines List of lines to be validated
 * @param[in] expectedLength Expected length of each row
 */
void unStepperInfo_validateColumnsNumber(const dyn_string &dsLines, const int iExpectedLength) {
    dyn_string dsLineItems;
    string sLine;
    string sErrorMsg;
    for (int j=1; j <= dynlen(dsLines); j++){
        // Get right and left trimmed line
        sLine = strrtrim(strltrim(dsLines[j]), "\r");
        if (sLine != ""){
            dsLineItems = strsplit(sLine, CSV_FILE_DELIMITER);
            if (dynlen(dsLineItems) != iExpectedLength) {
                sprintf(sErrorMsg, "Row %d has invalid number of columns", j);
                unStepperInfo_raiseAndDisplayError(sErrorMsg);
            }
        }
    }
}


/**
 * @brief Raises error and displays message in pop up window
 * @param[in] errorMsg Error message to display
 */
void unStepperInfo_raiseAndDisplayError(const string &errorMsg){
    dyn_string dsExceptionInfo;
    fwException_raise(dsExceptionInfo, "ERROR", errorMsg, "");
    fwExceptionHandling_display(dsExceptionInfo);   
}


/**
 * @brief Get name of the file where states or transitions data of a given alias is stored
 * @details Computes .csv file name as given by alias name without trailing digits
 * @param[in] Alias name
 * @return Name of the csv file
 */
string unStepperInfo_getFileName(const string &sAliasName) {
    // Find ending index, by ignoring trailing digits
    int endIndex =strlen(sAliasName) - 1;
    while(unStepperInfo_isDigit(sAliasName[endIndex]) && endIndex > 0)
        endIndex--;

    return substr(sAliasName, 0, endIndex + 1) + ".csv";
}


/**
 * @brief Checks if a character is a digit
 * @param[in] char Character to be checked
 * @return TRUE if character is digit, FALSE otherwhise
 */
bool unStepperInfo_isDigit(const char c) {
    if ((c>='0') && (c<='9')) return TRUE;
    return FALSE;
}


/**
 * @brief Adds HTML code such that input string gets word-wrapping
 * @param[in] sInput Alias name
 * @return Word-wrapped input
 */
string unStepperInfo_HTMLWrap(const string &sInput){
    return "<div style=\"word-break: break-all; word-wrap: break-word;\">" 
            + sInput 
            + "</div>";
}
