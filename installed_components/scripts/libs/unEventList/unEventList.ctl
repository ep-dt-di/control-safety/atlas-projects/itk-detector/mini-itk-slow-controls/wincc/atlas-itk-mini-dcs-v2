
#uses "ctrlEventList"


//--------------------------------------------------------------------------------------------------------------------------------

const int CTLREVENTLIST_VERSION = 4; //DLL version for Objects and Events List - update 29/03/06 (add function to check Alias filter)

//------------
// EVENT LIST
//------------

const string UN_EVENT_LIST_RAISING_EDGE = "R"; // Display raising edge, falling edge or all edges
const string UN_EVENT_LIST_FALLING_EDGE = "F";
const string UN_EVENT_LIST_ALL_EDGES = "A";

// Herve: 03/03/2003: modify allowDisplay, use patternMatch instead of strpos
const int UN_EVENT_LIST_PERIOD = 600;                    // In seconds, the query time period
const string UN_EVENT_LIST_DELIMITER = ";";              // Delimiter in g_tableHistory
const string UN_EVENT_LIST_CONFIGURATION_DELIMITER = ";";// Delimiter in configuration DPEs
const string UN_EVENT_LIST_FILTER_DELIMITER = "~";       // Delimiter in filter string
const string UN_EVENT_LIST_FILTER_COMBO_DELIMITER = ";"; // Delimiter in filter combobox
const string UN_EVENT_LIST_WAIT_PANEL_NAME = "<<Event List>>";
const string UN_EVENT_LIST_LIST_NAME = "List...";
const string UN_EVENT_LIST_FROM_PATTERN="Pattern...";

const int UN_EVENT_LIST_FIELD = 12;     // 12 fields for history
const int UN_EVENT_LIST_FIELD_SYSTEM=1;
const int UN_EVENT_LIST_FIELD_TIME = 2;
const int UN_EVENT_LIST_FIELD_ALIAS = 3;
const int UN_EVENT_LIST_FIELD_DESCRIPTION = 4;
const int UN_EVENT_LIST_FIELD_DOMAIN = 5;
const int UN_EVENT_LIST_FIELD_NATURE = 6;
const int UN_EVENT_LIST_FIELD_BITNAME = 7;
const int UN_EVENT_LIST_FIELD_EVENT = 8;
const int UN_EVENT_LIST_FIELD_INVALID = 9;
const int UN_EVENT_LIST_FIELD_OBJECT = 10;
const int UN_EVENT_LIST_FIELD_COLOR = 11;
const int UN_EVENT_LIST_FIELD_APPLICATION = 12;

const int UN_EVENT_LIST_FILTER_FIELD = 8;             // 8 fields for filter
const int UN_EVENT_LIST_FILTER_FIELD_ALIAS = 1;
const int UN_EVENT_LIST_FILTER_FIELD_DESCRIPTION = 2;
const int UN_EVENT_LIST_FILTER_FIELD_DOMAIN = 3;
const int UN_EVENT_LIST_FILTER_FIELD_NATURE = 4;
const int UN_EVENT_LIST_FILTER_FIELD_BITNAME = 5;
const int UN_EVENT_LIST_FILTER_FIELD_EVENT = 6;
const int UN_EVENT_LIST_FILTER_FIELD_INVALID = 7;
const int UN_EVENT_LIST_FILTER_FIELD_OBJECT = 8;

const string UN_EVENT_LIST_FILTER_DEFAULT = "*~*~*~*~*~*~*~*";
const string UN_EVENT_LIST_COLOR_DEFAULT = "#Normal#";

const string UN_EVENT_LIST_TEXTINVALIDDATA = "Invalid data";
const string UN_EVENT_LIST_TEXTVALIDDATA = "Valid data";

const string UN_GRANTED_REJECTED_DELIMITER = "GRANTED-REJECTED";


// mapping 	g_m_dsSubApplications;			//Applications list
// dyn_string			g_dsSubApplications;
// dyn_string 			g_tableHistory;						// To save dpQuery results
// dyn_string 			g_tableDisplay;						// To save dpQuery results
// int 		g_iLinesMax;											// Maximum displayed lines
// int 		g_iEvents;												// Events number
// int 		g_iLastDisplayedLine;							// Index in g_tableDisplay containing last displayed line
// int 		g_iFirstDisplayedLine;						// Index in g_tableDisplay containing first displayed line
// string 	g_sFilter;												// Display filter
// string 	g_sConfigSystemName;							// System name that contains event list configuration (set during initialization)
// string 	g_filterAll;
// string	g_sFrom;
// string	g_sOldFrom;
// string	g_sOldSystemList;
// bool 		g_bUTC, g_bGranted;
// int			g_iFilter;
// int			g_iSelected_Line;
// long 		g_qStart;
// time 		g_OldrequestBegin, g_OldrequestEnd;			// Old Begin and End time for historic request
// string  g_sColor_Line;
// 
// mapping g_bSystemState, g_bOldSystemState;
// 
// bool 		viewSystem, viewTime, viewTimeUTC, viewAlias, viewDescription, viewDomain, viewNature, viewBit, viewEvent, viewInvalid, viewObject, viewApplication;
// bool 		g_bEvent16 = true;
// string 	g_sDPEPattern = UN_DEFAULT_EVENTLIST_DPE;
// string g_sPanelFileName;
// dyn_string g_dsOldSelectedApplication, g_dsSelectedApplication, g_dsOldSelectedDeviceType, g_dsSelectedDeviceType;
// dyn_string g_dsUnicosObjects;
// mapping g_mDPELastValue00, g_mDPELastValue16;
// bool g_bRemoveUnknowState=false;
// int g_iMaxEventBeforeWarning = 5000;
// mapping g_m_dsNames, g_m_diPositions, g_m_dsChanges, g_m_dsColors;
// dyn_string g_dsExtraEventEdgeFilter; //containg extra filter values for the filter option "Event" (edge)


//-------------------------------------------------------------------------------------------
unEventList_panel_checkFilter()
{
  string sColorBad = "{255,154,52}";
  string sColorOk = "_Window";
  dyn_string dsShapeText = makeDynString("application_list", "filter_Object", "filter_Alias", "filter_Description",
                                         "filter_Invalid", "filter_Domain", "filter_Nature", "filter_Bit",
                                         "filter_Event");
  dyn_string dsMethod = makeDynString("selectedText", "selectedText", "selectedText", "text",
                                         "selectedText", "selectedText", "selectedText", "selectedText",
                                         "selectedText");
  int i, len;
  string sSelectedText, sColor;
  
  len = dynlen(dsShapeText);
  for(i=1;i<=len;i++) {
    getValue(dsShapeText[i], dsMethod[i], sSelectedText);
    if(sSelectedText != "*")
    {
      sColor = sColorBad;
    }
    else
    {
      sColor = sColorOk;
    }
    setValue(dsShapeText[i], "backCol", sColor);
  }
//DebugTN(application_list.selectedText());
}

//-------------------------------------------------------------------------------------------

unEventList_userCB(string sDp, string sUser)
{
  dyn_string exceptionInfo;
  bool bFileExists;
        
  if(isFunctionDefined("unGenericButtonFunctionsHMI_isAccessAllowed"))
    unGenericButtonFunctionsHMI_isAccessAllowed(g_sPanelFileName, UN_GENERIC_USER_ACCESS, 
                                  unGenericButtonFunctionsHMI_getPanelAccessLevel(g_sPanelFileName, bFileExists), g_bGranted, exceptionInfo);
  else
  {
    if(sUser == c_systemIntegrity_noFwAccessControl_userAdmin) 
      g_bGranted = true;
    else
      g_bGranted = false;
  }
	
  saveConfig.enabled=g_bGranted;
}

//-----------------------------------------------------------------------------------------------------------------------------

string unEventList_getWhereStatement()
{
  string sWhere;
  string sAlias;
  dyn_string ds;
  int i, len;
  dyn_string dsDevType;

  if(filter_Object.selectedText != "*") {
    if((filter_Object2.text != "-1") && (filter_Object2.text != "")){ // > 1 type
        dsDevType = strsplit(filter_Object2.text, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
    }
    else { // only one type
        dsDevType = makeDynString(filter_Object.selectedText);
    }
    len = dynlen(dsDevType);
    for(i=1;i<=len;i++) {
      if(sAlias == "")
        sAlias = "\""+dsDevType[i]+"\"";
      else
        sAlias = sAlias + ",\""+dsDevType[i]+"\"";
    }
    if(sAlias != "")
      sWhere = "WHERE _DPT IN ("+sAlias+") ";
  }
  if(sWhere == "")
    sWhere = "WHERE (_ELC==DPEL_INT || _ELC==DPEL_CHAR || _ELC==DPEL_UINT) ";
  else
    sWhere = sWhere + "& (_ELC==DPEL_INT || _ELC==DPEL_CHAR || _ELC==DPEL_UINT) ";
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getWhereStatement", "getWhereStatement", sWhere , filter_Object2.text, filter_Object.selectedText, dsDevType);  
  return sWhere;
}

//-----------------------------------------------------------------------------------------------------------------------------

bool unEventList_checkRequest(time &tBegin, time &tEnd, bool &bEstimate)
{
  bool bForce, bNeed=FALSE, bDiff;
  string sFrom;
  long qEnd;

//   Check From Statement and Distributed Status
  if((g_dsOldSelectedApplication != g_dsSelectedApplication) ||
     ((dynlen(dynIntersect(g_dsSelectedDeviceType, g_dsOldSelectedDeviceType)) != dynlen(g_dsSelectedDeviceType))))
    bDiff = true;
//DebugN("check REQ", bDiff/*, g_dsSelectedApplication, g_dsOldSelectedApplication, 
//       g_dsSelectedDeviceType, g_dsOldSelectedDeviceType, g_sFrom, g_sOldFrom, listOfSystem.text, g_sOldSystemList*/);
  if (bDiff || listOfSystem.text!=g_sOldSystemList)
  {
    dynClear(g_tableHistory);
    dynClear(g_tableDisplay);		
    bNeed=TRUE;
    mappingClear(g_mDPELastValue00);
    mappingClear(g_mDPELastValue16);
  }
//   Check Time Request	
  else
  {
    bForce=!btForceTime.state(0);
    if (bForce)
    {
      dynClear(g_tableHistory);
      dynClear(g_tableDisplay);			
      mappingClear(g_mDPELastValue00);
      mappingClear(g_mDPELastValue16);
    }
    else
    {
      if((tBegin<g_OldrequestBegin) && ((mappinglen(g_mDPELastValue00) >0) || (mappinglen(g_mDPELastValue16) > 0) )) {
        dynClear(g_tableHistory);
        dynClear(g_tableDisplay);			
        mappingClear(g_mDPELastValue00);
        mappingClear(g_mDPELastValue16);
        bNeed=TRUE;
      }
      else if ((tBegin<g_OldrequestBegin) || (tEnd>g_OldrequestEnd))
      {
        unLists_getInternalTimeRequest(tBegin, tEnd, bEstimate);
        bNeed=TRUE;
        if(!bEstimate) {
          mappingClear(g_mDPELastValue00);
          mappingClear(g_mDPELastValue16);
        }
      }
      else
        bEstimate=TRUE;
    }	
  }

  if (bNeed || bForce)
  {
    qEnd=tEnd;
    qEnd=qEnd+1;	//Add one second in order to be sure to get all data in the range
    tEnd=qEnd;
  }

  if(bNeed) {
    g_dsOldSelectedApplication = g_dsSelectedApplication;
    g_dsOldSelectedDeviceType = g_dsSelectedDeviceType;
  }
  else {
    if(g_dsOldSelectedApplication != g_dsSelectedApplication)
      g_dsOldSelectedApplication = g_dsSelectedApplication;
    if(dynlen(dynIntersect(g_dsSelectedDeviceType, g_dsOldSelectedDeviceType)) != dynlen(g_dsSelectedDeviceType))
      g_dsOldSelectedDeviceType = g_dsSelectedDeviceType;
  }
  return bNeed;	
}

//-----------------------------------------------------------------------------------------------------------------------------

string unEventList_getFromStatement(string sSystem)
{
  dyn_string dsFrom;
  string sFrom;
  bool bOk;
  int i, iLen, iRes;
  dyn_string dsDevType=makeDynString("*");
  int iType, lenType;
  string sTemp;
  dyn_string dsTempDPE=strsplit(g_sDPEPattern, ",");
  int iDPE, lenDPE;

  if(dynlen(dsTempDPE) < 1)
    dsTempDPE="";
  lenDPE = dynlen(dsTempDPE);
  
  if (g_sFrom=="*")
  {
    dsFrom=makeDynString("*");
  }
  dsFrom=strsplit(g_sFrom, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
  bOk=false;
//DebugN(g_dsTreeApplication, g_m_dsTreeApplication, g_m_dsSubApplications);		  
  iLen=dynlen(dsFrom);
  for(i=1;i<=iLen;i++) {
    if(dynContains(g_m_dsSubApplications[sSystem], dsFrom[i]) > 0){
      for(iDPE=1;iDPE<=lenDPE;iDPE++)
      {
        sTemp = "*-*-"+ dsFrom[i]+"-*-*." + dsTempDPE[iDPE];
        if (sFrom=="")
          sFrom=sTemp;	
        else
          sFrom=sFrom + "," + sTemp;	
					
        bOk=true;
      }
    }
  }
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getFromStatement", "getFromStatement", g_sFrom, sFrom, bOk);
  return sFrom;
}

//-----------------------------------------------------------------------------------------------------------------------------

//Execute the specified query and save it in the global variable g_tableHistory
bool unEventList_executeQueryOld(time tBegin, time tEnd, dyn_string& exceptionInfo)
{
  dyn_dyn_anytype queryTab1;
  string query, sFrom;	
  int iRes, iLen, j;
  string sWhere, sRemote, sSysteName;
  time i1, i2;
  float fDiff;
  dyn_string dsReturn;
  dyn_float dfReturn;
	
//	 1. Clear variables
  g_iEvents=-1;						
  g_iLastDisplayedLine=-1;
  g_iFirstDisplayedLine=-1;
	
  LinesInfo.text = "";
  index1.text = "";
  index2.text = "";
	
  iLen=mappinglen(g_bSystemState);
//DebugN(g_bSystemState, g_m_sTreePVSSSystem, g_m_bTreeDeviceOverviewSystemConnected);	
//  Query results for each system name
  for (j=1; j<=iLen;j++)				
  {
    if (mappingGetValue(g_bSystemState, j))	//if connected...
    {
      sFrom=unEventList_getFromStatement(mappingGetKey(g_bSystemState, j));	
      if (sFrom!="")
      {
        sWhere = unEventList_getWhereStatement();
        sFrom= "{" + sFrom + "}";
        txtwait.text="Extracting Events (Data Server: " + j + "/" + iLen  + "): please wait.";
			
        sSysteName = mappingGetKey(g_bSystemState, j);
        if (sSysteName != getSystemName())
        {
          sSysteName = substr(sSysteName, 0, strpos(sSysteName, ":"));
          sRemote = " REMOTE \'" + mappingGetKey(g_bSystemState, j) + "\' ";
        }
        else
          sRemote = "";
        query = "SELECT \'_offline.._stime\',\'_offline.._value\',\'_offline.._bad\' FROM \'" + sFrom + "\' "+ sRemote + " "+sWhere+ " "  ; // Config + pattern query
        query = query + " TIMERANGE(\"" + formatTime("%Y.%m.%d %H:%M:%S",tBegin) + "\",\"" + formatTime("%Y.%m.%d %H:%M:%S",tEnd) + "\",1,0) "; // Time query		
//DebugTN(" query: "+query);
        
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList executeQuery", "executeQuery", query);
        iRes = dpQuery(query, queryTab1);
        
        fDiff = i2 -i1;
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT1, "EventList executeQuery", "executeQuery", fDiff, queryTab1);
        if(dynlen(queryTab1) > g_iMaxEventBeforeWarning) 
          unGraphicalFrame_ChildPanelOnCentralModalReturn(  "vision/MessageInfo", "Confirm", 
                                           makeDynString("$1:the number of event returned is > "+g_iMaxEventBeforeWarning+"\nthe filtering can be slow\nDo you want to proceed with filtering?" ,
                                            "$2:Confirm", "$3:Cancel"), dfReturn, dsReturn);
        else 
          dynAppend(dfReturn,1);
        if(dynContains(dfReturn, 1) <= 0) 
        {
          iRes = -1;
          queryResult.visible = true;
        }
        
//DebugTN("Num event returned by query: "+dynlen(queryTab1));

        if(iRes >= 0)
        {
          txtwait.text="Calculating Events (Data Server: " + j + "/" + iLen  + "): please wait.";
          
          iRes=unEventList_getEvents(queryTab1);
          

          dyn_errClass err;
          err = getLastError(); // Test whether no errors
          if(dynlen(err) > 0)
          {
            fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: error in ctrlEventList.dll","");
            errorDialog(err); // Open dialog box with errors
            throwError(err); // Write errors to stderr
          }
          else
          {
            if (iRes==-1)
              fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: at least one error occured during query","");
          }
        }
      }
    }
  }

  dynSortAsc(g_tableHistory);
  g_iEvents=dynlen(g_tableHistory);
}//-----------------------------------------------------------------------------------------------------------------------------

//Execute the specified query and save it in the global variable g_tableHistory
bool unEventList_executeQuery(time tBegin, time tEnd, dyn_string& exceptionInfo)
{
  dyn_dyn_anytype queryTab1;
  string query, sFrom;	
  int iRes, iLen, j;
  string sWhere, sRemote, sSysteName;
  time i1, i2;
  float fDiff;
  dyn_string dsReturn;
  dyn_float dfReturn;
  string sTime1, sTime2;
  string queryTime;
	
//	 1. Clear variables
  g_iEvents=-1;						
  g_iLastDisplayedLine=-1;
  g_iFirstDisplayedLine=-1;
	
  LinesInfo.text = "";
  index1.text = "";
  index2.text = "";	 
  
  iLen=mappinglen(g_bSystemState);
//DebugN(g_bSystemState, g_m_sTreePVSSSystem, g_m_bTreeDeviceOverviewSystemConnected);	
//  Query results for each system name
  
//DebugTN("Num system " +iLen);
  for (j=1; j<=iLen;j++)				
  {
    if (mappingGetValue(g_bSystemState, j))	//if connected...
    {
      sFrom=unEventList_getFromStatement(mappingGetKey(g_bSystemState, j));	
      if (sFrom!="")
      {
        sWhere = unEventList_getWhereStatement();
        sFrom= "{" + sFrom + "}";
        txtwait.text="Extracting Events (Data Server: " + j + "/" + iLen  + "): please wait.";
			
        sSysteName = mappingGetKey(g_bSystemState, j);
        if (sSysteName != getSystemName())
        {
          sSysteName = substr(sSysteName, 0, strpos(sSysteName, ":"));
          sRemote = " REMOTE \'" + mappingGetKey(g_bSystemState, j) + "\' ";
        }
        else
          sRemote = "";
        sTime1 = formatTime("%Y.%m.%d %H:%M:%S",tBegin);
        sTime2 = formatTime("%Y.%m.%d %H:%M:%S",tEnd);
//        the problem is _offline_bad !!!!!!!!!!!  
//        original query:     
        query = "SELECT \'_offline.._stime\',\'_offline.._value\',\'_offline.._bad\' FROM \'" + sFrom + "\' "+ sRemote + " "+sWhere+ " "  ; // Config + pattern query
        query = "SELECT \'_offline.._stime\',\'_offline.._value\',\'_offline.._stime_inv\' FROM \'" + sFrom + "\' "+ sRemote + " "+sWhere+ " "  ; // Config + pattern query
        queryTime = query + " TIMERANGE(\"" + sTime1 + "\", \"" + sTime2 + "\" ,1,0) "; // Time query
        
  //DebugTN("Query: " + queryTime);
  
        //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList executeQuery", "executeQuery", queryTime);
        dynClear(exceptionInfo);
        
        i1=getCurrentTime();
        iRes = unEventList_exeQuery(queryTime, queryTab1, exceptionInfo);
        i2=getCurrentTime();
        unEventList_printTime("SQL Query", i1, i2);
        
        if(dynlen(exceptionInfo))//too much data: try to split the query into 2 time chunks
        { 
          dynClear(queryTab1);
//DebugN("queryTab1: "+queryTab1);
          unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/MessageInfo", "Confirm", 
                                           makeDynString("$1:The query returns too many values.\nThis causes some errors.\nTry query anyway (very slow)?" ,
                                            "$2:No", "$3:Yes"), dfReturn, dsReturn);
          if(dynContains(dfReturn, 1) <= 0)
            iRes = unEventList_executeQueryInChunks(query,tBegin,tEnd,1,queryTab1,exceptionInfo);
        }
        else
//DebugN("query ok");  

        i2=getCurrentTime();
        fDiff = i2 -i1;
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT1, "EventList executeQuery", "executeQuery", fDiff, queryTab1);
        if(dynlen(queryTab1) > g_iMaxEventBeforeWarning) 
          unGraphicalFrame_ChildPanelOnCentralModalReturn(  "vision/MessageInfo", "Confirm", 
                                           makeDynString("$1:"+"Many events returned."+"\nThe filtering can be very slow.\nDo you want to proceed with filtering?" ,
                                            "$2:Confirm", "$3:Cancel"), dfReturn, dsReturn);
        else 
          dynAppend(dfReturn,1);
        if(dynContains(dfReturn, 1) <= 0) 
        {
          iRes = -1;
          queryResult.visible = true;
        }
        if(iRes >= 0)
        {
          txtwait.text="Calculating Events (Data Server: " + j + "/" + iLen  + "): please wait.";
          
          i1=getCurrentTime();
          iRes=unEventList_getEvents(queryTab1);
          i2=getCurrentTime();
          unEventList_printTime("getEvents", i1, i2);
        
          dyn_errClass err;
          err = getLastError(); // Test whether no errors
          if(dynlen(err) > 0)
          {
            fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: error in ctrlEventList.dll","");
            errorDialog(err); // Open dialog box with errors
            throwError(err); // Write errors to stderr
          }
          else
          {
            if (iRes==-1)
              fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: at least one error occured during query","");
          }
        }
      }
    }
  }
  dynSortAsc(g_tableHistory);
  g_iEvents=dynlen(g_tableHistory);
}//-----------------------------------------------------------------------------------------------------------------------------


//Execute the specified query and save it in the global variable g_tableHistory
int unEventList_exeQuery(string query, dyn_dyn_anytype &queryTabPartial, dyn_string &exceptionInfo)
{
  int iRes;
  exceptionInfo = makeDynString("error in dpQuery");
  iRes = dpQuery(query, queryTabPartial);
  exceptionInfo = getLastError();
  return iRes;
}//-----------------------------------------------------------------------------------------------------------------------------

//Execute the specified query and save it in the global variable g_tableHistory
bool unEventList_executeQueryNewWithPatternSplit(time tBegin, time tEnd, dyn_string& exceptionInfo)
{
  dyn_dyn_anytype queryTab1, queryTabPartial;
  string query, sFrom;	
  int iRes, iLen, j, i;
  string sWhere, sRemote, sSysteName;
  time i1, i2;
  float fDiff;
  dyn_string dsReturn;
  dyn_float dfReturn;
  dyn_string dsFrom;
	
//	 1. Clear variables
  g_iEvents=-1;						
  g_iLastDisplayedLine=-1;
  g_iFirstDisplayedLine=-1;
	
  LinesInfo.text = "";
  index1.text = "";
  index2.text = "";
	
  iLen=mappinglen(g_bSystemState);
//DebugN(g_bSystemState, g_m_sTreePVSSSystem, g_m_bTreeDeviceOverviewSystemConnected);	
//  Query results for each system name
  for (j=1; j<=iLen;j++)				
  {
    if (mappingGetValue(g_bSystemState, j))	//if connected...
    {
      sFrom=unEventList_getFromStatement(mappingGetKey(g_bSystemState, j));	
      if (sFrom!="")
      {
        sWhere = unEventList_getWhereStatement();
        sFrom= "{" + sFrom + "}";
        txtwait.text="Extracting Events (Data Server: " + j + "/" + iLen  + "): please wait.";
			
        sSysteName = mappingGetKey(g_bSystemState, j);
        if (sSysteName != getSystemName())
        {
          sSysteName = substr(sSysteName, 0, strpos(sSysteName, ":"));
          sRemote = " REMOTE \'" + mappingGetKey(g_bSystemState, j) + "\' ";
        }
        else
          sRemote = "";
        strreplace(sFrom,"{","");
        strreplace(sFrom,"}","");
        dsFrom = strsplit(sFrom,",");
        dynClear(queryTab1);
        i1=getCurrentTime();
        for(i=1 ; i<=dynlen(dsFrom) ; i++)
        {
          query = "SELECT \'_offline.._stime\',\'_offline.._value\',\'_offline.._bad\' FROM \'" + dsFrom[i] + "\' "+ sRemote + " "+sWhere+ " "  ; // Config + pattern query
          query = query + " TIMERANGE(\"" + formatTime("%Y.%m.%d %H:%M:%S",tBegin) + "\",\"" + formatTime("%Y.%m.%d %H:%M:%S",tEnd) + "\",1,0) "; // Time query		
//DebugN("last error: "+getLastError());        
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList executeQuery", "executeQuery", query);
//DebugTN("query: "+query);
dynClear(exceptionInfo);
          iRes = unEventList_exeQuery(query, queryTabPartial, exceptionInfo);
          iRes = dpQuery(query, queryTabPartial);
//DebugN("query result: "+iRes + exceptionInfo);
if(dynlen(exceptionInfo))
{ 
  unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/MessageInfo", "Confirm", 
                                   makeDynString("$1:Too many values.\nTry query anyway (slow)?" ,
                                    "$2:No", "$3:Yes"), dfReturn, dsReturn);
  
  //DebugN("error");
}
else
  //DebugN("ok");        
          if(i>1 && dynlen(queryTabPartial)>0)
            dynRemove(queryTabPartial,1);//remove result from all the results after the first result
          dynAppend(queryTab1, queryTabPartial);
        }
        i2=getCurrentTime();
        fDiff = i2 - i1;
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT1, "EventList executeQuery", "executeQuery", fDiff, queryTab1);
        if(dynlen(queryTab1) > g_iMaxEventBeforeWarning) 
          unGraphicalFrame_ChildPanelOnCentralModalReturn(  "vision/MessageInfo", "Confirm", 
                                           makeDynString("$1:the number of event returned is > "+g_iMaxEventBeforeWarning+"\nthe filtering can be slow\nDo you want to proceed with filtering?" ,
                                            "$2:Confirm", "$3:Cancel"), dfReturn, dsReturn);
        else 
          dynAppend(dfReturn,1);
        if(dynContains(dfReturn, 1) <= 0) 
        {
          iRes = -1;
          queryResult.visible = true;
        }

        if(iRes >= 0)
        {
          txtwait.text="Calculating Events (Data Server: " + j + "/" + iLen  + "): please wait.";
          iRes=unEventList_getEvents(queryTab1);

          dyn_errClass err;
          err = getLastError(); // Test whether no errors
          if(dynlen(err) > 0)
          {
            fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: error in ctrlEventList.dll","");
            errorDialog(err); // Open dialog box with errors
            throwError(err); // Write errors to stderr
          }
          else
          {
            if (iRes==-1)
              fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: at least one error occured during query","");
          }
        }
      }
    }
  }

  dynSortAsc(g_tableHistory);
  g_iEvents=dynlen(g_tableHistory);
}
//-----------------------------------------------------------------------------------------------------------------------------

//Execute the specified query and save it in the global variable g_tableHistory
bool unEventList_executeQueryNewWithTimeSplit(time tBegin, time tEnd, dyn_string& exceptionInfo)
{
  dyn_dyn_anytype queryTab1;
  string query, sFrom;	
  int iRes, iLen, j;
  string sWhere, sRemote, sSysteName;
  time i1, i2;
  float fDiff;
  dyn_string dsReturn;
  dyn_float dfReturn;
  string sTime1, sTime2;
	
//	 1. Clear variables
  g_iEvents=-1;						
  g_iLastDisplayedLine=-1;
  g_iFirstDisplayedLine=-1;
	
  LinesInfo.text = "";
  index1.text = "";
  index2.text = "";
	
  iLen=mappinglen(g_bSystemState);
//DebugN(g_bSystemState, g_m_sTreePVSSSystem, g_m_bTreeDeviceOverviewSystemConnected);	
//  Query results for each system name
  for (j=1; j<=iLen;j++)				
  {
    if (mappingGetValue(g_bSystemState, j))	//if connected...
    {
      sFrom=unEventList_getFromStatement(mappingGetKey(g_bSystemState, j));	
      if (sFrom!="")
      {
        sWhere = unEventList_getWhereStatement();
        sFrom= "{" + sFrom + "}";
        txtwait.text="Extracting Events (Data Server: " + j + "/" + iLen  + "): please wait.";
			
        sSysteName = mappingGetKey(g_bSystemState, j);
        if (sSysteName != getSystemName())
        {
          sSysteName = substr(sSysteName, 0, strpos(sSysteName, ":"));
          sRemote = " REMOTE \'" + mappingGetKey(g_bSystemState, j) + "\' ";
        }
        else
          sRemote = "";
        sTime1 = formatTime("%Y.%m.%d %H:%M:%S",tBegin);
        sTime2 = formatTime("%Y.%m.%d %H:%M:%S",tEnd);
        //original
        //query = "SELECT \'_offline.._stime\',\'_offline.._value\',\'_offline.._bad\' FROM \'" + sFrom + "\' "+ sRemote + " "+sWhere+ " "  ; // Config + pattern query
        //query = query + " TIMERANGE(\"" + sTime1 + "\", \"" + sTime2 + "\" ,1,0) "; // Time query		
        //the problem is _offline_bad !!!!!!!!!!!
        
        query = "SELECT \'_offline.._stime\',\'_offline.._value\',\'_original.._bad\' FROM \'" + sFrom + "\' "+ sRemote + " "+sWhere+ " "  ; // Config + pattern query
          query = query + " TIMERANGE(\"" + sTime1 + "\", \"" + sTime2 + "\" ,1,0) "; // Time query		
        
        query = "SELECT \'_offline.._bad\' FROM \'" + sFrom + "\' "  ; // Config + pattern query
        query = query + " TIMERANGE(\"" + sTime1 + "\", \"" + sTime2 + "\" ,1,0) "; // Time query		
//DebugN("query: "+query);
        i1=getCurrentTime();
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList executeQuery", "executeQuery", query);

        iRes = dpQuery(query, queryTab1);
           iRes = unEventList_executeQueryInChunks(query,tBegin,tEnd,1,queryTab1,exceptionInfo);
        //DebugN(queryTab1);
//DebugN("query result: "+iRes + getLastError());        
        i2=getCurrentTime();
        fDiff = i2 -i1;
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT1, "EventList executeQuery", "executeQuery", fDiff, queryTab1);
        if(dynlen(queryTab1) > g_iMaxEventBeforeWarning) 
          unGraphicalFrame_ChildPanelOnCentralModalReturn(  "vision/MessageInfo", "Confirm", 
                                           makeDynString("$1:the number of event returned even is > "+g_iMaxEventBeforeWarning+"\nthe filtering can be slow\nDo you want to proceed with filtering?" ,
                                            "$2:Confirm", "$3:Cancel"), dfReturn, dsReturn);
        else 
          dynAppend(dfReturn,1);
        if(dynContains(dfReturn, 1) <= 0) 
        {
          iRes = -1;
          queryResult.visible = true;
        }

        if(iRes >= 0)
        {
          txtwait.text="Calculating Events (Data Server: " + j + "/" + iLen  + "): please wait.";
          iRes=unEventList_getEvents(queryTab1);

          dyn_errClass err;
          err = getLastError(); // Test whether no errors
          if(dynlen(err) > 0)
          {
            fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: error in ctrlEventList.dll","");
            errorDialog(err); // Open dialog box with errors
            throwError(err); // Write errors to stderr
          }
          else
          {
            if (iRes==-1)
              fwException_raise(exceptionInfo, "ERROR", "executeQuery() Events List: at least one error occured during query","");
          }
        }
      }
    }
  }

  dynSortAsc(g_tableHistory);
  g_iEvents=dynlen(g_tableHistory);
}

//-----------------------------------------------------------------------------------------------------------------------------

//Execute the specified query. if the result is error, try to re-execute it after splitting it into 2 time chunks.
//repeat this a few times before failing.
//if success, save it in the global variable g_tableHistory
int unEventList_executeQueryInChunks(string queryWithoutTime,
                         time tBegin,
                         time tEnd,
                         int currentTry,
                         dyn_dyn_anytype &queryTab,
                         dyn_string& exceptionInfo)
{
  dyn_dyn_anytype queryTab1, queryTab2;
  string query, sFrom;	
  int iRes, iLen, j;
  string sWhere, sRemote, sSysteName;
  time i1, i2;
  float fDiff;
  dyn_string dsReturn;
  dyn_float dfReturn;
  string sTime1, sTime2;
  const int MAX_TRIES = 4;
  time tMiddle1, tMiddle2;
  string sTimeM1,sTimeM2;
  int iTime, iBegin, iEnd;
	
  dynClear(exceptionInfo);
	       
    if(tEnd-tBegin > 2)
    {
      iBegin = tBegin;
      iEnd = tEnd;
      iTime = (iEnd - iBegin)/2 + iBegin;
      tMiddle1 = iTime;
      tMiddle2 = iTime;
      sTime1 = formatTime("%Y.%m.%d %H:%M:%S",tBegin);
      sTime2 = formatTime("%Y.%m.%d %H:%M:%S",tEnd);
      sTimeM1 = formatTime("%Y.%m.%d %H:%M:%S",tMiddle1);
      sTimeM2 = formatTime("%Y.%m.%d %H:%M:%S",(tMiddle2));
               
// DebugN("==================== try "+currentTry+" =======================");        
      i1=getCurrentTime();
        
      query = queryWithoutTime + " TIMERANGE(\"" + sTime1 + "\", \"" + sTimeM1 + "\" ,1,0) "; // Time query		
      //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList executeQuery", "executeQueryInChunks", "Too much data. Try to split the query (split #" +currentTry+ "). Execute Query chunk #1");
        
// DebugN("query 1: "+query);
      dynClear(exceptionInfo);
      iRes = unEventList_exeQuery(query, queryTab1, exceptionInfo);
      if(dynlen(exceptionInfo))//still too much data: must split the query again
      {
            if(tEnd-tBegin > 60)
            {
              currentTry++;
              iRes=executeQueryInChunks(queryWithoutTime,tBegin,tMiddle1,currentTry,queryTab,exceptionInfo);
// DebugN("query length: "+dynlen(queryTab));
            }
      }
      else
      {
        if(dynlen(queryTab)>=1)//header already present in the query result
          dynRemove(queryTab1,1);//remove header from further query results
        dynAppend(queryTab, queryTab1);       
      }
        
      query = queryWithoutTime + " TIMERANGE(\"" + sTimeM2 + "\", \"" + sTime2 + "\" ,1,0) "; // Time query		
      //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList executeQuery", "executeQueryInChunks", "Too much data. Try to split the query (split #" +currentTry+ "). Execute Query chunk #2");
        
// DebugN("query 2: "+query);
      dynClear(exceptionInfo);
      iRes = unEventList_exeQuery(query, queryTab2, exceptionInfo);
// DebugN("first result of chunk 2: "+queryTab2[2]);
      if(dynlen(exceptionInfo))//still too much data: must split the query again
      {
            if(tEnd-tBegin > 60)
            {
              currentTry++;
              iRes+=unEventList_executeQueryInChunks(queryWithoutTime,tMiddle2,tEnd,currentTry,queryTab,exceptionInfo);
// DebugN("query length: "+dynlen(queryTab));
            }
            else
            {
              dynClear(exceptionInfo);
              fwException_raise(exceptionInfo, "ERROR", "executeQueryInChunks() Events List: too much data. Try reducing the time window.","");
            }
      }
      else
      {
        if(dynlen(queryTab)>=1)//header already present in the query result
          dynRemove(queryTab2,1);
        dynAppend(queryTab, queryTab2);
      }
  }
  return iRes;
          
}

//-----------------------------------------------------------------------------------------------------------------------------

int unEventList_getEvents(const dyn_dyn_anytype queryTab)
{		
dyn_dyn_string ddsInput;
dyn_string dsNames, dsChanges, dsColors, queryTabTemp, dsPositions;
dyn_int diPositions;			
string sCurrentDpType, sAlias, sDescription, sCurrentDp, sTime, sDomain, sNature, sParam, deviceName, sAppli, sSystem;
int iRes, iOldValue, iNewValue, iValue, i, iLen, iReturn;	
bool bEvent=true;
dyn_string dsNames16, dsPositions16, dsChanges16, dsColors16, dsNames0, dsPositions0, dsChanges0, dsColors0;
dyn_string queryTabTemp00, queryTabTemp16;
string sDpType;
		
	iReturn=0;
	iLen = dynlen(queryTab);
	
	for(i=2; i<=iLen; i++)
	{
		dyn_string queryTabTemp;
		
		if (dynlen(queryTab[i])>=4)
		{			
			if (sCurrentDp != queryTab[i][1])											// New DP
			{
				sCurrentDp = queryTab[i][1];											
                                sDpType = dpTypeName(sCurrentDp);
                                if(mappingHasKey(g_m_dsNames, sDpType))
                                { // event definition
                                  bEvent = true;
				if (sCurrentDpType != sDpType)				// New DP type
					{
					sCurrentDpType = sDpType;				
					iReturn=unEventList_panel_getDeviceTypeEventConfiguration(sCurrentDpType, dsNames, diPositions, dsChanges, dsColors);	// Get bit configuration
					dsPositions=diPositions;
					}
                                }
                                else { // no bit definition in the EventBits assume this is a status DPE
                                  unEventList_panel_getStatusConfiguration(false, sCurrentDp, dsNames0, dsPositions0, dsChanges0, dsColors0);
                                  unEventList_panel_getStatusConfiguration(true, sCurrentDp, dsNames16, dsPositions16, dsChanges16, dsColors16);
                                  bEvent = false;
                                  sCurrentDpType = sDpType;
                                }
				
				deviceName = unGenericDpFunctions_getDpName(sCurrentDp);
				sSystem = unGenericDpFunctions_getSystemName(sCurrentDp);				
				sAlias = unGenericDpFunctions_getAlias(deviceName); 	
				sDescription = unGenericDpFunctions_getDescription(deviceName);
				sParam = unGenericDpFunctions_getDescription(deviceName + ".ProcessInput");
				sAppli = unGenericDpFunctions_getApplication(deviceName);
				
				ddsInput[1][1]=sSystem;
				ddsInput[1][2]=sCurrentDp;
				ddsInput[1][3]=sCurrentDpType;
				ddsInput[1][5]=sAlias;
				ddsInput[1][6]=sDescription;
				ddsInput[1][9]=sAppli;
				ddsInput[1][10]=sParam;
				ddsInput[2]= dsNames;
				ddsInput[3]= dsPositions;
				ddsInput[4]= dsColors;	
				ddsInput[5]= dsChanges;					//only for event 32			
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents device data", "getEvents device data", sSystem, sCurrentDp, sCurrentDpType, sAlias);

				if(g_bEvent16)
				{
        if(bEvent)
        {
  					iOldValue = queryTab[i][3];											
  					ddsInput[1][4]=(long)queryTab[i][2] + "." + milliSecond(queryTab[i][2]);	//sTime= Seconds.Milli;
  					ddsInput[1][7]=iOldValue;														
  					ddsInput[1][8]=queryTab[i][3];	//Invalid
            //DebugTN("1-ddsInput: " + ddsInput);
  					iRes=ctrlEventList_getBitStates(ddsInput, queryTabTemp);						//DLL
            //DebugTN("queryTabTemp: " + queryTabTemp);
                                //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents 16bits mode", "getEvents current state 16bits mode", queryTabTemp);
         }
				}
      if(!bEvent)
      {
        if(dynlen(dsNames0) > 0)
        {
          dynClear(queryTabTemp00);
          ddsInput[2]= dsNames0;
          ddsInput[3]= dsPositions0;
          ddsInput[4]= dsColors0;	
          ddsInput[5]= dsChanges0;					//only for event 32			
          ddsInput[1][4]=(long)queryTab[i][2] + "." + milliSecond(queryTab[i][2]);	//sTime= Seconds.Milli;
          ddsInput[1][8]=queryTab[i][4];	//Invalid
          if(!mappingHasKey(g_mDPELastValue00, sCurrentDp)) {
            iOldValue = queryTab[i][3]&65535;											
            ddsInput[1][7]=iOldValue;			
            //DebugTN("2-ddsInput: " + ddsInput);											
            iRes=ctrlEventList_getBitStates(ddsInput, queryTabTemp00);						//DLL
            //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents Status 00-15", "getEvents Status current state 00-15", g_mDPELastValue00[sCurrentDp], ddsInput, queryTabTemp00);
          }
          else
          {
            iOldValue=g_mDPELastValue00[sCurrentDp];
            iValue = queryTab[i][3]&65535;
            if(iValue != iOldValue)
            {
              ddsInput[1][7]= iOldValue<<16+iValue;	
              //DebugTN("3-ddsInput: " + ddsInput);
              iRes=ctrlEventList_getBitChanges(ddsInput, queryTabTemp00);
              iOldValue = iValue;
              if(g_bRemoveUnknowState)
                unEventList_panel_removeUnkwnonState(queryTabTemp00);
              //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents Status 00-15", "getEvents New DP Status change 00-15", g_mDPELastValue00[sCurrentDp], ddsInput, queryTabTemp00);
            }
          }
          g_mDPELastValue00[sCurrentDp] = iOldValue;
        }
        if(dynlen(dsNames16) > 0) 
        {
          dynClear(queryTabTemp16);
          ddsInput[2]= dsNames16;
          ddsInput[3]= dsPositions16;
          ddsInput[4]= dsColors16;	
          ddsInput[5]= dsChanges16;					//only for event 32			
          ddsInput[1][4]=(long)queryTab[i][2] + "." + milliSecond(queryTab[i][2]);	//sTime= Seconds.Milli;
          ddsInput[1][8]=queryTab[i][4];	//Invalid
          if(!mappingHasKey(g_mDPELastValue16, sCurrentDp)) {
            iOldValue = (queryTab[i][3]>>16)&65535;
            ddsInput[1][7]=iOldValue;		
            //DebugTN("4-ddsInput: " + ddsInput);												
            iRes=ctrlEventList_getBitStates(ddsInput, queryTabTemp16);						//DLL
            //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents Status 16-31", "getEvents Status current state 16-31", g_mDPELastValue16[sCurrentDp], ddsInput, queryTabTemp16);
          }
          else
          {
            iOldValue=g_mDPELastValue16[sCurrentDp];
            iValue = (queryTab[i][3]>>16)&65535;
            if(iValue != iOldValue)
            {
              ddsInput[1][7]= (iOldValue<<16)+iValue;	
              //DebugTN("5-ddsInput: " + ddsInput);
              iRes=ctrlEventList_getBitChanges(ddsInput, queryTabTemp16);
              iOldValue = iValue; 
              if(g_bRemoveUnknowState)
                unEventList_panel_removeUnkwnonState(queryTabTemp16);
              //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents Status 16-31", "getEvents New DP Status change 16-31", g_mDPELastValue16[sCurrentDp], ddsInput, queryTabTemp16);
            }
          }
          g_mDPELastValue16[sCurrentDp] = iOldValue;
        }
        if(dynlen(queryTabTemp00)>0)
          dynAppend(queryTabTemp, queryTabTemp00);
        if(dynlen(queryTabTemp16)>0)
          dynAppend(queryTabTemp, queryTabTemp16);
      }
			}
			else																	// Same DP, old / new value can be compared
			{
                          if(!bEvent)
                          {
                            if(dynlen(dsNames0) > 0)
                            {
                              dynClear(queryTabTemp00);
                              iOldValue = g_mDPELastValue00[sCurrentDp];
                              iValue = queryTab[i][3]&65535;
                              if(iOldValue!=iValue)
                              {
                                ddsInput[2]= dsNames0;
                                ddsInput[3]= dsPositions0;
                                ddsInput[4]= dsColors0;	
                                ddsInput[5]= dsChanges0;					//only for event 32			
                                ddsInput[1][4]=(long)queryTab[i][2] + "." + milliSecond(queryTab[i][2]);	//sTime= Seconds.Milli;
                                ddsInput[1][7]= (iOldValue<<16)+iValue;	
                                ddsInput[1][8]=queryTab[i][4];	//Invalid
                                //DebugTN("6-ddsInput: " + ddsInput);
                                iRes=ctrlEventList_getBitChanges(ddsInput, queryTabTemp00);						//DLL
                                g_mDPELastValue00[sCurrentDp] = iValue; 
                                if(g_bRemoveUnknowState)
                                  unEventList_panel_removeUnkwnonState(queryTabTemp00);
                                //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents Status 00-15", "getEvents Status change 00-15", g_mDPELastValue00[sCurrentDp], ddsInput, queryTabTemp00);
                              }
                            }
                            if(dynlen(dsNames16) > 0) 
                            {
                              dynClear(queryTabTemp16);
                              iOldValue = g_mDPELastValue16[sCurrentDp];
                              iValue = (queryTab[i][3]>>16)&65535;
                              if(iOldValue != iValue)
                              {
                                ddsInput[2]= dsNames16;
                                ddsInput[3]= dsPositions16;
                                ddsInput[4]= dsColors16;	
                                ddsInput[5]= dsChanges16;					//only for event 32			
                                ddsInput[1][4]=(long)queryTab[i][2] + "." + milliSecond(queryTab[i][2]);	//sTime= Seconds.Milli;
                                ddsInput[1][7]= (iOldValue<<16)+iValue;	
                                ddsInput[1][8]=queryTab[i][4];	//Invalid
                                //DebugTN("7-ddsInput: " + ddsInput);
                                iRes=ctrlEventList_getBitChanges(ddsInput, queryTabTemp16);						//DLL
                                g_mDPELastValue16[sCurrentDp] = iValue; 
                                if(g_bRemoveUnknowState)
                                  unEventList_panel_removeUnkwnonState(queryTabTemp16);
                                //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents Status 16-31", "getEvents Status change 16-31", g_mDPELastValue16[sCurrentDp], ddsInput, queryTabTemp16);
                              }
                            }
                            if(dynlen(queryTabTemp00)>0)
                              dynAppend(queryTabTemp, queryTabTemp00);
                            if(dynlen(queryTabTemp16)>0)
                              dynAppend(queryTabTemp, queryTabTemp16);
                          }
                          else
                          {
				if(g_bEvent16)
					{			
					iValue = iOldValue<<16;
					ddsInput[1][4]=(long)queryTab[i][2] + "." + milliSecond(queryTab[i][2]);	//sTime= Seconds.Milli;
					ddsInput[1][7]= iValue+(queryTab[i][3]&65535);	
					ddsInput[1][8]=queryTab[i][4];	//Invalid
          //DebugTN("8-ddsInput: " + ddsInput);
					iRes=ctrlEventList_getBitChanges(ddsInput, queryTabTemp);						//DLL
					iOldValue = queryTab[i][2]; 
                                        if(g_bRemoveUnknowState)
                                          unEventList_panel_removeUnkwnonState(queryTabTemp);
                                        //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents 16bits mode", "getEvents bit change 16bits mode", queryTabTemp);
					}
                          }
			}
				
			if(!g_bEvent16)	//bit 32
			{
                          if(bEvent)
                          {
				ddsInput[1][4]=(long)queryTab[i][2] + "." + milliSecond(queryTab[i][2]);	//sTime= Seconds.Milli;
				ddsInput[1][7]=queryTab[i][3];										
				ddsInput[1][8]=queryTab[i][4];	//Invalid
        //DebugTN("9-ddsInput: " + ddsInput);
				iRes=ctrlEventList_getBitChanges(ddsInput, queryTabTemp);						//DLL				
                                if(g_bRemoveUnknowState)
                                  unEventList_panel_removeUnkwnonState(queryTabTemp);
                                //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents 32bits mode", "getEvents bit change 32bits mode", queryTabTemp);
                          }
			}

			if (dynlen(queryTabTemp)>0)
				{
				dynAppend(g_tableHistory, queryTabTemp);								// Append Event
				}
			
			if (iRes!=0)
				iReturn=-1;
		}
		else
		{
			iReturn=-1;
			i=iLen;
		}
	}
					
	dynUnique(g_tableHistory);
  //DebugTN("Size of g_tableHistory: "+dynlen(g_tableHistory));
//unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getEvents", "getEvents end processing", g_tableHistory);
		
	return iReturn;
}
//-----------------------------------------------------------------------------------------------------------------------------

unEventList_panel_loadDeviceTypeEventConfiguration(dyn_string dsUnicosObjects)
{
  int i, len=dynlen(dsUnicosObjects);
  dyn_string dsNames, dsChanges, dsColors;
  dyn_int diPositions;
  int iReturn, iSize;
  
  for(i=1;i<=len;i++)
  {
    iReturn=unEventList_getConfiguration(dsUnicosObjects[i], dsNames, diPositions, dsChanges, dsColors);	// Get bit configuration
    iSize = dynlen(dsNames);
    if((iSize > 0) && (iSize == dynlen(diPositions)) && 
       (iSize == dynlen(dsChanges)) && (iSize == dynlen(dsColors)))
    { // correct event definition, load it into memory
      g_m_dsNames[dsUnicosObjects[i]] = dsNames;
      g_m_diPositions[dsUnicosObjects[i]] = diPositions;
      g_m_dsChanges[dsUnicosObjects[i]] = dsChanges;
      g_m_dsColors[dsUnicosObjects[i]] = dsColors;
    }
  }
  //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList loadDeviceTypeEventConfiguration", "loadDeviceTypeEventConfiguration", g_m_dsNames, g_m_diPositions, g_m_dsChanges, g_m_dsColors);
}

//-----------------------------------------------------------------------------------------------------------------------------

int unEventList_panel_getDeviceTypeEventConfiguration(string dpType, dyn_string &dsNames, dyn_int &diPositions, dyn_string &dsChanges, dyn_string &dsColors)
{
  if(mappingHasKey(g_m_dsNames, dpType)) {
    dsNames = g_m_dsNames[dpType];
    diPositions = g_m_diPositions[dpType];
    dsChanges = g_m_dsChanges[dpType];
    dsColors = g_m_dsColors[dpType];
  }
  else {
    dynClear(dsNames);
    dynClear(diPositions);
    dynClear(dsChanges);
    dynClear(dsColors);
  }
  //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getDeviceTypeEventConfiguration", "getDeviceTypeEventConfiguration", dpType, dsNames, diPositions, dsChanges, dsColors);
  return 0;
}

//-----------------------------------------------------------------------------------------------------------------------------

unEventList_panel_removeUnkwnonState(dyn_string &dsData)
{
  dyn_string dsResult;
  int len, i, iPos;
  
  dsResult = dynPatternMatch("*Unknow State: 0x*", dsData);
  len = dynlen(dsResult);
  for(i=1; i<=len;i++)
  {
    iPos = dynContains(dsData, dsResult[i]);
    if(iPos > 0)
      dynRemove(dsData, iPos);
  }
}

//-----------------------------------------------------------------------------------------------------------------------------

//closePanel function
//Function to be run before closing the panel (save filter, lineMax configuration)

void unEventList_closePanel()
{
	dyn_float dfLinesMax;				// Number of lines to be displayed in the history panel
	dyn_string dsFilter;				// Filter for history panel
	
	dfLinesMax = makeDynFloat(g_iLinesMax);
	dsFilter = makeDynString(g_sFilter);

	PanelOffReturn(dfLinesMax,dsFilter);
}

//-----------------------------------------------------------------------------------------------------------------------------

//_allowDisplay function
//This function returns in allow the authorization to display the current line, depending on the filter configuration

int unEventList__allowDisplay(string g_sFilter, dyn_string historicMessageSplit)
{
	int iRes=-1;
	dyn_string dsFilter;

	dsFilter = strsplit(g_sFilter, UN_EVENT_LIST_FILTER_DELIMITER);
	
	if (dynlen(dsFilter) != (UN_EVENT_LIST_FILTER_FIELD+1))
		{
		dsFilter = strsplit(UN_EVENT_LIST_FILTER_DEFAULT, UN_EVENT_LIST_FILTER_DELIMITER);
		}
	
	iRes=ctrlEventList_event_allowDisplay(historicMessageSplit, dsFilter);	//DLL
//  DebugN("Data: " , historicMessageSplit, "Filter: " ,  dsFilter);
//  DebugN("Data: Filter: " +  g_sFilter);
//  DebugN("Data: Filter: " +  dsFilter);

	return iRes;	
}

//-----------------------------------------------------------------------------------------------------------------------------

//getConfiguration
//This function returns the configuration of the StsReg01(02) for an Unicos object
//It uses DP of DP type : _UnicosObjects

int unEventList_getConfiguration(string dpType, dyn_string &dsNames, dyn_int &diPositions, dyn_string &dsChanges, dyn_string &dsColors)
{
	string sNames, sPositions, sChanges, sColors;
	dyn_string dsNamesTemp, dsPositionsTemp, dsChangesTemp, dsColorsTemp;
	int i, iRes, length, currentPosition;
	
//	 1. If no configuration exists
	dsNames = makeDynString();
	diPositions = makeDynInt();
	dsChanges = makeDynString();
	iRes=-1;
	
//	 2. Check if a valid configuration exists
	if ((g_sConfigSystemName != "") && (dpType != "") && (dpExists(g_sConfigSystemName + dpType + UN_OBJECT_EXTENSION)))
		{
		iRes = dpGet(g_sConfigSystemName + dpType + UN_OBJECT_EXTENSION + ".unEventList.Names", sNames,
					 g_sConfigSystemName + dpType + UN_OBJECT_EXTENSION + ".unEventList.Positions", sPositions,
					 g_sConfigSystemName + dpType + UN_OBJECT_EXTENSION + ".unEventList.Changes", sChanges,
					 g_sConfigSystemName + dpType + UN_OBJECT_EXTENSION + ".unEventList.Colors", sColors);
		if (iRes >=0)		// A configuration exists
			{
			dsNamesTemp = strsplit(sNames, UN_EVENT_LIST_CONFIGURATION_DELIMITER);
			dsPositionsTemp = strsplit(sPositions, UN_EVENT_LIST_CONFIGURATION_DELIMITER);
			dsChangesTemp = strsplit(sChanges, UN_EVENT_LIST_CONFIGURATION_DELIMITER);
			dsColorsTemp = strsplit(sColors, UN_EVENT_LIST_CONFIGURATION_DELIMITER);
			length = dynlen(dsNamesTemp);
			if ((length == dynlen(dsPositionsTemp)) && (length == dynlen(dsChangesTemp)) && (length == dynlen(dsColorsTemp)))	// Configuration is valid
				{
				dynClear(dsNames);						// Reset values
				dynClear(diPositions);
				dynClear(dsChanges);	
				dynClear(dsColors);		
				dsNames = dsNamesTemp;					// Set configuration
				dsChanges = dsChangesTemp;
				dsColors = dsColorsTemp;
				for (i=1;i<=length;i++)					// Convert dyn_string to dyn_int
					{
					currentPosition = dsPositionsTemp[i];
					dynAppend(diPositions,currentPosition);
					}
				}
			else
				iRes=-1;
			}
		}

	return iRes;
}
//-----------------------------------------------------------------------------------------------------------------------------

unEventList_panel_getStatusConfiguration(bool bHighBit, string sCurrentDp, dyn_string &dsNames, dyn_string &dsPositions, dyn_string &dsChanges, dyn_string &dsColors)
{
  string sDescription;
  int i, len, iPos;
  dyn_string dsTemp, dsSplit;
  
  dynClear(dsNames);						// Reset values
  dynClear(dsPositions);
  dynClear(dsChanges);	
  dynClear(dsColors);		
//   get the description
  sDescription = unGenericDpFunctions_getDescription(sCurrentDp);
//   extract the bits defined
  dsTemp = strsplit(sDescription, ",");
//   build the configuration
  len = dynlen(dsTemp);
  for(i=1;i<=len;i++) {
    dsSplit = strsplit(dsTemp[i], "=");
    if(dynlen(dsSplit) >=2) {
      iPos = dsSplit[1];
      if(bHighBit) {
        if(iPos <=15)
          dynClear(dsSplit);
        else{
          iPos -=16;
          dsSplit[1] = iPos;
        }
      }
      else {
        if(iPos >15)
          dynClear(dsSplit);
      }
      if(dynlen(dsSplit) >=2) {
          dynAppend(dsColors, "");
          dynAppend(dsChanges, "A");
          dynAppend(dsPositions, dsSplit[1]);
          dynAppend(dsNames, dsSplit[2]);
      }
    }
  }
  //unGenericDpFunctions_DebugTN(DEBUG_ONLY_BIT0, "EventList getStatusConfiguration", "getStatusConfiguration", sCurrentDp, bHighBit, sDescription, dsColors, dsChanges, dsPositions, dsNames);
}

//-----------------------------------------------------------------------------------------------------------------------------

//colorTable function
//This function returns the forecolor of the current line (empty if no specific color has to be used)

unEventList_colorTable(string lineColor, string sInvalid, string &color)
{
	color = "";
	if (sInvalid == getCatStr("unEventList","INVALIDDATA"))
		{
		color = "unDataNotValid";
		}
	else
		{
		if (lineColor != UN_EVENT_LIST_COLOR_DEFAULT)
			{
			color = lineColor;
			}
		}
}

//-----------------------------------------------------------------------------------------------------------------------------

//BeforeAfterWorking function
//This function disables(enables) shapes, show(hide) a waiting panel when starting(stopping) working

unEventList_BeforeAfterWorking(bool bFinish)
{
long qEnd;

	if (bFinish)
	{
	  unEventList_EnableDisable(bFinish);
 		unEventList_updateInfo();
	  
//	  to avoid to keep ProgressBar opened
	  qEnd=(long)getCurrentTime();

	  if ((qEnd-g_qStart)<=(UN_ALERT_DELAY/100))			//UN_ALERT_DELAY=100 ms
			delay(0, UN_ALERT_DELAY*10);		

//DebugTN("end wait");
		unGraphicalFrame_PanelOffPanel(UN_EVENT_LIST_WAIT_PANEL_NAME);	
           removeSymbol(myModuleName(), myPanelName(), UN_EVENT_LIST_WAIT_PANEL_NAME);
	}
	else		
	{
//DebugTN("START wait");
		g_qStart=(long)getCurrentTime();
               addSymbol(myModuleName(), myPanelName(), "vision/graphicalFrame/applicationWait.pnl",UN_EVENT_LIST_WAIT_PANEL_NAME,
                         makeDynString("$Color_actif:unAlarm_Ok","$Color_passif:_3DFace","$Min:1","$Max:20"),
                         10, 630, 0, 4.7, 1);
		unGraphicalFrame_ChildPanelOnCentral("vision/graphicalFrame/applicationWait.pnl",UN_EVENT_LIST_WAIT_PANEL_NAME,makeDynString("$Color_actif:unAlarm_Ok","$Color_passif:_3DFace","$Min:1","$Max:20"));
	  unEventList_EnableDisable(bFinish);
	}


}

//-----------------------------------------------------------------------------------------------------------------------------

unEventList_EnableDisable(bool bEnabled)
{
		setMultiValue("BeginTimeDay","enabled", bEnabled,
								  "BeginTimeHour","enabled", bEnabled,
								  "BeginTimeMinute","enabled", bEnabled,
								  "BeginTimeMonth","enabled", bEnabled,
								  "BeginTimeSecond","enabled", bEnabled,
								  "BeginTimeYear","enabled", bEnabled,
								  "EndTimeDay","enabled", bEnabled,
								  "EndTimeHour","enabled", bEnabled,
								  "EndTimeMinute","enabled", bEnabled,
								  "EndTimeMonth","enabled", bEnabled,
								  "EndTimeSecond","enabled", bEnabled,
								  "EndTimeYear","enabled", bEnabled,
								  "ExportToExcel","enabled", bEnabled,
								  "PrintTableButton","enabled",bEnabled,
								  "lines_Maximum","enabled", bEnabled,
								  "request","enabled", bEnabled,
								  "Start","enabled", bEnabled,
								  "Now","enabled", bEnabled,
								  "BackBackBack","enabled",bEnabled,
								  "BackBack","enabled",bEnabled,
								  "Back","enabled",bEnabled,
								  "Next","enabled",bEnabled,
								  "NextNext","enabled",bEnabled,
								  "NextNextNext","enabled",bEnabled,
								  "txtwait","visible", !bEnabled,
								  "filter_Object","enabled",bEnabled,
								  "filter_Alias","enabled",bEnabled,
								  "filter_Bit","enabled",bEnabled,
								  "filter_Description","enabled",bEnabled,
								  "filter_Domain","enabled",bEnabled,
								  "filter_Nature","enabled",bEnabled,
								  "filter_Event","enabled",bEnabled,
								  "filter_Invalid","enabled",bEnabled,
								  "openConfig","enabled",bEnabled,
								  "saveConfig","enabled",bEnabled && g_bGranted,
								  "LinesFilter","visible",bEnabled,								  
								  "application_list","enabled",bEnabled,
								  "List","visible",bEnabled);
}

//-----------------------------------------------------------------------------------------------------------------------------

//displayNextNext function
//This function is called to display a new full page of events
//The search of new events begins at the (g_iLastDisplayedLine-1) line in g_tableDisplay

time unEventList_displayNextNext()
{
bool last;			// Current message has to be displayed if _allowDisplay = true
int oldValueLast, oldValueFirst, i, iLen, iTime, iEvents;
string color, msg;
dyn_string dynTime, colorText, dynSystem, dynAlias, dynDescription, dynDomain, dynNature, dynBitName, dynEvent, dynInvalid, dynObject, dynApplication, dynTimeUTC, dynTimeLocal;
dyn_anytype historicMessageSplit;
time tLastEvent;
time t1, t2;
  
  t1 = getCurrentTime();
	iEvents=dynlen(g_tableDisplay);
	g_iFilter=0;
// DebugN("display", g_sFilter, dynlen(g_tableDisplay), dynlen(g_tableHistory));		
	if (iEvents > 0)
		{
		unEventList_BeforeAfterWorking(FALSE);
				
		oldValueLast = g_iLastDisplayedLine;
		oldValueFirst = g_iFirstDisplayedLine;
		last = true;
		
		g_iFilter=0;
		txtwait.text="Filtering Events (" + iEvents + "): please wait";										
		for (i=iEvents; i>=1; i--)	// go through all History		
			{
			historicMessageSplit = strsplit(g_tableDisplay[i],UN_EVENT_LIST_DELIMITER);
			if (dynlen(historicMessageSplit) == UN_EVENT_LIST_FIELD)
				{
				iTime++;
				dynAppend(dynTime, CtrlEventList_displayLocalTimefromString(historicMessageSplit[UN_EVENT_LIST_FIELD_TIME]));	//dll
		
				if (unEventList__allowDisplay(g_sFilter, historicMessageSplit)>0)					// If message is allowed to be displayed, append fields
					{
					g_iFilter++;	// number of Events corresponding with the filter							
					if ((i<=g_iLastDisplayedLine) && (i>=1) && (dynlen(dynTimeLocal) < g_iLinesMax))		// NEW: to be able to cout g_iFilter
						{
						if (last)
							{
							g_iLastDisplayedLine = i;
							last = false;
							}			
												
						dynAppend(dynSystem, historicMessageSplit[UN_EVENT_LIST_FIELD_SYSTEM]);
						dynAppend(dynTimeUTC, CtrlEventList_displayUTCTime(historicMessageSplit[UN_EVENT_LIST_FIELD_TIME]));	//dll
						dynAppend(dynTimeLocal, dynTime[iTime]);		//dll						
						dynAppend(dynAlias, historicMessageSplit[UN_EVENT_LIST_FIELD_ALIAS]);
						dynAppend(dynDescription, historicMessageSplit[UN_EVENT_LIST_FIELD_DESCRIPTION]);
						dynAppend(dynDomain, historicMessageSplit[UN_EVENT_LIST_FIELD_DOMAIN]);
						dynAppend(dynNature, historicMessageSplit[UN_EVENT_LIST_FIELD_NATURE]);
						dynAppend(dynBitName, historicMessageSplit[UN_EVENT_LIST_FIELD_BITNAME]);
						dynAppend(dynEvent, historicMessageSplit[UN_EVENT_LIST_FIELD_EVENT]);
						dynAppend(dynInvalid, historicMessageSplit[UN_EVENT_LIST_FIELD_INVALID]);
						dynAppend(dynObject, historicMessageSplit[UN_EVENT_LIST_FIELD_OBJECT]);
						dynAppend(dynApplication, historicMessageSplit[UN_EVENT_LIST_FIELD_APPLICATION]);
						
						unEventList_colorTable(historicMessageSplit[UN_EVENT_LIST_FIELD_COLOR], historicMessageSplit[UN_EVENT_LIST_FIELD_INVALID], color);
						dynAppend(colorText, color);
						
						g_iFirstDisplayedLine = i;
						}
					}
				}                       
			}

		if ((oldValueLast != g_iLastDisplayedLine) || 
			(oldValueFirst != g_iFirstDisplayedLine) ||
			(g_iFirstDisplayedLine == g_iLastDisplayedLine))
			{

			List.deleteAllLines();
			iLen=dynlen(dynTime);
			if (iLen>0)
				tLastEvent=dynTime[1];
			
			List.appendLines(dynlen(dynTimeLocal),							// Display table
										   "List_SystemName", dynSystem,
											 "List_Time",dynTimeLocal,
											 "List_Time_UTC",dynTimeUTC, 
											 "List_Alias", dynAlias,
											 "List_Description", dynDescription,
											 "List_Domain", dynDomain,
											 "List_Nature", dynNature,
											 "List_BitName", dynBitName,
											 "List_Event", dynEvent,
											 "List_Invalid", dynInvalid,
											 "List_Object", dynObject,
											 "List_Application", dynApplication);
			
			iLen = dynlen(colorText);
			for(i=1;i<=iLen;i++)
				{
				setMultiValue("List","cellForeColRC",i-1,"List_SystemName", colorText[i],
											"List","cellForeColRC",i-1,"List_Time", colorText[i],
											"List","cellForeColRC",i-1,"List_Time_UTC", colorText[i],
										  "List","cellForeColRC",i-1,"List_Alias", colorText[i],
										  "List","cellForeColRC",i-1,"List_Description", colorText[i],
										  "List","cellForeColRC",i-1,"List_Domain", colorText[i],
										  "List","cellForeColRC",i-1,"List_Nature", colorText[i],
										  "List","cellForeColRC",i-1,"List_BitName", colorText[i],
										  "List","cellForeColRC",i-1,"List_Event", colorText[i],
										  "List","cellForeColRC",i-1,"List_Invalid", colorText[i],
										  "List","cellForeColRC",i-1,"List_Object", colorText[i],
										  "List","cellForeColRC",i-1,"List_Application", colorText[i]); 
				}
			List.lineVisible(0);
			}
		
		unEventList_BeforeAfterWorking(TRUE);	
		}
		
  t2 = getCurrentTime();
  unEventList_printTime("displayNextNext", t1, t2);
  
	return tLastEvent;
}

//-----------------------------------------------------------------------------------------------------------------------------

//updateInfo function
//This function is called to display info on events and displayed events

unEventList_updateInfo()
{

	List.sort("List_Time_UTC", "List_BitName");

	if (g_iFirstDisplayedLine == 0 || g_iEvents==0)
		{
		index1.text="0";
		index2.text="0";		
		}
	else
		{
		index1.text=g_iFirstDisplayedLine;		
		index2.text=g_iLastDisplayedLine;
		}
		
	LinesFilter.text = g_iFilter;
	LinesInfo.text = g_iEvents;		

}

//-----------------------------------------------------------------------------------------------------------------------------

//displayFilter function
//Display the current filter configuration saved in the global g_sFilter

unEventList_displayFilter()
{
dyn_string dsFilter, dsTemp;
string currentFilter, g_filterAll, sGranted, sRejected;
int iPos;
	
	dsFilter = strsplit(g_sFilter, UN_EVENT_LIST_FILTER_DELIMITER);
//DebugTN(g_sFilter);
        if(dynlen(dsFilter) == UN_EVENT_LIST_FILTER_FIELD)
          dynAppend(dsFilter, "*");
	if (dynlen(dsFilter) != (UN_EVENT_LIST_FILTER_FIELD+1)) 
        {
		dsFilter = strsplit(UN_EVENT_LIST_FILTER_DEFAULT, UN_EVENT_LIST_FILTER_DELIMITER);
                dynAppend(dsFilter, "*");
        }
	
//	 1. Object
	currentFilter = dsFilter[UN_EVENT_LIST_FILTER_FIELD_OBJECT];
	filter_Object2.text = "";
	if (strltrim(strrtrim(currentFilter)) == "")
		filter_Object.text = g_filterAll;
	else
		{
		dsTemp = strsplit(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
		if (dynlen(dsTemp) > 1)
			{
			filter_Object.text = UN_EVENT_LIST_LIST_NAME;
			filter_Object2.text = currentFilter;
			}
		else
			filter_Object.text = currentFilter;
		}

//	 2. Alias
	currentFilter = dsFilter[UN_EVENT_LIST_FILTER_FIELD_ALIAS];
	dsTemp = strsplit(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);

	if (dynlen(dsTemp) > 1)
		{
		filter_Alias.text = UN_EVENT_LIST_LIST_NAME;
		
		iPos=strpos(currentFilter, UN_GRANTED_REJECTED_DELIMITER);
		if (iPos>=0)
			{
			sGranted = substr(currentFilter, 0, iPos);
			sRejected = substr(currentFilter, iPos + strlen(UN_GRANTED_REJECTED_DELIMITER) + 1);
			}

		filter_Alias2.text = sGranted;
		filter_Alias3.text = sRejected;
		}
	else
		{
		strreplace(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER, "");
		filter_Alias.text = currentFilter;
		filter_Alias2.text = "";
		filter_Alias3.text = "";		
		}

//	 3. Description
	if (strltrim(strrtrim(dsFilter[UN_EVENT_LIST_FILTER_FIELD_DESCRIPTION])) == "")
		filter_Description.text = g_filterAll;
	else
		filter_Description.text = dsFilter[UN_EVENT_LIST_FILTER_FIELD_DESCRIPTION];
		
//	 4. Domain
	currentFilter = dsFilter[UN_EVENT_LIST_FILTER_FIELD_DOMAIN];
	filter_Domain2.text = "";
	dsTemp = strsplit(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
	if (dynlen(dsTemp) > 1)
		{
		filter_Domain.text = UN_EVENT_LIST_LIST_NAME;
		filter_Domain2.text = currentFilter;
		}
	else
		{
		strreplace(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER, "");
		filter_Domain.text = currentFilter;
		}

//	 5. Nature
	currentFilter = dsFilter[UN_EVENT_LIST_FILTER_FIELD_NATURE];
	filter_Nature2.text = "";
	dsTemp = strsplit(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
	if (dynlen(dsTemp) > 1)
		{
		filter_Nature.text = UN_EVENT_LIST_LIST_NAME;
		filter_Nature2.text = currentFilter;
		}
	else
		{
		strreplace(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER, "");		
		filter_Nature.text = currentFilter;
		}	
	
//	 6. Bit
	currentFilter = dsFilter[UN_EVENT_LIST_FILTER_FIELD_BITNAME];
	filter_Bit2.text = "";
	if (strltrim(strrtrim(currentFilter)) == "")
		{
		filter_Bit.text = g_filterAll;
		}
	else
		{
		dsTemp = strsplit(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
		if (dynlen(dsTemp) > 1)
			{
			filter_Bit.text = UN_EVENT_LIST_LIST_NAME;
			filter_Bit2.text = currentFilter;
			}
		else
			filter_Bit.text = currentFilter;
		}
		
//	 7. Event     
	currentFilter = dsFilter[UN_EVENT_LIST_FILTER_FIELD_EVENT];
	filter_Event2.text = "";
	if (strltrim(strrtrim(currentFilter)) == "")
		{
		filter_Event.text = g_filterAll;
		}
	else
		{
		dsTemp = strsplit(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
		if (dynlen(dsTemp) > 1)
			{
			filter_Event.text = UN_EVENT_LIST_LIST_NAME;
			filter_Event2.text = currentFilter;
			}
		else
			filter_Event.text = currentFilter;
		}
		
        
        		
//	 8. Invalid
	if (strltrim(strrtrim(dsFilter[UN_EVENT_LIST_FILTER_FIELD_INVALID])) == "")
		filter_Invalid.text = g_filterAll;
	else
		filter_Invalid.text = dsFilter[UN_EVENT_LIST_FILTER_FIELD_INVALID];
        
  //       application
	currentFilter = dsFilter[UN_EVENT_LIST_FILTER_FIELD+1];
	application_list2.text = "";
	if (strltrim(strrtrim(currentFilter)) == "")
		application_list.text = g_filterAll;
	else
		{
		dsTemp = strsplit(currentFilter, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
		if (dynlen(dsTemp) > 1)
			{
			application_list.text = UN_EVENT_LIST_LIST_NAME;
			application_list2.text = currentFilter;
			}
		else
			application_list.text = currentFilter;
		}
        

  unEventList_panel_checkFilter();
}

//-----------------------------------------------------------------------------------------------------------------------------

//getBitNames function
//Return available bit names in StsReg

unEventList_getBitNames(dyn_string& dsBitNames)
{
dyn_string dsDpConfig, dsCurrentNames;
string sCurrentNames;
int i, length;
	
	dsDpConfig = dpNames("*","_UnObjects");
	length = dynlen(dsDpConfig);
	for(i=1;i<=length;i++)
		{
		dpGet(dsDpConfig[i] + ".unEventList.Names", sCurrentNames);
		dsCurrentNames = strsplit(sCurrentNames, ";");
		dynAppend(dsBitNames, dsCurrentNames);
		}
		
	dynUnique(dsBitNames);
	dynSortAsc(dsBitNames);
}

//-----------------------------------------------------------------------------------------------------------------------------

//displayBackBack function
//This function is called to display a new full page of events

unEventList_displayBackBack()
{
bool first;							// Current message has to be displayed if _allowDisplay = true
int oldValueLast, oldValueFirst, i, length, iTime, iEvents;
dyn_string historicMessageSplit;			// Contains 5 elements : time, alias, description, bitName, event
dyn_string dynTime, dynTimeUTC, dynAlias, dynDescription, dynBitName, dynEvent, dynInvalid, dynSystem, dynDomain, dynNature, dynObject, dynApplication, dynTimeLocal;
dyn_string colorText; 
string color, msg;
time tLastEvent;
	
	iEvents=dynlen(g_tableDisplay);
	
	if (iEvents > 0)
		{
		txtwait.text="Filtering Events: please wait.";
		oldValueLast = g_iLastDisplayedLine;
		oldValueFirst = g_iFirstDisplayedLine;
		first = true;
		
		for(i=g_iFirstDisplayedLine;((i<=iEvents)&&(dynlen(dynTimeLocal) < g_iLinesMax));i++)
			{
			historicMessageSplit = strsplit(g_tableDisplay[i],UN_EVENT_LIST_DELIMITER);
			if (dynlen(historicMessageSplit) == UN_EVENT_LIST_FIELD)
				{
				iTime++;
				dynAppend(dynTime, CtrlEventList_displayLocalTimefromString(historicMessageSplit[UN_EVENT_LIST_FIELD_TIME]));			//dll							
				if (unEventList__allowDisplay(g_sFilter, historicMessageSplit)>0)					// If message is allowed to be displayed, append fields
					{					
					if (first)
						{
						g_iFirstDisplayedLine = i;
						first = false;
						}
					
					dynAppend(dynSystem, historicMessageSplit[UN_EVENT_LIST_FIELD_SYSTEM]); 	
					dynAppend(dynTimeUTC, CtrlEventList_displayUTCTime(historicMessageSplit[UN_EVENT_LIST_FIELD_TIME]));		//dll
					dynAppend(dynTimeLocal, dynTime[iTime]);			//dll			
					dynAppend(dynAlias, historicMessageSplit[UN_EVENT_LIST_FIELD_ALIAS]);
					dynAppend(dynDescription, historicMessageSplit[UN_EVENT_LIST_FIELD_DESCRIPTION]);
					dynAppend(dynDomain, historicMessageSplit[UN_EVENT_LIST_FIELD_DOMAIN]);
					dynAppend(dynNature, historicMessageSplit[UN_EVENT_LIST_FIELD_NATURE]);
					dynAppend(dynBitName, historicMessageSplit[UN_EVENT_LIST_FIELD_BITNAME]);
					dynAppend(dynEvent, historicMessageSplit[UN_EVENT_LIST_FIELD_EVENT]);
					dynAppend(dynInvalid, historicMessageSplit[UN_EVENT_LIST_FIELD_INVALID]);
					dynAppend(dynObject, historicMessageSplit[UN_EVENT_LIST_FIELD_OBJECT]);
					dynAppend(dynApplication, historicMessageSplit[UN_EVENT_LIST_FIELD_APPLICATION]);
					
					unEventList_colorTable(historicMessageSplit[UN_EVENT_LIST_FIELD_COLOR], historicMessageSplit[UN_EVENT_LIST_FIELD_INVALID], color);
					dynAppend(colorText, color);

					g_iLastDisplayedLine = i;
					}
				}
			}

		if ((g_iFirstDisplayedLine != oldValueFirst)||
				(g_iLastDisplayedLine != oldValueLast)  ||
				(g_iFirstDisplayedLine==g_iLastDisplayedLine)) 
			{
			iTime=dynlen(dynTime);
			if (iTime>0)
				tLastEvent=dynTime[1];	
			
			List.deleteAllLines();
			List.appendLines(dynlen(dynTimeLocal),							// Display table
											 "List_SystemName",dynSystem,
											 "List_Time",dynTimeLocal,
												"List_Time_UTC",dynTimeUTC,							  
											 "List_Alias", dynAlias,
											 "List_Description", dynDescription,
											 "List_Domain", dynDomain,
											 "List_Nature", dynNature,
											 "List_BitName", dynBitName,
											 "List_Event", dynEvent,
											 "List_Invalid", dynInvalid,
											 "List_Object", dynObject,
											 "List_Application", dynApplication);
									 
			length = dynlen(colorText);
			for(i=1;i<=length;i++)
				{
				setMultiValue("List","cellForeColRC",i-1,"List_SystemName", colorText[i],
											"List","cellForeColRC",i-1,"List_Time", colorText[i],
											"List","cellForeColRC",i-1,"List_Time_UTC", colorText[i],
										  "List","cellForeColRC",i-1,"List_Alias", colorText[i],
										  "List","cellForeColRC",i-1,"List_Description", colorText[i],
										  "List","cellForeColRC",i-1,"List_Domain", colorText[i],
										  "List","cellForeColRC",i-1,"List_Nature", colorText[i],
										  "List","cellForeColRC",i-1,"List_BitName", colorText[i],
										  "List","cellForeColRC",i-1,"List_Event", colorText[i],
										  "List","cellForeColRC",i-1,"List_Invalid", colorText[i],
										  "List","cellForeColRC",i-1,"List_Object", colorText[i],
										  "List","cellForeColRC",i-1,"List_Application", colorText[i]);
				}
			List.lineVisible(0);
			}
		}
}

//-------------------------------------------------------------------------------------------
// CB function to know Systems status
unEventList_panel_EventList_TreeDevCB(string sCommandDp, int iCommand, string sSystemNameDp, string sSystemName)
{
//DebugTN("panel_EventList_TreeDevCB", sCommandDp, iCommand, sSystemName);
  switch(iCommand) {
    case DEVUN_ADD_COMMAND:
    case DEVUN_DELETE_COMMAND:
      synchronized(g_dsTreeApplication) {
//     build the list of all connected system and display
        unEventList_panel_buildSubsystemList();
        unEventList_EnableDisable(false);
        unEventList_displayFilter();
        unEventList_panel_checkFilter();
        unEventList_EnableDisable(true);
      }
      break;
  }
}

//-------------------------------------------------------------------------------------------

unEventList_panel_buildSubsystemList()
{
  int i, len;
  string sSystemName;
  dyn_string dsTemp;
  
  g_dsSubApplications = g_dsTreeApplication;
  len = mappinglen(g_m_bTreeDeviceOverviewSystemConnected);
//   DebugTN("len of g_m_bTreeDeviceOverviewSystemConnected " + len);
  for(i=1;i<=len;i++) {
    sSystemName = mappingGetKey(g_m_bTreeDeviceOverviewSystemConnected, i);
    g_bSystemState[sSystemName] = g_m_bTreeDeviceOverviewSystemConnected[sSystemName];
    dsTemp = g_m_dsTreeApplication[sSystemName];
    dynAppend(dsTemp, "*");
    g_m_dsSubApplications[sSystemName] = dsTemp;
  }
  application_list.items = g_dsTreeApplication;
  filter_Domain.items = g_dsTreeSubsystem1;
  filter_Nature.items = g_dsTreeSubsystem2;
// DebugN("buildSubsystemList", g_bSystemState);
  unLists_setSystemState();		
}

//-------------------------------------------------------------------------------------------

unEventList_displayColumns()
{
	setValue(	"List","namedColumnVisibility", "List_SystemName", viewSystem);
	setValue(	"List","namedColumnVisibility", "List_Time", viewTime);
	setValue(	"List","namedColumnVisibility", "List_Time_UTC",viewTimeUTC);			
	setValue(	"List","namedColumnVisibility", "List_Alias",viewAlias);
	setValue(	"List","namedColumnVisibility", "List_Description",viewDescription);
	setValue(	"List","namedColumnVisibility", "List_Domain",viewDomain);
	setValue(	"List","namedColumnVisibility", "List_Nature",viewNature);
	setValue(	"List","namedColumnVisibility", "List_BitName",viewBit);
	setValue(	"List","namedColumnVisibility", "List_Event",viewEvent);
	setValue(	"List","namedColumnVisibility", "List_Invalid",viewInvalid);
	setValue(	"List","namedColumnVisibility", "List_Object",viewObject);
	setValue(	"List","namedColumnVisibility", "List_Application",viewApplication);
}

//-------------------------------------------------------------------------------------------

string unEventList_getFilter()
{
string sFilter, sAlias, sDescription, sDomain, sNature, sBit, sEvent, sInvalid, sObject;

//	 1. Get info
	sAlias = filter_Alias.text;
	if (sAlias == UN_EVENT_LIST_LIST_NAME)
		sAlias = filter_Alias2.text + UN_GRANTED_REJECTED_DELIMITER + UN_EVENT_LIST_DELIMITER + filter_Alias3.text;
	else if (sAlias == "")
		sAlias += UN_EVENT_LIST_DELIMITER;	

	sDescription = filter_Description.text;
        
	sEvent = filter_Event.text;
	if (sEvent == UN_EVENT_LIST_LIST_NAME)
		sEvent = filter_Event2.text;
	else if (sEvent == "")
		sEvent += UN_EVENT_LIST_DELIMITER;	
        
	sInvalid = filter_Invalid.text;
	
	sDomain = filter_Domain.text;
	if (sDomain == UN_EVENT_LIST_LIST_NAME)
		sDomain = filter_Domain2.text;
	else if (sDomain == "")
		sDomain += UN_EVENT_LIST_DELIMITER;	
	
	sNature = filter_Nature.text;
	if (sNature == UN_EVENT_LIST_LIST_NAME)
		sNature = filter_Nature2.text;
	else if (sNature == "")
		sNature += UN_EVENT_LIST_DELIMITER;	
	
	sBit = filter_Bit.text;
	if (sBit == UN_EVENT_LIST_LIST_NAME)
		sBit = filter_Bit2.text;
	
	sObject = filter_Object.text;
	if (sObject == UN_EVENT_LIST_LIST_NAME)
		sObject = filter_Object2.text;

	g_sFrom=application_list.text();
	if (g_sFrom==UN_EVENT_LIST_LIST_NAME)
		g_sFrom=application_list2.text();

		
//	 2. Set filter
	sFilter =		sAlias + UN_EVENT_LIST_FILTER_DELIMITER + sDescription + UN_EVENT_LIST_FILTER_DELIMITER +
							sDomain + UN_EVENT_LIST_FILTER_DELIMITER + sNature + UN_EVENT_LIST_FILTER_DELIMITER +
							sBit + UN_EVENT_LIST_FILTER_DELIMITER + sEvent + UN_EVENT_LIST_FILTER_DELIMITER + 
							sInvalid + UN_EVENT_LIST_FILTER_DELIMITER + sObject +
                                                        UN_EVENT_LIST_FILTER_DELIMITER + g_sFrom;

        if(g_sFrom == "*")
        {
          g_dsSelectedApplication = g_dsSubApplications;
          dynRemove(g_dsSelectedApplication, 1);
          dynRemove(g_dsSelectedApplication, dynlen(g_dsSelectedApplication));
        }
        else
          g_dsSelectedApplication=strsplit(g_sFrom, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
        if(sObject == "*")
          g_dsSelectedDeviceType=g_dsUnicosObjects;
        else
          g_dsSelectedDeviceType=strsplit(sObject, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
	return sFilter;
}



/*
  Print the time in millisecond
  */
void unEventList_printTime( string msg, time start, time end)
{
  int ms1, ms2, sec1, sec2, min1, min2, h1, h2, d1, d2, m1, m2, y1, y2;
  long total1, total2;
  
  ms1 = milliSecond( start);    ms2 = milliSecond( end);
  sec1 = second( start);       sec2 = second( end);
  min1 = minute( start);       min2 = minute( end); 
  h1 = hour( start);           h2 = hour( end);
  d1 = day( start);            d2 = day( end);
  m1 = month( start);          m2 = month( end);
  y1 = year ( start);          y2 = year( end);
  
  total1 = ms1 + sec1*1000 + min1*60*1000 + h1*60*60*1000;
  total2 = ms2 + sec2*1000 + min2*60*1000 + h2*60*60*1000;
 
  //DebugTN("[time] " + msg + " " + (total2 - total1) + "ms");  
  
}

