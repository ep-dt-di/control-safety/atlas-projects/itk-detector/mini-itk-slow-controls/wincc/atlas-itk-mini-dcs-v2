/**@file
unExportDevice.ctl
This library contains the function used to Export Devices Config

@author
 Frederic BERNARD (AB/CO)

@par Creation Date
 21/04/2005

@par Modification History:

  12/09/2013: Marco Boccioli
  - @jira{IS-521} new function unExportDevice_checkFormat() it checks the format o analog value and the logarithmic scale for the trending.
  Modified function unExportDevice_setFormat(). It accepts second parameter logarithmic scale for trending.
  New function unExportDevice_getFormat() it returns the format o analog value and the logarithmic scale for the trending.
  
  12/06/2012: Marco Boccioli
  - @jira{IS-899} the export device exports now the PLC type as a couple PROTOCOL,MODEL.
  For example, instead of ;S7-PLC; now it exports ;S7,S7-300; and instead of ;PREMIUM; it does ;MODBUS,PREMIUM;
  
  30/03/2012: Herve
  - @jira{IS-855} unCore - General Variable <DELIMITER> already defined
  renamed to DPE_ARCHIVE_DELIMITER
  
  30/03/2012: Herve
  - @jira{IS-755}  unCore - unFrontEnd  Front-end (SIEMENS and SCHNEIDER PLCS UCPC v6) Information updated: baseline and 
  application versions added; spec version dropped. Front-end diagnostic panel updated accordingly. 
  
  20/03/2012: Herve
  - @jira{IS-692}  unCore - unFrontEnd  Schneider PLC front-end: Include Specs/App version and "PLC modbus address-of-the-application/specs version" 
  - @jira{IS-738}  unCore - unFrontEnd  SCHNEIDER PLC: new field "resources package employed" used for the CPC 6
  - @jira{IS-739}  unCore - unFrontEnd  mix Com and Data file for the _UnPlc front-end part of the device configuration
  - @jira{IS-735}  unCore - unFrontEnd  SIEMENS PLC: new field "resources package employed" used for the CPC 6 
 
  22/02/2012: Herve
  - @jira{IS-720}  bug fix: getting the archive DPE config with ORACLE archive returns no archive within the UNICOS export device utility. 
  
  02/05/2011: Herve
  - @jira{IS-510}: CPC 5 and CPC 6 compatibility of the export/import of the CPC device configuration with all the front-end types
  
  01/12/2008: Herve
    - old/new smoothing config
    modified function: unExportDevice_getDeadBand
    - put deadband value to 0 if deadband type = 0 (no deadband/smoothing config) or 3 (old-new/smooting config)
    modified function: _UnPlc_AnalogInput_ExportConfig, _UnPlc_AnalogOutput_ExportConfig, _UnPlc_AnaDig_ExportConfig,
    _UnPlc_Analog_ExportConfig

  16/11/2008  Quique
      - Export Alarm: includes its PCO and Type if exists (new function: unExportDevice_getAlarmMasterType)
      
  03/11/2008  Quique
      - Export AnaDig: when no archive is set the "0" must be set in TIME instead of "#VALUE!
      - Export config controller: the archive settings is splited in two: MV first and then OUT.
      - Export config controller: in case of not smooth set in MV the export was wrong (showed #VALUE!)
      - Export config analog input: bug when no archive is defined (showed #VALUE! instead of "")
      
  28/10/2008: Quique
      - _UnPlc_Controller_ExportConfig  modified to cope the new cases.

  21/10/2008: Quique
     - _unPlc_Analog_ExportConfig modified to taken into account possible double defintion of the stsReg01, evStsReg01
     - unExportDevice_getAddressConfig modified for the ANALOG case.
     
  02/06/2008: Herve
    - unExportDevice_getArchiveNames: archive PosAl 
    
  28/12/2007: Herve
    - device acccess control
    
  02/10/2007: Herve
		- bug in unExportDevice_getPCOParameters in case not 8 option mode
		
  18/07/2007: Herve
		new functions:
			- unExportDevice_getAnalogDPEAlarmConfig
			- unExportDevice_getAnalogNormalPosition1DPE
			- unExportDevice_getDigitalNormalPositionDPE

  02/07/2007: Herve
		- new function unExportDevices_getSmoothingDPE
		
  25/04/2007: Herve
		- deadband and deadbandtype for SPMV and OUT instead of both
		
  23/02/2007: Herve
		- add new format xEXP: x nbr of digits after the . in exp format. 
		Function modified: unExportDevice_convertFormat

  03/11/2006: Herve
		- extended archiving unExportDevice_getArchiveTemp
  VA,xxx.xx,A/O/N= absolute value smoothing with A: and time smoothing/O: or time smoothing/N or anything else no time smoothing
  VR,xxx.xx, A/O/N= relative value smoothing based on min/max with A: and time smoothing/O: or time smoothing/N or anything else no time smoothing

  13/07/2006: Frederic
		- in unExportDevice_getLimits()
		  default limits are 0.0 amd not based on PvRange
		
  29/06/2006: Herve
		- bug in unExportDevice_convertFormat() in case ### for ex.
		
  27/06/2006: Enrique
		- Export the PID default addresses. (Before only the values were exported)

  13/06/2006: Herve
		- case _UnPlc front-end device type, the default P, I, D and TDS are not used
		
  27/04/2006: Herve
		- mask ev
		
  20/04/2006: Frederic:
		- _UnPlc_Controller_ExportConfig(): bug export
		
  10/04/2006: Herve
		- implementation of link between devices: modified function unExportDevice_getAllCommonParameters()
		
  23/03/2006: Herve
		- unExportDevice_writeTransitionToFile(): add application to get the correct time

  22/03/2006: Herve
		- in the function unExportDevice_floatFormat(): wrong convertion in case of exp format .0 added when < 1e-6
		
  07/11/2005: Herve
		- in the function unExportDevice_writeTransitionToFile() replace Object by Device type

  09/09/2005: Enrique
		- Modify the function: S7_PLC_Com_ExportConfig()
		  to include the exportation of the UNICOS application version (PLC version and PVSS version)
		  
  06/09/2005: Herve
		- bug in export of SystemAlarm and _SystemAlarm for S7_PLC and _UnPlc

  07/07/2005: Enrique
		- add the function S7_PLC_getConfigToExport();
		 	to read the Comms parameters (TSSP extras, IP, rack, slot)
		 	
  07/07/2005: Herve
		- add the export for S7_PLC, unicos devices function for S7 PLC and S7 unicos address function
		
  01/07/2005: Herve
		- unExportDevice_getAllCommonParameters(): get the device number from the dpname not from the address config (does not work for S7)
		- rename unExportDevice_getDpSmooth to unExportDevice_getAddressConfig
		- in unExportDevice_getAddressConfig do an evalScript to get the address config in the UNICOS format of the driver, this 
		is based on the drv_ident value: the function DRV_drv_ident_convertToUnicosAddress must exists
			MODBUS: DRV_MODBUS_convertToUnicosAddress
			OPCLCIENT: DRV_OPC_convertToUnicosAddress
			S7: DRV_S7_convertToUnicosAddress
		- add the analog/digital input export for OPC
		
  01/06/2005: Herve
		- unExportDevice_getArchiveTemp: if smoothing old/new -> use UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_AND_TIME


@par Usage
Public. 
PVSS manager usage: Ctrl, NV, NG

@par Constraints
	. JCOP framework
	. PVSS version: 3.0.1
	. operating system: Wds2000 & wds XP
	. distributed system: No
*/

#uses "libunCore/unCoreDeprecated.ctl"

//Constants
const string DPE_ARCHIVE_DELIMITER = "~";


//@{


//------------------------------------------------------------------------------------------------------------------------
// _UnPlc_Com_ExportConfig
/**
Purpose: Export _UnPlc config 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_Com_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo)
{
dyn_string dsDpParameters;
string sUnit, sMod, sFEType, sVersion;
	
	if (dynlen(dsParam)==UN_CONFIG_EXPORT_LENGTH)
		{
    if(_UnPlc_isCPC6(dsParam[UN_CONFIG_EXPORT_PLCNAME]))
         _UnPlc_CPC6_Export(dsParam);
    else
    {
//DebugN("CPC5 export", dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		unExportDevice_general(dsParam[UN_CONFIG_EXPORT_PLCNAME], sFEType, sUnit, sMod, exceptionInfo);
		
		//Delete
		dynClear(dsDpParameters);
		dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
		unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
	
		//PLCCONFIG
		dynClear(dsDpParameters);
		dynAppend(dsDpParameters, UN_PLC_COMMAND);
		dynAppend(dsDpParameters, sFEType);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
		dynAppend(dsDpParameters, sUnit);
		sVersion = _UnPlc_getVersion(dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		if(sVersion != "")
			dynAppend(dsDpParameters, sVersion);

		unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    }
		}
	else
		fwException_raise(exceptionInfo, "ERROR", "_UnPlc_Com_ExportConfig(): Wrong Parameters", "");		
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_getVersion
/**
Purpose: Get the Fe version of the Modbus PLC 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
string _UnPlc_getVersion(string sPlcName)
{
  string sVersion;
  dpGet(c_unSystemIntegrity_UnPlc + sPlcName + ".version.import", sVersion);
  return sVersion;
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_isCPC6
/**
Purpose: check if the _UnPlc is a CPC6 PLC 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
bool _UnPlc_isCPC6(string sPlcName)
{
  bool bReturn = false;
  string sDrvType, sAddress=UN_CONFIG_EXPORT_UNDEF, sFunction, sDp=c_unSystemIntegrity_UnPlc + sPlcName;
  
  if(dpExists(sDp))
  {
    dpGet(sDp + ".version.PLCresourcePackageMajor:_address.._drv_ident" , sDrvType);
    sFunction = "DRV_"+sDrvType+"_convertToUnicosAddress";
    if(isFunctionDefined(sFunction)) {
      evalScript(sAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
                        "  string temp;"+
                        "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                        "  return temp;"+
                        "}", makeDynString(), 
                        sDp + ".version.PLCresourcePackageMajor", 
                        "_UnPlc");
    }
    else
      sAddress = UN_CONFIG_EXPORT_UNDEF;
  }
  bReturn = (sAddress == UN_CONFIG_EXPORT_UNDEF)? false:true;
//  DebugTN("_UnPlc_isCPC6", sPlcName, bReturn);
  return bReturn;
}

//------------------------------------------------------------------------------------------------------------------------

// _UnPlc_CPC6_Export
/**
Purpose: Export the _UnPlc CPC6 front-end config 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_CPC6_Export(dyn_string dsParam)
{
  string sVersion;
  int i, j, iLen, digitalOKRange, iNbr, alertRanges;
  dyn_int limitNumbers;
  dyn_float alertLimits;
  string sSendIpAddress, sUnit, sMod, sAlarmAddress, sAlarmAddressTemp, sAlarmType, sFEType, sType, sAlertText;
  string sSubIpAddress, sCommandInterfaceTemp, sCounterAddress, sIpAddress, sCommandInterface, sSendIpAddressTemp, sCounterAddressTemp, sSplitModPlc; 
  dyn_string dsCommandInterface, dsCounterAddress, dsAlarmDps, dsAlarmAddress, dsSplitIpAddress, dsIpAddress, dsSendIpAddress, dsAlertText;
  string sFunction;
  dyn_string exceptionInfo, dsDpParameters;

  if (dynlen(dsParam)==UN_CONFIG_EXPORT_LENGTH)
  {

    unExportDevice_general(dsParam[UN_CONFIG_EXPORT_PLCNAME], sFEType, sUnit, sMod, exceptionInfo);
    
    //Delete
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
  
    //PLCCONFIG
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, UN_PLC_COMMAND);
    dynAppend(dsDpParameters, sFEType);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
    dynAppend(dsDpParameters, sUnit);

    //Get IP Address 
    if (dpExists(sMod))
    {
      dpGet(  sMod + ".HostsAndPorts", dsSplitIpAddress);
//       dsSplitIpAddress=strsplit(sSubIpAddress , "|");
      
      if (dynlen(dsSplitIpAddress)>0)
        dsIpAddress=strsplit(dsSplitIpAddress[1] , ":");
      
      if (dynlen(dsIpAddress)>0)
        sIpAddress = dsIpAddress[1];
      
    //Get Redudnant IP Address if exists
      dpGet(  sMod + ".ReduHostsAndPorts", dsSplitIpAddress);
      if (dynlen(dsSplitIpAddress)>0)
      {      
//         dsSplitIpAddress=strsplit(sSubIpAddress , "|");
      
        if (dynlen(dsSplitIpAddress)>0)
          dsIpAddress=strsplit(dsSplitIpAddress[1] , ":");
      
        if (dynlen(dsIpAddress)>0)
          sIpAddress = sIpAddress + UN_PARAMETER_SUBITEM_DELIMITER + dsIpAddress[1];
      }
    }      
    if (dpExists(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME]))
    {
      //Get SendIP Address
      dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.send_IP:_address.._drv_ident" , sSendIpAddress);
      sFunction = "DRV_"+sSendIpAddress+"_convertToUnicosAddress";
      if(isFunctionDefined(sFunction)) {
        evalScript(sSendIpAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
                          "  string temp;"+
                          "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                          "  return temp;"+
                          "}", makeDynString(), 
                          c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.send_IP", 
                          "_UnPlc");
      }
      else
        sSendIpAddress = UN_CONFIG_EXPORT_UNDEF;

      //Get counter Address
      dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.counter:_address.._drv_ident" , sCounterAddress);
      sFunction = "DRV_"+sCounterAddress+"_convertToUnicosAddress";
      if(isFunctionDefined(sFunction)) {
        evalScript(sCounterAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
                          "  string temp;"+
                          "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                          "  return temp;"+
                          "}", makeDynString(), 
                          c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.counter", 
                          "_UnPlc");
      }
      else
        sCounterAddress = UN_CONFIG_EXPORT_UNDEF;
    
      //Get CommandInterface Address
      dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.commandInterface:_address.._drv_ident" , sCommandInterface);
      sFunction = "DRV_"+sCommandInterface+"_convertToUnicosAddress";
      if(isFunctionDefined(sFunction)) {
        evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType) {"+
                          "  string temp;"+
                          "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                          "  return temp;"+
                          "}", makeDynString(), 
                          c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.commandInterface", 
                          "_UnPlc");
      }
      else
        sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
    }        
    dynAppend(dsDpParameters, sIpAddress);
    dynAppend(dsDpParameters, sSendIpAddress);
    dynAppend(dsDpParameters, sCounterAddress);
    dynAppend(dsDpParameters, sCommandInterface);
    // get UN_CONFIG_UNPLC_CPC6_PLC_BASELINE_ADDRESS =9; // Addressing to read the version of the PLC baseline (float)
    dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCBaseline:_address.._drv_ident" , sCommandInterface);
    sFunction = "DRV_"+sCommandInterface+"_convertToUnicosAddress";
    if(isFunctionDefined(sFunction)) {
      evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType) {"+
                        "  string temp;"+
                        "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                        "  return temp;"+
                        "}", makeDynString(), 
                        c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCBaseline", 
                        "_UnPlc");
    }
    else
      sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
    dynAppend(dsDpParameters, sCommandInterface);
    // get UN_CONFIG_UNPLC_CPC6_PLC_APPLICATION_ADDRESS =10; // Addressing to read the version of the PLC application (float)
    dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCApplication:_address.._drv_ident" , sCommandInterface);
    sFunction = "DRV_"+sCommandInterface+"_convertToUnicosAddress";
    if(isFunctionDefined(sFunction)) {
      evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType) {"+
                        "  string temp;"+
                        "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                        "  return temp;"+
                        "}", makeDynString(), 
                        c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCApplication", 
                        "_UnPlc");
    }
    else
      sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
    dynAppend(dsDpParameters, sCommandInterface);
    // get UN_CONFIG_UNPLC_CPC6_RESPACK_MAJOR_VERSION_ADDRESS = 11; // (TSPP address word) WORD, Addressing to read the major version of the Resource Package
    dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCresourcePackageMajor:_address.._drv_ident" , sCommandInterface);
    sFunction = "DRV_"+sCommandInterface+"_convertToUnicosAddress";
    if(isFunctionDefined(sFunction)) {
      evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType) {"+
                        "  string temp;"+
                        "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                        "  return temp;"+
                        "}", makeDynString(), 
                        c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCresourcePackageMajor", 
                        "_UnPlc");
    }
    else
      sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
    dynAppend(dsDpParameters, sCommandInterface);
    // get UN_CONFIG_UNPLC_CPC6_RESPACK_MINOR_VERSION_ADDRESS = 12; // (TSPP address word) WORD, Addressing to read the minor version of the Resource Package
    dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCresourcePackageMinor:_address.._drv_ident" , sCommandInterface);
    sFunction = "DRV_"+sCommandInterface+"_convertToUnicosAddress";
    if(isFunctionDefined(sFunction)) {
      evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType) {"+
                        "  string temp;"+
                        "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                        "  return temp;"+
                        "}", makeDynString(), 
                        c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCresourcePackageMinor", 
                        "_UnPlc");
    }
    else
      sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
    dynAppend(dsDpParameters, sCommandInterface);
    // get UN_CONFIG_UNPLC_CPC6_RESPACK_SMALL_VERSION_ADDRESS= 13; // (TSPP address word) WORD, Addressing to read the small version of the Resource Package
    dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCresourcePackageSmall:_address.._drv_ident" , sCommandInterface);
    sFunction = "DRV_"+sCommandInterface+"_convertToUnicosAddress";
    if(isFunctionDefined(sFunction)) {
      evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType) {"+
                        "  string temp;"+
                        "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                        "  return temp;"+
                        "}", makeDynString(), 
                        c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.PLCresourcePackageSmall", 
                        "_UnPlc");
    }
    else
      sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
    dynAppend(dsDpParameters, sCommandInterface);
    // get UN_CONFIG_UNPLC_CPC6_RESPACK_VERSION = 14; // (String) : version of the Resource Package 
    dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".version.resourcePackage", sCommandInterface);
    dynAppend(dsDpParameters, sCommandInterface);
    
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
    
    //FESystemAlarm, SystemAlarm
    fputs("#FESystemAlarm (SystemAlarm)" + "\n" , g_fileToExport);
    dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + FE_pattern + c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + "_*", c_unSystemAlarm_dpType);
  
    iLen=dynlen(dsAlarmDps);
    for(i=1; i<=iLen; i++)
    {
      if (dpExists(dsAlarmDps[i]))
      {
        if(sFEType == UN_CONFIG_PREMIUM)
          fputs("#For " + UN_CONFIG_PREMIUM + " " + i + "\n" , g_fileToExport);
          
        if(sFEType == UN_CONFIG_QUANTUM)
          fputs("#For " + UN_CONFIG_QUANTUM + " " + i + "\n" , g_fileToExport);
                      
        if(sFEType == UN_CONFIG_UNITY)
          fputs("#For " + UN_CONFIG_UNITY + " " + i + "\n" , g_fileToExport);

        sSplitModPlc = substr(dsAlarmDps[i], strpos(dsAlarmDps[i], dsParam[UN_CONFIG_EXPORT_PLCNAME]) + strlen(dsParam[UN_CONFIG_EXPORT_PLCNAME]) + 1);
        
        //Get Alarm Address
        dpGet(dsAlarmDps[i] + ".alarm:_address.._drv_ident" , sAlarmAddress);
        sFunction = "DRV_"+sAlarmAddress+"_convertToUnicosAddress";
        if(isFunctionDefined(sFunction)) {
          evalScript(sAlarmAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
                            "  string temp;"+
                            "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                            "  return temp;"+
                            "}", makeDynString(), 
                            dsAlarmDps[i] + ".alarm", 
                            c_unSystemAlarm_dpType);
        }
        else
          sAlarmAddress = UN_CONFIG_EXPORT_UNDEF;
        
        dsAlertText = makeDynString();
        //Get alarm type
        dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl.._type", sAlarmType);
        if(sAlarmType == DPCONFIG_ALERT_NONBINARYSIGNAL)
        {
          fwAlertConfig_getLimits(dsAlarmDps[i] + ".alarm", limitNumbers, alertLimits, exceptionInfo);
          alertRanges = (dynMax(limitNumbers) + 1);
          for(j=1; j<=alertRanges; j++)
          {
            dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl." + j + "._text", sAlertText);
            dynAppend(dsAlertText, sAlertText);
          }
                                      
          if(dynlen(dsAlertText) > 0)
          {
            if (dsAlertText[1]==UN_SYSTEMINTEGRITY_OK)
            {
              sType = "TRUE";
            }
            else
            {
              sType = "FALSE";
            }
          }
          else
            sType = UN_CONFIG_EXPORT_UNDEF;
        }
        else
          sType = UN_CONFIG_EXPORT_UNDEF;  
                
        dynClear(dsDpParameters);
        dynAppend(dsDpParameters, UN_FESYSTEMALARM_COMMAND);
        dynAppend(dsDpParameters, i);                                                              //???? nbr
        dynAppend(dsDpParameters, sSplitModPlc);                                                  //identifier
        dynAppend(dsDpParameters, sAlarmAddress);                                                  //address
        dynAppend(dsDpParameters, sType);                                                          //alarm type
        dynAppend(dsDpParameters, unGenericDpFunctions_getDescription(dsAlarmDps[i] + ".alarm"));  //description
        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
      }
    }

    //PLC_PLC system _SystemAlarm for backward compatibility
    fputs("#PLC_PLC_SystemAlarm (_SystemAlarm)" + "\n" , g_fileToExport);
    dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + PLC_PLC_pattern + c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + "_*", c_unSystemAlarm_dpType);
  
    iLen=dynlen(dsAlarmDps);
    for(i=1; i<=iLen; i++)
    {
      if (dpExists(dsAlarmDps[i]))
      {
        if(sFEType == UN_CONFIG_PREMIUM)
          fputs("#For " + UN_CONFIG_PREMIUM + " " + i + "\n" , g_fileToExport);
          
        if(sFEType == UN_CONFIG_QUANTUM)
          fputs("#For " + UN_CONFIG_QUANTUM + " " + i + "\n" , g_fileToExport);
                      
        if(sFEType == UN_CONFIG_UNITY)
          fputs("#For " + UN_CONFIG_UNITY + " " + i + "\n" , g_fileToExport);

        sSplitModPlc = substr(dsAlarmDps[i], strpos(dsAlarmDps[i], dsParam[UN_CONFIG_EXPORT_PLCNAME]) + strlen(dsParam[UN_CONFIG_EXPORT_PLCNAME]) + 1);
        
        //Get Alarm Address
        dpGet(dsAlarmDps[i] + ".alarm:_address.._drv_ident" , sAlarmAddress);
        sFunction = "DRV_"+sAlarmAddress+"_convertToUnicosAddress";
        if(isFunctionDefined(sFunction)) {
          evalScript(sAlarmAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
                            "  string temp;"+
                            "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                            "  return temp;"+
                            "}", makeDynString(), 
                            dsAlarmDps[i] + ".alarm", 
                            c_unSystemAlarm_dpType);
        }
        else
          sAlarmAddress = UN_CONFIG_EXPORT_UNDEF;
        
        dsAlertText = makeDynString();
        //Get alarm type
        dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl.._type", sAlarmType);
        if(sAlarmType == DPCONFIG_ALERT_NONBINARYSIGNAL)
        {
          fwAlertConfig_getLimits(dsAlarmDps[i] + ".alarm", limitNumbers, alertLimits, exceptionInfo);
          alertRanges = (dynMax(limitNumbers) + 1);
          for(j=1; j<=alertRanges; j++)
          {
            dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl." + j + "._text", sAlertText);
            dynAppend(dsAlertText, sAlertText);
          }
                                      
          if(dynlen(dsAlertText) > 0)
          {
            if (dsAlertText[1]==UN_SYSTEMINTEGRITY_OK)
            {
              sType = "TRUE";
            }
            else
            {
              sType = "FALSE";
            }
          }
          else
            sType = UN_CONFIG_EXPORT_UNDEF;
        }
        else
          sType = UN_CONFIG_EXPORT_UNDEF;  
                
        dynClear(dsDpParameters);
        dynAppend(dsDpParameters, c_unSystemAlarm_dpType);
        dynAppend(dsDpParameters, i);                                                              //???? nbr
        dynAppend(dsDpParameters, sSplitModPlc);                                                  //identifier
        dynAppend(dsDpParameters, sAlarmAddress);                                                  //address
        unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
      }
    }
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "_UnPlc_Com_ExportConfig(): Wrong Parameters", "");    
}

//------------------------------------------------------------------------------------------------------------------------

// S7_PLC_Com_ExportConfig
/**
Purpose: Export the S7_PLC front-end config 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_Com_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo)
{
  int iRes, i, j, iLen, digitalOKRange, iNbr, alertRanges;
  dyn_int limitNumbers;
  dyn_float alertLimits;
  string sAlarmAddress, sAlarmType, sAlertText, sPlcName, sPlcDrv, sSplitModPlc;
  string sType, sFEType, sDp, sCounterAddress, sCommandInterface, sSendIpAddress,sPLCDataInfo, sPLCVersion,sPVSSVersion;
  dyn_string dsDpParameters, dsAlarmDps, dsAlarmAddress, dsSplitIpAddress, dsAlertText;
  dyn_string dsAnalogArchive, dsEventArchive, dsBoolArchive;
  string sFunction, sVersion;
  dyn_string dsTemp;

  // DebugN("FUNCTION: S7_PLC_Com_ExportConfig(dsParam= "+dsParam);
  if( dynlen(dsParam) == UN_CONFIG_EXPORT_LENGTH )
  {
    sPlcName = dsParam[UN_CONFIG_EXPORT_PLCNAME];
    sDp      = c_unSystemIntegrity_S7 + sPlcName;
    iRes     = dpGet(sDp + ".configuration.archive_bool",   dsBoolArchive,
                     sDp + ".configuration.archive_analog", dsAnalogArchive,
                     sDp + ".configuration.archive_event",  dsEventArchive,
                     sDp + ".communication.driver_num",     sPlcDrv,
                     sDp + ".configuration.type",           sFEType,
                     sDp + ".version.import",               sVersion);
    // Add protocol to the PLC type
    if( sFEType == "polling" )
    {
      unsigned uPollInterval;
      string sPollingGroupDp = "_poll_S7_PLC_" + sPlcName;
      dpGet(sPollingGroupDp + ".PollInterval", uPollInterval);

      sFEType = UN_CONFIG_PROTOCOL_S7 + "," + sFEType + "," + uPollInterval;
    }
    else
      sFEType = UN_CONFIG_PROTOCOL_S7 + "," + sFEType;

    if( unGenericObject_shapeExists("TextFieldDriver") )
    {
      TextFieldDriver.text = sPlcDrv;
    }

    if( unGenericObject_shapeExists("TextFieldBoolArchive") &&
        dynlen(dsBoolArchive)   > 0                         &&
        dynlen(dsAnalogArchive) > 0                         &&
        dynlen(dsEventArchive)  > 0                           )
    {
      TextFieldBoolArchive.text   = dsBoolArchive[1];
      TextFieldAnalogArchive.text = dsAnalogArchive[1];
      TextFieldEventArchive.text  = dsEventArchive[1];
    }

    if( unGenericObject_shapeExists("frontEndVersion") )
    {
      frontEndVersion.text = sVersion;
    }

    // Delete
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

    // PLCCONFIG
    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, "# Config Line : PLCCONFIG;type;fe_name;app_name;local_id;local_rack;local_slot;local_conn_resource;partner_rack;partner_slot;partner_conn_resource;connection_timeout;plc_ip_address;plc_ds_ip_address;counter_address;command_interface_address;send_ip_address;baseline_version_address;app_version_address;rp_version_x_address;rp_version_y_address;rp_version_z_address;rp_version");
    unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);

    dynClear(dsDpParameters);
    dynAppend(dsDpParameters, UN_PLC_COMMAND);
    dynAppend(dsDpParameters, sFEType);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
    dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);

    // Get S7 PLC Config
    S7_PLC_getConfigToExport(sDp, dsTemp, exceptionInfo);
    if( dynlen(exceptionInfo) <= 0 )
    {
      dynAppend(dsDpParameters, dsTemp);

      // Get address config
      // Get SendIP Address
      dpGet(sDp + ".communication.send_IP:_address.._drv_ident" , sSendIpAddress);
      if( sSendIpAddress != "" )
      {
        sFunction = "DRV_" + sSendIpAddress + "_convertToUnicosAddress";
        if( isFunctionDefined(sFunction) )
        {
          evalScript(sSendIpAddress, "string main(string sDeviceDpeName, string sDeviceType)"   +
                                     "{"                                                        +
                                     "  string temp;"                                           +
                                     "  temp = " + sFunction + "(sDeviceDpeName, sDeviceType);" +
                                     "  return temp;"                                           +
                                     "}",
                                     makeDynString(),
                                     sDp + ".communication.send_IP", S7_PLC_DPTYPE);
        }
        else
        {
          sSendIpAddress = "";
          //sSendIpAddress = UN_CONFIG_EXPORT_UNDEF;
        }
      }


      // Get counter Address
      dpGet(sDp + ".communication.counter:_address.._drv_ident" , sCounterAddress);
      if( sCounterAddress != "" )
      {
        sFunction = "DRV_" + sCounterAddress + "_convertToUnicosAddress";
        if( isFunctionDefined(sFunction) )
        {
          evalScript(sCounterAddress, "string main(string sDeviceDpeName, string sDeviceType)"   +
                                      "{"                                                        +
                                      "  string temp;"                                           +
                                      "  temp = " + sFunction + "(sDeviceDpeName, sDeviceType);" +
                                      "  return temp;"                                           +
                                      "}",
                                      makeDynString(),
                                      sDp + ".communication.counter", S7_PLC_DPTYPE);
        }
        else
        {
          sCounterAddress = "";
          // sCounterAddress = UN_CONFIG_EXPORT_UNDEF;
        }
      }


      // Get CommonInterface Address
      dpGet(sDp + ".communication.commandInterface.function:_address.._drv_ident" , sCommandInterface);
      if( sCommandInterface != "" )
      {
        sFunction = "DRV_" + sCommandInterface + "_convertToUnicosAddress";
        if( isFunctionDefined(sFunction) )
        {
          evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType)"   +
                                        "{"                                                        +
                                        "  string temp;"                                           +
                                        "  temp = " + sFunction + "(sDeviceDpeName, sDeviceType);" +
                                        "  return temp;"                                           +
                                        "}",
                                        makeDynString(),
                                        sDp + ".communication.commandInterface.function", S7_PLC_DPTYPE);
        }
        else
        {
          sCommandInterface = "";
          // sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
        }
      }


      // Get PLCInfo Address
      dpGet(sDp + ".communication.PLCInfo.data1:_address.._drv_ident" , sPLCDataInfo);
      if( sPLCDataInfo != "" )
      {
        sFunction = "DRV_" + sPLCDataInfo + "_convertToUnicosAddress";
        if( isFunctionDefined(sFunction) )
        {
          evalScript(sPLCDataInfo, "string main(string sDeviceDpeName, string sDeviceType)"   +
                                   "{"                                                        +
                                   "  string temp;"                                           +
                                   "  temp = " + sFunction + "(sDeviceDpeName, sDeviceType);" +
                                   "  return temp;"                                           +
                                   "}",
                                   makeDynString(),
                                   sDp + ".communication.PLCInfo.data1", S7_PLC_DPTYPE);
        }
        else
        {
          sPLCDataInfo = "";
          // sPLCDataInfo = UN_CONFIG_EXPORT_UNDEF;
        }
      }


      // Get PLC Version Address
      dpGet(sDp + ".version.PLC:_address.._drv_ident" , sPLCVersion);
      if( sPLCVersion != "" )
      {
        sFunction = "DRV_" + sPLCVersion + "_convertToUnicosAddress";
        if( isFunctionDefined(sFunction) )
        {
          evalScript(sPLCVersion, "string main(string sDeviceDpeName, string sDeviceType)"   +
                                  "{"                                                        +
                                  "  string temp;"                                           +
                                  "  temp = " + sFunction + "(sDeviceDpeName, sDeviceType);" +
                                  "  return temp;"                                           +
                                  "}",
                                  makeDynString(),
                                  sDp + ".version.PLC", S7_PLC_DPTYPE);
        }
        else
        {
          sPLCVersion = "";
          // sPLCVersion = UN_CONFIG_EXPORT_UNDEF;
        }
      }


      // Get the PVSS version (float number)
      dpGet(sDp + ".version.PVSS:_online.._value" , sPVSSVersion);
      if( sPVSSVersion == 0 )
      {
        sPVSSVersion = "";
      }


      dynAppend(dsDpParameters, sSendIpAddress);
      dynAppend(dsDpParameters, sCounterAddress);
      dynAppend(dsDpParameters, sCommandInterface);
      dynAppend(dsDpParameters, sPLCDataInfo);


      if( !isCPC6_PLC(sDp) )
      {
        dynAppend(dsDpParameters, sPLCVersion);
        dynAppend(dsDpParameters, sPVSSVersion);
        if( sVersion != "" )
          dynAppend(dsDpParameters, sVersion);
      }
      else
        S7_PLC_CPC6_Export(sDp, dsDpParameters);

      // write to file
      unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);


      // front-end system alarm
      fputs("# FESystemAlarm (SystemAlarm)" + "\n" , g_fileToExport);
      dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + FE_pattern + c_unSystemIntegrity_S7 + dsParam[UN_CONFIG_EXPORT_PLCNAME] + "_*", c_unSystemAlarm_dpType);
      iLen = dynlen(dsAlarmDps);
      for( i = 1 ; i <= iLen ; i++ )
      {
        if( dpExists(dsAlarmDps[i]) )
        {
          if( sFEType == UN_CONFIG_S7_300 )
            fputs("#For " + UN_CONFIG_S7_300 + " " + i + "\n" , g_fileToExport);

          if( sFEType == UN_CONFIG_S7_400 )
            fputs("#For " + UN_CONFIG_S7_400 + " " + i + "\n" , g_fileToExport);

          sSplitModPlc = substr(dsAlarmDps[i], strpos(dsAlarmDps[i], dsParam[UN_CONFIG_EXPORT_PLCNAME]) + strlen(dsParam[UN_CONFIG_EXPORT_PLCNAME]) + 1);

          // Get Alarm Address
          dpGet(dsAlarmDps[i] + ".alarm:_address.._drv_ident" , sAlarmAddress);
          sFunction = "DRV_" + sAlarmAddress + "_convertToUnicosAddress";
          if( isFunctionDefined(sFunction) )
          {
            evalScript(sAlarmAddress, "string main(string sDeviceDpeName, string sDeviceType)"   +
                                      "{"                                                        +
                                      "  string temp;"                                           +
                                      "  temp = " + sFunction + "(sDeviceDpeName, sDeviceType);" +
                                      "  return temp;"                                           +
                                      "}",
                                      makeDynString(),
                                      dsAlarmDps[i] + ".alarm", c_unSystemAlarm_dpType);
          }
          else
            sAlarmAddress = UN_CONFIG_EXPORT_UNDEF;

          dsAlertText = makeDynString();
          // Get alarm type
          dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl.._type", sAlarmType);
          if( sAlarmType == DPCONFIG_ALERT_NONBINARYSIGNAL )
          {
            fwAlertConfig_getLimits(dsAlarmDps[i] + ".alarm",
                                    limitNumbers,
                                    alertLimits,
                                    exceptionInfo);
            alertRanges = (dynMax(limitNumbers) + 1);
            for( j = 1 ; j <= alertRanges ; j++ )
            {
              dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl." + j + "._text", sAlertText);
              dynAppend(dsAlertText, sAlertText);
            }

            if( dynlen(dsAlertText) > 0 )
            {
              if( dsAlertText[1] == UN_SYSTEMINTEGRITY_OK )
              {
                sType = "TRUE";
              }
              else
              {
                sType = "FALSE";
              }
            }
            else
              sType = UN_CONFIG_EXPORT_UNDEF;
          }
          else
            sType = UN_CONFIG_EXPORT_UNDEF;

          dynClear(dsDpParameters);
          dynAppend(dsDpParameters, UN_FESYSTEMALARM_COMMAND);
          dynAppend(dsDpParameters, i);                        //???? nbr
          dynAppend(dsDpParameters, sSplitModPlc);             //identifier
          dynAppend(dsDpParameters, sAlarmAddress);            //address
          dynAppend(dsDpParameters, sType);                    //alarm type
          dynAppend(dsDpParameters, unGenericDpFunctions_getDescription(dsAlarmDps[i] + ".alarm"));  //description

          // write to file
          unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
        }
      }
    }
  }
  else
    fwException_raise(exceptionInfo, "ERROR", "S7_PLC_Com_ExportConfig(): Wrong Parameters", "");

}

//------------------------------------------------------------------------------------------------------------------------

/**
Purpose: check if the S7_PLC is a CPC6 PLC  

Parameters:
  sDp:   input, string, PLC DP name
	dsDpParameters:						output, dyn_string, CPC6 config

Usage: External function

PVSS manager usage: NG, NV
*/
bool isCPC6_PLC(string sDp)
{
  bool bReturn = false;
  string DRVType, sAddress=UN_CONFIG_EXPORT_UNDEF, sFunction;
  
  if(dpExists(sDp+ ".version.PLCresourcePackageMajor"))
  {
    //Get PLC PLCresourcePackageMajor Address
    dpGet(sDp + ".version.PLCresourcePackageMajor:_address.._drv_ident" , DRVType);
    sFunction = "DRV_"+DRVType+"_convertToUnicosAddress";
    if(isFunctionDefined(sFunction)) {
      evalScript(sAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
                "  string temp;"+
                "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
                "  return temp;"+
                "}", makeDynString(), 
                sDp + ".version.PLCresourcePackageMajor", 
                dpTypeName(sDp));
    }
    else
      sAddress = UN_CONFIG_EXPORT_UNDEF;
  }
  bReturn = (sAddress == UN_CONFIG_EXPORT_UNDEF)? false:true;
//  DebugTN("isCPC6_PLC", sAddress, bReturn);
  return bReturn;
}

//------------------------------------------------------------------------------------------------------------------------

/**
Purpose: S7_PLC_CPC6_Export get CPC6 export config (if any) 

Parameters:
  sDp:   input, string, PLC DP name
	dsDpParameters:						output, dyn_string, CPC6 config

Usage: External function

PVSS manager usage: NG, NV
*/
S7_PLC_CPC6_Export(string sDp, dyn_string &dsDpParameters)
{
  string sFunction, resPack, resPackMajor, resPackMinor, resPackSmall, sPLCBaseline, sPLCApplication;
  string DRVType;
  
  //Get PLC PLCBaseline Address
  dpGet(sDp + ".version.PLCBaseline:_address.._drv_ident" , DRVType);
  sFunction = "DRV_"+DRVType+"_convertToUnicosAddress";
  if(isFunctionDefined(sFunction)) {
    evalScript(sPLCBaseline, "string main(string sDeviceDpeName, string sDeviceType) {"+
              "  string temp;"+
              "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
              "  return temp;"+
              "}", makeDynString(), 
              sDp + ".version.PLCBaseline", 
              S7_PLC_DPTYPE);
  }
  else
    sPLCBaseline = UN_CONFIG_EXPORT_UNDEF;
				
  //Get PLC PLCApplication Address
  dpGet(sDp + ".version.PLCApplication:_address.._drv_ident" , DRVType);
  sFunction = "DRV_"+DRVType+"_convertToUnicosAddress";
  if(isFunctionDefined(sFunction)) {
    evalScript(sPLCApplication, "string main(string sDeviceDpeName, string sDeviceType) {"+
              "  string temp;"+
              "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
              "  return temp;"+
              "}", makeDynString(), 
              sDp + ".version.PLCApplication", 
              S7_PLC_DPTYPE);
  }
  else
    sPLCApplication = UN_CONFIG_EXPORT_UNDEF;
				
  //Get PLC PLCresourcePackageMajor Address
  dpGet(sDp + ".version.PLCresourcePackageMajor:_address.._drv_ident" , DRVType);
  sFunction = "DRV_"+DRVType+"_convertToUnicosAddress";
  if(isFunctionDefined(sFunction)) {
    evalScript(resPackMajor, "string main(string sDeviceDpeName, string sDeviceType) {"+
              "  string temp;"+
              "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
              "  return temp;"+
              "}", makeDynString(), 
              sDp + ".version.PLCresourcePackageMajor", 
              S7_PLC_DPTYPE);
  }
  else
    resPackMajor = UN_CONFIG_EXPORT_UNDEF;
				
  //Get PLC PLCresourcePackageMajor Address
  dpGet(sDp + ".version.PLCresourcePackageMinor:_address.._drv_ident" , DRVType);
  sFunction = "DRV_"+DRVType+"_convertToUnicosAddress";
  if(isFunctionDefined(sFunction)) {
    evalScript(resPackMinor, "string main(string sDeviceDpeName, string sDeviceType) {"+
              "  string temp;"+
              "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
              "  return temp;"+
              "}", makeDynString(), 
              sDp + ".version.PLCresourcePackageMinor", 
              S7_PLC_DPTYPE);
  }
  else
    resPackMinor = UN_CONFIG_EXPORT_UNDEF;
				
  //Get PLC PLCresourcePackageMajor Address
  dpGet(sDp + ".version.PLCresourcePackageSmall:_address.._drv_ident" , DRVType);
  sFunction = "DRV_"+DRVType+"_convertToUnicosAddress";
  if(isFunctionDefined(sFunction)) {
    evalScript(resPackSmall, "string main(string sDeviceDpeName, string sDeviceType) {"+
              "  string temp;"+
              "  temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
              "  return temp;"+
              "}", makeDynString(), 
              sDp + ".version.PLCresourcePackageSmall", 
              S7_PLC_DPTYPE);
  }
  else
    resPackSmall = UN_CONFIG_EXPORT_UNDEF;

  if((resPackMajor != UN_CONFIG_EXPORT_UNDEF) && (resPackMinor != UN_CONFIG_EXPORT_UNDEF) && (resPackSmall != UN_CONFIG_EXPORT_UNDEF))
  {		
    // Get the resourcePackage
    dpGet(sDp + ".version.resourcePackage" , resPack);
    dynAppend(dsDpParameters, sPLCBaseline);
    dynAppend(dsDpParameters, sPLCApplication);
    dynAppend(dsDpParameters, resPackMajor);
    dynAppend(dsDpParameters, resPackMinor);
    dynAppend(dsDpParameters, resPackSmall);
    dynAppend(dsDpParameters, resPack);
  }
}

//------------------------------------------------------------------------------------------------------------------------

/**
Purpose: Groups the S7_PLC comms config 

Parameters:
  sDp:            input,  string,     PLC_name data point (i.e: S7_PLC_plcname)
  dsParamConfig:  output, dyn_string, PLC/PVSS s7driver param: LocalId, TSAP, ...
                                          dsParamConfig[1] = UN_CONFIG_S7_LOCAL_ID,             index 4
                                          dsParamConfig[2] = UN_CONFIG_S7_LOCAL_RACK,           index 5
                                          dsParamConfig[3] = UN_CONFIG_S7_LOCAL_SLOT,           index 6
                                          dsParamConfig[4] = UN_CONFIG_S7_LOCAL_CONNRESOURCE,   index 7
                                          dsParamConfig[5] = UN_CONFIG_S7_PARTNER_RACK,         index 8
                                          dsParamConfig[6] = UN_CONFIG_S7_PARTNER_SLOT,         index 9
                                          dsParamConfig[7] = UN_CONFIG_S7_PARTNER_CONNRESOURCE, index 10
                                          dsParamConfig[8] = UN_CONFIG_S7_TIMEOUT,              index 11
                                          dsParamCOnfig[9] = UN_CONFIG_S7_PLC_IP,               index 12
  exceptionInfo: output, dyn_string, possible errors

Usage: External function

PVSS manager usage: NG, NV
*/

S7_PLC_getConfigToExport(string sDpName, dyn_string &dsParamConfig, dyn_string &exceptionInfo)
{
  unsigned uDeviceNumber;      // device number
  unsigned uReduDeviceNumber;  // device number for redundant PLC

  string sFrontEndName;        // Name of PLC (_S7_Conn)
  string sTemp;

  // Variables for _S7_Config
  dyn_uint duRack;             // Rack
  dyn_uint duSlot;             // Slot
  dyn_uint duTimeout;          // Timeout
  dyn_int diBuffer;
  dyn_string dsTsppExtrasTemp;
  dyn_string dsIpAddress;      // IP address
  dyn_string dsTsppExtras;     // Connection Id, Res. CPId, PC-Rack, PC-Slot, Res. PCId, S7-300


  if( sDpName == "" )
  {
    fwException_raise(exceptionInfo, "ERROR", "S7_PLC_getConfigToExport: " + "No PLC datapoint provided", "");
    return;
  }

  dpGet(sDpName + ".configuration.S7_Conn:_online.._value", sFrontEndName);
  if( !dpExists(sFrontEndName) )
  {
    fwException_raise(exceptionInfo, "ERROR", "S7_PLC_getConfigToExport: PLC not defined internally (_S7_Conn)", "");
    return;
  }

  dpGet(sFrontEndName          + ".DevNr:_online.._value",        uDeviceNumber,       // DeviceNumber identified
        sFrontEndName          + ".ReduCP.DevNr:_online.._value", uReduDeviceNumber,   // Redundant DeviceNumber identified
        S7_PLC_INT_DPTYPE_CONF + ".IPAddress:_original.._value",  dsIpAddress,
        S7_PLC_INT_DPTYPE_CONF + ".Rack:_original.._value",       duRack,
        S7_PLC_INT_DPTYPE_CONF + ".Slot:_original.._value",       duSlot,
        S7_PLC_INT_DPTYPE_CONF + ".Timeout:_original.._value",    duTimeout,
        S7_PLC_INT_DPTYPE_CONF + ".TSPPExtras:_original.._value", dsTsppExtras);

  dsParamConfig[2] = duRack     [uDeviceNumber];
  dsParamConfig[3] = duSlot     [uDeviceNumber];
  dsParamConfig[8] = duTimeout  [uDeviceNumber];
  dsParamConfig[9] = dsIpAddress[uDeviceNumber];

  if( dynlen(dsTsppExtras) > 0 )
  {
    dsTsppExtrasTemp = strsplit(dsTsppExtras[uDeviceNumber], ".");
    diBuffer[1]      = (int)dsTsppExtrasTemp[1];
    diBuffer[2]      = (int)dsTsppExtrasTemp[2];
    diBuffer[3]      = (int)dsTsppExtrasTemp[5];
    // TSPPExtras = "0.0.0.0.0.0";  localID, ResCPId and ResPCId must be converted from INT to HEX
    // TSPPExtras[1] = UN_CONFIG_S7_LOCAL_ID,              4 (HEX)
    // TSPPExtras[2] = UN_CONFIG_S7_LOCAL_CONNRESOURCE,    7 (HEX)
    // TSPPExtras[3] = UN_CONFIG_S7_PARTNER_RACK,          8
    // TSPPExtras[4] = UN_CONFIG_S7_PARTNER_SLOT,          9
    // TSPPExtras[5] = UN_CONFIG_S7_PARTNER_CONNRESOURCE, 10 (HEX)
    // TSPPExtras[6] = 0 (S7-300), 1 (Redundant PLC)

    sprintf(dsParamConfig[1],"%X", diBuffer[1]);  // Local ID (hex)
    strreplace(dsParamConfig[1]," ","0");

    sprintf(dsParamConfig[4],"%X", diBuffer[2]); // Local connection resource (ResCPId) (hex)
    strreplace(dsParamConfig[4]," ","0");

    sprintf(dsParamConfig[7],"%X", diBuffer[3]); // Partner connection resource (ResPCId) (hex)
    strreplace(dsParamConfig[7]," ","0");

    dsParamConfig[5] = dsTsppExtrasTemp[3];  // UN_CONFIG_S7_PARTNER_RACK
    dsParamConfig[6] = dsTsppExtrasTemp[4];  // UN_CONFIG_S7_PARTNER_SLOT
  }
  else
  {
    dsParamConfig[1] = "";
    dsParamConfig[4] = "";
    dsParamConfig[7] = "";
    dsParamConfig[5] = "";
    dsParamConfig[6] = "";
  }


  if( uReduDeviceNumber > 0 )
  {
    // The REDUNDANT PLC is defined: get its connection info as well
    dsParamConfig[2] = dsParamConfig[2] + UN_PARAMETER_SUBITEM_DELIMITER + duRack     [uReduDeviceNumber];
    dsParamConfig[3] = dsParamConfig[3] + UN_PARAMETER_SUBITEM_DELIMITER + duSlot     [uReduDeviceNumber];
    dsParamConfig[8] = dsParamConfig[8] + UN_PARAMETER_SUBITEM_DELIMITER + duTimeout  [uReduDeviceNumber];
    dsParamConfig[9] = dsParamConfig[9] + UN_PARAMETER_SUBITEM_DELIMITER + dsIpAddress[uReduDeviceNumber];

    dsTsppExtrasTemp = strsplit(dsTsppExtras[uReduDeviceNumber], ".");
    diBuffer[1] = (int)dsTsppExtrasTemp[1];
    diBuffer[2] = (int)dsTsppExtrasTemp[2];
    diBuffer[3] = (int)dsTsppExtrasTemp[5];
    // TSPPExtras = "0.0.0.0.0.0";  localID, ResCPId and ResPCId must be converted from INT to HEX

    sprintf(sTemp, "%X", diBuffer[1]);  // Local ID (hex)
    strreplace(sTemp," ","0");
    dsParamConfig[1] = dsParamConfig[1] + UN_PARAMETER_SUBITEM_DELIMITER + sTemp;

    sprintf(sTemp, "%X", diBuffer[2]);  // Local connection resource (ResCPId) (hex)
    strreplace(sTemp, " ", "0");
    dsParamConfig[4] = dsParamConfig[4] + UN_PARAMETER_SUBITEM_DELIMITER + sTemp;

    sprintf(sTemp, "%X", diBuffer[3]);  // Partner connection resource (ResPCId) (hex)
    strreplace(sTemp, " ", "0");
    dsParamConfig[7] = dsParamConfig[7] + UN_PARAMETER_SUBITEM_DELIMITER + sTemp;

    dsParamConfig[5] = dsParamConfig[5] + UN_PARAMETER_SUBITEM_DELIMITER + dsTsppExtrasTemp[3];
    dsParamConfig[6] = dsParamConfig[6] + UN_PARAMETER_SUBITEM_DELIMITER + dsTsppExtrasTemp[4];
  }
  else
  {
    dsParamConfig[1] = "";
    dsParamConfig[4] = "";
    dsParamConfig[7] = "";
    dsParamConfig[5] = "";
    dsParamConfig[6] = "";
  }

}




//------------------------------------------------------------------------------------------------------------------------
// _UnPlc_ComExtended_ExportConfig
/**
Purpose: Export Extended _UnPlc extended config 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
_UnPlc_ComExtended_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo)
{
	int i, j, iLen, digitalOKRange, iNbr, alertRanges;
	dyn_int limitNumbers;
	dyn_float alertLimits;
	string sSendIpAddress, sUnit, sMod, sAlarmAddress, sAlarmAddressTemp, sAlarmType, sFEType, sType, sAlertText;
	string sSubIpAddress, sCommandInterfaceTemp, sCounterAddress, sIpAddress, sCommandInterface, sSendIpAddressTemp, sCounterAddressTemp, sSplitModPlc; 
	dyn_string dsDpParameters, dsCommandInterface, dsCounterAddress, dsAlarmDps, dsAlarmAddress, dsSplitIpAddress, dsIpAddress, dsSendIpAddress, dsAlertText;
	string sFunction;

	if (dynlen(dsParam)==UN_CONFIG_EXPORT_LENGTH)
	{
		unExportDevice_general(dsParam[UN_CONFIG_EXPORT_PLCNAME], sFEType, sUnit, sMod, exceptionInfo);
	
		//Get IP Address 
		if (dpExists(sMod))
		{
			dpGet(	sMod + ".HostsAndPorts", dsSplitIpAddress);
// 			dsSplitIpAddress=strsplit(sSubIpAddress , "|");
			
			if (dynlen(dsSplitIpAddress)>0)
				dsIpAddress=strsplit(dsSplitIpAddress[1] , ":");
			
			if (dynlen(dsIpAddress)>0)
				sIpAddress = dsIpAddress[1];
			
  //Get Redudnant IP Address if exists

    dpGet(  sMod + ".ReduHostsAndPorts", dsSplitIpAddress);
    if (dynlen(dsSplitIpAddress)>0)
    {
//       dsSplitIpAddress=strsplit(sSubIpAddress , "|");
      
      if (dynlen(dsSplitIpAddress)>0)
        dsIpAddress=strsplit(dsSplitIpAddress[1] , ":");
      
      if (dynlen(dsIpAddress)>0)
        sIpAddress = sIpAddress + UN_PARAMETER_SUBITEM_DELIMITER + dsIpAddress[1];
    }
  }   
		if (dpExists(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME]))
		{
			//Get SendIP Address
			dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.send_IP:_address.._drv_ident" , sSendIpAddress);
			sFunction = "DRV_"+sSendIpAddress+"_convertToUnicosAddress";
			if(isFunctionDefined(sFunction)) {
				evalScript(sSendIpAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
													"	string temp;"+
													"	temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
													"	return temp;"+
													"}", makeDynString(), 
													c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.send_IP", 
													"_UnPlc");
			}
			else
				sSendIpAddress = UN_CONFIG_EXPORT_UNDEF;

			//Get counter Address
			dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.counter:_address.._drv_ident" , sCounterAddress);
			sFunction = "DRV_"+sCounterAddress+"_convertToUnicosAddress";
			if(isFunctionDefined(sFunction)) {
				evalScript(sCounterAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
													"	string temp;"+
													"	temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
													"	return temp;"+
													"}", makeDynString(), 
													c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.counter", 
													"_UnPlc");
			}
			else
				sCounterAddress = UN_CONFIG_EXPORT_UNDEF;
		
			//Get CommonInterface Address
			dpGet(c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.commandInterface:_address.._drv_ident" , sCommandInterface);
			sFunction = "DRV_"+sCommandInterface+"_convertToUnicosAddress";
			if(isFunctionDefined(sFunction)) {
				evalScript(sCommandInterface, "string main(string sDeviceDpeName, string sDeviceType) {"+
													"	string temp;"+
													"	temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
													"	return temp;"+
													"}", makeDynString(), 
													c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + ".communication.commandInterface", 
													"_UnPlc");
			}
			else
				sCommandInterface = UN_CONFIG_EXPORT_UNDEF;
		}				
		//fputs("PLCCONFIG_extended;" + sIpAddress + ";" + sSendIpAddress + ";" + sCounterAddress + ";" + sCommandInterface + ";\n" , g_fileToExport );
		dynClear(dsDpParameters);
		dynAppend(dsDpParameters, UN_PLC_COMMAND_EXTENDED);
		dynAppend(dsDpParameters, sIpAddress);
		dynAppend(dsDpParameters, sSendIpAddress);
		dynAppend(dsDpParameters, sCounterAddress);
		dynAppend(dsDpParameters, sCommandInterface);
		unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
								
		//FESystemAlarm, SystemAlarm
		fputs("#FESystemAlarm (SystemAlarm)" + "\n" , g_fileToExport);
		dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + FE_pattern + c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + "_*", c_unSystemAlarm_dpType);
	
		iLen=dynlen(dsAlarmDps);
		for(i=1; i<=iLen; i++)
		{
			if (dpExists(dsAlarmDps[i]))
			{
				if(sFEType == UN_CONFIG_PREMIUM)
					fputs("#For " + UN_CONFIG_PREMIUM + " " + i + "\n" , g_fileToExport);
					
				if(sFEType == UN_CONFIG_QUANTUM)
					fputs("#For " + UN_CONFIG_QUANTUM + " " + i + "\n" , g_fileToExport);
											
				if(sFEType == UN_CONFIG_UNITY)
					fputs("#For " + UN_CONFIG_UNITY + " " + i + "\n" , g_fileToExport);

				sSplitModPlc = substr(dsAlarmDps[i], strpos(dsAlarmDps[i], dsParam[UN_CONFIG_EXPORT_PLCNAME]) + strlen(dsParam[UN_CONFIG_EXPORT_PLCNAME]) + 1);
				
				//Get Alarm Address
				dpGet(dsAlarmDps[i] + ".alarm:_address.._drv_ident" , sAlarmAddress);
				sFunction = "DRV_"+sAlarmAddress+"_convertToUnicosAddress";
				if(isFunctionDefined(sFunction)) {
					evalScript(sAlarmAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
														"	string temp;"+
														"	temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
														"	return temp;"+
														"}", makeDynString(), 
														dsAlarmDps[i] + ".alarm", 
														c_unSystemAlarm_dpType);
				}
				else
					sAlarmAddress = UN_CONFIG_EXPORT_UNDEF;
				
				dsAlertText = makeDynString();
				//Get alarm type
				dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl.._type", sAlarmType);
				if(sAlarmType == DPCONFIG_ALERT_NONBINARYSIGNAL)
				{
					fwAlertConfig_getLimits(dsAlarmDps[i] + ".alarm", limitNumbers, alertLimits, exceptionInfo);
					alertRanges = (dynMax(limitNumbers) + 1);
					for(j=1; j<=alertRanges; j++)
					{
						dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl." + j + "._text", sAlertText);
						dynAppend(dsAlertText, sAlertText);
					}
																			
					if(dynlen(dsAlertText) > 0)
					{
						if (dsAlertText[1]==UN_SYSTEMINTEGRITY_OK)
						{
							sType = "TRUE";
						}
						else
						{
							sType = "FALSE";
						}
					}
					else
						sType = UN_CONFIG_EXPORT_UNDEF;
				}
				else
					sType = UN_CONFIG_EXPORT_UNDEF;	
								
				dynClear(dsDpParameters);
				dynAppend(dsDpParameters, UN_FESYSTEMALARM_COMMAND);
				dynAppend(dsDpParameters, i);																															//???? nbr
				dynAppend(dsDpParameters, sSplitModPlc);																									//identifier
				dynAppend(dsDpParameters, sAlarmAddress);																									//address
				dynAppend(dsDpParameters, sType);																													//alarm type
				dynAppend(dsDpParameters, unGenericDpFunctions_getDescription(dsAlarmDps[i] + ".alarm"));	//description
				unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
			}
		}

		//PLC_PLC system _SystemAlarm for backward compatibility
		fputs("#PLC_PLC_SystemAlarm (_SystemAlarm)" + "\n" , g_fileToExport);
		dsAlarmDps = dpNames(c_unSystemAlarm_dpPattern + PLC_PLC_pattern + c_unSystemIntegrity_UnPlc + dsParam[UN_CONFIG_EXPORT_PLCNAME] + "_*", c_unSystemAlarm_dpType);
	
		iLen=dynlen(dsAlarmDps);
		for(i=1; i<=iLen; i++)
		{
			if (dpExists(dsAlarmDps[i]))
			{
				if(sFEType == UN_CONFIG_PREMIUM)
					fputs("#For " + UN_CONFIG_PREMIUM + " " + i + "\n" , g_fileToExport);
					
				if(sFEType == UN_CONFIG_QUANTUM)
					fputs("#For " + UN_CONFIG_QUANTUM + " " + i + "\n" , g_fileToExport);
											
				if(sFEType == UN_CONFIG_UNITY)
					fputs("#For " + UN_CONFIG_UNITY + " " + i + "\n" , g_fileToExport);

				sSplitModPlc = substr(dsAlarmDps[i], strpos(dsAlarmDps[i], dsParam[UN_CONFIG_EXPORT_PLCNAME]) + strlen(dsParam[UN_CONFIG_EXPORT_PLCNAME]) + 1);
				
				//Get Alarm Address
				dpGet(dsAlarmDps[i] + ".alarm:_address.._drv_ident" , sAlarmAddress);
				sFunction = "DRV_"+sAlarmAddress+"_convertToUnicosAddress";
				if(isFunctionDefined(sFunction)) {
					evalScript(sAlarmAddress, "string main(string sDeviceDpeName, string sDeviceType) {"+
														"	string temp;"+
														"	temp = "+sFunction+"(sDeviceDpeName, sDeviceType);"+
														"	return temp;"+
														"}", makeDynString(), 
														dsAlarmDps[i] + ".alarm", 
														c_unSystemAlarm_dpType);
				}
				else
					sAlarmAddress = UN_CONFIG_EXPORT_UNDEF;
				
				dsAlertText = makeDynString();
				//Get alarm type
				dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl.._type", sAlarmType);
				if(sAlarmType == DPCONFIG_ALERT_NONBINARYSIGNAL)
				{
					fwAlertConfig_getLimits(dsAlarmDps[i] + ".alarm", limitNumbers, alertLimits, exceptionInfo);
					alertRanges = (dynMax(limitNumbers) + 1);
					for(j=1; j<=alertRanges; j++)
					{
						dpGet(dsAlarmDps[i] + ".alarm:_alert_hdl." + j + "._text", sAlertText);
						dynAppend(dsAlertText, sAlertText);
					}
																			
					if(dynlen(dsAlertText) > 0)
					{
						if (dsAlertText[1]==UN_SYSTEMINTEGRITY_OK)
						{
							sType = "TRUE";
						}
						else
						{
							sType = "FALSE";
						}
					}
					else
						sType = UN_CONFIG_EXPORT_UNDEF;
				}
				else
					sType = UN_CONFIG_EXPORT_UNDEF;	
								
				dynClear(dsDpParameters);
				dynAppend(dsDpParameters, c_unSystemAlarm_dpType);
				dynAppend(dsDpParameters, i);																															//???? nbr
				dynAppend(dsDpParameters, sSplitModPlc);																									//identifier
				dynAppend(dsDpParameters, sAlarmAddress);																									//address
				unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
			}
		}
	}
	else
		fwException_raise(exceptionInfo, "ERROR", "_UnPlc_ComExtended_ExportConfig(): Wrong Parameters", "");	
}	


//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_general
/**
Purpose:  set General settings in Export Panel

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_general(string sPlcName, string &sFrontEndType, string &sUnitAddress, string &sModPLC, dyn_string &ExceptionInfoTemp)
{
int iRes;
bool bEvent;
string sPlcDrv, sEvent;
dyn_string dsBoolArchive, dsAnalogArchive, dsEventArchive;
string sVersion;

	if (dpExists(c_unSystemIntegrity_UnPlc + sPlcName))
		{
		if(dpGet(c_unSystemIntegrity_UnPlc + sPlcName + ".configuration.mod_PLC", sModPLC) == -1)
			{
			fwException_raise(ExceptionInfoTemp, "ERROR", "getCommonParameters(): Error getting : " + c_unSystemIntegrity_UnPlc + sPlcName + ".configuration.mod_PLC", "");	
			}
						
		iRes = dpGet(	c_unSystemIntegrity_UnPlc + sPlcName + ".configuration.type", sFrontEndType,
									c_unSystemIntegrity_UnPlc + sPlcName + ".configuration.archive_bool", dsBoolArchive,
									c_unSystemIntegrity_UnPlc + sPlcName + ".configuration.archive_analog", dsAnalogArchive,
									c_unSystemIntegrity_UnPlc + sPlcName + ".configuration.archive_event", dsEventArchive,
									c_unSystemIntegrity_UnPlc + sPlcName + ".communication.driver_num", sPlcDrv,
									c_unSystemIntegrity_UnPlc + sPlcName + ".version.import", sVersion);
    //add protocol to the PLC type
  sFrontEndType = UN_CONFIG_PROTOCOL_MODBUS + "," + sFrontEndType;
		}
	else
		fwException_raise(ExceptionInfoTemp, "ERROR", "getCommonParameters(): " + getCatStr("unGeneration", "DPDOESNOTEXIST"), c_unSystemIntegrity_UnPlc + sPlcName);		
	
	if (dpExists(sModPLC))
		{	
		iRes = dpGet( sModPLC + ".UnitAddress",  sUnitAddress);
		}
	else
		fwException_raise(ExceptionInfoTemp, "ERROR", "getCommonParameters(): " + getCatStr("unGeneration", "DPDOESNOTEXIST"), sModPLC);	
	
	if (dpExists(UN_APPLICATION_DPNAME))
		{	
            iRes = dpGet(	UN_APPLICATION_DPNAME + ".event16", bEvent);
		
            if (unGenericObject_shapeExists("TextFieldEvent")) {
        		if (bEvent)
        			TextFieldEvent.text = getCatStr("unApplication", "EVENT16");
        		else
        			TextFieldEvent.text = getCatStr("unApplication", "EVENT32");
            }
		}
	else
		{
    		fwException_raise(ExceptionInfoTemp, "ERROR", "getCommonParameters(): " + getCatStr("unGeneration", "DPDOESNOTEXIST"), UN_APPLICATION_DPNAME);	
            if (unGenericObject_shapeExists("TextFieldEvent")) {
	           TextFieldEvent.text = "";
            }
		}
	
    if (unGenericObject_shapeExists("TextFieldDriver")) {
        TextFieldDriver.text = sPlcDrv;
    }
	
	if (unGenericObject_shapeExists("TextFieldBoolArchive") && dynlen(dsBoolArchive)>0 && dynlen(dsAnalogArchive)>0 && dynlen(dsEventArchive)>0)
		{
		TextFieldBoolArchive.text = dsBoolArchive[1];
		TextFieldAnalogArchive.text = dsAnalogArchive[1];
		TextFieldEventArchive.text = dsEventArchive[1];
		}
  if(unGenericObject_shapeExists("frontEndVersion"))
		frontEndVersion.text = sVersion;

}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_writeDsStringToFile
/**
Purpose:  Wrtite a table of string to a selected file

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_writeDsStringToFile(dyn_string dsTextToWrite,  string delimiter)
{
  int iLoop, iLen;
  string sTextToWrite = "";

  iLen = dynlen(dsTextToWrite);
  for( iLoop = 1 ; iLoop <= iLen ; iLoop++ )
  {
    // All lines should be finished by ";"
    sTextToWrite = sTextToWrite + dsTextToWrite[iLoop] + delimiter;
    /*
    if( iLoop != iLen )
    {
      sTextToWrite = sTextToWrite + dsTextToWrite[iLoop] + delimiter;
    }
    else
    {
      sTextToWrite = sTextToWrite + dsTextToWrite[iLoop];
    }
    */
  }

  fputs(sTextToWrite , g_fileToExport);
  fputs("\n" , g_fileToExport);
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_convertFormat
/**
Purpose:  Convert sFormat like generator ####.#

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
string unExportDevice_convertFormat(string sFormatToConvert, string sChar)
{
	string subFormat , convertedFormat, convertedFormatTemp;
	dyn_string splitFormat;
	int i, j, totalChar , decimal, result=0;
	
	
	if (substr(sFormatToConvert , strlen(sFormatToConvert)-1) != "e")
		{
		//Get number form like "5.1" -> ####.#		
		subFormat=substr(sFormatToConvert , 1 , strlen(sFormatToConvert)-2);
		splitFormat=strsplit(subFormat , ".");
		
		if(dynlen(splitFormat) >= 2) {
			sscanf(splitFormat[1], "%d", totalChar);
			sscanf(splitFormat[2], "%d", decimal);
	 	}
	 	else if(dynlen(splitFormat) == 1) {
			sscanf(splitFormat[1], "%d", totalChar);
			decimal = 0;
	 	}
	 	else {
			totalChar = 0;
			decimal = 0;
	 	}
	 	if(decimal == 0)
	 		{
	 		for(i=0; i<totalChar ; i++)
	 			{
	 			convertedFormatTemp = convertedFormatTemp + sChar;
	 			}
	 		}
	 	
	 	if(decimal == 20)
	 		{
	 		totalChar = totalChar - 1;
	 		convertedFormatTemp = totalChar + "d";
	 		}
	 	if(decimal != 0 && decimal != 20)
	 		{
	 		for(i=0; i<totalChar-decimal-1 ; i++)
	 			{
	 			convertedFormatTemp = convertedFormatTemp + sChar;
	 			}
	 		convertedFormatTemp = convertedFormatTemp + ".";
	 		
	 		for(j=0; j< decimal ; j++)
	 			{
	 			convertedFormatTemp = convertedFormatTemp + sChar ;
	 			}
	 		}
	 	} 	
	 else if(sFormatToConvert == UN_FACEPLATE_DEFAULT_EXP_FORMAT)
	 	{
	 	convertedFormatTemp = "EXP";
	 	}
	 	else 
	 	{
	 		convertedFormatTemp = sFormatToConvert;
	 		strreplace(convertedFormatTemp, "e", "");
	 		result = strpos(convertedFormatTemp, ".");
	 		convertedFormatTemp = substr(convertedFormatTemp, result+1, strlen(convertedFormatTemp))+"EXP";
	 	}	
//DebugN(convertedFormatTemp, sFormatToConvert, result, strlen(convertedFormatTemp));
 	convertedFormat = convertedFormatTemp;
 
	return convertedFormat;
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getWidget
/**
Purpose:  Get the widget type of a dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getWidget(string sDp, string &sWidget)
{
int iLen;
string tempWidget;
dyn_string splitWidget;
	
	tempWidget = unGenericDpFunctions_getDescription( sDp + ".ProcessInput");
	splitWidget=strsplit(tempWidget , UN_PARAMETER_DELIMITER);
	
	iLen=dynlen(splitWidget);
	if (iLen>0)
		sWidget = splitWidget[iLen];

}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getPIDName
/**
Purpose:  Get the PID name

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getPIDName(string sDp, string &sPIDName)
{
int iLen;
string tempPIDName;
dyn_string splitPIDName;
	
	tempPIDName = unGenericDpFunctions_getDescription( sDp + ".ProcessInput");
	splitPIDName=strsplit(tempPIDName , UN_PARAMETER_DELIMITER);
	
	iLen=dynlen(splitPIDName);
	if (iLen>=UN_PARAMETER_NAME)
		sPIDName = splitPIDName[UN_PARAMETER_NAME];
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getPCOParameters
/**
Purpose:  Get PCO parameters

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getPCOParameters(string sDp, dyn_string &dsPCOParameters)
{
string tempPCOName;
dyn_string splitPCOName;
int i;
	
	tempPCOName = unGenericDpFunctions_getDescription( sDp + ".ProcessInput.OpMoSt");
	splitPCOName=strsplit(tempPCOName , UN_PARAMETER_DELIMITER);
	for(i=1;i<=dynlen(splitPCOName);i++)
		{
		dsPCOParameters[i] = splitPCOName[i];
		}
	while(dynlen(dsPCOParameters) < 16)
		dynAppend(dsPCOParameters, "");
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getArchiveParameters
/**
Purpose:  Get archive parameters of a dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getArchiveParameters(string sDp, string sObject, string &sArchiveActive,  float &fArchiveTimeFilter, string sVersion="")
{
	bool bArchiveActive;
	
	if(sObject == "Controller")
	{
		sDp=sDp+".ProcessInput.MV";
	}
	else
	{
		sDp=sDp+".ProcessInput.PosSt";
	}
	
	unExportDevice_getArchiveParametersDpe(sDp, sArchiveActive, fArchiveTimeFilter, sVersion);
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getArchiveParametersDpe
/**
Purpose:  Get archive parameters of a dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getArchiveParametersDpe(string sDp, string &sArchiveActive,  float &fArchiveTimeFilter, string sVersion="")
{
	bool bArchiveActive;
	
	dpGet(sDp + ":_archive.._archive" , bArchiveActive);
		
	if(sVersion != "") {
		unExportDevice_getArchiveTemp(sDp, bArchiveActive, sArchiveActive, fArchiveTimeFilter);
//DebugN(sDp, bArchiveActive, sArchiveActive, fArchiveTimeFilter);
		if(sArchiveActive == "")
		{
			sArchiveActive = "N";
			fArchiveTimeFilter = -1;
		}
	}
	else {
		if (bArchiveActive)
		{
			unExportDevice_getArchiveTemp(sDp, bArchiveActive, sArchiveActive, fArchiveTimeFilter);
		}
		else
		{	
			sArchiveActive = "N";
			fArchiveTimeFilter = -1;
		}		
	}
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getAllCommonParameters
/**
Purpose:  Get all common parameters of a dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getAllCommonParameters( string sDp, string sObject ,dyn_string &dsAllCommonParameters)
{
  int i, j, iDeviceNbr;
  bool bIsSmoothDefined;
  string sDpName, sAlias, sDescription, sDpType, sDrvIdent, sExceptionInfo, sDeviceNbr, sWidget, sLink;
  dyn_string dsParameters , dsDpTypeTemp , dsLimits , exceptionInfo;
  string sAccessControl, sOperator, sExpert, sAdmin;
  
  dynClear(dsDpTypeTemp);
  dynClear(dsLimits);
		
  dsParameters=strsplit(sDp, "-");
  if (dynlen(dsParameters)>4)
    iDeviceNbr=(int)dsParameters[5];
  else
    iDeviceNbr= 0; // PG, 20160721, to review: was: UN_CONFIG_EXPORT_UNDEF which is a string "#VALUE!" that may not be casted
  sDeviceNbr=iDeviceNbr;
  dynClear(dsParameters);

  sDpName = unGenericDpFunctions_getDpName(sDp);		
  dsDpTypeTemp = strsplit(sDpName, "-");
  sDpType = dsDpTypeTemp[dynlen(dsDpTypeTemp)-1];
				
  sAlias = unGenericDpFunctions_getAlias(sDp);
  unGenericDpFunctions_getDeviceLink(sDp, sLink);
  if(sLink != "")
    sAlias = sAlias+UN_CONFIG_GROUP_SEPARATOR+sLink;
  sDescription = unGenericDpFunctions_getDescription(sDp);
  unExportDevice_getWidget(sDp, sWidget);
		
  unGenericDpFunctions_getParameters(sDp, dsParameters);
	
  dynAppend(dsAllCommonParameters , sDpType); //Dp type
  dynAppend(dsAllCommonParameters , sDeviceNbr); //Dp Device Number
  dynAppend(dsAllCommonParameters , sAlias); //Dp Alias, device link (if any)
  dynAppend(dsAllCommonParameters , sDescription); //Dp Description
  unConfigGenericFunctions_getDeviceAccessControl(sDp, sAccessControl, sOperator, sExpert, sAdmin);
  if(sAccessControl != UNICOS_ACCESSCONTROL_DOMAINNAME)
    dsParameters[UN_PARAMETER_DOMAIN] = sAccessControl+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+dsParameters[UN_PARAMETER_DOMAIN];
  if((sOperator != UNICOS_ACCESSCONTROL_DOMAINNAME) || (sExpert != UNICOS_ACCESSCONTROL_DOMAINNAME) ||
     (sAdmin != UNICOS_ACCESSCONTROL_DOMAINNAME))
    dsParameters[UN_PARAMETER_NATURE] = sOperator+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+
                                        sExpert+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+
                                        sAdmin+UN_ACCESS_CONTROL_DOMAIN_SEPARATOR+
                                        dsParameters[UN_PARAMETER_NATURE];
		
  //Get diagnostic, html link, default panel, domain, nature			
  for(i=1;i<dynlen(dsParameters)-2;i++)
  {
    if (dsParameters[i]=="")
      dynAppend(dsAllCommonParameters , UN_CONFIG_EXPORT_NONE); 
    else	
      dynAppend(dsAllCommonParameters , dsParameters[i]);
  }
  dynAppend(dsAllCommonParameters, sWidget);		
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getArchiveTemp
/**
Purpose:  Method rewrite to work fine compared to package unicos installed

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getArchiveTemp(string dpe, bool &isArchived, string &sArchiveActive, float &fTimeInterval) 
{ 
	string archiveClass;
	int iSmoothProcedure;
  float fDeadband;
  bool bConfigExists, isActive;
  int iArchiveType;
  dyn_string exceptionInfo;
  
  fwArchive_get(dpe, bConfigExists, archiveClass, iArchiveType, iSmoothProcedure, fDeadband, fTimeInterval, isActive, exceptionInfo);
  if(!bConfigExists)
  {
    isArchived = false; 
    sArchiveActive = "N";
    fTimeInterval = -1;
    return;
  }
  if(iArchiveType != DPATTR_ARCH_PROC_SIMPLESM) { // no archive smoothing
    isArchived = false; 
    sArchiveActive = "Y";
    fTimeInterval = 0;
    return;
  }
		
  switch(iSmoothProcedure)
  {
		case DPATTR_TIME_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE;
			break;
		case DPATTR_COMPARE_OLD_NEW:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_AND_TIME;
			fTimeInterval = 0;
			break;
		case DPATTR_OLD_NEW_OR_TIME_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_OR_TIME;
			break;
		case DPATTR_OLD_NEW_AND_TIME_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_OLDNEW_AND_TIME;
			break;
		case DPATTR_VALUE_REL_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_VALUE_RELATIVE + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR +
													fDeadband + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_NO_TIME;
			fTimeInterval = 0;
			break;
		case DPATTR_TIME_OR_VALUE_REL_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_VALUE_RELATIVE + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR +
													fDeadband + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_OR_TIME;
			break;
		case DPATTR_TIME_AND_VALUE_REL_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_VALUE_RELATIVE + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR +
													fDeadband + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_AND_TIME;
			break;
		case DPATTR_VALUE_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_VALUE_ABSOLUTE + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR +
													fDeadband + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_NO_TIME;
			fTimeInterval = 0;
			break;
		case DPATTR_TIME_OR_VALUE_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_VALUE_ABSOLUTE + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR +
													fDeadband + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_OR_TIME;
			break;
		case DPATTR_TIME_AND_VALUE_SMOOTH:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE_VALUE_ABSOLUTE + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR +
													fDeadband + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_SEPARATOR + UN_CONFIG_ARCHIVE_ACTIVE_EXTENDED_AND_TIME;
			break;
		default:
			sArchiveActive = UN_CONFIG_ARCHIVE_ACTIVE;
			fTimeInterval = 0;
			break;
	}
//DebugN(dpe, iSmoothProcedure, sArchiveActive, fTimeInterval, fDeadband);    
} 

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_fwSmoothing_getTemp
/**
Purpose:  Method rewrite to work fine compared to package unicos installed

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_fwSmoothing_getTemp(string dpe, bool &isSmoothDefined, int &smoothProcedure, float &deadband, float &timeInterval) 
{ 
int configExists;

	dpGet(dpe+":_smooth.._type", configExists);
	dpGet(dpe+":_smooth.._std_type", smoothProcedure); 
	
	//DebugN("smoothProcedure : " + smoothProcedure);
	if(configExists == 48)
	{
	        switch(smoothProcedure)
		{
			case "0":
				dpGet(dpe+":_smooth.._std_tol", deadband);
				break;
			case "2":
				dpGet(dpe+":_smooth.._std_tol", deadband);
	 			dpGet(dpe+":_smooth.._std_time", timeInterval);
				break;
			case "4":
				break;
			case "5":
	 			dpGet(dpe+":_smooth.._std_time", timeInterval);
				break;
			case "7":
				dpGet(dpe+":_smooth.._std_tol", deadband);
				break;
			default:
				break;
		}
		isSmoothDefined=TRUE;
	}
	else
		isSmoothDefined=FALSE;
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getDeadBand
/**
Purpose:  Get Deadband of a dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getDeadBand(float fDeadbandTemp, bool bSmooth,  float fMaxValue, float fMinValue , int iSmoothProcedure, float &fDeadband, int &iDeadBandType)
{

	if(bSmooth)
	{
		if(iSmoothProcedure == DPATTR_VALUE_REL_SMOOTH )
		{ 
			fDeadband = fDeadbandTemp;
			iDeadBandType = UN_CONFIG_DEADBAND_RELATIF;
		}
		else if(iSmoothProcedure == DPATTR_VALUE_SMOOTH)
		{
			if(fMaxValue - fMinValue > 0)
			{
				if(fMaxValue - fMinValue > 0)
				{
					fDeadband = (fDeadbandTemp * 100) / (fMaxValue - fMinValue);
					iDeadBandType = UN_CONFIG_DEADBAND_VALUE;
				}
				else
				{
					fDeadband = fDeadbandTemp;
					iDeadBandType = UN_CONFIG_DEADBAND_VALUE;
				}
			}
		}
                else if(iSmoothProcedure == DPATTR_COMPARE_OLD_NEW) 
                {
			fDeadband = 0;
			iDeadBandType = UN_CONFIG_DEADBAND_OLD_NEW;
                }
	}
	else
	{
		fDeadband = 0;
		iDeadBandType = DPCONFIG_NONE;
	}
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getFormat
/**
Purpose: Get value format pattern
  
@par Constraints
	
@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param sDp device datapoint
@param sDpe	optional. Device datapoint element. Default: .ProcessInput.PosSt
@param exceptionInfo	Details of any exceptions are returned here   
@return the format as string (for example, #.##) Plus the Logarithmic Scale flag for the trending. example:
          #.##|LOG_SCALE if logarithmic,
          #.##| if not logarithmic.
*/

string unExportDevice_getFormat(string sDp, string sDpe = ".ProcessInput.PosSt", dyn_string &exceptionInfo) 
{
	string sFormat;
 string sDpName = sDp+sDpe;
 dyn_string dsValue;
 string sLogScaleConfig;
 if(!dpExists(sDpName))
 {
						fwException_raise(exceptionInfo,"ERROR","unExportDevice_getFormat: " + sDpName + " " + getCatStr("unGeneration","DPDOESNOTEXIST"),"");   
      return "";
 }
	sFormat = unExportDevice_convertFormat(dpGetFormat(sDpName), "#");
 unGenericDpFunctions_getKeyUnicosConfiguration(sDp, UN_DEVICE_LOG_SCALE_CONFIGURATION, dsValue, exceptionInfo);
 if(dynlen(dsValue)>0)
   sLogScaleConfig = dsValue[1];
 else 
   sLogScaleConfig = "-1";
 switch(sLogScaleConfig)
 {
   case "1": //logarithmic scale on
      sFormat = sFormat + UN_PARAMETER_SUBITEM_DELIMITER + UN_DEVICE_LOG_SCALE_CONFIGURATION; 
   break;
   case "0": //logarithmic scale off
      sFormat = sFormat + UN_PARAMETER_SUBITEM_DELIMITER ; 
   break;
   case "-1": //nothing was efined for the logarithmic scale
      sFormat = sFormat ; 
   break;
   default:
    
   break;   
 }
// DebugN("unExportDevice_getFormat - "+sDpName+ " sLogScaleConfig:",sLogScaleConfig,"dsValue:",dsValue, "sFormat",sFormat);
 return sFormat;
}


//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_writeTransitionToFile
/**
Purpose: Write a transition betwen Objects to export

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_writeTransitionToFile(string sObject, string sPlcName, string sApplication, file fFile)
{
	string sImportTime;
	dyn_string dsImportTime, dsApplication;
	int pos;
		
	dpGet(sPlcName + ".configuration.subApplications", dsApplication, 
				sPlcName + ".configuration.importTime", dsImportTime);
	pos = dynContains(dsApplication, sApplication);
	if(pos > 0)
		sImportTime = dsImportTime[pos];
	sImportTime = substr(sImportTime, 0, 19);

    fputs("\n", fFile);
	fputs("# \n" , fFile);
	fputs("#  Database PVSS generated at " + sImportTime + " \n" , fFile);
	fputs("# \n" , fFile);
	fputs("# Device type: " + sObject + " \n" , fFile);
	fputs("# \n" , fFile);
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_getAnalogNormalPosition
/**
Purpose: get Normal Position of an Analog dp

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
void unExportDevice_getAnalogNormalPosition(string sDp, const unsigned iCaseAlarms, int &iNormalPosition  )
{
bool bAlarmMask,bSMS,bOkRange;
string sAlertClass1,sAlertClass2;

	iNormalPosition = -1;
	
	dpGet(sDp + ".ProcessInput.PosSt:_alert_hdl.._active",  bAlarmMask);
	dpGet(sDp + ".ProcessInput.PosSt:_alert_hdl.1._class",  sAlertClass1);
	dpGet(sDp + ".ProcessInput.PosSt:_alert_hdl.2._class",  sAlertClass2);
	dpGet(sDp + ".ProcessInput.PosSt:_alert_hdl.._ok_range", bOkRange);
		
	if((patternMatch(UN_PROCESSALARM_PATTERN,sAlertClass1))||(patternMatch(UN_PROCESSALARM_PATTERN,sAlertClass2)))
		bSMS = true;
	else bSMS = false;		

	
	if(bAlarmMask){ //Alarm unmasked
		if(iCaseAlarms == UN_AIAO_ALARM_5_HH_H_L_LL ){
		if(bOkRange){
			if(bSMS) iNormalPosition = 101;
				else iNormalPosition = 1;
		}
		else{
			if(bSMS) iNormalPosition = 100;
				else iNormalPosition = 0;
				}
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_H_L_LL ){
			if(bSMS) iNormalPosition = 105;
				else iNormalPosition = 5;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_HH_H_L ){
			if(bSMS)iNormalPosition = 106;
				else iNormalPosition = 6;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_H_L ){
			if(bSMS) iNormalPosition = 107;
				else iNormalPosition = 7;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_HH_H ){
			if(bSMS) iNormalPosition = 108;
				else iNormalPosition = 8;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_LL_L ){
			if(bSMS) iNormalPosition = 109;
				else iNormalPosition = 9;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_H ){
			if(bSMS) iNormalPosition = 110;
				else iNormalPosition = 10;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_L ){
			if(bSMS) iNormalPosition = 111;
				else iNormalPosition = 11;		
		}
		if(iCaseAlarms == DPCONFIG_NONE)
		{
		if(bSMS)iNormalPosition = 102;
		else iNormalPosition = 2;
		}
	}
	else{//Alarm masked
		if(iCaseAlarms == UN_AIAO_ALARM_5_HH_H_L_LL ){
		if(bOkRange){
					if(bSMS) iNormalPosition = 104;
				else iNormalPosition = 4;
		}
		else{
			if(bSMS) iNormalPosition = 103;
				else iNormalPosition = 3;
				}
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_H_L_LL ){
			if(bSMS) iNormalPosition = 112;
				else iNormalPosition = 12;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_HH_H_L ){
			if(bSMS) iNormalPosition = 113;
				else iNormalPosition = 13;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_H_L ){
			if(bSMS) iNormalPosition = 114;
				else iNormalPosition = 14;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_HH_H ){
			if(bSMS) iNormalPosition = 115;
				else iNormalPosition = 15;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_LL_L ){
			if(bSMS) iNormalPosition = 116;
				else iNormalPosition = 16;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_H ){
			if(bSMS) iNormalPosition = 117;
				else iNormalPosition = 17;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_L ){
			if(bSMS) iNormalPosition = 118;
				else iNormalPosition = 18;		
		}
		if(iCaseAlarms == DPCONFIG_NONE)
		{
		if(bSMS)iNormalPosition = 102;
		else iNormalPosition = 2;
		}
	}	
//	DebugN("bAlarmMask" + bAlarmMask, "bSMS", bSMS, "bOkRange", bOkRange, "iNormalPosition", iNormalPosition, "iCaseAlarms",iCaseAlarms);
}

//------------------------------------------------------------------------------------------------------------------------
// unExportDevice_floatFormat
/**
Purpose: convert the float to string, add .0 if needed

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
string unExportDevice_floatFormat(string sFloat)
{
	if(strpos(sFloat, "e")<0) {
		if (strpos(sFloat,".")<0)
			return sFloat + ".0";
		else
			return sFloat;
	}
	return sFloat;
}

//------------------------------------------------------------------------------------------------------------------------

// unExportDevice_getAnalogNormalPosition1DPE
/**
Purpose:  Get the normal position of one non-boolean DPE

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
unExportDevice_getAnalogNormalPosition1DPE(string sDpe, const unsigned iCaseAlarms, int &iNormalPosition)
{
	bool bAlarmMask,bSMS,bOkRange;
	string sAlertClass1,sAlertClass2;

	iNormalPosition = -1;
	
	dpGet(sDpe+":_alert_hdl.._active",  bAlarmMask);
	dpGet(sDpe+":_alert_hdl.1._class",  sAlertClass1);
	dpGet(sDpe+":_alert_hdl.2._class",  sAlertClass2);
	dpGet(sDpe+":_alert_hdl.._ok_range", bOkRange);
		
	if((patternMatch(UN_PROCESSALARM_PATTERN,sAlertClass1))||(patternMatch(UN_PROCESSALARM_PATTERN,sAlertClass2)))
		bSMS = true;
	else bSMS = false;		

	
	if(bAlarmMask){ //Alarm unmasked
		if(iCaseAlarms == UN_AIAO_ALARM_5_HH_H_L_LL ){
		if(bOkRange){
			if(bSMS) iNormalPosition = 101;
				else iNormalPosition = 1;
		}
		else{
			if(bSMS) iNormalPosition = 100;
				else iNormalPosition = 0;
				}
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_H_L_LL ){
			if(bSMS) iNormalPosition = 105;
				else iNormalPosition = 5;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_HH_H_L ){
			if(bSMS)iNormalPosition = 106;
				else iNormalPosition = 6;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_H_L ){
			if(bSMS) iNormalPosition = 107;
				else iNormalPosition = 7;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_HH_H ){
			if(bSMS) iNormalPosition = 108;
				else iNormalPosition = 8;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_LL_L ){
			if(bSMS) iNormalPosition = 109;
				else iNormalPosition = 9;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_H ){
			if(bSMS) iNormalPosition = 110;
				else iNormalPosition = 10;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_L ){
			if(bSMS) iNormalPosition = 111;
				else iNormalPosition = 11;		
		}
		if(iCaseAlarms == DPCONFIG_NONE)
		{
			iNormalPosition = 2;
		}
	}
	else{//Alarm masked
		if(iCaseAlarms == UN_AIAO_ALARM_5_HH_H_L_LL ){
		if(bOkRange){
					if(bSMS) iNormalPosition = 104;
				else iNormalPosition = 4;
		}
		else{
			if(bSMS) iNormalPosition = 103;
				else iNormalPosition = 3;
				}
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_H_L_LL ){
			if(bSMS) iNormalPosition = 112;
				else iNormalPosition = 12;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_4_HH_H_L ){
			if(bSMS) iNormalPosition = 113;
				else iNormalPosition = 13;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_H_L ){
			if(bSMS) iNormalPosition = 114;
				else iNormalPosition = 14;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_HH_H ){
			if(bSMS) iNormalPosition = 115;
				else iNormalPosition = 15;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_3_LL_L ){
			if(bSMS) iNormalPosition = 116;
				else iNormalPosition = 16;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_H ){
			if(bSMS) iNormalPosition = 117;
				else iNormalPosition = 17;
		}
		if(iCaseAlarms == UN_AIAO_ALARM_2_L ){
			if(bSMS) iNormalPosition = 118;
				else iNormalPosition = 18;		
		}
		if(iCaseAlarms == DPCONFIG_NONE)
		{
			iNormalPosition = 2;
		}
	}	
//	DebugN("bAlarmMask" + bAlarmMask, "bSMS", bSMS, "bOkRange", bOkRange, "iNormalPosition", iNormalPosition, "iCaseAlarms",iCaseAlarms);
}

//------------------------------------------------------------------------------------------------------------------------
// DRV_MODBUS_convertToUnicosAddress
/**
Purpose: convert the MODBUS address to the UNICOS format

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
string DRV_MODBUS_convertToUnicosAddress(string sDeviceDpeName, string sDeviceType)
{
	string temp, sResult;
	dyn_string dsTemp;
	int iLen;
	dpGet(sDeviceDpeName + ":_address.._reference" , temp);
	dsTemp=strsplit(temp , ".");
	iLen=dynlen(dsTemp);
	if (iLen>0)
		sResult = dsTemp[iLen];
	return sResult;
}

//------------------------------------------------------------------------------------------------------------------------
// DRV_S7_convertToUnicosAddress
/**
Purpose: convert the S7 address to the UNICOS format

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
string DRV_S7_convertToUnicosAddress(string sDeviceDpeName, string sDeviceType)
{
	string temp, sResult;
	int pos;

	dpGet(sDeviceDpeName + ":_address.._reference" , temp);
	pos = strpos(temp, ".");
	sResult = substr(temp, pos+1, strlen(temp));
	return sResult;
}

//------------------------------------------------------------------------------------------------------------------------
// OPC_Com_ExportConfig
/**
Purpose: Export the OPC front-end config 

Parameters:

Usage: External function

PVSS manager usage: NG, NV
*/
OPC_Com_ExportConfig(dyn_string dsParam, dyn_string &exceptionInfo)
{
	dyn_string dsDpParameters;
	string sUnit, sMod, sFEType;
	dyn_string dsBoolArchive, dsAnalogArchive, dsEventArchive;
	int iRes;
	string sOPCData, sPlcName, sPlcDrv;
	dyn_string dsTemp;
	
	if (dynlen(dsParam)==UN_CONFIG_EXPORT_LENGTH)
	{
		sPlcName = dsParam[UN_CONFIG_EXPORT_PLCNAME];
		iRes = dpGet(c_unSystemIntegrity_UNOPC + sPlcName + ".configuration.archive_bool", dsBoolArchive,
									c_unSystemIntegrity_UNOPC + sPlcName + ".configuration.archive_analog", dsAnalogArchive,
									c_unSystemIntegrity_UNOPC + sPlcName + ".configuration.archive_event", dsEventArchive,
									c_unSystemIntegrity_UNOPC + sPlcName + ".communication.driver_num", sPlcDrv,
									c_unSystemIntegrity_UNOPC + sPlcName + ".configuration.OPCConfiguration", sOPCData);
		TextFieldDriver.text = sPlcDrv;
	
		if (dynlen(dsBoolArchive)>0 && dynlen(dsAnalogArchive)>0 && dynlen(dsEventArchive)>0)
		{
			TextFieldBoolArchive.text = dsBoolArchive[1];
			TextFieldAnalogArchive.text = dsAnalogArchive[1];
			TextFieldEventArchive.text = dsEventArchive[1];
		}

		//Delete
		dynClear(dsDpParameters);
		dynAppend(dsDpParameters, "#" + UN_DELETE_COMMAND);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
		unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
	
		//PLCCONFIG
		dynClear(dsDpParameters);
		dynAppend(dsDpParameters, UN_PLC_COMMAND);
		dynAppend(dsDpParameters, "OPC");
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_PLCNAME]);
		dynAppend(dsDpParameters, dsParam[UN_CONFIG_EXPORT_APPLICATION]);
		dsTemp = strsplit(sOPCData, "$");
		while(dynlen(dsTemp) < 3)
			dynAppend(dsTemp, "");
		dynAppend(dsDpParameters, dsTemp[1]); // OPC Server
		dynAppend(dsDpParameters, dsTemp[2]); // OPC group in
		dynAppend(dsDpParameters, dsTemp[3]); // OPC group out
		unExportDevice_writeDsStringToFile(dsDpParameters, UN_PARAMETER_DELIMITER);
		
	}
	else
		fwException_raise(exceptionInfo, "ERROR", "OPC_Com_ExportConfig(): Wrong Parameters", "");		
}

//------------------------------------------------------------------------------------------------------------------------
// DRV_OPCCLIENT_convertToUnicosAddress
/**
Purpose: convert the OPC address to the UNICOS format

Parameters:

Usage: Internal function

PVSS manager usage: NG, NV
*/
string DRV_OPCCLIENT_convertToUnicosAddress(string sDeviceDpeName, string sDeviceType)
{
	string temp, sResult;
	dyn_string dsTemp;
	int iLen;
	dpGet(sDeviceDpeName + ":_address.._reference" , temp);
	dsTemp=strsplit(temp , "$");
	iLen=dynlen(dsTemp);
	if (iLen>0)
		sResult = dsTemp[iLen];
	return sResult;
}


#uses "unTreeDeviceOverview.ctl"
/*
  function return array of exported db devices of given device type
 NOTE: export data using functions from scripts/libs/unExportDevice.ctl, which can only write output into file
 (g_fileToExport global var). so exportDP creates temporary file, exports data, reads it and then delete file.
*/
unExportDevice_exportDPs(string sFrontEndType, string sPlcName, string sSubApplication, string sDeviceType, string sFileName, dyn_string& exceptionInfo, bool bExportOnlineValuesAsDefault = false)
{
    addGlobal("g_fileToExport", FILE_VAR);
    addGlobal("g_iAlertRange", INT_VAR);

    dyn_string dsParamaters, dsApplication, dsDevices;
    int iFileExists;
    string sFunction, sTime;

    string sFeProtocol = unConfigGenericFunctions_convertPlcTypeToLegacyType(sFrontEndType);

    unExportDevice_report(access(sFileName, F_OK) == 0 ? "File already exists, its content is lost." : "New file created: " + sFileName);

    g_fileToExport = fopen(sFileName, "w+");
    if (sSubApplication == "*") {
        unTreeDeviceOverview_getApplication(getSystemName(), sPlcName, dsApplication);
    } else {
        dsApplication[1] = sSubApplication;
   }
    if (sDeviceType == "*") {
        unGenericDpFunctions_getUnicosObjects(dsDevices);
    } else if (sDeviceType != "") {
        dynAppend(dsDevices, sDeviceType);
    }
    dsParamaters[1] = sPlcName;
    dsParamaters[3] = sFeProtocol;
    string sPlcDp = sFeProtocol == "_UnPlc" ? "_unPlc_" : sFeProtocol+"_";

    if (dynlen(dsApplication) > 1) {
        fputs("# \n" , g_fileToExport);
        fputs("#  WARNING multi application file \n" , g_fileToExport);
        fputs("# \n" , g_fileToExport);
        unExportDevice_report("WARNING: Multi-application file.");
    }
    for (int i = 1; i <= dynlen(dsApplication); i++) {
        unExportDevice_report("  ");
        unExportDevice_report("Starting to export " + sPlcName + "-" + dsApplication[i] + "-" + sDeviceType + " objects to file:");   
        unExportDevice_report(sFileName);
        sTime = (string) getCurrentTime();
        unExportDevice_report("Start time: " + sTime);
	      dsParamaters[2] = dsApplication[i];

        unExportDevice_writeTransitionToFile(sFeProtocol, sPlcDp + sPlcName, dsApplication[i], g_fileToExport);

        // Export COMMUNICATION Configuration
        dyn_string checkFrontEndInfo;
        sFunction = sFeProtocol + "_Com" + UN_CONFIG_EXPORT_FUNCTIONSUFFIX;
        evalScript(checkFrontEndInfo,   "dyn_string main(dyn_string dsParam){" + 
                                    "dyn_string exceptionInfoTemp;" +
                                     "if (isFunctionDefined(\"" + sFunction + "\"))" +
                                     "    {" +
                                     "    " + sFunction + "(dsParam, exceptionInfoTemp);" +
                                     "    }" +
                                     "else " +
                                     "    {" +
                                     "    fwException_raise(exceptionInfoTemp,\"ERROR\",\"Function is not defined\",\"\");" +
                                     "    }" +
                                     "return exceptionInfoTemp; }", makeDynString(), dsParamaters);     
        dynAppend(exceptionInfo, checkFrontEndInfo);

        for (int j = 1; j <= dynlen(dsDevices); j++) {
            string currentDevice = dsDevices[j];
            // Export DEVICE Configurations
            sFunction = sFeProtocol + "_" + currentDevice + UN_CONFIG_EXPORT_FUNCTIONSUFFIX;

            dyn_string dsDps = dpNames(getSystemName() + "*-" + sPlcName + "-" + dsApplication[i] + "-*", currentDevice);
            // dsDps -> founded DP names
            if (dynlen(dsDps) == 0) {
                unExportDevice_report("No " + currentDevice + " found.");
            } else {
                DebugFN(2,sFunction + " -- online as default is " + bExportOnlineValuesAsDefault);              
           
                unExportDevice_report(dynlen(dsDps) + " " + currentDevice + " found.");
                dyn_string checkDeviceInfo;
                unExportDevice_writeTransitionToFile(currentDevice, sPlcDp + sPlcName, dsApplication[i], g_fileToExport);
                
                DebugFN(2,sFunction + "OnlineValues is defined: " + isFunctionDefined(sFunction+"OnlineValues"));              

                // if default parameters are to be overriden by online values for the export
                // and if the device supports the feature
                if ( bExportOnlineValuesAsDefault && isFunctionDefined(sFunction+"OnlineValues") ) 
                {
                  DebugFN(2,"Calling " + sFunction + "OnlineValues");              
                  callFunction(sFunction+"OnlineValues", dsDps, checkDeviceInfo);
                }
                else // otherwise, use the regular default parameters for the export
                {
                  if ( isFunctionDefined(sFunction) )
                  {
                  		callFunction(sFunction, dsDps, checkDeviceInfo);
                  }
                  else
                  {
                		fwException_raise(checkDeviceInfo,"ERROR","Function is not defined","");
                 	}
                }
                dynAppend(exceptionInfo, checkDeviceInfo);
            }
        }
    }

    if (g_fileToExport != 0) {
        fclose(g_fileToExport); // close file
        g_fileToExport = 0;
    }

    if (dynlen(exceptionInfo) > 0) {
        unExportDevice_report("Error while exporting data.");
    } else { 
        unExportDevice_report("Data successfully exported.");
    }
    sTime = (string)getCurrentTime();
    unExportDevice_report("End time: " + sTime);
    unExportDevice_report("-------------------------------------------------------------------------------------------------------");
}

unExportDevice_report(string message) {
    if (unGenericObject_shapeExists("messages")) {
        messages.appendItem(message);
        //messages.bottomPos(0);
        messages.bottomPos(messages.itemCount);
    }
}

//@}

