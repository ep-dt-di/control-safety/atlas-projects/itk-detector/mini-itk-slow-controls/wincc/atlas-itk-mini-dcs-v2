/**@file

@brief Clone of unFaceplate.ctl

@brief This library contains generic functions for faceplate animation.

First two parameters are dpes and values, which should be transfered straight from callback.
First two places in dpes/values are reserved for frontend statuses.

All animation functions check system validness before animation which reflects on color.
Also they are capable to animate disconnection.

Functions are supposed to have corresponding field(s) in the faceplate named in a special way. Function description contains an information about required fields.
Naming convention for graphical objects:
- for analog values object should have the same name as dpe ("PosSt" for ".ProcessInput.PosSt" dpe);
- for bit values object should have a bit's name in capital that responds to bit's position constant (e.g. "IOERRORW").

For bit animation bit position constant should be used (e.g. "UN_STSREG01_IOERRORW"). 
This constant contains 3 parts joined with underscore:
- first part is reflect package prefix and does not participate in animation;
- second part contains an information about StsReg where a bit is located;
- third part is bit name ($BIT_NAME$ in other examples).

@author Alexey Merezhin (EN-ICE-SIC)

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@par Creation Date
  03.10.2011

*/

/**Animate text value.

Faceplate should contain unFaceplate_DisplayColor.pnl or unFaceplate_DisplayNormal.pnl graphical object with the '$paramName$' name.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param paramName name of parameter to animate
@param value value of parameter to animate
*/
void unFaceplate_animateText(string paramName, string value, string color) {
    if(g_bSystemConnected) {
        setMultiValue(paramName + ".display", "foreCol", color, paramName + ".display", "text", value);
    }
}


/**Animate text parameter.

Faceplate should contain unFaceplate_DisplayColor.pnl or unFaceplate_DisplayNormal.pnl graphical object with the '$paramName$' name.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param paramName name of parameter to animate
@param value value of parameter to animate
*/
void unFaceplate_animateTextParameter(dyn_string dpes, dyn_string values, string paramName, string value) {
    string color = (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value) ? "unDataNotValid" : "unDisplayValue_Parameter";

    if(g_bSystemConnected) {
        setMultiValue(paramName + ".display", "foreCol", color, paramName + ".display", "text", value);
    }
}

/**Fetch value of dpe and animate a translation from dict.

Faceplate should contain unFaceplate_DisplayColor.pnl or unFaceplate_DisplayNormal.pnl graphical object with the '$dpeName$' name.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of dpe to animate
@param dpeUnit dpe unit
@param dpeFormat dpe format
@param dict mapping with translated values
@param color field color on valid state
*/
void unFaceplate_animateTranslatedValue(dyn_string dpes, dyn_string values, string dpeName, string dpeUnit, string dpeFormat, mapping dict, string color) {
    int value;
    bool invalid;

    unFaceplate_fetchAnimationCBOnlineValue(dpes, values, dpeName, value, invalid);
    invalid = invalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value);
    
    if (mappingHasKey(dict, (string)value)) { // display label
        if (g_bSystemConnected) {
            setMultiValue(dpeName + ".display", "foreCol", invalid ? "unDataNotValid" : color, dpeName + ".display", "text", dict[(string)value]);
        }
    } else { // display original value
        if (g_bSystemConnected) {
            unGenericObject_DisplayValue(dpeFormat, dpeUnit, (float) value, dpeName, color, invalid);
        }
    }

}

/**Animate interlock.

Faceplate should contain unFaceplate_ColoredSquare.pnl graphical object with the '$interlockName$' name ('FuStopISt' for example).

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param bitName interlock bit constant in StsReg (e.g. UN_STSREG01_STARTAL); see detailed description for more info
@param interlockName interlock DPE name
@param unAckBitName AlUnAck (alarm un-acknowledge) bit constant as string
*/
void unFaceplate_animateInterlock(dyn_string dpes, dyn_string values, string bitName, string interlockName, string unAckBitName = "UN_STSREG01_ALUNACK") {
  if (dynlen(dpes) > 0) { // online animation
    bool value, invalid, unAck, unAckInvalid;

    int interlockState = unFaceplate_fetchAnimationCBValue(dpes, values, interlockName);
    unFaceplate_fetchStatusBitAndValidness(dpes, values, bitName, value, invalid);
    unFaceplate_fetchStatusBitAndValidness(dpes, values, unAckBitName, unAck, unAckInvalid);

    if (g_bSystemConnected)
        unFaceplate_ColorBoxAnimateInterlock(interlockName, interlockState, value, unAck,
                    invalid || unAckInvalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
  } else {
    unGenericObject_ColoredSquareDisconnect(interlockName);
  }
}

/**Animate analog value.

Faceplate should contain unFaceplate_DisplayColor.pnl or unFaceplate_DisplayNormal.pnl graphical object with the '$dpeName$' name ("PosSt" for example).

@todo: should be replaced with unFaceplate_animateAnalogValue

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of dpe to animate
@param dpeUnit dpe unit
@param dpeFormat dpe format
@param color field color on valid state
*/
void unFaceplate_animateOnlineValue(dyn_string dpes, dyn_string values, string dpeName, string dpeUnit, string dpeFormat, string color) {
  anytype value;
  bool invalid;
  if (dynlen(dpes) > 0) { // online animation
    unFaceplate_fetchAnimationCBOnlineValue(dpes, values, dpeName, value, invalid);
    if (g_bSystemConnected) {
        unGenericObject_DisplayValue(dpeFormat, dpeUnit, value, dpeName, color, 
            invalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
    }
  } else {
    unGenericObject_DisplayValueDisconnect(dpeName);
  }
}
/**Animate analog value.

Faceplate should contain unFaceplate_DisplayColor.pnl or unFaceplate_DisplayNormal.pnl graphical object with the '$dpeName$' name ("PosSt" for example).

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of dpe to animate
@param refPrefix prefix for unit and format in g_params
@param color field color on valid state
*/
void unFaceplate_animateAnalogValue(dyn_string dpes, dyn_string values, string dpeName, string refPrefix, string color) {
  anytype value;
  bool invalid;
  if (dynlen(dpes) > 0) { // online animation
    string dpeUnit = g_params[refPrefix + "Unit"];
    string dpeFormat = g_params[refPrefix + "Format"];
    unFaceplate_fetchAnimationCBOnlineValue(dpes, values, dpeName, value, invalid);
    if (g_bSystemConnected) {
        unGenericObject_DisplayValue(dpeFormat, dpeUnit, value, dpeName, color, 
            invalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
    }
  } else {
    unGenericObject_DisplayValueDisconnect(dpeName);
  }
}


/**Animate interlock

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param sShape graphical object name
@param pvssAlarmState state of pvss alert (value of ":_alert_hdl.._act_state" property of dpe)
@param alarmState state in StsReg
@param alarmAck is alarm acknowledged (normally it's a UN_STSREG01_ALUNACK value)
@param bInvalid is dpe invalid
*/
void unFaceplate_ColorBoxAnimateInterlock(string sShape, int pvssAlarmState, bool alarmState, bool alarmAck, bool bInvalid) {
    if (alarmState) {
        if(g_bSystemConnected) {
            unGenericObject_ColoredSquareAnimate(sShape, alarmState, alarmAck && pvssAlarmState == 1 || pvssAlarmState == 3, "unFaceplate_AlarmNotAck", "unFaceplate_AlarmActive", bInvalid);
        }
    } else {
        if(g_bSystemConnected) {
            unGenericObject_ColoredSquareAnimate(sShape, !(alarmAck && (pvssAlarmState == 1 || pvssAlarmState == 3)), true, "unFaceplate_Unactive", "unFaceplate_AlarmNotAck", bInvalid);
        }
    }
}

/**Animate status bit.

Function accept bit's constant name (e.g. "UN_StsReg01_IOERRORW") as bitName. Constant's value should correspond to bit position in status dpe (e.g. StsReg01).
Faceplate should contain unFaceplate_Colorbox.pnl graphical object named '$BIT_NAME$' ("IOERRORW" in given example).

Bit's order starts from 0 (i.e. first bit's index is 0).

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param bitName constant as a string; see detailed description for more info
@param enabled if it's enabled
@param color field color on valid state
*/
void unFaceplate_animateStatusBit(dyn_string dpes, dyn_string values, string bitName, bool enabled, string color) {
  dyn_string parts = strsplit(bitName, "_");
  if (dynlen(dpes) > 0) { // online animation
    bool invalid, value;
    unFaceplate_fetchStatusBitAndValidness(dpes, values, bitName, value, invalid);

    if(g_bSystemConnected)
        unGenericObject_ColorBoxAnimate(parts[3], value, enabled, color, 
                    invalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
  } else { // disconnection animation
    unGenericObject_ColorBoxDisconnect(makeDynString(parts[3]));
  }
}

/**Animate reverse StsReg bit.

Opposite animation (TRUE->FALSE) of unFaceplate_animateStsRegBit.
As bitName should be used normal bit constant.
Faceplate should contain unFaceplate_Colorbox.pnl graphical object named 'NOT_$BIT_NAME$' ("NOT_IOERRORW" in given example).

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@see unFaceplate_animateStsRegBit

@param dpes connected dpe names
@param values connected dpe values
@param bitName constant as a string
@param enabled if it's enabled
@param color field color on valid state
*/
void unFaceplate_animateStsRegNotBit(dyn_string dpes, dyn_string values, string bitName, bool enabled, string color) {
  dyn_string parts = strsplit(bitName, "_");
  if (dynlen(dpes) > 0) { // online animation
    bool invalid, value;
    unFaceplate_fetchStatusBitAndValidness(dpes, values, bitName, value, invalid);

    if(g_bSystemConnected)
        unGenericObject_ColorBoxAnimate("NOT_" + parts[3], !value, enabled, color, 
                    invalid || (!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
  } else { // disconnection animation
    unGenericObject_ColorBoxDisconnect(makeDynString("NOT_" + parts[3]));
  }
}

/**Animate dpe's range.

For given dpe function fetch range properties from dpes/values and animate them accordingly.
Faceplate should contain two unFaceplate_DisplayNormal.pnl graphical objects with the '$DPE_NAME$RangeMax' and '$DPE_NAME$RangeMin' names ("PosStRangeMax" and "PosStRangeMax" for "PosSt" dpe).

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@see unFaceplate_animateStsRegBit

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of dpe with range configured to animate
@param rangeType range type, should be statically initialized
@param dpeUnit dpe unit
@param dpeFormat dpe format
*/
void unFaceplate_animateRange(dyn_string dpes, dyn_string values, string dpeName, int rangeType, string dpeUnit, string dpeFormat) {
    float rangeMin, rangeMax;
    if (rangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK) {
        rangeMin = unFaceplate_fetchAnimationCBValue(dpes, values, dpeName + ":_pv_range.._min");
        rangeMax = unFaceplate_fetchAnimationCBValue(dpes, values, dpeName + ":_pv_range.._max");
    } else {
        rangeMin = 0;
        rangeMax = 0;
    }
    if(g_bSystemConnected)
        unGenericObject_DisplayRange(rangeType, dpeFormat, dpeUnit, rangeMin, rangeMax, dpeName + "RangeMin", dpeName + "RangeMax", "unDisplayValue_Parameter");
}

/**Fetch value and validness of given bit.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param bitName constant as a string
@param value output
@param invalid output
*/
void unFaceplate_fetchStatusBitAndValidness(dyn_string dpes, dyn_string values, string bitName, bool &value, bool &invalid) {
    int stsReg;
    dyn_string exceptionInfo;
    int bitPosition = unGenerateDatabase_getConstantByName(bitName);
    if (bitPosition < 0) {
        fwException_raise(exceptionInfo,"ERROR", "unFaceplate_fetchStatusBitAndValidness: no constant defined for " + bitName, "EMPTY");
        return;
    }
    dyn_string parts = strsplit(bitName, "_");
    if (dynlen(parts) != 3) {
        fwException_raise(exceptionInfo,"ERROR", "unFaceplate_fetchStatusBitAndValidness: wrong bit name " + bitName, "EMPTY");
        return;
    }
    unFaceplate_fetchBitAndValidness(dpes, values, parts[2], bitPosition, value, invalid);
}

/**Fetch value and validness of given bit of given dpe.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param bitName constant as a string
@param value output
@param invalid output
*/
void unFaceplate_fetchBitAndValidness(dyn_string dpes, dyn_string values, string dpeName, int bitPosition, bool &value, bool &invalid) {
    unsigned dpeValue;
    unFaceplate_fetchAnimationCBOnlineValue(dpes, values, dpeName, dpeValue, invalid);
    value = getBit(dpeValue, bitPosition);
}

/**Fetch value and validness of given dpe.
Returns as fast as both value and invalid are found.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of dpe
@param value output
@param invalid output
*/
bool unFaceplate_fetchAnimationCBOnlineValue(dyn_string dpes, dyn_string values, string dpeName, anytype &value, bool &invalid) {
    string dpeValue = "." + dpeName + ":_online.._value";
    string dpeValid = "." + dpeName + ":_online.._invalid";
    bool valueFound = false, invalidFound = false;
    for (int i = 1; i <= dynlen(dpes); i++) {
        if (strpos(dpes[i], dpeValue) >= 0) {
            value = values[i];
            valueFound = true;
        }
        if (strpos(dpes[i], dpeValid) >= 0) {
            invalid = values[i];
            invalidFound = true;
        }
        if (valueFound && invalidFound) return true;
    }
    return valueFound && invalidFound;
}

/**Fetch value of given dpe.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of dpe
@return dpe value or UN_FACEPLATE_MISSED_VALUE if dpe's not found in dpes/values
*/
anytype unFaceplate_fetchAnimationCBValue(dyn_string dpes, dyn_anytype values, string dpeName, anytype missed_value = UN_FACEPLATE_MISSED_VALUE) {
    int idx = unFaceplate_fetchAnimationCBIndex(dpes, values, dpeName);
    if (idx > 0) return values[idx];
    return missed_value;
}

/**Return the index of given dpe.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dpes connected dpe names
@param values connected dpe values
@param dpeName name of dpe
@return dpe index or -1 if dpe's not found in dpes/values
*/
int unFaceplate_fetchAnimationCBIndex(dyn_string dpes, dyn_anytype values, string dpeName) {
    for (int i = 1; i <= dynlen(dpes); i++) {
        if (strpos(dpes[i], dpeName) >= 0) {
            return i;
        }
    }
    return -1;
}

/**Return front-end validness.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@todo rename me to unFaceplate_frontEndValid
  
@return is front-end valid
*/
bool unFaceplate_connectionValid(dyn_string values) {
  return dynlen(values) > 0 && ((!values[2]) || (values[1] > c_unSystemIntegrity_no_alarm_value));
}

