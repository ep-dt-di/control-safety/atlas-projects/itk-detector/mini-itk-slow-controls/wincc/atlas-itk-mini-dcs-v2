//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_additionalDeviceInfo
/**
Purpose: check if additional device info must be in the import

Parameters:
		return: bool, true=device additional info must be in/false= no device additional info

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@deprecated 2018-09-17

*/

bool unConfigGenericFunctions_additionalDeviceInfo() {

  FWDEPRECATED();


    bool bReturn, bDollar = true;
    string sFEVersion;

    if (isFunctionDefined("shapeExists")) {
        if (shapeExists("frontEndVersion")) {
            sFEVersion = frontEndVersion.text;
            bDollar = false;
        }
    }
    if (bDollar) {
        sFEVersion = $sFrontEndVersion;
    }
    if (sFEVersion != "") {
        bReturn = true;
    }
//DebugN("unConfigGenericFunctions_additionalDeviceInfo", bReturn);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unImportDevice", "unConfigGenericFunctions_additionalDeviceInfo", sFEVersion, bReturn, "-");
    return bReturn;
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkAllRegisters
/**
Purpose : check stsreg, evstsreg, manreg, alarm bit config

Parameters:
		dsConfigs, dyn_string, input, full config line
		convertIndex, int, input, constant to add to index to obtain an absolute index
		diStsRegAddresses, dyn_int, input, index of addresses in dsConfigs
		diEvStsRegAddresses, dyn_int, input, index of addresses in dsConfigs
		diManRegAddresses, dyn_int, input, index of addresses in dsConfigs
		iNormalPosition, int, input, normal position of alarm bits (index in dsConfigs)

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
@deprecated 2018-09-17
*/

unConfigGenericFunctions_checkAllRegisters(dyn_string dsConfigs, int convertIndex, dyn_int diStsRegAddresses, dyn_int diEvStsRegAddresses,
        dyn_int diManRegAddresses, int iNormalPosition, dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    int i, length;
    dyn_string dsAddresses;

    // 1. Addresses
    length = dynlen(diStsRegAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diStsRegAddresses[i]]);
    }
    length = dynlen(diEvStsRegAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diEvStsRegAddresses[i]]);
    }
    length = dynlen(diManRegAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diManRegAddresses[i]]);
    }
    unConfigGenericFunctions_checkAddresses(dsAddresses, exceptionInfo);

    // 2. Alarm StsReg active
    if (iNormalPosition > 0) {
        unConfigGenericFunctions_checkNormalPosition(dsConfigs[convertIndex + iNormalPosition], exceptionInfo);
    }
}

// unConfigGenericFunctions_checkAllRegistersS7
/**
Purpose : check stsreg, evstsreg, manreg, alarm bit config

Parameters:
		dsConfigs, dyn_string, input, full config line
		convertIndex, int, input, constant to add to index to obtain an absolute index
		diStsRegAddresses, dyn_int, input, index of addresses in dsConfigs
		diEvStsRegAddresses, dyn_int, input, index of addresses in dsConfigs
		diManRegAddresses, dyn_int, input, index of addresses in dsConfigs
		iNormalPosition, int, input, normal position of alarm bits (index in dsConfigs)

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
  
@deprecated 2018-09-17
*/

unConfigGenericFunctions_checkAllRegistersS7(dyn_string dsConfigs, int convertIndex, dyn_int diStsRegAddresses, dyn_int diEvStsRegAddresses,
        dyn_int diManRegAddresses, int iNormalPosition, dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    int i, length;
    dyn_string dsAddresses;

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_checkAllRegisters(dsConfigs= "+dsConfigs+" convertIndex= "+convertIndex+
    				" diStsRegAddresses= "+diStsRegAddresses+" diEvStsRegAddresses= "+diEvStsRegAddresses+" diManRegAddresses= "+diManRegAddresses+
    				"  iNormalPosition= "+iNormalPosition+" )");
    */

    // 1. Addresses
    // 1.1 stsReg01 (word=16 bits)
    length = dynlen(diStsRegAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diStsRegAddresses[i]]);
    }
    unConfigGenericFunctions_checkAddressS7(dsAddresses, UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo);
    dynClear(dsAddresses);	// clear the contents

    // 1.2 EvstsReg01 (word=32 bits)
    length = dynlen(diEvStsRegAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diEvStsRegAddresses[i]]);
    }
    unConfigGenericFunctions_checkAddressS7(dsAddresses, UN_CONFIG_S7_PLC_DATATYPE_DWORD, exceptionInfo);
    dynClear(dsAddresses);	// clear the contents

    // 1.3 manReg (word=16 bits)
    length = dynlen(diManRegAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diManRegAddresses[i]]);
    }
    unConfigGenericFunctions_checkAddressS7(dsAddresses, UN_CONFIG_S7_PLC_DATATYPE_WORD, exceptionInfo);

    // 2. Alarm StsReg active
    if (iNormalPosition > 0) {
        unConfigGenericFunctions_checkNormalPosition(dsConfigs[convertIndex + iNormalPosition], exceptionInfo);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkFloatIntBlockS7
/**
Purpose : check a block of float or int dpe configs

Parameters:
		dsConfigs, dyn_string, input, full config line
		convertIndex, int, input, constant to add to index to obtain an absolute index
		diInputAddresses, dyn_int, input, index of addresses in dsConfigs
		diOutputAddresses, dyn_int, input, index of addresses in dsConfigs
		diCommonData, dyn_int, input, index of common data (unit format deadband ...)
			4 parameters : 1-unit, 2-format 3-deadband 4-daedband type
		diRange, dyn_int, string, index of range min max, empty dyn_int = no range (pv_range config is set on first dpname of dsInputDps)
			2 parameters : 1-range min 2-range max
		diArchiveConfig, dyn_int, input, index of archive config (archive active and time filter)
			2 parameters : 1-active 2-time filter

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. convertIndex must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
  
@deprecated 2018-09-17
*/

unConfigGenericFunctions_checkFloatIntBlockS7(dyn_string dsConfigs, int convertIndex, dyn_int diInputAddresses, dyn_int diOutputAddresses,
        dyn_int diCommonData, dyn_int diRange, dyn_int diArchiveConfig, dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    int i, length;
    dyn_string dsAddresses;

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_checkFloatIntBlockS7(dsConfigs= "+dsConfigs+" convertIndex= "+convertIndex+" diInputAddresses= "+diInputAddresses+
    				" diOutputAddresses= "+diOutputAddresses+" diCommonData= "+diCommonData+" diRange= "+diRange+" diArchiveConfig= "+diArchiveConfig+" )");
    */

    // 1. Addresses
    length = dynlen(diInputAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diInputAddresses[i]]);
    }
    unConfigGenericFunctions_checkAddressS7(dsAddresses, UN_CONFIG_S7_PLC_DATATYPE_FLOAT, exceptionInfo);
    dynClear(dsAddresses);

    length = dynlen(diOutputAddresses);
    for (i = 1; i <= length; i++) {
        dynAppend(dsAddresses, dsConfigs[convertIndex + diOutputAddresses[i]]);
    }
    unConfigGenericFunctions_checkAddressS7(dsAddresses, UN_CONFIG_S7_PLC_DATATYPE_FLOAT, exceptionInfo);

    // 2. Common data & Range & Archive
    unConfigGenericFunctions_checkCommonFloatIntBlock(dsConfigs, convertIndex,
            diCommonData, diRange, diArchiveConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_checkFloatIntBlock
/**
Purpose : check a block of float or int dpe configs

Parameters:
		dsConfigs, dyn_string, input, full config line
		convertIndex, int, input, constant to add to index to obtain an absolute index
		diInputAddresses, dyn_int, input, index of addresses in dsConfigs
		diOutputAddresses, dyn_int, input, index of addresses in dsConfigs
		diCommonData, dyn_int, input, index of common data (unit format deadband ...)
			4 parameters : 1-unit, 2-format 3-deadband 4-daedband type
		diRange, dyn_int, string, index of range min max, empty dyn_int = no range (pv_range config is set on first dpname of dsInputDps)
			2 parameters : 1-range min 2-range max
		diArchiveConfig, dyn_int, input, index of archive config (archive active and time filter)
			2 parameters : 1-active 2-time filter

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. convertIndex must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
@deprecated 2018-09-17
*/

unConfigGenericFunctions_checkFloatIntBlock(dyn_string dsConfigs, int convertIndex, dyn_int diInputAddresses, dyn_int diOutputAddresses,
        dyn_int diCommonData, dyn_int diRange, dyn_int diArchiveConfig, dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    int i, length;
    dyn_string dsAddresses;

    // 1. check address
    unConfigGenericFunctions_checkFloatIntBlockAddress(dsConfigs, convertIndex, diInputAddresses,	diOutputAddresses, exceptionInfo);

    // 2. Common data & Range & Archive
    unConfigGenericFunctions_checkCommonFloatIntBlock(dsConfigs, convertIndex,
            diCommonData, diRange, diArchiveConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

/** check if data are Ok to set a MODBUS address

Creation Date: 20/11/2002

Modification History: None

Usage: Public

PVSS manager usage: CTRL, NV, NG

Constraints:
	. In the parameters, the field addressReference could be obtain using fwPeriphAddress_getUnicosAddressReference
	. PVSS version: 2.12.1
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.

@param dsParameters: parameters used to set the _address config (see constants definition)
@param exceptionInfo: for any error. If a parameter is incorrect, exceptionInfo is not empty !

@author: Vincent Forest, Herve Milcent (LHC-IAS)

@deprecated 2018-09-17

*/
unConfigGenericFunctions_checkModbusParameters(dyn_string dsParameters, dyn_string& exceptionInfo) {

  FWDEPRECATED();

    int driverNum, addressSubindex, mode, intervalTime, dataType, iTemp;
    string active, lowLevel;
    string addressReference;
    time startingTime;
    dyn_string addressSplit;
    bool badAddress;

    // 1. Length & communication type
    if (dynlen(dsParameters) == FW_PARAMETER_FIELD_NUMBER) {
        if (dsParameters[FW_PARAMETER_FIELD_COMMUNICATION] != "MODBUS") {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADCOMM"), "");
        }
        // 2. Driver number
        if (sscanf(dsParameters[FW_PARAMETER_FIELD_DRIVER], "%d", driverNum) <= 0) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADDRIVERNUM"), "");
        }
        // 3. Address reference
        addressReference = dsParameters[FW_PARAMETER_FIELD_ADDRESS];
        addressSplit = strsplit(addressReference, ".");
        badAddress = false;
        if (dynlen(addressSplit) == 4) {
            if ((addressSplit[1] != UN_PREMIUM_INPUT_LETTER_EVENT) &&
                    (addressSplit[1] != UN_PREMIUM_INPUT_LETTER_MISC) &&
                    (addressSplit[1] != UN_QUANTUM_INPUT_LETTER_EVENT) &&
                    (addressSplit[1] != UN_QUANTUM_INPUT_LETTER_MISC) &&
                    (addressSplit[1] != UN_PREMIUM_QUANTUM_OUTPUT_LETTER_ALL)) {	// Letter
                badAddress = true;
            }

            if (sscanf(addressSplit[2], "%d", iTemp) <= 0) {	// PLC number
                badAddress = true;
            }

            iTemp = (int)addressSplit[3];
            if ((iTemp != UN_PREMIUM_INPUT_NB_EVENT) &&
                    (iTemp != UN_PREMIUM_INPUT_NB_EVENT32) &&
                    (iTemp != UN_PREMIUM_INPUT_NB_MISC) &&
                    (iTemp != UN_QUANTUM_INPUT_NB_EVENT) &&
                    (iTemp != UN_QUANTUM_INPUT_NB_EVENT32) &&
                    (iTemp != UN_QUANTUM_INPUT_NB_MISC) &&
                    (iTemp != UN_PREMIUM_QUANTUM_OUTPUT_NB_ALL)) {	// Number
                badAddress = true;
            }

            if (sscanf(addressSplit[4], "%d", iTemp) <= 0) {	// Address
                badAddress = true;
            }
        } else {
            badAddress = true;
        }

        if (badAddress) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADADDRESS"), "");
        }

        // 4. Address subindex
        if (sscanf(dsParameters[FW_PARAMETER_FIELD_SUBINDEX], "%d", addressSubindex) <= 0) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADSUBINDEX"), "");
        }
        // 5. Mode
        if (sscanf(dsParameters[FW_PARAMETER_FIELD_MODE], "%d", mode) <= 0) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADMODE"), "");
        }
        // 6. Starting time
        //startingTime = dsParameters[FW_PARAMETER_FIELD_START];
        // 7. Interval Time
        if (sscanf(dsParameters[FW_PARAMETER_FIELD_INTERVAL], "%d", intervalTime) <= 0) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADINTERVAL"), "");
        }
        // 8. Data type
        if (sscanf(dsParameters[FW_PARAMETER_FIELD_DATATYPE], "%d", dataType) <= 0) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADDATATYPE"), "");
        }
        // 9. Address active
        active = dsParameters[FW_PARAMETER_FIELD_ACTIVE];
        if ((active != "FALSE") && (active != "TRUE") && (active != "0") && (active != "1")) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADACTIVE"), "");
        }
        // 10. Lowlevel
        lowLevel = dsParameters[FW_PARAMETER_FIELD_LOWLEVEL];
        if ((lowLevel != "FALSE") && (lowLevel != "TRUE") && (lowLevel != "0") && (lowLevel != "1")) {
            fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADLOWLEVEL"), "");
        }
    } else {
        fwException_raise(exceptionInfo, "ERROR", "fwPeriphAddress_checkModbusParameters: " + getCatStr("fwPeriphAddress", "MODBUSBADPARAMNUMBER"), "");
    }
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigGenericFunctions_IPToName
/**

@deprecated 2018-09-17

*/

string unConfigGenericFunctions_IPToName(string sIP) {

  FWDEPRECATED();

    string line, filename, result, sTemp;
    dyn_string lineSplit, lineSplit2;
    bool bRes;
    int pingPos, position;

    result = "";
    filename = tmpnam();
    system("ping -a " + sIP + " > " + filename);
    bRes = fileToString(filename, line);
    system("del " + filename);
    pingPos = strpos(line, "Pinging");
    if (pingPos >= 0) {
        pingPos = pingPos + 8; // 8 = strlen("Pinging ")
        line = substr(line, pingPos);
        lineSplit = strsplit(line, " "); // now lineSplit[1] = <hostname>.<domain>.<suffix> ex : PCLHC600.cern.ch
        sTemp = strtolower(lineSplit[1]);
        position = strpos(sTemp, ".cern.ch");
        if (position >= 0) {
            strreplace(sTemp, ".cern.ch", ""); // delete ".cern.ch" part
            result = sTemp;
        }
    }
    return result;
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigGenericFunctions_NameToIP
/**

@deprecated 2018-09-17

*/

string unConfigGenericFunctions_NameToIP(string name) {

  FWDEPRECATED();

    string line, filename, result, sTemp;
    dyn_string lineSplit, lineSplit2;
    bool bRes;
    int namePos, position;

    result = "";
    filename = tmpnam();
    system("ping -a " + name + " > " + filename);
    bRes = fileToString(filename, line);
    system("del " + filename);
    namePos = strpos(line, name) + strlen(name);
    line = substr(line, namePos);
    lineSplit = strsplit(line, "[");
    if (dynlen(lineSplit) >= 2) {
        sTemp = lineSplit[2];
        lineSplit = strsplit(sTemp, "]");
        result = lineSplit[1];
    }
    return result;
}

//------------------------------------------------------------------------------------------------------------------------
/**  unConfigGenericFunctions_recodeFile
  
Gets an ISO-8859-1 encoded text file and returns the path of a new text file in UTF-8 encoding.

@par Constraints
	None

@par Usage
	DEPRECATED. No recoding should ever be needed (see IS-1933)

@par PVSS managers
	VISION, CTRL

@param fnameToRecode, string, filename+path which needs to be encoded
@return string path+filename of the encoded file.

@deprecated 2018-09-17
*/
string unConfigGenericFunctions_recodeFile(string fnameToRecode) {
  
  FWDEPRECATED();

    dyn_string exceptionInfo;

    // drop the "deprecated" warning into the log.
    dyn_string stackTrace=getStackTrace();
    if (dynlen(stackTrace)<2) stackTrace[2]=""; // make sure the printout itself will not fail
    dyn_errClass err=makeError("",PRIO_WARNING,ERR_IMPL,0,"Deprecated function "+__FUNCTION__+" called from ",stackTrace[2]);
    throwError(err);
    
    fwException_raise(exceptionInfo,"WARNING","Deprecated unConfigGenericFunctions_recodeFile() was called","");
    dynClear(exceptionInfo);

    return fwGeneral_recodeFile(fnameToRecode, exceptionInfo);

    // we assume that in unicos project exceptionInfo is printed to the log anyway;
    // this is to be improved.

}

//------------------------------------------------------------------------------------------------------------------------
// unConfigGenericFunctions_setAddressDefaultPIDParam_S7
/**
Purpose : set a block of 4 float addresses for the PID default parameters.

Parameters:
		dsConfigs, 				dyn_string, input, full config line
		dsOutputDps, 			dyn_string, input, names of available Dps (in ProcessOutput)
		diOutputAddresses, dyn_int, 	input, index of addresses in dsConfigs
		exceptionInfo, dyn_string, 		output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.1
	. operating system: Windows XP.
	. distributed system: yes.
  
@deprecated 2018-09-17
*/

unConfigGenericFunctions_setAddressDefaultPIDParam_S7(dyn_string dsConfigs, dyn_string dsOutputDps, dyn_int diOutputAddresses, dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    int i, length, iPLCNumber, driverNum, Type;
    string addressReference, sDpName, sPLCType;
    dyn_string exceptionInfoTemp;

    /* DebugN("FUNCTION: unConfigGenericFunctions_setAddressDefaultPIDParam_S7(dsConfigs= "+dsConfigs+
    																															" dsOutputDps= "+dsOutputDps+
    																															" diOutputAddresses= "+diOutputAddresses+")");
    */

    // Initialization
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    driverNum = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER];
    iPLCNumber = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER];
    sPLCType = dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE];

    // Output address config
    dynAppend(exceptionInfo, exceptionInfoTemp);
    dynClear(exceptionInfoTemp);

    length = dynlen(dsOutputDps);
    for (i = 1; i <= length; i++) {
        // Default values address
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(
                    DPATTR_ADDR_MODE_OUTPUT,
                    dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                    dsConfigs[diOutputAddresses[i]]),
                addressReference);

        if (addressReference != "") {
            fwPeriphAddress_set(sDpName + ".ProcessOutput." + dsOutputDps[i],
                                makeDynString(fwPeriphAddress_TYPE_S7,
                                              driverNum,
                                              addressReference,
                                              DPATTR_ADDR_MODE_OUTPUT,
                                              PVSS_S7_FLOAT,
                                              UN_CONFIG_DEFAULT_ADDRESS_ACTIVE,
                                              "",
                                              "",
                                              "",
                                              "",
                                              UN_CONFIG_LOWLEVEL_ANALOG_PROCESS_OUTPUT,
                                              0,
                                              UN_CONFIG_DEFAULT_ADDRESS_STARTING_TIME,
                                              UN_CONFIG_DEFAULT_ADDRESS_INTERVAL_TIME,
                                              ""),
                                exceptionInfoTemp);
        } else {
            fwException_raise(exceptionInfoTemp, "ERROR", "unConfigGenericFunctions_setFloatIntBlockS7: " + sDpName + ".ProcessInput." + dsOutputDps[i] + getCatStr("unGeneration", "ADDRESSFAILED"), "");
        }
    }

    dynClear(exceptionInfoTemp);

    dynAppend(exceptionInfo, exceptionInfoTemp);
}

//------------------------------------------------------------------------------------------------------------------------
/*
// unConfigGenericFunctions_setAliasDPE
/**
Purpose: Set the alias on all the DPE of the device DP = Alias/dsDpe[i]

Parameters:
		sDeviceDpName: string, input, the device DP name
		dsDpe: dyn_string, input, list of DPE
		Alias: string, input, the device type
		exceptionInfo: dyn_string, output, for errors

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
@deprecated 2018-09-17
@note: The following pattern of dpe alias(dpAlias + "/" + dpeName) is obsolete.
*/

unConfigGenericFunctions_setAliasDPE(string sDeviceDpName, dyn_string dsDpe, string Alias, dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    int i, len;

    len = dynlen(dsDpe);

    for (i = 1; i <= len; i++) {
        unConfigGenericFunctions_setAlias(sDeviceDpName + dsDpe[i], Alias + "/" + dsDpe[i], exceptionInfo);
        ////DebugN(sDeviceDpName+dsDpe[i], Alias+":"+dsDpes[i], exceptionInfo);
    }
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setAllRegisters
/**
Purpose : set stsreg, evstsreg, manreg, alarm bit config

Parameters:
		dsConfigs, dyn_string, input, full config line
		dsStsRegNames, dyn_string, input, names of available StsReg (1 StsReg => 1 evStsReg)
		diStsRegAddresses, dyn_int, input, index of addresses in dsConfigs
		diEvStsRegAddresses, dyn_int, input, index of addresses in dsConfigs
		dsManRegNames, dyn_string, input, names of available ManReg
		diManRegAddresses, dyn_int, input, index of addresses in dsConfigs
		dsAlarmBitNames, dyn_string, input, names of available alarm bit
											like this : makeDynString("bit1StsReg1;bit2StsReg1", "bit1StsReg2;bit2StsReg2;bit3StsReg2");
		dsAlarmBitPositions, dyn_string, input, bit positions in StsReg (same format than dsAlarmBitNames)
		iNormalPosition, int, input, normal position of alarm bits (index in dsConfigs)

		exceptionInfo, dyn_string, output, for errors

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
@deprecated 2018-09-17
*/

unConfigGenericFunctions_setAllRegisters(dyn_string dsConfigs,
        dyn_string dsStsRegNames, dyn_int diStsRegAddresses, dyn_int diEvStsRegAddresses,
        dyn_string dsManRegNames, dyn_int diManRegAddresses,
        dyn_string dsAlarmBitNames, dyn_string dsAlarmBitPositions, int iNormalPosition,
        dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    string sDpName, sPLCType, addressReference;
    int i, j, length, length2, iPLCNumber, driverNum;
    dyn_string currentAlarmBitNames, exceptionInfoTemp;
    dyn_int currentAlarmBitPositions;

    // 1. General parameters
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    driverNum = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER];
    iPLCNumber = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER];
    sPLCType = dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE];

    // 2. Input
    length = dynlen(dsStsRegNames);
    for (i = 1; i <= length; i++) {
        // 2.1. evStsReg address
        unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT, sPLCType, iPLCNumber, true, dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diEvStsRegAddresses[i]], dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]), addressReference);
        unConfigGenericFunctions_setevStsRegAddress(sDpName + ".ProcessInput.ev" + dsStsRegNames[i], driverNum, addressReference,
                dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16], exceptionInfo);

        // 2.2. StsReg address
        unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_INPUT_SPONT, sPLCType, iPLCNumber, false, dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diStsRegAddresses[i]], dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]), addressReference);
        unConfigGenericFunctions_setStsRegAddress(sDpName + ".ProcessInput." + dsStsRegNames[i], driverNum, addressReference, exceptionInfo);

        // 2.3. Alarm bit on StsReg
        if ((i <= dynlen(dsAlarmBitNames)) && (i <= dynlen(dsAlarmBitPositions))) {
            currentAlarmBitNames = strsplit(dsAlarmBitNames[i], ";");
            currentAlarmBitPositions = strsplit(dsAlarmBitPositions[i], ";");
            length2 = dynlen(currentAlarmBitNames);
            if (dynlen(currentAlarmBitPositions) < length2) {
                length2 = dynlen(currentAlarmBitPositions);
            }
            for (j = 1; j <= length2; j++) {
                // 2.2. StsReg bit alarm address
                unConfigGenericFunctions_setStsRegAlarmBitAddress(sDpName + ".ProcessInput." + currentAlarmBitNames[j], driverNum,
                        addressReference, currentAlarmBitPositions[j], exceptionInfo);
            }
        }
    }

    // 3. Output (ManReg)
    length = dynlen(dsManRegNames);
    for (i = 1; i <= length; i++) {
        unConfigGenericFunctions_getUnicosAddressReference(makeDynString(DPATTR_ADDR_MODE_OUTPUT, sPLCType, iPLCNumber, false, dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diManRegAddresses[i]], dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16]), addressReference);
        unConfigGenericFunctions_setManReg(sDpName + ".ProcessOutput." + dsManRegNames[i], driverNum, addressReference, exceptionInfo);
    }

    // 4. Set common data: archive, alarms, etc.
    unConfigGenericFunctions_setCommonAllRegisters(dsConfigs, dsStsRegNames,
            dsAlarmBitNames, dsAlarmBitPositions, iNormalPosition,
            exceptionInfoTemp);

    dynAppend(exceptionInfo, exceptionInfoTemp);
}

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_setAllRegistersS7
/**
Purpose : set stsreg, evstsreg, manreg, alarm bit config

Parameters:
		dsConfigs, 					dyn_string, input, full config line
		dsStsRegNames, 			dyn_string, input, names of available StsReg (1 StsReg => 1 evStsReg)
		diStsRegAddresses, 	dyn_int, 		input, index of addresses in dsConfigs
		diEvStsRegAddresses,dyn_int, 		input, index of addresses in dsConfigs
		dsManRegNames, 			dyn_string, input, names of available ManReg
		diManRegAddresses, 	dyn_int, 		input, index of addresses in dsConfigs
		dsAlarmBitNames, 		dyn_string, input, names of available alarm bit
											!!!!! like this : makeDynString("bit1StsReg1;bit2StsReg1", "bit1StsReg2;bit2StsReg2;bit3StsReg2");
		dsAlarmBitPositions,dyn_string, input, bit positions in StsReg (same format than dsAlarmBitNames)
		iNormalPosition, 		int, 				input, normal position of alarm bits (index in dsConfigs)

WARNING : each given index is "relative", i.e. UN_CONFIG_ALLCOMMON_LENGTH must be added to have absolute index

Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: Win XP.
	. distributed system: yes.
  
@deprecated 2018-09-17
*/
unConfigGenericFunctions_setAllRegistersS7(dyn_string dsConfigs,
        dyn_string dsStsRegNames, dyn_int diStsRegAddresses, dyn_int diEvStsRegAddresses,
        dyn_string dsManRegNames, dyn_int diManRegAddresses,
        dyn_string dsAlarmBitNames, dyn_string dsAlarmBitPositions, int iNormalPosition,
        dyn_string &exceptionInfo) {
  
  FWDEPRECATED();
  
    string sDpName, sPLCType, addressReference;
    int i, j, length, length2, iPLCNumber, driverNum;
    dyn_string currentAlarmBitNames, exceptionInfoTemp;
    dyn_int currentAlarmBitPositions;

    /*
    DebugN("FUNCTION: unConfigGenericFunctions_setAllRegistersS7(dsConfigs= "+dsConfigs+" dsStsRegNames="+dsStsRegNames+
    																			" diStsRegAddresses="+diStsRegAddresses+" diEvStsRegAddresses="+diEvStsRegAddresses+
    																			" dsManRegNames="+dsManRegNames+" diManRegAddresses="+diManRegAddresses+" dsAlarmBitNames="+
    																			dsAlarmBitNames+ " dsAlarmBitPositions="+dsAlarmBitPositions+" iNormalPosition="+iNormalPosition);

    */

    // 1. General parameters
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
    driverNum = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_DRIVER];
    iPLCNumber = (int)dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_NUMBER];
    sPLCType = dsConfigs[UN_CONFIG_OBJECTGENERAL_PLC_TYPE];

    // 2. Input
    length = dynlen(dsStsRegNames);
    for (i = 1; i <= length; i++) {
        // 2.1. evStsReg address
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(
                    DPATTR_ADDR_MODE_INPUT_SPONT,
                    dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diEvStsRegAddresses[i]]),
                addressReference);
        unConfigGenericFunctions_setevStsRegAddressS7(sDpName + ".ProcessInput.ev" + dsStsRegNames[i], driverNum, addressReference,
                dsConfigs[UN_CONFIG_OBJECTGENERAL_EVENT16], exceptionInfo);



        // 2.2. StsReg address
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(
                    DPATTR_ADDR_MODE_INPUT_SPONT,
                    dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diStsRegAddresses[i]]),
                addressReference);
        unConfigGenericFunctions_setStsRegAddressS7(sDpName + ".ProcessInput." + dsStsRegNames[i], driverNum, addressReference, exceptionInfo);



        // 2.3. Alarm bit on StsReg
        if ((i <= dynlen(dsAlarmBitNames)) && (i <= dynlen(dsAlarmBitPositions))) {
            currentAlarmBitNames = strsplit(dsAlarmBitNames[i], ";");
            currentAlarmBitPositions = strsplit(dsAlarmBitPositions[i], ";");
            length2 = dynlen(currentAlarmBitNames);
            if (dynlen(currentAlarmBitPositions) < length2) {
                length2 = dynlen(currentAlarmBitPositions);
            }
            for (j = 1; j <= length2; j++) {
                // 2.2. StsReg bit alarm address
                unConfigGenericFunctions_setStsRegAlarmBitAddressS7(sDpName + ".ProcessInput." + currentAlarmBitNames[j], driverNum,
                        addressReference, currentAlarmBitPositions[j], exceptionInfo);
            }
        }
    }

    // 3. Output (ManReg)
    length = dynlen(dsManRegNames);
    for (i = 1; i <= length; i++) {
        unConfigGenericFunctions_getUnicosAddressReferenceS7(makeDynString(
                    DPATTR_ADDR_MODE_OUTPUT,
                    dsConfigs[UN_CONFIG_OBJECTGENERAL_PLCNAME],
                    dsConfigs[UN_CONFIG_ALLCOMMON_LENGTH + diManRegAddresses[i]]),
                addressReference);
        unConfigGenericFunctions_setManRegS7(sDpName + ".ProcessOutput." + dsManRegNames[i], driverNum, addressReference, exceptionInfo);
    }

    // 4. Set common data: archive, alarms, etc.
    unConfigGenericFunctions_setCommonAllRegisters(	dsConfigs, dsStsRegNames,
            dsAlarmBitNames, dsAlarmBitPositions, iNormalPosition,
            exceptionInfoTemp);

    dynAppend(exceptionInfo, exceptionInfoTemp);

}
