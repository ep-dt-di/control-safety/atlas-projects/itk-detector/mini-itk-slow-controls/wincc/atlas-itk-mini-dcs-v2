/*
@author: Alexey Merezhin

Creation Date: 24/09/2015

version 1.0

External Function :
    . unGCSUserlib_UnicosInfosGCS

Purpose:
This library contains the user functions to create Configuration file for the LHCLogging API GCS projects

Usage: Public

PVSS manager usage: NV

Constraints:
    . PVSS version: 3.1
    . operating system: wds XP
    . distributed system: No

@copyright
        &copy;Copyright CERN 2013 - all rights reserved

@reviewed 2018-07-24 @whitelisted{BackgroundProcessing}
*/

//-----------------------------------------------------------------------------------------------------------


unGCSUserlib_UnicosInfosGCS(string sDpe, string &sName,  string &sDescription, string &sHierarchy, string &sAlias, string &sE) {
    dyn_string dsSplit;
    string sLocation, sApplication;
    bool bState, bUseDpe, bOK;
    int iNum, iLen;

    string sSystemName = getSystemName();
    sHierarchy = "ERROR";
    sE = sDpe;

    //remove system name
    //unCoreLHCServices_set_SystemName(sDpe, bState, false);

    //Domain / nature
    string deviceName = unGenericDpFunctions_getDpName(sDpe);

    //Location
    dsSplit = strsplit(sSystemName, "_");
    if (dynlen(dsSplit) > 1) {
        sLocation = dsSplit[1];
    } else {
        sLocation = substr(sSystemName, 0, strlen(sSystemName) - 1);
    }

    dsSplit = strsplit(sDpe, ".");
    if (dynlen(dsSplit) == 3) { //UNICOS Device
        sAlias = dpGetAlias(dsSplit[1] + "."); // TODO: check me
        sName = sAlias + "." + dsSplit[3];
        sE = substr(sDpe, strpos(sDpe, ".") + 1, strlen(sDpe));
        sDescription = unGenericDpFunctions_getDescription(deviceName);
        strreplace(sDescription, "$", "_");
        strreplace(sDescription, "?", "_");

        dynClear(dsSplit);
        dsSplit = strsplit(sSystemName, "_");
        if (dynlen(dsSplit) > 1) {
            sLocation = dsSplit[1];
        } else {
            sLocation = substr(sSystemName, 0, strlen(sSystemName) - 1);
        }

        //Application
        dynClear(dsSplit);
        dsSplit = strsplit(sDpe, "-");
        if (dynlen(dsSplit) > 3) {
            sApplication = dsSplit[3];
        } else {
            sApplication = "UNKNOWN";
        }

        sHierarchy = unGCSUserlib_Hierarchy(sApplication, dpTypeName(sDpe), sDpe);
    } else { // process front-end
        int s7pos = strpos(dsSplit[1], S7_PLC_DPTYPE);
        int un_pos = strpos(dsSplit[1], c_unSystemIntegrity_UnPlc);
        if (s7pos >= 0) {
            unLHCLoggingDBUserLib_getSystemAlarmInfo(dsSplit, sLocation, sDpe, sDescription, sAlias, sName, sHierarchy);
            string plcName = substr(dsSplit[1], s7pos);
            dpGet(sSystemName + plcName + ".configuration.subApplications", sApplication);
            sHierarchy = unGCSUserlib_Hierarchy(sApplication, "WCCOA_INTEGRITY", sDpe);
        } else if (un_pos >= 0) {
            unLHCLoggingDBUserLib_getSystemAlarmInfo(dsSplit, sLocation, sDpe, sDescription, sAlias, sName, sHierarchy);
            string plcName = substr(dsSplit[1], un_pos);
            dpGet(sSystemName + plcName + ".configuration.subApplications", sApplication);
            sHierarchy = unGCSUserlib_Hierarchy(sApplication, "WCCOA_INTEGRITY", sDpe);
        } else {
            DebugN("Can't process dpe: " + sDpe);
        }
    }

    // ignore all evStsReg0*
    if (strpos(sDpe, "evStsReg0") >= 0) {
        sHierarchy = "IGNORE";
    }else if (strpos(strtoupper(sDescription), "SPARE") >= 0) {
            DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE SPARE: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
            sHierarchy = "IGNORE";
    }else if (strpos(sDpe, "AnalogAlarm") >= 0) {
		if (strpos(sDpe, "HHSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "HSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "LSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "LLSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "PosSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "HHAlSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "HWSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "LWSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "LLAlSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog Alarm dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}			
	}else if (strpos(sDpe, "Analog") >= 0) {
		if (strpos(sDpe, "PosRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE Analog dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}
	}else if (strpos(sDpe, "AnalogDigital") >= 0) {
		if (strpos(sDpe, "PosRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE AnalogDigital dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}
	}else if (strpos(sDpe, "MassFlowController") >= 0) {
		if (strpos(sDpe, "MVoTMRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "AuVoTMRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "AuCCRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "AuPosRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "VoTSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "ActDMo") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "ActVoTMo") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "PosRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "MDMoRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}else if (strpos(sDpe, "MCCRSt") >= 0){
			DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE MassFlowController dpe: " + sDpe +  " alias: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
			sHierarchy = "IGNORE";
		}
	}else if (strpos(sDpe, "_unSystemAlarm_DS_Comm_SOFT_FE_RCP_FE.alarm") >= 0) {
        DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE _unSystemAlarm_DS_Comm_SOFT_FE_RCP_FE.alarm");
        sHierarchy = "IGNORE";
    } else {
        // ignore all RESERVE, and print name to log, for info
        if (strpos(strtoupper(sDescription), "RESERVE") >= 0) {
            DebugN("unGCSUserlib_UnicosInfosGCS: IGNORE RESERVE: " + sAlias + "; description: " + sDescription + "; hierarchy: " + sHierarchy);
            sHierarchy = "IGNORE";
        }
    }
}

/** Function builds a hierarchy for GCS project.
 * The hierarchy format: Location/SubLocation/Application/DeviceType
*/
string unGCSUserlib_Hierarchy(string application, string deviceType, string sDpe) {
    string hierarchy = "GCS/";
	if (patternMatch("*Cloud*", application) ) {
        hierarchy += "MEYRIN/CLOUD/RUN11";
		return hierarchy + "/" + deviceType;
    }else if (patternMatch("*AppLinac4*", application) ) {
        hierarchy += "MEYRIN/LINAC4/152/AppLinac4"; 
		return hierarchy + "/" + deviceType;
	}else if (patternMatch("*Linac4*", application) ) {
        hierarchy += "MEYRIN/LINAC4/400/Linac4";
		return hierarchy + "/" + deviceType;
	} else if (patternMatch("*NA62*", application) ) {
        hierarchy += "PREVESSIN/";
	} else if (patternMatch("*Mixer904*", application) ) {
		hierarchy += "PREVESSIN/MIXERS/904/Mixer904";
		return hierarchy + "/" + deviceType;
	}else if (patternMatch("*Mixer3570*", application) ) {
        hierarchy += "PREVESSIN/MIXERS/3570/";		
	}else if (patternMatch("*ALI*", application) ) {
		hierarchy += "LHC/ALICE/";
	}else if (patternMatch("*ATL*", application) ) {
		hierarchy += "LHC/ATLAS/";
	}else if (patternMatch("*CMS*", application) ) {
		hierarchy += "LHC/CMS/";
	}else if (patternMatch("*LHB*", application) ) {
		hierarchy += "LHC/LHCb/";    
	} else {
        DebugN("ERROR: can't detect application for " + sDpe + " app " + application);
        return "ERROR";
    }
    hierarchy += application + "/" + deviceType;
    return hierarchy;
}
