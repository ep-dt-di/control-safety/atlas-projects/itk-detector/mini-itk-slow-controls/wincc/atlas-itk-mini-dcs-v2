/**@file
  
// unGenericFunctions_core.ctl
Generic functions used by the unCore and unSystemIntegrity.

@par Creation Date
  18/07/2011

@par Modification History

  09/05/2014: Marco Boccioli
  - IS-1447: unGenericDpFunctions_getHostName: sometimes, IPv6 is returned instead of IPv4

  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  - IS-597: no kill/restart of the valarch during a online backup  
  
  18/07/2011: Herve
  - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity
  
@par Constraints
  . operating system: WXP and Linux.
  . distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Herve Milcent (EN-ICE)
*/

//@{

//------------------------------------------------------------------------------------------------------------------------

// unConfigGenericFunctions_getMyIP - OBSOLETE !!!
/** Return the IP address   -  OBSOLETE !!!

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL
*/
string unConfigGenericFunctions_getMyIP()
{
  string hostName, result;
  dyn_string addr_list;
  
  hostName = getHostname();
  result = getHostByName(hostName, addr_list);

  return result;
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_getDescription
/** similar to dpGetDescription, the function returns the description or an empty string if there is no description.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDp, input, datapoint or list of datapoints
@return output, for description
*/
anytype unGenericDpFunctions_getDescription(anytype dsDp)
{
  dyn_string dsDescription, dsInput;
  string description, sDpFormatted;
  int i, length, inputType;

  // 1. Find input type and format it
  inputType = getType(dsDp);
  switch (inputType)
  {
    case STRING_VAR:
      dsInput = makeDynString(dsDp);
      break;
    
    case DYN_STRING_VAR:
      dsInput = dsDp;
      break;
    
    default:
      dsInput = makeDynString();
      break;
  }
  length = dynlen(dsInput);
  for(i=1;i<=length;i++) // For each element
  {
  // 2. Add a point at the end if no point is found
    sDpFormatted = dsInput[i];
    if (strpos(sDpFormatted,".") < 0)
    {
      sDpFormatted = sDpFormatted + ".";
    }
    
  // 3. Get descritpion
    description = dpGetDescription(sDpFormatted,0); // return description or sDp (without system name) if empty
    
  // 4. Description is empty ?
    if (sDpFormatted == description)
    {
      description = "";
    }
  // 5. Append result
    dynAppend(dsDescription,description);
  }
  return dsDescription;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_dpAliasToName
/** Convert the alias to dp name, "" is returned if the alias does not exists.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sAlias, input, <sys name>:Alias
@return output, dpName
*/

string unGenericDpFunctions_dpAliasToName(string sAlias)
{
  string sDp;
  
  if(sAlias == "")
    return sAlias;
  
  sDp = dpAliasToName(sAlias);
  if(sDp == "") { // either wrong alias or BUG PVSS 3.1
//DebugTN("********** empty dp for alias: "+sAlias);
    if(isFunctionDefined("_unGenericDpFunctions_dpAliasToName"))
      sDp = _unGenericDpFunctions_dpAliasToName(sAlias);
  }
  
  return sDp;
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_debugTN
/** equivalent to DebugTN but with debug level and filter

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param b32DebugLevel input, the debug level
@param sDebugFilter input, the debug filter, "*" for everything
@param ... input, list of parameter to print in the PVSS_II.log
*/
unGenericDpFunctions_debugTN(bit32 b32DebugLevel, string sDebugFilter, ...)
{
  int i, len;
  va_list parameters;
  time t;
  
//DebugTN(g_b32DebugLevel&b32DebugLevel, b32DebugLevel, sDebugFilter);
  
  if(g_b32DebugLevel&b32DebugLevel)
  {
    if(patternMatch(g_sDebugFilter, sDebugFilter)) {
      len = va_start(parameters); 
      t=getCurrentTime();
      Debug(t);
      for(i=1;i<=len;i++) {
        Debug(va_arg(parameters));
      }
      DebugN("");
      va_end(parameters); 
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_debugSetLevel
/** Set the debug level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param b32DebugLevel input, the debug level
*/
unGenericDpFunctions_debugSetLevel(bit32 b32DebugLevel)
{
  g_b32DebugLevel = b32DebugLevel;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_debugGetLevel
/** Get the debug level

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param return value output, the debug level
*/
bit32 unGenericDpFunctions_debugGetLevel()
{
  return g_b32DebugLevel;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_debugSetFilter
/** Set the debug filter

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDebugFilter input, the debug filter, "*" for everything
*/
unGenericDpFunctions_debugSetFilter(string sDebugFilter)
{
  g_sDebugFilter = sDebugFilter;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_debugGetFilter
/** Get the debug filter

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@return output, the debug filter
*/
string unGenericDpFunctions_debugGetFilter()
{
  return g_sDebugFilter;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_debugSetLevelAndFilter
/** Set the debug level and filter

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param b32DebugLevel input, the debug level
@param sDebugFilter input, the debug filter, "*" for everything
*/
unGenericDpFunctions_debugSetLevelAndFilter(bit32 b32DebugLevel, string sDebugFilter)
{
  unGenericDpFunctions_debugSetLevel(b32DebugLevel);
  unGenericDpFunctions_debugSetFilter(sDebugFilter);
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_debugGetLevelAndFilter
/** Get the debug level and filter

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param b32DebugLevel output, the debug level
@param sDebugFilter output, the debug filter
*/
unGenericDpFunctions_debugGetLevelAndFilter(bit32 &b32DebugLevel, string &sDebugFilter)
{
  b32DebugLevel = unGenericDpFunctions_debugGetLevel();
  sDebugFilter = unGenericDpFunctions_debugGetFilter();
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_convert_PVSSDPE_to_UNICOSDPE
/** Convert the DPE in PVSS format to UNICOS DPE format

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sPVSSDpe input, the PVSS DPE
@param sUnicosDPE output, the DPE in UNICOS format
*/
unGenericDpFunctions_convert_PVSSDPE_to_UNICOSDPE(string sDpe, string &sUnicosDPE)
{
  string sAlias, deviceName, sDpType, sUnDPE, sLeafDpe, sSystemName, tempDpe;
  dyn_string dsDpel, dsPattern;
  dyn_int diDpelType;
  int pos;
  
//remove the systemName
  tempDpe = sDpe;
  pos = strpos(tempDpe, ":");
  if(pos<0)
    tempDpe = getSystemName()+tempDpe;
// check if ProcessInput
  deviceName= unGenericDpFunctions_getDpName(tempDpe);
  sAlias = unGenericDpFunctions_getAlias(deviceName);
  pos = strpos(tempDpe, deviceName+".ProcessInput.");
//DebugN(sDpe, sAlias, deviceName, pos);
  if(pos >= 0)
    sLeafDpe = substr(tempDpe, pos+strlen(deviceName)+1, strlen(tempDpe));
  else {
// check ProcessOutput
    pos = strpos(tempDpe, deviceName+".ProcessOutput.");
    if(pos >= 0)
      sLeafDpe = substr(tempDpe, pos+strlen(deviceName)+1, strlen(tempDpe));
  }
  if(sLeafDpe!="") {
    sSystemName = unGenericDpFunctions_getSystemName(deviceName);
//DebugN(pos, sLeafDpe, deviceName, sAlias);
      
    if(dpExists(deviceName)) {
      sDpType = dpTypeName(deviceName);
  // get the list of trendable dpe
      unGenericDpFunctions_getDPE(sDpType, dsDpel, diDpelType);  
      unGenericDpFunctions_getTrendDPE(sDpType+".", dsDpel, diDpelType, dsPattern);
//DebugN(dsPattern);
      pos = dynContains(dsPattern, sLeafDpe);
      if(pos > 0) {
        sUnDPE = sSystemName+sAlias+substr(dsPattern[pos], strpos(dsPattern[pos], "."), strlen(dsPattern[pos]));
      }
    }
  }
  if(sUnDPE == "")
    sUnDPE = sDpe;
    
  sUnicosDPE = sUnDPE;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getTrendDPE
/** Get the list of trendable DPEL: int, bool, float, uint, time.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpeStructToRemove input, the pattern to remove from the DPE list
@param dsDpel input, the list of DPEL
@param diDpelType input, DPEL type
@param dsTrendDpe output, the trendeable DPE
*/
unGenericDpFunctions_getTrendDPE(string sDpeStructToRemove, dyn_string dsDpel, dyn_int diDpelType, dyn_string &dsTrendDpe)
{
  int i, len;
  dyn_string dsTemp;
  string sTemp;
  
  len = dynlen(dsDpel);
  for(i=1;i<=len; i++) {
    switch(diDpelType[i]) {
      case DPEL_BOOL:
      case DPEL_INT:
      case DPEL_FLOAT:
      case DPEL_UINT:
      case DPEL_TIME:
        if(strpos(dsDpel[i], sDpeStructToRemove) >=0) 
          sTemp = substr(dsDpel[i], strlen(sDpeStructToRemove), strlen(dsDpel[i]));
        else
          sTemp = "";
        if(sTemp != "")
          dynAppend(dsTemp, sTemp);
        break;
      default:
        break;
    }
  }
  dsTrendDpe = dsTemp;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getDPE
/** Get the list of DPEL.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpType input, the data point type
@param dsDpel output, the list of DPEL
@param diDpelType output, DPEL type
*/
unGenericDpFunctions_getDPE(string sDpType, dyn_string &dsDpel, dyn_int &diDpelType)
{
  dyn_dyn_string ddsElements, ddsElements1;
  dyn_dyn_int ddiTypes, ddiTypes1;
  int i, j, lenI, lenJ, iType, iBis, lenIbis;
  string sTemp, sTempBis, sTypeRef;
  dyn_string dsTemp;
  dyn_int diTemp;
  dyn_string ds;
  dyn_int di;
  
  dpTypeGet(sDpType, ddsElements1, ddiTypes1);
  lenI = dynlen(ddsElements1);
//DebugN("**** before", lenI, sDpType, ddsElements1, ddiTypes1);
// remove all the empty dyn_string, dyn_int: case of dpref.
  for(i=1;i<=lenI;i++) {
    if(dynlen(ddsElements1[i]) > 0) {
      ddsElements[dynlen(ddsElements)+1] = ddsElements1[i];
      ddiTypes[dynlen(ddiTypes)+1] = ddiTypes1[i];
    }
  }
  lenI = dynlen(ddsElements);
//DebugN("**** result", sDpType, lenI, ddsElements, ddiTypes);

  for(i=2;i<=lenI;i++) {
    lenJ = dynlen(ddsElements[i]);
    for(j=1; j<=lenJ; j++) {
      if(ddsElements[i][j] == "") {
//DebugN(i, j, lenJ, ddsElements[i][j], ddsElements[i-1][j]);
        ddsElements[i][j] = ddsElements[i-1][j];
      }
    }
  }
//  DebugN("******* after", ddsElements, ddiTypes);
  for(i=1;i<=lenI;i++) {
    lenJ=dynlen(ddiTypes[i]);
    sTemp = "";
    sTypeRef = "";
    ds = makeDynString();
    di = makeDynInt();
    for(j=1;j<=lenJ;j++) {
      switch(ddiTypes[i][j]) {
        case DPEL_TYPEREF:
//DebugN(i, j, ddsElements[i][j], "DPEL_TYPEREF");  
          iType = ddiTypes[i][j];
          sTempBis = ddsElements[i][j];
          sTypeRef = ddsElements[i][j+1];
          unGenericDpFunctions_getDPE(sTypeRef, ds, di);
          break;
        default:
//DebugN(i, j, ddsElements[i][j], "default", ddiTypes[i][j]);  
          iType = ddiTypes[i][j];
          sTempBis = ddsElements[i][j];
          break;
      }
      if(sTemp == "") 
        sTemp = sTempBis;
      else
        sTemp = sTemp+"."+sTempBis;
//DebugN("sTempBis="+sTempBis, "stemp="+sTemp);
    }
    if(sTemp != "") {
      if(iType == DPEL_TYPEREF) {
        lenIbis = dynlen(ds);
        for(iBis = 1; iBis<=lenIbis;iBis++) {
          dynAppend(dsTemp, sTemp+substr(ds[iBis], strlen(sTypeRef), strlen(ds[iBis])));
          dynAppend(diTemp, di[iBis]);
        }
      }
      else {
        dynAppend(dsTemp, sTemp);
        dynAppend(diTemp, iType);
      }
    }
  }
//  DebugN(dsTemp);
  dsDpel = dsTemp;
  diDpelType = diTemp;
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE
/** Convert the DPE in UNICOS format to PVSS DPE

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sUnicosDPE input, the DPE in UNICOS format
@param sPVSSDpe output, the PVSS DPE
*/
unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(string sUnicosDPE, string &sPVSSDpe)
{
  string tempDpe, sDpe;
  string sLeafDpe;
  dyn_string dsSplit;
  int i, len;
  string sTemp;
  
  dsSplit = strsplit(sUnicosDPE, ".");
  len = dynlen(dsSplit);
// look for all the possible cases, the first alias found==> assume this is correct.
  for(i=1;i<=len;i++) {
    if(sTemp == "")
      sTemp +=dsSplit[i];
    else
      sTemp +="."+dsSplit[i];

    tempDpe=unGenericDpFunctions_dpAliasToName(sTemp);
    if(tempDpe != "") {
      // alias found try first UNICOS like Device DPE
      tempDpe=unGenericDpFunctions_getDpName(tempDpe);
      sLeafDpe = substr(sUnicosDPE, strlen(sTemp)+1, strlen(sUnicosDPE));
      sDpe = tempDpe + ".ProcessInput." + sLeafDpe;
//  DebugN(tempDpe, sTemp, sLeafDpe, sDpe);
      if(!dpExists(sDpe)) {
        sDpe = tempDpe + ".ProcessOutput." + sLeafDpe;
//  DebugN(tempDpe, sTemp, sLeafDpe, sDpe);
  // if the DPE is not existing then reset the DPE
  // WARNING here in case of distributed system
        if(!dpExists(sDpe)) 
        {
          // try proxy DPE
          sDpe = tempDpe+"."+sLeafDpe;
          if(!dpExists(sDpe))
            sDpe = "";
        }
      }
      if(sDpe != "") // dpe found exit
        i = len+1;
    }
  }

  if(sDpe == "")
    sDpe = sUnicosDPE;
  sPVSSDpe = sDpe;
//DebugN("convert end", sUnicosDPE, tempDpe, sLeafDpe, sPVSSDpe);
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_getHostName
/** Get the Data Server HostName and IP address. 
It gets the Data Server's Hostname (ie the host of the specified system name) and that host's IP Address 

@par Constraints
  Only ever returns an IPv4 address at the moment 

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sSystemName input, PVSS System Name  (in order to know which DS)
@param sHostName output, DS hostname
@param sIPAddress output, DS IP address
*/
void unGenericDpFunctions_getHostName(const string sSystemName, string &sHostName, string &sIPAddress)
{
	int iLength;
	bool bHostFound, bIpV4Found;
	dyn_string dsIpAddresses;
	string sCurrentHostName;
	sHostName="";
	sIPAddress="";
	if (sSystemName=="")
	{
		DebugTN( "WARNING", "unGenericDpFunctions_getHostName - "+getCatStr("unSystemIntegrity", "SYS_NAME_NOT_FOUND"),"");
		return;	
	}
	// get hostname of the system specified in the DPE 
	if (dpExists(sSystemName + "_unApplication.hostname"))
	{
		dpGet(sSystemName + "_unApplication.hostname", sHostName);
	}	
	else
	{
		sHostName=getHostname();
	}
	if (sHostName=="")
	{
		DebugTN( "ERROR", "unGenericDpFunctions_getHostName - "+getCatStr("unSystemIntegrity", "HOST_NAME_NOT_FOUND"),"");
		return;
	}
		
	// with PVSS < 3.8
	//sIPAddress=getHostByName(sHostName); 

	//the function above does not return the 
	//ip address of the given host if other devices are installed on the 
	//local machine

	// get IP Address
	sIPAddress=getHostByName(sHostName,dsIpAddresses);
	iLength=dynlen(dsIpAddresses);
	for (int i=1 ; i<=iLength ; i++)
	{
		sCurrentHostName = getHostByAddr(dsIpAddresses[i]);
		bHostFound = patternMatch(sHostName+"*.*", sCurrentHostName);
		 //be sure that the IP got is a IPv4
		bIpV4Found =  patternMatch(sHostName+"*.*.*.*", dsIpAddresses[i]);
		if(bHostFound && bIpV4Found)
		{
			sIPAddress=dsIpAddresses[i];
			break;
		}
	}
	if (sIPAddress=="")
	{
		DebugTN("ERROR", "unGenericDpFunctions_getHostName - "+getCatStr("unSystemIntegrity", "HOST_IPADDRESS_NOT_FOUND"),"");
		return;
	}
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_setHostName
/** Set the Data Server HostName

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL
*/
void unGenericDpFunctions_setHostName()
{
  string sHostName;
  int ipmonPort;
  
  sHostName=getHostname();
  ipmonPort=pmonPort();
  
  //by default local system nane
  if (dpExists("_unApplication.hostname") && dpExists("_unApplication.pmonPort") && sHostName!="" && ipmonPort!=0) // Local system
    dpSet("_unApplication.hostname", sHostName, "_unApplication.pmonPort", ipmonPort);
}

//--------------------------------------------------------------------------------------------------------------------------------------

// unGenericDpFunctions_getDpName
/** Get the DP name substring, to be used instead of dpSubStr in distributed system

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, DP name
@return the dpName
*/
string unGenericDpFunctions_getDpName(string sDp)
{
  string dpName, systemName;
  
  dpName = substr(sDp, 0, strpos(sDp,"."));
  if (dpName == "")
  {
    dpName = sDp;
  }
  systemName = substr(sDp, 0, strpos(sDp,":") + 1);
  if ((systemName == "") && (dpName != ""))
  {
    dpName = getSystemName() + dpName;
  }
  return(dpName);
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_getAlias
/** Similar to dpGetAlias, returns the alias if it exists, returns the DPE name if not.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsDp input, datapoint or list of datapoints
@return alias or list of alias
*/
anytype unGenericDpFunctions_getAlias(anytype dsDp)
{
  dyn_string dsAlias, dsInput;
  string alias, sDpFormatted;
  int i, length, inputType;

  // 1. Find input type and format it
  inputType = getType(dsDp);
  switch (inputType)
  {
    case STRING_VAR:
      dsInput = makeDynString(dsDp);
      break;
    
    case DYN_STRING_VAR:
      dsInput = dsDp;
      break;
    
    default:
      dsInput = makeDynString();
      break;
  }
  length = dynlen(dsInput);
  for(i=1;i<=length;i++) // For each element
  {
  // 2. Add a point at the end if no point is found
    sDpFormatted = dsInput[i];
    if (strpos(sDpFormatted,".") < 0)
    {
      sDpFormatted = sDpFormatted + ".";
    }
    
  // 3. Get Alias
    alias = dpGetAlias(sDpFormatted); // return alias or empty string if empty
    
  // 4. Alias is empty ?
    if (alias == "")
    {
      alias = sDpFormatted;
      if (substr(alias, strlen(alias)-1) == ".")
      {
        alias = substr(alias,0,strlen(alias)-1);
      }
    }
  // 5. Append result
    dynAppend(dsAlias,alias);
  }
  return dsAlias;
}
//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_getSystemName
/** Get the DP system name substring, to be used instead of dpSubStr in distributed system

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, DP name
@return the system name
*/
string unGenericDpFunctions_getSystemName(string sDp)
{
  string systemName;
  
  if (sDp == "")
    return("");
  systemName = substr(sDp, 0, strpos(sDp,":")+1);
  if (systemName == "")        // If system name is unkown, return mySystemName
    systemName = getSystemName();
  return(systemName);
}

//------------------------------------------------------------------------------------------------------------------------
//unGenericDpFunctions_getpmonPort
/** Get the pmon port number for the Data Server

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sSystemName input, PVSS System Name (in order to know which DS)
@param ipmonPort output, pmon port number
*/
void unGenericDpFunctions_getpmonPort(const string sSystemName, int &ipmonPort)
{
  ipmonPort=0;
  if (sSystemName!="")
  {  
    if (dpExists(sSystemName + "_unApplication.pmonPort"))
      dpGet(sSystemName + "_unApplication.pmonPort", ipmonPort);
    else
      ipmonPort=pmonPort();
  }
}

//------------------------------------------------------------------------------------------------------------------------
// _unGenericDpFunctions_getKeyConfiguration
/** Get the device dpe device or unicos configuration for a given key
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sType input, the type of configuration
@param sKey input, the key
@param dsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
_unGenericDpFunctions_getKeyConfiguration(string sDeviceDpName, string sType, string sKey, dyn_string &dsConfig, dyn_string &exceptionInfo)
{
  dyn_string dsKey;
  dyn_dyn_string ddsConfig;
  int iPos;
  
  _unGenericDpFunctions_getConfiguration(sDeviceDpName, sType, dsKey, ddsConfig, exceptionInfo);
  iPos = dynContains(dsKey, sKey);
  if(iPos > 0) 
    dsConfig = ddsConfig[iPos];
  else
    dynClear(dsConfig);
}

//------------------------------------------------------------------------------------------------------------------------
// _unGenericDpFunctions_setKeyConfiguration
/** Set the device dpe device or unicos configuration for a given key, an empty dyn_string for a key remove the key entry
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sType input, the type of configuration
@param sKey input, the key
@param dsConfig input, the setting of the key
@param exceptionInfo output, error returned here
*/
_unGenericDpFunctions_setKeyConfiguration(string sDeviceDpName, string sType, string sKey, dyn_string dsConfig, dyn_string &exceptionInfo)
{
  dyn_string dsKey;
  dyn_dyn_string ddsConfig;
  int iPos;
  
  _unGenericDpFunctions_getConfiguration(sDeviceDpName, sType, dsKey, ddsConfig, exceptionInfo);
  if(dynlen(exceptionInfo) <= 0)
  {
    iPos = dynContains(dsKey, sKey);
    if(iPos > 0) {
      if(dynlen(dsConfig) > 0)
        ddsConfig[iPos] = dsConfig;
      else {
        dynRemove(dsKey, iPos);
        dynRemove(ddsConfig, iPos);
      }
    }
    else
    {
      if(dynlen(dsConfig) > 0)
      {
        dynAppend(dsKey, sKey);
        dynAppend(ddsConfig, dsConfig);
      }
    }
    _unGenericDpFunctions_setConfiguration(sDeviceDpName, sType, dsKey, ddsConfig, exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------
// _unGenericDpFunctions_setConfiguration
/** Set the device dpe device or unicos configuration, an empty dyn_string for a key remove the key entry
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sType input, the type of configuration
@param dsKey input, the key
@param ddsConfig input, the setting of the key
@param exceptionInfo output, error returned here
*/
_unGenericDpFunctions_setConfiguration(string sDeviceDpName, string sType, dyn_string dsKey, dyn_dyn_string ddsConfig, dyn_string &exceptionInfo)
{
  int iKey, lenKey=dynlen(dsKey), iConfig, lenConfig;
  dyn_string dsConfig;
  
  while(dynlen(ddsConfig) < lenKey)
    dynAppend(ddsConfig, makeDynString());
  for(iKey=1;iKey<=lenKey;iKey++)
  {
    lenConfig = dynlen(ddsConfig[iKey]);
    for(iConfig=1;iConfig<=lenConfig;iConfig++)
      dynAppend(dsConfig, dsKey[iKey]+UN_CONFIGURATION_KEY_SEPARATOR+ddsConfig[iKey][iConfig]);
  }
  _unGenericDpFunctions_setRawConfiguration(sDeviceDpName, sType, dsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// _unGenericDpFunctions_getConfiguration
/** Get the device dpe device or unicos configuration
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sType input, the type of configuration
@param dsKey output, the key
@param ddsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
_unGenericDpFunctions_getConfiguration(string sDeviceDpName, string sType, dyn_string &dsKey, dyn_dyn_string &ddsConfig, dyn_string &exceptionInfo)
{
  int i, len, iPos;
  dyn_string dsConfig, dsTemp;
  string sKey, sConfig;
  mapping mTemp;
  
  _unGenericDpFunctions_getRawConfiguration(sDeviceDpName, sType, dsConfig, exceptionInfo);
  dynClear(dsKey);
  dynClear(ddsConfig);
  
  if(dynlen(exceptionInfo) <= 0)
  {
    len = dynlen(dsConfig);
    for(i=1;i<=len;i++)
    {
      iPos = strpos(dsConfig[i],UN_CONFIGURATION_KEY_SEPARATOR);
      if(iPos >0)
      {
        sKey = substr(dsConfig[i], 0, iPos);
        sConfig = substr(dsConfig[i], iPos+1, strlen(dsConfig[i]) - iPos -1);
        if(mappingHasKey(mTemp, sKey))
          dsTemp = mTemp[sKey];
        else
          dynClear(dsTemp);
        dynAppend(dsTemp, sConfig);
        mTemp[sKey] = dsTemp;
      }
    }
    len=mappinglen(mTemp);
    for(i=1;i<=len;i++)
    {
      dynAppend(dsKey, mappingGetKey(mTemp, i));
      dynAppend(ddsConfig, mappingGetValue(mTemp, i));
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// _unGenericDpFunctions_setRawConfiguration
/** Set the device dpe device or unicos configuration
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sType input, the type of configuration
@param sKey input, the key
@param dsConfig input, the setting of the key
@param exceptionInfo output, error returned here
*/
_unGenericDpFunctions_setRawConfiguration(string sDeviceDpName, string sType, dyn_string dsConfig, dyn_string &exceptionInfo)
{
  string sDpe=sDeviceDpName+sType;
  
  if(dpExists(sDpe))
    dpSet(sDpe, dsConfig);
  else
    fwException_raise(exceptionInfo, "ERROR", sDeviceDpName+getCatStr("unSystemIntegrity", "DPDOESNOTEXIST"),"");
}

//------------------------------------------------------------------------------------------------------------------------
// _unGenericDpFunctions_getRawConfiguration
/** Get the device dpe device or unicos configuration
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sDeviceDpName  input, device name DP name
@param sType input, the type of configuration
@param sKey output, the key
@param dsConfig output, the setting of the key
@param exceptionInfo output, error returned here
*/
_unGenericDpFunctions_getRawConfiguration(string sDeviceDpName, string sType, dyn_string &dsConfig, dyn_string &exceptionInfo)
{
  string sDpe=sDeviceDpName+sType;
  
  dynClear(dsConfig);
  if(dpExists(sDpe))
    dpGet(sDpe, dsConfig);
  else
    fwException_raise(exceptionInfo, "ERROR", sDeviceDpName+getCatStr("unSystemIntegrity", "DPDOESNOTEXIST"),"");
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_getComponentsVersion
/** returns the list of component and their version as a string
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsComponents  input, list of components to retreive
@return output, the list of component and their version as a string
*/
string unGenericDpFunctions_getComponentsVersion(dyn_string dsComponents)
{
  int iRes, i, len;
  dyn_dyn_string ddsComponentsInfo;
  string sMessage;
  dyn_string dsInstalledComponent, dsInstalledVersion;
  
  iRes = fwInstallation_getInstalledComponents(ddsComponentsInfo);
  len = dynlen(ddsComponentsInfo);
  for(i=1;i<=len;i++)
  {
    dynAppend(dsInstalledComponent, ddsComponentsInfo[i][1]);
    dynAppend(dsInstalledVersion, ddsComponentsInfo[i][2]);
  }
  len=dynlen(dsComponents);
  for(i=1;i<=len;i++)
  {
    iRes = dynContains(dsInstalledComponent, dsComponents[i]);
    if(iRes > 0)
    {
      if(sMessage == "")
        sMessage = dsInstalledComponent[iRes]+" version "+dsInstalledVersion[iRes];
      else
        sMessage +=","+ dsInstalledComponent[iRes]+" version "+dsInstalledVersion[iRes];
    }
  }
  return sMessage;
}

//------------------------------------------------------------------------------------------------------------------------
// unGenericDpFunctions_isOnlineBackupRunning
/** returns true=online backup running/false=online backup not running
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sSystemName  input, system name in which to check the online backup state
@return output, true=online backup running/false=online backup not running
*/
bool unGenericDpFunctions_isOnlineBackupRunning(string sSystemName)
{
  bool bOnlineBackup, bConnected;
  //dist if ever, dpExists.
  if(sSystemName == getSystemName())
    bConnected = true;
  else if(isFunctionDefined("unDistributedControl_isConnected"))
    unDistributedControl_isConnected(bConnected, sSystemName);
  else if(dpExists(sSystemName+"_DataManager.Backup.Status"))
    bConnected = true;
  if(bConnected)
    dpGet(sSystemName+"_DataManager.Backup.Status", bOnlineBackup);
  return bOnlineBackup;
}

//------------------------------------------------------------------------------------------------------------------------

//@}
