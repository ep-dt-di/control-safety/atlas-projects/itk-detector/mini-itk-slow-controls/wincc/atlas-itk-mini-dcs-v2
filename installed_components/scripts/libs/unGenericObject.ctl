/**@name LIBRARY: unGenericObject.ctl
 
@author: Vincent Forest (AB-CO)

Creation Date: 21 05 2003 

Modification History: 
  02/03/2015: Jean-Charles
  - {IS-1781} unCore: Integration with fwSynopticLocator. Added the function unGenericObject_addSynopticLocatorActionToMenu()
                       and modified unGenericObject_handleDefaultUnicos() & unGenericObject_addDefaultUnicosActionToMenu()

  09/04/2014: Marco Boccioli
  - IS-817  Added a conversion on ungenericObject.ctl, in unGenericObject_OpenSynopticDiagnosticPopup()
  
  26/02/2014: Jean-Charles Tournier
  - IS-241 Add a function unGenericObject_TabFaceplateAddTabulationInit to automatically add the tabulation to a device faceplate (e.g. to add the alarm and event faceplate)

  15/11/2013: Marco Boccioli
  - IS-817  Passing $ parameters into execScripts for unGenericObject_WidgetInit(), 
	UN_GENERICOBJECT_FUNCTION_BUTTONDISCONNECT, UN_GENERICOBJECT_FUNCTION_FACEPLATESTATUSDISCONNECT
 
  24/10/2013: Alexey Merezhin
  - IS-945  Added new functions to allow import/export API to be used by import/export panel
 
  17/10/2011: Herve
  - IS-628  unCore - General  Faceplate trending: raise exception when the Faceplate trend config is not correct. 
 
  10/10/2011: Herve
  - IS-622  unCore - General  In Linux, use firefox instead of mozilla for the Info link 
 
  06/05/2011: Herve
  - IS-524: Allow multiple selection per GUI
  
  26/11/2010: Herve
  - IS-464: in faceplate close, check if the remote system is connected before closing
  
  23/11/2010: Quique
    - [IS-460] originally [UCPC-35] WordStatusBitBig widget: error when 32 bits are defined, function: unGenericObject_ColoredSquareAnimate()
    
  10/11/2010: Quique
    - [IS-441] Start Interlock in the OnOff, Analog, AnaDig (field object) must not animate the widget body with the alarm state
    
  17/08/2010: Herve
    - IS-365: new function unGenericObject_displaySystemAlarmTrend
    
  05/08/2010: Marco
    - IS-294 (adding the new features of the fwTrending 3.3.0). New function:
    unGenericObject_getDeviceDpeTrendExtendedConfig: get (or create and get) the fwTrendingPlot config dpe name.

  10/08/2009: Herve
    - front-end proxy and trend implementation
    . modified functions: unGenericObject_InitializeFrontEnd, unGenericObject_GetFaceplateTrendConfig
    unGenericObject_GetTrendFaceplatePanel, unGenericObject_GetTrendDPEList, unGenericObject_getDeviceTrendConfig
    _unGenericObject_deviceGetTrendDPE, unGenericObject_handleSingleDPETrendAction, unGenericObject_getProxyDeviceType
    unGenericObject_handleTrendAction
    . new function: unGenericObject_FrontEndConfiguration
    
  04/08/2009: Herve
    - device proxy implementation
    . modified functions: unGenericObject_InitializeObjects, unGenericObject_addTrendActionToMenu, 
    unGenericObject_handleTrendAction, unGenericObject_handleSingleDPETrendAction, _unGenericObject_FaceplateTrendInit
    unGenericObject_FaceplateTrendInit, unGenericObject_TabFaceplateExtendedTrendInit, unGenericObject_ButtonFaceplateExtendedTrendInit
    . new functions: unGenericObject_getProxyDeviceType, unGenericObject_deviceGetAllTrendDPE, 
    unGenericObject_deviceGetTrendDPE, unGenericObject_deviceGetProxyTrendDPE
    
  07/05/2009: Herve
    - modified functions: 
    unGenericObject_addTrendActionToMenu: extra parameter to use DPE alias for legend
    unGenericObject_handleTrendAction: use DPE alias for legend if Alias exists
    
  17/02/2009: Herve
    - modified functions:
    unGenericObject_GetPLCNameFromDpName: return also the FE name for a FE device
	unGenericObject_GetFunctions: return the functions for a FE device
	unGenericObject_InitializeObjects: get all the device and FE widget, functions, etc.
	- new functions:
    unGenericObject_InitializeFrontEnd: get all the Front-End widget and configs
	unGenericObject_getFrontState: get the front-end state for snapshot & widget
	unGenericObject_getFrontEndColorState: get the front-end state color for each Front-end dpe
    
  16/02/2009 Herve
    - menu rigth click: Mail/SMS --> open unSetMailSMS.pnl

  10/02/2009 Quique
      - Case JIRA IS-18: Animation of the right click menu "ack alarm" taking into account the acknowledge status of the alarm 
      			 Modified function: unGenericObject_addUnicosActionToMenu()	
  23/01/2009 Quique
      - Case JIRA IS-14: Animation widget OnOff when both feedbacks ON and OFF are set to 1.
                         Modified visualization logic for the widgets: _WidgetXXXXXAnimation() where
                         XXXXX= Valve, Heater, InvertedValve, Motor, Square4 

  21/10/2008 Quique
      - Modification unGenericObject_addUnicosActionToMenu() to include the fact of being inhibit to block an alarm.
      
  13/10/2008 Quique
      - Animation of the Analog widget (include stsReg02)
      
  30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel
      . replace isPanelOpen by unGraphicalFrame_isPanelOpen
    
  07/05/2008: Herve
    - new functions for 3WayValve and InvertedValve (from GCS)
    
  21/04/2008: Herve
    - unGenericObject_FaceplateTrendInit use: unGenericObject_TabFaceplateExtendedTrendInit
    - unGenericObject_addDiagnosticAndInfoToActionToMenu: disabble Diagnostic and Synoptic if internal trigger does not exists
    
  15/04/2008: Herve
    - unGenericObject_InitializeObjects: do not use myModuleName as plot Dp name
    
  26/03/2008: Herve
    - check access control to delete dynamic trend
    
  20/03/2008: Herve
    - new function for the faceplate TrendConfig
    - unGenericObject_InitializeObjects updated
    
  27/02/2008: Herve
    - unGenericObject_OpenHtmlFile case Linux use mozilla file &
    
        19/12/2007: Herve
                QT implementation
                - new function: unGenericObject_deselectDisplay
                - unGenericObject_FaceplateTrendInit: missing in $iMarkerType = fwTrending_MARKER_TYPE_NONE
                - use lock.xpm
                  
	17/12/2007: Herve
		- bug dynamic trend if different manager and same module
		
	23/11/2007: Herve
		- dynamic trend with step instead of linear
		
	29/10/2007: Herve
		- new function unGenericObject_OpenHtmlFile
		
	22/10/2007: Herve
		- synoptic from RC: allow a link to WindowTree
		- diagnostic: allow a link to WindowTree
		- unGenericObject_OpenHtml put the link between ""
		
	25/09/2007: Frederic
		- new function: unGenericObject_compareMapping
	
	29/06/2007: Herve
		- RC menu, case AnaDig and Analog, if Controller device is in active mode add *
		
	26/04/2007: Herve
		- _unGenericObject_WidgetInit: same function as unGenericObject_WidgetInit but without calling unGenericObject_GetFunctions
		- unGenericObject_GetFunctions call _unGenericObject_GetFunctions and give functions.
		
	01/03/2007: Herve
		- unGenericObject_OpenHtml use local _unApplication
		
	14/12/2006: Herve
		- bug RC for device link in dist.
		
	04/02/2006: Frederic
		- update unGenericObject_OpenHtml() to open the html page with/without the VB script (disable/enable the ToolBar)

	23/11/2006: Herve
		- 3 new functions: unGenericObject_ColoredSquareAnimate, unGenericObject_ColoredSquareToolTip and unGenericObject_ColoredSquareDisconnect
		for the unFaceplate_ColoredSquare panel
				
	21/11/2006: Herve
		- new parameter in function unGenericObject_FaceplateTrendInit
			dyn_bool dbYScaleVisible =makeDynBool() --> visibility of the Y scale
			 
	10/10/2006: Herve
		- unGenericObject_FormatValue: alway put the same number of digit: 12.0, 120, 12.8 --> 12.9
		
	17/05/2006: Herve
		- bug multi link:
			. unGenericObject_addDiagnosticAndInfoToActionToMenu: device not existing in local system not greyed out
			. unGenericObject_WidgetRightClick: if menuAnswer <=0 no handling action launched
			. unGenericObject_handleDeviceLink: 6th link open faceplate does not work due to a < with a string
			
	15/05/2006: Herve
		- unGenericObject_WidgetAIAOAnimation: add set of alarmLetter to HH, H, L or LL
		
	02/05/2006: Herve
		- implement mask/unmask event
		- use constant for device links
		
	10/04/2006: Herve
		- implementation of link between devices in unGenericObject_WidgetRightClick, 
		unGenericObject_addDefaultUnicosActionToMenu, unGenericObject_addTrendActionToMenu
		unGenericObject_addDiagnosticAndInfoToActionToMenu, unGenericObject_handleDiagnosticAndInfoToActionToMenu
		
	13/12/2005: Celine
		- unGenericObject_WidgetAIAOAnimation, unGenericObject_WidgetAnimation: add g_bSystemConnected checking

	02/12/2005: Celine
		- unGenericObject_handleSelect,unGenericObject_ButtonSelectAction : add select message with users information

	15/11/2005: Herve
		- unGenericObject_addDiagnosticAndInfoToActionToMenu: add device config.
		
	26/08/2005: Frederic
		- update unGenericObject_OpenDiagnostic() to pass $sDpName

	07/07/2005: Herve
		- exceptionInfo overwritten with evalScript, modified function: unGenericObject_WidgetRightClick
	
	29/06/2005: Herve
		- unGenericObject_WidgetRightClick use sIdentifier instead of $sIdentifier
		- unGenericObject_FaceplateTrendInit 
			. replace any ., / and | in the facepalte plot zoom title (used as plot title)
			. zoom title based on alias and tab number
				
	17/06/2005: Celine
		- add Mail-SMS in unGenericObject_HandleUnicosMenu,  unGenericObject_addUnicosActionToMenu
		
	14/06/2005: Frederic
		- move unGenericObject_SendManRegPulse code to unGenericObject_SendPulse (to be able to send a pulse on PIC dpe)

	25/05/2005: Herve
		- printing: backcol AnalogInput/AnalogOutput
			. unGenericObject_WidgetAIAOAnimation: remove set Body1, backCol
			. unGenericObject_WidgetAIAODisconnection: copy the Body1Disconnection and remove the set Body1, backCol

	22/04/2005: Herve
		- new function unGenericObject_addDiagnosticAndInfoToActionToMenu: to add Diagnostic and Info to right click menu
		Always done, not configurable. 
		
	15/04/2005: Herve
		- unGenericObject_GetPLCNameFromDpName: return the PLCtype+"_"+plcName, in case of _UnPlc it returns _unPlc_+plcName
		
	28/03/2005: Herve
		- unGenericObject_FaceplateTrendInit: allow list of DPE to trend, if empty -> all configured DPE
		
	25/02/2005: Herve
		- dynamic trend: for the 7th curve the legend is not correct.
		
	22/02/2005: Herve
		- dynamicTrend: set the legend to deviceName.leafDPE instead of leafDPE.
		
	10/01/2005: Herve
		- unGenericObject_FormatValue: check the the value is not > pow(10, nbrDigit -1), if yes set the value to UN_DISPLAY_VALUE_DIGIT_TOO_BIG_VALUE

	14/12/2004: Herve
		- modify the function unGenericObject_FormatValue to accept fixed digit format

	07/12/2004: Herve
		- unGenericObject_FaceplateTrendInit: give to the faceplate trending DPE in UNICOS format instead of PVSS format
		
	22/11/2004: Herve
		- Dynamic trend: do not add DPE if already in the list
		
	05/11/2004: Herve
		bug in unGenericObject_WidgetAIAOAnimation case HH H L and H L LL wrong color 
		
	28/10/2004: Herve
		- for faceplate Trend use UNICOS DPE format instead of PVSS DPE format
		- add dynamic trend

	27/10/2004: Herve
		- add unGenericObject_GetTrendFaceplatePanel function
		
	26/10/2004: Herve
		- in the function unGenericObject_ButtonInit: reset the g_dsUserAccess (list of allowed button), it has to be initialized in the device_ButtonRegisterCB 
		in case the register and log in is at the same time.
		
	22/10/2004: Herve
		- remove the dpGetUnit and dpGetFormat from the unGenericObject_DisplayValue and unGenericObject_DisplayValue
		- modify the unGenericObject_FormatValue
		- modify unGenericObject_DisplayValue, unGenericObject_WidgetAIAOAnimation, unGenericObject_DisplayRange, 
		unGenericObject_WidgetDisplayValueAnimation, unGenericObject_DisplayEventLostFocus
		
	18/10/2004: Herve
		- update to v3.0
		- add the $bShowGrid=false, $bShowLegend=false and $templateParameters="" in the unGenericObject_FaceplateTrendInit
		- add new dyn_string in g_ddsObjectConfigs: list of trendable DPE.
		- add the functions: unGenericObject_addTrendActionToMenu, unGenericObject_handleTrendAction, unGenericObject_GetTrendDPEList
		- add in unGenericObject_HandleUnicosMenu the function call unGenericObject_handleTrendAction
		- add in unGenericObject_ColorBoxAnimate: sZeroBackCol, string, input, backcolor if command bit = 0, default value = "unFaceplate_Unactive"
		- in function unGenericObject_ClosePanel: test if manreg exists before setting it to 0 
		
	03/05/2004: Herve
		- in unGenericObject_WidgetInit: check if LockBmp exists before setting its attribute: case there is no lockBmp
		- add unGenericObject_GetPLCNameFromDpName: extract the PLC name from the DPname.

	13/04/2004: Herve
		- in unGenericObject_WidgetRightClick, call the device function to handle the result of the pop-up menu
		- add function to handle the select: unGenericObject_handleSelect and the UNICOS default action unGenericObject_handleDefaultUnicos 
		and the result of the pop-up menu for the UNICOS device: unGenericObject_HandleUnicosMenu
		- in unGenericObject_ButtonInit remove the dpSet ManReg01 to 0 if different from 0 and put it in all the device_ButtonRegisterCB functions
		- in unGenericObject_WidgetRightClick call the device function to create the menu
		- add the function call: unGenericObject_addSelectToMenu, unGenericObject_addUnicosActionToMenu and 
		unGenericObject_addDefaultUnicosActionToMenu
		- create the new function unGenericObject_addSelectToMenu, unGenericObject_addUnicosActionToMenu and 
		unGenericObject_addDefaultUnicosActionToMenu

	08/04/2004: Herve
		in unGenericObject_ButtonUserCB use stsReg02Value instead of stsReg02 and add unGenericObject_UserCBGetButtonState: function called before the buttonsetstate

	30/03/2004: Herve
		in funtion unGenericObject_WidgetInit: use Lock.emf instead of Lock.bmp

	25/03/2004: Herve
		add HH, H, L and LL, L, H alarm case for AnalogInput/AnalogOutput device

	24/02/2004: Herve
		- exceptionInfoTemp not delcared
		
	11/12/2003: Herve
		- in unGenericObject_WidgetAnimation use also the _unSystemAlarm_PLC_DS value 
		0 no alarm, 10 alarm and therefore set the widget to the same color as if it is invalid.

	26/11/2003: Herve
		- in unGenericObject_WidgetAIAOAnimation and unGenericObject_WidgetDIDOAnimation use also the _unSystemAlarm_PLC_DS value 
		0 no alarm, 10 alarm and therefore set the widget to the same color as if it is invalid.
		- widget: remove the dpConnect on pv_range, not anymore needed

	22/10/2003: Herve
		add the following cases for alarm: H&L, HH&H, LL&L, H, L
 
	17/11/2003: Herve
		in unGenericObject_WidgetRightClick function rset the manreg01 if not 0 in the case of Block/deblock PLC and ack Alarm. 
		This is due to a bug in PVSS: the rightClick event script is not executed until the end if another one is 
		started before the end of execution of the previous one.

version 1.0

External Function : 
	. unGenericObject_GetFunctions : return a list of available functions for an object
	. unGenericObject_WidgetGetFunctions : return a list of available functions for a widget type
	. unGenericObject_ClosePanel : action before faceplate close
	. unGenericObject_InitializeObjects : initialize object configurations

	. unGenericObject_Acknowledge : acknowledge current device alarms
	. unGenericObject_NeedSelect : return true if object could be selected
	. unGenericObject_Connection : this function checks the connection status and return a value for dp(dis)Connect
	. unGenericObject_SendManRegPulse : send a pulse on a specified bit of ManReg01
	. unGenericObject_SendPulse : send a pulse on a specified bit and on a specified Dpe
	. unGenericObject_WaitPulse : send ManReg pulse delay
	. unGenericObject_SendBinaryOrder : send a binary order to the PLC

	. unGenericObject_FaceplateStatusInit : initialization of Unicos faceplate
	. unGenericObject_ColorBoxAnimate : animate "non alarm" colorBoxes
	. unGenericObject_ColorBoxAnimateAlarm : animate alarm colorBoxes
	. unGenericObject_ColorBoxDisconnect : disconnection of colorBoxes
	. unGenericObject_DisplayValue : animate a display value
	. unGenericObject_DisplayRange : animate a display range
	. unGenericObject_FormatValue : format value
	. unGenericObject_StringToFloat : to convert a string in a float value
	. unGenericObject_DisplayEventLostFocus : check text in a text shape and update display
	. unGenericObject_DisplayValueDisconnect : disconnection of display values

	. unGenericObject_ButtonInit : initialize buttons
	. unGenericObject_ButtonAnimateSelect : animate button select
	. unGenericObject_ButtonSelectAction : select / deselect device
	. unGenericObject_ButtonDisconnect : disconnection of buttons

	. unGenericObject_GetFaceplateTrendConfig : get current configuration of device faceplate trend tab
	. unGenericObject_FaceplateTrendInit : create the trend tab in faceplate

	. unGenericObject_WidgetInit : initialize widget
	. unGenericObject_WidgetDisconnection : animate widget disconnection
	. unGenericObject_WidgetDisplayValueDisconnect : disconnection of widget display values
	. unGenericObject_WidgetBody1Disconnection : animate disconnection for body with only one shape
	. unGenericObject_WidgetAnimation : animate widget
	. unGenericObject_WidgetSelectAnimation : animate the select part of the widget
	. unGenericObject_WidgetWarningAnimation : animate the warning part of the widget
	. unGenericObject_WidgetControlStateAnimation : animate the control state part of the widget
	. unGenericObject_WidgetAlarmTextAnimation : animate the alarm part of the widget

	. unGenericObject_OpenFaceplate : open faceplate
	. unGenericObject_OpenDiagnostic : open diagnostic panel
	. unGenericObject_OpenHtml : open html link
	. unGenericObject_WidgetRightClick : right click event on widget area
	. unGenericObject_CheckAnalogAlarmType : check 5 ranges alarm
	
Internal Functions :
	. unGenericObject_ClosePanelCB : callback function called by unGenericObject_ClosePanel
	. unGenericObject_ButtonUserCB : user callback for buttons

Purpose: This library contains generic functions for Unicos objects.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. Unicos objects (DPType etc.)
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

#uses "unGenericObjectDeprecated.ctl"
#uses "unSelectDeselectHMI.ctl"
#uses "unSendMessage.ctl"
#uses "unPanel.ctl"
#uses "unGenericDpFunctionsHMI.ctl"
#uses "fwTrending/fwTrending.ctl"
#uses "fwGeneral/fwWebBrowser.ctl"
#uses "libunCore/unGenericObjectDeprecated.ctl"

private const bool unGenericObject_unHMILibLoaded = fwGeneral_loadCtrlLib("unHMI/unHMI.ctl",false);

//@{

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_GetPLCNameFromDpName
/**
Purpose: return the PLC Name. The PLC Name is extracted from the dp name (UNICOS format): prefix-PLCName-xxxx

Parameters:
	- sDpType, string, input, dpType
	- dsFunctions, dyn_string, output, list of functions that can be accessible using constants defined in unicos_declarations.ctl, unicos_declarations_core.ctl
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

string unGenericObject_GetPLCNameFromDpName(string sDp)
{
  string dpName, dpNoSystem, sPlcName = "", sPlcDpName, sPlcType, sSystemName, sReturnValue;
  dyn_string split;
	
  dpName = substr(sDp, 0, strpos(sDp,"."));
  sSystemName = unGenericDpFunctions_getSystemName(sDp);
	
  if (dpName == "")
  {
    dpName = sDp;
  }
  dpNoSystem = substr(dpName, strpos(dpName,":") + 1, strlen(dpName));
  if (dpNoSystem == "")
  {
    dpNoSystem = dpName;
  }

  if(!unGenericDpFunctions_isFrontEnd(dpNoSystem))
  {
    // get the PLC name 
    split = strsplit(dpNoSystem, UN_DPNAME_SEPARATOR);
    if(dynlen(split) >= 2) {
      sPlcName = split[2];	
    }

    if(sPlcName != "") {
      sPlcDpName = unGenericDpFunctions_dpAliasToName(sSystemName + sPlcName);
      if(dpExists(sPlcDpName)) {
        sPlcType = dpTypeName(sPlcDpName);
        if(sPlcType == UN_PLC_DPTYPE)
          sPlcType = "_unPlc";
      }
    }
    sReturnValue = sPlcType+"_"+sPlcName;
  }
  else {
    if(dpExists(sSystemName+dpNoSystem))
      sReturnValue = dpNoSystem;
  }
//DebugN("unGenericObject_GetPLCNameFromDpName", sDp, sReturnValue);
  return sReturnValue;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_GetFunctions
/**
Purpose: return a list of available functions for an object

Parameters:
	- sDpType, string, input, dpType
	- dsFunctions, dyn_string, output, list of functions that can be accessible using constants defined in unicos_declarations.ctl, unicos_declarations_core.ctl
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_GetFunctions(string sDpType, dyn_string &dsFunctions, dyn_string &exceptionInfo)
{
  dyn_string exceptionInfoTemp;
  int i, iObject;

  // 1. Initialize data if not already done
  dynClear(dsFunctions);
  if (globalExists("g_dsObjectList"))
  {
    if (dynlen(g_dsObjectList) <= 0)
    {
      unGenericObject_InitializeObjects(exceptionInfoTemp);
    }
  }
  else
  {
    unGenericObject_InitializeObjects(exceptionInfoTemp);
  }
  // 2. Get object
  iObject = dynContains(g_dsObjectList, sDpType);
  if (iObject <= 0)
  {
    if(mappingHasKey(g_mFrontEndConfigs, sDpType))
      dsFunctions = g_mFrontEndConfigs[sDpType];
    else
      fwException_raise(exceptionInfo, "ERROR", "unGenericObject_GetFunctions: unknown object", "");
//DebugN(sDpType, dsFunctions, exceptionInfo);
  }
  else
  {
  // 3. Get object functions
    for(i=1;i<=UN_GENERICOBJECT_FUNCTION_LENGTH;i++)
    {
      dynAppend(dsFunctions, g_ddsObjectConfigs[iObject][i]);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetGetFunctions
/**
Purpose: return a list of available functions for a widget type

Parameters:
	- sWidgetType, string, input, widget type ex: Heater, Valve etc.
	- sDpType, string, input, dpType
	- dsFunctions, dyn_string, output, list of functions that can be accessible using constants defined in unicos_declarations.ctl, unicos_declarations_core.ctl
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetGetFunctions(string sWidgetType, string sDpType, dyn_string &dsFunctions, dyn_string &exceptionInfo)
{
	dsFunctions = makeDynString("unGenericObject_Widget" + sWidgetType + "Disconnection",
							    "unGenericObject_Widget" + sWidgetType + "Animation");
}

//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_ClosePanel
/**
Purpose: action before faceplate close

Parameters:	None
			
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. This library works with an unicos button panel and need global variables declared in this panel
		-> $sDpName, string, the device DP name	
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_ClosePanel()
{
    string deviceName = unGenericDpFunctions_getDpName($sDpName);
    string sSystemName = unGenericDpFunctions_getSystemName(deviceName);

    bool bIsConnected;
    unDistributedControl_isConnected(bIsConnected, sSystemName);
    if(bIsConnected) {
        string panelName = _unGraphicalFrame_getModuleId() + unGenericDpFunctions_getAlias(deviceName) + " " + unGenericDpFunctions_getDescription(deviceName) + " Closing...";
        for (int i = 1; i <= 2; i++) {
            string manRegDPE = deviceName + ".ProcessOutput.ManReg0" + i;
            // if the manreg exists, wait until the competion of the command or reset it after a delay.
            if (dpExists(manRegDPE)) {
                int manRegValue;
                dpGet(manRegDPE, manRegValue);
                if (manRegValue > 0) {
                    ChildPanelOnCentralModal("vision/unicosObject/General/MessageWait.pnl", panelName, makeDynString("$1:" + getCatStr("unOperator","WAITMANREGPULSE")));
                    unGenericObject_PulseAdaptiveWait(deviceName, ".ProcessOutput.ManReg0" + i);
                    PanelOffModule(panelName, myModuleName());
                }
            }
        }
    }
    PanelOff();
}

//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_ClosePanelCB
/**
Purpose: callback function called by unGenericObject_ClosePanel

Parameters:	None
			
Usage: Internal function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_ClosePanelCB(string sManReg01, int fManReg01)
{
	if (fManReg01 == 0)
		{
		PanelOff();
		}
}


//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_InitializeObjects
/**
Purpose: initialize object configurations

Parameters:	
	- exceptionInfo, dyn_string, output, for errors
	
This function gets object configuration and saves it into 2 global variables :
	- g_dsObjectList, dyn_string, object list
	- g_ddsObjectConfigs, dyn_dyn_string, config table
			
Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_InitializeObjects(dyn_string &exceptionInfo)
{
	int i, length, iRes, j, length2;
	dyn_string dsFunctions, dsConfigLine, dsUnicosObjects, dsFaceplateTrendList, dsTrendDPE, dsResult, dsWidgets, dsProxy;
	bool bSelect;
	string faceplatePath, buttonPath, sFaceplateTrendList, sTrendDPE, sTrendFaceplate;
        int iWidget, lenWidget;

	if (g_bObjectInitRecall)
	{
		g_bObjectInitRecall = false;
		
		if (!globalExists("g_dsObjectList"))
			{
			addGlobal("g_dsObjectList", DYN_STRING_VAR);
			}
		if (!globalExists("g_ddsObjectConfigs"))
			{
			addGlobal("g_ddsObjectConfigs", DYN_DYN_STRING_VAR);
			}
		dynClear(g_dsObjectList);
		dynClear(g_ddsObjectConfigs);
		unGenericDpFunctions_getUnicosObjects(dsUnicosObjects);
		length = dynlen(dsUnicosObjects);
		for(i=1;i<=length;i++)
		{
			iRes = dpGet(getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".configHMI.Functions", dsFunctions,
						 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".configHMI.Select", bSelect,
						 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".configHMI.FaceplatePath", faceplatePath,
						 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".configHMI.ButtonPath", buttonPath,
						 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".configHMI.TrendFaceplatePanel", sTrendFaceplate,
						 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".trendingDpSelector.FaceplateTrendList", dsFaceplateTrendList,
						 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".trendingDpSelector.DpeList", dsTrendDPE,
                                                 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".unWidgets.List", dsResult,
                                                 getSystemName() + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + ".proxy", dsProxy);
			if (iRes < 0)
			{
				fwException_raise(exceptionInfo, "ERROR", "unGenericObject_InitializeObjects " + dsUnicosObjects[i] + UN_OBJECT_EXTENSION + getCatStr("unPVSS", "DPGETFAILED"),"");
				dynClear(dsFunctions);
				bSelect = false;
				faceplatePath = "";
				buttonPath = "";
				dynClear(dsFaceplateTrendList);
			}
			while (dynlen(dsFunctions) < UN_GENERICOBJECT_FUNCTION_LENGTH)
			{
				dynAppend(dsFunctions, "");
			}
			dsConfigLine = dsFunctions;
			dynAppend(dsConfigLine, makeDynString(bSelect, faceplatePath, buttonPath));
			sFaceplateTrendList = "";
			length2 = dynlen(dsFaceplateTrendList);
			for(j=1;j<=length2;j++)
			{
				sFaceplateTrendList = sFaceplateTrendList + UN_FACEPLATE_TRENDS_DELIMITER + dsFaceplateTrendList[j];
			}
			sFaceplateTrendList = substr(sFaceplateTrendList, strlen(UN_FACEPLATE_TRENDS_DELIMITER));
			dynAppend(dsConfigLine, sFaceplateTrendList);

			sTrendDPE = "";
			length2 = dynlen(dsTrendDPE);
			for(j=1;j<=length2;j++)
			{
				sTrendDPE = sTrendDPE + UN_FACEPLATE_TRENDS_DELIMITER + dsTrendDPE[j];
			}
			sTrendDPE = substr(sTrendDPE, strlen(UN_FACEPLATE_TRENDS_DELIMITER));
			dynAppend(dsConfigLine, sTrendDPE);
			
			if(sTrendFaceplate == "")
				sTrendFaceplate = UN_DEFAULT_TRENDFACEPLATEPANEL;
				
			dynAppend(dsConfigLine, sTrendFaceplate);
			
                        dynClear(dsWidgets);
                        lenWidget=dynlen(dsResult);
                        for(iWidget=1;iWidget<=lenWidget;iWidget++)
                        {
                          if (getPath(PANELS_REL_PATH, dsResult[iWidget]) != "")
			  {
			    dynAppend(dsWidgets, dsResult[iWidget]);
			  }
                        }
			g_ddsObjectConfigs[i] = dsConfigLine;
                        g_ddsDeviceTrendConfig[i] = unGenericObject_InitializeTrendConfig(dsUnicosObjects[i], exceptionInfo);
                        g_ddsObjectWidgetList[i] = dsWidgets;
                        g_ddsObjectProxyList[i] = dsProxy;
		}
                unGenericObject_InitializeFrontEnd();
		g_dsObjectList = dsUnicosObjects;
		g_bObjectInitRecall = true;
	}
	else
	{
		for(i=1;i<=20;i++)
		{
			delay(0,100);
			if (dynlen(g_dsObjectList) > 0)
			{
				return;
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

//unGenericObject_InitializeFrontEnd
/**
Purpose: get the front-end configs: widget, functions, etc.

Parameters:
	
Usage: External

PVSS manager usage: WCCOAui

Constraints:
	. PVSS version: 3.6 
	. operating system: WXP and Linux.
	. distributed system: no.
*/
unGenericObject_InitializeFrontEnd()
{
  dyn_string dsFrontEnd;
  int i, length, iRes, j, length2;
  dyn_string dsFunctions, dsConfigLine, dsUnicosObjects, dsFaceplateTrendList, dsTrendDPE, dsResult, dsWidgets, dsProxy;
  bool bSelect;
  string faceplatePath, buttonPath, sFaceplateTrendList, sTrendDPE, sTrendFaceplate;
  int iWidget, lenWidget;
  dyn_string exceptionInfo;
          
  unGenericDpFunctions_getFrontEndDeviceType(dsFrontEnd);
  length = dynlen(dsFrontEnd);
  
  for(i=1;i<=length;i++) {
    dynClear(dsFaceplateTrendList);
    dynClear(dsWidgets);
    dynClear(dsTrendDPE);
    sTrendFaceplate = buttonPath = faceplatePath = "";
    bSelect = false;
    dynClear(dsFunctions);
    dpGet(getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.configHMI.Functions", dsFunctions, 
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.unWidgets.List", dsResult,
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.configHMI.Select", bSelect,
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.configHMI.FaceplatePath", faceplatePath,
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.configHMI.ButtonPath", buttonPath,
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.trendingDpSelector.FaceplateTrendList", dsFaceplateTrendList,
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.trendingDpSelector.DpeList", dsTrendDPE,
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.configHMI.TrendFaceplatePanel", sTrendFaceplate,
          getSystemName() + dsFrontEnd[i] + UN_OBJECT_EXTENSION + ".deviceConfiguration.proxy", dsProxy);
    faceplatePath = "vision/graphicalFrame/Faceplate"+dsFrontEnd[i]+".pnl";
    dsResult = makeDynString("objects/FRONT_END/Widget_"+dsFrontEnd[i]+".pnl");
    while (dynlen(dsFunctions) < UN_GENERICOBJECT_FUNCTION_LENGTH)
      dynAppend(dsFunctions, "");
    
//UN_GENERICOBJECT_FUNCTION_ACKALARM;
    dsFunctions[UN_GENERICOBJECT_FUNCTION_FACEPLATESTATUSREGISTER]=dsFrontEnd[i]+"_FaceplateRegisterCB";
    dsFunctions[UN_GENERICOBJECT_FUNCTION_FACEPLATESTATUSDISCONNECT]=dsFrontEnd[i]+"_FaceplateDisconnection";
//UN_GENERICOBJECT_FUNCTION_BUTTONREGISTER;
//UN_GENERICOBJECT_FUNCTION_BUTTONSETSTATE;
//UN_GENERICOBJECT_FUNCTION_BUTTONDISCONNECT;
//UN_GENERICOBJECT_FUNCTION_BUTTONUSERACCESS;
    dsFunctions[UN_GENERICOBJECT_FUNCTION_WIDGETREGISTER]=dsFrontEnd[i]+"_WidgetRegisterCB";
    dsFunctions[UN_GENERICOBJECT_FUNCTION_WIDGETDISCONNECT]=dsFrontEnd[i]+"_WidgetDisconnection";
    dsFunctions[UN_GENERICOBJECT_FUNCTION_MENUCONFIG]=dsFrontEnd[i]+"_MenuConfiguration";
//UN_GENERICOBJECT_FUNCTION_GENERATIONCHECK;
//UN_GENERICOBJECT_FUNCTION_GENERATIONIMPORT;
//UN_GENERICOBJECT_FUNCTION_USERCBGETBUTTONSTATE;
    dsFunctions[UN_GENERICOBJECT_FUNCTION_HANDLEMENUANSWER]=dsFrontEnd[i]+"_HandleMenu";
    dsFunctions[UN_GENERICOBJECT_FUNCTION_OBJECTLIST]=dsFrontEnd[i]+"_ObjectListGetValueTime";
    
    dsConfigLine = dsFunctions;
    dynAppend(dsConfigLine, makeDynString(bSelect, faceplatePath, buttonPath));
    sFaceplateTrendList = "";
    length2 = dynlen(dsFaceplateTrendList);
    for(j=1;j<=length2;j++)
      sFaceplateTrendList = sFaceplateTrendList + UN_FACEPLATE_TRENDS_DELIMITER + dsFaceplateTrendList[j];
    sFaceplateTrendList = substr(sFaceplateTrendList, strlen(UN_FACEPLATE_TRENDS_DELIMITER));
    dynAppend(dsConfigLine, sFaceplateTrendList);

    sTrendDPE = "";
    length2 = dynlen(dsTrendDPE);
    for(j=1;j<=length2;j++)
      sTrendDPE = sTrendDPE + UN_FACEPLATE_TRENDS_DELIMITER + dsTrendDPE[j];
    sTrendDPE = substr(sTrendDPE, strlen(UN_FACEPLATE_TRENDS_DELIMITER));
    dynAppend(dsConfigLine, sTrendDPE);
			
    if(sTrendFaceplate == "")
      sTrendFaceplate = UN_DEFAULT_TRENDFACEPLATEPANEL;
				
    dynAppend(dsConfigLine, sTrendFaceplate);
			
    dynClear(dsWidgets);
    lenWidget=dynlen(dsResult);
    for(iWidget=1;iWidget<=lenWidget;iWidget++)
    {
      if(getPath(PANELS_REL_PATH, dsResult[iWidget]) != "")
        dynAppend(dsWidgets, dsResult[iWidget]);
    }
        
    g_mFrontEndConfigs[dsFrontEnd[i]] = dsConfigLine;
    g_mFrontEndWidgetList[dsFrontEnd[i]] = dsWidgets;
    g_mFrontEndProxyList[dsFrontEnd[i]] = dsProxy;
    g_mFrontEndTrendConfig[dsFrontEnd[i]] = unGenericObject_InitializeTrendConfig(dsFrontEnd[i], exceptionInfo);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

//unGenericObject_getFrontState
/**
Purpose: get the front-end state for snapshot & widget.

Parameters:
	
Usage: External

PVSS manager usage: WCCOAui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: no.
*/
unGenericObject_getFrontState(dyn_string dsText, dyn_string dsColor, dyn_int diValue, 
           int iFESystemIntegrityAlarmValue, bool bFESystemIntegrityAlarmEnabled,
           string &sText, string &sBodyColor, string &sWarningLetter, string &sWarningColor, string &sCompleteText)
{
  int iMax = dynMax(diValue), iPos;
  int i, len;
  
  sBodyColor="white";
  sText = "???";
  sCompleteText = "";
  if((!bFESystemIntegrityAlarmEnabled) || (iFESystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value))
  {
    sWarningLetter = UN_WIDGET_TEXT_OLD_DATA;
    sWarningColor = "unDataNotValid";
  }
  
  if(!bFESystemIntegrityAlarmEnabled) {
    sBodyColor = "unAlarmMasked";
    sText = "DISABLED";
    sCompleteText = "DISABLED";
  }
  else if(iMax == 0) {
    sBodyColor = "green";
    sText = "OK";
    sCompleteText = "OK";
  }
  else {
    iPos = dynContains(diValue, iMax);
    if(iPos >0) {
      sBodyColor = dsColor[iPos];
      sText = dsText[iPos];
    }
  }
  len=dynlen(dsText);
  for(i=1;i<=len;i++)
  {
    if(diValue[i] !=0)
    {
      if(sCompleteText != "")
        sCompleteText += "|"+dsText[i];
      else
        sCompleteText = dsText[i];
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

//unGenericObject_getFrontEndColorState
/**
Purpose: get the front-end state color for each Front-end dpe.

Parameters:
	
Usage: External

PVSS manager usage: WCCOAui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: no.
*/
unGenericObject_getFrontEndColorState(dyn_string dsDpe, dyn_string &dsColor)
{
  int i, len, iType;
  bool bConfigExists, bIsActive;
  int iAlertConfigType;
  dyn_string dsAlertTexts, dsAlertClasses, dsSummaryDpeList, dsAlertPanelParameters, exceptionInfo;
  dyn_float dfAlertLimits;
  string sAlertPanel, sAlertHelp;
  string sColor, sAlertColor;
  int iLevel;
  int iClass, lenClass, iPrio, iAlertPrio;
  string sSystemName;
  
  len = dynlen(dsDpe);
  for(i=1;i<=len;i++)
  {
    sColor = "unDataNoAccess";
    iPrio = -1;
    sSystemName = unGenericDpFunctions_getSystemName(dsDpe[i]);
    fwAlertConfig_get(dsDpe[i], bConfigExists, iAlertConfigType, dsAlertTexts, dfAlertLimits, dsAlertClasses, dsSummaryDpeList,
                      sAlertPanel, dsAlertPanelParameters, sAlertHelp, bIsActive, exceptionInfo);
    if((dynlen(exceptionInfo) <= 0) & bConfigExists) {
      lenClass = dynlen(dsAlertClasses);
      for(iClass=1;iClass<=lenClass;iClass++)
      {
        if(dsAlertClasses[iClass] != "") {
          dpGet(dsAlertClasses[iClass]+":_alert_class.._type", iType);
          if(iType ==  DPCONFIG_ALERT_CLASS)
          {
            dpGet(dsAlertClasses[iClass]+":_alert_class.._prior", iAlertPrio, 
                  dsAlertClasses[iClass]+":_alert_class.._color_c_ack", sAlertColor);
            if(iAlertPrio > iPrio) {
              if(sAlertColor == "")
                sAlertColor = "green";
              sColor = sAlertColor;
              iPrio = iAlertPrio;
            }
          }
        }
      }
    }
    dsColor[i] = sColor;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_Acknowledge
/**
Purpose: acknowledge current device alarms

Parameters:
	- sDpName, string, input, datapoint
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_Acknowledge(string sDpName, dyn_string &exceptionInfo)
{
	string sDpType, deviceName, alias, sFunction;
	dyn_string exceptionInfo1, exceptionInfo2, exceptionInfo3, dsNeedAck, exceptionInfoTemp, dsFunctions;
	bool alreadySent, bPLCAck;
	int i, length, iAlertType;
	
	sDpType = dpTypeName(sDpName);
	deviceName = unGenericDpFunctions_getDpName(sDpName);
	alias = unGenericDpFunctions_getAlias(deviceName);
	unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo3);
	if (dynlen(exceptionInfo3) <= 0)
		{
		sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_ACKALARM];
// 1. Get DPEs to be acknowledged
		if (sFunction != "")
			{
			evalScript(dsNeedAck,"dyn_string main(string device, string sType) {" + 
								 "dyn_string dsNeedAckTemp;" +
								 "if (isFunctionDefined(\"" + sFunction + "\"))" +
								 "    {" +
								 "    " + sFunction + "(device, sType, dsNeedAckTemp);" +
								 "    }" +
								 "else " +
								 "    {" +
								 "    dsNeedAckTemp = makeDynString();" +
								 "    }" +
								 "return dsNeedAckTemp; }", makeDynString(), sDpName, sDpType);
			}
		dynUnique(dsNeedAck);
		bPLCAck = (dynContains(dsNeedAck, UN_ACKNOWLEDGE_PLC) > 0);
		if (bPLCAck)
			{
			dynRemove(dsNeedAck, dynContains(dsNeedAck, UN_ACKNOWLEDGE_PLC));
			}
// 2. Ack PVSS alarms
		length = dynlen(dsNeedAck);
		for(i=1;i<=length;i++)
			{
			iAlertType = DPCONFIG_NONE;
			dpGet(deviceName + "." + dsNeedAck[i] + ":_alert_hdl.._type", iAlertType);
			if (iAlertType != DPCONFIG_NONE)
				{
				unAlarmConfig_acknowledge(deviceName + "." + dsNeedAck[i], exceptionInfo1);
				}
			}
// 3. Ack PLC if necessary
		if (bPLCAck)
			{
			unGenericObject_SendManRegPulse(deviceName, UN_MANREG01_MALACK, exceptionInfo2, alreadySent);
			}
		}
// 4. Display result
	if (dynlen(exceptionInfo1) > 0)
		{
		dynAppend(exceptionInfoTemp, exceptionInfo1);
		unSendMessage_toExpertException("Acknowledge", exceptionInfo1);
		}
	if ((dynlen(exceptionInfo2) > 0) && (!alreadySent))
		{
		dynAppend(exceptionInfoTemp, exceptionInfo2);
		unSendMessage_toExpertException("Acknowledge", exceptionInfo2);
		}
	if (dynlen(exceptionInfo3) > 0)
		{
		dynAppend(exceptionInfoTemp, exceptionInfo3);
		unSendMessage_toExpertException("Acknowledge", exceptionInfo3);
		}
	if (dynlen(exceptionInfoTemp) > 0)
		{
		dynAppend(exceptionInfo, exceptionInfoTemp);
		dynClear(exceptionInfoTemp);
		unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
								 getCatStr("unOperator", "ACKFAILED"), exceptionInfoTemp);
		if (dynlen(exceptionInfoTemp) > 0)
			{
			fwExceptionHandling_display(exceptionInfoTemp);
			}
		}
	else
		{
		unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
								 getCatStr("unOperator", "ACKOK"), exceptionInfoTemp);
		if (dynlen(exceptionInfoTemp) > 0)
			{
			fwExceptionHandling_display(exceptionInfoTemp);
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_NeedSelect
/**
Purpose: return true if object could be selected

Parameters:
	- sDpType, string, input, dpType
	- bRes, bool, output, result
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_NeedSelect(string sDpType, bool &bRes, dyn_string &exceptionInfo)
{
	dyn_string exceptionInfoTemp;
	bool bSelect = false;
	int iObject;
	
// 1. Initialize data if not already done
	bRes = false;
	if (globalExists("g_dsObjectList"))
		{
		if (dynlen(g_dsObjectList) <= 0)
			{
			unGenericObject_InitializeObjects(exceptionInfoTemp);
			}
		}
	else
		{
		unGenericObject_InitializeObjects(exceptionInfoTemp);
		}
// 2. Get object
	iObject = dynContains(g_dsObjectList, sDpType);
	if (iObject <= 0)
		{
		fwException_raise(exceptionInfo, "ERROR", "unGenericObject_NeedSelect: unknown object", "");
		}
	else
		{
// 3. Get select field
		bSelect = (bool)g_ddsObjectConfigs[iObject][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_SELECT];
		}
	bRes = bSelect;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_Connection
/**
Purpose: this function checks the connection status and return a value for dp(dis)Connect

Parameters:
	- sDpName, string, input, datapoint
	- bDpConnectionState, bool, input, dpConnect already done ?
	- bSystemConnected, bool, input, is system connected ?
	- iAction, int, output, action to be done : UN_ACTION_DPCONNECT		-> a dpConnect must be done
												UN_ACTION_DISCONNECT	-> a disconnect animation must be done
												UN_ACTION_DPDISCONNECT_DISCONNECT ->  a dpDisconnect & disconnect animation must be done
												UN_ACTION_NOTHING		-> no action
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_Connection(string sDpName, bool bDpConnectionState, bool bSystemConnected, int &iAction, dyn_string &exceptionInfo)
{
	bool isRemoteSystem = false, localConnected;
	string deviceSystemName, deviceName;
	
	deviceName = unGenericDpFunctions_getDpName(sDpName);
	deviceSystemName = unGenericDpFunctions_getSystemName(sDpName);
	// 1. Check if device is located in a remote system
	localConnected = bSystemConnected;
	unDistributedControl_isRemote(isRemoteSystem, deviceSystemName);
	if(!isRemoteSystem)
		{
		localConnected = true;
		}
	iAction = UN_ACTION_NOTHING;
	// 2. The system is not connected
	if(!localConnected)
		{
		iAction = UN_ACTION_DISCONNECT;
		if (bDpConnectionState)
			{
			iAction = UN_ACTION_DPDISCONNECT_DISCONNECT;
			}
		}
	// 3. The system is connected
	else
		{
		if ((dpExists(deviceName) == false) || (deviceName == deviceSystemName))
			{
			iAction = UN_ACTION_DISCONNECT;
			}
		else
			{
			if(!bDpConnectionState)
				{
				iAction = UN_ACTION_DPCONNECT;
				}
			}
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_SendManRegPulse
/**
Purpose: send a pulse on a specified bit of ManReg01  

Parameters:
	sDpName: string, input, object name
	bitPosition: string, input, the pulse position in ManReg (see Constants)
	exceptionInfo: dyn_string, output, details of any exceptions are returned here
	alreadySent: bool, output, set to true if ManReg bit is already equal to 1 at the beginning of the function

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

unGenericObject_SendManRegPulse(string sDpName, unsigned bitPosition, dyn_string &exceptionInfo, bool &alreadySent)
{
	unGenericObject_SendPulse(sDpName, ".ProcessOutput.ManReg01",  bitPosition, exceptionInfo, alreadySent);
}

//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_SendBinaryOrder
/**
Purpose: send a binary order to the PLC. Example : auto mode request etc.

Parameters:	
	- sDpName, string, input, device name
	- iManRegBit, int, input, bit position in ManReg01
	- sKey, string, input, catalogue key
	- exceptionInfo, dyn_string, output, for errors

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. In catalogue "unOperator.cat", keys <sKey + "FAILED"> and <sKey + "OK"> must exist
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_SendBinaryOrder(string sDpName, int iManRegBit, string sKey, dyn_string &exceptionInfo)
{
	dyn_string exceptionInfoTemp;
	string alias, deviceName;
	bool alreadySent, bRes;
	
	deviceName = unGenericDpFunctions_getDpName(sDpName);
// 1. Select device again
	unGenericObject_NeedSelect(dpTypeName(deviceName), bRes, exceptionInfo);
	if (bRes)
		{
		unSelectDeselectHMI_select(deviceName, true, exceptionInfo);
		dynClear(exceptionInfo);
		}
// 2. Send pulse
	unGenericObject_SendManRegPulse(deviceName, iManRegBit, exceptionInfo, alreadySent);
// 3. Display result
	if (!alreadySent)
		{
		alias = unGenericDpFunctions_getAlias(deviceName);
		if(dynlen(exceptionInfo) > 0)
			{
			unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
									 getCatStr("unOperator", sKey + "FAILED"), exceptionInfoTemp);
			if (dynlen(exceptionInfoTemp) > 0)
				{
				fwExceptionHandling_display(exceptionInfoTemp);
				}
			unSendMessage_toExpertException(this.name(), exceptionInfo);
			}
		else
			{
			unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
									 getCatStr("unOperator", sKey + "OK"), exceptionInfoTemp);
			if (dynlen(exceptionInfoTemp) > 0)
				{
				fwExceptionHandling_display(exceptionInfoTemp);
				}
			}
		}
}

//------------------------------------------------------------------------------------------------------------------------

int unGenericObject_GetPulseDuration()
{
    int iRes, pulseLength;
    string dpPulse;

    // If global variable that saves the pulse length is not initialized, get value
    if (g_unGraphicalFrame_manRegTimeOut <= 0) {
        dpPulse = getSystemName() + UN_APPLICATION_DPNAME + ".manRegTimeOut";
        if (dpExists(dpPulse)) {
            iRes = dpGet(dpPulse, pulseLength);
            if (iRes < 0) {          // dpGet failed -> use constant
                pulseLength = UN_FACEPLATE_PULSE_DELAY;
            } else {
                g_unGraphicalFrame_manRegTimeOut = (unsigned)pulseLength;
            }
        } else {
            pulseLength = UN_FACEPLATE_PULSE_DELAY;
        }
    } else {
        pulseLength = g_unGraphicalFrame_manRegTimeOut;
    }
    return pulseLength;
}
 
// unGenericObject_WaitPulse
/**
Purpose: send ManReg pulse delay

Parameters:	None
	
Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. Use global variable defined in the graphical frame library g_unGraphicalFrame_manRegTimeOut
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WaitPulse()
{
	// Delay
	delay(unGenericObject_GetPulseDuration());
}

unGenericObject_PulseAdaptiveWait(string deviceName, string dpeName = ".ProcessOutput.ManReg01")
{
    int pulseLength = unGenericObject_GetPulseDuration();

    int startTime = period(getCurrentTime());
    while (unGenericObject_isInPulse(deviceName, dpeName)) {
        delay(0, 250);
        if (pulseLength <= period(getCurrentTime()) - startTime) {
            dpSet(deviceName + dpeName, 0);
            return;
        }
    }
}

bool unGenericObject_isInPulse(string deviceName, string dpeName = ".ProcessOutput.ManReg01") {
    if(!dpExists(deviceName + dpeName)) return false;

    int manRegValue;
    if (dpGet(deviceName + dpeName, manRegValue)  == -1) { // dpe is hard-coded, the fix works for CPC.
        DebugTN("unGenericObject_isInPulse: error when dpGet");
        return false;
    } else {
        return manRegValue != 0;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_FaceplateStatusInit
/**
Purpose: Initialization of Unicos faceplate

Parameters:
	- sDpName, string, input, device name
	- sDpType, string, input, unicos object
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_FaceplateStatusInit(string sDpName, string sDpType, dyn_string &exceptionInfo)
{
	string sSystemName, alias, deviceName, sFunction;
	bool bRes, bConnected;
	dyn_string exceptionInfoTemp, dsFunctions;
	
	deviceName = unGenericDpFunctions_getDpName(sDpName);
	sSystemName = unGenericDpFunctions_getSystemName(sDpName);
	unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
	sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_FACEPLATESTATUSREGISTER];
// 1. Get DPEs to be acknowledged
	unDistributedControl_register(sFunction, bRes, bConnected, sSystemName, exceptionInfo);
	if (!bRes)
		{
		execScript("main() {" + dsFunctions[UN_GENERICOBJECT_FUNCTION_FACEPLATESTATUSDISCONNECT] + "();}", makeDynString("$sDpName:"+sDpName));
		}
	if (dynlen(exceptionInfo) > 0)
		{
		alias = unGenericDpFunctions_getAlias(deviceName);
		if (alias == "")
			{
			alias = sDpType;
			}
		unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
								 getCatStr("unFunctions","STATUSFACEPLATEINITFAILED"), exceptionInfoTemp);
		if (dynlen(exceptionInfoTemp) > 0)
			{
			fwExceptionHandling_display(exceptionInfoTemp);
			}
		unSendMessage_toExpertException("unGenericObject_FaceplateStatusInit", exceptionInfo);
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ColorBoxAnimate
/**
Purpose: animate "non alarm" colorBoxes

Parameters:
	- sShape, string, input, colorbox shape name
	- iCommandBit, int, input, command bit 
	- iEnablebit, int, input, enable bit
	- sOneBackCol, string, input, backcolor if command bit = 1
	- bInvalid, bool, input, invalid data ?
	- sZeroBackCol, string, input, backcolor if command bit = 0, default value = "unFaceplate_Unactive"
	
Color : - if data is invalid, backcolor = "unDataNotValid"
		- else if enable bit = false, backcolor = "unFaceplate_Disabled"
		-	   else if command bit = true, backcolor = sBackCol
		- 			else backcolor = "unFaceplate_Unactive"

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_ColorBoxAnimate(string sShape, int iCommandbit, int iEnableBit, string sOneBackCol, bool bInvalid, string sZeroBackCol="unFaceplate_Unactive")
{
	string sBackcolor;
	bool bEnabled = true;
	
	if (bInvalid)
		{
		sBackcolor = "unDataNotValid";
		}
	else
		{
		if (iEnableBit == 0)
			{
			bEnabled = false;
			sBackcolor = "unFaceplate_Disabled";
			}
		else
			{
			sBackcolor = (iCommandbit == 1) ? sOneBackCol : sZeroBackCol;
			}		
		}
	setValue(sShape + ".colorbox", "backCol", sBackcolor);
	if (shapeExists(sShape + "Text"))
		{
		setValue(sShape + "Text", "enabled", bEnabled);
		}
}


//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ColorBoxAnimateAlarm

/**
Purpose: animate alarm colorBoxes

Parameters:
	- sShape, string, input, colorbox shape name
	- iAlarmState, int, input, act state alarm
	- bBoolAlert, bool, input, is alert hdl defined ?
	- bInvalid, bool, input, invalid data ?
	
Color : - if alert hdl does not exist, backcolor = "unAlarmDoesNotExist"
		- else if bInvalid = true, backcolor = "unDataNotValid"
		-	   else if alarm unack., backcolor = "unFaceplate_AlarmNotAck"
		-			else if alarm active and ack., backcolor = "unFaceplate_AlarmActive"
		-				 else backcolor = "unFaceplate_Unactive"
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{FalsePositive}

*/

unGenericObject_ColorBoxAnimateAlarm(string sShape, int iAlarmState, bool bBoolAlert, bool bInvalid)
{
	string sColor;
	
	if (bBoolAlert)
		{
		switch (iAlarmState)
			{
			case DPATTR_ALERTSTATE_NONE:
				sColor = "unFaceplate_Unactive";
				break;
			case DPATTR_ALERTSTATE_APP_ACK:
				sColor = "unFaceplate_AlarmActive";
				break;
			case DPATTR_ALERTSTATE_APP_NOT_ACK:
			case DPATTR_ALERTSTATE_DISAPP_NOT_ACK:
			case DPATTR_ALERTSTATE_APP_DISAPP_NOT_ACK:
				sColor = "unFaceplate_AlarmNotAck";
				break;
			default:
				sColor = "unFaceplate_Unactive";
				break;
			}
		if (bInvalid)
			{
			sColor = "unDataNotValid";
			}
		}
	else
		{
		sColor = "unAlarmDoesNotExist";
		}
	setValue(sShape + ".colorbox", "backCol", sColor);
	if (shapeExists(sShape + "Text"))
		{
		setValue(sShape + "Text", "enabled", true);
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ColorBoxDisconnect
/**
Purpose: Disconnection of colorBoxes

Parameters:
	- dsShapes, dyn_string, input, colorboxes shapes
						  
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_ColorBoxDisconnect(dyn_string dsShapes)
{
	int i, length;
	
	length = dynlen(dsShapes);
	for(i=1;i<=length;i++)
		{
		setValue(dsShapes[i] + ".colorbox", "backCol", "unDataNoAccess");
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ColoredSquareAnimate
/**
Purpose: Set the animation of ColoredSquare

Parameters:
	- sShapes, string, input, ColoredSquare shapes
	- bValue, bool, input, the value
	- bOkValue, bool, input, the value for the case OK
	- sOkColor, string, input, the color for the OK case
	- sBadColor, string, input, the color for the BAD case
	- bInvalid, bool, input, true=data invalid, false=data not invalid

Color : 		- if data is invalid, backcolor = "unDataNotValid"
				- else if bValue == bOkValue, backcolor = sOkColor
				- else backcolor = sBadColor
Square filling:	if bValue == 0, Square empty
				else, Square full
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP.
	. distributed system: yes.
*/
unGenericObject_ColoredSquareAnimate(string sShape, bool bValue, bool bOkValue, string sOkColor, string sBadColor, bool bInvalid)
{
	string sBackcolor;

	if (bInvalid)
		sBackcolor = "unDataNotValid";
	else
		sBackcolor = (bValue == bOkValue) ? sOkColor : sBadColor;
		
 if (shapeExists(sShape + ".border"))
	  setMultiValue(sShape + ".border", "backCol",  sBackcolor, sShape + ".body", "visible", !bValue);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ColoredSquareToolTip
/**
Purpose: Set the toolTipText of ColoredSquare

Parameters:
	- sShapes, string, input, ColoredSquare shapes
	- sText, string, input, the tool tip text
						  
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP.
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{FalsePositive}

*/

unGenericObject_ColoredSquareToolTip(string sShape, string sText)
{
	setMultiValue(sShape + ".border", "toolTipText", sText, sShape + ".body", "toolTipText", sText);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ColoredSquareDisconnect
/**
Purpose: Disconnection of ColoredSquare

Parameters:
	- dsShapes, dyn_string, input, ColoredSquare shapes
						  
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: WXP.
	. distributed system: yes.
*/

unGenericObject_ColoredSquareDisconnect(dyn_string dsShapes)
{
	int i, length;
	
	length = dynlen(dsShapes);
	for(i=1;i<=length;i++)
		{
		setValue(dsShapes[i] + ".border", "backCol", "unDataNoAccess");
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_DisplayValue
/**
Purpose: animate a display value

Parameters:
	- sFormat, string, input, the format to use
	- sUnit, string, input, the unit to use
	- fValue, float, input, value to format
	- sShape, string, input, shape that displays the value
	- sForeCol, string, input, display color
	- bInvalid, bool, input, is value invalid ?
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_DisplayValue(string sFormat, string sUnit, float fValue, string sShape, string sForeCol, bool bInvalid)
{
	string formattedValue, sColor;

	formattedValue = unGenericObject_FormatValue(sFormat, fValue);
	sColor = sForeCol;
	if (bInvalid)
		{
		sColor = "unDataNotValid";
		}
	setMultiValue(sShape + ".display", "foreCol", sColor, sShape + ".display", "text", strrtrim(formattedValue + " " + sUnit));
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_DisplayRange
/**
Purpose: animate a display range

Parameters:
	- iRangeType, int, input, range type
	- sFormat, string, input, the format to use
	- sUnit, string, input, the unit to use
	- fValueMin, float, input, min value to format
	- fValueMax, float, input, max value to format
	- sShapeMin, string, input, min shape that displays the min value
	- sShapeMax, string, input, max shape that displays the max value
	- sForeCol, string, input, display color
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_DisplayRange(int iRangeType, string sFormat, string sUnit, float fValueMin, float fValueMax, 
						     string sShapeMin, string sShapeMax, string sForeCol)
{
	if (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)
		{
		unGenericObject_DisplayValue(sFormat, sUnit, fValueMin, sShapeMin, sForeCol, false);
		unGenericObject_DisplayValue(sFormat, sUnit, fValueMax, sShapeMax, sForeCol, false);
		if (shapeExists(sShapeMin + "Text"))
			{
			setValue(sShapeMin + "Text", "enabled", true);
			}
		if (shapeExists(sShapeMax + "Text"))
			{
			setValue(sShapeMax + "Text", "enabled", true);
			}
		}
	else
		{
		setMultiValue(sShapeMin + ".display", "foreCol", "unRangeDoesNotExist", sShapeMax + ".display", "foreCol", "unRangeDoesNotExist",
					  sShapeMin + ".display", "text", "???", sShapeMax + ".display", "text", "???");
		if (shapeExists(sShapeMin + "Text"))
			{
			setValue(sShapeMin + "Text", "enabled", false);
			}
		if (shapeExists(sShapeMax + "Text"))
			{
			setValue(sShapeMax + "Text", "enabled", false);
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_FormatValue
/**
Purpose: format value

Parameters:
	- sValueFormat, string, input, format to use
	- fValue, float, input, value to format
Return formatted string.

Note:
Using "20" as decimal value has a special feature: "show only the first 4 digits (including the dot).
Example: "%4.20f"
unGenericObject_FormatValue("%4.20f", 12.1234567895555); returns 12.1
unGenericObject_FormatValue("%4.9f", 12.1234567895555); returns 12.123456789

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

string unGenericObject_FormatValue(string sValueFormat, float fValue)
{
	string sVal, sExp, sFormat=sValueFormat;
	int pos, iExp;
	float fTemp;
	
	// empty format->use default
	if (sFormat == "")
		{
		sFormat = UN_FACEPLATE_DEFAULT_FORMAT;
		}
// convert the value in string
	sprintf(sVal, sFormat, fValue);
	// check if fixed display format
	pos = strpos(sFormat, UN_DISPLAY_VALUE_DIGIT_FLOAT_FORMAT+UN_DISPLAY_VALUE_DIGIT_FLOAT_TYPE);
	if(pos>0) { // fixed display format
// check the position of '.' --> if < iExp-1 --> Ok else UN_DISPLAY_VALUE_DIGIT_TOO_BIG_VALUE
// if OK substr iExp char

		sExp = substr(sFormat, 0, pos);
		iExp = (int)substr(sExp, 1, strlen(sExp));
		pos = strpos(sVal, ".");
		if(pos < iExp) {
			sFormat = "%"+ (iExp -1)+"."+(iExp -1-pos)+UN_DISPLAY_VALUE_DIGIT_FLOAT_TYPE;
			sprintf(sVal, sFormat, fValue);
		}
		else
			sVal = UN_DISPLAY_VALUE_DIGIT_TOO_BIG_VALUE;
	}
	pos = strpos(sVal, "e");
	if(pos >= 1) {
		sExp = substr(sVal, pos+1, strlen(sVal));
		iExp = (int)sExp;
		if (iExp != 0)
			{
			//error fixed 25/08/2005
			//strreplace(sExp, "0", "");
			sExp = " e" + iExp;
			}
		else
			{
			sExp = "";
			}
		sVal = substr(sVal, 0, pos) + sExp;
	}
//DebugN(sValueFormat, sFormat, fValue, sVal);
	return strrtrim(strltrim(sVal));
}

//---------------------------------------------------------------------------------------------------------------------------------------

// 	unGenericObject_StringToFloat
/**
Purpose: to convert a string in a float value

Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_StringToFloat(string sInput, float &fOutput, bool &opOk)
{
	string sInputTemp = sInput;
	float fTemp;
	
	strreplace(sInputTemp, " ", "");
	if (sscanf(sInputTemp, "%f", fTemp) > 0)
		{
		opOk = true;
		fOutput = fTemp;
		}
	else
		{
		opOk = false;
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// 	unGenericObject_DisplayEventLostFocus
/**
Purpose: check text in a text shape and update display

Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_DisplayEventLostFocus(string sShape, string sShapeOld, string sDp, string sFormat)
{
	string sText, sFormattedValue;
	float fOutput;
	bool opOk;

	if (shapeExists(sShape))
		{
		getValue(sShape, "text", sText);
		unGenericObject_StringToFloat(sText, fOutput, opOk);
		if (opOk)
			{
			sFormattedValue = unGenericObject_FormatValue(sFormat, fOutput);
			strreplace(sFormattedValue, " ", "");
			setValue(sShape, "text", sFormattedValue);
			}
		else
			{
			if (shapeExists(sShapeOld))
				{
				getValue(sShapeOld, "text", sText);
				strreplace(sText, " ", "");
				}
			else
				{
				dpGet(sDp, fOutput);
				sText = unGenericObject_FormatValue(sFormat, fOutput);
				strreplace(sText, " ", "");
				}
			beep(400,250);
			setValue(sShape, "text", sText);
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_DisplayValueDisconnect
/**
Purpose: Disconnection of display values

Parameters:
	- dsShapes, dyn_string, input, display values shapes

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_DisplayValueDisconnect(dyn_string dsShapes)
{
	int i, length;
	
	length = dynlen(dsShapes);
	for(i=1;i<=length;i++)
		{
		setValue(dsShapes[i] + ".display", "foreCol", "unDataNoAccess");
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ButtonInit
/**
Purpose: initialize buttons

Parameters:
	- sDpName, string, input, device name
	- sDpType, string, input, unicos object
	- exceptionInfo, dyn_string, output, for errors
	
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

unGenericObject_ButtonInit(string sDpName, string sDpType, dyn_string &exceptionInfo)
{
	string deviceSystemName, alias, deviceName, sFunction;
	int iRes;
	bool bRes, bConnected;
	dyn_string exceptionInfoTemp, dsFunctions;

	g_dsUserAccess = makeDynString();
	deviceSystemName = unGenericDpFunctions_getSystemName(sDpName);
	deviceName = unGenericDpFunctions_getDpName(sDpName);
	
	unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
	sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_BUTTONREGISTER];
// 1. User callback function
	unGenericDpFunctionsHMI_setCallBack_user("unGenericObject_ButtonUserCB",iRes, exceptionInfo);
// 2. For distributed systems
	unDistributedControl_register(sFunction, bRes, bConnected, deviceSystemName, exceptionInfo);
// 3. Check for errors
	if (dynlen(exceptionInfo) > 0)
		{
		alias = unGenericDpFunctions_getAlias(deviceName);
		if (alias == "")
			{
			alias = sDpType;
			}
		unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
								 getCatStr("unFunctions","BUTTONPANELINITFAILED"), exceptionInfoTemp);
		if (dynlen(exceptionInfoTemp) >0)
			{
			fwExceptionHandling_display(exceptionInfoTemp);
			}
		unSendMessage_toExpertException("unGenericObject_ButtonInit", exceptionInfo);
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ButtonUserCB
/**
Purpose: user callback for buttons

Parameters:
	sDp: string, input, current user name DP name
	sUser: string, input, current user name

Usage: Internal function

PVSS manager usage:  Ctrl, NG, NV

Constraints:
	. This library works with an unicos button panel and need global variables declared in this panel
		-> g_dsUserAccess, dyn_string, button identifier is added to this variable if user access 
		-> $sDpName, string, the device DP name
		-> g_sFaceplateButtonType, string, faceplate type
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

unGenericObject_ButtonUserCB(string sDp, string sUser)
{
	string deviceName, sFunction, systemName;
	bool bConnected;
	dyn_string exceptionInfo, dsFunctions, dsReturnData;
	
// 1. Update user access
	deviceName =  unGenericDpFunctions_getDpName($sDpName);
	systemName = unGenericDpFunctions_getSystemName($sDpName);
	unDistributedControl_isConnected(bConnected, systemName);
	unGenericObject_GetFunctions(g_sFaceplateButtonType, dsFunctions, exceptionInfo);
	if (bConnected)
	{
// 1.5 get the allowed buttons
		unGenericButtonFunctionsHMI_isAccessAllowedMultiple(deviceName, g_sFaceplateButtonType, g_dsUserAccess, exceptionInfo);

// 2. Get parameters to call the set button function
		sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_USERCBGETBUTTONSTATE];
		if(sFunction != "") {
			evalScript(dsReturnData, "dyn_string main(string deviceName, string sType) {" + 
								 	 "dyn_string dsTemp;" +
									 "if (isFunctionDefined(\"" + sFunction + "\"))" +
									 "    {" +
									 "    " + sFunction + "(deviceName, sType, dsTemp);" +
									 "    }" +
									 "else " +
									 "    {" +
									 "    dsTemp = makeDynString(\"\", \"\", \"\", \"\", \"\", \"\");" +
									 "    }" +
									 "return dsTemp; }", makeDynString(), deviceName, g_sFaceplateButtonType);
		}
		
		sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_BUTTONSETSTATE];

// 3. call the set button function
		if (sFunction != "")
		{
			while(dynlen(dsReturnData) < 6)
				dynAppend(dsReturnData, "");

			execScript("main(string deviceName, string sDpType, dyn_string dsUserAccess, dyn_string dsReturnData) {" + 
					   "if (isFunctionDefined(\"" + sFunction + "\"))" +
					   "    {" +
					   "    " + sFunction + "(deviceName, sDpType, dsUserAccess, dsReturnData);" +
					   "    }}", makeDynString(), deviceName, g_sFaceplateButtonType, g_dsUserAccess, dsReturnData);
		}
	}
	else
	{
		g_dsUserAccess = makeDynString();
		dyn_string dsDollarParams = makeDynString();
		if(isDollarDefined("$sDpName"))
		{
			dynAppend(dsDollarParams,"$sDpName:"+getDollarValue("$sDpName"));
		}
		execScript("main() {" + dsFunctions[UN_GENERICOBJECT_FUNCTION_BUTTONDISCONNECT] + "();}", dsDollarParams);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_UserCBGetButtonState
/**
Purpose: function called before the button set state when a user logs in. Common function for all the UNICOS device types.

Parameters:
	deviceName: string, input, the device name
	dsReturnData: dyn_string, output, the data are returned here

Usage: Internal function

PVSS manager usage:  Ctrl, NG, NV

Constraints:
	. This library works with an unicos button panel and need global variables declared in this panel
		-> g_dsUserAccess, dyn_string, button identifier is added to this variable if user access 
		-> $sDpName, string, the device DP name
		-> g_sFaceplateButtonType, string, faceplate type
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.

@reviewed 2018-08-02 @whitelisted{FalsePositive}

*/

unGenericObject_UserCBGetButtonState(string deviceName, string sType, dyn_string &dsReturnData)
{
	int iRes;
	string sDpLock, sDpSelectedManager, sSelectedManager, sStsReg01, sStsReg01Bad;
	string sStsReg02, sStsReg02Bad;
	bool bLocked, stsReg01Bad, stsReg02Bad;
	bit32 stsReg01Value, stsReg02Value;

// 2. Get parameters to call the set button function
	sDpLock = deviceName + ".statusInformation.selectedManager:_lock._original._locked";
	sDpSelectedManager = deviceName + ".statusInformation.selectedManager";
	sStsReg01 = deviceName + ".ProcessInput.StsReg01";
	sStsReg01Bad = deviceName + ".ProcessInput.StsReg01:_online.._aut_inv";
	sStsReg02 = deviceName + ".ProcessInput.StsReg02";
	sStsReg02Bad = deviceName + ".ProcessInput.StsReg02:_online.._aut_inv";
	if (dpExists(deviceName))
	{
		iRes = dpGet(sDpLock, bLocked, sDpSelectedManager, sSelectedManager, sStsReg01, stsReg01Value, sStsReg01Bad, stsReg01Bad);
		if (dpExists(sStsReg02))
		{
			iRes = iRes + dpGet(sStsReg02, stsReg02Value, sStsReg02Bad, stsReg02Bad);
		}
		else
		{
			sStsReg02 = "";
			sStsReg02Bad = "";
			stsReg02Value = (bit32)0;
			stsReg02Bad = false;
		}
	}
	else
	{
		iRes = -1;
	}
	if (iRes < 0) // Errors during dpget
	{
		sDpLock = "";
		bLocked = false;
		sDpSelectedManager = "";
		sSelectedManager = "";
		sStsReg01 = "";
		stsReg01Value = (bit32)0;
		sStsReg01Bad = "";
		stsReg01Bad = false;
		sStsReg02 = "";
		stsReg02Value = (bit32)0;
		sStsReg02Bad = "";
		stsReg02Bad = false;
	}
	dsReturnData=makeDynString(bLocked, sSelectedManager, stsReg01Value, stsReg01Bad, stsReg02Value, stsReg02Bad);
//DebugN("unGenericObject_UserCBGetButtonState", deviceName, sType, dsReturnData);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ButtonAnimateSelect
/**
Purpose: animate button select

Parameters: 
	- sSelectState, string, input, select state
	- bAccessGranted, bool, input, user access

Usage: External

PVSS manager usage:  Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

unGenericObject_ButtonAnimateSelect(string sSelectState, bool bAccessGranted)
{
	string sText;
	bool bEnabled;
	
	sText = UN_FACEPLATE_BUTTON_SELECT_TEXTDESELECT;
	if (sSelectState == "D")
		{
		sText = UN_FACEPLATE_BUTTON_SELECT_TEXTSELECT;
		}
	bEnabled = false;
	if (bAccessGranted && (sSelectState != "SO"))
		{
		bEnabled = true;
		}
	setMultiValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_SELECT, "enabled", bEnabled,
				  UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_SELECT, "text", sText);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ButtonSelectAction
/**
Purpose: select / deselect device

Parameters: 
	- sDpName, string, input, device
	- bSelect, bool, input, true = select device, false = deselect device
	- exceptionInfo, dyn_string, output, for errors

Usage: External

PVSS manager usage:  Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

unGenericObject_ButtonSelectAction(string sDpName, bool bSelect, dyn_string &exceptionInfo)
{
	bool bRes;
	string deviceName, buttonPanel, sAlias, message;
    int moduleNumber;

	deviceName = unGenericDpFunctions_getDpName(sDpName);
	sAlias = unGenericDpFunctions_getAlias(deviceName);
	
  if (dpExists(deviceName))
  {
    unGenericObject_NeedSelect(dpTypeName(deviceName), bRes, exceptionInfo);
    if (bRes)
    {
      unSelectDeselectHMI_select(deviceName, bSelect, exceptionInfo);
      if (bSelect)
      {
        unGraphicalFrame_setSelectMessage(deviceName);
        if (strpos(myModuleName(), ";unicosGFContextPanel") < 0)
        {
          unGenericDpFunctions_getContextPanel(deviceName, buttonPanel, exceptionInfo);
          if(buttonPanel != "")
          {
		    if(globalExists("g_unHMI_RunsInNewHMI") && (strpos(myModuleName(), UNHMI_ZOOMED_MODULE_PREFIX) >= 0))
            {
              moduleNumber = substr(myModuleName(), strpos(myModuleName(), UNHMI_ZOOMED_MODULE_PREFIX) + strlen(UNHMI_ZOOMED_MODULE_PREFIX), 1);
              unGraphicalFrame_showContextualPanel(buttonPanel, deviceName, UNHMI_COMMAND_MODULE_NAME + moduleNumber);
            } else
            {
              unGraphicalFrame_showContextualPanel(buttonPanel, deviceName);              
            }
          }
        }
        message = "Select ";	
      }
      else message = "Deselect ";
      unSendMessage_toAllUsers(message + unSendMessage_getDeviceDescription(deviceName), exceptionInfo);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ButtonDisconnect
/**
Purpose: disconnection of buttons

Parameters: 
	- dsShapes, dyn_string, input, button shapes

Usage: External

PVSS manager usage:  Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

unGenericObject_ButtonDisconnect(dyn_string dsShapes)
{
	int i, length;
	
	length = dynlen(dsShapes);
	for(i=1;i<=length;i++)
		{
		setValue(UN_FACEPLATE_BUTTON_PREFIX + dsShapes[i], "enabled", false);
		}
	if (shapeExists(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_SELECT))
		{
		setValue(UN_FACEPLATE_BUTTON_PREFIX + UN_FACEPLATE_BUTTON_SELECT, "text", UN_FACEPLATE_BUTTON_SELECT_TEXTSELECT);
		}
}

//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_GetFaceplateTrendConfig
/**
Purpose: get current configuration of device faceplate trend tab

Parameters:	
	sDpName, string, input, device name
	sDpType, string, input, device type
	exceptionInfo, dyn_string, output, for errors
	
Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_GetFaceplateTrendConfig(string sDpType, dyn_string &dsAnalog, dyn_string &dsRange, dyn_string &dsColor, dyn_string &exceptionInfo)
{
	int i, length, iObject;
	string sFaceplateConfig;
	dyn_string dsTemp, exceptionInfoTemp, dsConfig;
	
	dynClear(dsAnalog);
	dynClear(dsRange);
	dynClear(dsColor);
	if (globalExists("g_dsObjectList"))
		{
		if (dynlen(g_dsObjectList) <= 0)
			{
			unGenericObject_InitializeObjects(exceptionInfoTemp);
			}
		}
	else
		{
		unGenericObject_InitializeObjects(exceptionInfoTemp);
		}
	iObject = dynContains(g_dsObjectList, sDpType);
	if (iObject <= 0)
	{
              if(mappingHasKey(g_mFrontEndConfigs, sDpType)) {
                dsConfig = g_mFrontEndConfigs[sDpType];
                sFaceplateConfig = dsConfig[UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_FACEPLATETREND];
              }
              else
		fwException_raise(exceptionInfo, "ERROR", "unGenericObject_GetFaceplateTrendConfig: unknown object", "");
	}
	else
		{
		sFaceplateConfig = g_ddsObjectConfigs[iObject][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_FACEPLATETREND];
		}
	dsTemp = strsplit(sFaceplateConfig, UN_FACEPLATE_TRENDS_DELIMITER);
	length = dynlen(dsTemp) / 3;
	for(i=1;i<=length;i++)
		{
		dynAppend(dsAnalog, dsTemp[3*i-2]);
		dynAppend(dsRange, dsTemp[3*i-1]);
		dynAppend(dsColor, dsTemp[3*i]);
		}
}


//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_getDeviceDpeTrendExtendedConfig
/**
Purpose: get extended current configuration of device faceplate trend tab.
  the extended configuration is the full configuration available for the fwTrending tool.

Parameters:	
	dpTypeSelected, string, input, device type
	sExtendedDataDpe, string, output, datapoint name of the extended configuration (dp type: FwTrendingPlot)
	exceptionInfo, dyn_string, output, for errors
	
Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.8.1 
	. operating system: tested only under WXP.
	. distributed system: yes.
*/

unGenericObject_getDeviceDpeTrendExtendedConfig(string dpTypeSelected, string dsDataSelected, string &sExtendedDataDpe, dyn_string &exceptionInfo)
{
  dyn_string exc;
  dyn_dyn_string plotData;
  dyn_string dsData, dsTimeSplit;
  dyn_string dsDpe, dsDpeRange, dsColor, dsMin, dsMax, dsVisibility;
  string sName, sTimeRange, sMin, sMax, sFwTimeRange;
  dyn_float dfMax, dfMin;
  dyn_bool dbYScaleVisible;  
  
	if(strlen(dsDataSelected) && strlen(dpTypeSelected))
  {
    sExtendedDataDpe="_template_un"+dpTypeSelected+"_"+dsDataSelected;
// DebugN("sExtendedDataDpe: "+sExtendedDataDpe);////////////
    if(!dpExists(sExtendedDataDpe))
    //the extended config dpe is not specified. look if the dpe exists and create reference to it. 
    //if it does not exist, create dpe and refer to it.
    {
//		   fwException_raise(exceptionInfo, "INFO", "unGenericObject_getDeviceDpeTrendExtendedConfig: creating trending configuration "+sExtendedDataDpe, ""); 
// DebugN("creating plot "+ sExtendedDataDpe);
       fwTrending_createPlot(sExtendedDataDpe,exc);
       if(dynlen(exc) <=0)
       {
          //set plot as template
          fwTrending_setPlotIsTemplate(sExtendedDataDpe,TRUE);  
          //if exist, get current trend unicos settings and set them to the fwTrending settings
          if(dpExists(dpTypeSelected+"_trendConfiguration"))
          {
            unGenericObject_getDeviceTrendConfig(dpTypeSelected, dsData, exc);
            unGenericObject_getDeviceDpeTrendConfig(dsData, sName, dsDpe, dsDpeRange, 
                                                      dsColor, dfMax, dfMin, 
                                                      dbYScaleVisible, sTimeRange, exc);
            dsTimeSplit = strsplit(sTimeRange,"."); 
            if(dynlen(dsTimeSplit)>=3)       
              fwTrending_getTimeRangeFromDaysHoursMinutes(sFwTimeRange,dsTimeSplit[1],dsTimeSplit[2],dsTimeSplit[3],exc);
            else
              sFwTimeRange = fwTrending_SECONDS_IN_ONE_HOUR;
            //set current trend unicos setting to the advanced trend settings
            fwTrending_getPlot(sExtendedDataDpe, plotData,exc);
            plotData[fwTrending_PLOT_OBJECT_DPES] = dsDpe;
            plotData[fwTrending_PLOT_OBJECT_COLORS] = dsColor;
            plotData[fwTrending_PLOT_OBJECT_RANGES_MIN] = dfMin;
            plotData[fwTrending_PLOT_OBJECT_RANGES_MAX] = dfMax;
            plotData[fwTrending_PLOT_OBJECT_AXII] = dbYScaleVisible;
            plotData[fwTrending_PLOT_OBJECT_MARKER_TYPE] = fwTrending_MARKER_TYPE_NONE;
            plotData[fwTrending_PLOT_OBJECT_TIME_RANGE] = sFwTimeRange;
            fwTrending_setPlot(sExtendedDataDpe, plotData,exc);
            delay(0,200);      
          } 
        }
      exceptionInfo = exc;
    }
  }
  else
  {
		fwException_raise(exceptionInfo, "ERROR", "unGenericObject_getDeviceDpeTrendExtendedConfig: no device type or no trend name found", "");
  }
}
//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_GetTrendDPEList
/**
Purpose: get the list of trendable DPE

Parameters:	
	sDpName, string, input, device name
	sDpType, string, input, device type
	exceptionInfo, dyn_string, output, for errors
	
Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_GetTrendDPEList(string sDpType, dyn_string &dsAnalog, dyn_string &dsRange, dyn_string &exceptionInfo)
{
	int i, length, iObject;
	string sTrendConfig;
	dyn_string dsTemp, exceptionInfoTemp, dsConfig;
	
	dynClear(dsAnalog);
	dynClear(dsRange);
	if (globalExists("g_dsObjectList"))
		{
		if (dynlen(g_dsObjectList) <= 0)
			{
			unGenericObject_InitializeObjects(exceptionInfoTemp);
			}
		}
	else
		{
		unGenericObject_InitializeObjects(exceptionInfoTemp);
		}
	iObject = dynContains(g_dsObjectList, sDpType);
	if (iObject <= 0)
	{
              if(mappingHasKey(g_mFrontEndConfigs, sDpType)) {
                dsConfig = g_mFrontEndConfigs[sDpType];
                sTrendConfig = dsConfig[UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_TRENDDPE];
              }
              else
		fwException_raise(exceptionInfo, "ERROR", "unGenericObject_GetTrendDPEList: unknown object", "");
	}
	else
		{
		sTrendConfig = g_ddsObjectConfigs[iObject][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_TRENDDPE];
		}
	dsTemp = strsplit(sTrendConfig, UN_FACEPLATE_TRENDS_DELIMITER);
	length = dynlen(dsTemp) / 2;
	for(i=1;i<=length;i++)
		{
		dynAppend(dsAnalog, dsTemp[2*i-1]);
		dynAppend(dsRange, dsTemp[2*i]);
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_GetTrendFaceplatePanel
/**
Purpose: get the panel of the trend to put in the faceplate

Parameters:	
	sDpName, string, input, device name
	sDpType, string, input, device type
	exceptionInfo, dyn_string, output, for errors
	
Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_GetTrendFaceplatePanel(string sDpType, string &sTrendFaceplate, dyn_string &exceptionInfo)
{
	int iObject;
	string sTrendConfig;
	dyn_string exceptionInfoTemp, dsConfig;
	
	if (globalExists("g_dsObjectList"))
		{
		if (dynlen(g_dsObjectList) <= 0)
			{
			unGenericObject_InitializeObjects(exceptionInfoTemp);
			}
		}
	else
		{
		unGenericObject_InitializeObjects(exceptionInfoTemp);
		}
	iObject = dynContains(g_dsObjectList, sDpType);
	if (iObject <= 0)
	{
              if(mappingHasKey(g_mFrontEndConfigs, sDpType)) {
                dsConfig = g_mFrontEndConfigs[sDpType];
                sTrendConfig = dsConfig[UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_TRENDFACEPLATEPANEL];
              }
              else
		fwException_raise(exceptionInfo, "ERROR", "unGenericObject_GetTrendFaceplate: unknown object", "");
	}
	else
		{
		sTrendConfig = g_ddsObjectConfigs[iObject][UN_GENERICOBJECT_FUNCTION_LENGTH + UN_GENERICOBJECT_FIELD_TRENDFACEPLATEPANEL];
		}
	sTrendFaceplate = sTrendConfig;
}

//------------------------------------------------------------------------------------------------------------------------
 
// unGenericObject_FaceplateTrendInit
/**
Purpose: create the trend tab in faceplate

Parameters:	
	sDpName, string, input, device name
	sDpType, string, input, device type
	exceptionInfo, dyn_string, output, for errors
	dsDeviceDpe, dyn_string, input, list of Dpe of the device (among the confugred ones) to trend, if empty use all the configured one
	iTab, int, input, tabulation number
	
Usage: External function

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_FaceplateTrendInit(string sDpName,
                                   string sDpType,
                                   dyn_string &exceptionInfo,
                                   dyn_string dsDeviceDpe = makeDynString(),
                                   int iTab = 1,
                                   dyn_bool dbInYScaleVisible = makeDynBool() )
{
  string deviceName, sRangeMin, sRangeMax, sProcessPart;
  float fRangeMin, fRangeMax;
  int iRangeType, iRes, position, i, length, iLen;
  dyn_string dsDpe, dsCurveLegend, dsCurveRange, dsCurveParameters, dsAnalog, dsRange, dsColor;
  dyn_string exceptionInfoTemp;
  dyn_float dfMax, dfMin;
  /*added by Herve */
  string deviceSystemName, deviceAlias, sTrendFaceplate;
  dyn_string dpeUnit=makeDynString("", "", "", "", "", "", "", "");
  string curveDPE, curveLegend, curveToolTipText, curveColor, curveRange, curveUnit, curveVisibility, axiiText, curvesType;
  string sZoomWindowTitle;
  dyn_string dsTempColor, dsTempRange, dsTempAnalog;
  dyn_bool dbYScaleVisible = dbInYScaleVisible;
  dyn_string dsData;
  string sName, sTabName, sTabTitle, sTimeRange;
  dyn_float dfAnalogMax, dfAnalogMin;
  bool bOk;
  dyn_string dsCurveAlias;
  string sDPEAlias;
  string sFwTrendingConfigDpe;
  /* end */
  dyn_string dsDPEAnalog, dsDPEAlias, dsProxy, dsDPE, dsDPERange, dsCurveDPE;
  bool bProxy, bIsLog, bConfigExists;
  dyn_bool dbCurveVisible;
  dyn_string dsCurveColor;
  dyn_float dfCurveMin, dfCurveMax;
  dyn_string dsFaceplateDPE;

  // Init : get faceplate trend configuration
  unGenericObject_GetFaceplateTrendConfig(sDpType, dsAnalog, dsRange, dsColor, exceptionInfo);
  unGenericObject_GetTrendFaceplatePanel(sDpType, sTrendFaceplate, exceptionInfo);

  // 1. Get range configuration
  deviceName = unGenericDpFunctions_getDpName(sDpName);

  unGenericDpFunctions_getFaceplateRange(deviceName, dsDpe, dfMax, dfMin);

  /*added by Herve */
  //1.1 get system and alias of the devide
  deviceSystemName = unGenericDpFunctions_getSystemName(sDpName);
  deviceAlias      = unGenericDpFunctions_getAlias(sDpName);
  /* end */	


  if((dsDeviceDpe == makeDynString()) && (iTab ==1))
  {

    // get the first trend config
    unGenericObject_getDeviceTrendConfig(sDpType, dsData, exceptionInfo);
    if(dynlen(dsData) > 0)
    {
      dynClear(dsAnalog);
      dynClear(dsRange);
      dynClear(dsColor);
      dynClear(dbYScaleVisible);
      unGenericObject_getDeviceDpeTrendConfig(dsData[1], sName, dsAnalog, dsRange, dsColor, dfAnalogMax, dfAnalogMin, dbYScaleVisible, sTimeRange, exceptionInfo);

      unGenericDpFunctions_getProxyDeviceList(deviceName, bProxy, dsProxy);

      unGenericObject_deviceGetAllTrendDPE(deviceName, bProxy, true, dsProxy, dsAnalog, dsRange, 
                                           dbYScaleVisible, dsColor, dfAnalogMin, dfAnalogMax, 
                                           dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                           dbCurveVisible, dsCurveColor, dfCurveMin, dfCurveMax);

      unGenericObject_getDeviceDpeTrendExtendedConfig(sDpType, sName, sFwTrendingConfigDpe, exceptionInfo);

      _unGenericObject_FaceplateTrendInit(1,
                                          deviceSystemName,
                                          deviceName,
                                          deviceAlias,
                                          dsFaceplateDPE,
                                          dsDPEAnalog,
                                          dsDPE,
                                          dsDPEAlias,
                                          dsDPERange,
                                          dsCurveDPE,
                                          dbCurveVisible,
                                          dsCurveColor,
                                          dfCurveMin,
                                          dfCurveMax,
                                          dsDpe,
                                          dfMax,
                                          dfMin,
                                          dsCurveParameters,
                                          bOk,
                                          sTimeRange,
                                          sFwTrendingConfigDpe);

      // 3. show tab
      if(bOk)
      {
        sTabName = "Trend1";
        sTabTitle = sName;
        setMultiValue("Tab1", "registerName", 1, sTabName,
                      "Tab1", "namedColumnHeader", sTabName, sTabTitle, 
                      "Tab1", "registerPanel", 1, sTrendFaceplate, dsCurveParameters);
      }
    }
  }
  else
  {
    //DebugN("bef", dsDeviceDpe, dsAnalog, dynIntersect(dsDeviceDpe, dsAnalog), dynIntersect(dsAnalog, dsDeviceDpe));
    if(dynlen(dsDeviceDpe) > 0)
    {
      dsTempAnalog = dynIntersect(dsAnalog, dsDeviceDpe);

      // rebuilt dsColor and dsRange
      length = dynlen(dsTempAnalog);
      for(i=1; i<=length; i++) 
      {
        position = dynContains(dsAnalog, dsTempAnalog[i]);
        if(position > 0)
        {
          dsTempColor[i] = dsColor[position];
          dsTempRange[i] = dsRange[position];
        }
        else
        {
          dsTempColor[i] = dsColor[1];
          dsTempRange[i] = dsRange[1];
        }
      }

      dsAnalog = dsTempAnalog;
      dsColor = dsTempColor;
      dsRange = dsTempRange;
    }

    // 2. For each value to plot
    length = dynlen(dsAnalog);
    for(i=1;i<=length;i++)
    {
      if (dpExists(deviceName + ".ProcessInput." + dsAnalog[i]))	// Good value to plot
      {
        sProcessPart = ".ProcessInput.";
      }
      else
      {
        if (dpExists(deviceName + ".ProcessOutput." + dsAnalog[i]))	// Good value to plot
        {
          sProcessPart = ".ProcessOutput.";
        }
        else
        {
          sProcessPart = "";
        }
      }

      if (sProcessPart != "")
      {
        //dynAppend(dsCurveDPE, deviceName + sProcessPart + dsAnalog[i] + ":_online.._value");
        dynAppend(dsCurveDPE, deviceSystemName+deviceAlias+"."+dsAnalog[i]);
        /*added by Herve */
        dpeUnit[i] = dpGetUnit(deviceName + sProcessPart + dsAnalog[i]);
        /* end */

        sDPEAlias = dpGetAlias(deviceName+sProcessPart+dsAnalog[i]);
        dynAppend(dsCurveAlias, sDPEAlias);
        if(sDPEAlias != "")
          dynAppend(dsCurveLegend, sDPEAlias);
        else
          dynAppend(dsCurveLegend, dsAnalog[i]);

        position = dynContains(dsDpe, dsAnalog[i]);
        if (position > 0)								// A config exists for this dpe
        {
          sRangeMin = dfMin[position];
          sRangeMax = dfMax[position];
        }
        else											// Search for pv_range
        {
          if (dsRange[i] != "")
          {
            if (dpExists(deviceName + ".ProcessInput." + dsRange[i]))
            {
              dpGet(deviceName + ".ProcessInput." + dsRange[i] + ":_pv_range.._type", iRangeType);
              if (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)
              {
                iRes = dpGet(deviceName + ".ProcessInput." + dsRange[i] + ":_pv_range.._min", fRangeMin,
                deviceName + ".ProcessInput." + dsRange[i] + ":_pv_range.._max", fRangeMax);
              }
            }
            else
            {
              if (dpExists(deviceName + ".ProcessOutput." + dsRange[i]))
              {
                dpGet(deviceName + ".ProcessOutput." + dsRange[i] + ":_pv_range.._type", iRangeType);
                if (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)
                {
                  iRes = dpGet(deviceName + ".ProcessOutput." + dsRange[i] + ":_pv_range.._min", fRangeMin,
                  deviceName + ".ProcessOutput." + dsRange[i] + ":_pv_range.._max", fRangeMax);
                }
              }
              else
              {
                iRangeType = DPCONFIG_NONE;
              }
            }
          }
          else
          {
            iRangeType = DPCONFIG_NONE;
          }

          if ((iRangeType != DPCONFIG_MINMAX_PVSS_RANGECHECK) || (iRes < 0))
          {
            fRangeMin = UN_RANGE_MIN_DEFAULT;		// Default values
            fRangeMax = UN_RANGE_MAX_DEFAULT;
          }

          sRangeMin = fRangeMin;
          sRangeMax = fRangeMax;

        }

        dynAppend(dsCurveRange, sRangeMin + ":" + sRangeMax);
        dynAppend(dsCurveColor, dsColor[i]);
      }
    }

    // 3. Open trend panel
    if (dynlen(dsCurveDPE) == 0)
    {
      Tab1.registerVisible(iTab, false);
    }
    else
    {
      while (dynlen(dsCurveLegend)<=UN_FACEPLATE_TRENDS_MAX)
      {
        dynAppend(dsCurveLegend, "");
      }

      while (dynlen(dsCurveAlias)<=UN_FACEPLATE_TRENDS_MAX)
      {
        dynAppend(dsCurveAlias, "");
      }

      while (dynlen(dsCurveRange)<=UN_FACEPLATE_TRENDS_MAX)
      {
        dynAppend(dsCurveRange, "");
      }

      while (dynlen(dsCurveDPE)<=UN_FACEPLATE_TRENDS_MAX)
      {
        dynAppend(dsCurveDPE, "");
      }

      while (dynlen(dsCurveColor)<=UN_FACEPLATE_TRENDS_MAX)
      {
        dynAppend(dsCurveColor, "");
      }

      while (dynlen(dpeUnit)<=UN_FACEPLATE_TRENDS_MAX)
      {
        dynAppend(dpeUnit, "");
      }

      while (dynlen(dbYScaleVisible)<=UN_FACEPLATE_TRENDS_MAX)
      {
        dynAppend(dbYScaleVisible, false);
      }

      /* new added by Herve */
      for(i=1;i<=UN_FACEPLATE_TRENDS_MAX;i++)
      {

        if(dsCurveDPE[i] != "") // was dsAnalog[i]
        {
          curveDPE += fwTrending_createAliasRepresentation(dsCurveDPE[i])+";";
          curveVisibility +=true+";";
        }
        else
        {
          curveDPE += dsCurveDPE[i]+";";
          curveVisibility +=false+";";
        }

        curveRange += dsCurveRange[i]+";";
        curveLegend += dsCurveLegend[i]+";";

        if(dsCurveAlias[i] == "")
          curveToolTipText += deviceSystemName+deviceAlias+"."+dsCurveLegend[i]+" ["+dpeUnit[i]+"];";
        else
          curveToolTipText += dsCurveAlias[i]+" ["+dpeUnit[i]+"];";

        curveUnit  += dpeUnit[i]+";";
        axiiText   += dbYScaleVisible[i]+";";
        //axiiText += false+";";
        curvesType += fwTrending_PLOT_TYPE_STEPS+";";
        curveColor += dsCurveColor[i]+";";
      }

      for(i=UN_FACEPLATE_TRENDS_MAX;i<=fwTrending_TRENDING_MAX_CURVE;i++)
      {
        curveDPE += ";";
        curveVisibility +=false+";";
        curveRange += ";";
        curveLegend += ";";
        curveToolTipText += ";";
        curveUnit += ";";
        axiiText += false+";";
        curvesType += fwTrending_PLOT_TYPE_STEPS+";";
        curveColor += ";";
      }

      dynAppend(dsCurveParameters, "$dsCurveDPE:"+curveDPE);
      dynAppend(dsCurveParameters, "$dsCurveLegend:"+curveLegend);
      dynAppend(dsCurveParameters, "$dsCurveToolTipText:"+curveToolTipText);
      dynAppend(dsCurveParameters, "$dsCurveColor:"+curveColor);
      dynAppend(dsCurveParameters, "$dsCurveRange:"+curveRange);
      dynAppend(dsCurveParameters, "$dsUnit:"+curveUnit);
      dynAppend(dsCurveParameters, "$dsCurveVisible:"+curveVisibility);
      dynAppend(dsCurveParameters, "$dsCurveScaleVisible:"+axiiText);
      dynAppend(dsCurveParameters, "$dsCurvesType:"+curvesType);
      /* end */
      sZoomWindowTitle = deviceAlias;

      // replace any . by _
      strreplace(sZoomWindowTitle, ".", "_");

      // replace any / by _
      strreplace(sZoomWindowTitle, "/", "_");

      // replace any \ by _
      strreplace(sZoomWindowTitle, "\\", "_");

      sZoomWindowTitle = sZoomWindowTitle+"_"+iTab;
      dynAppend(dsCurveParameters, "$ZoomWindowTitle:" + sZoomWindowTitle);

      /* new added by Herve */
      dynAppend(dsCurveParameters, "$sRefName:");
      dynAppend(dsCurveParameters, "$sDpName:");
      dynAppend(dsCurveParameters, "$fMinForLog:"+fwTrending_MIN_FOR_LOG);
      dynAppend(dsCurveParameters, "$fMaxPercentageForLog:"+fwTrending_MAX_PERCENTAGE_FOR_LOG);

      dynAppend(dsCurveParameters, "$bTrendLog:"+false);
      dynAppend(dsCurveParameters, "$sTimeRange:"+ 3600);
      dynAppend(dsCurveParameters, "$sForeColor:unSynopticBackground");
      dynAppend(dsCurveParameters, "$sBackColor:_Window");
      dynAppend(dsCurveParameters, "$bShowGrid:"+true);
      dynAppend(dsCurveParameters, "$bShowLegend:"+false);
      dynAppend(dsCurveParameters, "$templateParameters:");
      dynAppend(dsCurveParameters, "$iMarkerType:"+fwTrending_MARKER_TYPE_NONE);
      //DebugN(dsCurveParameters);
      /* end */

      // Load the log settings from the device's configuration datapoint if they exist: sDpe + UN_UNICOS_CONFIGURATION has a key UN_DEVICE_LOG_SCALE_CONFIGURATION
      _unGenericObject_getDeviceTrendLogConfiguration(deviceName, bConfigExists, bIsLog);
      if( bConfigExists )
      {
        iLen = dynlen(dsCurveParameters);
        for( i = 1 ; i <= iLen ; i++ )
        {
          if( strpos(dsCurveParameters[i], "$bTrendLog:") >= 0 )
          {
            dsCurveParameters[i] = "$bTrendLog:" + bIsLog;
          }
        }
      }

      Tab1.registerPanel(iTab, sTrendFaceplate, dsCurveParameters);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGenericObject_WidgetInit
/**
Purpose: initialize widget

Parameters:
	- sIdentifier: string, input, device identifier
	- sDpType, string, input, object
	- sWidgetRegisterFunction, string, input, widget register function
	- sDisconnectdFunction, string, inut, widget disconnect function
	- exceptionInfo: dyn_string, output, details of any exceptions are returned here

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

_unGenericObject_WidgetInit(string sIdentifier, string sDpType, string sWidgetRegisterFunction, string sDisconnectdFunction, dyn_string &exceptionInfo)
{
	string deviceSystemName, sDpPanel, sPanelName;
	bool bConnected, bInitOk = true;
	dyn_string exceptionInfoTemp;
	
	if(shapeExists("LockBmp")) 
		setMultiValue("LockBmp", "visible", false, "LockBmp", "fill", "[pattern,[tile,xmp,unWidgets/lock.xpm]]");	// Load picture

	if (strltrim(sIdentifier) != "")
		{
		deviceSystemName = unGenericDpFunctions_getSystemName(sIdentifier);
		unDistributedControl_register(sWidgetRegisterFunction, bInitOk, bConnected, deviceSystemName, exceptionInfo);
		}
	else
		{
		bInitOk = false;
		}
	if (!bInitOk)
		{
		unGraphicalFrame_getPanel(myModuleName(), sDpPanel, sPanelName);
		unSendMessage_toAllUsers(g_sWidgetType + getCatStr("unOperator","UNKNOWNDOLLARPARAM") + " in panel " + sPanelName, exceptionInfoTemp);
		if (dynlen(exceptionInfoTemp) > 0)
			{
			fwExceptionHandling_display(exceptionInfoTemp);
			}
		fwException_raise(exceptionInfo, "EXPERTINFO", "$sIdentifier" + getCatStr("unPVSS","UNKNOWNDOLLARPARAM") + " or register failed", "");
		unSendMessage_toExpertException("unGenericObject_WidgetInit", exceptionInfo);
		execScript("main(string sWidgetType) {" + sDisconnectdFunction + "(sWidgetType);}",
				   makeDynString("$sIdentifier:" + sIdentifier, "$sDpType:" + sDpType), g_sWidgetType);
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetInit
/**
Purpose: initialize widget

Parameters:
	- sIdentifier: string, input, device identifier
	- sDpType, string, input, object
	- exceptionInfo: dyn_string, output, details of any exceptions are returned here

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetInit(string sIdentifier, string sDpType, dyn_string &exceptionInfo)
{
	dyn_string dsFunctions;

	unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);

	_unGenericObject_WidgetInit(sIdentifier, sDpType, dsFunctions[UN_GENERICOBJECT_FUNCTION_WIDGETREGISTER], dsFunctions[UN_GENERICOBJECT_FUNCTION_WIDGETDISCONNECT], exceptionInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetDisconnection
/**
Purpose: animate widget disconnection

Parameters:
	- sWidgetType, string, input, widget type
	- sDpType, string, input, object

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetDisconnection(string sWidgetType, string sDpType)
{
	dyn_string dsFunctions, exceptionInfo;
	string sFunction;

// 1. Disconnection of widget body
	unGenericObject_WidgetGetFunctions(sWidgetType, sDpType, dsFunctions, exceptionInfo);
	sFunction = dsFunctions[UN_GENERICWIDGET_FUNCTION_DISCONNECTION];
	if (isFunctionDefined(sFunction) && (sFunction != ""))
		{
		execScript("main() {" + sFunction + "();}", makeDynString());
		}
// 2. Disconnection of alarm, warning, mode letter & select animation & right click
	setMultiValue("WarningText", "text", "", "AlarmText", "text", "", "ControlStateText", "text", "", 
				  "SelectArea", "foreCol", "", "LockBmp", "visible", false, "WidgetArea", "visible", false);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetDisplayValueDisconnect
/**
Purpose: Disconnection of widget display values

Parameters:
	- dsShapes, dyn_string, input, display values shapes

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetDisplayValueDisconnect(dyn_string dsShapes)
{
	int i, length;
	
	length = dynlen(dsShapes);
	for(i=1;i<=length;i++)
		{
		setValue(dsShapes[i], "foreCol", "unDataNoAccess");
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAnimationDoubleStsReg
/**
Purpose: animate widget

Parameters: 
	- sWidgetType, string, input, widget type = <dpType> + <widget type>. ex: AnaDigHeater
	- sDpType, string, input, object
	- bLocked, bool, input, for select animation
	- sSelectedManager, string, input, for select animation
	- bit32StsReg01, bit32, input, status register
        - bit32StsReg01, bit32, input, status register (empty in case of not using the stsReg02)
	- diWarningBits, dyn_int, input, warning bit positions in status register.
        - diWarningBits2, dyn_int, input, warning bit positions in status register. (empty in case of not using the stsReg02)
	- dsWarningLetters, dyn_string, input, associated warning letters to be displayed.
        - dsWarningLetters2, dyn_string, input, associated warning letters to be displayed. (empty in case of not using the stsReg02)
	- diAlarmBits, dyn_int, input, alarm bit positions in status register.
	- dsAlarmLetters, dyn_string, input, associated alarm letters to be displayed.
	- diControlBits, dyn_int, input, control bit positions in status register.
	- dsControlLetters, dyn_string, input, associated control letters to be displayed.
	- iOnbitPosition, int, input, on status bit position in status register
	- iOffBitPosition, int, input, off status bit position in status register
	- iAlUnackBitPosition, int, input, alarm unack status bit position in status register
	- bInvalidLetter, bool, input, true if invalid letter as to be shown
	- bInvalidBody, bool, input, invalid animation on body ?
	- iSystemIntegrityAlarmValue, int, input, the value of the systemIntegrity alarm for the PLC: 
							c_unSystemIntegrity_no_alarm_value (0) no alarm PLC is running, 
							c_unSystemIntegrity_alarm_value_level1 (10): alarm PLC is not correctly running
	- bSystemIntegrityAlarmEnabled, bool, input, the systemIntegrity alarm is enabled
	
di<...>Bits and ds<...>Letters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority .

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericObject_WidgetAnimationDoubleStsReg(string sWidgetType, string sDpType,
				bool bLocked, string sSelectedManager, bit32 bit32StsReg01, bit32 bit32StsReg02,
				dyn_int diWarningBits, dyn_int diWarningBits2,
                                dyn_string dsWarningLetters, dyn_string dsWarningLetters2,
				dyn_int diAlarmBits, dyn_string dsAlarmLetters, 
				dyn_int diControlBits, dyn_string dsControlLetters, 
				int iOnBitPosition, int iOffBitPosition, int iAlUnackBitPosition,
				bool bInvalidLetter, bool bInvalidBody,
				int iSystemIntegrityAlarmValue,
				bool bSystemIntegrityAlarmEnabled)
{
	string sColorSelect, sWarningLetter, sWarningColor, sAlarmLetter, sAlarmColor, sControlLetter, sControlColor, sBodyColor, sFunction;
	bool bLockVisible;
	dyn_string dsFunctions, exceptionInfo;
	
  // DebugTN("FUNCTION::unGenericObject_WidgetAnimationDoubleStsReg()   ; sWidgetType="+sWidgetType+" sDpType="+sDpType);
// 1. Select
	unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sColorSelect, bLockVisible);
// 2. Warning (if the stsreg02 exists then the treatment is different)
	unGenericObject_WidgetWarningAnimationDoubleStsReg(bit32StsReg01, bit32StsReg02, diWarningBits, diWarningBits2,dsWarningLetters,dsWarningLetters2, sWarningLetter, sWarningColor);

// 2.1 if the device is not correclty running (the iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value) 
// or the checking is not enabled then set the letter to O and the color to not valid data

	if ((!bSystemIntegrityAlarmEnabled) || (iSystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value))
		{
		sWarningLetter = UN_WIDGET_TEXT_OLD_DATA;
		sWarningColor = "unDataNotValid";
		}

	if (bInvalidLetter)
		{
		sWarningLetter = UN_WIDGET_TEXT_INVALID;
		sWarningColor = "unDataNotValid";
		}
// 3. Alarm
	unGenericObject_WidgetAlarmTextAnimation(bit32StsReg01, diAlarmBits, dsAlarmLetters, sAlarmLetter, sAlarmColor);
// 4. Control state
	unGenericObject_WidgetControlStateAnimation(bit32StsReg01, diControlBits, dsControlLetters, sControlLetter, sControlColor);
// 5. Animate all except body
	if(g_bSystemConnected)
	setMultiValue("WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor,
				  "AlarmText", "text", sAlarmLetter, "AlarmText", "foreCol", sAlarmColor,
				  "ControlStateText", "text", sControlLetter, "ControlStateText", "foreCol", sControlColor,
				  "SelectArea", "foreCol", sColorSelect, "LockBmp", "visible", bLockVisible,
				  "WidgetArea", "visible", true);
// 6. Animate body
	sBodyColor = "unWidget_ControlStateAuto";					// Auto mode (default)
	if (bInvalidBody)
		{
		sBodyColor = "unDataNotValid";							// Invalid data
		}
	else
		{
		if (sControlLetter == UN_WIDGET_TEXT_CONTROL_FORCED)
			{
			sBodyColor = "unWidget_ControlStateForced";			// Forced mode
			}
   // IS-441: Start Interlock does not animate the body.
		if ( (sAlarmLetter != "") && (sAlarmLetter != "I" ))
			{
			    sBodyColor = "unFaceplate_AlarmActive";

      		if (getBit(bit32StsReg01, iAlUnackBitPosition) == 1)
      			{
      			sBodyColor = "unWidget_AlarmNotAck";
      			}
    }
		}
	unGenericObject_WidgetGetFunctions(sWidgetType, sDpType, dsFunctions, exceptionInfo);
	sFunction = dsFunctions[UN_GENERICWIDGET_FUNCTION_ANIMATION];
 
	if (isFunctionDefined(sFunction) && (sFunction != "") && 
		(iOnBitPosition >= 0) && (iOnBitPosition <= 15) && (iOffBitPosition >= 0) && (iOffBitPosition <= 15))
		{
		if(g_bSystemConnected)
			execScript("main(int iOn, int iOff, string sBodyColor) {" +
				   sFunction + "(iOn, iOff, sBodyColor);}", 
				   makeDynString(), getBit(bit32StsReg01, iOnBitPosition), getBit(bit32StsReg01, iOffBitPosition), sBodyColor);
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAnimation
/**
Purpose: animate widget

Parameters: 
	- sWidgetType, string, input, widget type = <dpType> + <widget type>. ex: AnaDigHeater
	- sDpType, string, input, object
	- bLocked, bool, input, for select animation
	- sSelectedManager, string, input, for select animation
	- bit32StsReg01, bit32, input, status register
	- diWarningBits, dyn_int, input, warning bit positions in status register.
	- dsWarningLetters, dyn_string, input, associated warning letters to be displayed.
	- diAlarmBits, dyn_int, input, alarm bit positions in status register.
	- dsAlarmLetters, dyn_string, input, associated alarm letters to be displayed.
	- diControlBits, dyn_int, input, control bit positions in status register.
	- dsControlLetters, dyn_string, input, associated control letters to be displayed.
	- iOnbitPosition, int, input, on status bit position in status register
	- iOffBitPosition, int, input, off status bit position in status register
	- iAlUnackBitPosition, int, input, alarm unack status bit position in status register
	- bInvalidLetter, bool, input, true if invalid letter as to be shown
	- bInvalidBody, bool, input, invalid animation on body ?
	- iSystemIntegrityAlarmValue, int, input, the value of the systemIntegrity alarm for the PLC: 
							c_unSystemIntegrity_no_alarm_value (0) no alarm PLC is running, 
							c_unSystemIntegrity_alarm_value_level1 (10): alarm PLC is not correctly running
	- bSystemIntegrityAlarmEnabled, bool, input, the systemIntegrity alarm is enabled
	
di<...>Bits and ds<...>Letters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority .

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericObject_WidgetAnimation(string sWidgetType, string sDpType,
				bool bLocked, string sSelectedManager, bit32 bit32StsReg01, 
				dyn_int diWarningBits, dyn_string dsWarningLetters,
				dyn_int diAlarmBits, dyn_string dsAlarmLetters, 
				dyn_int diControlBits, dyn_string dsControlLetters, 
				int iOnBitPosition, int iOffBitPosition, int iAlUnackBitPosition,
				bool bInvalidLetter, bool bInvalidBody,
				int iSystemIntegrityAlarmValue,
				bool bSystemIntegrityAlarmEnabled)
{
        dyn_int diWarningBits_EMPTY;
        dyn_string dsWarningLetters_EMPTY;
        bit32 bit32StsReg02_EMPTY;
        
        diWarningBits_EMPTY=makeDynInt();
        dsWarningLetters_EMPTY=makeDynString();
        
        // All the objects all the same function as they were 2 stsReg02.
        // Code of this function has been moved to the new unGenericObject_WidgetAnimationDoubleStsReg()
  	unGenericObject_WidgetAnimationDoubleStsReg(
                                sWidgetType, sDpType,
				bLocked, sSelectedManager, bit32StsReg01,
                                bit32StsReg02_EMPTY,
				diWarningBits, 
                                diWarningBits_EMPTY,
                                dsWarningLetters,
				dsWarningLetters_EMPTY,
                                diAlarmBits, dsAlarmLetters, 
				diControlBits, dsControlLetters, 
				iOnBitPosition, iOffBitPosition, iAlUnackBitPosition,
				bInvalidLetter, bInvalidBody,
				iSystemIntegrityAlarmValue,
				bSystemIntegrityAlarmEnabled);       
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetSelectAnimation
/**
Purpose: animate the select part of the widget

Parameters :
	- sSelectedManager, string, input, selected manager
	- bLocked, bool, input, lock state
	- lockVisible, bool, output, true if the bitmap (a lock) has to be shown
	- color, string, output, the color to be used in the selectArea object 

Usage: Public

PVSS manager usage: NG, NV 

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetSelectAnimation(bool bLocked, string sSelectedManager, string &color, bool &lockVisible)
{
	string localManager;

	localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedManager); // Result "S", "SO" or "D"
	switch (localManager)				// Border color & lock bitmap
		{
		case "S":
			color = "unWidget_Select";
			lockVisible = false;
			break;
		case "SO":
			color = "unWidget_Select";
			lockVisible = true;
			break;
		case "D":
			color = "";
			lockVisible = false;
			break;
		default:
			color = "";
			lockVisible = false;
			break;
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetWarningAnimation
/**
Purpose: animate the warning part of the widget

Parameters :
	- bit32StsReg01, bit32, input, StsReg01 value
	- diBits, dyn_int, input, bit positions
	- dsLetters, dyn_string, input, associated letters to be displayed
diBits and dsLetters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority.
	- letter, string, output, the letter to be displayed
	- color, string, output, the color to be used

Usage: Public

PVSS manager usage: NG, NV 

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetWarningAnimation(bit32 bit32StsReg01, dyn_int diBits, dyn_string dsLetters, string &letter, string &color)
{
        dyn_int diBits2_EMPTY;
        dyn_string dsLetters2_EMPTY;
        bit32 bit32StsReg02_EMPTY;
        
        diBits2_EMPTY=makeDynInt();
        dsLetters2_EMPTY=makeDynString();
        
        // Calls the generic function with two stsReg to check
        unGenericObject_WidgetWarningAnimationDoubleStsReg(bit32StsReg01, bit32StsReg02_EMPTY, 
                                       diBits,  diBits2_EMPTY, 
                                       dsLetters,  dsLetters2_EMPTY, 
                                       letter,  color);
}

unGenericObject_WidgetWarningAnimationDoubleStsReg(bit32 bit32StsReg01, bit32 bit32StsReg02, dyn_int diBits, dyn_int diBits2, dyn_string dsLetters, dyn_string dsLetters2, string &letter, string &color)
{
	int i,length;
	
        // Init
	letter = "";
	color = "unFaceplate_WarningActive";
        
        // Low Priority for the first stsReg: stsReg01
        length = dynlen(diBits);
	if (length == dynlen(dsLetters))
		{
                // For each defined warning
		for(i=1;i<=length;i++)
			{
			if (getBit(bit32StsReg01, diBits[i]) == 1)
				{
				letter = dsLetters[i];
				}
			}
		}
         // CASE double stsReg to be taken into account: Analog, AnaDig, OnOff, PCO.
        if (dynlen(diBits2)>0) 
          {     
                // First treatement of the stsreg02 bits
          	length = dynlen(diBits2);
          	if (length == dynlen(dsLetters2))
          		{
                          // For each defined warning
          		for(i=1;i<=length;i++)
          			{
          			if (getBit(bit32StsReg02, diBits2[i]) == 1)
          				{
          				letter = dsLetters2[i];
          				}
          			}
          		}    
          }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetControlStateAnimation
/**
Purpose: animate the control state part of the widget

Parameters :
	- bit32StsReg01, bit32, input, StsReg01 value
	- diBits, dyn_int, input, bit positions
	- dsLetters, dyn_string, input, associated letters to be displayed
diBits and dsLetters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority.
	- letter, string, output, the letter to be displayed
	- color, string, output, the color to be used
	
Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unGenericObject_WidgetControlStateAnimation(bit32 bit32StsReg01, dyn_int diBits, dyn_string dsLetters, string &letter, string &color)
{
	int i,length;
	
// 1. Init
	letter = "";
	color = "unWidget_ControlStateManualLocal";
	length = dynlen(diBits);
	if (length == dynlen(dsLetters))
		{
// 2. For each defined warning
		for(i=1;i<=length;i++)
			{
			if (getBit(bit32StsReg01, diBits[i]) == 1)
				{
				letter = dsLetters[i];
				}
			}
		}
// 3. Forced mode
	if (letter == UN_WIDGET_TEXT_CONTROL_FORCED)
		{
		color = "unWidget_ControlStateForced";
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetAlarmTextAnimation
/**
Purpose: animate the alarm part of the widget

Parameters :
	- bit32StsReg01, bit32, input, StsReg01 value
	- diBits, dyn_int, input, bit positions
	- dsLetters, dyn_string, input, associated letters to be displayed
diBits and dsLetters must have the same length. dyn arrays must be defined in priority order: lowest -> highest priority.
	- letter, string, output, the letter to be displayed
	- color, string, output, the color to be used
	
Usage: Internal function

PVSS manager usage:  NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetAlarmTextAnimation(bit32 bit32StsReg01, dyn_int diBits, dyn_string dsLetters, string &letter, string &color)
{
	int i, length;
	
// 1. Init
	letter = "";
	color = "unFaceplate_AlarmActive";
	length = dynlen(diBits);
	if (length == dynlen(dsLetters))
		{
// 2. For each defined alarm
		for(i=1;i<=length;i++)
			{
			if (getBit(bit32StsReg01, diBits[i]) == 1)
				{
				letter = dsLetters[i];
				}
			}
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetDisplayValueAnimation
/**
Purpose: animation of widget display values

Parameters:
	- dsShapes, dyn_string, input, display values shapes

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetDisplayValueAnimation(string sShape, string sFormat, float fValue, string sColor, bool bInvalid)
{
	string displayColor, formattedValue;

	displayColor = sColor;
	formattedValue = unGenericObject_FormatValue(sFormat, fValue);
	if (bInvalid)
		{
		displayColor = "unDataNotValid";
		}
	setMultiValue(sShape, "text", formattedValue, sShape, "foreCol", displayColor);
}

/**Animation of widget display values

@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  UI

@param sShape display values shapes
@param sUnit dpe unit
@param sFormat dpe format
@param fValue dpe value
@param sColor animation color 
@param bInvalid is dpe invalid

@reviewed 2018-08-03 @whitelisted{FalsePositive} Called in surveySU_FPS.ctl
*/
void unGenericObject_WidgetDisplayValue(string sShape, string sUnit, string sFormat, float fValue, string sColor, bool bInvalid) {
  string displayColor, formattedValue;

  displayColor = sColor;
  formattedValue = unGenericObject_FormatValue(sFormat, fValue);
  if (bInvalid) {
    displayColor = "unDataNotValid";
  }
  if (sUnit != "") {
    formattedValue = formattedValue + " " + sUnit;
  }
  if (g_bSystemConnected) {
    setMultiValue(sShape, "text", formattedValue, sShape, "foreCol", displayColor);
  }
}
//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_OpenFaceplate
/**
Purpose: open faceplate

Parameters:
	- sDpName, string, input, deviceName
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_OpenFaceplate(string sDpName, dyn_string &exceptionInfo)
{
	string sFaceplateName, deviceName, sPanelName, sModule1, sModule2;
	int xPos, yPos, openMarge = 5;
	float widgetHeight, widgetWidth;
	dyn_int diSize;
	bool bModule1, bModule2, bPanel1, bPanel2;
        
  if(strpos(myModuleName(), "_Widget") > 0) {
    return;
  }
	deviceName = unGenericDpFunctions_getDpName(sDpName);
	if (dpExists(deviceName))
		{
// 1. Get faceplate path
		unGenericDpFunctions_getFaceplate(deviceName, sFaceplateName, exceptionInfo);
		if ((dynlen(exceptionInfo) <= 0) && (sFaceplateName != ""))
			{
// 2. Build panel name
			sPanelName = unGenericDpFunctions_getAlias(deviceName) + " " + unGenericDpFunctions_getDescription(deviceName) + "   ";
// 3. Check if panel is already opened in unicos graphical frame			
			sModule1 = myModuleName();
			if (strpos(sModule1, ";unicosGFPanel") > 0)
				{
				sModule2 = sModule1;
				strreplace(sModule2, ";unicosGFPanel", "");
				}
			else
				{
				sModule2 = sModule1 + ";unicosGFPanel";
				}
                        bModule1 = (bool)isModuleOpen(sModule1);
                        bModule2 = (bool)isModuleOpen(sModule2);
                        if(bModule1)
                          bPanel1 = unGraphicalFrame_isPanelOpen(sPanelName, sModule1);
                        if(bModule2)
                          bPanel2 = unGraphicalFrame_isPanelOpen(sPanelName, sModule2);
			if (!bPanel1 && !bPanel2)
				{
// 4. Get open position
				if (this.name == "WidgetArea")
					{
					getValue("", "position", xPos, yPos);
					getValue("", "size", widgetWidth, widgetHeight);
					diSize = getPanelSize(sFaceplateName);
					widgetWidth = widgetWidth + 15;	// Add lock bitmap width
					xPos = (int) (xPos + widgetWidth + openMarge);
					yPos = (int) (yPos + widgetHeight + openMarge);
					if (dynlen(diSize) == 2)
						{
						if ((xPos + diSize[1]) >= (UN_BASE_PANEL_LEFT + UN_BASE_PANEL_WIDTH))
							{
							xPos = (int)(xPos - diSize[1] - widgetWidth - openMarge);
							}
						if ((yPos + diSize[2]) >= (UN_BASE_PANEL_TOP + UN_BASE_PANEL_HEIGHT))
							{
							yPos = (int)(yPos - diSize[2] - widgetHeight - openMarge);
							}
						}
// 5. Open faceplate
					unGraphicalFrame_ChildPanelOn(sFaceplateName, sPanelName, makeDynString("$sDpName:" + deviceName), xPos, yPos);
					}
				else
					{
					unGraphicalFrame_ChildPanelOnCentral(sFaceplateName, sPanelName, makeDynString("$sDpName:" + deviceName));
					}
				}
			else
				{
				if (bPanel1)
					{
					setInputFocus(sModule1, _unGraphicalFrame_getModuleId() + sPanelName, "Button1");
					}
				else if(bPanel2)
					{
					setInputFocus(sModule2, _unGraphicalFrame_getModuleId() + sPanelName, "Button1");
					}
				}
			}
		}
	else
		{
		fwException_raise(exceptionInfo, "ERROR", "unGenericObject_OpenFaceplate:" + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
		}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_OpenDiagnostic
/**
Purpose: open diagnostic panel

Parameters:
	- sDpName, string, input, deviceName
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_OpenDiagnostic(string sDpName, dyn_string &exceptionInfo)
{
	unGenericObject_OpenSynopticDiagnosticPopup(sDpName, UN_PARAMETER_DIAGNOSTIC, "EMPTYDIAG", "BADDIAG", " Diagnostic", exceptionInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_OpenSynopticPopup
/**
Purpose: open Synoptic as pop-up

Parameters:
	- sDpName, string, input, device DpName
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_OpenSynopticPopup(string sDpName, dyn_string &exceptionInfo)
{
	unGenericObject_OpenSynopticDiagnosticPopup(sDpName, UN_PARAMETER_PANEL, "EMPTYSYNOPTIC", "EMPTYSYNOPTIC", " Synoptic", exceptionInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_OpenSynopticDiagnosticPopup
/**
Purpose: open Synoptic/Diagnostic as pop-up

Parameters:
	- sDpName, string, input, device DpName
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_OpenSynopticDiagnosticPopup(string sDpName, int iPos, string sCatKey1, string sCatKey2, string sTitle, dyn_string &exceptionInfo)
{
	dyn_string dsParameters, dsUserData;
	string sLink, message, alias, path, deviceName, systemName, panelDp, sDpType, panelFile;
	bool badPath, bConnected;
	int len;
        
	systemName = unGenericDpFunctions_getSystemName(sDpName);
	unDistributedControl_isConnected(bConnected, systemName);
	if (bConnected)
	{
		deviceName = unGenericDpFunctions_getDpName(sDpName);
		unGenericDpFunctions_getParameters(deviceName, dsParameters);
		alias = unGenericDpFunctions_getAlias(deviceName);
		sLink = dsParameters[iPos];
		if (sLink == "")
		{
			fwException_raise(exceptionInfo, "EXPERTINFO", "unGenericObject_OpenSynopticDiagnosticPopup: " + alias + getCatStr("unDB",sCatKey1),"");
		}
		else
		{
			strreplace(sLink,"\\","/");
			if (getPath(PANELS_REL_PATH, sLink) == "")
			{
				badPath = true;
			// check if WT
				if(dpExists(sLink)) {//check if of type unTreeWidget_treeNodeDPT
					if(dpTypeName(sLink) == unTreeWidget_treeNodeDPT) {
						fwTree_getNodeUserData(_fwTree_getNodeName(sLink), dsUserData, exceptionInfo);
						fwTree_getNodeDevice(_fwTree_getNodeName(sLink), panelDp, sDpType, exceptionInfo);
						panelFile = unPanel_convertDpPanelNameToPanelFileName(panelDp);
						len = dynlen(dsUserData);
						if(len <= 0) { // case no userData
							dsUserData[1] = "";
							len = dynlen(dsUserData);
						}
						if((getPath(PANELS_REL_PATH, panelFile) != "") && (len > 0) 
															&& (sDpType == UN_PANEL_DPTYPE) && dpExists(panelDp)){
							badPath = false;
							unGraphicalFrame_ChildPanelOn(panelFile, alias + " " + unGenericDpFunctions_getDescription(deviceName) + sTitle, strsplit(dsUserData[1], ";"), 0, 25);
						}
					}
				}
				if(badPath) {
					fwException_raise(exceptionInfo, "EXPERTINFO", "unGenericObject_OpenSynopticDiagnosticPopup: " + alias + getCatStr("unDB",sCatKey2),"");
				}
			}
			else
			{
				unGraphicalFrame_ChildPanelOnCentral(sLink, alias + " " + unGenericDpFunctions_getDescription(deviceName) + sTitle, makeDynString("$sDpName:" + sDpName));
			}
		}
		if ((sLink == "") || (badPath == true))
		{
			unGraphicalFrame_ChildPanelOnCentralModal("vision/MessageWarning","Warning",makeDynString("$1:" + alias + getCatStr("unDB",sCatKey1)));
		}
	}
	else
		unGraphicalFrame_ChildPanelOnCentralModal("vision/MessageWarning","Warning",makeDynString("$1: Remote system" + systemName + " not connected"));
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_OpenHtml
/**
Purpose: open html link

Parameters:
	- sDpName, string, input, deviceName
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_OpenHtml(string sDpName, dyn_string &exceptionInfo)
{
	dyn_string dsParameters;
	string sLink, message, alias, path, deviceName, systemName, sDp;
	bool badPath, bConnected, bScript;
	int iRes;
	
	systemName = unGenericDpFunctions_getSystemName(sDpName);
	unDistributedControl_isConnected(bConnected, systemName);
	if (bConnected)
	{
		deviceName = unGenericDpFunctions_getDpName(sDpName);
		unGenericDpFunctions_getParameters(deviceName, dsParameters);
		alias = unGenericDpFunctions_getAlias(deviceName);
		sLink = dsParameters[UN_PARAMETER_HTML];
		if (sLink == "")
		{
			fwException_raise(exceptionInfo, "EXPERTINFO", "unGenericObject_OpenHtml: " + alias + getCatStr("unDB","EMPTYHTML"),"");
			unGraphicalFrame_ChildPanelOnCentralModal("vision/MessageWarning","Warning",makeDynString("$1:" + alias + getCatStr("unDB","EMPTYHTML")));
		}
		else
		{
			unGenericObject_OpenHtmlFile(sLink,alias);
		}
	}
}

unGenericObject_GetHtmlLinks(string sDpName, dyn_string &dsHtmlLinks, dyn_string exceptionInfo) {
	dyn_string dsParameters;
	string sLink, message, alias, path, deviceName, systemName, sDp;
	bool badPath, bConnected, bScript;
	int iRes;
	
	systemName = unGenericDpFunctions_getSystemName(sDpName);
	unDistributedControl_isConnected(bConnected, systemName);
	if (bConnected)
	{
		deviceName = unGenericDpFunctions_getDpName(sDpName);
		unGenericDpFunctions_getParameters(deviceName, dsParameters);
		alias = unGenericDpFunctions_getAlias(deviceName);
		sLink = dsParameters[UN_PARAMETER_HTML];
		if (sLink != "") {
			dsHtmlLinks = strsplit(sLink, UN_PARAMETER_SUBITEM_DELIMITER);
		}

	}
	
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_OpenHtmlFile
/**
Purpose: open html link

Parameters:
	- sDpName, string, input, deviceName
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_OpenHtmlFile(string url, string tabLabel="")
{
  dyn_string exceptionInfo;
  //fwGeneral_openInExternalBrowser(url,exceptionInfo);
  string windowName="UNICOS Devices"; // idenfifies the windo (panel) in which to open the link
   bool useInternalBrowser;
  fwGeneral_getHelpUseInternalBrowser(useInternalBrowser, exceptionInfo);
  
  if(useInternalBrowser){
    fwWebBrowser_showStandaloneWindow(url, exceptionInfo, tabLabel, windowName);
  } else {
    fwGeneral_openInExternalBrowser(url, exceptionInfo);
  }
  if (dynlen(exceptionInfo)) { fwExceptionHandling_display(exceptionInfo);return;};
}
	
//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_WidgetRightClick
/**
Purpose: right click event on widget area

Parameters:
	- sIdentifier, string, input, widget device name identifier
	- sDpType, string, input, device type
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. Use constants UN_POPUPMENU_***
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_WidgetRightClick(string sIdentifier, string sDpType, dyn_string &exceptionInfo)
{
	dyn_string menuList, dsFunctions, dsAccessOk, exceptionInfoTemp;
	string sFunction, deviceName, systemName;
	int menuAnswer;
	bool bConnected;

  if(strpos(myModuleName(), "_Widget") > 0) {
    return;
  }
	deviceName = unGenericDpFunctions_getWidgetDpName(sIdentifier);
	systemName = unGenericDpFunctions_getSystemName(sIdentifier);
	unDistributedControl_isConnected(bConnected, systemName);
	if (bConnected)
	{
// get the access state
		unGenericButtonFunctionsHMI_isAccessAllowedMultiple(deviceName, sDpType, dsAccessOk, exceptionInfo);
// Init
		unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
		sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_MENUCONFIG];
		
		if (sFunction != "")
		{
			evalScript(menuList, "dyn_string main(string device, string sType, dyn_string dsAccessOk) {" + 
								 	 "dyn_string dsMenuTemp;" +
									 "if (isFunctionDefined(\"" + sFunction + "\"))" +
									 "{" +
									 "    " + sFunction + "(device, sType, dsAccessOk, dsMenuTemp);" +
									 "}" +
									 "else " +
									 "{" +
									 "    dsMenuTemp = makeDynString();" +
									 "}" +
									 "return dsMenuTemp; }", makeDynString(), deviceName, sDpType, dsAccessOk);
		}
// 7. Open menu
		menuAnswer = 0;
		if (dynlen(menuList) > 0)
		{
			popupMenu(menuList, menuAnswer);
		}
	 		
//DebugN(deviceName, sDpType, menuList, menuAnswer);
		if(menuAnswer > 0) {
// 8. Actions
			sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_HANDLEMENUANSWER];
			if (sFunction != "")
			{
				evalScript(exceptionInfoTemp, "dyn_string main(string deviceName, string sDpType, dyn_string menuList, int menuAnswer) {" + 
									 	 "dyn_string exceptionInfo;" +
										 "if (isFunctionDefined(\"" + sFunction + "\"))" +
										 "{" +
										 "    " + sFunction + "(deviceName, sDpType, menuList, menuAnswer);" +
										 "}" +
										 "else " +
										 "{" +
										 "    exceptionInfo = makeDynString();" +
										 "}" +
										 "return makeDynString(); }", makeDynString(), deviceName, sDpType, menuList, menuAnswer);
				dynAppend(exceptionInfo, exceptionInfoTemp);
			}
		}
		
		if (dynlen(exceptionInfo) > 0)
		{
			unSendMessage_toExpertException("Right Click",exceptionInfo);
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_HandleUnicosMenu
/**
Purpose: handle the result of the pop-up menu for the UNICOS device

Parameters:
	- deviceName, string, input, the device dp name
	- sDpType, string, input, the device dp type
	- menuList, dyn_string, input, list of requested action
	- menuAnswer, int, input, selected action from the menuList
	- exceptionInfo, dyn_string, output, the error is returned here
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_HandleUnicosMenu(string deviceName, string sDpType, dyn_string menuList, int menuAnswer, dyn_string &exceptionInfo)
{ 
	dyn_string exceptionInfoTemp1, exceptionInfoTemp2;
	string sAlias, sCatKey, currentAlertClass;
	int iRes, iBit, alertType;
	bool bRes,isAlarmLocked;
	dyn_bool bSMStabOld;
        int xPos, yPos;

//DebugN(deviceName, sDpType, menuList, menuAnswer);
	sAlias = unGenericDpFunctions_getAlias(deviceName);

// handle the select
	unGenericObject_handleSelect(deviceName, sDpType, menuList, menuAnswer, exceptionInfo);
// handle the UNICOS default (open faceplate, diagnostic and info)
	unGenericObject_handleDefaultUnicos(deviceName, sDpType, menuList, menuAnswer, exceptionInfo);
// handle the UNICOS device actions
	switch (menuAnswer)
	{
		case UN_POPUPMENU_ACK_ID:					// Acknowledge alarm
			unGenericObject_Acknowledge(deviceName, exceptionInfo);
			break;
		case UN_POPUPMENU_MASK_POSST_ID:					// Mask PosSt PVSS Alarm
			bRes = (bool)dynContains(menuList, "PUSH_BUTTON, Mask PVSS Alarm, "+UN_POPUPMENU_MASK_POSST_ID+", 1");
			unAlarmConfig_mask(deviceName + ".ProcessInput.PosSt", (bRes > 0), exceptionInfoTemp1);
			sCatKey = (bRes > 0) ? "MASKPVSS" : "UNMASKPVSS";
			if(dynlen(exceptionInfoTemp1) > 0)
			{
				unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
										 getCatStr("unOperator", sCatKey + "FAILED"), exceptionInfoTemp2);
				if (dynlen(exceptionInfoTemp2) > 0)
				{
					fwExceptionHandling_display(exceptionInfoTemp2);
				}
				dynAppend(exceptionInfo, exceptionInfoTemp1);
			}
			else
			{
				unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
										 getCatStr("unOperator", sCatKey + "OK"), exceptionInfoTemp1);
				if (dynlen(exceptionInfoTemp1) > 0)
				{
					fwExceptionHandling_display(exceptionInfoTemp1);
				}
			}
			dynClear(exceptionInfoTemp1);
			break;
		case UN_POPUPMENU_MASK_POSAL_ID:					// Mask PosAl PVSS alarm
			bRes = (bool)dynContains(menuList, "PUSH_BUTTON, Mask PVSS Alarm, "+UN_POPUPMENU_MASK_POSAL_ID+", 1");
			unAlarmConfig_mask(deviceName + ".ProcessInput.PosAl", (bRes > 0), exceptionInfoTemp1);
			sCatKey = (bRes > 0) ? "MASKPVSS" : "UNMASKPVSS";
			if(dynlen(exceptionInfoTemp1) > 0)
			{
				unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
										 getCatStr("unOperator", sCatKey + "FAILED"), exceptionInfoTemp2);
				if (dynlen(exceptionInfoTemp2) > 0)
				{
					fwExceptionHandling_display(exceptionInfoTemp2);
				}
				dynAppend(exceptionInfo, exceptionInfoTemp1);
			}
			else
			{
				unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(deviceName) + 
										 getCatStr("unOperator", sCatKey + "OK"), exceptionInfoTemp1);
				if (dynlen(exceptionInfoTemp1) > 0)
				{
					fwExceptionHandling_display(exceptionInfoTemp1);
				}
			}
			dynClear(exceptionInfoTemp1);
			break;
		case UN_POPUPMENU_BLOCKPLC_ID:					// Block PLC alarm
			bRes = (bool)dynContains(menuList, "PUSH_BUTTON, "+UN_POPUPMENU_BLOCKPLC_LABEL+", "+UN_POPUPMENU_BLOCKPLC_ID+", 1");
			iBit = (bRes > 0) ? UN_MANREG01_MMASKALR : UN_MANREG01_MUNMASKALR;
			sCatKey = (bRes > 0) ? "MASKPLC" : "UNMASKPLC";
			unGenericObject_SendBinaryOrder(deviceName, iBit, sCatKey, exceptionInfo);
			break;
		case UN_POPUPMENU_SMS_ID:					// add config for Mail-SMS
			bRes = (bool)dynContains(menuList, "PUSH_BUTTON, "+UN_POPUPMENU_SMS_LABEL+", "+UN_POPUPMENU_SMS_ID+", 1");
			sCatKey = (bRes > 0) ? "ADDSMS" : "REMOVESMS";
			
			dpGet(deviceName + ".ProcessInput.PosAl:_alert_hdl.._type", alertType,
			  	   deviceName + ".ProcessInput.PosAl:_lock._alert_hdl._locked", isAlarmLocked);
			  	   
			if(isAlarmLocked)
			{
				fwException_raise(exceptionInfoTemp1,"EXPERTINFO","setAlarmLimits : " + deviceName + ".ProcessInput.PosAl" + getCatStr("unDB","ALARMLOCKED"),"");
			}
			else
			{
				if (alertType != DPCONFIG_NONE)
				{
                                  getCursorPosition(xPos, yPos);
                                  unGraphicalFrame_ChildPanelOnModal("vision/unicosObject/General/unSetMailSMS.pnl",
						  unGenericDpFunctions_getAlias(deviceName) + " " + unGenericDpFunctions_getDescription(deviceName) + ": Set Mail-SMS",
						  makeDynString("$sDpAlarmName:" + deviceName + ".ProcessInput.PosAl"),
						  xPos, yPos);
				}	
			}
			dynClear(exceptionInfoTemp1);
		break;		
		
            
		default:
			break;
	}
// handle the trend action
	unGenericObject_handleTrendAction(deviceName, sDpType, menuList, menuAnswer, exceptionInfo);
// handle the single DPE trend action
	unGenericObject_handleSingleDPETrendAction(deviceName, sDpType, menuList, menuAnswer, exceptionInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_handleSelect
/**
Purpose: handle the select

Parameters:
	- deviceName, string, input, the device dp name
	- sDpType, string, input, the device dp type
	- menuList, dyn_string, input, list of requested action
	- menuAnswer, int, input, selected action from the menuList
	- exceptionInfo, dyn_string, output, the error is returned here
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_handleSelect(string deviceName, string sDpType, dyn_string menuList, int menuAnswer, dyn_string &exceptionInfo)
{
	bool bRes;
	string sAlias, message;
	
	sAlias = unGenericDpFunctions_getAlias(deviceName);

	switch (menuAnswer)
	{
		case 1:					// select / deselect
			bRes = (bool) dynContains(menuList, "PUSH_BUTTON, Select, "+UN_POPUPMENU_SELECT_ID+", 1");
			unGenericObject_ButtonSelectAction(deviceName, (bRes > 0), exceptionInfo);
			
			if(bRes)	message = "Select ";
				else message = "Deselect ";
			unSendMessage_toAllUsers(message + unSendMessage_getDeviceDescription(deviceName), exceptionInfo);
			break;
		default:
			break;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_handleDefaultUnicos
/**
Purpose: handle the default UNICOS action: open faceplate, open diagnostic, open info.

Parameters:
	- deviceName, string, input, the device dp name
	- sDpType, string, input, the device dp type
	- menuList, dyn_string, input, list of requested action
	- menuAnswer, int, input, selected action from the menuList
	- exceptionInfo, dyn_string, output, the error is returned here
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_handleDefaultUnicos(string deviceName, string sDpType, dyn_string menuList, int menuAnswer, dyn_string &exceptionInfo)
{  
	switch (menuAnswer)
	{
		case UN_POPUPMENU_FACEPLATE_ID:					// Open faceplate
			unGenericObject_OpenFaceplate(deviceName, exceptionInfo);
			break;
		case UN_POPUPMENU_DIAGNOSTIC_ID:					// Open diagnostic panel
			unGenericObject_OpenDiagnostic(deviceName, exceptionInfo);
			break;
		case UN_POPUPMENU_DEVICECONFIG_ID:				// Open device configuration
			unGenericObject_DeviceConfiguration(deviceName, exceptionInfo);
			break;
		case UN_POPUPMENU_SYNOPTIC_ID: // open synoptic in pop-up panel
				unGenericObject_OpenSynopticPopup(deviceName, exceptionInfo);
				break;
		case UN_POPUPMENU_COMMENTS_ID:				// Open comments panel
				ChildPanelOnCentral("vision/unComment/unDeviceCommentNotificationScreen.pnl", 
							  "Active comments for " + dpGetAlias(deviceName + "."), 
							  makeDynString("$sDeviceDpe:" + deviceName, "$sDeviceName:" + dpGetAlias(deviceName + ".")));
				break;		
		case UN_POPUPMENU_SYNOPTICLOC_ID: // Open the fwSynopticLocator panel
		  unGenericObject_OpenFwSynopticLocatorPopup(deviceName, exceptionInfo);
		  break;
		default:				
			if ( menuAnswer-UN_POPUPMENU_INFO_ID >= 0 && menuAnswer-UN_POPUPMENU_INFO_ID < 99) {
				// info link max 99
				unGenericObject_OpenHtmLink(deviceName,menuAnswer);	
			} else {
				// device link
				unGenericObject_handleDeviceLink(deviceName, menuAnswer, exceptionInfo);
			}
			break;
		}
}
//unGenericObject_OpenHtmLink 
/**
  Purpose: open html link
       
            
  Usage: private
            
  Parameters:
	- deviceName, string, input, the device dp name
	- menuAnswer, int, input, selected action from the menuList
  */
unGenericObject_OpenHtmLink(string deviceName, int menuAnswer) {
	dyn_string dsParameters, dsHtmlLinks;
	string sLink;
	int iHtmlLinkId;
	iHtmlLinkId = menuAnswer - UN_POPUPMENU_INFO_ID;
	unGenericDpFunctions_getParameters(deviceName, dsParameters);
	if (dsParameters[UN_PARAMETER_HTML] != "") {
		dsHtmlLinks = strsplit(dsParameters[UN_PARAMETER_HTML], UN_PARAMETER_SUBITEM_DELIMITER);
		string tabLabel="";// label on the tab in fwWebBrowser
		if (dynlen(dsHtmlLinks) == 1) {
			//compatibility with old solution
			sLink = dsParameters[UN_PARAMETER_HTML];
			tabLabel="INFO";
		} else {
			sLink = dsHtmlLinks[iHtmlLinkId*2+2];
			tabLabel=dsHtmlLinks[iHtmlLinkId*2+1];
		}
		string devAlias=dpGetAlias(deviceName+".");
		tabLabel=devAlias+" "+tabLabel;
		unGenericObject_OpenHtmlFile(sLink,tabLabel);
	}

}
//unGenericObject_OpenFwSynopticLocaorPopup
/**
  Purpose: open the standalone panel of fwSynopticLocator for 
            a specific device.
            Check if the component is properly installed before opening the panel
            
  Usage: private
            
  Parameters:
	- deviceName, string, input, the device dp name
	- exceptionInfo, dyn_string, output, the error is returned here
  */
private void unGenericObject_OpenFwSynopticLocatorPopup( string deviceName, dyn_string &exceptionInfo)
{
  int iVersion; //not used as anyversion is ok
  bool bIsFwSynopticLocatorInstalled = fwInstallation_isComponentInstalled( "fwSynopticLocator", iVersion);
  string sSynopticDeviceName = dpGetAlias(deviceName + ".");           
   
  if ( bIsFwSynopticLocatorInstalled)
  {
    string sPanelFile = "fwSynopticLocator/SynopticLocator_Visualization/StandaloneDevices.pnl";
    ChildPanelOn(sPanelFile, "Locate", makeDynString("$sSelectedDevice:"+sSynopticDeviceName), 0, 0);
  } else
  {
     ChildPanelOn("vision/MessageWarning", "fwSynopticLocator", makeDynString("$1:fwSynopticLocator is not installed.\nPlease contact your system administrators."), 0, 0);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_handleDeviceLink
/**
Purpose: open device configuration

Parameters:
	- sDpName, string, input, deviceName
	- iMenuAnswer, int, input, selected action from the menuList
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_handleDeviceLink(string sDpName, int iMenuAnswer, dyn_string &exceptionInfo)
{
	int iCsteTrend = UN_POPUPMENU_TREND_CSTE, iMenuValue, pos, iCste = UN_POPUPMENU_DEVICELINK_ID;
	string sLink, sSystemName;
	dyn_string dsSplit;

	if(iMenuAnswer < iCsteTrend)
  { // device link
		unGenericDpFunctions_getDeviceLink(sDpName, sLink);
		dsSplit = strsplit(sLink, UN_CONFIG_GROUP_SEPARATOR);
		iMenuValue = iMenuAnswer - iCste;

		if((iMenuValue > 0) && (iMenuValue <= dynlen(dsSplit)))
    {
			pos = strpos(dsSplit[iMenuValue], ":");
			if(pos <= 0)
      {
				dsSplit[iMenuValue] = unGenericDpFunctions_getSystemName(sDpName) + dsSplit[iMenuValue];
      }

			unGenericObject_OpenFaceplate(unGenericDpFunctions_getWidgetDpName(dsSplit[iMenuValue]), exceptionInfo);
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_DeviceConfiguration
/**
Purpose: open device configuration

Parameters:
	- sDpName, string, input, deviceName
	- exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_DeviceConfiguration(string sDpName, dyn_string &exceptionInfo)
{
	string fileName = "vision/unicosObject/General/unModifyConfig.pnl";
	string panelName = "Device configuration (local): "+unGenericDpFunctions_getAlias(sDpName);

	unGraphicalFrame_ChildPanelOnCentral(fileName, panelName, makeDynString("$sDeviceDpName:" + sDpName));
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_FrontEndConfiguration
/**
Purpose: open the front-end configuration

Parameters:
  - sDpName, string, input, deviceName
  - exceptionInfo, dyn_string, output, for errors
	
Usage: External

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
unGenericObject_FrontEndConfiguration(string sDpName, dyn_string &exceptionInfo)
{
  unGenericObject_DeviceConfiguration(sDpName, exceptionInfo);
}



//---------------------------------------------------------------------------------------------------------------------------------------

// _unGenericObject_openDynamicPlotDisplay
/**
Purpose: opens a dynamic trend panel, helper function for unGenericObject_handleTrendAction

Parameters:
	- sPlotDpName, string, input, the dynamic plot DP name
	- iOpenMargeX, integer, input, the X-axis margin
	- iOpenMargeY, integer, input, the Y-axis margin
	
Usage: Internal, by unGenericObject_handleTrendAction()

PVSS manager usage: UI
*/
void _unGenericObject_openDynamicPlotDisplay(string sPlotDpName, int iOpenMargeX = 20, int iOpenMargeY = 120)
{
  if(!isModuleOpen(sPlotDpName))
  {
    // unGenericDpFunctions_getFaceplate(deviceName, sFaceplateName, exceptionInfo);
    string sFaceplateName = fwTrending_PANEL_PLOT_FACEPLATE_FULLCAPTION_WLAYOUT;
    int x, y, widgetWidth, widgetHeight;
    
    if(sFaceplateName != "")
    {
	  	if (this.name == "WidgetArea")
	  	{
	  		getValue("", "position", x, y);
	  		getValue("", "size", widgetWidth, widgetHeight);
	  		dyn_int diFreeCurves = getPanelSize(sFaceplateName);
	  		widgetWidth = widgetWidth + 15;	// Add lock bitmap width
	  		x = x + widgetWidth + iOpenMargeX;
	  		y = y + widgetHeight + iOpenMargeY;
	  		if (dynlen(diFreeCurves) == 2)
	  		{
	  			if ((x + diFreeCurves[1]) >= (UN_BASE_PANEL_LEFT + UN_BASE_PANEL_WIDTH))
	  			{
	  				x = x - diFreeCurves[1] - widgetWidth - iOpenMargeX;
	  			}
	  			if ((y + diFreeCurves[2]) >= (UN_BASE_PANEL_TOP + UN_BASE_PANEL_HEIGHT))
	  			{
	  				y = y - diFreeCurves[2] - widgetHeight - iOpenMargeY;
	  			}
	  		}
	  	}
	  }
	  fwTrending_openPlotDisplay(sPlotDpName, sFaceplateName, true, true, sPlotDpName, sPlotDpName, x, y, "Scale");
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_handleTrendAction
/**
Purpose: handle the trend action: add DPE to trend.

Parameters:
	- deviceName, string, input, the device dp name
	- sDpType, string, input, the device dp type
	- menuList, dyn_string, input, list of requested action
	- menuAnswer, int, input, selected action from the menuList
	- exceptionInfo, dyn_string, output, the error is returned here
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_handleTrendAction(string deviceName, string sDpType, dyn_string menuList, int menuAnswer, dyn_string &exceptionInfo)
{
 	int iCste = UN_POPUPMENU_TREND_CSTE, iMenuValue, iRangeType;
	dyn_string dsAnalog, dsRange;
	string sDeviceAlias, sDeviceSystemName;
	string sPlotDpName, sDpe, sModuleName, sRange, sDpRange;
	int iNumberOfCurves, i;
	bool bIsDefined, bAddCurve=true;
	dyn_string dsCurveData, dsTemp;
	dyn_dyn_string ddsCurves;
	dyn_int diFreeCurves;
	float fMin, fMax;
  //  openMargeX = 20 and openMargeY = 120 have been moved as default parameters to _unGenericObject_openDynamicPlotDisplay
	dyn_dyn_string plotData;
	string sCurveDPE;
	string sManNum=myManNum();
        string sDPEAlias;
        dyn_string dsDPEAnalog, dsDPEAlias, dsProxy, dsDPE, dsDPERange, dsCurveDPE;
        bool bProxy;
        int iPos;
        dyn_bool dbCurveVisible;
        dyn_string dsCurveColor;
        dyn_float dfCurveMin, dfCurveMax;
        dyn_string dsFaceplateDPE;

	sModuleName = myModuleName();
	dsTemp = strsplit(sModuleName, ";");
	if(dynlen(dsTemp)>1) {
		sModuleName = dsTemp[1];
	}
//	sPlotDpName = "_"+sManNum+"_"+sModuleName+UN_DEVICE_DYNAMICTREND;
	sPlotDpName = "_"+sManNum+"_"+UN_DEVICE_DYNAMICTREND;

	if(menuAnswer == iCste) // popup menu item: clear dynamic trend
	{
    if(isModuleOpen(sPlotDpName))
    {
      ModuleOff(sPlotDpName);
    }
    if(dpExists(sPlotDpName))
    {
      dpDelete(sPlotDpName);
    }
	}
	else if(menuAnswer > iCste) // popup menu item: add signal to dynamic trend
  {
		unGenericObject_GetTrendDPEList(sDpType, dsAnalog, dsRange, exceptionInfo);
                unGenericDpFunctions_getProxyDeviceList(deviceName, bProxy, dsProxy);
                unGenericObject_deviceGetAllTrendDPE(deviceName, bProxy, true, dsProxy, dsAnalog, dsRange,
                                                     makeDynBool(), makeDynString(), makeDynFloat(), makeDynFloat(), 
                                                     dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                                     dbCurveVisible, dsCurveColor, dfCurveMin, dfCurveMax);
		iMenuValue = menuAnswer - iCste;

		if((iMenuValue >= 1) && (iMenuValue <= dynlen(dsDPEAnalog))) {
			sDeviceSystemName = unGenericDpFunctions_getSystemName(deviceName);
			sDeviceAlias = unGenericDpFunctions_getAlias(deviceName);

			sDpRange = dsDPERange[iMenuValue];

			sRange = "0:0";			 // Range default
			if(dpExists(sDpRange)) {
				dpGet(sDpRange + ":_pv_range.._type", iRangeType);
				if(iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)
				{
					dpGet(sDpRange + ":_pv_range.._min", fMin,
								 sDpRange + ":_pv_range.._max", fMax);
					sRange = fMin + ":" + fMax;
				}
			}
			dsTemp = strsplit(sRange, ":");
			
			if(!dpExists(sPlotDpName))  {
                 	  fwTrending_createPlot(sPlotDpName, exceptionInfo);
                          if(dynlen(exceptionInfo)<=0)  {
                             dpSet(sPlotDpName+".plotType", fwTrending_PLOT_TYPE_STEPS,
                                   sPlotDpName+".markerType", fwTrending_MARKER_TYPE_NONE);
                              }
			}
      _unGenericObject_openDynamicPlotDisplay(sPlotDpName); // open the dynamic trend

			// prepare dpe information in case it represents aliases
			if(fwTrending_isAlias(dsCurveDPE[iMenuValue]))
			{
			  sCurveDPE = fwTrending_createAliasRepresentation(dsCurveDPE[iMenuValue]);
			}
			else
			{
			  sCurveDPE = dsCurveDPE[iMenuValue];
			}

	// check if DPE already in
			fwTrending_getPlot(sPlotDpName, plotData, exceptionInfo);
			i = dynContains(plotData[fwTrending_PLOT_OBJECT_DPES], sCurveDPE);
		
	// do not add the curve if already in
			if(i > 0)
				bAddCurve = false;			
		
			if(bAddCurve) {
				sDPEAlias = dsDPEAlias[iMenuValue];
				// remove first proxy constant if any
				iPos = strpos(sDPEAlias, UN_PROXY_DELIMITER);
				if(iPos > 0)
				  sDPEAlias = substr(sDPEAlias, iPos+strlen(UN_PROXY_DELIMITER), strlen(sDPEAlias));
				if(sDPEAlias == "")
				  sDPEAlias = dsCurveDPE[iMenuValue];
				fwTrending_getFreeCurves(sPlotDpName, diFreeCurves, exceptionInfo);
	
				fwTrending_getFreeCurves(sPlotDpName, diFreeCurves, exceptionInfo);
				iNumberOfCurves = dynlen(diFreeCurves);
				
				if(iNumberOfCurves > 0) {
			// append new curve					
					dsCurveData[fwTrending_CURVE_OBJECT_DPE] = sCurveDPE;
		
					dsCurveData[fwTrending_CURVE_OBJECT_TYPE] = fwTrending_PLOT_TYPE_STEPS;
				//	curveData[fwTrending_CURVE_OBJECT_COLOR] = "Green";
					dsCurveData[fwTrending_CURVE_OBJECT_LEGEND] = sDPEAlias;
//					dsCurveData[fwTrending_CURVE_OBJECT_LEGEND] = sDeviceAlias+"."+dsAnalog[iMenuValue];
					dsCurveData[fwTrending_CURVE_OBJECT_AXIS] = "FALSE";
					dsCurveData[fwTrending_CURVE_OBJECT_HIDDEN] = "FALSE";
					dsCurveData[fwTrending_CURVE_OBJECT_Y_MIN] = dsTemp[1];
					dsCurveData[fwTrending_CURVE_OBJECT_Y_MAX] = dsTemp[2];
		
					fwTrending_appendCurve(sPlotDpName, dsCurveData, exceptionInfo);
				}
				else {
					exceptionInfo = makeDynString();
	// shift up the curves and append to the last position
					for(i=2; i<=fwTrending_MAX_NUM_CURVES; i++)
					{
						fwTrending_getCurve(sPlotDpName, bIsDefined, dsCurveData, i, exceptionInfo);
		
		//	dsCurveData[fwTrending_CURVE_OBJECT_TYPE] = fwTrending_PLOT_TYPE_STEP;
						dsCurveData[fwTrending_CURVE_OBJECT_COLOR] = "";
		//	dsCurveData[fwTrending_CURVE_OBJECT_LEGEND] = "Specific legend text";
		//	dsCurveData[fwTrending_CURVE_OBJECT_AXIS] = "FALSE";
		//	dsCurveData[fwTrending_CURVE_OBJECT_HIDDEN] = "FALSE";
	//					dsCurveData[fwTrending_CURVE_OBJECT_Y_MIN] = "";
	//					dsCurveData[fwTrending_CURVE_OBJECT_Y_MAX] = "1000";
						
						ddsCurves[i-1] = dsCurveData;
					}
					dsCurveData[fwTrending_CURVE_OBJECT_DPE] = sCurveDPE;
		
					dsCurveData[fwTrending_CURVE_OBJECT_TYPE] = fwTrending_PLOT_TYPE_STEPS;
					dsCurveData[fwTrending_CURVE_OBJECT_COLOR] = "";
					dsCurveData[fwTrending_CURVE_OBJECT_LEGEND] = sDPEAlias;
//					dsCurveData[fwTrending_CURVE_OBJECT_LEGEND] = sDeviceAlias+"."+dsAnalog[iMenuValue];
					dsCurveData[fwTrending_CURVE_OBJECT_AXIS] = "FALSE";
					dsCurveData[fwTrending_CURVE_OBJECT_HIDDEN] = "FALSE";
					dsCurveData[fwTrending_CURVE_OBJECT_Y_MIN] = dsTemp[1];
					dsCurveData[fwTrending_CURVE_OBJECT_Y_MAX] = dsTemp[2];
	
					ddsCurves[fwTrending_MAX_NUM_CURVES] = dsCurveData;
					
					fwTrending_setManyCurves(sPlotDpName, ddsCurves, makeDynInt(1,2,3,4,5,6,7,8), exceptionInfo);
				}
			}
		}
	}
  else if (UN_POPUPMENU_TREND_REOPEN == menuAnswer) // popup menu item: add signal to dynamic trend
  {
		if(!dpExists(sPlotDpName)) // if the dynamic trend does not exist
    {
      DebugN("This line should never be reached: " + __FILE__ + ":" + __LINE__ + " in function " + __FUNCTION__ + "()");
    }
    else // if the dynamic trend exists
    {
      _unGenericObject_openDynamicPlotDisplay(sPlotDpName); // open the dynamic trend
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_handleSingleDPETrendAction
/**
Purpose: handle the trend action for single DPE: open trend panel likedynamic trend with one signal and auto scale.

Parameters:
	- deviceName, string, input, the device dp name
	- sDpType, string, input, the device dp type
	- menuList, dyn_string, input, list of requested action
	- menuAnswer, int, input, selected action from the menuList
	- exceptionInfo, dyn_string, output, the error is returned here
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_handleSingleDPETrendAction(string deviceName, string sDpType, dyn_string menuList, int menuAnswer, dyn_string &exceptionInfo)
{
  int iCste = UN_POPUPMENU_TREND_CSTE, iMenuValue;
  string sDeviceAlias, sDeviceSystemName;
  string sFaceplateName;
  int x=-2, y=-2, i;
  int widgetWidth, widgetHeight, openMargeX = 20, openMargeY = 120;
  bool bOk;
  dyn_string dsCurveParameters, dsAnalog, dsRange;
  dyn_int diFreeCurves;
  dyn_string dsDPEAnalog, dsDPEAlias, dsProxy, dsDPE, dsDPERange, dsCurveDPE;
  bool bProxy;
  dyn_bool dbCurveVisible;
  dyn_string dsCurveColor;
  dyn_float dfCurveMin, dfCurveMax;
  dyn_string dsFaceplateDPE;
  
  iCste +=10000;

  if(menuAnswer > iCste) {
    unGenericObject_GetTrendDPEList(sDpType, dsAnalog, dsRange, exceptionInfo);
    unGenericDpFunctions_getProxyDeviceList(deviceName, bProxy, dsProxy);
    unGenericObject_deviceGetAllTrendDPE(deviceName, bProxy, true, dsProxy, dsAnalog, dsRange, 
                                         makeDynBool(), makeDynString(), makeDynFloat(), makeDynFloat(), 
                                         dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                         dbCurveVisible, dsCurveColor, dfCurveMin, dfCurveMax);
    iMenuValue = menuAnswer - iCste;

    if((iMenuValue >= 1) && (iMenuValue <= dynlen(dsDPEAnalog))) {
      sDeviceSystemName = unGenericDpFunctions_getSystemName(deviceName);
      sDeviceAlias = unGenericDpFunctions_getAlias(deviceName);
//      unGenericDpFunctions_getFaceplate(deviceName, sFaceplateName, exceptionInfo);

//	Override panel so layout panel will be used
//      sFaceplateName = fwTrending_PANEL_PLOT_FACEPLATE_FULLCAPTION;
      sFaceplateName = fwTrending_PANEL_PLOT_FACEPLATE_FULLCAPTION_WLAYOUT;
      if(sFaceplateName != "") {
        if (this.name == "WidgetArea")
        {
          getValue("", "position", x, y);
          getValue("", "size", widgetWidth, widgetHeight);
          diFreeCurves = getPanelSize(sFaceplateName);
          widgetWidth = widgetWidth + 15;	// Add lock bitmap width

          x = x + widgetWidth + openMargeX;
          y = y + widgetHeight + openMargeY;
          if (dynlen(diFreeCurves) == 2)
          {
            if ((x + diFreeCurves[1]) >= (UN_BASE_PANEL_LEFT + UN_BASE_PANEL_WIDTH))
            {
              x = x - diFreeCurves[1] - widgetWidth - openMargeX;
            }
            if ((y + diFreeCurves[2]) >= (UN_BASE_PANEL_TOP + UN_BASE_PANEL_HEIGHT))
            {
              y = y - diFreeCurves[2] - widgetHeight - openMargeY;
            }
          }
        }
      }

      _unGenericObject_FaceplateTrendInit(dsDPEAnalog[iMenuValue], sDeviceSystemName, deviceName, sDeviceAlias,
                                          makeDynString(dsFaceplateDPE[iMenuValue]), 
                                          makeDynString(dsDPEAnalog[iMenuValue]), 
                                          makeDynString(dsDPE[iMenuValue]), 
                                          makeDynString(dsDPEAlias[iMenuValue]), 
                                          makeDynString(dsDPERange[iMenuValue]), 
                                          makeDynString(dsCurveDPE[iMenuValue]), 
										  makeDynBool(false), 
                                          makeDynString("FwTrendingCurve1"), 
										  makeDynFloat(0.0), 
										  makeDynFloat(0.0),
                                          makeDynString(), 
										  makeDynFloat(),  
										  makeDynFloat(),
                                          dsCurveParameters, bOk, "0.0.1");
      if(bOk) {
        ModuleOnWithPanel(sDeviceAlias+"_SingleTrend: "+dsDPEAnalog[iMenuValue], x, y, 10, 10, 1, 1, "Scale", fwTrending_PANEL_PLOT_FACEPLATE_FULLCAPTION_WLAYOUT, 
                          sDeviceAlias, dsCurveParameters);
        x = stayOnTop(true, sDeviceAlias+"_SingleTrend: "+dsDPEAnalog[iMenuValue]);
      }

    }
  }
}

//-------------------------------------------------------------------------------------------------

/** Adds the select action to the right-click menu of a UNICOS device

@par Constraints
  . WinCC version: 2.12.1 or later
	. Pperating system: Windows 7, Windows Server 2008 RC2
	. Distributed system: yes
  
@par Usage
	External

@par WinCC managers
	NG, NV
  
@param sDeviceName       string,     INPUT, name of the datapoint
@param sDpType           string,     INPUT, datapoint type
@param dsMenuConfig      dyn_string, INPUT, actions that need to be incorporated into the menu
@param dsAccessOk        dyn_string, INPUT, access rights for the different actions
@param dsMenuList        dyn_string, OUTPUT, the menu to which actions are added as items
*/
unGenericObject_addSelectToMenu(string sDeviceName, string sDpType, dyn_string dsMenuConfig, dyn_string dsAccessOk, dyn_string &dsMenuList)
{
	string sSelectedManager, selectText, localManager;
	bool bNeedSelect, bLocked, bEnabled;
	int iRes;
	dyn_string exceptionInfo;
	
	if(dynContains(dsMenuConfig, UN_POPUPMENU_SELECT_TEXT) > 0)
	{
		unGenericObject_NeedSelect(sDpType, bNeedSelect, exceptionInfo);
		if(bNeedSelect)
		{
			iRes = dpGet(sDeviceName + ".statusInformation.selectedManager:_lock._original._locked", bLocked,
							 sDeviceName + ".statusInformation.selectedManager", sSelectedManager);
      
			localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedManager);
			switch (localManager)
			{
				case "D":
					selectText = cste_select;
					bEnabled = TRUE;
					break;
				case "S":
					selectText = "Deselect";
					bEnabled = TRUE;
					break;
				case "SO":
					selectText = "Deselect";
					bEnabled = FALSE;
					break;
				default:
					selectText = cste_select;
					bEnabled = FALSE;
					break;
			}
      
			bEnabled = bEnabled && (dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_SELECT) > 0);
      
      unGenericObject_addItemToPopupMenu(dsMenuList, selectText, UN_POPUPMENU_SELECT_ID, bEnabled);
		}
	}
}

//-------------------------------------------------------------------------------------------------

/** Adds UNICOS actions to the right-click menu of a UNICOS device

@par Constraints
  . WinCC version: 2.12.1 or later
	. Pperating system: Windows 7, Windows Server 2008 RC2
	. Distributed system: yes
  
@par Usage
	External

@par WinCC managers
	NG, NV
  
@param sDeviceName       string,     INPUT, name of the datapoint
@param sDpType           string,     INPUT, datapoint type
@param dsMenuConfig      dyn_string, INPUT, actions that need to be incorporated into the menu
@param dsAccessOk        dyn_string, INPUT, access rights for the different actions
@param dsMenuList        dyn_string, OUTPUT, the menu to which actions are added as items
*/
void unGenericObject_addUnicosActionToMenu(string sDeviceName, string sDpType, dyn_string dsMenuConfig, dyn_string dsAccessOk, dyn_string &dsMenuList)
{
	bool isAlertActive, bEnabled;
	bit32 bit32StsReg01;
	string blockText, maskText, currentAlertClass;
	int iRes, iManReg01;
  int iAlertType = DPCONFIG_NONE;

  // Ack Alarm
	if (dynContains(dsMenuConfig, UN_POPUPMENU_ACK_TEXT) > 0)
	{
		bEnabled = FALSE;
    
		if(dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_ACK_ALARM) > 0)
		{
		  bEnabled = TRUE;
      
      // Keep into account if the alarm is not already acknowledged	
      if (sDpType=="Alarm")
      {
        iRes = dpGet(sDeviceName + ".ProcessInput.StsReg01", bit32StsReg01);
        if(iRes >= 0)
        {
          if ((getBit(bit32StsReg01, UN_STSREG01_ALUNACK) == 0) && sDpType=="Alarm")
          {
            bEnabled = FALSE;    // Alarm already acknowledged
          }
          else
          {
            bEnabled = TRUE; 
          }
        }
      } 

      // reset manreg01: to be able to ack alarm afterwards. Workaround due to a PVSS bug: right click event cancelled if another right
      // click is executed before the end of the previous one.
			iRes = dpGet(sDeviceName + ".ProcessOutput.ManReg01", iManReg01);
			if ((iRes < 0) || (iManReg01 != 0))
			{
				dpSet(sDeviceName + ".ProcessOutput.ManReg01", 0);
			}
		}
    
    unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_ACK_LABEL, UN_POPUPMENU_ACK_ID, bEnabled);
	}

  // Mask PosSt
	if(dynContains(dsMenuConfig, UN_POPUPMENU_MASK_POSST_TEXT) > 0)
	{
		bEnabled = dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_MASK_ALARM) > 0;

		dpGet(sDeviceName + ".ProcessInput.PosSt:_alert_hdl.._type", iAlertType);
		if(iAlertType != DPCONFIG_NONE)
		{
			iRes = dpGet(sDeviceName + ".ProcessInput.PosSt:_alert_hdl.._active", isAlertActive);
			if(iRes >= 0)
			{
				maskText = "Unmask PVSS Alarm";
				if(isAlertActive)
				{
					maskText = cste_maskPVSS;
				}
				unGenericObject_addItemToPopupMenu(dsMenuList, maskText, UN_POPUPMENU_MASK_POSST_ID, bEnabled);
			}
		}
	}

  // Mask PosAl
	if(dynContains(dsMenuConfig, UN_POPUPMENU_MASK_POSAL_TEXT) > 0)
	{
		bEnabled = dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_MASK_ALARM) > 0;

		dpGet(sDeviceName + ".ProcessInput.PosAl:_alert_hdl.._type", iAlertType);
		if(iAlertType != DPCONFIG_NONE)
		{
			iRes = dpGet(sDeviceName + ".ProcessInput.PosAl:_alert_hdl.._active", isAlertActive);
			if(iRes >= 0)
			{
				maskText = "Unmask PVSS Alarm";
				if(isAlertActive)
				{
					maskText = cste_maskPVSS;
				}
        
        unGenericObject_addItemToPopupMenu(dsMenuList, maskText, UN_POPUPMENU_MASK_POSAL_ID, bEnabled);
			}
		}
	}

  // Block Plc
	if(dynContains(dsMenuConfig, UN_POPUPMENU_BLOCK_PLC_TEXT) > 0)
	{
    iRes = dpGet(sDeviceName + ".ProcessInput.StsReg01", bit32StsReg01);
		if(iRes >=0)
		{
      // reset manreg01: to be able to ack alarm afterwards. Workaround due to a PVSS bug: right click event cancelled if another right
      // click is executed before the end of the previous one.
      bEnabled = (dynContains(dsAccessOk, UN_FACEPLATE_BUTTON_BLOCK_ALARM_ACTION) > 0) &&
                            (getBit(bit32StsReg01, UN_STSREG01_AUINHFMO) == 0);
      
			iRes = dpGet(sDeviceName + ".ProcessOutput.ManReg01", iManReg01);
			if((iRes < 0) || (iManReg01 != 0))
			{
				dpSet(sDeviceName + ".ProcessOutput.ManReg01", 0);
			}

      if(getBit(bit32StsReg01, UN_STSREG01_AUINHFMO) == 1)
      {
			  blockText = "Block PLC Alarm not allowed";
      }
      else
      {
        blockText = cste_BlockPLC;
      }
      
			if(getBit(bit32StsReg01, UN_STSREG01_ALMSKST) == 1)
			{
				blockText = "Deblock PLC Alarm";
			}
      
      unGenericObject_addItemToPopupMenu(dsMenuList, blockText, UN_POPUPMENU_BLOCKPLC_ID, bEnabled);
		}   
	}
  
	// SMS	
	if (dynContains(dsMenuConfig, UN_POPUPMENU_SMS_LABEL) > 0)
	{
		bEnabled = dynContains(dsAccessOk, UN_POPUPMENU_SMS_LABEL) > 0;

		dpGet(sDeviceName + ".ProcessInput.PosAl:_alert_hdl.._type", iAlertType);
		if (iAlertType != DPCONFIG_NONE)
		{
			iRes = dpGet(sDeviceName + ".ProcessInput.PosAl:_alert_hdl.1._class", currentAlertClass);
			iRes = dpGet(sDeviceName + ".ProcessInput.PosAl:_alert_hdl.._active", isAlertActive);
			
      if(iRes>=0)
			{
				if(isAlertActive)
				{
          unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_SMS_LABEL, UN_POPUPMENU_SMS_ID, bEnabled);
				}
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------

/** Adds UNICOS default actions to the right-click menu of a UNICOS device

@par Constraints
  . WinCC version: 2.12.1 or later
	. Pperating system: Windows 7, Windows Server 2008 RC2
	. Distributed system: yes
  
@par Usage
	External

@par WinCC managers
	NG, NV
  
@param sDeviceName       string,     INPUT, name of the datapoint
@param sDpType           string,     INPUT, datapoint type
@param dsMenuConfig      dyn_string, INPUT, actions that need to be incorporated into the menu
@param dsAccessOk        dyn_string, INPUT, access rights for the different actions
@param dsMenuList        dyn_string, OUTPUT, the menu to which actions are added as items
*/
void unGenericObject_addDefaultUnicosActionToMenu(string sDeviceName, string sDpType, dyn_string dsMenuConfig, dyn_string dsAccessOk, dyn_string &dsMenuList)
{
	if (dynContains(dsMenuConfig, UN_POPUPMENU_FACEPLATE_TEXT) > 0)
	{
		if(dynlen(dsMenuList) > 0) {
			if (dsMenuList[dynlen(dsMenuList)] != "SEPARATOR")
			{
				dynAppend(dsMenuList, "SEPARATOR");
			}
		}
    
    unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_FACEPLATE_LABEL, UN_POPUPMENU_FACEPLATE_ID, TRUE);
		  unGenericObject_addDiagnosticAndInfoToActionToMenu(sDeviceName, sDpType, dsMenuConfig, dsAccessOk, dsMenuList);
    unGenericObject_addSynopticLocatorActionToMenu(dsMenuList);
	}  
}

/** 
Add the right click entry menu for fwSynopticLocator in case the component is installed

@param dsMenuList string, OUTPUT, the menu to which actions are added as items 
*/

void unGenericObject_addSynopticLocatorActionToMenu( dyn_string &dsMenuList)
{
  int iVersion; //not used as anyversion is ok
  bool bIsFwSynopticLocatorInstalled = fwInstallation_isComponentInstalled( "fwSynopticLocator", iVersion);
  
  if (bIsFwSynopticLocatorInstalled)
  {
    unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_SYNOPTICLOC_LABEL, UN_POPUPMENU_SYNOPTICLOC_ID, TRUE);
  }
}

//-------------------------------------------------------------------------------------------------

/** Adds UNICOS default actions to the right-click menu of a UNICOS device 
  (only faceplate is added before)

@par Constraints
  . WinCC version: 2.12.1 or later
	. Pperating system: Windows 7, Windows Server 2008 RC2
	. Distributed system: yes
  
@par Usage
	External

@par WinCC managers
	NG, NV
  
@param sDeviceName       string,     INPUT, name of the datapoint
@param sDpType           string,     INPUT, datapoint type
@param dsMenuConfig      dyn_string, INPUT, actions that need to be incorporated into the menu
@param dsAccessOk        dyn_string, INPUT, access rights for the different actions
@param dsMenuList        dyn_string, OUTPUT, the menu to which actions are added as items
*/
void unGenericObject_addDiagnosticAndInfoToActionToMenu(string sDeviceName, string sDpType, dyn_string dsMenuConfig, dyn_string dsAccessOk, dyn_string &dsMenuList)
{
	string sLink, sDeviceLink;
	int i, len, iCste, pos, iMask;
	dyn_string dsSplit, dsParameters;
	bool bConnected, bEnabled;
	string sDeviceDpLink, sSystemLink, sMenuLabel;
	bit32 b32Sts;
    
  string sSystemName = unGenericDpFunctions_getSystemName(sDeviceName);  
  string sTriggerDp = _unGraphicalFrame_getInternalTriggerName(myModuleName());
  
	unGenericDpFunctions_getParameters(sDeviceName, dsParameters);
		
	if(dynlen(dsMenuList) > 0)
  {
		if (dsMenuList[dynlen(dsMenuList)] != "SEPARATOR")
		{
			dynAppend(dsMenuList, "SEPARATOR");
		}
	}
       
  unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_DIAGNOSTIC_LABEL, UN_POPUPMENU_DIAGNOSTIC_ID, dpExists(sTriggerDp));
  unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_INFO_LABEL, UN_POPUPMENU_INFO_ID, TRUE);
  unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_SYNOPTIC_LABEL, UN_POPUPMENU_SYNOPTIC_ID, dpExists(sTriggerDp) && dsParameters[UN_PARAMETER_PANEL] != "");
  unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_COMMENTS_LABEL, UN_POPUPMENU_COMMENTS_ID, TRUE);
  unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_DEVICECONFIG_LABEL, UN_POPUPMENU_DEVICECONFIG_ID, TRUE);

	if(unGenericDpFunctions_isMaskEvent(sDeviceName))
  {
		unGenericDpFunctions_getMaskEvent(sDeviceName, iMask);

		if(iMask == 0)
    {
      unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_UNMASKEVENT_LABEL, UN_POPUPMENU_UNMASKEVENT_ID, dynContains(dsAccessOk, UN_FACEPLATE_MASKEVENT));
		}
		else
    {
			unGenericObject_addItemToPopupMenu(dsMenuList, UN_POPUPMENU_MASKEVENT_LABEL, UN_POPUPMENU_MASKEVENT_ID, dynContains(dsAccessOk, UN_FACEPLATE_MASKEVENT));
    }
	}
  
	unGenericDpFunctions_getDeviceLink(sDeviceName, sLink);
	if(sLink != "") 
	{
		dynAppend(dsMenuList,"CASCADE_BUTTON, Device link(s), 1");
		dynAppend(dsMenuList,"Device link(s)");
		
		dsSplit = strsplit(sLink, UN_CONFIG_GROUP_SEPARATOR);
		
		len = dynlen(dsSplit);
		iCste = UN_POPUPMENU_DEVICELINK_ID;
		for(i = 1; i <= len; i++)
		{
			iCste++;
			bEnabled = TRUE;
			pos = strpos(dsSplit[i], ":");
			
			if(pos <= 0) // no system name add device system name
			{
				sDeviceLink = sSystemName + dsSplit[i];
			}
			else
			{
				sDeviceLink = dsSplit[i];
			}
				
			sSystemLink = unGenericDpFunctions_getSystemName(sDeviceLink);
			unDistributedControl_isConnected(bConnected, sSystemLink);
			
			if(!bConnected)
			{
				bEnabled = FALSE;
			}
			else
			{
				sDeviceDpLink = unGenericDpFunctions_getWidgetDpName(sDeviceLink);
				if(!dpExists(sDeviceDpLink))
				{
					bEnabled = FALSE;
				}
			}

      sMenuLabel = dsSplit[i];
				
      if(sDpType == "AnaDig" || sDpType == "Analog")
				{
					if(bEnabled)
					{ // dev existing
						if(sSystemLink == unGenericDpFunctions_getSystemName(sDeviceName))
						{
							if(dpTypeName(sDeviceDpLink) == "Controller")
							{ // controller
								dpGet(sDeviceDpLink+".ProcessInput.StsReg01", b32Sts);
							  
								if(getBit(b32Sts, UN_STSREG01_ACTIVE))
								{
									sMenuLabel = "* " + sMenuLabel;
								}
							}
						}
					}
				}
      
				unGenericObject_addItemToPopupMenu(dsMenuList, sMenuLabel, iCste, bEnabled);
		}
	}
}

//-------------------------------------------------------------------------------------------------

/** Adds the trend actions to the right-click menu of a UNICOS device

@par Constraints
  . WinCC version: 2.12.1 or later
	. Pperating system: Windows 7, Windows Server 2008 RC2
	. Distributed system: yes
  
@par Usage
	External

@par WinCC managers
	NG, NV
  
@param sDeviceName       string,     INPUT, name of the datapoint
@param sDpType           string,     INPUT, datapoint type
@param dsMenuConfig      dyn_string, INPUT, actions that need to be incorporated into the menu
@param dsAccessOk        dyn_string, INPUT, access rights for the different actions
@param dsMenuList        dyn_string, OUTPUT, the menu to which actions are added as items
@param bUseAlias         bool,       INPUT/OPTIONAL, true=use alias instead of DPE/false=use DPE (default)
*/
void unGenericObject_addTrendActionToMenu(string sDeviceName, string sDpType, dyn_string dsMenuConfig, dyn_string dsAccessOk, dyn_string &dsMenuList, bool bUseAlias=false)
{
	dyn_string dsAnalog, dsRange, exceptionInfo;
	int i, iCste = UN_POPUPMENU_TREND_CSTE, iMenuValue, pos;
  dyn_string dsDPEAnalog, dsDPEAlias, dsProxy, dsDPE, dsDPERange, dsCurveDPE;
  bool bProxy, bEnabled, bDynamicTrendExists;
  dyn_bool dbCurveVisible;
  dyn_string dsCurveColor;
  dyn_float dfCurveMin, dfCurveMax;
  dyn_string dsFaceplateDPE;
  string sClearDynTrendEnabled, sReopenDynTrendEnabled;
        
	if (dynContains(dsMenuConfig, UN_POPUPMENU_TREND_TEXT) > 0)
	{
		unGenericObject_GetTrendDPEList(sDpType, dsAnalog, dsRange, exceptionInfo);
    unGenericDpFunctions_getProxyDeviceList(sDeviceName, bProxy, dsProxy);
    unGenericObject_deviceGetAllTrendDPE(sDeviceName, bProxy, bUseAlias, dsProxy, 
                                         dsAnalog, dsRange, makeDynBool(), makeDynString(),
                                         makeDynFloat(), makeDynFloat(),
                                         dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                         dbCurveVisible, dsCurveColor, 
                                         dfCurveMin, dfCurveMax);

    // if a dynamic trend already exists, activate the clear and restore menu options
    bDynamicTrendExists	= dpExists("_"+myManNum()+"_"+UN_DEVICE_DYNAMICTREND);
    
		if(dynlen(dsDPEAnalog) > 0)
    {
			pos = dynContains(dsMenuList, "Device link(s)");
                        
			bEnabled = _unGenericObject_checkAccessRigth(fwAccessControl_CONFIG_AccessRight_DP, exceptionInfo);
      
			if(pos > 0) {
				if((pos-1) > 0) {
					if(dsMenuList[pos-1] != "SEPARATOR")
					{
						dynInsertAt(dsMenuList,"SEPARATOR", pos++);
					}
				}
				sClearDynTrendEnabled = (bDynamicTrendExists && bEnabled) ? "1" : "0";
				sReopenDynTrendEnabled = (bDynamicTrendExists) ? "1" : "0";
				dynInsertAt(dsMenuList,"PUSH_BUTTON, Clear dynamic trend, "+UN_POPUPMENU_TREND_CSTE+", " + sClearDynTrendEnabled, pos++);
				dynInsertAt(dsMenuList,"PUSH_BUTTON, Re-open dynamic trend, "+UN_POPUPMENU_TREND_REOPEN+", " + sReopenDynTrendEnabled, pos++);
				dynInsertAt(dsMenuList,"CASCADE_BUTTON, Add to dynamic trend, 1", pos++);
				dynInsertAt(dsMenuList,"CASCADE_BUTTON, Single DPE trend, 1", pos++);
			}
			else {
					pos = dynContains(dsMenuList, "Info");
                        
					bEnabled = _unGenericObject_checkAccessRigth(fwAccessControl_CONFIG_AccessRight_DP, exceptionInfo);
			  
					if(pos > 0) {
						if((pos-1) > 0) {
							if(dsMenuList[pos-1] != "SEPARATOR")
							{
								dynInsertAt(dsMenuList,"SEPARATOR", pos++);
							}
						}
						sClearDynTrendEnabled = (bDynamicTrendExists && bEnabled) ? "1" : "0";
						sReopenDynTrendEnabled = (bDynamicTrendExists) ? "1" : "0";
						dynInsertAt(dsMenuList,"PUSH_BUTTON, Clear dynamic trend, "+UN_POPUPMENU_TREND_CSTE+", " + sClearDynTrendEnabled, pos++);
						dynInsertAt(dsMenuList,"PUSH_BUTTON, Re-open dynamic trend, "+UN_POPUPMENU_TREND_REOPEN+", " + sReopenDynTrendEnabled, pos++);
						dynInsertAt(dsMenuList,"CASCADE_BUTTON, Add to dynamic trend, 1", pos++);
						dynInsertAt(dsMenuList,"CASCADE_BUTTON, Single DPE trend, 1", pos++);
					}
					else {
			
					if(dynlen(dsMenuList) > 0) {
						if(dsMenuList[dynlen(dsMenuList)] != "SEPARATOR")
						{
							dynAppend(dsMenuList, "SEPARATOR");
						}
					}
			
					unGenericObject_addItemToPopupMenu(dsMenuList, "Clear dynamic trend", UN_POPUPMENU_TREND_CSTE, (bEnabled && bDynamicTrendExists));
					unGenericObject_addItemToPopupMenu(dsMenuList, "Re-open dynamic trend", UN_POPUPMENU_TREND_REOPEN, (bDynamicTrendExists) );
					dynAppend(dsMenuList,"CASCADE_BUTTON, Add to dynamic trend, 1");
					dynAppend(dsMenuList,"CASCADE_BUTTON, Single DPE trend, 1");
				}
			}
      
			dynAppend(dsMenuList,"Add to dynamic trend");
                 
			for(i = 1; i <= dynlen(dsDPEAnalog); i++)
      {
				iMenuValue = iCste+i;
                                
        // if proxy DPE & exits add
        if(bUseAlias)
        {
          if(dsDPEAlias[i] != "")
          {
            unGenericObject_addItemToPopupMenu(dsMenuList, dsDPEAlias[i], iMenuValue, TRUE);
          }
        }
        else
        {
          unGenericObject_addItemToPopupMenu(dsMenuList, dsDPEAnalog[i], iMenuValue, TRUE);
        }
			}
      
			dynAppend(dsMenuList,"Single DPE trend");
      
			for(i = 1; i <= dynlen(dsDPEAnalog); i++)
      {
				iMenuValue = 10000+iCste+i;
                                
        // if proxy DPE & exits add
        if(bUseAlias)
        {
          if(dsDPEAlias[i] != "")
          {
            unGenericObject_addItemToPopupMenu(dsMenuList, dsDPEAlias[i], iMenuValue, TRUE);
          }
        }
        else
        {
          unGenericObject_addItemToPopupMenu(dsMenuList, dsDPEAnalog[i], iMenuValue, TRUE);
        }
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------

/** Add an item to the menu with proper access

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  UI

@param dsMenuList         dyn_string, INPUT/OUTPUT, the menu to which an item will be attached
@param sMenuLabel         string,     INPUT, the label of the item
@param iMenuReturnValue   int,        INPUT, the return number of the item (has to be unique)
@param bGranted           bool,       INPUT, flag to enable or disable the item
*/
void unGenericObject_addItemToPopupMenu(dyn_string &dsMenuList, string sMenuLabel, int iMenuReturnValue, bool bEnabled)
{
    string sAccess = bEnabled ? "1" : "0";
    dynAppend(dsMenuList, "PUSH_BUTTON, " + sMenuLabel + ", " + iMenuReturnValue + ", "+sAccess);
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGenericObject_deviceGetAllTrendDPE
/**
Purpose: get all the device DPE to trend

Parameters:
  - deviceName, string, input, the device dp name
  - bProxy, bool, input, true=the device accept proxy/false=the device does not accept proxy
  - bUseAlias, bool, input, true=use alias instead of DPE/false=use DPE (default)
  - dsProxy, dyn_string, input, list of device proxy
  - dsAnalog, dyn_string, input, list of DPE to trend from the device type definition
  - dsRange, dyn_string, input, list of range DPE from the device type definition
  - dbVisibleIn, dyn_bool, output, list DPE trend scale visible from the device type definition
  - dsColorIn, dyn_string, input, list of DPE curve color from the device type definition
  - dfMinIn, dyn_float, output, list of DPE min value from the device type definition
  - dfMaxIn, dyn_float, output, list of DPE max value from the device type definition
  - dsFaceplateDPE, dyn_string, output, list of faceplate config DPE with min/max and proxy if any
  - dsDPEAnalog, dyn_string, output, list of DPE to trend with proxy if any
  - dsDeviceDPE, dyn_string, output, list of device DPE with proxy if any
  - dsDPEAlias, dyn_string, output, list of device DPE Alias to trend with proxy if any
  - dsDPERange, dyn_string, output, list of range device DPE with proxy if any
  - dsCurveDPE, dyn_string, output, list of device trend curve DPE with proxy if any
  - dbCurveVisible, dyn_bool, output, list of device trend curve DPE visibility with proxy if any
  - dsCurveColor, dyn_string, output, list of device trend curve DPE color with proxy if any
  - dfCurveMin, dyn_float, output, list of device trend curve DPE min value with proxy if any
  - dfCurveMax, dyn_float, output, list of device trend curve DPE max value with proxy if any
	
Usage: External

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
unGenericObject_deviceGetAllTrendDPE(string deviceName, bool bProxy, bool bUseAlias, dyn_string dsProxy, 
                                     dyn_string dsAnalog, dyn_string dsRange, 
                                     dyn_bool dbVisibleIn, dyn_string dsColorIn, dyn_float dfMinIn, dyn_float dfMaxIn,
                                     dyn_string &dsFaceplateDPE, 
                                     dyn_string &dsDPEAnalog, dyn_string &dsDeviceDPE, dyn_string &dsDPEAlias, 
                                     dyn_string &dsDPERange, dyn_string &dsCurveDPE, dyn_bool &dbCurveVisible, 
                                     dyn_string &dsCurveColor, dyn_float &dfCurveMin, dyn_float &dfCurveMax)
{
  int i, len;
  bool bExists;
  dyn_string dsAlias;
  dyn_string dsDPE, dsDevDPE, dsDevRange, dsDeviceCurve;
  string sDeviceAlias = unGenericDpFunctions_getAlias(deviceName);
  string sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  dyn_bool dbReturnCurveVisible, dbVisible=dbVisibleIn;
  dyn_string dsReturnCurveColor, dsColor=dsColorIn, dsReturnFaceplateDPE;
  dyn_float dfReturnCurveMin, dfReturnCurveMax, dfMin=dfMinIn, dfMax=dfMaxIn;
  
  len = dynlen(dsAnalog);
  while(dynlen(dbVisible)<=len)
    dynAppend(dbVisible, false);
  while(dynlen(dsColor)<=len)
    dynAppend(dsColor, "");
  while(dynlen(dfMin)<=len)
    dynAppend(dfMin, 0);
  while(dynlen(dfMax)<=len)
    dynAppend(dfMax, 0);
    
  for(i=1;i<=len;i++) {
    bExists = unGenericObject_deviceGetTrendDPE(sSystemName, sDeviceAlias, deviceName, bProxy, bUseAlias, dsProxy, 
                                                dsAnalog[i], dsRange[i], dbVisible[i], dsColor[i], dfMin[i], dfMax[i],
                                                dsDPE, dsDevDPE, dsAlias, dsDevRange, dsDeviceCurve,
                                                dbReturnCurveVisible, dsReturnCurveColor, dfReturnCurveMin, dfReturnCurveMax,
                                                dsReturnFaceplateDPE);
    if(bExists) {
      dynAppend(dsDPEAnalog, dsDPE);
      dynAppend(dsDPEAlias, dsAlias);
      dynAppend(dsDeviceDPE, dsDevDPE);
      dynAppend(dsDPERange, dsDevRange);
      dynAppend(dsCurveDPE, dsDeviceCurve);
      dynAppend(dbCurveVisible, dbReturnCurveVisible);
      dynAppend(dsCurveColor, dsReturnCurveColor);
      dynAppend(dfCurveMin, dfReturnCurveMin);
      dynAppend(dfCurveMax, dfReturnCurveMax);
      dynAppend(dsFaceplateDPE, dsReturnFaceplateDPE);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGenericObject_deviceGetTrendDPE
/**
Purpose: get one device DPE to trend

Parameters:
  - sSystemName, string, input, the device system name
  - sDeviceAlias, string, input, the device name
  - deviceName, string, input, the device dp name
  - bProxy, bool, input, true=the device accept proxy/false=the device does not accept proxy
  - bUseAlias, bool, input, true=use alias instead of DPE/false=use DPE (default)
  - dsProxy, dyn_string, input, list of device proxy
  - sTrendDPE, string, input, the DPE to trend from the device type definition
  - sRangeDPE, string, input, the range DPE from the device type definition
  - bVisible, bool, output, the DPE trend scale visible from the device type definition
  - sColor, string, input, list of DPE curve color from the device type definition
  - fMin, float, output, the DPE min value from the device type definition
  - fMax, float, output, the DPE max value from the device type definition
  - dsMenuDPE, dyn_string, output, list of DPE to trend with proxy if any to use in the pop-up menu
  - dsDeviceDPE, dyn_string, output, list of device DPE with proxy if any
  - dsDeviceDPEAlias, dyn_string, output, list of device DPE Alias to trend with proxy if any
  - dsDPERange, dyn_string, output, list of range device DPE with proxy if any
  - dsCurveDPE, dyn_string, output, list of device trend curve DPE with proxy if any
  - dbCurveVisible, dyn_bool, output, list of device trend curve DPE visibility with proxy if any
  - dsCurveColor, dyn_string, output, list of device trend curve DPE color with proxy if any
  - dfCurveMin, dyn_float, output, list of device trend curve DPE min value with proxy if any
  - dfCurveMax, dyn_float, output, list of device trend curve DPE max value with proxy if any
  - dsFaceplateDPE, dyn_string, output, list of faceplate config DPE with min/max and proxy if any
  - return value, bool output, true=the device DPE exists/false=the device DPE does not exists
	
Usage: External

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
bool unGenericObject_deviceGetTrendDPE(string sSystemName, string sDeviceAlias, string deviceName, bool bProxy, bool bUseAlias, dyn_string dsProxy,
                                       string sTrendDPE, string sRangeDPE, 
                                       bool bVisible, string sColor, float fMin, float fMax,
                                       dyn_string &dsMenuDPE, 
                                       dyn_string &dsDeviceDPE, dyn_string &dsDeviceDPEAlias, dyn_string &dsDPERange, 
                                       dyn_string &dsCurveDPE, dyn_bool &dbCurveVisible, 
                                       dyn_string &dsCurveColor, dyn_float &dfCurveMin, dyn_float &dfCurveMax,
                                       dyn_string &dsFaceplateDPE)
{
  int iPos;
  bool bExists;
  dyn_string dsAlias, dsReturnDPE, dsReturnDPERange, dsReturnCurveDPE;
  dyn_string dsDPE;
  int i, len;
  string sDeviceDPE, sDeviceRangeDPE;
  dyn_bool dbReturnCurveVisible;
  dyn_string dsReturnCurveColor, dsReturnFaceplateDPE;
  dyn_float dfReturnCurveMin, dfReturnCurveMax;
  
  iPos = strpos(sTrendDPE, UN_PROXY_DELIMITER);
  if(iPos > 0) { // proxy DPE
    bExists = unGenericObject_deviceGetProxyTrendDPE(sSystemName, deviceName, bProxy, bUseAlias, dsProxy, 
                                                     sTrendDPE, sRangeDPE, bVisible, sColor, fMin, fMax,
                                                     dsReturnDPE, dsDPE, dsAlias, dsReturnDPERange, dsReturnCurveDPE,
                                                     dbReturnCurveVisible, dsReturnCurveColor, dfReturnCurveMin, 
                                                     dfReturnCurveMax, dsReturnFaceplateDPE);
  }
  else { 
    // get range
    if(strpos(sRangeDPE, UN_PROXY_DELIMITER) > 0)
    {
      unGenericObject_deviceGetFirstProxyRangeDPE(sSystemName, bProxy, dsProxy, sRangeDPE, sDeviceRangeDPE);
    }
    else
      bExists = _unGenericObject_deviceGetTrendDPE(deviceName, sRangeDPE, sDeviceRangeDPE);
    bExists = _unGenericObject_deviceGetTrendDPE(deviceName, sTrendDPE, sDeviceDPE);
    dsDPE = makeDynString(sDeviceDPE);
    dsReturnDPERange = makeDynString(sDeviceRangeDPE);
    dsReturnDPE = makeDynString(sTrendDPE);
    dsReturnFaceplateDPE = makeDynString(sTrendDPE);
    dsReturnCurveDPE = makeDynString(sSystemName+sDeviceAlias+"."+sTrendDPE);
    dbReturnCurveVisible = makeDynBool(bVisible);
    dsReturnCurveColor = makeDynString(sColor);
    dfReturnCurveMin = makeDynFloat(fMin);
    dfReturnCurveMax = makeDynFloat(fMax);
    if(bUseAlias & bExists) {
      len = dynlen(dsDPE);
      for(i=1;i<=len;i++) {
        dsAlias[i] = dpGetAlias(dsDPE[i]);
//        if(dsAlias[i] == "")
//          dsAlias[i] = dsDPE[i];
      }
    }
  }
  dsMenuDPE = dsReturnDPE;
  dsDeviceDPEAlias = dsAlias;
  dsDeviceDPE = dsDPE;
  dsDPERange = dsReturnDPERange;
  dsCurveDPE = dsReturnCurveDPE;
  dbCurveVisible = dbReturnCurveVisible;
  dsCurveColor = dsReturnCurveColor;
  dfCurveMin = dfReturnCurveMin;
  dfCurveMax = dfReturnCurveMax;
  dsFaceplateDPE = dsReturnFaceplateDPE;
  
  return bExists;
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGenericObject_deviceGetFirstProxyRangeDPE
/**
Purpose: get the first proxy DPE corresponding to the range, this function is called when the range DPE is a proxy and
the trend DPE is a device DPE, the function searches for the first proxy of the given corresponding type

Parameters:
  - sSystemName, string, input, the device system name
  - bProxy, bool, input, true=the device accept proxy/false=the device does not accept proxy
  - dsProxy, dyn_string, input, list of device proxy
  - sRangeDPE, string, input, the range DPE from the device type definition
  - sDeviceRangeDPE, string, output, list of range device DPE with proxy if any
	
Usage: External

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
unGenericObject_deviceGetFirstProxyRangeDPE(string sSystemName, bool bProxy, dyn_string dsProxy, string sRangeDPE, 
                                       string &sDeviceRangeDPE)
{
  string sReturnDPERange;
  bool bExists;
  int i, len;
  string sProxyDPE;
  string sType, sDP;
  
  // get the first corresponding proxy
  if(bProxy)
  {
    sProxyDPE = "."+substr(sRangeDPE, strpos(sRangeDPE, UN_PROXY_DELIMITER)+strlen(UN_PROXY_DELIMITER), strlen(sRangeDPE));
    sType = substr(sRangeDPE, 0, strpos(sRangeDPE, UN_PROXY_DELIMITER));
    len = dynlen(dsProxy);
    for(i=1;i<=len;i++)
    {
      unGenericDpFunctions_getProxyDP(sSystemName, dsProxy[i], sDP);
      if(dpExists(sDP+sProxyDPE))
      {
        if(sType == dpTypeName(sDP)) {
          sReturnDPERange = sDP+sProxyDPE;
          i = len+1;
        }
      }
    }
  }
  sDeviceRangeDPE=sReturnDPERange;
}

//---------------------------------------------------------------------------------------------------------------------------------------
// _unGenericObject_deviceGetTrendDPE
/**
Purpose: get the trend DPE, this function is used to get the trend DPE or range DPE

Parameters:
  - deviceName, string, input, the device DP name
  - sDPE, string, input, the DPE to look for
  - sDeviceDPE, string, output, DPE if exists
  - return value, bool output, true=the device DPE exists/false=the device DPE does not exists
	
Usage: External

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
bool _unGenericObject_deviceGetTrendDPE(string deviceName, string sDPE, string &sDeviceDPE)
{
  bool bExists;
  string sReturnDPE;
//device DPE, ProcessInput or ProcessOutput  
  sReturnDPE = deviceName+".ProcessInput."+sDPE;
  bExists = dpExists(sReturnDPE);
  if(!bExists) {
    sReturnDPE = deviceName+".ProcessOutput."+sDPE;
    bExists = dpExists(sReturnDPE);
  }
  if(!bExists) {
    sReturnDPE = deviceName+"."+sDPE;
    bExists = dpExists(sReturnDPE);
  }
  sDeviceDPE = sReturnDPE;
  return bExists;
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGenericObject_deviceGetProxyTrendDPE
/**
Purpose: get all the proxy device DPE to trend

Parameters:
  - sSystemName, string, input, the device system name
  - deviceName, string, input, the device dp name
  - bProxy, bool, input, true=the device accept proxy/false=the device does not accept proxy
  - bUseAlias, bool, input, true=use alias instead of DPE/false=use DPE (default)
  - dsProxy, dyn_string, input, list of device proxy
  - sTrendDPE, string, input, the DPE to trend from the device type definition
  - sRangeDPE, string, input, the range DPE from the device type definition
  - bVisible, bool, output, the DPE trend scale visible from the device type definition
  - sColor, string, input, list of DPE curve color from the device type definition
  - fMin, float, output, the DPE min value from the device type definition
  - fMax, float, output, the DPE max value from the device type definition
  - dsDPEToShow, dyn_string, output, list of DPE to trend with proxy if any to use in the pop-up menu
  - dsDPE, dyn_string, output, list of device DPE with proxy if any
  - dsAlias, dyn_string, output, list of device DPE Alias to trend with proxy if any
  - dsDPERange, dyn_string, output, list of range device DPE with proxy if any
  - dsCurveDPE, dyn_string, output, list of device trend curve DPE with proxy if any
  - dbCurveVisible, dyn_bool, output, list of device trend curve DPE visibility with proxy if any
  - dsCurveColor, dyn_string, output, list of device trend curve DPE color with proxy if any
  - dfCurveMin, dyn_float, output, list of device trend curve DPE min value with proxy if any
  - dfCurveMax, dyn_float, output, list of device trend curve DPE max value with proxy if any
  - dsFaceplateDPE, dyn_string, output, list of faceplate config DPE with min/max and proxy if any
  - return value, bool output, true=the proxy device DPE exists/false=the proxy device DPE does not exists
	
Usage: External

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
bool unGenericObject_deviceGetProxyTrendDPE(string sSystemName, string deviceName, bool bProxy, bool bUseAlias, dyn_string dsProxy, 
                                             string sTrendDPE, string sRangeDPE, 
                                             bool bVisible, string sColor, float fMin, float fMax,
                                             dyn_string &dsDPEToShow, dyn_string &dsDPE,  
                                             dyn_string &dsAlias, dyn_string &dsDPERange, dyn_string &dsCurveDPE,
                                             dyn_bool &dbCurveVisible, dyn_string &dsCurveColor, 
                                             dyn_float &dfCurveMin, dyn_float &dfCurveMax,
                                             dyn_string &dsFaceplateDPE)
{
  bool bExists, bRange;
  int i, len;
  string sProxyDPE;
  string sType, sDP, sDeviceRangeDPE;
  dyn_string dsReturnDPE, dsReturnDPEToShow, dsReturnDPERange, dsReturnCurveDPE;
  dyn_bool dbReturnCurveVisible;
  dyn_string dsReturnCurveColor, dsReturnFaceplateDPE;
  dyn_float dfReturnCurveMin, dfReturnCurveMax;
  string sAlias;
  
  if(bProxy)
  {
    sProxyDPE = "."+substr(sTrendDPE, strpos(sTrendDPE, UN_PROXY_DELIMITER)+strlen(UN_PROXY_DELIMITER), strlen(sTrendDPE));
    sType = substr(sTrendDPE, 0, strpos(sTrendDPE, UN_PROXY_DELIMITER));
    len = dynlen(dsProxy);
    for(i=1;i<=len;i++)
    {
      unGenericDpFunctions_getProxyDP(sSystemName, dsProxy[i], sDP);
      if(dpExists(sDP+sProxyDPE))
      {
        if(sType == dpTypeName(sDP)) {
          dynAppend(dsReturnDPE, sDP+sProxyDPE);
          dynAppend(dsReturnDPEToShow, sType+UN_PROXY_DELIMITER+dsProxy[i]+sProxyDPE);
          dynAppend(dsReturnCurveDPE, sSystemName+dsProxy[i]+sProxyDPE);
          dynAppend(dbReturnCurveVisible, bVisible);
          dynAppend(dsReturnCurveColor, sColor);
          dynAppend(dfReturnCurveMin, fMin);
          dynAppend(dfReturnCurveMax, fMax);
          dynAppend(dsReturnFaceplateDPE, sTrendDPE);
          bExists = true;
          // get the range DPE
          if(strpos(sRangeDPE, UN_PROXY_DELIMITER) > 0){
            unGenericObject_deviceGetProxyRangeDPE(sSystemName, dsProxy, sDP, sType, sRangeDPE, sDeviceRangeDPE);
            dynAppend(dsReturnDPERange, sDeviceRangeDPE);
          }
          else
          {
            bRange = _unGenericObject_deviceGetTrendDPE(deviceName, sRangeDPE, sDeviceRangeDPE);
            dynAppend(dsReturnDPERange, sDeviceRangeDPE);
          }
        }
      }
    }
  }
  
  if(bUseAlias){
    len = dynlen(dsReturnDPE);
    for(i=1;i<=len;i++) {
      sAlias = dpGetAlias(dsReturnDPE[i]);
      if(sAlias == "")
        sAlias = dsReturnCurveDPE[i];
      dsAlias[i] = sType+UN_PROXY_DELIMITER+sAlias;
    }
  }
  dsDPE = dsReturnDPE;
  dsDPEToShow = dsReturnDPEToShow;
  dsDPERange = dsReturnDPERange;
  dsCurveDPE = dsReturnCurveDPE;
  dbCurveVisible = dbReturnCurveVisible;
  dsCurveColor = dsReturnCurveColor;
  dfCurveMin = dfReturnCurveMin;
  dfCurveMax = dfReturnCurveMax;
  dsFaceplateDPE = dsReturnFaceplateDPE;
  
//  DebugN(dsDPE, dsDPEToShow, dsAlias);
  return bExists;
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unGenericObject_deviceGetProxyRangeDPE
/**
Purpose: get all the proxy DPE corresponding to the range, this function is called whe the range and the trend DPE are
proxy, the function returns the range DPE of the given device proxy if of same type, otherwise it searches 
for the first proxy of the given corresponding type

Parameters:
  - sSystemName, string, input, the device system name
  - dsProxy, dyn_string, input, list of device proxy
  - sDP, string, input, the proxy device DP
  - sType, string, input, the device type of the proxy device
  - sRangeDPE, string, input, the range DPE from the device type definition
  - sDeviceRangeDPE, string, output, list of range device DPE with proxy if any
	
Usage: External

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
unGenericObject_deviceGetProxyRangeDPE(string sSystemName, dyn_string dsProxy, string sDP, string sType, string sRangeDPE, 
                                       string &sDeviceRangeDPE)
{
  string sRangeType = substr(sRangeDPE, 0, strpos(sRangeDPE, UN_PROXY_DELIMITER));
  // if same type use the proxy dp
  if(sRangeType == sType) {
    sDeviceRangeDPE = sDP+"."+substr(sRangeDPE, strpos(sRangeDPE, UN_PROXY_DELIMITER)+strlen(UN_PROXY_DELIMITER), strlen(sRangeDPE));
  }
  else // if different type, get the first proxy
    unGenericObject_deviceGetFirstProxyRangeDPE(sSystemName, true, dsProxy, sRangeDPE, sDeviceRangeDPE);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unGenericObject_checkAccessRigth
/**
Purpose: Check if the current user logged in has the correct access rigth

Parameters:
	- iType, int, input, type of access control priviledge to check
	- exceptionInfo, dyn_string, output, errors are returned here
        - return value, bool, output, true=priviledge granted/false=priviledge not granted
	
Usage: External

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
bool _unGenericObject_checkAccessRigth(int iType, dyn_string &exceptionInfo)
{
  dyn_mixed configuration;
  string sDpAccessRight;
  bool bGranted = false;
  
  fwAccessControl_getConfiguration(configuration, exceptionInfo);
  if(dynlen(exceptionInfo)<=0) {
  
  sDpAccessRight=configuration[iType];
  
//  DebugN(fwAccessControl_CONFIG_AccessRight_DP, getUserName(), "To create/delete dpoints you need",sDpAccessRight);
  
    fwAccessControl_isGranted(sDpAccessRight,bGranted,exceptionInfo);
    
  }
  if(dynlen(exceptionInfo)>0)
    bGranted = false;
  return bGranted;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unGenericObject_CheckAnalogAlarmType
/**
Purpose: check 5 ranges alarm

Parameters:
	- sDpe, string, input, dpe
	- iAlertType, int, output, alert type
	- exceptionInfo, dyn_string, output, for errors
	
Return in iAlertType : 1. DPCONFIG_ALERT_NONBINARYSIGNAL if Unicos 5 ranges alert_hdl is correct (and if sDpe is analogic)
					   2. DPCONFIG_ALERT_BINARYSIGNAL for boolean sDpe
					   3. DPCONFIG_NONE if no alert_hdl is defined or if alert_hdl is a bad analogic alarm

Usage: External

PVSS manager usage: NG, NV

Constraints:
	. Unicos constants for alert_hdl
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unGenericObject_CheckAnalogAlarmType(string sDpe, int &iAlertType, dyn_string &exceptionInfo)
{
	int i, iAlertTypeTemp = DPCONFIG_NONE, iRes, rangesNumber = -1, rangeOk;
	string rangeClass;
	
	if (dpExists(sDpe))
		{
		dpGet(sDpe + ":_alert_hdl.._type", iAlertTypeTemp);
		switch (iAlertTypeTemp)
			{
			case DPCONFIG_ALERT_BINARYSIGNAL:
				iAlertType = DPCONFIG_ALERT_BINARYSIGNAL;
				break;
			case DPCONFIG_ALERT_NONBINARYSIGNAL:
				dpGet(sDpe + ":_alert_hdl.._num_ranges", rangesNumber);
				for(i=1;i<=rangesNumber;i++)
					{
					rangeClass = "#";
					dpGet(sDpe + ":_alert_hdl." + i + "._class", rangeClass);
					if (rangeClass == "")
						{
						rangeOk = i;	// Range Ok
						}
					}
					
				switch(rangesNumber) {
					case UN_AIAO_ALARM_5_RANGES:
						if(rangeOk == UN_AIAO_ALARM_5_RANGE_OK) {
							iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
							g_iAlertRange = UN_AIAO_ALARM_5_HH_H_L_LL;
						}
						break;
					case UN_AIAO_ALARM_4_RANGES:
						switch(rangeOk) {
							case UN_AIAO_ALARM_4_HH_H_L_RANGE_OK:
								iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
								g_iAlertRange = UN_AIAO_ALARM_4_HH_H_L;
								break;
							case UN_AIAO_ALARM_4_H_L_LL_RANGE_OK:
								iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
								g_iAlertRange = UN_AIAO_ALARM_4_H_L_LL;
								break;

						}
						break;
					case UN_AIAO_ALARM_3_RANGES:
						switch(rangeOk) {
							case UN_AIAO_ALARM_3_H_L_RANGE_OK:
								iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
								g_iAlertRange = UN_AIAO_ALARM_3_H_L;
								break;
							case UN_AIAO_ALARM_3_HH_H_RANGE_OK:
								iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
								g_iAlertRange = UN_AIAO_ALARM_3_HH_H;
								break;
							case UN_AIAO_ALARM_3_LL_L_RANGE_OK:
								iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
								g_iAlertRange = UN_AIAO_ALARM_3_LL_L;
								break;
						}
						break;
					case UN_AIAO_ALARM_2_RANGES:
						switch(rangeOk) {
							case UN_AIAO_ALARM_2_H_RANGE_OK:
								iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
								g_iAlertRange = UN_AIAO_ALARM_2_H;
								break;
							case UN_AIAO_ALARM_2_L_RANGE_OK:
								iAlertType = DPCONFIG_ALERT_NONBINARYSIGNAL;
								g_iAlertRange = UN_AIAO_ALARM_2_L;
								break;
						}
						break;
					default:
						iAlertType = DPCONFIG_NONE;
						break;
				}
				break;
			case DPCONFIG_NONE:
			default:
				iAlertType = DPCONFIG_NONE;
				break;
			}
		}
	else
		{
		fwException_raise(exceptionInfo, "ERROR", "unGenericObject_CheckAnalogAlarmType: " + sDpe + getCatStr("unGeneration", "DPDOESNOTEXIST"), "");
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_SendPulse
/**
Purpose: send a pulse on a specified bit 

Parameters:
	sDpName: string, input, object name
	sDpe: string, input, Dpe name
	bitPosition: string, input, the pulse position in ManReg (see Constants)
	exceptionInfo: dyn_string, output, details of any exceptions are returned here
	alreadySent: bool, output, set to true if ManReg bit is already equal to 1 at the beginning of the function

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. PVSS version: 3.0
	. operating system: XP, W2000 and Linux, but tested only under Wds.
	. distributed system: yes but not tested.
*/

unGenericObject_SendPulse(string sDpName, string sDpe, unsigned bitPosition, dyn_string &exceptionInfo, bool &alreadySent)
{
string RegName, dpType, alias;
bit32 RegValue;
dyn_string exceptionInfoTemp;
errClass exceptionLog;

	alreadySent = false;

// 1. Send 1 to the specified bit
	RegName = unGenericDpFunctions_getDpName(sDpName) + sDpe;
	if (dpGet(RegName, RegValue) == -1)
		{
		fwException_raise(exceptionInfo,"ERROR","unGenericObject_sendPulse : " + RegName + getCatStr("unPVSS","DPGETFAILED"),"");
		}
	else
		{
		if (getBit(RegValue, bitPosition) != 1)	// Send pulse only if bit isn't already equal to 1
			{
			setBit(RegValue, bitPosition, 1);
			if (dpSet(RegName,RegValue) == -1)
				{
				fwException_raise(exceptionInfo,"ERROR","unGenericObject_sendPulse : " + RegName + getCatStr("unPVSS","DPSETFAILED"),"");
				}
			}
		else										// Pulse has already be sent
			{
			alias = unGenericDpFunctions_getAlias(sDpName);
			dynClear(exceptionInfoTemp);
			unSendMessage_toAllUsers(unSendMessage_getDeviceDescription(unGenericDpFunctions_getDpName(sDpName)) + 
									 getCatStr("unOperator","WAITMANREGPULSE"),exceptionInfoTemp);
			if (dynlen(exceptionInfoTemp) > 0)
				{
                  switch( myManType() )
                  {
                    case UI_MAN:
       	 			  fwExceptionHandling_display(exceptionInfoTemp);
                      break;

                    case CTRL_MAN:
                      exceptionLog = makeError("", PRIO_SEVERE, ERR_CONTROL, 0, "unGenericObject_SendPulse()", exceptionInfoTemp);
                      throwError (exceptionLog);
                      break;

                    default:
                    DebugTN("unGenericObject_SendPulse() -> exception: ", exceptionInfoTemp);
                    break;
                  }
				}
			alreadySent = true;
			}
		}
	
// 2. Wait	
	unGenericObject_WaitPulse();
	
// 3. Send 0 to the specified bit
	if (dpGet(RegName, RegValue) == -1)			// Get current ManReg01 value (value could have changed !)
		{
		fwException_raise(exceptionInfo,"ERROR","unFaceplateButtons_sendPulse : " + RegName + getCatStr("unPVSS","DPGETFAILED"),"");
		}
	else
		{
		if (getBit(RegValue, bitPosition) != 0)	// Send pulse only if bit isn't already equal to 1
			{
			setBit(RegValue, bitPosition, 0);
			if (dpSet(RegName,RegValue) == -1)
				{
				fwException_raise(exceptionInfo,"ERROR","unFaceplateButtons_sendPulse : " + RegName + getCatStr("unPVSS","DPSETFAILED"),"");
				}
			}
		}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_setMaskEvent
/**
Purpose: mask/unmask the event

Parameters:
		sDpName: string, input, the device dpname
		iMask: integer, input, the event mask value
		exceptionInfo: dyn_string, output, the error is returned here
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
	
@reviewed 2018-08-02 @whitelisted{FalsePositive}

*/

unGenericObject_setMaskEvent(string sDpName, int iMask, dyn_string dsDpe, dyn_string &exceptionInfo)
{
	string sSystemName, sPlcName, sVersion;
	
// set the mask
	unConfigGenericFunctions_setMaskEvent(sDpName, iMask, dsDpe, exceptionInfo);

//	get the FE dp name set the version to mask/unmask event if DPE exists and = ""
// get the systemName
	sSystemName = unGenericDpFunctions_getSystemName(sDpName);
// get the PLC name 
	sPlcName = unGenericObject_GetPLCNameFromDpName(sDpName);

	if(dpExists(sSystemName+sPlcName+".version.import")) {
		dpGet(sSystemName+sPlcName+".version.import", sVersion);
		if(sVersion == "") {
			sVersion = "mask/unmask event";
			dpSet(sSystemName+sPlcName+".version.import", sVersion);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_compareMapping
/**
Purpose: compare two mapping variables

Parameters:
		m1: mapping, input, first mapping variable
		m2: mapping, input, second mapping variable		
		bEqual: boolean, output, result
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.1 
	. operating system: Linux and XP
	. distributed system: yes.

@reviewed 2018-08-02 @whitelisted{FalsePositive}

*/

bool unGenericObject_compareMapping(const mapping m1, const mapping m2)
{
bool bEqual;
int iLen1, iLen2, i;
anytype aValue1, aValue2;

	iLen1=mappinglen(m1);
	iLen2=mappinglen(m2);
	
	bEqual=(iLen1==iLen2);
	for(i=1; i<=iLen1 && bEqual; i++)
		{
		aValue1=mappingGetValue(m1, i);
		aValue2=mappingGetValue(m2, i);					
		bEqual=(aValue1==aValue2);
		}
		
	return bEqual;
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_deselectDisplay
/**
Purpose: temporary function, deslect a TEXT_FIELD

Parameters:
  sShape: string, input, graphical element name
		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.1 
	. operating system: Linux and XP
	. distributed system: yes.
*/
unGenericObject_deselectDisplay(string sShape)
{
  string sCol;
  getValue(sShape, "backCol", sCol);
  setMultiValue(sShape, "backCol", sCol, sShape, "select", 0,0);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_InitializeTrendConfig
/**
Purpose: get from the PVSS database the faceplate trend configuration

Parameters:
  sDpType: string, input, device type
  exceptionInfo: dyn_string, output, error reported here
  return value: dyn_string, the faceplate trend configuration
		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
dyn_string unGenericObject_InitializeTrendConfig(string sDpType, dyn_string &exceptionInfo)
{
  dyn_string dsData;
  
  if(dpExists(sDpType+"_trendConfiguration"))
  {
    dpGet(sDpType+"_trendConfiguration"+".dsTrendConfig", dsData);
  }
  return dsData;
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_getDeviceTrendConfig
/**
Purpose: get the faceplate trend configuration

Parameters:
  sDpType: string, input, device type
  dsDataConfig: dyn_string, output, the faceplate trend configuration
  exceptionInfo: dyn_string, output, error reported here
		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
unGenericObject_getDeviceTrendConfig(string sDpType, dyn_string &dsDataConfig, dyn_string &exceptionInfo)
{
  int i, length, iObject;
  dyn_string dsTemp, exceptionInfoTemp;
  
  dynClear(dsDataConfig);
  if (globalExists("g_dsObjectList"))
  {
    if (dynlen(g_dsObjectList) <= 0)
    {
      unGenericObject_InitializeObjects(exceptionInfoTemp);
    }
  }
  else
  {
    unGenericObject_InitializeObjects(exceptionInfoTemp);
  }
  iObject = dynContains(g_dsObjectList, sDpType);
  if (iObject <= 0)
  {
    if(mappingHasKey(g_mFrontEndConfigs, sDpType)) {
      dsDataConfig = g_mFrontEndTrendConfig[sDpType];
    }
    else
      fwException_raise(exceptionInfo, "ERROR", "unGenericObject_getDeviceTrendConfig: unknown device type", "");
  }
  else
  {
    dsDataConfig = g_ddsDeviceTrendConfig[iObject];
  }
}
    
//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_getDeviceDpeTrendConfig
/**
Purpose: get the faceplate DPE trend configuration of one device trend config

Parameters:
  sData: string, input, one trend config
  dsDataConfig: string, output, trend config name
  dsAnalog: dyn_string, output, list of DPE to trend
  dsRange: dyn_string, output, list of range DPE
  dsColor: dyn_string, output, list of curve color
  dfMax: dyn_float, output, list of Max of DPE curve
  dfMin: dyn_float, output, list of Min of DPE curve
  dbYScaleVisible: dyn_bool, output, curve scale visibility
  sTimeRange: string, output, time range of the trend
  exceptionInfo: dyn_string, output, error reported here
		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
unGenericObject_getDeviceDpeTrendConfig(string sData, string &sName, dyn_string &dsAnalog, dyn_string &dsRange, 
                                        dyn_string &dsColor, dyn_float &dfMax, dyn_float &dfMin, 
                                        dyn_bool &dbYScaleVisible, string &sTimeRange, dyn_string &exceptionInfo)
{
  dyn_string dsTemp;
  int iDpe, lenDpe;
  dyn_string dsTempDpe, dsTempDpeRange, dsTempColor, dsTempMin, dsTempMax, dsTempVisibility;
  float fMin, fMax;
  bool bVisibility;
  
  dsTemp = strsplit(sData, ";");
  while(dynlen(dsTemp) < 8)
    dynAppend(dsTemp, "");
  if(dsTemp[1] != "") {
    sName = dsTemp[1];
    if(dsTemp[2] =="")
      dsTemp[2] = "0.0.1";
    sTimeRange = dsTemp[2];
    dsTempDpe = strsplit(dsTemp[3], "|");
    lenDpe = 8;
    while(dynlen(dsTempDpe) < lenDpe)
      dynAppend(dsTempDpe, "");
    dsTempDpeRange = strsplit(dsTemp[4], "|");
    while(dynlen(dsTempDpeRange) < lenDpe)
      dynAppend(dsTempDpeRange, "");
    dsTempColor = strsplit(dsTemp[5], "|");
    while(dynlen(dsTempColor) < lenDpe)
      dynAppend(dsTempColor, "_Button");
    dsTempMin = strsplit(dsTemp[6], "|");
    while(dynlen(dsTempMin) < lenDpe)
      dynAppend(dsTempMin, "0");
    dsTempMax = strsplit(dsTemp[7], "|");
    while(dynlen(dsTempMax) < lenDpe)
      dynAppend(dsTempMax, "0");
    dsTempVisibility = strsplit(dsTemp[8], "|");
    while(dynlen(dsTempVisibility) < lenDpe)
      dynAppend(dsTempVisibility, "FALSE");
    for(iDpe = 1;iDpe<=lenDpe;iDpe++) {
      if(dsTempDpe[iDpe] != "") {
        dynAppend(dsAnalog, dsTempDpe[iDpe]);
        if(dsTempDpeRange[iDpe] == "")
          dsTempDpeRange[iDpe] = "--- no device data ---";
        dynAppend(dsRange, dsTempDpeRange[iDpe]);
        if(dsTempColor[iDpe] == "")
          dsTempColor[iDpe] = "_Button";
        dynAppend(dsColor, dsTempColor[iDpe]);
        sscanf(dsTempMin[iDpe], "%f", fMin);
        sscanf(dsTempMax[iDpe], "%f", fMax);
        if(fMin >=fMax) {
          fMin = 0.0;
          fMax = 0.0;
        }
        if(dsTempVisibility[iDpe] != "TRUE")
          bVisibility = false;
        else
          bVisibility = true;
        dynAppend(dfMin, fMin);
        dynAppend(dfMax, fMax);
        dynAppend(dbYScaleVisible, bVisibility);
      }
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// _unGenericObject_FaceplateTrend_getTimeRange
/**
Purpose: convert the time range from string ot dyn_int, [1] days, [2] hours, [3] minutes

Parameters:
  sTime: string, intput, time range of the trend
  retunr value: dyn_int, output, time as int: [1] days, [2] hours, [3] minutes
		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
dyn_int _unGenericObject_FaceplateTrend_getTimeRange(string sTime)
{
  dyn_string dsTemp = strsplit(sTime,".");
  dyn_int diRet;
  
  while(dynlen(dsTemp) <3)
    dynAppend(dsTemp, "");
  
  sscanf(dsTemp[1], "%d", diRet[1]);
  if(diRet[1] < 0)
    diRet[1] = 0;
  if(diRet[1] > 365)
    diRet[1] = 365;
  sscanf(dsTemp[2], "%d", diRet[2]);
  if(diRet[2] < 0)
    diRet[2] = 0;
  if(diRet[2] > 23)
    diRet[2] = 23;
  sscanf(dsTemp[3], "%d", diRet[3]);
  if(diRet[3] < 0)
    diRet[3] = 0;
  if(diRet[3] > 59)
    diRet[3] = 59;
  if((diRet[1] ==0) && (diRet[2] == 0) && (diRet[3] == 0))
    diRet[3] = 1;
  return diRet;
}

//------------------------------------------------------------------------------------------------------------------------

// _unGenericObject_FaceplateTrendInit
/**
Purpose: create the $-param for the trend panels

Parameters:
  - sId: string, input, identifier
  - deviceSystemName: string, input, device system name
  - deviceName: string, input, device dp name
  - deviceAlias: string, input, deice alias
  - dsFaceplateDPE, dyn_string, output, list of faceplate config DPE with min/max and proxy if any
  - dsDPEAnalog, dyn_string, output, list of DPE to trend with proxy if any
  - dsDeviceDPE, dyn_string, output, list of device DPE with proxy if any
  - dsDPEAlias, dyn_string, output, list of device DPE Alias to trend with proxy if any
  - dsDPERange, dyn_string, output, list of range device DPE with proxy if any
  - dsDPECurve, dyn_string, output, list of device trend curve DPE with proxy if any
  - dsColorCurve, dyn_bool, output, list of device trend curve DPE visibility with proxy if any
  - dsCurveColor, dyn_string, output, list of device trend curve DPE color with proxy if any
  - dfMinCurve, dyn_float, output, list of device trend curve DPE min value with proxy if any
  - dfMaxCurve, dyn_float, output, list of device trend curve DPE max value with proxy if any
  - dsDpe: dyn_string, input, list of DPE faceplate min/max 
  - dfDpeMax: dyn_float, input, list of Max of DPE faceplate
  - dfDpeMin: dyn_float, input, list of Min of DPE faceplate
  - dsCurveParameters: dyn_string, output, $-param for the trend panel
  - bOk: bool, output, true=$-param ok/false: discard $-param
  - sTimeRange: string, inputput, time range of the trend
    		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
_unGenericObject_FaceplateTrendInit(string sId,
                                    string deviceSystemName,
                                    string deviceName,
                                    string deviceAlias,
                                    dyn_string dsFaceplateDPE,
                                    dyn_string dsDPEAnalog,
                                    dyn_string dsDeviceDPE,
                                    dyn_string dsDPEAlias,
                                    dyn_string dsDPERange,
                                    dyn_string dsDPECurve,
                                    dyn_bool dbVisibleCurve,
                                    dyn_string dsColorCurve,
                                    dyn_float dfMinCurve,
                                    dyn_float dfMaxCurve,
                                    dyn_string dsDpe,
                                    dyn_float dfDpeMax,
                                    dyn_float dfDpeMin,
                                    dyn_string &dsCurveParameters,
                                    bool &bOk,
                                    string sTimeRange = "0.1.0",
                                    string sFwTrendingConfigDpe="" )
{
  string sRangeMin, sRangeMax;
  float fRangeMin, fRangeMax;
  dyn_string dpeUnit=makeDynString("", "", "", "", "", "", "", "");
  string curveDPE, curveLegend, curveToolTipText, curveColor, curveRange, curveUnit, curveVisibility, axiiText, curvesType;
  string sZoomWindowTitle;
  int iRangeType, iRes, position, i, length, iLen;
  dyn_string dsCurveLegend, dsCurveColor, dsCurveRange, dsCurveDPE;
  dyn_string dsTempAnalog, dsTempColor, dsTempRange;
  dyn_int diTime;
  int iTime;
  dyn_string dsCurveToolTip;
  string sDPEAlias, sCurveDPE;
  bool bExists, bIsLog, bConfigExists;
  dyn_bool dbYScaleVisible=dbVisibleCurve;
  int iPos;
  string sToolTip;
  dyn_dyn_string plotData, exceptionInfo;
  int iPlotDataIndex;

  // 2. For each value to plot
  length = dynlen(dsDPEAnalog);
  if(length>fwTrending_TRENDING_MAX_CURVE)
    length = fwTrending_TRENDING_MAX_CURVE;
  for(i=1;i<=length;i++)
  {
    bExists = false;
  
    if(dpExists(dsDeviceDPE[i]))	// Good value to plot
      bExists = true;

    if(bExists)
    {
      // prepare dpe information in case it represents aliases
      if(fwTrending_isAlias(dsDPECurve[i]))
      {
        sCurveDPE = fwTrending_createAliasRepresentation(dsDPECurve[i]);
      }
      else
      {
        sCurveDPE = dsDPECurve[i];
      }
      dynAppend(dsCurveDPE, sCurveDPE);

      /*added by Herve */
      dpeUnit[i] = dpGetUnit(dsDeviceDPE[i]);
      /* end */
      sDPEAlias = dsDPEAlias[i];
      // remove first proxy constant if any
      iPos = strpos(sDPEAlias, UN_PROXY_DELIMITER);
      if(iPos > 0)
        sDPEAlias = substr(sDPEAlias, iPos+strlen(UN_PROXY_DELIMITER), strlen(sDPEAlias));
      sToolTip = sDPEAlias;

      if(sDPEAlias == "")
        sDPEAlias = dsDPEAnalog[i];

      // case DPEAlias was "" remove again --> if any
      iPos = strpos(sDPEAlias, UN_PROXY_DELIMITER);
      if(iPos > 0)
        sDPEAlias = substr(sDPEAlias, iPos+strlen(UN_PROXY_DELIMITER), strlen(sDPEAlias));
        //sDPEAlias = substr(sDPEAlias, strpos(sDPEAlias, ":")+1, strlen(sDPEAlias));
      dynAppend(dsCurveLegend, sDPEAlias);

      if(sToolTip != "") 
        dynAppend(dsCurveToolTip, sDPEAlias+" ["+dpeUnit[i]+"]");
      else
      {
        if(strpos(dsDPEAnalog[i], UN_PROXY_DELIMITER) > 0) // proxy
          dynAppend(dsCurveToolTip, dsDPECurve[i]+" ["+dpeUnit[i]+"]");
        else 
          dynAppend(dsCurveToolTip, deviceSystemName+deviceAlias+"."+dsDPEAnalog[i]+" ["+dpeUnit[i]+"]");
      }

      position = dynContains(dsDpe, dsFaceplateDPE[i]);
      if (position > 0) // A config exists for this dpe
      {

        sRangeMin = dfDpeMin[position];
        sRangeMax = dfDpeMax[position];
      }
      else // Search for pv_range
      {
        iRangeType = DPCONFIG_NONE;
        if(dsDPERange[i] != "")
        {
          if(dpExists(dsDPERange[i]))
          {
            dpGet(dsDPERange[i] + ":_pv_range.._type", iRangeType);
            if (iRangeType == DPCONFIG_MINMAX_PVSS_RANGECHECK)
            {
              iRes = dpGet(dsDPERange[i] + ":_pv_range.._min", fRangeMin,
                           dsDPERange[i] + ":_pv_range.._max", fRangeMax);
            }
          }
        }
        if ((iRangeType != DPCONFIG_MINMAX_PVSS_RANGECHECK) || (iRes < 0))
        {
          fRangeMin = dfMinCurve[i];		// Default values
          fRangeMax = dfMaxCurve[i];
        }
        sRangeMin = fRangeMin;
        sRangeMax = fRangeMax;
      }
      dynAppend(dsCurveRange, sRangeMin + ":" + sRangeMax);
      dynAppend(dsCurveColor, dsColorCurve[i]);
    }
  }
  if (dynlen(dsCurveDPE) == 0)
  {
    bOk = false;
  }
  else
  {
    while (dynlen(dsCurveLegend)<=fwTrending_TRENDING_MAX_CURVE)
    {
      dynAppend(dsCurveLegend, "");
    }

    while (dynlen(dsCurveToolTip)<=fwTrending_TRENDING_MAX_CURVE)
    {
      dynAppend(dsCurveToolTip, "");
    }

    while (dynlen(dsCurveRange)<=fwTrending_TRENDING_MAX_CURVE)
    {
      dynAppend(dsCurveRange, "");
    }

    while (dynlen(dsCurveDPE)<=fwTrending_TRENDING_MAX_CURVE)
    {
      dynAppend(dsCurveDPE, "");
    }

    while (dynlen(dsCurveColor)<=fwTrending_TRENDING_MAX_CURVE)
    {
      dynAppend(dsCurveColor, "");
    }

    while (dynlen(dpeUnit)<=fwTrending_TRENDING_MAX_CURVE)
    {
      dynAppend(dpeUnit, "");
    }

    while (dynlen(dbYScaleVisible)<=fwTrending_TRENDING_MAX_CURVE)
    {
      dynAppend(dbYScaleVisible, false);
    }

    /* new added by Herve */
    for(i=1;i<=fwTrending_TRENDING_MAX_CURVE;i++)
    {
      curveDPE += dsCurveDPE[i]+";";
      if(dsCurveDPE[i] != "") // was dsAnalog[i]
        curveVisibility +=true+";";
      else
        curveVisibility +=false+";";

      curveRange       += dsCurveRange[i]+";";
      curveLegend      += dsCurveLegend[i]+";";
      curveToolTipText += dsCurveToolTip[i]+";";
      curveUnit        += dpeUnit[i]+";";
      axiiText         += dbYScaleVisible[i]+";";
      //axiiText += false+";";
      curvesType       += fwTrending_PLOT_TYPE_STEPS+";";
      curveColor       += dsCurveColor[i]+";";
    }
	
    dynAppend(dsCurveParameters, "$dsCurveDPE:"          + curveDPE);
    dynAppend(dsCurveParameters, "$dsCurveLegend:"       + curveLegend);
    dynAppend(dsCurveParameters, "$dsCurveToolTipText:"  + curveToolTipText);
    dynAppend(dsCurveParameters, "$dsCurveColor:"        + curveColor);
    dynAppend(dsCurveParameters, "$dsCurveRange:"        + curveRange);
    dynAppend(dsCurveParameters, "$dsUnit:"              + curveUnit);
    dynAppend(dsCurveParameters, "$dsCurveVisible:"      + curveVisibility);
    dynAppend(dsCurveParameters, "$dsCurveScaleVisible:" + axiiText);
    dynAppend(dsCurveParameters, "$dsCurvesType:"        + curvesType);
    /* end */
    sZoomWindowTitle = deviceAlias;
    // replace any . by _	
    strreplace(sZoomWindowTitle, ".", "_");
    // replace any / by _	
    strreplace(sZoomWindowTitle, "/", "_");
    // replace any \ by _	
    strreplace(sZoomWindowTitle, "\\", "_");

    sZoomWindowTitle = sZoomWindowTitle+"_"+sId;
    dynAppend(dsCurveParameters, "$ZoomWindowTitle:" + sZoomWindowTitle);

    /* new added by Herve */
    dynAppend(dsCurveParameters, "$sRefName:");
    dynAppend(dsCurveParameters, "$sDpName:");
    dynAppend(dsCurveParameters, "$fMinForLog:"           + fwTrending_MIN_FOR_LOG);
    dynAppend(dsCurveParameters, "$fMaxPercentageForLog:" + fwTrending_MAX_PERCENTAGE_FOR_LOG);	
    dynAppend(dsCurveParameters, "$bTrendLog:"            + FALSE);

    diTime = _unGenericObject_FaceplateTrend_getTimeRange(sTimeRange);
    iTime  = diTime[3]*60 + diTime[2]*3600 + diTime[1]*3600*24;
    dynAppend(dsCurveParameters, "$sTimeRange:"+ iTime);
    dynAppend(dsCurveParameters, "$sForeColor:FwTrendingTrendForeground");
    dynAppend(dsCurveParameters, "$sBackColor:FwTrendingTrendBackground");
    dynAppend(dsCurveParameters, "$bShowGrid:"+true);
    dynAppend(dsCurveParameters, "$bShowLegend:"+false);
    dynAppend(dsCurveParameters, "$templateParameters:");
    dynAppend(dsCurveParameters, "$iMarkerType:"+fwTrending_MARKER_TYPE_NONE);
    /* end */

    //try to add full fwTrending configs, if defined
    if(dpExists(sFwTrendingConfigDpe))
    {
      fwTrending_getPlot(sFwTrendingConfigDpe, plotData, exceptionInfo);
      plotData[fwTrending_PLOT_OBJECT_EXT_MIN_FOR_LOG]            = makeDynString(fwTrending_MIN_FOR_LOG);
      plotData[fwTrending_PLOT_OBJECT_EXT_MAX_PERCENTAGE_FOR_LOG] = makeDynString(fwTrending_MAX_PERCENTAGE_FOR_LOG);
      plotData[fwTrending_PLOT_OBJECT_EXT_UNITS_X]                = makeDynString("","","","","","","","");
      plotData[fwTrending_PLOT_OBJECT_EXT_TOOLTIPS_X]             = makeDynString("","","","","","","","");
      plotData[fwTrending_PLOT_OBJECT_EXT_MIN_MAX_RANGE_X]        = makeDynString("","","","","","","","");
      fwTrending_convertStringToDyn(curveUnit,        plotData[fwTrending_PLOT_OBJECT_EXT_UNITS],         exceptionInfo);
      fwTrending_convertStringToDyn(curveToolTipText, plotData[fwTrending_PLOT_OBJECT_EXT_TOOLTIPS],      exceptionInfo);
      fwTrending_convertStringToDyn(curveDPE,         plotData[fwTrending_PLOT_OBJECT_DPES],              exceptionInfo);
      fwTrending_convertStringToDyn(curveLegend,      plotData[fwTrending_PLOT_OBJECT_LEGENDS],           exceptionInfo);
      fwTrending_convertStringToDyn(curveRange,       plotData[fwTrending_PLOT_OBJECT_EXT_MIN_MAX_RANGE], exceptionInfo);
      fwTrending_convertStringToDyn(curveColor,       plotData[fwTrending_PLOT_OBJECT_COLORS],            exceptionInfo);
      //assume data coming is representing curve hidden state, we need to change it to curve visible state
      curveVisibility = "";
      for(i=1;i<=fwTrending_TRENDING_MAX_CURVE;i++)
      {
        plotData[fwTrending_PLOT_OBJECT_CURVES_HIDDEN][i] = !plotData[fwTrending_PLOT_OBJECT_CURVES_HIDDEN][i];
      }
      if(dynlen(plotData))
        _fwTrending_convertPlotDataToDollars(plotData,dsCurveParameters,exceptionInfo);
      dynAppend(dsCurveParameters, "$sRefName:");
      dynAppend(dsCurveParameters, "$sDpName:");
      dynAppend(dsCurveParameters, "$templateParameters:");
    }
    else
    {
      fwException_raise(exceptionInfo,"INFO", "_unGenericObject_FaceplateTrendInit - fwTrendingPlot config DPE does not exist: "+sFwTrendingConfigDpe+". Using basic configuration.","");
    }

    // Load the log settings from the device's configuration datapoint if they exist: sDpe + UN_UNICOS_CONFIGURATION has a key UN_DEVICE_LOG_SCALE_CONFIGURATION
    _unGenericObject_getDeviceTrendLogConfiguration(deviceName, bConfigExists, bIsLog);
    if( bConfigExists )
    {
      iLen = dynlen(dsCurveParameters);
      for( i = 1 ; i <= iLen ; i++ )
      {
        if( strpos(dsCurveParameters[i], "$bTrendLog:") >= 0 )
        {
          dsCurveParameters[i] = "$bTrendLog:" + bIsLog;
        }
      }
    }

    bOk = true;
  }
}

//------------------------------------------------------------------------------------------------------------------------

// _unGenericObject_getDeviceTrendLogConfiguration
/**
Purpose: get the device's configuration whether it's trend should show the log scale or not

@param sDp:            String, Input, device dp name
@param bConfigExists:  Bool,   Output, reports if config is defined TRUE/FALSE
@param bIsLog:         Bool,   Output, show log scale TRUE/FALSE

		
Usage: Internal function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
void _unGenericObject_getDeviceTrendLogConfiguration(string sDp, bool &bConfigExists, bool &bIsLog)
{
  int index;
  dyn_string dsKey, exceptionInfo;
  dyn_dyn_string ddsConfig;
  
  bConfigExists = FALSE;
  bIsLog        = FALSE;

  if( isFunctionDefined("unGenericDpFunctions_getUnicosConfiguration") )
  {
    unGenericDpFunctions_getUnicosConfiguration(sDp, dsKey, ddsConfig, exceptionInfo);

    for(index = 1; index <= dynlen(dsKey); index++)
    {
      if(strpos(dsKey[index], UN_DEVICE_LOG_SCALE_CONFIGURATION) == 0)
      {
        bConfigExists = TRUE;
        bIsLog        = (bool) ddsConfig[index][1];
        return;
      }
    }

  }
}



//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_TabFaceplateExtendedTrendInit
/**
Purpose: get faceplate trend and show it in tabulation

Parameters:
  sDpName: string, input, device dp name
  sDpType: string, output, device dp type
  sTabulationName: string, input, tabulation name
  sTrendPanel: string, input, trend panel name
  exceptionInfo: dyn_string, output, error reported here
		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
unGenericObject_TabFaceplateExtendedTrendInit(string sDpName, string sDpType, string sTabulationName, string sTrendPanel, dyn_string &exceptionInfo)
{
  string deviceName, sTimeRange;
  dyn_string dsAnalog, dsRange, dsColor, dsDpe, dsCurveParameters, dsData;
  /*added by Herve */
  string deviceSystemName, deviceAlias;
  dyn_bool dbYScaleVisible;
  dyn_float dfMax, dfMin, dfDpeMax, dfDpeMin;
  bool bOk;
  int i, len;
  /* end */	
  dyn_string dsDPEAnalog, dsDPEAlias, dsProxy, dsDPE, dsDPERange, dsCurveDPE;
  bool bProxy;
  dyn_bool dbCurveVisible;
  dyn_string dsCurveColor;
  dyn_float dfCurveMin, dfCurveMax;
  dyn_string dsFaceplateDPE;
  string sTabName, sTabTitle, sName;
  string sFwTrendingConfigDpe;

  // 1. Get range configuration
  deviceName = unGenericDpFunctions_getDpName(sDpName);
  unGenericDpFunctions_getFaceplateRange(deviceName, dsDpe, dfDpeMax, dfDpeMin);
  unGenericObject_getDeviceTrendConfig(sDpType, dsData, exceptionInfo);
  
  //1.1 get system and alias of the devide
  deviceSystemName = unGenericDpFunctions_getSystemName(sDpName);
  deviceAlias = unGenericDpFunctions_getAlias(sDpName);
  
  // list of trendName;d.h.m;dpelist;dperangelist/NONE;colorlist;minlist;maxlist;ScaleVisibilitylist
//DebugN(dsData);
  len=dynlen(dsData);
  for(i=1;i<=len;i++) {
    dynClear(dsAnalog);
    dynClear(dsRange);
    dynClear(dsColor);
    dynClear(dfMax);
    dynClear(dfMin);
    dynClear(dbYScaleVisible);
    dynClear(dsCurveParameters);
    dynClear(dsDPEAnalog);
    dynClear(dsDPE);
    dynClear(dsDPEAlias);
    dynClear(dsDPERange);
    dynClear(dsCurveDPE);
    dynClear(dbCurveVisible);
    dynClear(dsCurveColor);
    dynClear(dfCurveMin);
    dynClear(dfCurveMax);
    dynClear(dsFaceplateDPE);
    unGenericObject_getDeviceDpeTrendConfig(dsData[i], sName, dsAnalog, dsRange, dsColor, dfMax, dfMin, dbYScaleVisible, sTimeRange, exceptionInfo);
//DebugN(sName, dsAnalog, dsRange, dsColor, dfMax, dfMin, dbYScaleVisible);
    unGenericDpFunctions_getProxyDeviceList(deviceName, bProxy, dsProxy);
    unGenericObject_deviceGetAllTrendDPE(deviceName, bProxy, true, dsProxy, dsAnalog, dsRange, 
                                         dbYScaleVisible, dsColor, dfMin, dfMax, 
                                         dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                         dbCurveVisible, dsCurveColor, dfCurveMin, dfCurveMax);
    unGenericObject_getDeviceDpeTrendExtendedConfig(sDpType, sName, sFwTrendingConfigDpe, exceptionInfo);
    _unGenericObject_FaceplateTrendInit(i, deviceSystemName, deviceName, deviceAlias, 
                                        dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                        dbCurveVisible, dsCurveColor, dfCurveMin, dfCurveMax,
                                        dsDpe, dfDpeMax, dfDpeMin, 
                                        dsCurveParameters, bOk, sTimeRange, sFwTrendingConfigDpe);

    // 3. show tab
    if(bOk) {
      sTabName = "Trend"+i;
      sTabTitle = sName;
      setMultiValue(sTabulationName, "insertRegister", i, sTabulationName, "registerName", i, sTabName,
                    sTabulationName, "namedColumnHeader", sTabName, sTabTitle, 
                    sTabulationName, "registerPanel", i, sTrendPanel, dsCurveParameters);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_ButtonFaceplateExtendedTrendInit
/**
Purpose: get the faceplate trends for cascade button, WARNING, the cascade button is not initialised

Parameters:
  sDpName: string, input, device dp name
  sDpType: string, output, device dp type
  sButtonName: string, input, button name
  dsName: dyn_string, output, list of trend configuration name
  ddsCurveParameters: dyn_dyn_string, output, list of $-param
  exceptionInfo: dyn_string, output, error reported here
		
Usage: External function

PVSS manager usage: Ui

Constraints:
	. PVSS version: 3.6 
	. operating system: Linux and XP
	. distributed system: yes.
*/
unGenericObject_ButtonFaceplateExtendedTrendInit(string sDpName, string sDpType, string sButtonName, dyn_string &dsName, dyn_dyn_string &ddsCurveParameters, dyn_string &exceptionInfo)
{
  string deviceName, sTimeRange;
  dyn_string dsAnalog, dsRange, dsColor, dsDpe, dsCurveParameters, dsData;
  /*added by Herve */
  string deviceSystemName, deviceAlias;
  dyn_bool dbYScaleVisible;
  dyn_float dfMax, dfMin, dfDpeMax, dfDpeMin;
  bool bOk;
  int i, len;
  /* end */	
  dyn_string dsDPEAnalog, dsDPEAlias, dsProxy, dsDPE, dsDPERange, dsCurveDPE;
  bool bProxy;
  dyn_bool dbCurveVisible;
  dyn_string dsCurveColor;
  dyn_float dfCurveMin, dfCurveMax;
  dyn_string dsFaceplateDPE;
  string sFwTrendingConfigDpe;
	
  string sName;

  // 1. Get range configuration
  deviceName = unGenericDpFunctions_getDpName(sDpName);
  unGenericDpFunctions_getFaceplateRange(deviceName, dsDpe, dfDpeMax, dfDpeMin);
  unGenericObject_getDeviceTrendConfig(sDpType, dsData, exceptionInfo);
//DebugN(deviceName, dsDpe, dfDpeMax, dfDpeMin);  
  //1.1 get system and alias of the devide
  deviceSystemName = unGenericDpFunctions_getSystemName(sDpName);
  deviceAlias = unGenericDpFunctions_getAlias(sDpName);
  
  // list of trendName;d.h.m;dpelist;dperangelist/NONE;colorlist;minlist;maxlist;ScaleVisibilitylist
//DebugN(dsData);
  len=dynlen(dsData);
  for(i=1;i<=len;i++) {
    dynClear(dsAnalog);
    dynClear(dsRange);
    dynClear(dsColor);
    dynClear(dfMax);
    dynClear(dfMin);
    dynClear(dbYScaleVisible);
    dynClear(dsCurveParameters);
    dynClear(dsDPEAnalog);
    dynClear(dsDPE);
    dynClear(dsDPEAlias);
    dynClear(dsDPERange);
    dynClear(dsCurveDPE);
    dynClear(dbCurveVisible);
    dynClear(dsCurveColor);
    dynClear(dfCurveMin);
    dynClear(dfCurveMax);
    dynClear(dsFaceplateDPE);
    unGenericObject_getDeviceDpeTrendConfig(dsData[i], sName, dsAnalog, dsRange, dsColor, dfMax, dfMin, dbYScaleVisible, sTimeRange, exceptionInfo);
//DebugN(sName, dsAnalog, dsRange, dsColor, dfMax, dfMin, dbYScaleVisible);
    unGenericDpFunctions_getProxyDeviceList(deviceName, bProxy, dsProxy);
    unGenericObject_deviceGetAllTrendDPE(deviceName, bProxy, true, dsProxy, dsAnalog, dsRange, 
                                         dbYScaleVisible, dsColor, dfMin, dfMax, 
                                         dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                         dbCurveVisible, dsCurveColor, dfCurveMin, dfCurveMax);
//DebugN("!!!! Button", i, dsDpe, dsDPEAnalog, "!!!!");
    unGenericObject_getDeviceDpeTrendExtendedConfig(sDpType, sName, sFwTrendingConfigDpe, exceptionInfo);
    _unGenericObject_FaceplateTrendInit(i, deviceSystemName, deviceName, deviceAlias, 
                                        dsFaceplateDPE, dsDPEAnalog, dsDPE, dsDPEAlias, dsDPERange, dsCurveDPE,
                                        dbCurveVisible, dsCurveColor, dfCurveMin, dfCurveMax,
                                        dsDpe, dfDpeMax, dfDpeMin, 
                                        dsCurveParameters, bOk, sTimeRange, sFwTrendingConfigDpe);

    // 3. fill $-param
    if(bOk) {
      dynAppend(dsName, sName);
      dynAppend(ddsCurveParameters, dsCurveParameters);
      setValue(sButtonName, "insertItemId", "", 0, -1, sName, sName);
    }
  }
  if(len > 0)
    setValue(sButtonName, "enabled", true);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_TabFaceplateAddTabulationInit
/**
Purpose: add the panel given as parameter as the tabulation at the last position 

Parameters:
  sDpName: string, input, device dp name
  dyn_string dsAssociatedDp: list of associated dp which will have to be taken into account by the tabulation. 
                              e.g. in CPC it would be the list of DP representing the parent and children devices (IS-1381)
  bVisibleAdditionalDeviceOption: control the visibility of the checkbox "additional devices" in the filter area of the event and alert device faceplate tabulation
  sTabulationName: string, input, tabulation name
  sTabulationTitle: string, input, tabulation title
  sTrendPanel: string, input, trend panel name
  bLoadImmediate: bool, input, true to load the panel immediately in the tabulation, 
                               false to only add the tabulation without loading the panel 
                               if false the script associated to selectionChanged of the tab will be modified to make the content loaded once clicked
  bExtendSelectionChangedScript: bool, input, true to extend the selectionChanged script of the tabulation widget to handle the
                                  loading of the tabulation content automatically. False to not change the selectionChanged script, i.e
                                  in this case the user needs to do it manually. This option only makes sense if bLoadImmediate is set to false.
  exceptionInfo: dyn_string, output, error reported here
		
Example:
    To add an alarm tabulation to the device faceplate (to be placed in the init script of the faceplate): 
        unGenericObject_TabFaceplateAddTabulationInit(sDpName, 
                                                    makeDynString(),
                                                    true,
                                                    "Tab1", 
                                                    "vision/unAlertPanel/unAlertPanel_AES_for_FaceplateModule.pnl", 
                                                    "Alarm", false, true, exceptionInfo);
    Similarly, to add an event tabulation:
      unGenericObject_TabFaceplateAddTabulationInit(sDpName, 
                                                    makeDynString(),
                                                    true,
                                                    "Tab1", 
                                                    "vision/unEventList/unEventList_for_FaceplateModule.pnl", 
                                                    "Event", false, true, exceptionInfo);
                                                    
Usage: External function

PVSS manager usage: Ui

Constraints:
	. operating system: Linux and XP
	. distributed system: yes.
*/
unGenericObject_TabFaceplateAddTabulationInit(string sDpName, dyn_string dsAssociatedDp, bool bVisibleAdditionalDeviceOption, string sTabulationName, string sPanel, string sTabulationTitle, bool bLoadImmediate, bool bExtendSelectionChangedScript, dyn_string &exceptionInfo)
{
  int index;
  getValue(sTabulationName, "registerCount", index);  
  
  string sAssociatedDp;
  for( int i=1; i<=dynlen(dsAssociatedDp); i++)
  { 
    if (dpExists(dsAssociatedDp[i]))
    {
      sAssociatedDp =  dpSubStr(dsAssociatedDp[i], DPSUB_SYS_DP) + ";" + sAssociatedDp;
    }
  }
  
  int iWidth, iHeigth;
  getValue(sTabulationName, "size", iWidth, iHeigth);
  
  setMultiValue(sTabulationName, "insertRegister", index, sTabulationName, "registerName", index, sTabulationTitle,
                sTabulationName, "namedColumnHeader", sTabulationTitle, sTabulationTitle);
  
  if (bLoadImmediate == true)
  {
    setMultiValue(sTabulationName, "registerPanel", index, sPanel, makeDynString("$sDpName:"+sDpName, "$iWidth:"+iWidth, "$iHeigth:"+iHeigth, "$sAssociatedDp:"+sAssociatedDp, "$bVisibleOption:"+bVisibleAdditionalDeviceOption));  
  } else  if (bExtendSelectionChangedScript) //register a callback to selectionChanged to make the panel appears only once click on the tab
  {
    string sCodeSelectionChanged;
    getValue( sTabulationName, "script", "selectionChanged", sCodeSelectionChanged); 
    
    string sNewSourceCodeLine =  "_unGenericObject_TabFaceplateLoadTabContentWhenVisible( \""+sTabulationName+"\", \""+sTabulationTitle+"\",\""+sPanel+"\", \""+ sDpName+"\", \""+ sAssociatedDp+"\", \""+ bVisibleAdditionalDeviceOption+"\");";
    
    //We need to check if this is a new script or we are modifying a precedent one (e.g. another panel is already registered)
    if (strlen(sCodeSelectionChanged)>1)
    {
      strreplace( sCodeSelectionChanged, "main(){", "main(){\n"+sNewSourceCodeLine);    
    } else
    {
      sCodeSelectionChanged = "main(){\n";
      sCodeSelectionChanged += sNewSourceCodeLine+"\n";
      sCodeSelectionChanged += "}";
    }
    
    setValue( sTabulationName, "script", "selectionChanged", sCodeSelectionChanged);
    setValue(sTabulationName, "registerTooltip", index, "Click to load "+sTabulationTitle);
  }
  
}

/**
Purpose: add the panel to the tabulation once visible (used by unGenericObject_TabFaceplateAddTabulationInit) 

Parameters:
  sTabulationName: string, input, tabulation name
  sTabulationTitle: string, input, tabulation title
  sPanel: string, input,  panel name
  sDpName: string, input, name of the principal device for which this tab is opened
  sAssociatedDp: string, input, list of associated Dp
		
Usage: Internal function

PVSS manager usage: Ui

Constraints:
	. operating system: Linux and XP
	. distributed system: yes.
  
@reviewed 2018-09-17 @whitelisted{LiteralString}
**/
_unGenericObject_TabFaceplateLoadTabContentWhenVisible( string sTabulationName, string sTabulationTitle, string sPanel, string sDpName, string sAssociatedDp, bool bVisibleAdditionalDeviceOption)
{
  int iWidth, iHeigth;
  getValue(sTabulationName, "size", iWidth, iHeigth);
  
  int iCurrentTabIndex;
  getValue( sTabulationName, "activeRegister", iCurrentTabIndex);
  
  string sCurrentTabName;  
  getValue( sTabulationName, "registerHeader", iCurrentTabIndex, sCurrentTabName); 
  
  //use the tooltip to know if the panel has already been opened as we can not user getValue for registerPanel
  string sCurrentToolTip;
  getValue(sTabulationName, "registerTooltip", iCurrentTabIndex, sCurrentToolTip);
  
  if ( sCurrentTabName == sTabulationTitle && sCurrentToolTip!=sTabulationTitle)
  {
    dyn_string dsDollarParams = makeDynString("$sDpName:"+sDpName, "$iWidth:"+iWidth, "$iHeigth:"+iHeigth, "$sAssociatedDp:"+sAssociatedDp, "$bVisibleOption:"+bVisibleAdditionalDeviceOption);
    setValue(sTabulationName, "registerPanel", iCurrentTabIndex, sPanel, dsDollarParams);
    setValue(sTabulationName, "registerTooltip", iCurrentTabIndex, sTabulationTitle);
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_getProxyDeviceType
/**
Purpose: get the list of proxy device type of the given device type

Parameters:
  sType: string, input, device dp type
  dsProxyType: dyn_string, output, list of proxy device type
		
Usage: External function

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: Linux and XP
  . distributed system: yes.
*/
unGenericObject_getProxyDeviceType(string sType, dyn_string &dsProxyType)
{
  int iObject;
  dyn_string exceptionInfoTemp;
  
// 1. Initialize data if not already done
  if (globalExists("g_dsObjectList"))
  {
    if (dynlen(g_dsObjectList) <= 0)
    {
      unGenericObject_InitializeObjects(exceptionInfoTemp);
    }
  }
  else
  {
    unGenericObject_InitializeObjects(exceptionInfoTemp);
  }
// 2. Get object
  iObject = dynContains(g_dsObjectList, sType);
  if (iObject <= 0)
  {
//DebugN(sType, g_mFrontEndProxyList);
    if(mappingHasKey(g_mFrontEndProxyList, sType))
      dsProxyType = g_mFrontEndProxyList[sType];
    else
      dsProxyType = makeDynString();
  }
  else
  {
  // 3. Get select field
    dsProxyType = g_ddsObjectProxyList[iObject];
//DebugN("Found", g_ddsObjectProxyList);
  }
//DebugN(dsProxyType, dsWidgets, iObject);
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_trendConfigurationCleanDpe
/**
Purpose: check if the given text is in the list, if not reset it to the previous selected one 

Parameters:
  sType: string, input, device dp type
  dsProxyType: dyn_string, output, list of proxy device type
		
Usage: External function

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: Linux and XP
  . distributed system: yes.
*/
unGenericObject_trendConfigurationCleanDpe(string sText)
{
  int iPos = this.selectedPos;
  dyn_string dsItems=this.items();
  int iNew =dynContains(dsItems, sText);
  
  if(iPos > 0) {
    if(iNew<=0)
      this.text() = dsItems[iPos];
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unGenericObject_displaySystemAlarmTrend
/**
Purpose: show a trend widget with the systemAlarm stats DPE 

Parameters:
  sDpN: string, input, device dp name
		
Usage: External function

PVSS manager usage: Ui

Constraints:
  . PVSS version: 3.6 
  . operating system: Linux and XP
  . distributed system: yes.
*/
unGenericObject_displaySystemAlarmTrend(string sDpN)
{
  bool bOk;
  string sAlarm, sSystemName;
  dyn_string dsCurveParameters;
  int iX, iY;

  getCursorPosition(iX, iY);
  sAlarm=substr(sDpN, strpos(sDpN, "_unSystemAlarm_") + strlen("_unSystemAlarm_")); 
  sSystemName=unGenericDpFunctions_getSystemName(sDpN);
  
  _unGenericObject_FaceplateTrendInit("SystemAlarm",
                                      sSystemName,
                                      sDpN,
                                      sAlarm,
                                      makeDynString("alarmPerDay"), 
                                      makeDynString(sAlarm), 
                                      makeDynString(sDpN), 
                                      makeDynString(""), 
                                      makeDynString(sDpN), 
                                      makeDynString(sDpN),
                                      makeDynBool(true), 
                                      makeDynString("FwTrendingCurve1"),
                                      makeDynFloat(0.0),
                                      makeDynFloat(0.0),
                                      makeDynString(),
                                      makeDynFloat(),
                                      makeDynFloat(),
                                      dsCurveParameters, bOk,
                                      "7.0.0");

  if(bOk)
    ChildPanelOn(fwTrending_PANEL_PLOT_FACEPLATE_FULLCAPTION_WLAYOUT, sAlarm, dsCurveParameters, iX, iY);
//    ModuleOnWithPanel(substr(sAlarm,0,strpos(sAlarm,".statistic")), -1, -1, 10, 10, 1, 1, "Scale", fwTrending_PANEL_PLOT_FACEPLATE_FULLCAPTION, sAlarm, dsCurveParameters);
}    






//------------------------------------------------------------------------------------------------------------------------
// unGenericObject_selectDevice
/** selection of device from a widget
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui

@param deviceName  input, device name 
@param sDpType input, dp type
@param exceptionInfo output, errors are returned here
*/
unGenericObject_selectDevice(string deviceName, string sDpType, dyn_string &exceptionInfo)
{
  string sManager, localManager;
  bool granted, bLocked;
  int iRes;
  
  if(deviceName != "") {
    if(dpExists(deviceName + ".statusInformation.selectedManager"))
    {
      unGenericButtonFunctionsHMI_isAccessAllowed(deviceName, sDpType, UN_FACEPLATE_BUTTON_SELECT, granted, exceptionInfo);
      iRes = dpGet(deviceName + ".statusInformation.selectedManager:_lock._original._locked", bLocked,
               deviceName + ".statusInformation.selectedManager", sManager);
      if (iRes >= 0)
      {
        localManager = unSelectDeselectHMI_getSelectedState(bLocked, sManager);
        granted = granted && (localManager == "D");
      }
      if (granted)
      {
        unGenericObject_ButtonSelectAction(deviceName, true, exceptionInfo);
      }
    }
  }
}

/** The function is a proxy to WinCC OA shapeExists function that handle a case for Ctrl scripts

The function returns whether a graphics element exists in the panel.
In Ctrl shapeExists is not defined. For that case the function returns false.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui Ctrl

@param grephicsName a graphic element's name
@return does shape exist
*/
bool unGenericObject_shapeExists(string grephicsName)
{
    if (!isFunctionDefined("shapeExists")) return false;
    return shapeExists(grephicsName);
}

//@}


