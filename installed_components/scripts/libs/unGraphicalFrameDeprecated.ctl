//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_getDeviceIconFileList
/**
Purpose:
Get the list of icon files of the device. 6 files shall exist per device type.

	@param sDeviceType: string, input: device type
	@param imageClassParameters: string, output: the list of icon files of the device
	@param exceptionInfo: string, output: Details of any exceptions are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget
	. PVSS version: 3.6 
	. operating system: WXP and Linux.
	. distributed system: yes.

@deprecated 2018-08-02

*/
unGraphicalFrame_getDeviceIconFileList(string sDeviceType, string &imageClassParameters, dyn_string &exceptionInfo)
{

  FWDEPRECATED();

	switch(sDeviceType) {
		case UN_WINDOWTREE_ROOT_FOLDER:
		case UN_TRENDTREE_ROOT_FOLDER:
			imageClassParameters = getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenClose.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenOpen.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenPath.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithoutChildrenNotSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenSelected.bmp");
			break;
		case unTreeWidget_NODE:
		case unTreeWidget_NODE:
			imageClassParameters = getPath(PICTURES_REL_PATH,"node/iconWithChildrenClose.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"node/iconWithChildrenOpen.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"node/iconWithChildrenSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"node/iconWithChildrenPath.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"node/iconWithoutChildrenNotSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"node/iconWithChildrenSelected.bmp");
			break;
		case UN_WINDOWTREE_PANEL:
			imageClassParameters = getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenClose.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenOpen.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"_UnPanel/iconWithChildrenPath.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"_UnPanel/iconWithoutChildrenNotSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"_UnPanel/iconWithoutChildrenSelected.bmp");
			break;
		case UN_TRENTREE_PLOT:
		case UN_TRENTREE_PAGE:
			imageClassParameters = getPath(PICTURES_REL_PATH,"trend/iconWithChildrenClose.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"trend/iconWithChildrenOpen.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"trend/iconWithChildrenSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"trend/iconWithChildrenPath.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"trend/iconWithoutChildrenNotSelected.bmp") + ";" +
						   getPath(PICTURES_REL_PATH,"trend/iconWithoutChildrenSelected.bmp");
			break;
		default:
			fwException_raise (exceptionInfo, "WARNING", getCatStr("unTreeWidget", "UNKDEVICE")+ sDeviceType,"");
			break;
	}
}





//---------------------------------------------------------------------------------------------------------------------------------------

// unGraphicalFrame_select
/**
Purpose:
	This function is used to select or deselect a device from the graphical frame component, it calls the _unSelectDeselectHMI_select 
	function and sets the Application DP.
	
	@param sDpElementName: string, input, the dpElement to select
	@param bSelect: bool, input, true (1): select sDpElementName, false (0) deselect sDpElementName
	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. data point type needed: _UnApplication, _UnStatusInformation
	. the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
	must have a data point element of type _UnStatusInformation
	. data point: the following data point is needed, it must exist, no checking is done
		. _unSelectDeselect: of type _UnSelectDeselect 
		. _unApplication
	. PVSS version: 3.0 
	. operating system: WXP and Linux.
	. distributed system: yes.

@deprecated 2018-08-02

*/
unGraphicalFrame_select(string sDpElementName, bool bSelect, dyn_string &exceptionInfo)
{

  FWDEPRECATED();

	unGraphicalFrame_setSelectMessage(sDpElementName);
	unSelectDeselectHMI_select(sDpElementName, bSelect, exceptionInfo);
}