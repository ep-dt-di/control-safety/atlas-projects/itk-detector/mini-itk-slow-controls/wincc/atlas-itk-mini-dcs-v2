/**@name LIBRARY: unImpExpTree.ctl

@author: Daniel Davids and Herve Milcent (EN/ICE)

Creation Date: 20/08/2010

Modification History:

  22/06/2018: Mariusz
    - Deprecate _unImportTree_charsOfStringData() function, whitelist import and check tag functions  
  
  22/11/2013: Danny
    - IS-685: Using synchronized function unExportTree_uniqueTreeTagNames() to load read-only variables
    
  01/05/2011: Danny
    - IS-517: Authomatic conversion of Window/Trend Tree Xml files to the new format
    
  01/03/2011: Danny
    - IS-422: Modifications to use the new Trending Xml Structure

  01/11/2010: Danny
    - IS-297: Modifications to use PVSS' XML libraries

  25/10/2010: Herve
    - IS-421: Import/Export TrendTree/WindowTree: replace if required the local system name by #local in the DPE description
              modified function: unImportTree_FwTrendingPlot_checkTagSectionData, unImportTree_FwTrendingPlot_importTagSectionData

  30/10/2008: Herve
    - bug wrong icons fixed
    
  19/09/2008: Herve
    - missing marker type in export
    - back compatibility with previous version
    
  05/02/2007: Herve
    - bug in unImportTree_getTagValue: must look for </
		
  04/12/2006: Herve
    - bug if import FwTrendingPlot with no DPE
		
  29/06/2006: Herve
    - unImportTree__FwTreeNode_importTagSectionData: if parent == force set node.parent to "", bug fwTree.ctl
		
  07/07/2005: Herve
    - exceptionInfo overwritten with evalScript, modified function: unImportTree_importTagSectionData
	
  31/10/2006: Herve
    - add Export code in case of dsUserData is ""
    - do not replace #local in all the checkFunction for plot
				
version 1#

External Function:
        
Internal Functions:
        
Purpose:
This library contains the function used to export the tree and tree node linked to FwTrendingPlot, FwTrendingPage and _UnPanel devices

Usage: Public

PVSS manager usage: Ctrl, NV, NG

Constraints:
  . UNICOS-JCOP framework
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.
*/


#uses "unPanel.ctl"
#uses "unExportTree.ctl"
#uses "unProgressBar.ctl"
#uses "unTrendTree.ctl"
#uses "unWindowTree.ctl"
#uses "fwTrending/fwTrending.ctl"

#uses "libunCore/unCoreDeprecated.ctl"

// Constants
// global variable 

//@{

//------------------------------------------------------------------------------------------------------------------------


// unImportTree_checkNodeName
/**
Purpose:
Check if the node name is correct: true/false=correct/not correct

	@param sNodeName: string, input: the node name to check

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
bool unImportTree_checkNodeName(string sNodeName)
{
bool rtn = TRUE;

	string sTempNode=sNodeName;

        // replace any & by ""
	strreplace(sTempNode, "&", "");
	if(strpos(sNodeName,"---Clipboard") < 0)
        {
                rtn = unConfigGenericFunctions_nameCheck(sTempNode);
        }
        
	return ( rtn );	
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_xmlChildrenMapping
/**
Purpose:
Return the Children's Text-Nodes as a mapping

        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
        
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
mapping unImportTree_xmlChildrenMapping(int xmlDoc, int xmlTop)
{        
        dyn_int children;
        mapping elements;
        
        mappingClear(elements);
        xmlChildNodes(xmlDoc, xmlTop, children);
        for ( int index = 1 ; index <= dynlen(children) ; ++index )
        {
          string name = xmlNodeName(xmlDoc,children[index]);
          int node = xmlFirstChild(xmlDoc,children[index]);
          if ( node < 0 )
             { elements[name] = ""; }
          else
          {
            // Note that the returned value may be a string or an integer
            if ( xmlNodeType(xmlDoc,node) == XML_TEXT_NODE )
              { elements[name] = (string)xmlNodeValue(xmlDoc,node); }
            else
              { elements[name] = (int)children[index]; }
          }
        }
        // Debug("---> Mapping of "+xmlNodeName(xmlDoc,xmlTop)+":\n",elements);
        
        return ( elements );
}

        
//------------------------------------------------------------------------------------------------------------------------


// unImportTree_xmlChildrenListing
/**
Purpose:
Return the Children's Text-Nodes as a mapping

        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
        
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
dyn_string unImportTree_xmlChildrenListing(int xmlDoc, int xmlTop)
{        
        dyn_int children;
        dyn_string elements = makeDynString();
        
        xmlChildNodes(xmlDoc, xmlTop, children);
        for ( int index = 1 ; index <= dynlen(children) ; ++index )
        {
          int node = xmlFirstChild(xmlDoc,children[index]);
          if ( node < 0 )
             { elements[index] = ""; }
          else
          {
            if ( xmlNodeType(xmlDoc,node) == XML_TEXT_NODE )
              { elements[index] = xmlNodeValue(xmlDoc,node); }
            else
              { elements[index] = ""; }
          }
        }
        // Debug("---> Dyn_String of "+xmlNodeName(xmlDoc,xmlTop)+":\n",elements);
        
        return ( elements );
}

        
//------------------------------------------------------------------------------------------------------------------------


// unImportTree_checkConfigDynamicLists
/**
Purpose:
Check the contents of two dyn_strings with ignoring trailing empty cells...

  @param sFileName: string, input: Input Xml File in Old Format
  @param tFileName: string, output: Output Xml File in New Format
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_checkConfigDynamicLists ( string title , string dpname , dyn_string paradata , dyn_string filedata , dyn_string & exInfo )
{         
int index; 
int paralen = dynlen(paradata);
int filelen = dynlen(filedata);

  while ( ( paralen > 0 ) && ( paradata[paralen] == "" ) ) { dynRemove(paradata ,paralen); --paralen; }
  while ( ( filelen > 0 ) && ( filedata[filelen] == "" ) ) { dynRemove(filedata ,filelen); --filelen; }

  for ( index = 1 ; ( index <= paralen ) && ( index <= filelen ) ; ++index )
  {
    if ( paradata[index] != filedata[index] )
      fwException_raise(exInfo, "WARNING", "unImportTree_checkConfigDynamicLists: different "+title+" at entry "+index+", "+
                          dpname+" current:"+paradata[index]+"; file:"+filedata[index]+";", "");
  }
  while ( index <= paralen )
  {
    fwException_raise(exInfo, "WARNING", "unImportTree_checkConfigDynamicLists: different "+title+" at entry "+index+", "+
                      dpname+" current:"+paradata[index]+"; file:<non-existing>;", "");
    ++index;
  }
  while ( index <= filelen )
  {
    fwException_raise(exInfo, "WARNING", "unImportTree_checkConfigDynamicLists: different "+title+" at entry "+index+", "+
                      dpname+" current:<non-existing>; file:"+filedata[index]+";", "");
    ++index;
  }
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_translateXmlAmpersands
/**
Purpose:
Translate the '&'-signs to the Xml format which is '&amp;'

  @param sFileName: string, input: Input Xml File in Old Format
  @param tFileName: string, output: Output Xml File in New Format
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
int unImportTree_translateXmlAmpersands ( string sFile , string & tFile )
{
int pos;
int lnb = 0;
int cnt = 0;
file fi, fo;
string str;
int p, p1 = -1, p2 = 0;

  p = strlen(sFile);
  while ( p > 0 )
  {
    --p;
    str = substr(sFile,p,1);
    if ( str == "." ) { p2 = p; }
    if ( str == "/" ) { p1 = p; break; }
  }
  if ( p2 == 0 )
    { tFile = sFile + "_Ampersands"; }
  else
    { tFile = substr(sFile, 0, p2) + "_Ampersands" + substr(sFile, p2); }
  
  if ( ( fo = fopen ( tFile , "w" ) ) == 0 ) return -1;
  if ( ( fi = fopen ( sFile , "r" ) ) == 0 ) return -1;
  while ( feof(fi) == 0 )
  {
    fgets ( str , 10000 , fi );
    ++lnb;
    if ( lnb == 1 )
    {
      if ( strpos(str,"<tree>") < 0 ) break;
    }
    if ( ( ( pos = strpos(str,"&") ) >= 0 ) && ( substr(str,pos,5) != "&amp;" ) )
    {
      ++cnt;
      // DebugN("unImportTree_translateXmlAmpersands: Line="+lnb+" Ampersands converted to Xml Format");
      strreplace ( str , "&amp;" , "&" );
      strreplace ( str , "&" , "&amp;" );
    }
    if ( str != "" ) fputs ( str , fo );
  } 
  fclose ( fi );
  fclose ( fo );
  
  if ( cnt == 0 ) remove ( tFile );
  
  return ( cnt );
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_translateXmlNewFormatTrendingPage
/**
Purpose:
Translate a section of the Xml file to the new format

  @param sFileName: string, input: Input Xml File in Old Format
  @param tFileName: string, output: Output Xml File in New Format
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_translateXmlNewFormatTrendingPage(int xmlDoc, int pageconfId, int templateId, int plotconfId, int xmlOut, int xmlPar)
{
string pageconf, template, plotName, plotValue;
int index, idx, xmlTop, subElem, element, plotNumb;
dyn_int children;
dyn_string p_tbl, pg_alldata, pg_templates;
dyn_string pg_allplots = makeDynString("","","","","","");
dyn_string p_pageTags = makeDynString ( "model" , "pageTitle" , "nColumns" , "nRows" , "plots" , "controls" , "plotTemplateParams" , "" );

  if ( xmlNodeName(xmlDoc,pageconfId) != "pageconfiguration" ) return;
  if ( xmlNodeName(xmlDoc,templateId) != "pageplottemplateconfiguration" ) return;
  if ( xmlNodeName(xmlDoc,plotconfId) != "plotconfiguration" ) return;
  
  element = xmlFirstChild(xmlDoc,pageconfId);
  pageconf = xmlNodeValue(xmlDoc,element);
  element = xmlFirstChild(xmlDoc,templateId);
  template = xmlNodeValue(xmlDoc,element);
  xmlChildNodes(xmlDoc,plotconfId,children);
  
  p_tbl[dynlen(p_pageTags)] = "";
  
  pg_alldata = strsplit(pageconf+"ignore",';');
  
  p_tbl[1] = pg_alldata[1];
  p_tbl[2] = pg_alldata[2];
  p_tbl[3] = pg_alldata[3];
  p_tbl[4] = pg_alldata[4];
  p_tbl[6] = pg_alldata[5];

  for ( idx = 1 ; idx <= dynlen(children) ; ++idx )
  {
    plotName = xmlNodeName(xmlDoc,children[idx]);
    element = xmlFirstChild(xmlDoc,children[idx]);
    if ( element >= 0 )
    {
      plotValue = xmlNodeValue(xmlDoc,element);
      if ( plotValue != "" )
      {
        plotNumb = 6*((int)substr(plotName,7,1)-1) + (int)substr(plotName,5,1);
        pg_allplots[plotNumb] = plotValue;
      }
    }
  }
  
  pg_templates = strsplit(template+";;;;;;;;;;;;;;;;;;;;;;;;ignore",';');
      
  xmlTop = xmlAppendChild(xmlOut,xmlPar,XML_ELEMENT_NODE,"pageconfiguration");
  
  for ( index = 1 ; index <= dynlen(p_pageTags) ; ++index )
  {
    if ( p_pageTags[index] == "" ) continue;
    
    if ( index == 5 )
    {
      subElem = xmlAppendChild(xmlOut,xmlTop,XML_ELEMENT_NODE,p_pageTags[index]);
      for ( idx = 1 ; idx <= dynlen(pg_allplots) ; ++idx )
      {
        element = xmlAppendChild(xmlOut,subElem,XML_ELEMENT_NODE,p_pageTags[index]);
        element = xmlAppendChild(xmlOut,element,XML_TEXT_NODE,pg_allplots[idx]);
      }
    }
    else if ( index == 7 )
    {
      subElem = xmlAppendChild(xmlOut,xmlTop,XML_ELEMENT_NODE,p_pageTags[index]);
      for ( idx = 1 ; idx <= dynlen(pg_allplots) ; ++idx )
      {
        if ( pg_allplots[idx] == "" ) pg_templates[idx] = "";
    
        element = xmlAppendChild(xmlOut,subElem,XML_ELEMENT_NODE,p_pageTags[index]);
        element = xmlAppendChild(xmlOut,element,XML_TEXT_NODE,pg_templates[idx]);
      }  
    }
    else
    {
      element = xmlAppendChild(xmlOut,xmlTop,XML_ELEMENT_NODE,p_pageTags[index]);
      element = xmlAppendChild(xmlOut,element,XML_TEXT_NODE,p_tbl[index]);
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_translateXmlNewFormatTrendingPlot
/**
Purpose:
Translate a section of the Xml file to the new format

  @param sFileName: string, input: Input Xml File in Old Format
  @param tFileName: string, output: Output Xml File in New Format
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_translateXmlNewFormatTrendingPlot(int xmlDoc, int plotconfId, int curvconfId, int xmlOut, int xmlPar)
{
string plotconf, dpeConf;
int index, idx, cnt, xmlTop, subElem, element;
dyn_int children;
dyn_string p_tbl, pg_alldata, pg_allconfs, pg_toprint;
dyn_string p_plotTags = makeDynString (
                "model" ,        "plotTitle" ,     "legend" ,       "plotBackColor" ,      "plotForeColor" , 
                "xDpes" ,        "dpes" ,          "xLegendTexts" , "legendTexts" ,        "colors" ,
                "xAxii" ,        "axii" ,          "isTemplate" ,   "curvesHidden" ,       "xRangeMin" ,
                "xRangeMax" ,    "yRangeMin" ,     "yRangeMax" ,    "plotType" ,           "timeRange" ,
                "templateName" , "isLogarithmic" , "grid" ,         "curveTypes" ,         "markerType" ,
                "" ,             "controlBar" ,    "axiiPos" ,      "axiiLink" ,           "defaultFont" ,
                "curveStyle" ,   "xAxiiFormat" ,   "yAxiiFormat" ,  "legendValuesFormat" , "alarmLimitsVisible" );

  if ( xmlNodeName(xmlDoc,plotconfId) != "plotconfiguration" ) return;
  if ( xmlNodeName(xmlDoc,curvconfId) != "curveconfiguration" ) return;
  
  element = xmlFirstChild(xmlDoc,plotconfId);
  plotconf = xmlNodeValue(xmlDoc,element);
  xmlChildNodes(xmlDoc,curvconfId,children);
  
  p_tbl[dynlen(p_plotTags)] = "";
  
  p_tbl[6]  =  ";;;;;;;;";
  p_tbl[8]  =  ";;;;;;;;";
  p_tbl[11] = "FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;";
  p_tbl[15] = "0;0;0;0;0;0;0;0;";
  p_tbl[16] = "0;0;0;0;0;0;0;0;";
  p_tbl[27] = "0";
  p_tbl[28] = "0;0;0;0;0;0;0;0;";
  p_tbl[29] = "0;0;0;0;0;0;0;0;";
  p_tbl[30] = "MS Shell Dlg 2,8,-1,5,50,0,0,0,0,0";
  p_tbl[31] = "[solid,oneColor,JoinMiter,CapButt,0]";
  p_tbl[32] = "%c;;;;;;;;";
  p_tbl[33] = ";;;;;;;;";
  p_tbl[34] = ";;;;;;;;";

  pg_alldata = strsplit(plotconf+"ignore",';');
  
  p_tbl[1] = pg_alldata[1];
  p_tbl[2] = pg_alldata[2];
  p_tbl[3] = pg_alldata[3];
  p_tbl[4] = pg_alldata[4];
  p_tbl[5] = pg_alldata[5];
  p_tbl[19] = pg_alldata[6];
  p_tbl[20] = pg_alldata[7];
  p_tbl[22] = pg_alldata[8];
  p_tbl[23] = pg_alldata[9];
  p_tbl[13] = pg_alldata[10];
  p_tbl[21] = pg_alldata[11];
  p_tbl[25] = pg_alldata[12];

  for ( idx = 1 ; idx <= dynlen(children) ; ++idx )
  {
    if ( ( cnt = (int)substr(xmlNodeName(xmlDoc,children[idx]),strlen("dpeconfiguration")) ) != idx ) break;

    element = xmlFirstChild(xmlDoc,children[idx]);
    if ( element < 0 )
       dpeConf = dpeConf = ";;FwTrendingCurve"+(string)idx+";FALSE;FALSE;0;0;0;";
    else
       dpeConf = xmlNodeValue(xmlDoc,element);
    if ( dpeConf == "" ) dpeConf = ";;FwTrendingCurve"+(string)idx+";FALSE;FALSE;0;0;0;";

    pg_allconfs = strsplit(dpeConf+"ignore",';');
      
    p_tbl[7]  += pg_allconfs[1] + ";";
    p_tbl[9]  += pg_allconfs[2] + ";";
    p_tbl[10] += pg_allconfs[3] + ";";
    p_tbl[12] += pg_allconfs[4] + ";";
    p_tbl[14] += pg_allconfs[5] + ";";
    p_tbl[17] += pg_allconfs[6] + ";";
    p_tbl[18] += pg_allconfs[7] + ";";
    p_tbl[24] += pg_allconfs[8] + ";";
  }
  
  xmlTop = xmlAppendChild(xmlOut,xmlPar,XML_ELEMENT_NODE,"plotconfiguration");
  
  for ( index = 1 ; index <= dynlen(p_plotTags) ; ++index )
  {
    if ( p_plotTags[index] == "" ) continue;
    
    subElem = xmlAppendChild(xmlOut,xmlTop,XML_ELEMENT_NODE,p_plotTags[index]);
      
    if (  ( index == 6  ) || ( index == 7  ) || ( index == 8  ) || ( index == 9  )  
       || ( index == 10 ) || ( index == 11 ) || ( index == 12 ) || ( index == 14 )
       || ( index == 15 ) || ( index == 16 ) || ( index == 17 ) || ( index == 18 )
       || ( index == 24 ) || ( index == 28 ) || ( index == 29 ) || ( index == 32 )
       || ( index == 33 ) || ( index == 34 ) )
    {
      pg_toprint = strsplit(p_tbl[index]+"ignore",';');
    
      for ( idx = 1 ; idx < dynlen(pg_toprint) ; ++idx )
      {
        element = xmlAppendChild(xmlOut,subElem,XML_ELEMENT_NODE,p_plotTags[index]);
        element = xmlAppendChild(xmlOut,element,XML_TEXT_NODE,pg_toprint[idx]);
      }
    }
    else
    {
      element = xmlAppendChild(xmlOut,subElem,XML_TEXT_NODE,p_tbl[index]);
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_translateXmlNewFormatResursive ( xmlDoc , element , xmlOut , -1 , 0 );
/**
Purpose:
Translate a section of the Xml file to the new format

  @param sFileName: string, input: Input Xml File in Old Format
  @param tFileName: string, output: Output Xml File in New Format
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_translateXmlNewFormatResursive ( int xmlDoc , int xmlTop , string parName , int xmlOut , int xmlPar , int depth )
{
int xmlTyp = xmlNodeType(xmlDoc,xmlTop);
int theIndex;
string xmlTxt;
int element;
dyn_int children;

  if ( xmlTyp == XML_ELEMENT_NODE )
  {
    xmlTxt = xmlNodeName(xmlDoc,xmlTop);
    element = xmlAppendChild ( xmlOut , xmlPar , xmlTyp , xmlTxt );
    if ( ( depth == 0 ) && ( xmlTxt == "tree" ) ) xmlSetElementAttribute(xmlOut,element,"version","2");
    if ( xmlChildNodes(xmlDoc,xmlTop,children) < 0 ) return;
    if ( dynlen(children) == 0 )
    {
      if ( xmlTxt == "otherdevice" )
      {
        xmlRemoveNode ( xmlOut , element );
      }
      else
      {
        xmlAppendChild ( xmlDoc , xmlTop , XML_TEXT_NODE , "" );
        if ( xmlChildNodes(xmlDoc,xmlTop,children) < 0 ) return;
      }
    }
    
    theIndex = 1;
    
    if ( ( xmlTxt == "FwTrendingPage" ) && ( dynlen(children) >= 4 ) )
    {
      unImportTree_translateXmlNewFormatResursive ( xmlDoc , children[1] , xmlTxt , xmlOut , element , depth + 1 );
      unImportTree_translateXmlNewFormatTrendingPage(xmlDoc,children[2],children[3],children[4],xmlOut,element);
      theIndex = 5;
    }
      
    if ( ( xmlTxt == "FwTrendingPlot" ) && ( dynlen(children) >= 3 ) )
    {
      unImportTree_translateXmlNewFormatResursive ( xmlDoc , children[1] , xmlTxt , xmlOut , element , depth + 1 );
      unImportTree_translateXmlNewFormatTrendingPlot(xmlDoc,children[2],children[3],xmlOut,element);
      theIndex = 4;
    }
  
    while ( theIndex <= dynlen(children) )
    {
      unImportTree_translateXmlNewFormatResursive ( xmlDoc , children[theIndex] , xmlTxt , xmlOut , element , depth + 1 );

      ++theIndex;
    }
  }
  else if ( xmlTyp == XML_TEXT_NODE )
  {
    xmlTxt = xmlNodeValue(xmlDoc,xmlTop);
    
    strreplace(xmlTxt,"\r\r\n","\r\n");
    
    if ( strpos( xmlTxt , "@" ) >= 0 )
    {
      if ( ( parName == "name" ) || ( parName == "parentname" ) || ( parName == "children" ) || ( substr(parName,0,4) == "link" ) )
      {
        strreplace ( xmlTxt , "@" , "&" );
      }
    }
    
    xmlAppendChild ( xmlOut , xmlPar , xmlTyp , xmlTxt );
  }
  else return;
}


//------------------------------------------------------------------------------------------------------------------------

    
// unImportTree_translateXmlNewFormat      
/**
Purpose:
Translate the Xml file to the new format - Translate ANSI to UTF-8 format

  @param sFileName: string, input: Input Xml File in Old Format
  @param tFileName: string, output: Output Xml File in New Format
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_translateXmlNewFormat ( string sFile , string & tFile )
{  
file fi, fo;
string str, temporary;
int p, p1 = -1, p2 = 0;
int xmlDoc,xmlOut;
int element;
string errMsg;
int errLine,errColumn;

  p = strlen(sFile);
  while ( p > 0 )
  {
    --p;
    str = substr(sFile,p,1);
    if ( str == "." ) { p2 = p; }
    if ( str == "/" ) { p1 = p; break; }
  }
  if ( p2 == 0 )
    { tFile = sFile + "_NewFormat"; }
  else
    { tFile = substr(sFile, 0, p2) + "_NewFormat" + substr(sFile, p2); }
  
  if ( ( fo = fopen(sFile+"_temporary","w") ) == 0 ) return;
  if ( ( fi = fopen(sFile,"r") ) == 0 ) return;
  while ( feof(fi) == 0 )
  {
    fgets ( temporary , 1000000 , fi );

    sprintf(str,"%c%c",0303,0251);
    strreplace(temporary,"\xE9",str); // e'
    sprintf(str,"%c%c",0303,0250);
    strreplace(temporary,"\xE8",str); // e`
    sprintf(str,"%c%c",0303,0252);
    strreplace(temporary,"\xEA",str); // e^
    sprintf(str,"%c%c",0303,0253);
    strreplace(temporary,"\xEB",str); // e..
    sprintf(str,"%c%c",0303,0240);
    strreplace(temporary,"\xE0",str); // a`
    sprintf(str,"%c%c",0303,0247);
    strreplace(temporary,"\xE7",str); //c,
    sprintf(str,"%c%c",0303,0257);
    strreplace(temporary,"\xEF",str); // i..
    sprintf(str,"%c%c",0303,0261);
    strreplace(temporary,"\xF1",str); // n~
    sprintf(str,"%c%c",0303,0264);
    strreplace(temporary,"\xF4",str); // o^
    
    fputs(temporary,fo);
  }
  fclose(fi);
  fclose(fo);

  if ( ( xmlOut = xmlNewDocument() ) < 0 ) return;
  if ( ( xmlDoc = xmlDocumentFromFile(sFile+"_temporary",errMsg,errLine,errColumn) ) < 0 ) return;
  
  xmlAppendChild ( xmlOut , -1 , XML_PROCESSING_INSTRUCTION_NODE , "xml version=\"1.0\" encoding=\"UTF-8\"" );
  
  element = xmlFirstChild ( xmlDoc );
  
  unImportTree_translateXmlNewFormatResursive ( xmlDoc , element , "" , xmlOut , -1 , 0 );
  
  xmlCloseDocument ( xmlDoc );
  xmlDocumentToFile ( xmlOut , tFile );
  
  moveFile(tFile, tFile+"_temporary");
  unExportTree_indentXmlFile(tFile,tFile+"_temporary");
  
  remove (sFile+"_temporary");
  remove(tFile+"_temporary");
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_checkTreeDataXml
/**
Purpose:
Check the configuration data of the tree

  @param doc: int, input: XML document reference
  @param dataElems: dyn_int, input: XML nodes of the configs
  @param sTreeName: string, output: the tree node name
  @param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
  @param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_checkTreeDataXml(int doc, dyn_int dataElems, string &sTreeName, bool bKeepLocalSystem, dyn_string &exInfo)
{
string sValue, root, nodeTypes, nodeNames, sConf;
int refresh, i, len;
dyn_string dsMode;
string sTreeDp;
int node;

  unExportTree_uniqueTreeTagNames();
  
  sValue = "Not-in-Xml-File";
  if (  ( xmlNodeType(doc,dataElems[1]) != XML_ELEMENT_NODE )
     || ( xmlNodeName(doc,dataElems[1]) != "name" )
     || ( ( node = xmlFirstChild(doc,dataElems[1]) ) < 0 )
     || ( xmlNodeType(doc,node) != XML_TEXT_NODE )
     || ( ( sValue = xmlNodeValue(doc,node) ) == "" ) ) {
    fwException_raise(exInfo, "WARNING", "unImportTree_checkTreeDataXml: no tree definition, tree can be imported into a position", "");
    return;
  }

  if( ! unImportTree_checkNodeName(sValue) ) {
    fwException_raise(exInfo, "ERROR", "unImportTree_checkTreeDataXml: wrong tree node name, "+sValue, "");
  }
        
  sTreeName = sValue;
  sTreeDp = unTreeWidget_treeTypeDpPrefix+sTreeName;
        
  if ( ! dpExists ( sTreeDp ) ) return;
  
  // get the tree config and compare them with the given, if different raise warning
  dpGet(sTreeDp+".name", sTreeName, sTreeDp+".root", root, sTreeDp+".nodeTypes", nodeTypes, sTreeDp+".nodeNames", nodeNames,
        sTreeDp+".refresh", refresh, sTreeDp+".modes", dsMode);
  // if the root is the current system name replace it by #local
  if(root == getSystemName()) root = "#local:";
  sConf = root+UN_PARAMETER_DELIMITER+nodeTypes+UN_PARAMETER_DELIMITER+
  nodeNames+UN_PARAMETER_DELIMITER+refresh+UN_PARAMETER_DELIMITER;
                
  sValue = "Not-in-Xml-File";
  if (  ( xmlNodeType(doc,dataElems[2]) != XML_ELEMENT_NODE )
     || ( xmlNodeName(doc,dataElems[2]) != "treeparameters" )
     || ( ( node = xmlFirstChild(doc,dataElems[2]) ) < 0 )
     || ( xmlNodeType(doc,node) != XML_TEXT_NODE )
     || ( ( sValue = xmlNodeValue(doc,node) ) != sConf ) ) {
    fwException_raise(exInfo, "WARNING", "unImportTree_checkTreeDataXml: different treeparameters, "+sValue+", "+sConf, "");
  }
  
  sConf = "";
  len = dynlen(dsMode);
  for(i=1;i<=len;i++) {
    if(sConf == "") 
      sConf = dsMode[i];
    else
      sConf = sConf+UN_PARAMETER_DELIMITER+dsMode[i];
  }
  
  if(sConf != "")
    sConf += UN_PARAMETER_DELIMITER;
                
  sValue = "Not-in-Xml-File";
  if (  ( xmlNodeType(doc,dataElems[3]) != XML_ELEMENT_NODE )
     || ( xmlNodeName(doc,dataElems[3]) != "treemodes" )
     || ( ( node = xmlFirstChild(doc,dataElems[3]) ) < 0 )
     || ( xmlNodeType(doc,node) != XML_TEXT_NODE )
     || ( ( sValue = xmlNodeValue(doc,node) ) != sConf ) ) {
    fwException_raise(exInfo, "WARNING", "unImportTree_checkTreeData: different treemodes, "+sValue+", "+sConf, "");
  }
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_importTreeDataXml
/**
Purpose:
Import the configuration data of the tree

        @param doc: int, input: XML document reference
	@param dataElems: dyn_int, input: XML nodes of the configs
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_importTreeDataXml(int doc, dyn_int dataElems, dyn_string &exInfo)
{
	string sValue, root, nodeTypes, nodeNames, sConf;
	int refresh, i, len;
	dyn_string dsMode;
	string sTreeDp, sTreeName;
	dyn_string dsSplit;
  int node;

  unExportTree_uniqueTreeTagNames();
  
  // get tree name
  node = xmlFirstChild(doc,dataElems[1]);
  if ( node < 0 )
    sValue = "";
  else
    sValue = xmlNodeValue(doc,node);

  if(sValue != "") {
		sTreeName = sValue;
		sTreeDp = unTreeWidget_treeTypeDpPrefix+sTreeName;
		if(!dpExists(sTreeDp)) {
			unImportTree_createDp(sTreeDp,"_FwTreeType", exInfo);
		}
		if(dynlen(exInfo) <= 0) {
			if(dpTypeName(sTreeDp) == "_FwTreeType") {
	                        // get the treeparameters
                                node = xmlFirstChild(doc,dataElems[2]);
                                sValue = xmlNodeValue(doc,node);
				dsSplit = strsplit(sValue, UN_PARAMETER_DELIMITER);
				root = dsSplit[1];
			        // if the root is #local replace it by the current system name
				if(root == "#local:")
					root = getSystemName();
				nodeTypes = dsSplit[2];
				nodeNames = dsSplit[3];
				sscanf(dsSplit[4], "%d", refresh);
	                        // get the treeparameters
                                node = xmlFirstChild(doc,dataElems[3]);
                                sValue = xmlNodeValue(doc,node);
				dsMode = strsplit(sValue, UN_PARAMETER_DELIMITER);
	                        // get the tree config and compare them with the given, if different raise warning
				dpSet(sTreeDp+".name", sTreeName, sTreeDp+".root", root, sTreeDp+".nodeTypes", nodeTypes, sTreeDp+".nodeNames", nodeNames,
							sTreeDp+".refresh", refresh, sTreeDp+".modes", dsMode);
			}
			else
				fwException_raise(exInfo, "WARNING", "unImportTree_importTreeDataXml: wrong device type, "+sTreeDp, "");
		}
	}
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_checkTagSectionDataXml
/**
Purpose:
Check the all the data of a tag, trigger the device type check function based on the tag name

	@param bTreeNodeDevice: bool, input: true=the function is called from a treenode and the tree node device name is given
	@param sTreeNodeDeviceName: string, input: the tree node device name
	@param sTagName: string, input: the tag name without < and >
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_checkTagSectionDataXml(bool bTreeNodeDevice, string sTreeNodeDeviceName, string sTagName, int xmlDoc, int xmlTop, bool bKeepLocalSystem, dyn_string &exInfo)
{
string sFunction;
dyn_string dsReturn;

  // call the device function to export the device content
  if ( sTagName != "" )
  {
    sFunction = "unImportTree_"+sTagName+"_checkTagSectionDataXml";
    if ( isFunctionDefined(sFunction) )
    {
      evalScript ( dsReturn , "dyn_string main(bool bTreeNodeDevice, string sTreeNodeDeviceName, int xmlDoc, int xmlTop, bool bKeepLocalSystem)" + 
                              "{"+
                              "  dyn_string exInfo;"+
                              "  "+sFunction+"(bTreeNodeDevice, sTreeNodeDeviceName, xmlDoc, xmlTop, bKeepLocalSystem, exInfo);"+
                              "  return exInfo;"+
                              "}" ,
                              makeDynString(), bTreeNodeDevice, sTreeNodeDeviceName, xmlDoc, xmlTop, bKeepLocalSystem );
      dynAppend(exInfo, dsReturn);
    }
    else
      fwException_raise(exInfo, "ERROR", "unImportTree_checkTagSectionDataXml: unknown tag name function "+sFunction+" for tag "+sTagName, "");
  }
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_importTagSectionDataXml
/**
Purpose:
Check the all the data of a tag, trigger the device type import function based on the tag name

	@param sTagName: string, input: the tag name without < and >
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_importTagSectionDataXml(string sTagName, int xmlDoc, int xmlTop, dyn_string &exInfo)
{
string sFunction;
dyn_string dsReturn;
	
  // call the device function to export the device content
  if ( sTagName != "" )
  {
    sFunction = "unImportTree_"+sTagName+"_importTagSectionDataXml";
    if(isFunctionDefined(sFunction))
    {
      evalScript ( dsReturn , "dyn_string main(int xmlDoc, int xmlTop)" + 
                              "{"+
                              "  dyn_string exInfo;"+
                              "  "+sFunction+"(xmlDoc, xmlTop, exInfo);"+
                              "  return exInfo;"+
                              "}",
                              makeDynString(), xmlDoc, xmlTop);
      dynAppend(exInfo, dsReturn);
    }
    else
      fwException_raise(exInfo, "ERROR", "unImportTree_importTagSectionDataXml: unknown tag name function "+sFunction+" for tag "+sTagName, "");
  }
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree__FwTreeNode_checkTagSectionDataXml
/**
Purpose:
Check all the data of the _FwTreeNode

	@param bTreeNodeDevice: bool, input: true=the function is called from a treenode and the tree node device name is given
	@param sTreeNodeDeviceName: string, input: the tree node device name
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree__FwTreeNode_checkTagSectionDataXml(bool bTreeNodeDevice, string sTreeNodeDeviceName, int xmlDoc, int xmlTop, bool bKeepLocalSystem, dyn_string &exInfo)
{
string sValue, sNodeName, sParentNodeName, sCurrentParentNodeName, sDeviceName, sDeviceType, sCurrentDeviceName, sCurrentDeviceType, sConf;
dyn_string dsChildren, dsUserData, dsSplit;
int i, len, iNodeCU, iCurrentNodeCU;
int iDataIndex;
string sTagName;
mapping elements;
        
  elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
        
  // check the size: minimum 5
  if(mappinglen(elements) < 5) {
    fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong data length", "");
    return;
  }

  // get the name and check it
  sValue = elements["name"];
  sNodeName = sValue;
  if(!unImportTree_checkNodeName(sValue) || (sValue == "")) {
    fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong node name, "+sValue, "");
  }
  // get the parent and check it
  sParentNodeName = elements["parentname"];
  if(!unImportTree_checkNodeName(sParentNodeName)) {
    fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong parent node name, "+sParentNodeName, "");
  }
  if(sParentNodeName == sNodeName) {
    fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong parent node name = node name, "+sParentNodeName, "");
  }
  // check that the tree node device name and the tree name are the same, check the parent node name is ""
  if(bTreeNodeDevice) {
    if(sTreeNodeDeviceName != sNodeName)
      fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: tree name and treenode name are different, "+sTreeNodeDeviceName+", "+sNodeName, "");
    if(sParentNodeName != "")
      fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: first node of a tree, wrong parent nodename, "+sTreeNodeDeviceName+", "+sParentNodeName, "");
  }
  // get the children and check them
  sValue = elements["children"];
  dsChildren = strsplit(sValue, UN_PARAMETER_DELIMITER);
  len = dynlen(dsChildren);
  for(i=1;i<=len;i++) {
    if(!unImportTree_checkNodeName(dsChildren[i]) || (dsChildren[i] == "")) {
      fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong children node name, "+dsChildren[i], "");
    }
  }
  // get the device name and check them
  sValue = elements["devicedata"];
  dsChildren = strsplit(sValue, UN_PARAMETER_DELIMITER);
  if(dynlen(dsChildren) < 3)
    fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong device config, "+sValue, "");
  else {
    sDeviceName = dsChildren[1];
    sDeviceType = dsChildren[2];
    sscanf(dsChildren[3], "%d", iNodeCU);
    // check the device name
    if(!unImportTree_checkDpName(dsChildren[1])) {
      fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong device dp name, "+dsChildren[1], "");
    }
  }
                
  if ( mappingHasKey(elements,"deviceconfiguration") )
  {
    // check the device data
    int subNode;
    if ( ( subNode = xmlFirstChild(xmlDoc,elements["deviceconfiguration"]) ) >= 0 ) {
      sTagName = xmlNodeName(xmlDoc,subNode);
      unImportTree_checkTagSectionDataXml(true, sDeviceName, sTagName, xmlDoc, subNode, bKeepLocalSystem, exInfo);
    }
  }
                
  // if the device already exists, check if the parentnode name is the same, and device data is the same.
  if(dpExists(unTreeWidget_treeNodeDpPrefix + sNodeName)) {
    if(dpTypeName(unTreeWidget_treeNodeDpPrefix + sNodeName) == unTreeWidget_treeNodeDPT) {
      fwTree_getParent(sNodeName, sCurrentParentNodeName, exInfo);
      fwTree_getNodeDevice(sNodeName, sCurrentDeviceName, sCurrentDeviceType, exInfo);
      fwTree_getNodeCU(sNodeName, iCurrentNodeCU, exInfo);
      fwTree_getNodeUserData(sNodeName, dsUserData, exInfo);
				
      if(unExportTree_getSystemName(sDeviceName) == "#local:")
        sDeviceName = unExportTree_removeSystemName(sDeviceName);
      if(unExportTree_getSystemName(sCurrentDeviceName) == getSystemName())
        sCurrentDeviceName = unExportTree_removeSystemName(sCurrentDeviceName);
      // if no device in current node then no error
      if(sCurrentDeviceName == "") {
        sCurrentDeviceName = sDeviceName;
        sCurrentDeviceType = sDeviceType;
        iCurrentNodeCU = iNodeCU;
      }
      if(sTagName != sDeviceType) {
        fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: wrong device type in file, "+sNodeName, "");
      }
      if((sParentNodeName != sCurrentParentNodeName) || 
        (sDeviceName != sCurrentDeviceName) ||
        (sDeviceType != sCurrentDeviceType) ||
        (iNodeCU != iCurrentNodeCU) ) {
        fwException_raise(exInfo, "WARNING", "unImportTree__FwTreeNode_checkTagSectionDataXml: node exists but config different, "+
                          sNodeName+", current:"+sCurrentParentNodeName+","+sCurrentDeviceName+","+sCurrentDeviceType+","+
                          iCurrentNodeCU+"; file:"+sParentNodeName+","+sDeviceName+","+sDeviceType+","+iNodeCU, "");
      }
      // check user data
      len=dynlen(dsUserData);
      // if more than 1 entry in dsUserData --> assume panel navigation
      sConf = "";
      for(i=1;i<=len;i++) {
        // add the device to the list to export
        if(i>1) {
          dsSplit = strsplit(dsUserData[i], c_panel_navigation_delimiter);
          while(dynlen(dsSplit) <3)
            dynAppend(dsSplit, ""); 
          if(dsSplit[2] != "") {
            if(unExportTree_getSystemName(dsSplit[2]) == getSystemName())
              dsSplit[2] = "#local:"+unExportTree_removeSystemName(dsSplit[2]);
          }
          dsUserData[i]=dsSplit[1]+c_panel_navigation_delimiter+dsSplit[2]+c_panel_navigation_delimiter+dsSplit[3];
        }
        if(dsUserData[i] == "") // if empty force it to * in order to have an entry
          dsUserData[i] = "*";
        if(sConf == "") 
          sConf = dsUserData[i];
        else
          sConf = sConf+"~"+dsUserData[i];
      }
      if(sConf != "")
        sConf+="~";
      // if * then replace it by ""
      strreplace(sConf, "*", "");
      // get the userdata
      sValue = elements["userdata"];
      strreplace(sValue,"\r\r\n","\r\n");
      if(sConf != sValue)
        fwException_raise(exInfo, "WARNING", "unImportTree__FwTreeNode_checkTagSectionDataXml: node exists but user data config different, "+sNodeName+
                          ", current:"+sConf+"; file:"+sValue, "");
    }
    else
      fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_checkTagSectionDataXml: dpnode exists but wrong type name, "+unTreeWidget_treeNodeDpPrefix + sNodeName, "");
  }
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree__FwTreeNode_importTagSectionDataXml
/**
Purpose:
Import the data of the treenode

        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree__FwTreeNode_importTagSectionDataXml(int xmlDoc, int xmlTop, dyn_string &exInfo)
{
	string sValue, sDpNodeName, sNodeName, sParentNodeName, sDeviceName, sDeviceType, sTag, sParentDpNodeName;
	dyn_string dsSplit, dsChildren, dsDevData, dsSplitBis;
	int iNodeCU, i, len, iDataIndex;
        string sTagName;
        mapping elements;
        
        elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
	
        // get the name and check it
	sValue = elements["name"];
	sNodeName = sValue;
	sDpNodeName = _fwTree_makeNodeName(sNodeName);
	if(!dpExists(sDpNodeName)) {
		unImportTree_createDp(sDpNodeName,unTreeWidget_treeNodeDPT, exInfo);
	}
	if(dynlen(exInfo) <= 0) {
		if(dpTypeName(sDpNodeName) == unTreeWidget_treeNodeDPT) {
                        // get the parent
			sValue = elements["parentname"];
			sParentNodeName = sValue;

                        // get the device name, device type and nodeCU and check them
			sValue = elements["devicedata"];
			dsSplit = strsplit(sValue, UN_PARAMETER_DELIMITER);
			sDeviceName = dsSplit[1];
			if(unExportTree_getSystemName(sDeviceName) == "#local:")
				sDeviceName = getSystemName()+unExportTree_removeSystemName(sDeviceName);
			sDeviceType = dsSplit[2];
			sscanf(dsSplit[3], "%d", iNodeCU);
                        // get the list of children
			sValue = elements["children"];
			dsSplit = strsplit(sValue, UN_PARAMETER_DELIMITER);
			len = dynlen(dsSplit);
			for(i=1;i<=len;i++) {
				dsChildren[i] = dsSplit[i];
				if(dsChildren[i] != "")
					dsChildren[i] = _fwTree_makeNodeName(dsChildren[i]);
			}
                        // get the userdata
			sValue = elements["userdata"];
    strreplace(sValue,"\r\r\n","\r\n");
			dsSplit = strsplit(sValue, "~");
			len=dynlen(dsSplit);
                        // if more than 1 entry in dsSplit --> assume panel navigation
			for(i=1;i<=len;i++) {
                                // add the device to the list to export
				if(i>1) {
					dsSplitBis = strsplit(dsSplit[i], c_panel_navigation_delimiter);
					while(dynlen(dsSplitBis) <3)
						dynAppend(dsSplitBis, ""); 
					if(dsSplitBis[2] != "") {
						if(unExportTree_getSystemName(dsSplitBis[2]) == "#local:")
							dsSplitBis[2] = getSystemName()+unExportTree_removeSystemName(dsSplitBis[2]);
					}
					dsSplit[i]=dsSplitBis[1]+c_panel_navigation_delimiter+dsSplitBis[2]+c_panel_navigation_delimiter+dsSplitBis[3];
				}
			}
                        // set the tree node device data.
			fwTree_setNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
			fwTree_setNodeUserData(sNodeName, dsSplit, exInfo);
			fwTree_setNodeCU(sNodeName, iNodeCU, exInfo);
			_fwTree_setChildren(sDpNodeName, dsChildren, exInfo);

                        // if parent exists, add it into parent if not already added, and set the parent
			if(sParentNodeName != "" && sNodeName != "")
			{
				fwTree_addNode(sParentNodeName, sNodeName, exInfo, 1);
			}

			if(sParentNodeName == "") // case sParentNodeName == "" and sNodeName has a parent (bug fwTree.ctl)
				dpSet(sDpNodeName+".parent","");

                        if ( mappingHasKey(elements,"deviceconfiguration") ) {
                                // set the device data
                                int subNode;
                                if ( ( subNode = xmlFirstChild(xmlDoc,elements["deviceconfiguration"]) ) >= 0 )
                                {
                                        sTagName = xmlNodeName(xmlDoc,subNode);
				        unImportTree_importTagSectionDataXml(sTagName, xmlDoc, subNode, exInfo);
			        }
                        }
		}
		else
			fwException_raise(exInfo, "ERROR", "unImportTree__FwTreeNode_importTagSectionDataXml: wrong device type , "+sDpNodeName, "");
	}
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree__UnPanel_checkTagSectionDataXml
/**
Purpose:
Check the data of the _UnPanel device

	@param bTreeNodeDevice: bool, input: true=the function is called from a treenode and the tree node device name is given
	@param sTreeNodeDeviceName: string, input: the tree node device name
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree__UnPanel_checkTagSectionDataXml(bool bTreeNodeDevice, string sTreeNodeDeviceName, int xmlDoc, int xmlTop, bool bKeepLocalSystem, dyn_string &exInfo)
{
	string sDpName, sValue;
        mapping elements;
        
        elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
	
        // check the size: minimum 3
	if(mappinglen(elements) < 2) {
		fwException_raise(exInfo, "ERROR", "unImportTree__UnPanel_checkTagSectionDataXml: wrong data length", "");
	}
	else {
                // get the panel name and check it
                sValue = elements["panelname"];
		sDpName = unPanel_convertPanelFileNameToDp(sValue);
		sDpName = unExportTree_removeSystemName(sDpName);
		sValue = sDpName;
		strreplace(sValue, "-", "");
		if(!unImportTree_checkNodeName(sValue) || (sValue == "")) {
			fwException_raise(exInfo, "ERROR", "unImportTree__UnPanel_checkTagSectionDataXml: wrong device dp name, "+sValue, "");
		}
		
                // check that the tree node device name and panel file name are the same
		if(bTreeNodeDevice) {
			sValue = unExportTree_removeSystemName(sTreeNodeDeviceName);
			if(sValue != sDpName)
				fwException_raise(exInfo, "ERROR", "unImportTree__UnPanel_checkTagSectionDataXml: tree device node and panel file name are different, "+sValue+", "+sDpName, "");
		}
                // check the dptype if the dp exists
		if(dpExists(sDpName)) {
			if(dpTypeName(sDpName) != UN_WINDOWTREE_PANEL)
				fwException_raise(exInfo, "ERROR", "unImportTree__UnPanel_checkTagSectionDataXml: dpnode exists but wrong type name, "+sDpName, "");
		}
                // get the horizontal navigation and check them
		unImportTree_devicenavigation_checkTagSectionDataXml(sDpName, xmlDoc, elements["devicenavigation"], bKeepLocalSystem, exInfo);
	}
}

//------------------------------------------------------------------------------------------------------------------------


// unImportTree__UnPanel_importTagSectionDataXml
/**
Purpose:
Import the data of the _UnPanel device

        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree__UnPanel_importTagSectionDataXml(int xmlDoc, int xmlTop, dyn_string &exInfo)
{
	string sValue, sDpName;
        mapping elements;
        
        elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
	
        // get the panel name and check it
	sValue = elements["panelname"];
	sDpName = unPanel_convertPanelFileNameToDp(sValue);
	sDpName = unExportTree_removeSystemName(sDpName);
	if(!dpExists(sDpName)) {
		unImportTree_createDp(sDpName, UN_WINDOWTREE_PANEL, exInfo);
	}
	if(dynlen(exInfo) <= 0) {
                // get the horizontal navigation and check them
		unImportTree_devicenavigation_importTagSectionDataXml(sDpName, xmlDoc, elements["devicenavigation"], exInfo);
	}
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_FwTrendingPlot_checkTagSectionDataXml
/**
Purpose:
Check the data of the FwTrendingPlot device

	@param bTreeNodeDevice: bool, input: true=the function is called from a treenode and the tree node device name is given
	@param sTreeNodeDeviceName: string, input: the tree node device name
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree_FwTrendingPlot_checkTagSectionDataXml(bool bTreeNodeDevice, string sTreeNodeDeviceName, int xmlDoc, int xmlTop, bool bKeepLocalSystem, dyn_string &exInfo)
{
string sDpName, sValue, sSystemName;
dyn_string dsSplit;
int i, len, idx, node, plotlen, trendlen;
dyn_dyn_string ddsPlotData;
dyn_int children;
mapping elements, subelems;
dyn_string listing;

  elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
        
  // check the size: minimum 3
  if ( mappinglen(elements) < 3 ) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: wrong data length", "");
    return;
	}
  
  // get the dp name and check it
  sDpName = elements["dpname"];
  sSystemName = unExportTree_getSystemName(sDpName);
  if((sSystemName == "#local:") || (sSystemName == ""))
    sDpName = getSystemName()+unExportTree_removeSystemName(sDpName);
  sValue = unExportTree_removeSystemName(sDpName);
  strreplace(sValue, "-", "");
  if(!unImportTree_checkNodeName(sValue) || (sValue == "")) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: wrong device dp name, "+sValue, "");
  }
  
  subelems = unImportTree_xmlChildrenMapping(xmlDoc, elements["plotconfiguration"]);
  
  if ( mappinglen(subelems) < 34 ) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: wrong plotconfiguration length, "+sValue, "");
  }
    
  // check the plot title
  sValue = subelems["plotTitle"];
  if(!unImportTree_checkNodeName(sValue) || (sValue == "")) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: wrong plot title, "+sValue, "");
  }
			
  // check that the tree node device name and plot dpname are the same
  if(bTreeNodeDevice) {
    sValue = sTreeNodeDeviceName;
    sSystemName = unExportTree_getSystemName(sTreeNodeDeviceName);
    if((sSystemName == "#local:") || (sSystemName == ""))
      sValue = getSystemName()+unExportTree_removeSystemName(sValue);
    
    if(sValue != sDpName)
      fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: tree device node and plot dpname are different, "+sValue+", "+sDpName, "");
  }
  
  // if the dp exists check the content 
  if(dpExists(sDpName)) {
    if(dpTypeName(sDpName) == UN_TRENDTREE_PLOT) {
      
      // get the device data
				fwTrending_getPlot(sDpName, ddsPlotData, exInfo);
      trendlen = dynlen(ddsPlotData);
        
      if ( g_dsImportTree_plotTreeDataSize == -1 ) g_dsImportTree_plotTreeDataSize = trendlen;
      else if ( trendlen != g_dsImportTree_plotTreeDataSize )
      {
        fwException_raise(exInfo, "WARNING", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: "+sDpName+
                  ", ddsPlotData-Size '"+trendlen+"' differs from last size '"+g_dsImportTree_plotTreeDataSize+"'", "");
        g_dsImportTree_plotTreeDataSize = trendlen;
      }

      plotlen = dynlen(g_dsExportTree_plotTreeTagNames);

      for ( idx = 1 ; idx <= plotlen ; ++idx )
      {
        if ( idx > trendlen )
        {
          fwException_raise(exInfo, "WARNING", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: "+sDpName+
                            ", ddsPlotData index "+idx+" '"+g_dsExportTree_plotTreeTagNames[idx]+"' non-existing", "");
          continue;
        }
        
        if ( ! fwTrending_isPlotTreeTagExportable(idx) ) { continue; }
    
        if ( ! mappingHasKey(subelems,g_dsExportTree_plotTreeTagNames[idx]) ) { continue; }
        
        if ( getType(subelems[g_dsExportTree_plotTreeTagNames[idx]]) == STRING_VAR )
        {
          if ( dynlen(ddsPlotData[idx]) == 0 ) ddsPlotData[idx] = makeDynString("");
          
          if ( ddsPlotData[idx][1] != subelems[g_dsExportTree_plotTreeTagNames[idx]] )
            fwException_raise(exInfo, "WARNING", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: different '"+
                              g_dsExportTree_plotTreeTagNames[idx]+"', "+
                              sDpName+", current:"+ddsPlotData[idx][1]+"; file"+subelems[g_dsExportTree_plotTreeTagNames[idx]], "");
        }
        else if ( getType(subelems[g_dsExportTree_plotTreeTagNames[idx]]) == INT_VAR )
        {
          listing = unImportTree_xmlChildrenListing(xmlDoc, (int)subelems[g_dsExportTree_plotTreeTagNames[idx]]);
      
          if ( !bKeepLocalSystem )
          {
            if ( idx == fwTrending_PLOT_OBJECT_DPES )
            {
              for ( i = 1 ; i <= dynlen(listing) ; ++i )
              {
                if ( listing[i] != "" )
                {
                  // if the system name is present and equals to the current system, replace it by the key #local:
                  if ( unExportTree_getSystemName(listing[i]) == "#local:" )
                     listing[i] = getSystemName()+unExportTree_removeSystemName(listing[i]);
                 }
              }
            }
      
            if ( idx == fwTrending_PLOT_OBJECT_LEGENDS )
            {
              for ( i = 1 ; i <= dynlen(listing) ; ++i )
              {
                if ( listing[i] != "" )
                {
                  // if the system name is present and equals to the current system, replace it by the key #local:
                  if ( strpos(listing[i] , "#local:" ) >= 0 )
								         strreplace ( listing[i] , "#local:" , getSystemName() );
                }
              }
            }
          }
    
          unImportTree_checkConfigDynamicLists(g_dsExportTree_plotTreeTagNames[idx], sDpName, ddsPlotData[idx], listing, exInfo); 
        }
      }        
    }
    else
      fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPlot_checkTagSectionDataXml: dpnode exists but wrong type name, "+sDpName, "");
  }
  
  // get the horizontal navigation and check them
  unImportTree_devicenavigation_checkTagSectionDataXml(sDpName, xmlDoc, elements["devicenavigation"], bKeepLocalSystem, exInfo);
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_FwTrendingPlot_importTagSectionDataXml
/**
Purpose:
Check the data of the FwTrendingPlot device

        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree_FwTrendingPlot_importTagSectionDataXml(int xmlDoc, int xmlTop, dyn_string &exInfo)
{
string sDpName, sValue, sSystemName;
dyn_string dsSplit;
int i, len, idx, node, plotlen;
dyn_dyn_string ddsPlotData;
dyn_int children;
mapping elements, subelems;
dyn_string listing;
        
  elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
	
  // get the dp name and check it
  sDpName = elements["dpname"];
  sSystemName = unExportTree_getSystemName(sDpName);
  if ( sSystemName == "#local:" )
     sDpName = unExportTree_removeSystemName(sDpName);
  if ( !dpExists(sDpName) ) {
     unImportTree_createDp(sDpName, UN_TRENDTREE_PLOT, exInfo);
  }
  
  if(dynlen(exInfo) > 0) return;

  subelems = unImportTree_xmlChildrenMapping(xmlDoc, elements["plotconfiguration"]);
  
  plotlen = dynlen(g_dsExportTree_plotTreeTagNames);
  
  for ( idx = 1 ; idx <= plotlen ; ++idx )
  {
    ddsPlotData[idx] = makeDynString("");
    
    if ( ! fwTrending_isPlotTreeTagExportable(idx) ) { continue; }
    
    if ( mappingHasKey(subelems,g_dsExportTree_plotTreeTagNames[idx]) == FALSE )
       subelems[g_dsExportTree_plotTreeTagNames[idx]] = "";
    
    if ( getType(subelems[g_dsExportTree_plotTreeTagNames[idx]]) == STRING_VAR )
    {
      if ( dynlen(ddsPlotData[idx]) == 0 ) ddsPlotData[idx] = makeDynString("");
      
      if ( ( idx == fwTrending_PLOT_OBJECT_MARKER_TYPE ) && ( subelems[g_dsExportTree_plotTreeTagNames[idx]] == "" ) )
         ddsPlotData[idx][1] = fwTrending_MARKER_TYPE_NONE;
      else         
         ddsPlotData[idx][1] = subelems[g_dsExportTree_plotTreeTagNames[idx]];
    }
    else if ( getType(subelems[g_dsExportTree_plotTreeTagNames[idx]]) == INT_VAR )
    {
      listing = unImportTree_xmlChildrenListing(xmlDoc, (int)subelems[g_dsExportTree_plotTreeTagNames[idx]]);
      
      if ( idx == fwTrending_PLOT_OBJECT_DPES )
      {
        for ( i = 1 ; i <= dynlen(listing) ; ++i )
        {
          if ( listing[i] != "" )
          {
            // if the system name is present and equals to the current system, replace it by the key #local:
            if ( unExportTree_getSystemName(listing[i]) == "#local:" )
               listing[i] = getSystemName()+unExportTree_removeSystemName(listing[i]);
          }
        }
      }
      
      if ( idx == fwTrending_PLOT_OBJECT_LEGENDS )
      {
        for ( i = 1 ; i <= dynlen(listing) ; ++i )
        {
          if ( listing[i] != "" )
          {
            // if the system name is present and equals to the current system, replace it by the key #local:
            if ( strpos(listing[i] , "#local:" ) >= 0 )
								   strreplace ( listing[i] , "#local:" , getSystemName() );
          }
        }
      }

      ddsPlotData[idx] = listing; 
    }
  }
  
  while ( idx <= g_dsImportTree_plotTreeDataSize ) { ddsPlotData[idx] = makeDynString(); ++idx; }
    
  fwTrending_setPlot(sDpName, ddsPlotData, exInfo);

  // get the horizontal navigation and check them
  unImportTree_devicenavigation_importTagSectionDataXml(sDpName, xmlDoc, elements["devicenavigation"], exInfo);
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_FwTrendingPage_checkTagSectionDataXml
/**
Purpose:
Check the data of the FwTrendingPage device

	@param bTreeNodeDevice: bool, input: true=the function is called from a treenode and the tree node device name is given
	@param sTreeNodeDeviceName: string, input: the tree node device name
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree_FwTrendingPage_checkTagSectionDataXml(bool bTreeNodeDevice, string sTreeNodeDeviceName, int xmlDoc, int xmlTop, bool bKeepLocalSystem, dyn_string &exInfo)
{
string sDpName, sValue, sSystemName;
dyn_string dsSplit;
int i, len, idx, node, pagelen, trendlen;
dyn_dyn_string ddsPageData;
dyn_int children;
mapping elements, subelems;
dyn_string listing;
        
  elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
	
  // check the size: minimum 3 plus list of plot depending on row and col
  if ( mappinglen(elements) < 3 ) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPage_checkTagSectionDataXml: wrong data length", "");
    return;
  }

  // get the dp name and check it
  sDpName = elements["dpname"];
  sSystemName = unExportTree_getSystemName(sDpName);
  if((sSystemName == "#local:") || (sSystemName == ""))
    sDpName = getSystemName()+unExportTree_removeSystemName(sDpName);
  sValue = unExportTree_removeSystemName(sDpName);
  strreplace(sValue, "-", "");
  if(!unImportTree_checkNodeName(sValue) || (sValue == "")) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPage_checkTagSectionDataXml: wrong device dp name, "+sValue, "");
  }
  
  subelems = unImportTree_xmlChildrenMapping(xmlDoc, elements["pageconfiguration"]);
  
  if ( mappinglen(subelems) < 7 ) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPage_checkTagSectionDataXml: wrong pageconfiguration length, "+sValue, "");
  }
    
  // check the page title
  sValue = subelems["pageTitle"];
  if ( !unImportTree_checkNodeName(sValue) || ( sValue == "" ) ) {
    fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPage_checkTagSectionDataXml: wrong page title, "+sValue, "");
  }
  
  // check that the tree node device name and page dpname are the same
  if(bTreeNodeDevice) {
    sValue = sTreeNodeDeviceName;
    sSystemName = unExportTree_getSystemName(sTreeNodeDeviceName);
    if((sSystemName == "#local:") || (sSystemName == ""))
      sValue = getSystemName()+unExportTree_removeSystemName(sValue);
	
    if(sValue != sDpName)
      fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPage_checkTagSectionDataXml: tree device node and page dpname are different, "+sValue+", "+sDpName, "");
  }
  
  // if the dp exists check the content 
  if(dpExists(sDpName)) {
    if(dpTypeName(sDpName) == UN_TRENDTREE_PAGE) {
      
      // get the device data
				fwTrending_getPage(sDpName, ddsPageData, exInfo);
      trendlen = dynlen(ddsPageData);
      
      if ( g_dsImportTree_pageTreeDataSize == -1 ) g_dsImportTree_pageTreeDataSize = trendlen;
      else if ( trendlen != g_dsImportTree_pageTreeDataSize )
      {
        fwException_raise(exInfo, "WARNING", "unImportTree_FwTrendingPage_checkTagSectionDataXml: "+sDpName+
                  ", ddsPageData-Size '"+trendlen+"' differs from last size '"+g_dsImportTree_pageTreeDataSize+"'", "");
        g_dsImportTree_pageTreeDataSize = trendlen;
      }
        
      pagelen = dynlen(g_dsExportTree_pageTreeTagNames);
  
      for ( idx = 1 ; idx <= pagelen ; ++idx )
      {
        if ( idx > trendlen )
        {
          fwException_raise(exInfo, "WARNING", "unImportTree_FwTrendingPage_checkTagSectionDataXml: "+sDpName+
                            ", ddsPageData index "+idx+" '"+g_dsExportTree_pageTreeTagNames[idx]+"' non-existing", "");
          continue;
        }
        
        if ( ! fwTrending_isPageTreeTagExportable(idx) ) { continue; }
    
        if ( getType(subelems[g_dsExportTree_pageTreeTagNames[idx]]) == STRING_VAR )
        {
          if ( dynlen(ddsPageData[idx]) == 0 ) ddsPageData[idx] = makeDynString("");

          if ( ddsPageData[idx][1] != subelems[g_dsExportTree_pageTreeTagNames[idx]] )
            fwException_raise(exInfo, "WARNING", "unImportTree_FwTrendingPage_checkTagSectionDataXml: different '"+
                              g_dsExportTree_pageTreeTagNames[idx]+"', "+
                              sDpName+", current:"+ddsPageData[idx][1]+"; file"+subelems[g_dsExportTree_pageTreeTagNames[idx]], "");
        }
        else if ( getType(subelems[g_dsExportTree_pageTreeTagNames[idx]]) == INT_VAR )
        {
          listing = unImportTree_xmlChildrenListing(xmlDoc, (int)subelems[g_dsExportTree_pageTreeTagNames[idx]]);
      
          if ( idx == fwTrending_PAGE_OBJECT_PLOTS )
          {
            for ( i = 1 ; i <= dynlen(listing) ; ++i )
            {
              if ( unExportTree_getSystemName(listing[i]) == "#local:" )
					          listing[i] = getSystemName()+unExportTree_removeSystemName(listing[i]);
            }
          }
    
          unImportTree_checkConfigDynamicLists(g_dsExportTree_pageTreeTagNames[idx], sDpName, ddsPageData[idx], listing, exInfo); 
        }
      }
    }
    else
      fwException_raise(exInfo, "ERROR", "unImportTree_FwTrendingPage_checkTagSectionDataXml: dpnode exists but wrong type name, "+sDpName, "");
  }
  
  // get the horizontal navigation and check them
  unImportTree_devicenavigation_checkTagSectionDataXml(sDpName, xmlDoc, elements["devicenavigation"], bKeepLocalSystem, exInfo);
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_FwTrendingPage_importTagSectionDataXml
/**
Purpose:
Check the data of the FwTrendingPage device

        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSTreeImport}
*/
unImportTree_FwTrendingPage_importTagSectionDataXml(int xmlDoc, int xmlTop, dyn_string &exInfo)
{
string sDpName, sValue, sSystemName;
dyn_string dsSplit;
int i, len, idx, node, pagelen;
dyn_dyn_string ddsPageData;
dyn_int children;
mapping elements, subelems;
dyn_string listing;
        
  elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);
	
  // get the dp name and check it
  sDpName = elements["dpname"];
  sSystemName = unExportTree_getSystemName(sDpName);
  if(sSystemName == "#local:")
    sDpName = unExportTree_removeSystemName(sDpName);
  if(!dpExists(sDpName)) {
    unImportTree_createDp(sDpName, UN_TRENDTREE_PAGE, exInfo);
  }
  if ( dynlen(exInfo) > 0 ) return;
  
  subelems = unImportTree_xmlChildrenMapping(xmlDoc, elements["pageconfiguration"]);
  
  pagelen = dynlen(g_dsExportTree_pageTreeTagNames);
  
  for ( idx = 1 ; idx <= pagelen ; ++idx )
  {
    ddsPageData[idx] = makeDynString("");
    
    if ( ! fwTrending_isPageTreeTagExportable(idx) ) { continue; }
    
    if ( mappingHasKey(subelems,g_dsExportTree_pageTreeTagNames[idx]) == FALSE )
       subelems[g_dsExportTree_pageTreeTagNames[idx]] = "";
    
    if ( getType(subelems[g_dsExportTree_pageTreeTagNames[idx]]) == STRING_VAR )
    {
      if ( dynlen(ddsPageData[idx]) == 0 ) ddsPageData[idx] = makeDynString("");
      
      ddsPageData[idx][1] = subelems[g_dsExportTree_pageTreeTagNames[idx]];
    }
    else if ( getType(subelems[g_dsExportTree_pageTreeTagNames[idx]]) == INT_VAR )
    {
      listing = unImportTree_xmlChildrenListing(xmlDoc, (int)subelems[g_dsExportTree_pageTreeTagNames[idx]]);
      
      if ( idx == fwTrending_PAGE_OBJECT_PLOTS )
      {
        for ( i = 1 ; i <= dynlen(listing) ; ++i )
        {
          if ( unExportTree_getSystemName(listing[i]) == "#local:" )
					      listing[i] = getSystemName()+unExportTree_removeSystemName(listing[i]);
        }
      }
    
      ddsPageData[idx] = listing;
    }
  }
  
  while ( idx <= g_dsImportTree_pageTreeDataSize ) { ddsPageData[idx] = makeDynString(); ++idx; }
  
  /*
  // get pageconfiguration
  sValue = elements["pageconfiguration"];
  dsSplit = strsplit(sValue, UN_PARAMETER_DELIMITER);
		while(dynlen(dsSplit) < 5)
			dynAppend(dsSplit, "");
		ddsPageData[fwTrending_PAGE_OBJECT_MODEL][1] = dsSplit[1];
		ddsPageData[fwTrending_PAGE_OBJECT_TITLE][1] = dsSplit[2];
		ddsPageData[fwTrending_PAGE_OBJECT_NCOLS][1] = dsSplit[3];
		ddsPageData[fwTrending_PAGE_OBJECT_NROWS][1] = dsSplit[4];
		ddsPageData[fwTrending_PAGE_OBJECT_CONTROLS][1] = dsSplit[5];
                // get pageplottemplateconfiguration
		sValue = elements["pageplottemplateconfiguration"];
		ddsPageData[fwTrending_PAGE_OBJECT_PLOT_TEMPLATE_PARAMETERS] = strsplit(sValue, UN_PARAMETER_DELIMITER);
                // get the lists of plots
		iNavIndex = 1;
		dsNavData = makeDynString();

                xmlChildNodes(xmlDoc, elements["plotconfiguration"], children);

		len = dynlen(ddsPageData[fwTrending_PAGE_OBJECT_PLOTS]);
		sscanf(ddsPageData[fwTrending_PAGE_OBJECT_NCOLS][1], "%d", nCols);
		sscanf(ddsPageData[fwTrending_PAGE_OBJECT_NROWS][1], "%d", nRows);
		if(nCols >= fwTrending_MAX_COLS)
			nCols = fwTrending_MAX_COLS;
		if(nRows >= fwTrending_MAX_ROWS)
			nRows = fwTrending_MAX_ROWS;
		for(iCol=1;iCol<=nCols;iCol++) {
			for(iRow=1;iRow<=nRows;iRow++) {
                                for(i=1;i<=dynlen(children);i++) {
                                        sTagName = xmlNodeName(xmlDoc,children[i]);
                                        
                                        if ( sTagName == "plotC"+iCol+"R"+iRow ) {
                                                node = xmlFirstChild(xmlDoc,children[i]);
                                                if ( node < 0 ) sValue = "";
                                                else sValue = xmlNodeValue(xmlDoc,node);
				                if (unExportTree_getSystemName(sValue) == "#local:")
					           sValue = getSystemName()+unExportTree_removeSystemName(sValue);
				                ddsPageData[fwTrending_PAGE_OBJECT_PLOTS][iCol+fwTrending_MAX_ROWS*(iRow -1)] = sValue;
                                        }
                                }
			}
		}
    
    */
  
  
  fwTrending_setPage(sDpName, ddsPageData, exInfo);
		
  // get the horizontal navigation and check them
  unImportTree_devicenavigation_importTagSectionDataXml(sDpName, xmlDoc, elements["devicenavigation"], exInfo);
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_devicenavigation_checkTagSectionDataXml
/**
Purpose:
Check the device navigation

	@param sDpName: string, input: the data point name
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param bKeepLocalSystem: bool, input, keep the local system name, do not replace it by #local
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_devicenavigation_checkTagSectionDataXml(string sDpName, int xmlDoc, int xmlTop, bool bKeepLocalSystem, dyn_string &exInfo)
{
	int i;
	string sLink, sXMLName, sTagName, sValue, sConf;
        dyn_int children;
	dyn_string dsSplit;
	dyn_string panelLabel, panelNameList, panelDollarParam;
        mapping elements;
        
        elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);

        xmlChildNodes(xmlDoc, xmlTop, children);
        
        // check the size: 10
	if(mappinglen(elements) < 10) {
		fwException_raise(exInfo, "ERROR", "unImportTree_devicenavigation_checkTagSectionDataXml: wrong data length", "");
	}
	else {
                // if the device is on exists and the links are different --> WARNING
		if(dpExists(sDpName)) {
			unPanel_getPanelList(sDpName, panelLabel, panelNameList, panelDollarParam, exInfo);

			for(i=1; i<=c_unPanelConfiguration_maxPanelNavigation; i++) {
				sprintf(sLink, "link%d", i);
				sXMLName = panelNameList[i];
				if(unExportTree_getSystemName(sXMLName) == getSystemName())
					sXMLName = "#local:"+unExportTree_removeSystemName(sXMLName);
				sConf = panelLabel[i]+c_panel_navigation_delimiter+sXMLName+c_panel_navigation_delimiter+panelDollarParam[i];
                                sValue = elements[sLink];
                                sTagName = xmlNodeName(xmlDoc,children[i]);
				if(sValue == "")
					sValue = "||";
				if(sConf != sValue) {
						fwException_raise(exInfo, "WARNING", "unImportTree_devicenavigation_checkTagSectionDataXml: "+sDpName+" "+sLink+" value is different, "+sConf+", "+sValue, "");
				}
			}
		}
		else {
			sConf = unExportTree_getSystemName(sDpName);
			if(sConf == "")
				sConf = getSystemName();
			if(sConf != getSystemName())
				fwException_raise(exInfo, "ERROR", "unImportTree_devicenavigation_checkTagSectionDataXml: dp on remote system and not found, "+sDpName, "");
		}
		for(i=1;i<=10;i++) {
			sprintf(sLink, "link%d", i);
                        sTagName = xmlNodeName(xmlDoc,children[i]);
			if(sTagName != sLink)
				fwException_raise(exInfo, "ERROR", "unImportTree_devicenavigation_checkTagSectionDataXml: "+sDpName+"wrong or missing "+sLink+" tag name, "+sTagName, "");
		}
	}
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_devicenavigation_importTagSectionDataXml
/**
Purpose:
Check the device navigation

	@param sDpName: string, input: the data point name
        @param xmlDoc: int, input: XML document reference
        @param xmlTop: int, input: XML Top-node reference
	@param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_devicenavigation_importTagSectionDataXml(string sDpName, int xmlDoc, int xmlTop, dyn_string &exInfo)
{
	int i;
	string sLink, sValue, sXML;
	dyn_string dsSplit;
	dyn_string panelLabel, panelNameList, panelDollarParam;
        mapping elements;
        
        elements = unImportTree_xmlChildrenMapping(xmlDoc, xmlTop);

        if(dpExists(sDpName)) {
		for(i=1; i<=c_unPanelConfiguration_maxPanelNavigation; i++) {
			sprintf(sLink, "link%d", i);
			sValue = elements[sLink];
			if(sValue == "")
				sValue = "||";
	
			dsSplit = strsplit(sValue, "|");
			while(dynlen(dsSplit)<3)
				dynAppend(dsSplit, "");
			panelLabel[i] = dsSplit[1];
			panelNameList[i] = dsSplit[2];
			panelDollarParam[i] = dsSplit[3];
	
			sXML = panelNameList[i];
			if(unExportTree_getSystemName(sXML) == "#local:")
				sXML = getSystemName()+unExportTree_removeSystemName(sXML);
			panelNameList[i] = sXML;
		}
		unPanel_setPanelList(sDpName, panelLabel, panelNameList, panelDollarParam, exInfo);
	}
	else
		fwException_raise(exInfo, "ERROR", "unImportTree_devicenavigation_importTagSectionDataXml: dp does not exists "+sDpName, "");
}


//------------------------------------------------------------------------------------------------------------------------

// Below this are all the routines whicch have been untouched from the pre-Xml world

//------------------------------------------------------------------------------------------------------------------------


// unImportTree_initialize
/**
Purpose:
Executes the initialization of the unTreeWidget used to view the list of trees. This function is executed at startup of the unTreeWidget.
The unTreeWidget tree is disabled during the operation. This function is started in a separate thread.

	@param sGraphTree: string, input: the name of the unTreeWidget graphical element
	@param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
	@param bClipboard: bool, input: true show the clipbpard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget and 
	the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unImportTree_initialize(string sGraphTree, string sBar, bool bClipboard)
{
  string imageClassParameters;
  shape aShape=getShape(sGraphTree);
  dyn_string exceptionInfo;

  unTrendTree_loadImage("", "", exceptionInfo);
  unWindowTree_loadImage("", "", exceptionInfo);
  unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
  unTreeWidget_initialize(sGraphTree, sBar, bClipboard);
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_EventNodeLeft
/**
Purpose:
Function called called when a left click on a node of the unTreeWidget is done during the operation of the 
tree hierarchy. This function get the operation panel of the node, show it in the main frame.

	@param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
	@param sMode: string, input: the type of mouse click
	@param sCompositeLabel: string, input: the composite label of the node
	@param sGraphTree: string, input: the name of the unTreeWidget graphical element
	@param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
	@param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
	@param iPosX: int, input: x position of the mouse
	@param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. the JCOP hierarchy management
	. to be used with the unTreeWidget and 
	the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unImportTree_EventNodeLeft(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
	string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sNodeName;
	dyn_string exInfo;
	shape aShape;
	int id;
	string sParentKey, sCurrentSelectedNodeKey, sTopDisplayName;
	int iCurrentPointer, iMousePointer;
	string sFunctionName;
		
//DebugN("unImportTree_EventNodeLeft", sSelectedKey, sMode, sCompositeLabel, sGraphTree, sBar, bClipboard, iPosX, iPosY);
	
// start the progress bar
	id = unProgressBar_start(sBar);	
	aShape = getShape(sGraphTree);
//!!!!!!!!!!!!!! Do not disable the unTreeWidget tree because otherwise the dbl click to expand or collapse will not work.
//	unTreeWidget_setPropertyValue(aShape, "Enabled", false, true);
	iCurrentPointer = unTreeWidget_getPropertyValue(aShape, "getMousePointer", true);
	iMousePointer = unTreeWidget_ccHourglass;
	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iMousePointer, true);
	
// The unTreeWidget is not disable to allow a doubleClick as also expand

	sParentKey = unTreeWidget_getPropertyValue(aShape, "selectedParent", true);
	sCurrentSelectedNodeKey = unTreeWidget_getPropertyValue(aShape, "getNodeSelectedkey", true);

	unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
// get the device name & check it exists
	sNodeName = _fwTree_getNodeName(sDpName);

	switch(sMode){
		case "NodeLeftClick":
			if(dpExists("fwTT_"+sNodeName))
				dpTreeName.text = "fwTT_"+sNodeName;
			else
				dpTreeName.text = "";
			nodeName.text = sNodeName;
			dpNodeName.text = sDpName;
			break;
		default:
			break;
	}
	
// handle any errors here
	if(dynlen(exInfo)>0)
		fwExceptionHandling_display(exInfo);

// stop the progress bar
	unProgressBar_stop(id, sBar);
	unTreeWidget_setPropertyValue(aShape, "setMousePointer", iCurrentPointer, true);
//!!!!!!!!!!! as the unTreeWidget is not enabled then do not re-enabled it.
//	unTreeWidget_setPropertyValue(aShape, "Enabled", true, true);
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_checkDpName
/**
Purpose:
Check if the dpname is correct: true/false=correct/not correct

	@param sDpName: string, input: the datapoint name to check

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
bool unImportTree_checkDpName(string sDpName)
{
	bool bResult = true;
	string sTemp=sDpName;
	string sSystemName = unExportTree_getSystemName(sDpName);
	
	if((sSystemName == "#local:") || (sSystemName == getSystemName())) {
		strreplace(sTemp, "-", "");
		bResult = unImportTree_checkNodeName(unExportTree_removeSystemName(sTemp));
	}
	return bResult;
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_createDp
/**
Purpose: create datapoint

Parameters:
		@param sDpName, string, input, datapoint name
		@param sDpType, string , input, datapoint type
		@param exceptionInfo, dyn_string, output, for errors
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unImportTree_createDp(string sDpName, string sDpType, dyn_string &exceptionInfo)
{
	string deviceName=sDpName;
	bool nameOk = false;
	int pos = strpos(sDpName,":");
	
	if(pos >=0)
		deviceName = substr(sDpName,strpos(sDpName,":")+1);
	if (dpExists(deviceName) == false)
	{
		dpCreate(deviceName, sDpType);
		if (dpExists(deviceName) == false)
		{
			fwException_raise(exceptionInfo,"ERROR","unImportTree_createDp: " + deviceName + getCatStr("unGeneration","DPCREATEFAILED"),"");
		}
	}
}


//------------------------------------------------------------------------------------------------------------------------

// The Silent Versions of Import

//------------------------------------------------------------------------------------------------------------------------


// unImportTree_checkSilent
/**
Purpose: check in silent mode

Parameters:
		@param bKeepSystemName, bool , input, true=keep the system name for the plot DPE/false=replace the system name by #local:
		@param bImportInto, bool, input, true=import into sImportIntoNodeName/false=do not import into sImportIntoNodeName
		@param sImportIntoNodeName, string , input, the node name to import the data into
		@param fileName, string , input, the filename of the Xml-file to be checked
		@param exError, dyn_string, output, for errors
		@param exWarning, dyn_string, output, for warnings
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unImportTree_checkSilent(bool bKeepSystemName, bool bImportInto, string sImportIntoNodeName, string sFileName, 
			  dyn_string &exError, dyn_string &exWarning)
{
dyn_string exInfoTemp;
int len, pos, i;
bool bIgnore, bImport, bWarning, bOneWarning;
string sTreeName, sValue, sTagName;
bool bEmpty;
bool bCheckTree = false;
	
int xmlDoc,xmlTop,xmlNod;
string errMsg;
int errLine,errColumn;
int xmlLen,xmlIdx;
dyn_int xmlEls,xmlChn,xmlDev;
int versionnumber;

  if(bImportInto && (sImportIntoNodeName == "")) {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: empty node for importing too!", "");
    return;
  }
                  
  if ( ( xmlDoc = xmlDocumentFromFile(sFileName,errMsg,errLine,errColumn) ) < 0 ) {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: XML-file error lin="+errLine+" col="+errColumn+" msg="+errMsg, "");
    return;
  }
        
  if ( ( xmlTop = xmlFirstChild(xmlDoc) ) < 0 ) {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: XML-file must have top-node = 'tree'", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  while ( xmlNodeType(xmlDoc,xmlTop) != XML_ELEMENT_NODE )
  {
    if ( ( xmlTop = xmlNextSibling(xmlDoc,xmlTop) ) == -1 ) break;
  }
  if ( xmlTop == -1 )
  {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: XML-file must have top-node = 'tree'", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  if ( ( xmlNodeName(xmlDoc,xmlTop) ) != "tree" ) {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: XML-file must have top-node = 'tree'", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  if ( ( xmlGetElementAttribute(xmlDoc,xmlTop,"version",versionnumber) ) != 0 ) {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: XML-file tree-node must have version attribute", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  if ( versionnumber != "2" ) {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: XML-file tree-node must have version='2' attribute", "");
    xmlCloseDocument(xmlDoc);
    return;
  }
       
  if ( xmlChildNodes(xmlDoc,xmlTop,xmlEls) != 0 ) {
    fwException_raise(exError, "ERROR", "unImportTree_checkSilent: Can not obtain XML-file node-children", "");
    xmlCloseDocument(xmlDoc);
    return;
  }
                       
  xmlLen = dynlen(xmlEls);
  unImportTree_checkTreeDataXml(xmlDoc, xmlEls, sTreeName, bKeepSystemName, exInfoTemp);
        
  if(sTreeName != "")
    bCheckTree = true;
        
  if(dynlen(exInfoTemp) >0) {
    if(dynContains(exInfoTemp, "ERROR") >0) 
      dynAppend(exError, exInfoTemp);
    else {
      if(dynlen(dynPatternMatch("unImportTree_checkTreeDataXml: no tree definition*", exInfoTemp)) > 0) {
        bEmpty = true;
      }
      else
        dynAppend(exWarning, exInfoTemp);
    }
  }
        
  if ( bImportInto && bEmpty && ( sImportIntoNodeName != "" ) ) {
    // replace the first instance of parentname by the selected parent.
    xmlChildNodes(xmlDoc,xmlEls[4],xmlChn);
    for ( i = 1 ; i <= dynlen(xmlChn); ++i ) {
      if ( xmlNodeName(xmlDoc,xmlChn[i]) == "parentname" ) {
        xmlNod = xmlFirstChild(xmlDoc,xmlChn[i]);
        if ( xmlNod < 0 )
           xmlAppendChild(xmlDoc,xmlChn[i],XML_TEXT_NODE,sImportIntoNodeName);
        else
           xmlSetNodeValue(xmlDoc,xmlNod,sImportIntoNodeName);
      }
    }
  }

  for ( xmlIdx = 4 ; xmlIdx <= xmlLen ; ++xmlIdx )
  {
    sTagName = xmlNodeName(xmlDoc,xmlEls[xmlIdx]);
    exInfoTemp = makeDynString();
                                
    switch(sTagName) {
      case "":
        // do nothing
      break;
      case "otherdevice":
        // case of other devices, skip the <otherdevice> line and get the next tag section data
        if ( xmlChildNodes(xmlDoc,xmlEls[xmlIdx],xmlDev) == 0 )
        {
          dynAppend(xmlEls,xmlDev);
          xmlLen = dynlen(xmlEls);
        }
      break;
      default:
        // only the first time check if the treename is the same as the first node name.
        unImportTree_checkTagSectionDataXml(bCheckTree, sTreeName, sTagName, xmlDoc, xmlEls[xmlIdx], bKeepSystemName, exInfoTemp);
                                                
        if(bCheckTree) {
          bCheckTree = false;
          sTreeName = "";
        }
        if(dynlen(exInfoTemp) >0) {
          if(dynContains(exInfoTemp, "ERROR") >0)
            dynAppend(exError, exInfoTemp);
          else
            dynAppend(exWarning, exInfoTemp);
        }
      break;
    }
  }
        
  xmlCloseDocument(xmlDoc);
}


//------------------------------------------------------------------------------------------------------------------------


// unImportTree_importSilent
/**
Purpose: import in silent mode

Parameters:
		@param bKeepSystemName, bool , input, true=keep the system name for the plot DPE/false=replace the system name by #local:
		@param bImportInto, bool, input, true=import into sImportIntoNodeName/false=do not import into sImportIntoNodeName
		@param sImportIntoNodeName, string, input, the node name to import the data into
		@param fileName, string , input, the filename of the Xml-file to be imported
		@param exError, dyn_string, output, for errors
		@param exWarning, dyn_string, output, for warnings
		
Usage: External function

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 3.0 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/

unImportTree_importSilent(bool bKeepSystemName, bool bImportInto, string sImportIntoNodeName, string sFileName, 
			  dyn_string &exError, dyn_string &exWarning)
{
string sTagName;
dyn_string exInfoTemp;
int len, pos, i;
bool bIgnore, bImport, bWarning, bOneWarning;
string sTreeName, sValue;
bool bEmpty;
bool bCheckTree = false;
		
int xmlDoc,xmlTop,xmlNod;
string errMsg;
int errLine,errColumn;
int xmlLen,xmlIdx;
dyn_int xmlEls,xmlChn,xmlDev;
int versionnumber;
    
  if(bImportInto && (sImportIntoNodeName == "")) {
    fwException_raise(exError, "ERROR", "unImportTree_importSilent: empty node", "");
    return;
  }
	
  if ( ( xmlDoc = xmlDocumentFromFile(sFileName,errMsg,errLine,errColumn) ) < 0 ) {
    fwException_raise(exError, "ERROR", "unImportTree_importSilent: XML-file error lin="+errLine+" col="+errColumn+" msg="+errMsg, "");
    return;
  }

  if ( ( xmlTop = xmlFirstChild(xmlDoc) ) < 0 ) {
    fwException_raise(exError, "ERROR", "unImportTree_importSilent: XML-file must have top-node = 'tree'", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  while ( xmlNodeType(xmlDoc,xmlTop) != XML_ELEMENT_NODE )
  {
    if ( ( xmlTop = xmlNextSibling(xmlDoc,xmlTop) ) == -1 ) break;
  }
  if ( xmlTop == -1 )
  {
    fwException_raise(exError, "ERROR", "unImportTree_importSilent: XML-file must have top-node = 'tree'", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  if ( ( xmlNodeName(xmlDoc,xmlTop) ) != "tree" ) {
    fwException_raise(exError, "ERROR", "unImportTree_importSilent: XML-file must have top-node = 'tree'", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  if ( ( xmlGetElementAttribute(xmlDoc,xmlTop,"version",versionnumber) ) != 0 ) {
    fwException_raise(exError, "ERROR", "unImportTree_importSilent: XML-file tree-node must have version attribute", "");
    xmlCloseDocument(xmlDoc);
    return;
  }

  if ( versionnumber != "2" ) {
    fwException_raise(exError, "ERROR", "unImportTree_importSilent: XML-file tree-node must have version='2' attribute", "");
    xmlCloseDocument(xmlDoc);
    return;
  }
        
	if ( xmlChildNodes(xmlDoc,xmlTop,xmlEls) != 0 ) {
                xmlCloseDocument(xmlDoc);
		fwException_raise(exError, "ERROR", "unImportTree_importSilent: Can not obtain XML-file node-children", "");
                return;
  }
   
  bImport = false;
  bWarning = false; 
        
  xmlLen = dynlen(xmlEls);
  unImportTree_checkTreeDataXml(xmlDoc, xmlEls, sTreeName, bKeepSystemName, exInfoTemp);        
                
  if(sTreeName != "")
    bCheckTree = true;
        
  if(dynlen(exInfoTemp) >0) {
    if(dynContains(exInfoTemp, "ERROR") >0) 
      dynAppend(exError, exInfoTemp);
    else {
      bImport = true;
      bWarning = true;
      if(dynlen(dynPatternMatch("unImportTree_importSilent: no tree definition*", exInfoTemp)) > 0) {
        bImport = true;
        bWarning = true;
        bEmpty = true;
      }
      else
        dynAppend(exWarning, exInfoTemp);
    }
  }
  else
    bImport = true;

  if ( bImport ) {
    // importing tree
    exInfoTemp = makeDynString();
    unImportTree_importTreeDataXml(xmlDoc, xmlEls, exInfoTemp);
    if(dynlen(exInfoTemp) >0) {
    if(dynContains(exInfoTemp, "ERROR") >0)
      dynAppend(exError, exInfoTemp);
    }
    else {
      if(bWarning) {
        bOneWarning = true;
      }
    }
  }

  if ( bImport && bImportInto && ( sImportIntoNodeName != "" ) ) {
    // replace the first instance of parentname by the selected parent.
    xmlChildNodes(xmlDoc,xmlEls[4],xmlChn);
    for ( i = 1 ; i <= dynlen(xmlChn); ++i ) {
      if ( xmlNodeName(xmlDoc,xmlChn[i]) == "parentname" ) {
        xmlNod = xmlFirstChild(xmlDoc,xmlChn[i]);
        if ( xmlNod < 0 )
           xmlAppendChild(xmlDoc,xmlChn[i],XML_TEXT_NODE,sImportIntoNodeName);
        else
           xmlSetNodeValue(xmlDoc,xmlNod,sImportIntoNodeName);
      }
    }
  }

  for ( xmlIdx = 4 ; xmlIdx <= xmlLen ; ++xmlIdx )
  {
    sTagName = xmlNodeName(xmlDoc,xmlEls[xmlIdx]);
    exInfoTemp = makeDynString();
    bImport = false;
    bWarning = false;
                
    switch(sTagName) {
      case "":
        // do nothing
      break;
      case "otherdevice":
        // case of other devices, skip the <otherdevice> line and get the next tag section data
        if ( xmlChildNodes(xmlDoc,xmlEls[xmlIdx],xmlDev) == 0 )
        {
          dynAppend(xmlEls,xmlDev);
          xmlLen = dynlen(xmlEls);
        }
      break;
      default:
        unImportTree_checkTagSectionDataXml(bCheckTree, sTreeName, sTagName, xmlDoc, xmlEls[xmlIdx], bKeepSystemName, exInfoTemp);
        if(bCheckTree) {
          bCheckTree = false;
          sTreeName = "";
        }
        if(dynlen(exInfoTemp) >0) {
          if(dynContains(exInfoTemp, "ERROR") >0)
            dynAppend(exError, exInfoTemp);
          else {
            bImport = true;
            bWarning = true;
            dynAppend(exWarning, exInfoTemp);
          }
        }
        else
          bImport = true;

        if(bImport) {
          exInfoTemp = makeDynString();
          unImportTree_importTagSectionDataXml(sTagName, xmlDoc, xmlEls[xmlIdx], exInfoTemp);
          if(dynlen(exInfoTemp) >0) {
            if(dynContains(exInfoTemp, "ERROR") >0)
              dynAppend(exError, exInfoTemp);
            else
              dynAppend(exWarning, exInfoTemp);
          }
          else {
            if(bWarning) {
              dynAppend(exWarning, exInfoTemp);
              bOneWarning = true;
            }
          else
            dynAppend(exWarning, exInfoTemp);
          }
        }
      break;
    }
  }
        
  xmlCloseDocument(xmlDoc);
}


//------------------------------------------------------------------------------------------------------------------------


//@}




