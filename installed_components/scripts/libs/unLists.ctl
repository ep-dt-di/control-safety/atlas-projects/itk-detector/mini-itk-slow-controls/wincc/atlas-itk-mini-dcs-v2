#uses "ctrlEventList"

/**@name LIBRARY: unLists.ctl

@author Frederic BERNARD (AB/CO/IS)

Creation Date: 07/11/05

Modification History:
  30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

	-10/07/2007: Herve
		unLists_getApplicationsList: get all application of all front-end type instead of application from _UnPlc and S7_PLC front-end type
		
	-01/11/2006 Frederic: Add function
		unLists_SelectDeselect_line()

	-21/08/2006 Frederic: Add functions (to be used by QPS, UNICOS, INTERLOCKS...)
		unLists_saveOldGlobal()
		unLists_getDisplayBuffer()
		unLists_checkRequest()
		unLists_getInternalTimeRequest()

	- 12/07/2006 Frederic: Add functions
		unLists_getApplicationsList()
		unLists_getDomainNatureList()
		unLists_getFromStatement()
		unLists_displayFilterList()
	
	- 06/04/06 Frederic: Add 	unLists_setSystemState()

version 1.0

External functions:
	- unLists_getTimeRequest
	- unLists_initTimeInfo
	
Internal functions: 
			
Purpose:
This library contains function(s) associated with the EventList, ObjectList and Alarm List (Cryo)

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL) and UI (WCCOAui)

Constraints: 
	. constant:
	. the following panels must exist
	. PVSS version: 3.0 
	. operating system: WXP and Linux
	. distributed system: yes.
*/

//@{

// unLists_getTimeRequest
/**
Purpose:
get good Time request according to UTC or Local Environment (used by EventList, ObjectList, History of MessageText)

@param requestBegin: begin time request
@param requestEnd: end time request
@param bStart: user want to start query?

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL) and UI (WCCOAui)

Constraints: 
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
unLists_getTimeRequest(time &requestBegin, time &requestEnd, bool &bStart)
{	
bool bPeriod;
dyn_float df;
dyn_string ds;
time tNow;

	tNow=getCurrentTime();	//local time
	
	if (g_bUTC)
		{	//to display as local time
		requestEnd=CtrlEventList_displayLocalTimefromTime(requestEnd);			//dll
		requestBegin=CtrlEventList_displayLocalTimefromTime(requestBegin);	//dll
		}		

	if (requestEnd>tNow)
		requestEnd=tNow;

	bPeriod=CtrlEventList_checkTimePeriod(requestBegin, requestEnd);			//dll
	
	if (bPeriod)
		{
		bStart=false;
		unGraphicalFrame_ChildPanelOnModal("vision/MessageInfo", "Information", 
													makeDynString("$1:\"From Time\" request is between Summer -> Winter or Winter -> Summer switch period. To get all data set \"From Time\" <= 23:59:59 (UTC time format).", "$2:CONTINUE", "$3:ABORT"),
													50, 50);
													
		if (dynlen(df)>0)
			{
			if (df[1]==1)
				bStart=true;
			}
		}
}

// unLists_initTimeInfo
/**
Purpose:
display Time mode

@param iViewUTC: switch to display UTC time

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints: 
	. PVSS version: 3.0 
	. operating system: WXP
	. distributed system: yes.
*/
unLists_initTimeInfo(const int iViewUTC)
{
	if (iViewUTC==1)
		setMultiValue("txttime", "text", "U\nT\nC\n",
									"txttime", "toolTipText", "Display Time in UTC mode");
	else if (iViewUTC==0)
		setMultiValue("txttime", "text", "L\nT\nI\n",
									"txttime", "toolTipText", "Display Time in LOCAL mode");
	else
		setMultiValue("txttime", "text", "",
									"txttime", "toolTipText", "");

}

//-----------------------------------------------------------------------------------------------------------------------------

unLists_displayAliasFilterList(string combobox, string sFilter, string sPanel="vision/unEventList/unAliasFilter.pnl")
{
int iPos, i, iLen;
string sSelect, sRefuse, sFiler;
shape comboShape, comboShape2, comboShape3;
dyn_float dfReturn;
dyn_string dsOutputs, dsParameters;


	if (shapeExists(combobox) && shapeExists(combobox + "2") && shapeExists(combobox + "3"))
		{
		comboShape = getShape(combobox);
		comboShape2 = getShape(combobox + "2");
		comboShape3 = getShape(combobox + "3");		
		if (comboShape.text() == UN_OBJECT_LIST_LIST_NAME)
			{
			sSelect=comboShape2.text;
			sRefuse=comboShape3.text;
			dsParameters = makeDynString("$sSelect:" + sSelect, "$sRefuse:" + sRefuse, "$sFilter:" + sFilter);
			unGraphicalFrame_ChildPanelOnCentralModalReturn(sPanel, "Define filter", dsParameters, dfReturn, dsOutputs);
			
			sSelect="";
			sRefuse="";	
			
			iLen=dynlen(dfReturn);
			if (iLen>0)
				{
				if (dfReturn[1]==1.0)			// new filter saved
					{
					iLen=dynlen(dsOutputs);
					if (iLen>0)
						{
						iPos=dynContains(dsOutputs, UN_GRANTED_REJECTED_DELIMITER);
						if (iPos>1)
							{
							for (i=1; i<iPos; i++)					//GRANTED
								{
								if (dsOutputs[i]==ALL_FILTER_SELECTED)
									{
									i=iPos;		//to exit
									sSelect=ALL_FILTER_SELECTED;
									}
								else
									{
									if (sSelect=="")
										sSelect=dsOutputs[i] + UN_OBJECT_LIST_FILTER_COMBO_DELIMITER;
									else
										sSelect=sSelect + dsOutputs[i] + UN_OBJECT_LIST_FILTER_COMBO_DELIMITER;						
									}
								}
								
							for (i=iPos+1; i<=iLen; i++)		//REJECTED
								{
								if (sRefuse=="")
									sRefuse=dsOutputs[i] + UN_OBJECT_LIST_FILTER_COMBO_DELIMITER;
								else
									sRefuse=sRefuse + dsOutputs[i] + UN_OBJECT_LIST_FILTER_COMBO_DELIMITER;
								}
							}
						}
					}
				else		// new filter not saved
					{
					sSelect=comboShape2.text;
					sRefuse=comboShape3.text;
					}
				}
							
			if (sSelect=="" && sRefuse=="")
				{
				sFiler=UN_OBJECT_LIST_FILTER_ALL;	
				}
			else if (sSelect==UN_OBJECT_LIST_FILTER_ALL && sRefuse=="")
				{
				sFiler=UN_OBJECT_LIST_FILTER_ALL;
				sSelect="";
				}
			else if (sSelect==UN_OBJECT_LIST_FILTER_ALL && sRefuse!="")
				{
				sFiler=UN_OBJECT_LIST_LIST_NAME;
				sSelect+=UN_OBJECT_LIST_FILTER_COMBO_DELIMITER;			
				}
			else if (sSelect!="")
				{
				sFiler=UN_OBJECT_LIST_LIST_NAME;
				}
								
			}
		else
			{
			sFiler=UN_OBJECT_LIST_FILTER_ALL;
			sSelect="";
			sRefuse="";
			}
			
		comboShape.text=sFiler;
		comboShape2.text=sSelect;
		comboShape3.text=sRefuse;	
	}
}

//-----------------------------------------------------------------------------------------------------------------------------

unLists_setSystemState()
{
int i, len, pos;
string sListOfSystem, sColour = "_3DFace", sTempSystem, sSystem;

	len = mappinglen(g_bSystemState);
	
	for(i=1;i<=len;i++)
		{
		sSystem=mappingGetKey(g_bSystemState, i);
		
		pos = strpos(sSystem, ":");
		if(pos > 0)
			sTempSystem = substr(sSystem, 0, pos);
		else
			sTempSystem = sSystem;
			
		if(getSystemName() == sSystem)
			sListOfSystem = sListOfSystem+"["+sTempSystem+"] ";
		else //remote system 
			{
			if(!mappingGetValue(g_bSystemState, i))//not connected and not local
				{
				sColour = "unAlarm_DigitalBad";
				sListOfSystem = sListOfSystem+"[("+sTempSystem+")] ";
				}
			else
				sListOfSystem = sListOfSystem+"["+sTempSystem+"] ";
			}
		}	  

	listOfSystem.text = sListOfSystem;
	listOfSystem.backCol = sColour;
}

//-----------------------------------------------------------------------------------------------------------------------------
// loadDomainNatureList function
// This function is called to know the Domain/Nature list

void unLists_getDomainNatureList(dyn_string dsAdd, dyn_string &dsDomain, dyn_string &dsNature)
{
dyn_string dsTemp;

	unGenericDpFunctions_getDomainNatureList(dsDomain, dsNature);

	dsTemp=dsAdd;

	dynInsertAt(dsDomain, g_filterAll, 1);
	dynAppend(dsDomain, "");
	dynAppend(dsDomain, dsTemp);	
	dynUnique(dsDomain);	

	dynInsertAt(dsNature, g_filterAll, 1);
	dynAppend(dsNature, "");
	dynAppend(dsNature, dsAdd);
	dynUnique(dsNature);	
}

//-----------------------------------------------------------------------------------------------------------------------------
// loadApplicationsList function
// This function is called to know the applications list

void unLists_getApplicationsList(const dyn_string dsAdd, dyn_string &dsApplication)
{
int len, lenj, i, j, iPos, iSystem, iRes;
string sSystem;
dyn_string dsFrontEnd, dsPlcNames, dsTemp;
	
	dynClear(g_ddsSubApplications);
	dynClear(g_dsSubApplications);
	
	// 1. get all the front-end
	unGenericDpFunctions_getFrontEndDeviceType(dsFrontEnd);
		
	dynClear(dsPlcNames);
	len = dynlen(dsFrontEnd);
	for(i=1;i<=len;i++)
		dynAppend(dsPlcNames, dpNames("*:*",dsFrontEnd[i]));

	dynUnique(dsPlcNames);		
	len=dynlen(dsPlcNames);
	for(i=1; i<=len; i++)	//for each PLC on all Systems
	{						
		// 2. get all the applications
		if (dsPlcNames[i] != "")
		{
			sSystem = unGenericDpFunctions_getSystemName(dsPlcNames[i]);
			if (dpExists(dsPlcNames[i]))
				iRes=dpGet(dsPlcNames[i] + ".configuration.subApplications", dsTemp);
			
			iPos = dynContains(dsTemp, "COMMUNICATION");
			if(iPos > 0)
				dynRemove(dsTemp, iPos);
			
			lenj=dynlen(dsTemp);
			for(j=1; j<=lenj; j++)		//for each application
			{
				iPos = dynContains(g_dsSubApplications, dsTemp[j]);
				if (iPos>0)						//application already set...
				{
					iSystem = dynContains(g_ddsSubApplications[iPos], sSystem);
					if (iSystem<=0)			//...but not for this system name
					{
						dynAppend(g_ddsSubApplications[iPos], sSystem);	
					}
				}
				else
				{
					dynAppend(g_ddsSubApplications, dsTemp[j]);	
					dynAppend(g_dsSubApplications, dsTemp[j]);	
					
					iPos = dynContains(g_ddsSubApplications, dsTemp[j]);
					if (iPos>0)					//application set correctly
					{
						dynAppend(g_ddsSubApplications[iPos], sSystem);	
					}
				}
			}
		}	
	}
		
	dsApplication=g_dsSubApplications;
	dynSortAsc(dsApplication);
	dynInsertAt(dsApplication, g_filterAll, 1);
	dynAppend(dsApplication, dsAdd);	
}

//-----------------------------------------------------------------------------------------------------------------------------

string unLists_getFromStatement(string sSystem)
{
dyn_string dsFrom;
string sFrom;
bool bOk;
int i, iLen, iRes;
	
	if (g_sFrom=="*")
		{
		sFrom="*." + g_sDPEPattern;
		bOk=true;
		}
	else
		{
		dsFrom=strsplit(g_sFrom, UN_EVENT_LIST_FILTER_COMBO_DELIMITER);
		bOk=false;
		
		iLen=dynlen(dsFrom);
		for (i=1; i<=iLen; i++)
			{
			iRes=dynContains(g_dsSubApplications, dsFrom[i]);
			if(iRes>0)
				{						
				if(dynContains(g_ddsSubApplications[iRes], sSystem)>0)
					{
					if (sFrom=="")
						sFrom="*-" + dsFrom[i] + "-*." + g_sDPEPattern;	
					else
						sFrom=sFrom + "," + "*-" + dsFrom[i] + "-*." + g_sDPEPattern;	
					
					bOk=true;
					}
				}
			}			
		}

	if (!bOk)
		sFrom="";

	return sFrom;
}

//-----------------------------------------------------------------------------------------------------------------------------

void unLists_displayFilterList(string sCombobox, string sTitle, string sDelimiter, string sAdd)
{
dyn_string dsSelected, dsList, dsParameters;
dyn_float dfReturn;
string sList;
int i, iLen;
shape comboShape, comboShape2;
	
	if (shapeExists(sCombobox) && shapeExists(sCombobox + "2"))
		{
		comboShape = getShape(sCombobox);
		comboShape2 = getShape(sCombobox + "2");
		if (comboShape.text() == sAdd)
			{
			dsList = comboShape.items();

			iLen = dynlen(dsList);
			for(i=1; i<=iLen; i++)					// Don't take "*" and "List..." fields
				{
				if ((dsList[i]!=sAdd) && (dsList[i]!=g_filterAll))
					sList = sList + sDelimiter + dsList[i];
				}
			
			sList += sDelimiter;	
			sList = substr(sList, 1);
			dsParameters = makeDynString("$sList:" + sList, "$sSelect:" + comboShape2.text);
			unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/unEventList/unEventList_SelectList.pnl","Select " + sTitle + " Names", dsParameters, dfReturn, dsSelected);

			iLen = dynlen(dsSelected);
			if (iLen > 1)
				{
				sList = "";
				for(i=1;i<=iLen;i++)
					sList = sList + sDelimiter + dsSelected[i];

				sList += sDelimiter;
				sList = substr(sList, strlen(sDelimiter));
				comboShape2.text = sList;
				}
			else if (iLen==1)
				{
				comboShape.text = dsSelected[iLen];
				comboShape2.text = "-1";
				}
			else
				{
				comboShape.selectedPos(1);
				comboShape2.text = "-1";
				}
			}
		else
			comboShape2.text = "-1";
		}
}

//-----------------------------------------------------------------------------------------------------------------------------
/** unLists_checkRequest
  *
  * @reviewed 2018-06-25 @whitelisted{FalsePositive}
  */
bool unLists_checkRequest(time &tBegin, time &tEnd, bool &bEstimate)
{
bool bForce, bNeed=FALSE;
string sFrom;
long qEnd;

	// Check From Statement and Distributed Status
	if (g_sFrom!=g_sOldFrom || listOfSystem.text!=g_sOldSystemList)
		{
		dynClear(g_tableHistory);
		dynClear(g_tableDisplay);		
		bNeed=TRUE;
		}
	// Check Time Request	
	else
		{
		bForce=!btForceTime.state(0);
		if (bForce)
			{
			dynClear(g_tableHistory);
			dynClear(g_tableDisplay);			
			}
		else
			{
			if ((tBegin<g_OldrequestBegin) || (tEnd>g_OldrequestEnd))
				{
				unLists_getInternalTimeRequest(tBegin, tEnd, bEstimate);
				bNeed=TRUE;
				}
			else
				bEstimate=TRUE;
			}	
		}

	if (bNeed || bForce)
		{
		qEnd=(long)tEnd;
		qEnd=qEnd+1;	//Add one second in order to be sure to get all data in the range
		tEnd=(time)qEnd;
		}
			
	return bNeed;	
}

//-----------------------------------------------------------------------------------------------------------------------------

void unLists_getInternalTimeRequest(time &tInternalBegin, time &tInternalEnd, bool &bEstimate)
{

	if (tInternalBegin>=g_OldrequestEnd)
		{
		//DebugN("getInternalTimeRequest() - keep new Time range");
		bEstimate=FALSE;
		dynClear(g_tableHistory);
		}
	else if (tInternalEnd<=g_OldrequestBegin)
		{
		//DebugN("getInternalTimeRequest() - keep new Time range");
		bEstimate=FALSE;
		dynClear(g_tableHistory);
		}		
	else if ((tInternalBegin<g_OldrequestBegin) && (tInternalEnd<=g_OldrequestEnd))		
		{
		tInternalEnd=g_OldrequestBegin;	
		bEstimate=TRUE;
		//DebugN("getInternalTimeRequest() - update Time range to get before", tInternalBegin, tInternalEnd);
		}
	else if ((tInternalBegin>=g_OldrequestBegin) && (tInternalEnd>g_OldrequestEnd) && (g_OldrequestBegin!=0))		
		{
		tInternalBegin=g_OldrequestEnd;
		bEstimate=TRUE;		
		//DebugN("getInternalTimeRequest() - update Time range to get after", tInternalBegin, tInternalEnd);		
		}
	else if ((tInternalBegin<g_OldrequestBegin) && (tInternalEnd>g_OldrequestEnd))
		{
		//DebugN("getInternalTimeRequest() - keep new Time range");
		bEstimate=FALSE;
		dynClear(g_tableHistory);
		}		
	else
		{
		//DebugN("getInternalTimeRequest()", tInternalBegin, g_OldrequestBegin, tInternalEnd, g_OldrequestEnd);
		bEstimate=FALSE;
		dynClear(g_tableHistory);
		}
}

//-----------------------------------------------------------------------------------------------------------------------------

void unLists_saveOldGlobal(time tBegin, time tEnd, bool bBuffer)
{
	g_sOldFrom=g_sFrom;
	g_sOldSystemList=listOfSystem.text;

	if (!bBuffer)
		{
		g_OldrequestBegin=tBegin;
		g_OldrequestEnd=tEnd;	
		}
	else
		{
		//save new position only if BeginTime is smaller
		if ((g_OldrequestBegin>tBegin) || (g_OldrequestBegin==0))
			g_OldrequestBegin=tBegin;
		
		//save new position only if EndTime is bigger
	  if ((g_OldrequestEnd<tEnd) || (g_OldrequestEnd==0))
			g_OldrequestEnd=tEnd;	
		}
		
	//DebugN("saveOldGlobal() ", tBegin, g_OldrequestBegin, " - ", tEnd, g_OldrequestEnd);
}

//-----------------------------------------------------------------------------------------------------------------------------

void unLists_getDisplayBuffer(const time tBegin, const time tEnd, const bool bQuery, const bool bEstimBuffer)
{
dyn_string dsBuffer;
long qBegin, qEnd, qBuffer;
int i, iEvents;
bool bOut;

	dynClear(g_tableDisplay);			
		
	if (!bEstimBuffer && bQuery)						//new query
		g_tableDisplay=g_tableHistory;
	else																		//estimate result to display only needs
		{		
		iEvents=dynlen(g_tableHistory);		

		qBegin=(long)tBegin;
		qEnd=(long)tEnd;
						
		bOut=FALSE;
		for (i=1; (i<=iEvents) && !bOut; i++)
			{
			dsBuffer=strsplit(g_tableHistory[i], UN_EVENT_LIST_DELIMITER);
			qBuffer=(long)dsBuffer[2];					
			if (qBegin<=qBuffer)
				{
				if (qEnd>=qBuffer)
					dynAppend(g_tableDisplay, g_tableHistory[i]);
				else
					bOut=TRUE;
				}
			}
		}

	dynUnique(g_tableDisplay);
	//DebugN("Out re-estimate Buffer between: ", dynlen(g_tableDisplay));
}

//-----------------------------------------------------------------------------------------------------------------------------

void unLists_SelectDeselect_line(string sList, bool bSelected, int iRow)
{
int i, iCol;
string sBackColor, sForeColor, sName;

	if (bSelected)
		{
		sBackColor="white";
		sForeColor="black";
		if (g_iSelected_Line>=0)	//a line is already selected
			unLists_SelectDeselect_line(sList, FALSE, g_iSelected_Line);
		
		getValue(sList,"columnName", 1, sName);
		getValue(sList,"cellForeColRC", iRow, sName, g_sColor_Line);
		}
	else
		{
		sBackColor="unEventList_BackColor";
		sForeColor=g_sColor_Line;			
		}
					
	getValue(sList,"columnCount", iCol);
	for (i=0; i<iCol; i++)
		{
		getValue(sList,"columnName", i, sName);
		setMultiValue(sList,"cellBackColRC", iRow, sName, sBackColor,
									sList,"cellForeColRC", iRow, sName, sForeColor);
		}

	if (bSelected)
		g_iSelected_Line=iRow;
	else
		g_iSelected_Line=-1;
}

//@}

