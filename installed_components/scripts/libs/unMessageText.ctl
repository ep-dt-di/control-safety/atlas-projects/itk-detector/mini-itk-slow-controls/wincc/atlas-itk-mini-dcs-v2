/**@name LIBRARY: unMessageText.ctl

@authors: Vincent Forest , Herve Milcent (LHC-IAS)

Creation Date: 23/07/2002
Modification History: None
		02/07/2004: Herve 
			- in function unMessageText_getDpMessage: get the local unMessageText DP instead of all of them.
			set UN_MESSAGE_TEXT_HISTORY_PERIOD to 1800 sec (30min)
			- in function unMessageText_send: always sent to the local unMessageText DP.

			
version 1.0

External Functions: 
	- unMessageText_createDpMessage: create the DP _unMessageText in the DP type _UnMessageText
	- unMessageText_send: send messages throw the distributed system
	- unMessageText_sendException: send exceptionInfo throw the distributed system
	- unMessageText_getDpMessage: get the DP name that contains the message to display

Purpose: 
This library contain generic functions to be use with the graphical frame component 
UNICOS\PVSS\panels\objects\UN_INFOS\unMessageText.pnl

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
		. a DP type _UnMessageText must be existing
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/
#uses "libCTRL.ctl"


// Global constants for Message Text component
const string UN_MESSAGE_TEXT_DELIMITER = "\t";			// Message text delimiter
const string UN_MESSAGE_TEXT_TYPE_DELIMITER = "+";		// Message text type delimiter
const string UN_MESSAGE_TEXT_HISTORY_DELIMITER = "~"; 	// Delimiter for message history
const string UN_MESSAGE_TEXT_LOGIN_DELIMITER = "="; 	// Delimiter for login field
const unsigned UN_MESSAGE_TEXT_FIELD_NUMBER = 6;		// Filter numbers
const unsigned UN_MESSAGE_TEXT_FILTER_RECEIVER_SYSTEM_NAME = 1;	// Message text receiver system name filter position in the filter configuration
const unsigned UN_MESSAGE_TEXT_FILTER_RECEIVER_UI_NUMBER = 2;	// Message text receiver ui number filter position in the filter configuration
const unsigned UN_MESSAGE_TEXT_FILTER_SENDER = 3;		// Message text sender filter position in the filter configuration
const unsigned UN_MESSAGE_TEXT_FILTER_TYPE = 4;			// Message text type filter position in the filter configuration
const unsigned UN_MESSAGE_TEXT_FILTER_TEXT = 5;			// Message text text filter position in the filter configuration
const unsigned UN_MESSAGE_TEXT_FIELD_LOGIN = 6;			// Message text login field
const unsigned UN_MESSAGE_TEXT_FILTER_ALIAS = 6;			// Message text alias filter positon in the filter configuration
//don't be confused with 2 constans above - login is only a message field while alias is only a filter field
const unsigned UN_MESSAGE_TEXT_HISTORY_PERIOD = 1800;	// In seconds, the default display period


global bool gIsInitializationNeeded = true; // Global flag that indicates if initialization action should be performed. It is true when manager starts.
                                            // After an attempt to send the first message, the initialization is performed and this is set to false.

/** Returns the state of global flag, that indicates whether initialization is needed.
  */
private bool unMessageText_isInitializationNeeded(){
  return gIsInitializationNeeded;
}

/** Executes initialization actions, then resets initialization flag.
  */
private void unMessageText_doInitialization()
{
  if(useRDBArchive()){
    unMessageText_configureDpMessageArchiveClass();
  }
  gIsInitializationNeeded = false;
}

/** Checks the unMessageText dpe archive class and in case of the default Valarch, replaces it with RDB archive class.
  */
private void unMessageText_configureDpMessageArchiveClass()
{
  const string RDB_ClassDp = getSystemName() + "_EVENT";
  const string Valarch_DefaultClassDp = getSystemName() + "_ValueArchive_0";
  
  dyn_string exceptionInfo;
  string dpMessage = unMessageText_getDpMessage(exceptionInfo);
  if(dpMessage == ""){
    return;
  }
  string archiveClass;
  string dpMessageArchiveClassConfig = dpMessage + ":_archive.1._class";
  dpGet(dpMessageArchiveClassConfig, archiveClass);
  if(archiveClass == Valarch_DefaultClassDp){
    dpSet(dpMessageArchiveClassConfig, RDB_ClassDp);
    DebugTN("unMessageText: Changing default Valarch archive class to RDB archive class for internal dp");
  }
}

// ------------------------------------------------------------------------------------------------------------------------------------
//@{

// unMessageText_createDpMessage
/**
Purpose:	create the DP _unMessageText in the DP type _UnMessageText

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameter:	dyn_string& exceptionInfo, used in the error handling process (fwException_raise and fwExceptionHandling_display functions)

Constraints:
		. a DP type _UnMessageText must be existing
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/
void unMessageText_createDpMessage(dyn_string& exceptionInfo)
{
	dyn_string exceptionInfoTemp;
	
	// Check if _unMessageText exists and creates it if necessary
	if (unMessageText_getDpMessage(exceptionInfoTemp) == "") 
		if (dpCreate("_unMessageText","_UnMessageText") == -1)	// Create DP
			fwException_raise(exceptionInfo,"ERROR","Library unMessageText.ctl : can't create DP _unMessageText","");
}

// ------------------------------------------------------------------------------------------------------------------------------------

// unMessageText_send
/**
Purpose:	send messages throw the distributed system

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameters:	string messageToSystemName, receiver system name, * = every system name
			string messageToUiNumber, receiver ui number, * = every ui number			
			string messageSender, sender identifier
			string userOrGroup, for access control, equal to "user" or "group"
			string userOrGroupName, for access control, the user or group name ex: "root"
			string messageType, message type : ERROR, WARNING, INFO or EXPERTINFO
			string messageText, the message itself
			
			dyn_string& exceptionInfo, used in the error handling process (fwException_raise and fwExceptionHandling_display functions)

Constraints:
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/

void unMessageText_send(string messageToSystemName, string messageToUiNumber, string messageSender, 
                        string userOrGroup, string userOrGroupName,
                        string messageType, string messageText, dyn_string& exceptionInfo)
{
  if(unMessageText_isInitializationNeeded()){
    unMessageText_doInitialization();
  }

  string dpMessage; // Global DP that contains messages
  bool sendError = false;
  
  if((messageToSystemName == "") || (messageToUiNumber == "") || (messageSender == "") || (messageText == "") || (userOrGroupName == ""))
  {
    fwException_raise(exceptionInfo, "ERROR", "unMessageText Component : bad message format.","");
    sendError = true;
  }
  if((messageType != "INFO") && (messageType != "EXPERTINFO") && (messageType != "ERROR") && (messageType != "WARNING"))
  {
    fwException_raise(exceptionInfo, "ERROR", "unMessageText Component : bad message type.","");
    sendError = true;
  }
  if((userOrGroup != "user") && (userOrGroup != "group"))
  {
    fwException_raise(exceptionInfo, "ERROR", "unMessageText Component : bad message type.","");
    sendError = true;
  }

  dyn_string systems = unMessageText_getSystemsOfAlias(unMessageText_extractDeviceAliasFromMessage(messageText), exceptionInfo);
  
  if(sendError == false)
  {
    if(dynlen(systems) <= 0)
    {
      systems = makeDynString(getSystemName());
    }
    if(dynlen(systems) > 1)//more than one system found, add info about other system to the message
    {
      //fwException_raise(exceptionInfo, "WARNING", "Message: " + messageText + "was sent to systems:" + dynStringToString(systems, ","),"");
      messageText += "; Message was sent to more than one system: " + dynStringToString(systems, ",");
    }
    for(int i=1;i<=dynlen(systems);i++)
    {
      if(messageToSystemName == UN_MESSAGE_TEXT_MULTIPLE_SYSTEM_SPECIFIER)
      {
        messageToSystemName = systems[i];
      }
      
      dpMessage = unMessageText_getDpMessage(exceptionInfo, systems[i]); // Get DP name
      if(dpMessage != "") // Send message
      {
        dpMessage = dpMessage + ":_original.._value";
        if(dpSet(dpMessage, messageToSystemName + UN_MESSAGE_TEXT_DELIMITER + messageToUiNumber + UN_MESSAGE_TEXT_DELIMITER +
                 messageSender + UN_MESSAGE_TEXT_DELIMITER +
                 messageType + UN_MESSAGE_TEXT_DELIMITER + messageText + UN_MESSAGE_TEXT_DELIMITER +
                 userOrGroup + UN_MESSAGE_TEXT_LOGIN_DELIMITER + userOrGroupName) == -1)
        {
          fwException_raise(exceptionInfo, "ERROR", "Library unMessageText.ctl : dpSet failed. Message wasn't sent","");
          sendError = true;
        }
      }
      else
      {
        fwException_raise(exceptionInfo, "ERROR", "Library unMessageText.ctl : no unMessageText DP","");
        sendError = true;
      }
    }
  }

  if(sendError == true)
  {
    fwException_raise(exceptionInfo, "ERROR", 
                      "unMessageText_send : error trying to send message=" + messageText + ", type=" + messageType +
                      ", toSystemName=" + messageToSystemName + ", toUiNumber=" + messageToUiNumber +
                      ", to " + userOrGroup + "=" + userOrGroupName +
                      ", from sender=" + messageSender,"");
  }
}
// ------------------------------------------------------------------------------------------------------------------------------------

// unMessageText_getDpMessage
/**
Purpose:	get the DP name that contains the message to display

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameters:	dyn_string& exceptionInfo, used in the error handling process (fwException_raise and fwExceptionHandling_display functions)

Constraints:
		. Only one DP named "*:_unMessageText.message:" must be existing in the distributed system
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/
string unMessageText_getDpMessage(dyn_string& exceptionInfo, string systemName = "")
{
	dyn_string dpMessageTextNamesArray;			// Array containing each DP named [*]:_unMessageText.message in the DPT _UnMessageText
	int namesArrayLength;						// dpMessageTextNamesArray length
	
	// Get the DP name that contains the message text
	// Its name format is [*]:_unMessageText.message
	// Steps : get each DPE from this format, check for errors
	if(systemName == "")
	{
		systemName = getSystemName();
	}
	dpMessageTextNamesArray = dpNames(systemName + "_unMessageText.message","_UnMessageText");
	namesArrayLength = dynlen(dpMessageTextNamesArray);
	if (namesArrayLength <= 0)
	{
		fwException_raise(exceptionInfo, "ERROR", "Library unMessageText.ctl : the data point [*]:_unMessageText.message is not existing on system: " + systemName,"");
		return("");
	}
		return(dpMessageTextNamesArray[1]);
}

// ------------------------------------------------------------------------------------------------------------------------------------

// unMessageText_sendException
/**
Purpose:	send exceptionInfo throw the distributed system

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameters:	string messageToSystemName, receiver system name, * = every system name
			string messageToUiNumber, receiver ui number, * = every ui number			
			string messageSender, sender identifier
			string userOrGroup, for access control, equal to "user" or "group"
			string userOrGroupName, for access control, the user or group name ex: "root"
			dyn_string &exceptionInfo, contains messageType and messageText

Constraints:
		. Only one DP named "*:_unMessageText.message" must be existing in the distributed system
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/
void unMessageText_sendException(string messageToSystemName, string messageToUiNumber, string messageSender, 
						string userOrGroup, string userOrGroupName,	dyn_string &exceptionInfo)
{
	int numberOfMessages;
	int i;
	dyn_string errorArray;
	
	numberOfMessages = dynlen(exceptionInfo);
	for(i=1; i<=(numberOfMessages/3); i++)
	{
		unMessageText_send(messageToSystemName, messageToUiNumber, messageSender, userOrGroup, userOrGroupName,
					 		   exceptionInfo[3*i-2], exceptionInfo[3*i-1], errorArray);
	}
	exceptionInfo = makeDynString();
	if (dynlen(errorArray) > 0)
		dynAppend(exceptionInfo, errorArray);
}
//@}



const string UN_MESSAGE_TEXT_DEVICE_PATTERN = "Device=";
const string UN_MESSAGE_TEXT_MULTIPLE_SYSTEM_SPECIFIER = "%";
const string UN_MESSAGE_TEXT_ALIAS_END_CHARACTERS = ",;:?()! ";

/* UN_MESSAGE_TEXT_ALIAS_END_CHARACTERS contains characters that indicates end of device alias.
 * This will be used as an argument of strtok() function.
 * Note: Dot (.) might be a part of device alias, thus it cannot be used in strtok() function.
 * If there is a dot at the end of the device alias, it will be removed after substraction of the device alias.
 * 
 * Assumptions:
 *  - Before device alias there must be a keyword "Device=", otherwise it will not be treated as a device alias;
 *  - After device alias there must one of the character listed in UN_MESSAGE_TEXT_ALIAS_END_CHARACTERS;
 *  - Device alias cannot contain characters listed in UN_MESSAGE_TEXT_ALIAS_END_CHARACTERS;
 *  - Device alias cannot have dot (.) as a last character;
 *  - If there is a dot (.) after device alias as a part of message text (not device alias - see previous point)
 *    it must be followed by a character listed in UN_MESSAGE_TEXT_ALIAS_END_CHARACTERS (for example space " ");
 */

/* This function parses an returns device alias from given message text. Device is specified after
  * keyword "Device=". If this string is not present in message then empty string is returned.
  * Pattern:
  *   Device=deviceAlias
  * Example of valid messages:
  *   "Selected Device=SF_2_VMA2314"
  *   "Device=SF_2_VMA2314 alarm deleted"
  *
  * @param messageText (string) IN   Text of the message
  *
  * @return device alias if keyword "Device=" is present in the message, otherwise empty string
  */

string unMessageText_extractDeviceAliasFromMessage(string messageText)
{
	int devicePos = strpos(messageText, UN_MESSAGE_TEXT_DEVICE_PATTERN); 
	if(devicePos < 0)//in messageText there is no device specified
	{
		return "";
	}
	devicePos += strlen(UN_MESSAGE_TEXT_DEVICE_PATTERN);
	string data = substr(messageText, devicePos);//remove the beginning of the message
	int endPos = strtok(data, UN_MESSAGE_TEXT_ALIAS_END_CHARACTERS);//look for characters that indicates ending of device alias

	if(endPos == -1)//alias is at the end of the message
	{
		endPos = strlen(data);
	}
	return strrtrim(substr(data, 0, endPos), ".");//substract device alias and remove a dot from the end (if there is any).
}

/* This function returns the list of systems in which the device with given alias exists.
 *
 * @param deviceAlias (string) IN	 device alias
 * 
 * @return list of system with given device, if device doesn't exist - empty array
 */
dyn_string unMessageText_getSystemsOfAlias(string deviceAlias, dyn_string &exceptionInfo)
{
	if(deviceAlias == "")
	{
		return makeDynString();
	}
	//1. device exists in local system
	if(dpExists(dpAliasToName(deviceAlias)))
	{
		return makeDynString(getSystemName());
	}

	//2. look for device in remote systems
	dyn_string deviceNames = dpNames("*:@" + deviceAlias);//looks in all systems for given alias
	if(dynlen(deviceNames) <= 0)//device not found
	{
		fwException_raise(exceptionInfo, "ERROR", "Device: " + deviceAlias + " doesn't exist in any connected system", "");
		return makeDynString();
	}
	dyn_string systemNames;
	for(int i=1;i<=dynlen(deviceNames);i++)
	{
		string sysName = dpSubStr(deviceNames[i], DPSUB_SYS);//take the system from device dp
		if(sysName != "")
		{
			systemNames[i] = sysName;
		}
	}
	dynUnique(systemNames);//ensure that systems in the array are unique
	return systemNames;
}
