
/**@name LIBRARY: unMessageText_HMI.ctl

@authors: Vincent Forest , Herve Milcent (LHC-IAS)

Creation Date: 23/07/2002
Modification History: 
  11/02/2008: Herve
    - JCOP Access control
    
		12/05/2006: Herve
			optmization: unMessageText_HMI_getField new case "GLOBAL-FILTER" use g_sFilter (must be in the gloabal declaration of the panel
			
		23/11/2004: Herve
			unMessageText_HMI_createDpConfig: create the Dp with * as system name and manager number
			
		21/07/2004: Herve
			. use patternMatch instead of strpos in unMessageText_HMI_allowDisplay to allow the * with other letters.
			. save filter not based on uim number: use default name instead.
					functions modified: unMessageText_HMI_createDpConfig, unMessageText_HMI_setFilter, unMessageText_HMI_getField, unMessageText_HMI_allowDisplay

		05/07/2004: Herve 
			in function unMessageText_HMI_allowDisplay: if access control is installed, check the user/group 
			characteristic. If not then do not check it.

		12/07/2003: Herve
			in the function unMessageText_HMI_allowDisplay to cover the following limitations during the allow display of message with group: 
				- it is not possible to send a message to users in multiple group.
				- if a user is in two group, the message for the second group is not shown

version 1.0

External Functions: 
	- unMessageText_HMI_createDpConfig : create the DP _unMessageText_HMI_[uiNumber] in the DP type _UnMessageText_HMI
    - unMessageText_HMI_getField : return the field specified in arguments
	- unMessageText_HMI_setFilter : set the filter with specified arguments
	- unMessageText_HMI_allowDisplay : check if specified message could be displayed in the message text component using the filter
	
Purpose: 
This library contain generic functions to be use with the graphical frame component 
UNICOS\PVSS\panels\objects\UN_INFOS\unMessageText.pnl

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
		. this library has to used with the main library unMessageText.ctl (for the message text component)
		. a DP type _UnMessageText_HMI must be existing
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/

const string UN_MESSAGETEXT_HMI_defaultConfigName = "_unMessageText_HMI_unicos";

// ------------------------------------------------------------------------------------------------------------------------------------
//@{

// unMessageText_HMI_createDpConfig
/**
Purpose:	create the DP _unMessageText_HMI_[uiNumber] in the DP type _UnMessageText_HMI

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameter:	unsigned uiNumber, DP _unMessageText_HMI_[uiNumber] contains message text configuration for HMI number uiNumber 

			dyn_string& exceptionInfo, used in the error handling process (fwException_raise and fwExceptionHandling_display functions)

Constraints:
		. a DP type _UnMessageText_HMI must be existing
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/

void unMessageText_HMI_createDpConfig(string sFilterName, dyn_string& exceptionInfo)
{
	string dpConfig;					// dpConfig = "_unMessageText_HMI_[sFilterName]" and contains message text configuration for HMI number uiNumber 
	
	dpConfig = sFilterName;
	// 1. Check if _unMessageText_HMI_[sFilterName] exists and creates it if necessary
	if (dpExists(dpConfig) == false)
		if (dpCreate(dpConfig,"_UnMessageText_HMI") == -1)
			fwException_raise(exceptionInfo,"ERROR","Library unMessageText_HMI.ctl : can't create DP " + dpConfig,"");
		else
	// 2. Initialize DP dpConfig
	// Default filter is receiver system name= mySystemName, receiver ui number=MyManNum, sender="*", message type="INFO", message text="*"
			{
			if (dpSet(dpConfig + ".numberOfMessages", 100) == -1)
				fwException_raise(exceptionInfo,"ERROR","Library unMessageText_HMI.ctl : can't initialize DP " + dpConfig + ".numberOfMessages","");
			unMessageText_HMI_setFilter(sFilterName, "*", "*", "*", "INFO", "*", exceptionInfo);
			}
}

// ------------------------------------------------------------------------------------------------------------------------------------

// unMessageText_HMI_getField
/**
Purpose:	return the field specified in arguments

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameter:	string& result, function result
			string message, a delimited string where to find the required field
			string fieldRequired, part of the message to return (see CONSTANTS in the unMessageText.ctl library)

			dyn_string& exceptionInfo, used in the error handling process (fwException_raise and fwExceptionHandling_display functions)

Constraints:
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/

void unMessageText_HMI_getField(string sFilterName, string& result, string instruction, unsigned fieldRequired, dyn_string& exceptionInfo)
{
	string dpName;						// DP name that contains the string which field is required
	string message;						// String which field is required
	dyn_string splitMessageFilter;		// Contains each filter part

	// 1. Follow instruction, if necessary get the dpName and extract the message
	switch(instruction)
		{
		case "FILTER": dpName = getSystemName() + sFilterName + ".filter";
			break;
		case "MESSAGE": dpName = unMessageText_getDpMessage(exceptionInfo);
			break;
		case "GLOBAL-FILTER": message = g_sFilter; dpName = " ";
//DebugN("unMessageText_HMI_getField, GLOBAL-FILTER", g_sFilter, message, dpName);
			break;
		default:
			break;
		}
//DebugN("unMessageText_HMI_getField", instruction, message, dpName);		
	if (dpName == "")
		message = instruction;
	else
		{
		if(dpExists(dpName)) {
			if (dpGet(dpName,message) == -1)
				fwException_raise(exceptionInfo,"ERROR","Library unMessageText_HMI.ctl : dpGet failed for DP " + dpName,"");
			if (dynlen(exceptionInfo) > 0)
				{
				result = "";
				return;
				}
			}
		}
/*
if(instruction == "GLOBAL-FILTER") {
	if(g_sFilter != message)
		DebugN("********* unMessageText_HMI_getField", g_sFilter, message);
	else
		DebugN("unMessageText_HMI_getField same filter", g_sFilter, message);
}
*/
	// 2. Split message
	splitMessageFilter = strsplit(message,UN_MESSAGE_TEXT_DELIMITER);

//DebugN(fieldRequired, splitMessageFilter);	
	// 3. Return the required field
	if ((fieldRequired > UN_MESSAGE_TEXT_FIELD_NUMBER) || (fieldRequired == 0) || (fieldRequired > dynlen(splitMessageFilter)))
		{
		fwException_raise(exceptionInfo,"ERROR","Library unMessageText_HMI.ctl : bad required field " + fieldRequired,"");
		result = "";
		return;
		}
	else
		result = splitMessageFilter[fieldRequired];
}

// ------------------------------------------------------------------------------------------------------------------------------------

// unMessageText_HMI_setFilter
/**
Purpose:	set the filter with specified arguments

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameter:	string receiverSystemName
		    string receiverUiNumber
			string sender, sender identifier
			string type, message text type, must be EXPERTINFO INFO ERROR WARNING or a combination
			string text

			dyn_string& exceptionInfo, used in the error handling process (fwException_raise and fwExceptionHandling_display functions)

Constraints:
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/

void unMessageText_HMI_setFilter(string sFilterName, string receiverSystemName, string receiverUiNumber,
								 string sender, string type, string text,
								 dyn_string& exceptionInfo)
{
	string dpConfigFilter;					// dpConfig = "_unMessageText_HMI_[uiNumber].filter" and contains message text configuration filter for HMI number uiNumber 
	string messageFilter;					// Filter made with the arguments and set to the filter DP
	string oldFilter;						// Old filter value
	
	
	dpConfigFilter = getSystemName() + sFilterName + ".filter";
	// 1. Build messageFilter
	messageFilter = receiverSystemName + UN_MESSAGE_TEXT_DELIMITER;	// Receiver system name
	messageFilter = messageFilter + receiverUiNumber + UN_MESSAGE_TEXT_DELIMITER; // Receiver Ui number
	messageFilter = messageFilter + sender + UN_MESSAGE_TEXT_DELIMITER;	// Sender
	messageFilter = messageFilter + type + UN_MESSAGE_TEXT_DELIMITER;	// Type
	messageFilter = messageFilter + text + UN_MESSAGE_TEXT_DELIMITER;	// Text
	messageFilter = messageFilter + "*"; // No filter on device alias
	
	// 2. Set filter with messageFilter if a value change is detected
	if (dpGet(dpConfigFilter, oldFilter) != -1)
		if (oldFilter == messageFilter)
			return;
	
	if (dpSet(dpConfigFilter, messageFilter) == -1)
		fwException_raise(exceptionInfo,"ERROR","Library unMessageText_HMI.ctl : dpSet failed for DP " + dpConfigFilter,"");
}

// ------------------------------------------------------------------------------------------------------------------------------------

// unMessageText_HMI_allowDisplay
/**
Purpose:	check if specified message could be displayed in the message text component using the filter

Usage: 		Public

PVSS manager usage: UI (WCCOAui)

Parameter:	bool& allow, function result
			string messageToBeDisplayed, the message to display
			string instruction, parameter for function unMessageText_HMI_getField
			dyn_string& exceptionInfo, used in the error handling process (fwException_raise and fwExceptionHandling_display functions)

Constraints:
		. PVSS version: 3.0 
		. operating system: NT and W2000, but tested only under W2000.
		. distributed system: yes.
*/

void unMessageText_HMI_allowDisplay(string sFilterName, bool& allow,string instruction, string messageToBeDisplayed, dyn_string& exceptionInfo)
{
	string currentField;				// Current studied field 
	string currentFilter;				// Current studied filter
	dyn_string filterTypeNames;			// Array with each allowed filter type
	unsigned allowedFilterTypeNumber;	// Length of filterTypeNames
	bool filterTypeOk;					// Message could be displayed from a filter type point of view
	int i,j;
	dyn_string splitLogin;
	string userOrGroup;
	string userName, groupName;			// User / Group for control access
	int userId;				// user / Group for control access
	dyn_int groupId;
	dyn_string dsGroup;

//DebugN("unMessageText_HMI_allowDisplay");
	
	for(i=1;i<=UN_MESSAGE_TEXT_FIELD_NUMBER;i++)	// For each field
	{
		if(i<UN_MESSAGE_TEXT_FIELD_NUMBER)//don't get last field from message - we only want to read alias filter (6th field in filter)
		{                                 //and compare it with device alias extracted from message text (5th field in message)
			unMessageText_HMI_getField(sFilterName, currentField,messageToBeDisplayed,i,exceptionInfo);	// Get field string
		}
		unMessageText_HMI_getField(sFilterName, currentFilter,instruction,i,exceptionInfo);			// Get field filter
		switch (i)
			{
			case UN_MESSAGE_TEXT_FILTER_RECEIVER_SYSTEM_NAME:
			case UN_MESSAGE_TEXT_FILTER_RECEIVER_UI_NUMBER:
			case UN_MESSAGE_TEXT_FILTER_SENDER:
					if(!patternMatch(currentFilter, currentField))
					{
					allow = false;															// ... and filter, field are not set to "display all" e.g. "*"
					return;
					}
				break;
				
			case UN_MESSAGE_TEXT_FILTER_TYPE:
				filterTypeNames = strsplit(currentFilter,UN_MESSAGE_TEXT_TYPE_DELIMITER); 	// Get allowed types
				allowedFilterTypeNumber = dynlen(filterTypeNames);							// Get their number
				filterTypeOk = false;
				for(j=1;j<=allowedFilterTypeNumber;j++)
					{
					if (currentField == filterTypeNames[j])									// One allowed type matches the message type
						filterTypeOk = true;
					}
				if (filterTypeOk == false)													// Hide message if message type is not allowed
					{
					allow = false;
					return;
					}
				break;
				
			case UN_MESSAGE_TEXT_FILTER_TEXT:
					if(!patternMatch(currentFilter, currentField))
					{
					allow = false;															// ... and filter is not set to "display all" e.g. "*"
					return;
					}
				break;
			case UN_MESSAGE_TEXT_FILTER_ALIAS:
					if(currentFilter == "" || currentFilter == "*")//don't do anything, all devices allowed
					{
					break;
					}
					if(!patternMatch(currentFilter,
						 unMessageText_extractDeviceAliasFromMessage(currentField)))
					{
					allow = false;
					return;
					}
				break;
			default:
				break;
			}
		}
	unMessageText_HMI_getField(sFilterName, currentField,messageToBeDisplayed,UN_MESSAGE_TEXT_FIELD_LOGIN,exceptionInfo);	// Get login field
	splitLogin = strsplit(currentField,UN_MESSAGE_TEXT_LOGIN_DELIMITER);
	if (dynlen(splitLogin) == 2) {
		if ((splitLogin[1] == "user") || (splitLogin[1] == "group")) {
			if ((splitLogin[2] != "*") && (splitLogin[2] != "")) {
				if(isFunctionDefined("_unMessageText_HMI_getUserCharacteristics")) {
					_unMessageText_HMI_getUserCharacteristics(userName, userId, dsGroup, groupId, exceptionInfo);
					if (splitLogin[1] == "group") { // case group
						allow = false;
						for(i=1;(!allow) && (i<=dynlen(dsGroup));i++) {
							if (dsGroup[i] == splitLogin[2])
								allow = true;
						}
						if(!allow)
							return;
					}
					else { // case user
						userOrGroup = userName;				
						if (userOrGroup != splitLogin[2]){
							allow = false;
							return;
						}
					}
				}
			}
		}
	}
	allow = true;
}

/** Returns user information in UNICOS Access Control API.

@param userName: string, output, the user name
@param userId: int, output, the user id
@param dsGroupName: dyn_string, output, the user group name
@param diGroupId: dyn_int, output, the user group id
@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

@deprecated This is a legacy function that assures compatibility with UNICOS API\n
One should use @ref fwAccessControl_getUser instead

@ingroup UNICOSFunctions
@see fwAccessControl_getUser
*/
_unMessageText_HMI_getUserCharacteristics(string &userName, int &userId, dyn_string &dsGroupName, dyn_int &diGroupId, dyn_string exceptionInfo)
{
    dynClear(dsGroupName);
    dynClear(diGroupId);

    string userFullName, description;
    bool enabled;
    dyn_string groupNames;
    dyn_int groupIds;

    userName=""; // we will be asking about current user...

    fwAccessControl_getUser (userName, userFullName, description, userId, enabled, groupNames, exceptionInfo);

    _fwAccessControl_convert (GROUP_NAME_TO_IDX, groupNames, groupIds, exceptionInfo);
    if (dynlen(exceptionInfo)) return;

    dsGroupName = groupNames;
    diGroupId   = groupIds;
}

//@}
