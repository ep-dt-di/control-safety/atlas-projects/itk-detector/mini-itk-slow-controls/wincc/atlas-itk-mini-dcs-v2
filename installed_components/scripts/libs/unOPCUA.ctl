/**@name LIBRARY: unOPCUA.ctl

@author: Alexey Merezhin (EN/ICE)

Creation Date: 25/09/2015

Modification History:

version 1.0

Purpose: This library contains OPC UA device functions.
*/

/**
Purpose: Function called from the objectList to return the time and value

Parameters:
  - sDeviceName, string, input, device name
  - sDeviceType, string, input, unicos object
  - dsReturnData, dyn_string, output, return value
*/

OPCUA_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  SOFT_FE_ObjectListGetValueTime(sDeviceName, sDeviceType, dsReturnData);
}

/**
Purpose: configuration of popup menu

Parameters:
  - sDpName, string, input, device name
  - sDpType, string, input, unicos object
  - dsAccessOk, dyn_string, input, authorized action
  - menuList, dyn_string, output, menu to pop-up
*/
OPCUA_MenuConfiguration(string sDpName, string sDpType, dyn_string dsAccessOk, dyn_string &menuList)
{
  _UnPlc_MenuConfiguration(sDpName, sDpType, dsAccessOk, menuList);
}

/**
Purpose: handle the answer of the popup menu

Parameters:
  - deviceName, string, input, the device dp name
  - sDpType, string, input, the device dp type
  - menuList, dyn_string, input, list of requested action
  - menuAnswer, int, input, selected action from the menuList
*/
OPCUA_HandleMenu(string deviceName, string sDpType, dyn_string menuList, int menuAnswer)
{
  _UnPlc_HandleMenu(deviceName, sDpType, menuList, menuAnswer);
}

/**
Purpose: register callback function

Parameters:
*/
OPCUA_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
  SOFT_FE_WidgetRegisterCB(sDp, bSystemConnected);
}

/**
Purpose: Animate widget disconnection

Parameters:
  - sWidgetType, string, input, widget type. 
*/
OPCUA_WidgetDisconnection(string sWidgetType)
{
  SOFT_FE_WidgetDisconnection(sWidgetType);
}

/**
Purpose: faceplate register callback function

Parameters:
*/
OPCUA_FaceplateRegisterCB(string sDp, bool bSystemConnected)
{
  SOFT_FE_FaceplateRegisterCB(sDp, bSystemConnected);
}

/**
Purpose: Animate Faceplate disconnection

Parameters:
*/
OPCUA_FaceplateDisconnection()
{
  SOFT_FE_FaceplateDisconnection();
}
