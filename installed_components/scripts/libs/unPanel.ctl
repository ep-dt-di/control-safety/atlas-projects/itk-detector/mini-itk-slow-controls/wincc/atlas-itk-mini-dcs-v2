/**@file 
  
//LIBRARY: unPanel.ctl
This library contains function associated with the panel dp: _UnPanel.

Functions are provided for creating and deleting DPs of the type _UnPanel. The _UnPanel data point type represents a 
panel. It has a context: this is next, previous and 8 panels that can be configured as sibling panel. 

@par Creation Date
  30/10/2001

@par Modification History
  23/06/2010: Herve
    - IS-236: allow other filename extension, like xml, etc.
    function modified: unPanel_convertDpPanelNameToPanelFileName, unPanel_convertPanelFileNameToDp, unPanel_create
    
	07/11/2005: Frederic
		- remove unPanel_getLocal() and unPanel_getUTC() - Now in DLL => CtrlEventList_displayLocalTimefromString(), CtrlEventList_displayLocalTimefromTime(), CtrlEventList_displayUTCTime()

	20/10/2005: Frederic
		- add unPanel_getUTC() and unPanel_getLocal()
	
	18/02/2005: Herve
		- add save/get contextualPanel from _FwTreeNode or _UnPanel Dp
		
	22/07/2004: Herve, update to v3.0
		- remove c_nodeOperation device model used instead
		- remove the constant and put it in unicosObjects_declarations.ctl
		- remove functions unPanel_locateContainer, _unPanel_create, 
		unPanel_setPanelDollarParameterInContext and unPanel_addChildPanelInHierarchy
		- function unPanel_create:
			. signature modified
			. create the _UnPanel device, the dpName created is the based on folder and panel filename without the .pnl
		- new functions: unPanel_convertPanelFileNameToDp, unPanel_convertDpPanelNameToPanelFileName
		
	18/02/2004:
		modification of unPanel_getPanelList and unPanel_setPanelList to accept $param: 
		new function unPanel_setPanelDollarParameterInContext
		
	06-2002:
		add panel list get and set functions
		add comments

@par Constraints
	. constant:
		. c_unPanelConfiguration: panel name for configuration
		. c_unPanelConfiguration_maxPanelNavigation: maximum of panels for navigation, next and previous not included
	. the following panels must exist
		. vision/unPanel/unPanelHierarchyConfiguration
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
  
@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Herve Milcent (LHC-IAS)
*/

//@{

//------------------------------------------------------------------------------------------------------------------------
// unPanel_create
/** Creates a dp of type _UnPanel in the standard device container as well as the specified parent
data point. If the parent data point name is left blank, the device is only added in the default container if it 
exists.

@par Constraints
	. constant:
		. c_unPanelConfiguration: panel name for configuration
		. c_unPanelConfiguration_maxPanelNavigation: maximum of panels for navigation, next and previous not included
	. the following panels must exist
		. vision/unPanel/unPanelHierarchyConfiguration
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sPanelFileName: input, the panel filename
@param sPanelDpName: output, the panel data point name
@param exceptionInfo: output, Details of any exceptions are returned here
*/
unPanel_create(string sPanelFileName, string &sPanelDpName, dyn_string &exceptionInfo)
{
	dyn_string dsSplit;
	string sFileName = unPanel_convertPanelFileNameToDp(sPanelFileName);
	int i, len;
	bool bError = false;
	string sTempDpName;
  
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unPanel", "unPanel_create", sPanelFileName, sFileName);
	
	if(sFileName == "")
	{
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "PANELFILENAMEEMPTY"), "");
		return;
	}

// check if the sPanelFileName is correct:
	dsSplit = strsplit(sFileName, UN_PANEL_DPNAME_DELIMITER);
	len = dynlen(dsSplit);
// check if all folder in path and panel filename without the .pnl is correct
	for(i=1; (i<= len) && (!bError); i++) {
		if(!unConfigGenericFunctions_nameCheck(dsSplit[i])) {
			fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "WRONGNAME")+dsSplit[i], "");
			bError = true;
		}
	}
	if(!bError) {
	//if no problem, builds the dpname, creates it and checks it was created.
		sTempDpName = unPanel_convertPanelFileNameToDp(sPanelFileName);

		//create dp
		dpCreate(sTempDpName, UN_PANEL_DPTYPE);
		
		//check dp was created successfully
		if(!dpExists(sTempDpName))
		{
			fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "DPNOTCREATED")+sTempDpName, "");
			return;
		}
	}
	sPanelDpName = getSystemName()+sTempDpName;
}

//------------------------------------------------------------------------------------------------------------------------
// unPanel_convertPanelFileNameToDp
/** Convert the panel filename to a datapoint.

@par Constraints
	. constant:
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sFileName: input, the panel filename
@param return value: output, the panel data point name
*/
string unPanel_convertPanelFileNameToDp(string sPanelFileName)
{
// remove .pnl, .xml if any
  int iPos;
	string sTemp;
  
  iPos = strpos(sPanelFileName, ".pnl");
  if(iPos <= 0)
  {
    iPos = strpos(sPanelFileName, ".xml");
    if(iPos <= 0)
      iPos = strlen(sPanelFileName);
  }
  sTemp = substr(sPanelFileName, 0, iPos);
	
	strreplace(sTemp, UN_PANEL_FILENAME_PATH_DELIMITER, UN_PANEL_DPNAME_DELIMITER);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unPanel", "unPanel_convertPanelFileNameToDp", sPanelFileName, sTemp);
	return sTemp;
}

//------------------------------------------------------------------------------------------------------------------------
// unPanel_convertDpPanelNameToPanelFileName
/** Convert the datapoint name to panel name.

@par Constraints: 
	. constant:
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName: input, the data point name
@param return value: output, the panel file name
*/
string unPanel_convertDpPanelNameToPanelFileName(string sDpName)
{
// remove the system name
	string sTemp = dpSubStr(sDpName, DPSUB_DP);
  string sReturn;
	
	
	strreplace(sTemp, UN_PANEL_DPNAME_DELIMITER, UN_PANEL_FILENAME_PATH_DELIMITER);
  // check if the file exists with the following extention: .pnl, .xml, no extention
  if(getPath(PANELS_REL_PATH, sTemp+".pnl") != "")
    sReturn = sTemp+".pnl";
  else if(getPath(PANELS_REL_PATH, sTemp+".xml") != "")
    sReturn = sTemp+".xml";
  else if(getPath(PANELS_REL_PATH, sTemp) != "")
    sReturn = sTemp;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unPanel", "unPanel_convertDpPanelNameToPanelFileName", sDpName, sTemp, sReturn);
	return sReturn;
}

//------------------------------------------------------------------------------------------------------------------------
// unPanel_delete
/** Deletes a dp of type _UnPanel.

@par Constraints: 
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dpName: input, A name for the data point to be deleted, the system name must be included
@param exceptionInfo: output, Details of any exceptions are returned here
*/
unPanel_delete(string dpName, dyn_string &exceptionInfo)
{
	int length, i;
	dyn_string parentDps, childList;
	string sPanelDpName = dpSubStr(dpName, DPSUB_DP);
	
	//check dp name not empty
	if(sPanelDpName == "")
	{
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "DPEMPTY")+dpName, "");
		return;
	}

	// always delete locally
	//check that dp does exist
	if(!dpExists(sPanelDpName))
	{
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "DPEMPTY")+dpName, "");
		return;
	}

	// delete the _UnPanel
	dpDelete(sPanelDpName);

	//check dp was deleted successfully
	if(dpExists(sPanelDpName))
	{
		fwException_raise(exceptionInfo, "INFO", getCatStr("unPanel", "NOTDELETE") +" original DP: "+ dpName+
																							", "+sPanelDpName,"");
	}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unPanel", "unPanel_delete", dpName);
}

//------------------------------------------------------------------------------------------------------------------------
// unPanel_getPanelList
/** Get the list of panel context of a DP of type _UnPanel. The panel context is 10 other panels.

@par Constraints: 
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName: input, A name for the new data point
@param panelLabel: output, label of the panel
@param panelNameList: output, panels list to set: previous, next, panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8
@param panelDollarParam: output, $param of the panel, $parm1:ff;$param=rr;
@param exceptionInfo: output, Details of any exceptions are returned here
*/
unPanel_getPanelList(string sDpName, dyn_string &panelLabel, dyn_string &panelNameList, dyn_string &panelDollarParam, dyn_string &exceptionInfo)
{
	string panel;
	int i, len;
	dyn_string split, dsPanel;

//DebugN(sDpName);
	//check if dpName exists
	if(!dpExists(sDpName))
	{
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "GETNOTEXIST")+sDpName, "");
		return;
	}
	
	switch(dpTypeName(sDpName)) {
		case UN_PANEL_DPTYPE:
		case UN_TRENDTREE_PLOT:
		case UN_TRENDTREE_PAGE:
			for(i=1; i<=c_unPanelConfiguration_maxPanelNavigation; i++) {
				dpGet(sDpName+".navigation.panels["+i+"]", dsPanel[i]);
			}
			break;
		case unTreeWidget_treeNodeDPT:
			fwTree_getNodeUserData(_fwTree_getNodeName(sDpName), dsPanel, exceptionInfo);
			if(dynlen(dsPanel) >=1)
				dynRemove(dsPanel, 1);
			break;
	}
	len = dynlen(dsPanel);
	for(i=1; i<=c_unPanelConfiguration_maxPanelNavigation; i++) {
		if(i<=len)
			panel = dsPanel[i];
		else
			panel = "";
		split = strsplit(panel, c_panel_navigation_delimiter);
		switch(dynlen(split)) {
			case 2:
				split[3] = "";
			case 3:
				if(split[2] == "") {
					split[1] = "";
					split[3] = "";
				}
				break;
			default:
				split[1] = "";
				split[2] = "";
				split[3] = "";
				break;
		}
		dynAppend(panelLabel, split[1]);
		dynAppend(panelNameList, split[2]);
		dynAppend(panelDollarParam, split[3]);
	}
}

//------------------------------------------------------------------------------------------------------------------------
// unPanel_setPanelList
/** Set the list of panel context of a DP of type _UnPanel. The panel context is 10 other panels.

@par Constraints: 
	. PVSS version: 3.0 
	. operating system: WXP and Linux, but tested only under WXP and Linux.
	. distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName: input, A name for the new data point
@param panelLabel: input, label of the panel
@param panelNameList: input, panels list to set: previous, next, panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8
@param panelDollarParam: input, $param of the panel, $parm1:ff;$param=rr;
@param exceptionInfo: output, Details of any exceptions are returned here
*/
unPanel_setPanelList(string sDpName, dyn_string panelLabel, dyn_string panelNameList, dyn_string panelDollarParam, dyn_string &exceptionInfo)
{
	int i;
	dyn_string dsPanel, dsUserData;
	
	//check if dpName exists
	if(!dpExists(sDpName))
	{
		fwException_raise(exceptionInfo, "ERROR", getCatStr("unPanel", "SETNOTEXIST")+sDpName, "");
		return;
	}
	
	for(i=1; i<=c_unPanelConfiguration_maxPanelNavigation; i++) {
		dsPanel[i] = panelLabel[i]+c_panel_navigation_delimiter+panelNameList[i]+c_panel_navigation_delimiter+panelDollarParam[i];
	}

	switch(dpTypeName(sDpName)) {
		case UN_PANEL_DPTYPE:
		case UN_TRENDTREE_PLOT:
		case UN_TRENDTREE_PAGE:
			for(i=1; i<=c_unPanelConfiguration_maxPanelNavigation; i++) 
				dpSet(sDpName+".navigation.panels["+i+"]", dsPanel[i]);
			break;
		case unTreeWidget_treeNodeDPT:
			fwTree_getNodeUserData(_fwTree_getNodeName(sDpName), dsUserData, exceptionInfo);
			if(dynlen(dsUserData) < 1)
				dynInsertAt(dsPanel, "", 1);
			else {
				dynInsertAt(dsPanel, dsUserData[1], 1);
			}
			fwTree_setNodeUserData(_fwTree_getNodeName(sDpName), dsPanel, exceptionInfo);
			break;
	}
//DebugN(dpTypeName(sDpName), sDpName);
}

//------------------------------------------------------------------------------------------------------------------------

//@}

