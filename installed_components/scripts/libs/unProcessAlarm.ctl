/**@name LIBRARY: unGenericObject.ctl
 
@author: Celine VUILLERMOZ(AB-CO)

Creation Date: 17 06 2005

Modification History:
  04/10/2011: Herve
  - IS-629: remove access to unGetSMSConfiguration.pnl from systemIntegrity configuration

  08/2011: Herve
  - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity
  
  28/06/2011: Herve
  - IS-553: periodic report based on a user defined function

  11/05/2011: Herve
  -IS-529: Import: delete devices and front-end, remove the device and any associated DP from all the mail/SMS category and application
  
  11/04/2011: Herve
  - IS-518: SMS/Mail category managment functions
  unProcessAlarm_getCategory
  unProcessAlarm_createCategory
  unProcessAlarm_getCategoryPerSystem
  
  06/04/2011: Herve
  - IS-518: SMS/Mail category managment functions:
    unProcessAlarm_checkCategory
    unProcessAlarm_addUpdateCategory
    unProcessAlarm_deleteCategory
   
  25/11/2010: Herve
  - IS-462: remove the systemAlarm DPE from the mail/sms setting when the systemAlarm is deleted
  
  13/07/2009: Herve
    - unProcessAlarm_removeDpFromLis, unProcessAlarm_addRemoveDpToList, unProcessAlarm_disableCategory, unProcessAlarm_getAllDpConfigured: use sSystemName instead of sSystem
    
  11/02/2009: Herve
    - new function: 
      unProcessAlarm_getDPCategorySetting: get the list of categories the DP is in and the list of categories the DP is not in
      unProcessAlarm_addRemoveDpToList: add ore remove a DP from a list of category
    - modified function: 
      unProcessAlarm_removeDpFromList: accept one category or "*"
    
  24/04/2006: bug with 3 categories in unProcessAlarm_addDpToList
  06/04/2006: Celine
    - add unProcessAlarm_getAllDpConfigured to get the list of all dps with an sms configuration
  23/06/2005: Celine
    - bug in unProcessAlarm_isDpConfigured for alarm = H
  17/06/2005: Celine
    - add functions for SMS and mail:
      .unProcessAlarm_isDpConfigured
      .unProcessAlarm_removeDpFromList
      .unProcessAlarm_disableCategory
      .unProcessAlarm_addDpToList
version 1.0

External Function :
  .unProcessAlarm_isDpConfigured
  .unProcessAlarm_removeDpFromList
  .unProcessAlarm_disableCategory
  .unProcessAlarm_addDpToList
  .unProcessAlarm_getAllDpConfigured
Internal Functions :


Purpose: This library contains functions for mail and SMS configuration.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Unicos objects (DPType etc.)
  . PVSS version: 3.0.0 
  . operating system: NT and W2000, but tested only under XP.
  . distributed system: yes.
*/

//@{
//unProcessAlarm_getDPCategorySetting
/**
Purpose: get the list of categories the DP is in and the list of categories the DP is not in.

  @param sDpName: string, input, dpname
  @param sCategory: dyn_string, output, list of all categories
  @param dbIn: dyn_bool, output, true=DP in category/false=DP not in category
  @param exceptionInfo, dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
  . PVSS version: 3.0.1 
  . operating system: W2000, XP and Linux
  . distributed system: yes.
*/
unProcessAlarm_getDPCategorySetting(string sDpName, dyn_string &dsCategory, dyn_bool &dbIn, dyn_string &exceptionInfo)
{
  dyn_string dsMailSettings, dsSettings;
  string sSystemName, sCategory;
  bool bIn;
  int len,i;
  
  sSystemName = unGenericDpFunctions_getSystemName(sDpName);
  dynClear(dsCategory);
  dynClear(dbIn);
  
  if(dpExists (sSystemName+"_unSendMail.config.mail_settings:_original.._value"))
  {
    dpGet(sSystemName+"_unSendMail.config.mail_settings:_original.._value", dsMailSettings,
          sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
    len=dynlen(dsMailSettings);
    for(i=1;i<=len;i++)        //for each Category
    {
      sCategory = substr(dsMailSettings[i], 0, strpos(dsMailSettings[i], "~"));
      bIn = unProcessAlarm_isInDPCategory(sDpName, dsSettings[i]);
      dynAppend(dsCategory, sCategory);
      dynAppend(dbIn, bIn);
    }
  }  
  else 
  {
    fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.mail_settings doesn't exists","");
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT1, "unProcessAlarm.ctl unProcessAlarm_getDPCategorySetting", "unProcessAlarm_getDPCategorySetting", 
                               sDpName, sSystemName, dsMailSettings, dsSettings, dsCategory, dbIn);
}

/** Function returns true if dpe is in the pattern (_unSendMail.config.report_settings) of a Mail/SMS category
*/
bool unProcessAlarm_isInDPCategory(string dpName, string patternValue)
{
  dyn_string dpes;
    dyn_string patterns = strsplit(patternValue, "~");
    for (int j = 1; j <= dynlen(patterns); j++) {
      dynClear(dpes);
      if (strpos(patterns[j],"@")==0) {
        dyn_string dsAliases;
        dpGetAllAliases(dpes,dsAliases,substr(patterns[j],1));
      } else {
        dpes = dpNames(patterns[j]);
      }
      if (dynContains(dpes, dpName)) return true;
    }
    return false;
}


//---------------------------------------------------------------------------------------------------------------------------------------
//unProcessAlarm_addRemoveDpToList
/**
Purpose:
add ore remove a DP from a list of category

  @param sDpName: string, input, dpname
  @param dsCategory: dyn_string, input, list of category
  @param dbIn: dyn_bool, input, list of action to do, true=add/false=remove
  @param exceptionInfo, dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
  . PVSS version: 3.0.1 
  . operating system: W2000, XP and Linux
  . distributed system: yes.
*/
unProcessAlarm_addRemoveDpToList(string sDpName, dyn_string dsCategory, dyn_bool dbIn, dyn_string &exceptionInfo)
{
  int i, len=dynlen(dsCategory), j;
  dyn_string dsSettings,dsMailSettings,dsDpe;
  string sSettings, sSystemName;
  int iSetting, lenSetting, index, len_dpe;
  
  if(len>0) {
    sSystemName = unGenericDpFunctions_getSystemName(sDpName);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT1, "unProcessAlarm.ctl unProcessAlarm_addRemoveDpToList", "unProcessAlarm_addRemoveDpToList", 
                                 dsCategory, dbIn, sDpName, sSystemName);
    if(dpExists (sSystemName+"_unSendMail.config.mail_settings:_original.._value"))
    {
      dpGet(sSystemName+"_unSendMail.config.mail_settings:_original.._value", dsMailSettings);
      dpGet(sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
      
      for(i=1;i<=len;i++) {
        lenSetting = dynlen(dsSettings);
        for(iSetting=1;iSetting<=lenSetting; iSetting++) {
          if(patternMatch("*"+dsCategory[i]+"~*", dsMailSettings[iSetting]))
          {
            if(dbIn[i]) { // add if not already added
              if (!unProcessAlarm_isInDPCategory(sDpName, dsSettings[iSetting])) { // add
                unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT1, "unProcessAlarm.ctl unProcessAlarm_addRemoveDpToList", "unProcessAlarm_addRemoveDpToList add", 
                                 dsCategory[i]);
                sSettings="";
                dsDpe=strsplit(dsSettings[iSetting],"~");
                dynAppend(dsDpe, sDpName);
                dynUnique(dsDpe);
                len_dpe=dynlen(dsDpe);
                if(len_dpe>0)
                {
                  sSettings = dsDpe[1];
                  for(j=2;j<=len_dpe;j++)        //for each Dpe
                  {
                    sSettings =sSettings +"~" + dsDpe[j];  
                  }
                  dsSettings[iSetting] = sSettings;    
                }
                else 
                  dsSettings[iSetting]="";
              }
            }
            else { // remove if not already removed
              if(strpos(dsSettings[iSetting], sDpName) >= 0) 
              { // remove
                unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT1, "unProcessAlarm.ctl unProcessAlarm_addRemoveDpToList", "unProcessAlarm_addRemoveDpToList remove", 
                                 dsCategory[i]);
                
                sSettings="";
                dsDpe=strsplit(dsSettings[iSetting],"~");
                index= dynContains(dsDpe,sDpName);
                dynRemove(dsDpe, index);
                len_dpe=dynlen(dsDpe);
                if(len_dpe>0)
                {
                  sSettings = dsDpe[1];
                  for(j=2;j<=len_dpe;j++)        //for each Dpe
                  {
                    sSettings =sSettings +"~" + dsDpe[j];  
                  }
                  dsSettings[iSetting] = sSettings;    
                }
                else 
                  dsSettings[iSetting]="";
              }    
            }
          }
        }
      }
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT1, "unProcessAlarm.ctl unProcessAlarm_addRemoveDpToList", "unProcessAlarm_addRemoveDpToList result", 
                               dsSettings);
      dpSet(sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
    }
    else 
    {
        fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.report_settings doesn't exists","");
    }  
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProcessAlarm_addDpToList
/**
Purpose:
add the Dp to the category

  @param sDpName: string, input, dpname
  @param sCategory: string, input, category name
  @param exceptionInfo, dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
  . PVSS version: 3.0.1 
  . operating system: W2000, XP and Linux
  . distributed system: yes.
*/
unProcessAlarm_addDpToList(string sDpName, string sCategory, dyn_string &exceptionInfo)
{
  dyn_string dsSettings,dsMailSettings,dsDpe,dsDisable;
  string sSettings, sSystemName;
  int len,i,j,len_dpe,iRes;
  
  sSystemName = unGenericDpFunctions_getSystemName(sDpName);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT1, "unProcessAlarm.ctl unProcessAlarm_addDpToList", "unProcessAlarm_addDpToList", 
                               sCategory, sDpName, sSystemName);
  if(dpExists (sSystemName+"_unSendMail.config.mail_settings:_original.._value"))
  {
    dpGet(sSystemName+"_unSendMail.config.mail_settings:_original.._value", dsMailSettings);
    dpGet(sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
    dpGet(sSystemName+"_unSendMail.config.mail_disable:_original.._value", dsDisable);

    len=dynlen(dsMailSettings);
    if(len>=1)
    {  
      for (i=1;i<=len;i++)        //for each Categorie
      {
        if(patternMatch("*"+sCategory+"~*", dsMailSettings[i]))
        {
          if (!unProcessAlarm_isInDPCategory(sDpName, dsSettings[i])) { // add
            dsDpe=strsplit(dsSettings[i],"~");
            dynAppend(dsDpe,sDpName);
            dynUnique(dsDpe);
            len_dpe=dynlen(dsDpe);
            sSettings = dsDpe[1];
            if(len_dpe >=2)
            {
              for(j=2;j<=len_dpe;j++)        //for each Dpe
                {
                    sSettings =sSettings +"~" + dsDpe[j];  
                }
            }
            dsSettings[i] = sSettings;    
          }
        }  
      }  
      iRes = dpSet(sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
      if(iRes<0)
        fwException_raise(exceptionInfo,"ERROR","_unSendMail.config.report_settings was not updated","");    
    }
    else 
    {
      fwException_raise(exceptionInfo,"ERROR","Category unProcessAlarm not present","");
    }  
  }  
  else 
  {
    fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.mail_settings doesn't exists","");
  }

}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProcessAlarm_disableCategory
/**
Purpose:
enable/disable sending of the category

  @param bMail: bool, input, current setting of the sending mail category
  @param bOld: bool, input/output, old setting of the sending mail category
  @param sSystemName: string, input, system name
  @param sCategory: string, input, category name
  @param exceptionInfo, dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
  . PVSS version: 3.0.1 
  . operating system: W2000, XP and Linux
  . distributed system: yes.
  
  @reviewed 2018-06-26 @whitelisted{API} It should be used in 
          vision/systemIntegrity/unMailCategoryConfig.pnl or vision/systemIntegrity/unConfigMailSystem.pnl
          instead of direct dpSets!
*/
unProcessAlarm_disableCategory(bool bMail, bool &bOld, string sSystemName, string sCategory, dyn_string &exceptionInfo)
{
  dyn_string dsSettings,dsMailSettings,dsDisable;
  int len,i,index, iRes;

//DebugN("unProcessAlarm_disableCategory", sSystemName,  sCategory);

  if(dpExists (sSystemName+"_unSendMail.config.mail_settings:_original.._value"))
  {
    dpGet(sSystemName+"_unSendMail.config.mail_settings:_original.._value", dsMailSettings);
    dpGet(sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
    dpGet(sSystemName+"_unSendMail.config.mail_disable:_original.._value", dsDisable);
    
    len=dynlen(dsSettings);
    
    for (i=1;i<=len;i++)        //for each Categorie
      {
      if(patternMatch("*"+sCategory+"~*", dsMailSettings[i]))
        {
        if (bMail)  
            {
            bOld=(bool)dsDisable[i];
            dsDisable[i] = true;
            }
        else
          {
          if(!bOld)
            dsDisable[i] = false;
          }    
        }
      }    
    dpSet(sSystemName+"_unSendMail.config.mail_disable:_original.._value", dsDisable);
  }
  else 
  {
      fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.mail_settings doesn't exists","");
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProcessAlarm_isDpConfigured
/**
Purpose:
Check if the DP is correctly configured 
**** deprecated function ****

  @param sDpName: string, input, dpname
  @param isConfigured: bool, output, dp is correctly configured for mail

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
  . PVSS version: 3.0.1 
  . operating system: W2000, XP and Linux
  . distributed system: yes.
*/
unProcessAlarm_isDpConfigured(string sDpName,int alertType,int iAlertRange, dyn_bool &isConfigured)
{
  dyn_string dsSettings,dsDpe,dsMailSettings,currentAlertClass;
  int len,i,index,len_dpe,j;
  string sSettings;
  
//  DebugN("unProcessAlarm_isDpConfigured", sDpName);

  isConfigured = makeDynString(false,false,false,false);
  if(alertType == DPCONFIG_ALERT_NONBINARYSIGNAL)
    {
      switch(iAlertRange) {
        case UN_AIAO_ALARM_5_HH_H_L_LL:
          dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[4]);    
          dpGet(sDpName + ":_alert_hdl.2._class", currentAlertClass[3]);    
          dpGet(sDpName + ":_alert_hdl.4._class", currentAlertClass[2]);    
          dpGet(sDpName + ":_alert_hdl.5._class", currentAlertClass[1]);    
            
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[1])) 
            isConfigured[1] = true;
          else isConfigured[1] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[2])) 
            isConfigured[2] = true;
          else isConfigured[2] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[3])) 
            isConfigured[3] = true;
          else isConfigured[3] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[4])) 
            isConfigured[4] = true;
          else isConfigured[4] = false;
        break;
        case UN_AIAO_ALARM_4_HH_H_L:
          dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[3]);    
          dpGet(sDpName + ":_alert_hdl.3._class", currentAlertClass[2]);    
          dpGet(sDpName + ":_alert_hdl.4._class", currentAlertClass[1]);    
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[1])) 
            isConfigured[1] = true;
          else isConfigured[1] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[2])) 
            isConfigured[2] = true;
          else isConfigured[2] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[3])) 
            isConfigured[3] = true;
          else isConfigured[3] = false;
        break;
        case UN_AIAO_ALARM_4_H_L_LL:
          dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[4]);    
          dpGet(sDpName + ":_alert_hdl.2._class", currentAlertClass[3]);    
          dpGet(sDpName + ":_alert_hdl.4._class", currentAlertClass[2]);    
            
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[2])) 
            isConfigured[2] = true;
          else isConfigured[2] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[3])) 
            isConfigured[3] = true;
          else isConfigured[3] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[4])) 
            isConfigured[4] = true;
          else isConfigured[4] = false;
        break;
        case UN_AIAO_ALARM_3_H_L:
          dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[3]);    
          dpGet(sDpName + ":_alert_hdl.3._class", currentAlertClass[2]);    
            
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[2])) 
            isConfigured[2] = true;
          else isConfigured[2] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[3])) 
            isConfigured[3] = true;
          else isConfigured[3] = false;
        break;
        case UN_AIAO_ALARM_3_HH_H:
          dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[2]);    
          dpGet(sDpName + ":_alert_hdl.3._class", currentAlertClass[1]);    
                
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[1])) 
            isConfigured[1] = true;
          else isConfigured[1] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[2])) 
            isConfigured[2] = true;
          else isConfigured[2] = false;
        break;    
        case UN_AIAO_ALARM_3_LL_L:
          dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[4]);    
          dpGet(sDpName + ":_alert_hdl.3._class", currentAlertClass[3]);    
                
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[3])) 
            isConfigured[3] = true;
          else isConfigured[3] = false;
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[4])) 
            isConfigured[4] = true;
          else isConfigured[4] = false;
        break;
        case UN_AIAO_ALARM_2_H:
          dpGet(sDpName + ":_alert_hdl.2._class", currentAlertClass[2]);      
              
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[2])) 
            isConfigured[2] = true;
          else isConfigured[2] = false;
        break;
        case UN_AIAO_ALARM_2_L:
          dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[3]);    
              
          if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[3])) 
            isConfigured[3] = true;
          else isConfigured[3] = false;
        break;
    }
  }
  else
  {
    dpGet(sDpName + ":_alert_hdl.1._class", currentAlertClass[1]);
    if (patternMatch(UN_PROCESSALARM_PATTERN, currentAlertClass[1])) 
      isConfigured[1] = true;
    else isConfigured[1] = false;
  }
//      DebugN("currentAlertClass",currentAlertClass,"iAlertRange",iAlertRange,"isConfigured",isConfigured );
}  

//---------------------------------------------------------------------------------------------------------------------------------------

// unProcessAlarm_getAllDpConfigured
/**
Purpose: return the list of CP-5 DP configured with sms.
**** deprecated function ****

Parameter: dsConfiguredDP, dyn_string, output, result of the function
  @param exceptionInfo, dyn_string, output, the error is returned here
  
Usage: Public

PVSS manager usage: Ctrl, UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unProcessAlarm_getAllDpConfigured(string sSystem, dyn_string &dsConfiguredDP, dyn_string &dsDPType , dyn_string &dsCategory, dyn_string &dsReceiver, dyn_string &dsSystemName, dyn_string &exceptionInfo)
{
  dyn_string dsDP, dsTemp, dsCat, dsDPECat, dsTempReceiver,dsSystem, dsHost,
  dsType = makeDynString("Alarm", "AnalogInput", "AnalogOutput", "DigitalInput", "DigitalOutput"),
  dsExtension = makeDynString(".ProcessInput.PosAl", ".ProcessInput.PosSt", ".ProcessInput.PosSt", ".ProcessInput.PosSt", ".ProcessInput.PosSt");
  
  dyn_bool dbResult;
  int i, length, iRes, iType, iRange,len;
  string sSystemName, sReceiver, sCategory;
  dyn_int diSystem, diPort;

  for (int j=1; j<=dynlen(dsType); j++)
  {
    dynClear(dsTemp);
    dynAppend(dsTemp, dpNames(sSystem+"*" , dsType[j]));  // Get DPEs
    length = dynlen(dsTemp);

    for(i=1;i<=length;i++)  
    {
      iRes = dpGet(dsTemp[i] +dsExtension[j]+":_alert_hdl.._type", iType);
      if(iType != DPCONFIG_NONE) 
      {  
        iRes = dpGet(dsTemp[i] +dsExtension[j]+":_alert_hdl.._num_ranges", iRange);
        unProcessAlarm_isDpConfigured(dsTemp[i]+dsExtension[j],iType,iRange,dbResult);
        if(dynContains(dbResult, 1))
        {
          dynAppend(dsConfiguredDP, dpGetAlias(dsTemp[i]+"."));
          dynAppend(dsDP, dsTemp[i]);
          dynAppend(dsDPType, dsType[j]);
          dynAppend(dsSystemName,sSystem);
        }  
      }  
    }
  }

  if(dpExists(sSystem+"_unSendMail.config.report_settings"))
  {
    dpGet(sSystem+"_unSendMail.config.report_settings", dsDPECat);
    if(dpExists(sSystem+"_unSendMail.config.mail_settings"))
    {
      dpGet(sSystem+"_unSendMail.config.mail_settings", dsCat);
      for (int j=1; j<=dynlen(dsDP); j++)
      {
        if(dynlen(dsCat)>0)
        {
          sCategory="";
          dynClear(dsTempReceiver);
          sReceiver="";
          for(int c=1; c<=dynlen(dsDPECat); c++)
          {
            if(patternMatch("*"+dsDP[j]+"*", dsDPECat[c]) || patternMatch(dsDP[j]+"*", dsDPECat[c]))
            {
              dsTemp = strsplit(dsCat[c], "~");
              if(sCategory=="") sCategory= dsTemp[1];
                else sCategory = sCategory +"~"+ dsTemp[1];
              if(dynlen(dsTemp)>=5)
                dynAppend(dsTempReceiver, dsTemp[5]);
              else   dynAppend(dsTempReceiver, "");
            }
          }  
          dynUnique(dsTempReceiver);
          if(dynlen(dsTempReceiver)>1)
          {
            for(int c=1;c<=dynlen(dsTempReceiver); c++)
            {
              if(sReceiver!="")
                sReceiver=sReceiver+"~"+dsTempReceiver[c];
              else sReceiver =   dsTempReceiver[c];
            }
          }  
          if(dynlen(dsTempReceiver)==1)  sReceiver = dsTempReceiver[1]; 
        }  
        else 
        {
          sCategory="";
          sReceiver="";
        }
        dynAppend(dsCategory, sCategory);  
        dynAppend(dsReceiver, sReceiver);  
      }              
    }
    else
    {
      fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.mail_settings doesn't exists","");
    }  
  }
  else 
  {
    fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.report_settings doesn't exists","");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unProcessAlarm_checkCategory
/** check the category name
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsCategory  input, list of category to check, the system name must not be included
@param exceptionInfo  output, errors are returned here
*/
unProcessAlarm_checkCategory(dyn_string dsCategory, dyn_string &exceptionInfo)
{
  int i, len, iPos;
  string sCat;
  
  len = dynlen(dsCategory);
  for(i=1;i<=len;i++)
  {
    iPos = strpos(dsCategory[i], ":");
    if(iPos > 0)
      sCat = substr(dsCategory[i], iPos+1, strlen(dsCategory[i])-iPos-1);
    else
      sCat = dsCategory[i];
    if(nameCheck(sCat) != 0)
      fwException_raise(exceptionInfo, "ERROR", "Mail/SMS check category: wrong category name "+dsCategory[i]+" \n(allowed characters: 0..9, A..Z, _, a..z)", "");
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unProcessAlarm.ctl unProcessAlarm_checkCategory", "unProcessAlarm_checkCategory", 
                               dsCategory, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unProcessAlarm_addUpdateCategory
/** add or update the category, if the syntax of one category is bad, none of the category will created or updated
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsCategory  input, list of category to check, to delete a category on a remote system the system name must be included
@param sSender  input, sender of the email/SMS, the one that will appear as sender in outlook
@param dsReceiver  input, list of receiver for each category, many email or sms can be given for a category, they must be separated by ; like in outlook
@param exceptionInfo  output, errors are returned here
@param sMailDomain  input, list of mail/SMS domain, like cern.ch, default "cern.ch"
@param sSmtp  input, smtp server, default "cernmx.cern.ch"
@param diPeriod  input, list of period for the report in sec., default 86400 sec.
@param dbMailSMSDisabled  input, list of state of the mail/SMS, default true, if the mailSMSDisable is set to true, the ReportDisabled is set to true
@param dbReportDisabled  input, list of state of the periodic report, default true
@param dsMaxMailPerDay  input, maximum mail per day, defaul "250"
@param dsLink  input, list of category to check, to delete a category on a remote system the system name must be included
@param dbUpdateCategory  input, true=update category/false=do not update the category
*/
unProcessAlarm_addUpdateCategory(dyn_string dsCategory, string sSender, dyn_string dsReceiver, dyn_string &exceptionInfo, string sMailDomain="cern.ch", 
                           string sSmtp="cernmx.cern.ch", dyn_int diPeriod=makeDynInt(), dyn_bool dbMailSMSDisabled=makeDynBool(), 
                           dyn_bool dbReportDisabled=makeDynBool(), dyn_string dsMaxMailPerDay=makeDynString(), dyn_string dsLink=makeDynString(), 
                           dyn_bool dbUpdateCategory=makeDynBool())
{
  int i, len, iRes, iPos;
  string sSystemName, sHost, sIPAddress;
  string sCat, dpCat;
  dyn_string dsSetting, dsDisable, dsDpe, exceptionInfoTemp;
  dyn_string dsMatchingCat;
  bool bSet, bConnected;
  
  len = dynlen(dsCategory);
  while(dynlen(dsReceiver) < len)
    dynAppend(dsReceiver, "none");
  while(dynlen(diPeriod) < len)
    dynAppend(diPeriod, 86400);
  while(dynlen(dbMailSMSDisabled) < len)
    dynAppend(dbMailSMSDisabled, true);
  while(dynlen(dbReportDisabled) < len)
    dynAppend(dbReportDisabled, true);
  while(dynlen(dsMaxMailPerDay) < len)
    dynAppend(dsMaxMailPerDay, "250");
  while(dynlen(dsLink) < len)
  {
    dynAppend(dsLink, "");
    sSystemName=dpSubStr(dsCategory[dynlen(dsLink)], DPSUB_SYS);
    if(sSystemName == "")
      sSystemName = getSystemName();
    unGenericDpFunctions_getHostName(sSystemName, sHost, sIPAddress);
    dsLink[dynlen(dsLink)]="http://abinf1/lemon/info.php?host=" + strtoupper(sHost);
  }
  while(dynlen(dbUpdateCategory) < len)
    dynAppend(dbUpdateCategory, false);
  for(i=1;i<=len;i++)
  {
    bSet = false;
    iPos = strpos(dsCategory[i], ":");
    if(iPos > 0)
    {
      sSystemName = substr(dsCategory[i], 0, iPos+1);
      sCat = substr(dsCategory[i], iPos+1, strlen(dsCategory[i])-iPos-1);
    }
    else
    {
      sSystemName = getSystemName();
      sCat = dsCategory[i];
    }
    dynClear(exceptionInfoTemp);
    unProcessAlarm_checkCategory(makeDynString(sCat), exceptionInfoTemp);
    if(dynlen(exceptionInfoTemp) > 0)
      dynAppend(exceptionInfo, exceptionInfoTemp);
    else
    {
      unDistributedControl_isConnected(bConnected, sSystemName);
      if(bConnected)
      {
        dpCat = sSystemName+sCat;
        if(sSender == "")
          sSender = "none";
        if(dsReceiver[i] == "")
          dsReceiver[i] = "none";
        dpGet(sSystemName+"_unSendMail.config.mail_settings", dsSetting,
              sSystemName+"_unSendMail.config.mail_disable", dsDisable,
              sSystemName+"_unSendMail.config.report_settings", dsDpe);
        dsMatchingCat = dynPatternMatch(sCat+"~*", dsSetting);
        if((dynlen(dsMatchingCat) > 0) && dpExists(dpCat))// pattern found
        {
          iPos = dynContains(dsSetting, dsMatchingCat[1]);
          if(iPos > 0) // category already existing 
          {
            if(dbUpdateCategory[i]) // & update true ==> update
            {
              bSet = true;
              dsSetting[iPos] = sCat+ "~" + sMailDomain + "~" + sSmtp + "~" + sSender + "~" + dsReceiver[i] + "~" + dsLink[i] + "~" + dsMaxMailPerDay[i];
              dsDisable[iPos] = dbMailSMSDisabled[i];
            }
          }
          else
            fwException_raise(exceptionInfo, "ERROR", "Mail/SMS add/update category: "+dsCategory[i]+" not found", "");
        }
        else
        {
          bSet = true;
          dynAppend(dsSetting, sCat+ "~" + sMailDomain + "~" + sSmtp + "~" + sSender + "~" + dsReceiver[i] + "~" + dsLink[i] + "~" + dsMaxMailPerDay[i]);
          dynAppend(dsDisable, dbMailSMSDisabled[i]);
          dynAppend(dsDpe, "");
        }
        if(bSet)
        {
          _unSystemIntegrity_createScheduler(sCat, iRes, sSystemName);
          if(dpExists(dpCat))
          {
            dpSetWait(sSystemName+"_unSendMail.config.mail_settings", dsSetting,
                  sSystemName+"_unSendMail.config.mail_disable", dsDisable,
                  sSystemName+"_unSendMail.config.report_settings", dsDpe);
            if(dbMailSMSDisabled[i])
              dbReportDisabled[i] = true;
            dpSetWait(dpCat + ".time.timedFunc.interval", diPeriod[i], dpCat + ".mode", !dbReportDisabled[i]);
            dpSetWait(sSystemName + "_ScCom.transfer:_original.._value", sCat +"|"+ dpSubStr(dpCat, DPSUB_SYS_DP)); 
          }
          else
            fwException_raise(exceptionInfo, "ERROR", "Mail/SMS add/update category: "+dsCategory[i]+" not created", "");
        }
      }
      else
        fwException_raise(exceptionInfo, "ERROR", "Mail/SMS add/update category: "+dsCategory[i]+" system not connected", "");
    }
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unProcessAlarm.ctl unProcessAlarm_addUpdateCategory", "unProcessAlarm_addUpdateCategory", 
                               dsCategory, sSender, dsReceiver, exceptionInfo, sMailDomain, 
                               sSmtp, diPeriod, dbMailSMSDisabled, dbReportDisabled, dsMaxMailPerDay, dsLink, dbUpdateCategory);
}

//------------------------------------------------------------------------------------------------------------------------
// unProcessAlarm_deleteCategory
/** delete the category
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsCategory  input, list of category to check, to delete a category on a remote system the system name must be included
@param exceptionInfo  output, errors are returned here
@reviewed 2018-06-26 @whitelisted{API} It should be used in 
          vision/systemIntegrity/unMailCategoryConfig.pnl or vision/systemIntegrity/unConfigMailSystem.pnl
          instead of direct dpSets!
*/
unProcessAlarm_deleteCategory(dyn_string dsCategory, dyn_string &exceptionInfo)
{
  int i, len, iPos, iRes;
  string sCat, dpCat, sSystemName;
  dyn_string dsSetting, dsDisable, dsDpe;
  dyn_string dsMatchingCat;
  
  len = dynlen(dsCategory);
  for(i=1;i<=len;i++)
  {
    sSystemName = dpSubStr(dsCategory[i], DPSUB_SYS);
    sCat = dpSubStr(dsCategory[i], DPSUB_DP);
    dpCat = sSystemName+sCat;
    dpGet(sSystemName+"_unSendMail.config.mail_settings", dsSetting,
          sSystemName+"_unSendMail.config.mail_disable", dsDisable,
          sSystemName+"_unSendMail.config.report_settings", dsDpe);
    dsMatchingCat = dynPatternMatch(sCat+"~*", dsSetting);
    if(dynlen(dsMatchingCat) > 0) // pattern found
    {
      iPos = dynContains(dsSetting, dsMatchingCat[1]);
      if(iPos > 0)
      {
        dynRemove(dsSetting, iPos);
        dynRemove(dsDisable, iPos);
        dynRemove(dsDpe, iPos);    
        dpSetWait(sSystemName+"_unSendMail.config.mail_settings", dsSetting,
              sSystemName+"_unSendMail.config.mail_disable", dsDisable,
              sSystemName+"_unSendMail.config.report_settings", dsDpe);
        if(dpExists(dpCat))
        {
          sc_deactivateProgram(dpCat); 
          sc_deleteProgram(dpCat, iRes);
        }
        else
          fwException_raise(exceptionInfo, "ERROR", "Mail/SMS delete category: cannot remove scheduler "+dsCategory[i], "");
      }
      else
        fwException_raise(exceptionInfo, "ERROR", "Mail/SMS delete category: "+dsCategory[i]+" not matching "+dsMatchingCat[1], "");
    }
    else
      fwException_raise(exceptionInfo, "ERROR", "Mail/SMS delete category: "+dsCategory[i]+" not found", "");
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unProcessAlarm.ctl unProcessAlarm_deleteCategory", "unProcessAlarm_deleteCategory", 
                               dsCategory, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unProcessAlarm_getCategory
/** get all the category of the DP
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsDpName  input, list of DP
@param ddsCategory  output, list of category to for each DP
@param exceptionInfo  output, errors are returned here
*/
unProcessAlarm_getCategory(dyn_string dsDpName, dyn_dyn_string &ddsCategory, dyn_string &exceptionInfo)
{
  int i, len, iCat, lenCat;
  dyn_bool dbIn;
  dyn_string dsCategory, dsInCategory;
  
  dynClear(ddsCategory);
  len=dynlen(dsDpName);
  for(i=1;i<=len;i++)
  {
    dynClear(dbIn);
    dynClear(dsCategory);
    dynClear(dsInCategory);
    unProcessAlarm_getDPCategorySetting(dsDpName[i], dsCategory, dbIn, exceptionInfo);
    lenCat = dynlen(dsCategory);
    for(iCat = 1; iCat <= lenCat;iCat++)
    {
      if(dbIn[iCat])
        dynAppend(dsInCategory, dsCategory[iCat]);
    }
    dynAppend(ddsCategory, dsInCategory);
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unProcessAlarm.ctl unProcessAlarm_getCategory", "unProcessAlarm_getCategory", 
                               dsDpName, ddsCategory, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unProcessAlarm_createCategory
/** Create the category, if the syntax of one category is bad, none of the category will created
  if a categroy is already existing, it will not be updated.
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsCategory  input, list of category to check, to delete a category on a remote system the system name must be included
@param sSender  input, sender of the email/SMS, the one that will appear as sender in outlook
@param dsReceiver  input, list of receiver for each category, many email or sms can be given for a category, they must be separated by ; like in outlook
@param exceptionInfo  output, errors are returned here
@param sMailDomain  input, list of mail/SMS domain, like cern.ch, default "cern.ch"
@param sSmtp  input, smtp server, default "cernmx.cern.ch"
@param diPeriod  input, list of period for the report in sec., default 86400 sec.
@param dbMailSMSDisabled  input, list of state of the mail/SMS, default true, if the mailSMSDisable is set to true, the ReportDisabled is set to true
@param dbReportDisabled  input, list of state of the periodic report, default true
@param dsMaxMailPerDay  input, maximum mail per day, defaul "250"
@param dsLink  input, list of category to check, to delete a category on a remote system the system name must be included
*/
unProcessAlarm_createCategory(dyn_string dsCategory, string sSender, dyn_string dsReceiver, dyn_string &exceptionInfo, string sMailDomain="cern.ch", 
                           string sSmtp="cernmx.cern.ch", dyn_int diPeriod=makeDynInt(), dyn_bool dbMailSMSDisabled=makeDynBool(), 
                           dyn_bool dbReportDisabled=makeDynBool(), dyn_string dsMaxMailPerDay=makeDynString(), dyn_string dsLink=makeDynString())
{
  unProcessAlarm_addUpdateCategory(dsCategory, sSender, dsReceiver, exceptionInfo, sMailDomain, 
                                   sSmtp, diPeriod, dbMailSMSDisabled, 
                                   dbReportDisabled, dsMaxMailPerDay, dsLink);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unProcessAlarm.ctl unProcessAlarm_createCategory", "unProcessAlarm_createCategory", 
                               dsCategory, sSender, dsReceiver, exceptionInfo, sMailDomain, 
                               sSmtp, diPeriod, dbMailSMSDisabled, dbReportDisabled, dsMaxMailPerDay, dsLink);
}

//------------------------------------------------------------------------------------------------------------------------
// unProcessAlarm_getCategoryPerSystem
/** Get all the category of a given system
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param sSystemName  input, the system name
@param dsCategory  input, list of category to check, to delete a category on a remote system the system name must be included
@param exceptionInfo  output, errors are returned here

@reviewed 2018-06-26 @whitelisted{API}
*/
unProcessAlarm_getCategoryPerSystem(string sSystemName, dyn_string &dsCategory, dyn_string &exceptionInfo)
{
  dyn_string dsMailSettings, dsSettings;
  string sCategory;
  int len,i;
  
  dynClear(dsCategory);
  
  if(dpExists (sSystemName+"_unSendMail.config.mail_settings:_original.._value"))
  {
    dpGet(sSystemName+"_unSendMail.config.mail_settings:_original.._value", dsMailSettings,
          sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
    len=dynlen(dsMailSettings);
    for(i=1;i<=len;i++)        //for each Category
    {
      sCategory = substr(dsMailSettings[i], 0, strpos(dsMailSettings[i], "~"));
      dynAppend(dsCategory, sCategory);
    }
  }  
  else 
  {
    fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.mail_settings doesn't exists","");
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT1, "unProcessAlarm.ctl unProcessAlarm_getCategoryPerSystem", "unProcessAlarm_getCategoryPerSystem", 
                               sSystemName, dsMailSettings, dsSettings, dsCategory);
}

//---------------------------------------------------------------------------------------------------------------------------------------

//@}
