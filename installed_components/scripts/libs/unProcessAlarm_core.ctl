/**@file
  
// unProcessAlarm_core.ctl
Functions for Mail/SMS utility.

@par Creation Date
  18/07/2011

@par Modification History
  - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity
  
@par Constraints
  . operating system: WXP and Linux.
  . distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Herve Milcent (EN-ICE)
*/

//@{

//------------------------------------------------------------------------------------------------------------------------

//unProcessAlarm_removeDpFromList
/**
Purpose:
remove the Dp from all the category

  @param sDpName: string, input, dpname
  @param exceptionInfo, dyn_string, output, the error is returned here
  @param sCategory: string, input, category name or "*" for all

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.0.1 
  . operating system: W2000, XP and Linux
  . distributed system: yes.
*/
unProcessAlarm_removeDpFromList(string sDpName, dyn_string &exceptionInfo, string sCategory = "*")
{
  dyn_string dsSettings,dsDpe,dsMailSettings;
  int len,i,index,len_dpe,j;
  bool bRes;
  string sSettings,sSystemName;
  dyn_string dsTemp, dsMatchDpe;
  int iPos;
  int iMatch, lenMatch;

  sSystemName = unGenericDpFunctions_getSystemName(sDpName);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unProcessAlarm.ctl unProcessAlarm_removeDpFromList", "unProcessAlarm_removeDpFromList", 
                               sCategory, sDpName, sSystemName);

  if((dpExists(sSystemName+"_unSendMail.config.mail_settings:_original.._value")) &&(sDpName != ""))
  {
    dpGet(sSystemName+"_unSendMail.config.mail_settings:_original.._value", dsMailSettings);
    if(dpExists(sSystemName+"_unSendMail.config.report_settings:_original.._value"))
    {
      dpGet(sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
      len=dynlen(dsSettings);
      if(sCategory != "*") {
        dsTemp = dynPatternMatch(sCategory+"~*", dsSettings);
        dsSettings = dsTemp;
      }
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unProcessAlarm.ctl unProcessAlarm_removeDpFromList", "unProcessAlarm_removeDpFromList", 
                               dsSettings);
      for (i=1;i<=len;i++)        //for each Categorie
      {
        sSettings="";
        dsDpe=strsplit(dsSettings[i],"~");
        dsMatchDpe = dynPatternMatch("*"+sDpName+"*",dsDpe);
        lenMatch = dynlen(dsMatchDpe);
        for(iMatch=1;iMatch<=lenMatch;iMatch++)
        {
          index= dynContains(dsDpe,dsMatchDpe[iMatch]);
          if(index >0)
          {
            dynRemove(dsDpe, index);
          }
        }
        len_dpe=dynlen(dsDpe);
        if(len_dpe>0)
        {
          sSettings = dsDpe[1];
          for(j=2;j<=len_dpe;j++)        //for each Dpe
          {
            sSettings =sSettings +"~" + dsDpe[j];  
          }
          dsSettings[i] = sSettings;    
        }
        else dsSettings[i]="";
      }    
      dpSet(sSystemName+"_unSendMail.config.report_settings:_original.._value", dsSettings);
    }
    else 
    {
        fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.report_settings doesn't exists","");
    }  
  }
  else 
  {
      fwException_raise(exceptionInfo,"ERROR",sSystemName+"_unSendMail.config.mail_settings doesn't exists, or DPName empty","");
  }
}  

//------------------------------------------------------------------------------------------------------------------------
// unProcess_getReportCategoryFunction
/** Get the user defined function of a mail category
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sSystemName  input, system name
@param sCategory  input, the mail/SMS categroy
@param sFunction output, the user defined function
@param exceptionInfo output, error returned here
*/
unProcess_getReportCategoryFunction(string sSystemName, string sCategory, string &sFunction, dyn_string &exceptionInfo)
{
  dyn_string dsConfig;
  
  _unGenericDpFunctions_getKeyConfiguration(sSystemName+"_unSendMail", ".config.reportConfiguration", sCategory, dsConfig, exceptionInfo);
  if(dynlen(dsConfig)>=1)
    sFunction = dsConfig[1];
  else
    sFunction = "";
}

//------------------------------------------------------------------------------------------------------------------------
// unProcess_setReportCategoryFunction
/** Set the user defined function of a mail category
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sSystemName  input, system name
@param sCategory  input, the mail/SMS categroy
@param sFunction input, the user defined function
@param exceptionInfo output, error returned here
*/
unProcess_setReportCategoryFunction(string sSystemName, string sCategory, string sFunction, dyn_string &exceptionInfo)
{
  dyn_string dsConfig;
  
  if(sFunction != "")
    dsConfig=makeDynString(sFunction);
  _unGenericDpFunctions_setKeyConfiguration(sSystemName+"_unSendMail", ".config.reportConfiguration", sCategory, dsConfig, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

//@}
