/**@name LIBRARY: unProfileGraph.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 14/01/2005

Modification History: 
  27/01/2010: Herve
  - fix bug with Ygrid attribute
  
  12/01/2010: Herve
    - set bar border width to 0
    - restore bar border width setting
    
  29/06/2009: Herve
    - add g_dbSystemConnected
  
  20/10/2008: Herve
    - unProfileGraph_setDisconnectionDp: wrong color name
	
  20/10/2008: Herve
    - bug in rigth clik some device function not called
    
  30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

  - 28/05/2008: Herve
    unProfileGraph_exportData: configure fileSelector to select a file not existing.
      
  - 11/04/2008: Herve
    - add set Y scale in RC menu
    
  - 10/03/2008: Herve
    - all OLEColor function call to be removed.
    - colors for ProfileGraph as string
    
  - 28/02/2008: Herve
    - fileEditor in Linux
    
	- 25/02/2005: Herve
		- when setting the bar to disconnect state: unDataNoAccess color deselect always the bar just in case if was before.
		
	- 05/02/2005: Herve
		- missing bUseAlarm
				
version 1.0
		
Purpose: This library contains functions for the unProfileGraph widget.
Warning: The ProfileGraph once connected to te alert_hdl is not able to detech a change in the configuration of the alert: delete, add a range, 
delete a range. If the alert is deleted PVSS automatically disconnect the animation of the the DP and the bar is not anymore update. The panel 
containing the ProfileGraph must be closed and re-opened.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
	. UNICOS (DPType etc.)
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/

#uses "unSendMessage.ctl"

// constant definition for the ProfileGraph widget
const int UN_PROFILE_GRAPH_NODP = 0; // no DP
const int UN_PROFILE_GRAPH_DP = 1; // select
const int UN_PROFILE_GRAPH_DP_ALARM = 2; // select+alarm
const int UN_PROFILE_GRAPH_STSREG01 = 3; // select+stsreg01
const int UN_PROFILE_GRAPH_STSREG01_ALARM = 4; // select+stsreg01+alarm
const int UN_PROFILE_GRAPH_STSREG0102 = 5;  // select+stsreg01+stsreg02
const int UN_PROFILE_GRAPH_STSREG0102_ALARM = 6;  // select+stsreg01+stsreg02+alarm
const int UN_PROFILE_GRAPH_DP_NO_SELECT = 7; // select+noselect
const int UN_PROFILE_GRAPH_DP_NO_SELECT_ALARM = 8; // select+noselect+alarm
const string UN_PROFILE_GRAPH_NO_VALUE = "NO_VALUE";
const int UN_PROFILE_GRAPH_YGRID_SOLID = 0;
const int UN_PROFILE_GRAPH_YGRID_INVISIBLE_SOLID = 6;
const string UN_PROFILE_GRAPH_SYSTEMREGISTER_FUNCTION = "unProfileGraph_systemRegister";
const int UN_PROFILE_GRAPH_NO_YORIGIN = 0; // bars are drawn between bottom of graph and device's value
const int UN_PROFILE_GRAPH_YORIGIN = 1; // bars are drawn between given value (origin) and device's value

// constant definition for the ProfileGraph widget configuration panel
const string BAUTOSCALE = "$bAutoScale";
const string BAUTOSCALELABEL = "$bAutoScaleLabel";
const string BLOGSCALE = "$bLogScale";
const string BGRID = "$bGrid";
const string DSDEVICELABEL = "$dsDeviceLabel";
const string DSDEVICENAME = "$dsDeviceName";
const string DSDEVICEUSEALARM = "$dsDeviceUseAlarm";
const string DSSECTORTAG = "$dsSectorTag";
const string FYMAX = "$fYmax";
const string FYMIN = "$fYmin";
const string INUMSCALELABELS = "$iNumScaleLabels";
const string IUPDATEDELAY = "$iUpdateDelay";
const string IYGRIDSTYLE = "$iYGridStyle";
const string SAXISCOLOR = "$sAxisColor";
const string SBACKCOLOR = "$sBackColor";
const string SFORMAT = "$sFormat";
const string SGRAPHPROFILE_SYSTEMREGISTER = "$sGraphProfile_systemRegister";
const string STITLE = "$sTitle";
const string IBARWIDTH = "$iBarWidth";
const string GRAPHMODE = "$iBarOriginMode";
const string GRAPHORIGIN = "$fBarOriginValue";

//@{

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_registerToSystem
/**
Purpose: Register function to register to all devices 

Parameters:
	- sGraphProfile_systemRegister: string, input, register function
	- exceptionInfo, dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_registerToSystem(string sGraphProfile_systemRegister, dyn_string &exceptionInfo)
{
	dyn_string dsKey;
	int len, i;
	bool  bInitOk, bConnected;
	
	dsKey = mappingKeys(g_m_dsSystemName);
	len = dynlen(dsKey);
//DebugN("unProfileGraph_registerToSystem", len, g_m_dsSystemName);
	for(i=1;i<=len;i++) {
		unDistributedControl_register(sGraphProfile_systemRegister, bInitOk, bConnected, dsKey[i], exceptionInfo);
		if(!bInitOk) {
// the remote system is not configured 
// set the DeviceValue to invalid with 0 data and color to unDataNoAccess
			unProfileGraph_disconnectAllDp(dsKey[i]);
		}
//DebugN("unProfileGraph_registerToSystem", bInitOk, bConnected, dsKey[i], exceptionInfo);
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_disconnectAllDp
/**
Purpose: Set all DP of a system to not connected state

Parameters:
	- sSystem, string, input, PVSS system name

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_disconnectAllDp(string sSystem)
{
	dyn_string dsDevice = g_m_dsSystemName[sSystem];
	int i, len=dynlen(dsDevice);
	
	for(i=1;i<=len;i++) {
// get the value from the global variable
		unProfileGraph_setDisconnectionDp(dsDevice[i]);
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_setDisconnectionDp
/**
Purpose: Set a DP to not connected state

Parameters:
	- sIdentifier, string, input, identifier

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_setDisconnectionDp(string sIdentifier)
{
	float fValue;
	int iIndex, iRes;

// get the value from the global variable
	iIndex = g_mDeviceToIndex[sIdentifier];
	if(g_dsDeviceValue[iIndex] == UN_PROFILE_GRAPH_NO_VALUE)
		fValue = 0.0;
	else
		fValue = g_dsDeviceValue[iIndex];
		
// set the DeviceValue to invalid with 0 data and color to unDataNoAccess
	iRes = this.SetDeviceValue(sIdentifier, fValue, false, unProfileGraph_convertColor("unDataNoAccess"), false);
        if(g_dsDeviceSelectedColorState[iIndex] != "") {
//DebugTN("***** unProfileGraph_setDisconnectionDp SelectDese", sIdentifier, g_dsDeviceSelectedColorState[iIndex]);
          this.UnselectDevice(sIdentifier);
          g_dsDeviceSelectedColorState[iIndex] = "";
        }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_systemRegister
/**
Purpose: register function for each device

Parameters:
	- sDp, string, input, system name
	- bConnected, bool, input, state of the remote system

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_systemRegister(string sDp, bool bConnected)
{
	dyn_string dsDeviceName;
// get the dp to connect to.	
	string sSystemName, sSysDp = dpSubStr(sDp, DPSUB_SYS_DP), sDpName;
	int i, len, iIndex;
	int iAction, iRes;
	dyn_string exceptionInfo, dsResult;
	bool isRemoteSystem, localConnected;
	string sPVSSDpe;

	sSystemName = substr(sSysDp, strpos(sSysDp, c_unDistributedControl_dpName) + strlen(c_unDistributedControl_dpName), strlen(sSysDp)) + ":";
	dsDeviceName = g_m_dsSystemName[sSystemName];
//DebugTN("**********unProfileGraph_systemRegister", sDp, bConnected, sSystemName);

	unDistributedControl_isRemote(isRemoteSystem, sSystemName);
	localConnected = bConnected;
	if(!isRemoteSystem)
		{
		localConnected = true;
		}

	if(localConnected && !g_bSystemNameInitialized[sSystemName]) {
// check if g_mDeviceToIndex was built before. If not build it and call unProfileGraph_initDpe for each DP otherwise do nothing;
		len = dynlen(dsDeviceName);
		for(i=1;i<=len;i++) {
			unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(dsDeviceName[i], sPVSSDpe);
			iIndex = g_mDeviceToIndex[dsDeviceName[i]];
			g_dsDeviceDpe[iIndex] = sPVSSDpe;
			g_mDpToIndex[sPVSSDpe] = iIndex;
// call the init function
			unProfileGraph_initDpe(sPVSSDpe, g_dbDeviceUseAlarm[iIndex], g_diDeviceConnectType[iIndex], g_dsDeviceType[iIndex]);
		}
//DebugTN("INIT "+sSystemName, "lodev:", dsDeviceName, "dev:", g_dsDeviceName, "devDP:", g_dsDeviceDpe, "mapdev:", g_mDeviceToIndex, "mapDp:", g_mDpToIndex, "END "+sSystemName);
		g_bSystemNameInitialized[sSystemName] = true;
	}
	len = dynlen(dsDeviceName);
	for(i=1;i<=len;i++) {
		iIndex = g_mDeviceToIndex[dsDeviceName[i]];
		sPVSSDpe = g_dsDeviceDpe[iIndex];
		sDpName = unGenericDpFunctions_getDpName(sPVSSDpe);
//DebugN(sDpName, dsDeviceName[i], iIndex);
		unGenericObject_Connection(sDpName, g_dbCallbackConnected[iIndex], bConnected, iAction, exceptionInfo);
// if deviceData does not exists set iAction = UN_ACTION_DISCONNECT
                g_dbSystemConnected[iIndex] = localConnected;

		switch (iAction)
		{
			case UN_ACTION_DISCONNECT:
//DebugTN("UN_ACTION_DISCONNECT", sDpName, dsDeviceName[i], iIndex);
				unProfileGraph_setDisconnectionDp(dsDeviceName[i]);
				break;
			case UN_ACTION_DPCONNECT:
// dpConnect
				unProfileGraph_connectDisconnect(true, sDpName, sPVSSDpe, g_diDeviceConnectType[iIndex], dsDeviceName[i], iRes);

				g_dbCallbackConnected[iIndex] = (iRes >= 0);
//DebugTN("*************UN_ACTION_DPCONNECT", sDpName, sPVSSDpe, iIndex, g_dbCallbackConnected[iIndex], g_diDeviceConnectType[iIndex], g_dbDeviceUseAlarm[iIndex], g_dsDeviceType[iIndex]);
				break;
			case UN_ACTION_DPDISCONNECT_DISCONNECT:
//DebugTN("*************UN_ACTION_DPDISCONNECT_DISCONNECT", sDpName, sPVSSDpe, iIndex, g_dbCallbackConnected[iIndex], g_diDeviceConnectType[iIndex], g_dbDeviceUseAlarm[iIndex], g_dsDeviceType[iIndex]);
// dpDisconnect
				unProfileGraph_connectDisconnect(false, sDpName, sPVSSDpe, g_diDeviceConnectType[iIndex], dsDeviceName[i], iRes);
				
				g_dbCallbackConnected[iIndex] = !(iRes >= 0);
//DebugN("UN_ACTION_DPDISCONNECT_DISCONNECT", sDpName, sPVSSDpe, iIndex, g_dbCallbackConnected[iIndex], g_diDeviceConnectType[iIndex], g_dbDeviceUseAlarm[iIndex], g_dsDeviceType[iIndex]);

// set data
				unProfileGraph_setDisconnectionDp(dsDeviceName[i]);
				break;
			case UN_ACTION_NOTHING:
			default:
//DebugTN("UN_ACTION_NOTHING", sDpName, dsDeviceName[i], iIndex);
				break;
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_initDpe
/**
Purpose: initialise the type of dpConnect dpDisconnect call

Parameters:
	- sPVSSDpe, string, input, dpe name
	- bUseAlarm, bool, input, use PVSS alert_hdl if any
	- sDpName, string, input, dp name
	- iType, int, input, type of dpConnect/dpDisconnect
	- sDeviceType, string, output, the the type of the device is returned here

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_initDpe(string sPVSSDpe, bool bUseAlarm, int &iType, string &sDeviceType)
{
	int type=UN_PROFILE_GRAPH_NODP;
	bool bStsReg01, bStsReg02, bSelect, bAlarm=false;
	string sStsReg01, sStsReg02, sSelect;
	int iAlarm;
	string sType = UN_PROFILE_GRAPH_NO_VALUE;
	
	if(dpExists(sPVSSDpe)) {
		type=UN_PROFILE_GRAPH_DP_NO_SELECT;
		dpGet(sPVSSDpe+":_alert_hdl.._type", iAlarm);
		if(iAlarm != DPCONFIG_NONE)
			bAlarm = true;

		sStsReg01=dpSubStr(sPVSSDpe, DPSUB_SYS_DP);
		sType = dpTypeName(sStsReg01);
		sStsReg02 = sStsReg01+".ProcessInput.StsReg02";
		sSelect = sStsReg01+".statusInformation.selectedManager";

		sStsReg01+=".ProcessInput.StsReg01";
		bStsReg01 = dpExists(sStsReg01);
		bStsReg02 = dpExists(sStsReg02);
		bSelect = dpExists(sSelect);

		switch(sType) {
			case "AnaDig":
			case "Analog":
				type=UN_PROFILE_GRAPH_STSREG01;
				break;
			case "OnOff":
				type=UN_PROFILE_GRAPH_STSREG0102;
				break;
			case "Local":
				type=UN_PROFILE_GRAPH_STSREG01;
				break;
			case "Controller":
				type=UN_PROFILE_GRAPH_STSREG0102;
				break;
			case "ProcessControlObject":
				type=UN_PROFILE_GRAPH_STSREG0102;
				break;
			case "Alarm":
				if(bAlarm)
					type=UN_PROFILE_GRAPH_STSREG01_ALARM;
				else
					type=UN_PROFILE_GRAPH_STSREG01;
				break;
			case "AnalogInput":
			case "AnalogOutput":
				if(bAlarm)
					type=UN_PROFILE_GRAPH_STSREG01_ALARM;
				else
					type=UN_PROFILE_GRAPH_STSREG01;
				break;
			case "DigitalInput":
			case "DigitalOutput":
				if(bAlarm)
					type=UN_PROFILE_GRAPH_STSREG01_ALARM;
				else
					type=UN_PROFILE_GRAPH_STSREG01;
				break;
			default: // non default UNICOS device type
				if(bStsReg02 && bStsReg01 && bSelect) {
					if(bAlarm & bUseAlarm)
						type=UN_PROFILE_GRAPH_STSREG0102_ALARM;
					else
						type=UN_PROFILE_GRAPH_STSREG0102;
				}
				else if(bStsReg01 && bSelect) {
					if(bAlarm & bUseAlarm)
						type=UN_PROFILE_GRAPH_STSREG01_ALARM;
					else
						type=UN_PROFILE_GRAPH_STSREG01;
				}
				else if(bSelect) {
					if(bAlarm & bUseAlarm)
						type=UN_PROFILE_GRAPH_DP_ALARM;
					else
						type=UN_PROFILE_GRAPH_DP;
				}
				else if (bAlarm & bUseAlarm){
					type=UN_PROFILE_GRAPH_DP_NO_SELECT_ALARM;
				}
				break;
		}
	}

	iType = type;
	sDeviceType = sType;
//DebugN("unProfileGraph_initDpe", sPVSSDpe, iType, sSelect, bSelect, bAlarm, bUseAlarm);	
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_connectDisconnect
/**
Purpose: setup dpConnect dpDisconnect call

Parameters:
	- bConnect, bool, input, true dpConnect/false dpDisconnect
	- sDpName, string, input, dp name
	- sDpe, string, input, dpe name
	- iType, int, input, type of dpConnect/dpDisconnect
	- sDeviceName, string, input, device name
	- iResult, int, output, the result of dpConnect/dpDisconnect is returned here

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_connectDisconnect(bool bConnect, string sDpName, string sDpe, int iType, string sDeviceName, int &iResult)
{
	int iRes=0;
	
	switch(iType) { 
		case UN_PROFILE_GRAPH_DP_NO_SELECT:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_DP_NO_SELECT",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_DP_NO_SELECT",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime");
			}
			break;
		case UN_PROFILE_GRAPH_DP_NO_SELECT_ALARM:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_DP_NO_SELECT_ALARM",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_DP_NO_SELECT_ALARM",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible");
			}
			break;
		case UN_PROFILE_GRAPH_DP:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_DP", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_DP", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime");
			}
			break;
		case UN_PROFILE_GRAPH_DP_ALARM:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_DP_alarm", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_DP_alarm", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible");
			}
			break;
		case UN_PROFILE_GRAPH_STSREG01:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_01", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpName + ".ProcessInput.StsReg01");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_01", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpName + ".ProcessInput.StsReg01");
			}
			break;
		case UN_PROFILE_GRAPH_STSREG01_ALARM:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_01_alarm", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible",
								sDpName + ".ProcessInput.StsReg01");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_01_alarm", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible",
								sDpName + ".ProcessInput.StsReg01");
			}
			break;
		case UN_PROFILE_GRAPH_STSREG0102:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_0102", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpName + ".ProcessInput.StsReg01",
								sDpName + ".ProcessInput.StsReg02");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_0102", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpName + ".ProcessInput.StsReg01",
								sDpName + ".ProcessInput.StsReg02");
			}
			break;
		case UN_PROFILE_GRAPH_STSREG0102_ALARM:
			if(bConnect) {
				iRes = dpConnect("unProfileGraph_barAnimationCB_0102_alarm", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible",
								sDpName + ".ProcessInput.StsReg01",
								sDpName + ".ProcessInput.StsReg02");
			}
			else {
				iRes = dpDisconnect("unProfileGraph_barAnimationCB_0102_alarm", 
								sDpName + ".statusInformation.selectedManager:_lock._original._locked",
								sDpName + ".statusInformation.selectedManager",
								sDpe,
								sDpe + ":_online.._invalid",
								sDpe + ":_online.._stime",
								sDpe + ":_alert_hdl.._active",
								sDpe + ":_alert_hdl.._act_state",
								sDpe + ":_alert_hdl.._ack_possible",
								sDpName + ".ProcessInput.StsReg01",
								sDpName + ".ProcessInput.StsReg02");
			}
			break;
		case UN_PROFILE_GRAPH_NODP:
		default:
// set data
			unProfileGraph_setDisconnectionDp(sDeviceName);
			break;
	}
	iResult = iRes;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_DP_NO_SELECT
/**
Purpose: Callback function in case of non UNICOS device type 

Parameters:
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.

@Reviewed 2018-09-05 @Whitelisted{CallBack}

*/
unProfileGraph_barAnimationCB_DP_NO_SELECT(string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime
															)
{
	unProfileGraph_barAnimationCB_0102_alarm(UN_PROFILE_GRAPH_NO_VALUE, false, 
																UN_PROFILE_GRAPH_NO_VALUE, "",
																sDpeValue, fValue,
																sDpeInvalid, bInvalid,
																sDpeTime, tTime,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, 0,
																UN_PROFILE_GRAPH_NO_VALUE, 0
																);
}


//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_DP_NO_SELECT_ALARM
/**
Purpose: Callback function in case of non UNICOS device type with alarm

Parameters:
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value
	- sDpeHdlActive, string, input, the Dpe of the active state of the alert
	- bActive, bool, input, the active state of the alert: true=alert active/not masked, false=alert not active/masked
	- sDpeAlertState, string, input, the Dpe of the state of the alert
	- bAlertState, bool, input, the state of the alert: true=alert, false=no alert
	- sDpeHdlAckPossible, string, input, the Dpe of the acknowledge state of the alert
	- bAckPossible, bool, input, the state of the alert: true=alert acknowledgeable, false=alert not acknowledgeable

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.

@Reviewed 2018-09-05 @Whitelisted{CallBack}

*/
unProfileGraph_barAnimationCB_DP_NO_SELECT_ALARM(string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime,
															string sDpeHdlActive, bool bActive,
															string sDpeAlertState, bool bAlertState,
															string sDpeHdlAckPossible, bool bAckPossible
															)
{
	unProfileGraph_barAnimationCB_0102_alarm(UN_PROFILE_GRAPH_NO_VALUE, false, 
																UN_PROFILE_GRAPH_NO_VALUE, "",
																sDpeValue, fValue,
																sDpeInvalid, bInvalid,
																sDpeTime, tTime,
																sDpeHdlActive, bActive,
																sDpeAlertState, bAlertState,
																sDpeHdlAckPossible, bAckPossible,
																UN_PROFILE_GRAPH_NO_VALUE, 0,
																UN_PROFILE_GRAPH_NO_VALUE, 0
																);
}
//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_DP
/**
Purpose: Callback function in case of device without stsreg01 and stsreg02 and no alarm on the value 

Parameters:
	- sDpLock, string, input, the Dpe of the lock state
	- bLocked, bool, input, the state of the lock: true=locked/selected, false=notLocked/deselected
	- sDpManager, string, input, the Dpe of the manager that took the lock
	- sSelectedManager, string, input, contains the manager that locked the device
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
	
@Reviewed 2018-09-05 @Whitelisted{CallBack}
	
*/
unProfileGraph_barAnimationCB_DP(string sDpLock, bool bLocked, 
															string sDpManager, string sSelectedManager,
															string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime
															)
{
	unProfileGraph_barAnimationCB_0102_alarm(sDpLock, bLocked, 
																sDpManager, sSelectedManager,
																sDpeValue, fValue,
																sDpeInvalid, bInvalid,
																sDpeTime, tTime,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, 0,
																UN_PROFILE_GRAPH_NO_VALUE, 0
																);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_DP_alarm
/**
Purpose: Callback function in case of device with an alarm on the value dpe and without stsreg01 and stsreg02 

Parameters:
	- sDpLock, string, input, the Dpe of the lock state
	- bLocked, bool, input, the state of the lock: true=locked/selected, false=notLocked/deselected
	- sDpManager, string, input, the Dpe of the manager that took the lock
	- sSelectedManager, string, input, contains the manager that locked the device
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value
	- sDpeHdlActive, string, input, the Dpe of the active state of the alert
	- bActive, bool, input, the active state of the alert: true=alert active/not masked, false=alert not active/masked
	- sDpeAlertState, string, input, the Dpe of the state of the alert
	- bAlertState, bool, input, the state of the alert: true=alert, false=no alert
	- sDpeHdlAckPossible, string, input, the Dpe of the acknowledge state of the alert
	- bAckPossible, bool, input, the state of the alert: true=alert acknowledgeable, false=alert not acknowledgeable

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.

@Reviewed 2018-09-05 @Whitelisted{CallBack}

*/
unProfileGraph_barAnimationCB_DP_alarm(string sDpLock, bool bLocked, 
															string sDpManager, string sSelectedManager,
															string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime,
															string sDpeHdlActive, bool bActive,
															string sDpeAlertState, bool bAlertState,
															string sDpeHdlAckPossible, bool bAckPossible
															)
{
	unProfileGraph_barAnimationCB_0102_alarm(sDpLock, bLocked, 
																sDpManager, sSelectedManager,
																sDpeValue, fValue,
																sDpeInvalid, bInvalid,
																sDpeTime, tTime,
																sDpeHdlActive, bActive,
																sDpeAlertState, bAlertState,
																sDpeHdlAckPossible, bAckPossible,
																UN_PROFILE_GRAPH_NO_VALUE, 0,
																UN_PROFILE_GRAPH_NO_VALUE, 0
																);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_01
/**
Purpose: Callback function in case of UNICOS device without an alarm on the value dpe  and stsreg02 and with stsreg01 

Parameters:
	- sDpLock, string, input, the Dpe of the lock state
	- bLocked, bool, input, the state of the lock: true=locked/selected, false=notLocked/deselected
	- sDpManager, string, input, the Dpe of the manager that took the lock
	- sSelectedManager, string, input, contains the manager that locked the device
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value
	- sDpeStsReg01, string, input, the Dpe of the stsreg01
	- iStsReg01, bool, input, the value of the stsreg01

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.

@Reviewed 2018-09-05 @Whitelisted{CallBack}
*/
unProfileGraph_barAnimationCB_01(string sDpLock, bool bLocked, 
															string sDpManager, string sSelectedManager,
															string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime,
															string sDpeStsReg01, int iStsReg01
															)
{
	unProfileGraph_barAnimationCB_0102_alarm(sDpLock, bLocked, 
																sDpManager, sSelectedManager,
																sDpeValue, fValue,
																sDpeInvalid, bInvalid,
																sDpeTime, tTime,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																sDpeStsReg01, iStsReg01,
																UN_PROFILE_GRAPH_NO_VALUE, 0
																);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_01_alarm
/**
Purpose: Callback function in case of UNICOS device without stsreg02 and with stsreg01 and an alarm on the value dpe 

Parameters:
	- sDpLock, string, input, the Dpe of the lock state
	- bLocked, bool, input, the state of the lock: true=locked/selected, false=notLocked/deselected
	- sDpManager, string, input, the Dpe of the manager that took the lock
	- sSelectedManager, string, input, contains the manager that locked the device
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value
	- sDpeHdlActive, string, input, the Dpe of the active state of the alert
	- bActive, bool, input, the active state of the alert: true=alert active/not masked, false=alert not active/masked
	- sDpeAlertState, string, input, the Dpe of the state of the alert
	- bAlertState, bool, input, the state of the alert: true=alert, false=no alert
	- sDpeHdlAckPossible, string, input, the Dpe of the acknowledge state of the alert
	- bAckPossible, bool, input, the state of the alert: true=alert acknowledgeable, false=alert not acknowledgeable
	- sDpeStsReg01, string, input, the Dpe of the stsreg01
	- iStsReg01, bool, input, the value of the stsreg01

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.

	
@Reviewed 2018-09-05 @Whitelisted{CallBack}
*/
unProfileGraph_barAnimationCB_01_alarm(string sDpLock, bool bLocked, 
															string sDpManager, string sSelectedManager,
															string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime,
															string sDpeHdlActive, bool bActive,
															string sDpeAlertState, bool bAlertState,
															string sDpeHdlAckPossible, bool bAckPossible,
															string sDpeStsReg01, int iStsReg01
															)
{
	unProfileGraph_barAnimationCB_0102_alarm(sDpLock, bLocked, 
																sDpManager, sSelectedManager,
																sDpeValue, fValue,
																sDpeInvalid, bInvalid,
																sDpeTime, tTime,
																sDpeHdlActive, bActive,
																sDpeAlertState, bAlertState,
																sDpeHdlAckPossible, bAckPossible,
																sDpeStsReg01, iStsReg01,
																UN_PROFILE_GRAPH_NO_VALUE, 0
																);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_0102
/**
Purpose: Callback function in case of UNICOS device without an alarm on the value dpe and with stsreg01 and stsreg02 

Parameters:
	- sDpLock, string, input, the Dpe of the lock state
	- bLocked, bool, input, the state of the lock: true=locked/selected, false=notLocked/deselected
	- sDpManager, string, input, the Dpe of the manager that took the lock
	- sSelectedManager, string, input, contains the manager that locked the device
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value
	- sDpeStsReg01, string, input, the Dpe of the stsreg01
	- iStsReg01, bool, input, the value of the stsreg01
	- sDpeStsReg02, string, input, the Dpe of the stsreg02
	- iStsReg02, bool, input, the value of the stsreg02

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.

@Reviewed 2018-09-05 @Whitelisted{CallBack}

*/
unProfileGraph_barAnimationCB_0102(string sDpLock, bool bLocked, 
															string sDpManager, string sSelectedManager,
															string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime,
															string sDpeStsReg01, int iStsReg01,
															string sDpeStsReg02, int iStsReg02
															)
{
	unProfileGraph_barAnimationCB_0102_alarm(sDpLock, bLocked, 
																sDpManager, sSelectedManager,
																sDpeValue, fValue,
																sDpeInvalid, bInvalid,
																sDpeTime, tTime,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																UN_PROFILE_GRAPH_NO_VALUE, false,
																sDpeStsReg01, iStsReg01,
																sDpeStsReg02, iStsReg02
																);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_barAnimationCB_0102_alarm
/**
Purpose: Callback function in case of UNICOS device without an alarm on the value dpe and with stsreg01 and stsreg02 

Parameters:
	- sDpLock, string, input, the Dpe of the lock state
	- bLocked, bool, input, the state of the lock: true=locked/selected, false=notLocked/deselected
	- sDpManager, string, input, the Dpe of the manager that took the lock
	- sSelectedManager, string, input, contains the manager that locked the device
	- sDpeValue, string, input, the Dpe
	- fValue, float, input, the value in float format: boolean are converted to float, true=1.0, false=0.0
	- sDpeInvalid, string, input, the Dpe for invalidity
	- bInvalid, bool, input, the invalid state: true=invalid, false=valid
	- sDpeTime, string, input, the Dpe for timestamp
	- tTime, time, input, the time of the last modification of the value
	- sDpeHdlActive, string, input, the Dpe of the active state of the alert
	- bActive, bool, input, the active state of the alert: true=alert active/not masked, false=alert not active/masked
	- sDpeAlertState, string, input, the Dpe of the state of the alert
	- bAlertState, bool, input, the state of the alert: true=alert, false=no alert
	- sDpeHdlAckPossible, string, input, the Dpe of the acknowledge state of the alert
	- bAckPossible, bool, input, the state of the alert: true=alert acknowledgeable, false=alert not acknowledgeable
	- sDpeStsReg01, string, input, the Dpe of the stsreg01
	- b32StsReg01, bit32, input, the value of the stsreg01
	- sDpeStsReg02, string, input, the Dpe of the stsreg02
	- b32StsReg02, bit32, input, the value of the stsreg02

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_barAnimationCB_0102_alarm(string sDpLock, bool bLocked, 
															string sDpManager, string sSelectedManager,
															string sDpeValue, float fValue,
															string sDpeInvalid, bool bInvalid,
															string sDpeTime, time tTime,
															string sDpeHdlActive, bool bActive,
															string sDpeAlertState, bool bAlertState,
															string sDpeHdlAckPossible, bool bAckPossible,
															string sDpeStsReg01, bit32 b32StsReg01,
															string sDpeStsReg02, bit32 b32StsReg02
															)
{
	int iRes;
	string sDpe = dpSubStr(sDpeValue, DPSUB_SYS_DP_EL), sDeviceName;
	int iIndex = g_mDpToIndex[sDpe];
	string sColor;
	bool bFlashing=false;

//DebugTN("unProfileGraph_barAnimationCB_0102_alarm", sDpe, iIndex, fValue, tTime);
// keep the value and time
	g_dsDeviceValue[iIndex] = fValue;
	g_dtDeviceTimeValue[iIndex] = tTime;
	sDeviceName = g_dsDeviceName[iIndex];
	
	switch(g_dsDeviceType[iIndex]){
		case "AnaDig":
		case "Analog":
		case "OnOff":
			if(bInvalid)
				sColor = "unDataNotValid";
			else
				unProfileGraph_FieldAnimation(b32StsReg01, 
																		makeDynInt(UN_STSREG01_STARTIST, UN_STSREG01_STOPIST), 
																		makeDynString(UN_WIDGET_TEXT_ALARM_STARTIST, UN_WIDGET_TEXT_ALARM_STOPIST),
																		makeDynInt(UN_STSREG01_MMOST, UN_STSREG01_LDST, UN_STSREG01_FOMOST), 
																		makeDynString(UN_WIDGET_TEXT_CONTROL_MANUAL, UN_WIDGET_TEXT_CONTROL_LOCAL, UN_WIDGET_TEXT_CONTROL_FORCED), 
																		UN_STSREG01_ALUNACK, sColor);
			break;
		case "Local":
			if(bInvalid)
				sColor = "unDataNotValid";
			else
				unProfileGraph_FieldAnimation(b32StsReg01, 
																		makeDynInt(UN_STSREG01_POSAL), 
																		makeDynString(UN_WIDGET_TEXT_ALARM_POSAL),
																		makeDynInt(), 
																		makeDynString(), 
																		UN_STSREG01_ALUNACK, sColor);
			break;
		case "Controller":
			if(bInvalid)
				sColor = "unDataNotValid";
			else {
				unProfileGraph_FieldAnimation(b32StsReg01, 
																		makeDynInt(), 
																		makeDynString(),
																		makeDynInt(UN_STSREG01_REMOST, UN_STSREG01_TST, UN_STSREG01_MMOST, UN_STSREG01_FOMOST), 
																		makeDynString(UN_WIDGET_TEXT_CONTROL_REGULATION, UN_WIDGET_TEXT_CONTROL_TRACKING, UN_WIDGET_TEXT_CONTROL_MANUAL, UN_WIDGET_TEXT_CONTROL_FORCED), 
																		-1, sColor);
			}
			break;
		case "ProcessControlObject":
			if(bInvalid)
				sColor = "unDataNotValid";
			else {
				unProfileGraph_FieldAnimation(b32StsReg01, 
																		makeDynInt(UN_STSREG01_STARTAL, UN_STSREG01_TSTOPAL, UN_STSREG01_FUSTOPAL), 
																		makeDynString(UN_WIDGET_TEXT_ALARM_STARTAL, UN_WIDGET_TEXT_ALARM_TSTOPAL, UN_WIDGET_TEXT_ALARM_FUSTOPAL),
																		makeDynInt(UN_STSREG01_MMOST, UN_STSREG01_FOMOST), 
																		makeDynString(UN_WIDGET_TEXT_CONTROL_MANUAL, UN_WIDGET_TEXT_CONTROL_FORCED), 
																		-1, sColor);
				if(sColor != "unFaceplate_AlarmActive") {
// force mode or other
					if (getBit(b32StsReg02, UN_STSREG02_TFUSTOP) == 1)
						sColor = "unFaceplate_StatusAlarmActive";
				}
				if (getBit(b32StsReg02, UN_STSREG02_ALACTB) == 1)
					sColor = "unFaceplate_StatusAlarmActive";
				if (getBit(b32StsReg01, UN_STSREG01_ALUNACK) == 1)
					sColor = "unWidget_AlarmNotAck";
			}
			break;
		case "Alarm":
			if(bInvalid)
				sColor = "unDataNotValid";
			else {
				unProfileGraph_AlarmAnimation(b32StsReg01, sColor);
			}
			break;
		case "AnalogInput":
		case "AnalogOutput":
			if(bInvalid)
				sColor = "unDataNotValid";
			else {
				if(sDpeHdlActive == UN_PROFILE_GRAPH_NO_VALUE)
					unProfileGraph_AIAOAnimation(b32StsReg01, false, false, false, false, sColor);
				else
					unProfileGraph_AIAOAnimation(b32StsReg01, true, bActive, bAlertState, bAckPossible, sColor);
			}
			break;
		case "DigitalInput":
		case "DigitalOutput":
			if(bInvalid)
				sColor = "unDataNotValid";
			else {
				if(sDpeHdlActive == UN_PROFILE_GRAPH_NO_VALUE)
					unProfileGraph_DIDOAnimation(b32StsReg01, false, false, false, false, sColor);
				else
					unProfileGraph_DIDOAnimation(b32StsReg01, true, bActive, bAlertState, bAckPossible, sColor);
			}
			break;
		default:
			if(bInvalid)
				sColor = "unDataNotValid";
			else {
				if(sDpeHdlActive == UN_PROFILE_GRAPH_NO_VALUE)
					unProfileGraph_OtherDeviceAnimation(b32StsReg01, false, false, false, false, sColor);
				else
					unProfileGraph_OtherDeviceAnimation(b32StsReg01, true, bActive, bAlertState, bAckPossible, sColor);
			}
			break;
	}

// set the DeviceValue and the color state of the bar.
      if(g_dbSystemConnected[iIndex])
      {
	bFlashing = sColor == "unWidget_AlarmNotAck" ? true:false;
        sColor = unProfileGraph_convertColor(sColor);
	iRes = this.SetDeviceValue(sDeviceName, fValue, true, sColor, bFlashing);

// Select if needed
	if(sDpManager != UN_PROFILE_GRAPH_NO_VALUE) {
		unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, sColor, bFlashing);
                if(g_dsDeviceSelectedColorState[iIndex] != sColor) {
//DebugTN("SelectDese", sDeviceName, g_dsDeviceSelectedColorState[iIndex], sColor);
		  if(sColor == "unWidget_Select")
			this.SelectDevice(sDeviceName, false);
		  else
			this.UnselectDevice(sDeviceName);
                  g_dsDeviceSelectedColorState[iIndex] = sColor;
                }
	}
      }
//DebugN("unProfileGraph_barAnimationCB_0102_alarm", sDpeHdlActive, g_diDeviceConnectType[iIndex], g_dsDeviceType[iIndex], g_dsDeviceName[iIndex], sColor, iIndex, sDpe, bLocked, sSelectedManager, fValue, bInvalid, tTime, bActive, bAlertState, bAckPossible, b32StsReg01, b32StsReg02);

}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_convertColor
/**
Purpose: Convert color for EWO widget 

Parameters:
	- sColor, string, input, the color
	- return value the color for the EWO widget

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
string unProfileGraph_convertColor(string sColor)
{
  string sReturnColor;
  
  switch(sColor) {
    case "unWidget_Select": sReturnColor = "{255,255,255}"; break;
    case "unWidget_ControlStateAuto": sReturnColor = "{0,255,0}"; break;
    case "unWidget_ControlStateForced": sReturnColor = "{255,255,0}"; break;
    case "unWidget_ControlStateManualLocal": sReturnColor = "{255,255,255}"; break;
    case "unWidget_Background": sReturnColor = "{65,65,65}"; break;
    case "unWidget_AlternativeColor": sReturnColor = "{65,65,65}"; break;
    case "unWidget_AlarmNotAck": sReturnColor = "{255,51,0}"; break;
    case "unWidget_AlternativeColorAlarmNotAck": sReturnColor = "{65,65,65}"; break;
    case "unSynopticBackground": sReturnColor = "{65,65,65}"; break;
    case "unSynoptic_displayValue": sReturnColor = "{204,204,204}"; break;
    case "unSynoptic_staticText": sReturnColor = "{204,204,204}"; break;
    case "notUsed": sReturnColor = "{0,0,0}"; break;
    case "unRemoteNotAllowed": sReturnColor = "{222,222,222}"; break;
    case "unEventList_Warning": sReturnColor = "{255,126,0}"; break;
    case "unEventList_AlarmActive": sReturnColor = "{255,64,64}"; break;
    case "unEventList_ForcedMode": sReturnColor = "{255,255,0}"; break;
    case "unEventList_AlarmStatus": sReturnColor = "{255,255,0}"; break;
    case "unEventList_AlarmMasked": sReturnColor = "{255,255,0}"; break;
    case "unEventList_BackColor": sReturnColor = "{81,84,87}"; break;
    case "unEventList_ForeColor": sReturnColor = "{255,255,255}"; break;
    case "unDisplayValue_Parameter": sReturnColor = "{0,0,0}"; break;
    case "unDisplayValue_Status": sReturnColor = "{0,255,0}"; break;
    case "unDisplayValue_Request": sReturnColor = "{0,204,255}"; break;
    case "unDisplayValue_Background": sReturnColor = "{65,65,65}"; break;
    case "unDisplayValue_Output": sReturnColor = "{255,0,255}"; break;
    case "unDisplayValue_Setpoint": sReturnColor = "{0,204,255}"; break;
    case "unVLP_Helium": sReturnColor = "{102,204,255}"; break;
    case "unHelium_shield": sReturnColor = "{204,255,255}"; break;
    case "unHelium_aux": sReturnColor = "{51,153,255}"; break;
    case "unHelium": sReturnColor = "{102,255,255}"; break;
    case "unWater": sReturnColor = "{0,204,0}"; break;
    case "unNitrogen": sReturnColor = "{255,136,17}"; break;
    case "unArgon": sReturnColor = "{255,223,191}"; break;
    case "unOil": sReturnColor = "{223,182,55}"; break;
    case "unAir": sReturnColor = "{172,172,172}"; break;
    case "unLine_B": sReturnColor = "{0,102,255}"; break;
    case "unLine_C": sReturnColor = "{102,204,255}"; break;
    case "unLine_D": sReturnColor = "{51,255,255}"; break;
    case "unLine_E": sReturnColor = "{153,255,255}"; break;
    case "unLine_F": sReturnColor = "{153,255,255}"; break;
    case "unLine_WRL": sReturnColor = "{255,102,102}"; break;
    case "unAlarm_Ok": sReturnColor = "{0,255,0}"; break;
    case "unAlarm_AnalogHH": sReturnColor = "{255,51,0}"; break;
    case "unAlarm_AnalogH": sReturnColor = "{255,51,0}"; break;
    case "unAlarm_AnalogL": sReturnColor = "{255,51,0}"; break;
    case "unAlarm_AnalogLL": sReturnColor = "{255,51,0}"; break;
    case "unAlarm_DigitalBad": sReturnColor = "{255,51,0}"; break;
    case "unAlarmDoesNotExist": sReturnColor = "{0,0,0}"; break;
    case "unAlarmMasked": sReturnColor = "{255,255,0}"; break;
    case "unAlarmNotAck": sReturnColor = "{255,51,0}"; break;
    case "unDataNoAccess": sReturnColor = "{153,0,153}"; break;
    case "unDataNotValid": sReturnColor = "{102,255,255}"; break;
    case "unMouseHover": sReturnColor = "{204,204,204}"; break;
    case "unDataNotUpToDate": sReturnColor = "{204,204,204}"; break;
    case "pcWidgetBorderOKColor": sReturnColor = "{204,204,204}"; break;
    case "unFaceplate_Unactive": sReturnColor = "{255,255,255}"; break;
    case "unFaceplate_RequestActive": sReturnColor = "{0,0,255}"; break;
    case "unFaceplate_WarningActive": sReturnColor = "{255,193,64}"; break;
    case "unFaceplate_Opera{tionModeActive": sReturnColor = "{0,0,255}"; break;
    case "unFaceplate_AlarmActive": sReturnColor = "{255,51,0}"; break;
    case "unFaceplate_Text": sReturnColor = "{0,0,0}"; break;
    case "unFaceplate_StatusActive": sReturnColor = "{0,0,255}"; break;
    case "unFaceplate_OrderActive": sReturnColor = "{0,0,255}"; break;
    case "unFaceplate_StatusAlarmActive": sReturnColor = "{255,255,0}"; break;
    case "unFaceplate_ByOp": sReturnColor = "{0,0,255}"; break;
    case "unFaceplate_AlarmMasked": sReturnColor = "{212,208,200}"; break;
    case "unFaceplate_Disabled": sReturnColor = "{212,208,200}"; break;
    case "unFaceplate_AlarmNotAck": sReturnColor = "{255,51,0}"; break;
    case "FwTrendingCurve1": sReturnColor = "{10,0,240}"; break;
    case "FwTrendingCurve2": sReturnColor = "{255,200,50}"; break;
    case "FwTrendingCurve3": sReturnColor = "{100,200,100}"; break;
    case "FwTrendingCurve4": sReturnColor = "{255,100,180}"; break;
    case "FwTrendingCurve5": sReturnColor = "{0,200,255}"; break;
    case "FwTrendingCurve6": sReturnColor = "{0,150,150}"; break;
    case "FwTrendingCurve7": sReturnColor = "{255,0,10}"; break;
    case "FwTrendingCurve8": sReturnColor = "{130,130,255}"; break;
    case "FwTrendingFaceplateBackground": sReturnColor = "{255,255,255}"; break;
    case "FwTrendingViewBackground": sReturnColor = "{255,255,255}"; break;
    case "FwTrendingTrendBackground": sReturnColor = "{255,255,255}"; break;
    case "FwTrendingFaceplateTrendBackground": sReturnColor = "{255,255,255}"; break;
    case "FwTrendingFaceplateForeground": sReturnColor = "{65,65,65}"; break;
    case "FwTrendingTrendForeground": sReturnColor = "{65,65,65}"; break;
    case "FwTrendingViewForeground": sReturnColor = "{65,65,65}"; break;
    case "FwTrendingDataNoAccess": sReturnColor = "{153,0,153}"; break;
    case "_Window": sReturnColor = "{255,255,255}"; break;
    case "_3DFace": sReturnColor = "{212,208,200}"; break;
    case "_3DText":
    case "_WindowText":
      sReturnColor = "{0,0,0}"; break;
    default:
      sReturnColor = sColor; break;
  }
//DebugN("unProfileGraph_convertColor", sColor, sReturnColor);
  return sReturnColor;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_OtherDeviceAnimation
/**
Purpose: Animation of the color of the graph for any other devices 

Parameters:
	- b32StsReg01, bit32, input, the value of the stsreg01
	- bAlarmExists, bool, input, alarm exists: true=alert exist, false=alert does not exist
	- bActive, bool, input, the active state of the alert: true=alert active/not masked, false=alert not active/masked
	- bAlertState, bool, input, the state of the alert: true=alert, false=no alert
	- bAckPossible, bool, input, the state of the alert: true=alert acknowledgeable, false=alert not acknowledgeable
	- sColor, string, output, the color of the bar

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_OtherDeviceAnimation(bit32 b32StsReg01, bool bAlarmExists, bool bActive, bool bAlertState, bool bAckPossible, string &sColor)
{
	unProfileGraph_DIDOAnimation(b32StsReg01, bAlarmExists, bActive, bAlertState, bAckPossible, sColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_AlarmAnimation
/**
Purpose: Animation of the color of the graph for Alarm devices 

Parameters:
	- b32StsReg01, bit32, input, the value of the stsreg01
	- sColor, string, output, the color of the bar

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_AlarmAnimation(bit32 bit32StsReg01, string &sColor)
{
	string sBodyColor;
	
	sBodyColor = "unWidget_ControlStateAuto";
	if (getBit(bit32StsReg01, UN_STSREG01_POSST) == 1)
		sBodyColor = "unFaceplate_AlarmActive";
	if (getBit(bit32StsReg01, UN_STSREG01_ALUNACK) == 1)
		sBodyColor = "unWidget_AlarmNotAck";
	if (getBit(bit32StsReg01, UN_STSREG01_ALMSKST) == 1)
		sBodyColor = "unAlarmMasked";
	sColor = sBodyColor;
//DebugN("unProfileGraph_AlarmAnimation", sColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_FieldAnimation
/**
Purpose: Animation of the color of the graph for field devices: Analog, AnaDig, OnOff, Local, Controller

Parameters:
	- b32StsReg01, bit32, input, the value of the stsreg01
	- diAlarmBit, dyn_int, input, list of alarm bit of the stsreg01
	- dsAlarmText, dyn_string, input, list of corresponding text of the diAlarmBit
	- diControlBit, dyn_int, input, list of control bit of the stsreg01
	- dsControlText, dyn_string, input, list of corresponding text of the diControlBit
	- iAlarmNotAckBit, bool, input, bit position of the unacknowledge alarm bit of the stsreg01
	- sColor, string, output, the color of the bar

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_FieldAnimation(bit32 b32StsReg01, dyn_int diAlarmBit, dyn_string dsAlarmText,
															dyn_int diControlBit, dyn_string dsControlText, int iAlarmNotAckBit, string &sColor)
{
	string sAlarmLetter, sAlarmColor, sControlLetter, sControlColor;
	string sBodyColor;
	
// alarm state
	unGenericObject_WidgetAlarmTextAnimation(b32StsReg01, diAlarmBit, dsAlarmText, sAlarmLetter, sAlarmColor);
// Control state
	unGenericObject_WidgetControlStateAnimation(b32StsReg01, diControlBit, dsControlText, sControlLetter, sControlColor);

// 6. Animate body
	sBodyColor = "unWidget_ControlStateAuto";					// Auto mode (default)
	if (sControlLetter == UN_WIDGET_TEXT_CONTROL_FORCED)
	{
		sBodyColor = "unWidget_ControlStateForced";			// Forced mode
	}
	if (sAlarmLetter != "")
	{
		sBodyColor = "unFaceplate_AlarmActive";
	}
	if (getBit(b32StsReg01, iAlarmNotAckBit) == 1)
	{
		sBodyColor = "unWidget_AlarmNotAck";
	}
	sColor = sBodyColor;
//DebugN("unProfileGraph_FieldAnimation", sColor);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_AIAOAnimation
/**
Purpose: Animation of the color of the graph for AnalogInput/AnalogOutput devices 

Parameters:
	- b32StsReg01, bit32, input, the value of the stsreg01
	- bAlarmExists, bool, input, alarm exists: true=alert exist, false=alert does not exist
	- bActive, bool, input, the active state of the alert: true=alert active/not masked, false=alert not active/masked
	- bAlertState, bool, input, the state of the alert: true=alert, false=no alert
	- bAckPossible, bool, input, the state of the alert: true=alert acknowledgeable, false=alert not acknowledgeable
	- sColor, string, output, the color of the bar

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_AIAOAnimation(bit32 bit32StsReg01, bool bAlarmExists, bool bActive, bool bAlertState, bool bAckPossible, string &sColor)
{
	string sBodyColor;
	
	sBodyColor = "unWidget_ControlStateAuto";					// Auto mode (default)
	if(getBit(bit32StsReg01, UN_STSREG01_FOMOST) == 1)		// Forced mode
		sBodyColor = "unWidget_ControlStateForced";
	if(bAlarmExists) {
		if(bAckPossible)
			sBodyColor = "unWidget_AlarmNotAck";
		else {
			if(bAlertState)
				sBodyColor = "unFaceplate_AlarmActive";
		}
	}	
	sColor = sBodyColor;
//DebugN("unProfileGraph_AIAOAnimation", sColor, bAlarmExists, bActive, bAlertState, bAckPossible);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_DIDOAnimation
/**
Purpose: Animation of the color of the graph for DigitalInput/DigitalOutput devices 

Parameters:
	- b32StsReg01, bit32, input, the value of the stsreg01
	- bAlarmExists, bool, input, alarm exists: true=alert exist, false=alert does not exist
	- bActive, bool, input, the active state of the alert: true=alert active/not masked, false=alert not active/masked
	- bAlertState, bool, input, the state of the alert: true=alert, false=no alert
	- bAckPossible, bool, input, the state of the alert: true=alert acknowledgeable, false=alert not acknowledgeable
	- sColor, string, output, the color of the bar

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_DIDOAnimation(bit32 bit32StsReg01, bool bAlarmExists, bool bActive, bool bAlertState, bool bAckPossible, string &sColor)
{
	string sBodyColor;
	
	sBodyColor = "unWidget_ControlStateAuto";					// Auto mode (default)
	if(getBit(bit32StsReg01, UN_STSREG01_FOMOST) == 1)		// Forced mode
		sBodyColor = "unWidget_ControlStateForced";
	if(bAlarmExists) {
		if(bActive) {
			if(bAckPossible)
				sBodyColor = "unWidget_AlarmNotAck";
			else {
				if(bAlertState)
					sBodyColor = "unFaceplate_AlarmActive";
			}
		}
		else
			sBodyColor = "unAlarmMasked";
	}

	sColor = sBodyColor;
//DebugN("unProfileGraph_DIDOAnimation", sColor, bAlarmExists, bActive, bAlertState, bAckPossible);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_selectDevice
/**
Purpose: Select a device 

Parameters:
	- sDeviceDpName, string, input, the device dpName
	- sDpType, string, input, the device type

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_selectDevice(string sDeviceDpName, string sDpType)
{
	dyn_string exceptionInfo;
	unGenericObject_selectDevice(sDeviceDpName, sDpType, exceptionInfo);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_subscribeToData
/**
Purpose: Subscribe to a device 

Parameters:
	- sIdentifier, string, input, identifier of the data to subscribe to

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_subscribeToData(string sIdentifier)
{
//DebugN(sIdentifier, g_iNbOfDevice);
	if(sIdentifier != "") {
		g_iNbOfDevice++;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_RightClick
/**
Purpose: Function called when the rigth click event is raised 

Parameters:
	- sPVSSDPE, string, input, the device dpe name
	- sIdentifier, string, input, UNICOS identifier of the device
	- sDpType, string, input, the device type
	- iX, int, input, X coordinate of the mouse click
	- iY, int, input, Y coordinate of the mouse click
	- exceptionInfo, dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_RightClick(string sPVSSDPE, string sIdentifier, string sDpType, int iXPos, int iYPos, dyn_string &exceptionInfo)
{
	dyn_string menuList, dsFunctions, dsAccessOk, myMenu;
	string sFunction, systemName, sDeviceDpName;
	int menuAnswer, iX, iY;
	bool bState, bGrid, bLogScale, bAutoScale, bUnicos, bConnected;

// get the device dpname
	sDeviceDpName = dpSubStr(sPVSSDPE, DPSUB_SYS_DP);
	systemName = unGenericDpFunctions_getSystemName(sDeviceDpName);
	unDistributedControl_isConnected(bConnected, systemName);
	bUnicos = (sPVSSDPE!=sIdentifier) && (sDpType != "");
	
//DebugN("RRRC", sPVSSDPE, sDeviceDpName, sIdentifier, sDpType, bUnicos, bConnected);

// UNICOS devices
	if(bUnicos & bConnected) {
		unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
		if(dynlen(dsFunctions) < UN_GENERICOBJECT_FUNCTION_LENGTH)
			bUnicos = false;
		else {
// get the device menu configuration
// get the access state
			unGenericButtonFunctionsHMI_isAccessAllowedMultiple(sDeviceDpName, sDpType, dsAccessOk, exceptionInfo);
// Init
			sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_MENUCONFIG];
			if (sFunction != "")
			{
				evalScript(menuList, "dyn_string main(string device, string sType, dyn_string dsAccessOk) {" + 
									 	 "dyn_string dsMenuTemp;" +
										 "if (isFunctionDefined(\"" + sFunction + "\"))" +
										 "{" +
										 "    " + sFunction + "(device, sType, dsAccessOk, dsMenuTemp);" +
										 "}" +
										 "else " +
										 "{" +
										 "    dsMenuTemp = makeDynString();" +
										 "}" +
										 "return dsMenuTemp; }", makeDynString(), sDeviceDpName, sDpType, dsAccessOk);
			}
		}
//DebugN(deviceName, sDpType, menuList);
	}
// 7. Open menu
	menuAnswer = 0;

	bLogScale = graph.YlogScale;
	bAutoScale = graph.YautoScale;
	bGrid = graph.Ygrid;
	
	if(bUnicos && bConnected) {
		myMenu = makeDynString("PUSH_BUTTON, "+sIdentifier+", 100, 0", "SEPARATOR", "CASCADE_BUTTON, Graph, 1", "CASCADE_BUTTON, Device, 1");
	}
	else
		myMenu = makeDynString("PUSH_BUTTON, ProfileGraph, 100, 0", "SEPARATOR", "CASCADE_BUTTON, Graph, 1");

	dynAppend(myMenu, "Graph");
	dynAppend(myMenu, "PUSH_BUTTON, Export data to CSV, 70, 1");
	if(bLogScale) 
		dynAppend(myMenu, "PUSH_BUTTON, Normal scale, 71, 1");
	else 
		dynAppend(myMenu, "PUSH_BUTTON, Log scale, 71, 1");

        if(bAutoScale) {
		dynAppend(myMenu, "PUSH_BUTTON, No AutoScale, 72, 1");
	        dynAppend(myMenu, "PUSH_BUTTON, Set Scale, 74, 0");
        }
        else {
		dynAppend(myMenu, "PUSH_BUTTON, AutoScale, 72, 1");
	        dynAppend(myMenu, "PUSH_BUTTON, Set Scale, 74, 1");
        }

	if(bGrid)
		dynAppend(myMenu, "PUSH_BUTTON, Hide Y grid, 73, 1");
	else
		dynAppend(myMenu, "PUSH_BUTTON, Show Y grid, 73, 1");
        if(g_iBarBorderWidth != graph.BarBorderWidth)
          dynAppend(myMenu, "PUSH_BUTTON, Restore bar border width setting, 77, 1");
        else
          dynAppend(myMenu, "PUSH_BUTTON, Restore bar border width setting, 77, 0");
        if(graph.BarBorderWidth != 0)
          dynAppend(myMenu, "PUSH_BUTTON, No border width, 78, 1");
        else
          dynAppend(myMenu, "PUSH_BUTTON, No border width, 78, 0");
	if(bUnicos && bConnected) {
		dynAppend(myMenu, "Device");
		dynAppend(myMenu, menuList);
	}
	
//	DebugN(menuList, myMenu);
        getCursorPosition(iX, iY, true);
	popupMenuXY(myMenu, iX, iY, menuAnswer);
	
	if(bUnicos && bConnected) {
// handle the device menu action
// 8. device Actions
		sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_HANDLEMENUANSWER];
		if (sFunction != "")
		{
			evalScript(exceptionInfo, "dyn_string main(string deviceName, string sDpType, dyn_string menuList, int menuAnswer) {" + 
								 	 "dyn_string exceptionInfo;" +
									 "if (isFunctionDefined(\"" + sFunction + "\"))" +
									 "{" +
									 "    " + sFunction + "(deviceName, sDpType, menuList, menuAnswer);" +
									 "}" +
									 "else " +
									 "{" +
									 "    exceptionInfo = makeDynString();" +
									 "}" +
									 "return makeDynString(); }", makeDynString(), sDeviceDpName, sDpType, myMenu, menuAnswer);
		}
	}
	if(menuAnswer >= 70) {
		switch(menuAnswer) {
			case 70:
				unProfileGraph_exportData();
				break;
			case 71:
				if(bLogScale) 
					graph.YlogScale	= false;
				else
					graph.YlogScale	= true;
				break;
			case 72: 
				if(bAutoScale)
					graph.YautoScale = false;
				else
					graph.YautoScale = true;
				break;
			case 73: 
                                bGrid = graph.Ygrid;
                                bGrid = !bGrid;
                                graph.Ygrid = bGrid;
				break;
                         case 74:
                                _unProfileGraph_setYScale();
                                break;
                         case 77: // restore bar border width
                           graph.BarBorderWidth = g_iBarBorderWidth;
                           graph.BarHighlightWidth = g_iBarBorderWidth;
                           break;
                         case 78: // no bar border width
                           graph.BarBorderWidth = 0;
                           graph.BarHighlightWidth = 0;
                           break;
		}
	}
	
	if (dynlen(exceptionInfo) > 0)
	{
		unSendMessage_toExpertException("Right Click",exceptionInfo);
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _unProfileGraph_setYScale
/**
Purpose: Set the Y scale of the profile graph

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
_unProfileGraph_setYScale()
{
  dyn_string dsReturn;
  dyn_float dfReturn;
  float fYMin, fYMax;
  
  fYMin = graph.yMin;
  fYMax = graph.yMax;
  unGraphicalFrame_ChildPanelOnCentralReturn("vision/unicosObject/General/unProfileGraphSetY.pnl", "ProfileGraph: YMin/Ymax", makeDynString("$YMIN:"+fYMin, "$YMAX:"+fYMax), dfReturn, dsReturn);
//DebugN(dfReturn, dsReturn);
  if(dynlen(dfReturn) > 0) {
    if(dfReturn[1] == "1") {
      sscanf(dsReturn[1], "%f", fYMin);
      sscanf(dsReturn[2], "%f", fYMax);
//DebugN(dsReturn, fYMin, fYMax);
      graph.yMin(fYMin);
      graph.yMax(fYMax);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unProfileGraph_exportData
/**
Purpose: Export the data of the profile graph 

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
	. Global variables and $parameters defined in Alarm faceplate
	. PVSS version: 3.0 
	. operating system: WXP.
	. distributed system: yes.
*/
unProfileGraph_exportData()
{
	int i, len;
	string fileName, sTime, sValue;
	file ff;

	// 1. Select file
	fileSelector(fileName,getPath(DATA_REL_PATH),false,"*.xls", false);
	if (fileName != "")
	{
// check if there is a .xls if not add .xls
		if(strpos(fileName, ".xls") < 0)
			fileName+=".xls";
			
		ff = fopen(fileName, "w");
                if(ff != 0) {
  		  len = dynlen(g_dsDeviceName);
		  fprintf(ff,"device name\tPVSS DPE\tlast update time\tvalue\n");
		  for(i=1;i<=len;i++) {
//DebugN("unProfileGraph_exportData", i, g_dsDeviceName[i], g_dsDeviceDpe[i], g_dtDeviceTimeValue[i], g_dsDeviceValue[i]);
			sTime = g_dtDeviceTimeValue[i];
			sValue = g_dsDeviceValue[i];
			fprintf(ff,"%s\t%s\t%s\t%s\n", g_dsDeviceName[i], g_dsDeviceDpe[i], sTime, sValue);
		  }
		  fclose(ff);
	// 3. Display file with excel
		  if (_WIN32)
		  {
			strreplace(fileName, "/", "\\");
   		        system("CMD /c start excel.exe " + "\"" + fileName +"\"");
	    		
//	   DebugN("CMD /c start excel.exe " + "\"" + fileName +"\"");
 		  }
                  else
                    fileEditor(fileName);
              }
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------

//@}

