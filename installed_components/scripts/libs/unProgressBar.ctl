/**@name LIBRARY: unProgressBar.ctl

@author: Herve Milcent (LHC-IAS)

Creation Date: 19/04/2002

Modification History: 
        05/11/2007: Herve
                - update to PVSS 3.6: use PVSS progress bar instead of ActiveX
                
	13/09/2007: Herve
		- 2 new functions: unProgressBar_setMax, unProgressBar_showPosition
		
	27/08/2004: Herve
		- wrong function name: unProgessBar_start renamed to unProgressBar_start

version 1.0

External functions:	unProgressBar_start,
					unProgressBar_stop
								
Internal function:	_unProgessBar_handleProgressBar

Purpose:
This library contains function which starts and stops the Microsoft ProgressBar Control, version 6.0

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints: 
	. to be used with the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.


*/

//@{

//--------------------------------------------------------------------------------------------------------------------------------
// unProgressBar_start
/**
Purpose:
Starts the progress bar, it starts the _unProgessBar_handleProgressBar in a seperate thread

@param sBar: the name of the Microsoft ProgressBar Control, version 6.0 graphical element
@param return value: the thread id the the started thread

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. to be used with the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
int unProgressBar_start(string sBar)
{
	return startThread("_unProgessBar_handleProgressBar", sBar);
}

//--------------------------------------------------------------------------------------------------------------------------------
// _unProgessBar_handleProgressBar
/**
Purpose:
Handles the progress bar, it changes the value of the progress bar every 100 milli seconds

@param sBar: the name of the Microsoft ProgressBar Control, version 6.0 graphical element

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. to be used with the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{Thread}
*/
_unProgessBar_handleProgressBar(string sBar)
{
	int min, max, val;
	
	min = 0;
	getValue(sBar, "totalSteps", max);	
	val = min;
	while(true) {
		setValue(sBar, "progress", val);
		delay(0,100);
		val ++;
		if(val > max)
			val = min;		
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
// unProgressBar_stop
/**
Purpose:
Stops the progress bar function started before

@param iThreadId: the thread to stop
@param sBar: the name of the Microsoft ProgressBar Control, version 6.0 graphical element

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. to be used with the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unProgressBar_stop(int iThreadId, string sBar)
{
  shape sShape=getShape(sBar);
	stopThread(iThreadId);
  // the minimum value is set.
  // Note: reset is broken in WinCC OA 3.11 so manual reset is needed
  sShape.progress(sShape.minimum());      
}

//--------------------------------------------------------------------------------------------------------------------------------
// unProgressBar_setMax
/**
Purpose:
set the Max of a progress bar

@param sBar: string, input the name of the Microsoft ProgressBar Control, version 6.0 graphical element
@param iMax: int, input the max

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. to be used with the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unProgressBar_setMax(string sBar, int iMax)
{
	setValue(sBar, "totalSteps", iMax);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unProgressBar_setPosition
/**
Purpose:
set the position of the progress bar

@param sBar: string, input the name of the Microsoft ProgressBar Control, version 6.0 graphical element
@param iPos: int, input the max

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
	. to be used with the Microsoft ProgressBar Control, version 6.0
	. PVSS version: 2.12.1 
	. operating system: NT and W2000, but tested only under W2000.
	. distributed system: yes.
*/
unProgressBar_setPosition(string sBar, int iPos)
{
        setValue(sBar, "progress", iPos);
}

//--------------------------------------------------------------------------------------------------------------------------------

//@}

