/**
 * UNICOS
 * Copyright (C) CERN 2013 All rights reserved
 */
/**@file

// unConfigUnRcpInstance.ctl
This library contains the function to configure the UnRcpInstance.

@par Creation Date
  27/05/2011

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (EN-ICE)
*/

#uses "fwGeneral/fwException.ctl"
#uses "fwConfigurationDB/fwConfigurationDB.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "unicos_declarations.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unConfigGenericFunctions.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unRcpFunctions.ctl"
#uses "unRcpFunctions_privileges.ctl"

//@{
//------------------------------------------------------------------------------------------------------------------------
// unConfigUnRcpInstance_checkConfig
/** check the device configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
unConfigUnRcpInstance_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{ 
  if (dynlen(dsConfigs) >= UN_CONFIG_COMMON_LENGTH)
  {
    // 1. Common
    unConfigGenericFunctions_checkParameters(dsConfigs, UN_CONFIG_UNRCPINSTANCE_DPT_NAME, exceptionInfo);
    // 2. call the device check function
    unConfigGenericFunctions_checkDeviceConfig(dsConfigs, UN_CONFIG_UNRCPINSTANCE_DPT_NAME, exceptionInfo);
  }
  else
  {
    fwException_raise(exceptionInfo,"ERROR","unConfigUnRcpInstance_checkConfig: " + getCatStr("unGeneration","BADINPUTS"),"");
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unConfigUnRcpInstance_setConfig
/** set the device configuration
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param dsConfigs  input, config line as dyn_string
@param exceptionInfo output, for errors
*/
unConfigUnRcpInstance_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string sDpName;
  
  // 1. Initialization : checking + dpCreate
  unConfigGenericFunctions_checkAll(dsConfigs, UN_CONFIG_UNRCPINSTANCE_DPT_NAME, exceptionInfo);
  if (dynlen(exceptionInfo) <= 0)    // Good inputs
  {
    sDpName = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
	
	//if (!dpExists(sDpName))
		unConfigGenericFunctions_createDp(sDpName, UN_CONFIG_UNRCPINSTANCE_DPT_NAME, exceptionInfo);
		
    if (dynlen(exceptionInfo) <= 0)  // Creation ok or dp already exists
    {
      // 2. Generate device
      // 2.1. Parameters
      unConfigGenericFunctions_setCommon(dsConfigs, exceptionInfo);
      // 2.2 call the device check function
      unConfigGenericFunctions_setDeviceConfig(dsConfigs, UN_CONFIG_UNRCPINSTANCE_DPT_NAME, exceptionInfo);
    }    
  }
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_UnRcpInstance_checkConfig
/** check the UnRcpInstance device configuration for the SOFT_FE front-end
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
SOFT_FE_UnRcpInstance_checkConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  string instanceName   = dsConfigs[UN_CONFIG_COMMON_ALIAS];
  string isInitial		= dsConfigs[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPINSTANCE_INITIAL_RECIPE];
  string rcpValues  	= dsConfigs[UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPINSTANCE_VALUES_LIST];
  string pvssDpe;
    
  // Check the length of the config line
  if (dynlen(dsConfigs) != (UN_CONFIG_UNRCPINSTANCE_LENGHT + UN_CONFIG_COMMON_LENGTH)) {
	fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpInstance_checkConfig: " + getCatStr("unGeneration","BADINPUTS"),"");
  }
  
  // Check the recipe instance name
  if (!dpIsLegalName(instanceName)) {
	fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpInstance_checkConfig: Bad recipe instance name: " + instanceName,"");
  }
  


  
  // Check the format of the dpeValueList (deviceAlias.dpe|value)
  dyn_string dpeValueList = strsplit(rcpValues, ",");
  int len = dynlen(dpeValueList);
  dyn_string dsWrongRecipeValues, dsWrongDpeFormats, dsWrongDpeNames;
  for (int i=1; i<=len; i++) {
	dyn_string dpeValueSplit = strsplit(dpeValueList[i], "|");
	if (dynlen(dpeValueSplit)!=2) {
		dynAppend(dsWrongRecipeValues, dpeValueList[i]);
	}
	
	dyn_string aliasDpeSplit = strsplit(dpeValueSplit[1], ".");
	if (dynlen(aliasDpeSplit)!=2) {
		dynAppend(dsWrongDpeFormats, dpeValueSplit[1]);
	}
	
	// Check that the dpe exists
	unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(dpeValueSplit[1], pvssDpe);
	if (!dpExists(pvssDpe) || pvssDpe[strlen(pvssDpe)-1] == ".") {
		dynAppend(dsWrongDpeNames, dpeValueSplit[1]);
	}
  }
  
  if (dynlen(dsWrongRecipeValues)) {
	fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpInstance_checkConfig: The following recipe values have a wrong format: " + dsWrongRecipeValues + ".","");
  }
  
  if (dynlen(dsWrongDpeFormats)) {
	fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpInstance_checkConfig: The following recipe dpes have a wrong format: " +dsWrongDpeFormats + ".","");
  }
  
  if (dynlen(dsWrongDpeNames)) {
	fwException_raise(exceptionInfo,"ERROR","SOFT_FE_UnRcpInstance_checkConfig: The following device dpes don't exist: " + dsWrongDpeNames + ".","");
  }
  
  // Check that the 'isInitial' is a boolean value
  if (isInitial!="0" && isInitial!="1"){
	isInitial = strtolower(isInitial);
	if (isInitial!="true" && isInitial!="false"){
		fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpInstance_checkConfig: The InitialRecipe parameter must be boolean","");
		return;
	}
  }
}

//------------------------------------------------------------------------------------------------------------------------
// SOFT_FE_UnRcpInstance_setConfig
/** set the UnRcpInstance device configuration for the SOFT_FE front-end
  
@par Constraints
  None

@par Usage
  Internal

@par PVSS managers
  Ui, CTRL

@param dsConfigs input, config line as dyn_string
@param exceptionInfo output, for errors
*/
SOFT_FE_UnRcpInstance_setConfig(dyn_string dsConfigs, dyn_string &exceptionInfo)
{
  int rc, pos;
  string sPvssDpe;
  dyn_mixed recipeClassInfo;
  dyn_dyn_mixed recipeObject;
  dyn_string recipeInstances;
  string sDpName       = dsConfigs[UN_CONFIG_OBJECTGENERAL_DPNAME];
  string sClassName    = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPINSTANCE_RECIPE_CLASS];
  string sInstanceName = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_ALIAS];
  string sDescription  = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_DESCRIPTION];
  string sValues	   = dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPINSTANCE_VALUES_LIST];
  bool bInitial 	   = (bool) dsConfigs[UN_CONFIG_OBJECTGENERAL_LENGTH + UN_CONFIG_COMMON_LENGTH + UN_CONFIG_UNRCPINSTANCE_INITIAL_RECIPE];
  
  string sOperatorPrivileges, sExpertPrivileges, sAdminPrivileges;

  // Removes the recipe class name of the device alias
  pos = strpos(sInstanceName, "/");
  if (pos>=0){
    if (pos<(strlen(sInstanceName)-1)) {
      sInstanceName = substr(sInstanceName, pos+1);
    } else {
      fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpInstance_setConfig: The recipe instance name can't be empty","");	
      return;
    }
  }
  
  // Set the recipe instance data
  rc = dpSet(sDpName+".ProcessInput.ClassName", sClassName,
             sDpName+".ProcessInput.InstanceName", sInstanceName);
  
  if (rc){
    fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpInstance_setConfig: Error while setting the data of the recipe instance '" +sInstanceName+"'","");
    return;  
  }
  
  // Gets the list of recipe instances of the recipe class
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sClassName, sPvssDpe);
  rc = dpGet(sPvssDpe+"ProcessInput.Recipes",recipeInstances);
  if (rc) {
    fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpInstance_setConfig: Error while getting the data of the recipe class '" +sClassName+"'","");
    return;
  }
  
  // Adds the new recipe instance to the list of recipes that belong to the recipe class
  if (!dynContains(recipeInstances, sInstanceName)) {
    dynAppend(recipeInstances, sInstanceName);
    rc = dpSet(sPvssDpe+"ProcessInput.Recipes", recipeInstances);
    if (rc) {
      fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpInstance_setConfig: Error while setting the recipe instances of the recipe class '" +sClassName+"'","");
      return;	
    }
  }
  
  // If the recipe cache exists (JCOP) remove it, this will force to rewrite the recipe meta-data
  if (dpExists(fwConfigurationDB_RecipeCacheDpPrefix+sClassName+"/"+sInstanceName)) {
    fwConfigurationDB_deleteRecipeOfClass(sInstanceName, sClassName, exceptionInfo);
    if (dynlen(exceptionInfo)) { 
      DebugTN(exceptionInfo); 
      return; 
    }
  }
  
  // Creates the JCOP recipe instance
  fwConfigurationDB_instantiateRecipeOfClass(sInstanceName, sClassName, exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    if (dpExists(sDpName)) {
      dpDelete(sDpName);
    }
    return; 
  }

  // Gets the recipe class info
  unRecipeFunctions_getRecipeClassObject(sDpName, recipeClassInfo, exceptionInfo);
  if (dynlen(exceptionInfo))	{ 
    DebugTN(exceptionInfo); 
    return; 
  }
  
  // Checks that all the dpes of the recipe instance exist on the recipe class 
  dyn_string deviceList, dpeList, dpeValues, pvssDpList;
  dyn_string dpeValueList = strsplit(sValues, ",");
  int len = dynlen(dpeValueList);
  for (int i=1; i<=len; i++) {
    dyn_string dpeValueSplit = strsplit(dpeValueList[i], "|");
    dyn_string dpeSplit = strsplit(dpeValueSplit[1], ".");
    if (!dynContains(deviceList, dpeSplit[1])) {
      dynAppend(deviceList, dpeSplit[1]);
    }
    dynAppend(dpeList, dpeSplit[1] + ".ProcessOutput." + dpeSplit[2]);
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(dpeValueSplit[1], sPvssDpe);
    dynAppend(pvssDpList, sPvssDpe);
    dynAppend(dpeValues, dpeValueSplit[2]);
    if (!dynContains(recipeClassInfo[fwConfigurationDB_RCL_ELEMENTS], dpeList[i])) {
      fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpInstance_setConfig: The recipe class '" +sClassName+"' doesn't contain the element " + dpeList[i],"");
      return;		
    }
  }
    
  // Updates the data of the recipe instance
  fwConfigurationDB_loadRecipeFromCache(sClassName +"/" + sInstanceName, deviceList, "", recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    DebugTN(exceptionInfo); 
    return; 
  }

  // Check if the number of DPEs of the recipe instance is the same that the recipe instance values
  if (dynlen(dpeValues)!=dynlen(recipeObject[fwConfigurationDB_RO_DPE_NAME])) {
    fwException_raise(exceptionInfo, "ERROR", "SOFT_FE_UnRcpInstance_setConfig: The number of values of the recipe '" 
        +sClassName+"/" + sInstanceName + "' (" +dynlen(dpeValues) + ") is different than the number of dpes in the recipe class (" 
        + dynlen(recipeObject[fwConfigurationDB_RO_DPE_NAME]) + ").","");
    return;	
  }
  
  // Iterates through all the recipe dps and add the values in the correct order 
  len = dynlen(recipeObject[fwConfigurationDB_RO_DPE_NAME]);
  for (int i=1; i<=len; i++) {
    if ((pos=dynContains(pvssDpList, recipeObject[fwConfigurationDB_RO_DPE_NAME][i]))>0) {
      recipeObject[fwConfigurationDB_RO_VALUE][i] = dpeValues[pos];
    }
  }
  
  // Format the recipe values
  dyn_anytype formattedValues;
  unRecipeFunctions_formatValues(recipeObject[fwConfigurationDB_RO_DPE_NAME], 
								  recipeObject[fwConfigurationDB_RO_VALUE], 
								  formattedValues, exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    DebugTN(exceptionInfo); 
    return; 
  }
  
  
  recipeObject[fwConfigurationDB_RO_VALUE] = formattedValues;

  
  // Set the predefined and initial recipe flags
  recipeObject[fwConfigurationDB_RO_META_PREDEFINED] = bInitial;
  recipeObject[fwConfigurationDB_RO_META_COMMENT] = sDescription;
  
  fwConfigurationDB_saveRecipeToCache(recipeObject, "", sClassName +"/" + sInstanceName, exceptionInfo);
  
  unRecipeFunctions_updateRecipeInstancePrivileges(sDpName, sClassName);
  
  // Update the privileges in the default recipe instance
  string sACDomain, sDefaultRcpDp;
  dpGet(sDpName + ".statusInformation.accessControl.accessControlDomain", sACDomain,
        sDpName + ".statusInformation.accessControl.operator", sOperatorPrivileges,
        sDpName + ".statusInformation.accessControl.expert", sExpertPrivileges,
        sDpName + ".statusInformation.accessControl.admin", sAdminPrivileges);
  
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sClassName + "/" + UN_RCP_PREDEFINED_NAME, sDefaultRcpDp);
  dpSet(sDefaultRcpDp + "accessControl.accessControlDomain", sACDomain,
        sDefaultRcpDp + "accessControl.operator", sOperatorPrivileges,
        sDefaultRcpDp + "accessControl.expert", sExpertPrivileges,
        sDefaultRcpDp + "accessControl.admin", sAdminPrivileges);
        
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Performs additional processing for removing a recipe instance
 * @param sDevice - [IN] Datapoint name of a recipe instance
 *
 * @return
 *
 * @reviewed 2018-06-25 @whitelisted{UNICOSGenericDPFunctions}
 */
dyn_string UnRcpInstance_processDeviceToDelete(string sDevice)
{
  dyn_string dsRecipes;
  string sClassName, sInstanceName, sClassDp;
  dyn_string exceptionInfo;
     
  if (dpExists(sDevice + ".")) {
      dpGet(sDevice + ".ProcessInput.InstanceName", sInstanceName,
            sDevice + ".ProcessInput.ClassName", sClassName);
            
      fwConfigurationDB_deleteRecipeOfClass(sInstanceName, sClassName, exceptionInfo);
      if (dynlen(exceptionInfo)) {
        // The recipe instance cannot be deleted, forces its deletion
        if (dpExists(fwConfigurationDB_RecipeCacheDpPrefix + sClassName + "/" + sInstanceName + ".")) {
            dpDelete(fwConfigurationDB_RecipeCacheDpPrefix + sClassName + "/" + sInstanceName);
        }
        
        // Delete the recipe instance from the list of recipes
        sClassDp = fwConfigurationDB_RecipeClassDpPrefix + sClassName;
        if (dpExists(sClassDp + ".")) {
            dpGet(sClassDp + ".Recipes", dsRecipes);
            int iPos = dynContains(dsRecipes, sClassName + "/" + sInstanceName);
            if (iPos>0) {
              dynRemove(dsRecipes, iPos);
              dpSet(sClassDp + ".Recipes", dsRecipes);
            }
        }
      }      
  }

  return makeDynString();
}
//------------------------------------------------------------------------------------------------------------------------
//@}
