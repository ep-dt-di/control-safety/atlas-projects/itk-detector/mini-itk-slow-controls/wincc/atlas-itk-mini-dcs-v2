/**
 * UNICOS
 * Copyright (C) CERN 2014 All rights reserved
 */
/**@file

@name LIBRARY: unRecipeDbFunctions.ctl

@author: Ivan Prieto Barreiro (EN-ICE-SIC)

Creation Date: September 2014 

version 1.4.0

Modification History:
  09/09/2014: Ivan
    - v1.4.0 : Implementing DB features for recipe instance and recipe class
    
External Functions: 
  .  
  
Internal Functions:
  .
*/
#uses "unRcpFunctions_class.ctl"
#uses "unRcpFunctions_instance.ctl"
#uses "unRcpFunctions_dpe.ctl"
#uses "CtrlRDBAccess"
#uses "fwGeneral/fwException.ctl"
#uses "fwDevice/fwDevice.ctl"
#uses "fwConfigurationDB/fwConfigurationDB.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_DeviceConfiguration.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unExportTree.ctl"
#uses "unRcpFunctions.ctl"
#uses "unRcpFunctions_utilities.ctl"
#uses "unRcpConstant_declarations.ctl"

//@{

//------------------------------------------------------------------------------------------------------------------------
// Boolean flag to specify if the saving of the recipe must be cancelled for that class.
global bool g_bCancelSaveRecipeToDb;           

// Boolean flag to specify if the saving of the recipe has finished (after cancelling).                                                  
global bool g_bSaveRecipeToDbFinished;         

// List of devices currently locked by the an operation of saving recipes to DB.
global dyn_string g_dsLockedDevicesBySaveToDb;

global bool g_bCloseProgressDialog;
global dyn_int g_iProgressOperationIds;
global int g_iCurrentOperation;
global dyn_string g_dsProgressOperationNames;
global mapping g_mProgressOperationStatus;
global float g_fProgressValue;

const int PROGRESS_OPERATION_ON_PROGRESS  =  0;
const int PROGRESS_OPERATION_COMPLETED    =  1;
const int PROGRESS_OPERATION_FAILED       = -1;
const int PROGRESS_OPERATION_SKIPPED      = -2;

const int DB_SAVE_RCP_CLASS_SAVED                   =  1;
const int DB_SAVE_RCP_INSTANCE_SAVED                =  2;
const int DB_SAVE_RCP_CLASS_AND_INSTANCES_COMPLETED =  3;
const int DB_SAVE_ERROR_CANNOT_LOCK_CLASS           = -1;
const int DB_SAVE_ERROR_CANNOT_LOCK_INSTANCE        = -2;
const int DB_SAVE_ERROR_BAD_RCP_CLASS_STATE         = -3;
const int DB_SAVE_ERROR_BAD_RCP_INSTANCE_STATE      = -4;
const int DB_SAVE_ERROR_CANNOT_LOAD_INSTANCE        = -5;
const int DB_SAVE_ERROR_CANNOT_SAVE_CLASS           = -6;
const int DB_SAVE_ERROR_CANNOT_SAVE_INSTANCE        = -7;
const int DB_SAVE_ERROR_CANNOT_RESET_INSTANCE       = -8;
const int DB_SAVE_SKIP_RCP_INSTANCE                 = -9;
//------------------------------------------------------------------------------------------------------------------------
// Constants used by internal DB recipes
const string RCP_CLASS_INTERNAL_SUFFIX = ".internalClass";
const string RCP_INST_INTERNAL_SUFFIX  = ".internalInstance";

const int RCP_CLASS_INTERNAL_DESC         =  1;
const int RCP_CLASS_INTERNAL_EDITABLE     =  2;
const int RCP_CLASS_INTERNAL_ELEMENTS     =  3;
const int RCP_CLASS_INTERNAL_TYPE         =  4;
const int RCP_CLASS_INTERNAL_DEVICE_ORDER =  5;
const int RCP_CLASS_INTERNAL_ACDOMAIN     =  6;
const int RCP_CLASS_INTERNAL_ADMIN        =  7;
const int RCP_CLASS_INTERNAL_EXPERT       =  8;
const int RCP_CLASS_INTERNAL_OPERATOR     =  9;
const int RCP_CLASS_INTERNAL_DEVICE_LINK  = 10;

const int RCP_INST_INTERNAL_INITIAL       =  1;
const int RCP_INST_INTERNAL_DESC          =  2;
const int RCP_INST_INTERNAL_ACDOMAIN      =  3;
const int RCP_INST_INTERNAL_ADMIN         =  4;
const int RCP_INST_INTERNAL_EXPERT        =  5;
const int RCP_INST_INTERNAL_OPERATOR      =  6;

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Load the recipe values from the database in the recipe instance panel.
 * @param sRecipeDpName - [IN]  String containing the recipe instance DP name.
 * @param validAt       - [IN]  The recipe values are requested for the specified time in the database.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void unRecipeFunctions_displayDbValues(string sRecipeDpName, time validAt, dyn_string &exceptionInfo) {
  int iLen, iPos, iDeviceDpColumn, iValueColumn;
  dyn_string dsDpNamesFromDb, dsMissingDpNames, dsExtraDpNames, dsDeviceDps, dsCurrentValues;
  dyn_anytype daDbValues, daOrderedValues;

  unRecipeFunctions_writeInRecipeLog("Loading recipe values from database.", true);
  // Modify the recipe state
  dpSet(sRecipeDpName + ".ProcessInput.State", UN_RCP_INSTANCE_STATE_LOADING_VALUES);
  
  unRecipeDbFunctions_getRecipeInstanceDbValues(sRecipeDpName, dsDpNamesFromDb, daDbValues, validAt, exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    dpSet(sRecipeDpName + ".ProcessInput.State", UN_RCP_INSTANCE_STATE_EDIT);
    return;
  }
  
  getMultiValue("RecipeElements", "nameToColumn", "DeviceDp", iDeviceDpColumn,
                "RecipeElements", "nameToColumn", "Value", iValueColumn,
                "RecipeElements", "getColumnN", iDeviceDpColumn, dsDeviceDps,
                "RecipeElements", "getColumnN", iValueColumn, dsCurrentValues);
                
  // Check if the recipe elements are the same in the database and in the panel
  unRecipeFunctions_compareListElements(dsDeviceDps, dsDpNamesFromDb, dsMissingDpNames, dsExtraDpNames);
  
  // Show a message containing the list of missing recipe elements (if any)
  unRecipeFunctions_printDeviceList(dsMissingDpNames, "The following recipe elements are missing in the recipe stored in the ORACLE database:");
  
  // Show a message containing the list of additional recipe elements (if any)
  unRecipeFunctions_printDeviceList(dsExtraDpNames, "The following recipe elements saved in the ORACLE database are missing in the local recipe instance:");

  // The list of values must be ordered according to the recipe loaded in the panel 
  // adding the missing elements and removing the additional ones.                  
  iLen = dynlen(dsDeviceDps);
  for (int i=1; i<=iLen; i++) {
    iPos = dynContains(dsDpNamesFromDb, dsDeviceDps[i]);
    if (iPos>0) {
      // The recipe element is saved in the DB recipe, add its value
      dynAppend(daOrderedValues, daDbValues[iPos]);
    } else {
      // The recipe element is not saved in the DB recipe, add the current value
      dynAppend(daOrderedValues, dsCurrentValues[i]);
    }
  }
  
  unRecipeFunctions_editRecipeValuesAndShowDifferences(daOrderedValues, exceptionInfo);
  _unRecipeFunctions_checkRecipeValues (dsDeviceDps, daOrderedValues, TRUE, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
  } 

  dpSet(sRecipeDpName + ".ProcessInput.State", UN_RCP_INSTANCE_STATE_EDIT);
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the database values of a recipe instance.
 * @param sRcpDp     	    - [IN]  DP Names of the recipe instance.
 * @param dsDpNames         - [OUT] DP Names of the recipe elements.
 * @param daFormattedValues	- [OUT] Online formatted values corresponding to the recipe class DPEs.
 * @param validAt           - [IN]  The recipe values are requested for the specified time in the database.
 * @param exceptionInfo     - [OUT] Standard exception handling variable.
 */ 
void unRecipeDbFunctions_getRecipeInstanceDbValues(string sRcpDp, dyn_string &dsDpNames, dyn_anytype &daFormattedValues, time validAt, dyn_string &exceptionInfo) {
  string sTopDevice = "", sSystemName, sRcpName;
  dyn_string dsDeviceList;
  dyn_anytype daValues;
  dyn_dyn_mixed recipeObject;
  
  unRecipeFunctions_normalizeDp(sRcpDp);
  sRcpDp += ".";

  sRcpName    = dpGetAlias(sRcpDp);
  sSystemName = unGenericDpFunctions_getSystemName(sRcpDp);
  
  // Check if the DB tool is initialized 
  fwConfigurationDB_checkInit(exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
    return;
  }
  
  // Get the recipe instance object from the database.
  fwConfigurationDB_getRecipeFromDB(sTopDevice, dsDeviceList, fwDevice_LOGICAL, sSystemName + sRcpName, recipeObject, exceptionInfo, "", validAt);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
    return;
  }

  dsDpNames = recipeObject[fwConfigurationDB_RO_DPE_NAME];
  daValues  = recipeObject[fwConfigurationDB_RO_VALUE];
 
  // Format the recipe values (formats can be different than the ones stored in DB)
  unRecipeFunctions_formatValues(dsDpNames, daValues, daFormattedValues, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
    return;
  }

  return;
}

//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Save a recipe class and all its instances to the database.
 * The function is called from the recipe class/instance panel when the 'Save to DB ...' button is pressed.
 * @param sRecipeDp     - [IN]  The DP of the recipe instance.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */ 
void unRecipeFunctions_saveRecipeToDb(string sRecipeDp, dyn_string &exceptionInfo) {
  bool bDpIsRcpClass, bShowProgressPanel;
  string sDpTypeName, sRcpClassName, sRcpClassDp;
  dyn_float df;
  dyn_string ds, dsOperationNames;
  dyn_int diOperationIds;
  
    // If the sRcpClassDp does not end with ".", add the dot
  unRecipeFunctions_normalizeDp(sRecipeDp);    
  
  if (!_unRecipeFunctions_getRecipeClassData(sRecipeDp, sRcpClassName, sRcpClassDp, bDpIsRcpClass)) {
    unRecipeFunctions_writeInRecipeLog("Error: The datapoint type " + sDpTypeName + " cannot be saved in the database.", true, true);
    return;
  }
  
  if (!_unRecipeFunctions_isRecipeClassDefinitionValid(sRecipeDp, exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: The definition of the recipe class '" + sRcpClassName + "' is invalid: " + exceptionInfo[2], true, true);
    unRecipeFunctions_writeInRecipeLog("The recipe class and its instances will not be saved in the ORACLE database.", true, true);
    return;
  }
  
  // Show a confirmation message
  ChildPanelOnCentralModalReturn(
    "vision/unRecipe/UnRcpCommon/unRcp_SaveToDbConfirmation.pnl", "Save recipes to DB",
    makeDynString("$sTitle:Save recipes to DB ",
        "$text:The recipe class '" + sRcpClassName + "' and all its recipe instances will be \r\n" +
           "saved to the ORACLE database. Please enter a history comment:"),
    df, ds);

  if (!dynlen(df) || df[1]<=0) {
    return;
  }
     
  // If it's a recipe instance, try to save it locally.
  if (!bDpIsRcpClass) {
    if (!unRecipeFunctions_saveRecipeInstance(sRecipeDp, exceptionInfo, FALSE)) {
      unRecipeFunctions_writeInRecipeLog("The recipes cannot be saved in the ORACLE database.", true, true);
      _unRecipeFunctions_closeProgressDialog();      
      return;
    }
    dpSet(sRecipeDp + ".ProcessInput.State", "");
  }
  
  bShowProgressPanel = _unRecipeFunctions_getSaveOperationIds(makeDynString(sRcpClassDp), diOperationIds, dsOperationNames, exceptionInfo);
  if (bShowProgressPanel) {
    _unRecipeFunctions_openProgressDialog(diOperationIds, dsOperationNames);
  }
  
  _unRecipeFunctions_saveRecipeClassesToDb(makeDynString(sRcpClassDp), makeDynString(sRcpClassName), ds[1]);
  if (!bDpIsRcpClass) {
    _unRecipeFunctions_loadHistoryTableInfo(sRecipeDp);
  }
  dpSet(sRecipeDp + ".ProcessInput.State", "");
  _unRecipeFunctions_closeProgressDialog();
  recipeElementsFilter.setDiffToggleButtonState(false);

}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe class data from a datapoint (recipe class/recipe instance).
 * @param sRecipeDp     - [IN]  Datapoint element of a recipe class or recipe instance.
 * @param sRcpClassName - [OUT] Recipe class name associated to the sRecipeDp parameter.
 * @param sRcpClassDp   - [OUT] Recipe class datapoint associated to the sRecipeDp parameter.
 * @param bDpIsRcpClass - [OUT] TRUE if the sRecipeDp belongs to a recipe class, otherwise FALSE.
 * @return TRUE if the sRecipeDp datapoint is a belongs to a recipe class or a recipe instance, otherwise FALSE.
 */
bool _unRecipeFunctions_getRecipeClassData(string sRecipeDp, string &sRcpClassName, string &sRcpClassDp, bool &bDpIsRcpClass) {
  string sPcoLink, sDpTypeName;
  
  unRecipeFunctions_normalizeDp(sRecipeDp);
  sRecipeDp += ".";
  sDpTypeName = dpTypeName(sRecipeDp);
  if (sDpTypeName == UN_CONFIG_UNRCPCLASS_DPT_NAME) {
    // The sRecipeDp is a recipe class
    bDpIsRcpClass = TRUE;
    sRcpClassDp = sRecipeDp;
    sRcpClassName = dpGetAlias(sRecipeDp);
    return TRUE;
  } else if (sDpTypeName == UN_CONFIG_UNRCPINSTANCE_DPT_NAME) {
    // The sRecipeDp is a recipe instance
    bDpIsRcpClass = FALSE;
    _unRecipeFunctions_getRecipeClassDataFromInstance(sRecipeDp, sRcpClassName, sRcpClassDp, sPcoLink);
    return TRUE;
  } 
  
  return FALSE;
}
//--------------------------------------------------------------------------------------------------------------------------
/**
 * Get the operation IDs and names for the progress dialog when a recipe class (and all its instances) will be saved.
 * @param dsRcpClassDps    - [IN]  DPEs of the recipe classes to save in the database.
 * @param diOperationIds   - [OUT] List of operation IDs for the progress dialog.
 * @param dsOperationNames - [OUT] List of operation names for the progress dialog.
 * @param exceptionInfo    - [OUT] Standard exception handling variable. 
 * @return TRUE if the operation IDs and names were calculated successfully, otherwise FALSE.
 */
bool _unRecipeFunctions_getSaveOperationIds(dyn_string dsRcpClassDps, dyn_int &diOperationIds, dyn_string &dsOperationNames, dyn_string &exceptionInfo) {
  dyn_string dsRcpInstanceNames, dsRcpInstanceDps;
  dynClear(diOperationIds);
  dynClear(dsOperationNames);
  
  int iLen = dynlen(dsRcpClassDps);
  for (int i=1; i<=iLen; i++) {
    string sRcpClassDp = dsRcpClassDps[i];
    unRecipeFunctions_normalizeDp(sRcpClassDp);
    sRcpClassDp += ".";
    string sDpTypeName=dpTypeName(sRcpClassDp);
    if (sDpTypeName != UN_CONFIG_UNRCPCLASS_DPT_NAME)  {
      fwException_raise(exceptionInfo, "ERROR", "The DPs received in the parameter dsRcpClassDps " 
        + "of the function _unRecipeFunctions_getSaveOperationIds must be of type " + UN_CONFIG_UNRCPCLASS_DPT_NAME, "");
      return FALSE;
    }

    // Add the recipe class operation ID and name
    string sRcpClassName = dpGetAlias(sRcpClassDp);
    dynAppend(diOperationIds, fwConfigurationDB_OPER_SaveRecipeToDB);
    dynAppend(dsOperationNames, "Save recipe class '" + sRcpClassName + "' to the ORACLE database."); 
      
    // Add the operation ID and name for the recipe instances.
    _unRecipeFunctions_getRecipeInstancesOfClass(sRcpClassName, dsRcpInstanceNames, dsRcpInstanceDps);
    int jLen = dynlen(dsRcpInstanceNames);
    for (int j=1; j<=jLen; j++) {
      string sRecipeName = sRcpClassName + "/" + dsRcpInstanceNames[j];
      dynAppend(diOperationIds, fwConfigurationDB_OPER_SaveRecipeToDB);
      dynAppend(dsOperationNames, "Save recipe '" + sRecipeName + "' to the ORACLE database."); 
    }
  }
  
  return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------------
/**
 * Save a list of recipe classes to the ORACLE database.
 * @param dsRcpClassDps   - [IN]  Recipe class DP names.
 * @param dsRcpClassNames - [IN]  Recipe class names.
 * @param sComment        - [IN]  Comment for the DB history.
 */
void _unRecipeFunctions_saveRecipeClassesToDb(dyn_string dsRcpClassDps, dyn_string dsRcpClassNames, string sComment) {
  int iLen;
  dyn_string dsRcpDbList, exceptionInfo;
    
  // Check if the DB tool is initialized 
  fwConfigurationDB_checkInit(exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
    return;
  }
  
  // Get the list of recipes saved in the ORACLE database
  fwConfigurationDB_getRecipesInDB(dsRcpDbList, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error while getting the list of recipe instances in ORACLE database:" 
          + exceptionInfo[2], true, true);
    unRecipeFunctions_writeInRecipeLog("The recipes cannot be saved in the ORACLE database.", true, true);
    dynClear(exceptionInfo);
    return;
  }
 
  iLen = dynlen(dsRcpClassDps);
  for (int i=1; i<=iLen; i++) {
    _unRecipeFunctions_saveRecipeClassAndInstancesToDb(dsRcpClassDps[i], dsRcpClassNames[i], sComment, dsRcpDbList);
    dynClear(exceptionInfo);
  }
}
//-------------------------------------------------------------------------------------------------------------------------- 
/** 
 * Trigger the cancel of a save to db operation.
 */
void unRecipeFunctions_cancelSaveRecipeToDb() synchronized(g_bCancelSaveRecipeToDb) {
  g_bCancelSaveRecipeToDb   = TRUE;

  unRecipeFunctions_writeInRecipeLog("Cancelling the save of recipes to the Oracle database. Please wait ...", true, true);
  do {
    delay(0,100);
  } while (!g_bSaveRecipeToDbFinished);
  
  g_bCancelSaveRecipeToDb   = FALSE;
}
//-------------------------------------------------------------------------------------------------------------------------- 
/** 
 * Remove a device from the list of locked devices by a save to db operation.
 * @param sDeviceDp - [IN] The DP name of the device.
 */
void _unRecipeFunctions_removeLockedDeviceBySaveToDb(string sDeviceDp)  {
  int iPos = dynContains(g_dsLockedDevicesBySaveToDb, sDeviceDp);
  if (iPos <=0 ) {
    return;
  }

  dynRemove(g_dsLockedDevicesBySaveToDb, iPos);
}
//-------------------------------------------------------------------------------------------------------------------------- 
/** 
 * Unlock the devices currently locked by a save to db operation.
 */
void unRecipeFunctions_unlockDevicesLockedBySaveToDb() {
  dyn_string exceptionInfo;
  
  if (dynlen(g_dsLockedDevicesBySaveToDb)) {
    unRecipeFunctions_dpMultiSet(g_dsLockedDevicesBySaveToDb, ":_lock._original._locked", 0, exceptionInfo);
    unRecipeFunctions_dpMultiSet(g_dsLockedDevicesBySaveToDb, "ProcessInput.State", "", exceptionInfo);
  }
}
//-------------------------------------------------------------------------------------------------------------------------- 
/**
 * Save a recipe class and all its instances to the ORACLE database.
 * @param sRcpClassDp   - [IN]  Recipe class DP name.
 * @param sRcpClassName - [IN]  Recipe class name.
 * @param sComment      - [IN]  Comment for the DB history.
 * @param dsRcpDbList   - [IN]  List of recipes saved in the ORACLE database.
 */
void _unRecipeFunctions_saveRecipeClassAndInstancesToDb(string sRcpClassDp, string sRcpClassName, string sComment, dyn_string dsRcpDbList) {
  bool bSaveClassOk;
  string sRcpClassState;
  dyn_string dsRcpInstanceNames, dsRcpInstanceDps, exceptionInfo;

  unRecipeFunctions_normalizeDp(sRcpClassDp);
  sRcpClassDp += ".";
  
  g_bSaveRecipeToDbFinished = FALSE;
  bSaveClassOk = _unRecipeFunctions_saveRecipeClassToDb(sRcpClassDp, sRcpClassName, dsRcpDbList);
  _unRecipeFunctions_getRecipeInstancesOfClass(sRcpClassName, dsRcpInstanceNames, dsRcpInstanceDps);

  if (!bSaveClassOk) {
    _unRecipeFunctions_skipSaveRecipeInstancesToDb(sRcpClassName, dsRcpInstanceNames, dsRcpInstanceDps);
    return;
  }
  
  // Save all the recipe instances to the database  
  int iLen = dynlen(dsRcpInstanceDps);
  for (int i=1; i<=iLen && !g_bCancelSaveRecipeToDb; i++) {
    string sRcpDp = dsRcpInstanceDps[i] + ".";
    _unRecipeFunctions_saveRecipeInstanceToDb(sRcpDp, sRcpClassName, dsRcpInstanceNames[i], sComment, dsRcpDbList);
  }
  
  g_bSaveRecipeToDbFinished = TRUE;
  _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_RCP_CLASS_AND_INSTANCES_COMPLETED, sRcpClassName, sRcpClassDp, sRcpClassState, exceptionInfo);
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Skip saving the recipe instances after a failure saving the recipe class.
 * @param sClassName 	  - [IN] Recipe class name.
 * @param dsInstanceNames - [IN] Recipe instance names.
 * @param dsInstanceDps   - [IN] Recipe instance datapoint names.
 */
void _unRecipeFunctions_skipSaveRecipeInstancesToDb(string sClassName, dyn_string dsInstanceNames, dyn_string dsInstanceDps) {
  dyn_string exceptionInfo;
  int iLen = dynlen(dsInstanceNames);
  
  for (int i=1; i<=iLen; i++) {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_SKIP_RCP_INSTANCE, sClassName + "/" + dsInstanceNames[i], dsInstanceDps[i], "", exceptionInfo);
  }
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Internal function to save a recipe class to the ORACLE database.
 * @param sRcpClassDp   - [IN] Datapoint name of the recipe class.
 * @param sRcpClassName - [IN] Recipe class name.
 * @param dsRcpDbList   - [IN] List of recipes saved in the database.
 * @return TRUE if the recipe class was saved successfully, otherwise FALSE.
 */
bool _unRecipeFunctions_saveRecipeClassToDb(string sRcpClassDp, string sRcpClassName, dyn_string dsRcpDbList) {
  string sRcpClassState, sOperationName;
  dyn_string dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo;
  
  sOperationName = _unRecipeFunctions_getCurrentOperationId();
  _unRecipeFunctions_setOperationStatus(PROGRESS_OPERATION_ON_PROGRESS, sOperationName);
  
  // Try to lock the recipe class 
  if (!_unRecipeFunctions_lockDevices(makeDynString(sRcpClassDp), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo)) {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_CANNOT_LOCK_CLASS, sRcpClassName, sRcpClassDp, "", exceptionInfo);
    return FALSE;
  }
  
  dynAppend(g_dsLockedDevicesBySaveToDb, sRcpClassDp);  
  dpGet(sRcpClassDp + "ProcessInput.State", sRcpClassState);
  if (sRcpClassState != "") {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_BAD_RCP_CLASS_STATE, sRcpClassName, sRcpClassDp, sRcpClassState, exceptionInfo);
    return FALSE;
  }
  
  dpSet(sRcpClassDp + "ProcessInput.State", UN_RCP_SAVE_TO_DB_STATE);
  
  // Save the recipe class to the database
  unRecipeFunctions_writeInRecipeLog("Saving recipe class '" + sRcpClassName + "' to the ORACLE database.", true, true);
  _unRecipeFunctions_saveRecipeClassInternalToDb(sRcpClassDp, sRcpClassName, dsRcpDbList, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_CANNOT_SAVE_CLASS, sRcpClassName, sRcpClassDp, sRcpClassState, exceptionInfo);
    return FALSE;
  }
  
  _unRecipeFunctions_setOperationStatus(DB_SAVE_RCP_CLASS_SAVED, sOperationName);
  unRecipeFunctions_writeInRecipeLog("The recipe class '" + sRcpClassName + "' was saved successfully to the ORACLE database.", true, true);
  return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Internal function to save a recipe instance to the ORACLE database.
 * @param sRcpDp        - [IN] Datapoint name of the recipe instance.
 * @param sRcpClassName - [IN] Recipe class name.
 * @param sRcpInstName  - [IN] Recipe instance name.
 * @param sComment      - [IN] Comment for the DB history.
 * @param dsRcpDbList   - [IN] List of recipes already saved in the ORACLE database.
 * @return TRUE if the recipe instance was saved successfully in the ORACLE database, otherwise FALSE.
 */
bool _unRecipeFunctions_saveRecipeInstanceToDb(string sRcpDp, string sRcpClassName, string sRcpInstName, string sComment, dyn_string dsRcpDbList) {
  dyn_string dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo;
  dyn_dyn_mixed recipeObject;
  string sRcpName = sRcpClassName + "/" + sRcpInstName;
  string sRcpState;
  string sOperationName = _unRecipeFunctions_getCurrentOperationId();

  _unRecipeFunctions_setOperationStatus(PROGRESS_OPERATION_ON_PROGRESS, sOperationName);
    
  // Try to lock the recipe instance
  if (!_unRecipeFunctions_lockDevices(makeDynString(sRcpDp), dsLockedDeviceDp, dsLockedDeviceAlias, exceptionInfo)) {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_CANNOT_LOCK_INSTANCE, sRcpName, sRcpDp, "", exceptionInfo);
    return FALSE;
  }
    
  dynAppend(g_dsLockedDevicesBySaveToDb, sRcpDp);
  dpGet(sRcpDp + "ProcessInput.State", sRcpState);
  if (sRcpState!="" && sRcpState!=UN_RCP_SAVE_TO_DB_STATE) {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_BAD_RCP_INSTANCE_STATE, sRcpName, sRcpDp, sRcpState, exceptionInfo);
    return FALSE;
  }
    
  dpSet(sRcpDp + "ProcessInput.State", UN_RCP_SAVE_TO_DB_STATE);
  _unRecipeFunctions_getRecipeObjectFromDp(sRcpDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_CANNOT_LOAD_INSTANCE, sRcpName, sRcpDp, sRcpState, exceptionInfo);
	return FALSE;
  }
    
  // If the recipe instance already exists in the DB, reset the recipe
  if (dynContains(dsRcpDbList, getSystemName() + sRcpName)) {
    fwConfigurationDB_resetRecipeInDB(getSystemName() + sRcpName, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_CANNOT_RESET_INSTANCE, sRcpName, sRcpDp, sRcpState, exceptionInfo);
      return FALSE;
    }
  }
      
  if (!_unRecipeFunctions_saveRecipeInstanceInternalToDb(sRcpDp, sRcpName, dsRcpDbList, exceptionInfo) ||
      !_unRecipeFunctions_saveRecipeObjectToDb(recipeObject, sComment, exceptionInfo)) 
  {
    _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_ERROR_CANNOT_SAVE_INSTANCE, sRcpName, sRcpDp, sRcpState, exceptionInfo);
    return FALSE;
  }

  _unRecipeFunctions_finalizeSaveRecipeToDb(DB_SAVE_RCP_INSTANCE_SAVED, sRcpName, sRcpDp, sRcpState, exceptionInfo);
  return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Create an internal recipe from the recipe class. This recipe contains required DPEs from the recipe class definition.
 * @param sClassDp      - [IN]  Datapoint name of the recipe class.
 * @param sClassName    - [IN]  Recipe class name.
 * @param dsRcpDbList   - [IN]  List of recipes available in the Oracle database.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return TRUE if the recipe class internal was saved successfully in the Oracle database, otherwise FALSE.
 */
bool _unRecipeFunctions_saveRecipeClassInternalToDb(string sClassDp, string sClassName, dyn_string dsRcpDbList, dyn_string &exceptionInfo) {
  string sInternalRcpName, sDomainNature, sSystemName, sJcopDpName;
  dyn_string dsDeviceElements;
  
  sSystemName = unGenericDpFunctions_getSystemName(sClassDp);
  sJcopDpName = sSystemName + fwConfigurationDB_RecipeClassDpPrefix + sClassName;
  dynAppend(dsDeviceElements, sJcopDpName + ".Description");
  dynAppend(dsDeviceElements, sJcopDpName + ".Editable");
  dynAppend(dsDeviceElements, sJcopDpName + ".Elements");
  dynAppend(dsDeviceElements, sJcopDpName + ".RecipeType");
  dynAppend(dsDeviceElements, sClassDp + "ProcessInput.DeviceOrder");
  dynAppend(dsDeviceElements, sClassDp + "statusInformation.accessControl.accessControlDomain");
  dynAppend(dsDeviceElements, sClassDp + "statusInformation.accessControl.admin");  
  dynAppend(dsDeviceElements, sClassDp + "statusInformation.accessControl.expert");
  dynAppend(dsDeviceElements, sClassDp + "statusInformation.accessControl.operator");  
  dynAppend(dsDeviceElements, sClassDp + "statusInformation.deviceLink");

  sInternalRcpName = sClassName + RCP_CLASS_INTERNAL_SUFFIX;
  sDomainNature = (string) dpGetDescription(sClassDp + "ProcessInput");
  return _unRecipeFunctions_saveInternalRecipeToDb(sSystemName + sInternalRcpName, sDomainNature, dsDeviceElements, dsRcpDbList, exceptionInfo);
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Create an internal recipe from the recipe instance. This recipe contains required DPEs from the recipe instance definition.
 * @param sRcpDp        - [IN]  Datapoint name of the recipe instance.
 * @param sRcpName      - [IN]  Recipe instance name.
 * @param dsRcpDbList   - [IN]  List of recipes available in the Oracle database.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return TRUE if the recipe instance internal was saved successfully in the Oracle database, otherwise FALSE.
 */
bool _unRecipeFunctions_saveRecipeInstanceInternalToDb(string sRcpDp, string sRcpName, dyn_string dsRcpDbList, dyn_string &exceptionInfo) {
  string sSystemName, sJcopDpName, sInternalRcpName, sDomainNature;
  dyn_string dsDeviceElements;

  sSystemName = unGenericDpFunctions_getSystemName(sRcpDp);
  sJcopDpName = sSystemName + fwConfigurationDB_RecipeCacheDpPrefix + sRcpName;
  dynAppend(dsDeviceElements, sJcopDpName + ".MetaInfo.Predefined");
  dynAppend(dsDeviceElements, sJcopDpName + ".RecipeComment");
  dynAppend(dsDeviceElements, sRcpDp + "statusInformation.accessControl.accessControlDomain");
  dynAppend(dsDeviceElements, sRcpDp + "statusInformation.accessControl.admin");  
  dynAppend(dsDeviceElements, sRcpDp + "statusInformation.accessControl.expert");
  dynAppend(dsDeviceElements, sRcpDp + "statusInformation.accessControl.operator");  
    
  sInternalRcpName = sRcpName + RCP_INST_INTERNAL_SUFFIX;
  sDomainNature = (string) dpGetDescription(sRcpDp + "ProcessInput");
  return _unRecipeFunctions_saveInternalRecipeToDb(sSystemName + sInternalRcpName, sDomainNature, dsDeviceElements, dsRcpDbList, exceptionInfo);
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Creates and saves an internal recipe to the Oracle database.
 * @param sInternalRcpName - [IN]  Name of the internal recipe.
 * @param sDomainNature    - [IN]  Domain and nature of the recipe.
 * @param dsDeviceElements - [IN]  List of devices to include in the recipe.
 * @param dsRcpDbList      - [IN]  List of recipes available in the Oracle database.
 * @param exceptionInfo    - [OUT] Standard exception handling variable.
 * @return TRUE if the internal recipe was saved successfully in the Oracle database, otherwise FALSE.
 */
bool _unRecipeFunctions_saveInternalRecipeToDb(string sInternalRcpName, string sDomainNature, dyn_string dsDeviceElements, 
        dyn_string dsRcpDbList, dyn_string &exceptionInfo) 
{
  dyn_dyn_mixed recipeObject;

  // Create the internal recipe
  fwConfigurationDB_makeRecipe(dsDeviceElements, makeDynString(), recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return false;
  }
  recipeObject[fwConfigurationDB_RO_META_COMMENT] = sDomainNature;  
  if (dynContains(dsRcpDbList, sInternalRcpName)) {
    fwConfigurationDB_resetRecipeInDB(sInternalRcpName, exceptionInfo);    
    if (dynlen(exceptionInfo)) {
      return false;
    }
  }

  // TODO: Temporary fix!!! The JCOP function should be called but it fails when the internal DPEs of the recipe are modified.
  _unRecipeFunctions_saveRecipeToDb(recipeObject, fwDevice_HARDWARE, sInternalRcpName, exceptionInfo, "", TRUE, TRUE);
  //fwConfigurationDB_saveRecipeToDB(recipeObject, fwDevice_HARDWARE, sInternalRcpName, exceptionInfo, "", TRUE);
  if (dynlen(exceptionInfo)) {
    return false;
  } 

  return true;  
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * TODO: This function is used to replace fwConfigurationDB_saveRecipeToDB and be able to update the devices in the call to _fwConfigurationDB_saveItemsToDB
 */
int _unRecipeFunctions_saveRecipeToDb(dyn_dyn_mixed recipeObject, string hierarchyType, string recipeName, dyn_string &exceptionInfo, 
        string versionDescription="",  bool autoSaveDevices=FALSE, bool updateDevices=FALSE) {
	
    if (autoSaveDevices) {
      dyn_string hwDevList=recipeObject[fwConfigurationDB_RO_DP_NAME];
    
      _fwConfigurationDB_saveItemsToDB(hwDevList, hierarchyType, exceptionInfo, updateDevices);
	  if (dynlen(exceptionInfo)) {
        return -1;
      }
    }
    
	fwDbCommitTransaction(g_fwConfigurationDB_DBConnection);

    fwConfigurationDB_storeRecipeInDB (recipeObject, hierarchyType, versionDescription, exceptionInfo, recipeName);
	return 0;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Create a recipe class in the system from an internal recipe class saved in the Oracle database.
 * @param sSystemName   - [IN]  System name.
 * @param sClassName    - [IN]  Name of the recipe class to create.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void _unRecipeFunctions_createRecipeClassFromDbInternal(string sSystemName, string sClassName, dyn_string &exceptionInfo) {
  dyn_string dsRecipeElements, dsDeviceOrder;
  dyn_dyn_mixed recipeObject;
  string sRecipeElements, sDescription, sPcoLink, sRcpClassDp, sOperatorPrivileges, sExpertPrivileges, sAdminPrivileges, 
         sDeviceOrder, sNature, sAcDomain;
  string sRcpClassInternalName = sSystemName + sClassName + RCP_CLASS_INTERNAL_SUFFIX;
  
  // Check if the recipe is defined in the local system
  if (sSystemName != getSystemName()) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe class '" + sClassName + "' defined in the system '" + sSystemName 
        + "' can't be loaded from the system " + getSystemName(), "");
    return;
  }
  
  fwConfigurationDB_loadRecipeFromDB(sRcpClassInternalName, makeDynString(), fwDevice_HARDWARE, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }
   
  // Process the device elements to the right format
  sDeviceOrder = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_DEVICE_ORDER];
  if (strlen(sDeviceOrder) > 0) {
    dsDeviceOrder = strsplit(sDeviceOrder, "|");
  }
  sRecipeElements = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_ELEMENTS];
  dsRecipeElements = strsplit(sRecipeElements, "|");
  
  int iLen = dynlen(dsRecipeElements);
  for (int i=1; i<=iLen; i++) {
    dyn_string dsElementSplit = strsplit(dsRecipeElements[i], ".");
    dsRecipeElements[i] = dsElementSplit[1] + "." + dsElementSplit[dynlen(dsElementSplit)];
  }
 
  sDescription        = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_DESC];
  sPcoLink            = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_DEVICE_LINK];
  sAcDomain           = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_ACDOMAIN];
  sOperatorPrivileges = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_OPERATOR];
  sExpertPrivileges   = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_EXPERT];
  sAdminPrivileges    = recipeObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_ADMIN];
  sNature             = recipeObject[fwConfigurationDB_RO_META_COMMENT]; 
  
  // Create the recipe class
  _unRecipeFunctions_createRecipeClass(sClassName, sDescription, sPcoLink, sRcpClassDp, exceptionInfo, dsRecipeElements,
            sAcDomain, sOperatorPrivileges, sExpertPrivileges, sAdminPrivileges);
            
  if (dynlen(exceptionInfo)) {
    return;
  }
  
  if (dynlen(dsDeviceOrder)) {
    dpSet(sRcpClassDp + ".ProcessInput.DeviceOrder", dsDeviceOrder);
  }

  // Set the ProcessInput description (contains the domain and nature)
  if (sNature!="") {
    dpSetDescription(sRcpClassDp + ".ProcessInput", sNature);
  }
}
//--------------------------------------------------------------------------------------------------------------------------
void _unRecipeDbFunctions_getRecipeClassObjectFromDbInternal(string sRcpClassDp, dyn_mixed &recipeClassObject, dyn_string &exceptionInfo) {
  dyn_string dsRecipeElements;
  dyn_dyn_mixed recipeDbObject;
  string sSystemName, sClassName, sRcpClassInternalName;
  
  sSystemName = unGenericDpFunctions_getSystemName(sRcpClassDp);
  dpGet(sRcpClassDp + ".ProcessInput.ClassName", sClassName);
  sRcpClassInternalName = sSystemName + sClassName + RCP_CLASS_INTERNAL_SUFFIX;
  
  // Check if the recipe is defined in the local system
  if (sSystemName != getSystemName()) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe class '" + sClassName + "' defined in the system '" + sSystemName 
        + "' can't be loaded from the system " + getSystemName(), "");
    return;
  }
  
  // Check if the DB tool is initialized 
  fwConfigurationDB_checkInit(exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
    return;
  }  
  
  fwConfigurationDB_loadRecipeFromDB(sRcpClassInternalName, makeDynString(), fwDevice_HARDWARE, recipeDbObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }
  
  dsRecipeElements = strsplit(recipeDbObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_ELEMENTS], "|");
  //DebugTN("Making recipe with devices: ", dsRecipeElements);
  //fwConfigurationDB_makeRecipe(dsRecipeElements, makeDynMixed(), recipeClassObject, exceptionInfo, FALSE);
  //DebugTN("Recipe Class: ", recipeClassObject);
  
  recipeClassObject[fwConfigurationDB_RCL_CLASSNAME]   = sClassName;
  recipeClassObject[fwConfigurationDB_RCL_RECIPETYPE]  = recipeDbObject[fwConfigurationDB_RO_META_RECIPETYPE];
  recipeClassObject[fwConfigurationDB_RCL_DESCRIPTION] = recipeDbObject[fwConfigurationDB_RO_VALUE][RCP_CLASS_INTERNAL_DESC];
  recipeClassObject[fwConfigurationDB_RCL_ELEMENTS]    = dsRecipeElements;
  
}

//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Create a recipe instance in the system from an internal recipe instance saved in the Oracle database.
 * @param sSystemName   - [IN]  System name.
 * @param sClassName    - [IN]  Name of the recipe class.
 * @param sInstName     - [IN]  Name of the recipe instance.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void _unRecipeFunctions_createRecipeInstanceFromDbInternal(string sSystemName, string sClassName, string sInstName, dyn_string &exceptionInfo) {
  dyn_dyn_mixed recipeObject;
  string sDescription, sRcpDp, sOperatorPrivileges, sExpertPrivileges, sAdminPrivileges, sNature, sAcDomain, sInitial;
  string sRcpInternalName = sSystemName + sClassName + "/" + sInstName + RCP_INST_INTERNAL_SUFFIX;
  
  unRecipeFunctions_writeInRecipeLog("Creating recipe instance '" + sClassName + "/" + sInstName + "'.", TRUE, TRUE);
  fwConfigurationDB_loadRecipeFromDB(sRcpInternalName, makeDynString(), fwDevice_HARDWARE, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }
 
  sDescription        = recipeObject[fwConfigurationDB_RO_VALUE][RCP_INST_INTERNAL_DESC];
  sInitial            = recipeObject[fwConfigurationDB_RO_VALUE][RCP_INST_INTERNAL_INITIAL];
  sAcDomain           = recipeObject[fwConfigurationDB_RO_VALUE][RCP_INST_INTERNAL_ACDOMAIN];
  sOperatorPrivileges = recipeObject[fwConfigurationDB_RO_VALUE][RCP_INST_INTERNAL_OPERATOR];
  sExpertPrivileges   = recipeObject[fwConfigurationDB_RO_VALUE][RCP_INST_INTERNAL_EXPERT];
  sAdminPrivileges    = recipeObject[fwConfigurationDB_RO_VALUE][RCP_INST_INTERNAL_ADMIN];
  sNature             = recipeObject[fwConfigurationDB_RO_META_COMMENT]; 
  
  if (sInitial=="TRUE" && _unRecipeFunctions_hasInitialRecipe(sClassName)) {
    sInitial = "FALSE";
    unRecipeFunctions_writeInRecipeLog("Warning: The recipe class '" + sClassName + "' already has an initial recipe.", true, true);
    unRecipeFunctions_writeInRecipeLog("The recipe instance '" + sClassName + "/" + sInstName + "' will not be created as initial recipe.", true, true);    
  }
  
  // Create the recipe class
  _unRecipeFunctions_createRecipeInstance(sClassName, sInstName, sDescription, 
        exceptionInfo, sInitial, "", sAcDomain, sOperatorPrivileges, 
        sExpertPrivileges, sAdminPrivileges);
  
  if (dynlen(exceptionInfo)) {
    return;
  }
  
  // Set the ProcessInput description (contains the domain and nature)
  if (sNature!="") {
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sClassName + "/" + sInstName, sRcpDp);
    dpSetDescription(sRcpDp + "ProcessInput", sNature);
  }
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Load the instances of a recipe class from the ORACLE database.
 * @param sRcpClassDp - [IN] Datapoint name of the recipe class.
 * @param tValidAt    - [IN] Time when the recipe was valid.
 */
void unRecipeFunctions_loadRecipeClassInstancesFromDb(string sRcpClassDp, time tValidAt=0) {
  string sRcpClassName, sSystemName;
  dyn_string dsRcpInstanceNames, dsRcpInstanceDps;
  
  sSystemName   = unGenericDpFunctions_getSystemName(sRcpClassDp);
  sRcpClassName = dpGetAlias(sRcpClassDp);
  
  _unRecipeFunctions_getRecipeInstancesOfClass(sRcpClassName, dsRcpInstanceNames, dsRcpInstanceDps);
  int iLen = dynlen(dsRcpInstanceNames);
  for (int i=1; i<=iLen; i++) {
    dsRcpInstanceNames[i] = sSystemName + sRcpClassName + "/" + dsRcpInstanceNames[i];
  }
  unRecipeFunctions_loadRecipesFromDb(dsRcpInstanceNames, tValidAt);
}

//--------------------------------------------------------------------------------------------------------------------------
/**
 * Loads the recipe metadata (last saved by, last saved time) from the DB
 * @param sRcpDp        - [IN]  Datapoint name of the recipe. 
 * @param lastSavedBy   - [OUT] User who last saved the recipe in the database.
 * @param lastSavedTime - [OUT] Time when the recipe was last saved in the database.
 */
public void unRecipeFunctions_loadRecipeMetadataFromDb(string sRcpDp, string &lastSavedBy, string &lastSavedTime) {
  dyn_int diRcpVersionNumbers;
  dyn_time dtRcpVersionTime;
  dyn_string dsRcpVersionUser, dsRcpVersionComments, exceptionInfo;
  
  // Check if the DB tool is initialized 
  fwConfigurationDB_checkInit(exceptionInfo);
  if (dynlen(exceptionInfo)) {
    // No error message, the database access may not be configured in the project
    return;
  }  
  
  string sAlias = dpGetAlias(sRcpDp + ".");
  string systemName = unGenericDpFunctions_getSystemName(sRcpDp);
  fwConfigurationDB_getRecipeHistory(systemName + sAlias, diRcpVersionNumbers, dtRcpVersionTime, dsRcpVersionUser, dsRcpVersionComments,	exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }
   
  if (!dynlen(dsRcpVersionUser) || dynlen(dsRcpVersionUser) != dynlen(dtRcpVersionTime)) {
    unRecipeFunctions_writeInRecipeLog("Error: The metadata returned by fwConfigurationDB_getRecipeHistory(..) is wrong", true, true);
    return;
  }
  
  lastSavedBy = dsRcpVersionUser[dynlen(dsRcpVersionUser)];
  
  time t0;
  if (t0 == dtRcpVersionTime[dynlen(dtRcpVersionTime)]) {
    lastSavedTime = "";
  } else {
    lastSavedTime = dtRcpVersionTime[dynlen(dtRcpVersionTime)];
  }
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Load a list of recipe instances from the Oracle database.
 * If the recipe class or recipe instance don't exist in the system they will be created.
 * @param dsSelectedRecipes - [IN] List of recipes to load from the Oracle database.
 * @param tValidAt          - [IN] Time when the recipes were valid. 
 */
void unRecipeFunctions_loadRecipesFromDb(dyn_string dsSelectedRecipes, time tValidAt=0) {  
  int iLen = dynlen(dsSelectedRecipes);
  for (int i=1; i<=iLen; i++) {
    unRecipeFunctions_loadSingleRecipeFromDb(dsSelectedRecipes[i], tValidAt);
  }
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Load a recipe instance from the Oracle database.
 * If the recipe class or recipe instance don't exist in the system they will be created.
 * @param sRcpName  - [IN] Name of the recipe to load from the Oracle database.
 * @param tValidAt  - [IN] Time when the recipe is valid. 
 * @return TRUE if and only if the recipe was loaded successfully from the database.
 */
bool unRecipeFunctions_loadSingleRecipeFromDb(string sRcpName, time tValidAt=0) {
  dyn_string exceptionInfo;
  
  // Check if the DB tool is initialized 
  fwConfigurationDB_checkInit(exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
    return false;
  }
  
  string sPvssDp, sState;
  dyn_dyn_mixed recipeObject;
  dyn_mixed recipeClassObject;
  string sSystemName      = unGenericDpFunctions_getSystemName(sRcpName);
  string sRcpClassName    = unExportTree_removeSystemName(strsplit(sRcpName, "/")[1]);
  string sRcpInstanceName = strsplit(sRcpName, "/")[2];    
    
  if (sSystemName != getSystemName()) {
    unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' belongs to a different distributed system "
          + "and cannot be loaded.", true, true);
    return false;
  }
    
  // Check if the recipe class already exists. If not, create the recipe class.
  if (!_unRecipeFunctions_getExistingClassOrCreateNewClassFromDb(sSystemName, sRcpClassName, recipeClassObject)) {
    return false;
  }

  unRecipeFunctions_writeInRecipeLog("Loading recipe '" + sRcpName + "' from ORACLE database.", true, true);
    
  // Check if the recipe instance already exists. If not, create the recipe instance.
  if (!_unRecipeFunctions_getExistingInstanceOrCreateNewInstanceFromDb(sSystemName, sRcpClassName, sRcpInstanceName, recipeObject)) {
    return false;
  }
   
  // Check if the state of the recipe instance is empty
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sRcpName, sPvssDp);
  dpGet(sPvssDp + "ProcessInput.State", sState);
  if (sState != "") {
    unRecipeFunctions_writeInRecipeLog("Error: the recipe instance '" + sRcpName + "' is in state " + sState + ".", true, true);
    unRecipeFunctions_writeInRecipeLog("The recipe values will not be updated.", true, true);
    return false;
  }
  
  // Load the recipe class from DB only for the devices existing in the recipe class.
  dyn_string dsRcpClassDpes = recipeClassObject[fwConfigurationDB_RCL_DEVICES];
  fwConfigurationDB_loadRecipeFromDB(sRcpName, dsRcpClassDpes, fwDevice_LOGICAL, recipeObject, exceptionInfo, "", "", tValidAt);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Exception loading recipe '" + sRcpName + "' from ORACLE database: " + exceptionInfo[2], true, true);
    return false;
  }
  
  fwConfigurationDB_saveRecipeToCache(recipeObject, "", sRcpName, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Exception saving the recipe '" + sRcpName + "' in cache: " + exceptionInfo[2], true, true);
    return false;
  }
    
  _unRecipeFunctions_checkRecipeValues(recipeObject[fwConfigurationDB_RO_DPE_NAME], recipeObject[fwConfigurationDB_RO_VALUE], FALSE, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
  }
    
  unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' has been loaded from the ORACLE database. ", true, true);
  return true;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Internal function to load the recipe class data or to create a new recipe class if it doesn't exist.
 * @param sSystemName   - [IN]  System name.
 * @param sRcpClassName - [IN]  Recipe class name.
 * @param recipeObject  - [OUT] Recipe class data.
 * @return TRUE if the recipe class data was loaded successfully, otherwise FALSE.
 */
bool _unRecipeFunctions_getExistingClassOrCreateNewClassFromDb(string sSystemName, string sRcpClassName, dyn_dyn_mixed &recipeObject) {
  int iPos;
  string sDpName;
  dyn_string dsRcpClassNames, dsRcpClassDps, exceptionInfo;
  
  // Check if the recipe class already exists. If not, create the recipe class.
  bool bLocalClassesOnly = FALSE;
  bool bAddSystemName    = TRUE;
  unRecipeFunctions_getRecipeClasses(dsRcpClassNames, dsRcpClassDps, "*", bLocalClassesOnly, bAddSystemName);

  iPos = dynContains(dsRcpClassNames, sSystemName + sRcpClassName);
  if (iPos<=0) {
    _unRecipeFunctions_createRecipeClassFromDbInternal(sSystemName, sRcpClassName, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Exception creating the recipe class '" + sRcpClassName + "' : " + exceptionInfo[2], true, true);
      return FALSE;
    }
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + sRcpClassName, sDpName);
  } else {
    sDpName = dsRcpClassDps[iPos];
  }
 
  unRecipeFunctions_getRecipeClassObject(sDpName, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Exception loading the recipe class info: " + exceptionInfo[2], true, true);
    return FALSE;
  }
  
  return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Internal function to load the recipe instance data or to create a new recipe instance if it doesn't exist.
 * @param sSystemName      - [IN]  System name.
 * @param sRcpClassName    - [IN]  Recipe class name.
 * @param sRcpInstanceName - [IN]  Recipe instance name.
 * @param recipeObject     - [OUT] Recipe instance data.
 * @return TRUE if the recipe instance data was loaded successfully, otherwise FALSE.
 */
bool _unRecipeFunctions_getExistingInstanceOrCreateNewInstanceFromDb(string sSystemName, string sRcpClassName, string sRcpInstanceName, dyn_dyn_mixed &recipeObject) {
  dyn_string exceptionInfo;
  string sRcpName = sRcpClassName + "/" + sRcpInstanceName;
  
  // Check if the recipe instance already exists. If not, create the recipe instance.
  if (!unRecipeFunctions_hasRecipeInstance(sRcpClassName, sRcpInstanceName)) {
    _unRecipeFunctions_createRecipeInstanceFromDbInternal(sSystemName, sRcpClassName, sRcpInstanceName, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Exception creating the recipe instance '" + sRcpName + "' : " + exceptionInfo[2], true, true);
      return FALSE;
    }
  } 
   
  fwConfigurationDB_loadRecipeFromCache(sRcpName, makeDynString(), "", recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Exception loading recipe '" + sRcpName + "' from cache: " + exceptionInfo[2], true, true);
    return FALSE; 
  }
  
  return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Finalize the operation of saving a recipe device to the Oracle database.
 * @param iCode         - [IN]  Return code of the save operation.
 * @param sDeviceName   - [IN]  Name of the recipe device saved to the database.
 * @param sDeviceDp     - [IN]  Datapoint name of the recipe device saved to the database.
 * @param sState        - [IN]  State of the recipe device.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
void _unRecipeFunctions_finalizeSaveRecipeToDb(int iCode, string sDeviceName, string sDeviceDp, string sState, dyn_string &exceptionInfo) {
  int iOperationResult;
  string sOperationName;
  
  _unRecipeFunctions_printDbSaveMessage(iCode, sDeviceName, sState, exceptionInfo);
  
  switch(iCode) {
    case DB_SAVE_ERROR_CANNOT_LOCK_CLASS:
      return;
    case DB_SAVE_ERROR_BAD_RCP_CLASS_STATE:
       break;
    case DB_SAVE_ERROR_CANNOT_SAVE_CLASS:    
      dpSet(sDeviceDp + "ProcessInput.State", "");
      iOperationResult = PROGRESS_OPERATION_FAILED;
      sOperationName = _unRecipeFunctions_getCurrentOperationId();
      break;
    case DB_SAVE_ERROR_CANNOT_LOCK_INSTANCE:
    case DB_SAVE_ERROR_BAD_RCP_INSTANCE_STATE:
      dynClear(exceptionInfo);
      iOperationResult = PROGRESS_OPERATION_FAILED;
      sOperationName = _unRecipeFunctions_getCurrentOperationId();
      _unRecipeFunctions_setOperationStatus(iOperationResult, sOperationName);
      return;
    case DB_SAVE_ERROR_CANNOT_LOAD_INSTANCE:
    case DB_SAVE_ERROR_CANNOT_RESET_INSTANCE:
    case DB_SAVE_ERROR_CANNOT_SAVE_INSTANCE:
      dynClear(exceptionInfo);
      dpSet(sDeviceDp + "ProcessInput.State", sState);
      iOperationResult = PROGRESS_OPERATION_FAILED;
      sOperationName = _unRecipeFunctions_getCurrentOperationId();
      break;
    case DB_SAVE_RCP_INSTANCE_SAVED:     
      dynClear(exceptionInfo);    
      dpSet(sDeviceDp + "ProcessInput.State", sState);
      iOperationResult = PROGRESS_OPERATION_COMPLETED;
      sOperationName = _unRecipeFunctions_getCurrentOperationId();
      break;
    case DB_SAVE_RCP_CLASS_SAVED: 
      //dpSet(sDeviceDp + "ProcessInput.State", "");
      iOperationResult = PROGRESS_OPERATION_COMPLETED;
      sOperationName = _unRecipeFunctions_getCurrentOperationId();
      _unRecipeFunctions_setOperationStatus(iOperationResult, sOperationName);
      return;
    case DB_SAVE_RCP_CLASS_AND_INSTANCES_COMPLETED:
      dpSet(sDeviceDp + "ProcessInput.State", "");
      return;
    case DB_SAVE_SKIP_RCP_INSTANCE:
      iOperationResult = PROGRESS_OPERATION_SKIPPED;
      sOperationName = _unRecipeFunctions_getCurrentOperationId();
      _unRecipeFunctions_setOperationStatus(iOperationResult, sOperationName);
      return;
  }
  
  if (sOperationName != "" ) {
    _unRecipeFunctions_setOperationStatus(iOperationResult, sOperationName);
  }
  _unRecipeFunctions_unlockDevices(makeDynString(sDeviceDp), exceptionInfo);
  _unRecipeFunctions_removeLockedDeviceBySaveToDb(sDeviceDp);
}
//--------------------------------------------------------------------------------------------------------------------------
/**
 * Print a message in the recipe log window after trying to save a recipe in the Oracle database.
 * @param iCode         - [IN] Return code of the save operation.
 * @param sDeviceName   - [IN] Name of the recipe device saved to the database.
 * @param sState        - [IN] State of the recipe device.
 * @param exceptionInfo - [IN] Standard exception handling variable.
 */
void _unRecipeFunctions_printDbSaveMessage(int iCode, string sDeviceName, string sState, dyn_string exceptionInfo) {
  switch (iCode) {
    case DB_SAVE_ERROR_CANNOT_LOCK_CLASS: 
      unRecipeFunctions_writeInRecipeLog("Error: The recipe class '" + sDeviceName + "' could not be locked.");
      unRecipeFunctions_writeInRecipeLog("The recipes can not be saved to the ORACLE database.");
      break;
    case DB_SAVE_ERROR_BAD_RCP_CLASS_STATE:
      unRecipeFunctions_writeInRecipeLog("Error: The recipe class '" + sDeviceName + "' is in state '" + sState + "'.");
      unRecipeFunctions_writeInRecipeLog("The recipe class and all its instances will not be saved to the ORACLE database.");
      break;
    case DB_SAVE_ERROR_CANNOT_SAVE_CLASS:
      unRecipeFunctions_writeInRecipeLog("Exception saving the recipe class '" + sDeviceName + "' to the ORACLE database: " + exceptionInfo[2], TRUE, TRUE);
      break;
    case DB_SAVE_ERROR_CANNOT_SAVE_INSTANCE:
      unRecipeFunctions_writeInRecipeLog("Exception saving the recipe instance '" + sDeviceName + "' to the ORACLE database: " + exceptionInfo[2], TRUE, TRUE);
      break;
    case DB_SAVE_ERROR_CANNOT_LOCK_INSTANCE:
      unRecipeFunctions_writeInRecipeLog("Error: The recipe instance '" + sDeviceName + "' could not be locked.");
      unRecipeFunctions_writeInRecipeLog("The recipe will not be saved to the ORACLE database.");
      break;
    case DB_SAVE_ERROR_BAD_RCP_INSTANCE_STATE:
      unRecipeFunctions_writeInRecipeLog("Error: The recipe instance '" + sDeviceName + "' is in state " + sState + ".");
      unRecipeFunctions_writeInRecipeLog("The recipe will not be saved to the ORACLE database.");
      break;
    case DB_SAVE_ERROR_CANNOT_LOAD_INSTANCE:
      unRecipeFunctions_writeInRecipeLog("Error getting the data of the recipe instance: " + sDeviceName, true, true);
      unRecipeFunctions_writeInRecipeLog("The recipe instance cannot be saved to the ORACLE database.", true, true);
      break;
    case DB_SAVE_ERROR_CANNOT_RESET_INSTANCE:
      unRecipeFunctions_writeInRecipeLog("Error while resetting the recipe instance '" + sDeviceName
              + "' in the ORACLE database: " + exceptionInfo[2], true, true);
      break;
    case DB_SAVE_RCP_INSTANCE_SAVED:
      unRecipeFunctions_writeInRecipeLog("The recipe instance '" + sDeviceName + "' was saved successfully to the ORACLE database.", true, true);
      break;
    case DB_SAVE_RCP_CLASS_SAVED:
      unRecipeFunctions_writeInRecipeLog("The recipe class '" + sDeviceName + "' was saved successfully to the ORACLE database.", true, true);
      break;
    case DB_SAVE_SKIP_RCP_INSTANCE:
      unRecipeFunctions_writeInRecipeLog("The recipe instance '" + sDeviceName + "' will not be saved in the database (skipped).", true, true);
      break;
  }
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Save a list of recipes to the database.
 * @param dsRcpDps - [IN]  List of recipe DPs to save to the database.
 * @param sComment - [IN]  Comment for the DB history.
 */
void unRecipeFunctions_saveRecipesToDb(dyn_string dsRcpDps, string sComment) {
  dyn_int diOperationIds;
  dyn_string dsRcpClassDps, dsRcpClassNames, dsOperationNames, exceptionInfo;
      
  _unRecipeFunctions_getRecipeClassList(dsRcpDps, dsRcpClassDps, dsRcpClassNames); 
  if (dynlen(dsRcpClassDps)==0) {
    return;
  }
  
  _unRecipeFunctions_getSaveOperationIds(dsRcpClassDps, diOperationIds, dsOperationNames, exceptionInfo);
  if (!dynlen(exceptionInfo)) {
    _unRecipeFunctions_openProgressDialog(diOperationIds, dsOperationNames);
  }
  
  _unRecipeFunctions_saveRecipeClassesToDb(dsRcpClassDps, dsRcpClassNames, sComment);
  _unRecipeFunctions_closeProgressDialog();
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the list of recipe classes from a list of recipes.
 * @param dsRcpDps        - [IN]  List of recipe DPs.
 * @param dsRcpClassDps   - [OUT] List of recipe class DPs.
 * @param dsRcpClassNames - [OUT] List of recipe class Names.
 */
private void _unRecipeFunctions_getRecipeClassList(dyn_string dsRcpDps, dyn_string &dsRcpClassDps, dyn_string &dsRcpClassNames) {
  int iLen;
  string sRcpClassName, sRcpClassDp, sSystemName, sDpTypeName;
  dyn_string exceptionInfo;
  
  dynClear(dsRcpClassDps);
  dynClear(dsRcpClassNames);
  iLen = dynlen(dsRcpDps);
  for (int i=1; i<=iLen; i++) {
    sDpTypeName = dpTypeName(dsRcpDps[i]);
    sSystemName = unGenericDpFunctions_getSystemName(dsRcpDps[i]);
    if (sDpTypeName == UN_CONFIG_UNRCPCLASS_DPT_NAME) {
      // The sRecipeDp is a recipe class
      sRcpClassDp = dsRcpDps[i];
      sRcpClassName = dpGetAlias(dsRcpDps[i]);
    } else if (sDpTypeName == UN_CONFIG_UNRCPINSTANCE_DPT_NAME) {
      // The sRecipeDp is a recipe instance
      dpGet(dsRcpDps[i] + ".ProcessInput.ClassName", sRcpClassName);
      unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + sRcpClassName, sRcpClassDp);
    } else {
      continue;
    }
    
    if (!_unRecipeFunctions_isRecipeClassDefinitionValid(sRcpClassDp, exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Error: The definition of the recipe class '" + sRcpClassName + "' is invalid: " + exceptionInfo[2], true, true);
      unRecipeFunctions_writeInRecipeLog("The recipe class and its instances will not be saved in the ORACLE database.", true, true);
      continue;
    }
    
    dynAppend(dsRcpClassDps, sRcpClassDp);
    dynAppend(dsRcpClassNames, sRcpClassName);
  }
  
  dynUnique(dsRcpClassDps);
  dynUnique(dsRcpClassNames);
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Internal function to save a recipe object to the database.
 * @param recipeObject  - [IN]  Recipe object to be saved in the database.
 * @param sComment      - [IN]  Comment for the DB history.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return TRUE if the recipe object was successfully saved in the database, otherwise FALSE.
 */
bool _unRecipeFunctions_saveRecipeObjectToDb(dyn_dyn_mixed recipeObject, string sComment, dyn_string &exceptionInfo) {
  dynClear(exceptionInfo);
  
  string sRcpName = recipeObject[fwConfigurationDB_RO_META_ORIGNAME];
  
  unRecipeFunctions_writeInRecipeLog("Saving recipe instance '" + sRcpName + "' to the ORACLE database.", true, true);
  fwConfigurationDB_saveRecipeToDB(
      recipeObject,
      fwDevice_LOGICAL,
      sRcpName,
      exceptionInfo,
      sComment,
      TRUE);
  
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error saving the recipe " + sRcpName + " to DB: " + exceptionInfo[2], TRUE, TRUE);
    return FALSE;
  }
    
  return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the list of recipe instances from the Oracle database.
 * @param dsRcpInstanceNames - [OUT] Names of the recipe instances saved in the database.
 * @param exceptionInfo      - [OUT] Standard exception handling variable.
 * @return TRUE if the list of recipes was loaded successfully from the database, otherwise FALSE.
 */
bool unRecipeFunctions_getRecipeInstancesFromDatabase(dyn_string &dsRcpInstanceNames, dyn_string &exceptionInfo) {
  // Check if the DB tool is initialized 
  fwConfigurationDB_checkInit(exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true);
    return FALSE;
  }  
  
  fwConfigurationDB_getRecipesInDB(dsRcpInstanceNames, exceptionInfo);
  if (dynlen(exceptionInfo)){
    unRecipeFunctions_writeInRecipeLog("Error loading the list of recipe instances from ORACLE database: " + exceptionInfo[2], true, true);
    return FALSE;
  }	
  
  return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Delete a list of recipe instances from the ORACLE database.
 * @param dsRecipeNames - [IN] List of the recipe instances to delete from the ORACLE database.
 */
void unRecipeFunctions_deleteRecipeInstancesFromDb(dyn_string dsRecipeNames) {
  int iLen = dynlen(dsRecipeNames);
  for (int i=1; i<=iLen; i++) {
    string sRecipeName = dsRecipeNames[i];
    _unRecipeFunctions_dropRecipeInDb(sRecipeName);
    _unRecipeFunctions_dropRecipeInDb(sRecipeName + RCP_INST_INTERNAL_SUFFIX, FALSE);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Internal function to delete a recipe from the ORACLE database.
 * @param sRecipeName  - [IN] Name of the recipe to delete.
 * @param bShowMessage - [IN] TRUE if a confirmation message must be added to the log window, otherwise FALSE.
 */
void _unRecipeFunctions_dropRecipeInDb(string sRecipeName, bool bShowMessage=TRUE) {
  dyn_string exceptionInfo;
  
  fwConfigurationDB_dropRecipeInDB(sRecipeName, fwDevice_LOGICAL, exceptionInfo);
  if (dynlen(exceptionInfo)>0 && bShowMessage) {
    unRecipeFunctions_writeInRecipeLog("Exception deleting recipe '" + sRecipeName 
        + "' from Oracle DB: " + exceptionInfo[2], true, true);
    dynClear(exceptionInfo);
  } else if (bShowMessage) {
    unRecipeFunctions_writeInRecipeLog("The recipe '" + sRecipeName + "' has been deleted from the ORACLE database.", TRUE, TRUE);
  }
}

//------------------------------------------------------------------------------------------------------------------------
void _unRecipeFunctions_openProgressDialog(dyn_int diOperationIds, dyn_string dsOperationNames) {
  g_fProgressValue = 0.0;
  g_iCurrentOperation = 1;
  g_bCloseProgressDialog = FALSE;
  g_iProgressOperationIds = diOperationIds;
  g_dsProgressOperationNames = dsOperationNames;
  startThread("_unRecipeFunctions_startProgressDialog");
}
//------------------------------------------------------------------------------------------------------------------------
/**
* @reviewed 2018-06-25 @whitelisted{Thread}
*/
void _unRecipeFunctions_startProgressDialog() {
  ChildPanelOnCentralModal("vision/unRecipe/UnRcpCommon/unRcp_ProgressPanel.pnl", "Progress", makeDynString("$ids:"+g_iProgressOperationIds,"$names:"+g_dsProgressOperationNames));
}
//------------------------------------------------------------------------------------------------------------------------
void _unRecipeFunctions_closeProgressDialog() {
  g_bCloseProgressDialog = TRUE;
  mappingClear(g_mProgressOperationStatus);
}
//------------------------------------------------------------------------------------------------------------------------
string _unRecipeFunctions_getCurrentOperationId() {
  if (dynlen(g_dsProgressOperationNames) >= g_iCurrentOperation) {
    return g_dsProgressOperationNames[g_iCurrentOperation];
  }
  
  return "";
}
//------------------------------------------------------------------------------------------------------------------------
void _unRecipeFunctions_setOperationStatus(int status, string sOperationName) {
  string sStatus;
  
  int iPos = dynContains(g_dsProgressOperationNames, sOperationName);
  if (iPos <=0 ) {
    return;
  }
  
  switch(status) {
    case PROGRESS_OPERATION_ON_PROGRESS:
      sStatus = "On Progress...";
      break;
    case PROGRESS_OPERATION_COMPLETED:
      sStatus = "Completed";
      g_fProgressValue = iPos*100.0/(dynlen(g_dsProgressOperationNames)*1.0);
      g_iCurrentOperation += 1;
      break;
    case PROGRESS_OPERATION_FAILED:
      sStatus = "Failed";
      g_fProgressValue = iPos*100.0/(dynlen(g_dsProgressOperationNames)*1.0);
      g_iCurrentOperation += 1;
      break;
    case PROGRESS_OPERATION_SKIPPED:
      sStatus = "Skipped";
      g_fProgressValue = iPos*100.0/(dynlen(g_dsProgressOperationNames)*1.0);
      g_iCurrentOperation += 1;
      break;
    default:
      return;
  }
  
  g_mProgressOperationStatus[iPos] = sStatus;
}

//@}
//------------------------------------------------------------------------------------------------------------------------
