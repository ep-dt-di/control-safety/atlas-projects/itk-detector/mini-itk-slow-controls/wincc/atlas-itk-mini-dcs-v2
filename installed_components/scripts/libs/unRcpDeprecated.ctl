/**
 * UNICOS
 * Copyright (C) CERN 2018 All rights reserved
 */

/**@file

@name LIBRARY: fwDeviceCommentDeprecated.ctl

@author
  Dr. Marc Bengulescu (BE-ICS-FD)
*/



//------------------------------------------------------------------------------------------------------------------------
/**
 * Gets the recipe elements according to the distributed system.
 * @param elements - [IN] Recipe data point elements.
 * @param values - [IN] Recipe values.
 * @param systemDpes - [OUT] Map containing the recipe elements and values for each distributed system.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 *
 * @deprecated 2018-09-12
 * Originally found in unRcpFunctions.ctl
 */  
void _unRecipeFunctions_getElementsBySystem(dyn_string elements, dyn_anytype values, 
                                            mapping &systemDpes, dyn_string &exceptionInfo)
{
  FWDEPRECATED();

  string pvssDpe;
  dyn_dyn_string data;
  mappingClear(systemDpes);
  
  int len = dynlen(elements);
  if (len!=dynlen(values)) {
    fwException_raise(exceptionInfo, "ERROR", "The number of DPEs and values of the recipe must be equal.", "");
    return;
  }  
  
  // Inserts the DPEs in the mapping according to the system name
  for (int i=1; i<=len; i++) {
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(elements[i], pvssDpe);
    string systemName = unGenericDpFunctions_getSystemName(pvssDpe);
    
    dynClear(data);
    if (mappingHasKey(systemDpes, systemName)) {
      data = systemDpes[systemName];
    }

    dynAppend(data[1], pvssDpe);
    dynAppend(data[2], values[i]);
    systemDpes[systemName] = data;
  }  
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Gets the offset of an S7 address where a DPE is located.
 * @param sDpe - [IN] DPE which offset is required.
 * @param sDeviceType - [IN] Device type name of the dpe.
 * @param sOffset - [OUT] Offset of the DPE S7 address.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 *
 * @deprecated 2018-09-12
 * Originally found in unRcpFunctions.ctl
 */
void _unRecipeFunctions_getS7AddressOffset(string sDpe, string sDeviceType, string &sOffset, dyn_string &exceptionInfo)
{
  FWDEPRECATED();

  string fullAddress = DRV_S7_convertToUnicosAddress(sDpe, sDeviceType);

  int pos=strpos(fullAddress, ".");
  if(pos<0) {
    fwException_raise(exceptionInfo, "ERROR", "The S7 address format is invalid: " + fullAddress, "");
    return;
  }
  
  string temp = substr(fullAddress, pos+1, strlen(fullAddress)-pos);
  int len = strlen(temp);
  int j=0;
  for (int i=0; i<=len; i++){
    if ((temp[i]>='0' && temp[i]<='9') || temp[i]=='\0'){
      sOffset = sOffset + temp[i];
      j++;
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the list of PCO aliases.
 * @param dsPcoNames - [OUT] List of PCO aliases.
 * @param dsPcoDps   - [OUT] List of PCO Dps.
 * @param bLocalOnly - [IN]  TRUE if only the devices from the local system must be returned, otherwise FALSE.
 *
 * @deprecated 2018-09-12
 * Originally found in unRcpFunctions.ctl
 */
void unRecipeFunctions_getPcoList(dyn_string & dsPcoNames, dyn_string &dsPcoDps, bool bLocalOnly=FALSE) 
{
  FWDEPRECATED();

  dyn_string dsSelectedDeviceTypes, dsRetAppl;
  dynClear(dsPcoNames);
  dynClear(dsPcoDps);
  
  dsSelectedDeviceTypes = makeDynString(UN_CONFIG_CPC_PROCESSCONTROLOBJECT_DPT_NAME);
  _unRecipeFunctions_getFilteredDevices(dsSelectedDeviceTypes, dsPcoDps, dsPcoNames, dsRetAppl, bLocalOnly);
}

