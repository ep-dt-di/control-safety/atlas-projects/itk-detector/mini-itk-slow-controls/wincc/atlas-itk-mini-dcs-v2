/**
 * UNICOS
 * Copyright (C) CERN 2017 All rights reserved
 */
/**@file

// unRcpFunctions_class.ctl
This file contains functions related to the recipes classes.

@par Creation Date
  15/03/2017

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (BE-ICS)
*/
#uses "unRcpFunctions_gui.ctl"
#uses "unRcpFunctions_privileges.ctl"
#uses "unConfigSOFT_FE.ctl"
#uses "unGenericObject.ctl"
#uses "unImportDevice.ctl"
#uses "unGenericDpFunctions.ctl"
#uses "fwGeneral/fwExceptionHandling.ctl"
#uses "fwGeneral/fwGeneral.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "unExportTree.ctl"
#uses "unRcpFunctions.ctl"
#uses "fwGeneral/fwException.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unRcpFunctions_utilities.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unGraphicalFrame.ctl"
#uses "unRcpFunctions_dpe.ctl"
#uses "fwDevice/fwDevice.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Setup.ctl"
#uses "unicos_declarations.ctl"
//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the device mapping from a recipe class.
 * @param rcpClassObject - [IN] The recipe class object from the JCOP recipe class definition.
 * @param rcpDeviceDpes  - [OUT] Mapping where: Key=device name, Value=list of device DPEs included in the recipe.
 */
public void _unRecipeFunctions_getRecipeClassDeviceMapping(dyn_mixed rcpClassObject, mapping & rcpDeviceDpes) {
  int i, iLen;
  dyn_string rcpElements;
  
  // For each device in the recipe, get all the DPEs included in the recipe and
  // store them in the mapping rcpDeviceDpes as a string (comma separated)
  rcpElements = rcpClassObject[fwConfigurationDB_RCL_ELEMENTS];
  iLen = dynlen(rcpElements);
  for (i=1; i<=iLen; i++) {
	string sRcpElement = rcpElements[i];
    string sAlias = substr(sRcpElement, 0, strpos(sRcpElement, "."));
	dyn_string rcpElementSplit = strsplit(sRcpElement, ".");
	string rcpAliasDpes = "";
	if (mappingHasKey(rcpDeviceDpes, sAlias)) {
	  rcpAliasDpes = rcpDeviceDpes[sAlias];
	}
	
	if (strlen(rcpAliasDpes)) {
	  rcpAliasDpes += ", ";
    }
	rcpAliasDpes += rcpElementSplit[dynlen(rcpElementSplit)];
	rcpDeviceDpes[sAlias] = rcpAliasDpes;
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Check if a recipe class definition is valid.
 * @param sClassDp      - [IN]  Datapoint name of the recipe class.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public bool _unRecipeFunctions_isRecipeClassDefinitionValid(string sClassDp, dyn_string &exceptionInfo) {
  string sSystemName, sPvssDpe;
  dyn_mixed recipeClassInfo;
  
  unRecipeFunctions_normalizeDp(sClassDp);
  sSystemName = unGenericDpFunctions_getSystemName(sClassDp);
  unRecipeFunctions_getRecipeClassObject(sClassDp, recipeClassInfo, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return FALSE;
  }
  
  dyn_string dsDevices = recipeClassInfo[fwConfigurationDB_RCL_DEVICES];
  int iLen = dynlen(dsDevices);
  for (int i=1; i<=iLen; i++) {
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + dsDevices[i], sPvssDpe);
    if (!dpExists(sPvssDpe)) {
      fwException_raise(exceptionInfo, "ERROR", "The device " + dsDevices[i] + " does not exist." ,"");
      return FALSE;
    }
  }
  
  return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------
public dyn_string _unRecipeFunctions_getRcpClassDeviceOrder(string sRcpClassDp, dyn_string dsDevices, dyn_string &exceptionInfo) {
  dyn_string dsOrderedDevices;
  
  dynUnique(dsDevices);
  dpGet(sRcpClassDp + "ProcessInput.DeviceOrder", dsOrderedDevices);
  if (dynlen(dsOrderedDevices)==0) {
    return dsDevices;
  }
  
  // Check if the lists don't contain the same elements
  if (!unRecipeFunctions_areListsEqual(dsDevices, dsOrderedDevices)) {
    fwException_raise(exceptionInfo, "ERROR", "The device lists don't have the same devices.","");
    return dsDevices;
  }
  
  dpGet(sRcpClassDp + "ProcessInput.DeviceOrder", dsOrderedDevices);
  return dsOrderedDevices;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the recipe class data from a recipe instance datapoint.
 * @param sRcpInstanceDp - [IN] Datapoint name of the recipe instance.
 * @param sRcpClassName  - [OUT] Name of the recipe class related to the recipe instance.
 * @param sRcpClassDp    - [OUT] Datapoint name of the recipe class related to the recipe instance.
 * @param sPcoLink       - [OUT] Device link of the recipe class.
 */
public void _unRecipeFunctions_getRecipeClassDataFromInstance(string sRcpInstanceDp, string & sRcpClassName, string & sRcpClassDp,
		string & sPcoLink) 
{
  string sSystemName;
  
  unRecipeFunctions_normalizeDp(sRcpInstanceDp);
  sSystemName = unGenericDpFunctions_getSystemName(sRcpInstanceDp);
  
  // Get the recipe class name of the recipe instance
  if (!dpExists(sRcpInstanceDp)) {
    return;
  }
  
  dpGet(sRcpInstanceDp + ".ProcessInput.ClassName", sRcpClassName);
  if (sRcpClassName == "") {
    // If the recipe is in a remote system and this is disconnected, the recipe class name will be empty
    unRecipeFunctions_writeInRecipeLog("Error: Could not get data associated to the recipe class");
    return;
  }
  
  // Get the recipe class datapoint name and the recipe class pco link 
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + sRcpClassName, sRcpClassDp);
  unRecipeFunctions_normalizeDp(sRcpClassDp);
  if (!dpExists(sRcpClassDp)) {
    return;
  }
  
  dpGet(sRcpClassDp + ".statusInformation.deviceLink", sPcoLink);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Callback function executed when the recipe class elements have been modified.
 * The function will enable a warning message to indicate that the recipe must be reloaded.
 *
 * @reviewed 2018-09-12 @whitelisted{Callback}
 */
public void _unRecipeFunctions_dpConnectRcpClassElementsCB(string sRcpClassElementsDp, dyn_string dsValues) {
  _unRecipeFunctions_enableRecipeInstanceWarning(true, UN_RCP_WARNING_MESSAGE_CLASS_MODIFIED);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get all the recipe instances of a class.
 * @param sClassName 		    - [IN] Recipe class name.
 * @param dsRcpInstanceNames    - [OUT] Recipe instance names.
 * @param dsRcpInstanceDps      - [OUT] Recipe instance datapoint elements.
 */
// TODO: used in _privileges...
public void _unRecipeFunctions_getRecipeInstancesOfClass(string sClassName, dyn_string & dsRcpInstanceNames, 
			dyn_string &dsRcpInstanceDps) 
{
  int iLen;
  string sInstClassName, sInstName;
  dyn_string dsSelectedDevType, dsAppl;
  
  dsSelectedDevType = makeDynString(UN_CONFIG_UNRCPINSTANCE_DPT_NAME);
  _unRecipeFunctions_getFilteredDevices(dsSelectedDevType, dsRcpInstanceDps, dsRcpInstanceNames, dsAppl);
  
  if (sClassName=="*") {
    return;
  }
	
  // Check if the recipe instances belong to the specified recipe class
  iLen = dynlen(dsRcpInstanceDps);
  for (int i=1; i<=iLen; i++) {
    dpGet(dsRcpInstanceDps[i] + ".ProcessInput.ClassName", sInstClassName,
		  dsRcpInstanceDps[i] + ".ProcessInput.InstanceName", sInstName);
	
	if (sInstClassName!=sClassName) {
	  dynRemove(dsRcpInstanceDps, i);
	  dynRemove(dsRcpInstanceNames, i);
	  iLen--;
	  i--;
	  continue;
	} 
	
	dsRcpInstanceNames[i] = sInstName;
  }
}

//------------------------------------------------------------------------------------------------------------------------ 
/** 
 * Get the DP name of the initial recipe of the class 'sClassName'.
 * @param sClassDp      - [IN]  Datapoint name of the recipe class which initial recipe is required.
 * @param sInitialRcpDp - [OUT] DP Name of the initial recipe of the specified class.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return 0 if the initial recipe was found, otherwise returns a negative value
 */
public int _unRecipeFunctions_getInitialRecipeOfClass(string sClassDp, string & sInitialRcpDp, dyn_string & exceptionInfo) {
  
  int iRetVal, iPos;
  string sRcpInstanceName, sClassName, sSystemName;
  dyn_dyn_anytype resultTable;
  
  unRecipeFunctions_normalizeDp(sClassDp);
  dpGet(sClassDp + ".ProcessInput.ClassName", sClassName);
  sSystemName = unGenericDpFunctions_getSystemName(sClassDp);
    
  string sQuery = "SELECT '.RecipeName:_online.._value' FROM '*' REMOTE '" + sSystemName + "' WHERE "
                        + "('.RecipeName:_online.._value' LIKE \"" + sClassName + "/*\" OR '.RecipeName:_online.._value' LIKE \"*:" + sClassName + "/*\")"
                        + "AND '.MetaInfo.Predefined:_online.._value' = \"TRUE\" AND _DPT=\"_FwRecipeCache\" ";
                        
  // Get the initial recipe of the recipe class 'sClassName'
  iRetVal = dpQuery(sQuery, resultTable);   
 
  // Check if there isn't an initial recipe for the recipe class 
  if (dynlen(resultTable)<=1) {
    fwException_raise(exceptionInfo,"ERROR", "The recipe class '" + sClassName + "' doesn't have an initial recipe. ","");
	return RCP_INITIAL_NO_DEFINED;	   
  }

  // Check if there is more than one initial recipe instance
  if (dynlen(resultTable)>2) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe class '" + sClassName + "' has more than one initial recipe instance. ","");
	return RCP_INITIAL_TOO_MANY;	   
  }
  
  // Get the recipe instance name (Recipe name: rcpClassName/rcpInstanceName)
  iPos = strpos(resultTable[2][2], "/");
  if (iPos<0) {
	fwException_raise(exceptionInfo, "ERROR", "The recipe name is wrong ('/' character is missing).","");
	return RCP_INITIAL_UNDEFINED;
  }
  
  // Get the UNICOS recipe DP name
  sRcpInstanceName = unExportTree_removeSystemName(sRcpInstanceName);
  sRcpInstanceName = substr(resultTable[2][2], iPos+1);
  iRetVal = dpQuery("SELECT '.ProcessInput.ClassName:_online.._value' FROM '*' REMOTE '" + sSystemName + "' WHERE '.ProcessInput.ClassName:_online.._value' = \"" 
                    + sClassName + "\" AND '.ProcessInput.InstanceName:_online.._value' = \"" + sRcpInstanceName 
                    + "\" AND _DPT=\"" + UN_CONFIG_UNRCPINSTANCE_DPT_NAME + "\" ", resultTable);   
  
  if (dynlen(resultTable)!=2) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe instance '" + sClassName + "/" + sRcpInstanceName + "' doesn't exist. ","");
	return RCP_INITIAL_UNDEFINED;	 
  }
  
  // Get the PVSS DP name of the recipe instance
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(resultTable[2][1], sInitialRcpDp);
  
  return RCP_INITIAL_OK;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the DP name of the last activated recipe of the class 'sClassName'.
 * @param sClassDp - [IN] Datapoint name of the recipe class which last activated recipe is required.
 * @param sLastActivatedRcpDp - [OUT] DP Name of the last activated recipe of the specified class.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return 0 if the last activated recipe was found, otherwise returns a negative value
 */
public int _unRecipeFunctions_getLastActivatedRecipeDp(string sClassDp, string & sLastActivatedRcpDp, dyn_string & exceptionInfo) {
  int iRetVal, iPos;
  string sRcpInstanceName, sClassName, sSystemName;
  dyn_dyn_anytype resultTable;
  time t0;
  
  sSystemName = unGenericDpFunctions_getSystemName(sClassDp);
  unRecipeFunctions_normalizeDp(sClassDp);
  dpGet(sClassDp + ".ProcessInput.ClassName", sClassName);
  
  // Get the last activated recipe of the recipe class 'sClassName'
  iRetVal = dpQuery("SELECT '.RecipeName:_online.._value', '.MetaInfo.LastActivationTime:_online.._value' FROM '*' REMOTE '" + sSystemName + "' WHERE "
                + "('.RecipeName:_online.._value' LIKE \"" + sClassName + "/*\" OR '.RecipeName:_online.._value' LIKE \"*:" + sClassName + "/*\") "
                + "AND _DPT=\"_FwRecipeCache\" SORT BY 2 DESC", resultTable);   
 
  // Check if there aren't recipes matching the criteria
  if (dynlen(resultTable)<=1) {
    fwException_raise(exceptionInfo,"ERROR", "The recipe class '" + sClassName + "' doesn't have any recipe instance. ","");
	return -1;	   
  }
  
  // Check if the last activation time is the not valid (1970.01.01 01:00:00.000000000) 
  if (resultTable[2][3] == t0) {
    fwException_raise(exceptionInfo,"ERROR", "The recipe class '" + sClassName + "' doesn't have a last activated instance. ","");
    return -1;
  }
  
  // Get the recipe instance name (Recipe name: rcpClassName/rcpInstanceName)
  iPos = strpos(resultTable[2][2], "/");
  if (iPos<0) {
	fwException_raise(exceptionInfo, "ERROR", "The recipe name is wrong ('/' character is missing).","");
	return -1;
  }
  
  // Get the UNICOS recipe DP name
  sRcpInstanceName = unExportTree_removeSystemName(sRcpInstanceName);
  sRcpInstanceName = substr(resultTable[2][2], iPos+1);
  iRetVal = dpQuery("SELECT '.ProcessInput.ClassName:_online.._value' FROM '*' REMOTE '" + sSystemName + "' WHERE '.ProcessInput.ClassName:_online.._value' = \"" 
                + sClassName + "\" AND '.ProcessInput.InstanceName:_online.._value' = \"" + sRcpInstanceName 
                + "\"  AND _DPT=\"" + UN_CONFIG_UNRCPINSTANCE_DPT_NAME + "\" ", resultTable);   
  
  if (dynlen(resultTable)!=2) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe instance '" + sClassName + "/" + sRcpInstanceName + "' doesn't exist. ","");
	return -1;	 
  }
  
  // Get the PVSS DP name of the recipe instance
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(resultTable[2][1], sLastActivatedRcpDp);
  
  return 0;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the last activated recipe of each recipe class for the current PCO.
 * @param dsPcoDps - [IN] List of PCO datapoint elements which last activated recipes are required.
 * @param recipeList - [OUT] Datapoint elements of the last activated recipes of the PCO.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getLastActivatedPcoRecipes(dyn_string dsPcoDps, dyn_string &recipeList, dyn_string &exceptionInfo) {
  dyn_string dsRcpClasses, dsRcpClassDps;
  unRecipeFunctions_getRecipeClasses(dsRcpClasses, dsRcpClassDps);

  dyn_string dsPcoAlias;
  unRecipeFunctions_getDeviceAliases(dsPcoDps, dsPcoAlias);
  unRecipeFunctions_filterRecipeClasses(dsPcoAlias, makeDynString("*"), dsRcpClasses, dsRcpClassDps);

  int iLen = dynlen(dsRcpClassDps);
  for (int i=1; i<=iLen; i++) {
    // Get the last activated recipe of the recipe class
    string sClassDp = dsRcpClassDps[i];
    string sLastActivatedRcpDp;
    dyn_string exceptionInfo;

    int iRetVal = _unRecipeFunctions_getLastActivatedRecipeDp(sClassDp, sLastActivatedRcpDp, exceptionInfo);
    if (iRetVal < 0) {
      continue;
    }

    dynAppend(recipeList, sLastActivatedRcpDp);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the initial recipe of each recipe class for the current PCO.
 * @param dsPcoDps - [IN] List of PCO datapoint elements which initial recipes are required.
 * @param recipeList - [OUT] Datapoint elements of the initial recipes of the PCO.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getInitialPcoRecipes(dyn_string dsPcoDps, dyn_string &recipeList, dyn_string &exceptionInfo) {
  dyn_string dsRcpClasses, dsRcpClassDps;
  unRecipeFunctions_getRecipeClasses(dsRcpClasses, dsRcpClassDps);

  dyn_string dsPcoAlias;
  unRecipeFunctions_getDeviceAliases(dsPcoDps, dsPcoAlias);
  unRecipeFunctions_filterRecipeClasses(dsPcoAlias, makeDynString("*"), dsRcpClasses, dsRcpClassDps);

  int iLen = dynlen(dsRcpClassDps);
  for (int i=1; i<=iLen; i++) {
    // Get the last activated recipe of the recipe class
    string sClassDp = dsRcpClassDps[i];
    string sInitialRcpDp;
    dyn_string exceptionInfo;
    
    int iRetVal = _unRecipeFunctions_getInitialRecipeOfClass(sClassDp, sInitialRcpDp, exceptionInfo);
    if (iRetVal < 0) {
      continue;
    }

    dynAppend(recipeList, sInitialRcpDp);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the last activated recipe of each recipe class for the PCO hierarchy.
 * @param sPcoDp - [IN] The PCO datapoint element which last activated recipes are required.
 * @param recipeList - [OUT] Datapoint elements of the last activated recipes of the PCO hierarchy.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getLastActivatedRecipesFromPcoHierarchy(string sPcoDp, dyn_string &recipeList, dyn_string &exceptionInfo) {
  dyn_string dsPcoHierarchy;

  dynAppend(dsPcoHierarchy, sPcoDp);

  // Get all the PCO that are children of the sPcoDp device
  _unRecipeFunctions_getPcoChildren(sPcoDp, dsPcoHierarchy, exceptionInfo);
  unRecipeFunctions_getLastActivatedPcoRecipes(dsPcoHierarchy, recipeList, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the initial recipe of each recipe class for the PCO hierarchy.
 * @param sPcoDp - [IN] The PCO datapoint element which initial recipes are required.
 * @param recipeList - [OUT] Datapoint elements of the initial recipes of the PCO hierarchy.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_getInitialRecipesFromPcoHierarchy(string sPcoDp, dyn_string &recipeList, dyn_string &exceptionInfo) {  
  dyn_string dsPcoHierarchy;

  dynAppend(dsPcoHierarchy, sPcoDp);

  // Get all the PCO that are children of the sPcoDp device
  _unRecipeFunctions_getPcoChildren(sPcoDp, dsPcoHierarchy, exceptionInfo);
  unRecipeFunctions_getInitialPcoRecipes(dsPcoHierarchy, recipeList, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Add new devices to the recipe class that is being displayed in the recipe class panel.
 * @param sRcpClassDp - [IN] Datapoint element of the recipe class that will be modified.
 */
public void unRecipeFunctions_addRecipeClassDevices(string sRcpClassDp) {
  dyn_string exceptionInfo;
  string sPreviousState;
  dyn_string deviceList, dsAppl, existingDevices;
  dyn_dyn_string rcpTypeData;
  dyn_float df;
  dyn_string ds;
  mapping mRcpTypeMap, mRcpTypeMapString;
  

  _fwConfigurationDB_getRecipeTypeData(UN_CONFIG_UNRCPTYPE_DPT_NAME, rcpTypeData, exceptionInfo);  
  if (dynlen(exceptionInfo)) {
	fwExceptionHandling_display(exceptionInfo);
    DebugTN(exceptionInfo);
    return;
  }
  
  _unRecipeFunctions_getTypeMapping(UN_CONFIG_UNRCPTYPE_DPT_NAME, mRcpTypeMap, exceptionInfo);
  dyn_string mKeys = mappingKeys(mRcpTypeMap);
  for (int i=1; i<=dynlen(mKeys); i++) {
    string sCurrentType = mKeys[i];
    dyn_string dsTypeDpes = mRcpTypeMap[sCurrentType];
    dynSortAsc(dsTypeDpes);	
    string sDeviceTypeDpes = "";
    fwGeneral_dynStringToString(dsTypeDpes, sDeviceTypeDpes, ", ");
    mRcpTypeMapString[sCurrentType] = sDeviceTypeDpes;
  }

  dyn_dyn_string allfwDeviceTypeInfo;
  dyn_string allfwDpTypes,allfwDevTypes;
  fwDevice_getAllTypes(allfwDeviceTypeInfo,exceptionInfo);
  if (dynlen(exceptionInfo)) {
    fwExceptionHandling_display(exceptionInfo);
    return;
  }
  allfwDevTypes=allfwDeviceTypeInfo[1];
  allfwDpTypes=allfwDeviceTypeInfo[2];
  
  bool allowDevsWithNoAlias=true;
  if (isDollarDefined("$allowDevsWithNoAlias")) {
  	allowDevsWithNoAlias=$allowDevsWithNoAlias;
  }
  
  dyn_string allDevDps, rcpDeviceTypes;

  for (int i=1; i<=dynlen(rcpTypeData); i++) {
    string sCurrTypeName = rcpTypeData[i][2];
    dynAppend(rcpDeviceTypes, sCurrTypeName);
  }
  _unRecipeFunctions_getFilteredDevices(rcpDeviceTypes, allDevDps, deviceList, dsAppl, TRUE);
  
  // Get the current recipe state
  dpGet(sRcpClassDp + ".ProcessInput.State", sPreviousState);
  if (sPreviousState == "") {
    // Set the recipe State to Edit mode
    dpSet(sRcpClassDp + ".ProcessInput.State", UN_RCP_CLASS_STATE_EDIT);
  }
  
  // Get the list of existing devices from the RecipeElements table
  getValue("RecipeElementsTable", "getColumnN", 2, existingDevices);
  
  // Create a String with the allowed device types for the recipe type
  string sRcpTypeList;
  dynSortAsc(mKeys);
  fwGeneral_dynStringToString(mKeys, sRcpTypeList, ",");
  
  ChildPanelOnCentralModalReturn("vision/unRecipe/UnRcpClass/unUnRcpClass_SelectionList.pnl",
		"Select Devices",
		makeDynString("$sDpName:" + sRcpClassDp,
					  "$text:Select devices:",
					  "$items:"+deviceList,
                      "$itemDps:"+allDevDps,
                      "$appl:"+dsAppl,
					  "$types:"+sRcpTypeList,
        			  "$AliasSelector:FALSE",
        			  "$DPSelector:FALSE",
					  "$selectMultiple:"+TRUE,
					  "$disabledItems:"+existingDevices),
		df,ds);
		
  // Insert the selected elements in the table
  for (int i=1; i<=dynlen(ds); i++) {
    string dpName, alias;
    // firstly check if this is h/w or logical device
    if (strpos(ds[i],":")>0){
      alias="";
      dpName=ds[i];
    } else {
      alias=ds[i];
	  int idx=dynContains(deviceList,alias);
      if (idx<1) {
        continue;
      }
      dpName=dpSubStr(allDevDps[idx],DPSUB_SYS_DP);
    }
    if (!dpExists(dpName)) {
      continue;
    }

    string deviceDpType = dpTypeName(dpName);
    string deviceType=deviceDpType;
    dyn_string deviceElements = mRcpTypeMapString[deviceType];
    setValue("RecipeElementsTable", "appendLine",
             "DeviceDpType",deviceDpType,
             "DeviceType", deviceType,
             "DeviceName", ds[i],
			 "DeviceElements", deviceElements);             
  }
  
  if (dynlen(ds)) {
    unRecipeFunctions_writeInRecipeLog("The new devices have been added. Please apply the changes to the recipe class.");
    setValue("applyChangesFrame", "visible", true);
  } else if (sPreviousState=="") {
    dpSet(sRcpClassDp + ".ProcessInput.State", "");
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Apply the modifications done in the recipe class elements.
 * @param sRcpClassDp - [IN] DataPoint name of the recipe class where the changes must be applied.
 */
public void unRecipeFunctions_applyRecipeClassModifications(string sRcpClassDp) {
  int i, j, iCol;
  string sClassName, sTypeName, sDesc/*, sSystemName*/;
  dyn_string deviceAliases, deviceDpes, rcpDpes, exceptionInfo;
  dyn_mixed dmRcpClassInfo;

  dpSet(sRcpClassDp + ".ProcessInput.State", UN_RCP_CLASS_STATE_APPLY_CHANGES);
  // Get the data from the recipe class panel
  getMultiValue("RecipeClass", "text", sClassName,
				"RecipeType", "text", sTypeName,
				"RecipeClassComment", "text", sDesc,
				"RecipeElementsTable", "nameToColumn", "DeviceName", iCol,
				"RecipeElementsTable", "getColumnN", iCol, deviceAliases,
				"RecipeElementsTable", "nameToColumn", "DeviceElements", iCol,
				"RecipeElementsTable", "getColumnN", iCol, deviceDpes);
  
  for (i=1; i<=dynlen(deviceAliases); i++) {
    dyn_string deviceDpesSplit = strsplit(deviceDpes[i], ",");
    for (j=1; j<=dynlen(deviceDpesSplit); j++) {		
        dynAppend(rcpDpes, deviceAliases[i] + ".ProcessOutput." + strltrim(deviceDpesSplit[j]));
    }
  }
    
  // Get the recipe class info before applying any modifications
  unRecipeFunctions_getRecipeClassObject(sRcpClassDp, dmRcpClassInfo, exceptionInfo);
  
  // Updates the JCOP Recipe class DP
  fwConfigurationDB_modifyRecipeClass(sClassName, rcpDpes, sTypeName, sDesc, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: The recipe class could not be modified:");
    unRecipeFunctions_writeInRecipeLog(exceptionInfo[2]);
    dpSet(sRcpClassDp + ".ProcessInput.State", UN_RCP_CLASS_STATE_EDIT);
    return;
  }
  
  // Set the online values for the new DPEs added to the recipe 
  _unRecipeFunctions_setOnlineValuesForNewDpes(
    dmRcpClassInfo[fwConfigurationDB_RCL_ELEMENTS], 
    rcpDpes, 
    dmRcpClassInfo[fwConfigurationDB_RCL_INSTANCES]);
  
  // Remove the Edit state of the recipe class
  dpSet(sRcpClassDp + ".ProcessInput.State", "",
        sRcpClassDp + ".ProcessInput.DeviceOrder", deviceAliases);
  //sSystemName = unGenericDpFunctions_getSystemName(sRcpClassDp);
  unRecipeFunctions_loadRecipeClassData(sRcpClassDp);
  unRecipeFunctions_writeInRecipeLog("The changes have been applied to the recipe class " + sClassName + " and all its instances.", TRUE);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * When a recipe class is modified to include new DPEs the method sets the online values in all the recipe instances
 * for the new DPEs.
 * @param dsOldDpes         - [IN] Old DPEs of the recipe class.
 * @param dsNewDpes         - [IN] New DPEs of the recipe class.
 * @param dsRcpInstNames    - [IN] Recipe instance names.
 */
private void _unRecipeFunctions_setOnlineValuesForNewDpes(dyn_string dsOldDpes, dyn_string dsNewDpes, dyn_string dsRcpInstNames) {
  int i, j, pos;
  string sPvssDpe;
  dyn_int diPos;
  dyn_string dsDpeDiff, dsPvssDpes, exceptionInfo;
  dyn_anytype daOnlineValues;
  dyn_dyn_mixed recipeObject;
  
  // Get the list of new DPEs added to the recipe class
  unRecipeFunctions_dynDiff(dsNewDpes, dsOldDpes, dsDpeDiff);
  
  if (dynlen(dsDpeDiff) == 0) {
    return;
  }

  // Convert the UNICOS DPEs to PVSS DPEs
  for (i=1; i<=dynlen(dsDpeDiff); i++) {
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(dsDpeDiff[i], sPvssDpe);
    dynAppend(dsPvssDpes, sPvssDpe);
  }
  
  for (i=1; i<=dynlen(dsRcpInstNames); i++) {
    fwConfigurationDB_loadRecipeFromCache(dsRcpInstNames[i], makeDynString(), "", recipeObject, exceptionInfo);
    
    if (i == 1) {
        // Find the positions where the online values must be modified in the recipeObject
        unRecipeFunctions_getDynPos(dsPvssDpes, recipeObject[fwConfigurationDB_RO_DPE_NAME], diPos);
        unRecipeFunctions_getOnlineValues(dsPvssDpes, daOnlineValues, exceptionInfo);
    }
    
    // Set the online values
    for (j=1; j<=dynlen(daOnlineValues); j++) {
        pos = diPos[j];
        if (pos > 0) {
            recipeObject[fwConfigurationDB_RO_VALUE][pos] = daOnlineValues[j];
        }
    }
    
    fwConfigurationDB_saveRecipeToCache(recipeObject, "", dsRcpInstNames[i], exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Remove the selected recipe class devices from the table.
 * @param sRcpClassDp - [IN] Datapoint element of the recipe class to be modified.
 */
public void unRecipeFunctions_removeSelectedRecipeClassDevices(string sRcpClassDp) {
  int i, iLen;
  dyn_int diLines;
  string sRcpState;

  getValue("RecipeElementsTable", "getSelectedLines", diLines);
  iLen = dynlen(diLines);
  for (i=iLen; i>=1; i--) {
    setValue("RecipeElementsTable", "deleteLineN", diLines[i]);
  }
  
  if (dynlen(diLines)) {
    dpGet(sRcpClassDp + ".ProcessInput.State", sRcpState);
    if (sRcpState == UN_RCP_CLASS_STATE_INVALID || sRcpState == UN_RCP_CLASS_STATE_INVALID_EDIT) {
        dpSet(sRcpClassDp + ".ProcessInput.State", UN_RCP_CLASS_STATE_INVALID_EDIT);
    } else {
        dpSet(sRcpClassDp + ".ProcessInput.State", UN_RCP_CLASS_STATE_EDIT);
    }
    setValue("applyChangesFrame", "visible", true);
    unRecipeFunctions_writeInRecipeLog("The selected devices have been removed. Please apply the changes to the recipe class.");
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Create the recipe class device.
 * @param sClassName          - [IN]  Recipe class name.
 * @param sDescription        - [IN]  Recipe class description.
 * @param sPcoLink            - [IN]  Alias of the PCO where the recipe class will be linked.
 * @param sRcpClassDp         - [OUT] Datapoint of the new recipe class.
 * @param exceptionInfo       - [OUT] Standard exception handling variable.
 * @param dsDeviceList        - [IN]  List of recipe elements (optional).
 * @param sAcDomain           - [IN]  Access control domain (optional).
 * @param sOperatorPrivileges - [IN]  Operator privileges (optional).
 * @param sExpertPrivileges   - [IN]  Expert privileges (optional).
 * @param sAdminPrivileges    - [IN]  Admin privileges (optional).
 */
public void _unRecipeFunctions_createRecipeClass(string sClassName, string sDescription, string sPcoLink, string &sRcpClassDp, dyn_string &exceptionInfo, 
        dyn_string dsDeviceList=makeDynString(), string sAcDomain="", string sOperatorPrivileges="", string sExpertPrivileges="", string sAdminPrivileges="")
{
  int iDeviceNumber, iDriverNumber;
  string sPrefix, sDeviceList = "", sPcoLinkDpe, sPvssDpe, sFrontEndName, sFrontEndDp, sAppName;
  dyn_string dsConfig, dsRunningArchive, dsArchBool, dsArchAnalog, dsArchEvent;
  bool bEvent;
  
  unRecipeFunctions_writeInRecipeLog("Creating recipe class: '" + sClassName + "' ...");
  
  // Check if the recipe class already exists in any system
  if(_unRecipeFunctions_doesRecipeClassExist(sClassName)) {
    fwException_raise(exceptionInfo,"ERROR", "The recipe class '" + sClassName + "' already exists. ","");
	return;	   
  }

  // Get the application parameters from the PCO Link
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sPcoLink, sPcoLinkDpe);
  sAppName = unGenericDpFunctions_getApplication(sPcoLinkDpe);
  
  // Initialize the fwDevice and value archive global variables
  unImportDevice_initialize();
  unImportDevice_getApplicationCharacteristics(bEvent, sPrefix);
  
  // Get necessary data to create a new recipe instance
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(UN_RECIPE_FE_ALIAS, sPvssDpe);
  if (sPvssDpe==UN_RECIPE_FE_ALIAS) {
    // The RCP_FE doesn't exist, create it
    // Get the Front End where the PCO link is defined to get the archive files
    string sPcoLinkDpName;
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sPcoLink, sPcoLinkDpName);
    string sPcoFeName = unGenericObject_GetPLCNameFromDpName(sPcoLinkDpName);
    dyn_string dsAnalog, dsBoolean, dsEvent;
    dpGet(sPcoFeName + ".configuration.archive_analog", dsAnalog,
		  sPcoFeName + ".configuration.archive_bool", dsBoolean,
		  sPcoFeName + ".configuration.archive_event", dsEvent);
	
    bool bOk = unRecipeFunctions_createRecipeFrontEnd(sAppName, sPrefix, bEvent, dsAnalog[1], dsBoolean[1], dsEvent[1], exceptionInfo);
    if (bOk == false) {
      fwException_raise(exceptionInfo, "ERROR", "The " + UN_RECIPE_FE_ALIAS + " SOFT_FE couldn't be created.", "");
		  return;
	  }
  
	  if (dynlen(exceptionInfo)) { 
      DebugTN(exceptionInfo); 
      return; 
    }
  
	  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(UN_RECIPE_FE_ALIAS, sPvssDpe);
  }

  sFrontEndName = unGenericObject_GetPLCNameFromDpName(sPvssDpe);
  strreplace(sFrontEndName, SOFT_FE_DPTYPE+"_", "");
  
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sFrontEndName, sFrontEndDp);
  dpGet(sFrontEndDp + "configuration.archive_bool", dsArchBool,
		sFrontEndDp + "configuration.archive_analog", dsArchAnalog,
		sFrontEndDp + "configuration.archive_event", dsArchEvent,
		sFrontEndDp + "communication.driver_num", iDriverNumber);
  
  // [UCPC-1467] Setting the application in the SOFT FE
  dyn_string dsSoftFeConfig;
  dynAppend(dsSoftFeConfig, SOFT_FE_DPTYPE);
  dynAppend(dsSoftFeConfig, UN_RECIPE_FE_ALIAS);
  dynAppend(dsSoftFeConfig, sAppName);
  dynAppend(dsSoftFeConfig, "N");
  dynAppend(dsSoftFeConfig, iDriverNumber);
  dynAppend(dsSoftFeConfig, dsArchBool[1]);
  dynAppend(dsSoftFeConfig, dsArchAnalog[1]);
  dynAppend(dsSoftFeConfig, "FALSE");
  dynAppend(dsSoftFeConfig, dsArchEvent[1]);
  unConfigSOFT_FE_setApplication(dsSoftFeConfig, exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    DebugTN(exceptionInfo); 
    return; 
  }

  // Create the JCOP recipe class with the 'editable' flag to TRUE.
  fwConfigurationDB_createRecipeClass(sClassName, UN_CONFIG_UNRCPTYPE_DPT_NAME, sDescription, true, makeDynString(), exceptionInfo);
  if (dynlen(exceptionInfo)) { 
    DebugTN(exceptionInfo); 
    return; 
  }
  
  // If there is a device list specified, converts the dyn_string to a string
  if (dynlen(dsDeviceList)) {
    fwGeneral_dynStringToString(dsDeviceList, sDeviceList, ",");
  }
  
  // Create a unique recipe class DP that is not existing yet
  unRecipeFunctions_createNewRecipeDp(UN_CONFIG_UNRCPCLASS_DPT_NAME, sFrontEndName, sAppName, sRcpClassDp, iDeviceNumber);
  
  // Create the dsConfig for the device
  dynAppend(dsConfig, UN_CONFIG_UNRCPCLASS_DPT_NAME);	// Recipe instance DPType name
  dynAppend(dsConfig, iDeviceNumber);					// Device number
  dynAppend(dsConfig, sClassName);						// Device alias
  dynAppend(dsConfig, sDescription);					// Description
  dynAppend(dsConfig, "");								// Diagnostics
  dynAppend(dsConfig, "");								// WWWLink
  dynAppend(dsConfig, "");								// Synoptic
  dynAppend(dsConfig, "");								// Domain
  dynAppend(dsConfig, "");								// Nature
  dynAppend(dsConfig, "UnRcpClassState");				// Widget Type
  dynAppend(dsConfig, UN_CONFIG_UNRCPTYPE_DPT_NAME);	// Recipe Type
  dynAppend(dsConfig, sDeviceList);						// Device List
    
  unImportDevice_import(dsConfig, exceptionInfo, SOFT_FE_DPTYPE, sFrontEndName,
					    sAppName, sPrefix, iDriverNumber,
						dsArchBool[1], dsArchAnalog[1], dsArchEvent[1], bEvent,
						dsRunningArchive);
						
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return; 
  }
  
  unImportDevice_triggerUpdate();
  
  // Get the default values of the privileged actions
  if(sOperatorPrivileges=="" && sExpertPrivileges=="" && sAdminPrivileges=="") {
    unRecipeFunctions_getDeviceDefaultPrivileges(UN_CONFIG_UNRCPCLASS_DPT_NAME, sOperatorPrivileges, sExpertPrivileges, sAdminPrivileges, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      DebugTN(exceptionInfo);
      return; 
    }
  } 
  
  // Set the recipe class data
  dpSet(sRcpClassDp + ".statusInformation.deviceLink", sPcoLink,
		sRcpClassDp + ".ProcessInput.ClassName", sClassName,
		sRcpClassDp + ".ProcessInput.RecipeType", UN_CONFIG_UNRCPTYPE_DPT_NAME,
        sRcpClassDp + ".statusInformation.accessControl.accessControlDomain", sAcDomain,
		sRcpClassDp + ".statusInformation.accessControl.operator", sOperatorPrivileges,
		sRcpClassDp + ".statusInformation.accessControl.expert", sExpertPrivileges,
		sRcpClassDp + ".statusInformation.accessControl.admin", sAdminPrivileges);
	
  // Update the recipe class privileges. This is necessary when the privileges are specified in the function parameters
  // and there are missing or obsolete parameters.
  unRecipeFunctions_updateRecipeClassPrivileges(sRcpClassDp);  
    
  // Set the value of the recipe class created, it will be reloaded when
  // the callback method unRecipeFunctions_deviceUpdatedRcpClassCB(..) is executed
  if (shapeExists("LoadedRcpDp")) {
    setValue("LoadedRcpDp", "text", sRcpClassDp);
  }

  unRecipeFunctions_writeInRecipeLog("The recipe class '" + sClassName + "' has been created.", TRUE);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Function used to know if a recipe class already exists in any system.
 * @param sClassName - [IN] Recipe class name.
 * @return TRUE if the recipe class already exists, otherwise FALSE.
 */
private bool _unRecipeFunctions_doesRecipeClassExist(string sClassName) {
  dyn_string dsSystemNames, exceptionInfo;
  dyn_dyn_anytype resultTable;
  
  // Check if the recipe class already exists in any system
  dsSystemNames = mappingKeys(g_m_ddsTreeDeviceOverviewDeviceList);
  
  int iLen = dynlen(dsSystemNames);
  for (int i=1; i<=iLen; i++) {
    if (!unRecipeFunctions_isRemoteSystemConnected(dsSystemNames[i], exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Warning: The remote system '" + dsSystemNames[i] + "' is not connected.", true, true);
      dynClear(exceptionInfo);
      continue;
    }
    
    dpQuery("SELECT '.ProcessInput.ClassName:_online.._value' FROM '*' REMOTE '" + dsSystemNames[i] + "' "
             + "WHERE '.ProcessInput.ClassName:_online.._value' = \""+sClassName
             +"\" AND _DPT=\""+UN_CONFIG_UNRCPCLASS_DPT_NAME+"\"", resultTable); 
             
    if (dynlen(resultTable)>1) {
      return TRUE;
    }
  }
  
  return FALSE;
}  

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Callback function from the recipe class panel. The function is called when the devices list has been updated.
 * @param sCommandDp 	- [IN] DP Name of the command over the list of devices.
 * @param iCommand   	- [IN] Command value over the list of devices.
 * @param sSystemNameDp - [IN] DP Name of the system.
 * @param sSystemName   - [IN] System name.
 *
 * @reviewed 2018-09-12 @whitelisted{Callback}
 */
public void unRecipeFunctions_deviceUpdatedRcpClassCB(string sCommandDp, int iCommand, string sSystemNameDp, string sSystemName)
{
  int iCol;
  bool bAreEqual;
  string sPcoLink, sDpName, sState, sPanelName;
  dyn_float dfReturn;
  dyn_string dsRcpClasses, dsRcpClassesRet, dsRcpClassesDpRet, exceptionInfo, dsReturn;
  
  switch(iCommand) 
  {
    case DEVUN_ADD_COMMAND:
    case DEVUN_DELETE_COMMAND:
    {
      synchronized(g_dsTreeApplication) 
      {
	    // Get the PCO link selected in the panel
		getValue("PcoList", "selectedText", sPcoLink);
		
		// Get the List of recipe classes loaded in the panel
		getValue("RecipeClasses", "nameToColumn", "RecipeClass", iCol);
		getValue("RecipeClasses", "getColumnN", iCol, dsRcpClasses);
		
		// Get the list of recipe classes linked to the PCO
		unRecipeFunctions_getRecipeClasses(dsRcpClassesRet, dsRcpClassesDpRet, sPcoLink);
		
		// Compare the two recipe classes list
		bAreEqual = unRecipeFunctions_areListsEqual(dsRcpClasses, dsRcpClassesRet);
		
		if (!bAreEqual) {
		  // The list of recipe classes is different and it should be reloaded to avoid inconsistencies
		  getValue("LoadedRcpDp", "text", sDpName);

		  if (sDpName != "" && sDpName != "_UnRcpClass") {
		    // Check if the current recipe class is being edited
		    dpGet(sDpName + ".ProcessInput.State", sState);
		    if (sState != "") {
			    // The recipe class is being edited, display a confirmation message
			    unGraphicalFrame_ChildPanelOnCentralModalReturn(
				  "vision/MessageInfo",
				  sPanelName,
				  makeDynString("$1:The list of recipe classes has been modified.\nDo you want update the recipe class list?\n" +
						+ "(The changes of the recipe class will be applied)", "$2:Yes","$3:No"), 
				  dfReturn, 
				  dsReturn);
			    if(dynlen(dfReturn)>0 && dfReturn[1]==1)
			    { // Apply the changes to the recipe class
			      unRecipeFunctions_applyRecipeClassModifications(sDpName);
			    } else {
			      return;
			    }
		    }
        unRecipeFunctions_loadRecipeClasses(sPcoLink, exceptionInfo, FALSE);
		    unRecipeFunctions_selectRecipeClass(sDpName, exceptionInfo, TRUE);
		  } else {
		    // There is no recipe class selected, load the recipe classes
		    unRecipeFunctions_loadRecipeClasses(sPcoLink, exceptionInfo);
		  }
		}
      }
      break;
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Check if an initial recipe instance exists for the specified class.
 * @param sRecipeClass - [IN] Recipe class name.
 * @return TRUE if the specified recipe class has an initial recipe, otherwise FALSE.
 */
public bool _unRecipeFunctions_hasInitialRecipe(string sRecipeClass) {
  string sQuery;
  dyn_dyn_anytype resultTable;

  sQuery = "SELECT '.RecipeName:_online.._value' FROM '*' WHERE "
              + "('.RecipeName:_online.._value' LIKE \"" + sRecipeClass + "/*\" OR '.RecipeName:_online.._value' LIKE \"*:" + sRecipeClass + "/*\") "
              + " AND '.MetaInfo.Predefined:_online.._value' = \"TRUE\"  AND _DPT=\"_FwRecipeCache\" ";
  
  // Check if an initial recipe of the class already exists
  dpQuery(sQuery, resultTable);   
  
  // Check if there isn't an initial recipe for the recipe class 
  if (dynlen(resultTable)<=1) {
    return FALSE;
  }
  
  return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Delete a recipe class and all its recipe instances.
 * Note: The recipe class and its instances will be deleted ONLY if there are no initial recipes of the class.
 * @param sRcpClassDp - [IN] Data point name of the recipe instance to delete.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_deleteRecipeClass(string sRcpClassDp, dyn_string &exceptionInfo) {
  int i, iLen, iRetVal;
  bool bDeleteRecipes = TRUE;
  string sClassName, sInitialRcpDp, sPcoLink, sSystemName;
  dyn_string dsRcpInstances;
  
  unImportDevice_initialize();
  
  // Get the recipe class name
  sClassName = dpGetAlias(sRcpClassDp + ".");
  sSystemName = unGenericDpFunctions_getSystemName(sRcpClassDp);
  
  unRecipeFunctions_writeInRecipeLog("Removing recipe class '" + sClassName + "' and all its instances ...");
  
  // Get the initial recipe of the recipe class
  iRetVal = _unRecipeFunctions_getInitialRecipeOfClass(sRcpClassDp, sInitialRcpDp, exceptionInfo);
  if (iRetVal != RCP_INITIAL_NO_DEFINED) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe class has some initial recipes defined and it can not be deleted.","");
    unRecipeFunctions_writeInRecipeLog("Error: The recipe class '" + sClassName + "' has some initial recipes defined and it can not be deleted.");
    return;
  }
  
  dynClear(exceptionInfo);
    
  // Delete the JCOP recipe class and its instances
  fwConfigurationDB_deleteRecipeClass(sSystemName + sClassName, bDeleteRecipes, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error deleting the recipe class and its instances: " + exceptionInfo[2], true, true);
    return;
  }
  
  // Get the name of all the recipe instances of the recipe class
  dpGet(sRcpClassDp + ".ProcessInput.Recipes", dsRcpInstances);
  
  // Delete all the recipe instances 
  iLen = dynlen(dsRcpInstances);
  for (i=1; i<=iLen; i++) {
    string sRcpInstanceName, sPvssDpe;
    sRcpInstanceName = sSystemName + sClassName + "/" + dsRcpInstances[i];
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sRcpInstanceName, sPvssDpe);  
    dpDelete(sPvssDpe);
  }
  
  // Delete the recipe class
  dpDelete(sRcpClassDp);
  
  // Delete the predefined recipe instance if it exists
  string sDpName = _unGenericDpFunctions_dpAliasToName(sSystemName + sClassName + "/" + UN_RCP_PREDEFINED_NAME);
  if (sDpName != "") {
  	string sPvssDpe;
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sDpName, sPvssDpe);
    dpDelete(sPvssDpe);
  }
  
  unImportDevice_triggerUpdate();
  
  getValue("PcoList", "text", sPcoLink);
  unRecipeFunctions_loadRecipeClasses(sPcoLink, exceptionInfo);
  
  unRecipeFunctions_writeInRecipeLog("The recipe class '" + sClassName + "' and all its instances have been removed.", TRUE);
}
//------------------------------------------------------------------------------------------------------------------------
/**	 
 * Get the list of recipe class names and recipe class DPs linked to a PCO.
 * @param dsRecipeClassNames    - [OUT] List of recipe class names.
 * @param dsRecipeClassDps      - [OUT] List of recipe class DPs.
 * @param sPcoLink		          - [IN]  PCO alias which recipe classes are requested.
 * @param bLocalOnly            - [IN]  TRUE if only the recipe classes of the local system must be loaded, otherwise FALSE.
 * @param bAddSystemName        - [IN]  TRUE if the system name must be added to the recipe class name
 * @param bOnlyConnectedSystems - [IN]  TRUE if only the devices of the connected systems must be obtained.
 * @param sApplication          - [IN]  Name of the application for which the recipe classes are required.
 */
public void unRecipeFunctions_getRecipeClasses(dyn_string &dsRecipeClassNames, dyn_string &dsRecipeClassDps, 
        string sPcoLink="*", bool bLocalOnly=FALSE, bool bAddSystemName=FALSE, string sApplication="*", bool bOnlyConnectedSystems=FALSE)
{
	int iLen;
	string sRcpClassPcoLink, sSystemName;
	dyn_string dsSelectedDevType, dsAppl, exceptionInfo;
	
	dynClear(dsRecipeClassNames);
	dynClear(dsRecipeClassDps);
	
	// Get the list of recipe classes
	dsSelectedDevType = makeDynString(UN_CONFIG_UNRCPCLASS_DPT_NAME);	
	_unRecipeFunctions_getFilteredDevices(dsSelectedDevType, dsRecipeClassDps, dsRecipeClassNames, dsAppl, bLocalOnly);
				
	if (sPcoLink!="*" || sApplication!="*") {
	  // Removes all the recipe classes which are not linked with the selected PCO or belong to the specified application
	  /*iLen = dynlen(dsRecipeClassDps);
	  for (int i=1; i<=dynlen(dsRecipeClassDps); i++) {
	    if (!dpExists(dsRecipeClassDps[i] + ".statusInformation.deviceLink:_online.._value")) {
		    continue;
      }
      dpGet(dsRecipeClassDps[i] + ".statusInformation.deviceLink:_online.._value", sRcpClassPcoLink);
      string sRcpApplication = unGenericDpFunctions_getApplication(dsRecipeClassDps[i]);
      bool bRemoveElement = (sPcoLink!="*" && sRcpClassPcoLink!=sPcoLink) || (sApplication!="*" && sApplication!=sRcpApplication);
      if (bRemoveElement) {
        dynRemove(dsRecipeClassDps, i);
        dynRemove(dsRecipeClassNames, i);
        iLen--;
        i--;
      }
    }
    */
    unRecipeFunctions_filterRecipeClasses(makeDynString(sPcoLink), makeDynString(sApplication), dsRecipeClassNames, dsRecipeClassDps);
  }
	
  if (bOnlyConnectedSystems) {
    iLen = dynlen(dsRecipeClassDps);
    for (int i=1; i<=iLen; i++) {
      sSystemName = unGenericDpFunctions_getSystemName(dsRecipeClassDps[i]);
      if(!unRecipeFunctions_isRemoteSystemConnected(sSystemName, exceptionInfo)) {
        dynRemove(dsRecipeClassDps, i);
        dynRemove(dsRecipeClassNames, i);
        iLen--;
        i--;
      }
    }
  }
  
  // Add the system name to the recipe class names (if specified)
  if (bAddSystemName) {
    iLen = dynlen(dsRecipeClassDps);
    for (int i=1; i<=iLen; i++) {
      sSystemName = unGenericDpFunctions_getSystemName(dsRecipeClassDps[i]);
      dsRecipeClassNames[i] = sSystemName + dsRecipeClassNames[i];
    }
  }
    
  // Sort alphabetically the recipe class names and the recipe class DP Names in a consistent way
  unRecipeFunctions_sortAscDynLists(dsRecipeClassNames, dsRecipeClassDps, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Load the recipe classes linked to the PCO sPcoAlias.
 * @param sPcoAlias - [IN] The PCO alias which recipe classes muat be loaded.
 * @param exceptionInfo - [OUT] Standard exception handling routine.
 * @param bLoadFirstClass - [IN] TRUE if the data of the first recipe class must be loaded in the panel. 
 * @param sApplication - [IN] Application name for the recipe classes to load.
 * @param bOnlyConnectedSystems - [IN]  TRUE if only the devices of the connected systems must be obtained.
 * @return The number of recipe classes added.
 */
public int unRecipeFunctions_loadRecipeClasses(string sPcoAlias, dyn_string &exceptionInfo, bool bLoadFirstClass=TRUE, string sApplication="*", 
         bool bOnlyConnectedSystems=FALSE)
{
  int iLines = 0;
  string sType;
  dyn_string recipeClassNames, recipeClassDps;
  
  if (!shapeExists("RecipeClasses")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return 0;
  }

  bool bLocalOnly = FALSE;
  bool bAddSystemName = FALSE;
  unRecipeFunctions_getRecipeClasses(recipeClassNames, recipeClassDps, sPcoAlias, bLocalOnly, bAddSystemName, sApplication, bOnlyConnectedSystems);  
  
  // Get the type of the RecipeClasses element
  getValue("RecipeClasses", "type", sType);
  if (sType=="Table") {
    iLines = _unRecipeFunctions_loadRecipeClassesInTable(recipeClassNames, recipeClassDps, bLoadFirstClass);
  } else if (sType=="ComboBox") {
    iLines = _unRecipeFunctions_loadRecipeClassesInComboBox(recipeClassNames);
  } else {
    unRecipeFunctions_writeInRecipeLog("Error: The type of the 'RecipeClasses' shape is unknown.");
  }
  
  return iLines;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Load the recipe class list in a table.
 * @param rcpClassNames   - [IN] List containing the recipe class names.
 * @param rcpClassDps     - [IN] List containing the recipe class datapoints.
 * @param bLoadFirstClass - [IN] TRUE if the data of the first recipe class must be loaded in the panel.
 * @return The number of recipe classes added.
 */
private int _unRecipeFunctions_loadRecipeClassesInTable(dyn_string rcpClassNames, dyn_string rcpClassDps, bool bLoadFirstClass=TRUE) {
  int iLines;
  dyn_string exceptionInfo;

  iLines = dynlen(rcpClassNames);
  setValue("RecipeClasses", "deleteAllLines");
  if (iLines > 0) {
    setValue("RecipeClasses", "appendLines", iLines, 
			 "RecipeClass", rcpClassNames,
			 "RecipeClassDp", rcpClassDps);
	// Select the first recipe class in the table
	if (bLoadFirstClass==true) {
		unRecipeFunctions_selectRecipeClass(rcpClassDps[1], exceptionInfo, TRUE);
	}
  } else {
	setValue("RecipeClasses", "appendLine", "RecipeClass", "-None-");
	unRecipeFunctions_clearRecipeClassData();
  }
  
  return iLines;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Load the recipe class list in a combo box.
 * @param rcpClassNames - [IN] List containing the recipe class names.
 * @return The number of recipe classes added.
 */
private int _unRecipeFunctions_loadRecipeClassesInComboBox(dyn_string rcpClassNames) {
  int iLines;
  
  iLines = dynlen(rcpClassNames);
  setValue("RecipeClasses", "deleteAllItems");
  if (iLines > 0) {
    for (int i=1; i<=iLines; i++) {
	  setValue("RecipeClasses", "appendItem", rcpClassNames[i]);
	}
  } else {
	setValue("RecipeClasses", "appendItem", "-None-");
  }
  
  return iLines;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Clear all the data from the recipe class panel.
 */
public void unRecipeFunctions_clearRecipeClassData() {
  if (shapeExists("RecipeClass")) {
    RecipeClass.text = "";
  }
	
  if (shapeExists("RecipeType")) {
    RecipeType.text = "";
  }
	
  if (shapeExists("RecipeClassComment")) { 
    RecipeClassComment.text = "";
  }
	
  if (shapeExists("LastModifiedTime")) {
    LastModifiedTime.text = "";
  }
	
  if (shapeExists("LastModifiedUser")) {
    LastModifiedUser.text = "";
  }
	
  if (shapeExists("LastActivatedRecipe")) {
    LastActivatedRecipe.text = "";
  }
  
  if (shapeExists("LastActivationTime")) {
    LastActivationTime.text = "";
  }
  
  if (shapeExists("LastActivationUser")) {
    LastActivationUser.text = "";
  }
  
  if (shapeExists("LoadedRcpDp")) {
    LoadedRcpDp.text = "";
  }
	
  if (shapeExists("RecipeElementsTable")) {
    setValue("RecipeElementsTable", "deleteAllLines");
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Load the recipe class data in the recipe class panel.
 * @param sRcpClassDp - [IN] Datapoint name of the recipe class which data will be loaded in the panel.
 */
private void unRecipeFunctions_loadRecipeClassData(string sRcpClassDp) {
  string sSystemName = "", sClassName;
  dyn_mixed recipeClassInfo;
  dyn_string dsErrorRows, exceptionInfo;
  mapping rcpDeviceDpes;
  
  unRecipeFunctions_normalizeDp(sRcpClassDp);
  sRcpClassDp += ".";
  
  sSystemName = unGenericDpFunctions_getSystemName(sRcpClassDp);
  dpGet(sRcpClassDp + "ProcessInput.ClassName", sClassName);
  unRecipeFunctions_getRecipeClassObject(sRcpClassDp, recipeClassInfo, exceptionInfo);
  
  RecipeClass.text         = recipeClassInfo[fwConfigurationDB_RCL_CLASSNAME];
  RecipeType.text          = recipeClassInfo[fwConfigurationDB_RCL_RECIPETYPE];
  RecipeClassComment.text  = recipeClassInfo[fwConfigurationDB_RCL_DESCRIPTION];
  LastModifiedTime.text    = recipeClassInfo[fwConfigurationDB_RCL_LAST_MODIFIED_TIME];
  LastModifiedUser.text    = recipeClassInfo[fwConfigurationDB_RCL_LAST_MODIFIED_USER];
  LastActivatedRecipe.text = recipeClassInfo[fwConfigurationDB_RCL_LAST_ACTIVATED_RECIPE];
  LastActivationTime.text  = recipeClassInfo[fwConfigurationDB_RCL_LAST_ACTIVATED_TIME];
  LastActivationUser.text  = recipeClassInfo[fwConfigurationDB_RCL_LAST_ACTIVATED_USER];
  
  // For each device in the recipe, get all the DPEs included in the recipe and
  // store them in the mapping rcpDeviceDpes as a string (comma separated)
  _unRecipeFunctions_getRecipeClassDeviceMapping(recipeClassInfo, rcpDeviceDpes);
  
  // Add the recipe elements to the table
  setValue("RecipeElementsTable", "deleteAllLines");
  
  dyn_string devices = _unRecipeFunctions_getRcpClassDeviceOrder(sRcpClassDp, recipeClassInfo[fwConfigurationDB_RCL_DEVICES], exceptionInfo);
  if (dynlen(devices) > 0) {    
    if (dynlen(exceptionInfo)) {
      DebugTN("Warning: The list of devices can not be ordered: " + exceptionInfo[2]);
      dynClear(exceptionInfo);
    }
      
    setValue("RecipeElementsTable", "enabled", FALSE);
    dyn_string dsDeviceType, dsAlias, dsElements;
    for (int i=1; i<=dynlen(devices); i++) {
      string deviceType, pvssDpe, sAlias;
      unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + devices[i], pvssDpe);
      if (dpExists(pvssDpe)) {
        deviceType = dpTypeName(pvssDpe);
      } else {
        dynAppend(dsErrorRows, i);
      }
      sAlias = devices[i];
      dynAppend(dsDeviceType, deviceType);
      dynAppend(dsAlias, sAlias);
      dynAppend(dsElements, rcpDeviceDpes[sAlias]);
    }   
    
    setMultiValue("RecipeElementsTable", "appendLines", dynlen(dsDeviceType),
               "DeviceType", dsDeviceType, 
               "DeviceName", dsAlias, 
               "DeviceElements", dsElements);
      
    if (dynlen(dsErrorRows)) {
      // Set the recipe instance in INVALID state
      dpSet(sRcpClassDp+"ProcessInput.State", UN_RCP_CLASS_STATE_INVALID);
      _unRecipeFunctions_setInvalidRows(dsErrorRows, "RecipeElementsTable");
      unRecipeFunctions_writeInRecipeLog("Error: Some elements of the recipe class '" + sClassName + "' do not exist.");
      unRecipeFunctions_writeInRecipeLog("Please remove the missing recipe elements.");
    }
  }

  setMultiValue(
    "RecipeElementsTable", "deleteSelection",
    "RecipeElementsTable", "enabled", TRUE);	
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Select the specified recipe class in the recipe class panel.
 * @param sRcpClassDp   - [IN] Datapoint name of the recipe class.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @param bForceReload  - [IN] Boolean value to force the reload of the recipe class data.
 */ 
public void unRecipeFunctions_selectRecipeClass(string sRcpClassDp, dyn_string &exceptionInfo, bool bForceReload=FALSE)
{
  int iPos, iRangeMin, iRangeMax;  
  string sClassName, sLoadedRcpDp/*, sSystemName*/;
  dyn_string classNames;
  
  // If the sRcpClassDp ends with ".", remove the dot from the DP
  unRecipeFunctions_normalizeDp(sRcpClassDp);
 
  //sSystemName = unGenericDpFunctions_getSystemName(sRcpClassDp);
  sClassName  = unGenericDpFunctions_getAlias(sRcpClassDp + ".");
  getMultiValue("RecipeClasses", "getColumnN", 0, classNames,
				"LoadedRcpDp", "text", sLoadedRcpDp);
  
  iPos = dynContains(classNames, sClassName);
  if (iPos<=0) {
	return;
  }

  // Check if the recipe class is already loaded or the reload has been forced
  if (sRcpClassDp != sLoadedRcpDp || bForceReload)
  {	
    // Select the recipe class in the table (if it's not already selected)
    dyn_int diSelLines;    
    getValue("RecipeClasses", "getSelectedLines", diSelLines);

    if (!dynContains(diSelLines, iPos-1)) {
        RecipeClasses.selectLineN(iPos-1);
    }
	
	if (shapeExists("RecipeElementsTable")) {
	  removeSymbol(myModuleName(), myPanelName(), "RCCElements");
	}

  addSymbol(myModuleName(), myPanelName(), "vision/unRecipe/UnRcpClass/unUnRcpClass_ClassElements.pnl",
            "RCCElements", makeDynString("$sDpName:"+sRcpClassDp), 190, 313, 0, 1, 1);
  
	unRecipeFunctions_loadRecipeClassData(sRcpClassDp);
	setValue("LoadedRcpDp", "text", sRcpClassDp);		
  }
  
  getValue("RecipeClasses", "lineRangeVisible", iRangeMin, iRangeMax);
  if (iRangeMin > (iPos-1) || (iPos-1) > iRangeMax) {
    setValue("RecipeClasses", "lineVisible", (iPos-1));
  }

  if ( shapeExists("recipeElementsFilter") )
  {
    recipeElementsFilter.initRecipeElementsShape();
    recipeElementsFilter.updateCountersAndToggle();
    
  }

}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Method used to load the recipe class list associated to a PCO
 * @param sPcoLink Alias of the PCO which recipe classes will be loaded or "*" for all the available classes.
 */ 
public void _unRecipeFunctions_loadRecipeClassList(string sPcoLink="*", string sApplication="*")
{
  dyn_string recipeClassNames, recipeClassDps, recipeClassComments, exceptionInfo;
  dynClear(recipeClassNames);
  dynClear(recipeClassComments);

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeClasses")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return;
  }	

  bool bLocalOnly=FALSE;
  bool bAddSystemName=FALSE;
  unRecipeFunctions_getRecipeClasses(recipeClassNames, recipeClassDps, sPcoLink, bLocalOnly, bAddSystemName, sApplication);
  if (dynlen(recipeClassNames)==0) {
    setValue("RecipeClasses", "appendItem", "-None-");
  } else {
    for (int i=1; i<=dynlen(recipeClassNames); i++) {
      setValue("RecipeClasses", "appendItem", recipeClassNames[i]);
    }
  }
} 

//------------------------------------------------------------------------------------------------------------------------
/**
 * Filters a list of recipe class names & recipe class dps. Removes from the lists the recipes that don't belong to the
 * specified applications or are not linked to the specified PCOs.
 * @param dsPcoAlias         - [IN] List of PCO aliases for the filter.
 * @param dsApplication      - [IN] List of applications for the filter.
 * @param dsRecipeClassNames - [IN/OUT] List of recipe class names.
 * @param dsRecipeClassDps   - [IN/OUT] List of recipe class dps.
 */
public void unRecipeFunctions_filterRecipeClasses(dyn_string dsPcoAlias, dyn_string dsApplication, dyn_string &dsRecipeClassNames, dyn_string &dsRecipeClassDps) {
  int iLen = dynlen(dsRecipeClassDps);

  for (int i=1; i<=dynlen(dsRecipeClassDps); i++) {
    string sPcoLinkDp = dsRecipeClassDps[i] + ".statusInformation.deviceLink:_online.._value";
    if (!dpExists(sPcoLinkDp)) {
      continue;
    }

    string sRcpClassPcoLink;
    dpGet(sPcoLinkDp, sRcpClassPcoLink);
    string sRcpApplication = unGenericDpFunctions_getApplication(dsRecipeClassDps[i]);
    bool bRemoveElement = (dynContains(dsPcoAlias, "*")<=0 && dynContains(dsPcoAlias, sRcpClassPcoLink)<=0 )
                          || (dynContains(dsApplication, "*")<=0 && dynContains(dsApplication, sRcpApplication)<=0 );
    if (bRemoveElement) {
      dynRemove(dsRecipeClassDps, i);
      dynRemove(dsRecipeClassNames, i);
      iLen--;
      i--;
    }
  }

}

//------------------------------------------------------------------------------------------------------------------------
