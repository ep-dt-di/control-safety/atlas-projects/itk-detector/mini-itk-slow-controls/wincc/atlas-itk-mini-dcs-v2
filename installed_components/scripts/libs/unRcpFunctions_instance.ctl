/**
 * UNICOS
 * Copyright (C) CERN 2017 All rights reserved
 */
/**@file

// unRcpFunctions_privileges.ctl
This file contains functions related to the recipes instances.

@par Creation Date
  15/03/2017

@par Modification History

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  Ivan Prieto Barreiro (BE-ICS)
*/
#uses "unRcpDbFunctions.ctl"
#uses "unRcpFunctions_class.ctl"
#uses "unRcpFunctions_privileges.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unImportDevice.ctl"
#uses "unGenericDpFunctions.ctl"
#uses "unSelectDeselectHMI.ctl"
#uses "unRcpFunctions_gui.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unConfigGenericFunctions.ctl"
#uses "unExportTree.ctl"
#uses "unRcpFunctions.ctl"
#uses "unGenericObject.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "fwGeneral/fwException.ctl"
#uses "unRcpFunctions_utilities.ctl"
#uses "unicos_declarations.ctl"
#uses "unConfigSOFT_FE.ctl"
#uses "unSystemIntegrity.ctl"
#uses "unSystemIntegrity_unicosPLC.ctl"
#uses "unProgressBar.ctl"
#uses "unGraphicalFrame.ctl"
#uses "unRcpFunctions_dpe.ctl"

/******************************
 *      Global   Variables    *
 *****************************/
global int g_editingRowNumber = -1;
global string g_editingColName = "";
global bool g_bCreatingRecipeInstance = false;
//------------------------------------------------------------------------------------------------------------------------
/**
 * Save the recipe values.
 * @param recipeObject - [IN] Recipe instance data.
 * @param values - [IN/OUT] Values for the recipe elements.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
private void _unRecipeFunctions_saveRecipeValues(dyn_dyn_mixed recipeObject, dyn_anytype &values, dyn_string &exceptionInfo)
{
  dynClear(exceptionInfo);

  dyn_anytype formattedValues;
  string instanceName = recipeObject[fwConfigurationDB_RO_META_ORIGNAME];
  dyn_string elements = recipeObject[fwConfigurationDB_RO_DPE_NAME];

  // The number of elements in the recipe must be the same as the number of values
  if (dynlen(elements)!=dynlen(values)){
    fwException_raise(exceptionInfo,"ERROR","The number of elements in the recipe " +
                      "is different than the number of values.","");
    return;
  }

  unRecipeFunctions_formatValues(elements, values, formattedValues, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }

  // Save the recipe values in the JCOP recipe
  recipeObject[fwConfigurationDB_RO_VALUE] = formattedValues;
  recipeObject[fwConfigurationDB_RO_META_LASTMODIFICATIONTIME] = getCurrentTime();
  recipeObject[fwConfigurationDB_RO_META_LASTMODIFICATIONUSER] = getUserName();
  fwConfigurationDB_saveRecipeToCache(recipeObject, "", instanceName, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }

  values = formattedValues;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Check if the recipe values are the same than the online values.
 * @param dpes          - [IN] Recipe elements data points.
 * @param values        - [IN] Recipe values.
 * @param errDpe        - [OUT] If the recipe values are different than the online values, this variable
 *      will contain the first dpe where the values are different.
 * @param exceptionInfo - [OUT] Standard error handling variable.
 * @return TRUE if the recipe values are the same than the online values, otherwise FALSE.
 */
public bool _unRecipeFunctions_checkRecipeOnlineValues(dyn_string dpes, dyn_string values, string &errDpe, dyn_string &exceptionInfo)
{
  bool bOk;
  dyn_string outputDpes;
  dyn_anytype onlineValues;
  string sFormat, sOnlineValue;
  float fOnlineValue, fRcpValue;;

  if (dynlen(dpes)==0) {
    return true;
  }

  // Get the DPEs for the online values
  unRecipeFunctions_getOnlineValueDpes(dpes, outputDpes, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return FALSE;
  }
  dpGet(outputDpes, onlineValues);

  int len = dynlen(onlineValues);
  for (int i=1; i<=len; i++) {
      int ircpval;
      bool bval, brcpval;

      if (!dpExists(outputDpes[i])) {
        fwException_raise(exceptionInfo, "ERROR", "The datapoint " + outputDpes[i] + " doesn't exist.", "");
        continue;
      }

	     string dpType = dpTypeName(outputDpes[i]);
	     if (dpType=="CPC_DigitalParameter") {
        bval = onlineValues[i];
        brcpval = (bool) values[i];
        if (bval!=brcpval) {
      		  errDpe = dpes[i];
      		  return FALSE;
	       }
	       continue;
	     }

      int elementType = dpElementType(outputDpes[i]);
      switch(elementType){
        case DPEL_FLOAT:
          sFormat = (string) dpGetFormat(outputDpes[i]);
          // Format the online value (e.g. values like '10.579999923706' will be converted to '10.580')
          sOnlineValue = unGenericObject_FormatValue(sFormat, onlineValues[i]);
          // Get the online value as float
          unGenericObject_StringToFloat(sOnlineValue, fOnlineValue, bOk);
          // Get the recipe value as float
          unGenericObject_StringToFloat(values[i], fRcpValue, bOk);
          // Compare the float values
          if (fRcpValue != fOnlineValue) {
            errDpe = dpes[i];
            return FALSE;
          }
          break;
        case DPEL_INT:
          ircpval = (int)values[i];
          if (onlineValues[i]!=ircpval) {
            errDpe = dpes[i];
            return FALSE;
          }
          break;
        case DPEL_BOOL:
          brcpval = (bool)values[i];
          if (onlineValues[i]!=brcpval) {
            errDpe = dpes[i];
            return FALSE;
          }
          break;
      }
  }

  return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the status of the recipe instance (Activated, inactive, no plc connection,...).
 * @param recipeObject  - [IN] Recipe instance data from fwConfigDB.
 * @param lastActivated - [IN] String containing the name of the last activated recipe of the class.
 * @param status 	    - [OUT] Recipe status.
 * @param errMsg        - [OUT] Error message for the user.
 * @param exceptionInfo - [OUT] Standard error handling variable.
 */
public void unRecipeFunctions_getRecipeStatus(dyn_dyn_mixed recipeObject, string lastActivated, int &status,
                                       dyn_string &errMsg, dyn_string &exceptionInfo)
{
  bool active;
  string errDpe;
  string rcpName 		 = recipeObject[fwConfigurationDB_RO_META_ORIGNAME];
  dyn_string rcpElements = recipeObject[fwConfigurationDB_RO_DPE_NAME];
  dyn_string rcpValues   = recipeObject[fwConfigurationDB_RO_VALUE];
  dyn_string rcpTypes	 = recipeObject[fwConfigurationDB_RO_ELEMENT_TYPE];

  // Check the PLCs status
  unRecipeFunctions_getPlcStatus(rcpElements, active, errMsg, exceptionInfo);
  if(active==false){
    status = RCP_NO_PLC_CONNECTION;
    return;
  }

  // Check if all the DPEs used in the recipe exist
  status = unRecipeFunctions_checkRecipeStructure(rcpName, rcpElements, rcpValues, rcpTypes, exceptionInfo);
  if (dynlen(exceptionInfo)){
    return;
  }

  // Check if the last activated recipe of this type is the current one
  lastActivated = unExportTree_removeSystemName(lastActivated);
  rcpName       = unExportTree_removeSystemName(rcpName);
  if (lastActivated != rcpName){
    status = RCP_INACTIVE;
    return;
  }

  //Debug("unRecipeFunctions_getRecipeStatus - Getting online values \n");
  bool bOnlineValuesOk = _unRecipeFunctions_checkRecipeOnlineValues(rcpElements, rcpValues, errDpe, exceptionInfo);
  if(!bOnlineValuesOk || dynlen(exceptionInfo)>0){
    status = RCP_INACTIVE;
    return;
  }

  status = RCP_ACTIVATED;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Check if the recipe structure is coherent:
 *   - The number of DPEs and the number of values is the same
 *   - The values match the expected data type.
 * @param sRcpName      - [IN] Recipe instance name.
 * @param dsRcpElements - [IN] DPEs of the recipe.
 * @param dsRcpValues   - [IN] Recipe values.
 * @param dsRcpTypes    - [IN] Recipe value types.
 * @param exceptionInfo - [OUT] Standard error handling variable.
 * @return The result of the recipe structure check (bad types, bad structure ...)
 */
private int unRecipeFunctions_checkRecipeStructure(string sRcpName, dyn_string dsRcpElements, dyn_string dsRcpValues, dyn_string dsRcpTypes,
                                            dyn_string &exceptionInfo)
{
  float fval;
  int ival, elementType;
  dyn_string exception;
  string deviceType = "", lowerValue;

  dynClear(exceptionInfo);

  // Check if the number of elements is equal to the number of values
  if (dynlen(dsRcpElements)!=dynlen(dsRcpValues)){
    fwException_raise(exceptionInfo, "ERROR", "unRecipeFunctions_checkRecipeStructure: " +
                      "The number of elements is different than the number of values: " + sRcpName, "");
    return RCP_BAD_STRUCTURE;
  }

  // Check if the recipe value types match the data point element types
  int numElements = dynlen(dsRcpElements);
  for (int i=1; i<=numElements; i++){
    if (!dpExists(dsRcpElements[i])) {
        fwException_raise(exceptionInfo, "ERROR", "unRecipeFunctions_checkRecipeStructure: " +
			          "The recipe DPE does not exist: " + dsRcpElements[i], "");
        return RCP_BAD_DPES;
    }
    deviceType = dpTypeName(dsRcpElements[i]);

    if (deviceType=="CPC_DigitalParameter") {
      elementType = DPEL_BOOL;
    } else {
      elementType = (int) dsRcpTypes[i];
    }
    dynClear(exception);

    switch(elementType){
      case DPEL_FLOAT:
        unConfigGenericFunctions_checkFloat(dsRcpValues[i], fval, exception);
        if(dynlen(exception)){
          dynAppend(exceptionInfo, exception);
          return RCP_BAD_TYPES;
        }
        break;
      case DPEL_INT:
        unConfigGenericFunctions_checkInt(dsRcpValues[i], ival, exception);
        if(dynlen(exception)) {
          dynAppend(exceptionInfo, exception);
          return RCP_BAD_TYPES;
        }
        break;
      case DPEL_BOOL:
        lowerValue = strtolower(dsRcpValues[i]);
        if (lowerValue!="true" && lowerValue!="false") {
          fwException_raise(exceptionInfo, "ERROR", "unRecipeFunctions_checkRecipeStructure: " +
			          "Wrong boolean value: " + dsRcpValues[i], "");
          return RCP_BAD_TYPES;
        }
        break;
      default:
        fwException_raise(exceptionInfo, "ERROR", "unRecipeFunctions_checkRecipeStructure: " +
                          "Unknown DPType: " + dpTypeName(dsRcpElements[i]), "");
        return RCP_BAD_TYPES;
    }
  }

  return -1;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Check if the recipe values are valid according to the defined ranges.
 * @param recipeObject    - [IN]  Recipe object.
 * @param values          - [IN]  Recipe values.
 * @param bShowErrorLines - [IN]  TRUE if the values out of range must be highlighted in the table.
 * @param exceptionInfo   - [OUT] Standard exception handling variable.
 * @return TRUE if the recipe values are Ok according to the ranges, otherwise FALSE.
 */
public bool unRecipeFunctions_checkRecipeValues (dyn_dyn_mixed recipeObject, dyn_anytype &values,
                      bool bShowErrorLines, dyn_string &exceptionInfo)
{
  return _unRecipeFunctions_checkRecipeValues(recipeObject[fwConfigurationDB_RO_DPE_NAME], values, bShowErrorLines, exceptionInfo);
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Compares the recipe values and online values of a list of recipes.
 * @param dsRcpDpList [IN] - List of recipe instance datapoints.
 */
public void unRecipeFunctions_compareOnlineValues(dyn_string dsRcpDpList) {
  if (!shapeExists("RcpTable")) {
    unRecipeFunctions_writeInRecipeLog("Error: The shape 'RcpTable' is missing.", TRUE, TRUE);
    return;
  }

  int iDpCol;
  dyn_string dsAllRcpDps;

  getMultiValue("RcpTable", "nameToColumn", "RcpInstanceDp", iDpCol,
                "RcpTable", "getColumnN", iDpCol, dsAllRcpDps);

  int i, iLen;
  iLen = dynlen(dsRcpDpList);
  for (i=1; i<=iLen; i++) {
    string sRcpDp, sRcpName;
    dyn_dyn_mixed recipeObject;
    dyn_string exceptionInfo, dsDpNames, dsAlias;
    dyn_anytype daOnlineValues, daRecipeValues;
    dyn_int diRowStyleIndex, diRowStyles, diDiffRowIndex;

    int iPos = dynContains(dsAllRcpDps, dsRcpDpList[i]);
    sRcpDp = dsRcpDpList[i];
    unRecipeFunctions_normalizeDp(sRcpDp);
    sRcpName  = unGenericDpFunctions_getAlias(sRcpDp + ".");
    unRecipeFunctions_writeInRecipeLog("Checking online values of recipe " + sRcpName, TRUE, TRUE);
    _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "yellow");
    _unRecipeFunctions_getRecipeObjectFromDp(sRcpDp, recipeObject, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      DebugTN(exceptionInfo);
      continue;
    }
    daRecipeValues = recipeObject[fwConfigurationDB_RO_VALUE];
    dsDpNames = recipeObject[fwConfigurationDB_RO_DPE_NAME];
    dsAlias = recipeObject[fwConfigurationDB_RO_DP_NAME];
    unRecipeFunctions_getOnlineValues(dsDpNames, daOnlineValues, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      DebugTN(exceptionInfo);
      continue;
    }

    _unRecipeFunctions_getRowStylesForValueComparison(daRecipeValues, daOnlineValues, diRowStyleIndex, diRowStyles, diDiffRowIndex);
    unRecipeFunctions_writeDifferentOnlineValuesReport(diDiffRowIndex, dsDpNames, dsAlias, daRecipeValues, daOnlineValues);

    if (dynlen(diDiffRowIndex)) {
      _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "red");
    } else {
      _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "green");
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Saves the online values of the recipe elements into the recipe.
 * @param dsRcpDps [IN] - Datapoint names of the recipe instances.
 */
public void unRecipeFunctions_adoptOnlineValues(dyn_string dsRcpDps) {
  int i, iLen, iDpCol;
  dyn_string dsAllRcpDps;

  getMultiValue("RcpTable", "nameToColumn", "RcpInstanceDp", iDpCol,
                "RcpTable", "getColumnN", iDpCol, dsAllRcpDps);

  iLen = dynlen(dsRcpDps);
  for (i=1; i<=iLen; i++) {
    dyn_dyn_mixed recipeObject;
    dyn_string dsDpNames, exceptionInfo;
    dyn_anytype daOnlineValues;
    string sAlias = unGenericDpFunctions_getAlias(dsRcpDps[i]);

    int iPos = dynContains(dsAllRcpDps, dsRcpDps[i]);

    _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "yellow");
    if (!unRecipeFunctions_lockRecipe(dsRcpDps[i], sAlias)) {
      _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "red");
      continue;
    }

    _unRecipeFunctions_getRecipeObjectFromDp(dsRcpDps[i], recipeObject, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Exception loading the recipe '" + sAlias + "' : " + exceptionInfo[2], TRUE, TRUE);
      _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "red");
      continue;
    }

    dsDpNames = recipeObject[fwConfigurationDB_RO_DPE_NAME];
    unRecipeFunctions_getOnlineValues(dsDpNames, daOnlineValues, exceptionInfo);
    if (dynlen(exceptionInfo)){
      unRecipeFunctions_writeInRecipeLog("Exception loading the recipe '" + sAlias + "' " + exceptionInfo[2], TRUE, TRUE);
      _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "red");
      continue;
    }
    unRecipeFunctions_writeInRecipeLog("Saving online values in recipe: " + sAlias, TRUE, TRUE);
    _unRecipeFunctions_saveRecipeValues(recipeObject, daOnlineValues, exceptionInfo);
    if (dynlen(exceptionInfo)){
      unRecipeFunctions_writeInRecipeLog("Exception saving the values of recipe '" + sAlias + "' : " + exceptionInfo[2], TRUE, TRUE);
      _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "red");
      continue;
    }
    _unRecipeFunctions_setTableCellBGColor("RcpTable", (iPos-1), "RcpActivationStatus", "green");
    dpSet(dsRcpDps[i] + ".ProcessInput.State", "");
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Locks the recipe instance specified.
 * @param sRcpDp [IN] - Datapoint name of the recipe instace.
 * @param sAlias [IN] - Recipe alias.
 * @return TRUE if and only if the recipe instance was locked.
 */
private bool unRecipeFunctions_lockRecipe(string sRcpDp, string sAlias) {
  bool bLocked, bSelected, bSystemOk;
  string sSelectedManager, sSystemName, sRecipeState;

  unSelectDeselectHMI_isLocked(sRcpDp, bLocked, sSelectedManager);
  string localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedManager);
  bSelected = (localManager == "S");
  sSystemName = unGenericDpFunctions_getSystemName(sRcpDp);
  bSystemOk = (sSystemName == getSystemName());
  dpGet(sRcpDp + ".ProcessInput.State", sRecipeState);

  if (bLocked && !bSelected) {
    unRecipeFunctions_writeInRecipeLog("Error: The recipe '" + sAlias + "' is selected by a different manager (" + sSelectedManager + ").", TRUE, TRUE);
    return false;
  }

  if (sRecipeState != "") {
    unRecipeFunctions_writeInRecipeLog("Error: The recipe '" + sAlias + "' is in state (" + sRecipeState + ") and cannot be locked.", TRUE, TRUE);
    return false;
  }

  dpSet(sRcpDp + ".ProcessInput.State", UN_RCP_INSTANCE_STATE_LOCKED);

  return true;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Callback function executed when the PLC status has changed.
 * The properties table of the recipe instance will be reloaded.
 * @param sAlarmDpe - [IN] DP Name of the PLC alarm state.
 * @param iSystemIntegrityAlarmValue - [IN] Value of the PLC alarm state.
 * @param sEnabledDpe - [IN] DP Name of the system integrity alarm enabled.
 * @param bSystemIntegrityAlarmEnabled - [IN] Value of the system integrity alarm enabled.
 *
 * @reviewed 2018-09-12 @whitelisted{Callback}
 */
public void _unRecipeFunctions_plcStatusCB (string sAlarmDpe, int iSystemIntegrityAlarmValue,
									 string sEnabledDpe, bool bSystemIntegrityAlarmEnabled)
{
	if (!shapeExists("LoadedRecipeDp")) {
		return;
	}

	_unRecipeFunctions_loadPropertiesTableInfo();
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Create a predefined recipe instance to store the default privileges.
 * @param sClassName  - [IN] Recipe class name.
 * @param sDeviceLink - [IN] PCO link for the recipe class.
 * @param sAppName    - [IN] Application name.
 * @param exceptionInfo - [OUT] Standar exception handling variable.
 */
public void _unRecipeFunctions_createPredefinedRecipeInstance(string sClassName, string sDeviceLink, string sAppName, dyn_string &exceptionInfo) {
  bool bEvent;
  int iDeviceNumber;
  string sDpName, sOperatorPrivileges, sExpertPrivileges, sAdminPrivileges, sPvssDpe, sPrefix;

  // Check if the predefined recipe instance already exists
  sDpName = _unGenericDpFunctions_dpAliasToName(sClassName + "/" + UN_RCP_PREDEFINED_NAME);
  if (sDpName != "") {
    // The predefined recipe of the class already exists
    return;
  }

  // Get the recipe front end name
  unImportDevice_initialize();
  unImportDevice_getApplicationCharacteristics(bEvent, sPrefix);
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(UN_RECIPE_FE_ALIAS, sPvssDpe);
  if (sPvssDpe==UN_RECIPE_FE_ALIAS) {
    // The RCP_FE doesn't exist, create it
    string sDeviceLinkDp;
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sDeviceLink, sDeviceLinkDp);
    string sPcoFeName = unGenericObject_GetPLCNameFromDpName(sDeviceLinkDp);
    dyn_string dsAnalog, dsBoolean, dsEvent;
    dpGet(sPcoFeName + ".configuration.archive_analog", dsAnalog,
		  sPcoFeName + ".configuration.archive_bool", dsBoolean,
		  sPcoFeName + ".configuration.archive_event", dsEvent);

    bool bOk = unRecipeFunctions_createRecipeFrontEnd(sAppName, sPrefix, bEvent, dsAnalog[1], dsBoolean[1], dsEvent[1], exceptionInfo);
    if (bOk == false) {
      fwException_raise(exceptionInfo, "ERROR", "The " + UN_RECIPE_FE_ALIAS + " SOFT_FE couldn't be created.", "");
      return;
    }
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(UN_RECIPE_FE_ALIAS, sPvssDpe);
  }

  unRecipeFunctions_createNewRecipeDp(UN_CONFIG_INTERNAL_UNRCPINSTANCE_DPT_NAME, UN_RECIPE_FE_ALIAS, sAppName, sDpName, iDeviceNumber);
  dpCreate(sDpName, UN_CONFIG_INTERNAL_UNRCPINSTANCE_DPT_NAME);
  dpSetAlias(sDpName + ".", sClassName + "/" + UN_RCP_PREDEFINED_NAME);

  // Set the default privileges for the default recipe instance
  unRecipeFunctions_getDeviceDefaultPrivileges(UN_CONFIG_UNRCPINSTANCE_DPT_NAME, sOperatorPrivileges, sExpertPrivileges,
		    sAdminPrivileges, exceptionInfo);

  dpSet(sDpName + ".accessControl.operator", sOperatorPrivileges,
	       sDpName + ".accessControl.expert", sExpertPrivileges,
        sDpName + ".accessControl.admin", sAdminPrivileges);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Updates the privileges on the predefined recipe instance.
 * @param sClassName  - [IN] Recipe class name.
 * @param exceptionInfo - [OUT] Standar exception handling variable.
 */
public void unRecipeFunctions_updatePredefinedRecipeInstancePrivileges(string sClassName, dyn_string &exceptionInfo) {
  string sDpName, sDefaultOperatorPrivileges, sDefaultExpertPrivileges, sDefaultAdminPrivileges;
  string sDeviceOperatorPrivileges, sDeviceExpertPrivileges, sDeviceAdminPrivileges;

  // Check if the predefined recipe instance already exists
  sDpName = _unGenericDpFunctions_dpAliasToName(sClassName + "/" + UN_RCP_PREDEFINED_NAME);
  if (sDpName == "") {
    // The predefined recipe of the class does not exist
    return;
  }

  DebugTN("predefined sDpName", sDpName);
  unRecipeFunctions_normalizeDp(sDpName);
  unRecipeFunctions_getDeviceDefaultPrivileges(UN_CONFIG_UNRCPINSTANCE_DPT_NAME, sDefaultOperatorPrivileges, sDefaultExpertPrivileges,
		    sDefaultAdminPrivileges, exceptionInfo);

  dpGet(sDpName + ".accessControl.operator", sDeviceOperatorPrivileges,
	       sDpName + ".accessControl.expert", sDeviceExpertPrivileges,
        sDpName + ".accessControl.admin", sDeviceAdminPrivileges);

  unRecipeFunctions_arrangeRecipeInstancePrivileges(sDefaultOperatorPrivileges, sDefaultExpertPrivileges, sDefaultAdminPrivileges,
                                                     sDeviceOperatorPrivileges, sDeviceExpertPrivileges, sDeviceAdminPrivileges);

  dpSetWait(sDpName + ".accessControl.operator", sDeviceOperatorPrivileges,
	       sDpName + ".accessControl.expert", sDeviceExpertPrivileges,
        sDpName + ".accessControl.admin", sDeviceAdminPrivileges);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Callback function executed when the recipe values have been modified.
 * The function will enable a warning message to indicate that the recipe must be reloaded.
 *
 * @reviewed 2018-09-12 @whitelisted{Callback}
 */
public void _unRecipeFunctions_dpConnectRcpValuesCB(string sRcpValuesDp, dyn_string dsValues) {
  bool bVisible;
  string sRecipeName, sRecipeDp;
  dyn_string dsLoadedValues, dsRcpDpSplit, exceptionInfo;
  dyn_dyn_mixed recipeObject;

  getValue("WarningText", "visible", bVisible);
  if (bVisible) {
    return;
  }

  int iCol = RecipeElements.nameToColumn("Value");
  getValue("RecipeElements", "getColumnN", iCol, dsLoadedValues);

  int iLen = dynlen(dsLoadedValues);
  if (iLen != dynlen(dsValues)) {
    _unRecipeFunctions_enableRecipeInstanceWarning(true, UN_RCP_WARNING_MESSAGE_VALUES_MODIFIED);
    return;
  }

  // Compares the recipe object values with the values loaded in the recipe table
  dsRcpDpSplit = strsplit(sRcpValuesDp, ".");
  dpGet(dsRcpDpSplit[1] + ".RecipeName", sRecipeName);
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sRecipeName, sRecipeDp);
  _unRecipeFunctions_getRecipeObjectFromDp (sRecipeDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    return;
  }

  for (int i=1; i<=iLen; i++) {
    if (dsLoadedValues[i] != recipeObject[fwConfigurationDB_RO_VALUE][i]) {
      _unRecipeFunctions_enableRecipeInstanceWarning(true, UN_RCP_WARNING_MESSAGE_VALUES_MODIFIED);
      return;
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Put the recipe instance in Edit Mode so the recipe values can be modified.
 * @param sRecipeDp 	- [IN] DataPoint name of the recipe instance.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @param row			- [IN] Optional variable. It indicates the row number that is being edited.
 * @param column		- [IN] Optional variable. It indicates the name of the column that is being edited.
 */
public void unRecipeFunctions_editRecipeInstance(string sRecipeDp, dyn_string &exceptionInfo, int row=-1, string column="") {
  int iDpCol, iValueCol;
  string sSelectedDpName;
  dyn_anytype daDeviceDps, daValues;

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeElements") || !shapeExists("RecipeClasses") || !shapeExists("RecipeInstances")) {
	fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
	return;
  }

  unRecipeFunctions_getSelectedRecipeDp(sSelectedDpName, exceptionInfo);
  if (sSelectedDpName!=sRecipeDp) {
	  // If the instance selection fails, returns
	  if (!unRecipeFunctions_selectRecipeInstance(sRecipeDp, exceptionInfo)) {
		  return;
    }
  }

  // Recipe instance in EDIT mode
  dpSet(sRecipeDp+".ProcessInput.State", UN_RCP_INSTANCE_STATE_EDIT);

  // Modifies the color of the recipe values
  _unRecipeFunctions_setTableColumnBGColor("RecipeElements", "Value", CELL_EDIT_COLOR);
  setMultiValue("PcoList", "enabled", false,
        "ApplicationList", "enabled", false,
				"RecipeClasses", "enabled", false,
				"RecipeInstances", "enabled", false,
				"RecipeElements", "namedColumnEditable", "Value", true);

  // Check the range of the recipe values
  getMultiValue("RecipeElements", "nameToColumn", "DeviceDp", iDpCol,
                "RecipeElements", "nameToColumn", "Value", iValueCol,
                "RecipeElements", "getColumnN", iDpCol, daDeviceDps,
                "RecipeElements", "getColumnN", iValueCol, daValues);

  _unRecipeFunctions_checkRecipeValues(daDeviceDps, daValues, TRUE, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true );
  }

  recipeElementsFilter.setDiffToggleButtonState(true);

  g_editingRowNumber = row;
  g_editingColName = column;
  _unRecipeFunctions_copyRecipeInstanceToHiddenTable();
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Copy the RecipeElements table (Recipe instance) to a hidden table.
 */
private void _unRecipeFunctions_copyRecipeInstanceToHiddenTable() {
  int i, iCols;
  dyn_int diIndex;
  dyn_dyn_string dsData;

  getValue("RecipeElements", "columnCount", iCols);

  for (i=0; i<iCols; i++) {
    if (i==0) {
      getValue("RecipeElements", "getColumnN", i, diIndex);
    } else {
      getValue("RecipeElements", "getColumnN", i, dsData[i]);
    }
  }

  setValue("RecipeElementsCopy", "deleteAllLines");
  setValue("RecipeElementsCopy", "appendLines", dynlen(dsData[1]),
		   "Index", diIndex,
		   "DeviceDp", dsData[1],
		   "Alias", dsData[2],
		   "Description", dsData[3],
		   "Value", dsData[4],
		   "Unit", dsData[5],
		   "Range", dsData[6]);

  setValue("RecipeElementsCopy", "sort", true, "Index");
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Save the values of the recipe instance.
 * @param sRecipeDp - [IN] The DP of the recipe instance.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @param bShowConfirmationMessage - [IN] Flag to specify if the confirmation message must be displayed (TRUE by default)
 * @return TRUE if the recipe values were saved correctly, otherwise FALSE.
 */
public bool unRecipeFunctions_saveRecipeInstance(string sRecipeDp, dyn_string &exceptionInfo, bool bShowConfirmationMessage=TRUE)
{
  int iValColIndex = -1, iDpColIndex = -1;
  bool bAreEqual, bRangesOk;
  dyn_float df;
  dyn_string ds, dsDpNames, dsIndex;
  dyn_anytype values;
  dyn_dyn_mixed recipeObject;

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeElements") || !shapeExists("RecipeElementsCopy") || !shapeExists("RecipeClasses") || !shapeExists("RecipeInstances")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    DebugTN(exceptionInfo);
    return FALSE;
  }

  // Copy the table data to a hidden table (to order by index)
  _unRecipeFunctions_copyRecipeInstanceToHiddenTable();

  // Get the column index for the values
  getMultiValue("RecipeElementsCopy", "nameToColumn", "Value", iValColIndex,
				"RecipeElementsCopy", "nameToColumn", "DeviceDp", iDpColIndex);
  if (iValColIndex<0) {
    fwException_raise(exceptionInfo, "ERROR", "The 'RecipeElements' table doesn't have a column named 'Value'.","");
    return FALSE;
  }
  getMultiValue("RecipeElementsCopy", "getColumnN", iValColIndex, values,
				"RecipeElementsCopy", "getColumnN", iDpColIndex, dsDpNames);

  _unRecipeFunctions_getRecipeObjectFromDp(sRecipeDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return FALSE;
  }

  // Check if the DPEs of the recipe are coherent (comparing the ones on the table and the ones saved)
  bAreEqual = unRecipeFunctions_areListsEqual(recipeObject[fwConfigurationDB_RO_DPE_NAME], dsDpNames);
  if (!bAreEqual) {
    unRecipeFunctions_writeInRecipeLog("Error: The DP names in the table don't match the recipe DP names. Please reload the recipe.");
    return FALSE;
  }

  // Check the recipe values
  bRangesOk = unRecipeFunctions_checkRecipeValues(recipeObject, values, TRUE, exceptionInfo);
  if (dynlen(exceptionInfo)>0 || !bRangesOk) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
	return FALSE;
  }

  // Asks for user confirmation
  if (bShowConfirmationMessage) {
	ChildPanelOnCentralReturn("fwGeneral/fwOkCancel.pnl",
	 						  "Save recipe values",
							  makeDynString("$sTitle:Save recipe values ",
											  "$text: Do you want to save the recipe values?"  ),
							  df,
							  ds);

	  if (df[1]<=0) return FALSE;
  }

  _unRecipeFunctions_saveRecipeValues(recipeObject, values, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
    return FALSE;
  }
  recipeObject[fwConfigurationDB_RO_VALUE] = values;

  getMultiValue("RecipeElements", "getColumnN", 1, dsDpNames,
				"RecipeElements", "getColumnN", 0, dsIndex);
  // sets the formatted values in the table
  for (int i=1; i<=dynlen(dsIndex); i++) {
    int index = (int) dsIndex[i];
    setValue("RecipeElements", "cellValueRC", (i-1), "Value", values[index]);
  }
  _unRecipeFunctions_resetRecipeElementsBgColor();
  _unRecipeFunctions_loadHistoryTableInfo(sRecipeDp, recipeObject);
  _unRecipeFunctions_loadPropertiesTableInfo(sRecipeDp, recipeObject);

  recipeElementsFilter.setDiffToggleButtonState(false);

  setMultiValue("PcoList", "enabled", true,
                "ApplicationList", "enabled", true,
                "RecipeClasses", "enabled", true,
                "RecipeInstances", "enabled", true,
                "RecipeElements", "namedColumnEditable", "Value", false);

  g_editingRowNumber = -1;
  g_editingColName = "";
  unRecipeFunctions_writeInRecipeLog("The recipe " + recipeObject[fwConfigurationDB_RO_META_ORIGNAME] + " has been saved locally.", TRUE);
  dpSet(sRecipeDp+".ProcessInput.State", "");
  _unRecipeFunctions_enableRecipeInstanceWarning(false);

  return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Save the recipe values as default values in the devices.
 * @param sRecipeDp - [IN] The DP of the recipe instance.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_saveAsDeviceDefaultValues(string sRecipeDp, dyn_string &exceptionInfo) {
  dyn_string dsDefaultValueDpes;
  dyn_dyn_mixed recipeObject;

  // Get the recipe class of the selected recipe instance
  _unRecipeFunctions_getRecipeObjectFromDp(sRecipeDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return;
  }

  unRecipeFunctions_getDefaultValueDpes(recipeObject[fwConfigurationDB_RO_DPE_NAME], dsDefaultValueDpes, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return;
  }

  if(dpSet(dsDefaultValueDpes, recipeObject[fwConfigurationDB_RO_VALUE]) < 0) {
    fwException_raise(exceptionInfo,"ERROR", "Error saving the default values of the recipe devices.","");
    DebugTN(exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Save the values of the selected recipe in the initial recipe of the class.
 * @param sRecipeDp - [IN] The DP of the recipe instance which values must be save in the initial recipe.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_saveAsInitialRecipe(string sRecipeDp, dyn_string &exceptionInfo)
{
  int iCol;
  bool bRangesOk;
  string sRcpClassName, sRcpClassDp, sRcpInstanceName, sInitialRcpInstanceDp, sSystemName;
  dyn_dyn_mixed recipeObject, initialRecipeObject;
  dyn_string recipeValues;

  // Get the recipe class of the selected recipe instance
  _unRecipeFunctions_getRecipeObjectFromDp(sRecipeDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return;
  }

  sSystemName   = unGenericDpFunctions_getSystemName(sRecipeDp);
  sRcpClassName = recipeObject[fwConfigurationDB_RO_META_CLASSNAME];
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sSystemName + sRcpClassName, sRcpClassDp);

  // Get the initial recipe of the recipe class
  _unRecipeFunctions_getInitialRecipeOfClass(sRcpClassDp, sInitialRcpInstanceDp, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
    DebugTN(exceptionInfo);
    return;
  }

  // Get the values of the selected recipe
  _unRecipeFunctions_copyRecipeInstanceToHiddenTable();
  getMultiValue("RecipeElementsCopy", "nameToColumn", "Value", iCol,
    "RecipeElementsCopy", "getColumnN", iCol, recipeValues);

  // Get the initial recipe object
  _unRecipeFunctions_getRecipeObjectFromDp(sInitialRcpInstanceDp, initialRecipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error getting the data of the recipe instance with DP: " + sRecipeDp);
    DebugTN(exceptionInfo);
    return;
  }

  // Check if the recipe values are in the valid range
  bRangesOk = unRecipeFunctions_checkRecipeValues (initialRecipeObject, recipeValues, FALSE, exceptionInfo);
  if (dynlen(exceptionInfo)>0 || !bRangesOk) {
    unRecipeFunctions_writeInRecipeLog("Error: The recipe values can't be saved. Some recipe elements are out of range.");
    return;
  }

  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
    return;
  }

  // Save the new values of the initial recipe
  dpGet(sInitialRcpInstanceDp + ".ProcessInput.InstanceName", sRcpInstanceName);
  initialRecipeObject[fwConfigurationDB_RO_VALUE] = recipeValues;

  fwConfigurationDB_saveRecipeToCache(initialRecipeObject, "", sSystemName + sRcpClassName + "/" + sRcpInstanceName, exceptionInfo);

  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
    return;
  }

  unRecipeFunctions_writeInRecipeLog("The values of the recipe " + recipeObject[fwConfigurationDB_RO_META_ORIGNAME] +
				" have been saved in the initial recipe: " + sRcpClassName + "/" + sRcpInstanceName);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Cancel the recipe instance edition.
 * @param sRecipeDp - [IN] DP Name of the recipe instance that was being edited.
 */
public void unRecipeFunctions_cancelEditRecipeInstance(string sRecipeDp) {
  // Check that all the necessary shapes exist
  if ( !shapeExists("RecipeElements") || !shapeExists("RecipeClasses") || !shapeExists("RecipeInstances") || !shapeExists("recipeElementsFilter") ) {
    dyn_string exceptionInfo;
	fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
	return;
  }

  recipeElementsFilter.setDiffToggleButtonState(false);
  dpSet(sRecipeDp+".ProcessInput.State", "");

  int iMinLine, iMaxLine;
  getValue("RecipeElements", "lineRangeVisible", iMinLine, iMaxLine);
  setMultiValue("PcoList", "enabled", true,
                "ApplicationList", "enabled", true,
                "RecipeClasses", "enabled", true,
                "RecipeInstances", "enabled", true,
                "RecipeElements", "namedColumnEditable", "Value", false);
  _unRecipeFunctions_resetRecipeElementsBgColor();
  unRecipeFunctions_loadRecipeInstanceData();
  setValue("RecipeElements", "lineVisible", iMinLine);

  g_editingRowNumber = -1;
  g_editingColName = "";
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Callback function from the recipe instance panel. The function is called when the devices list has been updated.
 * @param sCommandDp 	- [IN] DP Name of the command over the list of devices.
 * @param iCommand   	- [IN] Command value over the list of devices.
 * @param sSystemNameDp - [IN] DP Name of the system.
 * @param sSystemName   - [IN] System name.
 *
 * @reviewed 2018-09-12 @whitelisted{Callback}
 */
public void unRecipeFunctions_deviceUpdatedRcpInstanceCB(string sCommandDp, int iCommand, string sSystemNameDp, string sSystemName)
{
  int iCol;
  bool bAreEqual;
  string sClassName, sLoadedRecipeInstance, sState, sPanelName;
  dyn_string dsRcpInstances, dsRcpInstanceNamesRet, dsRcpInstanceDpsRet, exceptionInfo, dsReturn;
  dyn_float dfReturn;

  if(g_bCreatingRecipeInstance) {
    return;
  }

  switch(iCommand)
  {
    case DEVUN_ADD_COMMAND:
    case DEVUN_DELETE_COMMAND:
    {
      synchronized(g_dsTreeApplication)
      {
		// Get the recipe class selected in the panel
		getValue("RecipeClasses", "selectedText", sClassName);

		// Get the list of recipe instances displayed in the panel
		getValue("RecipeInstances", "nameToColumn", "RecipeInstances", iCol);
		getValue("RecipeInstances", "getColumnN", iCol, dsRcpInstances);

		// Get the list of recipe instances of the selected class
		_unRecipeFunctions_getRecipeInstancesOfClass(sClassName, dsRcpInstanceNamesRet, dsRcpInstanceDpsRet);

		// Compare the two recipe classes list
		bAreEqual = unRecipeFunctions_areListsEqual(dsRcpInstances, dsRcpInstanceNamesRet);

		if (!bAreEqual) {
		  // The list of recipe instances is different and it should be reloaded to avoid inconsistencies

		  getValue("LoadedRecipeDp", "text", sLoadedRecipeInstance);
		  if (sLoadedRecipeInstance!="") {
  		    // Check if the current recipe has been removed

			if (!dpExists(sLoadedRecipeInstance + ".ProcessInput.State")) {
			  unRecipeFunctions_writeInRecipeLog("The recipe has been removed.");
			  _unRecipeFunctions_clearInstanceTables();
			  _unRecipeFunctions_loadRecipeInstances(sClassName);
			  return;
			}

			// Check if the current recipe is being edited
			dpGet(sLoadedRecipeInstance + ".ProcessInput.State", sState);
			if (sState == UN_RCP_INSTANCE_STATE_EDIT) {
			  // The recipe is being edited, display a confirmation message
			  unGraphicalFrame_ChildPanelOnCentralModalReturn(
					"vision/MessageInfo",
					sPanelName,
					makeDynString("$1:The list of recipe instances has been modified.\nDo you want update the recipe instance list?\n" +
							+ "(The changes of the recipe will be applied)", "$2:Yes","$3:No"),
					dfReturn,
					dsReturn);
			  if(dynlen(dfReturn)>0 && dfReturn[1]==1)
			  { // Apply the changes to the recipe
				unRecipeFunctions_saveRecipeInstance($sDpName, exceptionInfo, FALSE);
			  } else {
				return;
			  }
			}
		  }
		  _unRecipeFunctions_loadRecipeInstances(sClassName);
		  unRecipeFunctions_selectRecipeInstance(sLoadedRecipeInstance, exceptionInfo);
		}
      }
      break;
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Create a new recipe instance of the selected recipe class.
 */
public void unRecipeFunctions_createRecipeInstance()
{
  int iLen;
  string sAllowInitialRecipe = "FALSE";
  string sRecipeClass, sDpName, sRcpName, sRcpDesc, sInitial, sRecipeClassesType;
  string sDefaultRcpInstDp, sAcDomain, sOperatorAction, sExpertAction, sAdminAction;
  dyn_int lines;
  dyn_string exceptionInfo, ds, dsSelectedClassNames;
  dyn_float df;

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeClasses")) {
	unRecipeFunctions_writeInRecipeLog("Error: Some graphical shapes are missing.");
	return;
  }

  // TODO: This could be a function ... check if it's repeated in the code.
  // Get the type of the RecipeClasses shape (it can be "Table" or "ComboBox")
  getValue("RecipeClasses", "type", sRecipeClassesType);
  if (sRecipeClassesType=="Table") {
	// Get the selected recipe class from the table
	getValue("RecipeClasses", "getSelectedLines", lines);
	if (dynlen(lines)<=0) {
		unRecipeFunctions_writeInRecipeLog("Error: Select a recipe class before creating a recipe.");
		return;
	}
	getValue("RecipeClasses", "cellValueRC", lines[1], "RecipeClass", sRecipeClass);
  } else if (sRecipeClassesType=="ComboBox") {
    // Get the selected recipe class from the combo box
	getValue("RecipeClasses", "selectedText", sRecipeClass);
  } else {
    // The type of the RecipeClasses shape is unknown
	unRecipeFunctions_writeInRecipeLog("Error: The type of the RecipeClasses shape is unknown.");
	return;
  }

  // Check if the recipe class name is empty
  if (sRecipeClass=="" || sRecipeClass=="-None-") {
    unRecipeFunctions_writeInRecipeLog("Error: Please, select a recipe class before creating a recipe.");
    return;
  }

  // Check if the recipe class doesn't exist
  sDpName = _unGenericDpFunctions_dpAliasToName(sRecipeClass);
  if (sDpName == "") {
	unRecipeFunctions_writeInRecipeLog("Error: The recipe class '" + sRecipeClass + "' doesn't exist.");
	return;
  }

  // Check if an initial recipe of the class already exists
  if (!_unRecipeFunctions_hasInitialRecipe(sRecipeClass)) {
    sAllowInitialRecipe = "TRUE";
  }

  ChildPanelOnCentralModalReturn("vision/unRecipe/UnRcpInstance/unUnRcpInstance_Duplicate.pnl",
			"Create Recipe Instance",
			makeDynString("$sTitle:Create Recipe of class: " + sRecipeClass,
                "$sRecipeClass:" + sRecipeClass,
                "$sDescription: Input the name for the new recipe instance",
                "$sAllowInitialRecipe:"+sAllowInitialRecipe),
			df,ds);

  if (!dynlen(df) || df[1]<0) {
    return;
  }

  if (!dynlen(ds) || ds[1]=="") {
    unRecipeFunctions_writeInRecipeLog("Error: The recipe name can't be empty. ");
    DebugTN(exceptionInfo);
    return;
  }

  sRcpName = ds[1];
  sRcpDesc = ds[2];
  sInitial = ds[3];
  dsSelectedClassNames = strsplit(ds[4], "|");

  if (!unRecipeFunctions_isLegalRecipeName(sRcpName)) {
	unRecipeFunctions_writeInRecipeLog("Error: Wrong recipe name: " + sRcpName);
    return;
  }

  iLen = dynlen(dsSelectedClassNames);
  for (int i=1; i<=iLen; i++) {
    unRecipeFunctions_writeInRecipeLog("Creating recipe '" + sRcpName + "' of class '" + dsSelectedClassNames[i] + "' ...");
    unRecipeFunctions_getRecipeInstancePredefinedPrivileges(dsSelectedClassNames[i], sDefaultRcpInstDp, sAcDomain, sOperatorAction, sExpertAction, sAdminAction);
    _unRecipeFunctions_createRecipeInstance(dsSelectedClassNames[i], sRcpName, sRcpDesc, exceptionInfo, sInitial, "", sAcDomain, sOperatorAction, sExpertAction, sAdminAction);
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
      DebugTN(exceptionInfo);
      dynClear(exceptionInfo);
      continue;
    }
    unRecipeFunctions_writeInRecipeLog("The recipe '" + sRcpName + "' of class '" + dsSelectedClassNames[i] + "' has been created.", TRUE);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Create a new recipe instance.
 * @param sClassName 	      - [IN]  Name of the recipe class.
 * @param sInstanceName       - [IN]  Name of the new recipe instance.
 * @param sDescription        - [IN]  Description for the new recipe instance.
 * @param exceptionInfo       - [OUT] Standard exception handling variable.
 * @param sInitial		      - [IN]  Flag to define if the new recipe is the initial recipe of the class
 * @param sDpeValues 	      - [IN]  Recipe DPE|Values string. If the parameter is not specified the recipe elements will have the online values.
 * @param sAcDomain           - [IN]  Access control domain (optional).
 * @param sOperatorPrivileges - [IN]  Operator privileges (optional).
 * @param sExpertPrivileges   - [IN]  Expert privileges (optional).
 * @param sAdminPrivileges    - [IN]  Admin privileges (optional).
 */
synchronized public void _unRecipeFunctions_createRecipeInstance(string sClassName, string sInstanceName, string sDescription,
        dyn_string &exceptionInfo, string sInitial="FALSE", string sDpeValues="", string sAcDomain="",
        string sOperatorPrivileges="", string sExpertPrivileges="", string sAdminPrivileges="")
{
  int iDeviceNumber, iDriverNumber;
  string sDpName, sPrefix, sRcpClassDp, sFrontEndName, sFrontEndDp, sAppName, sDomain, sNature;
  dyn_mixed recipeClass;
  dyn_string dsConfig, dsRunningArchive, dsArchBool, dsArchAnalog, dsArchEvent, dsParameters;
  bool bEvent;

  g_bCreatingRecipeInstance = true;
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sClassName, sRcpClassDp);
  if (!_unRecipeFunctions_isRecipeClassDefinitionValid(sRcpClassDp, exceptionInfo)) {
    g_bCreatingRecipeInstance = false;
    return;
  }

  // Check if the recipe instance already exists
  if (unRecipeFunctions_hasRecipeInstance(sClassName, sInstanceName)) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe " + sClassName + "/" + sInstanceName +" already exists. ","");
    DebugTN(exceptionInfo);
    g_bCreatingRecipeInstance = false;
    return;
  }

  // Check if the initial recipe already exists
  if (sInitial == "TRUE" && _unRecipeFunctions_hasInitialRecipe(sClassName)) {
    fwException_raise(exceptionInfo, "ERROR", "The recipe class" + sClassName + " already has an initial recipe. ","");
    DebugTN(exceptionInfo);
    g_bCreatingRecipeInstance = false;
    return;
  }

  // Get necessary data to create a new recipe instance
  sFrontEndName = unGenericObject_GetPLCNameFromDpName(sRcpClassDp);
  strreplace(sFrontEndName, SOFT_FE_DPTYPE+"_", "");
  sAppName = unGenericDpFunctions_getApplication(sRcpClassDp);
  unGenericDpFunctions_getParameters(sRcpClassDp, dsParameters);
  sDomain = dsParameters[4];
  sNature = dsParameters[5];

  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sFrontEndName, sFrontEndDp);
  dpGet(sFrontEndDp + "configuration.archive_bool", dsArchBool,
		sFrontEndDp + "configuration.archive_analog", dsArchAnalog,
		sFrontEndDp + "configuration.archive_event", dsArchEvent,
		sFrontEndDp + "communication.driver_num", iDriverNumber);

  unRecipeFunctions_getRecipeClassObject(sRcpClassDp, recipeClass, exceptionInfo);
  if (dynlen(exceptionInfo)) {	// || dynlen(recipeClass[fwConfigurationDB_RCL_ELEMENTS])==0 )
    fwException_raise(exceptionInfo,"ERROR", "Error getting the recipe class data. The recipe can't be created.", "");
    DebugTN(exceptionInfo);
    g_bCreatingRecipeInstance = false;
    return;
  }

  // Creates a unique recipe DP that is not existing yet
  unRecipeFunctions_createNewRecipeDp(UN_CONFIG_UNRCPINSTANCE_DPT_NAME, sFrontEndName, sAppName, sDpName, iDeviceNumber);

  // Initialize the fwDevice and value archive global variables
  unImportDevice_initialize();
  unImportDevice_getApplicationCharacteristics(bEvent, sPrefix);

  if (sDpeValues == "") {
    // If the list of DPE|Value is not specified, get the online values for the recipe elements.
    sDpeValues = unRecipeFunctions_getDpeValueString(recipeClass[fwConfigurationDB_RCL_ELEMENTS], sDpeValues);
  }

  // Creates the dsConfig for the device
  dynAppend(dsConfig, UN_CONFIG_UNRCPINSTANCE_DPT_NAME);	// Recipe instance DPType name
  dynAppend(dsConfig, iDeviceNumber);						// Device number
  dynAppend(dsConfig, sClassName+"/"+sInstanceName);		// Device alias
  dynAppend(dsConfig, sDescription);						// Description
  dynAppend(dsConfig, "");									// Diagnostics
  dynAppend(dsConfig, "");									// WWWLink
  dynAppend(dsConfig, "");									// Synoptic
  dynAppend(dsConfig, sDomain);								// Domain
  dynAppend(dsConfig, sNature);								// Nature
  dynAppend(dsConfig, "UnRcpInstanceState");				// Widget Type
  dynAppend(dsConfig, sClassName);							// Recipe class
  dynAppend(dsConfig, sInitial);							// Initial recipe
  dynAppend(dsConfig, sDpeValues);							// Recipe DPE-Value definitions

  unImportDevice_import(dsConfig, exceptionInfo, SOFT_FE_DPTYPE, sFrontEndName,
					    sAppName, sPrefix, iDriverNumber,
						dsArchBool[1], dsArchAnalog[1], dsArchEvent[1], bEvent,
						dsRunningArchive);

  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
	if (dpExists(sDpName)) {
		dpDelete(sDpName);
	}
    g_bCreatingRecipeInstance = false;
	return;
  }

  unImportDevice_triggerUpdate();

  // Get the default values of the privileged actions
  if (sOperatorPrivileges=="" && sExpertPrivileges=="" && sAdminPrivileges=="") {
    string sPredefinedRcpInstance;
    unRecipeFunctions_getRecipeInstancePredefinedPrivileges(sClassName, sPredefinedRcpInstance, sAcDomain, sOperatorPrivileges, sExpertPrivileges, sAdminPrivileges);
  }

  // Set the recipe instance privileges
  dpSetWait(sDpName + ".statusInformation.accessControl.accessControlDomain", sAcDomain,
        sDpName + ".statusInformation.accessControl.operator", sOperatorPrivileges,
		sDpName + ".statusInformation.accessControl.expert", sExpertPrivileges,
		sDpName + ".statusInformation.accessControl.admin", sAdminPrivileges);

  unRecipeFunctions_updateRecipeInstancePrivileges(sDpName, sClassName);

  g_bCreatingRecipeInstance = false;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Check if a recipe class has an instance with the specified name.
 * @param sClassName    - [IN] Recipe class name.
 * @param sInstanceName - [IN] Recipe instance name.
 * @return TRUE if the recipe class has an instance with the specified name, otherwise FALSE.
 */
public bool unRecipeFunctions_hasRecipeInstance(string sClassName, string sInstanceName) {
  dyn_dyn_anytype resultTable;

  dpQuery("SELECT '.ProcessInput.InstanceName:_online.._value' FROM '*' WHERE '.ProcessInput.ClassName:_online.._value' = \""+sClassName+
				"\" AND _DPT=\""+UN_CONFIG_UNRCPINSTANCE_DPT_NAME+"\"", resultTable);

  if (dynlen(resultTable)>1) {
    dynRemove(resultTable, 1);
    for (int i=1; i<=dynlen(resultTable); i++) {
		// If the recipe instance already exists, show an error message and return
		if (resultTable[i][2] == sInstanceName) {
			return TRUE;
		}
    }
  }

  return FALSE;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Duplicate the selected recipe instance.
 * @param sRecipeDp - [IN] DP name of the recipe instance to duplicate.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_duplicateRecipeInstance(string sRecipeDp, dyn_string &exceptionInfo)
{
  string sRecipeInstance, sRecipeClass, sRecipeDesc, sInitial;
  dyn_float df;
  dyn_string ds;
  dyn_dyn_mixed recipeObject;

  unRecipeFunctions_normalizeDp(sRecipeDp);

  // Recipe instance in DUPLICATE mode
  dpSet(sRecipeDp+".ProcessInput.State", UN_RCP_INSTANCE_STATE_DUPLICATE);
  if (!shapeExists("RecipeClasses") || !shapeExists("RecipeInstances")) {
    dpSet(sRecipeDp+".ProcessInput.State", "");
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return;
  }

  _unRecipeFunctions_getRecipeObjectFromDp(sRecipeDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    dpSet(sRecipeDp+".ProcessInput.State", "");
    DebugTN(exceptionInfo);
    return;
  }

  ChildPanelOnCentralReturn("vision/unRecipe/UnRcpInstance/unUnRcpInstance_Duplicate.pnl",
			"Duplicate Recipe Instance",
			makeDynString("$sTitle:Duplicate Recipe Instance",
				      "$sDescription: Input the name for the new recipe instance"),
			df,ds);

  if (!dynlen(df) || df[1]<0) {
    dpSet(sRecipeDp+".ProcessInput.State", "");
    return;
  }

  if (!dynlen(ds) || ds[1]=="") {
    dpSet(sRecipeDp+".ProcessInput.State", "");
    fwException_raise(exceptionInfo,"ERROR", "The recipe name can't be empty. ","");
    return;
  }

  sRecipeInstance = ds[1];
  sRecipeDesc = ds[2];
  sInitial = "FALSE";
  sRecipeClass = recipeObject[fwConfigurationDB_RO_META_CLASSNAME];

  if (!unRecipeFunctions_isLegalRecipeName(sRecipeInstance)) {
	unRecipeFunctions_writeInRecipeLog("Error: Wrong recipe name: " + sRecipeInstance);
    dpSet(sRecipeDp+".ProcessInput.State", "");
    return;
  }

  unRecipeFunctions_writeInRecipeLog("Creating the recipe '" + sRecipeInstance + "' of class '" + sRecipeClass + "' ...");
  // Create a string containing the list of DPE|Value for all the recipe elements
  dyn_string dsDpes = unRecipeFunctions_joinDynString(
        recipeObject[fwConfigurationDB_RO_DP_NAME],
        recipeObject[fwConfigurationDB_RO_ELEMENT_NAME], exceptionInfo);
  string sDpeValue = unRecipeFunctions_getDpeValueString(dsDpes, recipeObject[fwConfigurationDB_RO_VALUE]);
  _unRecipeFunctions_createRecipeInstance(sRecipeClass, sRecipeInstance, sRecipeDesc,
										  exceptionInfo, sInitial, sDpeValue);

  if (dynlen(exceptionInfo)) {
    dpSet(sRecipeDp+".ProcessInput.State", "");
    DebugTN(exceptionInfo);
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2]);
    return;
  }
  unRecipeFunctions_writeInRecipeLog("The recipe '" + sRecipeInstance + "' of class '" + sRecipeClass + "' has been created.", TRUE);
  _unRecipeFunctions_loadRecipeInstances(sRecipeClass);
  dpSet(sRecipeDp+".ProcessInput.State", "");
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Delete a recipe instance.
 * @param sRecipeDp - [IN] Data point name of the recipe instance to delete.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_deleteRecipeInstance(string sRecipeDp, dyn_string &exceptionInfo)
{
  int iPos;
  dyn_dyn_mixed recipeObject;
  dyn_string ds;
  dyn_float df;
  string sClassName, sInstanceName;
  bool bInitial;

  // Set the recipe State to "Delete"
  dpSet($sDpName+".ProcessInput.State", UN_RCP_INSTANCE_STATE_DELETE);

  if (!shapeExists("RecipeClasses") || !shapeExists("RecipeInstances") ||
	  !shapeExists("SelectionProgressBar") || !shapeExists("PreviousRecipeDp") )
  {
    dpSet($sDpName+".ProcessInput.State", "");
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return;
  }

  _unRecipeFunctions_getRecipeObjectFromDp(sRecipeDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    dpSet($sDpName+".ProcessInput.State", "");
    DebugTN(exceptionInfo);
    return;
  }

  // Get the inital recipe value of the recipe instance
  bInitial = recipeObject[fwConfigurationDB_RO_META_PREDEFINED][1];
  if (bInitial) {
    unRecipeFunctions_writeInRecipeLog("Error: The initial recipes can't be deleted.");
    dpSet(sRecipeDp+".ProcessInput.State", "");
    return;
  }

  sClassName = recipeObject[fwConfigurationDB_RO_META_CLASSNAME];
  sInstanceName = recipeObject[fwConfigurationDB_RO_META_ORIGNAME];
  if ((iPos=strpos(sInstanceName, "/")) >=0 ) {
    sInstanceName = substr(sInstanceName, iPos+1);
  }

  // Asks for user confirmation
  ChildPanelOnCentralReturn("fwGeneral/fwOkCancel.pnl",
							"Delete Recipe",
							makeDynString("$sTitle:Delete recipe ",
										  "$text: Do you want to delete the recipe " + sClassName + "/" + sInstanceName + "?"  ),
							df,
							ds);

  if (df[1]<=0)  {
    dpSet(sRecipeDp+".ProcessInput.State", "");
    return;
  }

  setValue("LoadedRecipeDp", "text", "");
  unSelectDeselectHMI_select(sRecipeDp, false, exceptionInfo);
  unProgressBar_setPosition("SelectionProgressBar", 0);

  _unRecipeFunctions_deleteRecipeInstances(makeDynString(sRecipeDp));
  _unRecipeFunctions_clearInstanceTables();
  _unRecipeFunctions_loadRecipeInstances(sClassName);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Delete a list of recipe instances.
 * @param dsRecipeDps   - [IN] Data point names of the recipes to delete.
 */
public bool _unRecipeFunctions_deleteRecipeInstances(dyn_string dsRecipeDps) {
  int iLen, iPos, rc;
  bool bInitial, bDeleteOk = TRUE;
  string sRecipeDp, sClassName, sInstanceName, sPvssDpe, sSystemName;
  dyn_string recipeInstances, exceptionInfo;
  dyn_dyn_mixed recipeObject;

  unImportDevice_initialize();

  iLen = dynlen(dsRecipeDps);
  for (int i=1; i<=iLen; i++) {
    sRecipeDp = dsRecipeDps[i];
    sSystemName = unGenericDpFunctions_getSystemName(sRecipeDp);

    // Set the recipe State to "Delete"
    dpSet(sRecipeDp+".ProcessInput.State", UN_RCP_INSTANCE_STATE_DELETE);

    _unRecipeFunctions_getRecipeObjectFromDp(sRecipeDp, recipeObject, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      dpSet(sRecipeDp+".ProcessInput.State", "");
      unRecipeFunctions_writeInRecipeLog("Exception getting the recipe instance data: " + exceptionInfo[2], true, true);
      dynClear(exceptionInfo);
      bDeleteOk = FALSE;
      continue;
    }

    sClassName = recipeObject[fwConfigurationDB_RO_META_CLASSNAME];
    sInstanceName = recipeObject[fwConfigurationDB_RO_META_ORIGNAME];
    if ((iPos=strpos(sInstanceName, "/")) >=0 ) {
      sInstanceName = substr(sInstanceName, iPos+1);
    }

    if (sSystemName != getSystemName()) {
      unRecipeFunctions_writeInRecipeLog("Error: The recipe '" + sClassName + " / " + sInstanceName
            + "' is defined in a remote system and it can not be deleted.", true, true);
      dpSet(sRecipeDp+".ProcessInput.State", "");
      continue;
    }

    // Get the initial recipe value of the recipe instance
    bInitial = recipeObject[fwConfigurationDB_RO_META_PREDEFINED][1];
    if (bInitial) {
      unRecipeFunctions_writeInRecipeLog("Error: The initial recipe '" + sClassName + " / " + sInstanceName + "' can't be deleted.");
      dpSet(sRecipeDp+".ProcessInput.State", "");
      continue;
    }

    unRecipeFunctions_writeInRecipeLog("Deleting recipe '" + sClassName + " / " + sInstanceName + "' ...");
    // Delete the recipe instance from the fwConfigurationDB
    fwConfigurationDB_deleteRecipeOfClass(sInstanceName, sClassName, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Error deleting the recipe '" + sClassName + " / " + sInstanceName + "' : " + exceptionInfo[2], true, true);
      dpSet(sRecipeDp+".ProcessInput.State", "");
      dynClear(exceptionInfo);
      bDeleteOk = FALSE;
      continue;
    }

    // Get the list of recipe instances of the recipe class
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sClassName, sPvssDpe);
    rc = dpGet(sPvssDpe+"ProcessInput.Recipes",recipeInstances);
    if (rc) {
      dpSet(sRecipeDp+".ProcessInput.State", "");

      return FALSE;
    }

    // Remove the recipe instance of the list of recipes that belong to the recipe class
    if ((iPos=dynContains(recipeInstances, sInstanceName)) > 0) {
      dynRemove(recipeInstances, iPos);
      rc = dpSet(sPvssDpe+"ProcessInput.Recipes", recipeInstances);
      if (rc) {
        dpSet(sRecipeDp+".ProcessInput.State", "");
        bDeleteOk = FALSE;
        continue;
      }
    }

    // Delete the recipe instance
    rc = dpDelete(sRecipeDp);
    if (rc < 0) {
      dpSet(sRecipeDp+".ProcessInput.State", "");
      bDeleteOk = FALSE;
      continue;
    }

    unRecipeFunctions_writeInRecipeLog("The recipe '" + recipeObject[fwConfigurationDB_RO_META_ORIGNAME] + "' has been deleted.", TRUE);
  }

  unImportDevice_triggerUpdate();
  return bDeleteOk;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Show the online values in the recipe instance panel for the specified recipe instance.
 * @param sRecipeDpName - [IN] String containing the recipe instance DP name.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_displayOnlineValues(string sRecipeDpName, dyn_string &exceptionInfo)
{
  dyn_string dsDpNames;
  dyn_anytype daOnlineValues;

  if (!shapeExists("RecipeElementsCopy")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    DebugTN(exceptionInfo);
    return;
  }

  unRecipeFunctions_normalizeDp(sRecipeDpName);
  unRecipeFunctions_writeInRecipeLog("Loading recipe online values.", true);
  dpSet(sRecipeDpName + ".ProcessInput.State", UN_RCP_INSTANCE_STATE_LOADING_VALUES);
  getValue("RecipeElementsCopy", "getColumnN", 1, dsDpNames);

  unRecipeFunctions_getOnlineValues(dsDpNames, daOnlineValues, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return;
  }

  unRecipeFunctions_editRecipeValuesAndShowDifferences(daOnlineValues, exceptionInfo);
  _unRecipeFunctions_checkRecipeValues(dsDpNames, daOnlineValues, TRUE, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true );
  }

  dpSet(sRecipeDpName + ".ProcessInput.State", UN_RCP_INSTANCE_STATE_EDIT);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Edit the values of the recipe displayed in the table and highlight the values that are different.
 * @param daValues      - [IN]  Recipe values.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_editRecipeValuesAndShowDifferences(dyn_anytype daValues, dyn_string &exceptionInfo) {
  int iIndexCol, iDpCol, iAliasCol, iDescriptionCol, iValueCol, iUnitCol, iRangeCol;
  dyn_string dsDpNames, dsAlias, dsDescription, dsUnit, dsRange;
  dyn_anytype daRecipeValues;
  dyn_int diIndex, diRowStyles, diRowStyleIndex, diDiffRowIndex;

  if (!shapeExists("RecipeElements") || !shapeExists("RecipeElementsCopy")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    DebugTN(exceptionInfo);
    return;
  }

  getMultiValue("RecipeElementsCopy", "nameToColumn", "Index", iIndexCol,
                "RecipeElementsCopy", "nameToColumn", "DeviceDp", iDpCol,
                "RecipeElementsCopy", "nameToColumn", "Alias", iAliasCol,
                "RecipeElementsCopy", "nameToColumn", "Description", iDescriptionCol,
                "RecipeElementsCopy", "nameToColumn", "Value", iValueCol,
                "RecipeElementsCopy", "nameToColumn", "Unit", iUnitCol,
                "RecipeElementsCopy", "nameToColumn", "Range", iRangeCol,
                "RecipeElementsCopy", "getColumnN", iIndexCol, diIndex,
                "RecipeElementsCopy", "getColumnN", iDpCol, dsDpNames,
                "RecipeElementsCopy", "getColumnN", iAliasCol, dsAlias,
                "RecipeElementsCopy", "getColumnN", iDescriptionCol, dsDescription,
                "RecipeElementsCopy", "getColumnN", iValueCol, daRecipeValues,
                "RecipeElementsCopy", "getColumnN", iUnitCol, dsUnit,
                "RecipeElementsCopy", "getColumnN", iRangeCol, dsRange);

  _unRecipeFunctions_getRowStylesForValueComparison(daRecipeValues, daValues, diRowStyleIndex, diRowStyles, diDiffRowIndex);
  setMultiValue("RecipeElements", "deleteAllLines",
     "RecipeElements", "appendLines", dynlen(daRecipeValues),
     "Index", diIndex,
     "DeviceDp", dsDpNames,
     "Alias", dsAlias,
     "Description", dsDescription,
     "Value", daValues,
     "Unit", dsUnit,
     "Range", dsRange);

  recipeElementsFilter.updateCountersAndToggle(); // reset the counters of the recipe elements filter

  _unRecipeFunctions_setTableColumnBGColor("RecipeElements", "Value", CELL_EDIT_COLOR);
  setMultiValue("RecipeElements", "rowFontType", diRowStyleIndex, diRowStyles);
  recipeElementsFilter.updateDiff(diRowStyleIndex, diRowStyles);

  unRecipeFunctions_writeDifferentOnlineValuesReport(diDiffRowIndex, dsDpNames, dsAlias, daRecipeValues, daValues);
  unRecipeFunctions_writeInRecipeLog("Recipe values loaded.", true);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Selects the specified recipe instance in the recipe instance panel.
 * @param sRcpInstanceDp - [IN] Datapoint of the recipe instance which will be selected.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return True if the recipe instance was selected, otherwise False.
 */
public bool unRecipeFunctions_selectRecipeInstance(string sRcpInstanceDp, dyn_string &exceptionInfo)
{
  int iPos, iSelected;
  dyn_int selectedLines;
  string sButtonPanel, sClassName, sInstanceName, sLoadedRcpDp, sState;
  string sRcpClassDp, sPcoLink, sSelectedPco;
  dyn_string rcpClassesLoaded, rcpInstancesDp;

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeClasses") || !shapeExists("RecipeInstances") ||
      !shapeExists("RecipeClassLabel") || !shapeExists("RecipeInstanceLabel")) {
  	fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
	  return false;
  }

  // Check if the DP name exists
  if (!dpExists(sRcpInstanceDp)) {
    fwException_raise(exceptionInfo, "ERROR", "The DP name " + sRcpInstanceDp + " doesn't exist.","");
    return false;
  }

  // Get the recipe class data from the recipe instance datapoint
  _unRecipeFunctions_getRecipeClassDataFromInstance(sRcpInstanceDp, sClassName, sRcpClassDp, sPcoLink);

  // Get the recipe instance name
  dpGet(sRcpInstanceDp+".ProcessInput.InstanceName", sInstanceName);
  // Selects the PCO link in the combo box (if necessary)
  getValue("PcoList", "selectedText", sSelectedPco);
  if (sPcoLink != sSelectedPco) {
    string sPcoDp, sApplication;
    unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sPcoLink, sPcoDp);
    sApplication = unGenericDpFunctions_getApplication(sRcpInstanceDp);
    unRecipeFunctions_selectPcoRecipes(sPcoDp, exceptionInfo, false, sApplication);
  }

  // Get the recipe classes loaded in the combo box
  getValue("RecipeClasses", "items", rcpClassesLoaded);
  iPos = dynContains(rcpClassesLoaded, sClassName);
  if (iPos<=0) {
    // The recipe class of the recipe instance doesn't exist!
    iPos = dynContains(rcpClassesLoaded, "-None-");
    if (iPos<=0) {
      setValue("RecipeClasses", "appendItem", "-None-");
      setValue("RecipeClasses", "selectedPos", dynlen(rcpClassesLoaded)+1);
    } else {
      setValue("RecipeClasses", "selectedPos", iPos);
    }
    return false;
  }

  getMultiValue("RecipeClasses", "items", rcpClassesLoaded,
			    "RecipeClasses", "selectedPos", iSelected);
  iPos = dynContains(rcpClassesLoaded, sClassName);

  // Selects the recipe class in the combo box
  if (iPos!=iSelected) {
    setValue("RecipeClasses", "selectedPos", iPos);
    _unRecipeFunctions_loadRecipeInstances(sClassName);
  }

  // Selects the recipe instance in the table
  getMultiValue("RecipeInstances", "getSelectedLines", selectedLines,
				"RecipeInstances", "getColumnN", 1, rcpInstancesDp,
				"LoadedRecipeDp", "text", sLoadedRcpDp);
  iPos = dynContains(rcpInstancesDp, sRcpInstanceDp);
  if (iPos<=0) {
    // The required recipe doesn't exist, an error occurred in the dynContains method
    // or there aren't selected lines in the table
    return false;
  }

  if (dynlen(selectedLines)<=0 || (iPos-1)!=selectedLines[1] || sLoadedRcpDp=="" || sLoadedRcpDp!=sRcpInstanceDp) {
	  setValue("RecipeInstances", "selectLineN", (iPos-1));

	  // add  device action
      if (shapeExists("buttonActivate")) {
        removeSymbol(myModuleName(), myPanelName(), "RCPButton");
        removeSymbol(myModuleName(), myPanelName(), "RCPElementsTable");
      }

	  // get the button panel of the device from UNICOS configuration
	  unGenericDpFunctions_getContextPanel(sRcpInstanceDp, sButtonPanel, exceptionInfo);
      if(dynlen(exceptionInfo)) {
        DebugTN(exceptionInfo);
        return false;
      }

      setMultiValue("RecipeClassLabel", "text", sClassName,
					"RecipeInstanceLabel", "text", sInstanceName,
					"LoadedRecipeDp", "text", sRcpInstanceDp);

	  addSymbol(myModuleName(), myPanelName(), sButtonPanel, "RCPButton", makeDynString("$sDpName:"+sRcpInstanceDp), 62, 756, 0, 1, 1);
	  addSymbol(myModuleName(), myPanelName(), "vision/unRecipe/UnRcpInstance/unUnRcpInstance_RecipeElements.pnl",
				"RCPElementsTable", makeDynString(), 190, 300, 0, 1, 1);

	  unRecipeFunctions_loadRecipeInstanceData();
  }

  // Forces the reload of the recipe state to animate the Select button
  dpGet(sRcpInstanceDp+".ProcessInput.State", sState);
  dpSet(sRcpInstanceDp+".ProcessInput.State", sState);

  // Animate the Selection Progress Bar
  startThread("unRecipeFunctions_animateSelectionProgressBar", sRcpInstanceDp);

  return true;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Function to select the PCO recipes in the recipe instance panel.
 * @param sPcoDp - [IN] DP Name of the PCO.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @param bSelectFirstInstance - [IN] Flag to specify if the first recipe must be selected (TRUE by default).
 * @param sApplication - [IN] Application name where the PCOs must be loaded.
 */
public void unRecipeFunctions_selectPcoRecipes(string sPcoDp, dyn_string &exceptionInfo, bool bSelectFirstInstance=true, string sApplication="*") {
  int iNumRecipes;
  string sPcoAlias, sRcpClassName, sRcpInstanceDp;
  dyn_string dsRcpClasses;

  if (!shapeExists("PcoList") || !shapeExists("RecipeInstances") || !shapeExists("PcoLabel")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return;
  }

  unRecipeFunctions_normalizeDp(sPcoDp);
  sPcoDp = sPcoDp + ".";

  sPcoAlias = unGenericDpFunctions_getAlias(sPcoDp);
  PcoLabel.text = sPcoAlias;

  // Get the PCOs loaded in the combo box and select the sPcoAlias if it exists
  _unRecipeFunctions_loadPcosWithRecipes(sApplication);
  if (!unRecipeFunctions_selectItemInComboBox("PcoList", sPcoAlias)) {
    return;
  }

  // Load the recipe classes for the selected PCO
  unRecipeFunctions_loadRecipeClasses(sPcoAlias, exceptionInfo);

  // Load the recipe instances of the selected recipe class
  getValue("RecipeClasses", "items", dsRcpClasses);
  sRcpClassName = dsRcpClasses[1];
  if (sRcpClassName == "-None-") {
    setValue("RecipeInstances", "deleteAllLines");
    return;
  }

  _unRecipeFunctions_loadRecipeInstances(sRcpClassName);

  // Select the first recipe instance (if it exists)
  getValue("RecipeInstances", "lineCount", iNumRecipes);
  if (bSelectFirstInstance && iNumRecipes>0) {
    getValue("RecipeInstances", "cellValueRC", 0, "RecipeInstancesDP", sRcpInstanceDp);
    unRecipeFunctions_selectRecipeInstance(sRcpInstanceDp, exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Load the recipe instances in the table (recipe instances panel).
 * @param sRecipeClass - [IN] Recipe class name which instances will be loaded.
 */
synchronized public void _unRecipeFunctions_loadRecipeInstances(string sRecipeClass="")
{
  dyn_string dsRcpNames, dsRcpDps, exceptionInfo;

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeInstances") || !shapeExists("RecipeClasses")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return;
  }

  if (sRecipeClass=="") {
    // Get the name of the recipe class selected in the combo box
    getValue("RecipeClasses", "text", sRecipeClass);
    if (sRecipeClass=="") {
      return;
    }
  }

  setValue("RecipeInstances", "deleteAllLines");
  _unRecipeFunctions_getRecipeInstancesOfClass(sRecipeClass, dsRcpNames, dsRcpDps);
  unRecipeFunctions_sortAscDynLists(dsRcpNames, dsRcpDps, exceptionInfo);
  setValue("RecipeInstances", "appendLines", dynlen(dsRcpNames),
           "RecipeInstances", dsRcpNames,
           "RecipeInstancesDP", dsRcpDps);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Load the data of the recipe instance properties table.
 * @param sDpName - [IN] Datapoint element of the recipe instance.
 * @param recipeObject - [IN] Recipe instance data from fwConfigDB.
 */
synchronized public void _unRecipeFunctions_loadPropertiesTableInfo(string sDpName="", dyn_dyn_mixed recipeObject=makeDynMixed()) {
  int iStatus, iRows;
  string sClassName, sSystemName, sClassDp, sCurrentStatus;
  dyn_string message, exceptionInfo;
  dyn_mixed recipeClassObject;

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeProperties")) {
	  fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
	  return;
  }

  // Get the selected recipe DP name
  if (sDpName=="") {
	unRecipeFunctions_getSelectedRecipeDp(sDpName, exceptionInfo);
	if (dynlen(exceptionInfo)>0 || sDpName=="") {
        DebugTN(exceptionInfo);
        return;
    }
  }

  // Loads the recipe object data
  if (dynlen(recipeObject)==0) {
    _unRecipeFunctions_getRecipeObjectFromDp(sDpName, recipeObject, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      DebugTN(exceptionInfo);
      return;
    }
  }

  sSystemName = unGenericDpFunctions_getSystemName(sDpName);
  sClassName  = recipeObject[fwConfigurationDB_RO_META_CLASSNAME];
  if (strpos(sClassName, sSystemName) != 0) {
    sClassName  = sSystemName + recipeObject[fwConfigurationDB_RO_META_CLASSNAME];
  }
  unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sClassName, sClassDp);
  // Loads the recipe class info
  unRecipeFunctions_getRecipeClassObject(sClassDp, recipeClassObject, exceptionInfo);
  if (dynlen(exceptionInfo))  {
    DebugTN(exceptionInfo);
    return;
  }

  // The current activation status will be set to avoid text blinking when this function is called from _unRecipeFunctions_plcStatusCB()
  getValue("RecipeProperties", "lineCount", iRows);
  if (iRows >= 2) {
    getValue("RecipeProperties", "cellValueRC", 2, "Value", sCurrentStatus);
  }

  // Fill the data in the recipe properties table.
  setValue("RecipeProperties", "deleteAllLines");
  dyn_string names = makeDynString("Class:", "Last Activated:", "Status:", "Class Desc.:", "Rcp. Desc.:");
  dyn_string values= makeDynString(recipeClassObject[fwConfigurationDB_RCL_CLASSNAME],
                                   recipeClassObject[fwConfigurationDB_RCL_LAST_ACTIVATED_RECIPE],
                                   sCurrentStatus,
                                   recipeClassObject[fwConfigurationDB_RCL_DESCRIPTION],
								   recipeObject[fwConfigurationDB_RO_META_COMMENT]);
  setValue("RecipeProperties", "appendLines", 5,
           "Name", names,
           "Value", values);

  // Get the recipe instance status
  unRecipeFunctions_getRecipeStatus(recipeObject,
                                    recipeClassObject[fwConfigurationDB_RCL_LAST_ACTIVATED_RECIPE],
                                    iStatus,
                                    message,
                                    exceptionInfo);

  unRecipeFunctions_setRecipeStatusInfo(iStatus, message);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Set the recipe status in the RecipeProperties table.
 * @param status      - [IN] New status of the recipe instance.
 * @param message     - [IN] Contains a message to display when there is no PLC connection.
 */
synchronized public void unRecipeFunctions_setRecipeStatusInfo(int status, dyn_string message=makeDynString())
{
  string sCurrentStatus, sNewStatus, sNewColor;
  dyn_string exceptionInfo;

  // Check that all the necessary shapes exist
  if (!shapeExists("StatusColor") || !shapeExists("buttonActivate") || !shapeExists("RecipeProperties")) {
    fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
    return;
  }

  StatusColor.visible(true);
  getValue("RecipeProperties", "cellValueRC", 2, "Value", sCurrentStatus);

  switch(status){
    case RCP_NO_PLC_CONNECTION:
      sNewStatus = "     [No connection to PLCs: "+message+"]";
      sNewColor  = CELL_ERROR_COLOR;
      break;
    case RCP_BAD_STRUCTURE:
      sNewStatus = "     [Wrong recipe structure]";
      sNewColor  = CELL_ERROR_COLOR;
      break;
    case RCP_BAD_TYPES:
      sNewStatus = "     [Wrong value types]";
      sNewColor  = CELL_ERROR_COLOR;
      break;
    case RCP_BAD_DPES:
      sNewStatus = "     [Wrong recipe elements]";
      sNewColor  = CELL_ERROR_COLOR;
      break;
    case RCP_INACTIVE:
      sNewStatus = "     [Inactive]";
      sNewColor  = INACTIVE_RCP_COLOR;
      break;
    case RCP_ACTIVATED:
      sNewStatus = "     [Active]";
      sNewColor  = ACTIVE_RCP_COLOR;
      break;
    case RCP_ACTIVATION_FAILED:
      sNewStatus = "     [Activation failed]";
      sNewColor  = CELL_ERROR_COLOR;
      break;
    case RCP_ACTIVATION_TRIGGERED:
      sNewStatus = "     [Activating]";
      sNewColor  = INACTIVE_RCP_COLOR;
      break;
  }

  if (sNewStatus == sCurrentStatus) {
    return;
  }

  StatusColor.backCol(sNewColor);
  setValue("RecipeProperties", "cellValueRC", 2, "Value", sNewStatus);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Load the recipe instance data in the panel.
 */
synchronized public void unRecipeFunctions_loadRecipeInstanceData() {
  string sDpName, sClassName, sInstanceName;
  dyn_int diIndex;
  dyn_string exceptionInfo, dsDeviceDps, dsDeviceAlias, dsDescriptions, dsUnits, dsRanges, dsErrorIndex;
  dyn_dyn_mixed recipeObject;

  // Get the selected recipe DP name
  unRecipeFunctions_getSelectedRecipeDp(sDpName, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error loading the recipe: " + exceptionInfo[2]);
    DebugTN(exceptionInfo);
    return;
  } else if (sDpName=="") {
    unRecipeFunctions_writeInRecipeLog("Error loading the recipe: The DP Name is empty");
    return;
  }

  _unRecipeFunctions_getRecipeObjectFromDp(sDpName, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error loading the recipe: " +exceptionInfo[2]);
    DebugTN(exceptionInfo);
    return;
  }

  // Load the RecipeProperties table
  _unRecipeFunctions_loadPropertiesTableInfo(sDpName, recipeObject);

  // Load the History table
  _unRecipeFunctions_loadHistoryTableInfo(sDpName, recipeObject);

  // Inserts the recipe elements in the table
  int len = dynlen(recipeObject[fwConfigurationDB_RO_DPE_NAME]);
  setMultiValue("RecipeElements", "enabled", FALSE,
                "RecipeElements", "deleteAllLines");

  for (int i=1; i<=len; i++) {
    dyn_string tmp;
    string dpName, alias, description, dpe, unit="", range="";

    dpName = recipeObject[fwConfigurationDB_RO_DPE_NAME][i];
    alias = recipeObject[fwConfigurationDB_RO_DP_NAME][i];
    tmp = strsplit(dpName, ".");
    dpe = tmp[dynlen(tmp)];

    if (dpExists(dpName)) {
        description = unGenericDpFunctions_getDescription(unGenericDpFunctions_getDpName(dpName)) + " : " + dpe;
        range = unRecipeFunctions_getDeviceRange(recipeObject[fwConfigurationDB_RO_DPE_NAME][i], exceptionInfo);
        unit = unRecipeFunctions_getDpeUnit(dpName, exceptionInfo);
    } else {
        dynAppend(dsErrorIndex, i);
        description = " : " + dpe;
        range = "";
        unit  = "";
    }

    dynAppend(diIndex, i);
    dynAppend(dsDeviceDps, dpName);
    dynAppend(dsDeviceAlias, alias);
    dynAppend(dsDescriptions, description);
    dynAppend(dsUnits, unit);
    dynAppend(dsRanges, range);
  }

  dyn_anytype formattedValues;
  unRecipeFunctions_formatValues(dsDeviceDps, recipeObject[fwConfigurationDB_RO_VALUE],
                                  formattedValues, exceptionInfo);

  if (len>0) {
	  setValue("RecipeElements", "appendLines", len,
				 "Index", diIndex,
				 "DeviceDp", dsDeviceDps,
				 "Alias", dsDeviceAlias,
				 "Description", dsDescriptions,
				 "Value",  formattedValues,
				 "Unit", dsUnits,
				 "Range", dsRanges);

      _unRecipeFunctions_copyRecipeInstanceToHiddenTable();
  }

  recipeElementsFilter.initRecipeElementsShape();
  recipeElementsFilter.updateCountersAndToggle();

  string sInitial = recipeObject[fwConfigurationDB_RO_META_PREDEFINED];
  setValue("RecipeInitialLabel", "text", sInitial);
  _unRecipeFunctions_resetRecipeElementsBgColor();

  // Check if the recipe has invalid elements
  if (dynlen(dsErrorIndex)) {
    // Set the recipe instance in INVALID state
    dpSet(sDpName+".ProcessInput.State", UN_RCP_INSTANCE_STATE_INVALID);
    dpGet(sDpName+".ProcessInput.ClassName", sClassName,
          sDpName+".ProcessInput.InstanceName", sInstanceName);
    _unRecipeFunctions_setInvalidRows(dsErrorIndex);
    unRecipeFunctions_writeInRecipeLog("Error: Some elements of the recipe instance '" + sInstanceName + "' do not exist." );
    unRecipeFunctions_writeInRecipeLog("Please edit the recipe class '" + sClassName + "' to remove the missing recipe elements.");
  }

  setMultiValue("RecipeElements", "columnHeader", 0, "Index",
				"RecipeElements", "enabled", TRUE,
				"RecipeElements", "adjustColumn", 2);

  // Check the range of the recipe values
  _unRecipeFunctions_checkRecipeValues (dsDeviceDps, recipeObject[fwConfigurationDB_RO_VALUE], TRUE, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    unRecipeFunctions_writeInRecipeLog("Error: " + exceptionInfo[2], true, true );
  }
  _unRecipeFunctions_enableRecipeInstanceWarning(false);
}

//------------------------------------------------------------------------------------------------------------------------
/**
  * Load the recipe instance data in the history table.
  * @param sDpName - [IN] Datapoint element of the recipe instance.
  * @param recipeObject - [IN] Contents of the recipe instance from fwConfigDB.
  */
synchronized public void _unRecipeFunctions_loadHistoryTableInfo(string sDpName="", dyn_dyn_mixed recipeObject=makeDynMixed())
{
  string sUser, sTime;
  dyn_string exceptionInfo;

  // Check that all the necessary shapes exist
  if (!shapeExists("RecipeInstances") || !shapeExists("History") || !shapeExists("RecipeClasses")) {
	  fwException_raise(exceptionInfo, "ERROR", "Some graphical shapes are missing.","");
	  return;
  }

  // Get the selected recipe DP name
  if (sDpName=="") {
	unRecipeFunctions_getSelectedRecipeDp(sDpName, exceptionInfo);
	if (dynlen(exceptionInfo)>0 || sDpName=="") {
      DebugTN(exceptionInfo);
      return;
    }
  }

  // Loads the recipe object data
  if (dynlen(recipeObject)==0) {
	_unRecipeFunctions_getRecipeObjectFromDp(sDpName, recipeObject, exceptionInfo);
	if (dynlen(exceptionInfo)) {
      DebugTN(exceptionInfo);
      return;
    }
  }

  unRecipeFunctions_loadRecipeMetadataFromDb(sDpName, sUser, sTime);

  setValue("History", "deleteAllLines");
  dyn_string col1 = makeDynString("Creator:", "Last modifier:", "Last activator:", "Last saved in DB by:");
  dyn_string col2 = makeDynString(recipeObject[fwConfigurationDB_RO_META_AUTHOR],
                                  recipeObject[fwConfigurationDB_RO_META_LASTMODIFICATIONUSER],
                                  recipeObject[fwConfigurationDB_RO_META_LASTACTIVATIONUSER],
                                  sUser);
  dyn_string col3 = makeDynString("Creation time:", "Last modification time:", "Last activation time:", "Last saved in DB time:");
  dyn_string col4 = makeDynString(recipeObject[fwConfigurationDB_RO_META_CREATIONTIME],
                                  recipeObject[fwConfigurationDB_RO_META_LASTMODIFICATIONTIME],
                                  recipeObject[fwConfigurationDB_RO_META_LASTACTIVATIONTIME],
                                  sTime);
  setValue("History", "appendLines", 4,
           "#1", col1,
           "#2", col2,
           "#3", col3,
           "#4", col4);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Remove the recipe instance data from the panel.
 */
synchronized public void _unRecipeFunctions_clearInstanceTables() {
  setMultiValue("RecipeProperties", "deleteAllLines",
				"History", "deleteAllLines",
				"LoadedRecipeDp", "text", "",
				"StatusColor", "visible", false,
				"RecipeClassLabel", "text", "",
				"RecipeInstanceLabel", "text", "",
				"RecipeInitialLabel", "text", ""
				);

  if (shapeExists("RecipeElements"))
  {
  	setValue("RecipeElements", "deleteAllLines");
    recipeElementsFilter.updateCountersAndToggle();
  }
}

//--------------------------------------------------------------------------------------------------------------------------
/**
 * The edition of a cell value has been completed.
 * @param sDpName 	- [IN] The data point name of the recipe instance.
 * @param row	  	- [IN] Row number of the edited cell.
 * @param column  	- [IN] Column name of the edited cell.
 * @param iRowIndex - [IN] Row index in the "Index" column of the table (proper order in the JCOP recipe definition).
 */
public void _unRecipeFunctions_cellEditionFinished(string sDpName, int row, string column, int iRowIndex)
{
	int retVal;
	string newValue;
	dyn_string exceptionInfo, formattedValues;
	dyn_dyn_mixed recipeObject;

	_unRecipeFunctions_getRecipeObjectFromDp (sDpName, recipeObject, exceptionInfo);
	getValue("RecipeElements", "cellValueRC", row, column, newValue);

	// Check if the recipe value is in the defined range.
	retVal = unRecipeFunctions_checkRecipeValue (
					recipeObject[fwConfigurationDB_RO_DPE_NAME][iRowIndex],
					newValue,
					exceptionInfo);

	// If the value is out of range shows an error message and modifies the cell color
	if (retVal == -1) {
	  unRecipeFunctions_writeInRecipeLog("The new value is out of range");
	  setValue("RecipeElements", "cellBackColRC", (row), "Value", CELL_VALUE_OUT_OF_RANGE_COLOR);
	  return;
	} else {
		// Restore the cell color
		setValue("RecipeElements", "cellBackColRC", (row), "Value", CELL_EDIT_COLOR);
	}

	unRecipeFunctions_formatValues(makeDynString(recipeObject[fwConfigurationDB_RO_DPE_NAME][iRowIndex]),
								    makeDynString(newValue), formattedValues, exceptionInfo);

	if (dynlen(exceptionInfo)) {
		// The value introduced is wrong
		unRecipeFunctions_writeInRecipeLog("The introduced value is not valid: '" + newValue + "'.");
		setValue ("RecipeElements", "cellValueRC", row, column, recipeObject[fwConfigurationDB_RO_VALUE][iRowIndex]);
		RecipeElements.rowFontType (row, makeDynInt(0));
    		recipeElementsFilter.updateDiff(makeDynInt(row), makeDynBool(false));
	} else {
		setValue ("RecipeElements", "cellValueRC", row, column, formattedValues[1]);
		if (formattedValues[1] != recipeObject[fwConfigurationDB_RO_VALUE][iRowIndex]) {
			RecipeElements.rowFontType (row, makeDynInt(1));
    			recipeElementsFilter.updateDiff(makeDynInt(row), makeDynBool(true));
		} else {
			RecipeElements.rowFontType (row, makeDynInt(0));
    			recipeElementsFilter.updateDiff(makeDynInt(row), makeDynBool(false));
		}
	}
	g_editingRowNumber = -1;
	g_editingColName = "";
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Modify the font type of the edited rows to Bold.
 * @param sDpName - [IN] The data point name of the edited row.
 */
public void _unRecipeFunctions_highlightEditedValue(string sDpName)
{
	string newValue;
	dyn_string exceptionInfo, formattedValues;
	dyn_dyn_mixed recipeObject;

	if (g_editingRowNumber < 0 || g_editingColName=="") {
		return;
	}

	_unRecipeFunctions_getRecipeObjectFromDp (sDpName, recipeObject, exceptionInfo);
	getValue("RecipeElements", "cellValueRC", g_editingRowNumber, g_editingColName, newValue);

	unRecipeFunctions_formatValues(makeDynString(recipeObject[fwConfigurationDB_RO_DPE_NAME][g_editingRowNumber+1]),
					makeDynString(newValue), formattedValues, exceptionInfo);

	setValue ("RecipeElements", "cellValueRC", g_editingRowNumber, g_editingColName, formattedValues[1]);
	if (formattedValues[1] != recipeObject[fwConfigurationDB_RO_VALUE][g_editingRowNumber+1]) {
		RecipeElements.rowFontType (g_editingRowNumber, makeDynInt(1));
    		recipeElementsFilter.updateDiff(makeDynInt(g_editingRowNumber), makeDynBool(true));
  	}

	g_editingRowNumber = -1;
	g_editingColName = "";
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Disconnect from the PLC status of a recipe instance.
 * @param sRcpInstanceDp - [IN] DP Name of the recipe instance.
 * @param exceptionInfo  - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_disconnectPlcStatus(string sRcpInstanceDp, dyn_string &exceptionInfo)
{
  dyn_string dsRecipeElements, dsPlcList;
  dyn_dyn_mixed recipeObject;
  string sPlcName, sSystemName;
  int rc;

  _unRecipeFunctions_getRecipeObjectFromDp(sRcpInstanceDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return;
  }
  dsRecipeElements = recipeObject[fwConfigurationDB_RO_DPE_NAME];

  // Get the list of PLCs used in the recipe
  for (int i=1; i<=dynlen(dsRecipeElements); i++){
     sSystemName = unGenericDpFunctions_getSystemName(dsRecipeElements[i]);
     sPlcName    = sSystemName + unGenericObject_GetPLCNameFromDpName(dsRecipeElements[i]);
     if (!dynContains(dsPlcList, sPlcName)){
       dynAppend(dsPlcList, sPlcName);
     }
  }

  // connect to PLC status
  for (int i=1; i<=dynlen(dsPlcList); i++){
    sSystemName = unGenericDpFunctions_getSystemName(dsPlcList[i]);
    sPlcName    = substr(dsPlcList[i], strlen(sSystemName));
    rc = dpDisconnect("_unRecipeFunctions_plcStatusCB",
			  sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".alarm",
			  sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".enabled" );
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Establish a DP connection to the PLC status of all the PLCs containing devices of the recipe instance.
 * @param sRcpInstanceDp - [IN] DP Name of the recipe instance.
 * @param exceptionInfo  - [OUT] Standard exception handling variable.
 */
public void unRecipeFunctions_connectPlcStatus(string sRcpInstanceDp, dyn_string &exceptionInfo)
{
  dyn_string dsRecipeElements, dsPlcList;
  dyn_dyn_mixed recipeObject;
  string sPlcName, sSystemName;

  _unRecipeFunctions_getRecipeObjectFromDp(sRcpInstanceDp, recipeObject, exceptionInfo);
  if (dynlen(exceptionInfo)) {
    DebugTN(exceptionInfo);
    return;
  }

  dsRecipeElements = recipeObject[fwConfigurationDB_RO_DPE_NAME];

  // Get the list of PLCs used in the recipe
  for (int i=1; i<=dynlen(dsRecipeElements); i++){
    if (!dpExists(dsRecipeElements[i])) {
        fwException_raise(exceptionInfo, "ERROR", "The recipe element DPE does not exist: " + dsRecipeElements[i], "");
        return;
    }
    sSystemName = unGenericDpFunctions_getSystemName(dsRecipeElements[i]);
    sPlcName = sSystemName + unGenericObject_GetPLCNameFromDpName(dsRecipeElements[i]);

    if (!dynContains(dsPlcList, sPlcName)){
       dynAppend(dsPlcList, sPlcName);
    }
  }

  // connect to PLC status
  for (int i=1; i<=dynlen(dsPlcList); i++){
    sPlcName = dsPlcList[i];
    sSystemName = unGenericDpFunctions_getSystemName(dsRecipeElements[i]);
    sPlcName = substr(sPlcName, strlen(sSystemName));
	dpConnect("_unRecipeFunctions_plcStatusCB",
			  sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".alarm",
			  sSystemName + c_unSystemAlarm_dpPattern + PLC_DS_pattern + sPlcName + ".enabled");
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Export multiple recipes to a file (comma separated)
 * @param dsRcpInstList [IN] List of recipe names to export.
 */
public void unRecipeFunctions_exportMultipleRecipes(dyn_string dsRcpInstList) {
  string sExtension, sPath, sFileType, sFileName, sRcpFileName;
  dyn_string exceptionInfo;

  if (!dynlen(dsRcpInstList)) {
    unRecipeFunctions_writeInRecipeLog("No recipe instances selected to export.");
    return;
  }

  // 1. Select file
  if (_WIN32) {
    sFileType="xls";
  } else {
    sFileType="txt";
  }

  sExtension="." + sFileType;
  sPath=getPath(DATA_REL_PATH);
  if (folderSelector(sPath) == 0) {
    return;
  }

  int iLen = dynlen(dsRcpInstList);
  for (int i=1; i<=iLen; i++) {
    string sRcpName, sDpName;
    dyn_dyn_mixed recipeObject;

    sDpName  = dsRcpInstList[i];
    sRcpName = dpGetAlias(sDpName + ".");
    _unRecipeFunctions_getRecipeObjectFromDp(sDpName, recipeObject, exceptionInfo);
    if (dynlen(exceptionInfo)) {
      unRecipeFunctions_writeInRecipeLog("Error loading the recipe : " +exceptionInfo[2]);
      DebugTN(exceptionInfo);
      continue;
    }

    sRcpFileName = sRcpName;
    strreplace(sRcpFileName, "/", "_");
    sFileName = sPath + "/" + sRcpFileName + sExtension;

    _unRecipeFunctions_saveRecipeElementsToFile(sFileName, sRcpName, recipeObject);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Saves the recipe elements to a file.
 * @param sFileName [IN] The file name where to save the recipe elements.
 * @param recipeObject [IN] Recipe object to be saved.
 */
private void _unRecipeFunctions_saveRecipeElementsToFile(string sFileName, string sRcpName, dyn_dyn_mixed recipeObject) {
  file f = fopen(sFileName, "w");
  if (f == 0) {
    unRecipeFunctions_writeInRecipeLog("The file " + sFileName + " could not be opened for writing");
    return;
  }

  int jLen = dynlen(recipeObject[fwConfigurationDB_RO_DP_NAME]);
  for (int j=1; j<=jLen; j++) {
    dyn_string tmp, exceptionInfo;
    string dpName, alias, description = " : ", dpe, unit="", range="";

    dpName = recipeObject[fwConfigurationDB_RO_DPE_NAME][j];
    alias = recipeObject[fwConfigurationDB_RO_DP_NAME][j];
    tmp = strsplit(dpName, ".");
    dpe = tmp[dynlen(tmp)];

    if (dpExists(dpName)) {
      description = unGenericDpFunctions_getDescription(unGenericDpFunctions_getDpName(dpName)) + " : " + dpe;
      range = unRecipeFunctions_getDeviceRange(recipeObject[fwConfigurationDB_RO_DPE_NAME][j], exceptionInfo);
      unit = unRecipeFunctions_getDpeUnit(dpName, exceptionInfo);
    }

    int chars = fprintf(f,"%d\t%s\t%s\t%s\t%s\t%s\n", j, alias, description, recipeObject[fwConfigurationDB_RO_VALUE][j], unit, range);
    if (chars<0 || chars==EOF) {
      unRecipeFunctions_writeInRecipeLog("Error writing to file: " + sFileName);
      break;
    }
  }

  fclose(f);
  unRecipeFunctions_writeInRecipeLog("Recipe instance '" + sRcpName + "' saved to: " + sFileName);
}

