/**
 * UNICOS
 * Copyright (C) CERN 2017 All rights reserved
 */
/**@file

// unRcpFunctions_utilities.ctl
This file contains common utility functions.

@par Creation Date
  15/03/2017

@par Modification History
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Ivan Prieto Barreiro (BE-ICS)
*/
#uses "fwGeneral/fwException.ctl"
#uses "unDistributedControl/unDistributedControl.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unExportTree.ctl"
#uses "unSendMessage.ctl"

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Set the bit value in a register variable.
 * @param manRegVal - [IN/OUT] Register where the bit value will be modified.
 * @param position  - [IN] Bit position in the register variable.
 * @param value		- [IN] New value for the bit in the register.
 */
public void unRecipeFunctions_setBit(anytype &manRegVal, int position, bool value) {
  bit64 bitVariable;
  bitVariable = manRegVal;
  setBit(bitVariable, position, value);
  manRegVal = bitVariable;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the bit value in a register variable.
 * @param manRegVal - [IN] Register where the bit value will read.
 * @param position  - [IN] Bit position in the register variable.
 * @param value		- [IN] Value of the bit in the register.
 * @reviewed 2018-08-06 @whitelisted{API}
 */
public void unRecipeFunctions_getBit(anytype manRegVal, int position, int &value) {
  bit64 bitVariable;
  bitVariable = manRegVal;
  value = getBit(bitVariable, position);
}

//------------------------------------------------------------------------------------------------------------------------ 
/** 
 * Auxiliary function to order alphabetically a list (list1) and reorder the second list (list2) in a consistent way.
 * @param list1 - [IN/OUT] List to be ordered alphabetically.
 * @param list2 - [IN/OUT] List to be ordered in the same way as the list1.
 * @param exceptionInfo - [OUT] Standard error handling variable.
 */
public void unRecipeFunctions_sortAscDynLists(dyn_string &list1, dyn_anytype &list2, dyn_string &exceptionInfo) {
  int iLen, iPos;
  dyn_string cpyList1;
  dyn_anytype cpyList2;
  
  if (dynlen(list1) != dynlen(list2)) {
    fwException_raise(exceptionInfo, "ERROR", "The number of elements in the list must be equal","");
	return;
  }  
  
  cpyList1 = list1;    
  dynSortAsc(cpyList1);
  
  iLen = dynlen(cpyList1);
  for (int i=1; i<=iLen; i++) {
    // Check the position of the curren element in the original list1
	iPos = dynContains(list1, cpyList1[i]);
	
	// Place the element of the second list in the proper order
	cpyList2[i] = list2[iPos];
  }
  
  list1 = cpyList1;
  list2 = cpyList2;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the position of the dsSearch elements in the dsItems list.
 * @param dsSearch  - [IN] List of elements to search in the dsItems list.
 * @param dsItems   - [IN] List of items.
 * @param diResult  - [OUT] List containing the positions of the dsSearch elements in the dsItems list.
 */
public void unRecipeFunctions_getDynPos(dyn_string dsSearch, dyn_string dsItems, dyn_int &diResult) {
  int iPos;
  
  for (int i=1; i<=dynlen(dsSearch); i++) {
    iPos = dynContains(dsItems, dsSearch[i]);
    dynAppend(diResult, iPos);
  }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the difference of the daList1 with the daList2.
 * @param daList1   - [IN] First list of elements.
 * @param daList2   - [IN] Second list of elements.
 * @param daResult  - [OUT] List containing the elements of daList1 not contained in daList2.
 */
public void unRecipeFunctions_dynDiff(dyn_anytype daList1, dyn_anytype daList2, dyn_anytype &daResult) {
    for (int i=1; i<=dynlen(daList1); i++) {
        if (dynContains(daList2, daList1[i]) == 0) {
            dynAppend(daResult, daList1[i]);
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Check if a distributed system is connected.
 * @param sSystemName   - [IN] System name.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return TRUE if the system is not remote or if it is a remote and connected, otherwise FALSE.
 */
public bool unRecipeFunctions_isRemoteSystemConnected(string sSystemName, dyn_string &exceptionInfo) {
  bool bIsRemote, bIsConnected;
  
  unDistributedControl_isRemote(bIsRemote, sSystemName);
  if (bIsRemote) {
	unDistributedControl_isConnected(bIsConnected, sSystemName);
	if (!bIsConnected){
    string sError = "The distributed system '" + sSystemName + "' isn't connected.";
		fwException_raise(exceptionInfo, "ERROR", sError, "");
		return false;
	}
  }
  
  return true;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Function to check if the items of two lists are equal.
 * @param list1 - [IN] The first list of elements.
 * @param list2 - [IN] The second list of elements.
 * @return TRUE if the lists contain the same elements, otherwise FALSE.
 */
public bool unRecipeFunctions_areListsEqual(dyn_string list1, dyn_string list2) {
  if (dynlen(list1) != dynlen(list2)) {
    return false;
  }
	
  dynSortAsc(list1);
  dynSortAsc(list2);
  
  int iLen = dynlen(list1);
  for (int i=1; i<=iLen; i++) {
    if (list1[i] != list2[i]) {
      return false;
    }
  }
  
  return true;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Auxiliary function to add values to a mapping where the mapping value is a dynamic list.
 * @param m   		   - [OUT] Mapping where to insert the values.
 * @param key 		   - [IN] Key for the new value.
 * @param value        - [IN] The value to insert.
 * @param bUniqueValue - [IN] If TRUE the values can not be repeated for the same key.
 */
public void unRecipeFunctions_mappingInsertValue(mapping & m, string key, string value, bool bUniqueValue=TRUE) {
  dyn_string dsValues;
  
  if (mappingHasKey(m, key)) {
    dsValues = m[key];
  }
  
  if (bUniqueValue==FALSE || FALSE==dynContains(dsValues, value)) {
    dynAppend(dsValues, value);
  }

  m[key] = dsValues;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Check if a recipe name is legal (for a recipe class or a recipe instance).
 * @param sName - [IN] Recipe name.
 * @return TRUE if the recipe name is legal, otherwise FALSE.
 */
public bool unRecipeFunctions_isLegalRecipeName(string sName) {
  if (!dpIsLegalName(sName) || strpos(sName, "/") >= 0 || strpos(sName, "\\") >=0 || strpos(sName, ".") >= 0 ) {
    return FALSE;
  }
  return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Join the content of two dyn_string variables if they have the same number of elements.
 * @param ds1 - [IN] The first dyn_string variable.
 * @param ds2 - [IN] The second dyn_string variable.
 * @param exceptionInfo - [OUT] Standard exception handling variable.
 * @return New dyn_string with the concatenated elements of the variables ds1 and ds2.
 */
public dyn_string unRecipeFunctions_joinDynString(dyn_string ds1, dyn_string ds2, dyn_string &exceptionInfo) {
  dyn_string dsResult;
  
  if (dynlen(ds1) != dynlen(ds2)) {
    fwException_raise(exceptionInfo, "ERROR", "The number of elements in the lists is different.","");
    return dsResult;
  }
  
  int iLen = dynlen(ds1);
  for (int i=1; i<=iLen; i++) {
    dynAppend(dsResult, ds1[i] + ds2[i]);
  }
  
  return dsResult;
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Compares the elements contained in two lists.
 * @param daList1Orig      - [IN]  First list of elements.
 * @param daList2Orig      - [IN]  Second list of elements.
 * @param daMissingDpNames - [OUT] List of elements contained in daList1 and not contained in daList2.
 * @param daExtraDpNames   - [OUT] List of elements contained in daList2 and not contained in daList1.
 * @param bIgnoreSystemName- [IN] TRUE if the system name must be ignored.
 * @return TRUE if the lists contain the same elements, otherwise FALSE.
 */
public bool unRecipeFunctions_compareListElements(dyn_anytype daList1Orig, dyn_anytype daList2Orig, dyn_anytype &daMissingDpNames, dyn_anytype &daExtraDpNames, bool bIgnoreSystemName=TRUE) {
  int iLen;
  dyn_anytype daList1, daList2;
  
  if (bIgnoreSystemName) {
    daList1 = unRecipeFunctions_removeSystemNameFromList(daList1Orig);
    daList2 = unRecipeFunctions_removeSystemNameFromList(daList2Orig);
  } else {
    daList1 = daList1Orig;
    daList2 = daList2Orig;
  }

  // Look for the elements contained in daList1 and missing in daList2
  iLen = dynlen(daList1);
  for (int i=1; i<=iLen; i++) {
    if (!dynContains(daList2, daList1[i])) {
      dynAppend(daMissingDpNames, daList1Orig[i]);
    }
  }
    
  // Look for the elements contained in daList2 and missing in daList1
  iLen = dynlen(daList2);
  for (int i=1; i<=iLen; i++) {
    if (!dynContains(daList1, daList2[i])) {
      dynAppend(daExtraDpNames, daList2Orig[i]);
    }
  }
  
  return (dynlen(daMissingDpNames) == 0 && dynlen(daExtraDpNames) == 0);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Removes the system name for each element in the daList.
 * @param daList - [IN] List containing the elements.
 * @return New list without the system name.
 */
public dyn_anytype unRecipeFunctions_removeSystemNameFromList(dyn_anytype daList) {
  int iLen = dynlen(daList);
  dyn_anytype daRetList;
  
  for (int i=1; i<=iLen; i++) {
    dynAppend(daRetList, unExportTree_removeSystemName(daList[i]));
  }
  
  return daRetList;
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Replaces all ocurrences of the 'item' by 'replacement' in the 'daList'.
 * @param daList - [IN] List of elements.
 * @param item   - [IN] Item to be replaced in the list.
 * @param replacement - [IN] New item for the replacement.
 */
public void unRecipeFunctions_replaceDynItem(dyn_anytype daList, anytype item, anytype replacement) {
  int iLen=dynlen(daList);
  
  for (int i=1; i<=iLen; i++) {
    if (daList[i] == item) {
      daList[i] = replacement;
    }
  }
}
//------------------------------------------------------------------------------------------------------------------------
/**
 * Print a message in the recipe log containing a list of devices (if any).
 * @param dsDeviceDps - [IN] List of device DPs.
 * @param sMessage    - [IN] Message to print if the list is not empty.
 */
public void unRecipeFunctions_printDeviceList(dyn_string dsDeviceDps, string sMessage) {
  int iLen;
  string sRecipeElement;
  
  iLen = dynlen(dsDeviceDps);
  if (iLen == 0) {
    return;
  }
  
  for (int i=1; i<=iLen; i++) {
    unGenericDpFunctions_convert_PVSSDPE_to_UNICOSDPE(dsDeviceDps[i], sRecipeElement);
    sMessage += "\n" + sRecipeElement;
  }
  unRecipeFunctions_writeInRecipeLog(sMessage, true, true);
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Handle dpGet/dpSet errors.
 * @param err           - [IN] Value returned by dpGet/dpSet
 * @param exceptionInfo - [OUT] Standard exception handling variable. 
 * @return 
 */
public bool unRecipeFunctions_handleDpGetSetError (int err, dyn_string &exceptionInfo) {
  if((err != 0) || (dynlen(getLastError()) > 0)) {
    fwException_raise(exceptionInfo, "ERROR",
                      "Failed to get/set value from/to dpe (dpGet/dpSetWait).",
                      getLastError());
    return true;
  } 

  return false;
}

//------------------------------------------------------------------------------------------------------------------------
/** 
 * Function to normalize a datapoint name (the trailing dot will be removed if it exists).
 * @param sDpName - [IN/OUT] The datapoint name to normalize.
 */
public void unRecipeFunctions_normalizeDp(string &sDpName) {
  int iLen = strlen(sDpName);
  if (iLen==0) {
    return;
  }
  
  if (sDpName[(iLen-1)] == ".") {
    sDpName = substr(sDpName, 0, iLen-1);
  }
}
//------------------------------------------------------------------------------------------------------------------------

/** 
 * Write a message in the recipe log.
 * @param sMessage 			 - [IN] The message to write in the recipe log.
 * @param bWriteInGeneralLog - [IN] Flag to specify if the message must be written in the UNICOS HMI log (false by default).
 * @param bWriteInLogViewer  - [IN] Flag to specify if the message must be written in the WinCC OA log viewer (false by default).
 */
public void unRecipeFunctions_writeInRecipeLog(string sMessage, bool bWriteInGeneralLog=false, bool bWriteInLogViewer=false) {
  int iElements;
  dyn_string exceptionInfo;
 
  // Check that all the necessary shapes exist
  if (shapeExists("RecipeLog")) {
	string sTime = formatTime("[%Y.%m.%d %H:%M:%S]", getCurrentTime());
	RecipeLog.appendItem(sTime + " " + sMessage);
	getValue("RecipeLog", "itemCount", iElements);
	setValue("RecipeLog", "bottomPos", iElements);
  } 

  if (bWriteInGeneralLog) {
    unSendMessage_toAllUsers(sMessage, exceptionInfo);
  }
  
  if (bWriteInLogViewer) {
    DebugTN(sMessage);
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** 
 * Get the min. and max. values defined in a range string
 * @param range - [IN] String containing a range definition (i.e.: [0.0, 100.0])
 * @param min - [OUT] Minimum value defined in the range.
 * @param max - [OUT] Maximum value defined in the range.
 * @param exceptionInfo - [OUT] Standard exception handling routine.
 */
public void unRecipeFunctions_getRangeValues(string range, string &min, string &max, dyn_string &exceptionInfo)
{  
  min = "";
  max = "";
  
  string tmpRange = range;
  strreplace(tmpRange, "[", "");
  strreplace(tmpRange, "]", "");
  strreplace(tmpRange, " ", "");

  dyn_string rangeSplit = strsplit(tmpRange, ",");
  if (dynlen(rangeSplit)!=2)  {
    fwException_raise(exceptionInfo, "ERROR", "The specified range format is incorrect: " + range, "");
    return;
  }

  min = rangeSplit[1];
  max = rangeSplit[2];
  
  strreplace(min, " ", "");
  strreplace(max, " ", "");
  
  min = strrtrim(min);
  min = strltrim(min);
  max = strrtrim(max);
  max = strltrim(max);
}

//------------------------------------------------------------------------------------------------------------------------
/**
 * Get the device aliases from a list of dp names.
 * @param dsDeviceDpNames - [IN]  List of device dp names.
 * @param dsDeviceAliases - [OUT] List of device aliases.
 */
public void unRecipeFunctions_getDeviceAliases(dyn_string dsDeviceDpNames, dyn_string &dsDeviceAliases) {
  int iLen = dynlen(dsDeviceDpNames);
  dynClear(dsDeviceAliases);
  
  for (int i=1; i<=iLen; i++) {
    string sDpName = dsDeviceDpNames[i];
    unRecipeFunctions_normalizeDp(sDpName);
    sDpName += ".";
    
    if (!dpExists(sDpName)) {
      dynAppend(dsDeviceAliases, sDpName);
    } else {
      dynAppend(dsDeviceAliases, unGenericDpFunctions_getAlias(sDpName));
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
