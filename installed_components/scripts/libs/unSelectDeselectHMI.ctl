/**@file

// unSelectDeselectHMI.ctl
This library is used to select/deselect an object and to get the state of the selection. A global variable is used:
g_dpElementSelected to store the last data point element that was selected. There is only one data point element 
selected per manager.

@par Creation Date: 
  07/06/2002

@par Modification History: None
  06/05/2011: Herve
  - IS-524: Allow multiple selection per GUI
  
  05/07/2004: Herve
    - in the function unSelectDeselectHMI_select put the systemName if not included.

  14/07/2002: Herve
    modify the function in order to deselect only the dp that was selected in a module. Keep on selected dp
    per module opened

@par Constraints:
  . data point type needed: _UnSelectDeselect, _UnStatusInformation
  . the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
  must have a data point element of type _UnStatusInformation
  . data point: the following data point is needed, it must exist, no checking is done
    . _unSelectDeselect: of type _UnSelectDeselect 
  . global variable: the following variable are declared:
    . g_dpElementSelected: dyn_string, to store the latest selected element per opened module
    . g_openedModule: dyn_string, to store the opened module
  . constant:
    . c_unSelectDeselectDpName: string
  . PVSS version: 3.0 
  . operating system: NT, W2000 and Linux, but tested only under W2000.
  . distributed system: yes.
  
@par Usage: 
  Public

@par PVSS manager
  Ui

@author: Herve Milcent (LHC-IAS)
*/

#uses "unSelectDeselect/unSelectDeselectDeprecated.ctl"

//@{
// constant declaration
const string c_unSelectDeselectDpName = "_unSelectDeselect"; // data point name for the manager: _unSelectDeselect
// end constant declaration

// global declaration
global dyn_string g_dpElementSelected;
global dyn_string g_openedModule;
global mapping g_m_dsMultiple_DpElementSelected;
// end global declaration

//@}

//@{

//------------------------------------------------------------------------------------------------------------------------
// unSelectDeselectHMI_select
/** This function is used to select or deselect a device, it deselect all the selected dp of the current manager 
  (maximum one per opened module) the select/deselect is done via a call to the _unSelectDeselectHMI_select 
  function. The selected device is kept in g_dpElementSelected for each module opend kept in g_openedModule
  
@par Constraints:
  - data point type needed: _UnStatusInformation
  - the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
  must have a data point element of type _UnStatusInformation
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpElementName input, the dpElement to select
@param bSelect input, true (1): select sDpElementName, false (0) deselect sDpElementName
@param exceptionInfo output, Details of any exceptions are returned here
*/
unSelectDeselectHMI_select(string sDpElementName, bool bSelect, dyn_string &exceptionInfo) 
{
  string dpToSelect, dpSystemName;
  bool isRemote, isConnected;
  
  synchronized(g_openedModule)
  {
    dpToSelect = dpSubStr(sDpElementName, DPSUB_SYS_DP);
    dpSystemName = dpSubStr(sDpElementName, DPSUB_SYS);
    unDistributedControl_isRemote(isRemote, dpSystemName);
    if(isRemote) {
      unDistributedControl_isConnected(isConnected, dpSystemName);
    }
    else
      isConnected = true;
    
    if(isConnected) {
  // deselect all the previoulsy selected dp of the manager (one Dp selected per opened modules)
      _unSelectDeselectHMI_deselectAllExceptDp(dpToSelect, exceptionInfo);

  // select/deselect the requested dp.
      _unSelectDeselectHMI_select(dpToSelect, bSelect, myModuleName(), exceptionInfo);
    }
    else {
      fwException_raise(exceptionInfo, "ERROR", getCatStr("unSelectDeselect", "NOTCONNECTED")+": "+dpSystemName,"");
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unSelectDeselectHMI_multipleSelect
/** This function is used to select or deselect a device. This function is similar to unSelectDeselectHMI_select without deselecting the
  device already selected. The selected devices are kept in g_m_dsMultiple_DpElementSelected for each opened module.
  if bSelect = false and dynlen dsDpElementName <= 0 then this is equivalent to deselect all the DPE selected of the current module
  
@par Constraints:
  - data point type needed: _UnStatusInformation
  - the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
  must have a data point element of type _UnStatusInformation
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param bSelect input, true (1): select sDpElementName, false (0) deselect sDpElementName
@param exceptionInfo output, Details of any exceptions are returned here
@param dsDpElementName input, list of dpElement to select
*/
unSelectDeselectHMI_multipleSelect(bool bSelect, dyn_string &exceptionInfo, dyn_string dsDpElementName=makeDynString()) 
{
  dyn_string dsTempDp=dsDpElementName;
  string sModuleName=myModuleName();
  
  synchronized(g_m_dsMultiple_DpElementSelected)
  {
    if(!bSelect && (dynlen(dsTempDp) <= 0))
    {
      if(mappingHasKey(g_m_dsMultiple_DpElementSelected, sModuleName))
        dsTempDp = g_m_dsMultiple_DpElementSelected[sModuleName];
    }
    _unSelectDeselectHMI_multipleSelect(bSelect, exceptionInfo, dsTempDp, sModuleName);
  }
}

//------------------------------------------------------------------------------------------------------------------------
// _unSelectDeselectHMI_multipleSelect
/** This function is used to select or deselect a device. This function is similar to unSelectDeselectHMI_select without deselecting the
  device already selected. The selected devices are kept in g_m_dsMultiple_DpElementSelected for each opened module.
  if bSelect = false and dynlen dsDpElementName <= 0 then this is equivalent to deselect all the DPE selected of the current module
  
@par Constraints:
  - data point type needed: _UnStatusInformation
  - the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
  must have a data point element of type _UnStatusInformation
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param bSelect input, true (1): select sDpElementName, false (0) deselect sDpElementName
@param exceptionInfo output, Details of any exceptions are returned here
@param dsDpElementName input, list of dpElement to select
@param sModuleName input, the module name
*/
_unSelectDeselectHMI_multipleSelect(bool bSelect, dyn_string &exceptionInfo, dyn_string dsDpElementName, string sModuleName) 
{
  int i, len, iPos;
  string dpToSelect, dpSystemName;
  dyn_string dsCurrentDpSelected, exceptionInfoTemp;
  bool isRemote, isConnected;
  
  len=dynlen(dsDpElementName);
  if(mappingHasKey(g_m_dsMultiple_DpElementSelected, sModuleName))
    dsCurrentDpSelected = g_m_dsMultiple_DpElementSelected[sModuleName];
  for(i=1;i<=len;i++)
  {
    dynClear(exceptionInfoTemp);
    dpSystemName = dpSubStr(dsDpElementName[i], DPSUB_SYS);
    unDistributedControl_isRemote(isRemote, dpSystemName);
    if(isRemote) {
      unDistributedControl_isConnected(isConnected, dpSystemName);
    }
    else
      isConnected = true;
    if(isConnected) {
      if(dpExists(dsDpElementName[i]))
      {
        _unSelectDeselect_select(dsDpElementName[i], bSelect, exceptionInfoTemp);
        unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "SelectDeselect", "_unSelectDeselectHMI_multipleSelect", bSelect, exceptionInfoTemp, dsDpElementName[i], sModuleName);
        if(dynlen(exceptionInfoTemp) <= 0)
        {
          dpToSelect = dpSubStr(dsDpElementName[i], DPSUB_SYS_DP);
          if(bSelect)
            dynAppend(dsCurrentDpSelected, dpToSelect);
          else
          {
            iPos = dynContains(dsCurrentDpSelected, dpToSelect);
            if(iPos > 0)
              dynRemove(dsCurrentDpSelected, iPos);
          }
        }
        else
          dynAppend(exceptionInfo, exceptionInfoTemp);
      }
      else
        fwException_raise(exceptionInfo, "ERROR", getCatStr("unSelectDeselect", "LIBWRONGDP")+ dsDpElementName[i],"");
    }
    else
      fwException_raise(exceptionInfo, "ERROR", "system not connected: "+ dsDpElementName[i],"");
  }
  dynUnique(dsCurrentDpSelected);
  g_m_dsMultiple_DpElementSelected[sModuleName] = dsCurrentDpSelected;
}
//------------------------------------------------------------------------------------------------------------------------
// unSelectDeselectHMI_isLocked
/** This function returns back the state of the lock.
  
@par Constraints:
  - data point type needed: _UnStatusInformation
  - the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
  must have a data point element of type _UnStatusInformation
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpElementName input, the device data point element name
@param bLock output, true (1): the device is selected, false (0): the device is deselected
@param sSelectedManager output, "" if not selected, manNum;systemName if selected, the systeName and manNum are the the ones of the manager that requested the select
*/
unSelectDeselectHMI_isLocked(string sDpElementName, bool &bLock, string &sSelectedManager) 
{
  string dpToSelect;
  
  dpToSelect = dpSubStr(sDpElementName, DPSUB_SYS_DP);
  dpGet(dpToSelect+".statusInformation.selectedManager:_lock._original._locked", bLock,
    dpToSelect+".statusInformation.selectedManager", sSelectedManager);
}

//------------------------------------------------------------------------------------------------------------------------
// unSelectDeselectHMI_deselectOldDp_select
/** This function request a deselect of the previous dp that was selected and a select on the new dp. If the new dp
  to select is "" this is equivalent ot deselect the current selected dp
  
@par Constraints:
  - distributed system: yes.
  
@par Usage: 
  Public

@par PVSS manager
  Ui

Constraints:
  - distributed system: yes.
  
@param sDpName input, the dp name to select
@param exceptionInfo output, Details of any exceptions are returned here
*/
unSelectDeselectHMI_deselectOldDp_select(string sDpName, dyn_string &exceptionInfo) 
{
  int res;
  string sOldDpName;
  
  synchronized(g_openedModule)
  {
    res = _unSelectDeselectHMI_getSet_dpElementSelected(sOldDpName, "", myModuleName());

    if((sOldDpName != "") && (sOldDpName != sDpName)){
      _unSelectDeselectHMI_select(sOldDpName, false, myModuleName(), exceptionInfo);
    }
    if(sDpName != "")
      _unSelectDeselectHMI_select(sDpName, true, myModuleName(), exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unSelectDeselectHMI_deselectAllDp
/** This function deselects all the selected dps.

@par Constraints:
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param exceptionInfo output, Details of any exceptions are returned here
*/
unSelectDeselectHMI_deselectAllDp(dyn_string &exceptionInfo) 
{
  int pos, len, i;
  string dpName; 
  string sModuleName;
  dyn_string dsTempDp;

// deselect all the previoulsy selected dp of the manager (one Dp selected per opened modules)
  synchronized(g_openedModule)
  {
    len = dynlen(g_openedModule);
    for(i=1;i<=len;i++) {
      dpName = g_dpElementSelected[i];
      if(dpName != "")
        _unSelectDeselectHMI_select(dpName, false, g_openedModule[i], exceptionInfo);
    }
  }
  
  synchronized(g_m_dsMultiple_DpElementSelected)
  {
    len = mappinglen(g_m_dsMultiple_DpElementSelected);
    for(i=1;i<=len;i++)
    {
      sModuleName = mappingGetKey(g_m_dsMultiple_DpElementSelected, i);
      dsTempDp = mappingGetValue(g_m_dsMultiple_DpElementSelected, i);
      _unSelectDeselectHMI_multipleSelect(false, exceptionInfo, dsTempDp, sModuleName);
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unSelectDeselectHMI_getSelectedState
/** This function returns the selection state of the device
  
@par Constraints:
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param bLock input, the lock value: 1: device locked/0: device not locked
@param sManager input, manager type, must be "UI" or "CTRL"
@return the selection state, "S": selected by this manager, "D": not selected, "SO": selected by another manager
*/
string unSelectDeselectHMI_getSelectedState(bool bLock, string sManager) 
{
  string localManager, result;
// bLock shows if the device is selected/deselected, sManager shows which manager selected the device
// the convention is: manum;systemName where manum is the manager number of the PVSS manager that did the select
// and systemName is the syat name of the PVSS manager that did the select
// "" is set when the device is deselected.
// if the device is selected by another manager then it is not allowed to select/deselect it.
// refer to unSelectDeselect.ctl
  result = "D";
  if(bLock) {
    localManager = myManNum()+";"+getSystemName();
    if((sManager != localManager) && (sManager != "")){
      result = "SO";
    }
    else
      result = "S";
  }
  return result;
}

//------------------------------------------------------------------------------------------------------------------------
// _unSelectDeselectHMI_select
/** This function is used to select or deselect a device. All the selected device will be de-selected except the requested one 
  The device will be selected if the bSelect is true (1) and deselected if bSelect is false (0). The 
  sDpElementName must be a data point element of the device.

@par Constraints:
  - data point type needed: _UnSelectDeselect, _UnStatusInformation
  - the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
  must have a data point element of type _UnStatusInformation
  - data point: the following data point is needed, it must exist, no checking is done
    . _unSelectDeselect: of type _UnSelectDeselect 
  - distributed system: yes.
  
@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpElementName input, the dpElement to select
@param sType input, manager type, must be "UI" or "CTRL"
@param bSelect input, true (1): select sDpElementName, false (0) deselect sDpElementName
@param sModuleName input, the module name
@param exceptionInfo output, Details of any exceptions are returned here
*/
_unSelectDeselectHMI_select(string sDpElementName, bool bSelect, string sModuleName, dyn_string &exceptionInfo) 
{
  int res;
  string dpToSelect; 

  if(dpExists(sDpElementName))
  {
    _unSelectDeselect_select(sDpElementName, bSelect, exceptionInfo);
  
    dpToSelect = dpSubStr(sDpElementName, DPSUB_SYS_DP);

  //DebugN("_unSelectDeselectHMI_select", dpName, bSelect, dpSystName);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "SelectDeselect", "_unSelectDeselectHMI_select_unSelectDeselectHMI_select", sDpElementName, bSelect, sModuleName, exceptionInfo);
    if (!bSelect) { // if deselect of the object
      unGenericObject_PulseAdaptiveWait(sDpElementName);
      unGraphicalFrame_restoreLastPanelDpContextualPanel(sDpElementName);
    }


    if(dynlen(exceptionInfo) <= 0) {
      if(bSelect) 
        res = _unSelectDeselectHMI_set_dpElementSelected(dpToSelect, sModuleName);
      else
        res = _unSelectDeselectHMI_set_dpElementSelected("", sModuleName);
    }
  }
  else
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unSelectDeselect", "LIBWRONGDP")+ sDpElementName,"");
}

//------------------------------------------------------------------------------------------------------------------------
// _unSelectDeselect_select
/** This function is used to select or deselect a device. 
  The device will be selected if the bSelect is true (1) and deselected if bSelect is false (0). The 
  sDpElementName must be a data point element of the device.

@par Constraints:
  - data point type needed: _UnSelectDeselect, _UnStatusInformation
  - the data point that are given as argument to the functions: unSelectDeselectHMI_select and unSelectDeselectHMI_isLocked
  must have a data point element of type _UnStatusInformation
  - data point: the following data point is needed, it must exist, no checking is done
    . _unSelectDeselect: of type _UnSelectDeselect 
  - distributed system: yes.
  
@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpElementName input, the dpElement to select
@param sType input, manager type, must be "UI" or "CTRL"
@param bSelect input, true (1): select sDpElementName, false (0) deselect sDpElementName
@param exceptionInfo output, Details of any exceptions are returned here
*/
_unSelectDeselect_select(string sDpElementName, bool bSelect, dyn_string &exceptionInfo)
{
  int res;
  string dpName, dpSystName, dpToSelect; 

  dpSystName = dpSubStr(sDpElementName, DPSUB_SYS);
  dpName = dpSystName+c_unSelectDeselectDpName; // data point name for the manager: _unSelectDeselect

  dpToSelect = dpSubStr(sDpElementName, DPSUB_SYS_DP);

  if( (dpSystName!="") && (dpExists(dpName))) {
    dpSet(dpName+".dpElementName", dpToSelect, dpName+".select",bSelect, dpName+".managerId", myManNum()+";"+getSystemName());
  }
  else
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unSelectDeselect", "LIBWRONGDP")+ dpToSelect +", "+dpName,"");
}

//------------------------------------------------------------------------------------------------------------------------
// _unSelectDeselectHMI_set_dpElementSelected
/**  indivisible function: to set the last dp selected.

@par Constraints:
  - global variable: the following variable are declared:
    . g_dpElementSelected: string, to store the latest selected element
    . g_openedModule: dyn_string, to store the opened module
  - distributed system: yes.
  
@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpElementName input, the device data point element name
@param sModuleName input, the module name
@return 0
*/
int _unSelectDeselectHMI_set_dpElementSelected(string sDpElementName, string sModuleName) 
{
  int pos;
  dyn_string split;
  
//DebugN("_unSelectDeselectHMI_set_dpElementSelected", sDpElementName, sModuleName, g_openedModule);
  split = strsplit(sModuleName, ";");
  pos = dynContains(g_openedModule, split[1]);
  if(pos > 0) {
    g_dpElementSelected[pos] = sDpElementName;
  }
  else{
    dynAppend(g_openedModule, split[1]);
    dynAppend(g_dpElementSelected, sDpElementName);
  }
  return 0;
}

//------------------------------------------------------------------------------------------------------------------------
// _unSelectDeselectHMI_getSet_dpElementSelected
/** indivisible function: to get and set the last dp selected.
  
@par Constraints:
  - global variable: the following variable are declared:
    . g_dpElementSelected: string, to store the latest selected element
    . g_openedModule: dyn_string, to store the opened module
  - distributed system: yes.
  
@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpElementSet intput, the device data point element name to set
@param sDpElementGet output, the device data point element name
@param sModuleName input, the module name
@return 0
*/
int _unSelectDeselectHMI_getSet_dpElementSelected(string &sDpElementNameGet, string sDpelementSet, string sModuleName) 
{
  int pos;
  dyn_string split;
  
//DebugN("_unSelectDeselectHMI_getSet_dpElementSelected", sDpelementSet, sModuleName, g_openedModule);
  split = strsplit(sModuleName, ";");
  pos = dynContains(g_openedModule, split[1]);
  if(pos > 0) {
    sDpElementNameGet = g_dpElementSelected[pos];
    g_dpElementSelected[pos] = sDpelementSet;
  }
  else 
    sDpElementNameGet = "";
  return 0;
}

//------------------------------------------------------------------------------------------------------------------------
// _unSelectDeselectHMI_deselectAllExceptDp
/** This function deselects all the selected dps except the given one.
  
@par Constraints
  - distributed system: yes.

@par Usage: 
  Public

@par PVSS manager
  Ui

@param sDpName input, the dp name to not deselect
@param exceptionInfo output, Details of any exceptions are returned here
*/
_unSelectDeselectHMI_deselectAllExceptDp(string sDpName, dyn_string &exceptionInfo) 
{
  int pos, len, i;
  string dpName; 
  
// deselect all the previoulsy selected dp of the manager (one Dp selected per opened modules)
  len = dynlen(g_openedModule);
  for(i=1;i<=len;i++) {
    dpName = g_dpElementSelected[i];
    if((dpName != "") && (dpName != sDpName))
      _unSelectDeselectHMI_select(dpName, false, g_openedModule[i], exceptionInfo);
  }
}

//------------------------------------------------------------------------------------------------------------------------

//@}
