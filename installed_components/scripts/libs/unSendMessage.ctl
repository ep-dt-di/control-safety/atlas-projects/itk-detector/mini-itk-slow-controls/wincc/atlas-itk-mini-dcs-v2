/**@name LIBRARY: unSendMessage.ctl

@author: Vincent Forest (LHC-IAS)

Creation Date: 26/08/2002

Modification History: None

version 1.0

External Functions: 
	. unSendMessage_toAllUsers: to send a message to all the users
	. unSendMessage_toExpert : to send a message to the expert (developer)
	. unSendMessage_toExpertException : to send the exceptionInfo message to the expert (developer)
	
Purpose: 
This library is used by the unicos programs or object to send message using the message text component.

Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. This library needs the unicos message text component and especially the libs unMessageText.ctl and unMessageText_HMI.ctl 
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/

//@{

//------------------------------------------------------------------------------------------------------------------------

// unSendMessage_toAllUsers
/**
Purpose: to send a message to the all the users

Parameters:
	-> message, string input, message to send to the current user
	-> exceptionInfo, string output, any error is saved here
	
Usage: Public

PVSS manager usage: Ctrl, NG, NV

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unSendMessage_toAllUsers(string message, dyn_string &exceptionInfo)
{
	string systemName;
	dyn_string systems = unMessageText_getSystemsOfAlias(unMessageText_extractDeviceAliasFromMessage(message), exceptionInfo);
	if(dynlen(systems) <= 0)
	{
		systems = makeDynString(getSystemName());
	}
	if(dynlen(systems) == 1)
	{
		systemName = systems[1];
	}
	else if(dynlen(systems) > 1)//message will be sent to more than one system, they will be specified later
	{
		systemName = UN_MESSAGE_TEXT_MULTIPLE_SYSTEM_SPECIFIER;
	}

	unMessageText_send(systemName,											// Send to particular system
					   myManNum(),												// Send to current ui number
					   getSystemName()+","+myManNum()+","+getHostname()+","+getUserName(),		// Sender
					   "user","*",												// Send to each user
					   "INFO",													// Message type
					   message,													// Message
					   exceptionInfo);											// Exception...
}

//------------------------------------------------------------------------------------------------------------------------

// unSendMessage_toExpert
/**
Purpose: to send a message to the expert (developer)

Parameters:
	-> sender, string input, sender identifier
	-> messageType, string input, message type (WARNING ERROR or EXPERTINFO)
	-> message, string input, message to send to the current user
	-> exceptionInfo, string output, any error is saved here
	
Usage: Public

PVSS manager usage: Ctrl, NG, NV 

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unSendMessage_toExpert(string sender, string messageType, string message, dyn_string &exceptionInfo)
{
	string msgType;
	
	msgType = messageType;
	if (msgType == "INFO")
		msgType = "EXPERTINFO";
	unMessageText_send("*", "*",												// Send to each system, each ui number
					   getSystemName()+","+myManNum()+","+getHostname()+","+sender, 				// Sender
					   "user","*",											// Send group root
					   msgType,													// Message type
					   message,													// Message
					   exceptionInfo);											// Exception...
}

//------------------------------------------------------------------------------------------------------------------------

// unSendMessage_toExpertException
/**
Purpose: to send the exceptionInfo message to the expert (developer)

Parameters:
	-> sender, string input, sender identifier
	-> exceptionInfo, string input, exception message
	
Usage: Public

PVSS manager usage: Ctrl, NG, NV 

Constraints:
	. PVSS version: 2.12.1 
	. operating system: NT, W2000 and Linux, but tested only under W2000.
	. distributed system: yes but not tested.
*/
unSendMessage_toExpertException(string sender, dyn_string &exceptionInfo)
{
	unMessageText_sendException ("*", "*",										// Send to each system, each ui number
					   getSystemName()+","+myManNum()+","+getHostname()+","+sender, // Sender
					   "user","*",											// Send group root
					   exceptionInfo);											// Exception...
}




/* This function returns device's alias with keyword which indicates that it refers to device alias..
 * This function should be used to add device alias to the messages send with unSendMessage functions.
 *
 * @param deviceName (string) IN   Device name dp
 * 
 * @return Device alias with keyword 
 */
string unSendMessage_getDeviceDescription(string deviceName)
{
	string alias = unGenericDpFunctions_getAlias(deviceName);
	return UN_MESSAGE_TEXT_DEVICE_PATTERN + alias;
}


