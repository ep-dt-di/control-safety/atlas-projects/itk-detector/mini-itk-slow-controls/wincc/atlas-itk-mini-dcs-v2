/**@name LIBRARY: unSystemIntegrity.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 12/07/2002

Modification History:

  23/04/2016: Jonas Arroyo
  - IS-1789   _unSystemIntegrity_createSystemAlarm modified to add description to .enabled DPE.

  08/07/2014: Josef Hofer
  - IS-1492  unSystemIntegrity_createSystemAlarmDiscrete: add function to be able to have discrete alerts
             unSystemIntegrity_removeSystemName: add function for general purpose

  23/04/2013: Marco
  - IS-1056  unSystemIntegrity_addManager: add driver internal dps if don't exist.

  22/02/2012: Herve
  - IS-719  unSystemIntegrity  Manager manager from UI (only): being able to kill a manager even during the init phase

  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic
  - IS-612  Installation  postinstall script: remove or stop managers not requested if no in the console

  - IS-555: system integrity configuration panel: alow remote access, restart unicos_scripts

  - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity

  11/05/2011: Herve
  -IS-529: Import: delete devices and front-end, remove the device and any associated DP from all the mail/SMS category and application

  25/11/2010: Herve
  - IS-462: remove the systemAlarm DPE from the mail/sms setting when the systemAlarm is deleted

  16/08/2010: Herve
    - IS-380:
    modified function: unSystemIntergity_manageManagerperHostname, bug fix:
      - when the started manager has no argument, the search exclude it
      - restart, reset start counter, seconds to kill are set by default instead of being read and restored in case of set manual and set always
    add two new constants for DEBUG option and extended diagnostic.

  26/01/2010: Herve
    - add new function: _unSystemIntegrity_createSystemAlarmBasic
    create systemAlarm with more than 1 alarm level.

  23/09/2009: Herve
    - add constant for the unSystemIntegrity_computeStatistics
    - new function:
    _unSystemIntegrity_createScriptScheduler: creates a scheduler to start a script
    _unSystemIntegrity_startScheduler: start the scheduler

  20/04/2009: Herve
    - _unSystemIntegrity_createScheduler: add sSystemName as extra parameter of the function in order to
    create the scheduler on the remote system

	01/07/2008: Frederic
		- functions: unSystemIntergity_manageManagerperHostname() and unSystemIntegrity_getState()
		the problem with archive -num 10 (!= -num 1) fixed

	10/12/2007: Herve
		- new function: unSystemIntegrity_getState

	13/07/2007: Frederic
		- add unSystemIntergity_getManagerStateperHostname and unSystemIntergity_manageManagerperHostname

	02/03/2005: Herve
		- unSystemIntergity_manageManager: add del manager, set manager to manual mode

	26/02/2005: Herve
		- in unSystemIntergity_manageManager, kill and stop the manager only if it is in running or blocked mode.

	22/02/2005: Herve
		- add functions unSystemIntergity_getManagerState unSystemIntergity_manageManager

	15/02/2005: Herve
		- add a new function: unSystemIntegrity_createFrontEndSystemAlarm: that creates a _UnSystemAlarm with negative behaviour Ok>=10, error <10
		- modification of the signature of _unSystemIntegrity_createSystemAlarm to allow the choice of the alarm logic

	02/07/2004: Herve
		version 3.0
		move c_unSystemIntegrity_defaultDPECheckDelay to unSystemIntegrity_DPE.ctl

	16/04/2004: Herve
		move functions from the unSystemIntegrity script to the lib.
		move PLC function to systemIntegrity_unicos.ctl

	16/03/2004: Fredric
		add the function: _unSystemIntegrity_createScheduler

	26/01/2004: Herve
		add constant for check dpe value

	15/09/2003: Herve
		add the functions unSystemIntegrity_sendCmdToPLC and unSystemIntegrity_synchronizedTimeDateOfPlc

	07/10/2003: Herve
		move systemIntegrity from unicosObjects to unCore

version 1.0

External Functions:

Internal Functions:
	. _unSystemIntegrity_modifyApplicationAlertList: Add/remove a dp in the list of dp alert of the application DP
	. _unSystemIntegrity_createSystemAlarm: create the dp and set the alert hdl

Purpose:
	Utility library for the System Integrity component.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemAlarm, _UnSystemIntegrity, _UnPlc
	. data point:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

#uses "unSystemIntegrityDeprecated.ctl"

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
// SYSTEM INTEGRITY
//------------------
const string c_unSystemAlarm_dpPattern = "_unSystemAlarm_";
const string c_unSystemAlarm_dpType = "_UnSystemAlarm";
const string UN_SYSTEMINTEGRITY_EXTENSION = "_systemIntegrityInfo";
const string UN_SYSTEMINTEGRITY_DPTYPE = "_UnSystemIntegrity";

const int UN_SYSTEMINTEGRITY_ADD = 1;
const int UN_SYSTEMINTEGRITY_DELETE = 2;
const int UN_SYSTEMINTEGRITY_ENABLE = 3;
const int UN_SYSTEMINTEGRITY_DISABLE = 4;
const int UN_SYSTEMINTEGRITY_DIAGNOSTIC = 5;
const int UN_SYSTEMINTEGRITY_EXTENDED_DIAGNOSTIC = 10;
const int UN_SYSTEMINTEGRITY_DEBUG = 11;
const int UN_SYSTEMINTEGRITY_VERSION = 12;

const int c_unSystemIntegrity_alarm_value_level1 = 10;
const int c_unSystemIntegrity_no_alarm_value = 0;
const string UN_SYSTEMINTEGRITY_applicationDPName = "_unApplication.applicationName";
const string UN_SYSTEMINTEGRITY_OK = "Ok";
const string UN_SYSTEMINTEGRITY_BAD = "Bad";

const int UN_SYSTEMINTEGRITY_MAX_FUNCTION = 3;
const int UN_SYSTEMINTEGRITY_FUNCTION_INITIALIZE = 1;
const int UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_COMMAND = 2;
const int UN_SYSTEMINTEGRITY_FUNCTION_HANDLE_DATA = 3;

const string c_systemIntegrity_noFwAccessControl_userAdmin = "root";

const string UN_ARCHIVE = "_ArchivDisk.FreeKB";
const string UN_MEMORY = "_MemoryCheck.FreeKB";

const string SYSTEMINTEGRITY_STOP_COMMAND = "STOP";
const string SYSTEMINTEGRITY_PMON_STOP_COMMAND = "SINGLE_MGR:STOP ";
const string SYSTEMINTEGRITY_KILL_COMMAND = "KILL";
const string SYSTEMINTEGRITY_PMON_KILL_COMMAND = "SINGLE_MGR:KILL ";
const string SYSTEMINTEGRITY_START_COMMAND = "START";
const string SYSTEMINTEGRITY_PMON_START_COMMAND = "SINGLE_MGR:START ";
const string SYSTEMINTEGRITY_PMON_QUERY_COMMAND = "MGRLIST:STATI";
const string SYSTEMINTEGRITY_DEL_COMMAND = "DEL";
const string SYSTEMINTEGRITY_PMON_DEL_COMMAND = "SINGLE_MGR:DEL ";
const string SYSTEMINTEGRITY_SETMANUAL_COMMAND = "SETMANUAL";
const string SYSTEMINTEGRITY_SETALWAYS_COMMAND = "SETALWAYS";
const string SYSTEMINTEGRITY_PMON_SETMANUAL_COMMAND = "SINGLE_MGR:PROP_PUT ";
const int SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE = -1;
const int SYSTEMINTEGRITY_MANAGER_STOPPED = 0;
const int SYSTEMINTEGRITY_MANAGER_INIT = 1;
const int SYSTEMINTEGRITY_MANAGER_RUNNING = 2;
const int SYSTEMINTEGRITY_MANAGER_BLOCKED = 3;
const string SYSTEMINTEGRITY_MANAGER_MANUAL = "manual";
const string SYSTEMINTEGRITY_MANAGER_ALWAYS = "always";

const string USERNAME="";
const string PASSWORD="";

const string SYSTEMINTEGRITY_COMPUTESTATISTICS = "unSystemIntegrity_computeStatistics";

//Default dedicated SI manager numbers:
const int SYSTEMINTEGRITY_DEFAULT_MANAGER_NUMBER_RDB = 40;
const int SYSTEMINTEGRITY_DEFAULT_MANAGER_NUMBER_LOGGINGDB = 41;

//@{



/** This function checks if a given System Integrity manager number
 *  is in use by any component.
 *
 * @param managerNumber manager number to check if there are any System Integrity components using it
 * @param isInUse (out) true if manager is still used by some System Intgrity component/module, false if manager is not used anymore
 *
 * @return 0 on success (was able to check if a manager is in use), -1 on error - failed to check if a manager is in use,
 *           on error isInUse is not changed
 */
int _unSystemIntegrity_isManagerInUse(int managerNumber, bool &isInUse)
{
  dyn_string dpes, values;

  dpes = dpNames("*.config.dedicatedControlManager", "_UnSystemIntegrity");
  int res = dpGet(dpes, values);
  if(res == -1)
  {
    return -1; // -1 - failure
  }

  isInUse = (dynCount(values, managerNumber) > 0);

  return 0; // 0 - success
}

//if managerNumber == 0 then the component will be run by the unicos_scripts.lst
// TODO: sys is not taken into account when sending commands to managers
int unSystemIntegrity_runComponentOnDedicatedManager(string sComponent, int managerNumber, string sys = getSystemName())
{
  dyn_mixed properties;

  // Get manager number from datapoint of system integrity for a given component
  int previousManagerNumber = 0;
  unSystemIntegrity_getComponentControlManagerNumber(sComponent, previousManagerNumber, sys);

  // Display a message that a manager will be run as a dedicated manager (managerNumber <> 0) or in unicos_script.lst
  if(managerNumber == 0)
  {
    DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Moving component " + sComponent + " to default unicos_script.lst ctrl manager");
  }
  else
  {
    DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Moving component " + sComponent + " to a dedicated System Integrity Ctrl manager: " + managerNumber);
  }

  // Set a new manager number in system integrity datapoint and exit if it fails
  if(unSystemIntegrity_setComponentControlManagerNumber(sComponent, managerNumber, sys))
  {
    DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: Failed to set the dedicated System Integrity manager number for component: " + sComponent + ", Num: " + managerNumber);
    return -1;
  }

  string sHost;
  int iPort;
  unSystemIntegrity_getPmonHostAndPort(sHost, iPort, sys);

  // The component was run by a dedicated manager, we need to restart it
  if(previousManagerNumber > 0)
  {
    // Get properties of a (previous) manager from the console
    fwInstallationManager_getProperties("WCCOActrl", "unSystemIntegrity.ctl -num " + previousManagerNumber, properties, sHost, iPort);
    if(dynlen(properties) >= FW_INSTALLATION_MANAGER_OPTIONS)
    {
      // We got some properties (success)...

      // Set previous manager (in console) to manual, only under condition that it has a different number and is
      // no longer in use by other System Integrity threads
      bool isInUse;
      int result = _unSystemIntegrity_isManagerInUse(previousManagerNumber, isInUse);
      if(result == -1)
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: failed to check if manager no " + managerNumber + " is still in use");
        return result;
      }

      if((previousManagerNumber != managerNumber) && (!isInUse))
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Setting to manual previous dedicated manager for component " + sComponent + ", Previous System Integrity Ctrl manager: " + previousManagerNumber);
        fwInstallationManager_setMode("WCCOActrl", "unSystemIntegrity.ctl -num " + previousManagerNumber, "manual", sHost, iPort);
      }

      // Kill previous manager
      DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Killing previously used System Integrity manager for component " + sComponent + ". Manager number: " + previousManagerNumber);
      if(fwInstallationManager_command("KILL", "WCCOActrl", "unSystemIntegrity.ctl -num " + previousManagerNumber, sHost, iPort, "", "", true))
      {
        // We failed to kill the manager, bring back (revert) old manager number to system integrity datapoint and exit
        DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: Failed to KILL the previous System Integrity manager for component " + sComponent + ", Previous dedicated manager number: " + previousManagerNumber);
        DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Reverting System Integrity manager for component " + sComponent + " to previous manager number: " + previousManagerNumber);
        unSystemIntegrity_setComponentControlManagerNumber(sComponent, previousManagerNumber, sys);
        DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: Configuration of System Integrity for component " + sComponent + " on manager number " + managerNumber + " ABORTED!!");

        return -1;
      }
      delay(1); // add small delay to prevent race case with double start of active manager causing pmon to lose pid
    }
    else
    {
      // Previous manager (with old number) was not found in console - do nothing, just print a message
      DebugTN(__FILE__ + __FUNCTION__ + " -> Previous manager not found in the project console. CTRL Manager number: " + previousManagerNumber);
    }
  }
  else
  {
    // Previous manager number was run in unicos_scripts.lst, print message, do nothing
    DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: SI component " + sComponent + " was previously executed by the default unicos_scripts.lst");
  }

  // Do something only if caller requested a dedicated manager (managerNumber > 0)
  if(managerNumber > 0)
  {
    // Create a debug datapoint for new manager (why?)
    if(!dpExists("_CtrlDebug_CTRL_" + managerNumber))
    {
      DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Creating _CtrlDebug Dp for manager number: " + managerNumber);
      dpCreate("_CtrlDebug_CTRL_" + managerNumber, "_CtrlDebug");
    }

    // Try to see if we can reuse any of the system integrity CTRL managers if they are not used

    // Clear properties that could have been set by previous condidtions. This is to check
    // if a manager that we want to add already exists, we will try to reuse it
    dynClear(properties);
    string previousOptions = "unSystemIntegrity.ctl -num " + managerNumber; // TODO: "previousOptions" might be misleading
    fwInstallationManager_getProperties("WCCOActrl", previousOptions, properties, sHost, iPort);
    if(dynlen(properties) >= FW_INSTALLATION_MANAGER_OPTIONS)
    {
      // Yes there is a manager with the desired system number, we need to restart it,
      // again, set to manual and restart, manager will be in stopped state (should be)
      DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Recycling manager " + managerNumber + " for component " + sComponent);
      DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Stopping previous dedicated manager for component " + sComponent + ", Previous System Integrity Ctrl manager: " + managerNumber + ", manager options: " + previousOptions);
      if(fwInstallationManager_setMode("WCCOActrl", previousOptions, "manual", sHost, iPort))
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> WARNING: Failed to set unused SI manager to manual mode, number: " + managerNumber + ", manager options: " + previousOptions);
      }
      else
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Unused SI manager successfully set to manual mode, number: " + managerNumber + ", manager options: " + previousOptions);
      }

      // Send KILL signal to the manager that we want to reuse (so that a new code will be run), manager will be stopped
      // Note: pmon port is set to zero, as empty host will cause pmon port to be retrieved from current system
      if(fwInstallationManager_command("KILL", "WCCOActrl", previousOptions, sHost, iPort, "", "", true))
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> WARNING: Failed to KILL the unused System Integrity manager manager, number: " + managerNumber + ", manager options: " + previousOptions);
      }
      else
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Unused SI manager successfully killed, number: " + managerNumber + ", manager options: " + previousOptions);
      }

      // Wait for the manager to be killed:
      int timeout = 5;
      bool isRunning = true;
      int t = 0;
      fwInstallationManager_isRunning("WCCOActrl", previousOptions, isRunning, sHost, iPort);
      if(isRunning && t < timeout)
      {
        delay(1);
        ++t;
        fwInstallationManager_isRunning("WCCOActrl", previousOptions, isRunning, sHost, iPort);
      }
      if(t >= timeout)
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> WARNING: Failed to KILL the previously used System Integrity manager manager, number: " + managerNumber + ", manager options: " + previousOptions);
      }

      // Restart (start, because it was set to manual previously) reused manager...
      // Note: pmon port is set to zero, as empty host will cause pmon port to be retrieved from current system
      if(fwInstallationManager_command("RESTART", "WCCOActrl", "unSystemIntegrity.ctl -num " + managerNumber, sHost, iPort, "", "", true))
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: Failed to restart the previous System Integrity manager for component " + sComponent + ", dedicated manager number: " + managerNumber + ", manager options: " + "unSystemIntegrity.ctl -num " + managerNumber);
        DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: Configuration of System Integrity for component " + sComponent + " on manager number " + managerNumber + " ABORTED!!");
        return -1;
      }

      // Set reused manager to always
      DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Setting to always dedicated manager for component " + sComponent + ", ctr manager number: " + managerNumber);
      fwInstallationManager_setMode("WCCOActrl", "unSystemIntegrity.ctl -num " + managerNumber, "always", sHost, iPort);

      DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Dedicated manager for component " + sComponent + " successfully started, System Integrity Ctrl manager: " + managerNumber + ", manager options: " + "unSystemIntegrity.ctl -num " + managerNumber);
    }
    else
    {
      // There's no manager with requested number (we can not reuse anything, as in "if" case)
      DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Adding a new dedicated System Integrity manager to the project cosole for component: " + sComponent + ", Num: " + managerNumber);

      // Finally add a manager
      // Note: pmon port is set to zero, as empty host will cause pmon port to be retrieved from current system
      if(fwInstallationManager_add("WCCOActrl", "always", 2, 2, 30, "unSystemIntegrity.ctl -num " + managerNumber, sHost, iPort) == 0)
      {
        DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: Failed to add the dedicated System Integrity manager to the project cosole for component: " + sComponent + ", Num: " + managerNumber);
        DebugTN(__FILE__ + __FUNCTION__ + " -> ERROR: Configuration of System Integrity for component " + sComponent + " on manager number " + managerNumber + " ABORTED!!");
        return -1;
      }
    }
  }

  // The component could have been run by the unicos_scripts.lst, therefore, we need to restart the unicos_scripts.lst
  // to avoid that the same funcitonality is run by two managers now
  // Check if unicos_scripts.lst manager exists
  dyn_mixed manProperties;
  if(fwInstallationManager_getProperties("WCCOActrl", "-f unicos_scripts.lst", manProperties, sHost, iPort) != 0)
  {
    DebugTN(__FILE__ + __FUNCTION__ + " -> WARNING: Failed to check if unicos_scripts is present in project after moving component " +
            sComponent + " to dedicated manager: " + managerNumber);
    return 0;
  }
  if(manProperties[FW_INSTALLATION_MANAGER_PMON_IDX] < 0)
  { // unicos_scripts.lst manager does not exist, exit
    return 0;
  }
  // Manager exists, check if it is running now
  bool isRunning;
  if(fwInstallationManager_isRunning("WCCOActrl", "-f unicos_scripts.lst", isRunning, sHost, iPort) != 0)
  {
    DebugTN(__FILE__ + __FUNCTION__ + " -> WARNING: Failed to check if unicos_scripts manager is running. Restart will be forced");
    isRunning = true;
  }
  if(!isRunning)
  { // Manager is not running, do not restart it
    return 0;
  }
  // Manager exists and is running - restart it
  DebugTN(__FILE__ + __FUNCTION__ + " -> INFO: Restarting default unicos_scripts after moving component " + sComponent +
          " to a dedicated System Integrity Ctrl manager: " + managerNumber);
  if(fwInstallationManager_command("RESTART", "WCCOActrl", "-f unicos_scripts.lst", sHost, iPort, "", "", true) != 0)
  {
    DebugTN(__FILE__ + __FUNCTION__ + " -> WARNING: Failed to restart the default unicos_scripts after moving component " + sComponent + " to dedicated manager: " + managerNumber);
  }
  return 0;
}

int unSystemIntegrity_sendManagerCommandForComponent(string sComponent, string command, string sSystem=getSystemName())
{
  int managerNum = 0;
  unSystemIntegrity_getComponentControlManagerNumber(sComponent, managerNum, sSystem);

  string sHost;
  int iPort;
  unSystemIntegrity_getPmonHostAndPort(sHost, iPort, sSystem);

  string optionsPattern = "";

  if(managerNum == 0) //the component is under unicos_scripts.lst
  {
    optionsPattern = "-f unicos_scripts.lst";
  }
  else
  {
    optionsPattern = "unSystemIntegrity.ctl -num " + managerNum;// + " #SI:"+sComponent+"#";
  }

  return fwInstallationManager_command(strtoupper(command), "WCCOActrl", optionsPattern, sHost, iPort);
}




// unSystemIntegrity_createSystemAlarm
/**
Purpose:
This creates the dp and sets the alert hdl.

	@param sDp: string, input, data point name,
	@param sType: string, input, type of System Alarm
	@param description: string, input, description of the dp to create
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_createSystemAlarm(string sDp, string sType, string description, dyn_string &exceptionInfo)
{
	string alertText, dp;
	bool bCreate = false;
	int res;
	string arName;
	int arNumber;

// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;

	_unSystemIntegrity_createSystemAlarm(c_unSystemAlarm_dpPattern+sType+dp,
												description, true,
												exceptionInfo);
}

// unSystemIntegrity_createFrontEndSystemAlarm
/**
Purpose:
This creates the dp and sets the alert hdl.

	@param sDp: string, input, data point name,
	@param sType: string, input, type of System Alarm
	@param description: string, input, description of the dp to create
	@param bOkRange: bool, input, true=alarm if value >10, false=alarm if value <=10
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_createFrontEndSystemAlarm(string sDp, string sType, string description, bool bOkRange, dyn_string &exceptionInfo)
{
	string alertText, dp;
	bool bCreate = false;
	int res;
	string arName;
	int arNumber;

// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;

	_unSystemIntegrity_createSystemAlarm(c_unSystemAlarm_dpPattern+sType+dp,
												description, bOkRange,
												exceptionInfo);
}

// _unSystemIntegrity_modifyApplicationAlertList
/**
Purpose:
Add/remove a dp in the list of dp alert of the application DP.

	@param sDp: string, input, string to find in the list
	@param bAdd: bool, input, true = add in the list of alert, false = remove from the list of alert
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_unSystemIntegrity_modifyApplicationAlertList(string sDp, bool bAdd, dyn_string &exceptionInfo)
{
	dyn_string alertText, alertPanelParameter, dp_list;
	string alertPanel, sHelpPanel, dp;
	int pos;
  dyn_string dsTemp;
  int i, len;
  bool bSet;

	dp=dpSubStr(sDp, DPSUB_SYS_DP);
  if(dpExists(sDp))
  {
    	if(dpExists(UN_SYSTEMINTEGRITY_applicationDPName)) {
    		dpGet(UN_SYSTEMINTEGRITY_applicationDPName+":_alert_hdl.._text0", alertText[1],
    						UN_SYSTEMINTEGRITY_applicationDPName+":_alert_hdl.._text1", alertText[2],
    						UN_SYSTEMINTEGRITY_applicationDPName+":_alert_hdl.._panel",alertPanel,
    						UN_SYSTEMINTEGRITY_applicationDPName+":_alert_hdl.._panel_param",alertPanelParameter,
    						UN_SYSTEMINTEGRITY_applicationDPName+":_alert_hdl.._help",sHelpPanel,
    						UN_SYSTEMINTEGRITY_applicationDPName+":_alert_hdl.._dp_list", dp_list);
    	//DebugN(pos, dp);
    		if(bAdd) {
    		  pos = dynContains(dp_list, dp+".alarm");
    			if(pos <= 0){
    				dynAppend(dp_list, dp+".alarm");
    				fwAlertConfig_modifySummary(UN_SYSTEMINTEGRITY_applicationDPName, alertText, dp_list,
    													alertPanel, alertPanelParameter, sHelpPanel, exceptionInfo);
          unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unSystemIntegrity Application", "_unSystemIntegrity_modifyApplicationAlertList adding "+dp);
    			}
    		}
    		else {
          bSet = false;
          dsTemp = dynPatternMatch("*"+dp+"*", dp_list);
          len = dynlen(dsTemp);
          for(i=1;i<=len;i++)
          {
            pos = dynContains(dp_list, dsTemp[i]);
            if(pos > 0) {
    				    dynRemove(dp_list, pos);
                bSet = true;
            }
          }
          if(bSet) {
      				fwAlertConfig_modifySummary(UN_SYSTEMINTEGRITY_applicationDPName, alertText, dp_list,
      													alertPanel, alertPanelParameter, sHelpPanel, exceptionInfo);
          unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "unSystemIntegrity Application", "_unSystemIntegrity_modifyApplicationAlertList removing all instance of "+dp, dp_list);
          }
    		}
    	}
    if(!bAdd)
      unProcessAlarm_removeDpFromList(dp, exceptionInfo);
  }
  else
    fwException_raise(exceptionInfo,"ERROR","DP: "+sDp+" does not exists","");
}

// _unSystemIntegrity_createSystemAlarm
/**
Purpose:
This creates the dp and sets the alert hdl.

	@param sDp: string, input, data point name
	@param description: string, input, description of the dp
	@param bOkRange: bool, input, true=alarm if value >10, false=alarm if value <=10
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_unSystemIntegrity_createSystemAlarm(string sDp, string description, bool bOkRange, dyn_string &exceptionInfo)
{
	string dpe = sDp+".alarm";
	string sDpeEnabled = sDp + ".enabled";
	bool isActive;
	dyn_string dsAlertText, dsAlertClass;

//DebugN("_unSystemIntegrity_createSystemAlarm", sDp);
	dpCreate(sDp, c_unSystemAlarm_dpType);
// a second check is done after the creation to ensure that the data point is created (just in case of error)
// it is also a way to wait the creation which is asynchronous.
	if(!dpExists(sDp)) {
		fwException_raise(exceptionInfo, "ERROR",
						"_unSystemIntegrity_createSystemAlarm():"+getCatStr("unSystemIntegrity", "CANNOT_CREATE_DP")+sDp,"");
	}
	else {
// set its alarm config
		if(bOkRange) {
			dsAlertText = makeDynString(UN_SYSTEMINTEGRITY_OK, UN_SYSTEMINTEGRITY_BAD);
			dsAlertClass = makeDynString("", getSystemName()+"_unSystemIntegrityAlarm_lvl1.");
		}
		else {
			dsAlertText = makeDynString(UN_SYSTEMINTEGRITY_BAD, UN_SYSTEMINTEGRITY_OK);
			dsAlertClass = makeDynString(getSystemName()+"_unSystemIntegrityAlarm_lvl1.", "");
		}
		fwAlertConfig_setGeneral(dpe, dsAlertText,
									makeDynFloat(c_unSystemIntegrity_alarm_value_level1),
									dsAlertClass,
									"", makeDynString(), "", 1, exceptionInfo);
// set the description
	dpSetDescription(dpe, description);   // .alarm
    if (description != "" )               // .enabled
      dpSetDescription(sDpeEnabled, "System Alarm enabled for " + description);
	}
}

// _unSystemIntegrity_createScheduler
/**
Purpose:
This creates the dp and sets the schedulerwith default settings.

	@param sDp: string, input, data point name = Scheduler name
	@param iEr: int, output, Error code
        @param sSystemName: string, input, the system name, default =""=getSystemName()

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_unSystemIntegrity_createScheduler(string sCat, int &iEr, string sSystemName="")
{
  string sDp;
  int iNum;

//DebugN("_unSystemIntegrity_createScheduler", sCat, sSystemName);
  if(sSystemName == "")
    sSystemName = getSystemName();
  if(sSystemName == getSystemName())
    iEr=dpCreate(sCat,"_Sc");
  else
  {
    iNum = getSystemId(sSystemName);
    if(iNum >= 0)
      iEr=dpCreate(sCat,"_Sc", iNum);
//DebugN(iNum, sSystemName, sCat, iEr);
  }
  sDp = sSystemName+sCat;
//DebugN(sDp, dpExists(sDp));
  if(!dpExists(sDp))
    iEr = -1;
if (iEr>=0)
	{
	iEr=dpSetWait(	sDp + ".name", sCat,
					sDp + ".type", "F",
					sDp + ".mode", false,
					sDp + ".logEntry", true,
					sDp + ".actions.dpeList.dpes", makeDynString("_unSendMail.message.periodic_report"),	//dpe to set on action
					sDp + ".actions.dpeList.dpeValues", makeDynString(sCat),									//set Type
					sDp + ".actions.dpeList.active", true,
					sDp + ".time.timedFunc.validUntil", makeTime(2034,12,31,23,59,59),						//Max
					sDp + ".time.timedFunc.interval",86400,													//24 hours
					sDp + ".time.timedFunc.syncTime",0,														//01/01/this year at 0:00
					sDp + ".time.timedFunc.syncWeekDay",-1,
					sDp + ".time.timedFunc.syncDay",1,
					sDp + ".time.timedFunc.syncMonth",1,
					sDp + ".time.conditionType",1);															//Periodic

        if(sSystemName == getSystemName())
	  iEr=dpSetDescription(sDp + ".name", sCat);
	}
}

// _unSystemIntegrity_setList
/**
Purpose:
Add/remove a string in a list. Indivisible function

	@param dsList: dyn_string, input & output, list to look in
	@param sDp: string, input, string to find in the list
	@param toAdd: bool, input, true add in list if not already added, false remove from the list if sDp is in the list

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
int _unSystemIntegrity_setList(dyn_string &dsList, string sDp, bool bAdd)
{
	int pos;

	pos = dynContains(dsList, sDp);
	if(bAdd) { //add in list
		if(pos <= 0) {// if not in list do nothing
			dynAppend(dsList, sDp);
			pos = dynlen(dsList);
		}
	}
	else { // remove from list
		if(pos > 0) // if in list remove it
			dynRemove(dsList, pos);
	}
//DebugN("_unSystemIntegrity_setList",dsList, sDp, bAdd);
	return pos;
}

// _unSystemIntegrity_isInList
/**
Purpose:
Add/remove a string in a list. Indivisible function

	@param dsList: dyn_string, input & output, list to look in
	@param sDp: string, input, string to find in the list

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
int _unSystemIntegrity_isInList(dyn_string &dsList, string sDp)
{
	int pos;

	pos = dynContains(dsList, sDp);
//DebugN("_unSystemIntegrity_isInList",dsList, pos);
	return pos;
}

// _unSystemIntegrity_registerFunction
/**
Purpose:
Register function, does the dpConnect and dpDisconnect, add/remove the dp in/from the global list.

	@param bRegister: bool, input, true: add, false: remove from sGlobalVar
	@param sCallBack: string, input, callback function
	@param dsCallBackParam: dyn_string, input, callback parameters
	@param sGlobalVar: dyn_string, input-output, the global variable used to store the list of dp to check
	@param sDp: string, input, the dp to add/remove in/from the sGlobalVar
	@param exceptionInfo: dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_unSystemIntegrity_registerFunction(bool bRegister, string sCallBack, dyn_string dsCallBackParam,
					dyn_string &sGlobalVar, string sDp, dyn_string &exceptionInfo)
{
	int res;

	if(bRegister) {
// connect to the callback function if not already done
		res = _unSystemIntegrity_isInList(sGlobalVar, sDp);
		if(res <= 0) {
		// add in list first because if the dpConnect succeeds the callback may be called before the list is updated
		// and in the callback the list is used
			res = _unSystemIntegrity_setList(sGlobalVar, sDp, true);

//DebugN("gl",sGlobalVar);

 			switch(dynlen(dsCallBackParam)) {
				case 1:
					res = dpConnect(sCallBack, dsCallBackParam[1]);
					break;
				case 2:
					res = dpConnect(sCallBack, dsCallBackParam[1], dsCallBackParam[2]);
					break;
				case 3:
					res = dpConnect(sCallBack, dsCallBackParam[1], dsCallBackParam[2],dsCallBackParam[3]);
					break;
				default:
					res = -1;
					break;
			}
			if(res < 0) {
				fwException_raise(exceptionInfo, "ERROR",
							"_unSystemIntegrity_registerFunction(): "+getCatStr("unSystemIntegrity", "CANNOT_DP_CONNECT") + dsCallBackParam[1],"");

			// remove from list in case of error
				res = _unSystemIntegrity_setList(sGlobalVar, sDp, false);
			}
			else {
		// add in list
				res = _unSystemIntegrity_setList(sGlobalVar, sDp, true);
			// set the enable to true and activate the alarm if it is not activated.
				dpSet(sDp+".enabled", true);
				unAlarmConfig_mask(sDp+".alarm", false, exceptionInfo);
			}
		}
	}
	else {
// disconnect the callback function
		res = _unSystemIntegrity_isInList(sGlobalVar, sDp);
		if(res > 0) {
			switch(dynlen(dsCallBackParam)) {
				case 1:
					res = dpDisconnect(sCallBack, dsCallBackParam[1]);
					break;
				case 2:
					res = dpDisconnect(sCallBack, dsCallBackParam[1], dsCallBackParam[2]);
					break;
				case 3:
					res = dpDisconnect(sCallBack, dsCallBackParam[1], dsCallBackParam[2],dsCallBackParam[3]);
					break;
				default:
					res = -1;
					break;
			}
			if(res < 0) {
				fwException_raise(exceptionInfo, "ERROR",
							"_unSystemIntegrity_registerFunction():"+getCatStr("unSystemIntegrity", "CANNOT_DP_DISCONNECT") + sDp,"");
			}
			else {
			// remove from list
				res = _unSystemIntegrity_setList(sGlobalVar, sDp, false);
			// set the enable to false and de-activate the alarm and acknowledge it if necessary
				dpSet(sDp+".enabled", false, sDp+".alarm", c_unSystemIntegrity_no_alarm_value);
				unAlarmConfig_mask(sDp+".alarm", true, exceptionInfo);
			}
		}
	}
//DebugN("end",sGlobalVar);

}

// unSystemIntegrity_getComponents
/**
Purpose:
get the list of components for the systemIntegrity

	@param dsComponents: dyn_string, output, the list of components is returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_getComponents(dyn_string &dsComponents)
{
	dyn_string dsTemp;
	int i, length;
	string sSystemName;

	sSystemName = getSystemName();
	dsTemp = dpNames("*" + UN_SYSTEMINTEGRITY_EXTENSION, UN_SYSTEMINTEGRITY_DPTYPE);	// Get DPEs
//DebugN(dsTemp, UN_SYSTEMINTEGRITY_EXTENSION, UN_SYSTEMINTEGRITY_DPTYPE);
	length = dynlen(dsTemp);
	for(i=1;i<=length;i++)					// For each element, delete system name
	{
		dsTemp[i] = substr(dsTemp[i], strlen(sSystemName));
		dsTemp[i] = substr(dsTemp[i], 0, strlen(dsTemp[i]) - strlen(UN_SYSTEMINTEGRITY_EXTENSION));
	}
	dynUnique(dsTemp);
	dynSortAsc(dsTemp);
//DebugN(dsTemp, UN_SYSTEMINTEGRITY_EXTENSION, UN_SYSTEMINTEGRITY_DPTYPE);
	dsComponents = dsTemp;
}

// unSystemIntegrity_getComponentsFromSystem
/**
Purpose:
get the list of components for the systemIntegrity

	@param dsComponents: dyn_string, output, the list of components is returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_getComponentsFromSystem(dyn_string &dsComponents, string sSystemName)
{
	dyn_string dsTemp;
	int i, length;

	dsTemp = dpNames(sSystemName+"*" + UN_SYSTEMINTEGRITY_EXTENSION, UN_SYSTEMINTEGRITY_DPTYPE);	// Get DPEs
//DebugN(dsTemp, UN_SYSTEMINTEGRITY_EXTENSION, UN_SYSTEMINTEGRITY_DPTYPE);
	length = dynlen(dsTemp);
	for(i=1;i<=length;i++)					// For each element, delete system name
	{
		dsTemp[i] = substr(dsTemp[i], strlen(sSystemName));
		dsTemp[i] = substr(dsTemp[i], 0, strlen(dsTemp[i]) - strlen(UN_SYSTEMINTEGRITY_EXTENSION));
	}
	dynUnique(dsTemp);
	dynSortAsc(dsTemp);
//DebugN(dsTemp, UN_SYSTEMINTEGRITY_EXTENSION, UN_SYSTEMINTEGRITY_DPTYPE);
	dsComponents = dsTemp;
}

// unSystemIntegrity_getComponentsFunctions
/**
Purpose:
get the list function of a given component for the systemIntegrity

	@param sComponent: string, input, the component,
	@param dsFunctions: dyn_string, output, the list of function is returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_getComponentsFunctions(string sComponent, dyn_string &dsFunctions, string sSystem=getSystemName())
{
	dpGet(sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.functions", dsFunctions);
}

// unSystemIntegrity_getComponentsPanels
/**
Purpose:
get the list function of a given component for the systemIntegrity

	@param sComponent: string, input, the component,
	@param dsPanels: dyn_string, output, the panels are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_getComponentsPanels(string sComponent, dyn_string &dsPanels, string sSystem=getSystemName())
{
	dpGet(sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.configurationPanel", dsPanels[1],
			sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.operationPanel", dsPanels[2]);
}

// unSystemIntegrity_getComponentsData
/**
Purpose:
get the data of a given component for the systemIntegrity

	@param sComponent: string, input, the component,
	@param dsData: dyn_string, output, the config data are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_getComponentsData(string sComponent, dyn_string &dsData, string sSystem=getSystemName())
{
	dpGet(sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.data", dsData);
}

// unSystemIntegrity_getComponentControlManagerNumber
/**
Purpose:
gets the number of a dedicated control manager to run a particular function of system integrity

	@param sComponent: string, input, the component,
	@param managerNum: int, output, control manager number, 0 means no dedicated control manager;

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_getComponentControlManagerNumber(string sComponent, int &managerNum, string sSystem=getSystemName())
{
  if(dpExists(sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.dedicatedControlManager"))
  {
  	dpGet(sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.dedicatedControlManager", managerNum);
  }
  else
  {
    managerNum = 0;
  }
}

// unSystemIntegrity_setComponentControlManagerNumber
/**
Purpose:
sets the number of a dedicated control manager to run a particular function of system integrity

	@param sComponent: string, input, the component,
	@param managerNum: int, input, control manager number, 0 means no dedicated control manager;

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
int unSystemIntegrity_setComponentControlManagerNumber(string sComponent, int managerNum, string sSystem=getSystemName())
{
  if(dpExists(sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.dedicatedControlManager"))
  {
  	return dpSet(sSystem + sComponent + UN_SYSTEMINTEGRITY_EXTENSION + ".config.dedicatedControlManager", managerNum);
  }

  return -1;
}

// unSystemIntergity_manageManager
/**
Purpose:
Stop, kill, start a manager corresponding to sManager and sCommandLine

	@param sManager: string, input, the PVSS manager,
	@param sCommandLine: string, input, the command line of PVSS manager,
	@param sStopCommand: string, input, the command: SYSTEMINTEGRITY_STOP_COMMAND, SYSTEMINTEGRITY_KILL_COMMAND, SYSTEMINTEGRITY_START_COMMAND, SYSTEMINTEGRITY_DEL_COMMAND
	@param iManagerIndex: int, output, the index of the PVSS manager,
	@param exceptionInfo: dyn_string, output, the errors are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntergity_manageManager(string sManager, string sCommandLine, string sStopCommand, int &iManagerIndex, dyn_string &exceptionInfo, string sSystem="")
{
int iPort;
string sHost, sIP;

	if (sSystem=="")
		sSystem=getSystemName();

	unGenericDpFunctions_getHostName(sSystem, sHost, sIP);
	unGenericDpFunctions_getpmonPort(sSystem, iPort);

	if (sHost=="" || iPort==0)
		{
		sHost=getHostname();
		iPort=pmonPort();
		}

	if (sHost!="" && iPort>0)
		unSystemIntergity_manageManagerperHostname(sManager, sCommandLine, sStopCommand, sHost, iPort, iManagerIndex, exceptionInfo);
	else
		fwException_raise(exceptionInfo, "ERROR", "unSystemIntergity_manageManager() - Unable to get the Hostname or PMON port number!", "");
}
// unSystemIntegrity_removeManager
/**
Purpose:
  Remove manager from console

  @param sComponentPattern: string, input, the component patern,
  @param iManagerNumber: integer, input, the manager number,
  @return 0 if OK, -1 if error

Usage: Public

*/
int unSystemIntegrity_removeManager(string sComponentPattern, int iManagerNumber) {
  string previousManagerNumber;
  unSystemIntegrity_getComponentControlManagerNumber(sComponentPattern, previousManagerNumber, getSystemName());
  if(previousManagerNumber >0) iManagerNumber = previousManagerNumber;

  string sHost;
  int iPort;
  unSystemIntegrity_getPmonHostAndPort(sHost, iPort);

  return fwInstallationManager_remove("WCCOActrl","unSystemIntegrity.ctl -num "+iManagerNumber, sHost, iPort);
}
// unSystemIntegrity_runManagerIfNotRunning
/**
Purpose:
  Run the manager if it is not running

  @param sComponentPattern: string, input, the component patern,
  @param iManagerNumber: integer, input, the manager number,
  @return 0 if OK, -1 if error

Usage: Public

*/
int unSystemIntegrity_runManagerIfNotRunning(string sComponentPattern, int iManagerNumber) {
  string sHost;
  int iPort;
  unSystemIntegrity_getPmonHostAndPort(sHost, iPort);
  int managerNumber = iManagerNumber;
  int iError = 0;
  string previousManagerNumber;
  unSystemIntegrity_getComponentControlManagerNumber(sComponentPattern, previousManagerNumber, getSystemName());
  if(previousManagerNumber >0) managerNumber = previousManagerNumber;
  bool isRuning;
  iError = fwInstallationManager_isRunning("WCCOActrl","unSystemIntegrity.ctl -num "+managerNumber, isRuning, sHost, iPort);
  int attemptCount =0;
  while(!isRuning) {
    iError =unSystemIntegrity_runComponentOnDedicatedManager(sComponentPattern, managerNumber);
    delay(1);
    if (attemptCount>3) return -1;
    attemptCount++;
    iError = fwInstallationManager_isRunning("WCCOActrl","unSystemIntegrity.ctl -num "+managerNumber, isRuning, sHost, iPort);
    }
  return iError;
}

// _unSystemIntegrity_removeManagerPrefix
/**
Purpose:
Remove the prefix of a manager type (PVSS, WCCOA, or WCCIL)

	@param sManager: string, input, the PVSS manager,

Usage: Internal

*/
string _unSystemIntegrity_removeManagerPrefix(string sManager)
{
  string typeWithoutPrefix = sManager;
  strreplace(typeWithoutPrefix, "PVSS00", "");
  strreplace(typeWithoutPrefix, "WCCOA", "");
  strreplace(typeWithoutPrefix, "WCCIL", "");
  return typeWithoutPrefix;
}
// unSystemIntergity_manageManagerperHostname
/**
Purpose:
Stop, kill, start a manager corresponding to sManager and sCommandLine

	@param sManager: string, input, the PVSS manager,
	@param sCommandLine: string, input, the command line of PVSS manager,
	@param sStopCommand: string, input, the command: SYSTEMINTEGRITY_STOP_COMMAND, SYSTEMINTEGRITY_KILL_COMMAND, SYSTEMINTEGRITY_START_COMMAND, SYSTEMINTEGRITY_DEL_COMMAND
	@param sHost: string, input, computer hostname,
	@param iPort: int, input, pMon port number,
	@param iManagerIndex: int, output, the index of the PVSS manager,
	@param exceptionInfo: dyn_string, output, the errors are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: Linux, Windows XP
	. distributed system: yes
*/
unSystemIntergity_manageManagerperHostname(string sManager, string sCommandLine, string sStopCommand, string sHost, int iPort, int &iManagerIndex, dyn_string &exceptionInfo)
{
bool bFailed;
dyn_dyn_string ddsResult;
int i, iLen, iManagerState;
int iIndex=SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE;
string sPmonCommand;

	switch(sStopCommand)
		{
		case SYSTEMINTEGRITY_STOP_COMMAND:
			sPmonCommand=SYSTEMINTEGRITY_PMON_STOP_COMMAND;
			break;
		case SYSTEMINTEGRITY_KILL_COMMAND:
			sPmonCommand=SYSTEMINTEGRITY_PMON_KILL_COMMAND;
			break;
		case SYSTEMINTEGRITY_START_COMMAND:
			sPmonCommand=SYSTEMINTEGRITY_PMON_START_COMMAND;
			break;
		case SYSTEMINTEGRITY_DEL_COMMAND:
			sPmonCommand=SYSTEMINTEGRITY_PMON_DEL_COMMAND;
			break;
		case SYSTEMINTEGRITY_SETMANUAL_COMMAND:
		case SYSTEMINTEGRITY_SETALWAYS_COMMAND:
			sPmonCommand=SYSTEMINTEGRITY_PMON_SETMANUAL_COMMAND;
			break;
		default:
			break;
		}

	// get the list of manager
	bFailed = pmon_query(USERNAME+"#"+PASSWORD+"#MGRLIST:LIST", sHost, iPort, ddsResult, false, true);
	if(!bFailed)
		{
		iLen = dynlen(ddsResult);
		for(i=1;i<=iLen;i++)
			{
                  if(dynlen(ddsResult[i]) == 5) // case no argument
                    dynAppend(ddsResult[i], "");
			if (dynlen(ddsResult[i])>=6)
				{
				// if the manager is found with the command line - " " is needed to distinguish -num 1 and -num 10
				if((_unSystemIntegrity_removeManagerPrefix(sManager) == _unSystemIntegrity_removeManagerPrefix(ddsResult[i][1])) && (strpos(ddsResult[i][6] + " ",sCommandLine + " ") >=0))
					{
					iIndex=i-1;
					if(sPmonCommand != "")
						{
						sPmonCommand = USERNAME + "#" + PASSWORD + "#" + sPmonCommand + iIndex;

						// get the state of the manager
						unSystemIntergity_getManagerStateperHostname(iIndex, sHost, iPort, iManagerState, exceptionInfo);
						switch(sStopCommand)
							{
							case SYSTEMINTEGRITY_START_COMMAND:
							case SYSTEMINTEGRITY_DEL_COMMAND:
								if(iManagerState == SYSTEMINTEGRITY_MANAGER_STOPPED)
									bFailed = pmon_command(sPmonCommand, sHost, iPort, FALSE, TRUE);
								else
									bFailed = true;
								break;
							case SYSTEMINTEGRITY_SETMANUAL_COMMAND:
								sPmonCommand = sPmonCommand+
											" " + SYSTEMINTEGRITY_MANAGER_MANUAL +	//Mode
											" " + ddsResult[i][3] +															//Seconds to kill
											" " + ddsResult[i][4] +																//Restart
											" " + ddsResult[i][5] +																//Reset Start counter
											" " + ddsResult[i][6];									//Command Line
								bFailed = pmon_command(sPmonCommand, sHost, iPort, FALSE, TRUE);
								break;
							case SYSTEMINTEGRITY_SETALWAYS_COMMAND:
								sPmonCommand = sPmonCommand+
											" " + SYSTEMINTEGRITY_MANAGER_ALWAYS +	//Mode
											" " + ddsResult[i][3] +															//Seconds to kill
											" " + ddsResult[i][4] +																//Restart
											" " + ddsResult[i][5] +																//Reset Start counter
											" " + ddsResult[i][6];									//Command Line
								bFailed = pmon_command(sPmonCommand, sHost, iPort, FALSE, TRUE);
								break;
							default:
								if((iManagerState == SYSTEMINTEGRITY_MANAGER_RUNNING) || (iManagerState == SYSTEMINTEGRITY_MANAGER_BLOCKED) || (myManType() == UI_MAN && iManagerState == SYSTEMINTEGRITY_MANAGER_INIT))
									bFailed = pmon_command(sPmonCommand, sHost, iPort, FALSE, TRUE);

								else
									bFailed = (iManagerState != SYSTEMINTEGRITY_MANAGER_STOPPED);

								break;
							}
						if(bFailed)
							fwException_raise(exceptionInfo, "ERROR", "unSystemIntergity_manageManagerperHostname() - " + getCatStr("unSystemIntegrity", "CANNOT_EXECUTE_PMON_COMMAND") + sPmonCommand, "");
						}//end: sPmonCommand != ""
					i = iLen+1;	// exit the loop;
					}// end: if the manager is found with the command line
				}// end: dynlen(ddsResult[i])>=6
			}//end: for
		}
	else
		fwException_raise(exceptionInfo, "ERROR", "unSystemIntergity_manageManagerperHostname() - " + getCatStr("unSystemIntegrity", "CANNOT_CONNECT_PMON"), "");

	iManagerIndex = iIndex;
//	if(iManagerIndex==SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE)
//		DebugN("unSystemIntergity_manageManagerperHostname() - Manager not defined: " + sManager + " " + sCommandLine, sHost, iPort);
}

// unSystemIntergity_getManagerState
/**
Purpose:
Stop, kill, start a manager corresponding to sManager and sCommandLine

	@param iManagerIndex: int, input, the index of the PVSS manager returned by unSystemIntergity_manageManager
	@param iManagerState: int, output, the state of the PVSS manager:
	@param exceptionInfo: dyn_string, output, the errors are returned here
	@param dsResult: dyn_string, output, Full ManagerState

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
dyn_string unSystemIntergity_getManagerState(int iManagerIndex, int &iManagerState, dyn_string &exceptionInfo, string sSystem="")
{
int iPort;
string sHost, sIP;
dyn_string dsResult;

	if (sSystem=="")
		sSystem=getSystemName();

	unGenericDpFunctions_getHostName(sSystem, sHost, sIP);
	unGenericDpFunctions_getpmonPort(sSystem, iPort);

	if (sHost=="" || iPort==0)
		{
		sHost=getHostname();
		iPort=pmonPort();
		}

	if (sHost!="" && iPort>0)
		dsResult=unSystemIntergity_getManagerStateperHostname(iManagerIndex, sHost, iPort, iManagerState, exceptionInfo);
	else
		fwException_raise(exceptionInfo, "ERROR", "unSystemIntergity_getManagerState() - Unable to get the Hostname or PMON port number!", "");

	return dsResult;
}

// unSystemIntergity_getManagerStateperHostname
/**
Purpose:
Stop, kill, start a manager corresponding to sManager and sCommandLine on a Local or Remote system

	@param iManagerIndex: int, input, the index of the PVSS manager returned by unSystemIntergity_manageManager
	@param sHost: string, input, Hostname
	@param iPort: int, input, Port number of the Host
	@param iManagerState: int, output, the state of the PVSS manager:
	@param exceptionInfo: dyn_string, output, the errors are returned here
	@param dsResult: dyn_string, output, Full ManagerState

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

dyn_string unSystemIntergity_getManagerStateperHostname(int iManagerIndex, string sHost, int iPort, int &iManagerState, dyn_string &exceptionInfo)
{
dyn_dyn_string ddsResult;
dyn_string dsResult;
bool bFailed;
int iState = SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE;

	if(iManagerIndex>SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE)
		{
		bFailed = pmon_query (USERNAME+"#"+PASSWORD+"#"+SYSTEMINTEGRITY_PMON_QUERY_COMMAND, sHost, iPort, ddsResult, FALSE, TRUE);
		if((!bFailed) && (dynlen(ddsResult) > 0))
			{
			if(iManagerIndex < dynlen(ddsResult))
				{
				iState = (int)ddsResult[iManagerIndex+1][1];
				dsResult = ddsResult[iManagerIndex+1];
				}
			}
		else
			fwException_raise(exceptionInfo, "ERROR", "unSystemIntergity_getManagerStateperHostname() - " + getCatStr("unSystemIntegrity", "CANNOT_CONNECT_PMON"), "");
		}
	else
		fwException_raise(exceptionInfo, "ERROR", "unSystemIntergity_getManagerStateperHostname() - Manager not defined", "");

	iManagerState = iState;
	return dsResult;
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_getState
/**
Purpose:
get the current state of a manager

	@param sManager: string, input, manager name
	@param sCommandLine: int, input, the manager command line
	@param iManagerState: int, output, the manager state

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_getState(string sManager, string sCommandLine, int &iManagerState)
{
	bool bFailed;
	dyn_dyn_string ddsResult;
	int i, iLen;
	int iIndex=SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE;
	int iPort;
	string sHost, sIP,sSystem;
	dyn_string exceptionInfo;

	iManagerState = SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE;

	if (sSystem=="")
		sSystem=getSystemName();

	unGenericDpFunctions_getHostName(sSystem, sHost, sIP);
	unGenericDpFunctions_getpmonPort(sSystem, iPort);

	if (sHost=="" || iPort==0)
		{
		sHost=getHostname();
		iPort=pmonPort();
		}

	if (sHost!="" && iPort>0)
	{
		// get the list of manager
		bFailed = pmon_query(USERNAME+"#"+PASSWORD+"#MGRLIST:LIST", sHost, iPort, ddsResult, false, true);
		if(!bFailed)
		{
			iLen = dynlen(ddsResult);
			for(i=1;i<=iLen;i++)
			{
				if (dynlen(ddsResult[i])>=6)
				{
					// if the manager is found with the command line - " " is needed to distinguish -num 1 and -num 10
					if((_unSystemIntegrity_removeManagerPrefix(sManager) == _unSystemIntegrity_removeManagerPrefix(ddsResult[i][1])) && (strpos(ddsResult[i][6] + " ",sCommandLine + " ") >=0))
					{
						iIndex=i-1;
						// get the state of the manager
						unSystemIntergity_getManagerStateperHostname(iIndex, sHost, iPort, iManagerState, exceptionInfo);
						i = iLen+1;	// exit the loop;
					}// end: if the manager is found with the command line
				}// end: dynlen(ddsResult[i])>=6
			}//end: for
		}
	}
}

// _unSystemIntegrity_createScriptScheduler
/**
Purpose:
This function creates the dp and sets the scheduler with default settings.

	@param sDp: string, input, data point name = Scheduler name
	@param iEr: int, output, Error code
        @param sSystemName: string, input, the system name, default =""=getSystemName()

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_unSystemIntegrity_createScriptScheduler(string sCat, int &iEr, string sScript, int iInterval, string sSystemName="")
{
  string sDp;
  int iNum;

//DebugN("_unSystemIntegrity_createScheduler", sCat, sSystemName);
  if(sSystemName == "")
    sSystemName = getSystemName();
  if(sSystemName == getSystemName())
    iEr=dpCreate(sCat,"_Sc");
  else
  {
    iNum = getSystemId(sSystemName);
    if(iNum >= 0)
      iEr=dpCreate(sCat,"_Sc", iNum);
//DebugN(iNum, sSystemName, sCat, iEr);
  }
  sDp = sSystemName+sCat;
//DebugN(sDp, dpExists(sDp));
  if(!dpExists(sDp))
    iEr = -1;
  if (iEr>=0)
  {
    dpSetWait(sDp + ".name", sCat,
                  sDp + ".type", "F",
                  sDp + ".mode", true,
                  sDp + ".logEntry", true,
                  sDp + ".actions.script.path", sScript,
                  sDp + ".actions.script.active", true,
                  sDp + ".time.timedFunc.validUntil", makeTime(2034,12,31,23,59,59),
                  sDp + ".time.timedFunc.interval",iInterval,
                  sDp + ".time.timedFunc.syncTime",0,
                  sDp + ".time.timedFunc.syncWeekDay",-1,
                  sDp + ".time.timedFunc.syncDay",1,
                  sDp + ".time.timedFunc.syncMonth",1,
                  sDp + ".time.conditionType",1);															//Periodic

    if(sSystemName == getSystemName())
      dpSetDescription(sDp + ".name", sCat);
    dpSet(sDp + ".actions.reminderAlarm.alarmS:_alert_hdl.._type", DPCONFIG_ALERT_BINARYSIGNAL,
          sDp + ".actions.reminderAlarm.alarmS:_alert_hdl.._ok_range", 0,
          sDp + ".actions.reminderAlarm.alarmS:_alert_hdl.._class", "scAlarm.",
          sDp + ".actions.reminderAlarm.alarmS:_alert_hdl.._orig_hdl", 1,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.._type", DPCONFIG_ALERT_NONBINARYSIGNAL,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.1._type", 4,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.2._type", 4,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.3._type", 4,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.1._u_limit", 0,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.2._u_limit", 1,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.2._l_limit", 0,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.3._l_limit", 1,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.1._u_incl", FALSE,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.2._u_incl", FALSE,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.3._u_incl", TRUE,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.1._l_incl", TRUE,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.2._l_incl", TRUE,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.3._l_incl", TRUE,
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.1._class", "scAlarm.",
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.3._class", "scAlarm.",
          sDp + ".actions.reminderAlarm.alarmD:_alert_hdl.._orig_hdl", 1);
  }
}

// _unSystemIntegrity_startScheduler
/**
Purpose:
This function adds the scheduler.

	@param sDp: string, input, data point name = Scheduler name
	@param iEr: int, output, Error code
        @param sSystemName: string, input, the system name, default =""=getSystemName()

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
_unSystemIntegrity_startScheduler(string sCat, string sDp, string sSystemName="")
{
  dpSetWait(sSystemName+"_ScCom.transfer:_original.._value", sCat +"|"+ dpSubStr(sSystemName+sDp, DPSUB_SYS_DP));
}

/** This function returns the pmon host and port of the given (local) system.
  @param host  output, pmon hostname
  @param port  output, pmon port number
  @param sSystem  input (optional), system name
  @note sSystem should be empty or a local system, otherwise host and port are not guaranteed to be correct in some corner cases.
*/
void unSystemIntegrity_getPmonHostAndPort(string &host, int &port, string sSystem = "")
{
  if(sSystem == "")
    sSystem = getSystemName();

  string ipAddr;
  unGenericDpFunctions_getHostName(sSystem, host, ipAddr);
  unGenericDpFunctions_getpmonPort(sSystem, port);

  if(host == "" || port == 0)
  {
    host = getHostname();
    port = pmonPort();
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_addManager
/** This function allows to insert a manager into a project. It is checked before, if the
manager already exists.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL



@param manager				name of the manager
@param startMode			{manual, once, always}
@param secKill				seconds to kill after stop
@param restartCount		number of restarts
@param resetMin				restart counter reset time (minutes)
@param commandLine		commandline for the manager
@param exceptionInfo	errors are returned here
@param sSystem	the system name, "" = getSystemName()
@return 1 - manager added, 2 - manager already existing, 3 - manager addition disabled, 0 - manager addition failed
*/
int unSystemIntegrity_addManager(string manager,
                              string startMode,
                              int secKill,
                              int restartCount,
                              int resetMin,
                              string commandLine,
                              dyn_string &exceptionInfo,
                              string sSystem ="")
{
  bool failed, disabled;
  string str;
  dyn_mixed managerInfo;
  string sHost, user, pwd;
  int iPort;

  unSystemIntegrity_getPmonHostAndPort(sHost, iPort, sSystem);

  if(fwInstallation_getPmonInfo(user, pwd) != 0)
  {
    fwException_raise(exceptionInfo, "ERROR", "unSystemIntegrity_addManager() - Unable to get the pmon credentials!", "");
    return -1;
  }

  fwInstallationManager_getProperties(manager, commandLine, managerInfo, sHost, iPort, user, pwd);
  if(managerInfo[FW_INSTALLATION_MANAGER_PMON_IDX] != -1)
  {
    return 2; //Manager already in the PVSS console
  }
  str = user + "#" + pwd + "#SINGLE_MGR:INS "+unSystemIntegrity_pmonGetCount(sHost, iPort)+" "+
//  str = user + "#" + pwd + "#SINGLE_MGR:INS "+pmonGetCount()+" "+
        manager+" "+startMode+" "+secKill+" "+restartCount+" "+
        resetMin+" "+commandLine;

  //for drivers, create internal debug dpes
  if( (strpos(commandLine,"-num ") > -1) &&
      patternMatch("*sim",    manager)   ||
      patternMatch("*s7",     manager)   ||
      patternMatch("*mod",    manager)   ||
      patternMatch("*cmw*",   manager)   ||
      patternMatch("*CMW*",   manager)   ||
      patternMatch("*bacnet", manager)    )
  {
    int driver;
    string sDriver;
    dyn_string dsCommandLineSplit;

    //get driver number
    string s = commandLine;

    strreplace(s, "  ", " ");
    dsCommandLineSplit = strsplit(s," ");
    int iDriverPos = dynContains(dsCommandLineSplit,"-num");
    //Look for driver number. If internal dps don't exist, add them
    if( (iDriverPos > -1) && (dynlen(dsCommandLineSplit) > iDriverPos) )
    {
      driver = (int) dsCommandLineSplit[iDriverPos+1];
      if( !dpExists("_Driver" + driver) )
      {
        dpCreate("_Driver" + driver, "_DriverCommon");
      }

      if( !dpExists("_Stat_Configs_driver_" + driver) )
      {
        dpCreate("_Stat_Configs_driver_" + driver, "_Statistics_DriverConfigs");
      }

      if( patternMatch("*bacnet", manager) )
      {
        dpCreate("_Bacnet_" + driver, "_Bacnet");
      }
    }
  }
  if(pmon_command(str, sHost, iPort, FALSE, TRUE))
  {
    fwException_raise(exceptionInfo, "ERROR", "unSystemIntegrity_addManager(): Failed to insert manager: "  + manager + " " + commandLine + " " + sHost + " " + iPort + " "+user, "");
    return 0;
  }
//DebugTN(str, sHost, iPort);
  return 1;
}





//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_pmonGetCount
/** Equivalent to pmonGetCount(). This function returns the number of manager in the console.

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sHost  input, pmon hostname
@param iPort input, pmon port number
@return the number of manager in the console
*/
int unSystemIntegrity_pmonGetCount(string sHost, int iPort)
{
  bool bFailed;
  int iPmonCount = SYSTEMINTEGRITY_PMON_UNKNOWN_VALUE, i, len;
  dyn_dyn_string ddsResult;

  bFailed = pmon_query(USERNAME+"#"+PASSWORD+"#MGRLIST:LIST", sHost, iPort, ddsResult, false, true);
  if(!bFailed)
  {
    len = dynlen(ddsResult);
    for(i=1;i<=len;i++)
    {
      if(dynlen(ddsResult[i]) == 5) // case no argument
        dynAppend(ddsResult[i], "");
      if (dynlen(ddsResult[i])>=6)
      {
        iPmonCount = i;
      }
    }
  }
//  DebugTN(pmonGetCount(), iPmonCount);
  return iPmonCount;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_createDiscreteSystemAlarm
/**
* This creates the dp and sets the alert hdl.
*
* @param sSysAlarmDp       input, name of the alarm dp
* @param sDescription      input, description of the alarm dp
* @param dsAlertTexts      input, alert texts
* @param dsAlertMatch      input, alert values
* @param dsAlertClasses    input, alert classes
* @param exceptionInfo     output, exeption information
*/
public void unSystemIntegrity_createSystemAlarmDiscrete(string sSysAlarmDp, string sDescription, dyn_string dsAlertTexts, dyn_string dsAlertMatch, dyn_string dsAlertClasses, dyn_string &exceptionInfo)
{
  sSysAlarmDp = unSystemIntegrity_removeSystemName(sSysAlarmDp);

  // Create UnSystemAlarm DP and its alarm config if it is not existing
  if(!dpExists(sSysAlarmDp))
  {
    dpCreate(sSysAlarmDp, c_unSystemAlarm_dpType);
    if(!dpExists(sSysAlarmDp)) {
      fwException_raise(exceptionInfo, "ERROR", "_unSystemIntegrity_createSystemAlarm():"+ getCatStr("unSystemIntegrity", "CANNOT_CREATE_DP")+sSysAlarmDp,"");
    }
    else
    {
      string sSysAlarmDpe = sSysAlarmDp + ".alarm";

      // set the description
      dpSetDescription(sSysAlarmDpe, sDescription);

      // prepare some additional values
      dyn_bool dbDiscreteNegation;
      dyn_string dsStateMatch;
      for(int i=1; i<=dynlen(dsAlertTexts); i++) {
        dynAppend(dbDiscreteNegation, false);
        dynAppend(dsStateMatch, "");
      }

      // set discrete alarm config
      dyn_mixed alarmObject;
      fwAlertConfig_objectCreateDiscrete(
        alarmObject,
        dsAlertTexts,
        dsAlertMatch,
        dsAlertClasses,
        "",
        makeDynString(""),
        "",
        false,
        dbDiscreteNegation,
        "",
        dsStateMatch,
        exceptionInfo,
        false,
        false,
        "",
        false); //exception info returned here
      if(dynlen(exceptionInfo)){ return;}
      //set the alarm to the dpe
      fwAlertConfig_objectSet(sSysAlarmDpe, alarmObject, exceptionInfo);
      if(dynlen(exceptionInfo)){ return;}
      //activate it
      fwAlertConfig_activate(sSysAlarmDpe, exceptionInfo);
      if(dynlen(exceptionInfo)){ return;}
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_removeSystemName
/**
* Remove system name from a dpe name
*
* @param sDp       input, dpe string
*
* @return          output, dpe string without system name
*/
public string unSystemIntegrity_removeSystemName(string sDp)
{
  // Remove the system name
  int res = strpos(sDp, ":");
  string dp = res >= 0 ? substr(sDp, res+1, strlen(sDp)) : sDp ;
  return (dp);
}

//------------------------------------------------------------------------------------------------------------------------
// temporary solution for testing
int unSystemIntegrity_ShowWaitWindow(string message, int timeoutSeconds) {
  string sModuleName = "waitWindowModule"+cryptoHash(this.name());
  string sSymbolName = "waitWindowSymbol"+cryptoHash(this.name());
  moduleOff(sModuleName);
  int errorCode = addSymbol(myModuleName(),myPanelName(),"vision/systemIntegrity/systemIntegrity_waitWindowFrame.pnl",sSymbolName,makeDynString("$sModuleName:" + sModuleName,"$sMessage:" + message,"$iTimeoutSeconds:" + timeoutSeconds),0,0,2,1);
  if (errorCode == -1) return errorCode;
  shape embededModuleShape = getShape(sSymbolName +".EMBEDDED_MODULE1");
  int width,height,x,y;
  panelSize(myPanelName(),width,height);
  if (errorCode == -1) return errorCode;
  embededModuleShape.size(width,height);
  return 0;
}
int unSystemIntegrity_HideWaitWindow() {
  string sModuleName = "waitWindowModule"+cryptoHash(this.name());
  int errorCode = moduleOff(sModuleName);
  if (errorCode == -1) return errorCode;
  return 0;
}

bool unSystemIntegrity_showOkCancelWindow(string message, string windowTitle = "")
{
  dyn_float floatAnswer;
  dyn_string stringAnswer;
  string panelPath = "vision/systemIntegrity/systemIntegrity_questionPanel.pnl";
  floatAnswer[1] = 0;
  ChildPanelOnCentralReturn(panelPath, "questionPanel:"+cryptoHash(message), makeDynString("$message:"+message,"$windowTitle:"+windowTitle),  floatAnswer, stringAnswer);
  if(floatAnswer[1] == 1) {
    return true;
  } else {
    return false;
  }

}

bool unSystemIntegrity_showYesNoWindow(string message, string windowTitle = "")
{
  dyn_float floatAnswer;
  dyn_string stringAnswer;
  string panelPath = "vision/systemIntegrity/systemIntegrity_questionPanel.pnl";
  floatAnswer[1] = 0;
  ChildPanelOnCentralReturn(panelPath, "questionPanel:"+cryptoHash(message), makeDynString("$message:"+message,"$windowTitle:"+windowTitle,"$button1Text:Yes","$button2Text:No"),  floatAnswer, stringAnswer);
  if(floatAnswer[1] == 1) {
    return true;
  } else {
    return false;
  }

}
//@}


