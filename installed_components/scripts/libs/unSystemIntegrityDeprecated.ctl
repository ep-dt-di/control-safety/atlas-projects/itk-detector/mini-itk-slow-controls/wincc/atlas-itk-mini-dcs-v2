/** @name LIBRARY: unSystemIntegrityDeprecated.ctl

Creation date: 2018-09-05

Purpose:
This library contains deprecated functions from unSystemIntegrity libraries, 
that may be eventually removed from component distribution. 

*/


//------------------------------------------------------------------------------------------------------------------------
// _unSystemIntegrity_createSystemAlarmBasic
/** This function creates the systemAlarm dp and sets the alert hdl with more than one alert level if needed.
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp  input, data point name
@param description input, device type
@param dsAlertText input, list of alert text
@param dfLimits input, list of alert limits
@param dsAlertClass input, list of alert Class
@param exceptionInfo output, exception are returned here

@deprecated 2018-09-05
*/
_unSystemIntegrity_createSystemAlarmBasic(string sDp, string description, dyn_string dsAlertText, dyn_float dfLimits, dyn_string dsAlertClass, dyn_string &exceptionInfo)
{
  FWDEPRECATED();
  
  string dpe = sDp+".alarm";
  bool isActive;
	
  //DebugN("_unSystemIntegrity_createSystemAlarm", sDp);
  dpCreate(sDp, c_unSystemAlarm_dpType);
  // a second check is done after the creation to ensure that the data point is created (just in case of error)
  // it is also a way to wait the creation which is asynchronous.
  if(!dpExists(sDp)) 
  {
    fwException_raise(exceptionInfo, "ERROR", 
                      "_unSystemIntegrity_createSystemAlarm():"+getCatStr("unSystemIntegrity", "CANNOT_CREATE_DP")+sDp,"");
  }
  else 
  {
    // set its alarm config
    fwAlertConfig_setGeneral(dpe, dsAlertText, dfLimits, dsAlertClass,
                             "", makeDynString(), "", 1, exceptionInfo); 
    // set the description
    dpSetDescription(dpe, description);
  }
}
