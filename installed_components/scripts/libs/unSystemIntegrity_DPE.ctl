/**@name LIBRARY: unSystemIntegrity_DPE.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 12/07/2002

Modification History: 
  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  
  16/06/2011: Herve
  - IS-561: get the list of systemAlarm pattern

	02/07/2004: Herve
		version 3.0
		add c_unSystemIntegrity_defaultDPECheckDelay from unSystemIntegrity.ctl

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the DPE component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: DPE_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_DPE = "DPE";
const string UN_SYSTEM_INTEGRITY_DPE_check = "unSystemIntegrity_DPECheck";
const string DPE_pattern = "DPE_";
const int c_unSystemIntegrity_defaultDPECheckDelay = 10; // default value for the check DPE in sec.

// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_DPEList; // list of the DPE dp to check
global dyn_int g_unSystemIntegrity_DPE_ThreadId; // list of the thread Id checking the DPE
global int g_unSystemIntegrity_DPECheckingDelay;

//@{

//------------------------------------------------------------------------------------------------------------------------
// DPE_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string DPE_systemIntegrityInfo()
{
  return makeDynString(DPE_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_DPE_Initialize
/**
Purpose:
Get the list of DPE check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled DPE that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_DPE_Initialize(dyn_string &dsResult)
{
	dyn_string dsList;
	int len, i;
	string dpToCheck;
	bool enabled;

	dpGet(UN_SYSTEM_INTEGRITY_DPE+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
	unSystemIntegrity_DPE_DataCallback("", dsList);

	dsList = dpNames(c_unSystemAlarm_dpPattern+DPE_pattern+"*", c_unSystemAlarm_dpType);
//DebugN("DPE:", dsList);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+DPE_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
	}
//DebugN(dsResult);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_DPE_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_DPE_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo;
	int i, len =dynlen(parameters);
  string sMessage;
	
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_DPE_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_DPE_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+DPE_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+DPE_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+DPE_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+DPE_pattern+parameters[i]))
					unSystemIntegrity_DPE_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+DPE_pattern+parameters[i]))
					unSystemIntegrity_DPE_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(UN_SYSTEM_INTEGRITY_DPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_DPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_DPEList);
			break;
    case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
      dpSet(UN_SYSTEM_INTEGRITY_DPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_DPE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
		default:
			break;
	}

	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_DPE_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error in case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_DPE_HandleCommand", exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_DPEList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_DPE_DataCallback
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param dsConfigData: dyn_string, input, the config data

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_DPE_DataCallback(string sDpe1, dyn_string dsConfigData)
{
	int DPECheckingDelay;

	if(dynlen(dsConfigData) > 0)
		DPECheckingDelay = (int)dsConfigData[1];
//DebugN(DPECheckingDelay , dsConfigData);
	g_unSystemIntegrity_DPECheckingDelay = DPECheckingDelay;
	if(g_unSystemIntegrity_DPECheckingDelay <= 0)
		g_unSystemIntegrity_DPECheckingDelay = c_unSystemIntegrity_defaultDPECheckDelay;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_DPE_checking
/**
Purpose:
This function register/de-register the callback funtion of DPE manager. This function can also create the _unSystemAlarm_DPE dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_DPE_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string dp, description, alarmDp;
	int res, pos, thId;
	bool bError= false;
	
// remove the system name

	dp = dpSubStr(sDp, DPSUB_DP);
//DebugN("unSystemIntegrity_DPE_checking", dp, bCreate, bRegister);
	alarmDp = c_unSystemAlarm_dpPattern+DPE_pattern+dp;
	if(bCreate) {
// create the sDp and its alarm config if it is not existing
		if(!dpExists(alarmDp)) {
			description = getCatStr("unSystemIntegrity", "DPE_DESCRIPTION")+dp;
			unSystemIntegrity_createSystemAlarm(dp, DPE_pattern, description, exceptionInfo);
		}
	}

	if(dynlen(exceptionInfo)<=0) {
		if(bRegister) {
//DebugN("Enable", g_unSystemIntegrity_DPE_ThreadId, "end");
			pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_DPEList, alarmDp);
			if(pos <= 0) {
	// add it in the list, because one of the callback function needs it.				
				pos = _unSystemIntegrity_setList(g_unSystemIntegrity_DPEList, alarmDp, true);

// start the thread for checking the counter
				thId = startThread(UN_SYSTEM_INTEGRITY_DPE_check, alarmDp, dp);
				g_unSystemIntegrity_DPE_ThreadId[pos] = thId;
//DebugN(thId);
				if(thId<0)
					bError = true;

				if(bError) {

					pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_DPEList, alarmDp);
					if(pos > 0) {
		// kill the threads if they were started
						res = stopThread(g_unSystemIntegrity_DPE_ThreadId[pos]);
					}

					fwException_raise(exceptionInfo, "ERROR", 
							"_unSystemIntegrity_set_DPE_checking():"+getCatStr("unSystemIntegrity", "CANNOT_START_THREAD") +dp,"");
				}
				else {
//DebugN("Enable", g_unSystemIntegrity_DPE_ThreadId, "end");
			// set the enable to true and activate the alarm if it is not activated.
					dpSet(alarmDp+".enabled", true);
					unAlarmConfig_mask(alarmDp+".alarm", false, exceptionInfo);
				}
			}
		}
		else {
//DebugN("Disable", g_unSystemIntegrity_DPE_ThreadId, "end");
			pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_DPEList, alarmDp);
			if(pos > 0) {
// kill the thread
				res = stopThread(g_unSystemIntegrity_DPE_ThreadId[pos]);
				if(res<0)
					bError = true;

			// set the enable to false and de-activate the alarm.
				dpSet(alarmDp+".enabled", false, alarmDp+".alarm", c_unSystemIntegrity_no_alarm_value);
				unAlarmConfig_mask(alarmDp+".alarm", true, exceptionInfo);
				
				if(bError) {
					fwException_raise(exceptionInfo, "ERROR", 
							"_unSystemIntegrity_set_DPE_checking():"+getCatStr("unSystemIntegrity", "CANNOT_STOP_THREAD") +dp,"");
				}
				else {
			// remove from list
					pos = _unSystemIntegrity_setList(g_unSystemIntegrity_DPEList, alarmDp, false);
//DebugN("rmv", res);
					dynRemove(g_unSystemIntegrity_DPE_ThreadId, pos);
				}
			}
		}
	}

}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_DPECheck
/**
Purpose:
Function to check the value of a DPE. 
Set an alarm if the value has not changed and if the time has not changed and the stime_inv bit is set to false.
in the other case reset it.

	@param sAlarmDp: string, input, alarm dp
	@param dpToCheck: string, input, dpe to check

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_DPECheck(string sAlarmDp, string sDpToCheck)
{
	int waitingTime;
	int oldValue = -1, newValue, alarmValue, oldAlarmValue = -1;
//	time oldTime=0, newTime;
	bool bStimeInv;
	bool bEnabled;

//DebugN(g_unSystemIntegrity_DPECheckingDelay, sAlarmDp, sDpToCheck);
	// get the value of the DPE and the old time
	//dpGet(sDpToCheck+".", oldValue/*, sDpToCheck+".:_online.._stime", oldTime);
	dpGet(sDpToCheck+".", oldValue);
	while(true) {
	// if 0 set it to c_unSystemIntegrity_defaultDPECheckDelay
		waitingTime = g_unSystemIntegrity_DPECheckingDelay;
		if(waitingTime <= 0)
			waitingTime = c_unSystemIntegrity_defaultDPECheckDelay;
	// wait
		delay(waitingTime);
	
	// get the value of the counter and the timeout PLCChecking
		dpGet(sDpToCheck+".", newValue/*, sDpToCheck+".:_online.._stime", newTime*/, sDpToCheck+".:_online.._stime_inv", bStimeInv);

//		if( bStimeInv || ((newValue == oldValue) && (newTime == oldTime)) ){
		if( bStimeInv || (newValue == oldValue) ){
	// counter was not modified, set an alarm
			alarmValue = c_unSystemIntegrity_alarm_value_level1;
		}
		else {	
	// counter was modified, reset the alarm
			alarmValue = c_unSystemIntegrity_no_alarm_value;
		}
		if(alarmValue != oldAlarmValue)
			dpSet(sAlarmDp+".alarm", alarmValue);

//DebugN("unSystemIntegrity_DPECheck", sDpToCheck, oldValue, newValue, oldTime, newTime, alarmValue);	
		oldValue = newValue;
		oldAlarmValue = alarmValue;
//		oldTime = newTime;
	}
}

//@}
