/**@name LIBRARY: unSystemIntegrity_File.ctl

*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_File = "File";
const string UN_SYSTEM_INTEGRITY_File_check = "unSystemIntegrity_FileCheck";
const int File_Command_Update = 5;
const string File_pattern = "File_";
const string File_pattern_separator = "^";
const int c_unSystemIntegrity_defaultFileCheckDelay =60; // default time between the checks of the File in sec.
const int c_unSystemIntegrity_defaultFileUpdateDelay =600; // default time between the updates of the File in sec.

// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_FileList; // list of the File dp to check
global dyn_int g_unSystemIntegrity_File_ThreadId; // list of the thread Id checking the File
global int g_unSystemIntegrity_FileCheckingDelay;
global int g_unSystemIntegrity_FileUpdateDelay;

//@{

//------------------------------------------------------------------------------------------------------------------------
// File_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string File_systemIntegrityInfo()
{
  return makeDynString(File_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_File_Initialize
/**
Purpose:
Get the list of File check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled File that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:

*/
unSystemIntegrity_File_Initialize(dyn_string &dsResult)
{
	dyn_string dsList;
	int len, i;
	string dpToCheck;
	bool enabled;

	dpGet(UN_SYSTEM_INTEGRITY_File+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
	unSystemIntegrity_File_DataCallback("", dsList);

	dsList = dpNames(c_unSystemAlarm_dpPattern+File_pattern+"*", c_unSystemAlarm_dpType);
//DebugN("File:", dsList);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+File_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
	}
//DebugN(dsResult);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_File_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:

*/
unSystemIntegrity_File_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{

	dyn_string exceptionInfo;
	int i, len =dynlen(parameters);
  string sMessage;
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_File_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
   case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_File_checking(parameters[i], false, false, exceptionInfo);
      string alarmDp = parameters[i];
      strreplace(alarmDp, "\\", "/");
      strreplace(alarmDp, ".", "");
      strreplace(alarmDp, " ", ""); 
      dyn_string strArr;
      strArr = strsplit(alarmDp, File_pattern_separator);
      strArr = strsplit(strArr[1], "/");
      len = dynlen(strArr);
      string dp = len>2?strArr[len-1]+"_"+strArr[len]:strArr[len];
      alarmDp = c_unSystemAlarm_dpPattern+File_pattern+dp;
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(alarmDp, false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(alarmDp))
					dpDelete(alarmDp);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
					unSystemIntegrity_File_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
					unSystemIntegrity_File_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(UN_SYSTEM_INTEGRITY_File+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_File+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_FileList);
			break;
  case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
      dpSet(UN_SYSTEM_INTEGRITY_File+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_File+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
		default:
			break;
	}

	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_File_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error in case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_File_HandleCommand", exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_FileList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_File_DataCallback
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param dsConfigData: dyn_string, input, the config data

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:

*/
unSystemIntegrity_File_DataCallback(string sDpe1, dyn_string dsConfigData)
{
  int fileCheckingDelay;
  int fileUpdateDelay;
  if(dynlen(dsConfigData) > 0)
    fileCheckingDelay = (int)dsConfigData[1];
  if(dynlen(dsConfigData) > 1)
    fileUpdateDelay = (int)dsConfigData[2];
  
  g_unSystemIntegrity_FileCheckingDelay = fileCheckingDelay;
  g_unSystemIntegrity_FileUpdateDelay = fileUpdateDelay;  
  
  if(g_unSystemIntegrity_FileCheckingDelay <= 0)
    g_unSystemIntegrity_FileCheckingDelay = c_unSystemIntegrity_defaultFileCheckDelay;
  
   if(g_unSystemIntegrity_FileUpdateDelay <= 0)
    g_unSystemIntegrity_FileUpdateDelay = c_unSystemIntegrity_defaultFileUpdateDelay;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_File_checking
/**
Purpose:
This function register/de-register the callback funtion of File manager. This function can also create the _unSystemAlarm_File dp 
with the alarm config but it cannot delete it.

	@param fileName: string, input, file/folder name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_File_checking(string fileName, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
  string  description, alarmDp;
  int res, pos, thId;
  bool bError= false;

  string originalFileName = fileName;
  strreplace(fileName, "\\", "/");
  strreplace(fileName, ".", "");
  strreplace(fileName, " ", ""); 
  dyn_string strArr;
  strArr = strsplit(fileName, File_pattern_separator);
  strArr = strsplit(strArr[1], "/");
  int len = dynlen(strArr);
  string dp = len>2?strArr[len-1]+"_"+strArr[len]:strArr[len];
  alarmDp = c_unSystemAlarm_dpPattern+File_pattern+dp;
  if (bCreate)
  {
    if(!dpExists(alarmDp))
    {
      dyn_string tmpArr = strsplit(originalFileName, File_pattern_separator);
      description = "Check " + tmpArr[1];
   			unSystemIntegrity_createSystemAlarm(dp, File_pattern, description, exceptionInfo);
      dpSetAlias(alarmDp + ".", originalFileName);
  		}
   
  }
  

	if(dynlen(exceptionInfo)<=0 && dpExists(alarmDp))
 {
    if(bRegister) 
    {
      //DebugN("Enable", g_unSystemIntegrity_File_ThreadId, "end");
    		pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_FileList, alarmDp);

			   if(pos <= 0) 
      {
        // add it in the list, because one of the callback function needs it.				
				    pos = _unSystemIntegrity_setList(g_unSystemIntegrity_FileList, alarmDp, true);
        // start the thread for checking the counter
        string sFileToCheck = dpGetAlias(alarmDp + ".");
				    thId = startThread(UN_SYSTEM_INTEGRITY_File_check, alarmDp, sFileToCheck);
		    		g_unSystemIntegrity_File_ThreadId[pos] = thId;
        //DebugN(thId);    
    				if(thId<0)
      				bError = true;

				    if(bError) 
        {
          pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_FileList, alarmDp);
					     if(pos > 0) 
          {
          		// kill the threads if they were started
      						res = stopThread(g_unSystemIntegrity_File_ThreadId[pos]);
      				}

				     	fwException_raise(exceptionInfo, "ERROR", 
				                    			"_unSystemIntegrity_set_File_checking():"+getCatStr("unSystemIntegrity", "CANNOT_START_THREAD") +dp,"");
				    }
				  else 
      {
        //DebugN("Enable", g_unSystemIntegrity_File_ThreadId, "end");
      		// set the enable to true and activate the alarm if it is not activated.
					   dpSet(alarmDp+".enabled", true);
					   unAlarmConfig_mask(alarmDp+".alarm", false, exceptionInfo);
				  }
			  }
		  }
	  	else
    {
      //DebugN("Disable", g_unSystemIntegrity_File_ThreadId, "end");
    		pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_FileList, alarmDp);
			  if(pos > 0) 
      {
        // kill the thread
    				res = stopThread(g_unSystemIntegrity_File_ThreadId[pos]);
    				if(res<0)
      				bError = true;

			     // set the enable to false and de-activate the alarm.
    				dpSet(alarmDp+".enabled", false, alarmDp+".alarm", c_unSystemIntegrity_no_alarm_value);
    				unAlarmConfig_mask(alarmDp+".alarm", true, exceptionInfo);
				
    				if(bError) 
        {
      				fwException_raise(exceptionInfo, "ERROR", 
				                      			"_unSystemIntegrity_set_File_checking():"+getCatStr("unSystemIntegrity", "CANNOT_STOP_THREAD") +dp,"");
				    }
				    else 
        {
        		// remove from list
					     pos = _unSystemIntegrity_setList(g_unSystemIntegrity_FileList, alarmDp, false);
          //DebugN("rmv", res);
					     dynRemove(g_unSystemIntegrity_File_ThreadId, pos);
				    }
			  }
		  }
	}

}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_FileCheck
/**
Purpose:
Function to check the value of a File. 
Set an alarm if the value has not changed and if the time has not changed and the stime_inv bit is set to false.
in the other case reset it.

	@param sAlarmDp: string, input, alarm dp
	@param dpToCheck: string, input, dpe to check

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_FileCheck(string sAlarmDp, string sFileToCheck)
{
  int waitingTime;
  int alarmValue = 0;
  int alarmOldValue = -1;
  string pattern = "*";
  string filePath; 
  int updateDelay;
	 while(true) 
  {
     int updateTime;
    /* int pos = strpos(sFileToCheck, File_pattern_separator);
     int updateTime = g_unSystemIntegrity_FileUpdateDelay;
     if (pos >=0 )
     {
       dyn_string tmpArr = strsplit(sFileToCheck, File_pattern_separator);
       sFileToCheck = tmpArr[1];
       if(dynlen(tmpArr) > 1)
         pattern = tmpArr[2];   
       if(dynlen(tmpArr) > 2)
       {
         updateTime =(int) tmpArr[3] >  g_unSystemIntegrity_FileUpdateDelay? tmpArr[3]:g_unSystemIntegrity_FileUpdateDelay;
       }
           
     } 	*/

     unSystemIntegrity_File_getConfiguration(sAlarmDp, filePath,	pattern, updateDelay);
     updateTime = updateDelay > g_unSystemIntegrity_FileUpdateDelay?updateDelay:g_unSystemIntegrity_FileUpdateDelay;
     alarmValue = 0;

   	// if 0 set it to c_unSystemIntegrity_defaultFileCheckDelay
  		waitingTime = g_unSystemIntegrity_FileCheckingDelay;
    
    if (filePath != "" && (isDirectory(filePath) || isfile(filePath)))
    {
      if (isDirectory(filePath)) // when it is directory check whetehr all files in it has been modified since last time
      {
        dyn_string files = fwInstallation_getFileNamesRec(filePath, pattern);
        for(int i=1; i <= dynlen(files); i++)
        {
          time t=  getFileModificationTime(filePath + "/" + files[i]);
          int secondsSinceLastUpdate = (int)(getCurrentTime() - t);

          if (secondsSinceLastUpdate > updateTime)
          {

            alarmValue = 2 * c_unSystemIntegrity_alarm_value_level1;
            break;
          }
        }
       
      }
      else
      {
        time t=  getFileModificationTime(filePath);
        int secondsSinceLastUpdate = (int)(getCurrentTime() - t);
        alarmValue = secondsSinceLastUpdate > updateTime?2*c_unSystemIntegrity_alarm_value_level1:0;
      }
    }
    else 
    {
      alarmValue = c_unSystemIntegrity_alarm_value_level1;
    }   
    // set the value only if it is different than the previous
    if (alarmOldValue != alarmValue)
      dpSet(sAlarmDp+".alarm", alarmValue);
    
    alarmOldValue = alarmValue;

    delay(waitingTime);
	}
}

bool unSystemIntegrity_File_getConfiguration(string sDp, string& filePath, string& pattern, int& updateDelay)
{
  string path = dpGetAlias(sDp + ".");
  int pos = strpos(path, File_pattern_separator);
  if (pos > 0)
  {
    dyn_string tmpArr = strsplit(path, File_pattern_separator);
    if (dynlen(tmpArr) > 1)
    {
      filePath  = tmpArr[1];
      pattern = tmpArr[2];
    }
    if(dynlen(tmpArr) > 2)
      updateDelay = (int)tmpArr[3];
  }
  else
  {
    filePath = path;
  }
}


void unSystemIntegrity_File_setConfiguration(string sDp, string pattern, int updateDelay)
{
  string currentFilePath, currentPattern;
  int currentUpdateDelay;
  if(dpExists(sDp))
  {
    unSystemIntegrity_File_getConfiguration(sDp, currentFilePath, currentPattern, currentUpdateDelay);
    string updateDelayStr =  updateDelay!=0?updateDelay:"";
    string newAlias = currentFilePath + File_pattern_separator + pattern + File_pattern_separator +updateDelayStr;
    dpSetAlias(sDp + ".", newAlias);
  }
}
//@}
