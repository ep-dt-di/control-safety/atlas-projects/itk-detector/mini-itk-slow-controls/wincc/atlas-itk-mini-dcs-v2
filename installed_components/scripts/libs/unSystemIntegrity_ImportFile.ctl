/**@name LIBRARY: unSystemIntegrity_ImportFile.ctl

@author: Frederic BERNARD (AB-CO)

Creation Date: 15/08/2005

Modification History: 
  16/06/2011: Herve
  - IS-547: get the list of systemAlarm pattern
  
	14/01/2006: Herve
		- remove ImportFile_pattern
		- move IMPORT_PATTERN to unicosObjects_declarations.ctl
		- add system alarm at startup and check if DPT is S7_PLC

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library to check Import File version
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: ImportFile_systemIntegrity
	. PVSS version: 3.0.1
	. operating system: W2000, XP, Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_IMPORT = "ImportFile";
const string UN_SYSTEM_INTEGRITY_IMPORT_CALLBACK = "_unSystemIntegrity_ImportFileCallback";
//------------------

// global declaration
dyn_string g_unSystemIntegrity_ImportFileList; 	// list of the IMPORT_PATTERN dp checked

//@{

//------------------------------------------------------------------------------------------------------------------------
// ImportFile_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string ImportFile_systemIntegrityInfo()
{
  return makeDynString(IMPORT_PATTERN);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_ImportFile_Initialize
/**
Purpose:
Get the list of Import File Version check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of version that are checked

PVSS manager usage: CTRL (WCCOActrl)

*/
unSystemIntegrity_ImportFile_Initialize(dyn_string &dsResult)
{
dyn_string dsList;
int len, i, iRes;
string dpToCheck;
bool enabled;

	// get the list
	dsList = dpNames(c_unSystemAlarm_dpPattern + IMPORT_PATTERN + "*", c_unSystemAlarm_dpType);

	len = dynlen(dsList);
	for(i = 1; i<=len; i++)
	{
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);	
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+IMPORT_PATTERN),strlen(dpToCheck));
		if(dpExists(dpToCheck))
		{
			if(dpTypeName(dpToCheck) == S7_PLC_DPTYPE) 
			{
				dpGet(dsList[i]+".enabled", enabled);
				
				if(enabled)
					dynAppend(dsResult, dpToCheck);
			}
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_ImportFile_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

*/
unSystemIntegrity_ImportFile_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
dyn_string exceptionInfo;
int i, len =dynlen(parameters);
		
	switch(command)
		{
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++)
				unSystemIntegrity_ImportFile_checking(parameters[i], true, true, exceptionInfo);
			break;
			
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++)
				{
				unSystemIntegrity_ImportFile_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps				
				if(dpExists(c_unSystemAlarm_dpPattern+IMPORT_PATTERN+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+IMPORT_PATTERN+parameters[i]);
				}
			break;
			
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++)
				{
				if(dpExists(c_unSystemAlarm_dpPattern+IMPORT_PATTERN+parameters[i]))
					unSystemIntegrity_ImportFile_checking(parameters[i], false, true, exceptionInfo);
				}
			break;
			
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++)
				{
				if(dpExists(c_unSystemAlarm_dpPattern+IMPORT_PATTERN+parameters[i]))
					unSystemIntegrity_ImportFile_checking (parameters[i], false, false, exceptionInfo);
				}
			break;
			
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(	UN_SYSTEM_INTEGRITY_IMPORT + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.commandResult", command, 
							UN_SYSTEM_INTEGRITY_IMPORT + UN_SYSTEMINTEGRITY_EXTENSION + ".diagnostic.result", g_unSystemIntegrity_ImportFileList);
			break;
			
		default:
			break;
		}

	if(dynlen(exceptionInfo) > 0)
		{
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", "unSystemIntegrity_ImportFile_HandleCommand", "user", "*", exceptionInfo);
		}
		
// handle any error uin case the send message failed
	if(dynlen(exceptionInfo) > 0)
		{
		DebugN(getCurrentTime(), exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_ImportFileList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_ImportFile_checking
/**
Purpose:
This function register/de-register the callback funtion. This function can create the _unSystemAlarm dp 
with the alarm config but it cannot delete it.

	@param sDp_Num: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfoTemp: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 2.12.1 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_ImportFile_checking(string sFE, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
string sSystem, sDp_PLC, sDp_PVSS, sDp_Alarm, dpeToCheck_1, dpeToCheck_2;
int iRes;
	
	sSystem=getSystemName();
		
	//Get Front End DpN
	sDp_PLC = sSystem + sFE;
	
	//Get System Alarm
	sDp_Alarm = c_unSystemAlarm_dpPattern + IMPORT_PATTERN + sFE;
	
	dpeToCheck_1 = sDp_PLC + ".version.PLC";				//PLC config file version
	dpeToCheck_2 = sDp_PLC + ".version.PVSS"; 			//PVSS config file versio

	if (dpExists(dpeToCheck_1) && dpExists(dpeToCheck_2)) 
		{	
		if(bCreate)
			{
			// create the sDp and its alarm config if it is not existing
			unSystemIntegrity_createSystemAlarm(sFE, IMPORT_PATTERN, getCatStr("unSystemIntegrity", "IMPORT_DESCRIPTION") + sFE, exceptionInfo);
			}
	
		if(dynlen(exceptionInfo)<=0)
			{		
			_unSystemIntegrity_registerFunction(	bRegister,
																						UN_SYSTEM_INTEGRITY_IMPORT_CALLBACK,
																						makeDynString(dpeToCheck_1, dpeToCheck_2),
																						g_unSystemIntegrity_ImportFileList,
																						sDp_Alarm, exceptionInfo);
			}
		}
	else
		{
		fwException_raise(exceptionInfo, "ERROR", "unSystemIntegrity_ImportFile_checking(): " + getCatStr("unSystemIntegrity", "WRONG_ALARM_DPE") + sDp_PLC, "");	
		
		}
}

//------------------------------------------------------------------------------------------------------------------------

// _unSystemIntegrity_ImportFileCallback
/**
Purpose:
Callback function for the command interface. This function is called when the system alarm is enabled for this device.

	@param sDatapoint_1: string, input, data point name for PLC config file version
	@param fPLC: float, input, value of PLC config file version
	@param sDatapoint_2: string, input, data point name for PVSS config file version
	@param fPVSS: float, input, value of PVSS config file version

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 2.12.1 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

_unSystemIntegrity_ImportFileCallback(string sDatapoint_1, float fPLC, string sDatapoint_2, float fPVSS)
{
dyn_string exceptionInfo;
string sFE, sDp, sPLC, sPVSS;
int iValue, iRes;
			
	sFE=substr(sDatapoint_1, strpos(sDatapoint_1, ":") + 1);
	sFE=substr(sFE, 0, strpos(sFE, "."));			

  sprintf(sPLC,"%03.2f",fPLC);
  sprintf(sPVSS,"%03.2f",fPVSS);    
	if ((sPLC==sPVSS) && (fPLC!=0.0))
		iValue = 0;
	else
		iValue = c_unSystemIntegrity_alarm_value_level1;
		
	sDp=c_unSystemAlarm_dpPattern + IMPORT_PATTERN + sFE;	
	if (dpExists(sDp))
		iRes=dpSet(sDp + ".alarm", iValue);
	else
		fwException_raise(exceptionInfo, "ERROR", "_unSystemIntegrity_ImportFileCallback(): " + getCatStr("unSystemIntegrity", "WRONG_ALARM_DPE") + sDp, "");	
	
	//DebugN("ImportFileCallback => fPLC_version: " + fPLC, "fPVSS_version: " + fPVSS, "iValue: " + iValue);
			
	if(isFunctionDefined("unMessageText_sendException")) 
		unMessageText_sendException("*", "*", "SystemIntegrity", "user", "*", exceptionInfo);

}

//------------------------------------------------------------------------------------------------------------------------
//@}
