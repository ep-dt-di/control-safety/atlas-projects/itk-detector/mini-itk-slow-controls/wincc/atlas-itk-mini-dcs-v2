/**@name LIBRARY: unSystemIntegrity_api.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 12/07/2002

Modification History: 
  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  
  16/06/2011: Herve
  - IS-561: get the list of systemAlarm pattern

	20/12/2006: Herve
		- optimization in callback

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the api component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: api_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_API = "api";
const string UN_SYSTEM_INTEGRITY_API_callback = "unSystemIntegrity_api_CheckingCallback";
const string api_pattern = "api_";

// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_apiList; // list of the api dp checked
global mapping g_mSystemIntegrityApi;

//@{

//------------------------------------------------------------------------------------------------------------------------
// api_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string api_systemIntegrityInfo()
{
  return makeDynString(api_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_api_Initialize
/**
Purpose:
Get the list of api check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled api that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_api_Initialize(dyn_string &dsResult)
{
	dyn_string dsList;
	int len, i;
	string dpToCheck;
	bool enabled;
	
// get the list of api manager dp to checked
	dsList = dpNames(c_unSystemAlarm_dpPattern+api_pattern+"*", c_unSystemAlarm_dpType);
//DebugN("api:", dsList);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+api_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
	}
//DebugN(dsResult);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_api_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_api_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo;
	int i, len =dynlen(parameters);
  string sMessage;
	
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_api_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_api_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+api_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+api_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+api_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+api_pattern+parameters[i]))
					unSystemIntegrity_api_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+api_pattern+parameters[i]))
					unSystemIntegrity_api_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(UN_SYSTEM_INTEGRITY_API+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_API+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_apiList);
			break;
    case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
      dpSet(UN_SYSTEM_INTEGRITY_API+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_API+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
		default:
			break;
	}

	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_api_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error in case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_api_HandleCommand", exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_apiList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_api_checking
/**
Purpose:
This function register/de-register the callback funtion of api manager. This function can also create the _unSystemAlarm_api dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_api_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string sApiNum, dp, description;
	int res;
	
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;
	sApiNum = dp;
	dp = c_unSystemAlarm_dpPattern+api_pattern+dp;
//DebugN("_unSystemIntegrity_set_api_checking", dp, bCreate, bRegister, sApiNum);
	g_mSystemIntegrityApi[sApiNum] = -1;
	if(bCreate) {
// create the sDp and its alarm config if it is not existing
		if(!dpExists(dp)) {
			description = getCatStr("unSystemIntegrity", "API_DESCRIPTION")+sApiNum;
			unSystemIntegrity_createSystemAlarm(sApiNum, api_pattern, description, exceptionInfo);
		}
	}

	if(dynlen(exceptionInfo)<=0)
		_unSystemIntegrity_registerFunction(bRegister, UN_SYSTEM_INTEGRITY_API_callback, makeDynString("_Connections.Api.ManNums"), 
					g_unSystemIntegrity_apiList, dp, exceptionInfo);

}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_api_CheckingCallback
/**
Purpose:
Callback function. This function is called when the system alarm is enabled for this device.

	@param sDp1: string, input, data point name
	@param manNum: dyn_int, input, list of api numbers connected to the event manager

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_api_CheckingCallback(string sDp1, dyn_int manNum)
{
	int value, i, len, apiNum, pos;
	dyn_string dsTemp;
	string sApiNum;
	
	dsTemp = g_unSystemIntegrity_apiList;
//	DebugN(getCurrentTime(), "API", sDp1, manNum, dsTemp);
	len = dynlen(dsTemp);
	for(i=1; i<=len; i++) {
		sApiNum = substr(dsTemp[i], strlen(c_unSystemAlarm_dpPattern+api_pattern),strlen(dsTemp[i]));
		sscanf(sApiNum, "%d", apiNum);
		pos = dynContains(manNum, apiNum);
		if(pos>0) { // api found
			value = c_unSystemIntegrity_no_alarm_value;
		}
		else { //api not found
			value = c_unSystemIntegrity_alarm_value_level1;
		}
		if(g_mSystemIntegrityApi[sApiNum] != value) {
			dpSet( c_unSystemAlarm_dpPattern+api_pattern+sApiNum+".alarm", value);
			g_mSystemIntegrityApi[sApiNum] = value;
		}
	}
}

//@}
