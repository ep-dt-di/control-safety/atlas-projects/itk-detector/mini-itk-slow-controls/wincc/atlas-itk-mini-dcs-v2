/**@name LIBRARY: unSystemIntegrity_archive.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 12/07/2002

Modification History: 
  19/09/2011: Herve
  - IS-602: valarch systemIntegrity: raise an alarm if the AR_x file size is bigger than 90Mb
 
  05/09/2011: Herve
  - IS-597: no kill/restart of the valarch during a online backup  
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  
  16/06/2011: Herve
  - IS-561: get the list of systemAlarm pattern

  15/03/2011: Herve
  - IS-484: Archive systemIntegrity: periodic query of the stats
      request statistics, wait 5 sec. 
      in case of error: request again statistics 
      the wait time, and retry number is configurable 
      the settings are not overwritten when re-installing again the component 

  25/11/2010: Herve
  - IS-458: delete all systemAlarm valarch and alarm if RDB is used.
  
  05/01/2010: Herve
    - add diagnostic
    - add: automatic restart if no update statistics after n tries
      n=2 by default
      n configurable
      
  21/09/2009: Herve
    - in file switch check, add requesto the update of stats and check the timestmaps. The request for update is not
    faster than one minute
    
	07/12/2007: Herve
		- add check if manager state not in running mode
		
	20/12/2006: Herve
		- optimization in callback

	31/03/2006: Herve
		- add archive config on SystemAlarm, same archive class as the one for MessageText if it exists
		
	25/02/2005: Herve
		- add check on the archive file switch periodicity
		- add check on the alarm file size.
		
	02/07/2004: Herve
		version 3.0
		adding the error.errBit in the callback

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the archive component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: archive_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_ARCHIVE = "archive";
const string UN_SYSTEM_INTEGRITY_ARCHIVE_callback = "unSystemIntegrity_archive_CheckingCallback";
const string archive_pattern = "archive_";
const int c_unSystemIntegrity_defaultArchiveFileSwitchDelay = 600;
const int c_unSystemIntegrity_defaultAlarmFileSizeDelay = 30;
const string UN_SYSTEMINTEGRITY_ALARM = "Alarm";
const string UN_SYSTEMINTEGRITY_ALARM_DESC = "Alarm archive";
const string UN_SYSTEM_INTEGRITY_ARCHIVE_check = "unSystemIntegrity_archive_fileSwitchChecking";
const string UN_SYSTEM_INTEGRITY_ALARM_check = "unSystemIntegrity_alarm_fileSizeChecking";
const string UN_SYSTEMINTEGRITY_ALARM_OVF = "db/pvss/aloverflow";
const string UN_SYSTEMINTEGRITY_ALARM_ZERO = "db/pvss/al0000000000";
const string UN_SYSTEMINTEGRITY_ALARM_ARCHIVE_CONFIG_DP = "_AlertArchivControl";
const int UN_SYSTEMINTEGRITY_CURRENT_ALARM_FOLDER_SIZE = 100000000; // 100Mb
const int UN_SYSTEMINTEGRITY_ALOVF_FOLDER_SIZE = 50000000; // 50Mb
const int UN_SYSTEMINTEGRITY_MISSINGUPDATE = 2; // max 2 missing update per default
const int UN_SYSTEMINTEGRITY_UPDATESTATSMAXRETRY = 2;
const int UN_SYSTEMINTEGRITY_UPDATESTATSMAXWAITTIME = 5;
const int UN_SYSTEMINTEGRITY_AR_X_SIZE = 90000000; // 90Mb
const int UN_SYSTEMINTEGRITY_ALARM_FILESWITCH_TOLERANCE = 1200; // 20 minutes alarm file switch time tolerance
// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_archiveList; // list of the archive manager dp checked
global dyn_int g_unSystemIntegrity_archiveFileSwitch_ThreadId; // list of the thread Id checking the archive
global mapping g_mSystemIntegrity_archiveAlarmState; // state of the archive manager
global mapping g_mSystemIntegrity_archiveFileSwitchAlarmState; // state of the archive manager
global int g_unSystemIntegrity_archiveFileSwitchCheckingDelay;
global int g_unSystemIntegrity_alarmCheckingDelay;
global int g_unSystemIntegrity_alarm_ThreadId;
global mapping g_mSystemIntegrity_archiveMissingUpdate; // number of continuous failing update statistics
global int g_mSystemIntegrity_MaxArchiveMissingUpdate; //max number of missing update before taking action
global int g_mSystemIntegrity_updateStatsMaxRetry; //max retry stats
global int g_mSystemIntegrity_updateStatsMaxWaitTime; //max wait time
global int g_mSystemIntegrity_alarmFileSwitchTolerance; //alarm file switch time tolerance
//@{

//------------------------------------------------------------------------------------------------------------------------
// archive_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string archive_systemIntegrityInfo()
{
  return makeDynString(archive_pattern+"_ValueArchive", archive_pattern+"Alarm");
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_Initialize
/**
Purpose:
Get the list of archive check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled archive that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_archive_Initialize(dyn_string &dsResult)
{
	dyn_string dsList, exceptionInfo, dsAll;
	int len, i;
	string dpToCheck;
	bool enabled;
  bool bRDB;
	
	dpGet(UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
	unSystemIntegrity_archive_DataCallback("", dsList);

// get the list of archive manager dp to checked
	dsList = dpNames(c_unSystemAlarm_dpPattern+archive_pattern+"*", c_unSystemAlarm_dpType);
//DebugN("archive:", dsList);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+archive_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
   dynAppend(dsAll, dpToCheck);
	}
  dpGet("_DataManager.UseRDBArchive", bRDB);
  if((bRDB) && (dynlen(dsAll)>0)){
    DebugTN("--------------------SystemIntegrity archive INFO: RDB used deleting existing valarch and alarm system integrity");
    len = dynlen(dsAll);
			for(i=1; i<=len; i++) {
      DebugTN("------------------------------SystemIntegrity archive INFO: deleting "+dsAll[i]);
				unSystemIntegrity_archive_checking(dsAll[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+archive_pattern+dsAll[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+archive_pattern+dsAll[i]))
					dpDelete(c_unSystemAlarm_dpPattern+archive_pattern+dsAll[i]);
			}
    DebugTN("--------------------SystemIntegrity archive INFO: RDB used, valarch and alarm system integrity deleted");
    
    dsResult = makeDynString();
  }
//DebugN(dsResult, dsAll, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_archive_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo, dsTemp;
	int i, len =dynlen(parameters);
  dyn_string dsResult;
  string sResult;
  string sMessage;
	
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_archive_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_archive_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+archive_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+archive_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+archive_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+archive_pattern+parameters[i]))
					unSystemIntegrity_archive_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+archive_pattern+parameters[i]))
					unSystemIntegrity_archive_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dsTemp = g_unSystemIntegrity_archiveList;
			if(g_unSystemIntegrity_alarm_ThreadId > 0)
				dynAppend(dsTemp, c_unSystemAlarm_dpPattern+archive_pattern+UN_SYSTEMINTEGRITY_ALARM);
			dpSet(UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", dsTemp);
			break;
    case 10: // extended diagnostic
      dynAppend(dsResult, getCurrentTime());
	  
      dynAppend(dsResult, "g_unSystemIntegrity_archiveList");
      len = dynlen(g_unSystemIntegrity_archiveList);
      for(i=1;i<=len;i++) {
        sResult = g_unSystemIntegrity_archiveList[i];
        dynAppend(dsResult, sResult);
      }
	  
      dynAppend(dsResult, "g_unSystemIntegrity_archiveFileSwitch_ThreadId");
      len = dynlen(g_unSystemIntegrity_archiveFileSwitch_ThreadId);
      for(i=1;i<=len;i++) {
        sResult = g_unSystemIntegrity_archiveFileSwitch_ThreadId[i];
        dynAppend(dsResult, sResult);
      }
	  
      dynAppend(dsResult, "g_mSystemIntegrity_archiveAlarmState");
      len = mappinglen(g_mSystemIntegrity_archiveAlarmState);
      for(i=1;i<=len;i++) {
        sResult = mappingGetKey(g_mSystemIntegrity_archiveAlarmState, i)+": "+mappingGetValue(g_mSystemIntegrity_archiveAlarmState, i);
        dynAppend(dsResult, sResult);
      }
	  
      dynAppend(dsResult, "g_mSystemIntegrity_archiveFileSwitchAlarmState");
      len = mappinglen(g_mSystemIntegrity_archiveFileSwitchAlarmState);
      for(i=1;i<=len;i++) {
        sResult = mappingGetKey(g_mSystemIntegrity_archiveFileSwitchAlarmState, i)+": "+mappingGetValue(g_mSystemIntegrity_archiveFileSwitchAlarmState, i);
        dynAppend(dsResult, sResult);
      }
	  
      dynAppend(dsResult, "g_mSystemIntegrity_archiveMissingUpdate");
      len = mappinglen(g_mSystemIntegrity_archiveMissingUpdate);
      for(i=1;i<=len;i++) {
        sResult = mappingGetKey(g_mSystemIntegrity_archiveMissingUpdate, i)+": "+mappingGetValue(g_mSystemIntegrity_archiveMissingUpdate, i);
        dynAppend(dsResult, sResult);
      }
	  
      dynAppend(dsResult, "g_unSystemIntegrity_archiveFileSwitchCheckingDelay");
	  dynAppend(dsResult, g_unSystemIntegrity_archiveFileSwitchCheckingDelay);
      dynAppend(dsResult, "g_unSystemIntegrity_alarmCheckingDelay");
	  dynAppend(dsResult, g_unSystemIntegrity_alarmCheckingDelay);
      dynAppend(dsResult, "g_unSystemIntegrity_alarm_ThreadId");
	  dynAppend(dsResult, g_unSystemIntegrity_alarm_ThreadId);
      dynAppend(dsResult, "g_mSystemIntegrity_MaxArchiveMissingUpdate");
	  dynAppend(dsResult, g_mSystemIntegrity_MaxArchiveMissingUpdate);
      dynAppend(dsResult, "g_mSystemIntegrity_updateStatsMaxRetry");
	  dynAppend(dsResult, g_mSystemIntegrity_updateStatsMaxRetry);
      dynAppend(dsResult, "g_mSystemIntegrity_updateStatsMaxWaitTime");
	  dynAppend(dsResult, g_mSystemIntegrity_updateStatsMaxWaitTime);
	  
      dpSet(UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
          UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", dsResult);
      break;
    case UN_SYSTEMINTEGRITY_DEBUG:
      if(dynlen(parameters) >= 2)
      {
        g_b32DebugLevel = (bit32)parameters[1];
        g_sDebugFilter = parameters[2];
      }
      else
      {
        g_b32DebugLevel = (bit32)0;
        g_sDebugFilter = "";
      }
	  dpSet(UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
				UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(g_b32DebugLevel, g_sDebugFilter));
      break;
    case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
      dpSet(UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_ARCHIVE+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
		default:
			break;
	}

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity", "unSystemIntegrity_archive_HandleCommand", command, parameters);
	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_archive_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error in case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_archive_HandleCommand", exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_archiveList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_DataCallback
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param dsConfigData: dyn_string, input, the config data

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_archive_DataCallback(string sDpe1, dyn_string dsConfigData)
{
	int archiveFileSwitchCheckingDelay, alarmCheckingDelay;

	if(dynlen(dsConfigData) > 1) {
		archiveFileSwitchCheckingDelay = (int)dsConfigData[1];
		alarmCheckingDelay = (int)dsConfigData[2];
	}
//DebugN(dsConfigData);
	g_unSystemIntegrity_archiveFileSwitchCheckingDelay = archiveFileSwitchCheckingDelay;
	if(g_unSystemIntegrity_archiveFileSwitchCheckingDelay <= 0)
		g_unSystemIntegrity_archiveFileSwitchCheckingDelay = c_unSystemIntegrity_defaultArchiveFileSwitchDelay;

	g_unSystemIntegrity_alarmCheckingDelay = alarmCheckingDelay;
	if(g_unSystemIntegrity_alarmCheckingDelay <= 0)
		g_unSystemIntegrity_alarmCheckingDelay = c_unSystemIntegrity_defaultAlarmFileSizeDelay;
        while(dynlen(dsConfigData) < 6)
          dynAppend(dsConfigData, "0");
        if(dynlen(dsConfigData) >=3)
          g_mSystemIntegrity_MaxArchiveMissingUpdate = (int)dsConfigData[3];
        if(g_mSystemIntegrity_MaxArchiveMissingUpdate <= 0)
          g_mSystemIntegrity_MaxArchiveMissingUpdate = UN_SYSTEMINTEGRITY_MISSINGUPDATE;
        if(dynlen(dsConfigData) >=4)
          g_mSystemIntegrity_updateStatsMaxRetry = (int)dsConfigData[4];
        if(g_mSystemIntegrity_updateStatsMaxRetry <=0)
          g_mSystemIntegrity_updateStatsMaxRetry = UN_SYSTEMINTEGRITY_UPDATESTATSMAXRETRY;
        if(dynlen(dsConfigData) >=5)
          g_mSystemIntegrity_updateStatsMaxWaitTime = (int)dsConfigData[5];
        if(dynlen(dsConfigData) >=6)
          g_mSystemIntegrity_alarmFileSwitchTolerance = (int)dsConfigData[6];
        if( g_mSystemIntegrity_alarmFileSwitchTolerance <=0)
          g_mSystemIntegrity_alarmFileSwitchTolerance = UN_SYSTEMINTEGRITY_ALARM_FILESWITCH_TOLERANCE;

        if(g_mSystemIntegrity_updateStatsMaxWaitTime <= 0)
          g_mSystemIntegrity_updateStatsMaxWaitTime = UN_SYSTEMINTEGRITY_UPDATESTATSMAXWAITTIME;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity", "unSystemIntegrity_archive_DataCallback", dsConfigData, 
                               g_unSystemIntegrity_archiveFileSwitchCheckingDelay, g_unSystemIntegrity_alarmCheckingDelay,
                               g_mSystemIntegrity_updateStatsMaxRetry, g_mSystemIntegrity_updateStatsMaxWaitTime, g_mSystemIntegrity_alarmFileSwitchTolerance);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_checking
/**
Purpose:
This function register/de-register the callback funtion of archive manager. This function can also create the _unSystemAlarm_archive dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_archive_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string dpToCheck, dp, arName, description;
	int res, arNumber;
	
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;

	dpToCheck = dp;
	dp = c_unSystemAlarm_dpPattern+archive_pattern+dp;
//DebugN("unSystemIntegrity_archive_checking", dp, bCreate, bRegister, dpToCheck);
	if(dpToCheck != UN_SYSTEMINTEGRITY_ALARM) {
		if(bCreate) {
	// create the sDp and its alarm config if it is not existing
			if(!dpExists(dp)) {
				dpGet(dpToCheck+".arNr", arNumber, dpToCheck+".general.arName", arName);
				description = getCatStr("unSystemIntegrity", "ARCHIVE_DESCRIPTION")+arName +" "+arNumber;
	
				unSystemIntegrity_createSystemAlarm(dpToCheck, archive_pattern, description, exceptionInfo);
				unSystemAlarm_archive_setArchive(dp+".alarm");
			}
		}
	
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + dpToCheck, "unSystemIntegrity_archive_checking ARCHIVE", arName, arNumber);
		if(dynlen(exceptionInfo)<=0)
			unSystemAlarm_archive_register(bRegister, UN_SYSTEM_INTEGRITY_ARCHIVE_callback, makeDynString(dpToCheck+".state", 
																							dpToCheck+".error.errNum", dpToCheck+".error.errBit"), 
						g_unSystemIntegrity_archiveList, dp, dpToCheck, exceptionInfo);
	}
	else { // alarm
		if(bCreate) {
	// create the sDp and its alarm config if it is not existing
			if(!dpExists(dp)) {
				description = getCatStr("unSystemIntegrity", "ARCHIVE_DESCRIPTION")+"Alarm file size";
	
				unSystemIntegrity_createSystemAlarm(dpToCheck, archive_pattern, description, exceptionInfo);
				unSystemAlarm_archive_setArchive(dp+".alarm");
			}
		}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ALARM", "unSystemIntegrity_archive_checking ALARM");
		if(dynlen(exceptionInfo)<=0)
			unSystemAlarm_alarm_register(bRegister, dp, exceptionInfo);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemAlarm_archive_setArchive
/**
Purpose:
set an archive config.

	@param sDpe: string, input, dpe

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemAlarm_archive_setArchive(string sDpe)
{
	dyn_string exceptionInfo;
	bool configExists, isActive;
	int archiveType, smoothProcedure; 
	float deadband, timeInterval;
	string sMessage;
	
	string archiveClass;
	if(isFunctionDefined("unMessageText_getDpMessage")){
		sMessage = unMessageText_getDpMessage(exceptionInfo);

		fwArchive_get(sMessage, configExists, archiveClass, archiveType, smoothProcedure, 
										deadband, timeInterval, isActive, exceptionInfo);
/*DebugN(configExists, archiveClass, archiveType, smoothProcedure, 
										deadband, timeInterval, isActive, exceptionInfo);
*/
		fwArchive_set(sDpe, archiveClass, DPATTR_ARCH_PROC_SIMPLESM, DPATTR_COMPARE_OLD_NEW, 0.0, 0, exceptionInfo);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemAlarm_archive_register
/**
Purpose:
Register function, does the dpConnect and dpDisconnect, add/remove the dp in/from the global list.

	@param bRegister: bool, input, true: add, false: remove from sGlobalVar
	@param sCallBack: string, input, callback function
	@param dsCallBackParam: dyn_string, input, callback parameters
	@param sGlobalVar: dyn_string, input-output, the global variable used to store the list of dp to check
	@param sDp: string, input, the dp to add/remove in/from the sGlobalVar
	@param sArchiveDp: string, input, the archive dp
	@param exceptionInfo: dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemAlarm_archive_register(bool bRegister, string sCallBack, dyn_string dsCallBackParam, 
					dyn_string &sGlobalVar, string sDp, string sArchiveDp, dyn_string &exceptionInfo)
{
	int res, pos, thId, iCurrentTime=(int)getCurrentTime();
	dyn_int diFileSwitch;
	
	if(bRegister) {
// connect to the callback function if not already done
		pos = _unSystemIntegrity_isInList(sGlobalVar, sDp);
		if(pos <= 0) {
		// add in list first because if the dpConnect succeeds the callback may be called before the list is updated
		// and in the callback the list is used
			pos = _unSystemIntegrity_setList(sGlobalVar, sDp, true);

//DebugN("gl",sGlobalVar);
			g_mSystemIntegrity_archiveAlarmState[sArchiveDp] = -1;
	// get the file switch start time
			dpGet(sArchiveDp+".files.startTime", diFileSwitch); 
			if(dynlen(diFileSwitch) > 0) { // at least one file
	// if the last file switch was done in less that the checking time ==> file switch state = alarm
				if((iCurrentTime - diFileSwitch[1]) <= g_unSystemIntegrity_archiveFileSwitchCheckingDelay)
					g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] = c_unSystemIntegrity_alarm_value_level1;
				else
					g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] = c_unSystemIntegrity_no_alarm_value;
			}
			else // correct file switch
				g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] = c_unSystemIntegrity_no_alarm_value;

			res = dpConnect(sCallBack, dsCallBackParam[1], dsCallBackParam[2],dsCallBackParam[3]);
			if(res < 0) {
				fwException_raise(exceptionInfo, "ERROR", 
							"unSystemAlarm_archive_register(): "+getCatStr("unSystemIntegrity", "CANNOT_DP_CONNECT") + dsCallBackParam[1],"");
			
			// remove from list in case of error
				res = _unSystemIntegrity_setList(sGlobalVar, sDp, false);
			}
			else {
// start the thread for checking the counter
				thId = startThread(UN_SYSTEM_INTEGRITY_ARCHIVE_check, sDp, sArchiveDp);
				g_unSystemIntegrity_archiveFileSwitch_ThreadId[pos] = thId;
                                g_mSystemIntegrity_archiveMissingUpdate[sArchiveDp] = 0;
//DebugTN(sDp, thId, pos);
		// add in list
				pos = _unSystemIntegrity_setList(sGlobalVar, sDp, true);
//DebugTN("second", sDp, thId, pos);
			// set the enable to true and activate the alarm if it is not activated.
				dpSet(sDp+".enabled", true);
				unAlarmConfig_mask(sDp+".alarm", false, exceptionInfo);
			}
		}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveDp, "unSystemAlarm_archive_register register", sDp, sArchiveDp, sGlobalVar, 
                               g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp], diFileSwitch, iCurrentTime, g_unSystemIntegrity_archiveFileSwitch_ThreadId);
	}
	else {
// disconnect the callback function
		pos = _unSystemIntegrity_isInList(sGlobalVar, sDp);
		if(pos > 0) {
			res = dpDisconnect(sCallBack, dsCallBackParam[1], dsCallBackParam[2],dsCallBackParam[3]);
			if(res < 0) {
				fwException_raise(exceptionInfo, "ERROR", 
								"unSystemAlarm_archive_register():"+getCatStr("unSystemIntegrity", "CANNOT_DP_DISCONNECT") + sDp,"");
			}
			else {
// kill the thread
//DebugN("stop thre", sDp, dsCallBackParam[1], g_unSystemIntegrity_archiveFileSwitch_ThreadId[pos], pos);
				res = stopThread(g_unSystemIntegrity_archiveFileSwitch_ThreadId[pos]);
      dynRemove(g_unSystemIntegrity_archiveFileSwitch_ThreadId, pos);
				// remove from list
				pos = _unSystemIntegrity_setList(sGlobalVar, sDp, false);
//DebugN("stop thre, second", sDp, dsCallBackParam[1], g_unSystemIntegrity_archiveFileSwitch_ThreadId[pos], pos);
				// set the enable to false and de-activate the alarm and acknowledge it if necessary
				dpSet(sDp+".enabled", false, sDp+".alarm", c_unSystemIntegrity_no_alarm_value);
				unAlarmConfig_mask(sDp+".alarm", true, exceptionInfo);
			}
		}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveDp, "unSystemAlarm_archive_register deregister", sDp, sGlobalVar, 
                               g_unSystemIntegrity_archiveFileSwitch_ThreadId);
	}
//DebugN("end",sGlobalVar);

}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_fileSwitchChecking
/**
Purpose:
Check if the file switch of the archive are not too fast

	@param sDp: string, input, the dp to add/remove in/from the sGlobalVar
	@param sArchiveDp: string, input, the archive dp

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_archive_fileSwitchChecking(string sDp, string sArchiveDp)
{
	int waitingTime, iArNumber;
	int alarmValue, oldAlarmValue = -1;
	dyn_int diOldFileList, diNewFileList;
	int iManagerState;
        bool bAlarmUpdate=false;
  bool bOnlineBackup;
	
// get the list of files
	dpGet(sArchiveDp+".files.startTime", diOldFileList, sArchiveDp+".arNr", iArNumber); 
//DebugTN("unSystemIntegrity_archive_fileSwitchChecking", sDp, sArchiveDp, iArNumber);
	while(true) {
	// if 0 set it to c_unSystemIntegrity_defaultArchiveFileSwitchDelay
		waitingTime = g_unSystemIntegrity_archiveFileSwitchCheckingDelay;
	// wait
		delay(waitingTime);
    bOnlineBackup=unGenericDpFunctions_isOnlineBackupRunning(getSystemName());
    bAlarmUpdate = unSystemIntegrity_archive_checkUpdate(sArchiveDp);
    if(!bAlarmUpdate) {
      bAlarmUpdate = unSystemIntegrity_archive_IndexFileSizeTooBig(iArNumber);
      if(!bAlarmUpdate) {
	// get the list of files
		  dpGet(sArchiveDp+".files.startTime", diNewFileList);
		  if(dynlen(diOldFileList) <=0)
			diOldFileList[1] = 0;
	
		  if(dynlen(diNewFileList) > 1) { // more than one file
			if(diNewFileList[1] != diOldFileList[1]) { // new file added
				if(diNewFileList[2] == diOldFileList[1]) { // new file, previous one in second position, check the time diff
					if((diNewFileList[1] - diOldFileList[1]) <= waitingTime) { // file switch too fast time diff <= waiting time ==> raise an alarm
						alarmValue = c_unSystemIntegrity_alarm_value_level1;
	//DebugTN("**** file switch too fast", diNewFileList[1], diOldFileList[1], diNewFileList[1] - diOldFileList[1]);
					}
					else { // normal file switch
						alarmValue = c_unSystemIntegrity_no_alarm_value;
	//DebugTN("**** normal file switch", diNewFileList[1], diOldFileList[1], diNewFileList[1] - diOldFileList[1]);
					}
				}
				else { // new file, previous one not in second position, in third or further position ==> raise an alarm
					alarmValue = c_unSystemIntegrity_alarm_value_level1;
	//DebugTN("**** file switch not in second place", diNewFileList, diOldFileList);
				}
			}
			else { // same position as before
				alarmValue = c_unSystemIntegrity_no_alarm_value;
	//DebugTN("**** no change");
			}
		  }
		  else { // just one file or less
			alarmValue = c_unSystemIntegrity_no_alarm_value;
	//DebugTN("**** no file");
		  }
		  g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] = alarmValue;
		  unSystemIntegrity_getState("WCCOAvalarch", "-num "+iArNumber, iManagerState);
		  if(iManagerState != SYSTEMINTEGRITY_MANAGER_RUNNING)
			g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] = 2*c_unSystemIntegrity_alarm_value_level1;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveDp, "unSystemIntegrity_archive_fileSwitchChecking checking fileSwitch", sDp, sArchiveDp, alarmValue, diNewFileList, diOldFileList);
      }
      else
        g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] = 4*c_unSystemIntegrity_alarm_value_level1;
    }
    else
      g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] = 3*c_unSystemIntegrity_alarm_value_level1;
	// get the alarm value to set
		alarmValue = unSystemIntegrity_archive_getAlarmState(sArchiveDp);
		if(alarmValue != oldAlarmValue) {
			dpSet(sDp+".alarm", alarmValue);
		}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveDp, "unSystemIntegrity_archive_fileSwitchChecking", sDp, sArchiveDp, oldAlarmValue, alarmValue);
//DebugTN("unSystemIntegrity_archive_fileSwitchChecking", sDp, sArchiveDp, alarmValue, iManagerState);	
		oldAlarmValue = alarmValue;
		diOldFileList = diNewFileList;
                unSystemIntegrity_archive_handleMissingUpdate(sArchiveDp, bAlarmUpdate, iArNumber, bOnlineBackup);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_IndexFileSizeTooBig
/**
Purpose:
Check the if the AR_x files has a size lower than 90Mb.

@param sArchiveDp: string, input, the archive dp
@param return value: bool, output, true: size > 90Mb, false: OK


Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
bool unSystemIntegrity_archive_IndexFileSizeTooBig(int iArNumber)
{
  bool bReturn = false;
  string sArchiveIndexFileName, sArchiveNumber = iArNumber;
  int iSize;
  
  sArchiveIndexFileName = "db/pvss/VA_"+strexpand("\\fill{0}", 4 - strlen(sArchiveNumber)) + sArchiveNumber+"/AR_"+sArchiveNumber;
  iSize = getFileSize(PROJ_PATH+sArchiveIndexFileName);
  if(iSize > UN_SYSTEMINTEGRITY_AR_X_SIZE)
    bReturn = true;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveNumber, "unSystemIntegrity_archive_IndexFileSizeTooBig", sArchiveNumber, sArchiveIndexFileName, iSize, bReturn);
  return bReturn;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_checkUpdate
/**
Purpose:
Check the if the statistics of the archive are correctly updated.

@param sArchiveDp: string, input, the archive dp
@param return value: bool, output, true: stats not updated, false: OK


Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
bool unSystemIntegrity_archive_checkUpdate(string sArchiveDp)
{
  int iAlarm = 0;
  bool bAlarm=true;
  dyn_string dsFiles;
  int i, iTime, iRetry, iMaxRetry=g_mSystemIntegrity_updateStatsMaxRetry, iMaxWaitTime=g_mSystemIntegrity_updateStatsMaxWaitTime;
  
  for(iRetry=1;iRetry<=iMaxRetry;iRetry++)
  {
    dpGet(sArchiveDp+".files.fileName",dsFiles);
    dpSet(sArchiveDp+".statistics.index", dynlen(dsFiles));
    for(i=1;i<=iMaxWaitTime;i++)
    {
      delay(1);
      dpGet(sArchiveDp+".statistics.dpElements:_online.._stime", iTime);
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE stats " + sArchiveDp, "unSystemIntegrity_archive_checkUpdate stats", 
                               sArchiveDp, iTime, i, iRetry);
  //!!!DebugTN(sArchiveDp, iTime, (int)getCurrentTime());
      if((iTime+iMaxWaitTime) > getCurrentTime()) {
        i=iMaxWaitTime+1;
        iRetry = iMaxRetry+1;
        bAlarm = false;
      }
    }
  }
//!!!DebugTN(sArchiveDp, bAlarm);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveDp, "unSystemIntegrity_archive_checkUpdate result", 
                               sArchiveDp, bAlarm, iMaxWaitTime, iMaxRetry);
  return bAlarm;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_handleMissingUpdate
/**
Purpose:
Check the if the statistics of the archive are correctly updated.

@param sArchiveDp: string, input, the archive dp
@param bAlarm: bool, input, true: stats not updated, false: OK
@param iArNumber: int, input, archive number
@param bOnlineBackup: bool, input, online backup runnng state, true=online backup running/false=online backup not running

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.0 
  . operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
  . distributed system: yes.
*/
unSystemIntegrity_archive_handleMissingUpdate(string sArchiveDp, bool bAlarm, int iArNumber, bool bOnlineBackup)
{
  int iManagerState;
  int iIndex;
  dyn_string exInfo;
  
  if(bAlarm)
    g_mSystemIntegrity_archiveMissingUpdate[sArchiveDp] +=1;
  else
    g_mSystemIntegrity_archiveMissingUpdate[sArchiveDp] = 0;
  
  if(!bOnlineBackup)
  {  
    if(g_mSystemIntegrity_archiveMissingUpdate[sArchiveDp] >= g_mSystemIntegrity_MaxArchiveMissingUpdate) { // maximum missing update reached
      unSystemIntegrity_getState("WCCOAvalarch", "-num "+iArNumber, iManagerState);
      switch(iManagerState) {
        case SYSTEMINTEGRITY_MANAGER_RUNNING:
        case SYSTEMINTEGRITY_MANAGER_BLOCKED:
        case SYSTEMINTEGRITY_MANAGER_INIT:
          // kill the archive manager
          unSystemIntergity_manageManager("WCCOAvalarch", "-num "+iArNumber, SYSTEMINTEGRITY_KILL_COMMAND, iIndex, exInfo);
          break;
        default:
          // restart the archive manager
          unSystemIntergity_manageManager("WCCOAvalarch", "-num "+iArNumber, SYSTEMINTEGRITY_START_COMMAND, iIndex, exInfo);
          g_mSystemIntegrity_archiveMissingUpdate[sArchiveDp] = 0;
          break;
      }
    }
  }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveDp, "unSystemIntegrity_archive_handleMissingUpdate", sArchiveDp, bOnlineBackup, iArNumber, bAlarm, iManagerState, g_mSystemIntegrity_archiveMissingUpdate);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_CheckingCallback
/**
Purpose:
Callback function. This function is called when the system alarm is enabled for this device.

@param sDp1: string, input, data point name
@param state: int, input, state of the archive, 1: online, 0: offline
@param sDp2: string, input, data point name
@param error: int, input, error of the archive, 0: no error
@param sDp3: string, input, data point name
@param errBit: bool, input, true: archive overflow, false: no error


Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_archive_CheckingCallback(string sDp1, int state, string sDp2, int error, string sDp3, bool errBit)
{
	string sDp;
	int value, iIndex, iArNumber, oldValue;
	dyn_string exInfo;
	
//	DebugN(sDp1, state, error);
// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);
	oldValue = unSystemIntegrity_archive_getAlarmState(sDp);
	value = c_unSystemIntegrity_no_alarm_value;
// set the c_unSystemAlarm_dpPattern_archive_sdp1.alarm 
	if(state == 1) { // archive online
		if((error != 0) || (errBit)) // any error in the archive
			value = c_unSystemIntegrity_alarm_value_level1;
	}
	else { // archive offline
		value = c_unSystemIntegrity_alarm_value_level1;
		dpGet(sDp+".arNr", iArNumber);
//if the archive is offline stop it.
		unSystemIntergity_manageManager("WCCOAvalarch", "-num "+iArNumber, SYSTEMINTEGRITY_KILL_COMMAND, iIndex, exInfo);
	}
	g_mSystemIntegrity_archiveAlarmState[sDp] = value;
// get the alarm value to set
	value = unSystemIntegrity_archive_getAlarmState(sDp);
//DebugN(oldValue, value);
	if(oldValue != value)
		dpSet( c_unSystemAlarm_dpPattern+archive_pattern+sDp+".alarm", value);
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sDp, "unSystemIntegrity_archive_CheckingCallback", state,
                               error, errBit, g_mSystemIntegrity_archiveAlarmState[sDp], value);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_archive_getAlarmState
/**
Purpose:
Gives back the alarm value based on the alarm for the file switch check and state of the archive

@param sArchiveDp: string, input, the archive data point name
@param return value the alarm state

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
int unSystemIntegrity_archive_getAlarmState(string sArchiveDp) 
{
	int iRes=c_unSystemIntegrity_alarm_value_level1;
	
	if(g_mSystemIntegrity_archiveAlarmState[sArchiveDp] == c_unSystemIntegrity_no_alarm_value)  { // correct state of the archive
		if(g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp] == c_unSystemIntegrity_no_alarm_value) { // correct behavior of the file switch
			iRes = c_unSystemIntegrity_no_alarm_value;
		}
		else // file switch alarm ==> raise alarm
			iRes = 2*g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp];
//			iRes = 2*c_unSystemIntegrity_alarm_value_level1;
	}
// all the other case ==> raise alarm
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ARCHIVE " + sArchiveDp, "unSystemIntegrity_archive_getAlarmState", sArchiveDp, iRes, g_mSystemIntegrity_archiveAlarmState[sArchiveDp], g_mSystemIntegrity_archiveFileSwitchAlarmState[sArchiveDp]);
	return iRes;
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemAlarm_alarm_register
/**
Purpose:
Register function, does the check of the alarm file size.

	@param bRegister: bool, input, true: add, false: remove from sGlobalVar
	@param sDp: string, input, the alarm dp
	@param exceptionInfo: dyn_string, output, the error is returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemAlarm_alarm_register(bool bRegister, string sDp, dyn_string &exceptionInfo)
{
	int res, thId;
	
	if(bRegister) {
		if(g_unSystemIntegrity_alarm_ThreadId <= 0) {
// start the thread for checking the counter
			thId = startThread(UN_SYSTEM_INTEGRITY_ALARM_check, sDp);
			g_unSystemIntegrity_alarm_ThreadId = thId;
//DebugTN("ALARM", thId);
			dpSet(sDp+".enabled", true);
			unAlarmConfig_mask(sDp+".alarm", false, exceptionInfo);
		}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ALARM", "unSystemAlarm_alarm_register register", sDp, g_unSystemIntegrity_alarm_ThreadId);
	}
	else {
// kill the thread
//DebugTN("register stopThread, alarm", sDp, g_unSystemIntegrity_alarm_ThreadId);
    if(g_unSystemIntegrity_alarm_ThreadId > 0) {
		  res = stopThread(g_unSystemIntegrity_alarm_ThreadId);
    }
		g_unSystemIntegrity_alarm_ThreadId = -1;
    if(dpExists(sDp+".enabled"))
    {
		// set the enable to false and de-activate the alarm and acknowledge it if necessary
		  dpSet(sDp+".enabled", false, sDp+".alarm", c_unSystemIntegrity_no_alarm_value);
		  unAlarmConfig_mask(sDp+".alarm", true, exceptionInfo);
    }
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ALARM", "unSystemAlarm_alarm_register deregister", sDp, g_unSystemIntegrity_alarm_ThreadId);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_alarm_fileSizeChecking
/**
Purpose:
Check the alarm file size

	@param sDp: string, input, the unSystem alarm dp

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_alarm_fileSizeChecking(string sDp)
{
	int waitingTime;
	int alarmValue, oldAlarmValue = -1;
	string sAlarmFolder;
	dyn_int diOldFileList, diNewFileList;
	int lenNew, lenOld;
	
// get the list of files
	dpGet("_AlertDataSet.List.StartTimes", diOldFileList); 
	
//DebugTN("unSystemIntegrity_alarm_fileSizeChecking", sDp);
	while(true) {
	// if 0 set it to c_unSystemIntegrity_defaultArchiveFileSwitchDelay
		waitingTime = g_unSystemIntegrity_alarmCheckingDelay;
	// wait
		delay(waitingTime);

// check file switch
	// get the list of files
		dpGet("_AlertDataSet.List.StartTimes", diNewFileList);
		lenNew = dynlen(diNewFileList);
		lenOld = dynlen(diOldFileList);
		if(lenOld <=0)
			diOldFileList[1] = 0;
		lenOld = dynlen(diOldFileList);
	
		if(lenNew > 1) { // more than one file
			if(diNewFileList[lenNew] != diOldFileList[lenOld]) { // new file added
				if(diNewFileList[lenNew - 1] == diOldFileList[lenOld]) { // new file, previous one in second position, check the time diff
					if((diNewFileList[lenNew] - diOldFileList[lenOld]) <= waitingTime) { // file switch too fast time diff <= waiting time ==> raise an alarm
						alarmValue = c_unSystemIntegrity_alarm_value_level1;
//	DebugTN("**** alarm file switch too fast", diNewFileList[lenNew], diOldFileList[lenOld], diNewFileList[lenNew] - diOldFileList[lenOld]);
					}
					else { // normal file switch
						alarmValue = c_unSystemIntegrity_no_alarm_value;
//	DebugTN("**** alarm normal file switch", diNewFileList[lenNew], diOldFileList[lenOld], diNewFileList[lenNew] - diOldFileList[lenOld]);
					}
				}
				else { // new file, previous one not in second position, in third or further position ==> raise an alarm
					alarmValue = c_unSystemIntegrity_alarm_value_level1;
//	DebugTN("**** alarm file switch not in second place", diNewFileList, diOldFileList);
				}
			}
			else { // same position as before
				alarmValue = c_unSystemIntegrity_no_alarm_value;
//	DebugTN("**** alarm no change");
			}
		}
		else { // just one file or less
			alarmValue = c_unSystemIntegrity_no_alarm_value;
//	DebugTN("**** alarm no file");
		}

   if(alarmValue == c_unSystemIntegrity_no_alarm_value) 
   {
     int fileSwitchInterval = unSystemAlarm_alarm_getFileSwitchInterval(UN_SYSTEMINTEGRITY_ALARM_ARCHIVE_CONFIG_DP);
     fileSwitchInterval = fileSwitchInterval > 0? fileSwitchInterval + g_mSystemIntegrity_alarmFileSwitchTolerance:0; 
     int lastSwitchTime = lenNew > 0? diNewFileList[lenNew]: 0;      
     int timeSinceLastFileSwitch = (int)getCurrentTime() - lastSwitchTime; 
     
     if (timeSinceLastFileSwitch > fileSwitchInterval)
     {
       alarmValue = 4 * c_unSystemIntegrity_alarm_value_level1;
     }
     else
     {
       alarmValue = c_unSystemIntegrity_no_alarm_value;
     }
   }
   
		if(alarmValue == c_unSystemIntegrity_no_alarm_value) {
			unSystemIntegrity_alarm_checkFolderSize(UN_SYSTEMINTEGRITY_ALARM_OVF, alarmValue, UN_SYSTEMINTEGRITY_ALOVF_FOLDER_SIZE);
			if(alarmValue == c_unSystemIntegrity_no_alarm_value) {
				if(lenNew >0) {
					if(diNewFileList[lenNew] == 0)
						sAlarmFolder = UN_SYSTEMINTEGRITY_ALARM_ZERO;
					else
						sAlarmFolder = "db/pvss/al"+diNewFileList[lenNew];
				}
				unSystemIntegrity_alarm_checkFolderSize(sAlarmFolder, alarmValue, UN_SYSTEMINTEGRITY_CURRENT_ALARM_FOLDER_SIZE);
			}
		}			

		if(alarmValue != oldAlarmValue) {
			dpSet(sDp+".alarm", alarmValue);
		}
		
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ALARM", "unSystemIntegrity_alarm_fileSizeChecking", sDp, alarmValue, diNewFileList, diOldFileList);
//DebugTN("unSystemIntegrity_alarm_fileSizeChecking", sDp, alarmValue);	
		oldAlarmValue = alarmValue;
		diOldFileList = diNewFileList;
	}
}

int unSystemAlarm_alarm_getFileSwitchInterval(string dp)
{
  int fileSwitchInterval = 0; //in seconds
  if (dpExists(dp) && dpTypeName(dp) == "_ArchivControl")
  {
    int count, hours, minutes, resolution;
    dpGet(dp + ".CreationCycleTime.Count", count,
          dp + ".CreationCycleTime.Hours", hours,
          dp + ".CreationCycleTime.Minutes", minutes,
          dp + ".CreationCycleTime.Resolution", resolution);
  
    switch (resolution)
    {
      //annually
      case 1:
        fileSwitchInterval = 366 * 24 * 60 * 60;
        break;
      //monthly
      case 2:
        fileSwitchInterval = 31 * 24 * 60 * 60;
        break;
      //weekly
      case 3:
        fileSwitchInterval = 7 * 24 * 60 * 60 * count; //once in every count weeks
        break;
      //daily
      case 4:
        fileSwitchInterval = 24 * 60 * 60 * count; //once in every count days
        break;
      // less than daily
      case 5:
        fileSwitchInterval = hours * 60 * 60 + minutes * 60;
        break;
      default:
        DebugN("Unknown resolution: " + resolution);
        break;
    }
  }
  return fileSwitchInterval;
}


//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_alarm_checkFolderSize
/**
Purpose:
Check the file size of a given folder

	@param sFolder: string, input, the folder name
	@param iAlarmValue: int, output, the alarm value
	@param iFolderSize: int, input, the maximum folder size

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_alarm_checkFolderSize(string sFolder, int &iAlarmValue, int iFolderSize)
{
	dyn_string dsFileName;
	int iSize, i, len;
	
	dsFileName = getFileNames(PROJ_PATH+sFolder);
	if(sFolder != "") {
		len = dynlen(dsFileName);
		for(i=1;i<=len;i++) {
			iSize += getFileSize(PROJ_PATH+sFolder+"/"+dsFileName[i]);
//DebugTN(iSize, PROJ_PATH+sFolder+"/"+dsFileName[i]);
		}
		if(iSize <= iFolderSize)
			iAlarmValue = c_unSystemIntegrity_no_alarm_value;
		else
			iAlarmValue = 2*c_unSystemIntegrity_alarm_value_level1;
	}
	else
		iAlarmValue = 3*c_unSystemIntegrity_alarm_value_level1;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "archive systemIntegrity ALARM", "unSystemIntegrity_alarm_checkFolderSize", sFolder, iSize);
}

//------------------------------------------------------------------------------------------------------------------------

//@}


