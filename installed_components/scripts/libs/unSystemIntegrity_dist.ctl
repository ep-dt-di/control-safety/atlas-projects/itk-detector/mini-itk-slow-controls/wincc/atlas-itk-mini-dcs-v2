/**@name LIBRARY: unSystemIntegrity_dist.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 12/07/2002

Modification History: 
  16/06/2011: Herve
  - IS-561: get the list of systemAlarm pattern

	06/07/2004: Herve
		- in the unSystemIntegrity_dist_checking function missing {} when creating the systemAlarm and so bug at each startup: description set to ""

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the dist component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: dist_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_DIST = "dist";
const string UN_SYSTEM_INTEGRITY_DIST_callback = "unSystemIntegrity_dist_CheckingCallback";
const string dist_pattern = "dist_";

const string c_distributedDP = "_unDistributedControl_";

// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_distList; // list of the dist manager dp checked

//@{

//------------------------------------------------------------------------------------------------------------------------
// dist_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string dist_systemIntegrityInfo()
{
  return makeDynString(dist_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_dist_Initialize
/**
Purpose:
Get the list of dist check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled dist that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_dist_Initialize(dyn_string &dsResult)
{
	dyn_string dsList;
	int len, i;
	string dpToCheck;
	bool enabled;
	

// get the list of dist DP to checked
	dsList = dpNames(c_unSystemAlarm_dpPattern+dist_pattern+"*", c_unSystemAlarm_dpType);
// remove the local system, there is no need to check it
	i = dynContains(dsList, getSystemName()+c_unSystemAlarm_dpPattern+dist_pattern+c_distributedDP+substr(getSystemName(), 0, strpos(getSystemName(), ":")));
//DebugN(dsList, i, getSystemName()+c_unSystemAlarm_dpPattern+dist_pattern+c_distributedDP+substr(getSystemName(), 0, strpos(getSystemName(), ":")));
//DebugN("dist:", dsList);
	if(i>0)
		dynRemove(dsList, i);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+dist_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
	}
//DebugN(dsResult);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_dist_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_dist_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo;
	int i, len =dynlen(parameters), res;
	string dp;
	
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				res = strpos(parameters[i], ":");
				if(res >= 0)
					dp = substr(parameters[i], res+1, strlen(parameters[i]));
				else
					dp = parameters[i];

			// do not add the local system name
				if(dp != (c_distributedDP+substr(getSystemName(), 0, strpos(getSystemName(), ":")))) {
					unSystemIntegrity_dist_checking(parameters[i], true, true, exceptionInfo);
				}
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_dist_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+dist_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+dist_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+dist_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+dist_pattern+parameters[i]))
					unSystemIntegrity_dist_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+dist_pattern+parameters[i]))
					unSystemIntegrity_dist_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(UN_SYSTEM_INTEGRITY_DIST+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_DIST+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_distList);
			break;
		default:
			break;
	}

	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", "unSystemIntegrity_dist_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error uin case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugN(getCurrentTime(), exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters, g_unSystemIntegrity_distList);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_dist_checking
/**
Purpose:
This function register/de-register the callback funtion of dist manager. This function can also create the _unSystemAlarm_dist dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_dist_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string dpToCheck, dp, description;
	int res;
	
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;

	dpToCheck = dp;
	dp = c_unSystemAlarm_dpPattern+dist_pattern+dp;

//DebugN("_unSystemIntegrity_set_dist_checking", dp, bCreate, bRegister, dpToCheck);

	if(bCreate) {
// create the sDp and its alarm config if it is not existing
		if(!dpExists(dp)) {
			description = getCatStr("unSystemIntegrity", "DIST_DESCRIPTION")+substr(dpToCheck, strlen(c_distributedDP), strlen(dpToCheck));
			unSystemIntegrity_createSystemAlarm(dpToCheck, dist_pattern, description, exceptionInfo);
		}
	}
	if(dynlen(exceptionInfo)<=0)
		_unSystemIntegrity_registerFunction(bRegister, UN_SYSTEM_INTEGRITY_DIST_callback, makeDynString(dpToCheck+".connected"), 
					g_unSystemIntegrity_distList, dp, exceptionInfo);

}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_dist_CheckingCallback
/**
Purpose:
Callback function. This function is called when the system alarm is enabled for this device.

	@param sDp1: string, input, data point name
	@param connected: bool, input, state of the connection

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

unSystemIntegrity_dist_CheckingCallback(string sDp1, bool connected)
{
	string sDp;
	int value;
//	DebugN(sDp1, connected);
// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);
// set the c_unSystemAlarm_dpPattern_dist_sdp1.alarm 
	if(connected) {
		value = c_unSystemIntegrity_no_alarm_value;
	}
	else {
		value = c_unSystemIntegrity_alarm_value_level1;
	}
	dpSet( c_unSystemAlarm_dpPattern+dist_pattern+sDp+".alarm", value);
}

//@}
