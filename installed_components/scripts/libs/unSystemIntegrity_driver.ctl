/**@name LIBRARY: unSystemIntegrity_driver.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 12/07/2002

Modification History: 
  05/09/2011: Herve
  - IS-598: system name and component version added in the MessageText log and in the diagnostic  
  
  16/06/2011: Herve
  - IS-561: get the list of systemAlarm pattern

	20/12/2006: Herve
		- optimization in callback

	20/10/2006: Herve
		- add OV DPE checking for the driver

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the driver component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: driver_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_DRIVER = "driver";
const string UN_SYSTEM_INTEGRITY_DRIVER_callback_old = "unSystemIntegrity_driver_CheckingCallback_old";
const string UN_SYSTEM_INTEGRITY_DRIVER_callback = "unSystemIntegrity_driver_CheckingCallback";
const string drv_pattern = "drv_";

// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_drvList; // list of the driver dp checked
global mapping g_mSystemIntegrityDriver;

//@{

//------------------------------------------------------------------------------------------------------------------------
// driver_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string driver_systemIntegrityInfo()
{
  return makeDynString(drv_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_driver_Initialize
/**
Purpose:
Get the list of driver check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled driver that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_driver_Initialize(dyn_string &dsResult)
{
	dyn_string dsList;
	int len, i;
	string dpToCheck;
	bool enabled;
	
// get the list of driver manager dp to checked
	dsList = dpNames(c_unSystemAlarm_dpPattern+drv_pattern+"*", c_unSystemAlarm_dpType);
//DebugN("drv:", dsList);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+drv_pattern),strlen(dpToCheck));
		dpGet(dsList[i]+".enabled", enabled);
		if(enabled)
			dynAppend(dsResult, dpToCheck);
	}
//DebugN(dsResult);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_driver_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_driver_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo;
	int i, len =dynlen(parameters);
  string sMessage;
	
	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_driver_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_driver_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+drv_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+drv_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+drv_pattern+parameters[i]);
			}
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+drv_pattern+parameters[i]))
					unSystemIntegrity_driver_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+drv_pattern+parameters[i]))
					unSystemIntegrity_driver_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(UN_SYSTEM_INTEGRITY_DRIVER+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_DRIVER+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_drvList);
			break;
    case UN_SYSTEMINTEGRITY_VERSION:
      sMessage = unGenericDpFunctions_getComponentsVersion(makeDynString("unSystemIntegrity", "unCore"));
      dpSet(UN_SYSTEM_INTEGRITY_DRIVER+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
            UN_SYSTEM_INTEGRITY_DRIVER+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(getCurrentTime(),sMessage));
      break;
		default:
			break;
	}

	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", g_SystemIntegrity_sSystemName+"unSystemIntegrity_driver_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error in case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_SystemIntegrity_sSystemName+"unSystemIntegrity_driver_HandleCommand", exceptionInfo);
		}
	}
//	DebugN(sDpe1, command, parameters);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_driver_checking
/**
Purpose:
This function register/de-register the callback funtion of driver manager. This function can also create the _unSystemAlarm_drv dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_driver_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string sDrvNum, dp, description, sDrvCommonDPE, sCallBack;
	int res, iConfigType;
	dyn_string dsParam;
	
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;
	sDrvNum = dp;
	dp = c_unSystemAlarm_dpPattern+drv_pattern+dp;
//DebugN("_unSystemIntegrity_set_drv_checking", dp, bCreate, bRegister, sDrvNum);

	if(bCreate) {
// create the sDp and its alarm config if it is not existing
		if(!dpExists(dp)) {
			description = getCatStr("unSystemIntegrity", "DRV_DESCRIPTION")+sDrvNum;
			unSystemIntegrity_createSystemAlarm(sDrvNum, drv_pattern, description, exceptionInfo);
		}
	}

	g_mSystemIntegrityDriver[sDrvNum] = -1;
	sDrvCommonDPE = "_Driver"+sDrvNum+".OV";
	if(dpExists(sDrvCommonDPE)) {
// delete the alarm config
		dpGet( sDrvCommonDPE + ":_alert_hdl.._type", iConfigType);   
  	if(iConfigType != DPCONFIG_NONE)
			fwAlertConfig_delete(sDrvCommonDPE, exceptionInfo);
		sCallBack = UN_SYSTEM_INTEGRITY_DRIVER_callback;
		dsParam = makeDynString("_Connections.Driver.ManNums", sDrvCommonDPE);
	}
	else {
		sCallBack = UN_SYSTEM_INTEGRITY_DRIVER_callback_old;
		dsParam = makeDynString("_Connections.Driver.ManNums");
	}
	if(dynlen(exceptionInfo)<=0)
		_unSystemIntegrity_registerFunction(bRegister, sCallBack, dsParam, g_unSystemIntegrity_drvList, dp, exceptionInfo);

}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_driver_CheckingCallback_old
/**
Purpose:
Callback function. This function is called when the system alarm is enabled for this device.

	@param sDp1: string, input, data point name
	@param manNum: dyn_int, input, list of driver numbers connected to the event manager

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_driver_CheckingCallback_old(string sDp1, dyn_int manNum)
{
	int value, i, len, drvNum, pos;
	dyn_string dsTemp;
	string sDrvNum;
	
	dsTemp = g_unSystemIntegrity_drvList;
//	DebugN(getCurrentTime(), "DRV", sDp1, manNum, dsTemp, test_g++);
	len = dynlen(dsTemp);
	for(i=1; i<=len; i++) {
		sDrvNum = substr(dsTemp[i], strlen(c_unSystemAlarm_dpPattern+drv_pattern),strlen(dsTemp[i]));
		sscanf(sDrvNum, "%d", drvNum);
		pos = dynContains(manNum, drvNum);
		if(pos>0) { // driver found
			value = c_unSystemIntegrity_no_alarm_value;
		}
		else { //driver not found
			value = c_unSystemIntegrity_alarm_value_level1;
		}
		if(g_mSystemIntegrityDriver[sDrvNum] != value) {
			dpSet( c_unSystemAlarm_dpPattern+drv_pattern+sDrvNum+".alarm", value);
			g_mSystemIntegrityDriver[sDrvNum] = value;
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_driver_CheckingCallback
/**
Purpose:
Callback function. This function is called when the system alarm is enabled for this device.

	@param sDp1: string, input, data point name
	@param manNum: dyn_int, input, list of driver numbers connected to the event manager
	@param sDp2: string, input, data point name
	@param bOV: bool, input, true=driver overflow, 0=OK

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_driver_CheckingCallback(string sDp1, dyn_int manNum, string sDp2, bool bOV)
{
	int value, drvNum, pos;
	string sTemp;
	string sDrvNum;
	
	sTemp = dpSubStr(sDp2, DPSUB_DP);
//	DebugN(getCurrentTime(), "DRV", sDp1, manNum, sTemp, test_g++);
	sDrvNum = substr(sTemp, strpos(sTemp, "_Driver")+strlen("_Driver"), strlen(sTemp));
	sscanf(sDrvNum, "%d", drvNum);
//DebugN("unSystemIntegrity_driver_CheckingCallback", sDrvNum, manNum, sDp2, bOV, sTemp);
	pos = dynContains(manNum, drvNum);
	if(pos>0) { // driver found
		value = c_unSystemIntegrity_no_alarm_value;
		if(bOV)
			value = 2*c_unSystemIntegrity_alarm_value_level1;
	}
	else { //driver not found
		value = c_unSystemIntegrity_alarm_value_level1;
	}
	if(g_mSystemIntegrityDriver[sDrvNum] != value) {
		dpSet( c_unSystemAlarm_dpPattern+drv_pattern+sDrvNum+".alarm", value);
		g_mSystemIntegrityDriver[sDrvNum] = value;
	}
}

//------------------------------------------------------------------------------------------------------------------------

//@}
