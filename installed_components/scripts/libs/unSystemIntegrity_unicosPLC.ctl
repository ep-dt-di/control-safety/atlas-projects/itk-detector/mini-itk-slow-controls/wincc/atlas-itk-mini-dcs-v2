/**@name LIBRARY: unSystemIntegrity_unicosPLC.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 12/07/2002

Modification History: 
  12/06/2013: Marco Boccioli
  - @jira{IS-899}: possibility of having multiple PLC models. PREMIUM and QUANTUM are now all treated as PREMIUM
  
  16/06/2011: Herve
  - IS-547: get the list of systemAlarm pattern
  
  10/03/2011: Herve
    - IS-489: Enable/Disable the regular GQ opening in the driver
    - IS-504: UnPlc (Schneider) PLC errors: delay the reset of the errors of at least 1 msec. 
    
  22/11/2010: Herve
    - IS-444: Active DPE since PVSS 3.8 SP2
    
  25/08/2010: Herve
    - IS-372: 
        set default synchro PLC time to 200sec 
        bug fix: "_Connections.Driver.ManNums" CB started at each call to unSystemIntegrity_unicosPLC_HandleCommand
        extended diagnostic
        exclusive lock on send ip
        when the com is restored, launch a time synchro, delay 500msec & request all data 
        every multiple of check UnPLC open the GQ at the driver level, a 0 value means default (=30)
          
  29/09/2009: Herve
    - reset the MODBUS error
    
	20/10/2008: Frederic
		- Update unSystemIntegrity_synchronizedTimeDateOfPlc()  - use the DLL functions to get the correct UTC time (cf. CtrlTimeUtils.dll or .so)
	
	24/11/2005: Herve
		- add dpExists in unSystemIntegrity_unicosPLC_Initialize

	26/08/2005: Herve
		- bug in unSystemIntegrity_unicosPLCTimeCallback: set of the alarm everytime the counter is updated, should be
		if there is an alarm or if the alarm is reseted (use a global variable).
		
	31/05/2005: Herve
		- unSystemIntegrity_unicosPLC_ConnectionDriverCB: if driver is off reset the global variable for sending the ip so the next time
		the GQ bit will be set.
		
	25/05/2005: Herve
		- in unSystemIntegrity_unicosPLCTimeCallback: check that the time and date of the PLC (same as counter) is not too far 
		in the past, use the CONST_CHANGE_DIFF (valueChangeTimeDiff of the config file)
		
	15/04/2005: Herve
		- set PLC_DS_pattern to "DS_Comm_" instead of "PLC_DS_Comm_"
		- add DS_pattern
		- set PLC_DS_Time to "DS_Time_" instead of "PLC_DS_Time_"
		- add DS_Time_pattern
		- test if DpType is of _UnPlc
		
	12/04/2005: Herve
		- enable PLC -> set the GQ bit
		- timeout on PLC-DS communication set the GQ bit
		
	- 15/02/2005: Herve
		- add FESystemAlarm type
		
	- 09/11/2004: Herve
		- moving to PVSS 2.0 v3
		- systemAlarm error not reseted if the PLC is still not responding: counter not incremented.
		- start ip sending and time synchro only when the driver is completely running, ie: check _Connections.Driver.ManNums and wait one cycle.

version 1.0

External Functions: 
	
Internal Functions: 
	
Purpose: 
	Library of the unicosPLC component used by the systemIntegrity.
	 	
Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. global variable: the following variables are global to the script
	. data point type needed: _UnSystemIntegrity
	. data point: unicosPLC_systemIntegrity
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/

# uses "CtrlTimeUtils"

//--------------------------------------------------------------------------------------------------------------------------------
// constant declaration
//------------------
const string UN_SYSTEM_INTEGRITY_unicosPLC = "unicosPLC";
const string UN_SYSTEM_INTEGRITY_unicosPLC_check = "unSystemIntegrity_unicosPLCCheck";
const string UN_SYSTEM_INTEGRITY_unicosPLC_IPSending = "unSystemIntegrity_unicosPLCIPSending";
const string UN_SYSTEM_INTEGRITY_unicosPLC_TimeSynchro = "unSystemIntegrity_unicosPLCTimeSynchro";

const string c_unSystemIntegrity_PLC_DS_Time_callback = "unSystemIntegrity_unicosPLCTimeCallback";
const string c_unSystemIntegrity_PLC_DS_Error_callback = "unSystemIntegrity_unicosPLCErrorCallback";

const string PLC_DS_pattern = "DS_Comm_";
const string DS_pattern = "DS_Comm_";
const string PLC_DS_Time_pattern = "DS_Time_";
const string DS_Time_pattern = "DS_Time_";
const string PLC_DS_Error_pattern = "PLC_DS_Error_";
const string Mod_Plc_Error_pattern = "Mod_Plc_Error_";
const string PLCRedu1_DS_Error_pattern = "PLCRedu1_DS_Error_";
const string PLCRedu2_DS_Error_pattern = "PLCRedu2_DS_Error_";
const string PLC_PLC_pattern = "PLC_PLC_Comm_";
const string FE_pattern = "FESystemAlarm_";
const string UN_PLC_DPTYPE = "_UnPlc";

const string c_unSystemIntegrity_UnPlc = "_unPlc_";
const string c_unSystemIntegrity_ModPlc = "_Mod_Plc_";

const int c_unSystemIntegrity_defaultPLCCheckingDelay = 20; // default value for the checking in sec.
const int c_unSystemIntegrity_defaultPLCSendIpDelay = 4500; // default value for the send ip in msec.
const int c_unSystemIntegrity_defaultPLCTimeSynchroDelay = 200; // default value for the synchronizing the PLC time in sec.

const int c_unSystemIntegrity_functionParameterLenth = 5; // 5 parameters must be given to the function
const int c_unSystemIntegrity_PLCSynchonizedTimeDate = 1; // synchronise the clock of the PLC
const int c_unSystemIntegrity_PLCGetAll_BS_AS_table = 2; // request all the AS and BS table

const string CONNECTION_DRIVER = "_Connections.Driver.ManNums";
const int CONST_CHANGE_DIFF = 600; // 10 min in te past is allowed for the time stamp of the PLC.
const int c_unSystemIntegrity_UnPLCStartComDelayBefore_GQ =500;
mapping g_unSystemIntegrity_reset;
mapping g_unSystemIntegrity_UnPlc_TimeAlarm; // variable used to keep the latest alarm.
const int c_unSystemIntegrity_UnPLCGQThreadNbCheck = 30;

// 
//------------------

// global declaration
global dyn_string g_unSystemIntegrity_PLC_DSList; // list of the PLC-DS dp checked
global dyn_int g_unSystemIntegrity_PLC_DSThreadId; // list of the PLC-DS thread Id checking the counter
global dyn_string g_unSystemIntegrity_PLC_modPlcList; // list of the ModPlc Dp.
global dyn_int g_unSystemIntegrity_PLC_OldError; // list of the PLC-DS thread Id
global dyn_int g_unSystemIntegrity_PLCIPSending_ThreadId; // list of the thread Id sending the IP to the PLC
global dyn_int g_unSystemIntegrity_PLCTimeSynchro_ThreadId; // list of the thread Id synchronising the data and time of the PLC
global int g_unSystemIntegrity_PLCCheckingDelay;
global int g_unSystemIntegrity_PLCIpSendingDelay;
global int g_unSystemIntegrity_PLCTimeSynchroDelay;
global int g_IPValueToSend;
global dyn_int g_unSystemIntegrity_PLC_state;

global dyn_int g_DriverConnection; // keep the list of driver num started.
global bool g_bUnicosPLCNotStarted = true;
global bool g_unSystemIntegrityUnPLCLock;
global mapping g_m_unSystemIntegrity_UnPLC_Started; // 
global int g_i_unSystemIntegrity_UnPLC_DriverGQThread; 
global int g_unSystemIntegrity_UnPLCGQThreadNbCheck;

//@{

//------------------------------------------------------------------------------------------------------------------------
// unicosPLC_systemIntegrityInfo
/** Return the list of systemAlarm pattern. 
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@return list of systemAlarm pattern
*/
dyn_string unicosPLC_systemIntegrityInfo()
{
  return makeDynString(PLC_DS_pattern, PLC_DS_Error_pattern, PLC_DS_Time_pattern, FE_pattern, PLC_PLC_pattern);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLC_Initialize
/**
Purpose:
Get the list of unicosPLC check by the systemIntegrity. The ones that are enabled.

@param dsResult: dyn_string, output, the list of enabled unicosPLC that are checked

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLC_Initialize(dyn_string &dsResult)
{
	dyn_string dsList;
	int len, i;
	string dpToCheck, sHostName, sIP;
	bool enabled;

        g_unSystemIntegrity_UnPLCGQThreadNbCheck = c_unSystemIntegrity_UnPLCGQThreadNbCheck;
	// get the ip value to send
	unGenericDpFunctions_getHostName(getSystemName(), sHostName, sIP);	
	g_IPValueToSend = unConfigGenericFunctions_convertIP(sIP);

	dpGet(UN_SYSTEM_INTEGRITY_unicosPLC+UN_SYSTEMINTEGRITY_EXTENSION+".config.data", dsList);
	unSystemIntegrity_unicosPLC_DataCallback("", dsList);

	dsList = dpNames(c_unSystemAlarm_dpPattern+PLC_DS_pattern+"*", c_unSystemAlarm_dpType);
	len = dynlen(dsList);
	for(i = 1; i<=len; i++) {
		dpToCheck = dpSubStr(dsList[i], DPSUB_DP);
		dpToCheck = substr(dpToCheck, strlen(c_unSystemAlarm_dpPattern+PLC_DS_pattern),strlen(dpToCheck));
		if(dpExists(dpToCheck)) {
			if(dpTypeName(dpToCheck) == UN_PLC_DPTYPE) {
				dpGet(dsList[i]+".enabled", enabled);
				if(enabled)
					dynAppend(dsResult, dpToCheck);
			}
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLC_ConnectionDriverCB
/**
Purpose:
callback on the list of connected driver.

@param sDpe1: string, input, dpe
@param diDriverNum: dyn_int, input, the list of driver number started

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLC_ConnectionDriverCB(string sDp, dyn_int diDriverNum)
{
	dyn_string dsPlcList = g_unSystemIntegrity_PLC_DSList;
	string sPlcDpName, sModPlcDp;
	int i, len, drvNbr, plcNbr;

	g_DriverConnection = diDriverNum;
	len = dynlen(dsPlcList);
	for(i=1;i<=len;i++) {
		sPlcDpName = substr(dsPlcList[i], strlen(c_unSystemAlarm_dpPattern+PLC_DS_pattern), strlen(dsPlcList[i]));
		if(dpExists(sPlcDpName)) {
			dpGet(sPlcDpName+".communication.driver_num", drvNbr, sPlcDpName+".configuration.mod_PLC", sModPlcDp);
			if(!dynContains(diDriverNum, drvNbr)) {
// set the re-initialise send IP.
				g_unSystemIntegrity_reset[sPlcDpName] = true;
//DebugTN("unSystemIntegrity_unicosPLC_ConnectionDriverCB GQ", sPlcDpName, drvNbr, g_unSystemIntegrity_reset);
			}
		}
	}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLC_ConnectionDriverCB", "sDp= "+sDp+" diDriverNum= "+diDriverNum);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLC_HandleCommand
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param command: int, input, the systemIntegrity command
@param sDpe2: string, input, dpe
@param parameters: dyn_string, input, the list of parameters of the systemIntegrity command

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLC_Delete(dyn_string parameters, dyn_string exceptionInfo) {
  int i, len =dynlen(parameters);
  for(i=1; i<=len; i++) {
				unSystemIntegrity_unicosPLC_checking(parameters[i], false, false, exceptionInfo);
				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]);

				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+parameters[i]);

				// remove dp from the alert list of applicationDP if it is in
				_unSystemIntegrity_modifyApplicationAlertList(c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+parameters[i], false, exceptionInfo);
				// delete the _UnSystemAlarm dps
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+parameters[i]))
					dpDelete(c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+parameters[i]);
	}
}

unSystemIntegrity_unicosPLC_ExtendedDiagnostic(dyn_string parameters, int command) {
  dyn_string dsResult;
  string sResult;
  int i, len;
 			dynAppend(dsResult, getCurrentTime());
			dynAppend(dsResult, "g_unSystemIntegrity_PLC_DSList");
			len = dynlen(g_unSystemIntegrity_PLC_DSList);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_PLC_DSList[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_PLC_DSThreadId");
			len = dynlen(g_unSystemIntegrity_PLC_DSThreadId);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_PLC_DSThreadId[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_PLC_modPlcList");
			len = dynlen(g_unSystemIntegrity_PLC_modPlcList);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_PLC_modPlcList[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_PLC_OldError");
			len = dynlen(g_unSystemIntegrity_PLC_OldError);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_PLC_OldError[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_PLCIPSending_ThreadId");
			len = dynlen(g_unSystemIntegrity_PLCIPSending_ThreadId);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_PLCIPSending_ThreadId[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_PLCTimeSynchro_ThreadId");
			len = dynlen(g_unSystemIntegrity_PLCTimeSynchro_ThreadId);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_PLCTimeSynchro_ThreadId[i];
				dynAppend(dsResult, sResult);
			}
			dynAppend(dsResult, "g_unSystemIntegrity_PLCCheckingDelay");
			dynAppend(dsResult, g_unSystemIntegrity_PLCCheckingDelay);
			dynAppend(dsResult, "g_unSystemIntegrity_PLCIpSendingDelay");
			dynAppend(dsResult, g_unSystemIntegrity_PLCIpSendingDelay);
			dynAppend(dsResult, "g_unSystemIntegrity_PLCTimeSynchroDelay");
			dynAppend(dsResult, g_unSystemIntegrity_PLCTimeSynchroDelay);
			dynAppend(dsResult, "g_IPValueToSend");
			dynAppend(dsResult, g_IPValueToSend);
			
			dynAppend(dsResult, "g_unSystemIntegrity_PLC_state");
			len = dynlen(g_unSystemIntegrity_PLC_state);
			for(i=1; i<=len; i++) {
				sResult = g_unSystemIntegrity_PLC_state[i];
				dynAppend(dsResult, sResult);
			}
			
			dynAppend(dsResult, "g_DriverConnection");
			len = dynlen(g_DriverConnection);
			for(i=1; i<=len; i++) {
				sResult = g_DriverConnection[i];
				dynAppend(dsResult, sResult);
			}
			
			len=mappinglen(g_unSystemIntegrity_reset);
			for(i=1;i<=len;i++)
			{
				dynAppend(dsResult, "g_unSystemIntegrity_reset["+mappingGetKey(g_unSystemIntegrity_reset, i)+"]");
				dynAppend(dsResult, mappingGetValue(g_unSystemIntegrity_reset, i));
			}
			
			len=mappinglen(g_unSystemIntegrity_UnPlc_TimeAlarm);
			for(i=1;i<=len;i++)
			{
				dynAppend(dsResult, "g_unSystemIntegrity_UnPlc_TimeAlarm["+mappingGetKey(g_unSystemIntegrity_UnPlc_TimeAlarm, i)+"]");
				dynAppend(dsResult, mappingGetValue(g_unSystemIntegrity_UnPlc_TimeAlarm, i));
			}
			dynAppend(dsResult, "g_i_unSystemIntegrity_UnPLC_DriverGQThread");
			dynAppend(dsResult, g_i_unSystemIntegrity_UnPLC_DriverGQThread);
			
			len=mappinglen(g_m_unSystemIntegrity_UnPLC_Started);
			for(i=1;i<=len;i++)
			{
				dynAppend(dsResult, "g_m_unSystemIntegrity_UnPLC_Started["+mappingGetKey(g_m_unSystemIntegrity_UnPLC_Started, i)+"]");
				dynAppend(dsResult, mappingGetValue(g_m_unSystemIntegrity_UnPLC_Started, i));
			}
			
			dynAppend(dsResult, "g_unSystemIntegrity_UnPLCGQThreadNbCheck");
			dynAppend(dsResult, g_unSystemIntegrity_UnPLCGQThreadNbCheck);
			
			dpSet(UN_SYSTEM_INTEGRITY_unicosPLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
			  UN_SYSTEM_INTEGRITY_unicosPLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", dsResult);
}

unSystemIntegrity_unicosPLC_Debug(dyn_string parameters, int command) {
  if(dynlen(parameters) >= 2)
			{
				g_b32DebugLevel = (bit32)parameters[1];
				g_sDebugFilter = parameters[2];
			}
			else
			{
				g_b32DebugLevel = (bit32)0;
				g_sDebugFilter = "";
			}
			dpSet(UN_SYSTEM_INTEGRITY_unicosPLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
				UN_SYSTEM_INTEGRITY_unicosPLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", makeDynString(g_b32DebugLevel, g_sDebugFilter));
}

unSystemIntegrity_unicosPLC_HandleCommand(string sDpe1, int command, string sDpe2, dyn_string parameters)
{
	dyn_string exceptionInfo;
	int i, len =dynlen(parameters);
        dyn_string dsResult;
        string sResult;

	switch(command) {
		case UN_SYSTEMINTEGRITY_ADD: // add the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				unSystemIntegrity_unicosPLC_checking(parameters[i], true, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DELETE: // delete the _UnSystemAlarm DP
      unSystemIntegrity_unicosPLC_Delete(parameters,exceptionInfo);
			
			break;
		case UN_SYSTEMINTEGRITY_ENABLE: // enable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]))
					unSystemIntegrity_unicosPLC_checking(parameters[i], false, true, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DISABLE: // disable the _UnSystemAlarm DP
			for(i=1; i<=len; i++) {
				if(dpExists(c_unSystemAlarm_dpPattern+PLC_DS_pattern+parameters[i]))
					unSystemIntegrity_unicosPLC_checking(parameters[i], false, false, exceptionInfo);
			}
			break;
		case UN_SYSTEMINTEGRITY_DIAGNOSTIC: // give the list of _UnSystemAlarm DP and the state
			dpSet(UN_SYSTEM_INTEGRITY_unicosPLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.commandResult", command, 
					UN_SYSTEM_INTEGRITY_unicosPLC+UN_SYSTEMINTEGRITY_EXTENSION+".diagnostic.result", g_unSystemIntegrity_PLC_DSList);
			break;
        case UN_SYSTEMINTEGRITY_EXTENDED_DIAGNOSTIC: // extended diagnostic
        unSystemIntegrity_unicosPLC_ExtendedDiagnostic(parameters, command);
			break;
		case UN_SYSTEMINTEGRITY_DEBUG:
			unSystemIntegrity_unicosPLC_Debug(parameters, command);
			break;
		default:
			break;
	}

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLC_HandleCommand", command, parameters);
	if(dynlen(exceptionInfo) > 0) {
		if(isFunctionDefined("unMessageText_sendException")) {
			unMessageText_sendException("*", "*", "unSystemIntegrity_unicosPLC_HandleCommand", "user", "*", exceptionInfo);
		}
// handle any error uin case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugN(getCurrentTime(), exceptionInfo);
		}
	}

// do here the dpConnect to the list of drivers
	if(g_bUnicosPLCNotStarted) {
		dpConnect("unSystemIntegrity_unicosPLC_ConnectionDriverCB", CONNECTION_DRIVER);
		g_bUnicosPLCNotStarted = false;
	}
        if(g_i_unSystemIntegrity_UnPLC_DriverGQThread <=0)
          g_i_unSystemIntegrity_UnPLC_DriverGQThread = startThread("unSystemIntegrity_UnPLC_DriverGQThread");
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLC_DataCallback
/**
Purpose:
handle any systemIntegrity command.

@param sDpe1: string, input, dpe
@param sConfigData: string, input, the config data

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLC_DataCallback(string sDpe1, dyn_string dsConfigData)
{
	dyn_string dsTemp=dsConfigData;

	while(dynlen(dsTemp) <4)
		dynAppend(dsTemp, "0");

	g_unSystemIntegrity_PLCCheckingDelay = (int)dsTemp[1];
	if(g_unSystemIntegrity_PLCCheckingDelay <= 0)
		g_unSystemIntegrity_PLCCheckingDelay = c_unSystemIntegrity_defaultPLCCheckingDelay;

	g_unSystemIntegrity_PLCIpSendingDelay = (int)dsTemp[2];
	if(g_unSystemIntegrity_PLCIpSendingDelay <= 0)
		g_unSystemIntegrity_PLCIpSendingDelay = c_unSystemIntegrity_defaultPLCSendIpDelay;
	
	g_unSystemIntegrity_PLCTimeSynchroDelay = (int)dsTemp[3];
	if(g_unSystemIntegrity_PLCTimeSynchroDelay <= 0)
		g_unSystemIntegrity_PLCTimeSynchroDelay = c_unSystemIntegrity_defaultPLCTimeSynchroDelay;

        g_unSystemIntegrity_UnPLCGQThreadNbCheck = (int)dsTemp[4];
        if(g_unSystemIntegrity_UnPLCGQThreadNbCheck == 0)
          g_unSystemIntegrity_UnPLCGQThreadNbCheck = c_unSystemIntegrity_UnPLCGQThreadNbCheck;
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLC_DataCallback", dsConfigData, "---->>dsConfigData= "+dsConfigData
                               +" g_unSystemIntegrity_PLCCheckingDelay="+g_unSystemIntegrity_PLCCheckingDelay+" g_unSystemIntegrity_PLCIpSendingDelay="+g_unSystemIntegrity_PLCIpSendingDelay
                               +" g_unSystemIntegrity_PLCTimeSynchroDelay= "+g_unSystemIntegrity_PLCTimeSynchroDelay, "g_unSystemIntegrity_UnPLCGQThreadNbCheck = "+g_unSystemIntegrity_UnPLCGQThreadNbCheck);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLC_checking
/**
Purpose:
This function register/de-register the callback funtion of unicosPLC manager. This function can also create the _unSystemAlarm_unicosPLC dp 
with the alarm config but it cannot delete it.

	@param sDp: string, input, data point name
	@param bCreate: bool, input, true create the dp and the alarm config if it does not exist, enable it
	@param bRegister: bool, input, true do a dpConnect, false do a dpDisconnect
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLC_create(string sDpToChek, dyn_string &exceptionInfo) {
  //function creates the dp and the alarm config if it does not exist, enable it
  string plcHostname, description, dpTime, dpError,dp;
  int drvNum;
  
  dpTime = c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+sDpToChek;
  dpError = c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sDpToChek;
  dp = c_unSystemAlarm_dpPattern+PLC_DS_pattern+sDpToChek;
  dpGet(sDpToChek+".communication.driver_num", drvNum);
  
  // get the plc hostname and the driver number
		plcHostname = unGenericDpFunctions_getAlias(sDpToChek);
//		dpGet( sDpToChek+".communication.counter:_distrib.._driver", drvNum);
// create the sDp and its alarm config if it is not existing
		if(!dpExists(dp)) {
			description = getCatStr("unSystemIntegrity", "PLC_DS_DESCRIPTION")+plcHostname+" -> DS driver "+ drvNum;
			unSystemIntegrity_createSystemAlarm(sDpToChek, PLC_DS_pattern, description, exceptionInfo);
		}
// create the sDp time and its alarm config if it is not existing
		if(!dpExists(dpTime)) {
			description = getCatStr("unSystemIntegrity", "PLC_DS_TIME_DESCRIPTION")+plcHostname+" -> DS driver "+ drvNum;
			unSystemIntegrity_createSystemAlarm(sDpToChek, PLC_DS_Time_pattern, description, exceptionInfo);
		}
// create the sDp Error and its alarm config if it is not existing
		if(!dpExists(dpError)) {
			description = getCatStr("unSystemIntegrity", "PLC_DS_ERROR_DESCRIPTION")+drvNum+" -> "+plcHostname;
			unSystemIntegrity_createSystemAlarm(sDpToChek, PLC_DS_Error_pattern, description, exceptionInfo);
		}
}
unSystemIntegrity_unicosPLC_register(string sDp, string sDpToChek, dyn_string &exceptionInfo) {
  //do dpConnect
	string dpTime, dpError, dp, dpPlc;
	int drvNum, res, thId, pos, len, i,iPlcNumber; 
	bool bError = false; 
	dyn_string plc_plc_list;

	dpTime = c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+sDpToChek;
	dpError = c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sDpToChek;
	dp = c_unSystemAlarm_dpPattern+PLC_DS_pattern+sDpToChek;
	dpGet(sDpToChek+".configuration.mod_PLC", dpPlc);
	dpGet(sDpToChek+".communication.driver_num", drvNum);
	dpGet(dpPlc+".PlcNumber", iPlcNumber);

	// connect to the callback function if not already done
	pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_PLC_DSList, dp);
	if(pos <= 0) {
		dpSet(dpPlc+".Active", true);
		// add it in the list, because one of the callback function needs it.				
		pos = _unSystemIntegrity_setList(g_unSystemIntegrity_PLC_DSList, dp, true);

		// start the thread for checking the counter
		thId = startThread(UN_SYSTEM_INTEGRITY_unicosPLC_check, sDpToChek, drvNum);
    g_unSystemIntegrity_PLC_DSThreadId[pos] = thId;
		if(thId<0)
			bError = true;
		if(!bError) {
	// start the thread for sending the ip
			g_unSystemIntegrity_reset[sDpToChek] = true;
			thId = startThread(UN_SYSTEM_INTEGRITY_unicosPLC_IPSending, sDpToChek, drvNum);
			if(thId<0)
				bError = true;
			g_unSystemIntegrity_PLCIPSending_ThreadId[pos] = thId;
		}
		if(!bError) {
	// start the thread for synchronising the PLC
			thId = startThread(UN_SYSTEM_INTEGRITY_unicosPLC_TimeSynchro, sDpToChek, drvNum);
			if(thId<0)
				bError = true;

			g_unSystemIntegrity_PLCTimeSynchro_ThreadId[pos] = thId;
		}
				
		if(!bError) {
			g_unSystemIntegrity_UnPlc_TimeAlarm[dpSubStr(sDpToChek, DPSUB_SYS_DP)] = -1;
		// dpConnect to the stime of the counter
			res = dpConnect(c_unSystemIntegrity_PLC_DS_Time_callback, 
													sDpToChek+".communication.counter:_online.._stime_inv",
													sDpToChek+".communication.counter:_online.._stime");
			if(res<0)
				bError = true;
			if(!bError) {
		// dpConnect to _Mod_Plc dp for error.
		//dpGet(sDpToChek+".configuration.mod_PLC", dpPlc);
				g_unSystemIntegrity_PLC_modPlcList[pos] = dpPlc;
				g_unSystemIntegrity_PLC_OldError[pos] = -1;
				g_unSystemIntegrity_PLC_state[pos] = -1;
				res = dpConnect(c_unSystemIntegrity_PLC_DS_Error_callback, dpPlc+".Error", dpPlc+".ConnState");
				if(res<0)
					bError = true;
			}	
		}
		if(bError) {
			pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_PLC_DSList, dp);
			if(pos > 0) {
		// kill the threads if they were started
				res = stopThread(g_unSystemIntegrity_PLC_DSThreadId[pos]);
				res = stopThread(g_unSystemIntegrity_PLCIPSending_ThreadId[pos]);
				res = stopThread(g_unSystemIntegrity_PLCTimeSynchro_ThreadId[pos]);
		// and then remove it from the list
				pos = _unSystemIntegrity_setList(g_unSystemIntegrity_PLC_DSList, dp, false);
				dynRemove(g_unSystemIntegrity_PLC_DSThreadId, pos);
				dynRemove(g_unSystemIntegrity_PLC_modPlcList, pos);
				dynRemove(g_unSystemIntegrity_PLC_OldError, pos); 
				dynRemove(g_unSystemIntegrity_PLC_state, pos); 
				dynRemove(g_unSystemIntegrity_PLCIPSending_ThreadId, pos);
				dynRemove(g_unSystemIntegrity_PLCTimeSynchro_ThreadId, pos);
			dpSet(dpPlc+".Active", false);
			}

			fwException_raise(exceptionInfo, "ERROR", 
						"unSystemIntegrity_unicosPLC_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_CONNECT") +dp,"");
		}
		else {
			// set the enable to true and activate the alarm if it is not activated.
			dpSet(dp+".enabled", true);
			unAlarmConfig_mask(dp+".alarm", false, exceptionInfo);
			dpSet(dpTime+".enabled", true);
			unAlarmConfig_mask(dpTime+".alarm", false, exceptionInfo);
			dpSet(dpError+".enabled", true);
			unAlarmConfig_mask(dpError+".alarm", false, exceptionInfo);
			// get all the PLC-PLC dp, and enable them
			plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
			len = dynlen(plc_plc_list);
			for(i=1;i<=len;i++) {
				dpSet(plc_plc_list[i]+".enabled", true);
				unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
			}
			// get all the FESystemAlarm dp, and enable them, check the Ok state and set the correct value
			plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"*", c_unSystemAlarm_dpType);
			len = dynlen(plc_plc_list);
			for(i=1;i<=len;i++) {
				dpSet(plc_plc_list[i]+".enabled", true);
				unAlarmConfig_mask(plc_plc_list[i]+".alarm", false, exceptionInfo);
			}
			synchronized(g_m_unSystemIntegrity_UnPLC_Started) {
							  g_m_unSystemIntegrity_UnPLC_Started[iPlcNumber] = drvNum;
			}
		}
	}
	unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLC_checking register", sDp);
  
}
unSystemIntegrity_unicosPLC_unregister(string sDp, string sDpToChek, dyn_string &exceptionInfo) {
  // do dpDisconnect
	string dp, dpTime, dpError, dpPlc;
	int res, pos, drvNum, len,i,iPlcNumber;
	bool bError = false;
	dyn_string plc_plc_list;
  
	dpTime = c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+sDpToChek;
	dpError = c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sDpToChek;
	dp = c_unSystemAlarm_dpPattern+PLC_DS_pattern+sDpToChek;
	dpGet(sDpToChek+".communication.driver_num", drvNum);
	dpGet(sDpToChek+".configuration.mod_PLC", dpPlc);
  dpGet(dpPlc+".PlcNumber", iPlcNumber);
  
  // disconnect the callback function
	pos = _unSystemIntegrity_isInList(g_unSystemIntegrity_PLC_DSList, dp);
	if(pos > 0) {
		synchronized(g_m_unSystemIntegrity_UnPLC_Started) {
		mappingRemove(g_m_unSystemIntegrity_UnPLC_Started, iPlcNumber);
	}
	dpSet(dpPlc+".Active", false);
	// kill the thread
	res = stopThread(g_unSystemIntegrity_PLC_DSThreadId[pos]);
	if(res<0)
	  bError = true;
	res = stopThread(g_unSystemIntegrity_PLCIPSending_ThreadId[pos]);
	if(res<0)
	  bError = true;
	res = stopThread(g_unSystemIntegrity_PLCTimeSynchro_ThreadId[pos]);
	if(res<0)
		bError = true;

// dpDisconnect stime of the counter
	res = dpDisconnect(c_unSystemIntegrity_PLC_DS_Time_callback, 
													sDpToChek+".communication.counter:_online.._stime_inv",
													sDpToChek+".communication.counter:_online.._stime");
	if(res<0)
		bError = true;

// dpDisconnect to _Mod_Plc dp for error.
	dpGet(sDpToChek+".configuration.mod_PLC", dpPlc);
	res = dpDisconnect(c_unSystemIntegrity_PLC_DS_Error_callback, dpPlc+".Error", dpPlc+".ConnState");
	if(res<0)
		bError = true;

	if(bError) {
		fwException_raise(exceptionInfo, "ERROR", 
					"unSystemIntegrity_unicosPLC_checking():"+getCatStr("unSystemIntegrity", "CANNOT_DP_DISCONNECT") +dp,"");
	}
	else {
			// remove from list
	pos = _unSystemIntegrity_setList(g_unSystemIntegrity_PLC_DSList, dp, false);
	dynRemove(g_unSystemIntegrity_PLC_DSThreadId, pos);
	dynRemove(g_unSystemIntegrity_PLC_modPlcList, pos);
	dynRemove(g_unSystemIntegrity_PLC_OldError, pos); 
	dynRemove(g_unSystemIntegrity_PLC_state, pos); 
	dynRemove(g_unSystemIntegrity_PLCIPSending_ThreadId, pos);
	dynRemove(g_unSystemIntegrity_PLCTimeSynchro_ThreadId, pos);
// set the enable to false and de-activate the alarm and acknowledge it if necessary
	dpSet(dp+".enabled", false, dp+".alarm", c_unSystemIntegrity_no_alarm_value);
	unAlarmConfig_mask(dp+".alarm", true, exceptionInfo);
	dpSet(dpTime+".enabled", false, dpTime+".alarm", c_unSystemIntegrity_no_alarm_value);
	unAlarmConfig_mask(dpTime+".alarm", true, exceptionInfo);
	dpSet(dpError+".enabled", false, dpError+".alarm", c_unSystemIntegrity_no_alarm_value);
	unAlarmConfig_mask(dpError+".alarm", true, exceptionInfo);
// get all the PLC-PLC dp, and disable them
	plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+PLC_PLC_pattern+sDpToChek+"_*", c_unSystemAlarm_dpType);
	len = dynlen(plc_plc_list);
	for(i=1;i<=len;i++) {
// do not reset the alarm value because this DPE should be connected to an address config
		dpSet(plc_plc_list[i]+".enabled", false);
		unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
	}

// get all the FESystemAlarm dp, and disable them, check the Ok state and set the correct value
	plc_plc_list = dpNames(c_unSystemAlarm_dpPattern+FE_pattern+sDpToChek+"_*", c_unSystemAlarm_dpType);
	len = dynlen(plc_plc_list);
	for(i=1;i<=len;i++) {
// do not reset the alarm value because this DPE should be connected to an address config
		dpSet(plc_plc_list[i]+".enabled", false);
		unAlarmConfig_mask(plc_plc_list[i]+".alarm", true, exceptionInfo);
				}
		}
	}
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosUnPLC_checking deregister", sDp);
    
  
}
unSystemIntegrity_unicosPLC_checking(string sDp, bool bCreate, bool bRegister, dyn_string &exceptionInfo)
{
	string sDpToChek, dp;
	int res;        
// remove the system name
	res = strpos(sDp, ":");
	if(res >= 0)
		dp = substr(sDp, res+1, strlen(sDp));
	else
		dp = sDp;
	sDpToChek = dp;
  // in case the Dp is not of type _UnPlc do nothing. Should not happen, just for security.
	if(dpTypeName(sDpToChek) != UN_PLC_DPTYPE)
		return;

	if(bCreate) {
    unSystemIntegrity_unicosPLC_create(sDpToChek,exceptionInfo);
	}
	if(bRegister) {
    unSystemIntegrity_unicosPLC_register(sDp, sDpToChek, exceptionInfo);
	}
	else {
    unSystemIntegrity_unicosPLC_unregister(sDp, sDpToChek, exceptionInfo);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLCCheck
/**
Purpose:
This function checks if the counter of the _UnPlc dp is modified periodically. If not it generates and alarm.

	@param sPlcDp: string, input, data point name

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLCCheck(string sPlcDp, int iDrvNum)
{
	int waitingTime, oldAlarmValue = -1;
	int oldValue = -1, newValue, alarmValue;
	bool bEnabled;
	string sModPlc;
	int res, iPlcNumber;
        bool bQuantum;
        string type;
        dyn_string exceptionInfo;
	
	// get the value of the counter and the timeout PLCChecking
	dpGet(sPlcDp+".communication.counter", oldValue, sPlcDp+".configuration.mod_PLC", sModPlc);
 //the QUANTUM and the PREMIUM are now treated the same
//         dpGet(sPlcDp+".configuration.type", type);
//         if(type == UN_CONFIG_QUANTUM)
//           bQuantum = true;
	dpGet(sModPlc+".PlcNumber", iPlcNumber);
	while(true) {
	// if 0 set it to c_unSystemIntegrity_defaultPLCCheckingDelay
		waitingTime = g_unSystemIntegrity_PLCCheckingDelay;
		if(waitingTime <= 0)
			waitingTime = c_unSystemIntegrity_defaultPLCCheckingDelay;
	// wait
		delay(waitingTime);
	
	// get the value of the counter and the timeout PLCChecking
		dpGet(sPlcDp+".communication.counter", newValue);
		if(newValue == oldValue) {
		// counter was not modified, set an alarm
			alarmValue = c_unSystemIntegrity_alarm_value_level1;
		}
		else	
		// counter was modified, reset the alarm
			alarmValue = c_unSystemIntegrity_no_alarm_value;

		if(oldAlarmValue != alarmValue) {
			dpSet(c_unSystemAlarm_dpPattern+PLC_DS_pattern+sPlcDp+".alarm", alarmValue);
			if(alarmValue != c_unSystemIntegrity_no_alarm_value) {
// set the GQ bit so all data will bypass the smoothing
//DebugTN("check", "_Driver" + iDrvNum + ".SM:_original.._value", sPlcDp, iDrvNum);
				dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
				dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", iPlcNumber+256);
                                unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLCCheck open GQ", sPlcDp, iDrvNum, iPlcNumber);
			}
                        else
                        {
                                if(dynContains(g_DriverConnection, iDrvNum) > 0)
                                  unSystemIntegrity_synchronizedTimeDateOfPlc(sPlcDp, bQuantum, exceptionInfo);
                                // wait c_unSystemIntegrity_UnPLCStartComDelayBefore_GQ = 500msec.
                                delay(0, c_unSystemIntegrity_UnPLCStartComDelayBefore_GQ);
				dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
				dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", iPlcNumber+256);
                                unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLCCheck open GQ & request data", sPlcDp, iDrvNum, iPlcNumber);
                                unSystemIntegrity_sendCmdToPLC(sPlcDp, c_unSystemIntegrity_PLCGetAll_BS_AS_table, makeDynInt(0, 0, 0, 0, 0) , exceptionInfo);
                        }
		}
		
		res = _unSystemIntegrity_isInList(g_unSystemIntegrity_PLC_modPlcList, sModPlc);
		if(res > 0) {
			g_unSystemIntegrity_PLC_state[res] = alarmValue;
		}
		oldValue = newValue;
		oldAlarmValue = alarmValue;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLCTimeSynchro
/**
Purpose:
This function synchronized the clock of the PLC.

	@param sPlcDp: string, input, data point name

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLCTimeSynchro(string sPlcDp, int iDrvNum)
{
	int waitingTime;
	dyn_string exceptionInfo;
	string type;
	bool bQuantum = false;
	
 //the QUANTUM and the PREMIUM are now treated the same
// 	dpGet(sPlcDp+".configuration.type", type);
// 	if(type == UN_CONFIG_QUANTUM)
// 		bQuantum = true;
		
	while(true) {
                unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLCTimeSynchro", sPlcDp, iDrvNum);
// synchronized only if the driver is started
		if(dynContains(g_DriverConnection, iDrvNum) > 0) {
			unSystemIntegrity_synchronizedTimeDateOfPlc(sPlcDp, bQuantum, exceptionInfo);
		}
		
	// if 0 set it to c_unSystemIntegrity_defaultPLCTimeSynchroDelay
		waitingTime = g_unSystemIntegrity_PLCTimeSynchroDelay;
		if(waitingTime <= 0)
			waitingTime = c_unSystemIntegrity_defaultPLCTimeSynchroDelay;
	// wait
		delay(waitingTime);
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLCIPSending
/**
Purpose:
This function checks if the counter of the _UnPlc dp is modified periodically. If not it generates and alarm.

	@param sPlcDp: string, input, data point name

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLCIPSending(string sPlcDp, int iDrvNum)
{
	int waitingTime;
	int oldValue = -1, newValue;
	bool bFirstTime = true;
	string sModPlc;
	int iPlcNumber;
		
	// get the value of the counter and the timeout PLCChecking
	dpGet(sPlcDp+".configuration.mod_PLC", sModPlc);
	dpGet(sModPlc+".PlcNumber", iPlcNumber);
	while(true) {
	// if 0 set it to c_unSystemIntegrity_defaultPLCCheckingDelay
		waitingTime = g_unSystemIntegrity_PLCIpSendingDelay;
		if(waitingTime <= 0)
			waitingTime = c_unSystemIntegrity_defaultPLCSendIpDelay;
	// wait
		delay(0, waitingTime);
	
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLCIPSending", sPlcDp, iDrvNum);
		newValue = g_IPValueToSend;	
		if(newValue == oldValue) {
			newValue = 0;
		}
		if(g_unSystemIntegrity_reset[sPlcDp])
			bFirstTime = true;
		if(dynContains(g_DriverConnection, iDrvNum) > 0) {
// synchronized only if the driver is started
			if(!bFirstTime)
                        {
                          synchronized(g_unSystemIntegrityUnPLCLock)
                          {
				dpSet(sPlcDp+".communication.send_IP", newValue);
                          }
                        }
			else {
				bFirstTime = false;
				g_unSystemIntegrity_reset[sPlcDp] = false;
//DebugTN("sendIP", sPlcDp, "reset _Driver" + iDrvNum + ".SM:_original.._value", sPlcDp, iDrvNum, g_unSystemIntegrity_reset);				
// set the GQ bit so all data will bypass the smoothing
				dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
				dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", iPlcNumber+256);
			}
		}
		else
			bFirstTime = true;
		oldValue = newValue;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLCTimeCallback
/**
Purpose:
Callback function to check if the time is correctly set in the PLC.

	@param sDp1: string, input, data point name
	@param bStimeInvalid: bool, input, state of the stime bit of the counter
	@param sDp2: string, input, data point name
	@param iTime: int, input, the timestamp of the counter

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLCTimeCallback(string sDp1, bool bStimeInvalid, string sDpe2, int iTime)
{
	string sDp, sKey = dpSubStr(sDp1, DPSUB_SYS_DP);
	int value, oldValue = -1;
	int iCurrentTime;
	bool bHasKey;
	


	if(mappingHasKey(g_unSystemIntegrity_UnPlc_TimeAlarm, sKey)) {
		oldValue = g_unSystemIntegrity_UnPlc_TimeAlarm[sKey];
		bHasKey = true;
	}
// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);
// set the alarm 
	if(bStimeInvalid) { // bad time synchro
		value = c_unSystemIntegrity_alarm_value_level1;
	}
	else { // good time synchro
	// check if too late in the past
		iCurrentTime = (int)getCurrentTime();
		if(iTime <= iCurrentTime) {
			if((iCurrentTime - iTime) > CONST_CHANGE_DIFF) { // too far in the past
				value = c_unSystemIntegrity_alarm_value_level1;
			}
			else
				value = c_unSystemIntegrity_no_alarm_value;
		}
		else
			value = c_unSystemIntegrity_no_alarm_value;
	}
	if(oldValue != value) {
		dpSet( c_unSystemAlarm_dpPattern+PLC_DS_Time_pattern+sDp+".alarm", value);
	}		
	oldValue = value;

	if(bHasKey)
		g_unSystemIntegrity_UnPlc_TimeAlarm[sKey] = value;

  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLCTimeCallback", sKey, value);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_unicosPLCErrorCallback
/**
Purpose:
Callback function to check if there is no error in the _Mod_Plc data point.

	@param sDp1: string, input, data point name
	@param error: unsigned, input, error set by the Modbus driver
	@param sDp2: string, input, data point name
	@param state: unsigned, input, communication state of the PLC

Usage: Internal

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_unicosPLCErrorCallback(string sDp1, unsigned error, string sDp2, unsigned state)
{
  //ignor interna modbus communication error
  if (error==2)
    return;
	string sDp, sAlPlc;
	int value, res;
	dyn_string hostsList;
	
// get the dpname without the systemname
	sDp = dpSubStr(sDp1, DPSUB_DP);

	value = c_unSystemIntegrity_no_alarm_value;
// set the alarm value
	if(state == 1) { // _Mod_Plc connected
		switch(error) {
			case 0: // no error
				break;
			case 1: // communication problem, no more re-connect
			case 2: // communication problem, re-connection active
			case 3: // error in slave connection
			case 4: // Peer closed the slave connection
			case 5: // Overflow of the Modbus queue
			case 6: // Error in decoding of the frame
			default:
				value = c_unSystemIntegrity_alarm_value_level1+c_unSystemIntegrity_alarm_value_level1+error;
				break;
		}
	}
	else
		value = c_unSystemIntegrity_alarm_value_level1;
		
	res = _unSystemIntegrity_isInList(g_unSystemIntegrity_PLC_modPlcList, sDp);
	if(res > 0) {
	sAlPlc = g_unSystemIntegrity_PLC_DSList[res];
	strreplace(sAlPlc, c_unSystemAlarm_dpPattern+PLC_DS_pattern, c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern);
//	c_unSystemAlarm_dpPattern+PLC_DS_pattern
//		dpGet(sDp+".configuration.Mod_PLC",sPlc);
		dpSet( sAlPlc+".alarm", value);
	}
	if(error == 1) { // reset the communication
// get the old Error and if it was not 1 reset the comm
// this is to avoid to retrigger indefinitely this loop.
// the drivers rewrite Error and state when the dpSet is done.
		if(res > 0) {
			if(g_unSystemIntegrity_PLC_OldError[res] != error) {
				dpGet( sDp+".HostsAndPorts", hostsList);
				dpSet( sDp+".HostsAndPorts", hostsList);
				g_unSystemIntegrity_PLC_OldError[res] = error;
			}
		}
	}
	if(res > 0) 
		g_unSystemIntegrity_PLC_OldError[res] = error;
		
	switch(error) {
		case 1:
		case 2:
// if the PLC is seen as connected and there is an error: 1 or 2, then reset the error only if the PLC is up again.
			if(res > 0) {
				if(g_unSystemIntegrity_PLC_state[res] == c_unSystemIntegrity_no_alarm_value)
					dpSet( sDp+".Error", 0);
			}
			break;
                case 0 : // do nothing
                  break;
		default: // reset the error.
                  delay(0, 1);
                  dpSet( sDp+".Error", 0);
			break;
	}
  unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_unicosPLCErrorCallback", "sAlPlc= "+sAlPlc, error, state, value);
}

//------------------------------------------------------------------------------------------------------------------------

// unSystemIntegrity_sendCmdToPLC
/**
Purpose:
send a command to PLC.

	@param sPlcDp: string, input, PLC data point name of type _UnPlc
	@param iFunction: int, input, function 
	@param diParam: dyn_int, input, parameter list
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_sendCmdToPLC(string sPlcDp, int iFunction, dyn_int diParam, dyn_string &exceptionInfo)
{
	int len;
	string dp=dpSubStr(sPlcDp, DPSUB_SYS_DP);
	
	len = dynlen(diParam);
	
	if(len != c_unSystemIntegrity_functionParameterLenth) {
		fwException_raise(exceptionInfo, "ERROR", 
						"unSystemIntegrity_sendCmdToPLC():"+getCatStr("unSystemIntegrity", "WRONG_PARAM_LENGTH"),"");
		return;
	}
	if(dpExists(dp)) {
          synchronized(g_unSystemIntegrityUnPLCLock)
          {
		dpSet(dp+".communication.commandInterface.function", iFunction, 
										dp+".communication.commandInterface.param0", diParam[1], 
										dp+".communication.commandInterface.param1", diParam[2], 
										dp+".communication.commandInterface.param2", diParam[3], 
										dp+".communication.commandInterface.param3", diParam[4], 
										dp+".communication.commandInterface.param4", diParam[5]);
          }
	}
	else {
		fwException_raise(exceptionInfo, "ERROR", 
						"unSystemIntegrity_sendCmdToPLC():"+getCatStr("unSystemIntegrity", "PLCDP_DOES_NOT_EXIST") + sPlcDp,"");
	}
}

// unSystemIntegrity_synchronizedTimeDateOfPlc
/**
Purpose:
Synchronized the PLC clock.

	@param sPlcDp: string, input, PLC data point name of type _UnPlc
	@param bQuantum: bool, input, true for Quantum PLC, false for Premium PLC
	@param exceptionInfo: dyn_string, output, exception are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: W2000, NT and Linux, but tested only under W2000 and Linux.
	. distributed system: yes.
*/
unSystemIntegrity_synchronizedTimeDateOfPlc(string sPlcDp, bool bQuantum, dyn_string &exceptionInfo)
{
dyn_int param;
int iS, iH, iM, iD, iMM, iY, iWeekDay, iRes;
long qUTC;
string temp;

	qUTC = (long)getCurrentTime();
	iRes=ctrlTimeUtils_parseUtcTime(qUTC, iY, iMM, iD, iWeekDay, iH, iM, iS);
	
	// PVSS starts from Monday but PLC from Sunday
	iWeekDay = iWeekDay + 1;
	if(iWeekDay > 7)
		iWeekDay = 1;

	if(bQuantum) {
		param[1] = iS*256;
		param[2] = iH*256+iM;
		param[3] = iMM*256+iD;
		iY -=2000;
		param[4] = iY;
		param[5] = iWeekDay;
	}
	else {
		sprintf(temp, "%d00", iS);
		sscanf(temp, "%x", param[1]);
		sprintf(temp, "%2d%2d", iH, iM);
		strreplace(temp, " ", "0");
		sscanf(temp, "%x", param[2]);
		sprintf(temp, "%2d%2d", iMM, iD);
		strreplace(temp, " ", "0");
		sscanf(temp, "%x", param[3]);
		sprintf(temp, "%d", iY);
		sscanf(temp, "%x", param[4]);
		sprintf(temp, "00%d", iWeekDay);
		sscanf(temp, "%x", param[5]);
	}

	unSystemIntegrity_sendCmdToPLC(sPlcDp, c_unSystemIntegrity_PLCSynchonizedTimeDate, param, exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unSystemIntegrity_UnPLC_DriverGQThread
/**
Purpose:
Periodically open GQ.


Usage: Public

PVSS manager usage: CTRL (WCCOActrl)

Constraints:
	. PVSS version: 3.0 
	. operating system: Win XP
	. distributed system: yes.
*/
unSystemIntegrity_UnPLC_DriverGQThread()
{
  int i, len;
  int iDrvNum;
  int iWaitingDelay, iNb, iPlcNumber;
  
  while(true)
  {
    iWaitingDelay = g_unSystemIntegrity_PLCCheckingDelay;
    if(iWaitingDelay <= 0)
      iWaitingDelay = c_unSystemIntegrity_defaultPLCCheckingDelay;
    delay(iWaitingDelay);
    unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_UnPLC_DriverGQThread", iNb, g_unSystemIntegrity_UnPLCGQThreadNbCheck);
    if((g_unSystemIntegrity_UnPLCGQThreadNbCheck>0) && (iNb >= g_unSystemIntegrity_UnPLCGQThreadNbCheck))
    {
      iNb = 0;
      synchronized(g_m_unSystemIntegrity_UnPLC_Started)
      {
        len=mappinglen(g_m_unSystemIntegrity_UnPLC_Started);
        for(i=1;i<=len;i++)
        {
          iPlcNumber = mappingGetKey(g_m_unSystemIntegrity_UnPLC_Started, i);
          iDrvNum = mappingGetValue(g_m_unSystemIntegrity_UnPLC_Started, i);
          dpSet("_Driver" + iDrvNum + ".SM:_original.._value", 1);
          dpSet("_Driver" + iDrvNum + ".GQ:_original.._value", iPlcNumber+256);
          unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "UnPLC systemIntegrity", "unSystemIntegrity_UnPLC_DriverGQThread on PLC", iPlcNumber, iDrvNum);
        }
      }
    }
    if(g_unSystemIntegrity_UnPLCGQThreadNbCheck>0)
      iNb++;
    else
      iNb = 0;
  }
}

//@}
