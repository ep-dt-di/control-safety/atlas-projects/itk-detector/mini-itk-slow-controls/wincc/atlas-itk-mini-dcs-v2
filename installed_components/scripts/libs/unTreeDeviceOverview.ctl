#uses "unEventList/unEventList.ctl"
#uses "ctrlEventList"
#uses "CtrlTreeDeviceOverview"
#uses "unFilterDpFunctions.ctl"

/**@name LIBRARY: unTreeDeviceOverview.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 09/10/2002

Modification History: 
  - 11/05/2012: Cyril
  IS-684 Added application filter management.
  
  - 14/04/2012: Herve
  IS-742  unCore - General  DeviceTreeOverview snapshot mode: on rigth click on the table one can do also: 
  hide/view each fieald of the table, use as default, save config, load config/ re-order column
 
  - 11/11/2011: Herve
  IS-606  unCore - unGraphicalFrame  HMI interface: set system state to unknown for newly added remote system 
 
  15/06/2009: Herve
  - DisplayAlias: font smaller (7 instead of 8) --> allow 21 chars
  modified function unTreeDeviceOverview_getWidthSize
  
  11/05/2009: Herve
    - bug fix: removeSymbol in PVSS 3.8
    modified function: unTreeDeviceOverview_removedDisplayedDevices
    
  20/04/2009: Herve
    - unTreeDeviceOverview_WidgetInit: replace lock.emf by lock.xpm
    
  18/02/2009: Herve
    - Front-end widget implementation: rigth click on a front-end node
    modified function: unTreeDeviceOverview_EventNodeRigthClick, unTreeDeviceOverview_initializeEventConfiguration
      
  16/02/2009: Herve
    - Bug PVSS 3.8: removeSymbol that was not added
    unTreeDeviceOverview_removedDisplayedDevices: test if index == 0 
      
  21/01/2009: Herve
    - tree device widget
    - modify tree device overview to use the device tree widget
    - bug fix: unTreeDeviceOverview_getDPEObjectList DPE = set "-" instead of "" if no object list DPE is defined for a device type
    
  23/10/2008: Herve
    - if remote system not open do not open the diagnostic panel.
    
  30/09/2008: Herve
    - synchronized the update/read of global var
    
  23/09/2008: Herve
    - bug: if widget device is modified and click on page, snapshot/widget the widget cannot be redraw
    in some cases (never draw). one must click on the tree. fix error in log.
    
  30/06/2008: Herve
    - bug childPanel: 
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel
      . replace isPanelOpen by unGraphicalFrame_isPanelOpen
      . replace PanelOffPanel by unGraphicalFrame_PanelOffPanel

  28/04/2008: Herve
    - extended diag message
    - new function to show loading text.
    
  18/04/2008: Herve
    - use addSymbol instead of RootPanel..
    - bug 3.1 with addSymbol/removeSymbol fixed
    
  15/04/2008: Herve
    - remove rootOnModule and replace it by dpSet
    - check if panel open before closing it.
    
  19/12/2007: Herve
    - update to Qt
    
  18/12/2007: Herve
    - new functions to get Domain/Nature and FE config from file
    
  09/10/2007: Herve
    - two displayAlias panel
    
  29/09/2007: Herve
    - replace g_m_bTreeDeviceOverviewSystemForceSelectSort by g_m_bSystemForceSelectSort
    
  26/09/2007: Herve
    - file name configurable when reading file
    
  19/09/2007: Herve
    - optimization display: only show top node and load when expand/click
    - show text in activeX based on system
    - show progress bar on node to add in the activeX
    
  10/09/2007: Herve
    - unTreeDeviceOverview_getDeviceList: get data from file or get from PVSS
    
  04/07/2007: Herve
    - add bit filter for the snapshot
    
  26/06/2007: Herve
    - new function unTreeDeviceOverview_filterChanged

  26/04/2006: Herve
    - in unTreeDeviceOverview_WidgetInit if sDpType is "" do nothing
    
  23/04/2007: Herve
    - new function: unTreeDeviceOverview_WidgetInit
    - replace Devive by Device
    
  27/02/2007: Herve
    - dll for getDeviceList
    - bug with addSymbol/remove too many times, use a Canvas and panel on module (like windowTree)
    
  08/02/2007: Herve
    - new display
    - use dpSelector function
    - remove un-used function
    - comment out function of the dll:
        unTreeDeviceOverview_checkAlias
        unTreeDeviceOverview_createSubsystemList
    
  02/11/2007: Herve
    - missing two arguments in the function unTreeDeviceOverview_checkGroupAndState: dsCurrentSubsystem1 and dsCurrentSubsystem2
    - unTreeDeviceOverview_getDeviceWidgetList: remove the dp if it does not have a widget
    
  23/11/2006: Herve
    - unTreeDeviceOverview_getChildrenDeviceList: bug in case dev type use and then removed in filter the list of device does not correspond to the one shown un widget
    
  31/10/2006: Herve
    - replace dpGetAlias by unGenericDpFunctions_getAlias
    - remove global variable from dll function --> future dev.
    
  19/10/2006: Herve
    - add double click event and open faceplate
    - do not add any devices before at least one click on the apply filter button
    
  12/04/2006: Herve
    - busy panel (like event list) when click on shapshot/widget and page and click.

  03/04/2006: Herve
    - add widget/snapshot choice, new functions:
      unTreeDeviceOverview_getDeviceListData: Get the data from the call of the ObjectList function 
      unTreeDeviceOverview_getDeviceState: Get the state of the device 
      unTreeDeviceOverview_displayList: Display the list of device data in a table 
      unTreeDeviceOverview_getConfiguration: Get the the configuration of the StsReg01(02) for an Unicos device type 
    
  23/03/2006: Herve
    - re-organisation
    
  17/03/2006: Herve
    - bug: calculation of the number of device wrong in case of domain, nature selected
    - remove node which has no devices
    - filtering on alias
    
  14/01/2006: Herve
    - implementation of device in more than one domain and nature
    - implementation of list of subsystem1 and subsystem2
    - implementation of list: system name, front-end, application, device type
    
  02/09/2005: Herve
    - set lvlfilter according to $param
    
  31/08/2005: Herve
    - allow front-end APPLICATION as topNode and without front-end: unTreeDeviceOverview_addDeviceList modified
    
  29/08/2005: Herve
    - bug addTopNode in case of filter on devicetype only: include the systemname
    
  26/08/2005: Herve
    - internal trigger DP based on module name: so 2 different dev. overview with different settings
    - unTreeDeviceOverview_EventNodeLeftClick: disable pages buttons
    - unTreeDeviceOverview_initialize: reset listPages, beginDisplay_graph, endDisplay_graph, displayList_graph, displayInfo1
    lvl filter and selection graphical elements

version 1.1

External Functions: 

Internal Functions: 

Purpose: 
This library contains the functions to be used with the tree device overview panel.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . data point type needed: _UnTreeDevOv
  . to be used with the unTreeWidget and the Microsoft ProgressBar Control, version 6.0
  . the following graphical elements must exists when some functions are called:
    - SelectionList: connectedSystemsCombobox, filteredDevicesCountLabel, allFrontEndTypes, displayList_graph, Overview_dsWidgetList_graph, 
      selectedDeviceTypes, dsOpenedDiagnosticPanels, dsExcludedDeviceTypes
    - TextField: lvl1selection, lvl2selection, lvl3selection, lvl4selection, lvl5selection, lvl6selection, 
    beginDisplay_graph, endDisplay_graph, widthMax_graph, widthAlias_graph, heightAlias_graph, 
    lines_graph, columns_graph, b32SelectState_graph
    - Text: lvl1filter, lvl2filter, lvl3filter, lvl4filter, lvl5filter, lvl6filter
    - Button: applyFilter
    - CheckBox: filter
    - ComboBox: listPages
    - unTreeWidget (UNICOS tree unTreeWidget): treeDevice.tree
    - Microsoft ProgressBar Control, version 6.0 (Microsoft): treeDevice.treeBar
  . global variables must exist in a library or the general section of the panel
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/

//@{

//--------------------------------------------------------------------------------------------------------------------------------
//unTreeDeviceOverview_Connection
/**
Purpose: Check if the bTriggerConnected must be set to true/false.

Parameters:
  - sIdentifier: string, input, device identifier
  - sDpType, string, input, object
  - exceptionInfo: dyn_string, output, details of any exceptions are returned here

Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 3.1 
  . operating system: WXP.
  . PVSS manager type: UI (WCCOAui)
  . distributed system: yes.
*/
int unTreeDeviceOverview_Connection(string sIdentifier, bool bConnected, int iCommand, string sSystemName, bool &bTriggerConnected)
{
  string sCurrentSystemName = unGenericDpFunctions_getSystemName(sIdentifier);

//DebugN("start !!!!", iCommand, g_bSystemConnected);
  if((iCommand == DEVUN_APPLYFILTER_COMMAND) && (g_bSystemConnected))
    return UN_ACTION_NOTHING;
    
  if(mappingHasKey(g_m_bTreeDeviceOverview_deviceListAvailable, sCurrentSystemName)) {
    if(g_m_bTreeDeviceOverview_deviceListAvailable[sCurrentSystemName]) {
      bTriggerConnected = bConnected;
      if((sCurrentSystemName == sSystemName) && (iCommand == DEVUN_DELETE_COMMAND))
        bTriggerConnected = false;
    }
//DebugN("unTreeDeviceOverview_Connection", sIdentifier, sCurrentSystemName, g_m_bTreeDeviceOverview_deviceListAvailable[sCurrentSystemName], bConnected, iCommand, sSystemName, bTriggerConnected);
  }
/*
  else
DebugN("unTreeDeviceOverview_Connection", sIdentifier, sCurrentSystemName, "no Key", bConnected, iCommand, sSystemName, bTriggerConnected);
*/

  return UN_ACTION_DPCONNECT;
}

//--------------------------------------------------------------------------------------------------------------------------------
//unTreeDeviceOverview_WidgetInit
/**
Purpose: initialize widget via the treeDeviceOverview trigger, same function as unGenericObject_WidgetInit 
but it is synchronised with the update of all the global variables used in the treeDeviceOverview.

Parameters:
  - sIdentifier: string, input, device identifier
  - sDpType, string, input, object
  - exceptionInfo: dyn_string, output, details of any exceptions are returned here

Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 3.1 
  . operating system: WXP.
  . PVSS manager type: UI (WCCOAui)
  . distributed system: yes.
*/
unTreeDeviceOverview_WidgetInit(string sIdentifier, string sDpType, string sFunction, dyn_string &exceptionInfo)
{
  string deviceSystemName, sDpPanel, sPanelName;
  bool bConnected, bInitOk = true;
  dyn_string exceptionInfoTemp, dsFunctions;
  string sTreeDevOvDp=unTreeDeviceOverview_getInternalTriggerName();
  
//DebugN("unTreeDeviceOverview_WidgetInit", sFunction, sIdentifier, sTreeDevOvDp, dpExists(sTreeDevOvDp));

  if(shapeExists("LockBmp")) 
    setMultiValue("LockBmp", "visible", false, "LockBmp", "fill", "[pattern,[tile,xmp,unWidgets/lock.xpm]]");  // Load picture

  if(dpExists(sTreeDevOvDp)) {
    if (strltrim(sIdentifier) != "")
    {
      deviceSystemName = unGenericDpFunctions_getSystemName(sIdentifier);
      unTreeDeviceOverview_distributedRegister(sFunction, bInitOk, bConnected, deviceSystemName, sTreeDevOvDp, exceptionInfo);
    }
    else
      bInitOk = false;
  }
  else
      bInitOk = false;

  if (!bInitOk)
  {
    if(sDpType != "") {
      unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
      execScript("main(string sWidgetType) {" + dsFunctions[UN_GENERICOBJECT_FUNCTION_WIDGETDISCONNECT] + "(sWidgetType);}",
             makeDynString(), g_sWidgetType);
    }
    unGraphicalFrame_getPanel(myModuleName(), sDpPanel, sPanelName);
    unSendMessage_toAllUsers(g_sWidgetType + getCatStr("unOperator","UNKNOWNDOLLARPARAM") + " in panel " + sPanelName, exceptionInfoTemp);
    if (dynlen(exceptionInfoTemp) > 0)
      fwExceptionHandling_display(exceptionInfoTemp);

    fwException_raise(exceptionInfo, "EXPERTINFO", "$sIdentifier" + getCatStr("unPVSS","UNKNOWNDOLLARPARAM") + " or register failed", "");
    unSendMessage_toExpertException("unGenericObject_WidgetInit", exceptionInfo);
  }
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_distributedRegister
/**
Purpose:
In case of error, bResult is set to false and an exception is raised in exceptionInfo.

The call back function must be like:
CBFunc(string sDp, bool bConnected, string sTreeDp, int iCommand, string sTreeDpSystem, string sSystemName)
{
}
bConnected: true (the remote system is connected and synchronised)/false (the remote system is not connected)

it is recommended to do nothing if the bConnected is true. it is better to have another dpConnect function to other  
data. When a remote system is reconnected, any callback function to dp of the remot system will be called after the 
initialisation of the remote connection. Therefore the CBFunc will be called first and then any other call back 
function for the remote dp.

  @param sAdviseFunction: string, input, the name of the call back function
  @param bRes: bool, output, the result of the register call
  @param bConnected: bool, output, is the remote system connected
  @param sSystemName: string, input, the name of remote PVSS system
  @param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL), UI (WCCOAui)

Constraints:
  . constant: 
    . c_unDistributedControl_dpElementName: the data point element of the _unDistributedControl
  . data point type needed: _UnDistributedControl
  . data point: an instance of _UnDistributedControl: _unDistributedControl_xxx where xxx = system name 
  . PVSS version: 3.0 
  . operating system: Linux, NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_distributedRegister(string sAdviseFunction, bool &bRes, bool &bConnected, string sSystemName, string sTreeDevOvDp, dyn_string &exceptionInfo)
{
  string sDistributedControlDpName;
  int iRes;
    
  bRes = false;
  bConnected = false;

  if(!dpExists(sTreeDevOvDp)) {
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unDistributedControl", "LIBREGERR"),"");
    return;
  }
    
  if(sAdviseFunction == "") {
    fwException_raise(exceptionInfo, "ERROR", getCatStr("unDistributedControl", "LIBREGERR"),"");
    return;
  }
  sDistributedControlDpName = "_unDistributedControl_"+substr(sSystemName, 0, strpos(sSystemName, ":"));
  if(dpExists(sDistributedControlDpName)) {
    dynAppend(g_dsTreeDeviceOverview_triggerDisplayDP, sTreeDevOvDp);
    dynUnique(g_dsTreeDeviceOverview_triggerDisplayDP);

    iRes = dpConnect(sAdviseFunction, sDistributedControlDpName+c_unDistributedControl_dpElementName,
                                                    sTreeDevOvDp+".command", sTreeDevOvDp+".systemName");
    if(iRes == -1) {
      fwException_raise(exceptionInfo, 
        "ERROR", getCatStr("unDistributedControl", "LIBREGDPCONERR") + sAdviseFunction +", "+sDistributedControlDpName,"");
    }
    else {
      bRes = true;
      dpGet(sDistributedControlDpName+c_unDistributedControl_dpElementName, bConnected);
    }
  }
  else
    fwException_raise(exceptionInfo, 
        "ERROR", getCatStr("unDistributedControl", "LIBREGNODP") + sDistributedControlDpName,"");
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_initialize
/**
Purpose:
Initialize the tree device overview graphical components

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_initialize()
{
  int i;
  dyn_int diAliasDim;

  listPages.enabled(false);
  listPages.deleteAllItems();
  beginDisplay_graph.text = 0; 
  endDisplay_graph.text = 0; 
  displayList_graph.deleteAllItems();
  modeTab.enabled(false);
  
  for(i=1;i<=FILTER_MAX;i++)
  {
    setMultiValue("lvl"+i+"selection", "text", "");
  }

  // 4. Other...
  beginDisplay_graph.text = 0;
  endDisplay_graph.text = 0;
  // get DisplayAliasLarge dimension
  diAliasDim = getPanelSize("objects/UN_INFOS/unDisplayAliasLarge.pnl");
  if (dynlen(diAliasDim) >= 2)
  {
    widthAliasLarge_graph.text = diAliasDim[1];
  }
  else
  {
    widthAliasLarge_graph.text = 180;
  }
  // get DisplayAlias dimension
  diAliasDim = getPanelSize("objects/UN_INFOS/unDisplayAlias.pnl");
  if (dynlen(diAliasDim) >= 2)
  {
    widthAliasSmall_graph.text = diAliasDim[1];
    widthAlias_graph.text = diAliasDim[1];
    heightAlias_graph.text = diAliasDim[2];
  }
  else
  {
    widthAliasSmall_graph.text = 125;
    widthAlias_graph.text = 125;
    heightAlias_graph.text = 20;
  }
  if(modeTab.activeRegister() == 0)
  {
    listPages.enabled(true);
  }
  modeTab.enabled(true);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getInternalTriggerName
/**
Purpose:
get the internal trigger name

  @param return: string, the name of the trigger

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
string unTreeDeviceOverview_getInternalTriggerName()
{
  string sTriggerName;
  dyn_string dsSplit=strsplit(myModuleName(), ";");
  
  if(dynlen(dsSplit)<1)
    dsSplit[1] = "";
  return _unTreeDeviceOverview_getInternalTriggerName(dsSplit[1]);
}

//--------------------------------------------------------------------------------------------------------------------------------

// _unTreeDeviceOverview_getInternalTriggerName
/**
Purpose:
get the internal trigger name

  @param return: string, the name of the trigger

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
string _unTreeDeviceOverview_getInternalTriggerName(string sModule)
{
  string sTriggerName;

  sTriggerName = DEVUN_DPPREFIX+sModule+"_"+myManNum();
  return sTriggerName;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_triggerDisplay
/**
Purpose:
Trigger the exceution of the callback handling the the tree device unTreeWidget display

  @param iCommand: int, input:  the command to execute.
  @param sSystemName: string, input: the system name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_triggerDisplay(int iCommand, string sSystemName)
{
  string sTreeDevOvDp=unTreeDeviceOverview_getInternalTriggerName();
  
  _unTreeDeviceOverview_triggerDisplay(sTreeDevOvDp, iCommand, sSystemName);
}

//--------------------------------------------------------------------------------------------------------------------------------

// _unTreeDeviceOverview_triggerDisplay
/**
Purpose:
Trigger the exceution of the callback handling the the tree device unTreeWidget display

  @param iCommand: int, input:  the command to execute.
  @param sSystemName: string, input: the system name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
_unTreeDeviceOverview_triggerDisplay(string sTreeDevOvDp, int iCommand, string sSystemName)
{
//DebugN("!!!!!!!!!", sTreeDevOvDp, iCommand, sSystemName);
  dpSet(sTreeDevOvDp+".command", iCommand, sTreeDevOvDp+".systemName", sSystemName);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_displayCallBack
/**
Purpose:
callback handling the the tree device unTreeWidget display
  - DEVUN_ADD_COMMAND: add the data of a given system name
  - DEVUN_DELETE_COMMAND: delete the data of a given system name
  - DEVUN_APPLYFILTER_COMMAND: apply the new filter setting
  
  @param sCommandDp: string, input: the command DP
  @param iCommand: int, input: the command to execute.
  @param sB32Dp: string, input: the filter DP
  @param b32SelectState: b32, input: the filter setup.
  @param sSystemNameDp: string, input: the system DP
  @param sSystemName: string, input: the system name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_displayCallBack(string sCommandDp, int iCommand, string sSystemNameDp, string sSystemName)
{
//DebugTN("unTreeDeviceOverview_displayCallBack start");
  listPages.enabled(false);
  modeTab.enabled(false);

  unTreeDeviceOverview_displayCallBackTreeDeviceWidget(sCommandDp, iCommand, sSystemNameDp, sSystemName);
  if(modeTab.activeRegister() == 0)
  {
    listPages.enabled(true);
  }
  
  modeTab.enabled(true);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getDeviceList
/**
Purpose:
Get the list of all device and its configuration
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_getDeviceList(string sSystemName, dyn_string dsDeviceType, string sFileNameSuffix = "_TreeDeviceOverview_deviceList.dat")
{
  dyn_dyn_string ddsTemp;

//DebugTN("*** start unTreeDeviceOverview_getDeviceList", sSystemName);
  g_m_bTreeDeviceOverview_deviceListAvailable[sSystemName] = false;
  _unTreeDeviceOverview_getDeviceListFomFile(sSystemName, ddsTemp, sFileNameSuffix);
  g_m_ddsTreeDeviceOverviewDeviceList[sSystemName] = ddsTemp;
  g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName] = true;
  g_m_bTreeDeviceOverview_deviceListAvailable[sSystemName] = true;
//DebugTN("*** end unTreeDeviceOverview_getDeviceList", sSystemName, dynlen(g_m_ddsTreeDeviceOverviewDeviceList[sSystemName]));
}

// _unTreeDeviceOverview_getDeviceListFomFile
/**
Purpose:
Get the list of all device and its configuration from a file
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
_unTreeDeviceOverview_getDeviceListFomFile(string sSystemName, dyn_dyn_string &ddsDeviceList, string sFileNameSuffix = "_TreeDeviceOverview_deviceList.dat")
{
  string sFileName;
  string sBuffer;
  string sFileSystemName = sSystemName;
  file fFile;
  dyn_string dsSplit;
  dyn_dyn_string ddsTemp;
  int i, len, i2, len2;
  
  strreplace(sFileSystemName, ":", "");
  sFileName = getPath(DATA_REL_PATH)+"_"+sFileSystemName+sFileNameSuffix;
//DebugTN("*** start _unTreeDeviceOverview_getDeviceListFomFile", sSystemName, sFileName);
  fileToString(sFileName, sBuffer);
  dsSplit = strsplit(sBuffer, "\n");
  len = dynlen(dsSplit);
  for(i=1;i<=len;i++)
  {
    dynAppend(ddsTemp, strsplit(dsSplit[i], ";"));
  }
  if(!unTreeDeviceOverview_checkDeviceListSize(ddsTemp)) {
    ddsTemp[TREE_DEVICE_DPNAME] = makeDynString();
    ddsTemp[TREE_DEVICE_ALIAS] = makeDynString();
    ddsTemp[TREE_DEVICE_FE] = makeDynString();
    ddsTemp[TREE_DEVICE_FEDPNAME] = makeDynString();
    ddsTemp[TREE_DEVICE_FEDPTYPE] = makeDynString();
    ddsTemp[TREE_DEVICE_FEAPPLICATION] = makeDynString();
    ddsTemp[TREE_DEVICE_TYPE] = makeDynString();
    ddsTemp[TREE_DEVICE_SUBSYSTEM1] = makeDynString();
    ddsTemp[TREE_DEVICE_SUBSYSTEM2] = makeDynString();
  }
  ddsDeviceList = ddsTemp;
//DebugTN("*** end _unTreeDeviceOverview_getDeviceListFomFile", sSystemName, dynlen(ddsTemp));
}

// unTreeDeviceOverview_checkDeviceListSize
/**
Purpose:
Check the size of the different dyn_string if the deviceList
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
bool unTreeDeviceOverview_checkDeviceListSize(dyn_dyn_string ddsDeviceList)
{
  bool bResult = false;

  if(dynlen(ddsDeviceList) == 9) {
    if((dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_ALIAS])) && 
        (dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_FE])) &&
        (dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_FEDPNAME])) &&
        (dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_FEDPTYPE])) &&
        (dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_FEAPPLICATION])) &&
        (dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_TYPE])) &&
        (dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_SUBSYSTEM1])) &&
        (dynlen(ddsDeviceList[TREE_DEVICE_DPNAME]) == dynlen(ddsDeviceList[TREE_DEVICE_SUBSYSTEM2])))
          bResult = true;
  }
  return bResult;
}

//--------------------------------------------------------------------------------------------------------------------------------

// _unTreeDeviceOverview_getDeviceList
/**
Purpose:
Get the list of all device and its configuration
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
_unTreeDeviceOverview_getDeviceList(string sSystemName, dyn_string dsDeviceType, dyn_dyn_string &ddsDeviceDetail)
{
  dyn_string dsGrpName;
  int i, len;
  bool bRemote;
  string sDpNamePattern="*-*-*-*-*";
  string sFrontEnd, sFEApplication, sDeviceType, sDomain, sNature, sDeviceName, sFEDpName, sFEDPType;
  int iDeviceNumber;
  dyn_string dsDpNames, exceptionInfo;
  dyn_string dsFE, dsAppl, dsDevType, dsSubsystem1, dsSubsystem2, dsAlias, dsFEDpName, dsFEDPType;
  
//DebugTN("******** start", sSystemName);  
  if(sSystemName != getSystemName())
    bRemote = true;
  // get the list of all devices
//DebugTN("start query", sSystemName);  
  _unFilterDpFunctions_dpSelector(sSystemName,bRemote,dsGrpName,dsDeviceType,sDpNamePattern,dsDpNames,exceptionInfo);

//  len = dynlen(dsDpNames);
//DebugTN("end query", sSystemName, len);

  // build the list of detail for all devices
    CtrlTreeDeviceOverview_getDeviceList(sSystemName, getSystemId(sSystemName), dsDpNames, dsAlias, 
                              dsFE, dsFEDpName, dsFEDPType, 
                              dsAppl, dsDevType, 
                              dsSubsystem1, dsSubsystem2);
  len = dynlen(dsDpNames);
  for(i=1;i<=len;i++) {
// get FE characteristics
    if(dsFEDPType[i] == "") {
      sFEDpName = unGenericDpFunctions_dpAliasToName(sSystemName+dsFE[i]);

      if(dpExists(sFEDpName)) 
        sFEDPType = dpTypeName(sFEDpName);
      else
        sFEDPType=NOT_DP;
      dsFEDpName[i] = sFEDpName;
      dsFEDPType[i] = sFEDPType;
//DebugTN("_unTreeDeviceOverview_getDeviceList empty FE", sFEDpName, sFEDPType);
    }
  }

//DebugTN("end for",sSystemName, dynlen(dsDpNames));
  ddsDeviceDetail[TREE_DEVICE_DPNAME] = dsDpNames;
  ddsDeviceDetail[TREE_DEVICE_ALIAS] = dsAlias;
  ddsDeviceDetail[TREE_DEVICE_FE] = dsFE;
  ddsDeviceDetail[TREE_DEVICE_FEDPNAME] = dsFEDpName;
  ddsDeviceDetail[TREE_DEVICE_FEDPTYPE] = dsFEDPType;
  ddsDeviceDetail[TREE_DEVICE_FEAPPLICATION] = dsAppl;
  ddsDeviceDetail[TREE_DEVICE_TYPE] = dsDevType;
  ddsDeviceDetail[TREE_DEVICE_SUBSYSTEM1] = dsSubsystem1;
  ddsDeviceDetail[TREE_DEVICE_SUBSYSTEM2] = dsSubsystem2;
  
//DebugTN("******* end",sSystemName, dynlen(dsDpNames));
}

//--------------------------------------------------------------------------------------------------------------------------------

// _unTreeDeviceOverview_buildSubsystemList
/**
Purpose:
Build the list of subsystem1 and subsystem2
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
_unTreeDeviceOverview_buildSubsystemList()
{
  int len, i;
  string sSystem, sTemp;
  dyn_string dsSubsystem1, dsSubsystem2, dsTemp, dsPVSSSystem, dsFrontEnd, dsApplication;
  
  len = mappinglen(g_m_dsTreeSubsystem1);
  for(i=1;i<=len;i++) {
    sTemp = mappingGetValue(g_m_sTreePVSSSystem, i);
    if(sTemp != "")
      dynAppend(dsPVSSSystem, sTemp);
    dsTemp = mappingGetValue(g_m_dsTreeFrontEnd, i);
    dynAppend(dsFrontEnd, dsTemp);
    dsTemp = mappingGetValue(g_m_dsTreeApplication, i);
    dynAppend(dsApplication, dsTemp);
    dsTemp = mappingGetValue(g_m_dsTreeSubsystem1, i);
    dynAppend(dsSubsystem1, dsTemp);
    dsTemp = mappingGetValue(g_m_dsTreeSubsystem2, i);
    dynAppend(dsSubsystem2, dsTemp);
  }
  dynInsertAt(dsPVSSSystem, "*", 1);
  dynAppend(dsPVSSSystem, UN_EVENT_LIST_LIST_NAME);
  dynInsertAt(dsFrontEnd, "*", 1);
  dynAppend(dsFrontEnd, UN_EVENT_LIST_LIST_NAME);
  dynInsertAt(dsApplication, "*", 1);
  dynAppend(dsApplication, UN_EVENT_LIST_LIST_NAME);
  dynInsertAt(dsSubsystem1, "*", 1);
  dynAppend(dsSubsystem1, ""); // for empty subsystem1
  dynAppend(dsSubsystem1, UN_EVENT_LIST_LIST_NAME);
  dynInsertAt(dsSubsystem2, "*", 1);
  dynAppend(dsSubsystem2, ""); // for empty subsystem2
  dynAppend(dsSubsystem2, UN_EVENT_LIST_LIST_NAME);

  dynUnique(dsPVSSSystem);
  dynUnique(dsFrontEnd);
  dynUnique(dsApplication);
  dynUnique(dsSubsystem1);
  dynUnique(dsSubsystem2);

  g_dsTreePVSSSystem = dsPVSSSystem;
  g_dsTreeFrontEnd = dsFrontEnd;
  g_dsTreeApplication = dsApplication;
  g_dsTreeSubsystem1 = dsSubsystem1;
  g_dsTreeSubsystem2 = dsSubsystem2;
  
//DebugN("unTreeDeviceOverview_buildSubsystemList", dynlen(dsSubsystem1), dynlen(dsSubsystem2));
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_buildSubsystemList
/**
Purpose:
Build the list of subsystem1 and subsystem2
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_buildSubsystemList()
{
  // Load system list
  systemList.items = g_dsTreePVSSSystem;

  // Load front-end list
  dyn_string dsTreeFrontEnd = g_dsTreeFrontEnd;
  
  
  // Hide front-end of unwanted applications
  if (g_unicosHMI_bApplicationFilterInUse)
  {
    dyn_string dsTreeFrontEndTemp;
    
    synchronized (g_unicosHMI_dsApplicationList)
    {
      for (int i = 1 ; i <= dynlen(dsTreeFrontEnd) ; i++)
      {
        if (unApplicationFilter_isFrontEndVisible(dsTreeFrontEnd[i]))
        {
          dynAppend(dsTreeFrontEndTemp, dsTreeFrontEnd[i]);
        }
      }
    }   
    
    dsTreeFrontEnd = dsTreeFrontEndTemp;
    
    dynInsertAt(dsTreeFrontEnd, UN_OBJECT_LIST_FILTER_ALL, 1);
    dynAppend(dsTreeFrontEnd, UN_OBJECT_LIST_LIST_NAME);
  }
  
  frontEndList.items = dsTreeFrontEnd;

  // Load front-end application list  
  dyn_string dsTreeApplication = g_dsTreeApplication;
  
  // Hide unwanted applications
  if (g_unicosHMI_bApplicationFilterInUse)
  {
    synchronized (g_unicosHMI_dsApplicationList)
    {
      dsTreeApplication = dynIntersect(dsTreeApplication, g_unicosHMI_dsApplicationList);
      dynInsertAt(dsTreeApplication, UN_OBJECT_LIST_FILTER_ALL, 1);
      dynAppend(dsTreeApplication, UN_OBJECT_LIST_LIST_NAME);
    }    
  } 
  
  applicationList.items = dsTreeApplication;

  // Load sub-system list
  subsystem1List.items = g_dsTreeSubsystem1;
  subsystem2List.items = g_dsTreeSubsystem2;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_applyFilter
/**
Purpose:
Apply the new filter: erase everything and add all the system connected
  
  @param shTreeShape: shape, input: the unTreeWidget shape
  @param b32SelectState: b32, input: the filter setup.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_applyFilter(shape shTreeShape, bit32 b32SelectState, bit32 b32SortByState, string sRegisteredSystem, bool bRegisterSystem)
{
  int i, len;
  bool bSelect, bSortBy, bTree;
  string sSystemName;
  mapping m;
  
  bSelect = unTreeDeviceOverview_checkSelectState(b32SelectState);
  bSortBy = unTreeDeviceOverview_checkSortByState(b32SortByState);
  
  if(bSelect) // if new filter selection --> force tree update
  {
    bSortBy = true;
  }
  
  b32SelectState_graph.text = b32SelectState;
  b32SortByState_graph.text = b32SortByState;

  if(bSortBy)
  {
    bTree = true;
  }
  
  // system add or delete      
  if(bRegisterSystem) 
  { 
    // force tree redraw
    bSortBy = true;
    bTree = true;
  }
  
  if(bTree) // clear the tree if new display
  {
    unTreeWidget_setPropertyValue(shTreeShape, "clearTree", true, true);
  }
  
  if(bSortBy) 
  {
    // add the devices of the connected systems
    g_m_ddsNodeDevice = m;
    
    // stop all thread
    len = mappinglen(g_m_bTreeDeviceOverviewSystemConnected);
    unProgressBar_setMax(TREEBAR_NAME, len);
    
    for(i = 1 ; i <= len ; i++) 
    {
      sSystemName = mappingGetKey(g_m_bTreeDeviceOverviewSystemConnected, i);
      unTreeWidget_showText("Select/Sort devices fom " + sSystemName, "Please wait ... " + i + "/" + len);
      
      // wait to let a change to show the text
      delay(0,10);
      unProgressBar_setPosition(TREEBAR_NAME, i);
      
      if(mappingGetValue(g_m_bTreeDeviceOverviewSystemConnected, i)) 
      {
        g_m_dsNodeInTree[sSystemName] = makeDynString();
        g_m_dbTopNode[sSystemName] = makeDynBool();
        
        if(g_m_bSystemForceSelectSort[sSystemName]) 
        {
          // force select and sort
          g_m_bSystemForceSelectSort[sSystemName] = false;
          unTreeDeviceOverview_selectSortDeviceList(
              b32SelectState, 
              b32SortByState, 
              sSystemName, 
              true, 
              bSortBy,
              g_m_dsNodeInTree[sSystemName], 
              g_m_dbTopNode[sSystemName]
            );
        }
        else 
        {
          unTreeDeviceOverview_selectSortDeviceList(
              b32SelectState, 
              b32SortByState, 
              sSystemName, 
              bSelect, 
              bSortBy,
              g_m_dsNodeInTree[sSystemName], 
              g_m_dbTopNode[sSystemName]
            );
        }
      }
    }
    unProgressBar_setPosition(TREEBAR_NAME, 0);
    
    // keep the display state
    unTreeDeviceOverview_setSortByState(b32SortByState);
  }
  
  if(bTree) 
  {
    unTreeDeviceOverview_addNodeInTree(shTreeShape, g_m_bTreeDeviceOverviewSystemConnected, g_m_dsNodeInTree, g_m_dbTopNode);
  }
  
  if(bSelect) // keep the filter state
  {
    unTreeDeviceOverview_setSelectState(b32SelectState);
  }
  
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getSelectedDeviceList
/**
Purpose:
Get the list of device and its configuration based on the filter selection criteria
  
  @param shTreeShape: shape, input: the unTreeWidget shape
  @param b32SelectState: b32, input: the filter setup.
  @param sSystemName: string, input: the systemName.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_getSelectedDeviceList(bit32 b32SelectState, string sSystemName, dyn_dyn_string ddsDeviceList, dyn_dyn_string &ddsSelectedDevice)
{  
//  bool bPVSSSystem, bFrontEnd, bApplication, bSubsystem1, bSubsystem2, bDeviceType, bAlias;
//  int i, len;
  dyn_string dsFE, dsAppl, dsDevType, dsSubsystem1, dsSubsystem2, dsAlias, dsDpName, dsFEDpName, dsFEDPType;
  dyn_string dsRetFE, dsRetAppl, dsRetDevType, dsRetSubsystem1, dsRetSubsystem2, dsRetAlias, dsRetDpName, dsRetFEDpName, dsRetFEDPType;
//  bool bAdd;
  dyn_string dsDeviceType = selectedDeviceTypes.items;
//  dyn_string dsTemp;
//  dyn_string dsIntersectSubsystem1, dsIntersectSubsystem2;

//DebugTN("start getSelectedDeviceList", sSystemName, dynlen(ddsDeviceList));
  if(dynlen(ddsDeviceList) <= 0)
    return;
    
  dsDpName = ddsDeviceList[TREE_DEVICE_DPNAME];
  dsAlias = ddsDeviceList[TREE_DEVICE_ALIAS];
  dsFE = ddsDeviceList[TREE_DEVICE_FE];
  dsFEDpName = ddsDeviceList[TREE_DEVICE_FEDPNAME];
  dsFEDPType = ddsDeviceList[TREE_DEVICE_FEDPTYPE];
  dsAppl = ddsDeviceList[TREE_DEVICE_FEAPPLICATION];
  dsDevType = ddsDeviceList[TREE_DEVICE_TYPE];
  dsSubsystem1 = ddsDeviceList[TREE_DEVICE_SUBSYSTEM1];
  dsSubsystem2 = ddsDeviceList[TREE_DEVICE_SUBSYSTEM2];

  CtrlTreeDeviceOverview_getSelectedDeviceList(b32SelectState, sSystemName, 
                                                g_m_dsSelectedItems[FILTER_PVSS_SYSTEM], 
                                                g_m_dsSelectedItems[FILTER_FRONT_END], 
                                                g_m_dsSelectedItems[FILTER_APPLICATION],
                                                dsDeviceType, 
                                                g_m_dsSelectedItems[FILTER_SUBSYSTEM1], 
                                                g_m_dsSelectedItems[FILTER_SUBSYSTEM2],
                                                g_m_dsSelectedItems[FILTER_ALIAS], 
                                                g_m_dsSelectedItems[FILTER_ALIAS_EXCLUDED],
                                                dsDpName, dsAlias, dsFE, dsFEDpName, dsFEDPType, dsAppl, dsDevType,
                                                dsSubsystem1, dsSubsystem2, dsRetDpName, dsRetAlias, dsRetFE,
                                                dsRetFEDpName, dsRetFEDPType, dsRetAppl, dsRetDevType,
                                                dsRetSubsystem1, dsRetSubsystem2);

  ddsSelectedDevice[TREE_DEVICE_DPNAME] = dsRetDpName;
  ddsSelectedDevice[TREE_DEVICE_ALIAS] = dsRetAlias;
  ddsSelectedDevice[TREE_DEVICE_FE] = dsRetFE;
  ddsSelectedDevice[TREE_DEVICE_FEDPNAME] = dsRetFEDpName;
  ddsSelectedDevice[TREE_DEVICE_FEDPTYPE] = dsRetFEDPType;
  ddsSelectedDevice[TREE_DEVICE_FEAPPLICATION] = dsRetAppl;
  ddsSelectedDevice[TREE_DEVICE_TYPE] = dsRetDevType;
  ddsSelectedDevice[TREE_DEVICE_SUBSYSTEM1] = dsRetSubsystem1;
  ddsSelectedDevice[TREE_DEVICE_SUBSYSTEM2] = dsRetSubsystem2;
//DebugTN("end getSelectedDeviceList", dynlen(dsRetDpName), sSystemName);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_selectSortDeviceList
/**
Purpose:
Select and/or sort the device list
  
  @param shTreeShape: shape, input: the unTreeWidget shape
  @param b32SelectState: b32, input: the filter setup.
  @param sSystemName: string, input: the systemName.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_selectSortDeviceList(bit32 b32SelectState, bit32 b32SortByState, string sSystemName, bool bSelect, bool bSortBy, 
                                    dyn_string &dsNodeInTree, dyn_bool &dbTopNode)
{  
  int iTotalDevice;
  mapping mEmpty;
  
//DebugTN("start selectSortDeviceList", sSystemName);

  if(bSelect){
    unTreeWidget_showText("Selecting devices fom "+sSystemName, "Please wait ...");
//delay(0, 10);
    unTreeDeviceOverview_getSelectedDeviceList(b32SelectState, sSystemName, g_m_ddsTreeDeviceOverviewDeviceList[sSystemName], g_m_ddsSelectedDevice[sSystemName]);
  }

  if(bSortBy) {
    unTreeWidget_showText("Sorting devices fom "+sSystemName, "Please wait ...");
//delay(0, 10);
    unTreeDeviceOverview_sortSelectedDeviceList(b32SortByState, sSystemName, g_m_ddsSelectedDevice[sSystemName], dsNodeInTree, dbTopNode, g_m_ddsNodeDevice, "", "");
    iTotalDevice = dynlen(g_m_ddsSelectedDevice[sSystemName][1]);
  }
// keep here the number of devices found=total dev - excluded - clipboard.
  g_m_iDevicePerSystem[sSystemName] = iTotalDevice;
//DebugTN("End selectSortDeviceList", sSystemName, iTotalDevice);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_checkSelectState
/**
Purpose:
Check if the filter was modified.
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
bool unTreeDeviceOverview_checkSelectState(bit32 b32SelectState)
{
  bool bSelect;

  if(g_b32OldSelectState != b32SelectState) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_PVSS_SYSTEM] != g_m_dsSelectedItems[FILTER_PVSS_SYSTEM]) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_FRONT_END] != g_m_dsSelectedItems[FILTER_FRONT_END]) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_APPLICATION] != g_m_dsSelectedItems[FILTER_APPLICATION]) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_SUBSYSTEM1] != g_m_dsSelectedItems[FILTER_SUBSYSTEM1]) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_SUBSYSTEM2] != g_m_dsSelectedItems[FILTER_SUBSYSTEM2]) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_ALIAS] != g_m_dsSelectedItems[FILTER_ALIAS]) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_ALIAS_EXCLUDED] != g_m_dsSelectedItems[FILTER_ALIAS_EXCLUDED]) {
    bSelect = true;
  }
  else if(g_m_dsPreviousSelectedItems[FILTER_DEVICETYPE] != selectedDeviceTypes.items) {
    bSelect = true;
  }
  return bSelect;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_setSelectState
/**
Purpose:
Keep the current filter setting
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_setSelectState(bit32 b32SelectState)
{
  g_b32OldSelectState = b32SelectState;
  g_m_dsPreviousSelectedItems[FILTER_PVSS_SYSTEM] = g_m_dsSelectedItems[FILTER_PVSS_SYSTEM];
  g_m_dsPreviousSelectedItems[FILTER_FRONT_END] = g_m_dsSelectedItems[FILTER_FRONT_END];
  g_m_dsPreviousSelectedItems[FILTER_APPLICATION] = g_m_dsSelectedItems[FILTER_APPLICATION];
  g_m_dsPreviousSelectedItems[FILTER_SUBSYSTEM1] = g_m_dsSelectedItems[FILTER_SUBSYSTEM1];
  g_m_dsPreviousSelectedItems[FILTER_SUBSYSTEM2] = g_m_dsSelectedItems[FILTER_SUBSYSTEM2];
  g_m_dsPreviousSelectedItems[FILTER_ALIAS] = g_m_dsSelectedItems[FILTER_ALIAS];
  g_m_dsPreviousSelectedItems[FILTER_ALIAS_EXCLUDED] = g_m_dsSelectedItems[FILTER_ALIAS_EXCLUDED];
  g_m_dsPreviousSelectedItems[FILTER_DEVICETYPE] = selectedDeviceTypes.items;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_checkSortByState
/**
Purpose:
Check the sort by state
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
bool unTreeDeviceOverview_checkSortByState(bit32 b32SortByState)
{
  bool bSortBy;
  if(g_b32OldSortByState != b32SortByState) {
    bSortBy = true;
  }
  return bSortBy;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_setSortByState
/**
Purpose:
Keep the sort by state
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_setSortByState(bit32 b32SortByState)
{
  g_b32OldSortByState = b32SortByState;
}



//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_sortSelectedDeviceList
/**
Purpose:
Sort the list of selected device.
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_sortSelectedDeviceList(bit32 b32SortByState, string sSystemName, dyn_dyn_string ddsDeviceList, dyn_string &dsNodeInTree, dyn_bool &dbTopNode, mapping &m_ddsNodeDevice, string sSelectedKey, string sSelectedLabel)
{
  bool bFrontEnd, bApplication, bSubsystem1, bSubsystem2, bDeviceType, bPVSSSystem;
  dyn_string dsFE, dsAppl, dsDevType, dsDevSubsystem1, dsDevSubsystem2, dsSubsystem1, dsSubsystem2, dsAlias, dsDpName, dsFEDpName, dsFEDpType;
  string sTemp;
  bool bTop, bTopSubsystem1, bTopSubsystem2;
  int i, len, j, lenj, k, lenk;
  string sNewLabel, sNewKey, sKey;
  string sParentKey, sParentLabel, sParentKey1, sParentLabel1, sParentKey2, sParentLabel2;
  bool bTopSystem;
  string sParentSystemLabel, sParentSystemKey;
  dyn_dyn_string ddsNodeDevice, ddsDpNameNodeDevice, ddsAlias, ddsFE,
                ddsFEDpName, ddsFEDpType, ddsAppl, ddsDevType, ddsDevSubsystem1, ddsDevSubsystem2;
  dyn_string dsKey;
  bit32 b32 = b32SortByState;
  bool bKey;
  
/*
    // add topNode: class~key where key is |labelOfTheNode;dpNameOfTheNode
      aShape.addTopNode(class+"~"+key);


  // add Node: class~key where key is parentLabel;dpParent|childLabel;dpChild@parentKey
        aShape.addNode(class+"~"+key);

*/

//DebugTN("start sortSelectedDeviceList", sSystemName, dynlen(ddsDeviceList));
  if(dynlen(ddsDeviceList) <= 0)
    return;
    
  dsDpName = ddsDeviceList[TREE_DEVICE_DPNAME];
  dsAlias = ddsDeviceList[TREE_DEVICE_ALIAS];
  dsFE = ddsDeviceList[TREE_DEVICE_FE];
  dsFEDpName = ddsDeviceList[TREE_DEVICE_FEDPNAME];
  dsFEDpType = ddsDeviceList[TREE_DEVICE_FEDPTYPE];
  dsAppl = ddsDeviceList[TREE_DEVICE_FEAPPLICATION];
  dsDevType = ddsDeviceList[TREE_DEVICE_TYPE];
  dsDevSubsystem1 = ddsDeviceList[TREE_DEVICE_SUBSYSTEM1];
  dsDevSubsystem2 = ddsDeviceList[TREE_DEVICE_SUBSYSTEM2];

  if(sSelectedKey != "")
    bKey = true;
//DebugN("Lib: ", bKey, sSelectedKey, dynlen(dsDpName),dynlen(dsNodeInTree));
//DebugN(dynlen(dsDpName), dynlen(dsAlias), dynlen(dsFE), dynlen(dsFEDpName), dynlen(dsFEDpType), dynlen(dsAppl), dynlen(dsDevType), dynlen(dsDevSubsystem1), dynlen(dsDevSubsystem2));
  CtrlTreeDeviceOverview_sortSelectedDeviceList(b32SortByState, sSystemName, dsDpName, dsAlias, dsFE,
                                                  dsFEDpName, dsFEDpType, dsAppl, dsDevType, dsDevSubsystem1, dsDevSubsystem2,
                                                  dsNodeInTree, dbTopNode, 
                                                  dsKey, ddsDpNameNodeDevice, ddsAlias, ddsFE,
                                                  ddsFEDpName, ddsFEDpType, ddsAppl, ddsDevType, ddsDevSubsystem1, ddsDevSubsystem2, bKey, sSelectedKey, sSelectedLabel);

//DebugN("re end:", dynlen(dsNodeInTree));
//DebugN(dynlen(dsKey), dsKey[1], dynlen(ddsNodeDevice[1]));
  if(getBit(b32SortByState, FILTER_PVSS_SYSTEM))
    setBit(b32SortByState, FILTER_PVSS_SYSTEM, 0);
  else if(getBit(b32SortByState, FILTER_FRONT_END))
    setBit(b32SortByState, FILTER_FRONT_END, 0);
  else if(getBit(b32SortByState, FILTER_APPLICATION))
    setBit(b32SortByState, FILTER_APPLICATION, 0);
  else if(getBit(b32SortByState, FILTER_DEVICETYPE))
    setBit(b32SortByState, FILTER_DEVICETYPE, 0);
  else if(getBit(b32SortByState, FILTER_SUBSYSTEM1))
    setBit(b32SortByState, FILTER_SUBSYSTEM1, 0);
  else if(getBit(b32SortByState, FILTER_SUBSYSTEM2))
    setBit(b32SortByState, FILTER_SUBSYSTEM2, 0);

  len = dynlen(dsKey);
  for(i=1;i<=len;i++) {
    dsDpName = ddsDpNameNodeDevice[i];
    dsAlias = ddsAlias[i];
    dsFE = ddsFE[i];
    dsFEDpName = ddsFEDpName[i];
    dsFEDpType = ddsFEDpType[i];
    dsAppl = ddsAppl[i];
    dsDevType = ddsDevType[i];
    dsDevSubsystem1 = ddsDevSubsystem1[i];
    dsDevSubsystem2 = ddsDevSubsystem2[i];
    if(mappingHasKey(m_ddsNodeDevice, dsKey[i])) {
      ddsNodeDevice = m_ddsNodeDevice[dsKey[i]];
      dynAppend(ddsNodeDevice[TREE_DEVICE_DPNAME], dsDpName);
      dynAppend(ddsNodeDevice[TREE_DEVICE_ALIAS], dsAlias);
      dynAppend(ddsNodeDevice[TREE_DEVICE_FE], dsFE);
      dynAppend(ddsNodeDevice[TREE_DEVICE_FEDPNAME], dsFEDpName);
      dynAppend(ddsNodeDevice[TREE_DEVICE_FEDPTYPE], dsFEDpType);
      dynAppend(ddsNodeDevice[TREE_DEVICE_FEAPPLICATION], dsAppl);
      dynAppend(ddsNodeDevice[TREE_DEVICE_TYPE], dsDevType);
      dynAppend(ddsNodeDevice[TREE_DEVICE_SUBSYSTEM1], dsDevSubsystem1);
      dynAppend(ddsNodeDevice[TREE_DEVICE_SUBSYSTEM2], dsDevSubsystem2);
    }
    else {
      ddsNodeDevice[TREE_DEVICE_DPNAME] = dsDpName;
      ddsNodeDevice[TREE_DEVICE_ALIAS] = dsAlias;
      ddsNodeDevice[TREE_DEVICE_FE] = dsFE;
      ddsNodeDevice[TREE_DEVICE_FEDPNAME] = dsFEDpName;
      ddsNodeDevice[TREE_DEVICE_FEDPTYPE] = dsFEDpType;
      ddsNodeDevice[TREE_DEVICE_FEAPPLICATION] = dsAppl;
      ddsNodeDevice[TREE_DEVICE_TYPE] = dsDevType;
      ddsNodeDevice[TREE_DEVICE_SUBSYSTEM1] = dsDevSubsystem1;
      ddsNodeDevice[TREE_DEVICE_SUBSYSTEM2] = dsDevSubsystem2;
    }
    ddsNodeDevice[TREE_DEVICE_SORTSTATE][1] = b32SortByState;
    m_ddsNodeDevice[dsKey[i]] = ddsNodeDevice;
  }

/*
for(i=1;i<=mappinglen(m_ddsNodeDevice);i++) {
ddsNodeDevice = mappingGetValue(m_ddsNodeDevice, i);
DebugN(i, mappingGetKey(m_ddsNodeDevice, i), dynlen(ddsNodeDevice[TREE_DEVICE_DPNAME]), dynlen(ddsNodeDevice[TREE_DEVICE_ALIAS]), 
          dynlen(ddsNodeDevice[TREE_DEVICE_FE]),dynlen(ddsNodeDevice[TREE_DEVICE_FEDPNAME]),
          dynlen(ddsNodeDevice[TREE_DEVICE_FEDPTYPE]),dynlen(ddsNodeDevice[TREE_DEVICE_FEAPPLICATION]),
          dynlen(ddsNodeDevice[TREE_DEVICE_TYPE]),dynlen(ddsNodeDevice[TREE_DEVICE_SUBSYSTEM1]),
          dynlen(ddsNodeDevice[TREE_DEVICE_SUBSYSTEM2]), ddsNodeDevice[TREE_DEVICE_SORTSTATE]);
}
*/
//DebugTN("end sortSelectedDeviceList", dynlen(dsKey), sSystemName, mappinglen(g_m_ddsNodeDevice));
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_addNodeInTree
/**
Purpose:
Add the node in the unTreeWidget
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_addNodeInTree(shape shTree, mapping mbSystemConnected, mapping mdsNodeToAdd, mapping mdbTopNodeDevice)
{
  int i, len=mappinglen(mbSystemConnected);
  int iNode, lenNode;
  dyn_string dsNodeAdded;
  string sSystemName;
  
//DebugTN("start addNodeInTree", mbSystemConnected);
  for(i=1;i<=len;i++) {
    sSystemName = mappingGetKey(mbSystemConnected, i);
    if(mbSystemConnected[sSystemName]) {
      lenNode = dynlen(mdsNodeToAdd[sSystemName]);
      if(lenNode > 0)
        unProgressBar_setMax(TREEBAR_NAME, lenNode);
//DebugN(sSystemName, "lenNode: ", lenNode);
      for(iNode=1; iNode<=lenNode;iNode++) {
        unTreeWidget_showText("Loading devices from "+sSystemName, "Please wait ... adding "+iNode+"/"+lenNode);
        unProgressBar_setPosition(TREEBAR_NAME, iNode);
        if(dynContains(dsNodeAdded, mdsNodeToAdd[sSystemName][iNode]) <=0) { // node not added --> add it
          dynAppend(dsNodeAdded, mdsNodeToAdd[sSystemName][iNode]);
          if(mdbTopNodeDevice[sSystemName][iNode]) 
            unTreeWidget_setPropertyValue(shTree, "addTopNode", mdsNodeToAdd[sSystemName][iNode], true);
          else 
            unTreeWidget_setPropertyValue(shTree, "addNode", mdsNodeToAdd[sSystemName][iNode], true);
        }
//        else DebugN("node already added: ", mdsNodeToAdd[sSystemName][iNode], mdbTopNodeDevice[sSystemName][iNode]);
      }
    }
  }
  unProgressBar_setPosition(TREEBAR_NAME, 0);
//DebugTN("end addNodeInTree", dynlen(dsNodeAdded));
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getApplication
/**
Purpose:
Get the application list of the given front-end
  
  @param sSystemName: string, input: the systemName.
  @param sFrontEndName: string, input: the front-end device name.
  @param dsApplication: dyn_string, output: the list of application.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_getApplication(string sSystemName, string sFrontEndName, dyn_string &dsApplication)
{
  dyn_string dsSubApplications;
  string sUnPlc;
  int pos;
  
  if (sFrontEndName != "")
  {
// convert the device name to DP
    sUnPlc = unGenericDpFunctions_dpAliasToName(sSystemName+sFrontEndName);
    if (sUnPlc != "")
    {
      dpGet(sUnPlc + "configuration.subApplications", dsSubApplications);
      pos = dynContains(dsSubApplications, "COMMUNICATION");
      if(pos > 0)
        dynRemove(dsSubApplications, pos);
    }
  }
  dsApplication = dsSubApplications;
}



//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getFrontEndName
/**
Purpose:
Get all the front-end of the configured front-end (done int the initialize function) of the given system
  
  @param sSystemName: string, input: the systemName.
  @param dsFrontEnd: dyn_string, output: the list of front-end device name.
  @param dsFrontEndDp: dyn_string, output: the list of front-end DP name.
  @param dsReturnFrontEndType: dyn_string, output: the list of front-end DP type.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_getFrontEndName(string sSystemName, dyn_string &dsFrontEnd, dyn_string &dsFrontEndDp, dyn_string &dsReturnFrontEndType)
{
  dyn_string dsFrontEndType = allFrontEndTypes.items;

  _unTreeDeviceOverview_getFrontEndName(dsFrontEndType, sSystemName, dsFrontEnd, dsFrontEndDp, dsReturnFrontEndType);
}
//--------------------------------------------------------------------------------------------------------------------------------

// _unTreeDeviceOverview_getFrontEndName
/**
Purpose:
Get all the front-end of the configured front-end (done int the initialize function) of the given system
  
  @param dsFrontEndType: dyn_string, input: the list of front-end device type.
  @param sSystemName: string, input: the systemName.
  @param dsFrontEnd: dyn_string, output: the list of front-end device name.
  @param dsFrontEndDp: dyn_string, output: the list of front-end DP name.
  @param dsReturnFrontEndType: dyn_string, output: the list of front-end DP type.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
_unTreeDeviceOverview_getFrontEndName(dyn_string dsFrontEndType, string sSystemName, dyn_string &dsFrontEnd, dyn_string &dsFrontEndDp, dyn_string &dsReturnFrontEndType)
{
  dyn_string dsTempFrontEnd;
  int i, len, j, lenj;
  string sFrontEndAlias;
  
  len = dynlen(dsFrontEndType);
  for(i=1; i<=len; i++) {
    dsTempFrontEnd = dpNames(sSystemName+"*", dsFrontEndType[i]);
//DebugN(dsTempFrontEnd, dsFrontEndType[i]);
    lenj=dynlen(dsTempFrontEnd);
    for(j=1;j<=lenj;j++) {
      sFrontEndAlias = unGenericDpFunctions_getAlias(dsTempFrontEnd[j]);
// remove all the front-ends that are not of UNICOS standard
      if(sFrontEndAlias != dsTempFrontEnd[j]) {
        dynAppend(dsFrontEnd, sFrontEndAlias);
        dynAppend(dsFrontEndDp, dsTempFrontEnd[j]);
        dynAppend(dsReturnFrontEndType, dsFrontEndType[i]);
      }
    }
  }
//DebugN("inFi", dsFrontEnd, dsFrontEndDp, dsFrontEndType);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_removedDisplayedDevices
/**
Purpose:
Remove all the added symbol devices in the device overview

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_removedDisplayedDevices()
{
  int i, length;
  int beginDisplay = beginDisplay_graph.text; 
  int endDisplay = endDisplay_graph.text; 
  string sShape;
  
  for(i=endDisplay;i>=beginDisplay;i--)
  {
    sShape = ALIAS_REFERENCE + i;
  
    if(shapeExists(sShape+".objectAlias"))
    {
      removeSymbol(myModuleName(), myPanelName(), WIDGET_REFERENCE + i);
      removeSymbol(myModuleName(), myPanelName(), ALIAS_REFERENCE + i);
    }
  }
//  if(unGraphicalFrame_isPanelOpen(TREE_DEVICE_WIDGET, myModuleName()+TREE_DEVICE_WIDGET))
//    PanelOffModule(TREE_DEVICE_WIDGET, myModuleName()+TREE_DEVICE_WIDGET);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_displayDevices
/**
Purpose:
Show the symbol devices in the device overview
  
  @param newBeginDisplay: string, input: start position in the list of devices.
  @param newEndDisplay: dyn_string, input: end position in the list of devices.
  @param dsWidgetFileNameList: dyn_string, input: the list of widget panel file name.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_displayDevices(int newBeginDisplay, int newEndDisplay, dyn_string dsWidgetFileNameList)
{
  int i;
  string sWidget, sAlias;
  dyn_string dsDollar;
  int positionX, positionY, iTop, iLeft, idTop, idLeft;
  dyn_int diWidgetDim;
  float aliasXscale;
  dyn_string displayList = displayList_graph.items;
  int beginDisplay = beginDisplay_graph.text;
  int endDisplay = endDisplay_graph.text; 
  int widthMax = widthMax_graph.text;
  int heightMax = heightMax_graph.text;
  int widthAlias = widthAlias_graph.text;
  int heightAlias = heightAlias_graph.text;
  int leftSpacing = DEVUN_MIN_LEFT;
  int topSpacing = DEVUN_MIN_TOP;
  int columns=columns_graph.text;
  int widthAliasLarge = widthAliasLarge_graph.text;
  int iPerc;
        
  string sAliasList;
  string sWidgetList;
  
  unTreeDeviceOverview_displayLoadingText("Loading devices data", "Please wait ...");
//DebugTN("start unTreeDeviceOverview_displayDevices", newBeginDisplay, beginDisplay, newEndDisplay, endDisplay, dsWidgetFileNameList);
  if ((newBeginDisplay != beginDisplay) || (newEndDisplay != endDisplay))
  {
// remove the current devices symbol and keep the new value for future use
    unTreeDeviceOverview_removedDisplayedDevices();
    beginDisplay = newBeginDisplay;
    beginDisplay_graph.text = beginDisplay; 

    endDisplay = newEndDisplay;
    endDisplay_graph.text = endDisplay; 

    positionX = 1;
    positionY = 1;
    for(i=beginDisplay;i<=endDisplay;i++)
    {
                        iPerc = 100*(1+i-beginDisplay)/(1+endDisplay-beginDisplay);
      unTreeDeviceOverview_displayLoadingText("Loading devices data "+(1+i-beginDisplay)+"/"+(1+endDisplay-beginDisplay), "Please wait ... ", iPerc);
// get the device from its position and get its configured widget
      unGenericDpFunctions_getDeviceWidget(displayList[i], sWidget, dsWidgetFileNameList);
//      if(sWidgetList == "")
//        sWidgetList = sWidget;
//      else
//        sWidgetList += ";"+sWidget;

// get the device name
      sAlias = unGenericDpFunctions_getAlias(displayList[i]);
//      if(sAliasList == "")
//        sAliasList = displayList[i];
//      else
//        sAliasList += ";"+displayList[i];  
      dsDollar = makeDynString("$sIdentifier:" + unGenericDpFunctions_getSystemName(displayList[i]) + sAlias);
                        
// calculate the position, based on the previous added symbol, size of the widget, size of the lock,
// available place in the device overview
      iLeft = leftSpacing + widthMax * (positionX - 1);
      iTop = topSpacing + (heightMax + heightAlias) * (positionY - 1);
                        if(sWidget != "")
          diWidgetDim = getPanelSize(sWidget);
                        else
                          diWidgetDim = makeDynInt();
      if (dynlen(diWidgetDim) >= 2)
      {
        idLeft = (widthMax - diWidgetDim[1]) / 2 + DELTA_LOCKBMP_WIDTH / 2;
        idTop = (heightMax - (diWidgetDim[2] - 2*WIDGET_HEIGHT_SPACING)) / 2 - WIDGET_HEIGHT_SPACING + DELTA_LOCKBMP_HEIGHT;
      }
      else
      {
        idTop = 0;
        idLeft = 0;
      }

      aliasXscale = widthMax;
      aliasXscale = aliasXscale / widthAlias;
      if(sWidget != "") 
	  {
        addSymbol(myModuleName(), myPanelName(), sWidget, WIDGET_REFERENCE + i, dsDollar, iLeft + idLeft, iTop + idTop, 0, 1, 1);
      }

// add the alias large or small
            if(widthAlias == widthAliasLarge)
        addSymbol(myModuleName(), myPanelName(), "objects/UN_INFOS/unDisplayAliasLarge.pnl", ALIAS_REFERENCE + i, dsDollar, iLeft, iTop + heightMax, 0, aliasXscale, 1);
            else
        addSymbol(myModuleName(), myPanelName(), "objects/UN_INFOS/unDisplayAlias.pnl", ALIAS_REFERENCE + i, dsDollar, iLeft, iTop + heightMax, 0, aliasXscale, 1);
//DebugN(WIDGET_REFERENCE + i);
      setValue(ALIAS_REFERENCE + i + ".objectAlias", "text", sAlias);
      positionX++;
      if (positionX > columns)
      {
        positionX = 1;
        positionY++;
      }
    }
  }

//  if(sAliasList != "") {
//DebugTN("Open panel:"/*, sAliasList*/);
//    dsDollar = makeDynString("$widthMax:"+widthMax, "$widthAliasLarge:"+widthAliasLarge, "$heightMax:"+heightMax,
//                      "$widthAlias:"+widthAlias, "$heightAlias:"+heightAlias, "$columns:"+columns, 
//                      "$sAliasList:"+sAliasList, "$sWidgetList:"+sWidgetList);
//    dpSet("_Ui_"+myManNum()+".RootPanelOrigOn.ModuleName", myModuleName()+TREE_DEVICE_WIDGET,
//          "_Ui_"+myManNum()+".RootPanelOrigOn.FileName", TREE_DEVICE_WIDGET_PANEL,
//          "_Ui_"+myManNum()+".RootPanelOrigOn.PanelName", TREE_DEVICE_WIDGET,
//          "_Ui_"+myManNum()+".RootPanelOrigOn.Parameter", dsDollar);
//                RootPanelOnModule(TREE_DEVICE_WIDGET_PANEL, TREE_DEVICE_WIDGET, myModuleName()+TREE_DEVICE_WIDGET, dsDollar);
//  }
//DebugTN("end unTreeDeviceOverview_displayDevices:"/*, sAliasList*/);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_showDeviceWidget
/**
Purpose:
Show the symbol devices in the device widget panel
  
Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_showDeviceWidget()
{
  int i;
  string sAlias;
  dyn_string dsDollar;
  int positionX, positionY, iTop, iLeft, idTop, idLeft;
  dyn_int diWidgetDim;
  float aliasXscale;
  int widthMax = (int) $widthMax;
  int heightMax = (int) $heightMax;
  int widthAlias = (int) $widthAlias;
  int heightAlias = (int) $heightAlias;
  int columns=(int) $columns;
  int widthAliasLarge = (int) $widthAliasLarge;

  dyn_string dsAlias=strsplit($sAliasList, ";"), dsWidget=strsplit($sWidgetList, ";");

//DebugN("panel_displayDevices", $sAliasList);
//DebugN("panel_displayWidget", $sWidgetList);

  positionX = 1;
  positionY = 1;
  for(i=1;i<=dynlen(dsWidget);i++)
  {
// get the device name
    sAlias = unGenericDpFunctions_getAlias(dsAlias[i]);
    dsDollar = makeDynString("$sIdentifier:" + unGenericDpFunctions_getSystemName(dsAlias[i]) + sAlias);

// calculate the position, based on the previous added symbol, size of the widget, size of the lock,
// available place in the device overview
    iLeft = widthMax * (positionX - 1);
    iTop = (heightMax + heightAlias) * (positionY - 1);
    diWidgetDim = getPanelSize(dsWidget[i]);
    if (dynlen(diWidgetDim) >= 2)
    {
      idLeft = (widthMax - diWidgetDim[1]) / 2 + DELTA_LOCKBMP_WIDTH / 2;
      idTop = (heightMax - (diWidgetDim[2] - 2*WIDGET_HEIGHT_SPACING)) / 2 - WIDGET_HEIGHT_SPACING + DELTA_LOCKBMP_HEIGHT;
    }
    else
    {
      idTop = 0;
      idLeft = 0;
    }

    aliasXscale = widthMax;
    aliasXscale = aliasXscale / widthAlias;
//DebugN(widthMax, iLeft, idLeft, iTop, idTop, heightMax, aliasXscale,diWidgetDim[1], DELTA_LOCKBMP_WIDTH, (iLeft + idLeft)/widthMax);
    if(dsWidget[i] != "") {
      addSymbol(myModuleName(), myPanelName(), dsWidget[i], WIDGET_REFERENCE + i, dsDollar, iLeft + idLeft, iTop + idTop, 0, 1, 1);
    }

// add the alias large or small
    if(widthAlias == widthAliasLarge)
      addSymbol(myModuleName(), myPanelName(), "objects/UN_INFOS/unDisplayAliasLarge.pnl", ALIAS_REFERENCE + i, dsDollar, iLeft, iTop + heightMax, 0, aliasXscale, 1);
    else
      addSymbol(myModuleName(), myPanelName(), "objects/UN_INFOS/unDisplayAlias.pnl", ALIAS_REFERENCE + i, dsDollar, iLeft, iTop + heightMax, 0, aliasXscale, 1);
//DebugN(WIDGET_REFERENCE + i);
    setValue(ALIAS_REFERENCE + i + ".objectAlias", "text", sAlias);
    positionX++;
    if (positionX > columns)
    {
      positionX = 1;
      positionY++;
    }
  }
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_EventNodeLeftClick
/**
Purpose:
Function called called when a left click on a node of the unTreeWidget is done.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_EventNodeLeftClick(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
  unTreeDeviceOverview_ExpandOrNodeLeftClick(DEVUN_TREEDISPLAY_NODELEFTCLICK, sSelectedKey, sMode, sCompositeLabel, sGraphTree, sBar, bClipboard);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unTreeDeviceOverview_EventExpand
/**
Purpose:
Function called called when a left click on a node of the unTreeWidget is done.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_EventExpand(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard)
{
//DebugTN("start unTreeDeviceOverview_EventExpand:");
  unTreeDeviceOverview_ExpandOrNodeLeftClick(DEVUN_TREEDISPLAY_EXPAND, sSelectedKey, sMode, sCompositeLabel, sGraphTree, sBar, bClipboard);
//DebugTN("end unTreeDeviceOverview_EventExpand:");
}

//--------------------------------------------------------------------------------------------------------------------------------
// unTreeDeviceOverview_ExpandOrNodeLeftClick
/**
Purpose:
Function called when a left click or expand on a node of the unTreeWidget is done.

  @param sTriggerCommand: string, input: trigger command value
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_ExpandOrNodeLeftClick(string sTriggerCommand, string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard)
{
  string sChildrenCompositeLabelOfSelectedNode = unTreeWidget_getPropertyValue("tree", "getFirstChildrenOfNode", false, sSelectedKey);
  dyn_string dsChidren;
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sParentLabel;
  
// get first node name, if DEVUN_TREEDISPLAY_UNKNOWN_NODE assume never expanded
  if(sChildrenCompositeLabelOfSelectedNode == DEVUN_TREEDISPLAY_UNKNOWN_NODE) {
    unTreeWidget_setPropertyValue("tree", "deleteNodeWithNoEvent", DEVUN_TREEDISPLAY_UNKNOWN_NODE+unTreeWidget_keySeparator+sSelectedKey);
  }  
  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
  sParentLabel = sDisplayNodeName+unTreeWidget_labelSeparator+sDpName;

  selectedKey.text = sSelectedKey;
  selectedLabel.text = sParentLabel;
  treeTriggerCommand.text = sTriggerCommand;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_DoubleClick
/**
Purpose:
Function called called when a click on a node of the unTreeWidget is done.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_DoubleClick(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard)
{
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName;
  dyn_string dsUnicosObjects, exInfo;
  
  if((sMode == "doubleClick") && (sCompositeLabel != "")) {
    unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
    if(dpExists(sDpName)) {
      unGenericDpFunctions_getUnicosObjects(dsUnicosObjects);
      if(dynContains(dsUnicosObjects, dpTypeName(sDpName)) > 0) {
        unGenericObject_OpenFaceplate(sDpName, exInfo);
      }
    }
  }
}
//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getDeviceWidgetList
/**
Purpose:
Get the list of the widget of all the configured device type (configured in the initialize function), if a device name does not have
a widget, it is removed from dsDeviceDpName
  
  @param dsDeviceDpName: dyn_string, input/output: list of device DP name.
  @param dsDeviceType: dyn_string, input: list of device type.
  @param dsWidgetList: dyn_string, output: the list of widget panel file name to be used.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_getDeviceWidgetList(dyn_string &dsDeviceDpName, dyn_string dsDeviceType, dyn_string &dsWidgetList)
{
  int i, len;
  dyn_string dsTemp, dsTempWidget, dsReturn;
  string sWidget;
  dyn_string dsTempDeviceDp;
    
  len = dynlen(dsDeviceType);
  for(i=1;i<=len;i++) {
    unGenericDpFunctions_getDeviceWidgetList(dsDeviceType[i], dsTemp);
    dynAppend(dsTempWidget, dsTemp);
  }

  len = dynlen(dsDeviceDpName);
  for(i=1;i<=len;i++) {
// get the device from its position and get its configured widget
    unTreeDeviceOverview_displayLoadingText("Loading devices widget configuration "+i+"/"+len, "Please wait ...", 100*i/len);

    unGenericDpFunctions_getDeviceWidget(dsDeviceDpName[i], sWidget, dsTempWidget);
    if(sWidget != "") {
      dynAppend(dsReturn, sWidget);
      dynAppend(dsTempDeviceDp, dsDeviceDpName[i]);
    }
  }
  dynUnique(dsReturn);
  dsWidgetList = dsReturn;
  dsDeviceDpName = dsTempDeviceDp;
//DebugN(dsTempWidget, dsWidgetList);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_initializeDeviceToDisplay
/**
Purpose:
Initialize the device to show and the display variables.
  
  @param dsDeviceDpName: dyn_string, input: the list of device DP name.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_initializeDeviceToDisplay(dyn_string dsDeviceDpName)
{
  int i, iPages, msg1, msg2;
  int beginDisplay; 
  int endDisplay; 
  int lines=lines_graph.text;
  int columns=columns_graph.text;
  
  displayList_graph.items = dsDeviceDpName;
  beginDisplay = 0;
  beginDisplay_graph.text = beginDisplay;
  endDisplay = 0;
  endDisplay_graph.text = endDisplay;
  listPages.deleteAllItems();
  if (dynlen(dsDeviceDpName) > 0)
    {
    iPages = dynlen(dsDeviceDpName) / (lines * columns);
    if (iPages * lines * columns != dynlen(dsDeviceDpName))
      {
      iPages++;
      }
    for(i=1;i<=iPages-1;i++)
      {
      msg1 = (i-1) * lines * columns + 1;
      msg2 = i * lines * columns;
      listPages.appendItem("Device " + msg1 + " to " + msg2);
      }
    msg1 = (iPages-1) * lines * columns + 1;
    msg2 = dynlen(dsDeviceDpName);
    listPages.appendItem("Device " + msg1 + " to " + msg2);
    }
//DebugN("old", dsDeviceDpName);
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_EventNodeRigthClick
/**
Purpose:
Function executed when one does a right click on a node of the Tree. popupMenuXY can be used to show a popup menu. 
WARNING: popuMenu cannot be used.

  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sMode: string, input: the type of mouse click
  @param sCompositeLabel: string, input: the composite label of the node
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param bClipboard: bool, input: true show the clipboard, false don't show the clipboard
  @param iPosX: int, input: x position of the mouse
  @param iPosY: int, input: y position of the mouse

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and 
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_EventNodeRigthClick(string sSelectedKey, string sMode, string sCompositeLabel, string sGraphTree, string sBar, bool bClipboard, int iPosX, int iPosY)
{
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sParentCompositeLabel, sSystemName;
  string sDpType, sPanelFileName;
  dyn_string exInfo;
  int i, len, iAnswer;
  bool bConnected;
  dyn_string dsMenu, exceptionInfo;
  dyn_string dsFrontEndType = allFrontEndTypes.items;
  
  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
  //DebugN("unTreeDeviceOverview_EventNodeRigthClick",sSelectedKey, sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);  
  // if the DP is a front-end DP then ask for diagnostic
  // if not assume that this is a unicos device and call a function similar to unGenericObject_WidgetRightClick.
  if(sDpName != NOT_DP) {
    if(dpExists(sDpName)) {
      // check if of type FrontEnd
      sDpType = dpTypeName(sDpName);
      // else trigger the rigth click of device widget
      unTreeDeviceOverview_DeviceRightClick(sDisplayNodeName, sDpName, sDpType, iPosX, iPosY, exceptionInfo);
    }
  }
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_AddAndCleanDiagnosticPanel
/**
Purpose:
Keep the front-end diagnostic panel per system to be able the close them when the remote system is off (no distributed control 
in the front-end diagnostic panel). Remove from the list the panels that are closed.
  
  @param sDisplayNodeName: string, input: the panel name.
  @param sDpName: string, input: the DP name.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_AddAndCleanDiagnosticPanel(string sDisplayNodeName, string sDpName)
{
  dyn_string dsOpenedDiagnosticPanel = dsOpenedDiagnosticPanels.items;
  int i, len;
  dyn_string dsTemp, dsOpenTemp;
  
// panelName;dpName is saved
//DebugN("Add", dsOpenedDiagnosticPanel, myModuleName());
  len = dynlen(dsOpenedDiagnosticPanel);
  for(i=1;i<=len;i++) {
    dsTemp = strsplit(dsOpenedDiagnosticPanel[i], ";");
//DebugN(dsTemp, myModuleName(), unGraphicalFrame_isPanelOpen(dsTemp[1]), unGraphicalFrame_isPanelOpen(dsTemp[1], myModuleName()));
    if(dynlen(dsTemp) > 1) {
// remove from the list the panels that are closed
      if(unGraphicalFrame_isPanelOpen(dsTemp[1]))
        dynAppend(dsOpenTemp, dsOpenedDiagnosticPanel[i]);
    }
  }
// add the new one
  dynAppend(dsOpenTemp, sDisplayNodeName+";"+sDpName);
  dynUnique(dsOpenTemp);
  dsOpenedDiagnosticPanels.items = dsOpenTemp;
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_DeleteDiagnosticPanel
/**
Purpose:
Keep the front-end diagnostic panel per system to be able the close them when the remote system is off (no distributed control 
in the front-end diagnostic panel). Remove from the list the panels that are closed.
  
  @param sSystemName: string, input: the system name.
  @param sDpName: string, input: the DP name.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_DeleteDiagnosticPanel(string sSystemName)
{
  dyn_string dsOpenedDiagnosticPanel = dsOpenedDiagnosticPanels.items;
  int i, len;
  dyn_string dsTemp, dsOpenTemp;
  string sDevSystemName;

// panelName;dpName 
//DebugN("Delete", dsOpenedDiagnosticPanel);
  len = dynlen(dsOpenedDiagnosticPanel);
  for(i=1;i<=len;i++) {
    dsTemp = strsplit(dsOpenedDiagnosticPanel[i], ";");
    if(dynlen(dsTemp) > 1) {
// check the system name
      sDevSystemName = unGenericDpFunctions_getSystemName(dsTemp[2]);
      if(sDevSystemName == sSystemName) {
// close the opened panel that is opened with DP from the given system name
        if(unGraphicalFrame_isPanelOpen(dsTemp[1])) 
          unGraphicalFrame_PanelOffPanel(dsTemp[1]);
      }
      else
        dynAppend(dsOpenTemp, dsOpenedDiagnosticPanel[i]);
    }
  }
  dsOpenedDiagnosticPanels.items = dsOpenTemp;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_DeviceRightClick
/**
Purpose: 
Right click event on tree device instance, similar to the unGenericObject_WidgetRightClick

Parameters:
  @param sDeviceName: string, input, the device name
  @param sDeviceDpName: string, input, the device data point name
  @param sDeviceType: string, input, the device type
  @param iPosX: string, input, X position of the mouse
  @param iPosY: string, input, Y position of the mouse
  @param exceptionInfo: dyn_string, output, for errors
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_DeviceRightClick(string sDeviceName, string sDeviceDpName, string sDeviceType, int iPosX, int iPosY, dyn_string &exceptionInfo)
{
  dyn_string menuList, dsFunctions, dsAccessOk, exceptionInfoTemp;
  string sFunction, systemName;
  int menuAnswer;
  bool bConnected;

  systemName = unGenericDpFunctions_getSystemName(sDeviceDpName);
  unDistributedControl_isConnected(bConnected, systemName);
  if (bConnected)
  {
// get the access state
    unGenericButtonFunctionsHMI_isAccessAllowedMultiple(sDeviceDpName, sDeviceType, dsAccessOk, exceptionInfo);
// Init
    unGenericObject_GetFunctions(sDeviceType, dsFunctions, exceptionInfo);
    sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_MENUCONFIG];
    if (sFunction != "")
    {
      evalScript(menuList, "dyn_string main(string device, string sType, dyn_string dsAccessOk) {" + 
                    "dyn_string dsMenuTemp;" +
                   "if (isFunctionDefined(\"" + sFunction + "\"))" +
                   "{" +
                   "    " + sFunction + "(device, sType, dsAccessOk, dsMenuTemp);" +
                   "}" +
                   "else " +
                   "{" +
                   "    dsMenuTemp = makeDynString();" +
                   "}" +
                   "return dsMenuTemp; }", makeDynString(), sDeviceDpName, sDeviceType, dsAccessOk);
    }
//DebugN(deviceName, sDpType, menuList);
// 7. Open menu
    menuAnswer = 0;
    if (dynlen(menuList) > 0)
    {
      popupMenuXY(menuList, iPosX, iPosY, menuAnswer);
    }
       
// 8. Actions

    sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_HANDLEMENUANSWER];
    if (sFunction != "")
    {
      evalScript(exceptionInfoTemp, "dyn_string main(string deviceName, string sDpType, dyn_string menuList, int menuAnswer) {" + 
                    "dyn_string exceptionInfo;" +
                   "if (isFunctionDefined(\"" + sFunction + "\"))" +
                   "{" +
                   "    " + sFunction + "(deviceName, sDpType, menuList, menuAnswer);" +
                   "}" +
                   "else " +
                   "{" +
                   "    exceptionInfo = makeDynString();" +
                   "}" +
                   "return makeDynString(); }", makeDynString(), sDeviceDpName, sDeviceType, menuList, menuAnswer);
      dynAppend(exceptionInfo, exceptionInfoTemp);
    }

    if (dynlen(exceptionInfo) > 0)
    {
      unSendMessage_toExpertException("Right Click",exceptionInfo);
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getChildrenDeviceList
/**
Purpose: 
Get the list of children devices when a left click was done on a node of the tree 

Parameters:
  @param sTreeName: string, input, the unTreeWidget name
  @param sSelectedKey: string, input, the key of the selected node
  @param sCompositeLabel: string, input, the compostite label of the selected node
  @param dsDeviceDpName: dyn_string, output, the list of children device DP name of the selected node
  @param dsDeviceName: dyn_string, output, the list of children device name of the selected node
  @param dsDeviceType: dyn_string, output, the list of children device DP type of the selected node
  @param dsParentDeviceDpName: dyn_string, output, the list of parent (parent of the parent of the parent ...) device DP name of the selected node
  @param dsParentDeviceName: dyn_string, output, the list of parent (parent of the parent of the parent ...) device DP name of the selected node
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Use constants UN_POPUPMENU_***
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_getChildrenDeviceList(string sTreeName, string sSelectedKey, string sCompositeLabel, mapping &mDeviceNameAndDp, dyn_string &dsDeviceName, dyn_string &dsDeviceType, dyn_string &dsParentDeviceDpName, dyn_string &dsParentDeviceName)
{
  dyn_string dsChildrenCompositeLabel, dsDpName, dsDisplayNodeName, dsParentDpName, dsParentNodeName, dsParentCompositeLabel;
  int len, i;
  shape aShape = getShape(sTreeName);
  string sChildrenCompositeLabelOfSelectedNode;
  string sAllParentCompositeLabelOfSelectedNode;
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName;
  
  if(sSelectedKey != "") {
    unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
    unTreeDeviceOverview_displayLoadingText("Loading devices data of "+sDisplayNodeName, "Please wait ...");
    sChildrenCompositeLabelOfSelectedNode = unTreeWidget_getPropertyValue(aShape, "getChildrenCompositeLabelOfNode", true, sSelectedKey);
    sAllParentCompositeLabelOfSelectedNode = unTreeWidget_getPropertyValue(aShape, "getAllParentCompositeLabelOfNode", true, sSelectedKey);
    dsChildrenCompositeLabel = strsplit(sChildrenCompositeLabelOfSelectedNode, unTreeWidget_keySeparator);
    dsParentCompositeLabel = strsplit(sAllParentCompositeLabelOfSelectedNode, unTreeWidget_keySeparator);
    len = dynlen(dsChildrenCompositeLabel);
    for(i=1; i<=len; i++) {
      unTreeDeviceOverview_displayLoadingText("Extracting devices configuration "+i+"/"+len, "Please wait ...", 100*i/len);
      unTreeWidget_getNodeNameAndNodeDp(dsChildrenCompositeLabel[i], dsDpName[i], dsDisplayNodeName[i], dsParentDpName[i], dsParentNodeName[i]);
      if(dsDpName[i] != NOT_DP) {
        if(dpExists(dsDpName[i])) {
			mDeviceNameAndDp[dsDisplayNodeName[i]] = dsDpName[i];
            dynAppend(dsDeviceName, dsDisplayNodeName[i]);
            dynAppend(dsDeviceType, dpTypeName(dsDpName[i]));
        }
      }
    }

    dynInsertAt(dsParentCompositeLabel, sCompositeLabel, 1);
    len = dynlen(dsParentCompositeLabel);
    dsDpName = dsDisplayNodeName = dsParentDpName = dsParentNodeName = makeDynString();
    for(i=1; i<=len; i++) {
      unTreeDeviceOverview_displayLoadingText("Extracting configuration from device tree "+i+"/"+len, "Please wait ...", 100*i/len);
      unTreeWidget_getNodeNameAndNodeDp(dsParentCompositeLabel[i], dsDpName[i], dsDisplayNodeName[i], dsParentDpName[i], dsParentNodeName[i]);
      dynAppend(dsParentDeviceDpName, dsDpName[i]);
      dynAppend(dsParentDeviceName, dsDisplayNodeName[i]);
    }
  }
  dynUnique(dsDeviceType);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getDeviceState
/**
Purpose: 
Get the state of the device 

Parameters:
  @param deviceDpName: string, input, the device dp name
  @param sDeviceType: string, input, the device type
  @param diStsRegValues: dyn_int, input, the stsreg dpe value
  @param sBitListName: string, output, the list set bits of the stsreg
  @param bInvalidBit: bool, output, true if no bit sets.
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Use constants UN_POPUPMENU_***
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_getDeviceState(string deviceDpName, string sDeviceType, 
                           dyn_int diStsRegValues, string &sBitListName, bool &bInvalidBit)
{
  bool bEvent=false;
  int iPosition, i, length, iCurrentStsReg, iCurrentBit, iValue;
  string sBitMessage;
  float value;
  string sTime, sValue, sFunction;
  dyn_string dsFunctions, exceptionInfo;
  dyn_anytype dReturnData;
  
  sBitMessage = "";
  iPosition = dynContains(g_dsTreeDeviceOverviewUnicosObjects, sDeviceType);
  if (iPosition > 0)              // Good device type, else skip device
    {
    length = dynlen(g_ddiEventPositions[iPosition]);  // Number of used bit for current object
        
    for(i=1;i<=length;i++)          // For each used bit
      {
      iCurrentStsReg = g_ddiEventPositions[iPosition][i] / 16 + 1;  // Current StsReg number
      iCurrentBit = g_ddiEventPositions[iPosition][i] - 16 * (iCurrentStsReg-1);  // Current bit position in current StsReg
    
      if ((dynlen(diStsRegValues) >= iCurrentStsReg) && (iCurrentStsReg != 0))  // If current StsReg value was queried
        {
        iValue=diStsRegValues[iCurrentStsReg];
        if (getBit(diStsRegValues[iCurrentStsReg], iCurrentBit) == 1)  // If current bit is equal to 1, save bit name description
          {
          sBitMessage = sBitMessage + UN_OBJECT_LIST_BITS_DELIMITER + g_ddsEventNames[iPosition][i];
          bEvent=true;
          }
        }  
      }

    if (!bEvent)
      {
      string sTemp;
      sprintf(sTemp, "%x", iValue);
      sBitMessage = getCatStr("unEventList","UNKNOWSTATE") + sTemp;
      bInvalidBit = true;
      i = length;
      }
    }
  
  if (sBitMessage != "")
    {
    sBitMessage = substr(sBitMessage, strlen(UN_OBJECT_LIST_BITS_DELIMITER));
    }
  sBitListName = sBitMessage;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_displayList
/**
Purpose: 
Display the list of device data in a table 

Parameters:
  @param dsDeviceDpName: dyn_string, input, list of device dp name
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Use constants UN_POPUPMENU_***
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_displayList(dyn_string dsDeviceDpName)
{
  int i, len;
  time tTime;
  dyn_string dsParameters;
  string sAlias, sDomain, sNature, sDescription, sSynoptic, sSystemName, sDeviceType;
  dyn_string dsAlias, dsDomain, dsNature, dsDescription, dsSystemName, dsDeviceType;
  string sBitName, sTemp, sFaceplate;
  dyn_string dsBitName;
  bool bInvalidBit, bIsConnected, bInvalidData, bFEEnable;
  int iFEAlarm;
  string sFrontEndName, sApplication, sInvalid;
  dyn_string dsApplication, dsInvalid;
  dyn_string dsReturnData;
  string sValue, sTime, sUTCTime;
  dyn_string dsValue, dsTime, dsUTCTime;
  dyn_string dsColorText, exceptionInfo;
  dyn_int diColorLine;
  dyn_dyn_string ddsFaceplate, ddsSynoptic;
  bool bUTC;
  string sColor;

  dyn_string dsData, dsFilter;
  int iRes;
  int iDevice;

//DebugTN("start unTreeDeviceOverview_displayList");  
  List.deleteAllLines();
  tTime = getCurrentTime();
  if((bUTC_graph.text == "TRUE") || (bUTC_graph.text == "true"))
    bUTC = true;
  if (bUTC)
    {
    long q1;
    short q2;
    q1=(long)tTime;
    q2=milliSecond(tTime);
    tTime=CtrlEventList_displayUTCTime(q1 + "." + q2);    //dll: return Time as UTC display  
    }

  unTreeDeviceOverview_displayLoadingText("Loading devices data", "Please wait ...");

  sTime = (string)tTime;
  requestTime.text = sTime;
// gat all the data and add them in the table
  len = dynlen(dsDeviceDpName);
  for(i=1;i<=len;i++) {
//DebugN(i, dsDeviceDpName[i]);
    unTreeDeviceOverview_displayLoadingText("Loading devices data "+i+"/"+len, "Please wait ...", 100*i/len);
    sSystemName = dpSubStr(dsDeviceDpName[i], DPSUB_SYS);
    sDeviceType = dpTypeName(dsDeviceDpName[i]);
    unGenericDpFunctions_getParameters(dsDeviceDpName[i], dsParameters);
    sAlias = unGenericDpFunctions_getAlias(dsDeviceDpName[i]);
    sDomain = dsParameters[UN_PARAMETER_DOMAIN];
    sNature = dsParameters[UN_PARAMETER_NATURE]; 
    sDescription = unGenericDpFunctions_getDescription(dsDeviceDpName[i]); 
    sSynoptic = dsParameters[UN_PARAMETER_PANEL];
    sApplication = unGenericDpFunctions_getApplication(dsDeviceDpName[i]);
    exceptionInfo = makeDynString();
    unGenericDpFunctions_getFaceplate(dsDeviceDpName[i], sFaceplate, exceptionInfo);
    if(dynlen(exceptionInfo) > 0)
      sFaceplate = "";
    bInvalidBit = bInvalidData = bIsConnected = false;
    sBitName = "";
    sInvalid = "";
// get ObjectList data
    unTreeDeviceOverview_getDeviceListData(dsDeviceDpName[i], sDeviceType, dsReturnData);
    sTime = (string)CtrlEventList_displayLocalTimefromString(dsReturnData[1]);
    sUTCTime = (string)CtrlEventList_displayUTCTime(dsReturnData[1]);
    sValue = dsReturnData[2];
//DebugN(dsDeviceDpName[i], dsReturnData);
    if((dsReturnData[3] == "TRUE") || (dsReturnData[3] == "true"))
      bInvalidData = true;
    if(sBitName == "")
      sBitName = dsReturnData[4];
    sColor = dsReturnData[5];

    dsFilter = makeDynString("*", "*", "*", "*", "*", g_sBitNameFilter1, g_sBitNameFilter2, g_sBitNameFilter3, "*");
    dsData = makeDynString(sDeviceType, sAlias, sDescription, sDomain, sNature, sBitName, sSystemName, "", "", "", "");
    iRes=ctrlEventList_object_allowDisplay(dsData, dsFilter);  // cf. ctrlEventList.dll
//  DebugN(iRes, dsData, dsFilter);

    if(iRes != 0) { // add
  // check if syst connected
      unDistributedControl_isConnected(bIsConnected, sSystemName);
//DebugN(dsDeviceDpName[i], bIsConnected, sSystemName);  
  // check if Front-End ok
  // get the Front-End name 
      if(bIsConnected) {
        sFrontEndName = unGenericObject_GetPLCNameFromDpName(dsDeviceDpName[i]);
        if(sFrontEndName != "") {
          if(dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sFrontEndName)) {
    // get the alarm state of the front-end
            dpGet(sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sFrontEndName+".alarm", iFEAlarm,
                     sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_pattern+sFrontEndName+".enabled", bFEEnable);
  
            if (bInvalidData || bInvalidBit) {
              sInvalid = UN_WIDGET_TEXT_INVALID;
            }
            else if ((!bFEEnable) || (iFEAlarm > c_unSystemIntegrity_no_alarm_value)) {
              sInvalid = UN_WIDGET_TEXT_OLD_DATA;
              bInvalidData = true;
            }
          }
          else
            bIsConnected = false;
        }
        else 
          bIsConnected = false;
      }
//DebugN(dsDeviceDpName[i], sFrontEndName, iFEAlarm, bFEEnable, bInvalidData, bInvalidBit, bIsConnected);
  // put all the data in dyn_string
      dynAppend(dsAlias, sAlias);
      dynAppend(dsSystemName, sSystemName);
      dynAppend(dsDescription, sDescription);
      dynAppend(dsDomain, sDomain);
      dynAppend(dsNature, sNature);
      dynAppend(dsBitName, sBitName);
      dynAppend(dsInvalid, sInvalid);
      dynAppend(dsDeviceType, sDeviceType);
      iDevice++;
      
      if(sFaceplate != "")
        ddsFaceplate[dynlen(ddsFaceplate) + 1] = makeDynString("  F", "white", "black");
      else
        ddsFaceplate[dynlen(ddsFaceplate) + 1] = makeDynString("", "unEventList_BackColor", "black");
      if(sSynoptic != "")
        ddsSynoptic[dynlen(ddsSynoptic) + 1] = makeDynString("  S", "white", "black");
      else
        ddsSynoptic[dynlen(ddsSynoptic) + 1] = makeDynString("", "unEventList_BackColor", "black");
      dynAppend(dsValue, sValue);
      dynAppend(dsUTCTime, sUTCTime);
      dynAppend(dsTime, sTime);      
      dynAppend(dsApplication, sApplication);
  
  // keep invalid or old or keep no system (also no front-end) or keep color
      if(!bIsConnected) { // not connected --> color "unDataNoAccess"
        dynAppend(diColorLine, iDevice-1);
        dynAppend(dsColorText, "unDataNoAccess");
      }
      else if(bInvalidData || bInvalidBit) { // invalid data --> color "unDataNotValid"
        dynAppend(diColorLine, iDevice-1);
        dynAppend(dsColorText, "unDataNotValid");
      }
      else if(sColor != "") { // color given use it
        dynAppend(diColorLine, iDevice-1);
        dynAppend(dsColorText, sColor);
      }
    }
  }
// fill the table
  List.appendLines(dynlen(dsAlias),              // Display table
           "List_Alias", dsAlias,
           "List_SystemName", dsSystemName,
           "List_Application", dsApplication,
           "List_Description", dsDescription,
           "List_Domain", dsDomain,
           "List_Nature", dsNature,
           "List_Value", dsValue,
           "List_Time", dsTime,
           "List_Time_UTC", dsUTCTime,
           "List_BitName", dsBitName,
           "List_Invalid", dsInvalid,
           "List_Object", dsDeviceType, 
           "List_Faceplate", ddsFaceplate,
           "List_Synoptic", ddsSynoptic);

// set the color
  len = dynlen(diColorLine);
//DebugN(len, diColorLine, dsColorText);
  for(i=1;i<=len;i++) {
    setMultiValue("List","cellForeColRC",diColorLine[i],"List_Alias", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_SystemName", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Application", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Description", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Domain", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Nature", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Value", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Time", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Time_UTC", dsColorText[i],                      
                  "List","cellForeColRC",diColorLine[i],"List_BitName", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Invalid", dsColorText[i],
                  "List","cellForeColRC",diColorLine[i],"List_Object", dsColorText[i]);
//DebugN(i, diColorLine[i], dsColorText[i]);
  }
  // save data in case of re-ordering
  g_dsAlias = dsAlias;
  g_dsSystemName = dsSystemName;
  g_dsApplication = dsApplication;
  g_dsDescription = dsDescription;
  g_dsDomain = dsDomain;
  g_dsNature = dsNature;
  g_dsValue = dsValue;
  g_dsTime = dsTime;
  g_dsUTCTime = dsUTCTime;
  g_dsBitName = dsBitName;
  g_dsInvalid = dsInvalid;
  g_dsDeviceType = dsDeviceType;
  g_ddsFaceplate = ddsFaceplate;
  g_ddsSynoptic = ddsSynoptic;
  g_diColorLine = diColorLine;
  g_dsColorText = dsColorText;
// set table already intialized
  bSnapshot_graph.text = "TRUE";
//DebugTN("end unTreeDeviceOverview_displayList");  
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_displayLoadingText
/**
Purpose: 
Show the loading text of the treeDeviceOverview panel 

Parameters:
  @param sText1: string, input, the text 1
  @param sText2: string, input, the text 2
  @param iProgress: int, input, percentage of the progress
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_displayLoadingText(string sText1, string sText2, int iProgress =0)
{
  sLoadingText1.text = sText1;
  sLoadingText2.text = sText2;
  sLoadingText3.text = sText1;
  sLoadingText1.visible = true;
  sLoadingText2.visible = true;
  sLoadingText3.visible = true;
  progress_graph.progress(iProgress);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_showLoadingText
/**
Purpose: 
Show the loading text of the treeDeviceOverview panel 

Parameters:
  @param bShow: bool, input, true=text visible/false
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_showLoadingText(bool bShow)
{
  sLoadingText1.visible = bShow;
  sLoadingText2.visible = bShow;
  sLoadingText3.visible = bShow;

  if(!bShow)
    progress_graph.progress(0);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getDeviceListData
/**
Purpose: 
Get the data from the call of the ObjectList function 

Parameters:
  @param deviceDpName: string, input, the device dp name
  @param sDeviceType: string, input, the device type
  @param dsReturn: string, dyn_output, the return data from the call of the ObjectList function
  @param bInvalidBit: bool, output, true if no bit sets.
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_getDeviceListData(string sDeviceDpName, string sDeviceType, dyn_string &dsReturn)
{
  dyn_string dsFunctions, exceptionInfo;
  dyn_anytype dReturnData;
  string sFunction, sValue = " ", sTime = "0", sInv = "false", sBit = "", sColor = "";
  dyn_int diStsReg;
  bool bInvalidData, bInvalidBit;
  string sDPE=g_m_DPEObjectList[sDeviceType];
  
  unGenericObject_GetFunctions(sDeviceType, dsFunctions, exceptionInfo);
  if (dynlen(exceptionInfo) > 0)
  {
    sInv = "TRUE";
  }
  else
  {
    sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_OBJECTLIST];
    
    if(sFunction != "" )
    {
      evalScript(dReturnData,"dyn_string main(string deviceName, string sObject) {" + 
                              "dyn_anytype dReturnData;" +
                              "if (isFunctionDefined(\"" + sFunction + "\"))" +
                              "    {" +
                              "    " + sFunction + "(deviceName, sObject, dReturnData);" +
                              "    }" +
                              "return dReturnData; }", makeDynString(), sDeviceDpName, sDeviceType);
    }
  }
    
  if(dynlen(dReturnData) >= 5)
  {
    sTime = (long)((time)dReturnData[1]) + "." + milliSecond((time)dReturnData[1]);  //sTime= Seconds.Milli;
    sValue = dReturnData[2];
    sInv = dReturnData[3];
    sBit = dReturnData[4];
    sColor = dReturnData[5];
  }
  else if(dynlen(dReturnData) == 2)
  {
    sTime = (long)((time)dReturnData[1]) + "." + milliSecond((time)dReturnData[1]);  //sTime= Seconds.Milli;
    sValue = dReturnData[2];


    if(dpExists(sDeviceDpName+".ProcessInput.StsReg01")) {
      dpGet(sDeviceDpName+".ProcessInput.StsReg01", diStsReg[1],
            sDeviceDpName+".ProcessInput.StsReg01:_online.._bad", bInvalidData);
      if(dpExists(sDeviceDpName+".ProcessInput.StsReg02")) 
        dpGet(sDeviceDpName+".ProcessInput.StsReg02", diStsReg[2]);
      unTreeDeviceOverview_getDeviceState(sDeviceDpName, sDeviceType, diStsReg, sBit, bInvalidBit);
    }
    else if(dpExists(sDeviceDpName+sDPE)) {
      dpGet(sDeviceDpName+sDPE, diStsReg[1],
            sDeviceDpName+sDPE+":_online.._bad", bInvalidData);
      unTreeDeviceOverview_getDeviceState(sDeviceDpName, sDeviceType, diStsReg, sBit, bInvalidBit);
    }
//DebugN(sDeviceDpName, bInvalidBit, bInvalidData);    
    sInv = bInvalidBit || bInvalidData;

  }

  dsReturn[1] = sTime;
  dsReturn[2] = sValue;
  dsReturn[3] = sInv;
  dsReturn[4] = sBit;
  dsReturn[5] = sColor;
}

//-----------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_initializeEventConfiguration
/**
Purpose: 
Initialize the global variables containing the event bits, etc. definition 

Parameters:
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_initializeEventConfiguration()
{
  int i, length;
  dyn_string dsNames, dsChanges, dsColors;
  dyn_int diPositions;
  dyn_string dsFrontEndType;

  if(g_bEventInitRecall)
  {
    g_bEventInitRecall = false;

    // device
    unGenericDpFunctions_getUnicosObjects(g_dsTreeDeviceOverviewUnicosObjects);
    length = dynlen(g_dsTreeDeviceOverviewUnicosObjects);
    for(i=1;i<=length;i++)
    {
      unTreeDeviceOverview_getConfiguration(g_dsTreeDeviceOverviewUnicosObjects[i], dsNames, diPositions, dsChanges, dsColors);
      g_ddsEventNames[i] = dsNames;
      g_ddiEventPositions[i] = diPositions;
      dynAppend(g_dsEventBitNames, dsNames);
      g_m_DPEObjectList[g_dsTreeDeviceOverviewUnicosObjects[i]] = unTreeDeviceOverview_getDPEObjectList(g_dsTreeDeviceOverviewUnicosObjects[i]);
    }
    
    // front-end
    length = mappinglen(g_mFrontEndConfigs);
    for(i=1;i<=length;i++) {
      g_m_DPEObjectList[mappingGetKey(g_mFrontEndConfigs, i)] = "";
    }
    
    dynUnique(g_dsEventBitNames);
    dynSortAsc(g_dsEventBitNames);

    length = dynlen(g_dsEventBitNames);
    for(i=1;i<=length;i++) {
      g_sAllBitNames = g_sAllBitNames + UN_OBJECT_LIST_FILTER_COMBO_DELIMITER + g_dsEventBitNames[i];
    }
    g_sAllBitNames = substr(g_sAllBitNames, strlen(UN_OBJECT_LIST_FILTER_COMBO_DELIMITER));
  
    g_bEventInitRecall = true;
  }
}

//-----------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getDPEObjectList
/**
Purpose: 
get the DPE of a sDeviceType used for the objectList 

Parameters:
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
string unTreeDeviceOverview_getDPEObjectList(string sDeviceType)
{
  dyn_string dsDpel; 
  dyn_int diDpelType;
  string sDPE;
  dyn_string dsRes;
  
  dpGet("_unApplication.objectListDPE", sDPE);
  unGenericDpFunctions_getDPE(sDeviceType, dsDpel, diDpelType);  
  dsRes = dynPatternMatch("*"+sDPE, dsDpel);
  if(dynlen(dsRes) >=1 ) {
    strreplace(dsRes[1], sDeviceType, "");
    return dsRes[1];
  }
  else
    return "-";
}

//-----------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_getConfiguration
/**
Purpose: 
Get the the configuration of the StsReg01(02) for an Unicos device type 

Parameters:
  @param dpType: string, input, the device dp type
  @param dsNames: dyn_string, output, list of event bit names
  @param diPositions: dyn_int, output, list of event bit position
  @param dsChanges: dyn_string, output, list of event bit corresponding rising/falling edge
  @param dsColors: dyn_string, output, list of event bit color
  
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 2.12.1 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTreeDeviceOverview_getConfiguration(string dpType, dyn_string &dsNames, dyn_int &diPositions, dyn_string &dsChanges, dyn_string &dsColors)
{
  string sNames, sPositions, sChanges, sColors;
  dyn_string dsNamesTemp, dsPositionsTemp, dsChangesTemp, dsColorsTemp;
  int i, iRes, length, currentPosition;
  
  // 1. If no configuration exists
  dsNames = makeDynString();
  diPositions = makeDynInt();
  dsChanges = makeDynString();
  
  // 2. Check if a valid configuration exists
  if (dpType != "")
    {
    iRes = dpGet(dpType + UN_OBJECT_EXTENSION + ".unEventList.Names", sNames,
           dpType + UN_OBJECT_EXTENSION + ".unEventList.Positions", sPositions,
           dpType + UN_OBJECT_EXTENSION + ".unEventList.Changes", sChanges,
           dpType + UN_OBJECT_EXTENSION + ".unEventList.Colors", sColors);
    if (iRes >=0)    // A configuration exists
      {
      dsNamesTemp = strsplit(sNames, UN_OBJECT_LIST_CONFIGURATION_DELIMITER);
      dsPositionsTemp = strsplit(sPositions, UN_OBJECT_LIST_CONFIGURATION_DELIMITER);
      dsChangesTemp = strsplit(sChanges, UN_OBJECT_LIST_CONFIGURATION_DELIMITER);
      dsColorsTemp = strsplit(sColors, UN_OBJECT_LIST_CONFIGURATION_DELIMITER);
      length = dynlen(dsNamesTemp);
      if ((length == dynlen(dsPositionsTemp)) && (length == dynlen(dsChangesTemp)) && (length == dynlen(dsColorsTemp)))  // Configuration is valid
        {
        dynClear(dsNames);            // Reset values
        dynClear(diPositions);
        dynClear(dsChanges);  
        dynClear(dsColors);    
        dsNames = dsNamesTemp;          // Set configuration
        dsChanges = dsChangesTemp;
        dsColors = dsColorsTemp;
        for (i=1;i<=length;i++)          // Convert dyn_string to dyn_int
          {
          currentPosition = (int) dsPositionsTemp[i];
          dynAppend(diPositions,currentPosition);
          }
        }
      }
    }
}

//--------------------------------------------------------------------------------------------------------------------------------
// unTreeDeviceOverview_getWidthSize
/**
Purpose: 
get the width dimension based on the strlen of the alias
>=20 characters ==> Large Alias display
<20 characters ==> Small Alias display

Parameters:
  @param dsDeviceDpName: dyn_string, input, the device dp list
  @param widthAliasSmall: int, input, width alias small
  @param widthAliasLarge: int, input, width alias large
  @param return: int, the width alias

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
int unTreeDeviceOverview_getWidthSize(dyn_string dsDeviceDpName, int widthAliasSmall, int widthAliasLarge)
{
  int widthAlias=widthAliasSmall, i, len;
  bool bContinue=true;
  string sAlias;
  
  len = dynlen(dsDeviceDpName);
  for(i=1;(i<=len) && bContinue;i++) {
    sAlias = unGenericDpFunctions_getAlias(dsDeviceDpName[i]);
    if(strlen(sAlias) >=22) {
      widthAlias = widthAliasLarge;
      bContinue = false;
    }
  }
  return widthAlias;
}

//--------------------------------------------------------------------------------------------------------------------------------
// _unTreeDeviceOverview_getSubSystemFromFile
/**
Purpose: 
Get the list of subsystem1 and subsystem2 from file

Parameters:
  @param sSystemName: string, input, the system name
  @param dsSubsystem1: dyn_string, output, the subsystem1 list
  @param dsSubsystem2: dyn_string, output, the subsystem2 list
  @param sFileNameSuffix: string, input, the file name suffix = "_unTreeDeviceOverview_subSystem.dat" by default

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
_unTreeDeviceOverview_getSubSystemFromFile(string sSystemName, dyn_string &dsSubsystem1, dyn_string &dsSubsystem2, string sFileNameSuffix="_unTreeDeviceOverview_subSystem.dat")
{
  string sFileName;
  string sBuffer;
  string sFileSystemName = sSystemName;
  file fFile;
  dyn_string dsSplit;

//DebugTN("start _unTreeDeviceOverview_getSubSystemFromFile", sSystemName);
  strreplace(sFileSystemName, ":", "");
  sFileName = getPath(DATA_REL_PATH)+"_"+sFileSystemName+sFileNameSuffix;

  fileToString(sFileName, sBuffer);
  dsSplit = strsplit(sBuffer, "\n");
  if(dynlen(dsSplit) == 2) {
    dsSubsystem1 = strsplit(dsSplit[1], ";");
    dsSubsystem2 = strsplit(dsSplit[2], ";");
  }
//DebugTN("end _unTreeDeviceOverview_getSubSystemFromFile", sSystemName, dynlen(dsSubsystem1), dynlen(dsSubsystem2));
}

//--------------------------------------------------------------------------------------------------------------------------------
// _unTreeDeviceOverview_getFrontEndNameApplicationFromFile
/**
Purpose: 
Get the list of subsystem1 and subsystem2 from file

Parameters:
  @param sSystemName: string, input, the system name
  @param dsFrontEnd: dyn_string, output, the front-end list
  @param dsFrontEndDp: dyn_string, output, the front-end dp list
  @param dsFrontEndType: dyn_string, output, the front-end dp type list
  @param ddsFEApplication: dyn_dyn_string, output, the front-end application list
  @param sFileNameSuffix: string, input, the file name suffix = "_unTreeDeviceOverview_subSystem.dat" by default

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
_unTreeDeviceOverview_getFrontEndNameApplicationFromFile(string sSystemName, dyn_string &dsFrontEnd, dyn_string &dsFrontEndDp, dyn_string &dsFrontEndType, dyn_dyn_string &ddsFEApplication, string sFileNameSuffix="_unTreeDeviceOverview_FECharacteristics.dat")
{
  string sFileName;
  string sBuffer;
  string sFileSystemName = sSystemName;
  file fFile;
  dyn_string dsSplit;
  int i, len;
  dyn_dyn_string ddsTemp;
  dyn_string dsTemp, dsFe, dsFeDp, dsFeType;
  
//DebugTN("start _unTreeDeviceOverview_getFrontEndNameApplicationFromFile", sSystemName);
  strreplace(sFileSystemName, ":", "");
  sFileName = getPath(DATA_REL_PATH)+"_"+sFileSystemName+sFileNameSuffix;

  fileToString(sFileName, sBuffer);
  dsSplit = strsplit(sBuffer, "\n");
  len = dynlen(dsSplit);
  for(i=1;i<=len;i++) {
    dsTemp = strsplit(dsSplit[i], ";");
    if(dynlen(dsTemp) >=3) {
      dynAppend(dsFe, dsTemp[1]);
      dynRemove(dsTemp, 1);
      dynAppend(dsFeDp, dsTemp[1]);
      dynRemove(dsTemp, 1);
      dynAppend(dsFeType, dsTemp[1]);
      dynRemove(dsTemp, 1);
      dynAppend(ddsTemp, dsTemp);
    }
  }
  dsFrontEnd = dsFe;
  dsFrontEndDp = dsFeDp;
  dsFrontEndType = dsFeType;
  ddsFEApplication = ddsTemp;
//DebugTN("end _unTreeDeviceOverview_getFrontEndNameApplicationFromFile", sSystemName);
}

//--------------------------------------------------------------------------------------------------------------------------------
// unTreeDeviceOverview_loadAll
/**
Purpose: 
Load all files

Parameters:
  @param sSystemName: string, input, the system name
  @param dsDeviceType: dyn_string, input, the list of device type
  @param sFileNameSuffix: string, input, the file name suffix = "_TreeDeviceOverview_deviceList.dat" by default

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_loadAll(string sSystemName, dyn_string dsDeviceType, string sFileNameSuffix = "_TreeDeviceOverview_deviceList.dat")
{
  dyn_dyn_string ddsTemp;
  dyn_string dsDomain, dsNature;
  dyn_string dsFrontEnd, dsFrontEndDp, dsFrontEndType, dsTempApplication, dsReturn;
  int i, len;
  
  _unTreeDeviceOverview_getFrontEndNameApplicationFromFile(sSystemName, dsFrontEnd, dsFrontEndDp, dsFrontEndType, ddsTemp);
  len = dynlen(dsFrontEnd);
  for(i=1;i<=len;i++) {
    dsTempApplication = ddsTemp[i];
    dynAppend(dsReturn, dsTempApplication);
  }
  dynUnique(dsReturn);

//DebugTN("end get", sSystemName, dynlen(dsFrontEnd), dynlen(dsFrontEndDp), dynlen(dsFrontEndType), dynlen(dsReturn));  
// set the list of the corresponding system
  g_m_sTreePVSSSystem[sSystemName] = sSystemName;
// get all the front-end and all the applications.
  g_m_dsTreeFrontEnd[sSystemName] = dsFrontEnd;
  g_m_dsTreeFrontEndDp[sSystemName] = dsFrontEndDp;
  g_m_dsTreeFrontEndType[sSystemName] = dsFrontEndType;
  g_m_dsTreeApplication[sSystemName] = dsReturn;
  g_m_dsTreeFrontEndApplication[sSystemName] = ddsTemp;
//DebugTN(ddsTemp, g_m_dsTreeFrontEndApplication);
// get subsystem1 and subsystem2 for sSystem
  _unTreeDeviceOverview_getSubSystemFromFile(sSystemName, dsDomain, dsNature);
  g_m_dsTreeSubsystem1[sSystemName] = dsDomain;
  g_m_dsTreeSubsystem2[sSystemName] = dsNature;

  _unTreeDeviceOverview_buildSubsystemList();
//DebugTN("end list", sSystemName);
  unTreeDeviceOverview_getDeviceList(sSystemName, dsDeviceType, sFileNameSuffix);
}

//-------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_initializeTreeDeviceWidget
/**
Purpose:
Initialize the tree device widget 

  @param b32InitSelectState: b32, input:  the filter setup.
  @param dsExcludedDeviceType: dyn_string, input: the list of device type to exclude in the tree device overview
  @param sInitFunction: string, input: init function to be executed
  @param sCBFunction: string, input: CallBack function to be used for the trigger
  @param exInfo: dyn_string, output: exception are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_initializeTreeDeviceWidget(bit32 b32InitSelectState, bit32 b32InitSortByState, dyn_string dsExcludedDeviceType,
                                                string sInitFunction, string sCBFunction, dyn_string &exInfo)
{
  dyn_string dsSystemNames, dsHost;
  dyn_int diSystemId, diPort;
  int i, len, pos;
  shape shTreeShape = getShape(TREE_NAME);
  mapping m;
  dyn_string dsFrontEndType = allFrontEndTypes.items;
  bit32 b32SelectState = b32SelectState_graph.text;
  bit32 b32SortByState = b32SortByState_graph.text;
  dyn_string dsDeviceType;
  string imageClassParameters;
  string iconWithChildrenClose, iconWithChildrenOpen, iconWithChildrenSelected;
  string iconWithChildrenPath, iconWithoutChildrenNotSelected, iconWithoutChildrenSelected;
  dyn_int diAliasDim;
  string sTreeDevOvDp=unTreeDeviceOverview_getInternalTriggerName();
  dyn_dyn_string ddsTemp;
  
  applyFilter.enabled(false);
  saveFilter.enabled(false);
  openFilter.enabled(false);
  advancedFilterButton.enabled(false);
  sortBy.enabled(false);

  selectedFrontEndInfo.text = NO_FILTER_SELECTED;
  selectedApplication.text = NO_FILTER_SELECTED;
  selectedDeviceTypeInfo.text = NO_FILTER_SELECTED;
  selectedSubsystem1.text = NO_FILTER_SELECTED;
  selectedSubsystem2.text = NO_FILTER_SELECTED;  
  selectedAlias.text = NO_FILTER_SELECTED;

  if(!dpExists(sTreeDevOvDp)) {
    dpCreate(sTreeDevOvDp, DEVUN_DPTYPE);
  }
  dynAppend(g_dsTreeDeviceOverview_triggerDisplayDP, sTreeDevOvDp);
  dynUnique(g_dsTreeDeviceOverview_triggerDisplayDP);
  
  if(dpExists(sTreeDevOvDp)) {
  // 4. Other...
    
    b32SelectState = b32InitSelectState;
    b32SelectState_graph.text = b32SelectState;

    b32SortByState = b32InitSortByState;
    b32SortByState_graph.text = b32SortByState;
  
    // 2. get list of device type and remove the excluded
    unGenericDpFunctions_getUnicosObjects(dsDeviceType);
    len = dynlen(dsExcludedDeviceType);
    for(i=1;i<=len;i++) {
      pos = dynContains(dsDeviceType, dsExcludedDeviceType[i]);
      if(pos>0)
        dynRemove(dsDeviceType, pos);
    }
    selectedDeviceTypes.items = dsDeviceType;
    g_m_dsSelectedItems[FILTER_DEVICETYPE] = dsDeviceType;

    dsExcludedDeviceType = dsDeviceType;
    dynAppend(dsExcludedDeviceType, UN_EVENT_LIST_LIST_NAME);
    dynInsertAt(dsExcludedDeviceType, "*", 1);
    allDeviceTypes.items = dsExcludedDeviceType;
  
  /*-------------------------------------------------------------------------------------------*/
  // get the list of the systems connected to the system  
  /*-------------------------------------------------------------------------------------------*/
    unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemId, dsHost, diPort);
    len = dynlen(dsSystemNames);
    for(i=1;i<=len;i++) {
      if(strpos(dsSystemNames[i], ":") < 0)
        dsSystemNames[i] = dsSystemNames[i]+":";
    }
    // 1. get all the front-end type
    unGenericDpFunctions_getFrontEndDeviceType(dsFrontEndType);
    allFrontEndTypes.items = dsFrontEndType;
  
    unTreeWidget_setPropertyValue(shTreeShape, "set_bNodeNotRemovedOnCollapseExpand", true, true);
  
  // load the images for front-end type
    len = dynlen(dsFrontEndType);
    for(i=1;i<=len;i++) {
  // if the images exists then load it, otherwise use the default.
      iconWithChildrenClose = getPath(PICTURES_REL_PATH,dsFrontEndType[i]+"/iconWithChildrenClose.bmp");
      iconWithChildrenOpen = getPath(PICTURES_REL_PATH,dsFrontEndType[i]+"/iconWithChildrenOpen.bmp");
      iconWithChildrenSelected = getPath(PICTURES_REL_PATH,dsFrontEndType[i]+"/iconWithChildrenSelected.bmp");
      iconWithChildrenPath = getPath(PICTURES_REL_PATH,dsFrontEndType[i]+"/iconWithChildrenPath.bmp");
      iconWithoutChildrenNotSelected = getPath(PICTURES_REL_PATH,dsFrontEndType[i]+"/iconWithoutChildrenNotSelected.bmp");
      iconWithoutChildrenSelected = getPath(PICTURES_REL_PATH,dsFrontEndType[i]+"/iconWithoutChildrenSelected.bmp");
      
      if( (iconWithChildrenClose != "") && (iconWithChildrenOpen != "") &&
          (iconWithChildrenSelected != "") && (iconWithChildrenPath != "") &&
          (iconWithoutChildrenNotSelected != "") && (iconWithoutChildrenSelected != "")) {
        imageClassParameters = iconWithChildrenClose + ";" +
                     iconWithChildrenOpen + ";" +
                     iconWithChildrenSelected + ";" +
                     iconWithChildrenPath + ";" +
                     iconWithoutChildrenNotSelected + ";" +
                     iconWithoutChildrenSelected;
      
        unTreeWidget_setPropertyValue(shTreeShape, "addImageClass", dsFrontEndType[i]+";" + imageClassParameters, true);
      }
    }
  
  // load the images for device type
    len = dynlen(dsDeviceType);
    for(i=1;i<=len;i++) {
  // if the images exists then load it, otherwise use the default.
      iconWithChildrenClose = getPath(PICTURES_REL_PATH,dsDeviceType[i]+"/iconWithChildrenClose.bmp");
      iconWithChildrenOpen = getPath(PICTURES_REL_PATH,dsDeviceType[i]+"/iconWithChildrenOpen.bmp");
      iconWithChildrenSelected = getPath(PICTURES_REL_PATH,dsDeviceType[i]+"/iconWithChildrenSelected.bmp");
      iconWithChildrenPath = getPath(PICTURES_REL_PATH,dsDeviceType[i]+"/iconWithChildrenPath.bmp");
      iconWithoutChildrenNotSelected = getPath(PICTURES_REL_PATH,dsDeviceType[i]+"/iconWithoutChildrenNotSelected.bmp");
      iconWithoutChildrenSelected = getPath(PICTURES_REL_PATH,dsDeviceType[i]+"/iconWithoutChildrenSelected.bmp");
      
      if( (iconWithChildrenClose != "") && (iconWithChildrenOpen != "") &&
          (iconWithChildrenSelected != "") && (iconWithChildrenPath != "") &&
          (iconWithoutChildrenNotSelected != "") && (iconWithoutChildrenSelected != "")) {
        imageClassParameters = iconWithChildrenClose + ";" +
                     iconWithChildrenOpen + ";" +
                     iconWithChildrenSelected + ";" +
                     iconWithChildrenPath + ";" +
                     iconWithoutChildrenNotSelected + ";" +
                     iconWithoutChildrenSelected;
      }
      else {
        iconWithChildrenClose = getPath(PICTURES_REL_PATH,UN_OBJECT_DPTYPE+"/iconWithChildrenClose.bmp");
        iconWithChildrenOpen = getPath(PICTURES_REL_PATH,UN_OBJECT_DPTYPE+"/iconWithChildrenOpen.bmp");
        iconWithChildrenSelected = getPath(PICTURES_REL_PATH,UN_OBJECT_DPTYPE+"/iconWithChildrenSelected.bmp");
        iconWithChildrenPath = getPath(PICTURES_REL_PATH,UN_OBJECT_DPTYPE+"/iconWithChildrenPath.bmp");
        iconWithoutChildrenNotSelected = getPath(PICTURES_REL_PATH,UN_OBJECT_DPTYPE+"/iconWithoutChildrenNotSelected.bmp");
        iconWithoutChildrenSelected = getPath(PICTURES_REL_PATH,UN_OBJECT_DPTYPE+"/iconWithoutChildrenSelected.bmp");
        imageClassParameters = iconWithChildrenClose + ";" +
                     iconWithChildrenOpen + ";" +
                     iconWithChildrenSelected + ";" +
                     iconWithChildrenPath + ";" +
                     iconWithoutChildrenNotSelected + ";" +
                     iconWithoutChildrenSelected;
      }
      
      unTreeWidget_setPropertyValue(shTreeShape, "addImageClass", dsDeviceType[i]+";" + imageClassParameters, true);
    }
  
  // register the new system
  //DebugTN("START");
    unTreeWidget_setPropertyValue(shTreeShape, "clearTree", true, true);

    len = dynlen(dsSystemNames);
    if(len != dynlen(g_dsTreeDeviceOverviewRegisteredSystem))
    {
      fwException_raise(exInfo, "ERROR", "unTreeDeviceOverview_initializeTreeDeviceWidget: PVSS system added or removed, please restart the UI", "");
      return;
    }
    for(i=1;i<=len;i++) {
      g_m_iDevicePerSystem[g_dsTreeDeviceOverviewRegisteredSystem[i]] = 0;
      g_m_ddsSelectedDevice[g_dsTreeDeviceOverviewRegisteredSystem[i]] = ddsTemp;
      g_m_dsNodeInTree[g_dsTreeDeviceOverviewRegisteredSystem[i]] = makeDynString();
      g_m_dbTopNode[g_dsTreeDeviceOverviewRegisteredSystem[i]] = makeDynBool();
    }
    g_m_ddsNodeDevice = m;
    dpConnect(sCBFunction, false, sTreeDevOvDp+".command", /*sTreeDevOvDp+".filter", sTreeDevOvDp+".view", */sTreeDevOvDp+".systemName");
  }
  else {
    fwException_raise(exInfo, "ERROR", "unTreeDeviceOverview_initializeTreeDeviceWidget: dp not existing "+sTreeDevOvDp, "");
  }

        
  if(sInitFunction != "")
    execScript("main() {" + sInitFunction + "();}", makeDynString());
        
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_displayCallBackTreeDeviceWidget
/**
Purpose:
callback handling the the tree device unTreeWidget display
  - DEVUN_ADD_COMMAND: add the data of a given system name
  - DEVUN_DELETE_COMMAND: delete the data of a given system name
  - DEVUN_APPLYFILTER_COMMAND: apply the new filter setting
  
  @param sCommandDp: string, input: the command DP
  @param iCommand: int, input: the command to execute.
  @param sB32Dp: string, input: the filter DP
  @param b32SelectState: b32, input: the filter setup.
  @param sSystemNameDp: string, input: the system DP
  @param sSystemName: string, input: the system name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_displayCallBackTreeDeviceWidget(string sCommandDp, int iCommand, string sSystemNameDp, string sSystemName)
{
  shape shTreeShape = getShape(TREE_NAME);
  int iCurrentPointer, iMousePointer;
  int id, i, len;
  bool bUpdate = false;
  dyn_dyn_string ddsTemp, ddsEmptyTemp;
  mapping mEmpty;
  
  bit32 b32SortByState, b32SelectState;
  bool bPVSSSystem, bFrontEnd, bApplication, bDeviceType, bSubsystem1, bSubsystem2,bAlias;

  bPVSSSystem = sortBy.state(FILTER_PVSS_SYSTEM);
  bFrontEnd = sortBy.state(FILTER_FRONT_END);
  bApplication = sortBy.state(FILTER_APPLICATION);
  bDeviceType = sortBy.state(FILTER_DEVICETYPE);
  bSubsystem1 = sortBy.state(FILTER_SUBSYSTEM1);
  bSubsystem2 = sortBy.state(FILTER_SUBSYSTEM2);
  
  setBit(b32SortByState, FILTER_PVSS_SYSTEM, bPVSSSystem);
  setBit(b32SortByState, FILTER_FRONT_END, bFrontEnd);
  setBit(b32SortByState, FILTER_APPLICATION, bApplication);
  setBit(b32SortByState, FILTER_DEVICETYPE, bDeviceType);
  setBit(b32SortByState, FILTER_SUBSYSTEM1, bSubsystem1);
  setBit(b32SortByState, FILTER_SUBSYSTEM2, bSubsystem2);
  setBit(b32SortByState, FILTER_ALIAS, false);

  bPVSSSystem = selectedSystemInfo.text() == NO_FILTER_SELECTED?FALSE:TRUE;
  bFrontEnd = selectedFrontEndInfo.text() == NO_FILTER_SELECTED?FALSE:TRUE;
  bApplication = selectedApplication.text() == NO_FILTER_SELECTED?FALSE:TRUE;
  bDeviceType = selectedDeviceTypeInfo.text( )== NO_FILTER_SELECTED?FALSE:TRUE;
  bSubsystem1 = selectedSubsystem1.text() == NO_FILTER_SELECTED?FALSE:TRUE;
  bSubsystem2 = selectedSubsystem2.text() == NO_FILTER_SELECTED?FALSE:TRUE;
  bAlias = selectedAlias.text()==NO_FILTER_SELECTED?FALSE:TRUE;
          
  setBit(b32SelectState, FILTER_PVSS_SYSTEM, bPVSSSystem);
  setBit(b32SelectState, FILTER_FRONT_END, bFrontEnd);
  setBit(b32SelectState, FILTER_APPLICATION, bApplication);
  setBit(b32SelectState, FILTER_DEVICETYPE, bDeviceType);
  setBit(b32SelectState, FILTER_SUBSYSTEM1, bSubsystem1);
  setBit(b32SelectState, FILTER_SUBSYSTEM2, bSubsystem2);
  setBit(b32SelectState, FILTER_ALIAS, bAlias);

//DebugTN("unTreeDeviceOverview_displayCallBackTreeDeviceWidget start", sCommandDp, iCommand, b32SelectState, b32SortByState, sSystemName);
// start the progress bar
//  id = unProgressBar_start(TREEBAR_NAME);  
// disable the unTreeWidget tree to have one action at a time.
  unTreeWidget_setPropertyValue(shTreeShape, "Enabled", false, true);
  iCurrentPointer = unTreeWidget_getPropertyValue(shTreeShape, "getMousePointer", true);
  iMousePointer = unTreeWidget_ccHourglass;
  unTreeWidget_setPropertyValue(shTreeShape, "setMousePointer", iMousePointer, true);

  applyFilter.enabled(false);
  saveFilter.enabled(false);
  openFilter.enabled(false);
  advancedFilterButton.enabled(false);

  systemPopupButton.enabled(false);
  frontEndPopupButton.enabled(false);
  applicationPopupButton.enabled(false);
  deviceTypePopupButton.enabled(false);
  subsystem1PopupButton.enabled(false);
  subsystem2PopupButton.enabled(false);
  aliasPopupButton.enabled(false);
  
  systemCheckbox.enabled(false);
  frontEndCheckbox.enabled(false);
  applicationCheckbox.enabled(false);
  deviceTypeCheckbox.enabled(false);
  subsystem1Checkbox.enabled(false);
  subsystem2Checkbox.enabled(false);
  aliasCheckbox.enabled(false);
        
  sortBy.enabled(false);

        synchronized(g_dsTreeApplication) {
          switch(iCommand) {
            case DEVUN_ADD_COMMAND:
        //DebugTN("start");
              g_m_bSystemForceSelectSort[sSystemName] = true;

        // build the list of all connected system and display
              unTreeDeviceOverview_buildSubsystemList();
              // force a refresh of the tree!
              delay(0,500);
              unTreeDeviceOverview_applyFilter(shTreeShape, b32SelectState, b32SortByState, "", TRUE);
              bUpdate=true;
              break;
            case DEVUN_DELETE_COMMAND:

              unTreeDeviceOverview_DeleteDiagnosticPanel(sSystemName);

              // reset the list of the corresponding system
              g_m_bSystemForceSelectSort[sSystemName] = false;

              g_m_ddsSelectedDevice[sSystemName] = ddsTemp;
              g_m_dsNodeInTree[sSystemName] = makeDynString();
              g_m_dbTopNode[sSystemName] = makeDynBool();

        // build the list of all connected system and display
              unTreeDeviceOverview_buildSubsystemList();
              break;
            case DEVUN_APPLYFILTER_COMMAND:
              if(g_bApplyFilter) {
                mappingClear(g_m_sNumberOfFilteredDevices);
                connectedSystemsCombobox.deleteAllItems();
                filteredDevicesCountLabel.text = "0 / 0";
                unTreeDeviceOverview_applyFilter(shTreeShape, b32SelectState, b32SortByState, "", false); 
                bUpdate = true;
              }
              break;
            default:
              break;
          }
          if(bUpdate) {
            len = mappinglen(g_m_bTreeDeviceOverviewSystemConnected);
            for(i=1;i<=len;i++) {
              if(mappingGetValue(g_m_bTreeDeviceOverviewSystemConnected, i)) 
              {
                g_m_sNumberOfFilteredDevices[mappingGetKey(g_m_bTreeDeviceOverviewSystemConnected, i)] = mappingGetValue(g_m_iDevicePerSystem, i)+" / "+dynlen(g_m_ddsTreeDeviceOverviewDeviceList[mappingGetKey(g_m_bTreeDeviceOverviewSystemConnected, i)][1]);
                connectedSystemsCombobox.appendItem(mappingGetKey(g_m_bTreeDeviceOverviewSystemConnected, i));                
              }
            }
            
            connectedSystemsCombobox.selectedPos(1); 
            filteredDevicesCountLabel.text = g_m_sNumberOfFilteredDevices[connectedSystemsCombobox.selectedText];
          }
        }
// hide the text if it is not already hidden
  unTreeWidget_hideText();

  applyFilter.enabled(true && g_bChangedFilters);
  saveFilter.enabled(true);
  openFilter.enabled(true);
  advancedFilterButton.enabled(true);

  systemPopupButton.enabled(true);
  frontEndPopupButton.enabled(true);
  deviceTypePopupButton.enabled(true);
  subsystem1PopupButton.enabled(true);
  subsystem2PopupButton.enabled(true);
  aliasPopupButton.enabled(true);

  if (g_unicosHMI_bApplicationFilterInUse)
  {
    applicationPopupButton.enabled(false);
    applicationCheckbox.enabled(false);
  }
  else
  {
    applicationPopupButton.enabled(true);
    applicationCheckbox.enabled(true);
  }
   
  systemCheckbox.enabled(true);
  frontEndCheckbox.enabled(true);  
  deviceTypeCheckbox.enabled(true);
  subsystem1Checkbox.enabled(true);
  subsystem2Checkbox.enabled(true);
  aliasCheckbox.enabled(true);
    
  sortBy.enabled(true);

  unTreeWidget_setPropertyValue(shTreeShape, "setMousePointer", iCurrentPointer, true);
// enable the unTreeWidget tree.
  unTreeWidget_setPropertyValue(shTreeShape, "Enabled", true, true);
// stop the progress bar.
//  unProgressBar_stop(id, TREEBAR_NAME);
//DebugTN("unTreeDeviceOverview_displayCallBackTreeDeviceWidget end");
}

//--------------------------------------------------------------------------------------------------------------------------------

// unTreeDeviceOverview_HandleEvent
/**
Purpose:
DeviceTreeOverview funtion to trigger the device overview action when one clicks on a node of the device tree 

  @param sCommand: string, input: the trigger
  @param sSelectedKey: string, input: the key of the selected node of the tree
  @param sCompositeLabel: string, input: the composite label of the selected node of the tree

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.0 
  . PVSS manager type: UI (WCCOAui)
  . operating system: WXP.
  . distributed system: yes.
*/
unTreeDeviceOverview_HandleEvent(string sCommand, string sSelectedKey, string sCompositeLabel)
{
  switch(sCommand) {
      case DEVUN_TREEDISPLAY_NODELEFTCLICK:
        treeDevKey_graph.text = sSelectedKey;
        treeDevCompositeLabel_graph.text = sCompositeLabel;
        triggerCommand.text = sCommand;
        break;
      default:
        break;
    }
}

//--------------------------------------------------------------------------------------------------------------------------------

//@}


