/**@name LIBRARY: unTrendTree.ctl

@author: Herve Milcent (LHC-IAS)

Creation Date: 19/04/2002

Modification History:
  11/05/2009: Herve
    - bug fix: removeSymbol in PVSS 3.8
    modified function: unTrendTree_preExecuteEventRigthClick, unTrendTree_preExecuteEventNodeRigthClick

  30/06/2008: Herve
    - bug childPanel:
      . replace ChildPanel.. by unGraphicalFrame_ChildPanel

        09/06/2008: Herve
          - in unTrendTree_nodeListLoadImage call unTrendTree_loadImage

        05/06/2008 Quique
          - Include new icons for the multitrend: UN_TRENDTREE_PAGE

        02/06/2008: Herve
          - unTrendTree_loadImage: remove getShape--> useless

        13/11/2007: Herve
          - bug with addImageClass

  02/04/2007: Herve
    new function: unTrendTree_eventNodeRigthClickOperationMenu unTrendTree_handleEventNodeRigthClickOperation

  08/03/2005: Herve.
    bug in unTrendTree_savePlotAs when alias contains .

  04/03/2005: Herve.
    bug in unTrendTree_savePlotAs when alias contains .

  21/02/2005: Herve
    - new version of the trending, add unTrendTree_getDPEFormat

  07/12/2004: Herve
    - for UN_TRENDTREE_PREDEFINED_TRENDS (Predefined_Trends) and UN_TRENDTREE_USERDEFINED_TRENDS (User_Defined_Trends) do not allow link/unlink, rename, remove, cut.
    - getHierarchyState: return false for remove in case of UN_TRENDTREE_PREDEFINED_TRENDS and UN_TRENDTREE_USERDEFINED_TRENDS
    - unTrendTree_getDeviceState: return false for link allowed in case of UN_TRENDTREE_PREDEFINED_TRENDS and UN_TRENDTREE_USERDEFINED_TRENDS
    - add the two functions: unTrendTree_savePageAs, unTrendTree_savePlotAs

  01/12/2004: Herve
    -replace add by new
    -add the add existing

  15/11/2004: Herve
    -new dollar parameter for the fwTrendingPlotConf.pnl in function unTrendTree_openPanelGetDollarParam

  24/09/2004: Herve
    - add function to get dollar parameter

  27/08/2004: Herve
    - totally new file for v3.0

version 1.1

External Functions:

Internal Functions:

Purpose:
This library contain functions to be used with the UNICOS TrendTree utility.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . the following panels must exist:
    . vision/graphicalFrame/contextualButton.pnl: contextual buttons for the _UnPanel data point type
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.

*/

#uses "unTree.ctl"
#uses "fwTrending/fwTrending.ctl"

private const bool unTrendTree_unHMILibLoaded = fwGeneral_loadCtrlLib("unHMI/unHMI.ctl",false);

const int UN_TRENDTREE_OPENASPOPUP = 1;
const string unTrendTree_FILENAME_CONFIGPANEL = "vision/trendTree/unTrendTreeConfiguration.pnl";

//@{

// unTrendTree_loadImage
/**
Purpose:
Function to load the image of the TrendTree into the unTreeWidget.

  @param sDeviceIdentifier: string, input: the display node name
  @param sGraphTree: string, input: the graphical name of the unTreeWidget
  @param exceptionInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_loadImage(string sDeviceIdentifier, string sGraphTree, dyn_string &exceptionInfo)
{
  shape aShape;
  string imageClassParameters;

// get the icon list of the root folder device and load it in the unTreeWidget if there is no error
  imageClassParameters = getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenClose.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenOpen.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"rootNode/iconWithChildrenPath.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"rootNode/iconWithoutChildrenNotSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"rootNode/iconWithoutChildrenSelected.bmp");

  unTreeWidget_setPropertyValue(aShape, "addImageClass", UN_TRENDTREE_ROOT_FOLDER+";" + imageClassParameters, true);

// get the icon list of the node device and load it in the unTreeWidget if there is no error
  imageClassParameters = getPath(PICTURES_REL_PATH,"node/iconWithChildrenClose.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"node/iconWithChildrenOpen.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"node/iconWithChildrenSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"node/iconWithChildrenPath.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"node/iconWithoutChildrenNotSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"node/iconWithoutChildrenSelected.bmp");
  unTreeWidget_setPropertyValue(aShape, "addImageClass", unTreeWidget_NODE+";" + imageClassParameters, true);
  unTreeWidget_setPropertyValue(aShape, "addImageClass", unTreeWidget_CLIPBOARD+";" + imageClassParameters, true);

// get the icon list of the _UnPanel device and load it in the unTreeWidget if there is no error
  imageClassParameters = getPath(PICTURES_REL_PATH,"trend/iconWithChildrenClose.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"trend/iconWithChildrenOpen.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"trend/iconWithChildrenSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"trend/iconWithChildrenPath.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"trend/iconWithoutChildrenNotSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"trend/iconWithoutChildrenSelected.bmp");
  unTreeWidget_setPropertyValue(aShape, "addImageClass", UN_TRENDTREE_PLOT+";" + imageClassParameters, true);

// get the icon list of the _UnPanel device and load it in the unTreeWidget if there is no error
  imageClassParameters = getPath(PICTURES_REL_PATH,"trend/iconWithChildrenClose.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"multitrend/iconWithChildrenOpen.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"multitrend/iconWithChildrenSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"multitrend/iconWithChildrenPath.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"multitrend/iconWithoutChildrenNotSelected.bmp") + ";" +
               getPath(PICTURES_REL_PATH,"multitrend/iconWithoutChildrenSelected.bmp");

  unTreeWidget_setPropertyValue(aShape, "addImageClass", UN_TRENDTREE_PAGE+";" + imageClassParameters, true);
}

// unTrendTree_EventRigthClickMenu
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose:
Function to build the popup menu when for the eventRigthClick on the unTreeWidget.

  @param dsMenu: dyn_string, output: the popup menu is returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_EventRigthClickMenu(dyn_string &dsMenu, string sDisplayNodeName, string sPasteNodeDpName, string sPasteNodeName, dyn_string &exInfo)
{
  bool bGranted = false;
  int iAllowed;
  dyn_string dsChildren;
  string sTopDisplayName;
  unTree_isUserAllowed(bGranted, exInfo);

  if(bGranted)
  {
    iAllowed = 1;
  }
  else
  {
    iAllowed = 0;
  }

  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sTopDisplayName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Refresh Tree, "+unTreeWidget_popupMenu_clearTree+", 1");
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, New folder, "+UN_TREE_newFolder+", "+iAllowed);
  dynAppend(dsMenu,"PUSH_BUTTON, New plot, "+UN_TRENDTREE_newPlot+", "+iAllowed);
  dynAppend(dsMenu,"PUSH_BUTTON, New page, "+UN_TRENDTREE_newPage+", "+iAllowed);
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Add plot, "+UN_TRENDTREE_addPlot+", "+iAllowed);
  dynAppend(dsMenu,"PUSH_BUTTON, Add page, "+UN_TRENDTREE_addPage+", "+iAllowed);

  dynAppend(dsMenu,"SEPARATOR");
  if(sPasteNodeName != "") {
    dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
  }
  else
    dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");

  fwTree_getChildren(sTopDisplayName, dsChildren, exInfo);
  if(dynlen(dsChildren) > 0) {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", "+iAllowed);
  }
  else {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", 0");
  }
}

// unTrendTree_preExecuteEventRigthClick
/**
Purpose:
pre-prossessing function called before the handling of the event rigthClick

  @param iCommand: int, input: the command

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.
*/
unTrendTree_preExecuteEventRigthClick(int iCommand)
{
  if((iCommand == unTreeWidget_popupMenu_reorder) ||
     (iCommand == unTreeWidget_popupMenu_clearTree) ||
     (iCommand == unTreeWidget_popupMenu_initTree)){
    unTree_setUserPanelOpened(false);
    unTree_setDeviceButtonGlobalVariable(false, false, false, false);
    unTree_setDeviceButtonState(false, false, false, false);
    unTree_setHierarchyButtonGlobalVariable(false, false, false, false, false, false,
                                            false, false, false);
    unTree_setHierarchyButtonState(false, false, false, false, false, false,
                                   false, false, false);

    unTree_setHierarchyButtonText("", "");
    unTree_setPasteGlobalValue("", "");
    unTrendTree_openPanelPreExecute();
    if(unTree_removeUserPanel())
      removeSymbol(myModuleName(), myPanelName(), "userPanel");
  }
}

// unTrendTree_handleEventRigthClick
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose:
Basic unTrendTree function: handle the result of the event rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sPasteNodeDpName: string, input/output: the node dp name to paste
  @param sPasteNodeName: string, input/output: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param bDone: bool, output: action executed
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_handleEventRigthClick(int ans, string sGraphTree, string sBar, string sSelectedKey,
                                    string sDpName, string &sPasteNodeDpName, string &sPasteNodeName,
                                    bool bClipboard, bool &bDone, dyn_string &exInfo)
{
  dyn_float dfFloat;
  dyn_string dsText, dsUserData;
  int i, len;
  shape aShape = getShape(sGraphTree);
  string sNodeNameAdded, sTopDisplayName, sTitle;

  bDone = false;
/*
DebugN("unTrendTree_handleEventRigthClick", ans, sGraphTree, sBar, sSelectedKey,
        sDpName, sPasteNodeDpName, sPasteNodeName, bClipboard);
*/
  g_closeAllowed.text = false;
  switch(ans) {
    case UN_TRENDTREE_addPlot:
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
      unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/unPanel/unPanelNavigationConfiguration_SelectTrending.pnl",
                       "Select Trend",
                       makeDynString("$sDpName:"),
                       dfFloat, dsText);
      if ((dynlen(dfFloat) > 0) && (dynlen(dsText) > 0)) {
        if ((dfFloat[1] >= 0) && (dsText[1] != ""))  {
          if(dfFloat[1] == 1) { // plot
            fwTrending_getPlotTitle(dsText[1], sTitle);
            unTrendTree_addDeviceInHierarchy(dsText[1], sTitle, sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, true, bDone, true, exInfo);
            bDone = true;
          }
          else {
            fwException_raise(exInfo, "ERROR", dsText[1]+": is not a plot", "");
          }
        }
      }
      break;
    case UN_TRENDTREE_newPlot:
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  // new plot
      unTrendTree_addDevice(sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, true, bDone, true, exInfo);
      break;
    case UN_TRENDTREE_addPage:
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
      unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/unPanel/unPanelNavigationConfiguration_SelectTrending.pnl",
                       "Select Trend",
                       makeDynString("$sDpName:"),
                       dfFloat, dsText);
      if ((dynlen(dfFloat) > 0) && (dynlen(dsText) > 0)) {
        if ((dfFloat[1] >= 0) && (dsText[1] != ""))  {
          if(dfFloat[1] == 0) { // page
            fwTrending_getPageTitle(dsText[1], sTitle);
            unTrendTree_addDeviceInHierarchy(dsText[1], sTitle, sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, true, bDone, false, exInfo);
          }
          else {
            fwException_raise(exInfo, "ERROR", dsText[1]+": is not a page", "");
          }
        }
      }
      break;
    case UN_TRENDTREE_newPage:
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  // new page
      unTrendTree_addDevice(sGraphTree, sBar, sSelectedKey, sDpName, sTopDisplayName, true, bDone, false, exInfo);
      break;
    default:
      unTree_handleDefaultEventRigthClick(ans, sGraphTree, sBar, sSelectedKey,
                                          sDpName, sPasteNodeDpName, sPasteNodeName,
                                          bClipboard, bDone, exInfo);
      break;
  }
  if(!bDone)
    g_closeAllowed.text = true;
//  DebugN("after fun", exInfo);
}

// unTrendTree_postExecuteEventRigthClick
/**
@reviewed 2018-06-22 @whitelisted{BackgroundProcessing}
Purpose:
Basic unTrendTree function: handle the post execution of the rigth click of the event rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_postExecuteEventRigthClick(int ans, string sGraphTree, string sBar, string sSelectedKey,
                                    string sDpName, string sPasteNodeDpName, string sPasteNodeName,
                                    bool bClipboard, dyn_string &exInfo)
{
// do nothing for the time being except reset the g_closeAllowed.text
//DebugN("unTrendTree_postExecuteEventRigthClick", ans, sSelectedKey, sDpName, sPasteNodeDpName,sPasteNodeName);
  g_closeAllowed.text = true;
}

// unTrendTree_EventNodeRigthClickMenu
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose:
Function to build the popup menu when for the eventNodeRigthClick on the unTreeWidget.

  @param dsMenu: dyn_string, input: the popup menu is returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_EventNodeRigthClickMenu(dyn_string &dsMenu, string sDisplayNodeName, string sDpName, string sPasteNodeDpName, string sPasteNodeName, dyn_string &exInfo)
{
  dyn_string dsChildren;
  string sNodeName, sDeviceName, sDeviceType;
  int isClipboard, isRoot, iAllowed;
  bool isInClipboard;
  bool bGranted = false;
  bool isPredefinedTrend=false, isUserDefinedTrend=false;

  sNodeName = _fwTree_getNodeName(sDpName);
  isClipboard = fwTree_isClipboard(sNodeName, exInfo);
  isInClipboard = fwTree_isInClipboard(sNodeName, exInfo);
//DebugN("IsInCli", sNodeName, isInClipboard);
  isRoot = fwTree_isRoot(sNodeName, exInfo);

  unTree_isUserAllowed(bGranted, exInfo);
  if(bGranted)
  {
    iAllowed = 1;
  }
  else
  {
    iAllowed = 0;
  }

  isPredefinedTrend = (sDisplayNodeName == UN_TRENDTREE_PREDEFINED_TRENDS);
  isUserDefinedTrend = (sDisplayNodeName == UN_TRENDTREE_USERDEFINED_TRENDS);

  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  if(!isClipboard && !isPredefinedTrend && !isUserDefinedTrend) {
    dynAppend(dsMenu,"SEPARATOR");
  }
  if(!isRoot) {
    if(!isClipboard && !isPredefinedTrend && !isUserDefinedTrend) {
      dynAppend(dsMenu,"PUSH_BUTTON, Cut: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_cut+", "+iAllowed);
      dynAppend(dsMenu,"PUSH_BUTTON, Copy: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copy+", "+iAllowed);

      if((sPasteNodeName != "") && (!isInClipboard) && (sPasteNodeDpName!=sDpName)) {
        dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
      }
      else
        dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");
      if(dpExists(sDeviceName) && ((sDeviceType == UN_TRENDTREE_PLOT) || (sDeviceType == UN_TRENDTREE_PAGE)) ) {
        dynAppend(dsMenu,"SEPARATOR");
        dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", "+iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, Link to plot/page, "+UN_TREE_linkToDevice+", "+iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, Unlink from plot/page, "+UN_TREE_unLinkToDevice+", "+iAllowed);
      }
      else {
        dynAppend(dsMenu,"SEPARATOR");
        dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", 0");
        dynAppend(dsMenu,"PUSH_BUTTON, Link to plot/page, "+UN_TREE_linkToDevice+", "+iAllowed);
        dynAppend(dsMenu,"PUSH_BUTTON, Unlink from plot/page, "+UN_TREE_unLinkToDevice+", 0");
      }
    }
    else {
      dynAppend(dsMenu,"SEPARATOR");
      if((sPasteNodeName != "") && (!isClipboard) && (sPasteNodeDpName!=sDpName)) {
        dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
      }
      else
        dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");
    }
  }

  dynAppend(dsMenu,"SEPARATOR");
  if(!isRoot && !isClipboard  && !isPredefinedTrend && !isUserDefinedTrend) {
    dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", "+iAllowed);
    if(dpExists(sDeviceName) && ((sDeviceType == UN_TRENDTREE_PLOT) || (sDeviceType == UN_TRENDTREE_PAGE)) )
      dynAppend(dsMenu,"PUSH_BUTTON, Rename, "+unTreeWidget_popupMenu_rename+", 0");
    else
      dynAppend(dsMenu,"PUSH_BUTTON, Rename, "+unTreeWidget_popupMenu_rename+", "+iAllowed);
  }
  else
    dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", 0");

  fwTree_getChildren(sNodeName, dsChildren, exInfo);
  if(dynlen(dsChildren) > 0) {
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", "+iAllowed);
  }
  else {
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", 0");
  }
}

// unTrendTree_preExecuteEventNodeRigthClick
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose:
pre-prossessing function called before the handling of the event NodeRigthClick

  @param iCommand: int, input: the command
  @param sCurrentDpName: string, input: the current dpName
  @param sDpName: string, input: the dpName selected

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . data point type needed: _FwViews
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: WXP.
  . distributed system: yes.
*/
unTrendTree_preExecuteEventNodeRigthClick(int iCommand, string sCurrentDpName, string sDpName)
{
  if((iCommand == unTreeWidget_popupMenu_reorder) && (sCurrentDpName == sDpName)) {
    unTree_setHierarchyPasteButtonGlobalVariable(false);
    unTree_setHierarchyPasteButtonState("", false);
    unTree_setPasteGlobalValue("", "");
    if(sCurrentDpName == sDpName) {
      unTree_setUserPanelOpened(false);

      unTrendTree_openPanelPreExecute();
      if(unTree_removeUserPanel())
        removeSymbol(myModuleName(), myPanelName(), "userPanel");
    }
  }
}

// unTrendTree_handleEventNodeRigthClick
/**
Purpose:
Basic unTrendTree function: handle the result of the event node rigth click

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the display node name
  @param sCompositeLabel: string, input: the composite label of the node name
  @param sParentKey: string, input: the key of the parent
  @param sParentDpName: string, input: the parent data point name
  @param sParentNodeName: string, input: the parent node name
  @param sPasteNodeDpName: string, input/output: the node dp name to paste
  @param sPasteNodeName: string, input/output: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param bDone: bool, output: action executed
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unTrendTree_handleEventNodeRigthClick(int ans, string sGraphTree, string sBar,
                                        string sSelectedKey, string sDpName, string sDisplayNodeName,
                                        string sCompositeLabel, string sParentKey, string sParentDpName,
                                        string sParentNodeName, string &sPasteNodeDpName,
                                        string &sPasteNodeName, bool bClipboard, bool &bDone,
                                        string sCurrentSelectedNodeKey, string sParentKeyToAddInNewKey,
                                        dyn_string &exInfo)
{
  dyn_float dfFloat;
  dyn_string dsText;
  string sCopiedNodeName;
  int i, len;
  bool bUserPanelOpened;
  int pos;
  string sNodeName, sDeviceName, sDeviceType, sNodeNameAdded, sTitle;
  dyn_string dsUserData;

  bDone = false;

/*
DebugN("unTrendTree_handleEventNodeRigthClick", ans, sGraphTree, sBar, sSelectedKey,
        sDpName, sDisplayNodeName, sCompositeLabel, sParentKey, sParentDpName,
        sParentNodeName, sPasteNodeDpName, sPasteNodeName, bClipboard);
*/
  g_closeAllowed.text = false;
  switch(ans) {
    case unTreeWidget_popupMenu_copy:
      sNodeName = _fwTree_getNodeName(sDpName);
      fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
      if((dynlen(exInfo) <=0) && ((sDeviceType == UN_TRENDTREE_PLOT) || (sDeviceType == UN_TRENDTREE_PAGE) )&& (dpExists(sDeviceName))){
        unTreeWidget_copy(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sParentDpName,
                                      sParentNodeName, bClipboard, sDisplayNodeName, sCopiedNodeName, exInfo);
      }
      else {
        unTreeWidget_copy(sGraphTree, sSelectedKey, sDpName, sDisplayNodeName, sParentDpName,
                                      sParentNodeName, bClipboard, "Copy_of_"+sDisplayNodeName, sCopiedNodeName, exInfo);
      }
    // if no error put the copied node in sPasteNodeName and sPasteNodeDpName
      if(dynlen(exInfo) <= 0) {
        sPasteNodeName = fwTree_getNodeDisplayName(sCopiedNodeName, exInfo);;
        sPasteNodeDpName = _fwTree_makeNodeName(sCopiedNodeName);
        bDone = true;
      }
      break;
    case UN_TRENDTREE_addPlot:
      unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/unPanel/unPanelNavigationConfiguration_SelectTrending.pnl",
                       "Select Trend",
                       makeDynString("$sDpName:"),
                       dfFloat, dsText);
      if ((dynlen(dfFloat) > 0) && (dynlen(dsText) > 0)) {
        if ((dfFloat[1] >= 0) && (dsText[1] != ""))  {
          if(dfFloat[1] == 1) { // plot
            fwTrending_getPlotTitle(dsText[1], sTitle);
            unTrendTree_addDeviceInHierarchy(dsText[1], sTitle, sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, true, exInfo);
            bDone = true;
          }
          else {
            fwException_raise(exInfo, "ERROR", dsText[1]+": is not a plot", "");
          }
        }
      }
      break;
    case UN_TRENDTREE_addPage:
      unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/unPanel/unPanelNavigationConfiguration_SelectTrending.pnl",
                       "Select Trend",
                       makeDynString("$sDpName:"),
                       dfFloat, dsText);
      if ((dynlen(dfFloat) > 0) && (dynlen(dsText) > 0)) {
        if ((dfFloat[1] >= 0) && (dsText[1] != ""))  {
          if(dfFloat[1] == 0) { // page
            fwTrending_getPageTitle(dsText[1], sTitle);
            unTrendTree_addDeviceInHierarchy(dsText[1], sTitle, sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, false, exInfo);
          }
          else {
            fwException_raise(exInfo, "ERROR", dsText[1]+": is not a page", "");
          }
        }
      }
      break;
    case UN_TRENDTREE_newPlot:
  // add plot
      unTrendTree_addDevice(sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, true, exInfo);
      break;
    case UN_TRENDTREE_newPage:
  // add page
      unTrendTree_addDevice(sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, false, bDone, false, exInfo);
      break;
    case UN_TREE_horizontalNavigation:
	// TODO Remove
      sNodeName = _fwTree_getNodeName(sDpName);
      fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
      if((dynlen(exInfo) <=0) && ((sDeviceType == UN_TRENDTREE_PLOT) || (sDeviceType == UN_TRENDTREE_PAGE) )&& (dpExists(sDeviceName))){
        unGraphicalFrame_ChildPanelOnCentralModal ("vision/unPanel/unPanelConfiguration.pnl", "Panel Configuration",
                            makeDynString("$sDpName:"+sDpName,"$sInvokingTree:TrendTree"));
        bDone = true;
      }
      break;
    case UN_TREE_linkToDevice:
      sNodeName = _fwTree_getNodeName(sDpName);
      fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
      fwTree_getNodeUserData(sNodeName, dsUserData, exInfo);
      unGraphicalFrame_ChildPanelOnCentralModalReturn("vision/unPanel/unPanelNavigationConfiguration_SelectTrending.pnl",
                       "Select Trend",
                       makeDynString("$sDpName:"),
                       dfFloat, dsText);
      if ((dynlen(dfFloat) > 0) && (dynlen(dsText) > 0)) {
        if ((dfFloat[1] >= 0) && (dsText[1] != ""))  {
          if(dfFloat[1] == 0) { // page
            unTree_linkToDevice(sGraphTree, sDpName, sSelectedKey, dsText[1], UN_TRENDTREE_PAGE, dsUserData, exInfo);
            bDone = true;
          }
          if(dfFloat[1] == 1) { // plot
            unTree_linkToDevice(sGraphTree, sDpName, sSelectedKey, dsText[1], UN_TRENDTREE_PLOT, dsUserData, exInfo);
            bDone = true;
          }
//DebugN(unTreeWidget_getIconClass(sDpName));
        }
      }
      break;
    case UN_TRENDTREE_dblClick:
      bDone = true;
      break;
    default:
      unTree_handleDefaultEventNodeRigthClick(ans, sGraphTree, sBar,
                                              sSelectedKey, sDpName, sDisplayNodeName,
                                              sCompositeLabel, sParentKey, sParentDpName,
                                              sParentNodeName, sPasteNodeDpName,
                                              sPasteNodeName, bClipboard, bDone,
                                              sCurrentSelectedNodeKey, sParentKeyToAddInNewKey,
                                              exInfo);
      break;
  }
  if(!bDone)
    g_closeAllowed.text = true;
}

// unTrendTree_postExecuteEventNodeRigthClick
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose:
Basic unTrendTree function: handle the post execution of the event node rigth click
WARNING: sDpName, sSelectedKey, etc. are value that come from the eventNodeRigthClick, if the node name was rename or deleted, an exception could be raise

  @param ans: int, input: what to do
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the display node name
  @param sCompositeLabel: string, input: the composite label of the node name
  @param sParentKey: string, input: the key of the parent
  @param sParentDpName: string, input: the parent data point name
  @param sParentNodeName: string, input: the parent node name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_postExecuteEventNodeRigthClick(int ans, string sGraphTree, string sBar,
                                        string sSelectedKey, string sDpName, string sDisplayNodeName,
                                        string sCompositeLabel, string sParentKey, string sParentDpName,
                                        string sParentNodeName, string sPasteNodeDpName,
                                        string sPasteNodeName, bool bClipboard,
                                        string sCurrentSelectedNodeKeyBis, string sParentKeyToAddInNewKey,
                                        dyn_string &exInfo)
{
  string sTitle, sDeviceName, sDeviceType, sTopDisplayName, sReturnedNameNode;
  bool bMoreOneInstance=false, bUserPanelOpened, bGranted=false;;
  shape aShape;
  string sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
        sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey;
  dyn_string dsResult;
  int i, len;

/*
DebugN("unTrendTree_postExecuteEventNodeRigthClick", ans, sSelectedKey, sDpName, sDisplayNodeName,
                                        sCompositeLabel, sParentKey, sParentDpName,
                                        sParentNodeName, sPasteNodeDpName,
                                        sPasteNodeName, sCurrentSelectedNodeKeyBis, sParentKeyToAddInNewKey);
*/
  unTree_getGlobalValue(sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
                  sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey);
  switch(ans) {
    case UN_TREE_linkToDevice:
      unTrendTree_getDeviceTitle(sDpName, sTitle, sDeviceName, sDeviceType);
//      DebugN(sTitle, sDisplayNodeName);
//in case of linkToDevice, device Configuration: rename the nodeName and apply case rename, rst paste global and hierearchy device button
// rst paste global and hierarchy device button.
      if((sTitle!= "") && (sTitle != sDisplayNodeName)) {
        if(sCurrentDpName == sDpName) {
// remove all symbols
          unTrendTree_preExecuteEventRigthClick(unTreeWidget_popupMenu_clearTree);
        }
        unTrendTree_renameDevice(sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName,
                                        sCompositeLabel, sParentKey, sParentDpName,
                                        sParentNodeName, sPasteNodeDpName,
                                        sPasteNodeName, bClipboard,
                                        sCurrentSelectedNodeKeyBis, sParentKeyToAddInNewKey,
                                        sTitle, exInfo);
      }
      else if(sTitle == sDisplayNodeName) {
        if(unTree_isFromNodeList() || (sCurrentDpName == sParentDpName)) {
          sDpName = sParentDpName;
          _fwTree_getParent(sDpName, sParentDpName);
          unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
          if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
            sParentDpName = "";
          unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
          unTree_setUserPanelOpened(bUserPanelOpened);
        }
// set link to true and unlink and navigation to false
        else if(sCurrentDpName == sDpName) {
          unTree_isUserAllowed(bGranted, exInfo);
          unTree_setDeviceButtonGlobalVariable(false, true, false, false);
          unTree_setDeviceButtonState(false, bGranted, false, false);
// open the user panel with no detail
          unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
          unTree_setUserPanelOpened(bUserPanelOpened);
        }
      }

      break;
    case UN_TRENDTREE_dblClick:
//      DebugN(strsplit(unTreeWidget_getPropertyValue("tree", "getAllNodeCompositeLabel"), "@"));
// if title has changed, check if more than one instance in tree, if yes->Re-init tree
// if not rename the current tree. like linkToDevice
    case UN_TREE_deviceConfiguration:
//DebugN(sCurrentDpName, sDpName);
      unTrendTree_getDeviceTitle(sDpName, sTitle, sDeviceName, sDeviceType);
//      DebugN(sTitle, sDisplayNodeName);
      if((sTitle!= "") && (sTitle != sDisplayNodeName)) {
        if(sCurrentDpName == sDpName) {
// remove all symbols
          unTrendTree_preExecuteEventRigthClick(unTreeWidget_popupMenu_clearTree);
        }
        bMoreOneInstance = unTrendTree_checkMultipleOccurence(sGraphTree, sDpName, sDisplayNodeName, sTitle, sDeviceName, sDeviceType);
        if(!bMoreOneInstance) {
          unTrendTree_renameDevice(sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName,
                                          sCompositeLabel, sParentKey, sParentDpName,
                                          sParentNodeName, sPasteNodeDpName,
                                          sPasteNodeName, bClipboard,
                                          sCurrentSelectedNodeKeyBis, sParentKeyToAddInNewKey,
                                          sTitle, exInfo);
        }
// rename all devices
        fwTree_getAllNodesByDevice(true, "", "*", sDeviceName, sDeviceType, dsResult, exInfo);
//DebugN("rename all devices", dsResult);
        len = dynlen(dsResult);
        for(i=1;i<=len;i++) {
            if(fwTree_getNodeDisplayName(dsResult[i], exInfo) != sTitle) {
//DebugN("rename ", dsResult[i]);
              sReturnedNameNode = fwTree_renameNode(dsResult[i], sTitle, exInfo);
            }
        }

        if(bMoreOneInstance) {
// re-init the tree.
//DebugN("re-init tree");
          aShape = getShape(sGraphTree);
          unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
          unTree_setPasteGlobalValue("", "");
          unTree_loadTree(sGraphTree, sBar, bClipboard);
    // show the WindowTree content
          unTree_EventClick(sGraphTree, sBar, bClipboard, 0, 0);
        }
      }
      break;
    default:
      break;
  }
// finally allow the close panel
  g_closeAllowed.text = true;
}
// unTrendTree_checkMultipleOccurence
/**
Purpose:
check if there are multiple occurence of the same title in the unTreeWidget

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the display node name
  @param sTitle: string, input: the title
  @param sDeviceName: string, input: the device name linke to the node
  @param sDeviceType: string, input: the device type
  @param return value: bool, output: true if there are more than one occurence of the sTitle in the unTreeWidget, false if not

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
bool unTrendTree_checkMultipleOccurence(string sGraphTree, string sDpName, string sDisplayNodeName, string sTitle, string sDeviceName, string sDeviceType)
{
  bool bResult=false;
  shape treeShape = getShape(sGraphTree);
  dyn_string dsListOfCompositeLabel, exInfo;
  string sTempDpName, sTempTitle, sTempDeviceName, sTempDeviceType;
  string sTempDisplayNodeName, sTempParentDpName, sTempParentNodeName;
  int i, len, iCount;

  sTempDeviceName = unTreeWidget_getPropertyValue("tree", "getAllNodeCompositeLabel");
  dsListOfCompositeLabel = strsplit(sTempDeviceName, unTreeWidget_keySeparator);
  len = dynlen(dsListOfCompositeLabel);

  iCount = 0;
  for(i=1; i<=len; i++) {
// get dpName, displayNodeName, etc.
    unTreeWidget_getNodeNameAndNodeDp(dsListOfCompositeLabel[i], sTempDpName, sTempDisplayNodeName, sTempParentDpName, sTempParentNodeName);
//DebugN(i, dsListOfCompositeLabel[i], sDpName, sDisplayNodeName, sTempDpName, sTempDisplayNodeName, sTempParentDpName, sTempParentNodeName);
// reject the current dp.
//DebugN("HOLA", i, dsListOfCompositeLabel[i], sTempDpName, sTempDisplayNodeName);
    if(sDpName != sTempDpName) {
      sTempDisplayNodeName = fwTree_getNodeDisplayName(_fwTree_getNodeName(sTempDpName), exInfo);
  // get nodeName, deviceName and deviceType
      unTrendTree_getDeviceTitle(sTempDpName, sTempTitle, sTempDeviceName, sTempDeviceType);
  // if same device name & device type & same displayNode as the current one
//DebugN(i, sTempDpName, sDeviceName, sTempDeviceName, sDeviceType, sTempDeviceType, sDisplayNodeName, sTempDisplayNodeName);
      if((sDeviceName == sTempDeviceName) && (sDeviceType == sTempDeviceType)) {
        iCount++;
//DebugN("********** YES", i, dsListOfCompositeLabel[i], sTempDpName, sDeviceName, sDeviceType, sDisplayNodeName, sTempDisplayNodeName);
      }
      else {
//DebugN("********** NO", i, dsListOfCompositeLabel[i], sTempDpName, sDeviceName, sDeviceType, sDisplayNodeName, sTempDisplayNodeName);
      }
    }
    else {
//DebugN("*** HOLA same", i, dsListOfCompositeLabel[i], sTempDpName, sDeviceName, sDeviceType, sDisplayNodeName, sTempDisplayNodeName);
    }
  }
  if(iCount > 0) {
    bResult = true;
  }
//DebugN(iCount, dynlen(dsListOfCompositeLabel));
  return bResult;
}

// unTrendTree_renameDevice
/**
Purpose:
rename the device to the new title name

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name
  @param sDisplayNodeName: string, input: the display node name
  @param sCompositeLabel: string, input: the composite label of the node name
  @param sParentKey: string, input: the key of the parent
  @param sParentDpName: string, input: the parent data point name
  @param sParentNodeName: string, input: the parent node name
  @param sPasteNodeDpName: string, input: the node dp name to paste
  @param sPasteNodeName: string, input: the node name to paste
  @param bClipboard: bool, input: show clipboard
  @param sCurrentSelectedNodeKey: string, input the key of the last node that was selected
  @param sParentKeyToAddInNewKey: string, input the parent key to use to build the new key
  @param sTitle: string, input: the title
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_renameDevice(string sGraphTree, string sBar,
                                        string sSelectedKey, string sDpName, string sDisplayNodeName,
                                        string sCompositeLabel, string sParentKey, string sParentDpName,
                                        string sParentNodeName, string sPasteNodeDpName,
                                        string sPasteNodeName, bool bClipboard,
                                        string sCurrentSelectedNodeKeyBis, string sParentKeyToAddInNewKey,
                                        string sTitle, dyn_string &exInfo)
{
  string sTopDisplayName;
  string sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
                      sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey;
  string sFunctionName, sTextToShow;
  bool bNavigation=false, bLink=false, bDollar=false, bGranted = true, bUserPanelOpened=false;

//DebugN("Rename");
  unTree_getGlobalValue(sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
                  sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey);
  unTreeWidget_rename(sGraphTree, sSelectedKey, sParentKey, sCompositeLabel,
                  sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, bClipboard,
                  sTitle, sCurrentSelectedNodeKey, sParentKeyToAddInNewKey, exInfo);


  unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
  unTree_getUserPanelOpened(bUserPanelOpened);
  unTree_isUserAllowed(bGranted, exInfo);

  if(sPasteNodeDpName == sDpName) { // the current selected node to delete is also in the paste
    unTree_setHierarchyPasteButtonGlobalVariable(false);
    unTree_setHierarchyPasteButtonState("", false);
    unTree_setPasteGlobalValue("", "");
//DebugN("Rst paste");
  }
  if(unTree_isFromNodeList() || (sCurrentDpName == sParentDpName)) {
//DebugN("from  nodelist");
    sDpName = sParentDpName;
    _fwTree_getParent(sDpName, sParentDpName);
    if(sParentDpName == unTreeWidget_treeNodeDpPrefix+sTopDisplayName)
      sParentDpName = "";
    unTree_getAndShow(sDpName, sParentDpName, bUserPanelOpened, false, exInfo);
    unTree_setUserPanelOpened(bUserPanelOpened);
  }
}

// unTrendTree_getDeviceTitle
/**
Purpose:
get if the device title

  @param sDpName: string, input: the node dp name of the node
  @param sTitle: string, output: the title
  @param sDeviceName: string, output: the device name linke to the node
  @param sDeviceType: string, output: the device type

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_getDeviceTitle(string sDpName, string &sTitle, string &sDeviceName, string &sDeviceType)
{
  dyn_string exInfo;

  sTitle = "";
  sDeviceName = "";
  sDeviceType = "";
  fwTree_getNodeDevice(_fwTree_getNodeName(sDpName), sDeviceName, sDeviceType, exInfo);
  if(dpExists(sDeviceName)) {
    switch(sDeviceType) {
      case UN_TRENDTREE_PLOT:
        fwTrending_getPlotTitle(sDeviceName, sTitle);
        break;
      case UN_TRENDTREE_PAGE:
        fwTrending_getPageTitle(sDeviceName, sTitle);
        break;
      default:
        break;
    }
  }
}
// unTrendTree_getHierarchyState
/**
@reviewed 2018-06-22 @whitelisted{BackgroundProcessing}
Purpose:
Function to get the state of the 3 add buttons of the hierarchy cascade button and the rename.

  @param dsReturnData: dyn_string, output: the state are returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_getHierarchyState(dyn_string &dsReturnData, string sDisplayNodeName, string sDpName)
{
  dyn_string exInfo;
  string sDeviceName, sDeviceType;
  bool isPredefinedTrend=false, isUserDefinedTrend=false;

  isPredefinedTrend = (sDisplayNodeName == UN_TRENDTREE_PREDEFINED_TRENDS);
  isUserDefinedTrend = (sDisplayNodeName == UN_TRENDTREE_USERDEFINED_TRENDS);

  if(!isPredefinedTrend && !isUserDefinedTrend) {
    fwTree_getNodeDevice(_fwTree_getNodeName(sDpName), sDeviceName, sDeviceType, exInfo);
    if(dpExists(sDeviceName) && ((sDeviceType == UN_TRENDTREE_PLOT) || (sDeviceType == UN_TRENDTREE_PAGE)) )
      dsReturnData = makeDynString("TRUE","TRUE", "TRUE", "FALSE");
    else
      dsReturnData = makeDynString("TRUE","TRUE", "TRUE", "TRUE");
  }
  else
    dsReturnData = makeDynString("TRUE","TRUE", "TRUE", "FALSE");
}

// unTrendTree_getDeviceState
/**
@reviewed 2018-06-22 @whitelisted{BackgroundProcessing}
Purpose:
Function to get the state of the buttons of the device cascade button: the link, configuration, navigation, set dollar parameter, and link allowed

  @param dsReturnData: dyn_string, output: the state are returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param sDeviceName: string, input: the device name linke to the node
  @param sDeviceType: string, input: the device type

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_getDeviceState(dyn_string &dsReturnData, string sDisplayNodeName, string sDpName, string sDeviceName, string sDeviceType)
{
  bool isPredefinedTrend=false, isUserDefinedTrend=false;

  isPredefinedTrend = (sDisplayNodeName == UN_TRENDTREE_PREDEFINED_TRENDS);
  isUserDefinedTrend = (sDisplayNodeName == UN_TRENDTREE_USERDEFINED_TRENDS);

  if(!isPredefinedTrend && !isUserDefinedTrend)
    dsReturnData = makeDynString("TRUE","TRUE", "TRUE", "TRUE");
  else
    dsReturnData = makeDynString("false","false", "false", "false");
}

// unTrendTree_getDisplayNodeName
/**
@reviewed 2018-06-22 @whitelisted{BackgroundProcessing}
Purpose:
Function to get the display node name to show in the unTree.

  @param dsReturnData: dyn_string, output: the state are returned here
  @param sDisplayNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param sDeviceName: string, input: the device name linke to the node
  @param sDeviceType: string, input: the device type

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_getDisplayNodeName(dyn_string &dsReturnData, string sDisplayNodeName, string sDpName, string sDeviceName, string sDeviceType)
{
  string sNode, sTemp;

  switch(sDeviceType) {
    case UN_TRENDTREE_PLOT:
//      fwTrending_getPlotTitle(sDeviceName, sTemp);
//      sNode = sDisplayNodeName + UN_TREE_DEVICE_SEPARATOR+ sTemp;
      sNode = sDisplayNodeName;
      break;
    case UN_TRENDTREE_PAGE:
//      fwTrending_getPageTitle(sDeviceName, sTemp);
//      sNode = sDisplayNodeName + UN_TREE_DEVICE_SEPARATOR+sTemp;
      sNode = sDisplayNodeName;
      break;
    default:
      sNode = sDisplayNodeName;
      break;
  }
  dsReturnData = makeDynString(sNode);
}

// unTrendTree_openPanelGetDollarParam
/**
Purpose:
Function to get the get the dollar parameter to be given to the panel to show.

  @param sPanel: string, input: the panel
  @param sDpName: string, input: the dp name of the node name
  @param sDeviceName: string, input: the device name linke to the node
  @param sDeviceType: string, input: the device type
  @param return value: dyn_string, the dollar parameter are returned here
  @param sParentDpName: string, input: the parent dp name of the node name
  @param bDetail: bool, input: true, show detail panel

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
dyn_string unTrendTree_openPanelGetDollarParam(string sPanel, string sDpName, string sDeviceName, string sDeviceType, string sParentDpName, bool bDetail)
{
  dyn_string dsReturnData;

//DebugN(sPanel, bDetail);
  switch(sDeviceType) {
    case UN_TRENDTREE_PLOT:
      if(sPanel == UN_TRENDTREE_PLOT_CONFIGURATION_PANEL+".pnl") {
        if(bDetail)
          dsReturnData = makeDynString("$bHierarchyBrowser:TRUE", "$Command:", "$sDpName:" + sDeviceName, "$dsShowButtons:" + makeDynString("ok", "apply", "cancel"));
        else
          dsReturnData = makeDynString("$bHierarchyBrowser:TRUE", "$Command:", "$sDpName:" + sDeviceName, "$dsShowButtons:" + makeDynString("apply"));
      }
      else {
        dsReturnData = makeDynString("$sDpName:" + sDpName,"$sParentDpName:" + sParentDpName);
      }
      break;
    case UN_TRENDTREE_PAGE:
      if(sPanel == UN_TRENDTREE_PAGE_CONFIGURATION_PANEL+".pnl") {
        if(bDetail)
          dsReturnData = makeDynString("$bHierarchyBrowser:TRUE", "$Command:", "$sDpName:"+sDeviceName, "$WorkPageName:", "$dsShowButtons:" + makeDynString("ok", "apply", "cancel"));
        else
          dsReturnData = makeDynString("$bHierarchyBrowser:TRUE", "$Command:", "$sDpName:"+sDeviceName, "$WorkPageName:", "$dsShowButtons:" + makeDynString("apply"));
      }
      else {
        dsReturnData = makeDynString("$sDpName:" + sDpName,"$sParentDpName:" + sParentDpName);
      }
      break;
    case NO_DEVICE:
    default:
      dsReturnData = makeDynString("$sDpName:" + sDpName,"$sParentDpName:" + sParentDpName);
      break;
  }
//DebugN(sPanel, bDetail, dsReturnData);
  return dsReturnData;
}

// unTrendTree_nodeListLoadImage
/**
@reviewed 2018-06-22 @whitelisted{BackgroundProcessing}
Purpose:
load the image class of the nodeList panel.

  @param sNodeList: string, input: the name of the unTreeWidget graphical element

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_nodeList_loadImage(string sNodeList)
{
  dyn_string exceptionInfo;

  unTrendTree_loadImage("", sNodeList, exceptionInfo);
}

// unTrendTree_nodeList_EventNodeRigthClickMenu
/**
Purpose:
@reviewed 2018-06-22 @whitelisted{Callback}
popup menu of the nodeRigthClick event in the nodeList panel.

  @param dsMenuReturn: string, output: the popup menu
  @param sDisplayNodeName: string, input: the display node name of the node
  @param sNodeName: string, input: the node name of the node
  @param sDeviceName: string, input: the device name linked to the node
  @param sDeviceType: string, input: the device type of the device name

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_nodeList_EventNodeRigthClickMenu(dyn_string &dsMenuReturn, string sDisplayNodeName, string sNodeName, string sDeviceName, string sDeviceType)
{
  bool bGranted = false;
  int iAllowed;
  dyn_string dsMenu, exInfo;
  bool isPredefinedTrend=false, isUserDefinedTrend=false;

  isPredefinedTrend = (sDisplayNodeName == UN_TRENDTREE_PREDEFINED_TRENDS);
  isUserDefinedTrend = (sDisplayNodeName == UN_TRENDTREE_USERDEFINED_TRENDS);

  unTree_isUserAllowed(bGranted, exInfo);
  if(bGranted)
    iAllowed=1;
  else
    iAllowed = 0;

  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  if(!isPredefinedTrend && !isUserDefinedTrend) {
    dynAppend(dsMenu,"PUSH_BUTTON, Cut: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_cut+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, Copy: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copy+", "+iAllowed);
    if(dpExists(sDeviceName) && ((sDeviceType == UN_TRENDTREE_PLOT) || (sDeviceType == UN_TRENDTREE_PAGE))) {
      dynAppend(dsMenu,"SEPARATOR");
      dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", "+iAllowed);
      dynAppend(dsMenu,"PUSH_BUTTON, Link to plot/page, "+UN_TREE_linkToDevice+", "+iAllowed);
      dynAppend(dsMenu,"PUSH_BUTTON, Unlink from plot/page, "+UN_TREE_unLinkToDevice+", "+iAllowed);
      dynAppend(dsMenu,"SEPARATOR");
      dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", "+iAllowed);
      dynAppend(dsMenu,"PUSH_BUTTON, Rename, "+unTreeWidget_popupMenu_rename+", 0");
    }
    else {
      dynAppend(dsMenu,"SEPARATOR");
      dynAppend(dsMenu,"PUSH_BUTTON, Panel configuration, "+UN_TREE_horizontalNavigation+", 0");
      dynAppend(dsMenu,"PUSH_BUTTON, Link to plot/page, "+UN_TREE_linkToDevice+", "+iAllowed);
      dynAppend(dsMenu,"PUSH_BUTTON, Unlink from plot/page, "+UN_TREE_unLinkToDevice+", 0");
      dynAppend(dsMenu,"SEPARATOR");
      dynAppend(dsMenu,"PUSH_BUTTON, Remove..., "+unTreeWidget_popupMenu_remove+", "+iAllowed);
      dynAppend(dsMenu,"PUSH_BUTTON, Rename, "+unTreeWidget_popupMenu_rename+", "+iAllowed);
    }
  }
  else {
    dynAppend(dsMenu,"PUSH_BUTTON, Copy: "+sDisplayNodeName+", "+unTreeWidget_popupMenu_copy+", "+iAllowed);
  }
  dsMenuReturn = dsMenu;
}

// unTrendTree_nodeList_EventRigthClickMenu
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose:
popup menu of the rigthClick event in the nodeList panel.

  @param dsMenuReturn: string, output: the popup menu
  @param sDisplayNodeName: string, input: the display node name of the node
  @param sNodeName: string, input: the display node name of the node
  @param sDpName: string, input: the node name of the node
  @param sPasteNodeName: string, input: the node name to paste
  @param sPasteNodeDpName: string, input: the node name dp name to paste

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_nodeList_EventRigthClickMenu(dyn_string &dsMenuReturn, string sDisplayNodeName, string sNodeName, string sDpName, string sPasteNodeName, string sPasteNodeDpName)
{
  bool isInClipboard, bGranted = false;
  int isClipboard, iAllowed;
  dyn_string exInfo, dsMenu, dsChildren;

  unTree_isUserAllowed(bGranted, exInfo);
  if(bGranted)
    iAllowed = 1;
  else
    iAllowed = 0;

  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sDisplayNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  isClipboard = fwTree_isClipboard(sNodeName, exInfo);
  if(!isClipboard) {
    dynAppend(dsMenu,"PUSH_BUTTON, New folder, "+UN_TREE_newFolder+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, New plot, "+UN_TRENDTREE_newPlot+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, New page, "+UN_TRENDTREE_newPage+", "+iAllowed);
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Add plot, "+UN_TRENDTREE_addPlot+", "+iAllowed);
    dynAppend(dsMenu,"PUSH_BUTTON, Add page, "+UN_TRENDTREE_addPage+", "+iAllowed);
    dynAppend(dsMenu,"SEPARATOR");
  }

  isInClipboard = fwTree_isInClipboard(sNodeName, exInfo);

  if((sPasteNodeName != "") && (!isInClipboard) && (sPasteNodeDpName!=sDpName)) {
    dynAppend(dsMenu,"PUSH_BUTTON, Paste: "+sPasteNodeName+", "+unTreeWidget_popupMenu_paste+", "+iAllowed);
  }
  else
    dynAppend(dsMenu,"PUSH_BUTTON, Paste, "+unTreeWidget_popupMenu_paste+", 0");

  fwTree_getChildren(sNodeName, dsChildren, exInfo);
  if(dynlen(dsChildren) > 0) {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", "+iAllowed);
  }
  else {
    dynAppend(dsMenu,"SEPARATOR");
    dynAppend(dsMenu,"PUSH_BUTTON, Reorder, "+unTreeWidget_popupMenu_reorder+", 0");
  }
  dsMenuReturn = dsMenu;
}

// unTrendTree_nodeList_EventDblClickPreExecute
/**
@reviewed 2018-06-22 @whitelisted{Callback}
Purpose:
post execution of the event dbl click in the nodeList: get the current plot/page title.

  @param sNodeName: string, input: the display node name of the node
  @param return value: dyn_string, output: the current plot/page title

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
dyn_string unTrendTree_nodeList_EventDblClickPreExecute(string sNodeName)
{
  string sDeviceName, sDeviceType, sTitle;
  dyn_string dsRetData, exInfo;
//DebugN(sNodeName);

  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
  if(dpExists(sDeviceName)) {
    switch(sDeviceType) {
      case UN_TRENDTREE_PLOT:
        fwTrending_getPlotTitle(sDeviceName, sTitle);
        dsRetData = sTitle;
        break;
      case UN_TRENDTREE_PAGE:
        fwTrending_getPageTitle(sDeviceName, sTitle);
        dsRetData = sTitle;
        break;
      default:
        break;
    }
  }

  return dsRetData;
}

// unTrendTree_nodeList_EventDblClickPostExecute
/**
@reviewed 2018-06-22 @whitelisted{BackgroundProcessing}
Purpose:
post execution of the event dbl click in the nodeList: return true to redraw the nodeList, false to do nothing.

  @param sNodeName: string, input: the display node name of the node
  @param sCompositeLabel: string, input: the current composite label
  @param dsData: dyn_string, input: the data from the unTrendTree_nodeList_EventDblClickPreExecute function
  @param exInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_nodeList_EventDblClickPostExecute(string sNodeName, string sCompositeLabel, dyn_string dsData, dyn_string &exInfo)
{
  string sDeviceName, sDeviceType, sTitle;

//DebugN(sNodeName, sCompositeLabel);

  if(dynlen(dsData) == 1) {
    fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);
    if(dpExists(sDeviceName)) {
      switch(sDeviceType) {
        case UN_TRENDTREE_PLOT:
          fwTrending_getPlotTitle(sDeviceName, sTitle);
          break;
        case UN_TRENDTREE_PAGE:
          fwTrending_getPageTitle(sDeviceName, sTitle);
          break;
        default:
          break;
      }
    }
    if((sTitle != dsData[1]) && (sTitle != ""))
      unTrendTree_nodeList_EventDblClickTriggerEvent(sCompositeLabel);
  }
}

// unTrendTree_nodeList_EventDblClickTriggerEvent
/**
Purpose:
function to triger the event in the case of the event dbl click in the nodeList:

  @param sCompositeLabel: string, input: the current composite label

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_nodeList_EventDblClickTriggerEvent(string sCompositeLabel)
{
  string sTreeDpName, sTreeDpNameKey, sTreeParentDpName;
  string sTreeParentDpNameKey, sTreeDeviceName, sTreeDeviceType;
  string sTreeCompositeLabel, sTreeCurrentSelectedNodeKey;
  string sTreeDisplayNodeName, sTreeParentNodeName;
  string sDpName, sDisplayNodeName, sParentDpName, sParentNodeName, sNodeName, sDeviceName, sDeviceType;
  string sDpNameKey, sParentKeyToAddInNewKey, sNodeCompositeLabel, sPasteNodeName, sPasteNodeDpName;
  dyn_string exInfo;

// build menu on node right click: remove, rename, cut, copy, link/unlink, panel navigation
  unTree_getGlobalValue(sTreeDpName, sTreeDpNameKey, sTreeParentDpName,
                            sTreeParentDpNameKey, sTreeDeviceName, sTreeDeviceType,
                            sTreeCompositeLabel, sTreeCurrentSelectedNodeKey);

/*
DebugN("unTrendTree_nodeList_EventDblClickTriggerEvent", sCompositeLabel, sTreeDpName, sTreeDpNameKey, sTreeParentDpName,
                            sTreeParentDpNameKey, sTreeDeviceName, sTreeDeviceType,
                            sTreeCompositeLabel);
*/
  if(sTreeCompositeLabel != "") {
    unTreeWidget_getNodeNameAndNodeDp(sTreeCompositeLabel, sTreeDpName, sTreeDisplayNodeName,
                                sTreeParentDpName, sTreeParentNodeName);
  }
  else {
    sTreeDisplayNodeName = fwTree_getNodeDisplayName(_fwTree_getNodeName(sTreeDpName), exInfo);
  }

  unTree_getPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
  unTreeWidget_getNodeNameAndNodeDp(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
//DebugN(sCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);
  sNodeName = _fwTree_getNodeName(sDpName);
  sDisplayNodeName = fwTree_getNodeDisplayName(sNodeName, exInfo);
  sNodeCompositeLabel = sTreeDisplayNodeName+unTreeWidget_labelSeparator+sTreeDpName+
                          unTreeWidget_compositeLabelSeparator+
                          sDisplayNodeName+unTreeWidget_labelSeparator+sDpName;
  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);

//    DebugN(sTreeCompositeLabel, sCompositeLabel, sNodeCompositeLabel, sDpName, sDisplayNodeName, sParentDpName, sParentNodeName);

// build the key in the tree
  sDpNameKey = sNodeCompositeLabel+
                unTreeWidget_keySeparator+
                sTreeDpNameKey;
// if no composite label then it means that this is a child of WindowTree. so reset the parent key for the handelEventNodeRigthClick
  sParentKeyToAddInNewKey = sTreeDpNameKey;
  if(sTreeCompositeLabel == "") {
    sTreeDpNameKey = "";
  }

/*
DebugN("DblClick", UN_TRENDTREE_dblClick, makeDynString(sDpNameKey, sDpName, sDisplayNodeName,
                                              sNodeCompositeLabel, sTreeDpNameKey, sTreeDpName,
                                              sTreeDisplayNodeName, sPasteNodeDpName,
                                              sPasteNodeName, sTreeCurrentSelectedNodeKey, sParentKeyToAddInNewKey));
*/

  unTree_setNodeList(true);
  unTree_triggerEventNodeRigthClick(UN_TRENDTREE_dblClick, makeDynString(sDpNameKey, sDpName, sDisplayNodeName,
                                          sNodeCompositeLabel, sTreeDpNameKey, sTreeDpName,
                                          sTreeDisplayNodeName, sPasteNodeDpName,
                                          sPasteNodeName, sTreeCurrentSelectedNodeKey, sParentKeyToAddInNewKey), exInfo);
}
// unTrendTree_addDevice
/**
Purpose:
Add a panel.

  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name to add the panel in
  @param sDisplayNodeName: string, input: the display node name
  @param bTop: bool, input: true add as top node in the unTreeWidget
  @param bDone: bool, output: true the panel was correctly added, false, it was not
  @param bPlot: bool, input: true create the plot, false create the page
  @param exInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_addDevice(string sGraphTree, string sBar, string sSelectedKey, string sDpName,
                        string sDisplayNodeName, bool bTop, bool &bDone, bool bPlot, dyn_string &exInfo)
{
  dyn_float dfFloat;
  dyn_string dsText;
  int i, len;

  unGraphicalFrame_ChildPanelOnCentralModalReturn ("vision/unTreeWidget/unTreeWidgetAddNode.pnl", "Add Plot / Page", makeDynString("$sTextQuestion:New plot / page name:"), dfFloat, dsText);
  if(dynlen(dfFloat)>0) {
    if(dfFloat[1] == 1) {
  // new node.
      bDone = true;
      len = dynlen(dsText);
      for(i=1;i<=len;i++) {
        if(dsText[i] != "") {

          if(dpExists(dsText[i])) {
            if(bPlot) {
        // check that it is really a plot
              if(dpTypeName(dsText[i]) != UN_TRENDTREE_PLOT)
                fwException_raise(exInfo, "ERROR", getCatStr("unGeneral", "TREEWRONGTYPE")+ ", "+dsText[i], "");
            }
            else {
        // check it is really a page
              if(dpTypeName(dsText[i]) != UN_TRENDTREE_PAGE)
                fwException_raise(exInfo, "ERROR", getCatStr("unGeneral", "TREEWRONGTYPE")+ ", "+dsText[i], "");
            }
          }
          if(dynlen(exInfo) <= 0) {
            if(!dpExists(dsText[i])) {
              if(bPlot)
                fwTrending_createPlot(dsText[i], exInfo);
              else
                fwTrending_createPage(dsText[i], exInfo);
            }
            if(dynlen(exInfo) <= 0) {
              unTrendTree_addDeviceInHierarchy(dsText[i], dsText[i], sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, bTop, bDone, bPlot, exInfo);
            }
          }
        }
      }
    }
  }
}

// unTrendTree_addDeviceInHierarchy
/**
Purpose:
Add a panel.

  @param sDeviceDpName: string the device data point name to add
  @param sTitle: string the device name to add
  @param sGraphTree: string, input: the name of the unTreeWidget graphical element
  @param sBar: string, input: the name of the the Microsoft ProgressBar Control, version 6.0 graphical element
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param sDpName: string, input: data point name to add the panel in
  @param sDisplayNodeName: string, input: the display node name
  @param bTop: bool, input: true add as top node in the unTreeWidget
  @param bDone: bool, output: true the panel was correctly added, false, it was not
  @param bPlot: bool, input: true create the plot, false create the page
  @param exInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_addDeviceInHierarchy(string sDeviceDpName, string sTitle, string sGraphTree, string sBar, string sSelectedKey, string sDpName,
                                    string sDisplayNodeName, bool bTop, bool &bDone, bool bPlot, dyn_string &exInfo)
{
  string sNodeName, sDeviceName, sNodeNameAdded, sDpNodeNameAdded, sDisplayNodeNameAdded, sKey;
  dyn_string dsUserData;

//DebugN("unTrendTree_addDeviceInHierarchy", sDeviceDpName, sSelectedKey, sDpName, sDisplayNodeName);

  unTree_add(sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, sTitle, bTop, sNodeNameAdded, exInfo);
//DebugN("before check", exInfo);
//  DebugN("after check", exInfo);
  if(dynlen(exInfo)<=0) {
    sDeviceName = dpSubStr(sDeviceDpName, DPSUB_SYS_DP);
//  DebugN("dev", sDeviceName);

// make the new dpName
    sDpNodeNameAdded = _fwTree_makeNodeName(sNodeNameAdded);
    sDisplayNodeNameAdded = fwTree_getNodeDisplayName(sNodeNameAdded, exInfo);
// make the new key
    sKey = sDisplayNodeName+unTreeWidget_labelSeparator+sDpName+
                    unTreeWidget_compositeLabelSeparator+
                    sDisplayNodeNameAdded+unTreeWidget_labelSeparator+sDpNodeNameAdded+
                    unTreeWidget_keySeparator+
                    sSelectedKey;

//DebugN("ans", sGraphTree, sBar, sSelectedKey, sDpName, sDisplayNodeName, sTitle, sDeviceName, bTop, sNodeNameAdded, bPlot);
    if(bPlot)
      unTree_linkToDevice(sGraphTree, sDpNodeNameAdded, sKey, sDeviceName, UN_TRENDTREE_PLOT, dsUserData, exInfo);
    else
      unTree_linkToDevice(sGraphTree, sDpNodeNameAdded, sKey, sDeviceName, UN_TRENDTREE_PAGE, dsUserData, exInfo);

    bDone = true;
  }
}

// unTrendTree_handleEventNodeClickOperation
/**
Purpose:
Basic unTrendTree function: handle the event node left click

  @param sNodeName: string, input: the node name
  @param sDpName: string, input: data point name
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unTrendTree_handleEventNodeClickOperation(string sNodeName, string sDpName, string sSelectedKey, dyn_string &exInfo)
{
  string sDeviceName, sDeviceType, sDollarParam;
  string sPanelFileName, buttonPanel;

  fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);

//DebugN("unTrendTree_handleEventNodeClickOperation", sNodeName, sDpName, sSelectedKey, sDeviceName, sDeviceType);
  switch(sDeviceType) {
    case UN_TRENDTREE_PLOT:
      if(dpExists(sDeviceName)) {
        sDollarParam = "$PageName:;$OpenPageName:;$bEdit:TRUE;$PlotName:"+sDeviceName/*+";"*/;
// get the panel from the device definition, check it exists and show it.
        sPanelFileName = "fwTrending/fwTrendingPlot.pnl";
        if((getPath(PANELS_REL_PATH,sPanelFileName)!="") && (sPanelFileName != "")){
          //FLAG NEWHMI
          if (globalExists("g_unHMI_RunsInNewHMI")) {
            shape mainWindow = getShape(UNHMI_MAIN_MODULE_NAME + "." + UNHMI_MAIN_PANEL_NAME + ":");
            int currentModule = mainWindow.unHMI_getCurrentModule();
            if ((currentModule >=1) && (currentModule <= UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES)) {
              unHMI_openPreviewModuleHelper(sPanelFileName, currentModule, sDollarParam);
            }  else {
              for (int i=1; i<=UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES; i++) {
                if (mainWindow.unHMI_getIsPreviewModuleOpen(i)){
                  unHMI_openPreviewModuleHelper(sPanelFileName, i, sDollarParam);
                  break;
                }
              }
            }
            return;
          } else {
            unGraphicalFrame_showUserPanel(sPanelFileName, sDollarParam, sDeviceName);
            unGenericDpFunctions_getContextPanel(sDeviceName, buttonPanel, exInfo);
            if(buttonPanel != "")
              unGraphicalFrame_showContextualPanel(buttonPanel, sDeviceName);
          }
        }
        else {
          fwException_raise(exInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL") + sPanelFileName,"");
        }
      }
      break;
    case UN_TRENDTREE_PAGE:
      if(dpExists(sDeviceName)) {
        sDollarParam = "$PlotName:;$OpenPageName:;$bEdit:FALSE;$PageName:"+sDeviceName/*+";"*/;
// get the panel from the device definition, check it exists and show it.
        sPanelFileName = "fwTrending/fwTrendingPage.pnl";
        if((getPath(PANELS_REL_PATH,sPanelFileName)!="") && (sPanelFileName != "")){
          //FLAG NEWHMI
          if (globalExists("g_unHMI_RunsInNewHMI")) {
            shape mainWindow = getShape(UNHMI_MAIN_MODULE_NAME + "." + UNHMI_MAIN_PANEL_NAME + ":");
            int currentModule = mainWindow.unHMI_getCurrentModule();
            if ((currentModule >=1) && (currentModule <= UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES)) {
              unHMI_openPreviewModuleHelper(sPanelFileName, currentModule, sDollarParam);
            } else {
              for (int i=1; i<=UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES; i++) {
                if (mainWindow.unHMI_getIsPreviewModuleOpen(i)){
                  unHMI_openPreviewModuleHelper(sPanelFileName, i, sDollarParam);
                  break;
                }
              }
            }
            return;
          } else {
            unGraphicalFrame_showUserPanel(sPanelFileName, sDollarParam, sDeviceName);
            unGenericDpFunctions_getContextPanel(sDeviceName, buttonPanel, exInfo);
            if(buttonPanel != "")
              unGraphicalFrame_showContextualPanel(buttonPanel, sDeviceName);
          }
        }
        else {
          fwException_raise(exInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL") + sPanelFileName,"");
        }
      }
      break;
    case NO_DEVICE:
    default:
      break;
  }
}

// unTrendTree_eventNodeRigthClickOperationMenu
/**
Purpose:
Function to build the popup menu when for the eventNodeRigthClickOperationMenu on the unTreeWidget.

  @param dsMenu: dyn_string, input: the popup menu is returned here
  @param sNodeName: string, input: the display node name
  @param sDpName: string, input: the dp name of the node name
  @param exInfo: dyn_string, output: errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unTrendTree_eventNodeRigthClickOperationMenu(dyn_string &dsMenu, string sNodeName, string sDpName, dyn_string &exInfo)
{
//DebugN("unTrendTree_eventNodeRigthClickOperationMenu");
  dynAppend(dsMenu, "PUSH_BUTTON, --- "+sNodeName+" ---, "+unTreeWidget_popupMenu_nodeName+", 0");
  dynAppend(dsMenu,"SEPARATOR");
  dynAppend(dsMenu,"PUSH_BUTTON, Open on top, "+UN_TRENDTREE_OPENASPOPUP+", 1");
  dynAppend(dsMenu,"SEPARATOR");
  //FLAG NEWHMI
  if (globalExists("g_unHMI_RunsInNewHMI")) {
    shape mainWindow = getShape(UNHMI_MAIN_MODULE_NAME + ".mainWindow:");
    for (int i=1; i<=UNHMI_MAX_NUMBER_OF_PREVIEW_MODULES; i++) {
      if (mainWindow.unHMI_getIsPreviewModuleOpen(i)){
        dynAppend(dsMenu,"PUSH_BUTTON, Add to module" + i + ", " + (i+UN_TRENDTREE_openEditor) +", 1");
      }
    }
  }

 //append menu entry for opening configuration panel, protected by AC
 bool bGranted, bFileExists;
 string sAccessLevel;
 dyn_string exceptionInfo;
  sAccessLevel = unGenericButtonFunctionsHMI_getPanelAccessLevel(unTrendTree_FILENAME_CONFIGPANEL, bFileExists);
  unGenericButtonFunctionsHMI_isAccessAllowed(unTrendTree_FILENAME_CONFIGPANEL,
                                              UN_GENERIC_USER_ACCESS,
                                              sAccessLevel,
                                              bGranted,
                                              exceptionInfo);
  dynAppend(dsMenu,"PUSH_BUTTON, Trend Tree Configuration..., "+UN_TRENDTREE_openEditor+", "+(int)(bFileExists && bGranted));
}

// unTrendTree_handleEventNodeRigthClickOperation
/**
Purpose:
Basic unWindowTree function: handle the event node rigth click

  @param sNodeName: string, input: the node name
  @param sDpName: string, input: data point name
  @param sSelectedKey: string, input: the key of the node that was selected in the unTreeWidget, the format of the sSelectedKey is sContext|childrenLabel@parentKey
  @param iAnswer: int, input: selected menu
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.

@Reviewed 2018-08-14 @Whitelisted{Callback}

*/
unTrendTree_handleEventNodeRigthClickOperation(string sNodeName, string sDpName, string sSelectedKey, int iAnswer, dyn_string &exInfo)
{
  string sDeviceName, sDeviceType, sDollarParam;
  string sPanelFileName;
  dyn_string dsDollarParam;
  string sGraphTree = "tree";
  shape aShape = getShape(sGraphTree);
  dyn_float dfRet;
  dyn_string dsRet;

//DebugN("unTrendTree_handleEventNodeRigthClickOperation", sNodeName, sDpName, sSelectedKey, sDeviceName, sDeviceType, iAnswer);
  switch(iAnswer){
    case UN_TRENDTREE_OPENASPOPUP:

      fwTree_getNodeDevice(sNodeName, sDeviceName, sDeviceType, exInfo);

      switch(sDeviceType) {
        case UN_TRENDTREE_PLOT:
          if(dpExists(sDeviceName)) {
            sDollarParam = "$PageName:;$OpenPageName:;$bEdit:TRUE;$PlotName:"+sDeviceName/*+";"*/;
    // get the panel from the device definition, check it exists and show it.
            sPanelFileName = "fwTrending/fwTrendingPlot.pnl";
            if((getPath(PANELS_REL_PATH,sPanelFileName)!="") && (sPanelFileName != "")){
              dsDollarParam = strsplit(sDollarParam, ";");
              ModuleOnWithPanel(sNodeName, 0,150,0,0,0,0,0, sPanelFileName, sNodeName, dsDollarParam);
              stayOnTop(true, sNodeName);
            }
            else {
              fwException_raise(exInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL") + sPanelFileName,"");
            }
          }
          break;
        case UN_TRENDTREE_PAGE:
          if(dpExists(sDeviceName)) {
            sDollarParam = "$PlotName:;$OpenPageName:;$bEdit:FALSE;$PageName:"+sDeviceName/*+";"*/;
    // get the panel from the device definition, check it exists and show it.
            sPanelFileName = "fwTrending/fwTrendingPage.pnl";
            if((getPath(PANELS_REL_PATH,sPanelFileName)!="") && (sPanelFileName != "")){
              dsDollarParam = strsplit(sDollarParam, ";");
              ModuleOnWithPanel(sNodeName, 0,150,0,0,0,0,0, sPanelFileName, sNodeName, dsDollarParam);
              stayOnTop(true, sNodeName);
            }
            else {
              fwException_raise(exInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL") + sPanelFileName,"");
            }
          }
          break;
        case NO_DEVICE:
        default:
          break;
      }
      break;

    case UN_TRENDTREE_openEditor:
      unGraphicalFrame_ChildPanelOnCentralReturn(unTrendTree_FILENAME_CONFIGPANEL,"TrendTreeConfiguration",makeDynString(),dfRet, dsRet);
      delay(0,100);
      //refresh
      unTreeWidget_setPropertyValue(aShape, "clearTree", true, true);
      unTree_EventInitializeOperation(sGraphTree, "treeBar", false);
      if(g_iSortMode!=unTreeWidget_popupMenu_sortCustom)
      {
        unTreeWidget_setPropertyValue(aShape, "sort", g_iSortMode, true);
      }
    break;
    //NEWHMI
    //Case for docked module
    case UN_TRENDTREE_openInPreview1:
      _openInPreview(sNodeName, 1);
    break;
    case UN_TRENDTREE_openInPreview2:
      _openInPreview(sNodeName, 2);
    break;
    case UN_TRENDTREE_openInPreview3:
      _openInPreview(sNodeName, 3);
    break;
    case UN_TRENDTREE_openInPreview4:
      _openInPreview(sNodeName, 4);
    break;
  }
}

private void _openInPreview(string nodeName, int number) {
  string deviceName, deviceType, sDollarParam, sPanelFileName;
  dyn_string exInfo;
  fwTree_getNodeDevice(nodeName,  deviceName, deviceType, exInfo);
  if (deviceType == UN_TRENDTREE_PLOT) {
    sDollarParam = "$PageName:;$OpenPageName:;$bEdit:TRUE;$PlotName:"+deviceName/*+";"*/;
    sPanelFileName = "fwTrending/fwTrendingPlot.pnl";
  } else if (deviceType == UN_TRENDTREE_PAGE) {
    sDollarParam = "$PlotName:;$OpenPageName:;$bEdit:FALSE;$PageName:"+deviceName/*+";"*/;
    sPanelFileName = "fwTrending/fwTrendingPage.pnl";
  }
  if(dpExists(deviceName)) {
    unHMI_openPreviewModuleHelper(sPanelFileName, number, sDollarParam);
  } else {
    fwException_raise(exInfo, "WARNING", getCatStr("unGeneral", "TREENOPANEL") + deviceName,"");
  }
}

// unTrendTree_deviceConfiguration_postExecution
/**
@reviewed 2018-06-22 @whitelisted{BackgroundProcessing}
Purpose:
Function called by the Plot configuration panel and Page configuration panel at the end of the handling of the Apply button code.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_deviceConfiguration_postExecution()
{
  string sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName;
  string sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey;
  dyn_string dsReturnData, exInfo;
  string sCurrentNodeDisplayName, sCurrentNodeName;
  string sPasteNodeName, sPasteNodeDpName, sParentKeyToAddInNewKey;
  string sCurrentParentDisplayNodeName, sTopDisplayName, sTitle;

  if(shapeExists("treeConfigTitle")) {
    unTree_getGlobalValue(sCurrentDpName, sCurrentSelectedKey, sCurrentParentDpName,
                      sCurrentParentKey, sCurrentDeviceName, sCurrentDeviceType, sCurrentCompositeLabel, sCurrentSelectedNodeKey);

    sCurrentNodeName = _fwTree_getNodeName(sCurrentDpName);
    sCurrentNodeDisplayName = fwTree_getNodeDisplayName(sCurrentNodeName, exInfo);


    if(dpExists(sCurrentDeviceName)) {
      switch(sCurrentDeviceType) {
        case UN_TRENDTREE_PLOT:
          fwTrending_getPlotTitle(sCurrentDeviceName, sTitle);
          break;
        case UN_TRENDTREE_PAGE:
          fwTrending_getPageTitle(sCurrentDeviceName, sTitle);
          break;
        default:
          break;
      }
    }
    if((sTitle != sCurrentNodeDisplayName) && (sTitle != "")) {
      unTree_getPasteGlobalValue(sPasteNodeName, sPasteNodeDpName);
      unTree_getConfigurationVariable("", UN_TREE_TREENODENAME, sTopDisplayName);
      if(sCurrentParentKey == "") { // case of node in Tree, set the parentKey to use for rename
        sParentKeyToAddInNewKey = unTreeWidget_compositeLabelSeparator+sTopDisplayName+
                                    unTreeWidget_labelSeparator+
                                    unTreeWidget_treeNodeDpPrefix+sTopDisplayName;
      }
      else {
        sParentKeyToAddInNewKey = sCurrentParentKey;
      }
      sCurrentParentDisplayNodeName = fwTree_getNodeDisplayName(_fwTree_getNodeName(sCurrentParentDpName), exInfo);
  // trigger only if title != DisplayNodeName

      unTree_triggerEventNodeRigthClick(UN_TRENDTREE_dblClick, makeDynString(sCurrentSelectedKey, sCurrentDpName, sCurrentNodeDisplayName,
                                            sCurrentCompositeLabel, sCurrentParentKey, sCurrentParentDpName,
                                            sCurrentParentDisplayNodeName, sPasteNodeDpName,
                                            sPasteNodeName, sCurrentSelectedNodeKey, sParentKeyToAddInNewKey), exInfo);
    }
  }
}

// unTrendTree_openPanelPreExecute
/**
Purpose:
function executed before the unTree_openPanel execution in the case of adding the panel as symbol in the configuration panel.

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_openPanelPreExecute()
{
  int iCol, iRow, i, j;
//DebugN("unTrendTree_openPanelPreExecute");
  if(shapeExists("ColsSpinButton") && shapeExists("RowsSpinButton")) {
//    DebugN(ColsSpinButton.text, RowsSpinButton.text);
    iCol = ColsSpinButton.text;
    iRow = RowsSpinButton.text;
    for(i=1;i<=iCol;i++) {
      for(j=1;j<=iRow;j++) {
        removeSymbol(myModuleName(), myPanelName(), "Plot_" + i + "_" + j);
      }
    }
  }
}

// unTrendTree_savePlotAs
/**
Purpose:
function called by the save as button of the fwTrending/fwTrendingPlotConfPanel.pnl

  @param sPlotDpName: string, input: the plot dp name
  @param bIsNewDp: bool, input: true=the sPlotDpName is new/false=the sPlotDpName is overwritten
  @param bIsFromDp: bool, input: true=the data were originally from sPlotDpName is new/false=the data were originally from dollar parameters
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_savePlotAs(string sNewPlotDpName, bool bIsNewDp, bool bIsFromDp, dyn_string &exceptionInfo)
{
  string sPlotTitle = dpSubStr(sNewPlotDpName, DPSUB_DP), sNodeNameAdded, sSelectedKey, sPlotDpName = dpSubStr(sNewPlotDpName, DPSUB_SYS_DP);
  bool bDone;
  string sTrendTreeDp=_fwTree_makeNodeName(UN_TRENDTREE_ROOT_FOLDER), sUserDefDp=_fwTree_makeNodeName(UN_TRENDTREE_USERDEFINED_TRENDS);
  dyn_dyn_string ddsPlotData;
  int i, len, len2, pos;
  string sDPEName, sLegend, sPVSSDPEName, sLeafDPE;

  if(sPlotTitle != "")
    dpSet(sPlotDpName + fwTrending_PLOT_TITLE, sPlotTitle);
  else
    dpGet(sPlotDpName + fwTrending_PLOT_TITLE, sPlotTitle);

  if(!bIsFromDp) { // save as from faceplate, all data can be queried because fron faceplate and system must be running for the save as.
// get all the legends and curveDPE
    fwTrending_getPlot(sNewPlotDpName, ddsPlotData, exceptionInfo);
// change the legends of the curves to deviceName:leafDPE if equals to leafDPE && UNICOS device
    len = dynlen(ddsPlotData[fwTrending_PLOT_OBJECT_DPES]);
    len2 = dynlen(ddsPlotData[fwTrending_PLOT_OBJECT_LEGENDS]);
//DebugN(ddsPlotData[fwTrending_PLOT_OBJECT_LEGENDS], len, len2);
    for(i=1;i<=len;i++) {
      sDPEName = ddsPlotData[fwTrending_PLOT_OBJECT_DPES][i];
      sLegend = ddsPlotData[fwTrending_PLOT_OBJECT_LEGENDS][i];
      unGenericDpFunctions_convert_UNICOSDPE_to_PVSSDPE(sDPEName, sPVSSDPEName);
      if(sDPEName != sPVSSDPEName) { // assume this is a UNICOS format DPE
    // get the leafDPE
      pos = strpos(sPVSSDPEName, ".ProcessInput.");
      if(pos >= 0)
        sLeafDPE = substr(sPVSSDPEName, pos+strlen(".ProcessInput."), strlen(sPVSSDPEName));
      else {
    // check ProcessOutput
        pos = strpos(sPVSSDPEName, ".ProcessOutput.");
        if(pos >= 0)
          sLeafDPE = substr(sPVSSDPEName, pos+strlen(".ProcessOutput."), strlen(sPVSSDPEName));
      }

//DebugN(sLeafDPE, sPVSSDPEName);

    // check if equals to legend
        if(sLeafDPE == sLegend) {
    // change it to the UNICOS format
  // get the device name from the PVSS DP
          sPVSSDPEName = unGenericDpFunctions_getDpName(sPVSSDPEName);
          sDPEName = unGenericDpFunctions_getAlias(sPVSSDPEName);
  // remove the systemName
          sPVSSDPEName = substr(sDPEName, strpos(sDPEName, ":") +1, strlen(sDPEName));
  // add the sLeafDPE.
          ddsPlotData[fwTrending_PLOT_OBJECT_LEGENDS][i] = sPVSSDPEName+"."+sLeafDPE;
        }
//DebugN("****", i, ddsPlotData[fwTrending_PLOT_OBJECT_LEGENDS][i], sLegend, sDPEName, sPVSSDPEName);
      }
    }
// set the plot data.
    fwTrending_setPlot(sNewPlotDpName, ddsPlotData, exceptionInfo);
  }

  if(bIsNewDp) {
    if(shapeExists("tree")) {
  // key is: ["TrendTree;fwTN_TrendTree|User_Defined_Trends;fwTN_User_Defined_Trends@|TrendTree;fwTN_TrendTree"]
      sSelectedKey = UN_TRENDTREE_ROOT_FOLDER+unTreeWidget_labelSeparator+sTrendTreeDp+
                      unTreeWidget_compositeLabelSeparator+
                      UN_TRENDTREE_USERDEFINED_TRENDS+unTreeWidget_labelSeparator+sUserDefDp+unTreeWidget_keySeparator+
                      unTreeWidget_compositeLabelSeparator+UN_TRENDTREE_ROOT_FOLDER+unTreeWidget_labelSeparator+sTrendTreeDp;

  // add the plot in User_defined
      unTrendTree_addDeviceInHierarchy(sPlotDpName, sPlotTitle, "tree", "treeBar", sSelectedKey,
                                        sUserDefDp,
                                        UN_TRENDTREE_USERDEFINED_TRENDS, false, bDone, true, exceptionInfo);
    }
    else {
  // add the plot in User_defined
      // make the node and add it in the tree
      sNodeNameAdded = fwTree_createNode(UN_TRENDTREE_USERDEFINED_TRENDS, sPlotTitle, exceptionInfo);

    // set the device name and type
      fwTree_setNodeDevice(sNodeNameAdded, sPlotDpName, UN_TRENDTREE_PLOT, exceptionInfo);

    // set the user data
      fwTree_setNodeUserData(sNodeNameAdded, makeDynString(), exceptionInfo);
    }
  }
}

//unTrendTree_getDPEFormat
/**
Purpose:
function called by the save as button of the fwTrending/fwTrendingPlotConfPanel.pnl

  @param sDPE: string, input: the dpe name
  @param sReturnedFormat: string, output: return the format of the dpe here
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_getDPEFormat(string sDPE, string &sReturnedFormat, dyn_string &exceptionInfo)
{
  string sDpeName = dpSubStr(sDPE, DPSUB_SYS_DP_EL), sFormat;
  int pos;

  sFormat = dpGetFormat(sDpeName);
  if (sFormat == "")
  {
    sFormat = UN_FACEPLATE_DEFAULT_FORMAT;
  }
  pos = strpos(sFormat, UN_DISPLAY_VALUE_DIGIT_FLOAT_FORMAT+UN_DISPLAY_VALUE_DIGIT_FLOAT_TYPE);
  if(pos>0) { // fixed display format
    sFormat = substr(sFormat, 0, pos);
    sFormat = sFormat+UN_TREND_DEFAULT_FORMAT_FIXED_DISPLAY_EXTENTION;
  }
  sReturnedFormat = sFormat;
//  DebugN("unTrendTree_getDPEFormat", sDPE, sDpeName, sFormat);
}

// unTrendTree_savePageAs
/**
Purpose:
function called by the save as button of the fwTrending/fwTrendingPlotsPage.pnl

  @param sPageDpName: string, input: the page dp name
  @param bIsNewDp: bool, input: true=the sPageDpName is new/false=the sPageDpName is overwritten
  @param bIsFromDp: bool, input: true=the data were originally from sPageDpName is new/false=the data were originally from dollar parameters
  @param exceptionInfo: dyn_string, output: the errors are returned here

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . the JCOP hierarchy management
  . to be used with the WidgetTree and
  the Microsoft ProgressBar Control, version 6.0
  . PVSS version: 3.0
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/
unTrendTree_savePageAs(string sNewPageDpName, bool bIsNewDp, bool bIsFromDp, dyn_string &exceptionInfo)
{
  string sPageTitle = dpSubStr(sNewPageDpName, DPSUB_DP), sNodeNameAdded, sSelectedKey, sPageDpName = dpSubStr(sNewPageDpName, DPSUB_SYS_DP);
  bool bDone;
  string sTrendTreeDp=_fwTree_makeNodeName(UN_TRENDTREE_ROOT_FOLDER), sUserDefDp=_fwTree_makeNodeName(UN_TRENDTREE_USERDEFINED_TRENDS);

  if(sPageTitle != "")
    dpSet(sPageDpName + fwTrending_PAGE_TITLE, sPageTitle);
  else
    dpGet(sPageDpName + fwTrending_PAGE_TITLE, sPageTitle);

  if(bIsNewDp) {
    if(shapeExists("tree")) {
  // key is: ["TrendTree;fwTN_TrendTree|User_Defined_Trends;fwTN_User_Defined_Trends@|TrendTree;fwTN_TrendTree"]
      sSelectedKey = UN_TRENDTREE_ROOT_FOLDER+unTreeWidget_labelSeparator+sTrendTreeDp+
                      unTreeWidget_compositeLabelSeparator+
                      UN_TRENDTREE_USERDEFINED_TRENDS+unTreeWidget_labelSeparator+sUserDefDp+unTreeWidget_keySeparator+
                      unTreeWidget_compositeLabelSeparator+UN_TRENDTREE_ROOT_FOLDER+unTreeWidget_labelSeparator+sTrendTreeDp;

  // add the age in User_defined
      unTrendTree_addDeviceInHierarchy(sPageDpName, sPageTitle, "tree", "treeBar", sSelectedKey,
                                        sUserDefDp,
                                        UN_TRENDTREE_USERDEFINED_TRENDS, false, bDone, false, exceptionInfo);
    }
    else {
  // add the plot in User_defined
      // make the node and add it in the tree
      sNodeNameAdded = fwTree_createNode(UN_TRENDTREE_USERDEFINED_TRENDS, sPageTitle, exceptionInfo);

    // set the device name and type
      fwTree_setNodeDevice(sNodeNameAdded, sPageDpName, UN_TRENDTREE_PAGE, exceptionInfo);

    // set the user data
      fwTree_setNodeUserData(sNodeNameAdded, makeDynString(), exceptionInfo);
    }
  }
}

//@}








