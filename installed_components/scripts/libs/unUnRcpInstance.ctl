#uses "unRcpFunctions_gui.ctl"
/**
 * UNICOS
 * Copyright (C) CERN 2014 All rights reserved
 */
/**@file

// unRcpUnRcpInstance.ctl
This library contains the widget, faceplate, etc. functions of UnRcpInstance.

@par Creation Date
  01/06/2011

@par Modification History
  18/08/2012: Ivan
	- New actions added to the contextual buttons.
  
  15/09/2014 : Ivan
    - New actions added to the contextual buttons (database actions).

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author
  Ivan Prieto Barreiro (EN-ICE)
*/

#uses "fwGeneral/fwException.ctl"
#uses "fwConfigurationDB/fwConfigurationDB_Recipes.ctl"
#uses "unDistributedControl/unDistributedControl.ctl"
#uses "unGenericObject.ctl"
#uses "unGenericDpFunctions.ctl"
#uses "unGenericFunctions_core.ctl"
#uses "unSelectDeselectHMI.ctl"
#uses "unSystemIntegrity.ctl"
#uses "unicos_declarations.ctl"
#uses "unicos_declarations_core.ctl"
#uses "unRcpConstant_declarations.ctl"
#uses "unRcpFunctions_privileges.ctl"
#uses "unSystemIntegrity_unicosPLC.ctl"

//@{

//------------------------------------------------------------------------------------------------------------------------
// unUnRcpInstance_ObjectListGetValueTime
/** Function called from snapshot utility of the treeDeviceOverview to get the time and value

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDeviceName  input, device name
@param sDeviceType input, device type
@param dsReturnData output, return data, array of 5 strings

@reviewed 2018-06-25 @whitelisted{UNICOSFrontend}
*/
unUnRcpInstance_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  string sTime, sValue, sColor;
  bool bBad;

  dpGet(sDeviceName+".ProcessInput.State", sValue,
        sDeviceName+".ProcessInput.State:_online.._stime", sTime,
        sDeviceName+".ProcessInput.State:_online.._bad", bBad);

  dsReturnData[1] = sTime;
  dsReturnData[2] = sValue;
  dsReturnData[3] = bBad;
  dsReturnData[4] = sValue;
  sColor="unEventList_ForeColor";
  dsReturnData[5] = sColor;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_FaceplateStatusRegisterCB
/** faceplate DistributedControl callback of the faceplate status panel

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name

@reviewed 2018-06-25 @whitelisted{UNICOSFaceplate}
*/
unUnRcpInstance_FaceplateStatusRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction;
  dyn_string exceptionInfo;
  bool bRemote;

  deviceName = unGenericDpFunctions_getDpName($sDpName);
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);

  switch (iAction)
  {
    case UN_ACTION_DISCONNECT:
      unUnRcpInstance_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_DPCONNECT:
      unUnRcpInstance_FaceplateConnect(deviceName);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      unUnRcpInstance_FaceplateDisconnect(deviceName);
      unUnRcpInstance_FaceplateStatusDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_FaceplateStatusDisconnection
/** set the faceplate when the device system is disconnected

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
unUnRcpInstance_FaceplateStatusDisconnection()
{
  // there is no need to do here execScript because there is one faceplate per device type
  // set all the graphical element to the state data not connnected
  // ex. of functions that can be used
  //unGenericObject_ColorBoxDisconnect, unGenericObject_DisplayValueDisconnect, etc.
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_FaceplateConnect
/** dpConnect to the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
*/
unUnRcpInstance_FaceplateConnect(string deviceName)
{
  string sFrontEnd, sSystemName;

  g_sDeviceAlias = unGenericDpFunctions_getAlias(deviceName);

  g_bUnSystemAlarmPlc = false;
  // get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  // get the PLC name
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
  g_sFrontEndDp = sSystemName+sFrontEnd;
  g_sFrontEndAlias = unGenericDpFunctions_getAlias(g_sFrontEndDp);

  if(sFrontEnd != ""){
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
    if(dpExists(sFrontEnd)) {
      g_bUnSystemAlarmPlc = true;
    }
  }

  if(g_bUnSystemAlarmPlc) {
    // set all faceplate global variable usefull for the callback, e.g.: description, format, unit, etc.
    // set graphical element of the faceplate to the init state if needed

    // if there are many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // keep the kind of dpConnect in a variable global to the faceplate and do a swith for the dpConnect
    // and do to the correct dpDisconnect in the unUnRcpInstance_FaceplateDisconnect
	string sAction = "";
	if (isDollarDefined("$sAction")) {
		sAction = $sAction;
	}
    addSymbol(myModuleName(), myPanelName(), "vision/unRecipe/UnRcpInstance/unUnRcpInstance_UnRcpInstance.pnl", "RCP", makeDynString("$sDpName:"+$sDpName, "$sAction:"+sAction), 0, 0, 0, 1, 1);
    g_bCallbackConnected = true;
  }
  else
    unUnRcpInstance_FaceplateStatusDisconnection();
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_FaceplateDisconnect
/** dpDisconnect to the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
*/
unUnRcpInstance_FaceplateDisconnect(string deviceName)
{
  string sFrontEnd, sSystemName;

  if(g_bUnSystemAlarmPlc) {
    // get the systemName
    sSystemName = unGenericDpFunctions_getSystemName(deviceName);
    // get the PLC name
    sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;

    // if there were many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // re-use the kind of dpConnect kept in a variable global to the faceplate set during unUnRcpInstance_FaceplateConnect
    // and do to the correct dpDisconnect

	if (shapeExists("RecipeInstances"))
	{
		removeSymbol(myModuleName(), myPanelName(), "RCP");
	}
	if (shapeExists("buttonActivate"))
	{
		removeSymbol(myModuleName(), myPanelName(), "RCPButton");
	}
    g_bCallbackConnected = false;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_ButtonRegisterCB
/** Contextual button DistributedControl callback of the contextual device button panel

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name

@reviewed 2018-06-25 @whitelisted{UNICOSDevice}
*/
unUnRcpInstance_ButtonRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sDpType, sFunction;
  int iAction, iRes;
  dyn_string exceptionInfo, dsFunctions, dsUserAccess;

  deviceName = unGenericDpFunctions_getDpName($sDpName);
  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);

  switch (iAction)
  {
    case UN_ACTION_DISCONNECT:
      unUnRcpInstance_ButtonDisconnection();
      break;
    case UN_ACTION_DPCONNECT:

      sDpType=UN_CONFIG_UNRCPINSTANCE_DPT_NAME;
      unGenericObject_GetFunctions(sDpType, dsFunctions, exceptionInfo);
      sFunction = dsFunctions[UN_GENERICOBJECT_FUNCTION_BUTTONUSERACCESS];

      if (sFunction != "")
      {
        evalScript(dsUserAccess, "dyn_string main(string deviceNameTemp, string sDpTypeTemp) {" +
               "dyn_string dsUserAccessReturn;" +
               "if (isFunctionDefined(\"" + sFunction  + "\"))" +
               "    {" +
                 sFunction  + "(deviceNameTemp, sDpTypeTemp, dsUserAccessReturn);" +
               "    }" +
               "return dsUserAccessReturn; }", makeDynString(), deviceName, sDpType);
        // keep the list of the device allowed action
        g_dsUserAccess=dsUserAccess;
      }
      else
        fwException_raise(exceptionInfo,"ERROR", "unUnRcpInstance_ButtonRegisterCB:" + getCatStr("unGeneration","UNKNOWNFUNCTION"), "EMPTY");

      // set all contextual panel global variable usefull for the unUnRcpInstance_ButtonAnimationCB, e.g.: description, format, unit, etc.
      iRes = dpConnect("unUnRcpInstance_ButtonAnimationCB",
               deviceName + ".statusInformation.selectedManager:_lock._original._locked",
               deviceName + ".statusInformation.selectedManager",
               deviceName + ".ProcessInput.State");
      g_bCallbackConnected = (iRes >= 0);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      iRes = dpDisconnect("unUnRcpInstance_ButtonAnimationCB",
               deviceName + ".statusInformation.selectedManager:_lock._original._locked",
               deviceName + ".statusInformation.selectedManager",
               deviceName + ".ProcessInput.State");
      g_bCallbackConnected = !(iRes >= 0);
      unUnRcpInstance_ButtonDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_UserLoginGetButtonState
/** return the device data, this function is called when the user logs in and logs out

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param sType input, the device type
@param dsReturnData output, the device data, [1] = lock state, [2] = lock by, [3] .. [6] device data

@reviewed 2018-06-25 @whitelisted{UNICOSDevice}
*/
unUnRcpInstance_UserLoginGetButtonState(string deviceName, string sType, dyn_string &dsReturnData)
{
  int iRes;
  string sDpLock, sDpSelectedManager, sSelectedManager, sDpState, sState;
  bool bLocked;

  // read the device data (6 string)
  sDpLock = deviceName + ".statusInformation.selectedManager:_lock._original._locked";
  sDpSelectedManager = deviceName + ".statusInformation.selectedManager";
  sDpState = deviceName + ".ProcessInput.State";

  if (dpExists(deviceName))
  {
    iRes = dpGet(sDpLock, bLocked, sDpSelectedManager, sSelectedManager, sDpState, sState);
  }
  else
  {
    iRes = -1;
  }
  if (iRes < 0) // Errors during dpget
  {
    bLocked = false;
    sSelectedManager = "";
  }
  dsReturnData=makeDynString((string)bLocked, sSelectedManager, sState, "..", "..", "..");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_ButtonSetState
/** Set the state of the contextual button of the device

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, the device DP name
@param sDpType input, the device type
@param dsUserAccess input, list of allowed action on the device
@param dsData input, the device data [1] = lock state, [2] = lock by, [3] .. [6] device data
*/
unUnRcpInstance_ButtonSetState(string deviceName, string sDpType, dyn_string dsUserAccess, dyn_string dsData)
{
  bool bSelected;
  dyn_string dsButtons;
  int i, length;
  string localManager, sRcpLoaded, sSystemName;
  bool bLocked, buttonEnabled, bSystemOk;
  string sSelectedManager, sState, sDeviceValue2, sDeviceValue3, sDeviceValue4;

  bLocked = (bool) dsData[1];
  sSelectedManager = dsData[2];
  sState = dsData[3];
  sDeviceValue2 = dsData[4];
  sDeviceValue3 = dsData[5];
  sDeviceValue4 = dsData[6];

  // dsButtons = list of all the possible actions except Select
  // use the name of the graphical element without "button"
  // use the name of the graphical elements.
  dsButtons = makeDynString("Activate", "Edit", "NewInstance", "Duplicate", "Delete", "OnlineValues", "DbValues", "Save", 
						    "SaveToDb", "Cancel", "SaveAsInitial", "InitialRecipes", "LastActivatedRecipes");
  length = dynlen(dsButtons);
  
  // 1. Selection state
  localManager = unSelectDeselectHMI_getSelectedState(bLocked, sSelectedManager);
  bSelected = (localManager == "S");
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  bSystemOk = (sSystemName == getSystemName());
  
  // Get the DP name of the recipe loaded in the panel
  if (shapeExists("LoadedRecipeDp")) {
	getValue("LoadedRecipeDp", "text", sRcpLoaded);	
  } else {
    sRcpLoaded="";
  }

  // 2. for each possible action check if the action is in the list dsUserAccess
  // cross-check if needed with the state of the device
  for(i=1;i<=length;i++)
  {
    buttonEnabled = (dynContains(dsUserAccess, dsButtons[i]) > 0);  // User access
    switch(dsButtons[i])
    {
      case "NewInstance":
        buttonEnabled = buttonEnabled & sState=="" && bSystemOk;
        break;		
      case "OnlineValues":
      case "DbValues":
      case "Save":
      case "Cancel":
        buttonEnabled = buttonEnabled & bSelected & sState==UN_RCP_INSTANCE_STATE_EDIT && sRcpLoaded!="";
        break;	
      case "SaveToDb":
        buttonEnabled = buttonEnabled & bSelected & (sState=="" || sState==UN_RCP_INSTANCE_STATE_EDIT) && sRcpLoaded!="" && bSystemOk;
        break;
      case "Activate":
        buttonEnabled = buttonEnabled & bSelected & sState=="" && sRcpLoaded!="";
        break;
      case "Delete":
        // calculate the enabled state of the button from the device data
        buttonEnabled = buttonEnabled & bSelected & sState=="" && sRcpLoaded!="" && bSystemOk;
        break;
      case "Edit":      
      case "SaveAsInitial":
        // calculate the enabled state of the button from the device data
        buttonEnabled = buttonEnabled & bSelected & sState=="" && sRcpLoaded!="";
        break;
      case "Duplicate":
        // calculate the enabled state of the button from the device data
        buttonEnabled = buttonEnabled & bSelected & sState=="" && sRcpLoaded!="" && bSystemOk;
        break;
      case "InitialRecipes":
      case "LastActivatedRecipes":
        // calculate the enabled state of the button from the device data
        buttonEnabled = buttonEnabled & bSelected & sState=="" && sRcpLoaded!="" ;
        break;		
      default:
        break;
    }
    setValue(UN_FACEPLATE_BUTTON_PREFIX + dsButtons[i], "enabled", buttonEnabled);
  }

  // 3. Button Select state
  buttonEnabled = (dynContains(dsUserAccess, UN_FACEPLATE_BUTTON_SELECT) > 0) && sRcpLoaded!="" 
                  && (sState!=UN_RCP_INSTANCE_STATE_ACTIVATE && sState!=UN_RCP_INSTANCE_STATE_UNLOCK_BUFFERS 
                      && sState!=UN_RCP_INSTANCE_STATE_CANCELLED && sState != UN_RCP_INSTANCE_STATE_LOCKED);
  unGenericObject_ButtonAnimateSelect(localManager, buttonEnabled);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_ButtonDisconnection
/** set the state of the device action button when the device system is disconnected

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
unUnRcpInstance_ButtonDisconnection()
{
  // set all the device action button to disabled
  unGenericObject_ButtonDisconnect(makeDynString(UN_FACEPLATE_BUTTON_SELECT, "Cmd1", ".."));
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_ButtonUserAccess
/** returns the list of allowed action on the device for a user logged in

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName input, the device DP name
@param sDpType input, the device type
@param dsAccess input, list of allowed action on the device

@reviewed 2018-06-25 @whitelisted{FalsePositive}
*/
unUnRcpInstance_ButtonUserAccess(string sDpName, string sDpType, dyn_string &dsAccess)
{
  bool operator, expert, admin;
  dyn_bool dbPermissions;
  dyn_string exceptionInfo;
  string sOperAction, sExpAction, sAdminAction;
  bool bActionDefined = false;

  // get the configured list of authorized action per privilege
  unGenericDpFunctions_getAccessControlPriviledgeRigth(sDpName, g_dsDomainAccessControl, dbPermissions, sOperAction, sExpAction, sAdminAction, exceptionInfo);
  if((sOperAction != UNICOS_ACCESSCONTROL_DOMAINNAME) || (sExpAction != UNICOS_ACCESSCONTROL_DOMAINNAME) ||
     (sAdminAction != UNICOS_ACCESSCONTROL_DOMAINNAME)) 
  {
    bActionDefined=true;
  }
  
  operator = dbPermissions[2];
  expert = dbPermissions[3];
  admin = dbPermissions[4];
  dsAccess = makeDynString();

  // for each privilege get the list of allowed actions if there are action defined on the device
  // if there are no action defined for the device get the default one
  // in the example, the default behavior is
  // the Select and Cmd1 buttons are allowed if one has the expert privilege
  if (operator)
  {
    if(bActionDefined) {
      dynAppend(dsAccess, strsplit(sOperAction, UN_ACCESS_CONTROL_SEPARATOR));
    } else {
	  unRecipeFunctions_getDefaultInstancePrivileges("operator", dsAccess);
    }
  }
  if (expert)
  {
    if(bActionDefined) {
      dynAppend(dsAccess, strsplit(sExpAction, UN_ACCESS_CONTROL_SEPARATOR));
    } else {
	  unRecipeFunctions_getDefaultInstancePrivileges("expert", dsAccess);
    }
  }
  if(admin){
    if(bActionDefined) {
      dynAppend(dsAccess, strsplit(sAdminAction, UN_ACCESS_CONTROL_SEPARATOR));
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_ButtonAnimationCB
/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpLock input, the lock DPE
@param bLocked input, selected lock state of the device
@param sDpSelect input, the select DPE
@param sSelectedManager input, user and Ui that selected the device
@param sDpState input, the datapoint name of the recipe instance
@param sState input, state of the recipe instance

@reviewed 2018-06-25 @whitelisted{UNICOSDevice}
*/
unUnRcpInstance_ButtonAnimationCB(string sDpLock, bool bLocked, string sDpSelect, string sSelectedManager,
                                  string sDpState, string sState)
{
  // call the set button state with the device data and the current allowed action on the device
  // which is in g_dsUserAccess
  unUnRcpInstance_ButtonSetState(unGenericDpFunctions_getDpName(sDpLock), UN_CONFIG_UNRCPINSTANCE_DPT_NAME, g_dsUserAccess,
                  makeDynString((string) bLocked, sSelectedManager, sState, "..", "..", ".."));
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_WidgetRegisterCB
/** widget DistributedControl callback

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the DistributedControl system name DP name
@param bSystemConnected input, the state of the system name

@reviewed 2018-06-25 @whitelisted{unUnRcpInstance_WidgetRegisterCB}
*/
unUnRcpInstance_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction;
  dyn_string exceptionInfo;
  string sFrontEnd;
  bool bRemote;

  deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
  if (deviceName == "")  // In case of disconnection
  {
    deviceName = g_sDpName;
  }
  g_bUnSystemAlarmPlc = false;
  // get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  // get the PLC name
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);

  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;

  if(sFrontEnd != ""){
    sFrontEnd = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
    if(dpExists(sFrontEnd)) {
      g_bUnSystemAlarmPlc = true;
    }
  }

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  switch(iAction)
  {
    case UN_ACTION_DISCONNECT:
      unUnRcpInstance_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPCONNECT:
      g_sDpName = deviceName;
      unUnRcpInstance_WidgetConnect(deviceName, sFrontEnd);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      unUnRcpInstance_WidgetDisconnect(deviceName, sFrontEnd);
      unUnRcpInstance_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_WidgetDisconnection
/** set the widget when the system is disconnected

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sWidgetType input, the type of widget
*/
unUnRcpInstance_WidgetDisconnection(string sWidgetType)
{
  /** use the exec mechanism to allow multiple widget. **/
  string sFunction = "unUnRcpInstance_"+sWidgetType+"Disconnection";

  if (isFunctionDefined(sFunction) && (sFunction != ""))
  {
    execScript("main(string sWidgetType) {" + sFunction + "(sWidgetType);}", makeDynString(), sWidgetType);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_WidgetConnect
/** dpConnect to the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name
*/
unUnRcpInstance_WidgetConnect(string deviceName, string sFrontEnd)
{
  int iRes;
  bool bExists;
  string sSystemName, sRcpClassName, sFwConfDpName;
  dyn_string exceptionInfo;

  //  check if the device name is of type "UnRcpInstance" if not like no front-end. -> not available.
  if(dpTypeName(deviceName) != UN_CONFIG_UNRCPINSTANCE_DPT_NAME)
    g_bUnSystemAlarmPlc = false;

  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  if(g_bUnSystemAlarmPlc)
  {
    // set all widget global variable useful for the callback, e.g.: description, format, unit, etc.

	// Get the name of the last activated recipe of this class
	bExists = TRUE;
	dpGet(deviceName + ".ProcessInput.ClassName", sRcpClassName); 
	sFwConfDpName = _fwConfigurationDB_getRecipeClassDP(sRcpClassName, bExists, exceptionInfo);
	
    // if there are many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // keep the kind of dpConnect in a variable global to the widget and do a swith for the dpConnect
    // and do to the correct dpDisconnect in the unUnRcpInstance_WidgetDisconnect
    iRes = dpConnect("unUnRcpInstance_WidgetCB",
                          deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                          deviceName + ".statusInformation.selectedManager",
                          deviceName + ".ProcessInput.State",
                          sFrontEnd+".alarm",
                          sFrontEnd+".enabled",
						  sSystemName + sFwConfDpName + ".LastActivatedOfThisType");
    g_bCallbackConnected = (iRes >= 0);
  }else{
    unUnRcpInstance_WidgetDisconnection(g_sWidgetType);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_WidgetDisconnect
/** dpDisconnect to the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, front-end device DP name
@param sFrontEnd input, front-end device name

*/
unUnRcpInstance_WidgetDisconnect(string deviceName, string sFrontEnd)
{
  int iRes;
  bool bExists;
  string sRcpClassName, sFwConfDpName;
  dyn_string exceptionInfo;

  if(g_bUnSystemAlarmPlc)
  {
  	bExists = TRUE;
	dpGet(deviceName + ".ProcessInput.ClassName", sRcpClassName); 
	sFwConfDpName = _fwConfigurationDB_getRecipeClassDP(sRcpClassName, bExists, exceptionInfo);
	
    // if there were many dpConnect possibilities: e.g. dpConnect with or without _alert_hdl config
    // re-use the kind of dpConnect kept in a variable global to the widget set during unUnRcpInstance_WidgetConnect
    // and do to the correct dpDisconnect
    iRes = dpDisconnect("unUnRcpInstance_WidgetCB",
                          deviceName + ".statusInformation.selectedManager:_lock._original._locked",
                          deviceName + ".statusInformation.selectedManager",
                          deviceName + ".ProcessInput.State",
                          sFrontEnd+".alarm",
                          sFrontEnd+".enabled",
						  sFwConfDpName + ".LastActivatedOfThisType");
    g_bCallbackConnected = !(iRes >= 0);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_WidgetCB
/** callback function on the device data

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpLocked input, the lock DPE
@param bLocked input, the lock state
@param sDpSelectedManager input, the selected manager DPE
@param sSelectedManager input, the selected manager
@param sDpState input, the recipe instance state DPE
@param sState input, the recipe instance state value
@param sDpFESystemIntegrityAlarmValue input, front-end device system integrity alarm DPE
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param sDpFESystemIntegrityAlarmEnabled input, front-end device system integrity enable DPE
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value
@param sDpLastActivatedOfThisType input, datapoint name of the last recipe activated of the recipe class
@param sLastActivatedOfThisType input, name of the last recipe activated of the recipe class

@reviewed 2018-06-25 @whitelisted{UNICOSWidget}
*/
unUnRcpInstance_WidgetCB(string sDpLocked, bool bLocked,
                                  string sDpSelectedManager, string sSelectedManager,
                                  string sDpState, string sState,
                                  string sDpFESystemIntegrityAlarmValue, int iFESystemIntegrityAlarmValue,
                                  string sDpFESystemIntegrityAlarmEnabled, bool bFESystemIntegrityAlarmEnabled,
								  string sDpLastActivatedOfThisType, string sLastActivatedOfThisType)
{
  /** use the exec mechanism to allow multiple widget. **/
  string sFunction = "unUnRcpInstance_"+g_sWidgetType+"Animation";
  string selectColor, sWarningLetter, sWarningColor;
  bool bSelectVisible;

  // 1. set the widget.
  unGenericObject_WidgetSelectAnimation(bLocked, sSelectedManager, selectColor, bSelectVisible);

  // if bInvalid --> N letter and everything blue
  // if Front-end problem or problems-> O letter
  if((!bFESystemIntegrityAlarmEnabled) || (iFESystemIntegrityAlarmValue > c_unSystemIntegrity_no_alarm_value))
  {
    sWarningLetter = UN_WIDGET_TEXT_OLD_DATA;
    sWarningColor = "unDataNotValid";
  }
  if(g_bSystemConnected) {
    string sRcpAlias = dpGetAlias(substr(sDpState, 0, strpos(sDpState, ".") + 1));
	string sRcpClassName = strsplit(sRcpAlias, "/")[1];
	string sRcpInstName  = strsplit(sRcpAlias, "/")[2];
	sRcpClassName = strrtrim(sRcpClassName);
	sRcpInstName  = strltrim(sRcpInstName);

	int bodyWidth, bodyHeight;
	getValue("Body1", "size", bodyWidth, bodyHeight);	
	sRcpClassName = _unRecipeFunctions_adjustStringToSize(getShape("HiddenName"), sRcpClassName, bodyWidth);
	sRcpInstName = _unRecipeFunctions_adjustStringToSize(getShape("HiddenName"), sRcpInstName, bodyWidth);

	bool bIsLastActivated = (sLastActivatedOfThisType == sRcpAlias);
	string sBorderColor = "white";
	if (bIsLastActivated)
		sBorderColor = "cpcColor_Widget_Status";
    setMultiValue("WarningText", "text", sWarningLetter,
                  "WarningText", "foreCol", sWarningColor,
                  "SelectArea", "foreCol", selectColor,
                  "LockBmp", "visible", bSelectVisible,
                  "WidgetArea", "visible", true,
				  "StateText", "text", sState,
				  "RecipeClassName", "text", sRcpClassName,
                  "RecipeInstanceName", "text", sRcpInstName,
				  "Body1", "foreCol", sBorderColor);
  }

  if (isFunctionDefined(sFunction) && (sFunction != ""))
  {
    // set here all the common graphical element value or
    // give to the function all the variables
    execScript("main(bool bSystemConnected, string sWidgetType, int iFESystemIntegrityAlarmValue, bool bFESystemIntegrityAlarmEnabled) {" +
               sFunction + "(bSystemConnected, sWidgetType, iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);}",
               makeDynString(), g_bSystemConnected, g_sWidgetType, iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_MenuConfiguration
/** pop-up menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDpName input, device DP name
@param sDpType input, device type
@param dsAccessOk input, the access control
@param menuList output, pop-up menu to show, dyn_string to be given to the popupMenu function

@reviewed 2018-06-25 @whitelisted{UNICOSWidget}
*/
unUnRcpInstance_MenuConfiguration(string sDpName, string sDpType, dyn_string dsAccessOk, dyn_string &menuList)
{
  dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_SELECT_TEXT, UN_POPUPMENU_FACEPLATE_TEXT, UN_POPUPMENU_TREND_TEXT);

  unGenericObject_addUnicosActionToMenu(sDpName, sDpType, dsMenuConfig, dsAccessOk, menuList);
  unGenericObject_addDefaultUnicosActionToMenu(sDpName, sDpType, dsMenuConfig, dsAccessOk, menuList);
  unGenericObject_addTrendActionToMenu(sDpName, sDpType, dsMenuConfig, dsAccessOk, menuList, true);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_HandleMenu
/** handle the answer of the popup menu

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param deviceName input, device DP name
@param sDpType input, device type
@param menuList input, the access control
@param iMenuAnswer input, selected menu value

@reviewed 2018-06-25 @whitelisted{UNICOSWidget}
*/
unUnRcpInstance_HandleMenu(string deviceName, string sDpType, dyn_string menuList, int iMenuAnswer)
{
  int iUN_POPUPMENU_MIN = UN_POPUPMENU_MIN;
  dyn_string exceptionInfo;

  // handle unicos menu
  if(iMenuAnswer<iUN_POPUPMENU_MIN) {
    unGenericObject_HandleUnicosMenu(deviceName, sDpType, menuList, iMenuAnswer, exceptionInfo);
  }

}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_StateAnimation
/** animate the device State widget
!!!!! function trigger by exec call, $-param and variable of the widget cannot be used, all the necessary data
must be given to the function. The use of global var (global keyword declaration) is allowed

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param bSystemConnected input, connection state of the system, true=system connected/false=system not connected
@param sWidgetType input, the type of widget
@param iFESystemIntegrityAlarmValue input, front-end device system integrity alarm value
@param bFESystemIntegrityAlarmEnabled input, front-end device system integrity enable value

@reviewed 2018-06-25 @whitelisted{UNICOSWidget}
*/
unUnRcpInstance_StateAnimation(bool bSystemConnected, string sWidgetType, int iFESystemIntegrityAlarmValue, bool bFESystemIntegrityAlarmEnabled)
{
  /*!!!!!!!! all parameters must be given: function launched by exec ***/
  // encapsulate all the setValue or function call with the following if(bSystemConnected)
  if(bSystemConnected) {
	//setMultiValue("Body1", "foreCol", "white");
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unUnRcpInstance_StateDisconnection
/** animate the the device State widget when the system is disconnected
!!!!! function trigger by exec call, $-param and variable of the widget cannot be used, all the necessary data
must be given to the function. The use of global var (global keyword declaration) is allowed

@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sWidgetType input, the type of widget

@reviewed 2018-06-25 @whitelisted{UNICOSWidget}
*/
unUnRcpInstance_StateDisconnection(string sWidgetType)
{
  // set all the graphical element of the widget
  setMultiValue("Body1", "backCol", "unDataNoAccess",
                "WarningText", "text", "",
                "WidgetArea", "visible", false,
                "SelectArea", "foreCol", "",
                "LockBmp", "visible", false,
                "StateText", "text", "");
}

//---------------------------------------------------------------------------------------------------------------------------------------

//@}
