/**@name LIBRARY: un_UnPlc.ctl

@author: Herve Milcent (EN/ICE)

Creation Date: 23 02 2009

Modification History:

version 1.0
  14/10/2011: Herve
  - IS-579  unCore - unFrontEnd  front-end diagnostic panel: stop/start/set manual driver, simulator 
 
  10/08/2009: Herve
    - front-end proxy and trend implementation
    . modified functions: _UnPlc_MenuConfiguration, _UnPlc_HandleMenu
    
External Function : 
  . _UnPlc_ObjectListGetValueTime
  . _UnPlc_MenuConfiguration
  . _UnPlc_HandleMenu
  . _UnPlc_WidgetRegisterCB
  . _UnPlc_WidgetDisconnection

Internal Functions :

Purpose: This library contains _UnPlc device functions.

Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in _UnPlc faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

//@{

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_ObjectListGetValueTime
/**
Purpose: Function called from the objectList to return the time and value

Parameters:
  - sDeviceName, string, input, device name
  - sDeviceType, string, input, unicos object
  - dsReturnData, dyn_string, output, return value

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

_UnPlc_ObjectListGetValueTime(string sDeviceName, string sDeviceType, dyn_string &dsReturnData)
{
  string sTime="0", sSystemName, sFrontEnd;
  int iTime, iError, iCom, iRedu1Error, iRedu2Error;
  bool bEnable;
  string sWarningLetter, sWarningColor, sBodyColor="unDataNoAccess", sText = "???", sCompleteText;
  dyn_string dsColor;
  
  
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(sDeviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(sDeviceName);

  if(dpExists(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm")) 
  {
  //if redundant PLC, check redu connection alarms as well
    if(dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm"))
    {
      dpGet(sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm", iTime,
            sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm", iRedu1Error,
            sSystemName+c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern+sFrontEnd+".alarm", iRedu2Error,
            sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm", iCom,
            sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".enabled", bEnable,
            sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm:_online.._stime", sTime);
      iError = iRedu1Error>0 ? iRedu1Error : iRedu2Error;
      unGenericObject_getFrontEndColorState(makeDynString(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm",
                                                          sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm",
                                                          sSystemName+c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern +sFrontEnd+".alarm",
                                                          sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm"), dsColor);
    }
    //else, non-redundant PLC:
    else   
    {
      dpGet(sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm", iTime,
            sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sFrontEnd+".alarm", iError,
            sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm", iCom,
            sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".enabled", bEnable,
            sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm:_online.._stime", sTime);
      unGenericObject_getFrontEndColorState(makeDynString(sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd+".alarm",
                                                          sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sFrontEnd+".alarm",
                                                          sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm"), dsColor);
    }
    unGenericObject_getFrontState(makeDynString("COM ERR", "ERROR", "TIME ERR"),
                                  makeDynString(dsColor[1], dsColor[2], dsColor[3]), 
                                  makeDynInt(3*iCom, 2*iError, iTime), 
                                  iCom, bEnable, sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
  }

  dsReturnData[1] = sTime;
  dsReturnData[2] = sText;
  dsReturnData[3] = "FALSE";
  dsReturnData[4] = sCompleteText;
  dsReturnData[5] = sBodyColor;
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_MenuConfiguration
/**
Purpose: configuration of popup menu

Parameters:
  - sDpName, string, input, device name
  - sDpType, string, input, unicos object
  - dsAccessOk, dyn_string, input, authorized action
  - menuList, dyn_string, output, menu to pop-up
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Constants UN_POPUPMENU_****
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
_UnPlc_MenuConfiguration(string sDpName, string sDpType, dyn_string dsAccessOk, dyn_string &menuList)
{
  dyn_string dsMenuConfig = makeDynString(UN_POPUPMENU_TREND_TEXT);
//  DebugN(sDpName, sDpType, dsAccessOk);
  dynAppend(menuList, "PUSH_BUTTON, Front-end diagnostic, 2, 1");
  dynAppend(menuList,"PUSH_BUTTON, Device configuration, 10, 1");
  unGenericObject_addTrendActionToMenu(unGenericDpFunctions_getDpName(sDpName), sDpType, dsMenuConfig, dsAccessOk, menuList);
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_HandleMenu
/**
Purpose: handle the answer of the popup menu

Parameters:
  - deviceName, string, input, the device dp name
  - sDpType, string, input, the device dp type
  - menuList, dyn_string, input, list of requested action
  - menuAnswer, int, input, selected action from the menuList
  
Usage: Public

PVSS manager usage: NG, NV

Constraints:
  . Constants UN_POPUPMENU_****
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/
_UnPlc_HandleMenu(string deviceName, string sDpType, dyn_string menuList, int menuAnswer)
{
  dyn_string exInfo;
  
//  DebugN(deviceName, sDpType, menuList, menuAnswer);
  switch(menuAnswer)
  {
    case 2:
      unGenericObject_OpenFaceplate(unGenericDpFunctions_getDpName(deviceName), exInfo);
      break;
    case 10:
      unGenericObject_FrontEndConfiguration(unGenericDpFunctions_getDpName(deviceName), exInfo);
      break;
    default:
// handle the trend action
      unGenericObject_handleTrendAction(unGenericDpFunctions_getDpName(deviceName), sDpType, menuList, menuAnswer, exInfo);
// handle the single DPE trend action
      unGenericObject_handleSingleDPETrendAction(unGenericDpFunctions_getDpName(deviceName), sDpType, menuList, menuAnswer, exInfo);
      break;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_WidgetRegisterCB
/**
Purpose: register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in _UnPlc faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

_UnPlc_WidgetRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction;
  dyn_string exceptionInfo;
  string sFrontEnd, sSystInt;
  bool bRemote;
  
  deviceName = unGenericDpFunctions_getWidgetDpName($sIdentifier);
  if (deviceName == "")  // In case of disconnection
  {
    deviceName = g_sDpName;
  }
  g_bUnSystemAlarmPlc = false;
// get the systemName
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
// get the PLC name 
  sFrontEnd = unGenericObject_GetPLCNameFromDpName(deviceName);

  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;
    
  if(sFrontEnd != ""){
    sSystInt = sSystemName+c_unSystemAlarm_dpPattern+DS_pattern+sFrontEnd;
//DebugN(sSystemName, sFrontEnd);
    if(dpExists(sSystInt)) {
      g_bUnSystemAlarmPlc = true;
    }
  }

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
//DebugN("REG", deviceName, sFrontEnd, sSystInt);
  switch (iAction)
    {
    case UN_ACTION_DISCONNECT:
      _UnPlc_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_DPCONNECT:
      g_sDpName = deviceName;
      _UnPlc_WidgetConnect(deviceName, sFrontEnd, sSystInt);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      _UnPlc_WidgetDisconnect(deviceName, sFrontEnd, sSystInt);
      _UnPlc_WidgetDisconnection(g_sWidgetType);
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_WidgetDisconnection
/**
Purpose: Animate widget disconnection

Parameters:
  - sWidgetType, string, input, widget type. 
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in _UnPlc faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

_UnPlc_WidgetDisconnection(string sWidgetType)
{
  setMultiValue("Body1", "foreCol", "unDataNoAccess", "Body1", "text", "???", 
                "WidgetArea", "visible", false, "WarningText", "text", "");
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_WidgetConnect
/**
Purpose: widget connect function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables defined in Analog faceplate
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

_UnPlc_WidgetConnect(string deviceName, string sFrontEnd, string sSystInt)
{
  int iRes;
  string sSystemName, sPlcReduAlarm;
  dyn_string dsColor;
  int iAlarm;
  
//  check if the device name is of type "_UnPlc" if not like of no PLC. -> not available.
  if((dpTypeName(deviceName) != "_UnPlc") && (dpTypeName(deviceName) != "S7_PLC"))
    g_bUnSystemAlarmPlc = false;
  
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  if(g_bUnSystemAlarmPlc)
  {
    if(dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm"))
    {//different alarms if redundant PLC
      dpGet(sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm",iAlarm);
      if(iAlarm>9)
        sPlcReduAlarm = sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm";
      else
        sPlcReduAlarm = sSystemName+c_unSystemAlarm_dpPattern+PLCRedu2_DS_Error_pattern+sFrontEnd+".alarm";
        
      unGenericObject_getFrontEndColorState(makeDynString(sSystInt+".alarm",
                                                          sPlcReduAlarm,
                                                          sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm"), dsColor);
      g_sCommColor = dsColor[1];
      g_sErrorColor = dsColor[2];
      g_sTimeColor = dsColor[3];
      iRes = dpConnect("_UnPlc_WidgetCB", 
                        sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm",
                        sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm",
                        sSystInt+".alarm", sSystInt+".enabled");
    }
    else
    {
      unGenericObject_getFrontEndColorState(makeDynString(sSystInt+".alarm",
                                                          sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sFrontEnd+".alarm",
                                                          sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm"), dsColor);  
      g_sCommColor = dsColor[1];
      g_sErrorColor = dsColor[2];
      g_sTimeColor = dsColor[3];
      iRes = dpConnect("_UnPlc_WidgetCB", 
                        sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm",
                        sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sFrontEnd+".alarm",
                        sSystInt+".alarm", sSystInt+".enabled");      
    }      

    g_bCallbackConnected = (iRes >= 0);    
  }else{
    _UnPlc_WidgetDisconnection(g_sWidgetType);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_WidgetDisconnect
/**
Purpose: widget disconnect function 

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables defined in Analog faceplate
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

_UnPlc_WidgetDisconnect(string deviceName, string sFrontEnd, string sSystInt)
{
  int iRes;
  string sSystemName;

  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  
  if(g_bUnSystemAlarmPlc)
  {
    if(dpExists(sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm"))
    {//different alarms if redundant PLC    
      iRes = dpDisconnect("_UnPlc_WidgetCB", 
                        sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm",
                        sSystemName+c_unSystemAlarm_dpPattern+PLCRedu1_DS_Error_pattern+sFrontEnd+".alarm",
                        sSystInt+".alarm", sSystInt+".enabled");
    }
    else
    {
      iRes = dpDisconnect("_UnPlc_WidgetCB", 
                          sSystemName+c_unSystemAlarm_dpPattern+DS_Time_pattern+sFrontEnd+".alarm",
                          sSystemName+c_unSystemAlarm_dpPattern+PLC_DS_Error_pattern+sFrontEnd+".alarm",
                          sSystInt+".alarm", sSystInt+".enabled");      
    }
    g_bCallbackConnected = !(iRes >= 0);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_WidgetCB
/**
Purpose: widget animation function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . PVSS version: 3.0 
  . operating system: NT and W2000, but tested only under W2000.
  . distributed system: yes.
*/

_UnPlc_WidgetCB(string sTime, int iAlarmTime, string sError, int iAlarmError,
                    string sDpSystemIntegrityAlarmValue, int iFESystemIntegrityAlarmValue, 
                    string sDpSystemIntegrityAlarmEnabled, bool bFESystemIntegrityAlarmEnabled)
{
//DebugTN(sTime, iAlarmTime, sError, iAlarmError,  sDpSystemIntegrityAlarmValue, iFESystemIntegrityAlarmValue, sDpSystemIntegrityAlarmEnabled, bFESystemIntegrityAlarmEnabled);
  string sWarningLetter, sWarningColor, sBodyColor="white", sText = "???", sCompleteText;
  if(g_bSystemConnected) {
    unGenericObject_getFrontState(makeDynString("COM ERR", "ERROR", "TIME ERR"),
                                  makeDynString(g_sCommColor, g_sErrorColor, g_sTimeColor),
                                  makeDynInt(3*iFESystemIntegrityAlarmValue, 2*iAlarmError, iAlarmTime), 
                                  iFESystemIntegrityAlarmValue, bFESystemIntegrityAlarmEnabled,
                                  sText, sBodyColor, sWarningLetter, sWarningColor, sCompleteText);
    setMultiValue("WidgetArea", "visible", true, 
                  "WarningText", "text", sWarningLetter, "WarningText", "foreCol", sWarningColor, 
                  "Body1", "foreCol", sBodyColor, "Body1", "text", sText);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_FaceplateRegisterCB
/**
Purpose: faceplate register callback function

Parameters:

Usage: Internal

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in _UnPlc faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

_UnPlc_FaceplateRegisterCB(string sDp, bool bSystemConnected)
{
  string deviceName, sSystemName;
  int iAction, iRes;
  dyn_string exceptionInfo;
  bool bRemote;
  
  deviceName = unGenericDpFunctions_getDpName($sDpName);
  sSystemName = unGenericDpFunctions_getSystemName(deviceName);
  unDistributedControl_isRemote(bRemote, sSystemName);
  if(bRemote)
    g_bSystemConnected = bSystemConnected;
  else
    g_bSystemConnected = true;

  unGenericObject_Connection(deviceName, g_bCallbackConnected, bSystemConnected, iAction, exceptionInfo);
  switch (iAction)
  {
    case UN_ACTION_DISCONNECT:
      setValue("sText", "text", "Remote system "+sSystemName+" not connected");
      _UnPlc_FaceplateDisconnection();
      break;
    case UN_ACTION_DPCONNECT:
      setMultiValue("sText", "text", "", "sText", "visible", false);
      addSymbol(myModuleName(), myPanelName(), g_sFileName, "DIAG", makeDynString("$sFrontEnd:"+$sDpName), 0, 0, 0, 1, 1);
      break;
    case UN_ACTION_DPDISCONNECT_DISCONNECT:
      setValue("sText", "text", "Remote system "+sSystemName+" not connected");
      _UnPlc_FaceplateDisconnection();
      break;
    case UN_ACTION_NOTHING:
    default:
      break;
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------

// _UnPlc_FaceplateDisconnection
/**
Purpose: Animate Faceplate disconnection

Parameters:
   
Usage: External

PVSS manager usage: NG, NV

Constraints:
  . Global variables and $parameters defined in _UnPlc faceplate and widget
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
*/

_UnPlc_FaceplateDisconnection()
{
  if(shapeExists(g_sShapeRefName)) {
    removeSymbol(myModuleName(), myPanelName(), "DIAG");
    if(isPanelOpen(_unGraphicalFrame_getModuleId()+"Manage Manager/Driver/Ctrl", myModuleName()))
      PanelOffModule(_unGraphicalFrame_getModuleId()+"Manage Manager/Driver/Ctrl", myModuleName());
    removeSymbol(myModuleName(), myPanelName(), "Manager-Ctrl-Driver");
    setValue("sText", "visible", true);
  }
}

//---------------------------------------------------------------------------------------------------------------------------------------
//@}

