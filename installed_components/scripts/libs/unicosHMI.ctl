/**@name LIBRARY: unicosHMI.ctl

@author: Herve Milcent (AB-CO)

Creation Date: 24/10/2008

Modification History: 
  - 11/11/2011: Herve
  IS-606  unCore - unGraphicalFrame  HMI interface: set system state to unknown for newly added remote system 
 
version 1.0

External Functions:
  - unicosHMI_init: HMI init function
  - unicosHMI_applicationUpdateDeviceList: HMI device data  trigger update function
  - unicosHMI_registerTreeDeviceOverview: HMI register function
	
Internal Functions: 
  - _unicosHMI_TreeDeviceOverview_register: Distributed control register callback function
  
Purpose: 
This library contain generic functions to be used with the unicosHMI.

Usage: Public

PVSS manager usage: UI (WCCOAui) 

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP & Linux.
  . distributed system: yes.
*/

//@{
//---------------------------------------------------------------------------------------------------------------------------------------

// unicosHMI_init
/**
Purpose:
HMI init function. Put here any initialization, global, etc. This function is executed by evalScript

  @param return value: bool, output, true=OK/false=BAD

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . function executed by evalScript->no dpConnect, dpQueryConnect, startThread, etc.
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
  
  @reviewed 2018-09-05 @whitelisted{UNICOSHMIElement}
*/
bool unicosHMI_init()
{
   // initialise the number of max opened module
//  DebugTN("unicosHMI_init");
  g_iMaxOpenedModule = MAX_OPENED_MODULE;
  return unGraphicalFrame_checkLibLoaded(makeDynString());
}

//---------------------------------------------------------------------------------------------------------------------------------------

// unicosHMI_applicationUpdateDeviceList
/**
Purpose:
HMI device data  trigger update function. Callback function triggered when the device data is updated 
Put here any initialization, global, etc. This function is executed by dpConnect

  @param return value: bool, output, true=OK/false=BAD

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSHMIElement}
*/
unicosHMI_applicationUpdateDeviceList(string sdpe, int iCommand, string sdpe2, string sSystemName)
{
//DebugTN("unicosHMI_applicationUpdateDeviceList", iCommand, sSystemName);
  synchronized(g_dsTreeApplication) {
    if(mappingHasKey(g_m_bTreeDeviceOverviewRegisteredSystemInitialized, sSystemName))
    {
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TreeGlobal", "unicosHMI_applicationUpdateDeviceList", sSystemName+" updated");
      g_m_unicosGraphicalFrame_bUpdate[sSystemName] = true;
      if(g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName]) {
       if(!g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName]) {
    //DebugTN("trigger upda", sSystemName);
         g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = true;
         unTreeDeviceOverview_loadAll(sSystemName, g_dsDeviceType);
         unGraphicalFrame_triggerDisplay(DEVUN_ADD_COMMAND, sSystemName);
         g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = false;
       }
      }
    }
    else
      unGenericDpFunctions_debugTN(DEBUG_ONLY_BIT0, "TreeGlobal", "unicosHMI_applicationUpdateDeviceList", sSystemName+" not defined");
  }
//DebugTN("end unicosHMI_applicationUpdateDeviceList", iCommand, sSystemName);
}

//---------------------------------------------------------------------------------------------------------------------------------------


// unicosHMI_registerTreeDeviceOverview
/**
Purpose:
HMI register function. Put here any code to be executed once at startup, etc. This function is executed by startThread

  @param return value: bool, output, true=OK/false=BAD

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSHMIElement}
*/
bool unicosHMI_registerTreeDeviceOverview()
{
    string query = "SELECT '.config:_original.._value' FROM '*' WHERE _DPT = \"_UnDistributedControl\"";
    int iRes = dpQueryConnectSingle("unicosHMI_registerTreeDeviceOverviewCB",true,"Trace Changes in _UnDistributedControl DPs",query,3000);
}

bool unicosHMI_registerTreeDeviceOverviewCB(anytype userData, dyn_dyn_mixed result)
{
  int i, len;
  dyn_string dsSystemNames, dsHost, exInfo;
  dyn_int diSystemId, diPort;
  bool bRes, bConnected;
  dyn_dyn_string ddsTemp;
  
//  DebugTN("unicosHMI_registerTreeDeviceOverview");
/*-------------------------------------------------------------------------------------------*/
// get the list of the systems connected to the system  
/*-------------------------------------------------------------------------------------------*/
  unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemId, dsHost, diPort);
  len = dynlen(dsSystemNames);
  //DebugTN("start unicosHMI_registerTreeDeviceOverview", dsSystemNames[i]);

  for(i=1;i<=len;i++) { if(strpos(dsSystemNames[i], ":") < 0) dsSystemNames[i] = dsSystemNames[i]+":"; }
  dyn_string addedSystems;

  synchronized(g_dsTreeApplication) {

    // check for those that were removed
    for (int i=1;i<=dynlen(g_dsTreeDeviceOverviewRegisteredSystem);i++) {
	string sysName=g_dsTreeDeviceOverviewRegisteredSystem[i];
	if (!dynContains(dsSystemNames,sysName)) {
	    // remove
	    dynRemove(g_dsTreeDeviceOverviewRegisteredSystem,i);
	    i--; // adjust iterator after modifying the list
	    unDistributedControl_deregister("_unicosHMI_TreeDeviceOverview_register", bRes, bConnected, sysName, exInfo);
	    mappingRemove(g_m_bTreeDeviceOverviewRegisteredSystem,sysName);
    	    mappingRemove(g_m_bTreeDeviceOverviewSystemConnected,sysName);
    	    mappingRemove(g_m_ddsTreeDeviceOverviewDeviceList,sysName);
    	    mappingRemove(g_m_bTreeDeviceOverviewRegisteredSystemInitialized,sysName);
    	    mappingRemove(g_m_bTreeDeviceOverview_gettingDeviceRunning,sysName);
    	    mappingRemove(g_m_bTreeDeviceOverview_deviceListAvailable,sysName);
	    mappingRemove(g_m_sTreePVSSSystem,sysName);
    	    mappingRemove(g_m_dsTreeFrontEnd,sysName);
    	    mappingRemove(g_m_dsTreeFrontEndDp,sysName);
    	    mappingRemove(g_m_dsTreeFrontEndType,sysName);
    	    mappingRemove(g_m_dsTreeApplication,sysName);
    	    mappingRemove(g_m_dsTreeFrontEndApplication,sysName);
    	    mappingRemove(g_m_dsTreeSubsystem1,sysName);
    	    mappingRemove(g_m_dsTreeSubsystem2,sysName);
	}
    }

    for(i=1;i<=len;i++) {
      // check if already exists...
      string sysName=dsSystemNames[i];
      if (dynContains(g_dsTreeDeviceOverviewRegisteredSystem,sysName)) continue;

      dynAppend(addedSystems,                            sysName);
      g_dsTreeDeviceOverviewRegisteredSystem[i] =        sysName;
      g_m_bTreeDeviceOverviewRegisteredSystem           [sysName] = false;
      g_m_bTreeDeviceOverviewSystemConnected            [sysName] = false;
      g_m_ddsTreeDeviceOverviewDeviceList               [sysName] = ddsTemp;
      g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sysName] = false;
      g_m_bTreeDeviceOverview_gettingDeviceRunning      [sysName] = false;
      g_m_bTreeDeviceOverview_deviceListAvailable       [sysName] = false;

      g_m_sTreePVSSSystem                               [sysName] = "";
      g_m_dsTreeFrontEnd                                [sysName] = makeDynString();
      g_m_dsTreeFrontEndDp                              [sysName] = makeDynString();
      g_m_dsTreeFrontEndType                            [sysName] = makeDynString();
      g_m_dsTreeApplication                             [sysName] = makeDynString();
      g_m_dsTreeFrontEndApplication                     [sysName] = ddsTemp;
      g_m_dsTreeSubsystem1                              [sysName] = makeDynString();
      g_m_dsTreeSubsystem2                              [sysName] = makeDynString();
    }
  }
  // UNCORE-30: we need to wait for the devicelist file to be created/updated before we trigger a refresh of device list
  // as there seems to be no sensible synchronization, we put an arbitrary delay of 3 secs. here
  delay(3,0);
  for(i=1;i<=dynlen(addedSystems);i++) {
    unDistributedControl_register("_unicosHMI_TreeDeviceOverview_register", bRes, bConnected, addedSystems[i], exInfo);
//DebugTN(bRes, bConnected, dsSystemNames[i]);
  }
  return true;
}



//---------------------------------------------------------------------------------------------------------------------------------------

// unicosHMI_waitTreeDeviceOverviewRegistration
/**
Purpose:
End initialisation function. Put here any code to be executed once at startup after the initialisation. 
This function is executed by evalScript

  @param return value: bool, output, true=OK/false=BAD

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . function executed by evalScript->no dpConnect, dpQueryConnect, startThread, etc.
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
  
  @reviewed 2018-06-22 @whitelisted{UNICOSHMIElement}
*/
bool unicosHMI_waitTreeDeviceOverviewRegistration()
{
  int i, len;
  dyn_string dsSystemNames, dsHost, exInfo;
  dyn_int diSystemId, diPort;
  
//  DebugTN("unicosHMI_waitTreeDeviceOverviewRegistration");
/*-------------------------------------------------------------------------------------------*/
// get the list of the systems connected to the system  
/*-------------------------------------------------------------------------------------------*/
  unDistributedControl_getAllDeviceConfig(dsSystemNames, diSystemId, dsHost, diPort);
  len = dynlen(dsSystemNames);

// wait until the initialization is done
  for(i=1;i<=len;i++) {
   if(strpos(dsSystemNames[i], ":") < 0)
     dsSystemNames[i] = dsSystemNames[i]+":";

   unGraphicalFrame_setLoadingText("Waiting ack ("+i+"/"+len+") from "+dsSystemNames[i]);

   while(!mappingHasKey(g_m_bTreeDeviceOverviewRegisteredSystemInitialized, dsSystemNames[i]))
     delay(1);
   while(!g_m_bTreeDeviceOverviewRegisteredSystemInitialized[dsSystemNames[i]])
     delay(1);
   g_m_bTreeDeviceOverview_gettingDeviceRunning[dsSystemNames[i]] = false;
   unGraphicalFrame_setLoadingText("Connection ("+i+"/"+len+") to "+dsSystemNames[i]+" OK");
//DebugTN("reg done", dsSystemNames[i], g_m_bTreeDeviceOverviewRegisteredSystemInitialized[dsSystemNames[i]], "done");
   unGraphicalFrame_triggerDisplay(0, dsSystemNames[i]);
  }

  unGraphicalFrame_setLoadingText("Initialisation completed");
//  DebugTN("!!end unicosHMI_waitTreeDeviceOverviewRegistration");
  return true;
}

//--------------------------------------------------------------------------------------------------------------------------------

// _unicosHMI_TreeDeviceOverview_register
/**
Purpose:
Distributed control register callback function

  @param sDpe: string, input:  the distributed control DPE of a PVSS system.
  @param bSystemConnected: bool, input: state of the system

Usage: Public

PVSS manager usage: UI (WCCOAui)

Constraints:
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: yes.
  
  @reviewed 2018-06-21 @whitelisted{Thread}
*/
_unicosHMI_TreeDeviceOverview_register(string sDpe, bool bSystemConnected)
{
  string sSystemName;
  string sDp = dpSubStr(sDpe, DPSUB_DP);
  bool bRemote, localConnected;
  dyn_dyn_string ddsTemp;
  int i, len;
  
  synchronized(g_dsTreeApplication) {
// get the system name and check if it is local or remote
    sSystemName = substr(sDp, strlen(c_unDistributedControl_dpName), strlen(sDp))+":";
//  DebugTN("start _unicosHMI_TreeDeviceOverview_register", sSystemName, bSystemConnected);
    unDistributedControl_isRemote(bRemote, sSystemName);
  //DebugN("unTreeDeviceOverview_register", g_m_bTreeDeviceOverviewRegisteredSystem, sDp, bSystemConnected, bRemote, sSystemName, mappingHasKey(g_m_bTreeDeviceOverviewRegisteredSystem, sSystemName));
    if(mappingHasKey(g_m_bTreeDeviceOverviewRegisteredSystem, sSystemName)) {
    //DebugN("has key");
       if(bRemote)
         localConnected = bSystemConnected;
       else
         localConnected = true;
       if(localConnected) { //the system is connected
        // if the system is not already connected add it: trigger the display cb
        // else do nothing
         if(!g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName]) {
            g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName] = true;
            g_m_bTreeDeviceOverviewSystemConnected[sSystemName] = true;
        //    g_m_ddsTreeDeviceOverviewDeviceList[sSystemName] = ddsTemp;
        //DebugTN("set connc", sSystemName);

          if(!g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName]) { // startup, start thread
             g_m_bTreeDeviceOverview_gettingDeviceRunning[sSystemName] = true;
      //     startThread("unTreeDeviceOverview_loadAll", sSystemName, g_dsDeviceType);
             unTreeDeviceOverview_loadAll(sSystemName, g_dsDeviceType);
          }
         }
       }
       else { // the system is disconnected
        // if the system is connected delete it: trigger the display cb
        // else do nothing
         if(g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName]) {
          g_m_bTreeDeviceOverviewRegisteredSystem[sSystemName] = false;
          g_m_bTreeDeviceOverviewSystemConnected[sSystemName] = false;
      //    g_m_ddsTreeDeviceOverviewDeviceList[sSystemName] = ddsTemp;

          g_m_sTreePVSSSystem[sSystemName] = "";
          g_m_dsTreeFrontEnd[sSystemName] = makeDynString();
          g_m_dsTreeFrontEndDp[sSystemName] = makeDynString();
          g_m_dsTreeFrontEndType[sSystemName] = makeDynString();
          g_m_dsTreeApplication[sSystemName] = makeDynString();
          g_m_dsTreeFrontEndApplication[sSystemName] = ddsTemp;
          g_m_dsTreeSubsystem1[sSystemName] = makeDynString();
          g_m_dsTreeSubsystem2[sSystemName] = makeDynString();

          _unTreeDeviceOverview_buildSubsystemList();

      //DebugN("set disc", sSystemName);
          if(g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName]) // startup, start thread
           unGraphicalFrame_triggerDisplay(DEVUN_DELETE_COMMAND, sSystemName);
         }
         g_m_bTreeDeviceOverviewRegisteredSystemInitialized[sSystemName] = true;
       }
     }
  }
//DebugTN("end _unicosHMI_TreeDeviceOverview_register", sSystemName);
}

//---------------------------------------------------------------------------------------------------------------------------------------

//@}
