/**@file
  
// unicosObjects_declarations_core.ctl
definition of the constant and global variables used by unSystemIntegriy and unCore.

@par Creation Date
  18/07/2011

@par Modification History
  - 14/09/2011: Herve
  IS-595: MAIL/SMS: automatic send email at startup is now configurable, by default if no entry ==> no mail at startup
  
  - IS-373: split the unCore libs, files, etc, used in unSystemIntegrity
  
@par Constraints
  . operating system: WXP and Linux.
  . distributed system: yes.

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@author 
  Herve Milcent (EN-ICE)
*/

//@{

const string UN_APPLICATION_DPNAME = "_unApplication";
const string UN_CONFIGURATION_KEY_SEPARATOR = ":";

//------------
//global for the DEBUG utility
//------------
global bit32 g_b32DebugLevel;
global string g_sDebugFilter;

const int DEBUG_ONLY_BIT0 = 1;
const int DEBUG_ONLY_BIT1 = 2;
const int DEBUG_ONLY_BIT2 = 4;
const int DEBUG_ONLY_BIT3 = 8;
const int DEBUG_ONLY_BIT4 = 16;
const int DEBUG_ONLY_BIT5 = 32;
const int DEBUG_ONLY_BIT6 = 64;
const int DEBUG_ONLY_BIT7 = 128;
const int DEBUG_ONLY_BIT8 = 256;
const int DEBUG_ONLY_BIT9 = 1024;
const int DEBUG_ONLY_BIT10 = 2048;
const int DEBUG_ONLY_BIT11 = 4096;
const int DEBUG_ONLY_BIT12 = 8192;
const int DEBUG_ONLY_BIT13 = 16384;
const int DEBUG_ONLY_BIT14 = 32767;
const int DEBUG_ONLY_BIT15 = 65536;

// global variable
global string g_SystemIntegrity_sSystemName;

// Constants for unSendMailt script 
const int UN_SEND_MAIL_TITLE=1;
const int SMS_TITLE=1;
const int MAIL_TITLE=2;
const int UN_SEND_MAIL_BODY=2;
const string UN_SEND_MAIL_KEY_CATEGORY_MAIL_EXTENSION = "_MAIL";
const string UN_SEND_MAIL_KEY_CATEGORY_REPORT_EXTENSION = "_REPORT";
const string UN_SEND_MAIL_KEY_GET_CATEGORY = "GETCATEGORY";
const string UN_SEND_MAIL_KEY_CATEGORY_STARTUP = "_STARTUP";

//@}
