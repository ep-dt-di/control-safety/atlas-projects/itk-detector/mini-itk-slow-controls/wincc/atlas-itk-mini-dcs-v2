/**@name SCRIPT: unBackup.ctl

@author: Celine Vuillermoz (AB-CO)

Creation Date: 2006

Modification History:
  09/02/2012: Cyril
  - IS-694 Connected to the new DPE _unApplication.backupConfiguration.command to trigger the online backup. 
           This allows to remotely launch an online backup on any project
  17/10/2010
  - IS-615  unCore - General  unBackup: add colorDB, help and help/en_US.iso88591 folders 
 
  10/10/2011: Herve
  - IS-609  unCore - General  system name and component version added in all unCore script 
 
  27/06/2011: Herve
  IS-567: backup: PROJ/bin folder included (needed to start the backup project since fwInstallation5.0.4) 
  
  19/10/2009: Frederic
    - init dpe (g_sDpBackupStatusInfo) at the startup of the backup

  15/12/2008: Herve
    - add alsave and save folder in backup
    
  10/10/2006: Herve
    replace unMessageText_sendException by unMessageText_send

  10/07/2006 Frederic:
    fix bug with mv al... files

  20/06/2006 Celine:
    add more error checking

  28/03/2006 Celine:
    Creation of the library : after the online backup 
      - delete archive and alarm files
      - add other directory to the backup excepted folders defined in dsFolderToRemove

      

version 2.0

Purpose: 
Backup of the PVSS project without value and alarm archive, but with installed components, panels, scripts, libs, images, pictures

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . online backup must be configured
  . PVSS version: 3.6 
  . operating system: WXP and Linux.
  . distributed system: no.
*/

//@{

//------------------------------------------------------------------------------------------------------------------------

const string g_sDpBackupStatusInfo = "_unApplication.unBackupStatus";
const string g_sDpBackupCommand = "_unApplication.backupConfiguration.command";
const string g_sDpOnlineBackupStatus = "_unApplication.backupConfiguration.status";
const string g_sDpDefaultOnlineBackupStatus = "_DataManager.Backup.Status";

bool bBackupIsRunning;

//------------------------------------------------------------------------------------------------------------------------

// main
/**
Purpose:
main of the script


Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 
  . operating system: W2000, XP and Linux
  . distributed system: no.
*/
main()
{

  if (!checkBackupFolder()) exit(1);

  int iRes;
  string sDpStatus = "_DataManager.Backup.Status:_original.._value",
  sDpTimeStatus = "_DataManager.Backup.Status:_original.._stime";
  dyn_string exInfo;
  bBackupIsRunning = false;
  // Init DPEs
  if(dpExists(g_sDpBackupStatusInfo))
  {
    dpSet(g_sDpBackupStatusInfo, "");
  }
  
  if(dpExists(g_sDpOnlineBackupStatus))
  {
    dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_STOPPED);
  }
  
  if(dpExists(g_sDpBackupCommand))
  {
    dpSet(g_sDpBackupCommand, UNONLINE_BACKUP_COMMAND_STOP);
  }
  
  // Connect dp to trigger online backup
  if (dpExists(g_sDpBackupCommand))
  {
    iRes = dpConnect("_unBackup_callbackStart", false, g_sDpBackupCommand);  
    if (iRes<0)
    {
      if(isFunctionDefined("unMessageText_send"))
      {
        unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "ERROR", "Backup was not started, CB failed", exInfo);
      }
    }
  }
  else if(isFunctionDefined("unMessageText_send"))
  {
    unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "ERROR", "Backup was not started, DP doesnt exist", exInfo);
  }  
  
  // Connect dp to trigger only file copy  
  if (dpExists(g_sDpDefaultOnlineBackupStatus))
  {
    iRes = dpConnect("_unBackupFilesCB", false, g_sDpDefaultOnlineBackupStatus);  
    if (iRes<0)
    {
      if(isFunctionDefined("unMessageText_send"))
      {
        unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "ERROR", "Backup was not started, CB failed", exInfo);
      }
    }
  }
  else if(isFunctionDefined("unMessageText_send"))
  {
    unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "ERROR", "Backup was not started, DP doesnt exist", exInfo);
  }  
  
  
  delay(0, 10);
  if(isFunctionDefined("unMessageText_send"))
    unMessageText_send("*", "*", getSystemName()+"unBackup", "user", "*", "INFO", "unBackup "+unGenericDpFunctions_getComponentsVersion(makeDynString("unCore"))+" loaded", exInfo);
}
 
/** Check whether backup folder is configured correctly
	
	By default WinCC OA pre-configures the backup to be performed
	in the <PROJ_DIR>/data/OnlineBackup .
	This, in turn, is in collision with the mechanisms that copy
	the components, ie. the functionality of unBackup.
	
	For that reason, if the configured backup folder is a subfolder
	of the project we abort unBackup.ctl execution.
*/
bool checkBackupFolder()
{
	string configuredBackupFolder;
	dpGet(fwInstallationRedu_resolveDp("_DataManager.Backup.InputFile.Device"),configuredBackupFolder);
	dyn_string exceptionInfo;
	string errMsg="";
	
	if (configuredBackupFolder=="") {
		errMsg= "unBackup abort: backup folder not configured";	
	}
	
	configuredBackupFolder=makeNativePath(configuredBackupFolder);
	if (strpos(configuredBackupFolder,makeNativePath(PROJ_PATH))==0) {
		errMsg= "unBackup abort: backup folder invalid (must not be a subfolder of the project):"+configuredBackupFolder;
	}
	
	if (errMsg!="") {
		if(isFunctionDefined("unMessageText_send")) {
			unMessageText_send("*", "*", getSystemName()+"unBackup", "user", "*", "WARNING",errMsg, exceptionInfo);
			delay(0,50); // make sure it is flushed before the manager stops
		}
		throwError(makeError("",PRIO_FATAL,ERR_PARAM,0,errMsg));
		return false;
	}
	return true;
}

//------------------------------------------------------------------------------------------------------------------------
// _unBackup_updateStatus
/**
Purpose:
  Updates the current state of the backup
  
  @param sStatus: string, input: the new status to display.
  
  Usage: Internal
*/
_unBackup_updateStatus(string sStatus)
{
  dpSet(g_sDpBackupStatusInfo, sStatus);
  // Always leave a short delay for the information to spread out
  delay(2);
}

//------------------------------------------------------------------------------------------------------------------------
// _unBackup_callbackStart
/**
Purpose:
  Callback function that will call the backup on a different thread. 
  If no thread is used, and someone tries to trigger the backup while one is already running, this new callback will wait for the end of the first one.
  So the UI will display a fail, whereas as soon as the previous backups is over a new one will start without notice
  
  Usage: Internal
    
  @reviewed 2018-06-22 @whitelisted{Callback}
*/
_unBackup_callbackStart(string sDpCommand, int iCommand) 
{
  if (iCommand == UNONLINE_BACKUP_COMMAND_START)
  {
    if (!bBackupIsRunning)
    {    
      bBackupIsRunning = true;
      startThread("_unBackup_startOnlineBackup", sDpCommand, iCommand);  
    }
  }
}




//------------------------------------------------------------------------------------------------------------------------
// _unBackup_startOnlineBackup
/**
Purpose:
  Function to trigger online backup after creating a file as in the default panel

  @param sDpCommand: string, input, data point name
  @param iCommand: int, input, data point value

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:

  @reviewed 2018-06-22 @whitelisted{Thread}
*/
_unBackup_startOnlineBackup(string sDpCommand, int iCommand)
{  
  string sSystemName = getSystemName();       
  if (!globalExists("host1"))
  {
    initHosts();
  }  
  
  string sDpName = "_DataManager";  
  // Create the needed file before launching the online backup
  string sPath = PROJ_PATH;
  addGlobal("fName",STRING_VAR);
  if ((host2 != "") && (host1 != host2))
  {
    if (_WIN32)
    {
      for (int i = strlen(sPath) - 1 ; i >= 0 ; i--)
      {
        if (i < strlen(sPath) - 1 && (sPath[i] == "\\" || sPath[i] == "/"))
        {
          sPath = substr(sPath, i, strlen(sPath)-i);
          break;
        }
      }
      fName = "\\\\" + host1 + sPath + "db\\" + backupReadmeFileName("db");
    }
    else
    {
      fName = sPath + "db/"+backupReadmeFileName("db");
    }
  }
  else
  {
    fName = sPath + "db/"+backupReadmeFileName("db");
  }
  
  // Remove any remain of file that can be in the db repertory (after a failed backup for instance)    
  dyn_string dsFiles = getFileNames(dirName(fName),"*.txt");   
  for (int i = 1 ; i <= dynlen(dsFiles) ; i++)
  {
    if (_WIN32)
    {
      system("cmd /C rm " + dirName(fName) + "\\" + dsFiles[i]);
    }
    else
    {
      system("rm " + dirName(fName) + "/" + dsFiles[i]);
    }
  }   
    
    
  file fd = fopen(fName, "w");  
  if (fd == 0)
  {
     DebugN("Error Onlinebackup: Cannot open " + fName);
      _unBackup_updateStatus("BAD-Online backup did not start on system \"" + sSystemName + "\", could not write backup on disk."); 
   
     // Reset backup state  
    bBackupIsRunning = false;  
    _unBackup_updateStatus(""); 
    
     return;
  }    
  fclose(fd);

  
  // Start online backup
  dpSet(sDpName+".Backup.Command:_original.._value","1");
    

  // Wait 5 seconds for status to change  
  dyn_anytype daReturnValue;  
  time tTimeOut = 5;    
  bool bTimerExpired;
  dpWaitForValue(
      makeDynString("_DataManager.Backup.Status:_online.._value"), 
      makeDynAnytype(), 
      makeDynString("_DataManager.Backup.Status:_online.._value"), 
      daReturnValue,
      tTimeOut,
      bTimerExpired
  );
    
  if (bTimerExpired)
  {
    // If it has not changed within 5 seconds we consider there is an error
    _unBackup_updateStatus("BAD-Online backup did not start on system \"" + sSystemName + "\".");
    
     // Reset backup state  
    bBackupIsRunning = false;  
    _unBackup_updateStatus(""); 
    
    return; 
  }
    
  // OK, running
  // dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_RUNNING);
  _unBackup_updateStatus("Online backup is running on system \"" + sSystemName + "\"."); 
      
  // Wait for end
  tTimeOut = 5;    
  dpWaitForValue(
    makeDynString("_DataManager.Backup.Status:_online.._value"), 
    makeDynAnytype(), 
    makeDynString("_DataManager.Backup.Status:_online.._value"), 
    daReturnValue,
    0,
    bTimerExpired
  );
      
  // Online backup is over
  // Remove previously created file
  if (_WIN32)
  {
    system("cmd /C rm "+fName);
  }
  else
  {
    system("rm "+fName);
  }
    
  // Launch custom backup step
  int iBackupStatus;
  dpGet("_DataManager.Backup.Status", iBackupStatus);
  if (iBackupStatus < 0) // Error
  {
     // dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_UNKOWN_ERROR);  // Read errors
    dyn_string dsErrorList;
    string sErrors;
    dpGet(sSystemName + "_DataManager.Backup.OutputFile.FileError", dsErrorList);
    for (int i = 1 ; i <= dynlen(dsErrorList) ; i++)
    {
      sErrors += dsErrorList[i] + "\n";
    }
    _unBackup_updateStatus("BAD-Backup encountered error(s) on system \"" + sSystemName + "\".\n" + sErrors);
    
     // Reset backup state  
    bBackupIsRunning = false;  
    _unBackup_updateStatus(""); 
    return;    
  }      
  else
  {
     // dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_DONE);  
    _unBackup_updateStatus("Online backup is over on system \"" + sSystemName + "\"."); 
  }
    
  // Launch script custom backup and archive cleaning
  _unBackupFiles();   
}


//------------------------------------------------------------------------------------------------------------------------
// _unBackupFilesCB
/**
Purpose:
  Callback to trigger the backup of other files. Is called when online backup state

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 
  . operating system: W2000, XP and Linux
  . distributed system: no.
  
  @reviewed 2018-06-22 @whitelisted{Callback}
*/
_unBackupFilesCB(string sCommandDp, int iCommandValue)
{
  if ((iCommandValue == 0) && (!bBackupIsRunning))
  {
    bBackupIsRunning = true;
    _unBackupFiles();
  }  
}


//------------------------------------------------------------------------------------------------------------------------
// _unBackupFiles
/**
Purpose:
  Function to backup other files

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 
  . operating system: W2000, XP and Linux
  . distributed system: no.
*/
_unBackupFiles()
{
  string sSystemName = getSystemName();    
  int iRes, deviceSimu;
  int i;
  string proj_path,  sPath, device,dp, sResult, sPathBack, sSourcePath,sPathBackBis, sCommand;
  dyn_string files=makeDynString(""), dsAlarmDir;
  time tCurrentTime;
  dyn_string exInfo;
  
    
  dp = "_ValueArchiveMedia";  
  
//   if(dpExists(g_sDpBackupStatusInfo))  
//   {
//     dpSet(g_sDpBackupStatusInfo, "backup running");
//   }
    
  _unBackup_updateStatus("Backing up files and cleaning archives on system \"" + sSystemName + "\"."); 
  
  // Backup is running custom file copy operation
  // dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_UNBACKUP_RUNNING);
		
  dpGet("_DataManager.Backup.InputFile.Type:_original.._value",device,
        "_DataManager.Backup.InputFile.Device:_original.._value",sPath);
          
  if ( _WIN32)
  {    
    sPathBack = sPath + PROJ + "_tmp\\" + PROJ + "\\";
    sPathBackBis = sPath + PROJ + "_tmp\\";
  }
  else   
  {
    sPathBack = sPath + PROJ + "_tmp/" + PROJ;
    sPathBackBis = sPath + PROJ + "_tmp";
  }
          
  //make a backup of the old backup folders
  if ( _WIN32)
  {
    sCommand="cmd /c rmdir /S /Q "+substr(sPath, 0, strlen(sPath)-1)+"_old";
    iRes = system(sCommand); //Remove the old backup
    sCommand="cmd /c move "+sPath+PROJ+" "+substr(sPath, 0, strlen(sPath)-1)+"_old";   
    iRes = system(sCommand); 
  }
  else
  {
    sCommand="rm -r "+substr(sPath, 0, strlen(sPath)-1)+"_old";
    iRes = system(sCommand); //Remove the old backup
    sCommand="mv -f "+sPath+PROJ+" "+substr(sPath, 0, strlen(sPath)-1)+"_old";      
    iRes = system(sCommand); 
  }
  DebugTN( "Copy the old backup directories to Backup_old directory: " + sCommand); 
    
    // Delete Archive and alarm files
  getRecursiveDirectoryNames(files, sPath);
  for (i=dynlen(files);i>0;i--)
  {
    if (patternMatch("db*wincc_oa*VA_*",files[i])) 
    {
      if ( _WIN32)
      {
        sCommand="cmd /c rm -r "+sPath+substr(files[i],0,strlen(files[i])-1);           
        system(sCommand); 
      }
      else
      {
        sCommand="rm -r "+sPath+substr(files[i],0,strlen(files[i])-1);             
        system(sCommand); 
      }

      DebugTN("Remove archive: " + sCommand);
    }
     
    if (
        patternMatch("db*wincc_oa*al*1*",files[i]) || patternMatch("db*wincc_oa*al*2*",files[i]) || patternMatch("db*wincc_oa*al*3*",files[i]) ||
        patternMatch("db*wincc_oa*al*4*",files[i]) || patternMatch("db*wincc_oa*al*5*",files[i]) || patternMatch("db*wincc_oa*al*6*",files[i]) ||
        patternMatch("db*wincc_oa*al*7*",files[i]) || patternMatch("db*wincc_oa*al*8*",files[i]) || patternMatch("db*wincc_oa*al*9*",files[i])
       ) 
    {
      dynAppend(dsAlarmDir,files[i]);
    }
  }
    
  if(dynlen(dsAlarmDir)>1)
  {
    dynSortAsc(dsAlarmDir);
    for (i=1;i<dynlen(dsAlarmDir);i++) // IS-1916: we want to keep the last alarm dir; "<" is intentional
    {
      if ( _WIN32)
      {
        sCommand="cmd /c rm -r "+sPath+substr(dsAlarmDir[i],0,strlen(dsAlarmDir[i])-1);
        system(sCommand); 
      }
      else
      {
        sCommand="rm -r "+ sPath + substr(dsAlarmDir[i],0,strlen(dsAlarmDir[i])-1);
        system(sCommand);
      }
      DebugTN("Remove alarm: " + sCommand);
    }
  }
    
  DebugTN("Create Configuation file for WCCOAtoolMedia");
  if(device == "DAT")
  {
    deviceSimu = 0;
  }
  else
  {
    deviceSimu = 1;
  }
    
/*
  if ( _WIN32) proj_path = substr(PROJ_PATH, 0, strlen(PROJ_PATH)-(strlen(PROJ)+1));
  else proj_path = substr(PROJ_PATH, 0, strlen(PROJ_PATH)-(strlen(PROJ)+2));
*/
  proj_path = substr(PROJ_PATH, 0, strlen(PROJ_PATH)-(strlen(PROJ)+1));

  iRes = _unBackup_writeFile(proj_path, sPathBackBis, deviceSimu, dp);
    
  if(iRes>=0) 
  {
    DebugTN("Add db online backup to the project backup");
    if ( _WIN32)
    {
      sCommand="cmd /c move "+sPath+"\\db " +sPathBack;
      iRes = system(sCommand); //Copy the online backup to the directory sPath/PROJ 
  		 _unBackup_addMissingFolder(sPathBack);		 
      if(iRes>=0)
      {
        sCommand="cmd /c ren \"" + sPath + PROJ + "_tmp\\\" " + PROJ;           
        iRes = system(sCommand); //Rename the temporary folder
      }  
    }
    else
    {
      sCommand="mv -f "+sPath+"/db " +sPathBack+"/db";                            
      iRes = system(sCommand);
   		_unBackup_addMissingFolder(sPathBack + "/");
      if(iRes>=0)
      {
        sCommand="mv -f "+sPath+PROJ+"_tmp "+ sPath+PROJ;                                        
        iRes = system(sCommand);
      }  
    }
    
    DebugTN("Rename the temporary folder: " + sCommand);
  
    if(iRes >=0)
    {
      if(isFunctionDefined("unMessageText_send"))
      {
        unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "INFO", "Backup completed", exInfo);
      }
      if(dpExists(g_sDpBackupStatusInfo))  
      {
        if (isdir(sPath + "/" + unBackup_getProjName()))
        {            
          _unBackup_updateStatus("OK"); 
          // dpSet(g_sDpBackupStatusInfo, "OK");  
          // dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_UNBACKUP_DONE);
        }
        else // Error : folder does not exist
        {
          _unBackup_updateStatus("BAD-Backup folder \"" + sPath + "/" + unBackup_getProjName() + "\" not found on system \"" + sSystemName + "\"."); 
          // dpSet(g_sDpBackupStatusInfo, "BAD-Backup folder not found.");    
          // dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_UNKOWN_ERROR);      
        }
      }
    }    
    else   
    {
      if(isFunctionDefined("unMessageText_send"))
      {
        unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "INFO", "Backup failed", exInfo);
      }
      if(dpExists(g_sDpBackupStatusInfo))
      {
        _unBackup_updateStatus("BAD-Error in process on system \"" + sSystemName + "\"."); 
        // dpSet(g_sDpBackupStatusInfo, "BAD-Error in process.");    
        // dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_UNKOWN_ERROR);          
      }
      DebugTN("Backup failed");    
    }  
  }
  else   
  {
    if(isFunctionDefined("unMessageText_send"))
    {
      unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "INFO", "Backup failed", exInfo);
    }
    if(dpExists(g_sDpBackupStatusInfo))
    {
      _unBackup_updateStatus("BAD-Error in process on system \"" + sSystemName + "\"."); 
      //dpSet(g_sDpBackupStatusInfo, "BAD");    
      //dpSet(g_sDpOnlineBackupStatus, UNONLINE_BACKUP_STATUS_UNKOWN_ERROR);          
    }      
    DebugTN("Backup failed");    
  }  
  
  
  // Backup finally over
  bBackupIsRunning = false;  
  _unBackup_updateStatus(""); 
}




//------------------------------------------------------------------------------------------------------------------------
// _unBackup_writeFile
/**
Purpose:
Create the WCCOAtoolMedia file and execute it

  @param source: string, input, folder to copy
  @param destination: string, input, destination folder
  @param return value: int, output, 0=OK/ <0=error

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 
  . operating system: W2000, XP and Linux
  . distributed system: no.
*/
int _unBackup_writeFile(string source,string destination, int deviceSimulation,  string dp)
{
  file       f;
  int        i;
  string     device, sHost, sTempFileIn=DATA_PATH+"UNTEMPFILE_IN", sTempFileOut = DATA_PATH+"UNTEMPFILE_OUT";
  
  int        iAnswer, iReturn;
  dyn_string ds, dsFolterToRemove ;
  dyn_string files,dsTemp, exInfo;

  files = makeDynString("");
    
  if(_WIN32)
  {
    dsFolterToRemove = makeDynString(PROJ+"\\*",PROJ+"\\db",PROJ+"\\log\\", PROJ+"\\help\\", PROJ+"\\data\\xml_sender",PROJ+"\\data\\OnlineBackup", PROJ+"\\data\\OnlineBackup_old");
  }  
  else
  {
    dsFolterToRemove = makeDynString(PROJ+"/*",PROJ+"/db",PROJ+"/log/", PROJ+"/help/", PROJ+"/data/xml_sender", PROJ+"/data/OnlineBackup", PROJ+"/data/OnlineBackup_old");
  } 
  
  getRecursiveDirectoryNames(files, source); // get all files names in the PVSS_projects/Project_name directory
  for (i=1;i<=dynlen(files);i++) //for every files check if it's not in the list of removed folder
  {   
     if((strpos(files[i],dsFolterToRemove[1])!=0) && (strpos(files[i],dsFolterToRemove[2])!=0) &&
     (strpos( files[i],dsFolterToRemove[3])!=0) && (strpos( files[i],dsFolterToRemove[4])!=0) &&
     (strpos(files[i],dsFolterToRemove[5])!=0) &&
	 files[i]!="*")
     {
      dynAppend(dsTemp,files[i]);
    }
  }
  
  dynUnique(dsTemp);
  dynClear(files);
  files =dsTemp;

  ds[1]=source;
  dynAppend(ds,files); files=ds;
  device = destination;
  
  f=fopen(sTempFileIn,"w");
  fputs("backup",f);fputs("\n",f);                 
  fputs(destination,f);fputs("\n",f);                  
  fputs(deviceSimulation==0?"DAT\n":"FILE\n",f);  
  fputs("false",f);fputs("\n",f);                 
  
  for (i=1; i<= dynlen(files);i++)                
  {
    fputs(files[i]+"\n",f);
  }
                  
  if (ferror(f) == 0)
  {
    fclose(f);
    DebugTN("Start WCCOAToolMedia");
    if (_WIN32)
       iReturn = system( PVSS_PATH + "\\bin\\WCCOAtoolMedia "+sTempFileIn+" "+sTempFileOut);
    else
        iReturn = system( PVSS_PATH + "/bin/WCCOAtoolMedia "+sTempFileIn+" "+sTempFileOut);   
    DebugTN("Stop WCCOAToolMedia"); 
  }
  else 
  {
    if(isFunctionDefined("unMessageText_send"))
      unMessageText_send("*", "*", getSystemName()+"Backup", "user", "*", "ERROR", "WCCOAtoolMedia file was not created", exInfo);
    iReturn = -1;
  }
  return(iReturn);
}

//------------------------------------------------------------------------------------------------------------------------

// _unBackup_addMissingFolder
/**
Purpose:
Create missing folder

  @param sPath: string, input, path of the bck 

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 
  . operating system: W2000, XP and Linux
  . distributed system: no.
*/
bool _unBackup_addMissingFolder(string sPath)
{
string sCommand;
dyn_bool dbResult;
bool bResult = false;
    
// add bin folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "bin"));
// add colorDB folder
  dynAppend(dbResult, _unBackup_createFolder(sPath, "colorDB"));
// add config folder
  dynAppend(dbResult, _unBackup_createFolder(sPath, "config"));
// add data folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "data"));
// add dplist folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "dplist"));
// add help folder
  dynAppend(dbResult, _unBackup_createFolder(sPath, "help"));
// add help/en_US.iso88591 folder
  dynAppend(dbResult, _unBackup_createFolder(sPath, "help/en_US.utf8"));
// add images folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "images"));
// add log folder
  dynAppend(dbResult, _unBackup_createFolder(sPath, "log"));
// add msg folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "msg"));
// add msg/en_US.iso88591 folder
  dynAppend(dbResult, _unBackup_createFolder(sPath, "msg/en_US.utf8"));
// add panels folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "panels"));
// add pictures folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "pictures"));
// add printers folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "printers"));
// add scripts folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "scripts"));
// add scripts/libs folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "scripts/libs"));
// add source folder if not already existing
  dynAppend(dbResult, _unBackup_createFolder(sPath, "source"));

  dynUnique(dbResult);
  if(dynlen(dbResult) ==1)
    bResult = dbResult[1];
  return bResult;
}
 
//------------------------------------------------------------------------------------------------------------------------

// _unBackup_createFolder
/**
Purpose:
Create missing folder

  @param sPath: string, input, path of the bck 
  @param sFolder: string, input, folder to create 

Usage: Internal

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 
  . operating system: W2000, XP and Linux
  . distributed system: no.
*/
bool _unBackup_createFolder(string sPath, string sFolder)
{
  string sCommand;
  bool bResult;
    
  sCommand = sPath + sFolder;
  if (_WIN32)	strreplace(sCommand, "/", "\\");
  bResult = mkdir(sCommand,"777");

  return bResult;
}

//------------------------------------------------------------------------------------------------------------------------



//@}
