// IS-1718; Initial setup for the UNICOS Menu
// we need to initialize the menu entries in the init script,
// and hence for that we need the unGraphicalFrame.ctl loaded to
// provide the functions with default menu entries;
// This should be changed once IS-1631 is implemented!
#uses "unGraphicalFrame.ctl"

int main()
{
    DebugTN("unCore_init_installMenu running");
    int rc=unCore_init_setupMenus();
}


int unCore_init_setupMenus()
{
    int iTime;
    int iAttempt=0;
    while (iAttempt<=10 &&  !dpExists("_unApplication")) {
	DebugTN("unCore_init_installMenu: Waiting for _unApplication datapoint to be created");
	iAttempt++;
	delay(1,0);
    }
    
    if (iAttempt>=10) {
	DebugTN("********** ERROR during init configuration: _unApplication DP does not exist. Contact UNICOS support team");
	return 1;
    }

    // do the initialization with defaults only the first time:
    dpGet("_unApplication.menu.menuConfiguration:_original.._stime", iTime);
    if(iTime ==0) {
      dyn_string dsMenu;
      unGraphicalFrame_getUNICOSDefaultMenuConfiguration(dsMenu);
      DebugTN("unCore.init: initial configuration of the UNICOS menu");
      dpSet("_unApplication.menu.menuConfiguration", dsMenu);
    }
    dpGet("_unApplication.menu.menuFileAccessControl:_original.._stime", iTime);
    if(iTime ==0) {
      dyn_string dsMenu;
      unGraphicalFrame_getUNICOSDefaultFileAccessControl(dsMenu);
      DebugTN("unCore.init: initial configuration of the file access control");
      dpSet("_unApplication.menu.menuFileAccessControl", dsMenu);
    }
    
    return 0;
}


