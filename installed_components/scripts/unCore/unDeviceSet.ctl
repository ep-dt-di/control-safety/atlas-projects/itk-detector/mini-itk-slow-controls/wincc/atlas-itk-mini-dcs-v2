/**@file

// unDeviceSet.ctl
This script does:
  - the query of the archive DPE list.

@par Creation Date
  31/03/2011

@par Modification History
  24/11/2011: Herve
  - IS-675: UNICOS scripts: system name added in the MessageText log 
  
  10/10/2011: Herve
  - IS-609  unCore - General  system name and component version added in all unCore script 
 
  - IS-472
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  CTRL

@author 
  Herve Milcent (EN-ICE)
*/

//@{

//------------------------------------------------------------------------------------------------------------------------
// main
/** main
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

*/
main()
{
  dyn_string exceptionInfo;
  
  if(dpExists("_unApplication.commandInterface.request"))
  {
    dpConnect("unDeviceSet_CB", false, "_unApplication.commandInterface.request");
    unMessageText_send("*", "*", getSystemName()+"DeviceSet", "user", "*", "EXPERTINFO", "DeviceSet initialisation successfully done" , exceptionInfo);
  }
  else
    unMessageText_send("*", "*", getSystemName()+"DeviceSet", "user", "*", "EXPERTINFO", "DeviceSet initialisation failed" , exceptionInfo);
  
  delay(0, 10);
  if(isFunctionDefined("unMessageText_send"))
    unMessageText_send("*", "*", getSystemName()+"unDeviceSet", "user", "*", "INFO", "unDeviceSet "+unGenericDpFunctions_getComponentsVersion(makeDynString("unCore"))+" loaded", exceptionInfo);
}

//------------------------------------------------------------------------------------------------------------------------
// unDeviceSet_CB
/** Callback function, this function:
  - returns the list of DPE per requested archive
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sDp input, the device dp
@param sDeviceType input, the device type
@param dsNames output, the archive names (Bool, Analog and Event)

@reviewed 2018-06-25 @whitelisted{Callback}
*/
unDeviceSet_CB(string sDp, dyn_string dsData)
{
  int iCommand;
  string sId;
  dyn_string dsParameters, dsReturn;
  
  if(dynlen(dsData) > 2) // something to do
  {
    dsParameters = dsData;
    sId = dsParameters[1];
    dynRemove(dsParameters, 1);
    iCommand = (int) dsParameters[1];
    dynRemove(dsParameters, 1);
    switch(iCommand)
    {
      case DEVICESET_GETARCHIVE_DPE:
        unDeviceSet_getArchiveDPE(sId, iCommand, dsParameters);
        break;
      case DEVICESET_SET_ALIAS:
        break;
      case DEVICESET_SET_DESCRIPTION:
        break;
      case UN_SYSTEMINTEGRITY_EXTENDED_DIAGNOSTIC:
        break;
      default:
        break;
    }
  }
}

//------------------------------------------------------------------------------------------------------------------------
// unDeviceSet_getArchiveDPE
/** This function does the asciiexport of the all the archive config
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sId input, the id of the query
@param iCommand input, the command
@param dsArchive input, the list of archive to query
*/
unDeviceSet_getArchiveDPE(string sId, int iCommand, dyn_string dsArchive)
{
  string sFile = PROJ_PATH + "data/asciiOutput.export";
  string asciiManager = PVSS_BIN_PATH + "WCCOAascii";
  string cmd;
  int err, i, len;
  dyn_string dsReturn;
  
  cmd = asciiManager + " -filter P:_archive -out " + sFile;
  if (_WIN32) 
  {
    err = system("cmd /c " + cmd);
    if (err != 0)
    {
      fwInstallation_throw("Could not import properly the file " + " (Error number = " + err +")");
    }
  }
  else  //LIN
  {
    err = system(cmd);
    if (err != 0)
    {
      fwInstallation_throw("Could not import properly the file " + " (Error number = " + err +")");
    }
  }

  len = dynlen(dsArchive);
  for(i=1;i<=len;i++)
  {
    dsReturn = unDeviceSet_Analyse_data(sFile, dsArchive[i]);
    dynInsertAt(dsReturn, makeDynString(sId, iCommand, dsArchive[i]), 1);
    dpSet("_unApplication.commandInterface.result", dsReturn);
  }
}
    
//------------------------------------------------------------------------------------------------------------------------
// unDeviceSet_getArchiveDPE
/** This function does the asciiexport of the all the archive config
  
@par Constraints
  None

@par Usage
  Public

@par PVSS managers
  Ui, CTRL

@param sArchive input, the archive to query
@return the list of DPE for the given archive
*/
dyn_string unDeviceSet_Analyse_data(string sFile, string sArchive)
{
  string sBuffer;
  dyn_string dsBuffer, dsResult, dsDPE;
  int len, i;
  
  fileToString(sFile, sBuffer);
  strreplace(sBuffer,"\r","");
  dsBuffer = strsplit(sBuffer, "\n");
  dsResult = dynPatternMatch("*	"+sArchive+"	*", dsBuffer);
  len=dynlen(dsResult);
  for(i=1;i<=len;i++)
  {
    dynAppend(dsDPE, getSystemName()+substr(dsResult[i], 0, strpos(dsResult[i], "\t")));
  }
  return dsDPE;
}

//------------------------------------------------------------------------------------------------------------------------

//@}
