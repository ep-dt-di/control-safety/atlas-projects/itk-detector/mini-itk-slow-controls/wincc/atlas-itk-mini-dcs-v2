/**@name SCRIPT: unSystemIntegrity_computeStatistics.ctl

@author: Herve Milcent (EN-ICE)

Creation Date: 24/09/2009

Modification History:

version 2.0

Purpose: 
This script calculate the number of alarm per _UnSystemAlarm per day and set the statistics DPE.
This script is triggered by the scheduler.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 SP2 
  . operating system: WXP and Linux.
*/

//@{

//--------------------------------------------------------------------------------------------------------------------------------
// main
/**
Purpose:
This is the main of the script. It does a dpQuery with the interval sets to the interval of the scheduler DP.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
  . PVSS version: 3.6 SP2 
  . operating system: WXP and Linux.
*/
main()
{
  int iTime, iStart, iEnd;
  time t;
  string sTimeStart, sTimeEnd;
  string sQuery;
  dyn_dyn_anytype dda;
  dyn_errClass dcErr;
  dyn_string dsDp;
  int i, len, lenDDA, iValue;
  mapping mStats;
  string sKey;

  if(dpExists(SYSTEMINTEGRITY_COMPUTESTATISTICS+".time.timedFunc.interval")) 
  {
    dsDp = dpNames("*", "_UnSystemAlarm");
    len = dynlen(dsDp);
    for(i=1;i<=len;i++)
    {
      mStats[dpSubStr(dsDp[i], DPSUB_SYS_DP)] = 0;
    }
    dpGet(SYSTEMINTEGRITY_COMPUTESTATISTICS+".time.timedFunc.interval", iTime);
    iEnd = (int)getCurrentTime();
    iStart = iEnd - iTime;
//SELECT ALERT '_alert_hdl.._value' FROM '_unSystemAlarm_*' WHERE _DPT = "_UnSystemAlarm" TIMERANGE("2009.09.23 22:07:41","2009.09.23 23:17:57",1,0)
    t= iStart;
    sTimeStart = year(t)+"."+month(t)+"."+day(t)+" "+hour(t)+":"+minute(t)+":"+second(t);
    t= iEnd;
    sTimeEnd = year(t)+"."+month(t)+"."+day(t)+" "+hour(t)+":"+minute(t)+":"+second(t);
    sQuery = "SELECT ALERT '_alert_hdl.._value' FROM '_unSystemAlarm_*' WHERE _DPT = \"_UnSystemAlarm\" TIMERANGE(\""+sTimeStart+"\",\""+sTimeEnd+"\",1,0)";
    dpQuery(sQuery, dda );
//DebugTN("start unSystemIntegrity_statistics", dda);
    dcErr  = getLastError();
    if(dynlen( dcErr ) > 0 )
    {
      // set all stats to -1
      for(i=1;i<=len;i++)
      {
        mStats[dpSubStr(dsDp[i], DPSUB_SYS_DP)] = -1;
      }
    }
    else 
    {
      lenDDA = dynlen(dda);
      if(lenDDA>=1) {
        for(i=2;i<=lenDDA;i++) {
          sKey = dpSubStr(dda[i][1], DPSUB_SYS_DP);
          iValue = dda[i][3];
//  DebugN(sKey, iValue, c_unSystemIntegrity_no_alarm_value);
          if(iValue != c_unSystemIntegrity_no_alarm_value) {
            if(mappingHasKey(mStats, sKey)) {
              mStats[sKey] +=1;
            }
            else
              mStats[sKey] = 1;
          }
        }
      }
    }
    len = mappinglen(mStats);
    for(i=1;i<=len;i++) {
      sKey = mappingGetKey(mStats, i);
      if(dpExists(sKey+".statistic.alarmPerDay"))
        dpSet(sKey+".statistic.alarmPerDay", mStats[sKey]);
    }
//DebugTN("start unSystemIntegrity_statistics", dda, mStats);
  }
}

//--------------------------------------------------------------------------------------------------------------------------------

//@}
